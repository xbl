#!/bin/sh
#	Name:   build.sh Build X.Smiles using Ant
#	Author: Shane Curcuru

# Alternatively, you can just call "ant" 

echo "X-Smiles Build"
echo "-------------"

_JAVACMD=$JAVA_HOME/bin/java
if [ "$JAVA_HOME" = "" ] ; then
    echo "Warning: JAVA_HOME environment variable is not set."
    _JAVACMD=java
fi

# Use _underscore prefix to not conflict with user's settings
# Default to UNIX-style pathing
CLPATHSEP=:
# if we're on a Windows box make it ;
uname | grep WIN && CLPATHSEP=\;

# Default locations of jars we depend on to run Ant on our build.xml file
if [ "$ANT_HOME" = "" ] ; then
    ANT_HOME=.
fi
if [ "$ANT_JAR" = "" ] ; then
    ANT_JAR=./bin/ant.jar
fi
if [ "$PARSER_JAR" = "" ] ; then
    PARSER_JAR=../bin/lib/xerces.jar
fi
if [ "$FOP_JAR" = "" ] ; then
    FOP_JAR=../bin/lib/fop.jar
fi
if [ "$XALAN_JAR" = "" ] ; then
    XALAN_JAR=../bin/lib/endorsed/xalan.jar${CLPATHSEP}../bin/lib/xml-apis.jar
fi
OPTIONAL_JUNIT_JAR=bin/optional.jar${CLPATHSEP}bin/junit.jar${CLPATHSEP}bin/jdepend.jar${CLPATHSEP}bin/styler.jar${CLPATHSEP}bin/Tidy.jar
_CLASSPATH="$ANT_JAR${CLPATHSEP}$PARSER_JAR${CLPATHSEP}$XALAN_JAR${CLPATHSEP}$FOP_JAR${CLPATHSEP}${OPTIONAL_JUNIT_JAR}${CLPATHSEP}$CLASSPATH"

# Attempt to automatically add system classes to _CLASSPATH
if [ -f $JAVA_HOME/lib/tools.jar ] ; then
  _CLASSPATH=${_CLASSPATH}${CLPATHSEP}${JAVA_HOME}/lib/tools.jar
fi

if [ -f $JAVA_HOME/lib/classes.zip ] ; then
  _CLASSPATH=${_CLASSPATH}${CLPATHSEP}${JAVA_HOME}/lib/classes.zip
fi


echo "Starting Ant with targets: $@"
echo "        ...with classpath: $_CLASSPATH"

"$_JAVACMD" $JAVA_OPTS -Djava.endorsed.dirs=../bin/lib/endorsed -Dant.home=$ANT_HOME -classpath "$_CLASSPATH" org.apache.tools.ant.Main $@




