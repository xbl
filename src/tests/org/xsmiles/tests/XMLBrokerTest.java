package org.xsmiles.tests;

import java.util.*;

import javax.swing.*;

import junit.framework.*;
import junit.extensions.*;



import org.apache.xerces.dom.*;
import org.w3c.dom.*;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XMLBroker;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * This test tests the XMLBroker class. Because XMLBroker uses reflection to 
 * create classes that have registered to it, this uses 2 external classes:
 * TestMLFCA & B.
 * @author Mikko Honkala
 */
public class XMLBrokerTest extends TestCase {

	private XMLBroker broker;
	private DocumentImpl doc;

		private final String nsA = "http://a/";
	private final String MLFCA = "org.xsmiles.tests.TestMLFCA";
	private final String nsB = "http://b/";
	private final String MLFCB = "org.xsmiles.tests.TestMLFCB";

	public XMLBrokerTest(String name)
	{
		super(name);
	}
	public static void main (String[] args)
	{
		junit.textui.TestRunner.run (suite());
	}
	protected void setUp()
	{
		broker.registerMLFCNS(nsA,MLFCA);
		broker.registerMLFCNS(nsB,MLFCB);
		doc = new DocumentImpl();
		broker = new XMLBroker();
		TestMLFCA.classcount = 0;
		TestMLFCB.classcountb = 0;
		//broker.registerMLFCNS(nsA,"fi.hut.tml.xsmiles.mlfc.xforms.XFormsMLFC");
	}
	public static Test suite()
	{
		return new TestSuite(XMLBrokerTest.class);
	}

	public void testClassNSCreation()
	{
		broker.createElementNS(doc,new String(""+nsA+""),"root");
		assertEquals("classcount wrong",1,TestMLFCA.classcount);
		assertEquals("classcount wrong",0,TestMLFCB.classcountb);
		broker.createElementNS(doc,new String(""+nsA+""),"nonRoot");
		assertEquals("classcount wrong",1,TestMLFCA.classcount);
		assertEquals("classcount wrong",0,TestMLFCB.classcountb);
		broker.createElementNS(doc,new String(""+nsB+""),"nonRoot");
		assertEquals("classcount wrong",1,TestMLFCA.classcount);
		assertEquals("classcount wrong",1,TestMLFCB.classcountb);
		broker.createElementNS(doc,new String(""+nsB+""),"nonRoot2");
		assertEquals("classcount wrong",1,TestMLFCA.classcount);
		assertEquals("classcount wrong",1,TestMLFCB.classcountb);
		broker.createElementNS(doc,new String(""+nsA+""),"nonRoot3");
		assertEquals("classcount wrong",1,TestMLFCA.classcount);
		assertEquals("classcount wrong",1,TestMLFCB.classcountb);

		hostAParasiteB();		
	}

	protected void hostAParasiteB()
	{
		MLFC hostMLFC = broker.getHostMLFC();
		assertTrue("host MLFC wrong",hostMLFC instanceof TestMLFCA);
		if (hostMLFC instanceof TestMLFCA)
		{
			TestMLFCA p = (TestMLFCA)hostMLFC;
			assertEquals("classno",0,p.classno);
		}
		Hashtable parasites = broker.getParasiteMLFCs();
		assertEquals("parasites count",1,parasites.size());

		int i = 0;
		for (Enumeration e = parasites.elements() ; e.hasMoreElements() ;)
		{
			i++;
			if (i==1)
			{
				MLFC parasite=(MLFC)e.nextElement();
				assertTrue("parasite MLFC",parasite instanceof TestMLFCB);
				if (parasite instanceof TestMLFCB)
				{
					TestMLFCB p = (TestMLFCB)parasite;
					assertEquals("classno",0,p.classno);
				}

			}
		}
		assertEquals("number of parasites",1,i);
	}

	public void testElementNSCreation()
	{
		broker.createElementNS(doc,new String(""+nsB+""),"rootElem");
		assertEquals("Elementname wrong","rootElem",TestMLFCB.latestElement);
		assertEquals("Namespace wrong",nsB,TestMLFCB.latestNS);
		broker.createElementNS(doc,new String(""+nsB+""),"elem2");
		assertEquals("Elementname wrong","elem2",TestMLFCB.latestElement);
		assertEquals("Namespace wrong",nsB,TestMLFCB.latestNS);
		broker.createElementNS(doc,new String(""+nsA+""),"elem3");
		assertEquals("Elementname wrong","elem3",TestMLFCB.latestElement);
		assertEquals("Namespace wrong",nsA,TestMLFCB.latestNS);
		broker.createElementNS(doc,new String(""+nsA+""),"elem4");
		assertEquals("Elementname wrong","elem4",TestMLFCB.latestElement);
		assertEquals("Namespace wrong",nsA,TestMLFCB.latestNS);
		broker.createElementNS(doc,new String(""+nsB+""),"elem5");
		assertEquals("Elementname wrong","elem5",TestMLFCB.latestElement);
		assertEquals("Namespace wrong",nsB,TestMLFCB.latestNS);
	}
	
	public void testAttributeNSCreation()
	{
		broker.createElementNS(doc,new String(""+nsA+""),"root");
		broker.createAttributeNS(doc,new String(""+nsB+""),"attribute");
		hostAParasiteB();		
	}



}

