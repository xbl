package org.xsmiles.tests;

import java.util.*;

import javax.swing.*;

import junit.framework.*;
import junit.extensions.*;



import org.apache.xerces.dom.*;
import org.w3c.dom.*;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XMLBroker;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * This test MLFC is used by XMLBrokerTest
 * @author Mikko Honkala
 */
	public class TestMLFCB extends TestMLFCA
	{
		public static int classcountb = 0;
	

		
		public TestMLFCB()
		{
			classno = classcountb++;
		}

		public final String getVersion()
		{
			return "0.1"+classno;
		}
		
	}
	
