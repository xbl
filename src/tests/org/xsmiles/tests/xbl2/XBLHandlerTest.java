package org.xsmiles.tests.xbl2;

import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.ScriptHandler;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.HandlerElementImpl;
import junit.framework.TestCase;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.apache.xerces.dom.events.MutationEventImpl;
import org.w3c.dom.events.Event;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;


public class XBLHandlerTest extends TestCase
{
    XBLHandler xblhandler1;
    XBLHandler xblhandler2;
	XSmilesElementImpl htmlnode;
    
    HandlerElementImpl h1;
    HandlerElementImpl h2;
    String xblns = new String("http://www.w3.org/ns/xbl");
    String htmns = new String("http://www.w3.org/1999/xhtm");
    private DocumentImpl doc;
    
    public XBLHandlerTest(String name)
    {
        super(name);
    }

    protected void setUp() throws Exception
    {        

        doc = new DocumentImpl();
        h1 = new HandlerElementImpl(doc, xblns, "handler");
        h2 = new HandlerElementImpl(doc, xblns, "handler");
                
        xblhandler1 = new XBLHandler();
        xblhandler1.setHandlerElementImpl(h1);

        xblhandler2 = new XBLHandler();
        xblhandler2.setHandlerElementImpl(h2);
        
        super.setUp();
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public final void testSetScript()
    {
        HandlerElementImpl h = new HandlerElementImpl(doc, xblns, "handler");
        h.appendChild(new TextImpl(doc, "object.action()"));
        xblhandler1.setHandlerElementImpl(h);
        assertEquals("object.action()", xblhandler1.getScript());
    }
    
    public final void testFilter() {
        
        XSmilesElementImpl elem1 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "img");

        // Button tests
        
        h1.setAttribute("event", "click");
        
        h1.setAttribute("button", "1");        
        MouseEventImpl evt1 = this.createMouseEventImpl(1, elem1, 0);
        MouseEventImpl evt2 = this.createMouseEventImpl(2, elem1, 1);
        MouseEventImpl evt3 = this.createMouseEventImpl(44, elem1, 2);

        assertTrue(xblhandler1.filter(evt1));
        assertTrue(!xblhandler1.filter(evt2));
        assertTrue(!xblhandler1.filter(evt3));

        h1.setAttribute("button", "2");        
        assertTrue(!xblhandler1.filter(evt1));
        assertTrue(xblhandler1.filter(evt2));
        assertTrue(!xblhandler1.filter(evt3));

        h1.setAttribute("button", "  1 2 78 9  ");        
        assertTrue(xblhandler1.filter(evt1));
        assertTrue(xblhandler1.filter(evt2));
        assertTrue(!xblhandler1.filter(evt3));
        
        h1.setAttribute("button", "44 12");        
        assertTrue(!xblhandler1.filter(evt1));
        assertTrue(!xblhandler1.filter(evt2));
        assertTrue(xblhandler1.filter(evt3));
        
        // click-count
        h1.setAttribute("button", "1 2 44"); 
        h1.setAttribute("click-count", "foo");
        assertTrue(!xblhandler1.filter(evt1));
        assertTrue(!xblhandler1.filter(evt2));
        assertTrue(!xblhandler1.filter(evt3));
        
        h1.setAttribute("click-count", "1");
        assertTrue(xblhandler1.filter(evt2));
        assertTrue(!xblhandler1.filter(evt1));
        assertTrue(!xblhandler1.filter(evt3));
        
        h1.setAttribute("click-count", "2"); 
        assertTrue(!xblhandler1.filter(evt1));
        assertTrue(!xblhandler1.filter(evt2));
        assertTrue(xblhandler1.filter(evt3));
        
        //for later tests
        h1.removeAttribute("click-count");
                
        // event tests
        h1.setAttribute("event", "onmouseover");
        h1.setAttribute("button", "1");
        assertTrue(!xblhandler1.filter(evt1));
        assertTrue(!xblhandler1.filter(evt2));

        
        // propagate tests
        h1.setAttribute("event", "click");
        h1.setAttribute("button", "1");        
        h1.setAttribute("propagate", "stop");
        xblhandler1.filter(evt1);
        assertTrue(evt1.stopPropagation);                
        h1.setAttribute("phase", "default-action");
        xblhandler1.filter(evt2);
        assertTrue(!evt2.stopPropagation);
        h1.removeAttribute("phase");
        
        // default-action test 
        h1.setAttribute("default-action", "cancel");
        xblhandler1.filter(evt1);
        assertTrue(evt1.preventDefault);
        evt1.preventDefault=false; 
        h1.setAttribute("default-action", "perform"); 
        xblhandler1.filter(evt1);
        assertTrue(!evt1.preventDefault);
        evt1.preventDefault=false;
        h1.removeAttribute("default-action");
        xblhandler1.filter(evt1); 
        assertTrue(!evt1.preventDefault);
        //for later tests
        evt1.preventDefault=false;
        
        //phase tests
        //TARGET
        evt1.eventPhase=Event.AT_TARGET; 
        h1.setAttribute("phase", "target");
        assertTrue(xblhandler1.filter(evt1)); 
        h1.setAttribute("phase", "default-action");
        assertTrue(!xblhandler1.filter(evt1));
        h1.setAttribute("phase", "capture");
        assertTrue(!xblhandler1.filter(evt1));
        h1.setAttribute("phase", "esimbubble");
        assertTrue(!xblhandler1.filter(evt1)); 
        
        //CAPTURE
        evt1.eventPhase=Event.CAPTURING_PHASE; 
        h1.setAttribute("phase", "target");
        assertTrue(!xblhandler1.filter(evt1)); 
        h1.setAttribute("phase", "default-action");
        assertTrue(!xblhandler1.filter(evt1));
        h1.setAttribute("phase", "capture");
        assertTrue(xblhandler1.filter(evt1));
        h1.setAttribute("phase", "esimbubble");
        assertTrue(!xblhandler1.filter(evt1));
        
        //BUBBLE
        evt1.eventPhase=Event.BUBBLING_PHASE; 
        h1.setAttribute("phase", "target");
        assertTrue(!xblhandler1.filter(evt1)); 
        h1.setAttribute("phase", "default-action");
        assertTrue(!xblhandler1.filter(evt1));
        h1.setAttribute("phase", "capture");
        assertTrue(!xblhandler1.filter(evt1));
        h1.setAttribute("phase", "esimbubble");
        assertTrue(xblhandler1.filter(evt1));
        
        //DEFAULT-ACTION cannot be tested yet
        
        //for later tests
        h1.removeAttribute("phase"); 

          
        // Modifier tests
        
        /* 
         * createMouseEventImplWithModifiers 
         * (boolean ctrlKey, boolean shiftKey, boolean altKey, boolean metaKey...)
         */

        h1.setAttribute("event", "click");
        /* Button must be set to 1 with modifier tests
         * (because of createMouseEventImplWithModifiers)
         */
        h1.setAttribute("button", "1");          
        h1.setAttribute("propagate", "continue");
        h1.removeAttribute("phase");
        
        MouseEventImpl evtNone = this.createMouseEventImplWithModifiers(
                false, false, false, false, elem1);        
        MouseEventImpl evtAll = this.createMouseEventImplWithModifiers(
                true, true, true, true, elem1);
        MouseEventImpl evtCtrl = this.createMouseEventImplWithModifiers(
                true, false, false, false, elem1);
        MouseEventImpl evtShift = this.createMouseEventImplWithModifiers(
                false, true, false, false, elem1);
        MouseEventImpl evtAlt = this.createMouseEventImplWithModifiers(
                false, false, true, false, elem1);
        MouseEventImpl evtMeta = this.createMouseEventImplWithModifiers(
                false, false, false, true, elem1);
        MouseEventImpl evtCtrlAlt = this.createMouseEventImplWithModifiers(
                true, false, true, false, elem1);
        
        h1.setAttribute("modifiers", "any");
        assertTrue(xblhandler1.filter(evtNone));
        assertTrue(xblhandler1.filter(evtAll));
        assertTrue(xblhandler1.filter(evtCtrl));
        assertTrue(xblhandler1.filter(evtShift));
        assertTrue(xblhandler1.filter(evtAlt));
        assertTrue(xblhandler1.filter(evtMeta));
        assertTrue(xblhandler1.filter(evtCtrlAlt));

        h1.setAttribute("modifiers", "none");
        assertTrue(xblhandler1.filter(evtNone));
        assertTrue(!xblhandler1.filter(evtAll));
        assertTrue(!xblhandler1.filter(evtCtrl));
        assertTrue(!xblhandler1.filter(evtShift));
        assertTrue(!xblhandler1.filter(evtAlt));
        assertTrue(!xblhandler1.filter(evtMeta));
        assertTrue(!xblhandler1.filter(evtCtrlAlt));

        h1.setAttribute("modifiers", "alt");
        assertTrue(!xblhandler1.filter(evtNone));
        assertTrue(xblhandler1.filter(evtAlt));

        h1.setAttribute("modifiers", "control");
        assertTrue(!xblhandler1.filter(evtNone));
        assertTrue(xblhandler1.filter(evtCtrl));

        h1.setAttribute("modifiers", "shift");
        assertTrue(!xblhandler1.filter(evtNone));
        assertTrue(xblhandler1.filter(evtShift));
        
        h1.setAttribute("modifiers", "meta");
        assertTrue(!xblhandler1.filter(evtNone));
        assertTrue(xblhandler1.filter(evtMeta));
        
        h1.setAttribute("modifiers", "-shift");
        assertTrue(xblhandler1.filter(evtNone));
        assertTrue(!xblhandler1.filter(evtCtrlAlt));
        assertTrue(!xblhandler1.filter(evtShift));

        h1.setAttribute("modifiers", "any -shift");
        assertTrue(xblhandler1.filter(evtNone));
        assertTrue(!xblhandler1.filter(evtAll));
        assertTrue(!xblhandler1.filter(evtShift));
        assertTrue(xblhandler1.filter(evtCtrlAlt));

        h1.setAttribute("modifiers", "alt control?");
        assertTrue(xblhandler1.filter(evtAlt));
        assertTrue(!xblhandler1.filter(evtCtrl));
        assertTrue(xblhandler1.filter(evtCtrlAlt));

        h1.setAttribute("modifiers", "foobar?");
        assertTrue(xblhandler1.filter(evtNone));
        assertTrue(!xblhandler1.filter(evtAll));
        assertTrue(!xblhandler1.filter(evtCtrl));
        assertTrue(!xblhandler1.filter(evtShift));
        assertTrue(!xblhandler1.filter(evtAlt));
        assertTrue(!xblhandler1.filter(evtMeta));
        assertTrue(!xblhandler1.filter(evtCtrlAlt));

        h1.setAttribute("modifiers", "foobar");
        assertTrue(!xblhandler1.filter(evtNone));
        assertTrue(!xblhandler1.filter(evtAll));
        assertTrue(!xblhandler1.filter(evtCtrl));
        assertTrue(!xblhandler1.filter(evtShift));
        assertTrue(!xblhandler1.filter(evtAlt));
        assertTrue(!xblhandler1.filter(evtMeta));
        assertTrue(!xblhandler1.filter(evtCtrlAlt));
        
        h1.setAttribute("modifiers", "+control -alt -shift");
        assertTrue(xblhandler1.filter(evtCtrl));
        assertTrue(!xblhandler1.filter(evtMeta));
        
        
        /* ================================================
         *    Tests for mutation event handler filters
         * ================================================ */
        
        // Mutation events informing changes in DOM structure.
        MutationEventImpl SubtreeModif = createMutationEvent(
                MutationEventImpl.DOM_SUBTREE_MODIFIED, 
                null, null, null, (short)0);
        MutationEventImpl NodeInstert = createMutationEvent(
                MutationEventImpl.DOM_NODE_INSERTED, 
                null, null, null, (short)0);        
        MutationEventImpl NodeRemoved = createMutationEvent(
                MutationEventImpl.DOM_NODE_REMOVED, 
                null, null, null, (short)0);        
        MutationEventImpl NodeRemovedDoc = createMutationEvent(
                MutationEventImpl.DOM_NODE_REMOVED_FROM_DOCUMENT, 
                null, null, null, (short)0);        
        MutationEventImpl NodeInsertedDoc = createMutationEvent(
                MutationEventImpl.DOM_NODE_INSERTED_INTO_DOCUMENT, 
                null, null, null, (short)0);
        
        // Element's valign attribute has changed from value "top" to "bottom"
        MutationEventImpl AttrModified = createMutationEvent(
                MutationEventImpl.DOM_ATTR_MODIFIED, 
                "top", "bottom", "valign", MutationEventImpl.MODIFICATION);

        /* An attribute named valign has been added to an element with
         * a value of "bottom". */ 
        MutationEventImpl AttrAdded = createMutationEvent(
                MutationEventImpl.DOM_ATTR_MODIFIED, 
                null, "bottom", "valign", MutationEventImpl.ADDITION);

        /* An attribute named valign has been removed from an element with
         * a previous value of "top". */ 
        MutationEventImpl AttrRemoved = createMutationEvent(
                MutationEventImpl.DOM_ATTR_MODIFIED, 
                "top", null, "valign", MutationEventImpl.REMOVAL);
     
        // Character data has been modified from "top" to "bottom"
        MutationEventImpl CharDataModif = createMutationEvent(
                MutationEventImpl.DOM_CHARACTER_DATA_MODIFIED, 
                "top", "bottom", null, (short)0);        
        
        // Events informing DOM structure changes 
        h2.setAttribute("event", MutationEventImpl.DOM_SUBTREE_MODIFIED);
        assertTrue(xblhandler2.filter(SubtreeModif));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_INSERTED);
        assertTrue(xblhandler2.filter(NodeInstert));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_REMOVED);
        assertTrue(xblhandler2.filter(NodeRemoved));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_REMOVED_FROM_DOCUMENT);
        assertTrue(xblhandler2.filter(NodeRemovedDoc));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_INSERTED_INTO_DOCUMENT);
        assertTrue(xblhandler2.filter(NodeInsertedDoc));
        
        
        /* Events informing DOM structure changes don't have the following
         * attributes: prev-value, new-value, attr-name or attr-change,
         * so they always fail.
         */  
        h2.setAttribute("prev-value", "top");
        
        h2.setAttribute("event", MutationEventImpl.DOM_SUBTREE_MODIFIED);
        assertTrue(!xblhandler2.filter(SubtreeModif));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_INSERTED);
        assertTrue(!xblhandler2.filter(NodeInstert));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_REMOVED);
        assertTrue(!xblhandler2.filter(NodeRemoved));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_REMOVED_FROM_DOCUMENT);
        assertTrue(!xblhandler2.filter(NodeRemovedDoc));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_INSERTED_INTO_DOCUMENT);
        assertTrue(!xblhandler2.filter(NodeInsertedDoc));
                
        h2.removeAttribute("prev-value");
        h2.setAttribute("attr-change", "modification");        
        
        h2.setAttribute("event", MutationEventImpl.DOM_SUBTREE_MODIFIED);
        assertTrue(!xblhandler2.filter(SubtreeModif));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_INSERTED);
        assertTrue(!xblhandler2.filter(NodeInstert));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_REMOVED);
        assertTrue(!xblhandler2.filter(NodeRemoved));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_REMOVED_FROM_DOCUMENT);
        assertTrue(!xblhandler2.filter(NodeRemovedDoc));

        h2.setAttribute("event", MutationEventImpl.DOM_NODE_INSERTED_INTO_DOCUMENT);
        assertTrue(!xblhandler2.filter(NodeInsertedDoc));
        
        
        // Events informing changes in element's attribute
        h2.removeAttribute("attr-change");
        h2.setAttribute("event", MutationEventImpl.DOM_ATTR_MODIFIED);
        
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(xblhandler2.filter(AttrAdded));
        assertTrue(xblhandler2.filter(AttrRemoved));
        
        h2.setAttribute("attr-change", "modification");
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));
        
        h2.setAttribute("attr-change", "addition");
        assertTrue(!xblhandler2.filter(AttrModified));
        assertTrue(xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));

        h2.setAttribute("attr-change", "removal");
        assertTrue(!xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(xblhandler2.filter(AttrRemoved));
        
        h2.removeAttribute("attr-change");
        
        h2.setAttribute("attr-name", "valign");
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(xblhandler2.filter(AttrAdded));
        assertTrue(xblhandler2.filter(AttrRemoved));

        h2.setAttribute("attr-name", "halign");
        assertTrue(!xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));
        
        h2.setAttribute("attr-name", "valign");
        h2.setAttribute("new-value", "bottom");
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));
        
        h2.removeAttribute("new-value");
        h2.setAttribute("prev-value", "top");
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(xblhandler2.filter(AttrRemoved));

        h2.setAttribute("new-value", "bottom");
        h2.setAttribute("prev-value", "top");
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));

        h2.setAttribute("prev-value", "top");
        h2.setAttribute("new-value", "bottom");
        h2.setAttribute("attr-change", "modification");
        assertTrue(xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));
        
        h2.removeAttribute("new-value");        
        h2.setAttribute("prev-value", "top");
        h2.setAttribute("attr-change", "removal");
        assertTrue(!xblhandler2.filter(AttrModified));
        assertTrue(!xblhandler2.filter(AttrAdded));
        assertTrue(xblhandler2.filter(AttrRemoved));

        h2.removeAttribute("prev-value");        
        h2.setAttribute("new-value", "bottom");
        h2.setAttribute("attr-change", "addition");
        assertTrue(!xblhandler2.filter(AttrModified));
        assertTrue(xblhandler2.filter(AttrAdded));
        assertTrue(!xblhandler2.filter(AttrRemoved));

        h2.removeAttribute("new-value");        
        h2.setAttribute("event", MutationEventImpl.DOM_CHARACTER_DATA_MODIFIED);
        h2.setAttribute("attr-change", "addition");
        assertTrue(!xblhandler2.filter(CharDataModif));
       
        /* DOMCharacterDataModified doesn't have attributes attr-change or attr-name,
         * only attributes new-value and prev-value.
         */
        h2.removeAttribute("attr-change");
        h2.removeAttribute("attr-name");
        h2.setAttribute("new-value", "bottom");
        h2.setAttribute("prev-value", "top");                
        assertTrue(xblhandler2.filter(CharDataModif));
                
    }

    
    /**
     * Creates MouseEventImpl with button given in paramaters.
     * @param button
     * @param elem
     * @param detail for click-count
     * @return
     */
    public MouseEventImpl createMouseEventImpl(int button, XSmilesElementImpl elem, int detail) {

        /* 
         * e1.initMouseEvent(
         * typeArg, 
         * canBubbleArg, 
         * cancelableArg, 
         * viewArg, 
         * detailArg, 
         * screenXArg, 
         * screenYArg, 
         * clientXArg, 
         * clientYArg, 
         * ctrlKeyArg, 
         * altKeyArg, 
         * shiftKeyArg, 
         * metaKeyArg, 
         * buttonArg, 
         * relatedTargetArg) 
         * 
         */
        
        MouseEventImpl evt = (MouseEventImpl) EventFactory.createEvent("click");
        evt.initMouseEvent(
            "click",
            false,
            false,
            null,
            (short) detail,
            (int) 0,
            (int) 0,
            (float) 0,
            (float) 0,
            false,
            false,
            false,
            false,
            (short) button,
            elem); 
        
        return evt;
        
    }

    /**
     * Creates MouseEventImpl with modifiers given in 
     * parameters.
     * @param elem
     * @param ctrlKey
     * @param shiftKey
     * @param altKey
     * @param metaKey
     * @return
     */
    public MouseEventImpl createMouseEventImplWithModifiers(
                                                boolean ctrlKey,
                                                boolean shiftKey,
                                                boolean altKey,
                                                boolean metaKey,
                                                XSmilesElementImpl elem) {

        /* 
         * e1.initMouseEvent(
         * typeArg, 
         * canBubbleArg, 
         * cancelableArg, 
         * viewArg, 
         * detailArg, 
         * screenXArg, 
         * screenYArg, 
         * clientXArg, 
         * clientYArg, 
         * ctrlKeyArg, 
         * altKeyArg, 
         * shiftKeyArg, 
         * metaKeyArg, 
         * buttonArg, 
         * relatedTargetArg) 
         * 
         */
        
        MouseEventImpl evt = (MouseEventImpl) EventFactory.createEvent("click");
        evt.initMouseEvent(
            "click",
            false,
            false,
            null,
            (short) 0,
            (int) 0,
            (int) 0,
            (float) 0,
            (float) 0,
            ctrlKey,
            altKey,
            shiftKey,
            metaKey,
            (short) 1,
            elem); 
        
        return evt;
        
    }
    
    /**
     * Creates MutationEventImpl with the given parameters
     * @param type
     * @param prevValue
     * @param newValue
     * @param attrName
     * @param attrChange
     * @return
     */
    public MutationEventImpl createMutationEvent(String type,
            String prevValue, String newValue, String attrName, 
            short attrChange) {
        
        MutationEventImpl evt = new MutationEventImpl();
        evt.initMutationEvent(type, evt.getBubbles(), evt.getCancelable(), 
                evt.getRelatedNode(), prevValue, newValue, attrName, attrChange);
                
        return evt;
    }
    
    
    public final void testSetEvent()
    {
    	xblhandler1.setEvent("click");
        assertEquals("click", xblhandler1.getEvent());
    }
    
    public final void testSetElement()
    {
    	doc = new DocumentImpl();
		htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
		
    	xblhandler1.setElement(htmlnode);
        assertEquals(htmlnode, xblhandler1.getElement());
    }
    
    public final void testSetEventListener() {
        HandlerElementImpl h = new HandlerElementImpl(doc, xblns, "handler");
        h.appendChild(new TextImpl(doc, "object.action()"));
        xblhandler1.setHandlerElementImpl(h);
    	xblhandler1.setEvent("click");
    	
    	doc = new DocumentImpl();
		htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
    	xblhandler1.setElement(htmlnode);
    	
    	ScriptHandler sh = new ScriptHandler();
    	ScriptHandler.EventHandler eventHandler = sh.new EventHandler(xblhandler1);
    	xblhandler1.setEventListener(eventHandler);
    }
}
