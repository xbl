/**
 * 
 */
package org.xsmiles.tests.xbl2;

import junit.framework.TestCase;

import java.util.LinkedList;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xbl2.ScriptHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.HandlerElementImpl;
//import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ScriptElementImpl;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.ecma.ECMAScripterFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;
import org.xsmiles.tests.xbl2.dom.ScriptElementImplTest.TestMLFC;

/**
 * @author Juho Vuohelainen
 *
 */
public class ScriptHandlerTest extends TestCase {
	ScriptHandler sh;
	XSmilesElementImpl htmlnode;
	XSmilesDocumentImpl doc;
	String xblns = new String("http://www.w3.org/ns/xbl");
	String htmns = new String("http://www.w3.org/1999/xhtm");
	TestMLFC hostMLFC;
	//ScriptElementImpl scriptElem;
    XSmilesElementImpl bodynode;
    
	public ScriptHandlerTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public final void testSriptHandler() {
		sh = new ScriptHandler();
		
		if(sh == null)
			fail("Null object");
	}
	
	public final void testAddScript() {
		doc = new XSmilesDocumentImpl();
		htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
		
		XBLHandler xh = new XBLHandler();
        HandlerElementImpl h = new HandlerElementImpl(doc, xblns, "handler");
        h.appendChild(new TextImpl(doc, "object.action()"));
        xh.setHandlerElementImpl(h);        
		xh.setElement(htmlnode);
		xh.setEvent("click");
		
		sh = new ScriptHandler();
		sh.addHandler(xh);
		
		LinkedList scriptList = sh.getHandlers(htmlnode);
		assertEquals(scriptList.get(0), xh);
		
		if(!( ((XBLHandler) scriptList.get(0)).getEventListener() instanceof ScriptHandler.EventHandler)) {
			fail("EventHandler instance not found!");
		}
	}
	
	public final void testHandleEvent() {
		doc = new XSmilesDocumentImpl();
		htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
        hostMLFC = new TestMLFC();
        hostMLFC.setXMLDocument(new TestXMLDocument(null, doc));
        doc.setHostMLFC(hostMLFC);
        bodynode = new XSmilesElementImpl(doc, htmns, "body");
        bodynode.setAttribute("id", "oldID");
        XSmilesElementImpl htmlnode = new XSmilesElementImpl(doc, htmns, "html");
        XSmilesElementImpl xblnode = new XSmilesElementImpl(doc, xblns, "xbl");
        doc.appendChild(htmlnode);
        htmlnode.appendChild(xblnode);
        htmlnode.appendChild(bodynode);
		
		XBLHandler xh = new XBLHandler();
        HandlerElementImpl h = new HandlerElementImpl(doc, xblns, "handler");
        h.appendChild(new TextImpl(doc, "document.getElementById(\"oldID\").setAttribute(\"id\", \"newID\")"));
        h.setAttribute("event", "click");
        xh.setHandlerElementImpl(h);
		xh.setElement(htmlnode);
		xh.setEvent("click");
		
		sh = new ScriptHandler();
		sh.addHandler(xh);
		
		//LinkedList scriptList = sh.getHandlers(htmlnode);
		EventListener eh = xh.getEventListener();
		
		//MouseEventImpl event = new MouseEventImpl();
		org.w3c.dom.events.MouseEvent event = (org.w3c.dom.events.MouseEvent)EventFactory.createEvent("click");
		
        event.initMouseEvent("click", true, true, null, 1, 0, 0, 0, 0, false, false, false, false, (short) 0, null);
        
        Log.debug(event.getType() + " / " + h.getEventName());
		eh.handleEvent(event);

        assertEquals("newID", bodynode.getAttribute("id"));
	}
	
	public class TestMLFC extends MLFC
    {
        
        public int started = 0;
        public int stopped = 0;
        
        public TestMLFC()
        {
            //super();
        }
        
        public final String getVersion()
        {
            return "0.1";
        }
        
        /**
         * Create a DOM element.
         */
        public Element createElementNS(DocumentImpl doc, String ns, String tag)
        {
            return null;
        }
        public void start()
        {
            started++;
        }
        
        public void stop()
        {
            stopped++;
        }
        
    }
    
    public class TestXMLDocument extends XSmilesXMLDocument
    {
        public ExtendedDocument edoc;
        public TestXMLDocument(BrowserWindow browser,ExtendedDocument extdoc)
        {
            super(browser,extdoc);
            edoc=extdoc;
        }
        
        public Document getDocument()
        {
            return edoc;
        }
        
        public ECMAScripter getECMAScripter() {
        	return ECMAScripterFactory.getScripter("rhino",this.m_browser);
        }
    }
}
