/**
 * 
 */
package org.xsmiles.tests.xbl2;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xsmiles.tests.xbl2.dom.ScriptElementImplTest.TestMLFC;
import org.xsmiles.tests.xbl2.dom.ScriptElementImplTest.TestXMLDocument;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLMLFC;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.*;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.ecma.ECMAScripterFactory;

/**
 * @author Juho Vuohelainen
 *
 */
public class BindingHandlerTest extends TestCase {
	BindingHandler bh;
	XSmilesDocumentImpl doc;
	XSmilesElementImpl htmlnode;
	XSmilesElementImpl xblnode;
	XSmilesElementImpl divnode;
	BindingElementImpl be1;
	BindingElementImpl be2;
	BindingElementImpl be3;
    String xblns = new String("http://www.w3.org/ns/xbl");
    String htmns = new String("http://www.w3.org/1999/xhtm");
	
	public BindingHandlerTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		doc = new XSmilesDocumentImpl();
		htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
		xblnode = new XSmilesElementImpl(doc, "http://www.w3.org/ns/xbl", "xbl");
		divnode = new XSmilesElementImpl(doc, "div");
	
		divnode.setAttribute("id", "a");
		
		doc.appendChild(htmlnode);
		htmlnode.appendChild(xblnode);
		htmlnode.appendChild(divnode);
		
		htmlnode.init();
		xblnode.init();
		divnode.init();
		
		be1 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl", "binding");
		be2 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl", "binding");
		be3 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl", "binding");
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}	
    
	/**
	 * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#addBinding(fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl, fi.hut.tml.xsmiles.dom.XSmilesElementImpl)},
	 * {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#getBindings(fi.hut.tml.xsmiles.dom.XSmilesElementImpl)} and
	 * {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#getBindings(fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl)}.
	 */
	public void testAddBindingGetBindings() {
		bh = new BindingHandler(doc);
		
		bh.addBinding(be1, htmlnode);
		bh.addBinding(be1, xblnode);
		bh.addBinding(be1, divnode);
		
		bh.addBinding(be2, xblnode);
		bh.addBinding(be2, divnode);
		bh.addBinding(be2, htmlnode);
		
		bh.addBinding(be3, divnode);
		bh.addBinding(be3, htmlnode);
		bh.addBinding(be3, xblnode);
		
		Vector v = bh.getBindings(be1);
		assertEquals(htmlnode, v.get(0));
		assertEquals(xblnode, v.get(1));
		assertEquals(divnode, v.get(2));
		
		v = bh.getBindings(be2);
		assertEquals(xblnode, v.get(0));
		assertEquals(divnode, v.get(1));
		assertEquals(htmlnode, v.get(2));
		
		v = bh.getBindings(be3);
		assertEquals(divnode, v.get(0));
		assertEquals(htmlnode, v.get(1));
		assertEquals(xblnode, v.get(2));
		
		v = bh.getBindings(htmlnode);
		assertEquals(be1, v.get(0));
		assertEquals(be2, v.get(1));
		assertEquals(be3, v.get(2));
		
		v = bh.getBindings(xblnode);
		assertEquals(be1, v.get(0));
		assertEquals(be2, v.get(1));
		assertEquals(be3, v.get(2));
		
		v = bh.getBindings(divnode);
		assertEquals(be1, v.get(0));
		assertEquals(be2, v.get(1));
		assertEquals(be3, v.get(2));
		
		bh = null;
	}


	/**
	 * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#addBinding(fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl, fi.hut.tml.xsmiles.dom.XSmilesElementImpl)}.
	 *
	public void testAddBinding() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#getBindings(fi.hut.tml.xsmiles.dom.XSmilesElementImpl)}.
	 *
	public void testGetBindingsXSmilesElementImpl() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#getBindings(fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl)}.
	 *
	public void testGetBindingsBindingElementImpl() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#applyBindings()}.
	 
	public void testApplyBindings() {
        bh = new BindingHandler(doc);
        TemplateElementImpl template = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl", "template");
        DivElementImpl newdiv = new DivElementImpl(doc, "http://www.w3.org/ns/xbl", "div");
        XSmilesElementImpl olddiv = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "div");        
        TextImpl oldtext = new TextImpl(doc, "ERROR");
        TextImpl newtext = new TextImpl(doc, "SUCCESS");
        htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
        be1 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl", "binding");
        
        olddiv.appendChild(oldtext);
        htmlnode.appendChild(olddiv);
        
        newdiv.appendChild(newtext);
        template.appendChild(newdiv);
        be1.appendChild(template);
        be1.setAttribute("id", "be1");
        
        bh.addBinding(be1, olddiv);
        bh.applyBindings();
                
        assertEquals("SUCCESS", htmlnode.getFirstChild().getFirstChild().getTextContent());
	}

	/**
	 * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#applyBindings(fi.hut.tml.xsmiles.dom.XSmilesElementImpl)}.
	 */
	//public void testApplyBindingsXSmilesElementImpl() {
//		//fail("Not yet implemented");
	//}
    /**
     * Test method for {@link fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler#applyBindings()}.
     */
    public void testApplyDynamicBindings() {
        bh = new BindingHandler(doc);
        TemplateElementImpl template = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl", "template");
        DivElementImpl newdiv = new DivElementImpl(doc, "http://www.w3.org/ns/xbl", "div");
        XSmilesElementImpl olddiv = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "div");
        XSmilesElementImpl olddiv2 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "div");
        TextImpl oldtext1 = new TextImpl(doc, "ERROR1");
        TextImpl oldtext2 = new TextImpl(doc, "ERROR2");
        TextImpl newtext = new TextImpl(doc, "SUCCESS");
        htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
        be1 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl", "binding");
        
        olddiv.appendChild(oldtext1);
        htmlnode.appendChild(olddiv);
        
        olddiv2.appendChild(oldtext2);
        htmlnode.appendChild(olddiv2);
        
        newdiv.appendChild(newtext);
        template.appendChild(newdiv);
        be1.appendChild(template);
        be1.setAttribute("id", "be1");
        
        bh.addBinding(be1, olddiv);
        bh.applyBindings();
        assertEquals("SUCCESS", htmlnode.getFirstChild().getFirstChild().getTextContent());
        assertEquals("ERROR2", htmlnode.getFirstChild().getNextSibling().getFirstChild().getTextContent());
        
        bh.addBinding(be1, olddiv2);
        bh.applyBindings();
        assertEquals("SUCCESS", htmlnode.getFirstChild().getFirstChild().getTextContent());
        assertEquals("SUCCESS", htmlnode.getFirstChild().getNextSibling().getFirstChild().getTextContent());
    }
    
    public void testGetBindingDocumentsPI() {
                                       
        XSmilesDocumentImpl xblDoc = new XSmilesDocumentImpl();
        xblDoc.setDocumentURI("http://www.documentXBL.uri");                        
        XBLMLFC hostMLFCXBL = new XBLMLFC();
        TestXMLDocument xblXMLDoc = new TestXMLDocument(null, xblDoc);
        hostMLFCXBL.setXMLDocument(xblXMLDoc);
        xblDoc.setHostMLFC(hostMLFCXBL);
        
        XSmilesDocumentImpl xblDoc2 = new XSmilesDocumentImpl();
        xblDoc2.setDocumentURI("http://www.documentXBL.uri");                        
        XBLMLFC hostMLFCXBL2 = new XBLMLFC();
        TestXMLDocument xblXMLDoc2 = new TestXMLDocument(null, xblDoc2);
        hostMLFCXBL2.setXMLDocument(xblXMLDoc2);
        xblDoc2.setHostMLFC(hostMLFCXBL2);
                 
        XSmilesDocumentImpl doc = new XSmilesDocumentImpl();        
        doc.setDocumentURI("http://www.document.uri");
        XBLMLFC hostMLFCHTML = new XBLMLFC();
        hostMLFCHTML.setXMLDocument(new XSmilesXMLDocument(null, doc));
        doc.setHostMLFC(hostMLFCHTML);
        
        BindingHandler bh = new BindingHandler(doc);
        doc.setBindingHandler(bh);
        
        bh.addBindingDocumentPI(xblXMLDoc);
        bh.addBindingDocumentPI(xblXMLDoc2);
        
        Vector piDocs = bh.getBindingDocumentsPI();
        
        assertEquals(xblXMLDoc, piDocs.get(0));
        assertEquals(xblXMLDoc2, piDocs.get(1));
        
    }
    
    public void testStart() {
        
        XSmilesDocumentImpl doc = buildHTMLDocWithInlineBindings();
        
        assertEquals("ERROR",((XSmilesElementImpl)doc.getElementById("bound1")).getText());        
        assertEquals("ERROR",((XSmilesElementImpl)doc.getElementById("bound2")).getText());        
        assertEquals("NOTMODIFIED",((XSmilesElementImpl)doc.getElementById("bound3")).getText());        
        doc.init(); // This calls BindingHandler.start()
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound1")).getText());        
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound2")).getText());        
        assertEquals("NOTMODIFIED",((XSmilesElementImpl)doc.getElementById("bound3")).getText());                
                
    }
    
    public void testGetAllBindings() {
        
        XSmilesDocumentImpl doc = buildHTMLDocWithInlineBindings();        
        doc.init(); // This calls BindingHandler.start()

        ArrayList bindings = doc.getBindingHandler().getAllBindings();
        assertEquals(doc.getElementById("b1"), bindings.get(0));
        assertEquals(doc.getElementById("b2"), bindings.get(1));        
        
    }
    
    public void testAddBindingDynamically() {
        
        XSmilesDocumentImpl doc = buildHTMLDocWithInlineBindings();
        
        assertEquals("ERROR",((XSmilesElementImpl)doc.getElementById("bound1")).getText());        
        assertEquals("ERROR",((XSmilesElementImpl)doc.getElementById("bound2")).getText());        
        assertEquals("NOTMODIFIED",((XSmilesElementImpl)doc.getElementById("bound3")).getText());        
        doc.init(); // This calls BindingHandler.start()
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound1")).getText());        
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound2")).getText());        
        assertEquals("NOTMODIFIED",((XSmilesElementImpl)doc.getElementById("bound3")).getText());                
        
        XSmilesElementImpl elem = (XSmilesElementImpl)doc.getElementById("bound3");        
        doc.getBindingHandler().addBindingDynamically("#b2", elem);
        
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound1")).getText());        
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound2")).getText());        
        assertEquals("OK",((XSmilesElementImpl)doc.getElementById("bound3")).getText());                
                
    }
    
    public XSmilesDocumentImpl buildHTMLDocWithInlineBindings() {
        
        /* ==========================================
         * Build HTML document with inline bindings
         * ========================================== */

        XSmilesDocumentImpl doc = new XSmilesDocumentImpl();        
        doc.setDocumentURI("http://www.document.uri");
        XBLMLFC hostMLFCHTML = new XBLMLFC();
        hostMLFCHTML.setXMLDocument(new XSmilesXMLDocument(null, doc));
        doc.setHostMLFC(hostMLFCHTML);
        
        XBLElementImpl xblNode = new XBLElementImpl(doc, xblns, "xbl");
        BindingElementImpl b1 = new BindingElementImpl(doc, xblns, "binding");
        BindingElementImpl b2 = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templateNode1 = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl htmlNode = new XSmilesElementImpl(doc, htmns, "html");
        XSmilesElementImpl headNode = new XSmilesElementImpl(doc, htmns, "head");
        XSmilesElementImpl bodyNode = new XSmilesElementImpl(doc, htmns, "body");
        XSmilesElementImpl pNode1 = new XSmilesElementImpl(doc, htmns, "p");
        XSmilesElementImpl pNode2 = new XSmilesElementImpl(doc, htmns, "p");
        XSmilesElementImpl pNode3 = new XSmilesElementImpl(doc, htmns, "p");

        pNode1.setAttribute("id", "bound1");
        pNode2.setAttribute("id", "bound2");
        pNode3.setAttribute("id", "bound3");
        pNode2.setAttribute("class", "testClass");
        b1.setAttribute("id", "b1");
        b1.setAttribute("element", ".testClass");
        b2.setAttribute("id", "b2");
        b2.setAttribute("extends", "#b1");
        b2.setAttribute("element", "#bound1");
                
        doc.appendChild(htmlNode);
        htmlNode.appendChild(headNode);
        headNode.appendChild(xblNode);
        htmlNode.appendChild(bodyNode);
        bodyNode.appendChild(pNode1);
        bodyNode.appendChild(pNode2);
        bodyNode.appendChild(pNode3);
        xblNode.appendChild(b1);
        xblNode.appendChild(b2);
        b1.appendChild(templateNode1);
        
        pNode1.appendChild(new TextImpl(doc, "ERROR"));
        pNode2.appendChild(new TextImpl(doc, "ERROR"));
        pNode3.appendChild(new TextImpl(doc, "NOTMODIFIED"));
        templateNode1.appendChild(new TextImpl(doc, "OK"));

        return doc;
    }
    
    public class TestXMLDocument extends XSmilesXMLDocument
    {
        public ExtendedDocument edoc;
        public TestXMLDocument(BrowserWindow browser,ExtendedDocument extdoc)
        {
            super(browser,extdoc);
            edoc=extdoc;
        }
        
        public Document getDocument()
        {
            return edoc;
        }
        
    }    
            
}
