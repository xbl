package org.xsmiles.tests.xbl2.dom;

import java.util.Vector;

import junit.framework.TestCase;
import junit.framework.Assert;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ContentElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.TemplateElementImpl;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesTextImpl;
import org.w3c.dom.NodeList; 
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.DivElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.InheritedElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler;
import fi.hut.tml.xsmiles.Log; 

import org.w3c.dom.Node;

/**
 * Test for TemplateElementImpl class.
 * 
 * @author Jukka Julku
 *
 */

public class TemplateElementImplTest extends TestCase 
{

	public TemplateElementImplTest(String arg0) 
	{
		super(arg0);
	}

	protected void setUp() throws Exception 
	{
		super.setUp();
	}

	protected void tearDown() throws Exception 
	{
		super.tearDown();
	}

	/**
	 * Tests TemplateElementImpl.distributeChildren in case where:
	 * - There is template with matching content element
	 */
	
	public void testDistributeChildrenContentOnly() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		a.appendChild(b);
		a.appendChild(c);
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		templ.appendChild(cont);
		
		assertTrue("Could not init final flattened tree!",a.initFinalFlattenedTree());
		
		templ.distributeChildren(a);
		
		NodeList n = templ.getChildNodes();
		assertEquals("The tree was changed",1, n.getLength());
		
		Node child = templ.getFirstChild();
		assertNotNull("child is null!", child);
		assertEquals("Wrong child element!", cont.toString(), child.toString());
		
		Vector v = cont.getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		assertEquals("Wrong number of elements assigned", 2, v.size());
		
		assertEquals("Wrong child element!", b.toString(), ((Node)v.elementAt(0)).toString());

		assertEquals("Wrong child element!", c.toString(), ((Node)v.elementAt(1)).toString());
				
	}
	
	/**
	 * Tests TemplateElementImpl.distributeChildren in case where:
	 * - There is tree under template with matching content element
	 */
	
	public void testDistributeChildrenTree() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
			
		a.appendChild(b);	
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		templ.appendChild(d);
		templ.appendChild(c);
		c.appendChild(cont);
		
		a.initFinalFlattenedTree();
		
		templ.distributeChildren(a);
		
		Node child = templ.getFirstChild();
		
		assertEquals("Wrong child element!", d.toString(), child.toString());
		
		assertNotNull("The tree was modified", child.getNextSibling());
		
		child = child.getNextSibling();
		
		assertEquals("Wrong child element!", c.toString(), child.toString());
		
		assertEquals("Wrong child element!", cont.toString(), child.getFirstChild().toString());
		
		Vector v = ((ContentElementImpl)child.getFirstChild()).getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		assertEquals("Wrong number of elements assigned", 1, v.size());
		
		assertEquals("Wrong child element!", b.toString(), ((Node)v.elementAt(0)).toString());
    }
	
	/**
	 * Tests TemplateElementImpl.distributeChildren in case where:
	 * - There is tree under template with no matching contents
	 */
	
	public void testDistributeChildrenTreeNoMatch() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
			
		a.appendChild(b);	
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		templ.appendChild(d);
		templ.appendChild(c);
		c.appendChild(cont);
		cont.setAttribute("includes", "a");
		
		a.initFinalFlattenedTree();
		
		templ.distributeChildren(a);
		
		Node child = templ.getFirstChild();
		
		assertEquals("Wrong child element!", d.toString(), child.toString());
		
		child = child.getNextSibling();
		
		assertEquals("Wrong child element!", c.toString(), child.toString());
		
		child = child.getFirstChild();
		
		assertEquals("Wrong child element!", cont.toString(), child.toString());
		
        Vector v = ((ContentElementImpl)child).getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		assertEquals("Wrong number of elements assigned", 0, v.size());
						
	}
	
	/**
	 * Tests TemplateElementImpl.distributeChildren in case where:
	 * - There is tree under template with multiple content elements that match
	 *   the elements distributed
	 *   
	 */
	
	public void testDistributeChildrenTreeMultipleMatch() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
			
		a.appendChild(b);	
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont1 = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		ContentElementImpl cont2 = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");

		templ.appendChild(d);
		templ.appendChild(c);
		
		c.appendChild(cont2);
		d.appendChild(cont1);
		
		a.initFinalFlattenedTree();
		
		templ.distributeChildren(a);
		
		Node child = templ.getFirstChild();
		
		assertEquals("Wrong child element!", d.toString(), child.toString());
		
		Node child2 = child.getFirstChild();
		
		assertEquals("Wrong child element!", cont1.toString(), child2.toString());
		
        Vector v = ((ContentElementImpl)child2).getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		assertEquals("Wrong number of elements assigned", 1, v.size());
		
		assertEquals("Wrong child element!", b.toString(), ((Node)v.elementAt(0)).toString());
		
		
		child = child.getNextSibling();
		
		assertEquals("Wrong child element!", c.toString(), child.toString());
		
		child2 = child.getFirstChild();
		
		assertEquals("Wrong child element!", cont2.toString(), child2.toString());
		
		v = ((ContentElementImpl)child2).getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		
		assertEquals("Wrong number of elements assigned", 0, v.size());			
	}
	
	/**
	 * Tests TemplateElementImpl.distributeChildren in case where:
	 * - There is tree under template with multiple content elements that match
	 *   some of the the elements distributed
	 */
	
	public void testDistributeChildrenTreeMatchMultiple() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
			
		a.appendChild(b);
		a.appendChild(c);
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont1 = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		ContentElementImpl cont2 = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		templ.appendChild(d);
		d.appendChild(cont1);
		templ.appendChild(cont2);
		
		cont1.setAttribute("includes", "b");
		
		a.initFinalFlattenedTree();
		
		templ.distributeChildren(a);
		
		Node child = templ.getFirstChild();
		
		assertEquals("Wrong child element!", d.toString(), child.toString());
		assertEquals("Wrong child element!", cont1.toString(), child.getFirstChild().toString());
		
        Vector v = ((ContentElementImpl) child.getFirstChild()).getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		assertEquals("Wrong number of elements assigned", 1, v.size());
		
		assertEquals("Wrong child element!", b.toString(), ((Node)v.elementAt(0)).toString());
				
		child = child.getNextSibling();
		
		assertEquals("Wrong child element!", cont2.toString(), child.toString());
     
		v = ((ContentElementImpl)child).getAssignedElements();
		
		assertNotNull("No elements assigned", v);
		assertEquals("Wrong number of elements assigned", 1, v.size());
		
		assertEquals("Wrong child element!", c.toString(), ((Node)v.elementAt(0)).toString());
							
	}
	
	/**
	 * Tests TemplateElementImpl.distributeChildren in case where:
	 * - There is tree under template with np content element
	 */
	
	public void testDistributeChildrenTreeNoContent() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
			
		a.appendChild(b);	
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");

		templ.appendChild(d);
		templ.appendChild(c);
		
		a.initFinalFlattenedTree();
		
		templ.distributeChildren(a);
		
		Node child = templ.getFirstChild();
		
		assertEquals("Wrong child element!", d.toString(), child.toString());
		assertNull("Element added when should not!", child.getFirstChild());
		
		child = child.getNextSibling();

		assertEquals("Wrong child element!", c.toString(), child.toString());
		assertNull("Element added when should not!", child.getFirstChild());
				
	}
	
	/**
	 * Tests TemplateElementImpl.createShadowContent in case where:
	 * - There is tree under template
	 */
	
	public void testCreateShadowContent() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		
		templ.appendChild(b);
		b.appendChild(c);
		

	    TemplateElementImpl t = templ.createShadowContent(a);
	    
	    assertEquals("Template was not copied!", templ.toString(), t.toString());
	    assertTrue("Template was not copied!", templ != t);
	    
	    Node child = t.getFirstChild();
	    
	    assertEquals("Template was not copied!", child.toString(), b.toString());
		
	    assertTrue("Template was not copied!", child != b);
	    
	    assertNull("Template was not copied!", t.getNextSibling());
	    
	    child = child.getFirstChild();
	    
	    assertEquals("Template was not copied!", child.toString(), c.toString());
		
	    assertTrue("Template was not copied!", child != c);
	    
	    assertNull("Template was not copied!", t.getNextSibling());
	    
	    assertNull("Template was not copied!", child.getFirstChild());
	}
	
	/**
	 * Tests TemplateElementImpl.unassignChildren
	 */
	
	public void testUnassignChildren() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.appendChild(b);
		a.appendChild(c);
		a.initFinalFlattenedTree();
		
		templ.appendChild(cont);
		cont.assignElement(b);
		cont.assignElement(c);
		
		templ.unassignChildren(a, templ, false);
		
		Node child = templ.getFirstChild();
		
		assertNotNull("Template was changed", child);
		
		assertEquals("Template was changed", cont.toString(), child.toString());
		
		Vector v = cont.getAssignedElements();
		
		assertEquals("Wrong number of assigned elements", 0, v.size());
		
		Vector n = a.getUndistributedChildNodes();
		
		assertEquals("Wrong number of elements added to bound element", 2, n.size());
		
		boolean test = false;
		for(int i = 0; i < 2; i++)
		{
			if(b == (Node)n.elementAt(i))
			{
				test = true;
			}
		}
		assertTrue("undistributed element not found in bound element", test);
		
		test = false;
		for(int i = 0; i < 2; i++)
		{
			if(c == (Node)n.elementAt(i))
			{
				test = true;
			}
		}
		assertTrue("undistributed element not found in bound element", test);

	}
	
	/**
	 * Tests TemplateElementImpl.unassignChildren in case:
	 * - There is tree under the template
	 */
	
	public void testUnassignChildrenTree() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
		
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont1 = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		ContentElementImpl cont2 = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.appendChild(b);
		a.appendChild(c);
		a.initFinalFlattenedTree();
		
		templ.appendChild(cont1);
		templ.appendChild(d);
		d.appendChild(cont2);
		cont1.assignElement(b);
		cont2.assignElement(c);
		
		templ.unassignChildren(a, templ, false);
		
		Node child = templ.getFirstChild();
		
		assertNotNull("Template was changed", child);
		
		assertEquals("Template was changed", cont1.toString(), child.toString());
		
		child = child.getNextSibling();
		
		assertNotNull("Template was changed", child);
		
		assertEquals("Template was changed", d.toString(), child.toString());
		
		assertNull("Template was changed", child.getNextSibling());
		
		child = child.getFirstChild();
		
		assertNotNull("Template was changed", child);
		
		assertEquals("Template was changed", cont2.toString(), child.toString());
		
		Vector v = cont1.getAssignedElements();
		
		assertEquals("Wrong number of assigned elements", 0, v.size());
		
		v = cont2.getAssignedElements();
		
		assertEquals("Wrong number of assigned elements", 0, v.size());
		
		Vector n = a.getUndistributedChildNodes();
		
		assertEquals("Wrong number of elements added to bound element", 2, n.size());
		
		boolean test = false;
		for(int i = 0; i < 2; i++)
		{
			if(b == (Node)n.elementAt(i))
			{
				test = true;
			}
		}
		assertTrue("undistributed element not found in bound element", test);
		
		test = false;
		for(int i = 0; i < 2; i++)
		{
			if(c == (Node)n.elementAt(i))
			{
				test = true;
			}
		}
		assertTrue("undistributed element not found in bound element", test);

	}
	
	/**
	 * Tests TemplateElementImpl.unassignChildren in case:
	 * - There is locked content under the template and all childs cannot be unassigned  
	 */
	
	public void testUnassignChildrenLocked() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.initFinalFlattenedTree();
		
		templ.appendChild(cont);
		cont.assignElement(b);
		cont.assignElement(c);
		
		cont.setAttribute("locked", "true");
		
		templ.unassignChildren(a, templ, false);
		
		Node child = templ.getFirstChild();
		
		assertNotNull("Template was changed", child);
		
		assertEquals("Template was changed", cont.toString(), child.toString());
		
		Vector v = cont.getAssignedElements();
		
		assertEquals("Wrong number of assigned elements", 2, v.size());
		
		Vector n = a.getUndistributedChildNodes();
		
		assertEquals("Wrong number of elements added to bound element", 0, n.size());
	}
	
	/**
	 * Tests TemplateElementImpl.unassignChildren in case:
	 * - There is locked content under the template and all childs 
	 *   cannot be unassigned, but the unassigment is forced.
	 */
	
	public void testUnassignChildrenLockedForce() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.appendChild(b);
		a.appendChild(c);
		
		a.initFinalFlattenedTree();
		
		templ.appendChild(cont);
		cont.assignElement(b);
		cont.assignElement(c);
		
		cont.setAttribute("locked", "true");
		
		
		templ.unassignChildren(a, templ, true);
		
		Node child = templ.getFirstChild();
		
		assertNotNull("Template was changed", child);
		
		assertEquals("Template was changed", cont.toString(), child.toString());
		
		Vector v = cont.getAssignedElements();
		
		assertEquals("Wrong number of assigned elements", 0, v.size());
		
		Vector n = a.getUndistributedChildNodes();
		
		assertEquals("Wrong number of elements added to bound element", 2, n.size());
	
		boolean test = false;
		for(int i = 0; i < 2; i++)
		{
			if(b == (Node)n.elementAt(i))
			{
				test = true;
			}
		}
		assertTrue("undistributed element not found in bound element", test);
		
		test = false;
		for(int i = 0; i < 2; i++)
		{
			if(c == (Node)n.elementAt(i))
			{
				test = true;
			}
		}
		assertTrue("undistributed element not found in bound element", test);

	}
	
	public void testCreateFinalFlattenedTreeFirst()
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		
		a.initFinalFlattenedTree();
		a.appendChild(b);
		
		templ.appendChild(c);
		
		templ.createFinalFlattenedTree(a, true, false);
		
        Node child = a.getFirstChild();
		
		assertNotNull("Final tree was not generated", child);
		
		assertEquals("The final tree was not generated correctly", c.toString(), child.toString());
				
		assertNull("The final tree was not generated correctly", child.getNextSibling());
		
		assertNull("The final tree was not generated correctly", child.getFirstChild());
	}
	
	public void testCreateFinalFlattenedTreeLast()
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		InheritedElementImpl inh = new InheritedElementImpl(doc, "http://www.w3.org/ns/xbl","inherited");
		InheritedElementImpl inh2 = new InheritedElementImpl(doc, "http://www.w3.org/ns/xbl","inherited");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.appendChild(inh);
		templ.appendChild(b);
		templ.appendChild(c);
		c.appendChild(inh2);
		c.appendChild(cont);
		cont.appendChild(d);
		
		templ.createFinalFlattenedTree(a, false, true);
		
        Node child = a.getFirstChild();
		
		assertNotNull("Final tree was not generated", child);
		
		assertEquals("The final tree was not generated correctly", b.toString(), child.toString());
				
		assertNotNull("The final tree was not generated correctly", child.getNextSibling());
		
		child = child.getNextSibling();
		
		assertEquals("The final tree was not generated correctly", c.toString(), child.toString());

		child = child.getFirstChild();
		
		assertEquals("The final tree was not generated correctly", d.toString(), child.toString());
		
		child = child.getNextSibling();
		
		assertNull("The final tree was not generated correctly", child);
	}
	
	public void testCreateFinalFlattenedTreeMiddle()
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");	
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		InheritedElementImpl inh = new InheritedElementImpl(doc, "http://www.w3.org/ns/xbl","inherited");
		InheritedElementImpl inh2 = new InheritedElementImpl(doc, "http://www.w3.org/ns/xbl","inherited");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.appendChild(inh);
		templ.appendChild(b);
		b.appendChild(inh2);
		b.appendChild(cont);
		
		cont.assignElement(d);
		cont.appendChild(c);
		
		templ.createFinalFlattenedTree(a, false, false);
		
		Node child = a.getFirstChild();
			
	    assertNotNull("Final tree was not generated", child);
			
		assertEquals("The final tree was not generated correctly", b.toString(), child.toString());
					
		assertNotNull("The final tree was not generated correctly", child.getFirstChild());
			
		child = child.getFirstChild();
			
		assertEquals("The final tree was not generated correctly", inh2.toString(), child.toString());

		child = child.getNextSibling();
		
		assertEquals("The final tree was not generated correctly", cont.toString(), child.toString());

		Vector v = ((ContentElementImpl)child).getAssignedElements();
		
		assertEquals("Wrong number of assigned elements", 1, v.size());
		
		assertEquals("The final tree was not generated correctly", d.toString(), v.elementAt(0).toString());

		child = child.getFirstChild();
		
		assertEquals("The final tree was not generated correctly", c.toString(), child.toString());
	}
	
	public void testCreateFinalFlattenedTreeMiddleNoInherited()
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		
		a.appendChild(c);
		
		templ.appendChild(b);
		
		templ.createFinalFlattenedTree(a, false, false);
		
		Node child = a.getFirstChild();
		
	    assertNotNull("Final tree was not generated", child);
			
		assertEquals("The final tree was not generated correctly", c.toString(), child.toString());
					
        assertNull("The final tree was not generated correctly", child.getNextSibling());
		
		assertNull("The final tree was not generated correctly", child.getFirstChild());
	
	}
	
	public void testCreateFinalFlattenedTreeFirstAndLast()
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl c = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "c");
		XSmilesElementImpl d = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "d");
		
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		InheritedElementImpl inh = new InheritedElementImpl(doc, "http://www.w3.org/ns/xbl","inherited");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		a.appendChild(b);
		
		templ.appendChild(c);
		c.appendChild(inh);
		c.appendChild(cont);
		cont.appendChild(d);
		
		templ.createFinalFlattenedTree(a, true, true);
		
        Node child = a.getFirstChild();
		
		assertNotNull("Final tree was not generated", child);
		
		assertEquals("The final tree was not generated correctly", c.toString(), child.toString());
				
		child = child.getFirstChild();
		
		assertEquals("The final tree was not generated correctly", d.toString(), child.toString());
		
		child = child.getNextSibling();
		
		assertNull("The final tree was not generated correctly", child);
	
	}
	
	public void testXBLAttr() 
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		
		templ.appendChild(b);
		b.setAttribute("xbl:attr", "value");
		a.setAttribute("value", "correctval1"); 
	    TemplateElementImpl t = templ.createShadowContent(a);
	    assertEquals("correctval1", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value")); 

	    b.setAttribute("xbl:attr", "value=testvalue"); 
	    a.setAttribute("testvalue", "correctval2"); 
	    t = templ.createShadowContent(a); 
	    assertEquals("correctval2", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value"));
	    
	    b.setAttribute("xbl:attr", "xbl:value=xhtm:testvalue"); 
	    a.setAttribute("testvalue", "correctval3"); 
	    t = templ.createShadowContent(a); 
	    assertEquals("correctval3", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value"));
	    
	    b.setAttribute("xbl:attr", "value=testvalue#text value2=testvalue2 value3"); 
	    a.setAttribute("testvalue", "correctval4");
	    a.setAttribute("testvalue2", "correctval5"); 
	    a.setAttribute("value3", "correctval6"); 
	    t = templ.createShadowContent(a); 
	    assertEquals("correctval4", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value"));
	    assertEquals("correctval5", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value2"));
	    assertEquals("correctval6", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value3"));
	    
	    b.setAttribute("xbl:attr", "value#grr xbl:text=value"); 
	    a.setAttribute("value", "errorval7");
	    t = templ.createShadowContent(a); 
	    assertTrue(!((XSmilesElementImpl)t.getFirstChild()).hasAttribute("value"));
	    
	    b.setAttribute("xbl:attr", ""); 
	    a.setAttribute("value", "errorval8");
	    t = templ.createShadowContent(a); 
	    assertTrue(!((XSmilesElementImpl)t.getFirstChild()).hasAttribute("value"));
	}
	
	public void testXBLText ()
	{
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		TemplateElementImpl templ = new TemplateElementImpl(doc, "http://www.w3.org/ns/xbl","template");
		templ.appendChild(b);
		
		b.setAttribute("xbl:attr", "xbl:text=value"); 
		a.setAttribute("value", "textnode1");
		TemplateElementImpl t = templ.createShadowContent(a);
		Node x = t.getFirstChild().getFirstChild(); 
		assertTrue(x.getNodeType()==Node.TEXT_NODE);
		assertEquals("textnode1", x.getNodeValue()); 
		
		b.setAttribute("xbl:attr", "value2=xbl:text"); 
		XSmilesTextImpl text = new XSmilesTextImpl(doc, "textnode2");
		XSmilesTextImpl text2 = new XSmilesTextImpl(doc, "textnode3"); 
		a.appendChild(text);
		a.appendChild(text2);
		t = templ.createShadowContent(a); 
		assertEquals("textnode2textnode3", ((XSmilesElementImpl)t.getFirstChild()).getAttribute("value2"));
		
		b.setAttribute("xbl:attr", "xbl:text=value4#esa"); 
		a.setAttribute("value4", "textnode3");
		t = templ.createShadowContent(a);
		assertTrue(!(t.getFirstChild().hasChildNodes()));
		
		b.setAttribute("xbl:attr", "xbl:text=value4#text value5=xbl:text"); 
		a.setAttribute("value4", "textnode4");
		text.replaceData(0, 9, "textnode5");
		a.removeChild(text2); 
		t = templ.createShadowContent(a);
		
		assertEquals("textnode5", ((XSmilesElementImpl) t.getFirstChild()).getAttribute("value5"));
		x = t.getFirstChild().getFirstChild();
	    assertEquals("textnode4", x.getNodeValue()); 
	}
 
}
