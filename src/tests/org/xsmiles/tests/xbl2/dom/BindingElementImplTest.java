package org.xsmiles.tests.xbl2.dom;

import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.*;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import junit.framework.TestCase;

public class BindingElementImplTest extends TestCase
{

    
    public BindingElementImplTest(String name) {
        super(name);
    }
    
    protected void setUp() throws Exception
    {
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }


    public void testBindingMatchesElement()
    {
        
        DocumentImpl doc = new DocumentImpl();
        XSmilesElementImpl e1 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "h1");
        XSmilesElementImpl e2 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "h2");
        XSmilesElementImpl e3 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "h3");
        XSmilesElementImpl e4 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "h3");
        XSmilesElementImpl e5 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "img");
        XSmilesElementImpl e6 = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "img");
        
        BindingElementImpl b1 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b2 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b3 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b4 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b5 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b6 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b7 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b8 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b9 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
               
        b1.setAttribute("element", "#elem1");
        b2.setAttribute("element", "elem1");
        b3.setAttribute("element", "h1");
        b4.setAttribute("element", "h3");
        b5.setAttribute("element", "h1,h2");
        b6.setAttribute("element", "h1,h2,h3");
        b7.setAttribute("element", ".class1");
        b8.setAttribute("element", "h1 img");

        e3.setAttribute("id", "elem1");
        e6.setAttribute("class", "class1");
        e1.appendChild(e6);
                
        assertTrue("Binding matches element!", b1.bindingMatchesElement(e3));
        assertTrue("Binding does not match element!", !b1.bindingMatchesElement(e4));
        assertTrue("Binding does not match element!", !b2.bindingMatchesElement(e3));
        assertTrue("Binding matches element!", b3.bindingMatchesElement(e1));
        assertTrue("Binding matches element!", b4.bindingMatchesElement(e3));
        assertTrue("Binding matches element!", b4.bindingMatchesElement(e4));
        assertTrue("Binding matches element!", b5.bindingMatchesElement(e1));
        assertTrue("Binding matches element!", b5.bindingMatchesElement(e2));
        assertTrue("Binding matches element!", b6.bindingMatchesElement(e1));
        assertTrue("Binding matches element!", b6.bindingMatchesElement(e2));
        assertTrue("Binding matches element!", b6.bindingMatchesElement(e3));
        assertTrue("Binding matches element!", b6.bindingMatchesElement(e4));
        assertTrue("Binding matches element!", b6.bindingMatchesElement(e1));
        assertTrue("Binding matches element!", b7.bindingMatchesElement(e6));
        assertTrue("Binding matches element!", b8.bindingMatchesElement(e6));        
        assertTrue("Binding does not match element!", !b8.bindingMatchesElement(e5));
        assertTrue("Binding does not match element!", !b9.bindingMatchesElement(e1));
        
    }


    public void testMatchesElementURI()
    {
             
        DocumentImpl doc = new DocumentImpl();
        BindingElementImpl b1 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b2 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
        BindingElementImpl b3 = new BindingElementImpl(doc, "http://www.w3.org/ns/xbl","binding");
               
        b1.setAttribute("id", "b1");
        b2.setAttribute("id", "b2");
        b3.setAttribute("id", "b1");

        b1.setOrigBaseURI("http://www.site.com/binding.xml");
        b2.setOrigBaseURI("http://www.site.com/binding.xml");
        b3.setOrigBaseURI("http://www.site2.com/binding.xml");
        
        String uri1 = "http://www.site.com/binding.xml#b1"; 
        String uri2 = "http://www.site.com/binding.xml#b2"; 
        String uri3 = "http://www.site.com/binding.xml b1";
        String uri4 = "http://www.site.com/binding.xml"; 
        
        b1.setAttribute("element", "#elem1");
        b2.setAttribute("element", "elem1");
        b3.setAttribute("element", "h1");
                        
        assertTrue("Binding matches element URI!", b1.matchesElementURI(uri1));
        assertTrue("Binding matches element URI!", b2.matchesElementURI(uri2));
        assertTrue("Binding does not match element URI!", !b3.matchesElementURI(uri1));
        assertTrue("Binding does not match element URI!", !b1.matchesElementURI(uri3));
        assertTrue("Binding does not match element URI!", !b1.matchesElementURI(uri4));
                
    }

}
