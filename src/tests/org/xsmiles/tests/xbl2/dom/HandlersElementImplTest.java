package org.xsmiles.tests.xbl2.dom;

import org.apache.xerces.dom.CoreDocumentImpl;
import org.apache.xerces.dom.DocumentImpl;

import org.apache.xerces.dom.TextImpl;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.HandlerElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.HandlersElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.ScriptHandler;
import junit.framework.TestCase;

public class HandlersElementImplTest extends TestCase
{
    DocumentImpl doc;
    String xblns = new String("http://www.w3.org/ns/xbl");
    String htmns = new String("http://www.w3.org/1999/xhtm");

    ScriptHandler scriptHandler;
    HandlersElementImpl handlers;
    HandlerElementImpl h1, h2, h3;
    
    public HandlersElementImplTest(String name)
    {
        super(name);
    }

    protected void setUp() throws Exception
    {
        doc = new DocumentImpl();
        handlers = new HandlersElementImpl(doc, xblns, "handlers");
        scriptHandler = new ScriptHandler();
        h1 = new HandlerElementImpl(doc, xblns, "handler");
        h1.appendChild(new TextImpl((CoreDocumentImpl)doc, "h1.action()"));
        h2 = new HandlerElementImpl(doc, xblns, "handler");
        h2.appendChild(new TextImpl((CoreDocumentImpl)doc, "h2.action()"));
        h3 = new HandlerElementImpl(doc, xblns, "handler");
        h3.appendChild(new TextImpl((CoreDocumentImpl)doc, "h3.action()"));
        super.setUp();
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    public final void testSetScriptHandler()
    {
        handlers.setScriptHandler(scriptHandler);
        
        assertEquals(scriptHandler, handlers.getScriptHandler());
    }
    
    public final void testApply()
    {
        XSmilesElementImpl xse = new XSmilesElementImpl(doc, htmns, "div");
        h1.setAttribute("event", "click");
        handlers.appendChild(h1);
        
        handlers.apply(xse);
        assertEquals("div -> h1.action()", handlers.getScriptHandler().toString());
    }

    public final void testApplyWithMultipleHandlers()
    {
        XSmilesElementImpl xse = new XSmilesElementImpl(doc, htmns, "div");
        h1.setAttribute("event", "click");
        h2.setAttribute("event", "click");
                
        handlers.appendChild(h1);
        handlers.appendChild(h2);
        
        handlers.apply(xse);
        assertEquals("div -> h1.action()\ndiv -> h2.action()", handlers.getScriptHandler().toString());
    }

}
