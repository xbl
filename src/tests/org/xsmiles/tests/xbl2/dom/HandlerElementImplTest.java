package org.xsmiles.tests.xbl2.dom;

import org.apache.xerces.dom.CoreDocumentImpl;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.apache.xerces.dom.events.MutationEventImpl;

import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.HandlerElementImpl;
import junit.framework.TestCase;
import java.util.Arrays;

public class HandlerElementImplTest extends TestCase
{
    HandlerElementImpl h1;
    HandlerElementImpl h2;
    String xblns = new String("http://www.w3.org/ns/xbl");
    String htmns = new String("http://www.w3.org/1999/xhtm");
    private DocumentImpl doc;

    public HandlerElementImplTest(String name)
    {
        super(name);
    }

    protected void setUp() throws Exception
    {
        doc = new DocumentImpl();
        h1 = new HandlerElementImpl(doc, xblns, "handler");
        h2 = new HandlerElementImpl(doc, xblns, "handler");
        super.setUp();
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public final void testGetXBLHandler()
    {
        h1.appendChild(new TextImpl((CoreDocumentImpl)doc, "h1.action()"));
        
        XBLHandler handler = h1.getXBLHandler();
        
        assertEquals("h1.action()", handler.getScript());
    }
    
    public final void testPropagate()
    {
        h1.setAttribute("propagate", "continue");
        assertTrue(h1.propagate());
        
        h1.setAttribute("propagate", "stop");
        assertTrue(!h1.propagate());
        
        h1.setAttribute("propagate", "foobar");
        assertTrue(h1.propagate());
        
        h1.setAttribute("propagate", "stop");
        assertTrue(!h1.propagate());
    }

    public final void testacceptsButton() 
    {
                
        h1.setAttribute("button", "1 2 3");
        assertTrue(!h1.acceptsButton(0));
        assertTrue(h1.acceptsButton(1));
        assertTrue(h1.acceptsButton(2));
        assertTrue(h1.acceptsButton(3));
        assertTrue(!h1.acceptsButton(4));
    
        h1.setAttribute("button", "  200  200  0999 \t ");
        assertTrue(h1.acceptsButton(200));
        assertTrue(h1.acceptsButton(999));
        assertTrue(!h1.acceptsButton(1));
        
        h1.setAttribute("button", " foo bar 2 0 000 lol ");
        assertTrue(h1.acceptsButton(0));
        assertTrue(h1.acceptsButton(2));

        h1.setAttribute("button", " ");
        assertTrue(!h1.acceptsButton(1));
        
    }

    public final void testacceptsModifiers() 
    {
                
        //acceptsModifiers(ctrlKey, shiftKey, altKey, metaKey);
        
        h1.setAttribute("modifiers", "any");
        assertTrue(h1.acceptsModifiers(true, true, true, true));
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(h1.acceptsModifiers(true, false, true, false));

        h1.setAttribute("modifiers", "");
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, true));
        
        h1.setAttribute("modifiers", "  \t \n");
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, true));
        
        h1.setAttribute("modifiers", "none");
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, true));
        
        h1.setAttribute("modifiers", "accel");
        // These works only on Windows platforms
        assertTrue(h1.acceptsModifiers(true, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, false));
        // These works only on Mac
        //assertTrue(h1.acceptsModifiers(false, false, false, true));
        //assertTrue(!h1.acceptsModifiers(false, false, false, false));
        
        h1.setAttribute("modifiers", "access");
        assertTrue(h1.acceptsModifiers(false, false, true, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, false));

        h1.setAttribute("modifiers", "-shift");
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, true, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, true, true));

        h1.setAttribute("modifiers", "+shift");
        assertTrue(h1.acceptsModifiers(false, true, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, false, false));

        h1.setAttribute("modifiers", "shift?");
        assertTrue(h1.acceptsModifiers(false, true, false, false));
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, true, false));
        assertTrue(!h1.acceptsModifiers(true, false, true, true));
        
        h1.setAttribute("modifiers", "alt control");
        assertTrue(h1.acceptsModifiers(true, false, true, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, false));
        assertTrue(!h1.acceptsModifiers(false, false, true, false));
                        
        h1.setAttribute("modifiers", "+alt +control");
        assertTrue(h1.acceptsModifiers(true, false, true, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, false));
        assertTrue(!h1.acceptsModifiers(false, false, true, false));

        h1.setAttribute("modifiers", "+alt -control");
        assertTrue(!h1.acceptsModifiers(true, false, true, false));
        assertTrue(h1.acceptsModifiers(false, false, true, false));
        assertTrue(!h1.acceptsModifiers(false, false, true, true));
        
        h1.setAttribute("modifiers", "alt control?");
        assertTrue(h1.acceptsModifiers(false, false, true, false));
        assertTrue(h1.acceptsModifiers(true, false, true, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, false));
        assertTrue(!h1.acceptsModifiers(true, false, false, false));

        h1.setAttribute("modifiers", "control +access");
        assertTrue(h1.acceptsModifiers(true, false, true, false));
        assertTrue(!h1.acceptsModifiers(true, false, false, false));

        h1.setAttribute("modifiers", "+alt -alt");
        assertTrue(!h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, true));

        h1.setAttribute("modifiers", "+alt -accel");
        assertTrue(!h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
               
        h1.setAttribute("modifiers", "foobar?");
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(!h1.acceptsModifiers(false, false, true, false));

        h1.setAttribute("modifiers", "foo? bar?");
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(!h1.acceptsModifiers(false, false, true, false));

        h1.setAttribute("modifiers", "foo? bar");
        assertTrue(!h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(!h1.acceptsModifiers(false, false, true, false));
        
        h1.setAttribute("modifiers", "alt foobar?");
        assertTrue(h1.acceptsModifiers(false, false, true, false));
        assertTrue(!h1.acceptsModifiers(true, false, true, false));

        h1.setAttribute("modifiers", "alt foobar");
        assertTrue(!h1.acceptsModifiers(false, false, true, false));

        h1.setAttribute("modifiers", "any -control");
        assertTrue(h1.acceptsModifiers(false, true, true, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, false));

        h1.setAttribute("modifiers", "any none");
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, true, true));
        assertTrue(!h1.acceptsModifiers(false, true, false, false));

        h1.setAttribute("modifiers", " control?  none  +shift");
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(true, true, false, false));
        
        h1.setAttribute("modifiers", "Alt");
        assertTrue(!h1.acceptsModifiers(false, false, true, false));

        h1.setAttribute("modifiers", "alt alt alt");
        assertTrue(h1.acceptsModifiers(false, false, true, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, false));

        h1.setAttribute("modifiers", "-control +shift -alt meta?");
        assertTrue(h1.acceptsModifiers(false, true, false, true));
        assertTrue(h1.acceptsModifiers(false, true, false, false));
        assertTrue(!h1.acceptsModifiers(false, false, false, false));

        h1.setAttribute("modifiers", "-control any -shift -meta -alt ");
        assertTrue(h1.acceptsModifiers(false, false, false, false));
        assertTrue(!h1.acceptsModifiers(false, true, false, false));
        
    }    
    
    public final void testParseSpaceSeparatedList()
    {        

        String ssList1 = "1 2 3 4 5 6";
        String ssList2 = "\n \t\n 1\t\t\n\r2    3    4\n5 6\t \n  \r    ";
        String ssList3 = "123 456 99 0    ";
        String ssList4 = "  key - note - rocks";
        String ssList5 = " \n\t  ";
        
        String[] result1 = {"1","2","3","4","5","6"};
        String[] result2 = {"1","2","3","4","5","6"};
        String[] result3 = {"123","456","99","0"};
        String[] result4 = {"key","-","note","-","rocks"};
        String[] result5 = {};
        
        String[] array1 = HandlerElementImpl.parseSpaceSeparatedList(ssList1);
        String[] array2 = HandlerElementImpl.parseSpaceSeparatedList(ssList2);
        String[] array3 = HandlerElementImpl.parseSpaceSeparatedList(ssList3);
        String[] array4 = HandlerElementImpl.parseSpaceSeparatedList(ssList4);
        String[] array5 = HandlerElementImpl.parseSpaceSeparatedList(ssList5);
        
        assertTrue(Arrays.equals(result1, array1));
        assertTrue(Arrays.equals(result2, array2));
        assertTrue(Arrays.equals(result3, array3));
        assertTrue(Arrays.equals(result4, array4));
        assertTrue(Arrays.equals(result5, array5));
        
    }
    public final void testAcceptsClickCount ()
    {
    	h1.setAttribute("click-count", "1");
    	assertTrue(h1.acceptsClickCount(1));
    	assertTrue(!h1.acceptsClickCount(2));

    	
    	h1.setAttribute("click-count", "1 0 3 6");
    	assertTrue(h1.acceptsClickCount(1));
    	assertTrue(h1.acceptsClickCount(3));
    	assertTrue(h1.acceptsClickCount(6));
    	assertTrue(!h1.acceptsClickCount(4));
  
    	
    	h1.setAttribute("click-count", "0 3 foo 6");
    	assertTrue(h1.acceptsClickCount(0));
    	assertTrue(h1.acceptsClickCount(6));
    	assertTrue(!h1.acceptsClickCount(2));
    	
    }
    
    public final void testAcceptsTrusted()
    {
    	h1.setAttribute("trusted", "true");
    	assertTrue(h1.acceptsTrusted(true)); 
    	assertTrue(!h1.acceptsTrusted(false));
    	
    	h1.setAttribute("trusted", "foobaz");
    	assertTrue(h1.acceptsTrusted(true)); 
    	assertTrue(h1.acceptsTrusted(false));

    	
    	h1.removeAttribute("trusted"); 
    	assertTrue(h1.acceptsTrusted(true)); 
    	assertTrue(h1.acceptsTrusted(false));

    }
    
    public final void testAcceptsPhase() 
    {
    	  
        //public static final short CAPTURING_PHASE           = 1;
        //public static final short AT_TARGET                 = 2;
        //public static final short BUBBLING_PHASE            = 3;
    
        h1.setAttribute("phase", "capture");
        assertTrue(h1.acceptsPhase(1));
        assertTrue(!h1.acceptsPhase(2));
        assertTrue(!h1.acceptsPhase(3));
        assertTrue(!h1.acceptsPhase(2019716164));
    
        h1.setAttribute("phase", "target"); 
        assertTrue(!h1.acceptsPhase(1));
        assertTrue(h1.acceptsPhase(2));
        assertTrue(!h1.acceptsPhase(3));
        assertTrue(!h1.acceptsPhase(2019716164));
    
        h1.setAttribute("phase", "default-action"); 
        assertTrue(!h1.acceptsPhase(1));
        assertTrue(!h1.acceptsPhase(2));
        assertTrue(!h1.acceptsPhase(3));
        assertTrue(h1.acceptsPhase(2019716164));
        
        h1.setAttribute("phase", "bubbleoorsomethingelse"); 
        assertTrue(!h1.acceptsPhase(1));
        assertTrue(!h1.acceptsPhase(2));
        assertTrue(h1.acceptsPhase(3));
        assertTrue(!h1.acceptsPhase(2019716164));
        
        h1.removeAttribute("phase");
        assertTrue(h1.acceptsPhase(1));
        assertTrue(h1.acceptsPhase(2));
        assertTrue(h1.acceptsPhase(3));
        assertTrue(h1.acceptsPhase(2019716164));
    }
    
    public final void testAcceptsPrevValue() {
        
        h2.setAttribute("prev-value", "top"); 
        assertTrue(h2.acceptsPrevValue("top"));
        assertTrue(!h2.acceptsPrevValue("bottom"));
        assertTrue(!h2.acceptsPrevValue(null));

        h2.setAttribute("prev-value", ""); 
        assertTrue(h2.acceptsPrevValue(""));
        assertTrue(!h2.acceptsPrevValue("bottom"));
        assertTrue(!h2.acceptsPrevValue(null));
        
        h2.removeAttribute("prev-value"); 
        assertTrue(h2.acceptsPrevValue(null));
        assertTrue(h2.acceptsPrevValue(""));
        assertTrue(h2.acceptsPrevValue("top"));
        assertTrue(h2.acceptsPrevValue("bottom"));        
        
    }

    public final void testAcceptsNewValue() {

        h2.setAttribute("new-value", "top"); 
        assertTrue(h2.acceptsNewValue("top"));
        assertTrue(!h2.acceptsNewValue("bottom"));
        assertTrue(!h2.acceptsNewValue(null));

        h2.setAttribute("new-value", ""); 
        assertTrue(h2.acceptsNewValue(""));
        assertTrue(!h2.acceptsNewValue("bottom"));
        assertTrue(!h2.acceptsNewValue(null));
        
        h2.removeAttribute("new-value"); 
        assertTrue(h2.acceptsNewValue(null));
        assertTrue(h2.acceptsNewValue(""));
        assertTrue(h2.acceptsNewValue("top"));
        assertTrue(h2.acceptsNewValue("bottom"));        
        
    }

    public final void testAcceptsAttrName() {

        h2.setAttribute("attr-name", "valign"); 
        assertTrue(h2.acceptsAttrName("valign"));
        assertTrue(!h2.acceptsAttrName("halign"));
        assertTrue(!h2.acceptsAttrName(null));

        h2.setAttribute("attr-name", ""); 
        assertTrue(h2.acceptsAttrName(""));
        assertTrue(!h2.acceptsAttrName("halign"));
        assertTrue(!h2.acceptsAttrName(null));
        
        h2.removeAttribute("attr-name"); 
        assertTrue(h2.acceptsAttrName(null));
        assertTrue(h2.acceptsAttrName(""));
        assertTrue(h2.acceptsAttrName("valign"));
        assertTrue(h2.acceptsAttrName("halign"));        
        
    }

    public final void testAcceptsAttrChange() {
        
        /* 
         * If specified, this attribute's value must be 
         * a space-separated list of values from the three literal 
         * (case-sensitive) keywords: modification, addition, removal 
         * (with no duplicates), which map to the DOM values 
         * 0x00, 0x01, or 0x02 respectively. 
         */
        
        short modification = MutationEventImpl.MODIFICATION;
        short addition = MutationEventImpl.ADDITION;
        short removal = MutationEventImpl.REMOVAL;
        
        h2.setAttribute("attr-change", "modification"); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(!h2.acceptsAttrChange(addition));
        assertTrue(!h2.acceptsAttrChange(removal));

        h2.setAttribute("attr-change", "addition"); 
        assertTrue(!h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(!h2.acceptsAttrChange(removal));

        h2.setAttribute("attr-change", "removal"); 
        assertTrue(!h2.acceptsAttrChange(modification));
        assertTrue(!h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));

        h2.setAttribute("attr-change", "removal addition modification"); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));

        h2.setAttribute("attr-change", "modification removal"); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(!h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));
        
        h2.setAttribute("attr-change", "modification addition"); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(!h2.acceptsAttrChange(removal));

        h2.setAttribute("attr-change", ""); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));

        // Unknown and duplicate values are in error and the UA must ignore them
        h2.setAttribute("attr-change", " foo  bar   \n  \t "); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));

        // ... although without causing correct values to be dropped.
        h2.setAttribute("attr-change", " foo  bar  addition  \n  removal \t "); 
        assertTrue(!h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));

        h2.setAttribute("attr-change", "addition addition addition"); 
        assertTrue(!h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(!h2.acceptsAttrChange(removal));
        
        // Remember: values are case-sensitive
        h2.setAttribute("attr-change", "Addition Modification Removal addition"); 
        assertTrue(!h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(!h2.acceptsAttrChange(removal));
        
        h2.removeAttribute("attr-change"); 
        assertTrue(h2.acceptsAttrChange(modification));
        assertTrue(h2.acceptsAttrChange(addition));
        assertTrue(h2.acceptsAttrChange(removal));
        
    }

}
