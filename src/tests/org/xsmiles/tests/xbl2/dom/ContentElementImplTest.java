package org.xsmiles.tests.xbl2.dom;

import junit.framework.TestCase;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ContentElementImpl;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import java.util.Vector;

/**
 * Test for ContentElementImpl class.
 * 
 * @author Jukka Julku
 *
 */

public class ContentElementImplTest extends TestCase {
	
	protected void setUp() throws Exception {
		super.setUp();

	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public ContentElementImplTest(String name) {
		super(name);
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - the element assigned is ok
	 * 
	 * true should be returned
	 *
	 */	

	public void testAssignElement() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		assertTrue("Element was not assigned!", cont.assignElement(a));
		
        Vector v = cont.getAssignedElements();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 1);
        
        assertSame("Wrong element!", v.elementAt(0), a);
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - the element assigned is ok, but the content is locked.
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testAssignElementLocked() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("locked", "true");	
		
		assertTrue("Element was assigned, when content was locked!", !cont.assignElement(a));
		
        Vector v = cont.getAssignedElements();
        
        assertEquals("Non-empty vector!", v.size(), 0);
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - the element assigned is null
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testAssignElementNull() {
		DocumentImpl doc = new DocumentImpl();
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		assertTrue("null was assigned!", !cont.assignElement(null));
		
        Vector v = cont.getAssignedElements();
        
        assertEquals("Non-empty vector!", v.size(), 0);
	}
	
	/**
	 * Tests ContentElementImpl.unassignElement in case where:
	 * - the element is assigned
	 * 
	 * true should be returned
	 *
	 */	

	public void testUnassignElement() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.assignElement(a);
		
		assertTrue("Element was not unassigned!", cont.unassignElement(a, false));
		
        Vector v = cont.getAssignedElements();
        
        assertEquals("Non-empty vector!", v.size(), 0);
	}
	
	/**
	 * Tests ContentElementImpl.unassignElement in case where:
	 * - the element is not assigned
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testUnassignElementUnknown() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.assignElement(a);
		
		assertTrue("Unknown element was unassigned!", !cont.unassignElement(b, false));
				
        Vector v = cont.getAssignedElements();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 1);
	}
	
	/**
	 * Tests ContentElementImpl.unassignElement in case where:
	 * - the element could be unassigned, but the node is locked
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testUnassignElementLocked() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.assignElement(a);
		cont.setAttribute("locked", "true");	
		
		assertTrue("Element was unassigned, when content was locked!", !cont.unassignElement(a, false));
		
        Vector v = cont.getAssignedElements();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 1);	
	}
	
	/**
	 * Tests ContentElementImpl.unassignElement in case where:
	 * - the element could be unassigned, but the node is locked.
	 * - The unassigment is forced.
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testUnassignElementLockedForce() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.assignElement(a);
		cont.setAttribute("locked", "true");	
		
		assertTrue("Forcing unassigment did not work", cont.unassignElement(a, true));
		
        Vector v = cont.getAssignedElements();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 0);	
	}
	
	/**
	 * Tests ContentElementImpl.matchesElement in case where:
	 * - all elements match the content element
	 * 
	 * true should be returned
	 *
	 */		
	
	public void testMatchesElementMatchAll() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl node = new XSmilesElementImpl(doc, "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		assertTrue("Node didin't match content element!",cont.matchesElement(node)); 
	}
	
	/**
	 * Tests ContentElementImpl.matchesElement in case where:
	 * - the element matches the include atribute 
	 * 
	 * true should be returned
	 *
	 */	
	
	public void testMatchesElementMatchSpecific() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl node = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("includes", "a");	
		
		assertTrue("Node didin't match content element!", cont.matchesElement(node)); 
	
	}
	
	/**
	 * Tests ContentElementImpl.matchesElement in case where:
	 * - the element does not match
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testMatchesElementNoMatch() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl node = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("includes", "a");		
		
		assertTrue("Node did match content element!", !cont.matchesElement(node)); 
	
	}
	
	/**
	 * Tests ContentElementImpl.matchesElement in case where:
	 * - the element matches, but the selector is complex
	 * 
	 * true should be returned
	 *
	 */	
	
	public void testMatchesElementMatchComplex() {		
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl parent = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		XSmilesElementImpl node = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		parent.appendChild(node);
		
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("includes", "b > c, e, b > a");		
		
		assertTrue("Node did not match content element!", cont.matchesElement(node)); 
	
	}
	
	/**
	 * Tests ContentElementImpl.matchesElement in case where:
	 * - the element matches, but the node is locked
	 * 
	 * false should be returned
	 *
	 */	
	
	public void testMatchesElementMatchButLocked() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl node = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("locked", "true");	
		
		assertTrue("Node did match content element!", !cont.matchesElement(node)); 
	
	}
	
	/**
	 * Tests ContentElementImpl.getElementsToAssign in case where:
	 * - there are no childrens, but assigned elements
	 * 
	 * The assigned elements should be returned
	 *
	 */	
	
	public void testGetElementsToAssignNormal() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.assignElement(a);
		cont.assignElement(b);
		
        Vector v = cont.getElementsToAssign();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 2);
		
        assertSame("Wrong element!", v.elementAt(0), a);
        assertSame("Wrong element!", v.elementAt(1), b);
	}
	
	/**
	 * Tests ContentElementImpl.getElementsToAssign in case where:
	 * - there are no childrens or assigned elements
	 * 
	 * null should be returned
	 *
	 */	
	
	public void testGetElementsToAssignNone() {
		DocumentImpl doc = new DocumentImpl();
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
        Vector v = cont.getElementsToAssign();
        
        assertEquals("Non-empty vector!", v.size(), 0);
        
	}
	
	/**
	 * Tests ContentElementImpl.getElementsToAssign in case where:
	 * - there are childrens, but no assigned elements
	 * 
	 * The child elements should be returned
	 *
	 */	
	
	public void testGetElementsToAssignChildren() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
        cont.appendChild(a);
        cont.appendChild(b);
		
        Vector v = cont.getElementsToAssign();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 2);
        
        assertSame("Wrong element!", v.elementAt(0), a);
        assertSame("Wrong element!", v.elementAt(1), b);
        
        
	}
	
	/**
	 * Tests ContentElementImpl.getElementsToAssign in case where:
	 * - there are assigned elements
	 * - there are childrens
	 * 
	 * The assigned elements should be returned
	 *
	 */
	
	public void testGetElementsToAssignNormalNotChildren() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		XSmilesElementImpl b = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "b");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.assignElement(a);
        cont.appendChild(b);
        
        Vector v = cont.getElementsToAssign();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 1);
        assertSame("Wrong element!", v.elementAt(0), a);	
	}

	
	/**
	 * Tests ContentElementImpl.getAssignedElements in case where:
	 * - there is element assigned
	 *
	 */	

	public void testGetAssignedElements() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		
		assertTrue("Element was not assigned!", cont.assignElement(a));
		
        Vector v = cont.getAssignedElements();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 1);
        
        assertSame("Wrong element!", v.elementAt(0), a);
	}
	
	/**
	 * Tests ContentElementImpl.getAssignedElements in case where:
	 * - there is no element assigned
	 *
	 */	

	public void testGetAssignedElementsNoElement() {
		DocumentImpl doc = new DocumentImpl();
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");

        Vector v = cont.getAssignedElements();
        
        assertNotNull("Null vector!", v);
        assertEquals("Wrong vector size!", v.size() , 0);
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - apply-binding-sheets attribute is false
	 * 
	 */	
	
	public void testAssignElementWithApplyBindingSheetsFalse() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("apply-binding-sheets", "false");	
		
		assertTrue("Apply-binding-sheets: Wrong default value!",a.getApplyBindingSheets());
		cont.assignElement(a);
		assertTrue("Apply-binding-sheets: Wrong value (true)!", !a.getApplyBindingSheets());
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - apply-binding-sheets attribute is true
	 * 
	 */	
	
	public void testAssignElementWithApplyBindingSheetsTrue() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("apply-binding-sheets", "true");	
		
		assertTrue("Apply-binding-sheets: Wrong default value!", a.getApplyBindingSheets());
		cont.assignElement(a);
		assertTrue("Apply-binding-sheets: Wrong value (false)!", a.getApplyBindingSheets());
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - apply-binding-sheets attribute is missing
	 * 
	 */	
	
	public void testAssignElementWithoutApplyBindingSheets() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
	
		assertTrue("Apply-binding-sheets: Wrong default value!", a.getApplyBindingSheets());
		cont.assignElement(a);
		assertTrue("Apply-binding-sheets: Wrong value (true)!", !a.getApplyBindingSheets());
	}
	
	/**
	 * Tests ContentElementImpl.assignElement in case where:
	 * - apply-binding-sheets attribute is invalid
	 * 
	 */	
	
	public void testAssignElementWithInvalidApplyBindingSheets() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
		cont.setAttribute("apply-binding-sheets", "invalid");	
	
		assertTrue("Apply-binding-sheets: Wrong default value!", a.getApplyBindingSheets());
		cont.assignElement(a);
		assertTrue("Apply-binding-sheets: Wrong value (true)!", !a.getApplyBindingSheets());	
	}
	
	/**
	 * Tests ContentElementImpl.unassignElement and defaulting of apply-binding-sheets
	 */	
	
	public void testUnassignElementWithApplyBindingSheets() {
		DocumentImpl doc = new DocumentImpl();
		XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		ContentElementImpl cont = new ContentElementImpl(doc, "http://www.w3.org/ns/xbl","content");
	
		assertTrue("Apply-binding-sheets: Wrong default value!", a.getApplyBindingSheets());
		cont.assignElement(a);
		assertTrue("Apply-binding-sheets: Wrong value (true)!", !a.getApplyBindingSheets());
		cont.unassignElement(a, false);
		assertTrue("Apply-binding-sheets: Wrong default value!", a.getApplyBindingSheets());
	}	
		
}
