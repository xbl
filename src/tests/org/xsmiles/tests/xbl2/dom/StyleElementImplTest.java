package org.xsmiles.tests.xbl2.dom;

import java.awt.Container;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;

import org.apache.xerces.dom.TextImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.BrowserID;
import fi.hut.tml.xsmiles.BrowserLogic;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.DocumentHistory;
import fi.hut.tml.xsmiles.EventBroker;
import fi.hut.tml.xsmiles.GUIManager;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Resources;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.content.ContentLoaderListener;
import fi.hut.tml.xsmiles.content.ContentManager;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.messaging.Messaging;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCController;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ResourcesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.StyleElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.TemplateElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.XBLElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.ScriptHandler;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.xslt.XSLTEngine;
import junit.framework.TestCase;

public class StyleElementImplTest extends TestCase
{
    public static String fileurl;
    
    XSmilesDocumentImpl doc;
    String xblns = new String("http://www.w3.org/ns/xbl");
    String htmns = new String("http://www.w3.org/1999/xhtm");

    ScriptHandler scriptHandler;
    StyleElementImpl style_elem;
    BindingElementImpl binding;
    BindingHandler bh;
    XSmilesElementImpl bodynode;
    XSmilesElementImpl divnode;       
    TestMLFC hostMLFC;
    
    public StyleElementImplTest(String name)
    {
        super(name);
    }

    protected void setUp() throws Exception
    {
        Log.debug("SetUp new test case");
        doc = new XSmilesDocumentImpl();
        hostMLFC = new TestMLFC();
        hostMLFC.setXMLDocument(new TestXMLDocument(null, doc));
        doc.setHostMLFC(hostMLFC);
        XSmilesElementImpl htmlnode = new XSmilesElementImpl(doc, htmns, "html");
         XBLElementImpl xblnode = new XBLElementImpl(doc, xblns, "xbl");
          binding = new BindingElementImpl(doc, xblns, "binding");
           ResourcesElementImpl resources = new ResourcesElementImpl(doc, xblns, "resources");
            style_elem = new StyleElementImpl(doc, xblns, "style");
         bodynode = new XSmilesElementImpl(doc, htmns, "body");
          divnode = new XSmilesElementImpl(doc, htmns, "div");
        
        doc.appendChild(htmlnode);
         htmlnode.appendChild(xblnode);
          xblnode.appendChild(binding);
           binding.appendChild(resources);
            resources.appendChild(style_elem);
         htmlnode.appendChild(bodynode);
          bodynode.appendChild(divnode);                          
          
        binding.setAttribute("element", "#bbb");                                        
        bodynode.setAttribute("id", "bbb");                
                
        bh = new BindingHandler(doc);
        super.setUp();
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    public void testStyleElement()
    {                          
        String style_text = "body, #a { background-color: red; border: 1px; } \n#a { border: 2px } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);        
        style_elem.appendChild(style_text_node);
        
        doc.init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", bodynode.getStyle().getPropertyValue("border"));
    }
    
    public void testStyleBubbling()
    {                    
        divnode.setAttribute("id", "a");
        
        String style_text = "body, #a { background-color: red; border: 1px; }";
        TextImpl style_text_node = new TextImpl(doc, style_text);        
        style_elem.appendChild(style_text_node);
        
        doc.init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", bodynode.getStyle().getPropertyValue("border"));
        assertEquals("1px", divnode.getStyle().getPropertyValue("border"));
        assertEquals("red", divnode.getStyle().getPropertyValue("background-color"));        
    }
    
    public void testDifferentStyles()
    {                    
        divnode.setAttribute("id", "a");
        
        String style_text = "body, #a { background-color: red; border: 1px; } \n#a { border: 2px } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);        
        style_elem.appendChild(style_text_node);
        
        doc.init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", bodynode.getStyle().getPropertyValue("border"));
        assertEquals("2px", divnode.getStyle().getPropertyValue("border"));
        assertEquals("red", divnode.getStyle().getPropertyValue("background-color"));        
    }
    
    public void testStyleAffectingTemplate()
    {
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl tdivnode = new XSmilesElementImpl(doc, xblns, "div");
        binding.appendChild(templatenode);
        templatenode.appendChild(tdivnode);
        
        divnode.setAttribute("id", "a");
        tdivnode.setAttribute("id", "b");
        
        String style_text = "body, #b { background-color: red; border: 1px; } \n#a { border: 2px } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);        
        style_elem.appendChild(style_text_node);
        
        style_elem.init();
        
        doc.init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", bodynode.getStyle().getPropertyValue("border"));
        assertEquals("2px", divnode.getStyle().getPropertyValue("border"));        
        assertEquals("1px", tdivnode.getStyle().getPropertyValue("border"));
        assertEquals("red", tdivnode.getStyle().getPropertyValue("background-color"));        
    }
    
    // TODO: Needs to start some Browser or something to get the XMLUrl
    public void testSrcAttribute()
    {
        String style_text = "body, #a { background-color: red; border: 1px; } \n#a { border: 2px } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);
        FileWriter style_file = null;
        try
        {
            style_file = new FileWriter("style.xml");
            BufferedWriter bw = new BufferedWriter(style_file);
            bw.write(style_text);
            bw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
            fail();
        }        
        
        //style_elem.appendChild(style_text_node);
        style_elem.setAttribute("src", "style.xml");
        File file = new File("style.xml");
        StyleElementImplTest.fileurl = file.getAbsolutePath();
        
        doc.init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", bodynode.getStyle().getPropertyValue("border"));
        
        
        file.delete();
    }
    
    public void testStyleAffectingOtherElements()
    {        
        XSmilesElementImpl tdivnode = new XSmilesElementImpl(doc, xblns, "div");
        bodynode.appendChild(tdivnode);

        divnode.setAttribute("id", "a");
        binding.setAttribute("element", "#a");
        
        String style_text = "div { background-color: red; border: 1px; } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);        
        style_elem.appendChild(style_text_node);        
        style_elem.init();
               
        doc.init();

        assertEquals("red", divnode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", divnode.getStyle().getPropertyValue("border"));
        assertEquals("", tdivnode.getStyle().getPropertyValue("background-color"));
    }

    public void testStyleAffectingOtherDifferentElements()
    {        
        XSmilesElementImpl spannode = new XSmilesElementImpl(doc, xblns, "span");
        bodynode.appendChild(spannode);

        divnode.setAttribute("id", "a");
        binding.setAttribute("element", "#a");
        
        String style_text = "div, span { background-color: red; border: 1px; } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);        
        style_elem.appendChild(style_text_node);        
        style_elem.init();
               
        doc.init();

        assertEquals("red", divnode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", divnode.getStyle().getPropertyValue("border"));
        assertEquals("", spannode.getStyle().getPropertyValue("background-color"));
    }

    public void testMultipleStyleElements()
    {                          
        String style_text = "body, #a { background-color: red; border: 1px; } \n#a { border: 2px } ";
        String style_text2 = "body { border: 2px } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);     
        TextImpl style_text_node2 = new TextImpl(doc, style_text2);
        style_elem.appendChild(style_text_node);
        
        StyleElementImpl style_elem2 = new StyleElementImpl(doc, xblns, "style");
        style_elem2.appendChild(style_text_node2);
        
        binding.getFirstChild().appendChild(style_elem2);
        
        doc.init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("2px", bodynode.getStyle().getPropertyValue("border"));
    }

    public void testMediaAttribute()
    {                          
       String style_text = "body{ background-color: red; border: 1px; } ";
        String style_text2 = "body { border: 2px } ";
        TextImpl style_text_node = new TextImpl(doc, style_text);     
        TextImpl style_text_node2 = new TextImpl(doc, style_text2);
        style_elem.appendChild(style_text_node);
        style_elem.setAttribute("media", "desktop");
        
        StyleElementImpl style_elem2 = new StyleElementImpl(doc, xblns, "style");
        style_elem2.appendChild(style_text_node2);
        style_elem2.setAttribute("media", "print");
        
        binding.getFirstChild().appendChild(style_elem2);
        
        ((XSmilesDocumentImpl)doc).init();
        
        assertEquals("red", bodynode.getStyle().getPropertyValue("background-color"));
        assertEquals("1px", bodynode.getStyle().getPropertyValue("border"));  
    }
    
    public class TestMLFC extends MLFC
    {
        
        public int started = 0;
        public int stopped = 0;
        
        public TestMLFC()
        {
            //super();
        }
        
        public final String getVersion()
        {
            return "0.1";
        }
        
        /**
         * Create a DOM element.
         */
        public Element createElementNS(DocumentImpl doc, String ns, String tag)
        {
            return null;
        }
        public void start()
        {
            started++;
        }
        
        public void stop()
        {
            stopped++;
        }
        
    }
    
    public class TestXMLDocument extends XSmilesXMLDocument
    {
        public ExtendedDocument edoc;
        public TestXMLDocument(BrowserWindow browser,ExtendedDocument extdoc)
        {
            super(browser,extdoc);
            edoc=extdoc;
        }
        
        public Document getDocument()
        {
            return edoc;
        }
        
        public URL getXMLURL()
        {
            try
            {
                return new URL("file://"+StyleElementImplTest.fileurl);
            } catch (MalformedURLException e)
            {
                Log.debug(e.getLocalizedMessage());
            }
            return null;
        }
        
        public boolean evalMediaQuery(String s)
        {
            Log.debug("warning: evalMediaQuery() called in TestXMLDocument. returns "+s+" == desktop");
            return s == "desktop";
            
        }
    }
}
