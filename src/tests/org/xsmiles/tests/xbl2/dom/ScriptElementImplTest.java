package org.xsmiles.tests.xbl2.dom;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.ecma.ECMAScripterFactory;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ScriptElementImpl;
import junit.framework.TestCase;

public class ScriptElementImplTest extends TestCase
{
	XSmilesDocumentImpl doc;
	String xblns = new String("http://www.w3.org/ns/xbl");
	String htmns = new String("http://www.w3.org/1999/xhtm");
	TestMLFC hostMLFC;
	ScriptElementImpl scriptElem;
    XSmilesElementImpl bodynode;
	
    public ScriptElementImplTest (String name) {
        super(name);
    }
    
	protected void setUp() throws Exception
    {
        doc = new XSmilesDocumentImpl();
        hostMLFC = new TestMLFC();
        hostMLFC.setXMLDocument(new TestXMLDocument(null, doc));
        doc.setHostMLFC(hostMLFC);
        scriptElem = new ScriptElementImpl(doc, xblns, "script");
        bodynode = new XSmilesElementImpl(doc, htmns, "body");
        bodynode.setAttribute("id", "oldID");
        XSmilesElementImpl htmlnode = new XSmilesElementImpl(doc, htmns, "html");
        XSmilesElementImpl xblnode = new XSmilesElementImpl(doc, xblns, "xbl");
        xblnode.appendChild(scriptElem);
        doc.appendChild(htmlnode);
        htmlnode.appendChild(xblnode);
        htmlnode.appendChild(bodynode);
        
        super.setUp();
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

	public void testLoadScript() {
		String script =
			"document.getElementById(\"oldID\").setAttribute(\"id\", \"newID\")";
        scriptElem.setTextContent(script);
        scriptElem.loadScript();
        assertEquals("newID", bodynode.getAttribute("id"));
	}
	
	public class TestMLFC extends MLFC
    {
        
        public int started = 0;
        public int stopped = 0;
        
        public TestMLFC()
        {
            //super();
        }
        
        public final String getVersion()
        {
            return "0.1";
        }
        
        /**
         * Create a DOM element.
         */
        public Element createElementNS(DocumentImpl doc, String ns, String tag)
        {
            return null;
        }
        public void start()
        {
            started++;
        }
        
        public void stop()
        {
            stopped++;
        }
        
    }
    
    public class TestXMLDocument extends XSmilesXMLDocument
    {
        public ExtendedDocument edoc;
        public TestXMLDocument(BrowserWindow browser,ExtendedDocument extdoc)
        {
            super(browser,extdoc);
            edoc=extdoc;
        }
        
        public Document getDocument()
        {
            return edoc;
        }
        
        public ECMAScripter getECMAScripter() {
        	return ECMAScripterFactory.getScripter("rhino",this.m_browser);
        }
    }
}
