package org.xsmiles.tests;

import java.util.*;

import javax.swing.*;

import junit.framework.*;
import junit.extensions.*;



import org.apache.xerces.dom.*;
import org.w3c.dom.*;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XMLBroker;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * This test MLFC is used by XMLBrokerTest
 * @author Mikko Honkala
 */
	public class TestMLFCA extends MLFC
	{
		public static int classcount = 0;
		public int classno;
		public int elementCount =0;
		public static String latestElement;
		public static String latestNS;

		public TestMLFCA()
		{
		
			if (!(this instanceof TestMLFCB)) classno = classcount++;
		}

		public String getVersion()
		{
			return "0.1"+classno;
		}


		/**
		 * Create a DOM element.
		 */
		public Element createElementNS(DocumentImpl doc, String ns, String tag)
		{
			latestElement = tag;
			latestNS=ns;
			elementCount++;
			return null;
		}
		
		public void start()
		{
		}
		public void stop()
		{
		}
	}
	
