package org.xsmiles.tests.dom;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.tritonus.share.TDebug.AssertException;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.css.CSSStyleSheet;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.InitializableElement;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectors;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSOMParser;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleSheetImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.InheritedElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ResourcesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.StyleElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.TemplateElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.XBLElementImpl;

import junit.framework.TestCase;

import java.io.IOException;
import java.io.StringReader;
import java.util.Vector;

public class XSmilesElementTest extends TestCase {
    String xblns = new String("http://www.w3.org/ns/xbl");
    String htmns = new String("http://www.w3.org/1999/xhtm");

	XSmilesDocumentImpl doc;
    BindingHandler bh;
	public XSmilesElementImpl htmlnode;
	public XBLElementImpl xblnode;
	public XSmilesElementImpl divnode;
	
	public XSmilesElementTest(String name) {
		super(name);
		
	}

	protected void setUp() throws Exception {
		super.setUp();
		
		doc = new XSmilesDocumentImpl();
        doc.setDocumentURI("http://document.uri/");
        bh = new BindingHandler(doc);
		htmlnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "html");
		xblnode = new XBLElementImpl(doc, "http://www.w3.org/ns/xbl", "xbl");
		divnode = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "div");
	
		divnode.setAttribute("id", "a");
		
		doc.appendChild(htmlnode);
		htmlnode.appendChild(xblnode);
		htmlnode.appendChild(divnode);
		
		htmlnode.initFinalFlattenedTree();
		xblnode.initFinalFlattenedTree();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testXSmilesElementImplDocumentImplString() {
		XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
		XSmilesElementImpl xbl = (XSmilesElementImpl) html.getChildNodes().item(0);
		//assertEquals(1, node.getLength());
		assertEquals("html", html.getNodeName());
		assertEquals("xbl", xbl.getNodeName());
	}
	
	public void testReplaceNode() {
		XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
		XSmilesElementImpl div = (XSmilesElementImpl) html.getChildNodes().item(1);
		XSmilesElementImpl span = new XSmilesElementImpl(doc, "span");
		
		html.replaceNode(div, span);
		
		assertEquals("span", html.getChildNodes().item(1).getNodeName());
		try {
		String divstr = html.getChildNodes().item(2).getNodeName();
		if (divstr == div.getNodeName())
			fail("There should be no div-element any more");
		else
			fail("There is element named "+divstr+" that isn't same as the old");
		} catch (NullPointerException e) { 
			; // Expected
		}
	}
	
	public void testGetOriginalClone() {
		XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
		XSmilesElementImpl div = (XSmilesElementImpl) html.getChildNodes().item(1);
		XSmilesElementImpl span = new XSmilesElementImpl(doc, "span");
		
		assertEquals("div", div.getNodeName());
		
		html.replaceNode(div, span);
		
		XSmilesElementImpl olddiv = null;
				
		olddiv = ((XSmilesElementImpl) html.getOriginalClone().getChildNodes().item(1));
		div = (XSmilesElementImpl) html.getChildNodes().item(1);
				
		assertEquals("div", olddiv.getNodeName());
		assertEquals("span", div.getNodeName());
	}
	
	public void testResetFinalFlattenedTree()
	{
		XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
		XSmilesElementImpl div = (XSmilesElementImpl) html.getChildNodes().item(1);
		XSmilesElementImpl span = new XSmilesElementImpl(doc, "span");
		
		assertEquals("div", div.getNodeName());
		
		html.replaceNode(div, span);
		div = (XSmilesElementImpl) html.getChildNodes().item(1);
		assertEquals("span", div.getNodeName());
		
		html.resetFinalFlattenedTree();
		
		div = (XSmilesElementImpl) html.getChildNodes().item(1);
		assertEquals("div", div.getNodeName());
	}
	
	public void testResetFinalFlattenedTreeWithAttribute()
	{
		XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
		XSmilesElementImpl div = (XSmilesElementImpl) html.getChildNodes().item(1);
		XSmilesElementImpl span = new XSmilesElementImpl(doc, "span");
		
		assertEquals("a", div.getAttribute("id"));
		div.setAttribute("id", "b");
		assertEquals("b", div.getAttribute("id"));
		//div.resetFinalFlattenedTree();
		//assertEquals("a", div.getAttribute("id"));
	}
    
    public void testGetUndistributedChildNodes()
    {
        XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
        Vector nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size());        
    }
    
    public void testRemoveUndistributedChild()
    {
        XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
        Vector nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size());        
        Node n = html.removeUndistributedChild((Node)nl.elementAt(1));
        
        assertEquals("div", n.getNodeName());
        nl = html.getUndistributedChildNodes();
        assertEquals(1, nl.size());
        assertEquals("xbl", ((Node)nl.elementAt(0)).getNodeName());
        /*try {
            String a = ((Node)nl.elementAt(1)).getNodeName();
            fail("Should throw NullPointerException");
        } catch (NullPointerException e) {
            ; // Expected
        }*/
    }
    
    public void testAddUndistributedChild()
    {
        XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
        Vector nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size());        
        Node n = html.removeUndistributedChild((Node)nl.elementAt(1));
        
        assertEquals("div", n.getNodeName());
        nl = html.getUndistributedChildNodes();
        assertEquals(1, nl.size());
        assertEquals("xbl", ((Node)nl.elementAt(0)).getNodeName());
        
        html.addUndistributedChild(n);
        
        nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size());
        assertEquals("xbl", ((Node)nl.elementAt(0)).getNodeName());
        assertEquals("div", ((Node)nl.elementAt(1)).getNodeName());
    }
    
    public void testResetFinalFlattenedTreeWithUndistributedChildren()
    {
        XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
        Vector nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size()); 
        Node n = html.removeUndistributedChild((Node)nl.elementAt(1));        
        assertEquals("div", n.getNodeName());
        nl = html.getUndistributedChildNodes();
        assertEquals(1, nl.size());
        
        html.resetFinalFlattenedTree();
        
        nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size());
        assertEquals("xbl", ((Node)nl.elementAt(0)).getNodeName());
        assertEquals("div", ((Node)nl.elementAt(1)).getNodeName());
    }
    
    public void testInitFinalFlattenedTree()
    {
        XSmilesElementImpl html = (XSmilesElementImpl) doc.getChildNodes().item(0);
        Vector nl = html.getUndistributedChildNodes();
        assertEquals(2, nl.size()); 
        Node divnode = html.removeUndistributedChild((Node)nl.elementAt(1));
        
        assertEquals(false, html.initFinalFlattenedTree());
        
        nl = html.getUndistributedChildNodes();
        assertEquals("xbl", ((Node)nl.elementAt(0)).getNodeName());
        assertEquals(1, nl.size());
    }
    
    public void testAddBinding()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        bindnode.setAttribute("id", "bind");
        
        xblnode.appendChild(bindnode);
        bindnode.appendChild(templatenode);
        templatenode.appendChild(newdivnode);               
        
        ((XSmilesDocumentImpl) doc).init();
        divnode.addBinding("#bind");

        System.out.println(htmlnode.getFirstChild().getNextSibling().getFirstChild().getNodeName());
        assertEquals(htmlnode.getFirstChild().getNextSibling().getFirstChild().getNodeName(), "div");
    }

    public void testAddBindingWithImplicitInheritance()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        BindingElementImpl bindnode2 = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        TemplateElementImpl templatenode2 = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl newdivnode2 = new XSmilesElementImpl(doc, xblns, "div");
        InheritedElementImpl inheritednode = new InheritedElementImpl(doc, xblns, "inherited");
        
        bindnode.setAttribute("id", "bind");
        bindnode.setAttribute("element", "#a");
        bindnode2.setAttribute("id", "bind2");

        
        xblnode.appendChild(bindnode);
        xblnode.appendChild(bindnode2);
        bindnode.appendChild(templatenode);
        bindnode2.appendChild(templatenode2);
        templatenode.appendChild(newdivnode);
        templatenode2.appendChild(newdivnode2);
        newdivnode2.appendChild(inheritednode);
        
        newdivnode.appendChild(new TextImpl(doc, "OK"));
        
        ((XSmilesDocumentImpl) doc).init();
        divnode.addBinding("#bind2");

        assertEquals("OK", divnode.getFirstChild().getFirstChild().getFirstChild().getNodeValue());
    }
    
    public void testAddBindingInheritedFrom()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        BindingElementImpl bindnode2 = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        TemplateElementImpl templatenode2 = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl newdivnode2 = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl italicsnode = new XSmilesElementImpl(doc, xblns, "i");
        InheritedElementImpl inheritednode = new InheritedElementImpl(doc, xblns, "inherited");
        
        bindnode.setAttribute("id", "bind");
        
        bindnode2.setAttribute("id", "bind2");
        bindnode2.setAttribute("element", "#a");
        
        xblnode.appendChild(bindnode);
        xblnode.appendChild(bindnode2);
        bindnode.appendChild(templatenode);
        bindnode2.appendChild(templatenode2);
        templatenode.appendChild(newdivnode);
        templatenode2.appendChild(newdivnode2);
        newdivnode2.appendChild(italicsnode);
        italicsnode.appendChild(inheritednode);
        inheritednode.appendChild(new TextImpl(doc, "ERROR"));
        
        newdivnode.appendChild(new TextImpl(doc, "OK"));       
        
        ((XSmilesDocumentImpl) doc).init();
        assertEquals("ERROR", divnode.getFirstChild().getFirstChild().getFirstChild().getNodeValue());
        
        divnode.addBinding("#bind");

        assertEquals("OK", divnode.getFirstChild().getFirstChild().getNodeValue());
    }
    
    public void testAddBindingWithExplicitInheritance()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        BindingElementImpl bindnode2 = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        TemplateElementImpl templatenode2 = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl newdivnode2 = new XSmilesElementImpl(doc, xblns, "div");
        InheritedElementImpl inheritednode = new InheritedElementImpl(doc, xblns, "inherited");
        
        bindnode.setAttribute("id", "bind");        
        bindnode2.setAttribute("id", "bind2");
        bindnode2.setAttribute("extends", "#bind");
        bindnode2.setAttribute("element", "#a");
        
        xblnode.appendChild(bindnode);
        xblnode.appendChild(bindnode2);
        bindnode.appendChild(templatenode);
        bindnode2.appendChild(templatenode2);
        templatenode.appendChild(newdivnode);
        templatenode2.appendChild(newdivnode2);
        newdivnode2.appendChild(inheritednode);
        
        inheritednode.appendChild(new TextImpl(doc, "ERROR"));
        newdivnode.appendChild(new TextImpl(doc, "OK"));
        
        ((XSmilesDocumentImpl) doc).init();
        
        assertEquals("OK", divnode.getFirstChild().getFirstChild().getFirstChild().getNodeValue());
    }
    
    public void testRemoveBinding()
    {        
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        ResourcesElementImpl resourcesnode = new ResourcesElementImpl(doc, xblns, "resources");
        StyleElementImpl stylenode = new StyleElementImpl(doc, xblns, "style");
        String styletext = "#a { background-color: red; border: 1px; }";
        stylenode.appendChild(new TextImpl(doc, styletext));
        
        bindnode.setAttribute("id", "bind");
        bindnode.setAttribute("element", "#a");
        
        xblnode.appendChild(bindnode);
        bindnode.appendChild(resourcesnode);
        resourcesnode.appendChild(stylenode);               
        
        ((XSmilesDocumentImpl) doc).init();
        // remove binding
        divnode.removeBinding("#bind");
        // remove binding
        assertNull(divnode.getStyle());
    }
    
    public void testIsInitedForReset()
    {
        XSmilesElementImpl el = new XSmilesElementImpl(doc, htmns, "div");
        assertTrue(!el.isInitedForReset());
        el.initFinalFlattenedTree();
        assertTrue(el.isInitedForReset());
    }
    
    public void testRemoveTemplateBinding()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        bindnode.setAttribute("id", "bind");
        bindnode.setAttribute("element", "#a");
        
        xblnode.appendChild(bindnode);
        bindnode.appendChild(templatenode);
        templatenode.appendChild(newdivnode);
        divnode.appendChild(new TextImpl(doc, "DIVOK"));
        
        ((XSmilesDocumentImpl) doc).init();
        
        // remove binding
        divnode.removeBinding("#bind");

        assertEquals("DIVOK", divnode.getFirstChild().getNodeValue());
    }
    
    public void testRemoveDoubleTemplateBinding()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        BindingElementImpl bindnode2 = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl newdivnode2 = new XSmilesElementImpl(doc, xblns, "div");
        TemplateElementImpl templatenode2 = new TemplateElementImpl(doc, xblns, "template");
        InheritedElementImpl inheritednode = new InheritedElementImpl(doc, xblns, "inherited");
        
        bindnode.setAttribute("id", "bind");
        bindnode.setAttribute("element", "#a");
        bindnode2.setAttribute("id", "bind2");
        bindnode2.setAttribute("element", "#a");
        
        xblnode.appendChild(bindnode);
        xblnode.appendChild(bindnode2);
        bindnode.appendChild(templatenode);
        bindnode2.appendChild(templatenode2);
        templatenode.appendChild(newdivnode);
        templatenode2.appendChild(newdivnode2);
        newdivnode2.appendChild(inheritednode);
        newdivnode.appendChild(new TextImpl(doc, "OK"));
        
        ((XSmilesDocumentImpl) doc).init();
        
        assertEquals("OK", divnode.getFirstChild().getFirstChild().getFirstChild().getNodeValue());
        
        // remove binding
        divnode.removeBinding("#bind");

        assertNull(divnode.getFirstChild().getFirstChild());
    }
    
    public void testRemoveBindingDoubleBounded()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl divnode2 = new XSmilesElementImpl(doc, xblns, "div");
        
        bindnode.setAttribute("id", "bind");
        bindnode.setAttribute("element", "#a, #b");
        divnode2.setAttribute("id", "b");
        
        htmlnode.appendChild(divnode2);
        xblnode.appendChild(bindnode);
        bindnode.appendChild(templatenode);
        templatenode.appendChild(newdivnode);
        newdivnode.appendChild(new TextImpl(doc, "OK"));
        
        ((XSmilesDocumentImpl) doc).init();
        
        assertEquals("OK", divnode.getFirstChild().getFirstChild().getNodeValue());
        assertEquals("OK", divnode2.getFirstChild().getFirstChild().getNodeValue());
        
        // remove binding
        divnode.removeBinding("#bind");

        assertNull(divnode.getFirstChild());
        assertEquals("OK", divnode2.getFirstChild().getFirstChild().getNodeValue());
    }
    
    public void testRemoveBindingWithExplicitInheritance()
    {
        BindingElementImpl bindnode = new BindingElementImpl(doc, xblns, "binding");
        BindingElementImpl bindnode2 = new BindingElementImpl(doc, xblns, "binding");
        TemplateElementImpl templatenode = new TemplateElementImpl(doc, xblns, "template");
        TemplateElementImpl templatenode2 = new TemplateElementImpl(doc, xblns, "template");
        XSmilesElementImpl newdivnode = new XSmilesElementImpl(doc, xblns, "div");
        XSmilesElementImpl newdivnode2 = new XSmilesElementImpl(doc, xblns, "div");
        InheritedElementImpl inheritednode = new InheritedElementImpl(doc, xblns, "inherited");
        
        bindnode.setAttribute("id", "bind");        
        bindnode2.setAttribute("id", "bind2");
        bindnode2.setAttribute("extends", "#bind");
        //bindnode2.setAttribute("element", "#a");
        
        xblnode.appendChild(bindnode);
        xblnode.appendChild(bindnode2);
        bindnode.appendChild(templatenode);
        bindnode2.appendChild(templatenode2);
        templatenode.appendChild(newdivnode);
        templatenode2.appendChild(newdivnode2);
        newdivnode2.appendChild(inheritednode);
        
        divnode.appendChild(new TextImpl(doc, "DIVOK"));
        inheritednode.appendChild(new TextImpl(doc, "ERROR"));
        newdivnode.appendChild(new TextImpl(doc, "NEWOK"));
        
        ((XSmilesDocumentImpl) doc).init();
        
        assertEquals("DIVOK", divnode.getFirstChild().getNodeValue());
        divnode.addBinding("#bind2");
        assertEquals("NEWOK", divnode.getFirstChild().getFirstChild().getFirstChild().getNodeValue());
        divnode.removeBinding("#bind2");
        assertEquals("DIVOK", divnode.getFirstChild().getNodeValue());
    }
    
    /**
     * Tests XSmilesElementImpl.setApplyBindingSheet() in case where:
	 * - the value is set to true
     *
     */
    
    public void testChangeApplyBindingSheetsTrue()
    {
    	DocumentImpl doc = new DocumentImpl();
    	XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		
    	assertTrue("Wrong default value with apply-binding-sheets!", a.getApplyBindingSheets());
    	a.setApplyBindingSheets(true);
    	assertTrue("Apply-binding-sheets is false, should be true", a.getApplyBindingSheets());
    }
    
    /**
     * Tests XSmilesElementImpl.setApplyBindingSheet() in case where:
	 * - the value is set to false
     *
     */
    
    public void testChangeApplyBindingSheetsFalse()
    {
    	DocumentImpl doc = new DocumentImpl();
    	XSmilesElementImpl a = new XSmilesElementImpl(doc, "http://www.w3.org/1999/xhtm", "a");
		
    	assertTrue("Wrong default value with apply-binding-sheets!", a.getApplyBindingSheets());
    	a.setApplyBindingSheets(false);
    	assertTrue("Apply-binding-sheets is true, should be false", !a.getApplyBindingSheets());
    }
    
}
