package org.xsmiles.tests.xforms;

import junit.framework.*;

import java.util.Enumeration;
import java.util.Vector;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
//import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathLookup;

/**
 * Tests the XPathLookup, which is used by the XFOrms constraints engine,
 * to find out which nodes a single XPath references.
 * It runs different XPaths, and analyzes the result.
 * @author Mikko Honkala
 */
public class XPathLookupTest extends XPathBase
{
    private Document doc;
    private Document docNS;
    
    
    public XPathLookupTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
        try
        {
            String docStr =
            "<?xml version=\"1.0\" standalone=\"no\"?><root><child>10</child><child>20<extra/></child><child>30</child><info><tax>0.22</tax><currency rate=\"0.5\">0</currency></info></root>";
            String docStr2 =
            "<?xml version=\"1.0\" standalone=\"no\"?><my:root xmlns:my=\"myns\"><my:child>10</my:child><my:child>20</my:child><my:child>30</my:child><my:tax>0.22</my:tax><my:currency rate=\"0.5\">0</my:currency></my:root>";
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            dfactory.setNamespaceAware( true );
            DocumentBuilder parser=dfactory.newDocumentBuilder();
            doc=parser.parse(new InputSource(new java.io.StringReader(docStr)));
            docNS=parser.parse(new InputSource(new java.io.StringReader(docStr2)));
            super.setUp();
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
    public static Test suite()
    {
        return new TestSuite(XPathLookupTest.class);
    }
    
    
    public void testBasicXPath() throws Exception
    {
        final String xpath1 = "/root/child[2] * /root/child[3]";
        assertNotNull("lookup class",this.xpathAPI);
        Element root = doc.getDocumentElement();
        assertNotNull("doc elem",root);
        Vector l = evalWithTrace(root,xpath1,root);
        Vector expected = new Vector(); // the expected result
        expected.addElement(root.getElementsByTagNameNS("","child").item(1));
        expected.addElement(root.getElementsByTagNameNS("","child").item(2));
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    public void testSum() throws Exception
    {
        final String xpath1 = "sum(/root/child)";
        Element root = doc.getDocumentElement();
        Vector l = evalWithTrace(root,xpath1,root);
        Vector expected = new Vector(); // the expected result
        expected.addElement(root.getElementsByTagNameNS("","child").item(0));
        expected.addElement(root.getElementsByTagNameNS("","child").item(1));
        expected.addElement(root.getElementsByTagNameNS("","child").item(2));
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    public void testContext() throws Exception
    {
        final String xpath1 = "tax";
        Element root = doc.getDocumentElement();
        Node ctx = root.getElementsByTagNameNS("","info").item(0);
        assertNotNull("context node null",ctx);
        Vector l = evalWithTrace(ctx,xpath1,ctx);
        Vector expected = new Vector(); // the expected result
        expected.addElement(root.getElementsByTagNameNS("","tax").item(0));
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    protected Vector evalWithTrace(Node ctx, String path, Node nsctx) throws Exception
    {
        LookupResult res = new LookupResult();
        this.xpathAPI.evalWithTrace(ctx,this.xpathAPI.createXPathExpression(path),nsctx,null,res);
        Vector l = res.referredNodes;
        return l;
    }
    public void testAttributes() throws Exception
    {
        final String xpath1 = "currency * currency/@rate";
        Element root = doc.getDocumentElement();
        Node ctx = root.getElementsByTagNameNS("","info").item(0);
        Vector l = evalWithTrace(ctx,xpath1,ctx);
        Vector expected = new Vector(); // the expected result
        Element currency = (Element)root.getElementsByTagNameNS("","currency").item(0);
        expected.addElement(currency);
        expected.addElement(currency.getAttributeNode("rate"));
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    public void testAncestor() throws Exception
    {
        final String xpath1 = "..";
        Element root = doc.getDocumentElement();
        Node ctx = root.getElementsByTagNameNS("","extra").item(0);
        assertNotNull("context node null",ctx);
        Vector l = evalWithTrace(ctx,xpath1,ctx);
        Vector expected = new Vector(); // the expected result
        expected.addElement(root.getElementsByTagNameNS("","child").item(1));
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    
    public void testNamespace() throws Exception
    {
        final String xpath1 = "/my:root/my:child[1] * /my:root/my:child[3]";
        Element root = docNS.getDocumentElement();
        Node ctx = root.getElementsByTagNameNS("myns","child").item(0);
        assertNotNull("context node null",ctx);
        Vector l = evalWithTrace(ctx,xpath1,ctx);
        Vector expected = new Vector(); // the expected result
        expected.addElement(root.getElementsByTagNameNS("myns","child").item(0));
        expected.addElement(root.getElementsByTagNameNS("myns","child").item(2));
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    public void testAsterix() throws Exception
    {
        final String xpath1 = "sum(//*)";
        Element root = docNS.getDocumentElement();
        Node ctx = root.getElementsByTagNameNS("myns","child").item(0);
        assertNotNull("context node null",ctx);
        Vector l = evalWithTrace(ctx,xpath1,ctx);
        Vector expected = new Vector(); // the expected result
        NodeList nl  = docNS.getElementsByTagNameNS("*","*");
        assertNotNull("nodelist null", nl);
        assertTrue("nodelist empty", nl.getLength()>0);
        for (int i=0;i<nl.getLength();i++)
        {
            expected.addElement(nl.item(i));
        }
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    /** and will not work if a special and operator is not installed that takes care of 
     * executing or analyzing both sides of the and operator... */
    public void testAnd() throws Exception
    {
        final String xpath1 = ".='15' and ..='40'";
        Element root = doc.getDocumentElement();
        Node ctx = root.getElementsByTagNameNS("","extra").item(0);
        assertNotNull("context node null",ctx);
        Vector l = evalWithTrace(ctx,xpath1,ctx);
        Vector expected = new Vector(); // the expected result
        expected.addElement(root.getElementsByTagNameNS("","child").item(1));
        expected.addElement(ctx);
        assertTrue("vectors not the same",compareVectors(l,expected));
    }
    
    protected void  dumpVector(Vector v)
    {
        Log.debug("Contents of a vector:"+v);
        if (v!=null)
        {
	        Enumeration e=v.elements();
	        while (e.hasMoreElements())
	        {
	            Node n = (Node)e.nextElement();
	            Log.debug(n+" : "+n.getNodeValue());
	        }
        }
    }
    /** this method compares non-ordered vectors */
    private boolean compareVectors(Vector v1, Vector v2)
    {
        dumpVector(v1);
        dumpVector(v2);
        if (v1==null||v2==null) return false;
        if (v1.size()!=v2.size()) return false;
        for (int i=0;i<v1.size();i++)
        {
            Object o = v1.elementAt(i);
            if (!v2.remove(o)) return false;
        }
        if (v2.size()!=0) return false;
        return true;
    }
    
    
}