package org.xsmiles.tests.xforms;

import junit.framework.*;

/**
 * TestSuite that runs all the sample tests
 *
 */
public class AllTests {

	public static void main (String[] args) {
		junit.textui.TestRunner.run (suite());
	}
	public static Test suite ( ) {
		TestSuite suite= new TestSuite("All XForms Tests");
		suite.addTest(XMLSerializerTest.suite());
		suite.addTest(DatatypeTest.suite());
        suite.addTest(XPathTest.suite());
		suite.addTest(XPathLookupTest.suite());
		suite.addTest(XPathExtFuncTest.suite());
		suite.addTest(XPathForNodeTest.suite());
		suite.addTest(DependencyGraphTest.suite());

        suite.addTest(JaxenXPathTest.suite());
		suite.addTest(JaxenXPathLookupTest.suite());
		suite.addTest(JaxenDependencyGraphTest.suite());
        suite.addTest(JaxenExtFuncTest.suite());

        return suite;
	}
}