package org.xsmiles.tests.xforms;

import junit.framework.*;

// Imported JAVA API for XML Parsing 1.0 classes

// Imported Serializer classes

// XPath

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
//import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathLookup;

/**
 * Tests the XPathLookup, which is used by the XFOrms constraints engine,
 * to find out which nodes a single XPath references.
 * It runs different XPaths, and analyzes the result.
 * @author Mikko Honkala
 */
public class JaxenExtFuncTest extends XPathExtFuncTest
{
    
    
    public JaxenExtFuncTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
            System.setProperty(XFormsConfiguration.PROPERTY_XPATH_ENGINE,XFormsConfiguration.XPATH_JAXEN_CLASS);
            super.setUp();
    }
    protected void tearDown() throws Exception
    {
            System.setProperty(XFormsConfiguration.PROPERTY_XPATH_ENGINE,XFormsConfiguration.XPATH_XALAN_CLASS);
            super.tearDown();
    }
    public static Test suite()
    {
        return new TestSuite(JaxenExtFuncTest.class);
    }
    
    public void testPosition() throws Exception
	{
        // position and size do not work in Jaxen
	}
        /** test functions that require the context nodeset to be set correctly, 
         * such as size() and position() */
        public void testSize() throws Exception
	{
            // position and size do not work in Jaxen
	}
    
    

}