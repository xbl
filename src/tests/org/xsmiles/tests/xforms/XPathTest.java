package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
//import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathLookup;

/**
 * Tests the XPathLookup, which is used by the XFOrms constraints engine,
 * to find out which nodes a single XPath references.
 * It runs different XPaths, and analyzes the result.
 * @author Mikko Honkala
 */
public class XPathTest extends XPathBase
{
    protected Document doc;
    protected Document docNS;
    
    
    public XPathTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
        try
        {
            String docStr =
            "<?xml version=\"1.0\" standalone=\"no\"?><root><child>10</child><child>20<extra/></child><child>30</child><info><tax>0.22</tax><currency rate=\"0.5\">0</currency></info></root>";
            String docStr2 =
            "<?xml version=\"1.0\" standalone=\"no\"?><my:root xmlns:my=\"myns\"><my:child>10</my:child><my:child>20</my:child><my:child>30</my:child><my:tax>0.22</my:tax><my:currency rate=\"0.5\">0</my:currency></my:root>";
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            dfactory.setNamespaceAware( true );
            DocumentBuilder parser=dfactory.newDocumentBuilder();
            doc=parser.parse(new InputSource(new java.io.StringReader(docStr)));
            docNS=parser.parse(new InputSource(new java.io.StringReader(docStr2)));
            super.setUp();
            assertNotNull("lookup class",this.xpathAPI);
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
    public static Test suite()
    {
        return new TestSuite(XPathTest.class);
    }
    
    
    public void testBasicXPath() throws Exception
    {
        final String xpath1 = "/root/child[2] * /root/child[3]";
        Element root = doc.getDocumentElement();
        assertNotNull("doc elem",root);
        XPathExpr expr = this.xpathAPI.createXPathExpression(xpath1);
        Object ret=this.xpathAPI.eval(root,expr,root,null);
        assertTrue("wrong type of return value",ret instanceof Number);
        assertEquals("wrong result",ret,new Double(600.0));
    }
    public void testSum() throws Exception
    {
        final String xpath1 = "sum(/root/child)";
        Element root = doc.getDocumentElement();
        assertNotNull("doc elem",root);
        XPathExpr expr = this.xpathAPI.createXPathExpression(xpath1);
        Object ret=this.xpathAPI.eval(root,expr,root,null);
        assertTrue("wrong type of return value",ret instanceof Number);
        assertEquals("wrong result",ret,new Double(60.0));
    }
    public void testNamespace() throws Exception
    {
        final String xpath1 = "/my:root/my:child[1] * /my:root/my:child[3]";
        Element root = docNS.getDocumentElement();
        assertNotNull("doc elem",root);
        XPathExpr expr = this.xpathAPI.createXPathExpression(xpath1);
        Object ret=this.xpathAPI.eval(root,expr,root,null);
        assertTrue("wrong type of return value",ret instanceof Number);
        assertEquals("wrong result",ret,new Double(300.0));
    }

}