package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import java.util.Hashtable;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException; 

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;
import org.apache.xpath.*;
import org.apache.xpath.objects.*;
import org.xml.sax.SAXException;    



import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
/*
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.CachedXPathAPIEx;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.ExtensionFunctions;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathLookup;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XalanXPathEngine;
*/
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatIndexHandler;


import fi.hut.tml.xsmiles.gui.components.ComponentFactory;


/**
 * Tests the XPathLookup, which is used by the XFOrms constraints engine, 
 * to find out which nodes a single XPath references.
 * It runs different XPaths, and analyzes the result.
 * @author Mikko Honkala
 */
public class XPathBase extends TestCase implements XFormsContext,ModelContext {
	protected XPathEngine xpathAPI;
    protected Hashtable cursors;	

	public XPathBase(String name) {
		super(name);
	}
	protected void setUp() {
		try
		{
			xpathAPI = XFormsConfiguration.getInstance().createXPathEngine(this,this);
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	

	protected Object evalXPathEx(Node contextNode,String path,Node nsNode)
	{
	    return this.evalXPath(contextNode,path,nsNode);
	}
	protected Object evalXPath(Node contextNode,String path,Node nsNode)
	{
		if (xpathAPI==null) xpathAPI=XFormsConfiguration.getInstance().createXPathEngine(this,this); 
		// NOTE: must check that node is the an ancestor of instance
		String xpath = path;
		Object x=null;
		try
		{
		    XPathExpr expr=xpathAPI.createXPathExpression(xpath);
			x = xpathAPI.eval(contextNode,expr,nsNode,null);
		} catch (Exception e)
		{
			System.out.println(e.toString());
		}
		return x;
	}
       
     protected Object evalXPath(Node contextNode,String path,Node nsNode, NodeList contextList)
	{
		//if (xpathAPI==null) xpathAPI=new CachedXPathAPIEx(this,this); 
		if (xpathAPI==null) xpathAPI=XFormsConfiguration.getInstance().createXPathEngine(this,this); 
		// NOTE: must check that node is the an ancestor of instance
		String xpath = path;
		Object x=null;
		try
		{
		    XPathExpr expr = xpathAPI.createXPathExpression(xpath);
			x = (Object)xpathAPI.eval(contextNode,expr,nsNode,contextList);
		} catch (Exception e)
		{
			System.out.println(e.toString());
		}
		return x;
	}
	public void setCursor(String id, int value) {
		cursors.put(id,new Integer(value));
	}	
	
	public int getCursor(String id) {
		Integer ret = (Integer)cursors.get(id);
		if (ret == null) return -1;
		return ret.intValue();
	}
    
    public ComponentFactory getComponentFactory() {
        return null;
    }
        public boolean getDebugEvents()
    {
        return true;
    }
	    public Hashtable getModels()
    {
        return null;
    }
    
    public Document getInstanceDocument(String id) {
        return null;
    }
        public RepeatIndexHandler getRepeatIndexHandler() {
        return null;
    }
     public XPathEngine getXPathEngine()
     {
         return XFormsConfiguration.getInstance().createXPathEngine(null,this);
        
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext#getNodeIndex(java.lang.String)
     */
    public Node getNodeIndex(String id)
    {
        // TODO Auto-generated method stub
        return null;
    }

}