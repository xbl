package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import java.util.Hashtable;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException; 

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;
import org.apache.xpath.*;
import org.apache.xpath.objects.*;
import org.xml.sax.SAXException;    



import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
/*
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.CachedXPathAPIEx;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.ExtensionFunctions;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathLookup;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XalanXPathEngine;
*/
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatIndexHandler;


import fi.hut.tml.xsmiles.gui.components.ComponentFactory;


/**
 * Tests the XPathLookup, which is used by the XFOrms constraints engine, 
 * to find out which nodes a single XPath references.
 * It runs different XPaths, and analyzes the result.
 * @author Mikko Honkala
 */
public class XPathExtFuncTest extends TestCase implements XFormsContext,ModelContext {
	private Document doc;
	protected XPathEngine xpathAPI;
	protected Hashtable cursors;
	
	public XPathExtFuncTest(String name) {
		super(name);
	}
	public static void main (String[] args) {
		junit.textui.TestRunner.run (suite());
	}
	protected void setUp() {
		try
		{
			final String docStr = 
			"<?xml version=\"1.0\" standalone=\"no\"?><root xmlns:xforms=\"http://www.w3.org/2002/xforms\"><child>10</child><child>20<extra/></child><child>30</child><info><tax>0.22</tax><child/><currency rate=\"0.5\">0</currency></info></root>";
		  	DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
		  	dfactory.setNamespaceAware( true );
		  	DocumentBuilder parser=dfactory.newDocumentBuilder();	  
			doc=parser.parse(new InputSource(new java.io.StringReader(docStr)));
			xpathAPI = XFormsConfiguration.getInstance().createXPathEngine(this,this);
			cursors = new Hashtable();
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	public static Test suite() {
		return new TestSuite(XPathExtFuncTest.class);
	}
	

	public void testBasicXPath() throws Exception
	{
		final String xpath1 = "2 * /root/child[1]";
		final int value = 20;
		assertNotNull("xpathAPI class",xpathAPI);
		Element root = doc.getDocumentElement();
		assertNotNull("doc elem",root);
		Object ret = this.evalXPath(root,xpath1,root);
		assertNotNull("Xpath return value = null",ret);
		this.assertTrue("Return type wrong",ret instanceof Number);
		this.assertEquals("Cursor returned wrong number",((Number)ret).intValue(),value);
	}
        /** test functions that require the context nodeset to be set correctly, 
         * such as size() and position() */
        public void testPosition() throws Exception
	{
		final String contextXPath = "//child";
		final int value = 20;
		assertNotNull("xpathAPI class",xpathAPI);
		Element root = doc.getDocumentElement();
		assertNotNull("doc elem",root);
		Object ret = this.evalXPath(root,contextXPath,root);
		assertNotNull("Xpath return value = null",ret);
                // run the second xpath for all nodes in the returned nodeset
                NodeList nl = (NodeList)ret;
                String positionXPath = "position()";

                for (int i = 0;i<nl.getLength();i++)
                {
                    Node n = nl.item(i);
                    ret = this.evalXPath(n, positionXPath, n,nl);
                    assertNotNull("Xpath return value = null",ret);
                    this.assertTrue("Return type wrong",ret instanceof Number);
                    int position = ((Number)ret).intValue();
                    //System.out.println("Position: "+position+ "i:"+i);
                    this.assertEquals("Position returned wrong number",position,i+1);
                }
	}
        /** test functions that require the context nodeset to be set correctly, 
         * such as size() and position() */
        public void testSize() throws Exception
	{
		final String contextXPath = "//child";
		final int value = 20;
		assertNotNull("xpathAPI class",xpathAPI);
		Element root = doc.getDocumentElement();
		assertNotNull("doc elem",root);
		Object ret = this.evalXPath(root,contextXPath,root);
		assertNotNull("Xpath return value = null",ret);
                // run the second xpath for all nodes in the returned nodeset
                NodeList nl = (NodeList)ret;
                String positionXPath = "position()";
                String sizeXPath = "last()";
                ret = this.evalXPath(nl.item(0), sizeXPath, nl.item(0),nl);
                assertNotNull("Xpath return value = null",ret);
                this.assertTrue("Return type wrong",ret instanceof Number);
                int size = ((Number)ret).intValue();
                System.out.println("size: "+size+ "nl:"+nl.getLength());
                this.assertEquals("Size returned wrong number",size,nl.getLength());
	}
	public void testKnownCursor() throws Exception
	{
		final String xpath1 = "index('xy')";
		final int value = 5;
		this.setCursor("xy",5);
		//final String xpath1 = "//*";
		assertNotNull("xpathAPI class",xpathAPI);
		Element root = doc.getDocumentElement();
		assertNotNull("doc elem",root);
		Object ret = this.evalXPath(root,xpath1,root);
		assertNotNull("Xpath return value = null",ret);
		this.isNumber(ret);
		this.assertEquals("Cursor returned wrong number",((Number)ret).intValue(),value);
	}
	
	public void isNumber(Object ret)
	{
	    this.assertTrue("Return type wrong",ret instanceof Number);
		   
	}
	
	public void testUnKnownCursor() throws Exception
	{
		final String xpath1 = "index('xx')";
		//final String xpath1 = "//*";
		assertNotNull("xpathAPI class",xpathAPI);
		Element root = doc.getDocumentElement();
		assertNotNull("doc elem",root);
		Object ret = this.evalXPath(root,xpath1,root);
		assertNotNull("Xpath return value = null",ret);
		this.isNumber(ret);
		this.assertEquals("Cursor returned wrong number",((Number)ret).intValue(),-1);
	}
	public void testCountNonEmpty() throws Exception
	{
		final String xpath1 = "count-non-empty(//child)";
		//final String xpath1 = "//*";
		final int expected = 2;
		assertNotNull("xpathAPI class",xpathAPI);
		Element root = doc.getDocumentElement();
		assertNotNull("doc elem",root);
		Object ret = this.evalXPath(root,xpath1,root);
		assertNotNull("Xpath return value = null",ret);
		this.isNumber(ret);
		this.assertEquals("Cursor returned wrong number",expected,((Number)ret).intValue());
	}

	
	protected Object evalXPath(Node contextNode,String path,Node nsNode)
	{
		if (xpathAPI==null) xpathAPI=XFormsConfiguration.getInstance().createXPathEngine(this,this); 
		// NOTE: must check that node is the an ancestor of instance
		String xpath = path;
		Object x=null;
		try
		{
		    XPathExpr expr=xpathAPI.createXPathExpression(xpath);
			x = xpathAPI.eval(contextNode,expr,nsNode,null);
		} catch (Exception e)
		{
			System.out.println(e.toString());
		}
		return x;
	}
        
        	protected Object evalXPath(Node contextNode,String path,Node nsNode, NodeList contextList)
	{
		//if (xpathAPI==null) xpathAPI=new CachedXPathAPIEx(this,this); 
		if (xpathAPI==null) xpathAPI=XFormsConfiguration.getInstance().createXPathEngine(this,this); 
		// NOTE: must check that node is the an ancestor of instance
		String xpath = path;
		Object x=null;
		try
		{
		    XPathExpr expr = xpathAPI.createXPathExpression(xpath);
			x = (Object)xpathAPI.eval(contextNode,expr,nsNode,contextList);
		} catch (Exception e)
		{
			System.out.println(e.toString());
		}
		return x;
	}
	public void setCursor(String id, int value) {
		cursors.put(id,new Integer(value));
	}	
	
	public int getCursor(String id) {
		Integer ret = (Integer)cursors.get(id);
		if (ret == null) return -1;
		return ret.intValue();
	}
    
    public ComponentFactory getComponentFactory() {
        return null;
    }
        public boolean getDebugEvents()
    {
        return true;
    }
	    public Hashtable getModels()
    {
        return null;
    }
    
    public Document getInstanceDocument(String id) {
        return null;
    }
        public RepeatIndexHandler getRepeatIndexHandler() {
        return null;
    }
     public XPathEngine getXPathEngine()
     {
         return XFormsConfiguration.getInstance().createXPathEngine(null,this);
        
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext#getNodeIndex(java.lang.String)
     */
    public Node getNodeIndex(String id)
    {
        // TODO Auto-generated method stub
        return null;
    }

}