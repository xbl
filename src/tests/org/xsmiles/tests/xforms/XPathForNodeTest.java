package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException; 

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;


/**
 * This unit test class tests the XPath string generator class XPathForNode. 
 * XPathForNode should generate an unique XPath string for every node in an XML DOM.
 * This class contains multiple tests for namespaced and non-namespaced elements and attributes
 *
 * @author Mikko Honkala
 */
public class XPathForNodeTest extends TestCase {
	private Document doc;
	private Document docNS;
	public XPathForNodeTest(String name) {
		super(name);
	}
	public static void main (String[] args) {
		junit.textui.TestRunner.run (suite());
	}
	protected void setUp() {
		try
		{
			String docStr = 
			"<?xml version=\"1.0\" standalone=\"no\"?><root><child>10</child><child>20</child><child>30</child><tax>0.22</tax><currency rate=\"0.5\">0</currency></root>";
		  	String docStr2 = 
		  	"<?xml version=\"1.0\" standalone=\"no\"?><root xmlns=\"my\"><child>10</child><child>20</child><child>30</child><tax>0.22</tax><currency rate=\"0.5\">0</currency></root>";
		  	DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
		  	dfactory.setNamespaceAware( true );
		  	DocumentBuilder parser=dfactory.newDocumentBuilder();	  
			doc=parser.parse(new InputSource(new java.io.StringReader(docStr)));
			docNS=parser.parse(new InputSource(new java.io.StringReader(docStr2)));
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	public static Test suite() {
		return new TestSuite(XPathForNodeTest.class);
	}
	
	public void testDocument()
	{
		assertNotNull("parsed doc should not be null",doc);
		assertNotNull("parsed doc2 should not be null",docNS);
	}

	public void testXPathForNodeSimple()
	{
		Element root = doc.getDocumentElement();
		assertNotNull("root element null",root);
		assertEquals("root element's name",root.getNodeName(),"root");
		Node secondChild = root.getElementsByTagNameNS("","child").item(1);
		assertNotNull("second child null",secondChild);
		String xpath = XPathForNode.getXPath(secondChild,null);
		assertEquals("xpath wrong",xpath,"/root[1]/child[2]");
	}
	
	public void testXPathForNodeSimpleNS()
	{
		Element root = docNS.getDocumentElement();
		assertNotNull("root element null",root);
		assertEquals("root element's name",root.getNodeName(),"root");
		Node secondChild = root.getElementsByTagNameNS("my","child").item(1);
		assertNotNull("second child null",secondChild);
		String xpath = XPathForNode.getXPath(secondChild,null);
		assertEquals("xpath wrong",xpath,"/*[name()='root'][1]/*[name()='child'][2]");
	}
	
	/** 
	 * this test generates an XPath string for all the nodes in the document, and then runs
	 * the xpath and compares the result and the original.
	 */
	public void testXPathForNodeFullNS()
	{
		XPathDocumentTest(doc);
		XPathDocumentTest(docNS);
	}
	private void XPathDocumentTest(Document docum)
	{
		CachedXPathAPI XPathAPI = new CachedXPathAPI();
		assertNotNull("XPathAPI null",XPathAPI);
	
		Element root = docum.getDocumentElement();
		assertNotNull("root element null",root);
		NodeIterator iter = ((DocumentTraversal)docum).createNodeIterator(root, NodeFilter.SHOW_ALL, null, true);
		assertNotNull("walker null",iter);
		Node node;
		while ((node = iter.nextNode()) != null)
		{
			try 
			{
				String xpath = XPathForNode.getXPath(node,null);
//				System.out.println(xpath);
				Node selected = XPathAPI.selectSingleNode(docum,xpath);
				assertSame("not the same node",selected,node);
				if (node.getNodeType()==Node.ELEMENT_NODE)
				{
					NamedNodeMap attributes = node.getAttributes();
					for (int i=0;i<attributes.getLength();i++)
					{
						Attr attr = (Attr)attributes.item(i);
						if (!attr.getNodeName().startsWith("xmlns"))
						{  
							xpath = XPathForNode.getXPath(attr,null);
							selected = XPathAPI.selectSingleNode(docum,xpath);
							assertSame("not the same node",selected,attr);
						}
					}
				}
			} catch (Exception e)
			{
				assertTrue("There was an exception "+e.toString(),false);
			}
		}
		
	
	}

	
//	public void testFail() {
//		assertTrue("fail test",false); 
//	}
	
}