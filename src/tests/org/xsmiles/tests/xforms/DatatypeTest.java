package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import java.util.Hashtable;
import junit.extensions.*;

/*
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DOMErrorHandler;
import org.w3c.dom.DOMError;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

*/

import org.w3c.dom.traversal.*;



import  org.apache.xerces.dom.*;
import  org.w3c.dom.*;
import  org.w3c.dom.ls.*;

import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes

// Imported Serializer classes

// XPath
import org.xml.sax.SAXException;    

import java.io.*;
import java.net.URL;


import fi.hut.tml.xsmiles.content.DummyFetcher;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceParser;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.BindElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.*;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.*;
import fi.hut.tml.xsmiles.xml.JaxpXMLParser;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.MainDependencyGraph;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.SubDependencyGraph;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;

import org.xml.sax.ErrorHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.SAXParseException;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

import org.apache.xerces.parsers.DOMParser;
import org.apache.xerces.parsers.DOMBuilderImpl;

import org.apache.xerces.dom.PSVIElementNSImpl;
import org.apache.xerces.xni.psvi.*;
import org.apache.xerces.impl.xs.psvi.*;
import org.apache.xerces.impl.xs.*;
import org.apache.xerces.impl.dv.*;

/**
 * @author Mikko Honkala
 */
public class DatatypeTest extends TestCase  
    //implements DOMErrorHandler,DOMEntityResolver,EntityResolver, ErrorHandler 
{
	private Document doc;
	private MainDependencyGraph maindep;
	private SubDependencyGraph sub;
	//protected CachedXPathAPIEx xpathAPI;
	protected Hashtable cursors;
    
    protected Reader schemaReader;
    
	DOMParser parser;
	static final String documentClassname = XFormsConfiguration.getInstance().getInstanceDocumentClassName();
	final String xmlFilename = "../src/tests/org/xsmiles/tests/xforms/datatypes.xml";
	//final String schemaFilename = "../src/tests/org/xsmiles/tests/xforms/datatypes.xsd";
	final String schemaFilename = "datatypes.xsd";
	final String base = "file:../src/tests/org/xsmiles/tests/xforms";
        final    String schemaURLstr = base+"/"+schemaFilename;
	
	public DatatypeTest(String name)  {
		super(name);
	}
	public static void main (String[] args) {
		junit.textui.TestRunner.run (suite());
	}
	public static Test suite() {
		return new TestSuite(DatatypeTest.class);
	}
	protected void setUp() {
		try
		{
			FileReader r = new FileReader(new File(xmlFilename));
            
            doc = this.loadDocument_newest(r,this.openURL(schemaURLstr)); // give the schema reader as well
            //doc = this.loadDocument_newest(r,this.openURL(schemaURLstr)); // give the schema reader as well
			//doc = this.loadDocument(r,new File(schemaFilename));
			//doc = this.loadDocument_new(r,base,false);

		
		//doc = this.loadDocument(r,base+"/"+xmlFilename);
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace(System.out);
		}
		
	}

	public void testInstanceDocument() throws Exception
	{
		this.assertTrue("Instance document was not created",doc instanceof InstanceDocument);
	}
	
	public void testPSVIImpl() throws Exception
	{
		Element root = doc.getDocumentElement();
		this.assertNotNull("root element==null",root);
		this.assertTrue("The root element was not of type PSVIElementNSImpl", root instanceof PSVIElementNSImpl);
		this.assertTrue("The root element was not of type ElementPSVI", root instanceof ElementPSVI);
		ElementPSVI integer = (ElementPSVI)root.getElementsByTagNameNS("*","integer").item(0);
		this.assertNotNull("int element==null",integer);
		XSTypeDefinition typedef = integer.getTypeDefinition();
		this.assertNotNull("integer typedef null",typedef);
		XSTypeDefinition basetypedef = typedef.getBaseType();
		System.out.println("typedef: "+typedef+"\n"+"basetypedef :"+basetypedef);
		this.assertTrue("basetypedef was not of type XSSimpleType", basetypedef instanceof XSSimpleType);
		/*
		 */
	}
	public void testBasicPSVI()
	{
		Element root = doc.getDocumentElement();
		
		System.out.println("** DOUBLE");
		//String type;
		int primitivetype;
		InstanceNode elem;
		InstanceItem item;
		//ElementPSVI doubleE = (ElementPSVI)root.getElementsByTagNameNS("*","double").item(0);
		//type = this.getBaseType(doubleE);
		//primitivetype = this.getPrimitiveType(doubleE);
	
		elem = (InstanceNode)root.getElementsByTagNameNS("*","double").item(0);
		item = elem.getInstanceItem();
		//type = this.getBaseType(doubleE);
		primitivetype = item.getPrimitiveTypeId();

		
		//System.out.println("base type: "+type+" primitive type:"+primitivetype);
		assertEquals("primitive type for integer wrong",primitivetype,XSSimpleType.PRIMITIVE_DOUBLE);

		//integer.
		System.out.println("** INT complex");
		
		elem = (InstanceNode)root.getElementsByTagNameNS("*","integer").item(0);
		item = elem.getInstanceItem();
		//type = this.getBaseType(doubleE);
		primitivetype = item.getPrimitiveTypeId();
		assertEquals("primitive type for integer wrong",primitivetype,XFormsConfiguration.getInstance().getDataFactory().XSMILES_INTEGER);
		//System.out.println("base type: "+type+" primitive type:"+primitivetype);

	}
	/** this needs revalidation in order to work, I guess */
	public void testAddNode() throws Exception
	{
		Element root = doc.getDocumentElement();
		
		System.out.println("** ADD NODE : DOUBLE");
		//String type;
		int primitivetype;
		InstanceNode elem;
		InstanceItem item;
		//ElementPSVI doubleE = (ElementPSVI)root.getElementsByTagNameNS("*","double").item(0);
		//type = this.getBaseType(doubleE);
		//primitivetype = this.getPrimitiveType(doubleE);
	
		elem = (InstanceNode)root.getElementsByTagNameNS("*","double").item(0);
		
		Element parent = (Element)elem.getParentNode();
		//parent.removeChild(elem);
		
		InstanceNode newNode = (InstanceNode)root.getOwnerDocument().createElementNS(null,"double");
		Node textNode = root.getOwnerDocument().createTextNode("9.34234");
		newNode.appendChild(textNode);
		
		//String xmls;
		//xmls = XMLSerializerTest.writeXML(root);
		//System.out.println(xmls);
		
		parent.replaceChild(newNode,elem);

		//xmls = XMLSerializerTest.writeXML(root);
		//System.out.println(xmls);

		//errorList = new Vector();
		Vector errorList = revalidateDocument(doc);
		assertEquals("Wrong number of errors",0,errorList.size());
		
		item = newNode.getInstanceItem();
		//type = this.getBaseType(doubleE);
		primitivetype = item.getPrimitiveTypeId();

		
		//System.out.println("base type: "+type+" primitive type:"+primitivetype);
		assertEquals("primitive type for integer wrong",primitivetype,XSSimpleType.PRIMITIVE_DOUBLE);
	}
	/** this needs revalidation in order to work, I guess */
	public void testAddAttribute() throws Exception
	{
		Element root = doc.getDocumentElement();
		
		System.out.println("** ADD Attribute : doubleAttr");
		//String type;
		int primitivetype;
		InstanceNode elem;
		InstanceItem item;
		//ElementPSVI doubleE = (ElementPSVI)root.getElementsByTagNameNS("*","double").item(0);
		//type = this.getBaseType(doubleE);
		//primitivetype = this.getPrimitiveType(doubleE);
	
		elem = (InstanceNode)root.getElementsByTagNameNS("*","attributes").item(0);
		
		
		InstanceNode newNode = (InstanceNode)root.getOwnerDocument().createAttributeNS(null,"doubleAttr");
        newNode.setNodeValue("9.234234err34");
        ((Element)elem).setAttributeNodeNS((Attr)newNode);
		
		String xmls;
		xmls = XMLSerializerTest.writeXML(root);
		System.out.println(xmls);
		

		//xmls = XMLSerializerTest.writeXML(root);
		//System.out.println(xmls);

		Vector errorList = revalidateDocument(doc);
		assertEquals("Wrong number of errors",2,errorList.size());
		
		item = newNode.getInstanceItem();
		//type = this.getBaseType(doubleE);
		primitivetype = item.getPrimitiveTypeId();

		
		//System.out.println("base type: "+type+" primitive type:"+primitivetype);
		assertEquals("primitive type for double wrong",primitivetype,XSSimpleType.PRIMITIVE_DOUBLE);
        
        newNode.setNodeValue("9.234234e34");

		errorList = new Vector();
		revalidateDocument(doc);
		assertEquals("Wrong number of errors",0,errorList.size());
		primitivetype = item.getPrimitiveTypeId();

		
		//System.out.println("base type: "+type+" primitive type:"+primitivetype);
		assertEquals("primitive type for double wrong",primitivetype,XSSimpleType.PRIMITIVE_DOUBLE);
	}
	public void testReValidateSingleValue() throws Exception
	{
		short primitivetype;
		InstanceNode elem;
		InstanceItem item;
	
		elem = (InstanceNode)doc.getElementsByTagNameNS("*","double").item(0);
		item=elem.getInstanceItem();
		try
		{
			Object ret = item.revalidate();
		} catch (InvalidDatatypeValueException e)
		{
			assertNotNull("Should have validated "+XFormsUtil.getText(elem),null);
		}
		XFormsUtil.setText(elem,"3.1errror");
		boolean error=false;
		try
		{
			Object ret = item.revalidate();
		} catch (InvalidDatatypeValueException e)
		{
			System.out.println("Error (this is good!) : "+e.getMessage());
			error=true;
		}
		assertEquals("Should have not validated"+XFormsUtil.getText(elem),error,true);
	}
	public void testReValidateDocument() throws Exception
	{
		Element elem;
		elem = (Element)doc.getElementsByTagNameNS("*","double").item(0);
		Vector errorList = 
            revalidateDocument(doc);
		assertEquals("Wrong number of errors",0,errorList.size());

		elem.getFirstChild().setNodeValue("3.1errror");
		errorList = 
		revalidateDocument(doc);
		assertEquals("Wrong number of errors",2,errorList.size());
	}
	
	protected Vector revalidateDocument(Document adoc)
	{
		assertTrue("Document is not normalizable",adoc instanceof InstanceDocument);
		PSVIInstanceDocumentImpl core = (PSVIInstanceDocumentImpl)adoc;
        return core.validateDocument(false);

	}
    
    protected Reader openURL(String URLstr) throws Exception
    {
        URL url = new URL(URLstr);
        InputStreamReader reader = new InputStreamReader(url.openConnection().getInputStream());
        return reader;
    }

    protected Document loadDocument_newest(Reader r, Reader schema) throws Exception
    {
        //Document doc = this.loadDocument_new(r,null,true);
        //Document tempDoc = new JaxpXMLParser().openDocument(r,false);

        //return new InstanceParser().read(tempDoc,schema);
        SchemaPool pool = new XercesSchemaPoolImpl();//new DummyFetcher());
        pool.addSchema(this.schemaURLstr);
        return new InstanceParser().read(r,this.schemaURLstr,pool);
        //return this.read(doc,schema);
    }
}