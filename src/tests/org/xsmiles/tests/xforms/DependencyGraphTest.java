package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import java.util.Hashtable;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;
import org.apache.xpath.*;
import org.apache.xpath.objects.*;
import org.xml.sax.SAXException;



import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.BindElement;
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatIndexHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.ExpressionContainer;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.MainDependencyGraph;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.SubDependencyGraph;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;

/**
 * @author Mikko Honkala
 */
public class DependencyGraphTest extends XPathBase implements XFormsContext, ModelContext
{
    private Document doc;
    private MainDependencyGraph maindep;
    private SubDependencyGraph sub;
    
    public DependencyGraphTest(String name)
    {
        super(name);
    }
    
    
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
        try
        {
                final String documentClassname = XFormsConfiguration.getInstance().getInstanceDocumentClassName();
            //final String docStr =
            //"<?xml version=\"1.0\" standalone=\"no\"?>item/total\" calculate=\"../units * ../price\" /></root>";
            final String docStr =
            "<?xml version=\"1.0\" standalone=\"no\"?><purchaseOrder xmlns=\"\"><items><item><name>X-Smiles desktop</name><units>5</units><price>100</price><total>0</total><discount>0</discount></item></items><totals><subtotal>0</subtotal><tax>0</tax><total>0</total><rowcount>0</rowcount>	</totals><info><tax>0.22</tax></info>	<payment><as>cash</as><cc/><exp>01/2003</exp></payment></purchaseOrder>";
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            dfactory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",documentClassname);
            dfactory.setNamespaceAware( true );
            DocumentBuilder parser=dfactory.newDocumentBuilder();
            doc=parser.parse(new InputSource(new java.io.StringReader(docStr)));
            super.setUp();
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
    public static Test suite()
    {
        return new TestSuite(DependencyGraphTest.class);
    }
    
    public void testInstanceDocument() throws Exception
    {
        this.assertTrue("Instance document was not created",doc instanceof InstanceDocument);
    }
    
    public void testMainDepCreation() throws Exception
    {
        maindep = new MainDependencyGraph();
        this.createBinds(maindep,"<?xml version=\"1.0\" standalone=\"no\"?><root><bind nodeset=\"/*\" calculate=\"500 * 2\"/></root>");
        //maindep.printGraph();
    }
    
    public void testSimpleSum() throws Exception
    {
        maindep = new MainDependencyGraph();
        this.createBinds(maindep,"<?xml version=\"1.0\" standalone=\"no\"?><root><bind nodeset=\"/purchaseOrder/items/item/total\" calculate=\"../units * ../price\" /></root>");
        //maindep.printGraph();
        maindep.recalculate();
        String retStr  = this.xpathAPI.evalToString(doc,this.xpathAPI.createXPathExpression("/purchaseOrder/items/item/total/text()"),doc,null);
        //System.out.println("Returns: "+ret);
        String expected = "500";
        System.out.println("Comparison between : '"+expected+"' and '"+retStr+"' ="+retStr.equals(expected));

        this.assertTrue("Calculation result '"+retStr+"' wrong, expected: '"+expected+"'",retStr.equals(expected));
        
    }
    
    public void testDependency1() throws Exception
    {
        maindep = new MainDependencyGraph();
        this.createBinds(maindep,"<?xml version=\"1.0\" standalone=\"no\"?><root><bind nodeset=\"/purchaseOrder/items/item/total\" calculate=\"../units * ../price\" /><bind nodeset=\"purchaseOrder/totals/subtotal\" calculate=\"sum(/purchaseOrder/items/item/total)\"/></root>");
        //maindep.printGraph();
        maindep.recalculate();
        String retStr  = this.xpathAPI.evalToString(doc,this.xpathAPI.createXPathExpression(
                "/purchaseOrder/items/item/total/text()"
                ),doc,null);
        //System.out.println("Returns: "+ret);
        String expected = "500";
        this.assertTrue("Calculation result '"+retStr+"' wrong, expected: '"+expected+"'",retStr.equals(expected));
        
        //retStr  = this.evalXPathEx(doc,"/purchaseOrder/totals/subtotal/text()",doc);
       retStr  = this.xpathAPI.evalToString(doc,this.xpathAPI.createXPathExpression(
                "/purchaseOrder/totals/subtotal/text()"),doc,null);
        //System.out.println("Returns: "+ret);
        expected = "500";
        this.assertTrue("Calculation result '"+retStr+"' wrong, expected: '"+expected+"'",retStr.equals(expected));
        
    }
    
    public void testDependency2() throws Exception
    {
        maindep = new MainDependencyGraph();
        this.createBinds(maindep,"<?xml version=\"1.0\" standalone=\"no\"?><root><bind nodeset=\"purchaseOrder/totals/subtotal\" calculate=\"sum(/purchaseOrder/items/item/total)\"/><bind nodeset=\"/purchaseOrder/items/item/total\" calculate=\"../units * ../price\" /></root>");
        //maindep.printGraph();
        maindep.recalculate();
        //Object ret  = this.evalXPathEx(doc,"/purchaseOrder/items/item/total/text()",doc);
        String retStr  = this.xpathAPI.evalToString(doc,this.xpathAPI.createXPathExpression(
                "/purchaseOrder/items/item/total/text()"
                ),doc,null);
        //System.out.println("Returns: "+ret);
        String expected = "500";
        //String retStr = ret.toString();
        this.assertTrue("Calculation result '"+retStr+"' wrong, expected: '"+expected+"'",retStr.equals(expected));
        
        //ret  = this.evalXPathEx(doc,"/purchaseOrder/totals/subtotal/text()",doc);
        retStr  = this.xpathAPI.evalToString(doc,this.xpathAPI.createXPathExpression(
                "/purchaseOrder/totals/subtotal/text()"
                ),doc,null);
        //System.out.println("Returns: "+ret);
        expected = "500";
        this.assertTrue("Calculation result '"+retStr+"' wrong, expected: '"+expected+"'",retStr.equals(expected));
    }
    
    public void testCircularDependency() throws Exception
    {
        maindep = new MainDependencyGraph();
        this.createBinds(maindep,"<?xml version=\"1.0\" standalone=\"no\"?><root><bind nodeset=\"purchaseOrder/totals/subtotal\" calculate=\"/purchaseOrder/items/item/total * 2\"/><bind nodeset=\"/purchaseOrder/items/item/total\" calculate=\"purchaseOrder/totals/subtotal * 3\" /></root>");
        //maindep.printGraph();
        maindep.recalculate();
        Object ret  = this.evalXPathEx(doc,"/purchaseOrder/items/item/total/text()",doc);
        //System.out.println("Returns: "+ret);
        String expected = "500";
        //this.assertTrue("Calculation result '"+ret+"' wrong, expected: '"+expected+"'",expected.equals(ret.toString()));
        
    }
    
    private void createBinds(MainDependencyGraph mdep,String d) throws Exception
    {
        //final String docStr =
        //"<?xml version=\"1.0\" standalone=\"no\"?><root xmlns:xforms=\"http://www.w3.org/2002/08/xforms/cr\"><child>10</child><child>20<extra/></child><child>30</child><info><tax>0.22</tax><child/><currency rate=\"0.5\">0</currency></info></root>";
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware( true );
        DocumentBuilder parser=dfactory.newDocumentBuilder();
        Document tempdoc=parser.parse(new InputSource(new java.io.StringReader(d)));
        NodeList binds = tempdoc.getElementsByTagNameNS("*","bind");
        for (int i=0;i<binds.getLength();i++)
        {
            Element bind = (Element)binds.item(i);
            BindImpl bindImpl = new BindImpl(bind);
            mdep.addBindToGraph(bindImpl,this);
            //mdep.printGraph();
        }
    }
    
    public class BindImpl implements BindElement, ExpressionContainer
    {
        Element node;
        public BindImpl(Element n)
        {
            node = n;
        }
        public String getCalculateString()
        {
            return node.getAttribute("calculate");
        }
        public String getRelevantString()
        {
            return node.getAttribute("relevant");
        }
        public String getRequiredString()
        {
            return node.getAttribute("required");
        }
        public String getReadonlyString()
        {
            return node.getAttribute("readonly");
        }
        public String getIsvalidString()
        {
            return node.getAttribute("isvalid");
        }
        public Node getNamespaceContextNode()
        {
            return node;
        }
        /**
         * executes an arbitrary XPath expression
         */
        //public XObject evalXPath(Node context,XPathExpression xpath, Node nsnode) throws TransformerException;
        public XPathEngine getXPathEngine()
        {
            return xpathAPI;
        }

        /** @return the context node for the XPath expression */
        public Node getContextNode()
        {
            return node;
        }
        
        /** @return the namespace context node for the XPath expression */
        //public Node getNamespaceContextNode();
        public NodeList getBoundNodeset()
        {
            try
            {
                String nodeset = node.getAttribute("nodeset");
                Node nscontext = getNamespaceContextNode();
                Object ret = xpathAPI.eval(doc,getXPathEngine().createXPathExpression(nodeset),nscontext,null);
                System.out.println("getBoundNodeset");
                return (NodeList)ret;
            } catch (Exception e)
            {
                System.out.println(e);
                return null;
            }
            // cachedXPathApiEX.eval(this.getAttribute("nodeset"));
        }
        /** @return the list of context nodes of this expression */
        public NodeList getContextNodeList()
        {
            return this.getBoundNodeset();
        }
        
        public String getTypeString()
        {
            return "";
        }
        
        /** @return the model context for this expression  */
        public ModelContext getModelContext()
        {
            return null;
        }
        
    }
  
}