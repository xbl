package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import junit.extensions.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
//import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathLookup;

/**
 * Tests the XPathLookup, which is used by the XFOrms constraints engine,
 * to find out which nodes a single XPath references.
 * It runs different XPaths, and analyzes the result.
 * @author Mikko Honkala
 */
public class JaxenXPathTest extends XPathTest
{
    
    
    public JaxenXPathTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
            System.setProperty(XFormsConfiguration.PROPERTY_XPATH_ENGINE,XFormsConfiguration.XPATH_JAXEN_CLASS);
            super.setUp();
    }
    protected void tearDown() throws Exception
    {
            System.setProperty(XFormsConfiguration.PROPERTY_XPATH_ENGINE,XFormsConfiguration.XPATH_XALAN_CLASS);
            super.tearDown();
    }
    public static Test suite()
    {
        return new TestSuite(JaxenXPathTest.class);
    }
    
    

}