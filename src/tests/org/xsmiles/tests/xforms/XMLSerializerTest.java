package org.xsmiles.tests.xforms;

import junit.framework.*;
import java.util.Vector;
import java.io.*;

import junit.extensions.*;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.ls.DOMWriter;
import org.w3c.dom.ls.DOMImplementationLS;
import org.apache.xerces.dom.DOMImplementationImpl;


// Imported Serializer classes
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

// XPath
import org.apache.xpath.CachedXPathAPI;

import org.apache.xml.serialize.*;
import org.apache.xerces.dom.DocumentImpl;

//import fi.hut.tml.xsmiles.mlfc.xforms.instance.XMLSerializer;
import fi.hut.tml.xsmiles.xslt.JaxpXSLT;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceParser;

/**
 * This unit test tests whether an XML DOM can be serialized an de-serialized
 * without losing, for instance namespace information. There has
 * been problems with certain Xerces versions.
 * @author Mikko Honkala
 */
public class XMLSerializerTest extends TestCase
{
    private Document doc,doc2,doc3;
    //private Document docNS;
    public XMLSerializerTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
        try
        {
            String docStr =
            "<?xml version=\"1.0\" standalone=\"no\"?><root xmlns:my=\"my\"><wrapper><child>10</child><my:child>20</my:child><child>30</child><my:tax>0.22</my:tax><currency my:rate=\"0.5\">0</currency></wrapper></root>";
            String docStr2 =
            "<?xml version=\"1.0\" standalone=\"no\"?><root xmlns:my=\"my\"><wrapper><child>10</child><my:child>20</my:child><child>30</child><my:tax>0.22</my:tax><hip/><currency my:rate=\"0.5\">0</currency></wrapper></root>";
            String docStr3 =
            "<?xml version=\"1.0\" standalone=\"no\"?><root xmlns:my=\"my\" xmlns:xsd=\"xsdns\" xmlns:xsi=\"xsins\"><wrapper><child>10</child><my:child>20</my:child><child>30</child><my:tax>0.22</my:tax><hip/><currency my:rate=\"0.5\" xsi:type=\"xsd:decimal\">0</currency></wrapper></root>";
            doc=this.readXML(new java.io.StringReader(docStr));
            doc2=this.readXML(new java.io.StringReader(docStr2));
            doc3=this.readXML(new java.io.StringReader(docStr3));
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
    
    protected Document readXML(java.io.Reader r) throws Exception
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware( true );
        DocumentBuilder parser=dfactory.newDocumentBuilder();
        return parser.parse(new InputSource(r));
    }
        /*
        protected void writeXML_old(Element elem,java.io.Writer w) throws Exception {
                //XMLSerializer serializer = new XMLSerializer();
                //serializer.serialize(elem, w);
                //JaxpXSLT.SerializeNode(w,elem,true,false,true);
        }
         
        protected void writeXML_201(Element elem,java.io.Writer w) throws Exception {
         
                        // This seems to work with Xerces 2.0.1, but not with Xerces 2.1.0
         
                        //JaxpXSLT.SerializeNode(wr,root,true,true,true);
                        Document adoc;
                        if (elem instanceof Document) adoc=(Document)elem;
                        else adoc=elem.getOwnerDocument();
         
                        // i18n:  DocumentImpl contains the encoding specified
                        //        in the XForms submitInfo tag.  If no encoding
                        //        was specified, use ISO-8859-1 as default.
                        String encoding = null;
            if (adoc instanceof DocumentImpl)
            {
                encoding = ((DocumentImpl)adoc).getEncoding();
            }
            if (encoding == null) encoding = "ISO-8859-1";
         
                        OutputFormat of = new OutputFormat(adoc);
                        of.setIndenting(false);
                        of.setPreserveSpace(true);
                        of.setLineSeparator(org.apache.xml.serialize.LineSeparator.Windows);
            // i18n:  Use the document's encoding
            of.setEncoding(encoding);
                        SerializerFactory fac = SerializerFactory.getSerializerFactory("xml");
                        org.apache.xml.serialize.Serializer serializer = fac.makeSerializer(w,of);
                        ((XMLSerializer)serializer).setNamespaces(true);
                        //serializer.setNamespaces(true);
                        if (elem instanceof Document)
                                serializer.asDOMSerializer().serialize((Document)elem);
                        else if (elem instanceof Element)
                                serializer.asDOMSerializer().serialize((Element)elem);
         
        }
        protected String writeXML(Node elem) throws Exception {
                    // create DOM Serializer
         
            System.out.println("\n---DOMWriter output---");
                        Document adoc;
                        if (elem instanceof Document) adoc=(Document)elem;
                        else adoc=elem.getOwnerDocument();
            DOMWriter domWriter = ((DOMImplementationLS)DOMImplementationImpl.getDOMImplementation()).createDOMWriter();
                        // i18n:  DocumentImpl contains the encoding specified
                        //        in the XForms submitInfo tag.  If no encoding
                        //        was specified, use ISO-8859-1 as default.
                        String encoding = null;
            if (adoc instanceof DocumentImpl)
            {
                encoding = ((DocumentImpl)adoc).getEncoding();
            }
            if (encoding == null) encoding = "ISO-8859-1";
                        domWriter.setEncoding(encoding);
            return domWriter.writeToString(elem);
        }
         */
    static String writeXML(Node elem) throws Exception
    {
        return writeXML(elem,true);
    }
    
    static String writeXML(Node elem, boolean fixupNS) throws Exception
    {
        // create DOM Serializer
        fi.hut.tml.xsmiles.xml.serializer.XMLSerializer serializer = new
        fi.hut.tml.xsmiles.xml.serializer.XMLSerializer();
        return serializer.writeToString(elem,fixupNS);
    }
    
    public static Test suite()
    {
        return new TestSuite(XMLSerializerTest.class);
    }
    
    public void testDocument()
    {
        assertNotNull("parsed doc should not be null",doc);
    }
    
    public void testSimple()
    {
        this.compareDOM(doc,doc);
    }
    
    public void testDoc1() throws Exception
    {
        this.outputInputDoc(doc.getDocumentElement());
    }
    public void testDoc2() throws Exception
    {
        this.outputInputDoc(doc2.getDocumentElement());
    }
    public void testNamespaceFixing() throws Exception
    {
    }
    public void testNamespaceFixing2() throws Exception
    {
        Element elem = (Element)doc3.getDocumentElement().getElementsByTagNameNS("*","currency").item(0);
        //InstanceParser.copyNamespaceDeclarations(elem,(Element)elem.getParentNode(),true,null);
        assertNotNull("could not find elem 2",elem);
        this.outputInputDoc(elem,false);
    }
    public void testNamespaceFixingWithNSCreation() throws Exception
    {
        Element elem = (Element)doc.getDocumentElement().getElementsByTagNameNS("my","child").item(0);
        // re-create a nsdeclaration here
        System.out.println("Creating a new nsdecl");
        //elem.setAttribute("xmlns:my", "my");
        elem.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:my", "my");
        assertNotNull("could not find wrapper",elem);
        this.outputInputDoc(elem,true);
    }
    protected void outputInputDoc(Element e1) throws Exception
    {
        this.outputInputDoc(e1,true);
    }
    protected void outputInputDoc(Element e1,boolean fixupNS) throws Exception
    {
        //StringWriter w = new StringWriter();
        String str = this.writeXML(e1,fixupNS);
        //String str = w.toString();
        StringReader r = new StringReader(str);
        System.out.println(str);
        Document in = this.readXML(r);
        this.compareDOM(e1,in.getDocumentElement());
    }
    protected void compareDOM(Node elem1, Node elem2)
    {
        assertEquals("Node types wrong",elem1.getNodeType(),elem2.getNodeType());
        assertEquals("Element localnames wrong",elem1.getLocalName(),elem2.getLocalName());
        assertEquals("Element namespaces",elem1.getNamespaceURI(),elem2.getNamespaceURI());
        assertEquals("Element values",elem1.getNodeValue(),elem2.getNodeValue());
        
        Node child1 = elem1.getFirstChild();
        Node child2 = elem2.getFirstChild();
        while (child1!=null)
        {
            assertNotNull("less children in elem2",child2);
            this.compareDOM(child1,child2);
            // proceed
            child1=child1.getNextSibling();
            child2=child2.getNextSibling();
        }
        assertNull("Elem2 had more children",child2);
    }
    
    
    //	public void testFail() {
    //		assertTrue("fail test",false);
    //	}
    
}