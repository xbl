/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package org.xsmiles.tests.xforms;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.xslt.JaxpXSLT;

import org.w3c.dom.*;

/**
 * This class is used to create an XPath expression that matches a given node.
 * It is used when the Xerces validation engine reports errors, and the erroneus 
 * node must be located in the original document as well.
 */


public class XPathForNode {

	/**
	 * This method initiates the creation of the XPath string for a node and a 
	 * given context node.
	 * For namespaced element nodes it uses the test name()=="ns:nodename"
	 * For namespaced attributes : attribute::*[name()='ns:attrname'] 
	 */
	public static String getXPath(Node node, Node context)
	{
		if(node==null)
			return null;

		String result;

		if(node==context)
			return ".";

		String step;
		switch(node.getNodeType())
		{
		case Node.ELEMENT_NODE:
			if (node.getNamespaceURI()==null)
			{
				step = 
					node.getNodeName()+"["+getNodePosition(node, node.getNodeName())+"]";
			} else
			{
				step = "*[name()='"+node.getNodeName()+"']["+
					getNodePosition(node, node.getNodeName())+"]";
			}
			result = getXPath(node.getParentNode(), context) + "/" + step;
			break;
		case Node.ATTRIBUTE_NODE:
			if (node.getNamespaceURI()==null)
			{
				step = 	"@" + node.getNodeName();
					
			} else
			{
				step = "attribute::*[name()='"+node.getNodeName()+"']";
			}
			result=getXPath(((Attr)node).getOwnerElement(),context) + "/" + step;
			break;
		case Node.DOCUMENT_NODE:
			result = "";
			break;
		case Node.TEXT_NODE:
			result = getXPath(node.getParentNode(), context) +
			"/text()["+getNodePosition(node, Node.TEXT_NODE)+"]";
			break;
		default:
			result = getXPath(node.getParentNode(), context) +
			"/*["+getNodePosition(node)+"]";
		}
//		Log.debug("XPathForNode called, result: "+result);
		return result;

	}
    /**
     *
     */
    protected static int getNodePosition(Node node) {
        Node parent = node.getParentNode();
        if(parent==null)
            return 1;
        
        NodeList nl = parent.getChildNodes();
        for(int i=0;i<nl.getLength();i++) {
            if(nl.item(i)==node)
                return i+1;
        }
        return -1;
    }
    
    /**
     *
     */
    protected static int getNodePosition(Node node, short nodetype) {
        Node parent = node.getParentNode();
        if(parent==null)
            return 1;
        
        NodeList nl = parent.getChildNodes();
        int pos = nl.getLength()-1;
        while(nl.item(pos)!=node)
            pos--;
        
        int count=pos-1;
        while(count>=0) {
            if(nl.item(count).getNodeType()!=nodetype)
                pos--;
            count --;
        }
        return pos+1;   
    }
    
	/**
	 *
	 */
    protected static int getNodePosition(Node node, String nodeName) {
        Node parent = node.getParentNode();
        if(parent==null)
            return 1;
        
        NodeList nl = parent.getChildNodes();
        int pos = nl.getLength()-1;
        while(nl.item(pos)!=node)
            pos--;
        
        int count=pos-1;
        while(count>=0) {
            if(!nl.item(count).getNodeName().equals(nodeName))
                pos--;
            count --;
        }
        return pos+1;   
    }

}

