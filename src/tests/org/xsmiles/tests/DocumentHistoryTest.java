package org.xsmiles.tests;

import java.util.*;

import javax.swing.*;

import junit.framework.*;
import junit.extensions.*;



import fi.hut.tml.xsmiles.DocumentHistory;
import fi.hut.tml.xsmiles.XLink;

/**
 * This test tests the DocumentHIstory class
 * @author Mikko Honkala
 */
public class DocumentHistoryTest extends TestCase {

        //protected Vector links;
        protected XLink[] links = {
            new XLink("http://www.xsmiles.org/test0"),
            new XLink("http://www.xsmiles.org/test1"),
            new XLink("http://www.xsmiles.org/test2"),
            new XLink("http://www.xsmiles.org/test3"),
            new XLink("http://www.xsmiles.org/test4")
        };
        
        protected DocumentHistory history;
	public DocumentHistoryTest(String name)
	{
		super(name);
	}
	public static void main (String[] args)
	{
		junit.textui.TestRunner.run (suite());
	}
	protected void setUp()
	{
            history = new DocumentHistory();
	}
	public static Test suite()
	{
		return new TestSuite(DocumentHistoryTest.class);
	}

	public void testBasicBackForward()
	{
            // go to 0 - 1 - 2, and go to back 1 time. Finger should be on 1 and
            // the history should contain all 3
            this.add(new int[] {0,1,2});
            history.backwardsInHistory();
            this.compareResults(1, new int[] {0,1,2});
            history.forwardInHistory();
            this.compareResults(2, new int[] {0,1,2});
	}
	public void testAddMultiple()
	{
            // go to 0 - 1 - 2 - 2 - 2 ... Finger should be on 2 and
            // the history should contain everything only once
            this.add(new int[] {0,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2});
            this.compareResults(2, new int[] {0,1,2});
	}
        
        protected void goBack(int times)
        {
            for (int i=0;i<times;i++)
            {
                history.backwardsInHistory();
            }
        }
        protected void goForward(int times)
        {
            for (int i=0;i<times;i++)
            {
                history.forwardInHistory();
            }
        }
        public void testGoPastStartEnd()
	{
            // go to 0 - 1 - 2 - 2 - 2 ... Finger should be on 2 and
            // the history should contain everything only once
            this.add(new int[] {0,1,2});
            this.goBack(17);
            this.compareResults(0, new int[] {0,1,2});
            XLink test = history.backwardsInHistory();
            assertNull("back should be null if in first",test);
            this.goForward(17);
            this.compareResults(2, new int[] {0,1,2});
            this.add(new int[] {3,4});
            this.compareResults(4, new int[] {0,1,2,3,4});
            XLink test2 = history.forwardInHistory();
            assertNull("forward should be null if in last",test2);
	}
	public void testDestroyPartsOfHistory()
	{
            // go to 0 - 1 - 2 - 3-4, then 2*back and then again 0 -1 -2-3 -4 -
            //Finger should be on 4 and
            // the history should contain 01201234
            this.add(new int[] {0,1,2,3,4});
            this.compareResults(4, new int[] {0,1,2,3,4});
            history.backwardsInHistory();
            history.backwardsInHistory();
            this.compareResults(2, new int[] {0,1,2,3,4});
            this.add(new int[] {0,1,2,3,4});
            this.compareResults(4, new int[] {0,1,2,0,1,2,3,4});
	}
        
        protected void add(int[] contents)
        {
            for (int i = 0;i<contents.length;i++)
            {
                history.addURL(links[contents[i]]);
            }
        }
        protected void compareResults(int finger, int[] contents)
        {
            XLink last = history.getLastDocument();
            Vector visited = history.getHistory();
            assertEquals("finger",finger,getPos(last));
            assertEquals("history size",contents.length,visited.size());
            for (int i = 0;i<contents.length;i++)
            {
                assertEquals("history pos "+i,visited.elementAt(i),links[contents[i]]);
            }
        }
        
        protected int getPos(XLink link)
        {
            for (int i=0;i<links.length;i++)
            {
                if (links[i]==link) return i;
            }
            return -1;
        }


}

