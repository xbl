/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 28, 2004
 *
 */
package org.xsmiles.tests.security;

import java.io.InputStream;
import java.net.URL;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import fi.hut.tml.xsmiles.mlfc.signature.Resolver;
import fi.hut.tml.xsmiles.mlfc.signature.SignDialog;
import fi.hut.tml.xsmiles.mlfc.signature.SignatureCreator;
import fi.hut.tml.xsmiles.mlfc.signature.SignatureElementImpl;
import fi.hut.tml.xsmiles.mlfc.signature.VerifySignature;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * @author honkkis
 *
 */
public class SignatureTest extends TestCase 
{
    static
    {
        org.apache.xml.security.Init.init();
    }
    protected Document docNS;
    
    
    public SignatureTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
        try
        {
            docNS=this.createDoc();
         } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    protected Document createDoc() throws Exception
    {
        javax.xml.parsers.DocumentBuilderFactory dbf =
            javax.xml.parsers.DocumentBuilderFactory.newInstance();

         //XML Signature needs to be namespace aware
         dbf.setNamespaceAware(true);

         javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
         org.w3c.dom.Document doc = db.newDocument();

         //Build a sample document. It will look something like:
         //<!-- Comment before -->
         //<apache:RootElement xmlns:apache="http://www.apache.org/ns/#app1">Some simple text
         //</apache:RootElement>
         //<!-- Comment after -->
         doc.appendChild(doc.createComment(" Comment before "));

         Element root = doc.createElementNS("http://www.apache.org/ns/#app1",
                                            "apache:RootElement");
         String nsSpecNS="http://www.w3.org/2000/xmlns/";
         root.setAttributeNS(null, "attr1", "test1");
         root.setAttributeNS(null, "attr2", "test2");
         root.setAttributeNS(nsSpecNS, "xmlns:foo", "http://example.org/#foo");
         root.setAttributeNS("http://example.org/#foo", "foo:attr1", "foo's test");



         root.setAttributeNS(nsSpecNS, "xmlns:apache", "http://www.apache.org/ns/#app1");
         doc.appendChild(root);
         root.appendChild(doc.createTextNode("Some simple text\n"));
         return doc;
    }
    /*
    protected Document createDoc2() throws Exception
    {
        String docStr2 =
            "<?xml version=\"1.0\" standalone=\"no\"?>\n<root><child xmlns=\"hephop\">10</child><child>20</child><child>30</child><tax>0.22</tax><currency rate=\"0.5\">0</currency></root>";
            //"<?xml version=\"1.0\" standalone=\"no\"?><root xmlns=\"myns\" xmlns:my=\"myns\"><my:child>10</my:child><my:child>20</my:child><my:child>30</my:child><my:tax>0.22</my:tax><my:currency rate=\"0.5\">0</my:currency></root>";
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            dfactory.setNamespaceAware( true );
            dfactory.setAttribute("http://xml.org/sax/features/namespaces", Boolean.TRUE);

            DocumentBuilder parser=dfactory.newDocumentBuilder();
            return parser.parse(new InputSource(new java.io.StringReader(docStr2)));
        
    }*/
    
    public static Test suite()
    {
        return new TestSuite(SignatureTest.class);
    }
    
    
/*    public void testEnvelopedSignature() throws Exception
    {
        SignatureCreator.signEnveloped(docNS,null,null);
        InputStream is = Resolver.createStream(docNS);
        URL baseURL=new URL("http://www.xsmiles.org/");
        boolean valid = VerifySignature.verify(is,baseURL,System.out);
        assertTrue("Verify signature failed",valid);
    }*/
    public void testEnvelopingSignature() throws Exception
    {
        this.sign(SignatureElementImpl.SIGNATURE_ENVELOPING);
    }
    public void testEnvelopedSignature() throws Exception
    {
        this.sign(SignatureElementImpl.SIGNATURE_ENVELOPED);
    }
    public void sign(short type) throws Exception
    {
        Vector docs=new Vector();
        Vector URLs=new Vector();
        docs.addElement(docNS);
        URL baseURL=new URL("http://www.xsmiles.org/");
        Element signature = SignatureCreator.sign(type,docs, URLs, baseURL,null, null);
        //SignDialog.debugNode(signature.getOwnerDocument());
        InputStream is = Resolver.createStream(signature.getOwnerDocument());
        boolean valid = VerifySignature.verify(is,baseURL,System.out);
        assertTrue("Verify signature failed",valid);
    }
    public void testCopySig() throws Exception
    {
        short type = SignatureElementImpl.SIGNATURE_ENVELOPING;
        Vector docs=new Vector();
        Vector URLs=new Vector();
        docs.addElement(docNS);
        URL baseURL=new URL("http://www.xsmiles.org/");
        Document newDoc = SignatureCreator.createEmptyDoc(true);
        Element signature = SignatureCreator.sign(type,docs, URLs, baseURL,null, newDoc);
        //SignDialog.debugNode(signature.getOwnerDocument());
        boolean valid = VerifySignature.verify(signature,baseURL,System.out);
        Document copyDoc = SignatureCreator.createEmptyDoc(true);
        Element copySig = (Element)copyDoc.importNode(signature,true);
        valid = VerifySignature.verify(signature,baseURL,System.out);
        copyDoc.appendChild(copySig);
        valid = VerifySignature.verify(copySig,baseURL,System.out);
        assertTrue("Verify signature failed",valid);
    }
    /*
    public void testEnvelopedSignature() throws Exception
    {
        Vector docs=new Vector();
        Vector URLs=new Vector();
        docs.addElement(docNS);
        URL baseURL=new URL("http://www.xsmiles.org/");
        Element signature = SignatureCreator.signEnveloped(docs, URLs, baseURL,null, null);
        //SignDialog.debugNode(signature.getOwnerDocument());
        InputStream is = Resolver.createStream(signature.getOwnerDocument());
        boolean valid = VerifySignature.verify(is,baseURL,System.out);
        assertTrue("Verify signature failed",valid);
    }*/


}
