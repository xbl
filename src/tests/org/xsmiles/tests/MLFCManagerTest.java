package org.xsmiles.tests;

import java.awt.EventQueue;
import java.util.*;

import javax.swing.*;

import junit.framework.*;
import junit.extensions.*;



import org.apache.xerces.dom.*;
import org.w3c.dom.*;


import fi.hut.tml.xsmiles.content.xml.MLFCManager;
import fi.hut.tml.xsmiles.dom.AsyncChangeHandler;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;

//import fi.hut.tml.xsmiles.MLFCManager;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * This test tests the MLFCManager. It creates an imaginary XMLDocument, extended document,
 * and MLFCs (primary, secondary, host, parasites) and checks that they are destroyed
 * properly.
 *
 * @author Mikko Honkala
 */
public class MLFCManagerTest extends TestCase
{
    
    private MLFCManager manager;
    private TestExtendedDocument testDoc;
    private JPanel cont;
    
    private TestMLFC hostMLFC;
    private TestMLFC parasiteMLFC;
    private Hashtable parasites;
    
    private TestXMLDocument xmldoc;
    
    public MLFCManagerTest(String name)
    {
        super(name);
    }
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }
    protected void setUp()
    {
        //manager = new MLFCManager(null,null);
        cont = new JPanel();
        
    }
    public static Test suite()
    {
        return new TestSuite(MLFCManagerTest.class);
    }
    public void testDumb()
    {
    	Browser.printParserVersions();
    }
        public void testActivatePrimaryHostWithNoParasites()
        {
                hostMLFC = new TestMLFC();
                testDoc = new TestExtendedDocument();
                xmldoc = new TestXMLDocument(null,testDoc);
                assertNotNull("testDoc null",testDoc);
                assertNotNull("hostmlfc null",hostMLFC);
                testDoc.hostMLFC=hostMLFC;
 
                assertEquals("test doc inited",testDoc.inited,0);
                assertEquals("test doc destroyed",testDoc.destroyed,0);
                assertEquals("mlfc started",hostMLFC.started,0);
                assertEquals("mlfc stopped",hostMLFC.stopped,0);
 
                MLFCManager.initMLFC(xmldoc,cont,null,null,null);
                try {
                    MLFCManager.activateMLFC(xmldoc,cont,null,null,true);
                } catch(Exception e) {
                        e.printStackTrace(System.err);
                }
                assertEquals("test doc inited",testDoc.inited,1);
                assertEquals("mlfc started",hostMLFC.started,1);
                //assertTrue("secondary/primary flag",hostMLFC.isPrimary());
                MLFCManager.closeMLFC(xmldoc);
                assertEquals("test doc destroyed",testDoc.destroyed,1);
                assertEquals("mlfc stopped",hostMLFC.stopped,1);
        }
 
        public void testActivatePrimaryHostAndParasiteMLFC()
        {
                hostMLFC = new TestMLFC();
                parasiteMLFC = new TestMLFC();
                parasites = new Hashtable();
                parasites.put("test",parasiteMLFC);
                testDoc = new TestExtendedDocument();
                xmldoc = new TestXMLDocument(null,testDoc);
                assertNotNull("testDoc null",testDoc);
                assertNotNull("hostmlfc null",hostMLFC);
                assertNotNull("parasitemlfc null",parasiteMLFC);
                testDoc.hostMLFC=hostMLFC;
                testDoc.parasiteMLFCs=parasites;
 
                assertEquals("test doc inited",testDoc.inited,0);
                assertEquals("test doc destroyed",testDoc.destroyed,0);
                assertEquals("mlfc started",hostMLFC.started,0);
                assertEquals("mlfc stopped",hostMLFC.stopped,0);
 
                MLFCManager.initMLFC(xmldoc,cont,null,null,null);
                try {
                    MLFCManager.activateMLFC(xmldoc,cont,null,null,true);
                } catch(Exception e) {
                        e.printStackTrace(System.err);
                }
                assertEquals("test doc inited",testDoc.inited,1);
                assertEquals("mlfc started",hostMLFC.started,1);
                assertEquals("parasitemlfc started",parasiteMLFC.started,1);
                //assertTrue("secondary/primary flag",hostMLFC.isPrimary());
                MLFCManager.closeMLFC(xmldoc);
                assertEquals("test doc destroyed",testDoc.destroyed,1);
                assertEquals("mlfc stopped",hostMLFC.stopped,1);
                assertEquals("parasitemlfc stopped",parasiteMLFC.stopped,1);
        }
/* 
        public void testActivateSecondaryHostAndParasiteMLFC()
        {
                hostMLFC = new TestMLFC();
                parasiteMLFC = new TestMLFC();
                parasites = new Hashtable();
                parasites.put("test",parasiteMLFC);
                testDoc = new TestExtendedDocument();
                xmldoc = new TestXMLDocument(null,testDoc);
                assertNotNull("testDoc null",testDoc);
                assertNotNull("mlfcmanager null",manager);
                assertNotNull("hostmlfc null",hostMLFC);
                assertNotNull("parasitemlfc null",parasiteMLFC);
                testDoc.hostMLFC=hostMLFC;
                testDoc.parasiteMLFCs=parasites;
 
 
                assertEquals("test doc inited",testDoc.inited,0);
                assertEquals("test doc destroyed",testDoc.destroyed,0);
                assertEquals("mlfc started",hostMLFC.started,0);
                assertEquals("mlfc stopped",hostMLFC.stopped,0);
                manager.activateSecondaryMLFC(xmldoc,cont);
                assertEquals("test doc inited",testDoc.inited,1);
                assertEquals("mlfc started",hostMLFC.started,1);
                assertEquals("parasitemlfc started",parasiteMLFC.started,1);
                assertTrue("secondary/primary flag",hostMLFC.isPrimary()==false);
                manager.stopCurrentActiveMLFCs();
                assertEquals("test doc destroyed",testDoc.destroyed,1);
                assertEquals("mlfc stopped",hostMLFC.stopped,1);
                assertEquals("parasitemlfc stopped",parasiteMLFC.stopped,1);
        }
 
        //	public void testXPathForNodeSimple()
        //	{
        //		Element root = doc.getDocumentElement();
        //		assertNotNull("root element null",root);
        //		assertEquals("root element's name",root.getNodeName(),"root");
        //		Node secondChild = root.getElementsByTagNameNS("","child").item(1);
        //		assertNotNull("second child null",secondChild);
        //		String xpath = XPathForNode.getXPath(secondChild,null);
        //		assertEquals("xpath wrong",xpath,"/root[1]/child[2]");
        //	}
 */
    
    public class TestExtendedDocument extends DocumentImpl implements ExtendedDocument, AsyncChangeHandler
    {
        public MLFC hostMLFC;
        public Hashtable parasiteMLFCs;
        
        public int inited = 0;
        public int destroyed = 0;
        
        public XSmilesStyleSheet getStyleSheet()
        {
            return null;
        }
        
        
        public MLFC getHostMLFC()
        {
            return hostMLFC;
        }
        public Hashtable getParasiteMLFCs()
        {
            return parasiteMLFCs;
        }
        public void init()
        {
            inited++;
        }
        public void destroy()
        {
            destroyed++;
        }
        public void setHostMLFC(MLFC mlfc)
        {
        }
        public boolean isHTMLDocument()
        {
            return false;
        }
        /** was this originally HTML document -> XHTML */
        public void setHTMLDocument(boolean isHTMLDoc)
        {
        }
        
        public boolean isInited()
        {
            return true;
        }


        /* (non-Javadoc)
         * @see fi.hut.tml.xsmiles.dom.ExtendedDocument#getTitle()
         */
        public String getTitle()
        {
            // TODO Auto-generated method stub
            return null;
        }


        public AsyncChangeHandler getChangeHandler()
        {
            // TODO Auto-generated method stub
            return this;
        }


        public void invokeLater(Runnable doRun)
        {
            EventQueue.invokeLater(doRun);
        }


        public boolean isDispatchThread()
        {
            return EventQueue.isDispatchThread();
        }


		public Element getAnonymousElementByAttribute(Element root, String attrname, String value) {
			// TODO Auto-generated method stub
			return null;
		}


		public void eval(String scriptText) {
			// TODO Auto-generated method stub
			
		}


		public void exposeToScriptEngine(String name, Object theObject) {
			// TODO Auto-generated method stub
			
		}


		public void deleteExposedObject(String name, Object theObject) {
			// TODO Auto-generated method stub
			
		}
        
    }
    
    public class TestMLFC extends MLFC
    {
        
        public int started = 0;
        public int stopped = 0;
        
        public TestMLFC()
        {
            //super();
        }
        
        public final String getVersion()
        {
            return "0.1";
        }
        
        /**
         * Create a DOM element.
         */
        public Element createElementNS(DocumentImpl doc, String ns, String tag)
        {
            return null;
        }
        public void start()
        {
            started++;
        }
        
        public void stop()
        {
            stopped++;
        }
        
    }
    
    public class TestXMLDocument extends XSmilesXMLDocument
    {
        public ExtendedDocument edoc;
        public TestXMLDocument(BrowserWindow browser,ExtendedDocument extdoc)
        {
            super(browser,extdoc);
            edoc=extdoc;
        }
        
        public Document getDocument()
        {
            return edoc;
        }
        
    }
}
