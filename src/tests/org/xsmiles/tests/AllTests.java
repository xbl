package org.xsmiles.tests;


import org.xsmiles.tests.security.SignatureTest;

import junit.framework.*;

/**
 * TestSuite that runs all the sample tests
 *
 */
public class AllTests {

	public static void main (String[] args) {
		junit.textui.TestRunner.run (suite());
	}
	public static Test suite ( ) {
		TestSuite suite= new TestSuite("All X-Smiles Tests");
		fi.hut.tml.xsmiles.Browser.printParserVersions();
		
		// XForms Tests
		suite.addTest(org.xsmiles.tests.xforms.AllTests.suite());
		
		// general X-Smiles tests
	    suite.addTest(DocumentHistoryTest.suite());
	    suite.addTest(MLFCManagerTest.suite());
	    suite.addTest(XMLBrokerTest.suite());
	    
	    // XML signature test
	    suite.addTest(SignatureTest.suite());

	    return suite;
	}
}