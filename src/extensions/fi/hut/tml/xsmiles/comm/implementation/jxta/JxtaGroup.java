/*
 * Created on Sep 24, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.jxta;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.comm.CommBroker;
import fi.hut.tml.xsmiles.comm.Group;
import fi.hut.tml.xsmiles.comm.implementation.general.ContactBase;
import fi.hut.tml.xsmiles.comm.session.Session;

/**
 * @author qlin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class JxtaGroup extends ContactBase implements Group {
    public JxtaGroup()
    {
    	super();
    }
    
        
    public JxtaGroup(String name, String address)//, String image)
    {
        super();
        
        if (name == null || name.equals("") || address == null) {
            Log.error("No user name defined!");
            return;
        }
        
        infoTable.put("name", name);
        infoTable.put("address", address);
        /*if (image != null)
            userInformation.put("image", image);*/
    }
	
    public Session connect()
    {
        Session session = CommBroker.getCommSession("jxta").createSession();
        session.openSession(this);
        return session;
    }
}
