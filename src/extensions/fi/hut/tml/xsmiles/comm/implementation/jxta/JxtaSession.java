/*
 * Created on Aug 6, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.jxta;

import java.util.Vector;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.comm.Contact;
import fi.hut.tml.xsmiles.comm.Group;
import fi.hut.tml.xsmiles.comm.User;
import fi.hut.tml.xsmiles.comm.session.*;
import fi.hut.tml.xsmiles.comm.implementation.general.events.CommEventSenderBase;

/**
 * @author qlin
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class JxtaSession extends CommEventSenderBase implements Messaging, Session
{
	
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Messaging#sendXMLMessage(org.w3c.dom.Document)
	 */
	public void sendXMLMessage(Document message) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Messaging#sendTextMessage(java.lang.String)
	 */
	public void sendTextMessage(String message) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Messaging#addMessageListener(fi.hut.tml.xsmiles.comm.session.MessageListener)
	 */
	public void addMessageListener(MessageListener listener) {
		// TODO Auto-generated method stub
		
	}
	
	public void removeMessageListener(MessageListener listener) {
		// TODO Auto-generated method stub
	}


	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Session#openSession(fi.hut.tml.xsmiles.comm.Group)
	 */
	public void openSession(Contact _contact)
	{
	    
	    if (_contact instanceof Group)
	        Log.debug("Trying to create a JXTA session with group:"+ _contact.getName());
		// TODO: try to search a group with the name from JXTA 
		// TODO: if not found, create the group within JXTA
		// TODO: join the group
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Session#closeSession()
	 */
	public void closeSession() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Session#getUsers()
	 */
	public Vector getUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Session#getTarget()
	 */
	public Contact getTarget() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.session.Session#accept()
	 */
	public void accept() {
		// TODO Auto-generated method stub
		
	}
	
	public void decline()
	{
	    
	}
	
	public void sendMessage(Message msg)
	{
	    
	}
	
	public Messaging createMessaging()
	{
	    return (Messaging)this;
	}
}
