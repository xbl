/*
 * Created on Aug 6, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.jxta;

import org.apache.fop.apps.Starter;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.comm.AddressBook;
import fi.hut.tml.xsmiles.comm.CommSession;
import fi.hut.tml.xsmiles.comm.Group;
import fi.hut.tml.xsmiles.comm.User;
import fi.hut.tml.xsmiles.comm.presence.Presentity;
import fi.hut.tml.xsmiles.comm.session.Messaging;
import fi.hut.tml.xsmiles.comm.session.Session;
import fi.hut.tml.xsmiles.comm.session.SessionListener;
import fi.hut.tml.xsmiles.comm.implementation.general.events.CommEventSenderBase;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import net.jxta.peergroup.PeerGroup;
import net.jxta.peergroup.PeerGroupFactory;
import net.jxta.protocol.PeerGroupAdvertisement;
import net.jxta.discovery.DiscoveryService;
import net.jxta.pipe.PipeService;
import net.jxta.exception.PeerGroupException;



/**
 * @author qlin
 *
 * TODO Start Jxta
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class JxtaCommSession extends CommEventSenderBase implements CommSession{
	// TODO: implements Groups
	
    static PeerGroup netPeerGroup = null;
    static PeerGroupAdvertisement groupAdvertisement = null;
    private DiscoveryService discovery;
    private PipeService pipe;

    public JxtaCommSession() { }

   public void init()
   {
   }
  
    void startJxta() {
        try {
            // create, and Start the default jxta NetPeerGroup
            netPeerGroup = PeerGroupFactory.newNetPeerGroup();
        } catch (PeerGroupException e) {
            // could not instanciate the group, print the stack and exit
            System.out.println("fatal error : group creation failure");
            e.printStackTrace();
            System.exit(1);
        }
        
        // this is how to obtain the group advertisement
        groupAdvertisement = netPeerGroup.getPeerGroupAdvertisement();
        // get the discovery, and pipe service
        System.out.println("Getting DiscoveryService");
        discovery = netPeerGroup.getDiscoveryService();
        System.out.println("Getting PipeService");
        pipe = netPeerGroup.getPipeService();
    }

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#signIn()
	 */
	public void signIn() {
		// TODO Auto-generated method stub
		JxtaCommSession starter = new JxtaCommSession();
        System.out.println ("Starting jxta ....");
        starter.startJxta();
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#signOut()
	 */
	public void signOut() {
		// TODO Auto-generated method stub
		
		System.out.println ("Good Bye ....");
        System.exit(0);
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#getOwner()
	 */
	public User getOwner() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#addSessionListener(fi.hut.tml.xsmiles.comm.session.SessionListener)
	 */
	public void setSessionListener(SessionListener listener) {
		// TODO Auto-generated method stub
		
	}
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createUser()
	 */
	public User createUser() {
		Log.error("JxtaCommSession.createUser() not supported");
		return null;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createGroup()
	 */
	public Group createGroup() {
		// TODO Auto-generated method stub
		return new JxtaGroup();
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createSession()
	 */
	public Session createSession() {
        return new JxtaSession();
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createSession(java.lang.String)
	 */
	public Session createSession(String address) {
		Log.error("JxtaCommSession.createSession(address) not supported");
		return null;
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createMessaging()
	 */
	public Messaging createMessaging(Session session) {
		return (Messaging)session;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#getAddressBook()
	 */
	public AddressBook getAddressBook() {
		Log.error("JxtaCommSession.getAddressBook() not supported");
		return null;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#getPresentity(fi.hut.tml.xsmiles.comm.User)
	 */
	public Presentity getPresentity(User _user) {
		Log.error("JxtaCommSession.getPresentity(user) not supported");
        return null;
	}


	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#setMLFCListener(fi.hut.tml.xsmiles.mlfc.MLFCListener)
	 */
	public void setMLFCListener(MLFCListener listener) {
		// TODO Auto-generated method stub
		
	}
	
	public Object getFeature(Class feature)
	{
	    // TODO: Support for extensions.
	    return null;
	}
	
}
