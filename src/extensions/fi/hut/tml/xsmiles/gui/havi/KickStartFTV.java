/*
 * Created on Apr 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/*
 * This class is the actual xlet
 *
 **/

package fi.hut.tml.xsmiles.gui.havi;

import fi.hut.tml.xsmiles.gui.gui2.KickStart;

//import javax.tv.xlet.*;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 *
 * Modified by @pcesar in order to run on MHP
 */

public class KickStartFTV extends KickStart{
   
    public static void main(String args[])
    {
	instance = new KickStartFTV();
	instance.initSequence(args);
    }
    

    public String getGUIClass()
    {
        return "fi.hut.tml.xsmiles.gui.havi.FTVGUI";
    }
}
