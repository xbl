/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.gui.havi;


import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.gui.components.havi.HaviComponentFactory;

import fi.hut.tml.xsmiles.EventBroker;
import fi.hut.tml.xsmiles.BrowserLogic;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFCController;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.XSmilesUIAWT;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
import fi.hut.tml.xsmiles.gui.media.general.JMFContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import java.io.File;
import java.net.URL;

import org.ftv.ui.HScene;

/**
 * A stub GUI. The main function is to send events to third party GUIs and also
 * to provide default implementations of the ComponentFactory and ContentHandler
 * classes.
 * 
 * @author Juha
 */
public class NullGUIHavi extends XSmilesUIAWT 
{
    //  private MLFCController mlfcController;
    private BrowserWindow browser;
    private BrowserConstraints constraints;
    private ContentHandlerFactory contentHandlerFactory;

    /**
     * @param b the BrowserWindow
     * @param c componentContainer (optional)
     */
    public NullGUIHavi(BrowserWindow b, Container c) {
        super(b,c);
        browser=b;
    
    }
    
    /**
     * @param b the BrowserWindow
     */
    public NullGUIHavi(BrowserWindow b) {
        super(b,null);
        browser=b;
    }
    
    protected MLFCControls mlfcControls;
    
    public MLFCControls getMLFCControls() {
        if (mlfcControls == null) {
            mlfcControls = new DefaultMLFCControls(getComponentFactory());
        }
        return mlfcControls;
    }


    
    public ComponentFactory getComponentFactory() {
        return new HaviComponentFactory(browser);
        //return new AWTComponentFactory(browser);
    }
    
    public void validate() {

    }

    
    public BrowserConstraints getBrowserConstraints() {
        return constraints;
    }
  
    

 


    public ContentHandlerFactory getContentHandlerFactory()
    {
        if (contentHandlerFactory==null) contentHandlerFactory=new JMFContentHandlerFactory(browser);
        return contentHandlerFactory;
    }
  
    /**
     * Return mainFrame
     * @return mainFrame
     */
    
    protected Window window;
    public Window getWindow() 
    {
        return window;
    }
    
    public void setWindow(Window win)
    {
        this.window=win;
    }
        
  
    public void destroy() 
    {
        browser=null;
    }

   
    /**
     * Returns the MLFCToolbar where the MLFC's can then 
     * add their controls
     * 
     * @return The container
     */
    public Container getMLFCToolbar() {
        return new Panel();
    }
  

        /**
     * Stop animating. 
     */
    
    public void browserReady()
    {
	super.browserReady();
	Log.debug("Browser resting.");
	Container c = browser.getContentArea();
	//c.setBackground(Color.yellow);
	//c.add(new fi.hut.tml.xsmiles.gui.components.havi.ColoredCanvas());
	Log.debug("Browser  Container: "+c.getClass());
	try
	{
	    if (c.getParent()!=null)
	    {
	        if (c.getParent().getParent()!=null)
	        {
	Container parent = c.getParent().getParent();
	//Container parent = c.getParent();
	//Log.debug("SMILMLFC parents parent: "+parent.getClass());
	if (parent instanceof HScene)
	    {
		HScene scene = (HScene)parent;
		scene.repaint();
	    }
        //Log.debug("");
        //printComponentHierarchy(this.browser.getContentArea(),0);
	        }
	    }
	} catch (NullPointerException e)
	{
	    Log.error(e,"The browser container does not have parent scene. Continueing.");
	}
    }
	
    private void printComponentHierarchy(Component comp,int level)
    {
        for (int i=0;i<level;i++)
            System.out.print("--");
        System.out.println(comp);
        if (comp instanceof Container)
        {
            Container cont = (Container)comp;
            for (int i=0;i<cont.getComponentCount();i++)
            {
                Component child=cont.getComponent(i);
                if (child!=null) this.printComponentHierarchy(child,level+1);
            }
        }
    }
/*
    private class NewComponentFactory extends DefaultComponentFactory {
        public NewComponentFactory(BrowserWindow browser)
        {
            super(browser);
        }
    }*/
  
}
