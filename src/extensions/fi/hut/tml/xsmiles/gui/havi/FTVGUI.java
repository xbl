/*
 * Created on Apr 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.havi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.Color;
import java.awt.Point;

import org.ftv.ui.HContainer;
import org.ftv.ui.HScene;
import org.ftv.ui.HTextButton;
import org.ftv.ui.HNavigable;
import org.ftv.ui.HVisible;
import org.ftv.ui.HSceneTemplate;
import org.ftv.ui.HStaticIcon;
import org.ftv.ui.HStaticText;

import org.ftv.ui.ftvextensions.HScrollPane;
import org.ftv.ui.event.*;

import fi.hut.tml.xsmiles.gui.components.*;

import fi.hut.tml.xsmiles.gui.gui2.awt.GUITab;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;

import fi.hut.tml.xsmiles.gui.gui2.awt.AWTFocusPointsProvider;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import java.util.Vector;

//import jmfkaffe.*;
import javax.media.*;

import com.sun.media.util.Registry;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 *
 * Modified by @pcesar in order to run on MHP
 */
public class FTVGUI extends AWTGUI
{
    HScene scene;
    protected Container createRootContainer()
    {

	scene = new HScene();
	scene.setLayout(new BorderLayout());
	scene.setVisible(false);
	scene.setBackgroundMode(HScene.NO_BACKGROUND_FILL);

	scene.validate();
        scene.setVisible(false);

	
	/** In case we are using SDL as graphics package, 
	    we need to include its rendering for Timesheets/SMIL
	    This code has to be moved where it belongs (smilar than xine)
	**/
	//PlugInManager.removePlugIn("jmfkaffe.SDLRenderer",PlugInManager.RENDERER);
	//PlugInManager.addPlugIn("jmfkaffe.SDLRenderer",(new SDLRenderer()).getSupportedInputFormats(),null,PlugInManager.RENDERER);
	//new JMFKaffe();
	//Registry.set("allowLogging", new Boolean(true));
	//Registry.set("secure.logDir", "/home/jvierine/ubik/build");
	//try{
	//  Registry.commit();
	// PlugInManager.commit();
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// Vector vect = PlugInManager.getPlugInList(null, null,PlugInManager.RENDERER);
	// for (int i=0; i<vect.size();i++)
	// System.out.println("PlugIn:"+vect.elementAt(i));

	/** root window **/
        return this.scene;

    }

    // In order to use in in Digital Television
    public void setDefaultWindowSize()
    {
	this.getRootContainer().setSize(720, 576);
    }
    
    protected String getComponentFactoryClassName()
    {
        return "fi.hut.tml.xsmiles.gui.components.havi.HaviComponentFactory";
    }
    protected String getGUIName()
    {
        return "nullGUIHavi";
    }
    
    protected Container createContainer()
    {
        return new HContainer();
    }
    
     protected GUITab createGUITabInternal()
    {
	 GUITab tab = new GUITabHavi(this);
	 return tab;
    }

    class GUITabHavi extends GUITab
    {

	/** We add our own images for the topContainer **/
	public GUITabHavi(AWTGUI gui){
	    super(gui);
	    changeGUIElements("img/tv/backTV.png","img/tv/forwardTV.png","img/tv/homeTV.png","img/tv/reloadTV.png",50,50);
	}
	
	
	/** In the South instead of the Status container we include two colour buttons that allows the user to exit and to access the menu if needed **/
	//public void createStatusContainer(){
	/**
	   HStaticIcon redButton;
	   HStaticIcon blueButton;
	   HStaticText redText;
	   HStaticText blueText;
	   Container redContainer;
	   Container blueContainer;
	**/
	
	/** We create the container **/
	/**
	   this.statusContainer = this.createContainer();
	   this.statusContainer.setLayout(new BorderLayout());
	   this.addMLFCControls();
	**/

	/** We get the Images **/
	/**
	   Toolkit toolkit = Toolkit.getDefaultToolkit();
	   Image redImage =  toolkit.getImage("../bin/img/tv/tele_unarmred.png"); 
	   Image blueImage = toolkit.getImage("../bin/img/tv/tele_unarmblue.png");
	**/
	/** We create the Buttons **/
	/**
	   redButton = new HStaticIcon(redImage);
	   blueButton = new HStaticIcon(blueImage);
	**/
	/** We create the Text **/	    
	/**
	   redText =  new HStaticText("EXIT");
	   blueText = new HStaticText("MENU");
	   redText.setFont(new Font("verdana", Font.BOLD, 16));
	   blueText.setFont(new Font("verdana", Font.BOLD, 16));
	   redText.setForeground(Color.white);
	   blueText.setForeground(Color.white);
	**/
	
	/** We create a container for red button **/
	/**
	   redContainer = this.createContainer();
	   redContainer.setLayout(new BorderLayout());
	   redContainer.add(redButton, BorderLayout.NORTH);
	   redContainer.add(redText, BorderLayout.SOUTH);
	**/
	
	/** We create a container for blue button **/
	/**
	   blueContainer = this.createContainer();
	   blueContainer.setLayout(new BorderLayout());
	   blueContainer.add(blueButton, BorderLayout.NORTH);
	   blueContainer.add(blueText, BorderLayout.SOUTH);
	   
	   this.statusContainer.add(redContainer, BorderLayout.EAST);
	   this.statusContainer.add(blueContainer, BorderLayout.WEST);
	**/
	//}
	

	public void browserReady()
	{
	    super.browserReady();
	    scene.validate();
	    scene.setVisible(true);
	}
    }

    public  FocusPointsProvider createFocusPointsProvider()
    {
	FocusPointsProvider fp = new HaviFocusPointsProvider(this);
	return fp;
    }

    public class HaviFocusPointsProvider extends AWTFocusPointsProvider{
	
	public HaviFocusPointsProvider(AWTGUI gui){
	    super(gui);
	}

	public boolean isFocusablePoint(Component component){
	    return ( (component instanceof HNavigable) && (super.isFocusablePoint(component)) );
	}
    }
    

    // for some strange reason we have to do tricks for havi to show the doc.
    // These are not needed anymore    
    /*
      public void browserReady()
      {
      super.browserReady();
      this.dumpComponentHierarchy(this.scene);
     //this.browserWindow.getContentArea().setSize(800,400);
     //this.actionContainer.setSize(800,100);
     //this.scene.setSize(800,600);
     //this.getContentArea().getComponent(0).setSize(this.getContentArea().getSize()); // HAVI TEST
     scene.validate();
     this.dumpComponentHierarchy(this.scene);
     this.scene.repaint();
     System.out.println("Repaintng SCEK ");
     this.dumpComponentHierarchy(this.scene);
     Container c = (Container)(this.scene);	 
     Container cont = ((Container) (((Container) (c.getComponent(1))).getComponent(0)));
     if (cont instanceof HScrollPane)
     ((HScrollPane) (cont)).getButton().processHFocusEvent(new HFocusEvent(((HScrollPane) (cont)).getButton(), HFocusEvent.FOCUS_GAINED));
     
     };
    */

    /* This is now done at the AWT level

    public void decorateGUI()
    {
	this.getRootContainer().setLayout(new BorderLayout());
        
        Container contentArea = this.getContentArea();
        this.createListeners();
        this.createActionContainer();
        //this.createStatusContainer();
        //this.getRootContainer().add(this.statusContainer, BorderLayout.SOUTH);
        this.getRootContainer().add(this.topContainer, BorderLayout.NORTH);
	
	//        int width=850,height=640;
        // DEBUG START
	
	//  int actionH=200;
	//  int contentW=width,contentH=height-actionH;
	//  Dimension actionSize=new Dimension(width,actionH);
	// contentArea.setBounds(0,actionH+1,contentW,contentH);
		// this.topContainer.setSize(actionSize);
	//  this.actionContainer.setSize(actionSize);

	this.getRootContainer().add(contentArea, BorderLayout.CENTER);
        // DEBUG END
        //this.createMenus();
        //contentArea.setSize(800, 600);
	//this.getRootContainer().setSize(width, height);
	//this.getRootContainer().setSize(800, 600);
        //contentArea.setSize(780,580);
        this.dumpComponentHierarchy(this.scene);        
    }
    */

    protected Font getActionFont()
    {
	return new Font("Verdana",Font.PLAIN,16);
    }
    
    protected void disposeWindow(){
	if (scene != null) {
	    scene.setVisible(false);
	    scene = null;
	}
    }
    
    private void dumpComponentHierarchy(Component c)
    {
        System.out.println("Dumping components:");
        this.dumpComponentHierarchy(c,0);
    }
    
    private void dumpComponentHierarchy(Component c, int level)
    {
        StringBuffer debug = new StringBuffer();
        for (int i=0;i<level;i++) debug.append("----");
        debug.append(c.toString());
        debug.append(c.getSize().toString());
        debug.append("\n");
        System.out.print(debug);
        if (c instanceof Container)
        {
            Container cont = (Container)c;	   
            int compCount = cont.getComponentCount();
            for (int i=0;i<compCount;i++)
		{
		    Component comp = cont.getComponent(i);
		    this.dumpComponentHierarchy(comp,level+1);
            }
        }
        
    }
	

}
