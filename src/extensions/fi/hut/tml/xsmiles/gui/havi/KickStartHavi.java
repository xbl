/*
 * Created on Apr 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/*
 * This class is the actual xlet
 *
 **/

package fi.hut.tml.xsmiles.gui.havi;

import fi.hut.tml.xsmiles.gui.gui2.KickStart;

import javax.tv.xlet.*;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 *
 * Modified by @pcesar in order to run on MHP
 */

public class KickStartHavi extends KickStartFTV implements Xlet
{
    /** context of the Xlet **/
    private XletContext context;
    /** Start web page. This has to be changed **/
    //    private String[] args = {"/home/pcesar/src/xsmiles/demo/demos.xml"};
    private String[] args = {"-nosplash","../demo/demos.xml"};
    //  private String[] args = {"-nosplash","http://www.tml.hut.fi/~pcesar/2d-nav/2d-nav-big/2d-nav-big.xml"};
    //    private String[] args = {"-nosplash","http://lab.vodafone.com/tm/buttons/2d-nav-big.xhtml"};
   
    /** In MHP the main method does not exists, 
	but applications are defined as Xlets **/
    /**
       public static void main(String args[])
       {
       instance = new KickStartHavi();
       instance.initSequence(args);
       }
    **/

    public String getGUIClass()
    {
        return "fi.hut.tml.xsmiles.gui.havi.HaviGUI";
    }

   public void initXlet(XletContext ctx) throws XletStateChangeException{
       this.context = ctx;
   }


    public void startXlet() throws XletStateChangeException {
	instance = new KickStartHavi();
	instance.initSequence(args);
    }
    
    public void pauseXlet() {
    }
    
    /** This class should notify the GUI, 
	so all the widgets including scene are disposed
    **/
    public void destroyXlet(boolean b) {
	context.notifyDestroyed();
    }
}
