/*
 * Created on Apr 5, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.havi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.Color;
import java.awt.Point;

import org.ftv.ui.HContainer;
import org.ftv.ui.HTextButton;
import org.ftv.ui.HNavigable;
import org.ftv.ui.HVisible;

import org.havi.ui.HScene;
import org.havi.ui.HSceneFactory;
import org.havi.ui.HSceneTemplate;
import org.havi.ui.HScreenDimension;
import org.havi.ui.HScreenPoint;

import org.ftv.ui.HStaticIcon;
import org.ftv.ui.HStaticText;

import org.ftv.ui.ftvextensions.HScrollPane;
import org.ftv.ui.event.*;

import fi.hut.tml.xsmiles.gui.components.*;

import fi.hut.tml.xsmiles.gui.gui2.awt.GUITab;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;

import fi.hut.tml.xsmiles.gui.gui2.awt.AWTFocusPointsProvider;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import java.util.Vector;

//import jmfkaffe.*;
import javax.media.*;

import com.sun.media.util.Registry;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 *
 * Modified by @pcesar in order to run on MHP
 */
public class HaviGUI extends FTVGUI
{
    HScene scene;
    protected Container createRootContainer()
    {

	/** The root window or HScene is requested in this manner **/
	HSceneFactory factory = HSceneFactory.getInstance();
	HSceneTemplate hst = new HSceneTemplate();
	hst.setPreference(HSceneTemplate.SCENE_SCREEN_DIMENSION, new HScreenDimension(1, 1), HSceneTemplate.REQUIRED);
	hst.setPreference(HSceneTemplate.SCENE_SCREEN_LOCATION, new HScreenPoint(0, 0), HSceneTemplate.REQUIRED);
	scene = factory.getBestScene(hst);
	scene.setLayout(new BorderLayout());
	scene.setVisible(false);
	scene.setBackgroundMode(HScene.NO_BACKGROUND_FILL);
	
	scene.validate();
        scene.setVisible(false);
        return this.scene;
    }

    public void browserReady()
    {
	super.browserReady();
	scene.validate();
	scene.setVisible(true);
    }
    
    protected void disposeWindow(){
	if (scene != null) {
	    scene.setVisible(false);
	    HSceneFactory.getInstance().dispose(scene);
	    scene = null;
	}
    }
}
