package fi.hut.tml.xsmiles.gui.components.havi;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.net.URL;

import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*; // le interfaces for components
import fi.hut.tml.xsmiles.gui.components.awt.*; // le interfaces for components
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import org.ftv.ui.HContainer;
import org.ftv.ui.*;
import org.ftv.ui.ftvextensions.HScrollPane;

/**
 * component factory implementation for HAVI
 * @author Mikko Honkala
 * @version 0
 */
public class HaviComponentFactory implements ComponentFactory
{
    BrowserWindow b;
    CSSFormatter formatter;
    
    public HaviComponentFactory(BrowserWindow b)
    {
        this.b=b;
        //setOpaque(false);
    }
    
    public HaviComponentFactory()
    {
        this.b=null;
    }
    
    /**
     * @return a container for components
     */
    public XContainer getXContainer()
    {
        return null;
        //return new SwingContainer();
    }

    public XLinkComponent getXLinkComponent(String dest)
    {
        /*
        XLinkComponent lc=new SwingLink();
        lc.setDestination(dest);
        return lc;
         */
        return null;
    }
    
    /**
     * Give a XMLDocument, and get a rendered document
     * If you are in for events, just add an actionListener
     * and receive the XMLEvents from the component
     * @param doc The document
     * @return the visual component that contains rendered xmldocument. Null
     *         is returned if rendering is not possible.
     *
     * XForms notice:
     * (you might want to use this component to render XOutput)
     */
    public XDocument getXDocument(XLink link)
    {
        return null;
/*
    if(b!=null) {
      XDocument xd=new XADocument(b,link);
      return xd;
    } else {
      Log.error("Cannot return a document, because no link to browser");
      return null;
    }
 */
    }
    
    /**
     * @param s The intial text
     * @return a textarea
     */
    public XTextArea getXTextArea(String s)
    {
        HaviTextArea a =  new HaviTextArea();// TODO c);
        a.setText(s);
        return a;
        
        /*
        XTextArea ta=new SwingTextArea(s,b.getCurrentGUI());
        return ta;
         */
    }
    
    /**
     * @return A secret input control
     */
    public XSecret getXSecret(char c)
    {
        return new HaviInput();// TODO c);
    }
    
    /**
     * @return  one line input
     */
    public XInput getXInput()
    {
        return new HaviInput();
    }
    
    /**
     * @param from from what
     * @param to to where
     * @return a range control
     */
    public XRange getXRange(int from, int to, int step, int orientation)
    {
        return null;
        
        //return new SwingRange(from, to,step,orientation);
    }
    
    /**
     * Give an URL and receive the mediaelement.
     */
    public XMedia getXMedia(URL u)
    {
        Log.debug("No media handler yet");
        return null;
    }
    
    /**
     * @param v A vector containing the items to put in selector
     * @return a XSelectOne, which is used to select one item at a time
     */
    public XSelectOne getXSelectOne(String appearance, boolean open)
    {
        return new HaviSelectOneCompact();
        /*
        if (open) return new SwingSelectOneMinimal(true);
         
        if (appearance.equalsIgnoreCase("listbox")||appearance.equalsIgnoreCase("compact"))
            return new SwingSelectOneCompact();
        //if (appearance.equalsIgnoreCase("menu")||appearance.equalsIgnoreCase("minimal"))
        else
            return new SwingSelectOneMinimal(false);
         */
        
    }
    
    /**
     * @return a XSelectBoolean, which is used to select true / false
     */
    public XSelectBoolean getXSelectBoolean()
    {
        return new HaviSelectBoolean();
    }
    
    /**
     * @param v The vector that contains all possible selections
     * @return A selectMany control.
     */
    public XSelectMany getXSelectMany(String appearance, boolean open)
    {
        return new HaviSelectManyCompact();
    }
    
    /**
     * @return an upload control.
     */
    public XUpload getXUpload(String caption)
    {
        return new HaviUpload(caption,this);
        //return new SwingUpload(caption,this);
    }
    
    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save)
    {
        return null;
        //return new SwingFileDialog(this.b,save);
    }
    
    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save, String filename)
    {
        return null;
        //return new SwingFileDialog(this.b,save,filename);
    }
    /**
     * @return an authenticator control.
     */
    public XAuthDialog getXAuthDialog()
    {
        return null;
        //Frame parentframe = (Frame)b.getCurrentGUI().getWindow();
        //return new AuthDialog(parentframe);
    }
    /**
     * @return an confirmation control.
     */
    public XConfirmDialog getXConfirmDialog()
    {
        return null;
        //Frame parentframe = (Frame)b.getCurrentGUI().getWindow();
        //return new AWTConfirmDialog(parentframe);
    }
    /**
     * @return a button w a label
     */
    public XButton getXButton(String label, String iconUrl)
    {
        return new HaviButton(label,iconUrl);
        //return new SwingButton(label, iconUrl);
    }
    
    /**
     * @return a iconned button
     */
    public XButton getXButton(String iconUrl)
    {
        return new HaviButton(iconUrl);
        //return new SwingButton(iconUrl);
    }
    
    
    /**
     * @return a button with images
     */
    public XButton getXButton(String imageUrl, String focusedImage, String disabledImage)
    {
        // TODO
        return new HaviButton(imageUrl);
    }
    
    /**
     * @return a new calender control
     */
    public XCalendar getXCalendar()
    {
        return null;
        //return new SwingCalendar();
    }
    
    /** show an error dialog */
    public void showError(String title, String explanation)
    {
        return ;
        /*
         javax.swing.JOptionPane.showMessageDialog(null,
        explanation,title,javax.swing.JOptionPane.ERROR_MESSAGE,null);
         */
    }
    
    public void showLinkPopup(URL url, XMLDocument doc,java.awt.event.MouseEvent e, MLFCListener listener)
    {
        return ;
        /*
        SwingLinkPopup popup = new SwingLinkPopup();
        popup.show(e.getComponent(),e.getX(),e.getY(),doc,url,listener);
         */
    }
    /**
     * @return a caption (label) component
     */
    public XCaption getXCaption(String captText)
    {
        return new HaviCaption(captText);
    }
    
    /**
     * @ return a compound designed for holding a label and a component
     */
    public XLabelCompound getXLabelCompound(XComponent comp, XCaption capt, String captSide)
    {
        return new HaviLabelCompound(comp,capt,captSide);
    }
    //protected static KeyStroke HELPKEY = KeyStroke.getKeyStroke("F1");
    /** listen for all help keypresses etc, for this component and its ancestors */
    public void addHelpListener(Component component, ActionListener listener)
    {
        /*
        if (component instanceof JComponent)
        {
            JComponent comp = (JComponent)component;
            comp.registerKeyboardAction(listener,"help",HELPKEY,JComponent.WHEN_IN_FOCUSED_WINDOW);
        }
         */
    }
    
    /** remove a help listener */
    public void removeHelpListener(Component component, ActionListener listener)
    {
        /*
        if (component instanceof JComponent)
        {
            JComponent comp = (JComponent)component;
            comp.unregisterKeyboardAction(HELPKEY);
        }*/
    }
    
    /** create a content panel for the browser, add also maybe a layout manager */
    public Container createContentPanel()
    {
        Container c = createContainer();
        //c.setBackground(java.awt.Color.yellow);
        //((HContainer)c).setOpaque(false);
        return c;
    }
    
    public static Container createContainer()
    {
        Container c = new MyContainer();
        return c;
    }
    
    /** create a scroll pane for this components. e.g. in swing, create a 
     * JScrollPane and put the component inside 
     **/
    public Container createScrollPane(Component comp)
    {
        // no scrolling yet in havi
	HContainer scrollPane = new HScrollPane(); // Create a scrollpane for the text pane.
        //System.out.println("CREATE SCROLL PANE");
	//scrollPane.setSize(800,600);
	scrollPane.add(comp);
        
        //scrollPane.setVerticalScrollBarPolicy(scrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        return scrollPane;
    }

    
    /**
     * @param c Add a component to the MLFC Controls container of the GUI.
     */
    public void add(XComponent c)
    {
        /*
        Component co=c.getComponent();
        //if(co instanceof JComponent) {
        //  ((JComponent)co).setOpaque(true);
        //}
        add(c.getComponent());
        invalidate();
        validate();
        repaint();
         */
    }
    
    /**
     * @param c Remove a component to the MLFC Controls container of the GUI.
     */
    public void remove(XComponent c)
    {
        /*
        remove(c.getComponent());
        invalidate();
        validate();
        repaint();
         */
    }
    public void removeAll()
    {
    }
    
    public void validate()
    {
    }


    
    
    public XMenuBar getXMenuBar()
    {
        return new HaviMenuBar();
	//return null;
    }
    public XMenu getXMenu(String name)
    {
        return new HaviMenu(name);
	//return null;
    }
    
    public XMenuItem getXMenuItem( String name)
    {
        return new HaviMenuItem(name);
	//return null;
    }
    
    public void showMessageDialog(boolean isModal, String title, String message, long timeToLiveMillis) 
    {
        this.b.getCurrentGUI().setStatusText(title+" : "+message);
    }
    
    /** this container also paints the background before painting children */
    public static class MyContainer extends HContainer
    {
        
        public MyContainer()
	{
	    //Log.debug(this+" created");
            super();
	    this.setLayout(new BorderLayout());
        }
	
	
        public void paint(Graphics g)
        {
            //Log.debug("paint: "+g+" :width:"+g.getClipBounds().width+" height:"+g.getClipBounds().height);
            
	    if (isShowing())
		{
		    Color old=g.getColor();
		    g.setColor(this.getBackground());
		    Rectangle bounds = g.getClipBounds();
		    g.fillRect(bounds.x,bounds.y,bounds.width,bounds.height);
		    g.setColor(old);
		}
	    
            super.paint(g);
        }
	
        /*
	  public  void 	paintComponents(Graphics g) 
	  {
	  Log.debug("paintComponents: "+g);
	  if (isShowing())
	  {
	  }
	  super.paintComponents(g);
	  }
	*/
    }
    
    /** query for extension availability. For instance, calendar control is an extension */
    public boolean hasExtension(Class c)
    {
        return false;
    }
    
    /** return an extension control. hasExtension must be called first */
    public Object getExtension(Class c)
    {
            Log.error("There is no extension for : "+c);
            return null;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#createScrollPane(java.awt.Component, int)
     */
    public Container createScrollPane(Component arg0, int arg1)
    {
	Container c = new HScrollPane(arg0, arg1);
        return c;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#setScrollBar(java.awt.Container, int, int)
     */
    public void setScrollBar(Container arg0, int arg1, int arg2)
    {
        // TODO Auto-generated method stub
        
    }
    public XTabbedPane getXTabbedPane()
    {
        // TODO: create HAVITabbedPane
        return new AWTTabbedPane();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getXPanel()
     */
    public XPanel getXPanel()
    {
        // TODO Auto-generated method stub
        return new HaviPanel();
    }

    public XFocusManager getXFocusManager()
    {
	return new HaviFocusManager();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getCSSFormatter()
     */
    public CSSFormatter getCSSFormatter()
    {
        // TODO Auto-generated method stub
        if (formatter==null)
            formatter=AWTCSSFormatter.getInstance();
        return formatter;
    }
}
