/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.gui.components.XSelectMany;


/**
 * Select Many compact mode
 * @author Mikko Honkala
 * @author juha awt mod
 */
public class HaviSelectManyCompact extends HaviSelectOneCompact
    implements XSelectMany
{
    public HaviSelectManyCompact() 
    {
	this.multiple=true;
    }
    
    /*    
	  protected int getListSelectionModel()
	  {
	  return ListSelectionModel.MULTIPLE_INTERVAL_SELECTION ;
	  }
    */

    /**
     * @param o The object to set
     * @param b The value to set it as
     */
    public void setSelected(Object o, boolean b) 
    {
        System.err.println("HaviSelectManyCompact.setSelected(Object, boolean) not implemented!");
	//list.setSelectedValue(o,true);
        /*
        this.selectComponent.setItemSelected()
	String [] itemz=this.selectComponent..getItems();
	
	for(int i=0;i<list.getItemCount();) {
	    if(o.equals(list.getItem(i))) {
		if(b)
		    list.select(i);
		else
		    list.deselect(i);
	    }
	}*/
	//list.setSelectedValue(o,true);
    }
    
    public void clearSelection() 
    {
        this.selectComponent.clearSelection();
    }
    
    public int[] getSelectedIndices() 
    {
	return this.selectComponent.getSelectionIndices();
    }
    
    public void setSelectedIndices(int[] indices) 
    {
		for(int i=0;i<indices.length;i++) {
	        this.selectComponent.setItemSelected(indices[i],true);
		}
    }
}
