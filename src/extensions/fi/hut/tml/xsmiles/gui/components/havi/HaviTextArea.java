/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Oct 25, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.components.havi;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import org.ftv.ui.HMultilineEntry;
import org.ftv.ui.HSinglelineEntry;


/**
 * @author honkkis
 *
 */
public class HaviTextArea extends HaviInput
{
    /** creates the content component */
    public Component createContent()
    {
	//        textcomponent = new HMultilineEntry("Please, insert text in here ", 100, new Font("Verdana",Font.PLAIN,10),Color.blue);
	textcomponent = new HMultilineEntry("", 100, new Font("Verdana",Font.PLAIN,10),Color.blue);
        textcomponent.setEnabled(true);
	/*
	  if (!isSecret)
	  {
	  textcomponent=new JTextField();
	  }
	  else
	  {
	  textcomponent = new JPasswordField();
	  ((JPasswordField)textcomponent).setEchoChar(echoChar);
	  }*/
        textcomponent.addHTextListener(this);
        return textcomponent;
    }
}
