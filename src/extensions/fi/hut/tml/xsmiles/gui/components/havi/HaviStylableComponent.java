/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.awt.AWTStylableComponent;
import fi.hut.tml.xsmiles.gui.components.awt.AWTCSSFormatter;
import fi.hut.tml.xsmiles.gui.components.general.ComponentBase;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;

//import javax.swing.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.FocusListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.*;

import org.w3c.dom.css.CSSStyleDeclaration;
/**
 * This ontains common functionality
 * to style each of the components (caption, content)
 *
 * @author Mikko Honkala
 */
public class HaviStylableComponent extends ComponentBase
{
    
    
    public HaviStylableComponent()
    {
        super();
    }
    public HaviStylableComponent(Component comp)
    {
        this.content=comp;
    }
    
    public void setBackground(Color col){
	if (col==null)
	    this.content.setBackground(Color.white);
	if (col != null)
	    this.content.setBackground(col);
    }
    
}

