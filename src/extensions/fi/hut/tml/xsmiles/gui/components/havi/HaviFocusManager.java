/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import java.util.Vector;

import java.awt.*;
import java.awt.event.*;

import org.ftv.ui.*;
import org.ftv.ui.event.*;

import fi.hut.tml.xsmiles.gui.components.awt.AWTFocusManager;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;

import org.ftv.ui.ftvextensions.HScrollPane;
import java.awt.Point;
import fi.hut.tml.xsmiles.gui.components.XFocusManager;

/**
 * 
 * This is the base class that is extended by FocusPointProviders
 * @author Pablo Cesar, Mikko Honkala
 * 
 *  
 */

public class HaviFocusManager extends AWTFocusManager{

    public HaviFocusManager(){
	super();
    }


    public boolean isScrollPane(Container cont){
	return (cont instanceof HScrollPane);
    }

    public Point getScrollPosition(Component component){
	return ((HScrollPane)component).getScrollPosition();
    }
    
    public void cleanFocusPoint(FocusPoint fp){
	Component component = fp.getComponent();
	if (component == null)
	    super.cleanFocusPoint(fp);
	else
	    if (component instanceof HNavigable){
		((HNavigable)(component)).processHFocusEvent(new HFocusEvent(component, HFocusEvent.FOCUS_LOST));	
		rootContainer.requestFocus();
	    }
	    else
		super.cleanFocusPoint(fp);
    }
    
    public void drawFocusPoint(FocusPoint fp){
	Component component = fp.getComponent();
	if (component == null){
	    super.drawFocusPoint(fp);
	}else
	    if (component instanceof HNavigable)
		((HNavigable)(component)).processHFocusEvent(new HFocusEvent(component, HFocusEvent.FOCUS_GAINED));
	    else
		super.drawFocusPoint(fp);
    }  

    public boolean controlHaviScrollPage(FocusPoint fp){
	// Control that the focusPoint is in current HScroll Page
	HScrollPane scrollPane = ((HScrollPane)getScrollContainer(contentArea));
	Dimension scroll_size = scrollPane.getSize();	       
	int page_number = scrollPane.getCurrentPage();
	boolean fits=true;
	for (int i=0;i<fp.getNumRectangles();i++)
	    fits = fits && ( (0<=fp.getRectangleAt(i).y) && (scroll_size.height>=fp.getRectangleAt(i).y));
	return fits;
	//return ( (0<fp.getRectangleAt(0).y) && (scroll_size.height>fp.getRectangleAt(0).y) );
    }
 
}
