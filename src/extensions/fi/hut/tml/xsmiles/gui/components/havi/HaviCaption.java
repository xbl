/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.XCaption;



import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;

import java.util.Vector;
import java.util.Enumeration;

import org.ftv.ui.HText;
import org.ftv.ui.HVisible;
import org.ftv.ui.HStaticText;

/**
 * A caption object
 * @author Mikko Honkala
 */
public class HaviCaption extends HaviStylableComponent implements XCaption
{
    /** the caption component */
    protected NonFocusableLabel caption;
    protected static final Dimension labelMinSize = new Dimension(60,20);
    protected Dimension myMinSize = labelMinSize; // for captions, minumun size is 0,0
    // for outputs, the minimum size may be set to something else
    
    protected String textContent;
    
    /** the text listeners of this caption */
    protected Vector textListeners;
    
    public HaviCaption(String text)
    {
        super();
        this.caption=new NonFocusableLabel(text);
        this.content=this.caption;
        this.textContent=text;
    }

    
    public Dimension getMinimumSize()
    {
	this.myMinSize=super.getMinimumSize();
	return myMinSize;
    }
    
    public void setMinimumSize(Dimension min)
    {
	this.myMinSize=min;
    }
    
    public Color getDefaultBackgroundColor()
    {
        return Color.white;
    }
    public boolean isCaption()
    {
        return false;
    }
    
    public String getText()
    {
        return this.textContent;
        //this.caption.getTextContent(HVisible.ALL_STATES);//"HText not implemented";//this.caption.getText();
    }
    
    public void setText(String text)
    {
        //this.caption.setText(text);
        this.textContent=text;
        this.caption.setTextContent(text,HVisible.ALL_STATES);
	
        if (this.textListeners!=null)
        {
            Enumeration e = textListeners.elements();
            while(e.hasMoreElements())
            {
                TextListener l = (TextListener)e.nextElement();
                l.textValueChanged(new TextEvent(text,0));
            }
        }
    }
    
    /** non - focusable JTextField to be used as a caption */
    public class NonFocusableLabel extends HStaticText
    {
        public NonFocusableLabel(String text)
        {
            super(text);
        }
        public boolean isFocusTraversable()
        {
            return false;
        }		
    }
    
    public void addTextListener(TextListener tl)
    {
        if (tl==null) return;
        if (this.textListeners==null) this.textListeners=new Vector();
        else if (this.textListeners.contains(tl)) return;
        this.textListeners.addElement(tl);
    }
    public void removeTextListener(TextListener tl)
    {
        if (tl==null||textListeners==null) return;
        this.textListeners.removeElement(tl);
    } 

    public void setEnabled(boolean value){
	super.setEnabled(value);
	if (caption instanceof Component)
	    ((Component)caption).setVisible(value);
    }
    
}
