/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.gui.components.XText;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.Log;

import java.awt.Component;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.*;
import java.awt.AWTEventMulticaster;

import org.ftv.ui.HVisible;
import org.ftv.ui.HSinglelineEntry;


/**
 * Common base class for text components
 * @author Mikko Honkala
 */

public abstract class HaviTextComponent extends HaviStylableComponent implements XText//,  FocusListener
{
    protected HSinglelineEntry textcomponent; // can be also HMultilineEntry, since that is a subclass
    TextListener textlistener = null;
    
    protected boolean insideChangeEvent = false;
    
    protected String fText;
    
    public HaviTextComponent()
    {
        super();
    }
    
    
    public void setText(String text)
    {
        Log.debug(this.getClass()+" set text: "+text);
        if (!insideChangeEvent)
        {
            this.textcomponent.setTextContent(text,HVisible.ALL_STATES);
            this.fText=text;
        }
    }
    public String getText()
    {
        return this.fText;
    }
    public void addTextListener(TextListener tl)
    {
        textlistener = AWTEventMulticaster.add(textlistener, tl);
    }
    public void removeTextListener(TextListener tl)
    {
        textlistener = AWTEventMulticaster.remove(textlistener, tl);
    }
    
    public Color getDefaultBackgroundColor()
    {
        return Color.white;
    }
    
    // DOCUMENT LISTENER METHODS
/*    public void changedUpdate(DocumentEvent e)
    {
        notifyIncrementalChange(e);
    }
    public void insertUpdate(DocumentEvent e)
    {
        notifyIncrementalChange(e);
    }
    public void removeUpdate(DocumentEvent e)
    {
        notifyIncrementalChange(e);
    }
    
    public void notifyIncrementalChange(DocumentEvent e)
    {
        insideChangeEvent=true;
        try
        {
            // when event occurs which causes "action" semantic
            if (textlistener != null)
            {
                TextEvent te = new TextEvent(this,TextEvent.TEXT_VALUE_CHANGED);
                textlistener.textValueChanged(te);
            }
        } catch (Exception ex)
        {
        }
        finally
        {
            insideChangeEvent=false;
        }
        
    }*/
    public void setEditable(boolean editable)
    {
        this.textcomponent.setEnabled(editable);
    }
    public boolean getEditable()
    {
        return this.textcomponent.isEnabled();
    }
    
    
    
    // FOCUS LISTENER METHODS
    public void focusGained(FocusEvent ae)
    {
    }
    public void focusLost(FocusEvent ae)
    {
    }
    
    public void addFocusListener(FocusListener fl)
    {
        this.textcomponent.addFocusListener(fl);
    }
    
    public void removeFocusListener(FocusListener fl)
    {
        this.textcomponent.removeFocusListener(fl);
    }
    
    
    
}
