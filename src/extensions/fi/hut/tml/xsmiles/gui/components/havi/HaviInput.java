/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.XSecret;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.Container;
import java.awt.event.*;

import org.ftv.ui.HSinglelineEntry;
import org.ftv.ui.HVisible;
import org.ftv.ui.event.HTextListener;
import org.ftv.ui.event.HTextEvent;

import org.w3c.dom.css.CSSStyleDeclaration;



/**
 * textinput line
 * this is extended from SwingComponent, which will add captions and styling etc
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public class HaviInput extends HaviTextComponent implements XInput, XSecret, HTextListener,XTextArea
{
    //private JTextField visualComponent;
    /** the action for enter keypress */
    protected char echoChar = '*';
    protected boolean isSecret = false;
    //final static Dimension minSize = new Dimension(60,20);
    Dimension minSize = new Dimension(60,20);
    
    /**
     * @param c The number of columns initially
     * gui is given for the inputmode functionality
     */
    public HaviInput()
    {
        super();
        init();
    }
    public HaviInput(char passwdchar, GUI gui)
    {
        super();
        this.echoChar = passwdchar;
        this.isSecret = true;
        init();
    }
    
   
    /**
     * in init the subclass always creates its component first and then calls
     * super.init
     */
    public void init()
    {
        this.content = this.createContent();
    }
    
    /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
	this.minSize=super.getMinimumSize();
        return minSize;
    }
    
    public Color getDefaultBackgroundColor()
    {
        return null; // transparent
        //return Color.white;
        //return SwingCSSFormatter.getTransparentColor();
    }

    
    /** creates the content component */
    public Component createContent()
    {
        textcomponent = new HSinglelineEntry("TEST_TEXT IS IN HERE", 100, new Font("Verdana",Font.PLAIN,16),Color.blue);
        textcomponent.setEnabled(true);
	/*
	  if (!isSecret)
	  {
	  textcomponent=new JTextField();
	  }
	  else
	  {
	  textcomponent = new JPasswordField();
	  ((JPasswordField)textcomponent).setEchoChar(echoChar);
	  }*/
        textcomponent.addHTextListener(this);
        return textcomponent;
    }
    
    public String getPassword()
    {
        return this.getText();
    }
    
    /**
     * the default background color for this component
     * null = transparent.
     */
    
    
    
    public void addActionListener(ActionListener al)
    {
        /*
        this.textcomponent.registerKeyboardAction(
        al,
        ENTER_STROKED,
        KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER,0),
        JComponent.WHEN_FOCUSED
        );
         */
        
    }
    /**
     * @param b Active or not active
     */
    public void setEnabled(boolean b)
    {
        // we don't want to change the font color
        //this.content.getStylableComponent().setEnabled(b);
        this.textcomponent.setEnabled(b);
    }
    
    public void removeActionListener(ActionListener al)
    {
        //this.textcomponent.resetKeyboardActions();
    }
    
    public void caretMoved(HTextEvent e)
    {
    }    
    
    public void textChanged(HTextEvent e)
    {
        this.fText=textcomponent.getTextContent(HVisible.NORMAL_STATE);
        Log.debug("Text changed: "+fText);
        TextEvent te = new TextEvent(this,TextEvent.TEXT_VALUE_CHANGED);
        textlistener.textValueChanged(te);
    }    
    
    public void setWordWrapping(boolean wrap)
    {
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTextArea#append(java.lang.String)
     */
    public void append(String arg0)
    {
        // TODO Auto-generated method stub
       String newText=this.getText()+arg0;
       this.setText(newText);
       
        
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTextArea#setCaretPosition(int)
     */
    public void setCaretPosition(int arg0)
    {
        // TODO Auto-generated method stub
        
    }
    
    
}
