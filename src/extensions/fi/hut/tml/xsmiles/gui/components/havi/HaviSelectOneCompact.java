/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Component;
import java.awt.ItemSelectable;
import java.awt.event.*;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.w3c.dom.css.*;

import org.ftv.ui.HListGroup;
import org.ftv.ui.HListElement;
import org.ftv.ui.HVisible;
import org.ftv.ui.event.HItemListener;
import org.ftv.ui.event.HItemEvent;

/**
 * select one compact mode
 * @author Mikko Honkala
 */
public class HaviSelectOneCompact extends HaviSelectOne implements XSelectOne, HItemListener//, ListSelectionListener
{
    
    //final static Dimension minSize = new Dimension(100,100);
    Dimension minSize = new Dimension(100,100);
    /**
     * @param v The vector containing all items, the first item is default
     * @param s The style of the selectOne
     */
    public HaviSelectOneCompact()
    {
        super();
    }
    
    
    public Component createComponent()
    {
        selectComponent=new HListGroup();//;list;
        selectComponent.setMultiSelection(this.multiple);
        selectComponent.addItemListener(this);
        
        return selectComponent;
    }
    protected int getListSelectionModel()
    {
        return -1;
    }
    
    
    /**
     * the default background color for this type of component
     * null = default.
     */
    public Color getDefaultBackgroundColor()
    {
        return Color.white;
    }
    
    /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
	this.minSize=super.getMinimumSize();
        return minSize;
    }
    
    
    public void addSelection(Object o)
    {
        HListElement item = null;
        if (o instanceof String)
        {
            item = new HListElement((String)o);
        }
        if (item!=null) this.selectComponent.addItem(item,HListGroup.ADD_INDEX_END);
        // TEST this.selectComponent.setItemSelected(this.selectComponent.getListContent().length-1,true);
    }
    
    public void removeSelection(Object o)
    {
	this.selectComponent.clearSelection();
        //Log.error(this.getClass()+" removeSelection notimplemented3");
    }
    
    public void removeAll()
    {
        this.selectComponent.removeAllItems();
	//Log.error(this.getClass()+" remove all notimplemented2");
            /*
                  listmodel.removeAllElements();
             */
    }
    
    public void setSelected(Object o)
    {
        Log.error(this.getClass()+"setselected no	timplemented1");
            /*
                  list.setSelectedValue(o,true);
             */
    }
    public void setSelectedIndex(int index)
    {
        Log.debug("SETTING SELECTED INDEX: "+index);
        this.selectComponent.setItemSelected(index,true);
        this.selectComponent.setCurrentItem(index);
    }
    public int getSelectedIndex()
    {
        
        return this.selectComponent.getCurrentIndex();
            /*
                  return list.getSelectedIndex();
             */
    }
    /** the selection events from a list arrive here */
        /*
        public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting())
                return;
         
                Object item = ((JList)selectComponent).getSelectedValue();
                this.notifyChange(item, ItemEvent.ITEM_STATE_CHANGED);
        }
         */
    public Component getContentComponent()
    {
        return this.selectComponent;
    }
    
    public Component getAddableComponent()
    {
        return this.getContentComponent();
    }
    public Component getSizableComponent()
    {
        return this.getContentComponent();
    }
    public Component getStylableComponent()
    {
        return this.getContentComponent();
    }
    
    public void currentItemChanged(org.ftv.ui.event.HItemEvent hItemEvent)
    {
        Log.debug(this.getClass()+".currentItemChanged(): "+hItemEvent+"ID"+hItemEvent.getID()+" item:"+hItemEvent.getItem().getClass()+" :"+hItemEvent.getItem());
        
    }
    
    public void selectionChanged(org.ftv.ui.event.HItemEvent hItemEvent)
    {
        Log.debug(this.getClass()+".selectionChanged(): "+hItemEvent+"ID"+hItemEvent.getID()+" item:"+hItemEvent.getItem().getClass()+" :"+hItemEvent.getItem());
        if (hItemEvent.getID()==HItemEvent.ITEM_SELECTED)
        {
            Log.debug("Item selected");
            Object item = hItemEvent.getItem();
            if (item instanceof HListElement)
            {
                item=((HListElement)item).getLabel();
                Log.debug("Selected item label: "+item);
            }
            this.notifyChange(item, ItemEvent.ITEM_STATE_CHANGED);
        }
        else         if (hItemEvent.getID()==HItemEvent.ITEM_SELECTED)
        {
            Log.debug("Selection cleared");
        }
    }
    /** for some reason PC's Havi's HItemListener extends FocusListener */
/*    
    public void focusGained(FocusEvent e)
    {
    }
    
    public void focusLost(FocusEvent e)
    {
    }
*/    
    
}
