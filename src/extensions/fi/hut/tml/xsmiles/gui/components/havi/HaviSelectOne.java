/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;

import java.awt.Container;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.ItemSelectable;
import java.awt.event.*;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    
import org.w3c.dom.*;

import org.ftv.ui.HListGroup;
import org.ftv.ui.HVisible;

/**
 * textinput line
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public abstract class HaviSelectOne extends HaviSelectBase implements XSelectOne, ItemSelectable
{
    protected boolean multiple=false;
    protected HListGroup selectComponent;
  
    /**
   * @param v The vector containing all items, the first item is default
   * @param s The style of the selectOne
   */
  public HaviSelectOne() 
    {
	  super();
    }
    
  
    
    
    protected void notifyChange(Object item, int status)
    {
      ItemEvent ie=new ItemEvent(this, -1, item, status);
      if (this.itemlistener!=null) this.itemlistener.itemStateChanged(ie);
    }
    
    /** method from itemselectable interface */
    public Object[] getSelectedObjects()
	{
	    return null;
          //Returns the selected items or null if no items are selected.
	}
}
