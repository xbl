/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;
import java.awt.AWTEventMulticaster;

import fi.hut.tml.xsmiles.gui.components.XSelect;
import fi.hut.tml.xsmiles.Log;



/**
 * Common base class for text components
 * @author Mikko Honkala
 */

public abstract class HaviSelectBase extends HaviStylableComponent implements XSelect
{

    ItemListener itemlistener = null;
    public HaviSelectBase()
    {
	super();
	this.content=this.createComponent();
    }
    
    public abstract Component createComponent();
    
    /** add a listener for changes in the range control */
    public void addItemListener(ItemListener l)
    {
	itemlistener = AWTEventMulticaster.add(itemlistener, l);
    }
    /** remove a listener for changes in the range control */
    public void removeItemListener(ItemListener l)
    {
	itemlistener = AWTEventMulticaster.remove(itemlistener, l);
    }
    public void addSelection(Object o)
    {
	Log.error(this+" not supported0");
    }
    
    public void removeSelection(Object o)
    {
	Log.error(this+" not supported1");
    }
    
    public void removeAll()
    {
	Log.error(this+" not supported2");
    }
    
    public void setSelected(Object o) 
    {
	Log.error(this+" not supported3");
    }
    public void setSelectedIndex(int index) 
    {
	Log.error(this+" not supported4");
    }
    public int getSelectedIndex() {
	Log.error(this+" not supported5");
	return -1;
    }	
    
    
}
