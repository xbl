/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.havi;

import java.awt.*;
import java.awt.event.*;
import java.awt.AWTEventMulticaster;

import java.net.URL;

import java.util.Vector;
import java.util.Enumeration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;

import org.ftv.ui.HToggleButton;
import org.ftv.ui.HVisible;
import org.ftv.ui.HContainer;
import org.ftv.ui.event.HActionListener;

/**
 * An implementation of XSelectBoolean = a single checkbox
 * @author Mikko Honkala
 */
public class HaviSelectBoolean extends HaviSelectBase implements XSelectBoolean, HActionListener, ItemSelectable
{
    protected HToggleButton button;
    protected String name;
    
    //protected boolean layoutDone = false;
    
    final static Dimension minSize = new Dimension(50,50);

    /**
     * A plain swing button
     */
    public HaviSelectBoolean()
    {
        super();
    }
    
    
    /** creates the content component */
    public Component createComponent()
    {
	Image normal = loadImage("/home/pcesar/src/xsmiles/src/extensions/fi/hut/tml/xsmiles/mlfc/smil/viewer/havi/images/stopnormal.gif", new HContainer());
        this.button = new HToggleButton(normal);
	Image actioned = loadImage("/home/pcesar/src/xsmiles/src/extensions/fi/hut/tml/xsmiles/mlfc/smil/viewer/havi/images/images/exitnormal.gif",this.button);
	//Image stopimagea = loadImage("/home/pcesar/src/xsmiles/src/extensions/fi/hut/tml/xsmiles/mlfc/smil/viewer/havi/images/exitactioned.gif", this.button);
	//button.setGraphicContent(stopimagen, HVisible.ALL_STATES);
	button.setGraphicContent(normal, HVisible.FOCUSED_STATE);
        button.setGraphicContent(normal,HVisible.NORMAL_STATE);
        button.setGraphicContent(normal,HVisible.DISABLED_STATE);
        button.setGraphicContent(normal,HVisible.DISABLED_FOCUSED_STATE);

        button.setGraphicContent(actioned, HVisible.ACTIONED_STATE);
        button.setGraphicContent(actioned, HVisible.ACTIONED_FOCUSED_STATE);
        button.setGraphicContent(actioned, HVisible.DISABLED_ACTIONED_FOCUSED_STATE);
        button.setGraphicContent(actioned, HVisible.DISABLED_ACTIONED_STATE);

        this.button.addHActionListener(this);
        //this.button.setSize(50,50);
        

        
        return button;
    }
    
    
    /**
     * the default background color for this component
     * null = transparent.
     */
    public Color getDefaultBackgroundColor()
    {
        //return CompatibilityFactory.getCompatibility().createTransparentColor();
        return Color.lightGray;
    }
    public void setCaptionText(String text)
    {
        this.setLabel(text);
    }
    public void setLabel(String t)
    {
        Log.debug("boolean: setting the label: "+t);
        //button.setLabel(t);
    }
    
        /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
        return minSize;
    }
    

    
    public void setSelected(boolean selected)
    {
        Log.debug(this.getClass()+".setSelected("+selected);
        button.setSwitchableState(selected);
    }
    
    public boolean getSelected()
    {
        Log.debug(this.getClass()+".getSelected()="+button.getSwitchableState());
        return button.getSwitchableState();
    }
    
    public Image loadImage(String name, Component comp){
	Image icon;
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	MediaTracker tracker = new MediaTracker(comp);
	icon =  toolkit.getImage(name);
	tracker.addImage(icon,0);
	try{
	    tracker.waitForID(0);
	}
	catch(InterruptedException ie){
	    System.out.println("Problem loading image: "+ie);
	}
	return icon;
    }
    
    

    /**
       public Image loadImage(String s, Component comp) 
       {
       try
       {
       Image image = null;
       URL resURL=this.getClass().getResource(s);
       Toolkit tk = Toolkit.getDefaultToolkit();
       MediaTracker mt = new MediaTracker(comp);
       System.out.println(resURL.getFile());
       // 1st try to find the image in the resources, 2nd from a file
       if (resURL!=null) 
       image=tk.getImage(resURL);
       else 
       image = tk.getImage(s);
       
       //       mt.addImage(image, 1);
       //       try { 
       //       mt.waitForID(1);
       //       } catch (InterruptedException e) {
       //       System.out.println("Error:" + e);
       //       }
       return image;
       } catch (Throwable t)
       {
       t.printStackTrace(System.err);
       return null;
       }
       }
    **/
        
    public void actionPerformed(ActionEvent e)
    {
	Log.debug("action performed: "+e+" mode:"+this.button.getSwitchableState());
	ItemEvent ie=new ItemEvent(this, -1, this.button, -1);
	if (this.itemlistener!=null) this.itemlistener.itemStateChanged(ie);
    }        
    
    public Object[] getSelectedObjects()
    {
	return new Object[] {};
    }        
    
}
