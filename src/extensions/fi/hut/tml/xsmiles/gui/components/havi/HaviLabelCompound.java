/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;

import org.ftv.ui.HContainer;

/**
 * A caption object
 * @author Mikko Honkala
 */
public class HaviLabelCompound implements XLabelCompound, TextListener
{
    /** the caption component */
    protected XCaption caption;
    
    /** the component itself */
    protected XComponent component;
    
    /** the caption-side string */
    protected String captionSide;
    
    /** the top-level container, containing the caption and the content component */
    protected Component container;
    
    /** the axis depends on the caption side CSS attribute */
    protected int axis;
    
    protected boolean layoutDone = false;
    
    protected double currentZoom = 1.0;
    
    /** Specifies that components should be laid out left to right.    */
    public static final int X_AXIS = 0;
    
    /** Specifies that components should be laid out top to bottom.   */
    public static final int Y_AXIS = 1;
    
    
    
    public HaviLabelCompound(XComponent comp, XCaption capt, String captSide)
    {
        super();
        this.caption=capt;
        this.component=comp;
        this.captionSide=captSide;
        this.init();
    }
    public void init()
    {
        // we have few exceptions, where the caption is part of the component
        // button and selectBoolean (checkbox)
        if (this.component instanceof HaviButton)
        {
            this.container=this.component.getComponent();
            String labelText = this.caption.getText();
            ((HaviButton)this.component).setLabel(labelText);
            this.caption.addTextListener(this);
        }
        /*
        else if (this.component instanceof SwingSelectBoolean)
        {
            this.container=(JComponent)this.component.getComponent();
            if (this.caption!=null)
            {
                String labelText = this.caption.getText();
                ((SwingSelectBoolean)this.component).setLabel(labelText);
                this.caption.addTextListener(this);
            }
        }*/
        else
        {
            // layout container
            this.createContainer();
            this.layoutComponent();
        }
    }
    
    /** creates the main container */
    protected void createContainer()
    {
        this.container=new HContainer();
        //this.container=HaviComponentFactory.createContainer();
        //container.setBackground(java.awt.Color.yellow);
        //((HContainer)container).setOpaque(false);
        //((JComponent)this.container).setOpaque(false);
    }
    
    public static final short CAPTION_TOP = 3;
    public static final short CAPTION_BOTTOM = 4;
    public static final short CAPTION_LEFT = 5;
    public static final short CAPTION_RIGHT = 6;
    /**
     * layouts the component using the caption, content and the main container parts of the control
     * for havi, the components must be CSS formatted before this layout!!!!
     */

    protected void layoutComponent()
    {
        // NOTE: for havi, the components must be formatted before this layout!!!!
        if (this.container instanceof Container)
        {
            Container cont = (Container)container;
            
            short cs = CAPTION_LEFT;
            if (captionSide!=null)
            {
                if (captionSide.equalsIgnoreCase("top")||captionSide.equalsIgnoreCase("bottom"))
                {
                    cs = CAPTION_TOP;
                }
            }
            
            if (this.layoutDone)
            {
                cont.removeAll();
            }
            float alignment= Component.LEFT_ALIGNMENT;
            boolean captionFirst=true;
            //axis = BoxLayout.Y_AXIS;

            //((JComponent)this.container).setAlignmentX(alignment);


            // caption-side was left 
            if (cs==CAPTION_LEFT)
            {
                int x=0,y=0;
                if (caption!=null)
                {
                    caption.getComponent().setLocation(x,0);
                    y=caption.getSize().height;
                    x=caption.getSize().width;
		    cont.add(caption.getComponent());
                    
                }
                component.getComponent().setLocation(x,0);
                int heightC=Math.max(y,component.getSize().height);
                int widthC=x+component.getSize().width;
		cont.setSize(widthC,heightC); // TODO: this should really be calculated from the components
		cont.add(component.getComponent());                               
                //cont.setSize(200,50); // TODO: this should really be calculated from the components
                //cont.setSize(100,100);
            } else
            {
                int y=0,x=0;
                if (caption!=null)
                {
                    caption.getComponent().setLocation(0,y);
                    y=caption.getSize().height;
                    x=caption.getSize().width;
		    cont.add(caption.getComponent());
                    
                }
                component.getComponent().setLocation(0,y);
                int heightC=y+component.getSize().height;
                int widthC=Math.max(x,component.getSize().width);
                cont.setSize(widthC,heightC); // TODO: this should really be calculated from the components
		cont.add(component.getComponent());
            }
        }
        // debug:
        //this.component.getComponent().setSize(100,100);
        this.layoutDone=true;
    }
     
/*     
     protected void layoutComponent_swing()
    {
        // JV: moved this to ComponentWCaption
        if (this.layoutDone)
        {
            this.container.removeAll();
        }
        float alignment= Component.LEFT_ALIGNMENT;
        boolean captionFirst=true;
        axis = BoxLayout.Y_AXIS;
        if (this.caption!=null)
        {
            // Determine which side the caption should be put, default is top
            if (captionSide!=null)
            {
                String cs=captionSide;
                if (cs.equals("left"))
                {
                    captionFirst=true;
                    axis=BoxLayout.X_AXIS;
                }
                else if (cs.equals("right"))
                {
                    captionFirst=false;
                    axis=BoxLayout.X_AXIS;
                }
                else if (cs.equals("bottom"))
                {
                    captionFirst=false;
                    axis=BoxLayout.Y_AXIS;
                }
                
            }
        }
        
        BoxLayout box = new BoxLayout(container, axis);
        this.container.setLayout(box);
        ((JComponent)this.container).setAlignmentX(alignment);
        
        
        if (captionFirst)
        {
            // caption-side was left or top
            if (caption!=null)container.add(caption.getAddableComponent());
            container.add(component.getComponent());
        }
        else
        {
            // caption-side was right or bottom
            container.add(component.getComponent());
            if (caption!=null)container.add(caption.getAddableComponent());
        }
        this.layoutDone=true;
    }*/
/*    
    protected String calculateAxis(String cs)
    {
        //default
        String captionPlacement=BorderLayout.NORTH;
        if (cs!=null)
        {
            if (cs.equals("left"))
            {
                captionPlacement=BorderLayout.WEST;
                this.axis=X_AXIS;
            }
            else if (cs.equals("right"))
            {
                captionPlacement=BorderLayout.EAST;
                this.axis=X_AXIS;
            }
            else if (cs.equals("bottom"))
            {
                captionPlacement=BorderLayout.SOUTH;
                this.axis=Y_AXIS;
            }
            else if (cs.equals("top"))
            {
                captionPlacement=BorderLayout.NORTH;
                this.axis=Y_AXIS;
            }
        }
        return captionPlacement;
    }
 */
    /**
     * Set's this components zoom (1.0 is the default).
     */
    public void setZoom(double zoom)
    {
        if (component!=null) this.component.setZoom(zoom);
        if (caption!=null) ((XComponent)this.caption).setZoom(zoom);
        //this.sizeComponent(zoom);
    }
    
    
    public Component getComponent()
    {
        return this.container;
    }
    public Dimension getSize()
    {
        if (this.container.getSize().height>0)
            return this.container.getSize();
        if (this.container.getPreferredSize().height>0)
            return this.container.getPreferredSize();
        return new Dimension(0,0);
    }
    
    /**
     * Set this drawing area visible.
     * @param v    true=visible, false=invisible
     */
    public void setVisible(boolean v)
    {
        this.container.setVisible(v);
    }
    
    /**
     * @param c The component to be added
     */
    public void add(XComponent c)
    {
        if (this.container instanceof Container)
        {
            ((Container)this.container).add(c.getComponent());
        }
    }
    
    /**
     * @param c Remove component c
     */
    public void remove(XComponent c)
    {
        if (this.container instanceof Container)
        {
            ((Container)this.container).remove(c.getComponent());
        }
    }
    
    /**
     * Remove all components from conatiner.
     */
    public void removeAll()
    {
        if (this.container instanceof Container)
        {
            ((Container)this.container).removeAll();
        }
    }
    
    /**
     * whether this is layed out north-south or east-west
     */
    int getAxis()
    {
        return this.axis;
    }
    
    /**
     * Invoked when the value of the text has changed.
     * The code written for this method performs the operations
     * that need to occur when text changes.
     */
    public void textValueChanged(TextEvent e)
    {
        String newCaption = this.caption.getText();
        // we have few exceptions, where the caption is part of the component
        // button and selectBoolean (checkbox)
        if (this.component instanceof HaviButton)
        {
            ((HaviButton)this.component).setLabel(newCaption);
        }
        /*
        else if (this.component instanceof SwingSelectBoolean)
        {
            ((SwingSelectBoolean)this.component).setLabel(newCaption);
        }*/
        // otherwise the caption has already changed
    }
    
    
    
    
}
