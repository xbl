/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.havi;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import java.net.URL;

import org.ftv.ui.HState;
import org.ftv.ui.HGraphicButton;
import org.ftv.ui.HTextButton;
import org.ftv.ui.HState;
import org.ftv.ui.HVisible;
import org.ftv.ui.event.HActionListener;
import org.ftv.ui.HActionable;

/**
 * Similar functionality as normal AWT button.
 * Features:
 * <ul>
 * <li>lightweight</li>
 * <li>three different states (focus, non-focus, pressed), each with graphical representations.</li>
 * <li>not all button functionality is included.</li>
 * </ul>
 * @author Mikko Honkala
 * @author Juha Vierine
 */

/** Note: pcesar
 * in Havi there are several kinds of buttons depending on the content
 * they can be Text or Graphic Buttons
 * For that reason a Graphic button is the first option
 * in case there is no image, a text button is created
 **/

public class HaviButton extends HaviStylableComponent implements XButton,HActionListener
{
    protected HActionable button;
    protected String name;
    protected Image icon;
    protected boolean layoutDone = false;
    
    protected Hashtable actionListenerList;
    /**
     * A plain swing button
     */
    public HaviButton(String iconUrl)
    {
        this(null,iconUrl);
    }
    
    /**
     * A iconed swing button
     */
    public HaviButton(String a_name, String iconUrl)
    {
        name=a_name;
	if (iconUrl!=null){
	    HTextButton button_fake = new HTextButton("I AM clompletely faked");//,icon);
	    Toolkit toolkit = Toolkit.getDefaultToolkit();
	    MediaTracker tracker = new MediaTracker(button_fake);
	    String file = new String(System.getProperty("user.dir") +"/../bin/"+iconUrl);	
	    System.out.println("Trying to load:"+file);
	    icon =  toolkit.getImage(file);
	    tracker.addImage(icon,0);
	    try{
		tracker.waitForID(0);
	    }
	    catch(InterruptedException ie){
		System.out.println("Problem loading image: "+ie);
	    }
	}
	init();
    }
    
    
    public void init()
    {
        this.content = this.createContent();
        //this.container = (Container)this.content.getAddableComponent();
    }

    /** creates the content component */
    private static Insets insets = new Insets(2,4,2,4);
    public Component createContent()
    {
	if (icon!=null){	
	    this.button = new HGraphicButton(icon);
	    ((HGraphicButton)this.button).setBackground(Color.lightGray);
	    ((HGraphicButton)this.button).setEnabled(true);
	    this.button.setActionCommand("button");
	    return ((HGraphicButton)this.button);
	}
	else{
	    this.button = new HTextButton(name);//,icon);
	    ((HTextButton)this.button).setBackground(Color.lightGray);
	    ((HTextButton)this.button).setEnabled(true);
	    this.button.setActionCommand("button");
	    return ((HTextButton)this.button);
	}
    }
    
    
    public Component getComponent()
    {
	if (this.button instanceof HGraphicButton)
	    return ((HGraphicButton)this.button);
	if (this.button instanceof HTextButton)
	    return ((HTextButton)this.button);
	else
	    return null;
    }
    
    /**
     * the default background color for this component
     * null = default.
     */
    public Color getDefaultBackgroundColor()
    {
        //return null;
        return Color.lightGray;
    }
    
    
    public void setCaptionText(String text)
    {
        this.setLabel(text);
    }
    
    public void setLabel(String t)
    {
	if (this.button instanceof HTextButton)
	    ((HTextButton)this.button).setTextContent(t,HVisible.ALL_STATES);
    }
    
    public void addActionListener(ActionListener al)
    {
        HaviActionListenerWrapper wr = new HaviActionListenerWrapper(al);
        if (actionListenerList==null) this.actionListenerList=new Hashtable();
        this.actionListenerList.put(al,wr);
        button.addHActionListener(wr);
    }
    
    public void removeActionListener(ActionListener al)
    {
        if (this.actionListenerList==null) return;
        HaviActionListenerWrapper wr = (HaviActionListenerWrapper)this.actionListenerList.get(al);
        if (wr==null) return;
        else
        {
            button.removeHActionListener(wr);
            this.actionListenerList.remove(al);
        }
        
    }
    
    public void setImage(String fn)
    {
        //button.setIcon(new ImageIcon(fn));
    }
    
    public void setImagePressed(String fn)
    {
        Log.debug("Not implemented");
    }
    
    public void setImageRollOver(String fn)
    {
        Log.debug("Not implemented");
    }
    
    
    public void setImageDisabled(String fn)
    {
        Log.debug("Not implemented");
    }
    
    public void setActionCommand(String ac)
    {
        button.setActionCommand(ac);
    }
    
    public void actionPerformed(ActionEvent e)
    {
    }


    public void addKeyListener(KeyListener kl)
    {
	if (button instanceof Component)
	    ((Component)button).addKeyListener(kl);
    }
    
    public class HaviActionListenerWrapper implements HActionListener
    {
        protected ActionListener al;
        public HaviActionListenerWrapper(ActionListener l)
        {
            al=l;
        }
        
        public void actionPerformed(ActionEvent e)
        {
            Log.debug("action performed: "+e);
            al.actionPerformed(e);
        }
        
    }

    public void setEnabled(boolean value){
	super.setEnabled(value);
	if (button instanceof Component)
	    ((Component)button).setVisible(value);
    }
    
}
