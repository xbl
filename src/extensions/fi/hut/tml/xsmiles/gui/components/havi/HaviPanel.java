/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.havi;

import java.awt.Component;
import java.awt.Panel;

import org.ftv.ui.HContainer;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XPanel;


/**
 * Class AWTPanel
 * 
 * @author tjjalava
 * @since Oct 22, 2004
 * @version $Revision: 1.3 $, $Date: 2005/10/18 08:58:31 $
 */
public class HaviPanel extends HContainer implements XPanel {
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#add(fi.hut.tml.xsmiles.gui.components.XComponent)
     */
    public void add(XComponent c) {
        add(c.getComponent());
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#remove(fi.hut.tml.xsmiles.gui.components.XComponent)
     */

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#remove(fi.hut.tml.xsmiles.gui.components.XComponent)
     */
    public  void remove(XComponent c) {
        remove(c.getComponent());
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#getComponent()
     */
    public Component getComponent() {
        return this;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#removeComp(java.awt.Component)
     */
    public void removeComp(Component component)
    {
        // TODO Auto-generated method stub
        this.remove(component);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#addComp(java.awt.Component)
     */
    public void addComp(Component component)
    {
        // TODO Auto-generated method stub
        this.add(component);
    }
}
