/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 5, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;

import java.awt.Dimension;

import javax.swing.JFrame;


/**
 * @author honkkis
 *
 */
public class SpeechDialog extends JFrame
{
    public SpeechDialog()
    {
        init();
    }
    public void init()
    {
        this.setSize(new Dimension(800,600));
        
    }

}
