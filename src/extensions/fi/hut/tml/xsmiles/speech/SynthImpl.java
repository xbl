/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 1, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;

import java.beans.PropertyVetoException;
import java.io.File;
import java.util.Locale;

import javax.speech.Central;
import javax.speech.EngineException;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import javax.speech.synthesis.SynthesizerProperties;
import javax.speech.synthesis.Voice;

import fi.hut.tml.xsmiles.Log;


/**
 * @author honkkis
 *
 */
public class SynthImpl implements Synth
{
    
    public SynthImpl()
    {
        this.init();
    }
    Synthesizer synthesizer;
    public void init()
    {

        String voiceName = "kevin16";
        try {
            /* Find a synthesizer that has the general domain voice
             * we are looking for.  NOTE:  this uses the Central class
             * of JSAPI to find a Synthesizer.  The Central class
             * expects to find a speech.properties file in user.home
             * or java.home/lib.
             *
             * If your situation doesn't allow you to set up a
             * speech.properties file, you can circumvent the Central
             * class and do a very non-JSAPI thing by talking to
             * FreeTTSEngineCentral directly.  See the WebStartClock
             * demo for an example of how to do this.
             */
            SynthesizerModeDesc desc = new SynthesizerModeDesc(
                null,          // engine name
                "general",     // mode name
                Locale.US,     // locale
                null,          // running
                null);         // voice
            synthesizer = Central.createSynthesizer(desc);

            /* Just an informational message to guide users that didn't
             * set up their speech.properties file.
             */
            if (synthesizer == null) {
                String message = "\nCan't find synthesizer.\n"
                    + "Make sure that there is a \"speech.properties\" file "
                    + "at either of these locations: \n";
                message += "user.home    : "
                    + System.getProperty("user.home") + "\n";
                message += "java.home/lib: " + System.getProperty("java.home")
                    + File.separator + "lib\n";

                System.err.println(message);
            }

            /* Get the synthesizer ready to speak
             */
            synthesizer.allocate();
            synthesizer.resume();

            /* Choose the voice.
             */
            desc = (SynthesizerModeDesc) synthesizer.getEngineModeDesc();
            Voice[] voices = desc.getVoices();
            Voice voice = null;
            for (int i = 0; i < voices.length; i++) {
                if (voices[i].getName().equals(voiceName)) {
                    voice = voices[i];
                    break;
                }
            }
            if (voice == null) {
                System.err.println(
                    "Synthesizer does not have a voice named "
                    + voiceName + ".");
                System.exit(1);
            }
            synthesizer.getSynthesizerProperties().setVoice(voice);
            this.setPitch(110.0f);
            this.setSpeakingRate(140.0f);


            /* Clean up and leave.
             */
            //synthesizer.deallocate();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    
    /**
     * Returns the speaking rate.
     *
     * @return the speaking rate, or -1 if unknown or an error occurred
     */
    public float getSpeakingRate() {
	if (synthesizer != null) {
	    return synthesizer.getSynthesizerProperties().getSpeakingRate();
	} else {
	    return -1;
	}
    }
	

    /**
     * Sets the speaking rate in terms of words per minute.
     *
     * @param wordsPerMin the new speaking rate
     *
     * @return the speaking rate, or -1 if unknown or an error occurred
     */
    public boolean setSpeakingRate(float wordsPerMin) {
	float oldSpeed = getSpeakingRate();
	SynthesizerProperties properties =
	    synthesizer.getSynthesizerProperties();
	try {
	    properties.setSpeakingRate(wordsPerMin);
	    return true;
	} catch (PropertyVetoException pve) {
	    try {
		properties.setSpeakingRate(oldSpeed);
	    } catch (PropertyVetoException pe) {
		pe.printStackTrace();
	    }
	    return false;
	}
    }	
    /**
     * Returns the baseline pitch for the current synthesis voice.
     *
     * @return the baseline pitch for the current synthesis voice
     */
    public float getPitch() {
	return synthesizer.getSynthesizerProperties().getPitch();
    }	
    
    public boolean setPitch(float pitch) {
    	float oldPitch = getPitch();
    	try {
    	    synthesizer.getSynthesizerProperties().setPitch(pitch);
    	    return true;
    	} catch (PropertyVetoException pve) {
    	    try {
    		synthesizer.getSynthesizerProperties().setPitch(oldPitch);
    	    } catch (PropertyVetoException pe) {
    		pe.printStackTrace();
    	    }
    	    return false;
    	}
        }

    
    
    public void speak(String s,boolean wait)
    {
        /* The the synthesizer to speak and wait for it to
         * complete.
         */
        try
        {
            synthesizer.resume();
            Log.debug("SynthImpl speaking: "+s);
	        synthesizer.speakPlainText(s, null);
	        if (wait) this.waitEngine();
	        
        } catch (Exception e)
        {
            Log.error(e);
        }
    }




    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.Synth#cancelAll()
     */
    public void cancelAll()
    {
        try
        {
	        this.synthesizer.cancelAll();
	        synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
        } catch (Exception e)
        {
            Log.error(e);
        }
	        
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.Synth#waitEngine()
     */
    public void waitEngine()
    {
        try
        {
            synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
        } catch (InterruptedException e)
        {
            Log.error(e);
        }
        
    }


    /* (non-Javadoc)



    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.Synth#destroy()
     */
    public void destroy()
    {
        try
        {
            this.synthesizer.deallocate();
            SpeechFactory.destroyed(this);
        } catch (EngineException e)
        {
            Log.error(e);
        }
        
    }
}
