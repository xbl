/*
 /* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 2, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;

import java.io.StringReader;
import java.util.Timer;

import fi.hut.tml.xsmiles.Log;

/**
 * @author honkkis
 *  
 */
public class SpeechTest implements XRecognizerListener
{

    public static String testGrammar = "#JSGF V1.0;\n\ngrammar hello;\npublic <greet> = (Good morning | Hello) ( Bhiksha | Evandro | Paul | Philip | Rita | Will );\n";

    public static void main(String[] args)
    {
        new SpeechTest();
    }

    SynthImpl s;

    RecognizerImpl r;

    public SpeechTest()
    {
        try
        {
            /*
             * Thread t = new Thread() { public void run() { try { } catch
             * (Exception e) { Log.error(e); }
             *  } }; t.start();
             */
            r = new RecognizerImpl();
            //s = new SynthImpl();
            r.addXResultListener(this);
            //s.speak("Welcome to X-Smiles! Say a number:",true);
            r.getOneResultSync();
            r.setGrammar(new StringReader(testGrammar));
            r.getOneResultSync();

            //s.speak("Welcome to X-Smiles! Say a number:",true);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.speech.XResultListener#resultEvent(fi.hut.tml.xsmiles.speech.XResultEvent)
     */
    public void resultEvent(XResultEvent ev)
    {
        // TODO Auto-generated method stub
        Log.debug(ev.result);
        try
        {
            Thread.sleep(2000);
        } catch (InterruptedException e)
        {
            Log.error(e);
        }
        if (ev != null && s != null)
                s.speak("I think you said: " + ev.result, true);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.XRecognizerListener#statusChange(short, java.lang.String)
     */
    public void statusChange(short statusCode, String statusMessage)
    {
        // TODO Auto-generated method stub
        
    }
}