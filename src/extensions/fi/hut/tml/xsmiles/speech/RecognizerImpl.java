/*
 /* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 2, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import javax.speech.Central;
import javax.speech.EngineModeDesc;

import javax.speech.recognition.ResultAdapter;
import javax.speech.recognition.ResultEvent;
import javax.speech.recognition.ResultToken;
import javax.speech.recognition.RuleGrammar;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.jsapi.SphinxRecognizer;
import edu.cmu.sphinx.linguist.Linguist;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Resources;
import fi.hut.tml.xsmiles.mlfc.general.Helper;

/**
 * @author honkkis
 *  
 */
public class RecognizerImpl extends ResultAdapter implements Recognizer
{

    static edu.cmu.sphinx.recognizer.Recognizer rec;

    static Microphone microphone;
    private Vector resultlisteners;
    ConfigurationManager cm;

    public String testGrammarStr = "grammar javax.speech.demo;\n\npublic <sentence> = browser home | browser reload | form close";

    public RecognizerImpl()
    {
        this.init();
    }

    public void setGrammar(Reader r) throws Exception
    {
        Log.debug("Switching Grammar.");
        String newGrammarName = "tempgrammar";
        // TODO: GET THIS DYNAMICALLY
        //File dir = new File("/home/honkkis/workspace/xsmiles/build/fi/hut/tml/xsmiles/speech/");
        File dir = new File(".");
        File f = new File(dir,newGrammarName + ".gram");
        FileWriter wr = new FileWriter(f);
        Helper.copyStream(r, wr, 1000);
        wr.close();

        Helper.copyStream(new FileInputStream(f), System.out, 1000);
        System.out.println("Swapping to grammar " + newGrammarName);
        Linguist linguist = (Linguist) cm.lookup("flatLinguist");
        linguist.deallocate();
        cm.setProperty("jsgfGrammar", "grammarName", newGrammarName);
        linguist.allocate();
        Log.debug("Grammar swapped.");
    }



    protected synchronized void init()
    {
        if (rec == null)
        {
            try
            {
                // Create a recognizer that supports English.
                /*
                 * rec = Central.createRecognizer( new
                 * EngineModeDesc(Locale.ENGLISH)); if (rec==null) rec=new
                 * SphinxRecognizer();
                 */
                URL url;
                url = RecognizerImpl.class.getResource("xsmiles.sphinxconfig.xml");

                if (url == null)
                        url = Resources.getResourceURL("xsmiles.sphinx.config");
                Log.debug("URL: " + url);
                cm = new ConfigurationManager(url); // Start
                // up
                // the
                // recognizer
                rec = (edu.cmu.sphinx.recognizer.Recognizer) cm
                        .lookup("recognizer");
                microphone = (Microphone) cm.lookup("microphone");

                /* allocate the resource necessary for the recognizer */
                rec.allocate();

                status = STATUS_PAUSED;

                //rec.allocate();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    protected final int STATUS_NOT_INITIALIZED = 3;

    protected final int STATUS_PAUSED = 5;

    protected final int STATUS_STARTED = 7;

    protected boolean should_stop = false;

    protected int status = STATUS_NOT_INITIALIZED;

    Thread recThread = null;

    public void start()
    {
        recThread = new Thread()
        {

            public void run()
            {
                Log.debug("Recognizer thread started, waiting for result.");
                    getOneResultSync();
            }
        };

        recThread.start();
        status = STATUS_STARTED;
    }

    public Vector listeners = new Vector();

    public void addXResultListener(XRecognizerListener l)
    {
        if (!this.listeners.contains(l)) this.listeners.addElement(l);
    }

    public void removeXResultListener(XRecognizerListener l)
    {
        this.listeners.remove(l);
    }

    public void stop()
    {
        should_stop = true;
        Log.debug("Trying to stop recognizer thread by stopping microphone.");
        /*
        if (recThread != null&&recThread.isAlive())
        {
            try
            {
                recThread.join(500);
            } catch (InterruptedException e)
            {
                Log.error(e);
            }
        }
        */
        /*
        if (recThread!=null && recThread.isAlive())
        {
            recThread.interrupt();
        }
        recThread = null;
        */
        if (microphone!=null&&microphone.isRecording())
        {
            microphone.stopRecording();
        }
        this.postStatusChange(XRecognizerListener.statusIdle,"Idle.");
        status = STATUS_PAUSED;
    }

    /**
     * TODO: return a result object
     * 
     * @return
     */
    protected synchronized Result recognize()
    {
        try
        {
            microphone.clear();
            if (microphone.startRecording())
            {
                Log.info("Microphone started.");

                System.out
                        .println("Say any digit(s): e.g. \"two oh oh four\", "
                                + "\"three six five\".");
                System.out.println("Start speaking. Press Ctrl-C to quit.\n");

                /*
                 * This method will return when the end of speech is reached.
                 * Note that the endpointer will determine the end of speech.
                 */
                Result result=null;
                try
                {
                    this.postStatusChange(XRecognizerListener.statusRecognizing,"Recognizing.");
                    result = rec.recognize();
                } catch (Throwable e)
                {
                    Log.error(e,"Recognizer thread was interrupted");
                }
                microphone.clear();
                microphone.stopRecording();
                microphone.clear();
                Log.info("Microphone stopped.");
                this.postStatusChange(XRecognizerListener.statusIdle,"Idle.");

                if (result != null)
                {
                    String resultText = result.getBestResultNoFiller();
                    System.out.println("You said: " + resultText + "\n");
                    return result;
                } else
                {
                    System.out.println("I can't hear what you said.\n");
                }
            }

            else

            {
                System.out.println("Cannot start microphone.");
                //rec.deallocate();
            }
            // Load the grammar from a file, and enable it
            /*
             * Reader reader = new StringReader(this.testGrammarStr);
             * RuleGrammar gram = rec.loadJSGF(reader); gram.setEnabled(true); //
             * Add the listener to get results rec.addResultListener(this); //
             * Commit the grammar rec.commitChanges(); // Request focus and
             * start listening rec.requestFocus(); rec.resume();
             */
        } catch (Exception e)
        {
            Log.error(e);
        }
        return null;
    }

    public Vector getLastResult()
    {
        return lastResult;
    }

    Vector lastResult = null;

    // Receives RESULT_ACCEPTED event: print it, clean up, exit
    /*
     * public void resultAccepted(ResultEvent e) { Result r =
     * (Result)(e.getSource()); ResultToken tokens[] = r.getBestTokens();
     * lastResult=new Vector(); for (int i = 0; i < tokens.length; i++) {
     * lastResult.addElement(tokens[i].getSpokenText());
     * System.out.print(tokens[i].getSpokenText() + " "); }
     * System.out.println(); // Deallocate the recognizer and exit
     * //rec.deallocate(); //System.exit(0); }
     */

    /**
     *  
     */
    public String getOneResultSync()
    {
        // TODO Auto-generated method stub
        Result result = recognize();
        if (result != null)
        {
            String resText = result.getBestResultNoFiller();
            XResultEvent resEv = new XResultEvent(resText);
            Enumeration e = listeners.elements();
            while (e.hasMoreElements())
            {
                XRecognizerListener l = (XRecognizerListener) e.nextElement();
                l.resultEvent(resEv);
            }
            return resText;
        }
        return null;
    }
    private void postStatusChange(short code, String text)
    {
        Enumeration e = listeners.elements();
        while (e.hasMoreElements())
        {
            XRecognizerListener l = (XRecognizerListener) e.nextElement();
            l.statusChange(code,text);
        }
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.Recognizer#getOneResultAsync()
     */
    public void getOneResultAsync()
    {
        this.start();
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.Recognizer#stopRecognizing()
     */
    public void stopRecognizing()
    {
        this.stop();
    }


}