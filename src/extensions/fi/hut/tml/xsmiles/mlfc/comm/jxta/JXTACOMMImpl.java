/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.comm.jxta;

// MLFC related imports
import  fi.hut.tml.xsmiles.Log;
import  fi.hut.tml.xsmiles.mlfc.comm.COMMInterface;
import  fi.hut.tml.xsmiles.mlfc.comm.DataListener;
import java.net.URL;

// JXTA related imports
import net.jxta.document.Advertisement;
import net.jxta.document.AdvertisementFactory;
import net.jxta.document.MimeMediaType;

import net.jxta.pipe.PipeService;
import net.jxta.pipe.InputPipe;
import net.jxta.pipe.OutputPipe;
import net.jxta.pipe.PipeMsgListener;
import net.jxta.pipe.PipeMsgEvent;

import net.jxta.id.IDFactory;

import net.jxta.peergroup.PeerGroup;
import net.jxta.peergroup.PeerGroupFactory;

import net.jxta.endpoint.Message;
import net.jxta.endpoint.MessageElement;

import net.jxta.discovery.DiscoveryService;
import net.jxta.discovery.DiscoveryListener;
import net.jxta.discovery.DiscoveryEvent;

import net.jxta.exception.PeerGroupException;

import net.jxta.protocol.PeerGroupAdvertisement;
import net.jxta.protocol.PipeAdvertisement;
import net.jxta.protocol.DiscoveryResponseMsg;

import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Enumeration;
import java.util.Date;
import java.util.Vector;

import java.net.InetAddress;
import java.net.UnknownHostException;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;

// This bit was downloaded from http://lpdwww.epfl.ch/sbaehni/work/jxta/wireChat/pages/wireChat.html
/**
 * This class implements a simple chat application. 
 * <p>
 * It's very inspired by the <a href="http://jxta-wire.jxta.org/hotwire.html" target="_parent">hotwire</a> application developped by Eric Pouyoul.
 * Thanks to him for the advices and also for providing the source code of hotwire. 
 * A particular big thanks to William R. Bauer who spends time to reading the code of this application
 * and who gives some useful advices, he spends also a lot of time trying it, thanks to him !!!
 * <p>
 * Here is how it works:
 * First we try to find an advertisement for a chat group.
 * If we find it, we create the pipes in order to talk with the member
 * of the group.
 * If we don't find it, we create a new propagate Pipe Advertisement and we publish it.
 * It's possible that two chat groups may be on the same network.
 * To avoid that, you must raise the sleeping time.
 * @author Sebastien Baehni (in collaboration with William R. Bauer and Eric Pouyoul)
 */
public class JXTACOMMImpl implements COMMInterface, PipeMsgListener, DiscoveryListener {

	public static final String MIME_TEXT = "text/plain"	;
	/******************************************************
	 * MLFC SPECIFIC METHODS
	 ******************************************************/

	Vector dataListeners = null;

	PipeService sendpipe = null;

	// Counter - how many JXTA elements are using JXTA?
	private static int jxtaElements = 0;

	// A flag to keep track if a group was joined or created.
	int joinstatus = JOIN_ERROR;

	/**
	* Send data to target
	* @return True if successful false if not
	*/
	public boolean send(String target, String data) {
		try {	    
			if (this.inputPipe != null) {
				//Log.debug("Waiting for new message from the pipe msg listener.");				
			}
			else {
				Log.error("JXTA: Sorry but you are not able to send messages. Maybe restart the application.");
				return false;
			}

			// We only looks for the user's entries if the output pipe is not null.
			// Otherwise, we will not been able to send messages.
			if (this.outputPipe != null) {	  
				Hashtable myHashtable = new Hashtable();

				// MLFC: Get sendpipe only once
				if (sendpipe == null)
					sendpipe = this.mainGroup.getPipeService();
				// We send the data.
				if (data != null) {			
					// The message we are going to send is composed by the name of the user (the name of the localmachine)
					// and the message he has typed.
					myHashtable.put("sender", this.mainGroup.getPeerName());
					myHashtable.put("message", data);

					// Creation of the message.		
					Message message = this.createMessage(sendpipe, myHashtable);

					// We send the message.
					this.outputPipe.send(message);		    
					Log.debug("JXTA has sent: "+message);		    
				}
			}
			else {
				Log.error("JXTA: Sorry, you can't send any messages but only receive them. Try to restart the application in order to be able to send messages.");
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			Log.error("JXTA: Something bad happened. We stop the application.");
			System.exit(-1);
		}
		return true;
	}

	/**
	* Send data to target
	* @return True if successful false if not
	*/
	public boolean sendMime(String target, byte[] data,String mimetype) {
		try {	    
			if (this.inputPipe != null) {
				//Log.debug("Waiting for new message from the pipe msg listener.");				
			}
			else {
				Log.error("JXTA: Sorry but you are not able to send messages. Maybe restart the application.");
				return false;
			}

			// We only looks for the user's entries if the output pipe is not null.
			// Otherwise, we will not been able to send messages.
			if (this.outputPipe != null) {	  
				Hashtable myHashtable = new Hashtable();

				// MLFC: Get sendpipe only once
				if (sendpipe == null)
					sendpipe = this.mainGroup.getPipeService();
				// We send the data.
				if (data != null) {			
					// The message we are going to send is composed by the name of the user (the name of the localmachine)
					// and the message he has typed.
					String sender = this.mainGroup.getPeerName();

					// Creation of the message.		
					// We create the message.
					Message message = sendpipe.createMessage();
					// add the username
					MessageElement messageElement = message.newMessageElement("user", new MimeMediaType(MIME_TEXT), new ByteArrayInputStream(sender.getBytes()));             
					message.addElement(messageElement);
					messageElement = message.newMessageElement("message", new MimeMediaType(mimetype), data);             
					message.addElement(messageElement);

					// We send the message.
					this.outputPipe.send(message);		    
					Log.debug("JXTA has sent: "+message);		    
				}
			}
			else {
				Log.error("JXTA: Sorry, you can't send any messages but only receive them. Try to restart the application in order to be able to send messages.");
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			Log.error("JXTA: Something bad happened. We stop the application.");
			System.exit(-1);
		}
		return true;
	}
	public void addDataListener(DataListener dl) {
		dataListeners.add(dl);
	}

	/**
	* @param target Send a file to a target
	* @param data The URL where file is located
	* @return true if successful
	*/
	public boolean sendFile(String target, URL data) {
		return true;
	}

	/**
	* @param location Where to save the file
	* @param name The filename to get
	* @return true if successful
	*/
	public boolean getFile(URL location, String name) {
		return true;
	}

	/**
	 * Get local Peer name.
	 * @return 	Local peer name
	 */
	public String getLocalPeerName() {
		if (mainGroup == null)
			return null;
			
		return this.mainGroup.getPeerName();
	}

	/**
	 * Get PeerGroup name.
	 * @return 	Local peer name
	 */
	public String getPeerGroupName() {
		if (mainGroup == null)
			return null;
			
		return this.mainGroup.getPeerGroupName();
	}

	/**
	* This actually starts up the JXTA network and join a "group"
	* A group is service named: "XSmiles: "+groupname
	* @param params The parameters that might be needed
	* @param name The group to join
	* @return JOIN_ERROR, JOIN_GROUPCREATED or JOIN_GROUPJOINED
	*/
	public int join(String name, String params) {
		int status = JOIN_ERROR;

		// Add counter
		jxtaElements ++;
		
		// Save group name
		groupName = name;
	
		try {
			status = this.initJXTAChat();
		}
		catch (Exception e) {
			Log.error("JXTA: Sorry an exception occurs. Try to start the application again.");
			e.printStackTrace();
			return JOIN_ERROR;
		}
		return status;
	}

	/**
	* @param name Leave a group
	*/
	public void leave(String name) {
		Log.debug("Leaving the group - destroy() called.");

		if (inputPipe != null)
			inputPipe.close();
		inputPipe = null;

		if (outputPipe != null)
			outputPipe.close();
		outputPipe = null;

		if (discovery != null) {
//			try {
//				discovery.flushAdvertisements(null, DiscoveryService.ADV);
//			} catch (IOException exp) {
//			}
			// Listener was added in getRemoteAdvertisements()
			discovery.removeDiscoveryListener(this);
		}
		discovery = null;
		sendpipe = null;
		nogroup = true;
	}

	/**
	* Do cleanup - is this enough?
	*/
	public void destroy() {
		// Stop rendezvous
		if (mainGroup != null)
			this.mainGroup.getRendezVousService().stopRendezVous();

		sendpipe = null;

//		mainGroup = null;

		if (inputPipe != null)
			inputPipe.close();
		inputPipe = null;

		if (outputPipe != null)
			outputPipe.close();
		outputPipe = null;

		if (discovery != null) {
			try {
				Log.debug("JXTA Flushing Advertisements");
				discovery.flushAdvertisements(null, DiscoveryService.ADV);
			} catch (IOException exp) {
			}
			// Listener was added in getRemoteAdvertisements()
			discovery.removeDiscoveryListener(this);
		}
		discovery = null;
	}

	/******************************************************
	 * JXTA SPECIFIC METHODS
	 ******************************************************/


	/**
	* The group that is used to start the application.
	* This group is the one that is passed in the init method.
	* It is used to get the group ID in order to create
	* the advertisement.
	* MLFCmod: modified to be static
	*/
	static private PeerGroup mainGroup;

	/**
	* The input pipe, used to get messages from the 
	* members of the chat.
	*/
	private InputPipe inputPipe;

	/**
	* The output pipe, used to send messages to the 
	* members of the chat.
	*/
	private OutputPipe outputPipe;  

	/**
	* The time we must wait in opening the output pipe.
	* If the endpoint of the pipe is "bad" (down) and 
	* we don't specify a time (-1 option), then the creation
	* of the pipe will block forever.
	*/
	private final int TIME_TO_WAIT = 1000;

	/**
	* The time we wait between sending the 
	* request to find the advertisements and
	* retrieving them. In fact, it's the time we wait
	* between the getRemoteAdvertisment method and
	* the getLocalAdvertisement one.
	* If you can't find any advertisement and you are sure
	* there is one, then raise that time.
	*/
	private final int SLEEPING_TIME = 20*1000;

	/**
	* Suffix for describing a chat advertisement.
	* Very useful when we are trying to find new advertisements, we only take the one that
	* have this suffix.
	*/
	private final String PREFIX ="X-Smiles: ";   

	// Group name, which is appended to the end of PREFIX
	private String groupName = null;

	/**
	* The discovery service used to discover and publish new advertisements
	*/
	private DiscoveryService discovery;

	/**
	* Boolean to find new advertisements
	*/
	private boolean nogroup = true;

	/**
	* Main constructor.
	* Initialize the jxta platform.
	* Inspired by the exemple 2 of the tutorial section of http://platform.jxta.org
	*/
	public JXTACOMMImpl() {

		dataListeners = new Vector();
	}

	/**
	* Method used to init the application.
	* This method tries to discover a chat advertisement. 
	* If no chat advertisement is discovered then it creates and 
	* publishes a new one.   
	* @return 	JOIN_ERROR if a fatal error occurred, JOIN_GROUPJOINED if the group was successfully joined,
	*			JOIN_HROUPCREATED if the group wasn't found and a new group was created.
	* @throws PeerGroupException If a peer group exception occurs.
	*/
	public int initJXTAChat() throws Exception {
		Log.debug("Starting the JXTA application.");		

		try {
			// let's start the jxta platform
			// MLFCmod: Do it only once per browser (this is static)
			if (this.mainGroup == null)
				this.mainGroup = PeerGroupFactory.newNetPeerGroup();

		} catch ( PeerGroupException e) {
			// could not instantiate the group, print the stack and exit
			Log.error("JXTA fatal error : group creation failure");
			e.printStackTrace();
			joinstatus = JOIN_ERROR;
			return JOIN_ERROR;
		}

		// Get handles to the discovery services that we are going to need later
		discovery = this.mainGroup.getDiscoveryService();	

		// We start this peer as a rdv
		this.mainGroup.getRendezVousService().startRendezVous();

		// First time -> flush old advertisements
		if (jxtaElements == 1) {
			// We flush all the old advertisements.	
			Log.debug("Flushing the old advertisements.");
			try {
				discovery.flushAdvertisements(null, DiscoveryService.ADV);
			}
			catch(IOException ioe) {
				ioe.printStackTrace();
				Log.error("JXTA: Error flushing old advertisements. We can still continue (maybe).");
			}
		}

		// We send a request to find new propagate pipe advertisements.			
		Log.debug("Trying to find a propagate pipe advertisement. Name: '"+PREFIX+groupName+"'");

		// It seems that specifying a prefix does not work very well. In this case we can't narrow to our prefix.
		// The user must look manually to it when new advertisements are found.
		/*this.discovery.getRemoteAdvertisements(null,
		   Discovery.GROUP,
		   "Name",
		   WireService.WirePrefix+SUFFIX+"*",
		   30);*/
		discovery.getRemoteAdvertisements(null, DiscoveryService.ADV, null, null, 30, this);

		// Sleep until the timer expires or a correct advertisement is found by discovery service.
		for (int i=0; i<SLEEPING_TIME ; i+= 1000) {
			try {
				Thread.currentThread().sleep(1000);
				// Has a adv been found?
				if (nogroup == false) {
					// Listener was added in getRemoteAdvertisements() - it is no longer needed.
					discovery.removeDiscoveryListener(this);
					return joinstatus;
				}
			} catch(InterruptedException ie) {
				ie.printStackTrace();
				Log.error("JXTA: An error occurs while sleeping in getting the advertisements. Not a disaster, we can continue, but maybe restart the application.");
			}	    	    
		}
		Log.debug("JXTA sleep expired! - creating a new group!");
		// Listener was added in getRemoteAdvertisements() - it is no longer needed.
		discovery.removeDiscoveryListener(this);

		if (nogroup == false)
			return joinstatus;

		// Create a new "group"
		PipeAdvertisement chatAdv = null;
		chatAdv = this.createChatAdvertisement();
		if (chatAdv != null) {	
			this.createInputAndOutputPipes(chatAdv);
			joinstatus = JOIN_GROUPCREATED;		 
			nogroup = false;
		}
		// This is set in discoveryEvent()
		return joinstatus;	    	    		
	}  

	/**
	* Method used to receive new discovery event.
	* An event is received when a new advertisement is received.
	* This method is synchronized because we do not want receiving two new advertisements when the user is already trying to choose one.
	* @param event The received event.
	* @throws An exception if something bad happened
	*/
	public synchronized void discoveryEvent(DiscoveryEvent event) {
		Log.debug("JXTA discovery event received!");
		// If we have already choosen an advertisement, we are not interested anymore in new ones.
		if (nogroup == false) {
			return;
		}

		try {

			PipeAdvertisement chatAdv = null;

			// We get all the responses of the event.
			DiscoveryResponseMsg rep = event.getResponse();
			Enumeration e = rep.getResponses();

			while (e != null && e.hasMoreElements()) {            
				// Try to get an advertisement from the responses.
				String advString =  (String) e.nextElement();
				Advertisement adv = AdvertisementFactory.newAdvertisement(new MimeMediaType("text/xml"), new ByteArrayInputStream (advString.getBytes()));
				if (adv instanceof PipeAdvertisement) {
					chatAdv = (PipeAdvertisement)adv;                
					if (chatAdv != null) {                    		
						// We try to find a chat advertisement. If several are available, we do a loop in order the user can choose the one he wants.
						Log.debug("Advertisement name:"+chatAdv.getName());

						if (chatAdv.getName().startsWith(PREFIX+groupName) && chatAdv.getType().compareTo(PipeService.PropagateType) == 0) {
							Log.debug("Pipe advertisement found: "+chatAdv.getName());	
//MLFCmod					String answer = reader.readLine();
							String answer = "y";
							// The user is happy with this advertisement, we create input and output pipes associated.
							if ((answer.compareTo("y"))==0) {                                                       
								this.createInputAndOutputPipes(chatAdv);
								joinstatus = JOIN_GROUPJOINED;
								nogroup = false;		 
								return;                            
							}
						}
					}	
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	* Method used to create a chat advertisement.
	* This advertisement is a pipe advertisement with a special name (a prefix plus the inetaddress of the local machine).
	* @return A new chat advertisement.
	*/
	public PipeAdvertisement createChatAdvertisement() {        
		Log.debug("We create a new advertisement. Name: '"+PREFIX+groupName+"'");
		return this.createPipeAdvertisement(PREFIX+groupName);
	}

	/**
	* Method used to create input and output pipes from the chat advertisement.
	* @param chatAdv The chat advertisement used to create the pipes.
	* @param Exception If the param is null, we throw an exception.
	*/
	public void createInputAndOutputPipes(PipeAdvertisement chatAdv) throws Exception {
		// If the chat advertisement is not null we create new input and output pipes.
		if (chatAdv != null) {	 	   

			// We get the pipe from the group.
			PipeService pipe = this.mainGroup.getPipeService();

			// Creation of the input and output pipes used to get and to send messages.
			try {
				this.inputPipe = pipe.createInputPipe(chatAdv, this);
				this.outputPipe = pipe.createOutputPipe(chatAdv, TIME_TO_WAIT);	
			}
			catch(IOException ioe) {                
				Log.error("JXTA: Error while creating input or output pipe");
				throw ioe;
			}
		}
		else {
			// If the advertisement is null, then something botched !!!
			throw new Exception("Unable to create the pipes because the advertisement is null");
		}
	}


	/**
	* Method used to create a message. It gets a hashtable and read the fields from it.
	* Then it creates a new message (with the pipe service) and put the fields into the message.
	* The fields are put like in the hashtable, the key and the value associated with the key.
	* By exemple, if a hashtable contains: "name", "hello". We will put in the messag the key "name"
	* and the value "hello".
	* @param pipeService The pipe service used to create a new message.
	* @param fields The hashtable that contains the keys and values we are going to put in the message.
	* @return The message associated with the hashtable.
	* @throws NoSuchElementException If a key is not associated with a value (should never happen).
	* @throws IOException If an IOException occurs when creating the message.
	*/
	private Message createMessage(PipeService pipe, Hashtable fields) throws NoSuchElementException, IOException {
		// We create the message.
		Message message = pipe.createMessage();
		// We get the key of the hashtable.
		Enumeration keys = fields.keys();
		// We get the elements of the hashtable.
		Enumeration elements = fields.elements();

		// The number of keys and elements must be the same.
		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			String field = (String)elements.nextElement();
			MessageElement messageElement = message.newMessageElement(key, new MimeMediaType(MIME_TEXT), new ByteArrayInputStream(field.getBytes()));             
			// I add the newly element to the message. 
			// The message is made of two elements: the name of the sender and the message.	    
			message.addElement(messageElement);
		}
		return message;
	}

	/**
	* Method used to create the propagate pipe advertisement.
	* @param name The name of the PipeAdvertisement.
	* @return The new pipe advertisement.
	*/
	private PipeAdvertisement createPipeAdvertisement(String name) {		

		if ((name == null) || (name.length() == 0)) {
			name = "null";
		}

		// The pipe advertisement used to create the peer group advertisement.
		// We must create a pipe advertisement because for creating a service advertisement (that will be used in the
		// peer group advertisement), we need it.
		PipeAdvertisement pipeAdv = null;
		try {	    
            pipeAdv = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(PipeAdvertisement.getAdvertisementType());	    
			pipeAdv.setPipeID(IDFactory.newPipeID(this.mainGroup.getPeerGroupID()));
			pipeAdv.setName(name);
			pipeAdv.setType(PipeService.PropagateType);
		} catch (Exception e) {
		    e.printStackTrace();
			Log.error("JXTA: An exception occured, we can't create the peer group advertisement.");
			return null;
		}
        
        // We publish the pipe advertisement in order for the other peers to find it.
        try {
            discovery.publish(pipeAdv, DiscoveryService.ADV);
			discovery.remotePublish(pipeAdv, DiscoveryService.ADV);
			}
        catch (IOException ioe) {
            ioe.printStackTrace();
			Log.error("JXTA: Error while publishing the adv.");
			}
        return pipeAdv;
	}

	/**
	* This method is used to receive new message from the inputPipe we have created before.
	* MLFC: This receives data and sends it to the listeners.
	* @param pipeMsgEvent The pipe message event we will retrieve the message from it.
	*/
	public void pipeMsgEvent(PipeMsgEvent pipeMsgEvent) {
		try {
			// We get the message from the pipe event
			Message message = pipeMsgEvent.getMessage();
			// We get all the names. In fact only the sender and the message subject are interesting for us.
			Enumeration names = message.getNames();

			String sender = "";
			String msg = "";
			byte[] bytes=null;
			String mimeType=MIME_TEXT;
			// We get and print the content of the interesting names.
			while (names.hasMoreElements()) {
				String name = (String)names.nextElement();
				if (name.compareTo("sender") == 0) {
					InputStream inputStream = (message.getElement(name)).getStream();
					byte[] sbytes = new byte[inputStream.available()];
					inputStream.read(sbytes);
					sender = new String(sbytes);
				}
				else if (name.compareTo("message") == 0) {
					InputStream inputStream = (message.getElement(name)).getStream();
					mimeType = message.getElement(name).getType().toString();
					Log.debug("PipeMsgEvent: got message with mime:"+mimeType);
					bytes = new byte[inputStream.available()];
					inputStream.read(bytes);
				}
			}
			// We print the message.
			Log.debug("<"+sender+"> wrote: "+msg);
			// MLFC: We call the data listeners 
			for (Enumeration e = dataListeners.elements() ; e.hasMoreElements() ;) {
				if (mimeType.equals(MIME_TEXT))
				{
					if (bytes!=null) msg=new String(bytes);
					((DataListener)(e.nextElement())).dataReceived(sender, msg);
				}
				else
				{
					((DataListener)(e.nextElement())).dataReceived(sender, bytes,mimeType);
				}
			}
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
			Log.error("JXTA: Error while reading a message");
		}
	}  

}


