/**
 * LICENSE FOUND IN licenses.txt
 */
package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.Browser;

import fi.hut.tml.xsmiles.mlfc.*;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.apache.xerces.dom.DocumentImpl;
import java.util.Vector;

/**
 * A MLFC for communication for intercommunication of XML languages.
 *
 * @author juha
 */
public class COMMLFC extends MLFC 
{
  private Vector commElements;

  /**
   * Constructor.
   */
  public COMMLFC()
  {
    Log.debug("COMMLFC()");
    commElements=new Vector();
  }

  /**
   * Get the version of the MLFC. This version number is updated 
   * with the browser version number at compilation time. This version number
   * indicates the browser version this MLFC was compiled with and should be run with.
   * @return 	MLFC version number.
   */
  public final String getVersion() {
  	return Browser.version;
  }

  /**
   * Start the MLFC. 
   * This function is the pair to stop().
   */
  public  void start() 
  {
    Log.debug("Starting comm mlfc");
  }

  /**
   * Stop the MLFC. 
   * This function is the pair to start().
   */
  public  void stop()
  {
    Log.debug("Stopping comm mlfc");
  }


  /**
   * Create a DOM element.
   */
  public Element createElementNS(DocumentImpl doc, String ns,String tagname)
  {
    if(getLocalname(tagname).equals("comm")) {
      Log.debug("CREATING COMM ELEMENT tag: "+tagname );
      Element element = new COMMElementImpl(doc, ns, tagname, this);
      commElements.addElement(element);
      return element;
    } else if(getLocalname(tagname).equals("capture")) {
      Log.debug("CREATING COMM ELEMENT tag: "+tagname );
      Element element = new CaptureElementImpl(doc, ns, tagname, this);
      commElements.addElement(element);
      return element;
    } else {
      Log.debug("COMM MLFC didn't understand element, it is propably not implemented yet: "+tagname);
      return null;
    }
  }
  
  /**
   * Create a DOM attribute.
   */
  public Attr createAttributeNS(DocumentImpl doc,String namespaceURI, String qualifiedName)
    throws DOMException
  {
    Log.debug("CREATING COMM ELEMENT attr: "+qualifiedName );
    return null;
  }
}
