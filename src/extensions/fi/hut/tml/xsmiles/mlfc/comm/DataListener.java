/**
 * LICENSE FOUND IN licenses.txt
 */
package fi.hut.tml.xsmiles.mlfc.comm;

public interface DataListener 
{
  /**
   * @param sender The sender of the data
   * @param data The data sent
   */
  public void dataReceived(String sender, String data);
  public void dataReceived(String sender, byte[] data, String mime);
}
