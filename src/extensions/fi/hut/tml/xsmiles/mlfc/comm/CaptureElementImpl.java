/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import java.net.URL;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Xsmiles;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.comm.jxta.*;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.xslt.JaxpXSLT;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;

import fi.hut.tml.xsmiles.dom.VisualComponentService;

import javax.swing.*;
import java.awt.*;

import javax.media.*;
import javax.media.control.*;
import javax.media.format.*;
import javax.media.protocol.*;
import javax.media.rtp.rtcp.*;
import javax.media.rtp.*;
import javax.media.rtp.event.*;
import java.net.*;
import java.io.*;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.*;
import com.sun.media.ui.*;


/**
 * Capture  Element captures video using JMF 
 *
 * @author Kari
 */
public class CaptureElementImpl extends XSmilesElementImpl implements VisualComponentService
{
  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;

  // COMMMLFC
  private COMMLFC mlfc = null;

  private DataSource dataSource = null;
  private Processor p=null;
  private Player player = null;

  private Integer stateLock = new Integer(0);	
  private boolean failed = false;

  /**
   * Constructor - Set the owner, name and namespace.
   */
  public CaptureElementImpl(DocumentImpl owner, String namespace, String tag, COMMLFC m) {
    super(owner, namespace, tag);
    ownerDoc = owner;
	mlfc = m;
    Log.debug("CAPTURE element created!");
  }

  private void dispatch(String type) {
    // Dispatch the event
    Log.debug("Dispatching Capture Event");
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }
	
  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() {
    Log.debug("Capture.init");
    Format[] formats = new Format[1];
    formats[0]=new VideoFormat(VideoFormat.JPEG_RTP);
    createRealizedProcessor(formats);

    try {
    	player = Manager.createRealizedPlayer(dataSource);
    	Log.debug("Realized Player created");
    } catch(Exception e) {
        Log.debug("NewReceiveStreamEvent exception" + e.getMessage());
    	return;
    }

    if (player ==null) { 
		Log.debug("player == null");
	    return;
    }

    paraComponent = player.getVisualComponent();

    Log.debug("Player Started");
    player.start();
  }
  // currently supports EVENT_STYLECHANGED with object=null, in future there would be more 
  public void visualEvent(int event,Object object)
  {
      
  }


  /**
   * Do cleanup 
   */
  public void destroy() 
  {
	  Log.debug("Capture element destroy()");
	  try {		 
		  if (player != null) {
		  	player.stop();
		  	player = null;
		  }
		  Log.debug("Player stopped.");

		if (p != null) {
	  		p.stop();		
	  		p.close();
	  		p=null;
		}
		Log.debug("Processor closed.");
	  } catch(Exception e) {
	  	Log.debug(e.getMessage());
	  }

  }

  /**********************************************************************
   * VisualComponentService Interface - to use Capture element as parasite language.
   */

  private boolean visualComponentVisible = false;
  private Component paraComponent = null;
  /**
   * Return the visual component for this extension element
   */
  public Component getComponent() {
  	Log.debug("CAPTURE: getComponent()");
  	// If already fetched and started, just return the component
  	if (paraComponent != null)
  		return paraComponent;
  		
	// Do the timing!
//	JButton comp = new JButton("test");
//	paraComponent = comp;
	return new JLabel("Video Capture Failed.");
  }
  /**
   * Returns the approximate size of this extension element
   */
  public Dimension getSize() {
  	return new Dimension(100,100);
  }
  double zoom = 1.0;
  public void setZoom(double zoom) {
  	if (paraComponent != null)
		paraComponent.setSize(new Dimension((int)((double)100*zoom),
  							(int)((double)100*zoom)));
  	return;
  }
  public void setVisible(boolean visible) {
  	Log.debug("SET VISIBLE!"+visible);
  	visualComponentVisible = visible;
  	if (paraComponent != null)
  		paraComponent.setVisible(visible);
  	return;
  }
  public boolean getVisible() {
  	return visualComponentVisible; 
  }

  private String createRealizedProcessor(Format[] formats) {
  	DataSource ds;
  	DataSource clone;
  	CaptureDeviceInfo di=null;
  	Vector deviceList = null;

  	Log.debug("Creating processor, format:" + formats);

  	// Get the CaptureDeviceInfo for the live audio capture device 
  	if(formats[0] instanceof AudioFormat)
  		deviceList = CaptureDeviceManager.getDeviceList(new AudioFormat(AudioFormat.LINEAR));//, 8000, 8, 1));//formats[0]);
  	//	    new AudioFormat(AudioFormat.LINEAR,
  	//			    44100, 16, 2));
  	if(formats[0] instanceof VideoFormat)
  		deviceList = CaptureDeviceManager.getDeviceList(
  			new VideoFormat(VideoFormat.RGB));

  	for (Enumeration e = deviceList.elements() ; e.hasMoreElements() ;) { 
  		CaptureDeviceInfo cdi = (CaptureDeviceInfo)e.nextElement();
  		Log.debug(cdi.toString());
  	} 

  	if (deviceList.size() > 0)
  		di = (CaptureDeviceInfo)deviceList.firstElement();
  	else {
  		// Exit if we can't find a device that does linear, 44100Hz, 16 bit, 
  		// stereo audio. 
  		Log.debug("deviceList == 0");
  		return "no device";
  	}
  	try {
  		ds = javax.media.Manager.createDataSource(di.getLocator());
  		dataSource = ds;
  	} catch (Exception e) {
  		return "Couldn't create DataSource";
  	}
  	Log.debug("Datasource created");

  	// Try to create a processor to handle the input media locator
  	try {
  		p = javax.media.Manager.createProcessor(ds);
  	} catch (NoProcessorException npe) {
  		return "Couldn't create processor";
  	} catch (IOException ioe) {
  		return "IOException creating processor";
  	} 
  	Log.debug("Processor created");

  	// Wait for it to configure
  	boolean result = waitForState(p, Processor.Configured);
  	if (result == false)
  		return "Couldn't configure processor";

  	Log.debug("Processor configured");

  	// Get the tracks from the processor
  	TrackControl [] tracks = p.getTrackControls();

  	// Do we have atleast one track?
  	if (tracks == null || tracks.length < 1)
  		return "Couldn't find tracks in processor";

	ContentDescriptor cd = new ContentDescriptor(ContentDescriptor.RAW_RTP);
  	p.setContentDescriptor(cd);

	Format supported[];
  	Format chosen = formats[0];
  	boolean atLeastOneTrack = false;

  	// Program the tracks.
  	for (int i = 0; i < tracks.length; i++) {
  		Format format = tracks[i].getFormat();
  		if (tracks[i].isEnabled()) {

  			supported = tracks[i].getSupportedFormats();

  			// We've set the output content to the RAW_RTP.
  			// So all the supported formats should work with RTP.
  			// We'll just pick the first one.

  			if (supported.length > 0) {
  				if (formats[0] instanceof VideoFormat) {

  					chosen = checkForVideoSizes(tracks[i].getFormat(), 
  						matches(formats[0], supported));

  					tracks[i].setFormat(chosen);

  				}
  				else
  					//chosen = supported[0];
  					tracks[i].setFormat(matches(formats[0], supported));
  				System.err.println("Track " + i + " is set to transmit as:");
  				System.err.println("  " + chosen);
  				atLeastOneTrack = true;
  			}
  			else
  				tracks[i].setEnabled(false);
  		}
  		else
  			tracks[i].setEnabled(false);
  	}

  	if (!atLeastOneTrack)
  		return "Couldn't set any of the tracks to a valid RTP format";

  	// Realize the processor. This will internally create a flow
  	// graph and attempt to create an output datasource for JPEG/RTP
  	// audio frames.
  	result = waitForState(p, Controller.Realized);
  	if (result == false)
  		return "Couldn't realize processor";

  	// Set the JPEG quality to .5.
  	if(formats[0] instanceof VideoFormat)
  		setJPEGQuality(p, 0.5f);

  	// Get the output data source of the processor
  	//	dataOutput = p.getDataOutput();

  	return null;
  }

  /**
  * For JPEG and H263, we know that they only work for particular
  * sizes.  So we'll perform extra checking here to make sure they
  * are of the right sizes.
  */
  Format checkForVideoSizes(Format original, Format supported) {

  	int width=0, height=0;
  	Dimension size = ((VideoFormat)original).getSize();
  	Format jpegFmt = new Format(VideoFormat.JPEG_RTP);
  	Format h263Fmt = new Format(VideoFormat.H263_RTP);

  	if (supported.matches(jpegFmt)) {
  		// For JPEG, make sure width and height are divisible by 8.
  		width = (size.width % 8 == 0 ? size.width :
  		(int)(size.width / 8) * 8);
  		height = (size.height % 8 == 0 ? size.height :
  		(int)(size.height / 8) * 8);
  	}
  	else if (supported.matches(h263Fmt)) {
  		// For H.263, we only support some specific sizes.
  		//  if (size.width < 128) {
  		//	width = 128;
  		// 	height = 96;
  		//  } else if (size.width < 176) {
  		width = 176;
  		height = 144;
  		// } else {
  		//	width = 352;
  		// 	height = 288;
  	}
  	//	} else {
  	// We don't know this particular format.  We'll just
  	// leave it alone then.
  	//	    return supported;
  	//	}

  	return (new VideoFormat(null, 
  		new Dimension(width, height), 
  		Format.NOT_SPECIFIED,
  		null,
  		Format.NOT_SPECIFIED)).intersects(supported);
  }

  /**
  * Setting the encoding quality to the specified value on the JPEG encoder.
  * 0.5 is a good default.
  */
  void setJPEGQuality(Player p, float val) {

  	Control cs[] = p.getControls();
  	QualityControl qc = null;
  	VideoFormat jpegFmt = new VideoFormat(VideoFormat.JPEG);

  	// Loop through the controls to find the Quality control for
  	// the JPEG encoder.
  	for (int i = 0; i < cs.length; i++) {

  		if (cs[i] instanceof QualityControl &&
  			cs[i] instanceof Owned) {
  			Object owner = ((Owned)cs[i]).getOwner();

  			// Check to see if the owner is a Codec.
  			// Then check for the output format.
  			if (owner instanceof Codec) {
  				Format fmts[] = ((Codec)owner).getSupportedOutputFormats(null);
  				for (int j = 0; j < fmts.length; j++) {
  					if (fmts[j].matches(jpegFmt)) {
  						qc = (QualityControl)cs[i];
  						qc.setQuality(val);
  						System.err.println("- Setting quality to " + 
  							val + " on " + qc);
  						break;
  					}
  				}
  			}
  			if (qc != null)
  				break;
  		}
  	}

  }

  private static Format matches(Format format, Format [] supported) {
  	if (supported == null)
  		return null;
  	for (int i = 0; i < supported.length; i++) {
  		if (supported[i].matches(format))
  			return supported[i];
  	}
  	return null;
  }

  private synchronized boolean waitForState(Processor p, int state) {
  	p.addControllerListener(new StateListener());
  	failed = false;

  	// Call the required method on the processor
  	if (state == Processor.Configured) {
  		p.configure();
  	}
  	else if (state == Processor.Realized) {
  		p.realize();
  	}

  	// Wait until we get an event that confirms the
  	// success of the method, or a failure event.
  	// See StateListener inner class
  	while (p.getState() < state && !failed) {
  		synchronized (getStateLock()) {
  			try {
  				getStateLock().wait();
  			} catch (InterruptedException ie) {
  				return false;
  			}
  		}
  	}

  	if (failed)
  		return false;
  	else
  		return true;
  }



  class StateListener implements ControllerListener {

  	public void controllerUpdate(ControllerEvent ce) {

  		// If there was an error during configure or
  		// realize, the processor will be closed
  		if (ce instanceof ControllerClosedEvent)
  			setFailed();

  		// All controller events, send a notification
  		// to the waiting thread in waitForState method.
  		if (ce instanceof ControllerEvent) {
  			synchronized (getStateLock()) {
  				getStateLock().notifyAll();
  			}
  		}
  	}
  }

  void setFailed() {
  	failed = true;
  }

  Integer getStateLock() {
  	return stateLock;
  }

}

