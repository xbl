/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import java.net.URL;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Xsmiles;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.comm.jxta.*;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.xslt.JaxpXSLT;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;

/**
 * GPSData  Element send dom changed events to inform 
 * changes in coordinate data
 *
 * @author Juha
 */
public class COMMElementImpl extends XSmilesElementImpl implements DataListener
{
  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;

  // COMMMLFC
  private COMMLFC mlfc = null;

  // The communications channel
  private COMMInterface commChannel;
  
  // Last received data
  private String receivedData = null;

  // Last received sender
  private String receivedSender = null;
  
  // Last document received
  private Document receivedDoc =null;
  
  /**
   * Constructor - Set the owner, name and namespace.
   */
  public COMMElementImpl(DocumentImpl owner, String namespace, String tag, COMMLFC m) {
    super(owner, namespace, tag);
    ownerDoc = owner;
	mlfc = m;
    Log.debug("COMM element created!");
  }

  private void dispatch(String type) {
    // Dispatch the event
    Log.debug("Dispatching COMM Event");
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }
	
  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() {
    Log.debug("COMM.init");
    String href=this.getAttribute("href");

	// Create the JXTA connection, optionally creating one group as well.
	String autoGroup = getAttribute("group");
	if (autoGroup != null && autoGroup.length() > 0) {

		// Logging text
		mlfc.getMLFCListener().setStatusText("Logging to the network... please wait.");

		commChannel=new JXTACOMMImpl();
		int status = commChannel.join(autoGroup, null);
		if (status == COMMInterface.JOIN_ERROR) {
			mlfc.getMLFCListener().setStatusText("Login to the network FAILED!");
			Log.error("Login to the network FAILED!");
		}
		if (status == COMMInterface.JOIN_GROUPCREATED) {
			mlfc.getMLFCListener().setStatusText("Logged to the network - no peers found! This may mean you're the first user.");
			Log.error("Logged to the network - no peers found! This may mean you're the first user.");
		}
		if (status == COMMInterface.JOIN_ERROR) {
			mlfc.getMLFCListener().setStatusText("Logged to the network successfully.");
			Log.debug("Logged to the network successfully.");
		}
	} else	
		commChannel=new JXTACOMMImpl();

	commChannel.addDataListener(this);
  }

  /**
   * @param params The parameters that might be needed
   * @param name The group to join
   * @return true if successful
   */
  public boolean join(String name, String params)
  {
    int status = commChannel.join(name, null);
    if (status == COMMInterface.JOIN_ERROR) {
    	mlfc.getMLFCListener().setStatusText("Login to the network FAILED!");
    	Log.error("Login to the network FAILED!");
	  	return false;
    }
    if (status == COMMInterface.JOIN_GROUPCREATED) {
    	mlfc.getMLFCListener().setStatusText("Logged to the network - no peers found! This may mean you're the first user.");
    	Log.error("Logged to the network - no peers found! This may mean you're the first user.");
    }
    if (status == COMMInterface.JOIN_ERROR) {
    	mlfc.getMLFCListener().setStatusText("Logged to the network successfully.");
    	Log.debug("Logged to the network successfully.");
    }
    return true;
  }
  
  /**
   * Send data to target
   * @return True if successful false if not
   */
  public boolean send(String target, String data) 
  { 
    commChannel.send(target,data);
    return true;
  }
  /**
   * Send XML data to target
   * @return True if successful false if not
   */
  public boolean sendXML(String target, org.w3c.dom.Node node)
  {
  	try
  	{
	  	ByteArrayOutputStream ba = new ByteArrayOutputStream();
	  	//public static void SerializeNode(Writer writer,Node node,boolean preserveSpace,boolean indenting, boolean addXMLDecl)
		JaxpXSLT.SerializeNode(new OutputStreamWriter(ba),node,true,false,true);
	  	commChannel.sendMime(target, ba.toByteArray(), "application/xml");
		return true;
  	} catch (Exception e)
  	{
		return false;
  	}
  }
  
  public boolean sendMime(String target, byte[] data, String mime)
  {
  	return commChannel.sendMime(target,data,mime);
  }
  
  /**
   * @param location Where to save the file
   * @param name The filename to get
   * @return true if successful
   */
  public boolean getFile(URL location, String name) 
  {
    commChannel.getFile(location,name);
    return true;
  }
  
  /**
   * Data has been received from COMM. Dispatch an event.
   */
  public void dataReceived(String source, String data) 
  {
    Log.debug("Data Received from "+ source+ ": " + data);
	// Save last received data so that scripts can retrieve it using getReceivedData()
	receivedData = data;
	receivedSender = source;
	// Dispatch event into DOM tree
	dispatch("commEvent");
  }

  /**
   * Data has been received from COMM. Dispatch an event.
   */
  public void dataReceived(String source, byte[] data, String mime) 
  {
    Log.debug("Data Received from "+ source+ ": " + data+ "mime:"+mime);
  // Save last received data so that scripts can retrieve it using getReceivedData()
  // TODO: parse as XML if needed
  	receivedSender = source;
  	if (mime.equals("application/xml"))
	{
		try
		{
			receivedDoc = Browser.xmlParser.openDocument(new InputStreamReader(new ByteArrayInputStream(data)),false);
		} catch (Exception e)
		{
			Log.error(e);
			return;
		}
	}
	else
	{
	  receivedData = new String(data);
    	// Dispatch event into DOM tree
		dispatch("commEvent");
	}
  }


  public String getReceivedData() {
  	return receivedData;
  }

  public String getReceivedSender() {
  	return receivedSender;
  }

  /**
   * Get local Peer name.
   * @return 	Local peer name
   */
  public String getLocalPeerName() {
  	if (commChannel == null)
		return null;
  	return commChannel.getLocalPeerName();
  }
  
  /**
   * Get PeerGroup name.
   * @return 	Local peer name
   */
  public String getPeerGroupName() {
	  if (commChannel == null)
	  	return null;
	  return commChannel.getPeerGroupName();
  }


  /**
   * @param name Leave a group
   */
  public void leave(String name) 
  {
    commChannel.leave(name);
  }
  
  /**
   * Do cleanup 
   */
  public void destroy() 
  {
  	Log.debug("COMM element destroy()");
    commChannel.destroy();
  }
}

