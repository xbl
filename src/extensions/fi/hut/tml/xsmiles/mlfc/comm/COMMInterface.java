/**
 * LICENSE FOUND IN licenses.txt
 */
package fi.hut.tml.xsmiles.mlfc.comm;

import java.net.URL;
/**
 * A MLFC for establishing a communication channel 
 * for intercommunication of XML languages.
 *
 * @author juha
 */
public interface COMMInterface
{
  /**
   * Send data to target
   * @return True if successful false if not
   */
  public boolean send(String target, String data);
  
  /**
   * Send data to target
   * @return True if successful false if not
   */
  public boolean sendMime(String target, byte[] data, String mimetype);
  /**
   * Receive data
   */
  public void addDataListener(DataListener dl);

  /**
   * @param target Send a file to a target
   * @param data The URL where file is located
   * @return true if successful
   */
  public boolean sendFile(String target, URL data);

  /**
   * @param location Where to save the file
   * @param name The filename to get
   * @return true if successful
   */
  public boolean getFile(URL location, String name);

  public final static int JOIN_ERROR = 0;
  public final static int JOIN_GROUPJOINED = 1;
  public final static int JOIN_GROUPCREATED = 2;

  /**
   * @param params The parameters that might be needed
   * @param name The group to join
   * @return JOIN_ERROR, JOIN_GROUPCREATED or JOIN_GROUPJOINED
   */
  public int join(String name, String params);

  /**
   * @param name Leave a group
   */
  public void leave(String name);

  /**
   * Do cleanup
   */ 
  public void destroy();
  
  /**
   * Get local Peer name.
   * @return 	Local peer name
   */
  public String getLocalPeerName(); 
  
  /**
   * Get PeerGroup name.
   * @return 	Local peer name
   */
  public String getPeerGroupName();
  
}
