/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Decorator;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.gui.components.havi.HaviComponentFactory;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;
/*
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCMediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCLinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCDrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCBrushHandler;
*/
// TODO: create AWT version!
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCForeignHandler;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.havi.ui.HNavigable;

import java.net.URLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.awt.*;
import java.awt.event.*;


import fi.hut.tml.xsmiles.Log;

import org.havi.ui.HContainer;

/**
 *
 */
public class HaviDecorator implements Decorator, FocusableLinkHandler
{
    // THIS IS BAD. REMOVE
    HaviFocusManager focusManager=SMILViewer.focusManager;
    
    public HaviDecorator()
    {
        Log.debug(this+"AWTDecorator created!");
    }
    public Container createRootLayout()
    {
        // Set the root layout drawing area
        // This MLFC has scrollbars.
        
        // TODO: create Havi panels
        Container root = HaviComponentFactory.createContainer();
        //Container root = new HContainer();
        //((javax.swing.JPanel)root).setOpaque(true);
        return root;
        
    }
    // The MLFC has to take care of the scroll bars...
    public Container createScrollPanel(Container rootLayout)
    {
        // TODO: in jdk 1.1 you cannot create containers...
        // TODO: create Have containers
        return rootLayout;
    }
    // The MLFC has to take care of the scroll bars...
    /*
    public Container createScrollPanel_orig(Container rootLayout)
    {
        // TODO: in jdk 1.1 you cannot create containers...
        // TODO: create Have containers
        //Container scrollPanel = new Container();
        Container scrollPanel = new javax.swing.JPanel();
        //((Panel)root).setOpaque(false);
        scrollPanel.add(rootLayout);
        return scrollPanel;
    }*/
    public void createSecondaryBorders(Container rootLayout, Container scrollPanel)
    {
        /*
            Border empty = BorderFactory.createEmptyBorder();
            ((JLayeredPane)rootLayout).setBorder(empty);
            ((JComponent)scrollPanel).setBorder(empty);
         */
    }
    
    public void addToContainer(Container scrollPanel, Container container)
    {
        
            container.add(scrollPanel, 0);
            //container.setSize(640,400);
            //scrollPanel.setSize(640,400);
            container.doLayout();
            container.validate();
    }
    
        /**
     * Returns a new MediaHandler for SMIL core logic.
     */
    public MediaHandler getNewMediaHandler()
    {
        // Pass XMLDocument for ExtensionMedia
        return new HaviMediaHandler();
    }
    
    /**
     * Returns a new BrushHandler for SMIL core logic.
     */
    public BrushHandler getNewBrushHandler(Viewer v)
    {
        HaviBrushHandler bh = new HaviBrushHandler();
        bh.setViewer(v);
        return bh;
    }
    
    /**
     * Returns a new LinkHandler for SMIL core logic.
     */
    public LinkHandler getNewLinkHandler()
    {
        return new HaviLinkHandler();
    }
    
    /**
     * Returns a new DrawingArea for SMIL core logic.
     * @param type		ROOTLAYOUT for the broswer container, TOPLAYOUT for a new frame.
     * @param block		CSS: used to create a JBlockPanel (true) instead of JPanel (false)
     */
    public DrawingArea getNewDrawingArea(int type, boolean block, Container rootLayout,boolean layoutModel)
    {
        // CSS: Create a DrawingArea with the CSS layout information
        // It will cause the area to have a flow layout...
        if (type == DrawingArea.ROOTLAYOUT)
            return new HaviDrawingArea(rootLayout);//, layoutModel);
        else
            return new HaviDrawingArea(type);
    }
    
    /**
     * Returns a new ForeignHandler for SMIL core logic.
     */
    public MediaHandler getNewForeignHandler(Element e)
    {
        // TODO: create AWT version
        Log.debug("Creating SMILMLFCForeignHandler instead of AWT!");
        MediaHandler mh = new HaviForeignHandler(e);
        return mh;
    }
    
    
    /**
     * Check if JMF class is available. At the same time, set the hint...
     * @return		true if JMF is available, false otherwise
     */
    public boolean isJMFAvailable(MLFCListener listener)
    {
        return true;
    }
    
    /** from focusable link handler interface */
    /**
     * Add a link component to a focus list.
     * @param	link	Link component, HNavigable
     */
    public void addFocusableLink(HNavigable link) {
        //linkFocusList.addElement(link);
        focusManager.addFocusable(link);
        return;		
    }
    
    /**
     * Remove a link component from a focus list. Causes the whole focus path to be recalculated.
     * @param	link	Link component, HNavigable
     */
    public void removeFocusableLink(HNavigable link) {
        focusManager.removeFocusable(link);
        return;	
    }
}

