/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.media;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

import fi.hut.tml.xsmiles.gui.media.general.CSSStylableMedia;
import fi.hut.tml.xsmiles.gui.media.general.Media;

import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.Container;
import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.FontMetrics;

import java.net.MalformedURLException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.EOFException;
import java.io.IOException;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.css.CSSStyleDeclaration;


/**
 * This is the implementation of text media. Uses internal CSS parser for simple html.
 */
public class TextMedia extends HaviMedia implements Media, Runnable, CSSStylableMedia
{
    
    // Text media
    private MyLabel jtext;
    
    // Container for media (DrawingArea)
    Container container = null;
    
    // Url to be shown
    String url = null;
    
    // Text to be shown
    String text = null;
    
    /** the CSS style
     */
    protected CSSStyleDeclaration CSSStyle;
    
    // MediaListener - called after prefetch and endOfMedia.
    MediaListener mediaListener = null;
    
    // Location and coords for the media
    int x=0, y=0, width=0, height=0;
    
    public TextMedia()
    {
        // Create the components
        jtext=new MyLabel();
    }
    
    /**
     * Checks if this media is static or continuous.
     * @return true	if media is static.
     */
    public boolean isStatic()
    {
        return true;
    }
    
    /**
     * Set the url for this text component. Text will have higher priority than URL.
     * @param url		URL string to show
     */
    public void setUrl(String url)
    {
        this.url = url;
    }
    
    /** (CSSStylableMedia interface) set the resolved CSS style for this element */
    public void setStyle(CSSStyleDeclaration style)
    {
        this.CSSStyle=style;
    }
    
    /**
     * Set the text for this text component. Text has higher priority than URL.
     * @param text		String to show
     */
    public void setText(String text)
    {
	System.out.println("In here");
        this.text = text;
        if (text == null)
            this.text = "";
    }
    
    /**
     * Fetch the text from url and return it.
     * @param 	url		URL to be retrieved
     * @return			string in the URL
     */
    private String fetchText(String urlString)
    {
        StringBuffer intext = new StringBuffer();
        String str;
        BufferedReader in = null;
        URL url;
        
        if (text != null)
            return text;
        
        // Load text from URL as data
        if (urlString.startsWith("data:"))
        {
            return urlString.substring(6);
        }
        
        // Load text from URL
        try
        {
            url = new URL(urlString);
        } catch (MalformedURLException e)
        {
            //Log.error("Not valid URL: "+urlString+" - skipping text.");
            return "";
        }
        try
        {
            //  Create an input stream
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            
            // Read a string
            while ((str = in.readLine()) != null)
                intext.append(str);
        } catch(EOFException e)
        {
            // OK
        } catch(IOException e)
        {
            //Log.error("I/O Error reading text from "+url);
            return "";
        }
        finally
        {
            try
            { in.close(); } catch(Exception e)
            {}
        }
        // Add text to the proxy
        String contents = new String(intext);
        return contents;
    }
    
    public void prefetch()
    {
        Log.debug("TEXTMEDIA: "+this+".prefetch("+url+") ");
        
        String realBody, content = "! not found !";
        String type = "text/html";
        
        content = fetchText(url);
        
        // Set content style - this should be "text/plain" OR "text/html"
        realBody = setContentStyle(content, type);
        
        jtext.setText(realBody);
        //		jtext.setLineWrap(true);
        //		jtext.setWrapStyleWord(true);
        //		jtext.setEditable(false);
        //jtext.setVisible(true);
    }
    
    public void setContainer(Container container)
    {
        this.container = container;
    }
    
    public void play()
    {
        //Log.debug("TEXTMEDIA: "+this+".play("+url+") ");
        
        if (container != null)
        {
            jtext.setLocation(x, y);
            jtext.setSize(width, height);
	    container.add(jtext, 0);
            // Set up text component
            
            jtext.setVisible(true);
        }
        //else
        //Log.error("Container not set for media "+url);
        
        // Media ends immediately for static media (almost..).
        // Media ended - inform the SMIL player.
        if (mediaListener != null)
        {
            Thread t = new Thread(this);
            t.start();
        }
    }
    
    public void run()
    {
        try
        {
            Thread.sleep(100);
        } catch (InterruptedException e)
        {
        }
        // Media ended immediately (almost...)
        if (mediaListener != null)
            mediaListener.mediaEnded();
    }
    
    
    public void pause()
    {
    }
    
    public void stop()
    {
        //Log.debug("PROXY: stop()");
        //		timer.stop();
        
        // Second event, this is the stop event.
        //	gfxComponent.setVisible(false);
        //		container.showComponents(false);
        //		container.repaint();
        //		container.setVisible(false);
        jtext.setVisible(false);
    }
    
    public void setBounds(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void close()
    {
        mediaListener = null;
    }
    
    public void addMediaListener(MediaListener listener)
    {
        mediaListener = listener;
    }
    
    /**
     * Set the style, if found in the content. (<html><body style="color:blue">Jee</body></html>)
     * This is a very ugly way to find the style attribute and body content.
     *
     * @param content	Content string
     */
    private String setContentStyle(String content, String contentType)
    {
        int i, end, len = content.length();
        // First quick checks for the style
        
        boolean styleset = false;
        {
            // If this is html and style attribute exists, then get the style
            if (contentType.equals("text/html")== true)
            {
                // Go through the content and search for 'style='
                for (i = 0 ; i < len-8 ; i++)
                {
                    
                    if (content.substring(i, i+6).equals("style="))
                    {
                        // Found, use it
                        end = content.indexOf('"', i+7);
                        if (end == -1)
                            end = i+6;
                        //Log.debug("STYLE FOUND: "+content.substring(i+7, end));
                        // MH: CSS does not work in JDK 1.1:
                        CSSTextFormatter csstf = new CSSTextFormatter();
                        csstf.setStyle(content.substring(i+7, end));
                        csstf.formatComponent(jtext);
                        styleset=true;
                    }
                }
                if (styleset==false&&CSSStyle!=null&&CSSStyle.getLength()>0)
                {
                    // try to use CSS style from the element
                    // how to get the element?
                        RealCSSTextFormatter csstf = new RealCSSTextFormatter();
                        csstf.setStyle(CSSStyle);
                        csstf.formatComponent(jtext);
                }
            } else
            { // No html - maybe text/plain
                //Log.debug("STYLE is text/plain: "+content);
                return content;
            }
        }
        
        // Search for body text
        // Go through the content and search for '<body'
        String s;
        for (i = 0 ; i < content.length()-8 ; i++)
        {
            s = content.substring(i,i+5);
            if (s.equals("<body"))
            {
                // Found, use it
                try
                {
                    end = content.indexOf('<', i+6);
                    i = content.indexOf('>', i);
                    //Log.debug("BODY FOUND: "+content.substring(i+1, end));
                    return content.substring(i+1, end);
                } catch (StringIndexOutOfBoundsException e)
                {
                    return content;
                }
                
            }
        }
        // Body not found - show it as is
        return content;
        
    }
    
    /**
     * This moves the time position in media. Not effective for this media.
     * @param millisecs		Time in millisecs
     */
    public void setMediaTime(int millisecs)
    {
    }
    
    /**
     * Get the duration of media. Only applicable for continuous media (audio, video).
     * @return The duration of media in millisecs.
     */
    public int getOriginalDuration()
    {
        return 0;
    }
    
    /**
     * Get the real width of the media.
     */
    public int getOriginalWidth()
    {
        return -1;
    }
    
    /**
     * Get the real height of the media.
     */
    public int getOriginalHeight()
    {
        return -1;
    }
    
    /**
     * Set the sound volume for media. Only applicable for sound media formats.
     * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
     * dB change in signal level = 20 log10(percentage / 100) )
     */
    public void setSoundVolume(int percentage)
    {
    }
    
    /**
     * Simple class to draw text. This is required to get quick and nice looking text.
     */
    public class MyLabel extends Component
    {
        String text = "";
        Font font = null;
        Color foreground = Color.black, background = null;
        int width = 0, height = 0;
	
        public MyLabel()
        {
            font = new Font("verdana", Font.PLAIN, 12);
        }
        
        public void setText(String t)
        {
            if (t.length()==0)
                text = "";
	    else
                text = t;
        }
        
        public void setFont(Font f)
        {
            font = f;
        }
        
        public void setBackground(Color c)
        {
            background = c;
            super.setBackground(c);
        }
        
        public void setForeground(Color c)
        {
            foreground = c;
            super.setForeground(c);
        }
        
        public void setSize(int w, int h)
        {
            width = w;
            height = h;
            super.setSize(w, h);
        }
        
        public void setOpaque(boolean f)
        {
            // false = transparent
            if (f == false)
                background = null;
        }
        
        /**
         * Draw text
         */
        public void paint(Graphics g)
        {
	    int h = 12;
	    int w = 0;
	    if (background != null)
		{
		    g.setColor(background);
		    g.fillRect(0,0, width, height);
		}
	    
	    g.setColor(foreground);
	    if (font != null)
		{
		    g.setFont(font);
		    FontMetrics fmt = g.getFontMetrics(font);
		    h = (int)fmt.getHeight();
		    w = (int)fmt.getMaxAdvance();
		    //h = (int)font.getSize();
		}
	    g.drawString(text, w, h);
	}
    }
    
}
