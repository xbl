/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.media;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.gui.media.general.Media;

import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.Component;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
//import sdl.video.SDLSurface;

import java.net.URL;
import java.net.MalformedURLException;

import fi.hut.tml.xsmiles.Log;

/**
 * This is the implementation of image media.
 */
public class ImageMedia extends HaviMedia implements Media, Runnable {

    // ImageCanvas for current image
    PImageCanvas canvas;

    // Container for media (DrawingArea)
    Container container = null;

    // MediaListener - called after prefetch and endOfMedia.
    MediaListener mediaListener = null;
    
    // Location and coords for the media
    int x=0, y=0, width=0, height=0;
    
    // Orig. width & height
    int origWidth = -1, origHeight = -1;
    
    public ImageMedia() {
	// Create the components		
	canvas=new PImageCanvas();
    }

    /**
     * Checks if this media is static or continuous.
     * @return true	if media is static.
     */
    public boolean isStatic() {
	return true;
    }
    
    String url = null;
    
    public void setUrl(String url) {
	this.url = url;
        Log.debug("URL SET: "+this.getClass()+" : "+url);
        this.prefetch();
    }

    public void prefetch() {
	URL u = null;
	try {
	    u = new URL(url);
	} catch (MalformedURLException e) {
	    return;
	}
				
	Toolkit tk=Toolkit.getDefaultToolkit();
	Image pic=tk.getImage(u);
	MediaTracker mt=new MediaTracker(canvas);
	/**
	mt.addImage(pic, 1);
	   try { 
	   mt.waitForID(1);
	   } catch (InterruptedException e) {
	   System.out.println(e);
	   }
	**/

	canvas.setImage(pic);
	// System.out.println("Image: "+pic);
	//canvas.setVisible(false);
	
	origWidth = pic.getWidth(canvas);
	origHeight = pic.getHeight(canvas);
		
	// Set the gfxComponent in the parent class 
	// - this is used to set the component visible
	//		gfxComponent = jlabel;
	
	// Return immediately with PREFETCH_COMPLETE event...
	
	//		SmilControllerEvent e = new SmilControllerEvent((Object)this, SmilControllerEvent.PREFETCH_COMPLETE_EVENT);
	//		notifyListeners(e);
    }

	public void setContainer(Container container) {
		this.container = container;
	}

	public void play() {
	    if (container != null) {
		//System.out.println("HAVI DRAWING IS:"+ container.class);
		canvas.setLocation(x,y);
		canvas.setSize(width, height);
		container.add(canvas, 0);
		//canvas.setBounds(x,y,width,height);		
		canvas.setVisible(true);
		//canvas.repaint();
	    } 
	    
	    // Media ends immediately for static media (almost..).
	    // Media ended - inform the SMIL player.
	    if (mediaListener != null) {
		Thread t = new Thread(this);
		t.start();
	    }
	    
	}

    public void run() {
	try {
	    Thread.sleep(100);
	} catch (InterruptedException e) {
	}
	// Media ended immediately (almost...)
	if (mediaListener != null)
	    mediaListener.mediaEnded();
    }

    public void pause() {
    }

    public void stop() {
	canvas.setVisible(false);
	//canvas.getParent().repaint();
    }
	
    public void setBounds(int x, int y, int width, int height) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	// AWT cannot cope with size setting before adding to a container
	if (canvas.isVisible() == true)
	    canvas.setBounds(x,y,width, height);
    }

    public void close() {
	mediaListener = null;
    }

    /**
     * This moves the time position in media. Not effective for this media.
     * @param millisecs		Time in millisecs
     */
    public void setMediaTime(int millisecs) {
    }
	 
    /**
     * Get the duration of media. Only applicable for continuous media (audio, video).
     * @return The duration of media in millisecs.
     */
    public int getOriginalDuration() {
	return 0;
    }
    
    /** 
     * Get the real width of the media.
     */
    public int getOriginalWidth() {
		return origWidth;	
    }
    
    /** 
     * Get the real height of the media.
     */
    public int getOriginalHeight() {
	return origHeight;	
    }

    /**
     * Set the sound volume for media. Only applicable for sound media formats.
     * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
     * dB change in signal level = 20 log10(percentage / 100) )
     */
	 public void setSoundVolume(int percentage) {
	 }
    
    public void addMediaListener(MediaListener listener) {
	    mediaListener = listener;
    }
    

    /**
     * Simple class to draw images. This is required to scale the images.
     * (ImageIcon couldn't handle it)
     */
    public class PImageCanvas extends Component {
	int width = 0;
	int height = 0;
	Image image = null;

	public PImageCanvas() {
	}
	
	public void setImage(Image i) {
	    image = i;
            //this.invalidate();
            //this.repaint();
	}

	public void setSize(int w, int h) {
	    width = w;
	    height = h;
	    super.setSize(w, h);
	}

	
	public void paint(Graphics g) {
	    if (image != null){
		g.drawImage(image, 0, 0, this);
	    }	    
	    return;
	}  
    } 

}
