/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.media;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;

import java.awt.Font;
import java.awt.Component;
import java.awt.Color;
import java.util.Vector;
import java.util.Enumeration;
//import java.util.Iterator;
import java.util.Hashtable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
 
// For CSS2 - this JDK1.0 class uses its own simple CSS parser and no TextAttributes
//import com.steadystate.css.parser.CSSOMParser;
//import org.w3c.css.sac.InputSource;
//import org.w3c.dom.css.*;

import java.io.StringReader;
import java.lang.Float; 

import java.util.Map;
 
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;


 /**
  * Reads in a style string and formats a text component (font, color).
  *
  * @author Kari Pihkala
  * @author Mikko Honkala
  */
 public class RealCSSTextFormatter
 {
	// Style for this component
	protected CSSStyleDeclaration cssStyle;
        
        public RealCSSTextFormatter()
 	{
 	}
	
	public void setStyle(CSSStyleDeclaration style) {
            this.cssStyle=style;
        }

	public void formatComponent(Component comp)
	{
		if (cssStyle==null) return;
		Font font = getFont();
//		Log.debug("[Proxy]"+" FONTTI: "+cssStyle.toString()+" fontti:"+font);
		//
		// Set the style
		if (comp != null) {
			Color color, bgcolor;

			comp.setFont(font);
			
			// get color property
			CSSValue colorVal = getProperty("color");
			if (colorVal != null) {
				color = ColorConverter.hexaToRgb(colorVal.getCssText());
				comp.setForeground(color);
			}
			
			// get background-color property
			CSSValue bgcolorVal = getProperty("background-color");
			if (bgcolorVal != null) {
			    //Log.debug("bgcolor:"+bgcolorVal.getCssText());
				bgcolor = ColorConverter.hexaToRgb(bgcolorVal.getCssText());
				comp.setBackground(bgcolor);	
				//comp.setOpaque(true);
			}
		}
	}
	
	/**
	 * Returns the specified property's CSSValue.
	 * @param property 		The name of the property to return.
	 * @return 				The specified property's CSSValue.
	 */
	public CSSValue getProperty(String property) {
	  CSSValue v = null;
	
	  if (cssStyle != null) {
	    v = cssStyle.getPropertyCSSValue(property);
	  }

	  return v;
	}

	
	/**
	 * Returns the Font accoring to the cssStyle variable.
	 * @return The Font that should be used when painting this element.
	 */
	public Font getFont() {
	
	  Float fontSize = new Float(12);     // default size 12px
	  String fontFamily = "tt7268m_";
	  int fontStyle = Font.BOLD;//TextAttribute.POSTURE_REGULAR; 
	  Float fontWeight = new Float(0);//TextAttribute.WEIGHT_REGULAR;
	  Float fontStretch = new Float(0);//TextAttribute.WIDTH_REGULAR;
	  boolean underline = false;
	  boolean strikeThrough = false;
	  boolean overline = false;
	
	  // get font-size property
	  CSSValue sizeVal = getProperty("font-size");
	  if (sizeVal != null) {
	      //Log.debug("Font Size: "+sizeVal.getCssText());
	  	  String sizeStr = sizeVal.getCssText();
		  // Pixels
	  	  if (sizeStr.endsWith("px")) {
		  	try {
				fontSize = new Float(sizeStr.substring(0, sizeStr.length()-2));
		  	} catch (NumberFormatException e) {
		  	}

	  	  }
	  }
	
	  // get font-family property
	  CSSValue familyVal = getProperty("font-family");
	  if (familyVal != null) {
	      //Log.debug("Font Family: "+familyVal.getCssText());
//	    String[] supportedFamilies = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	    String[] supportedFamilies = { "Arial", "Times New Roman", "Impact", "Helvetica", "Courier", "Webdings" };
	    if (familyVal.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE) {
	      fontFamily = familyVal.getCssText();
	      fontFamily = fontFamily.replace('\"', ' ');
	      fontFamily = fontFamily.trim();
	      if (fontFamily.equalsIgnoreCase("Courier")) {
	        fontFamily = "Courier New";
	      } else if (fontFamily.indexOf("Times") != -1 && fontFamily.indexOf("Roman") != -1) {
	        fontFamily = "Times New Roman";
	      }
	      boolean supported = false;
	      for (int i = 0; i < supportedFamilies.length; i++) {
	        if (fontFamily.equalsIgnoreCase(supportedFamilies[i])) {
	          supported = true;
	          break;
	        }
	      }
	      if (!supported) {
	        fontFamily = "Default";
	      }
	
	   } else if (familyVal.getCssValueType() == CSSValue.CSS_VALUE_LIST) {
	      CSSValueList familyList = (CSSValueList)familyVal;
	      int numItems = familyList.getLength();
	      String familyString = "";
	      if (numItems > 0) {
	        for (int i = 0; i < numItems; i++) {
	          CSSValue family = familyList.item(i);
	          familyString = family.getCssText();
	          familyString = familyString.replace('\"', ' ');
	          familyString = familyString.trim();
	          if (familyString.equalsIgnoreCase("Courier")) {
	            familyString = "Courier New";
	          } else if (familyString.indexOf("Times") != -1 && familyString.indexOf("Roman") != -1) {
	            familyString = "Times New Roman";
	          }
	          boolean supported = false;
	          for (int j = 0; j < supportedFamilies.length; j++) {
	            if (familyString.equalsIgnoreCase(supportedFamilies[j])) {
	              supported = true;
	              break;
	            }
	          }
	          if (supported) {
	            break;
	          } else {
	            familyString = "";
	          }
	        }
	      }
	      if (familyString.length() > 0) {
	        fontFamily = familyString;
	      }
	    }
	  }

	  // get font-style property
	  CSSValue styleVal = getProperty("font-style");
	  if (styleVal != null) {
	      //Log.debug("Font Style: "+styleVal.getCssText());
	    String styleString = styleVal.getCssText();
	    if (styleString.equalsIgnoreCase("italic") || styleString.equalsIgnoreCase("oblique")) {
	      fontStyle = Font.ITALIC; //new Float(((float)Font.ITALIC)); //TextAttribute.POSTURE_OBLIQUE;
	    }
		  if (styleString.equalsIgnoreCase("normal")) {
		    fontStyle = Font.PLAIN; //new Float(((float)Font.PLAIN)); //TextAttribute.POSTURE_OBLIQUE;
		  }
	  }
	
	  // get font-weight property
	  CSSValue weightVal = getProperty("font-weight");
	  if (weightVal != null) {
	      //Log.debug("Font Weight: "+weightVal.getCssText());
	     String weightString = weightVal.getCssText();
	    if (weightString.equalsIgnoreCase("bold")) {
	      //fontWeight = TextAttribute.WEIGHT_BOLD;
		  fontStyle = Font.BOLD;
	    } else if (!weightString.equals("normal") && !weightString.equals("all")) {
	      int fontWeightInt = Integer.parseInt(weightString);
	      switch (fontWeightInt) {
	        case 100: break; //fontWeight = TextAttribute.WEIGHT_EXTRA_LIGHT;  break;
	        case 200: break; //fontWeight = TextAttribute.WEIGHT_LIGHT; break;
	        case 300: break; //fontWeight = TextAttribute.WEIGHT_REGULAR; break;
	        case 400: break; //fontWeight = TextAttribute.WEIGHT_SEMIBOLD; break;
	        case 500: break; //fontWeight = TextAttribute.WEIGHT_MEDIUM; break;
	        case 600: break; //fontWeight = TextAttribute.WEIGHT_DEMIBOLD; break;
	        case 700: fontStyle = Font.BOLD; break; //fontWeight = TextAttribute.WEIGHT_BOLD; break;
	        case 800: fontStyle = Font.BOLD; break; //fontWeight = TextAttribute.WEIGHT_EXTRABOLD; break;
	        case 900: fontStyle = Font.BOLD; break; //fontWeight = TextAttribute.WEIGHT_ULTRABOLD; break;
	      }
	    }
	  }
	
	  // it seems that any font weight that isn't bold or regular is treated as regular
	  // make sure that any font that is supposed to be at least bold is drawn bold
//	  if (fontWeight.floatValue() > TextAttribute.WEIGHT_BOLD.floatValue()) {
//	    fontWeight = TextAttribute.WEIGHT_BOLD;
//	  }
	
	  // get font-stretch property
//	  CSSValue stretchVal = getProperty("font-stretch");
//	  if (stretchVal != null) {
//	    Log.debug("Font Stretch: "+stretchVal.getCssText());
//	    String stretchString = stretchVal.getCssText();
//	    if (stretchString.equalsIgnoreCase("ultra-condensed")) {
//	      fontStretch =  new Float(0.5);
//	    } else if (stretchString.equalsIgnoreCase("extra-condensed")) {
//	      fontStretch =  new Float(0.625);
//	    } else if (stretchString.equalsIgnoreCase("condensed")) {
//	      fontStretch =  new Float(0.75);
//	    } else if (stretchString.equalsIgnoreCase("semi-condensed")) {
//	      fontStretch =  new Float(0.875);
//	    } else if (stretchString.equalsIgnoreCase("semi-expanded")) {
//	      fontStretch =  new Float(1.125);
//	    } else if (stretchString.equalsIgnoreCase("expanded")) {
//	      fontStretch =  new Float(1.25);
//	    } else if (stretchString.equalsIgnoreCase("extra-expanded")) {
//	      fontStretch =  new Float(1.375);
//	    } else if (stretchString.equalsIgnoreCase("ultra-expanded")) {
//	      fontStretch =  new Float(1.5);
//	    }
//	  }
	
	  // get text-decoration property
//	  CSSValue decorVal = getProperty("text-decoration");
//	  if (decorVal != null) {
//	    Log.debug("Font Decoration: "+decorVal.getCssText());
//	    String decorString = decorVal.getCssText();
//	    if (decorString.equalsIgnoreCase("underline")) {
//	      underline = true;
//	    } else if (decorString.equalsIgnoreCase("overline")) {
//	      overline = true;
//	    } else if (decorString.equalsIgnoreCase("line-through")) {
//	      strikeThrough = true;
//	    }
//	  }
	
//	  Font font = new Font("Serif", Font.PLAIN, 1);
	  Font font = new Font(fontFamily, (int)fontStyle, (int)fontSize.floatValue());
//	  Map fontAttributes = font.getAttributes();
//	  fontAttributes.put(TextAttribute.FAMILY, fontFamily);
//	  fontAttributes.put(TextAttribute.POSTURE, fontStyle);
	  // java seems to render fonts too small need to increase by 1.3?? times - NOT compared to IE6.0/NS6.0
//	  fontSize = new Float((float)(fontSize.floatValue()));
//	  fontAttributes.put(TextAttribute.SIZE, fontSize);
//	  fontAttributes.put(TextAttribute.WEIGHT, fontWeight);
//	  fontAttributes.put(TextAttribute.WIDTH, fontStretch);
//	  if (underline) {
//	    fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
//	  }
//	  if (overline) {  // no overline support??
//	   //  fontAttributes.put(TextAttribute.OVERLINE, TextAttribute.OVERLINE_ON);
//	  }
//	  if (strikeThrough) {
//	    fontAttributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
//	  }
//	
//	  font = new Font(fontAttributes);

	  return font;
	}

     /**
	public Font deriveFont(Font orig,int style,float size)
	{
	    // I had to take it awy because is not part of JDK1.1
	    //return orig.deriveFont(style,size);
	}
     **/
	


 }
