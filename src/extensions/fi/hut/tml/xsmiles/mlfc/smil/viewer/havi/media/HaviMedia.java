/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.media;

import java.awt.Container;
import java.net.URL;

import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.gui.media.general.Media;

/**
 * This is the interface for binary media. The methods should be called in the following
 * order: <ol>
 * <li>setUrl(url to media)
 * <li>setContainer(myContainer)  [not necessary for audio]
 * <li>setBounds(0,0, 100, 100)
 * <li>addMediaListener(listener) [optional]
 * <li>prefetch()
 * <li>play()
 * <li>pause() [optional]
 * <li>play()
 * <li>close()
 * </ol>
 */
public abstract class HaviMedia {
    /**
     * Requests the media player to display a control panel for media.
     * For audio and video, these can be a volume/play/stop controls,
     * for images, these can be zoom controls etc.
     * The controls are GUI dependent, generated through ComponentFactory.
     * @param visible	true=Display controls, false=don't display controls.
     */
    public void showControls(boolean visible)
    {
    }
    
    abstract public  void setUrl(String URL);
    /*
    {
        Log.error("NEVER CALL SETURL ON HAVIMEDIA");
    }*/
    /** 
     * Sets the URL for this media. This method will only set the URL for the media.
     * To actually download the data, prefetch() or play() should be called.
     * @param url		URL for media
     */
    public void setUrl(URL url)
    {
        String urlString = url.toString();
        Log.debug("setURL:  "+urlString +this.getClass());
        this.setUrl(urlString);
        //super.setURurl);
    }

    public void abort(){

    }
    
    /**
     * All traffic to the browser, such as ComponentFactory, etc goes through this listener.
     * If no listener supplied media players should still function with some basic level. 
     * 
     * @param listener The MLFCListener supplied by the browser
     * @see MLFCListener
     */
    public void setMLFCListener(MLFCListener listener)
    {
        Log.debug("setMLFCListener not implemented");
    }
}	
