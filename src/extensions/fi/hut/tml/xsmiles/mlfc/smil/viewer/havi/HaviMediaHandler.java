/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.media.ImageMedia;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.media.TextMedia;
import fi.hut.tml.xsmiles.gui.media.general.JMFMedia;

import fi.hut.tml.xsmiles.gui.media.general.CSSStylableMedia;


import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.XLink;

import java.net.URLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.File;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;

import org.w3c.dom.Element;
import org.w3c.dom.css.CSSStyleDeclaration;
import fi.hut.tml.xsmiles.Log;

import javax.media.*;
import javax.media.format.*;
import com.sun.media.util.Registry;
import jmfkaffe.MergeServerRenderer;
import jmfkaffe.JMFKaffe;

/**
 *  Interface to media.
 */
public class HaviMediaHandler implements MediaHandler, ActionListener, CSSStylableMedia
{
    
    protected String alt, url;
    protected MediaListener mediaListener;
    protected HaviDrawingArea drawingArea;
    protected Container container;
    protected String documentURL;
    protected boolean prefetched = false;
    protected boolean playing = false;
    protected Media media = null;
    
    // Overridden MIME Type for this media. Can be set from MediaElement, if type attr is set.
    protected String mimeType = null;
    
    protected CSSStyleDeclaration CSSStyle ;
    
    /**
     * Create a new MediaHandler for MLFC.
     * @param documentURL		The URL for smil document path.
     */
    public HaviMediaHandler()
    {
        this.documentURL = null;
    }
    
    /**
     * Create a new MediaHandler for Swing.
     * @param documentURL		The URL for smil document path.
     */
    public HaviMediaHandler(String documentURL)
    {
        this.documentURL = documentURL;
    }
    
    public SMILDocumentImpl smilDoc = null;
    public Viewer viewer = null;
    
    public void setViewer(Viewer v)
    {
        viewer = v;
        smilDoc = (SMILDocumentImpl)(viewer.getSMILDoc());
    }
    
    /**
     * Set the drawing area. The given drawing area MUST be SwingDrawingArea.
     */
    public void setDrawingArea(DrawingArea d)
    {
        // The given drawing areaa MUST be HaviDrawingArea
        drawingArea = (HaviDrawingArea)d;
        container = drawingArea.getContainer();
        if (media != null)
            media.setContainer(container);
    }
    
    /**
     * Add a media listener (currently supports only one listener)
     */
    public void addListener(MediaListener mediaListener)
    {
        this.mediaListener = mediaListener;
        
        // Try to add listeners to this media, if it has already been prefetched.
        if (media != null)
            media.addMediaListener(mediaListener);
    }
    
    /**
     * Checks if this media is static or continuous.
     * @return true	if media is static.
     */
    public boolean isStatic()
    {
        if (media != null)
            return media.isStatic();
        else
            return false;
    }
    
    /**
     * This media handler will know the rootlayout size after this is called.
     * @param width		RootLayout width
     * @param height	RootLayout height
     */
    public void setRootLayoutSize(int width, int height)
    {
    }
    
    public void setAlt(String alt)
    {
        this.alt=alt;
    }
    /** (CSSStylableMedia interface) set the resolved CSS style for this element */
    public void setStyle(CSSStyleDeclaration style)
    {
        this.CSSStyle=style;
    }
    public void setURL(String url)
    {
        Log.debug(this.getClass()+".setURL: "+url);
        if (viewer instanceof SMILMLFC)
        {
            url = ((SMILMLFC)viewer).createURL(url).toString();
        }
        this.url = url;
        try
        {
            if (media!=null) media.setUrl(new URL(url));
        } catch (MalformedURLException e)
        {
            Log.error(e);
        }
    }
    
    public void setMIMEType(String type)
    {
        mimeType = type;
    }
    
    public void prefetch()
    {
        int lastDot = 0;
        if (url == null)
        {
            return;
        }
        //System.out.println("URL:" +url);
        // This is RealPlayer's cache http protocol, use it as normal http protocol
        if (url.startsWith("chttp") == true)
        {
            url = url.substring(1);
        }
        
        if (url.startsWith("data"))
        {
            media = new TextMedia();
        } else
            // Check the type of the url
            try
            {
                // Check the type of the url
                if (viewer instanceof SMILMLFC)
                {
                    url = ((SMILMLFC)viewer).createURL(url).toString();
		    //System.out.println("We are in here");
                }
                else
                {
                    if (!url.startsWith("http") && !url.startsWith("data") && !url.startsWith("file")){
                        url = documentURL+'/'+url;
			//System.out.println("We are in here1");
		    }
                }
                if (!url.startsWith("http") && !url.startsWith("data") && !url.startsWith("file")){
                    url = "file:"+url;
		    //System.out.println("We are in here2");
		}
                //System.out.println("URL:"+url);
                URL u = new URL(url);
                URLConnection urlConn = u.openConnection();
		//System.out.println("Loading from URL: " + urlConn.getURL());
                String urltype = urlConn.getContentType();
		if (urltype==null) 
		    {
			Log.debug("ContentType was null");
			urltype="content/unknown";
		    }

                // Override MIME type?
                if (mimeType != null)
                    urltype = mimeType;
                
                try
                {
                    lastDot = url.lastIndexOf('.')+1;
                } catch(Exception allExp)
                {
                    // This is meant to catch 'out of index' exceptions
                    lastDot = 0;
                }

                /*
                // url ends with ".ogg"
                if (url.substring(lastDot).equals("ogg"))
                {
                    return;
                }
                
                // url ends with ".svg"
                if (url.substring(lastDot).equals("svg"))
                {
                    System.out.println("Creating svg proxy for type:"+urltype);
                }
                // url ends with ".fo"
                else if (url.substring(lastDot).equals("fo"))
                {
                    System.out.println("Creating svg proxy for type:"+urltype);
                }*/
                
                // "image/gif" "image/jpeg"
                if (urltype.startsWith("image")||url.endsWith(".jpg")||url.endsWith(".gif")||url.endsWith(".png"))
                {
                    //System.out.println("IMAGE FUNCIONA");
                    if (viewer.getPlayImage() == true)
                    {
                        media = new ImageMedia();
                    } else
                    {
                        media = new TextMedia();
                        ((TextMedia)media).setText(alt);
                    }
                }
                
                // "text/plain"
                else if (urltype.startsWith("text"))
                {
                    // Always create textproxy - no alt texts
                    media = new TextMedia();
                }
                
                // "audio/*"
                else if (urltype.startsWith("audio"))
                {
                    if (viewer.getPlayAudio() == true)
                    {
                        media = new JMFMedia();
                    } else
                    {
                        media = new TextMedia();
                        ((TextMedia)media).setText(alt);
                    }
                }
                // TODO: all media could be handled with X-Smiles media framework
                // just do not have time to convert them all now
                // if this is another xml document, try to open it with X-Smiles core
                else if (this.shouldBeHandledByXSmiles(url,urltype))
                {
                    // Check the type of the url
                    try {			
                        Log.info("This URL is an X-Smiles handled url:  "+url+". Trying to open with ContentHandler framework.");
                        XLink link = new XLink(u);
                        
                        // Override MIME type?
                        if (mimeType != null)
                            link.setMIMEOverride(mimeType);
                        SMILMLFC mlfc = (SMILMLFC)viewer;
                        XSmilesContentHandler c = mlfc.getMLFCListener().createContentHandler(link,null,false);
                        media = (Media)c;
                    } catch (Exception io) {
                        Log.error(io,"URL type error: ");
                    }
                }
                // others... (video)
                else
                {
                    if (viewer.getPlayVideo() == true)
                    {
			media = new JMFMedia();
                    } else
			{
			    media = new TextMedia();
			    ((TextMedia)media).setText(alt);
			}
                }
            }  catch (Exception ex)
            {
                // This was prefetched with an error...
                //System.out.println("Error:");
                Log.error(ex, "while trying to open "+url);
                prefetched = true;
                return;
            }
    //System.out.println("MEDIA FUNCIONA");
        if (media != null)
        {
            if (CSSStyle!=null && media instanceof CSSStylableMedia)
            {
                ((CSSStylableMedia)media).setStyle(CSSStyle);
            }
            try
            {
	            media.setUrl(new URL(url));
	            media.prefetch();
	            // Add all listeners to this media, now as it has been created.
	            media.addMediaListener(mediaListener);
            } catch (Exception e)
            {
                Log.error(e);
            }
        }
        prefetched = true;
    }
    
    public boolean shouldBeHandledByXSmiles(String url, String mime)
    {
        if (url.endsWith(".smil") || url.endsWith(".xml") || url.endsWith("html")||url.endsWith("xhtml")) return true;
        return false;
    }
    
    public void play()
    {
        //System.out.println("PLAY MEDIA HANDLER");
        if (container == null)
        {
            return;
        }
        
        // If this media hasn't yet been prefetched, then prefetch it.
        if (prefetched == false)
            prefetch();
        
        if (media != null)
        {
            try
            {
	            media.setContainer(container);
	            media.setBounds(left, top, width, height);
	            //System.out.println("Set Bounds:" + left + top + width + height);
	            media.play();
            } catch (Exception e)
            {
                Log.error(e);
            }
        }
        playing = true;
    }
    
    public void pause()
    {
    }
    
    public void stop()
    {
        playing = false;
        if (container != null && media != null)
            media.stop();
        
    }
    public void freeze()
    {
        if (container != null && media != null)
            media.pause();
        // The media will not be restarted - only paused to get it frozen
    }
    
    public void close()
    {
        playing = false;
        if (media != null)
            media.close();
        
        viewer = null;
        smilDoc = null;
        container = null;
        mediaListener = null;
        
        media = null;
    }
    
    /**
     * Set the media time position. Usually used to skip the beginning of media.
     * @param millisecs			Time in milliseconds
     */
    public void setMediaTime(int millisecs)
    {
        //	if (media != null)
        //		media.setMediaTime(millisecs);
    }
    
    int top = 0, left = 0, width = 100, height = 100;
    public int getTop()
    {
        return top;
    }
    public int getLeft()
    {
        return left;
    }
    public int getWidth()
    {
        return width;
    }
    public int getHeight()
    {
        return height;
    }
    public void setBounds(int x, int y, int w, int h)
    {
        left = x;
        top = y;
        width = w;
        height = h;
        if (media != null)
        {
            media.setContainer(container);
            media.setBounds(left, top, width, height);
        }
    }
    
    /**
     * Get the real width of the media.
     */
    public int getOriginalWidth()
    {
        if (media != null)
            return media.getOriginalWidth();
        else
            return -1;
    }
    
    /**
     * Get the real height of the media.
     */
    public int getOriginalHeight()
    {
        if (media != null)
            return media.getOriginalHeight();
        else
            return -1;
    }
    
    /**
     * Set the volume of audio (if available).
     * @param percentage      0-100-oo , 100 giving normal sound level.
     */
    public void setAudioVolume(int percentage)
    {
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if (mediaListener != null)
            mediaListener.mediaEnded();
    }
    
    public Object getComponent()
    {
        return null;
    }
    
    
    
}

