/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
//import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFCHavi;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Decorator;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCForeignHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.basic.TimeChildList;
import fi.hut.tml.xsmiles.mlfc.smil.basic.ElementBasicTimeImpl;


import org.w3c.dom.smil20.SMILDocument;
import org.w3c.dom.smil20.*;
import org.w3c.dom.Node;

import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;

import org.havi.ui.HText;
import org.havi.ui.HGraphicButton;
import org.havi.ui.HScene;
import org.havi.ui.HSinglelineEntry;
import org.havi.ui.HNavigable;
import org.havi.ui.HTransparentLook;
import org.havi.ui.HInvalidLookException;
import org.havi.ui.event.HActionListener;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;

/**
 *  Implements links in Swing. 
 */
public class HaviForeignHandler extends SMILMLFCForeignHandler {
    
    public HaviForeignHandler() {
        super();
    }
    public HaviForeignHandler(Element elem) {
        super(elem);
    }
    

    public void play() {
        super.play();
        Decorator dec = viewer.getDecorator();
        if (dec!=null && dec instanceof FocusableLinkHandler)
        {
            if (visualComponent instanceof HNavigable)
            {
                ((FocusableLinkHandler)dec).addFocusableLink((HNavigable)visualComponent);
            } else if (visualComponent instanceof Container)
            {
                this.addFocusableRecursive((Container)visualComponent,(FocusableLinkHandler)dec);
            }
        }
        this.container.repaint();
        // TODO: register component to viewer
    }
    
    protected void addFocusableRecursive(Container container,FocusableLinkHandler fhandler)
    {
        for (int i=0;i<container.getComponentCount();i++)
        {
            Component comp = container.getComponent(i);
            if (comp instanceof HNavigable && comp.isFocusTraversable())
            {
                fhandler.addFocusableLink((HNavigable)comp);
            }
        }
    }
}

