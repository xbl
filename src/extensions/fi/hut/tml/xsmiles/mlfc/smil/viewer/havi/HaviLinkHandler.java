/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
//import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFCHavi;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Decorator;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.basic.TimeChildList;
import fi.hut.tml.xsmiles.mlfc.smil.basic.ElementBasicTimeImpl;


import org.w3c.dom.smil20.SMILDocument;
import org.w3c.dom.smil20.*;
import org.w3c.dom.Node;

import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;

import org.havi.ui.HText;
import org.havi.ui.HGraphicButton;
import org.havi.ui.HScene;
import org.havi.ui.HSinglelineEntry;
import org.havi.ui.HNavigable;
import org.havi.ui.HTransparentLook;
import org.havi.ui.HInvalidLookException;
import org.havi.ui.event.HActionListener;

import fi.hut.tml.xsmiles.Log;

/**
 *  Implements links in Swing. 
 */
public class HaviLinkHandler extends HaviMediaHandler implements LinkHandler, MouseListener, HActionListener {

    protected Component linkComp;
    protected String linkTitle = null;
    protected SMILDocumentImpl smilDoc = null;
    protected Viewer viewer = null;
    
    public HaviLinkHandler() {
    }
    
    public void setTitle(String title) {
	if (title == null || title.length() == 0)
	    linkTitle = null;
	else
	    linkTitle = title;
    }	

    public void play() {
	if (container == null) {
	    //Log.debug("No drawing area.container for link "+url);
	    return;
	}
	HGraphicButton link = new HGraphicButton();
	HTransparentLook look = new HTransparentLook();
	try {
	    link.setLook(look);
	} catch (HInvalidLookException e) {
	    System.out.println("oh no! Look!");
	}
	link.setActionCommand("link");
	link.addHActionListener(this);
	linkComp = link;
	linkComp.setBounds(left, top, width, height);
	container.add(linkComp, 0);
	linkComp.setEnabled(true);
	linkComp.setVisible(true);
        // TODO:
	Decorator dec = viewer.getDecorator();
	if (dec!=null && dec instanceof FocusableLinkHandler)
	    ((FocusableLinkHandler)dec).addFocusableLink(link);
    }

    public void pause() {
    }

    public void stop() {
	if (container != null && linkComp != null) {
	    linkComp.setVisible(false);
	    container.remove(linkComp);
	}

	((FocusableLinkHandler)viewer.getDecorator()).removeFocusableLink((HNavigable)linkComp);
	
    }
    
    public void close() {
	((FocusableLinkHandler)viewer.getDecorator()).removeFocusableLink((HNavigable)linkComp);
	super.close();
    }
    
    public void mouseEntered(MouseEvent e) {
	viewer.displayStatusText(url);
    }
    public void mouseExited(MouseEvent e) {
    }
    public void mousePressed(MouseEvent e) {
    }
    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
	linkClicked(); 
    }

    public void linkClicked() {
	if (mediaListener != null)
	    mediaListener.mouseClicked(null);
    }

    public void setViewer(Viewer v) {
	viewer = v;
	smilDoc = (SMILDocumentImpl)(viewer.getSMILDoc());
    }

    /**
     * Playback buttons.
     */
    public void actionPerformed(ActionEvent e) {
	System.out.println("Link Action: "+e.getActionCommand());
	
	String text = e.getActionCommand();
	if (text.equals("link")) {
	    linkClicked(); 
	}
    }
    
}

