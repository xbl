/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.io.Reader;
import java.io.StringReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.MalformedURLException;
import java.awt.Font;
import java.awt.MediaTracker;



import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;



import org.w3c.dom.Document;

 // This will import JMF Manager
import javax.media.Manager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import org.havi.ui.HContainer;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;
import org.havi.ui.HVisible;
import org.havi.ui.HContainer;
import org.havi.ui.HDefaultTextLayoutManager;
import org.havi.ui.HStaticText;
import org.havi.ui.HStaticIcon;
import org.havi.ui.HText;
import org.havi.ui.HNavigable;
import org.havi.ui.HGraphicButton;
import org.havi.ui.HScene;
import org.havi.ui.HSinglelineEntry;
import org.havi.ui.event.HActionListener;
import org.havi.ui.event.HTextListener;
import org.havi.ui.event.HTextEvent;
import org.havi.ui.event.HFocusEvent;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;

import java.awt.Panel;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.BrowserHavi;

import fi.hut.tml.xsmiles.event.GUIEventAdapter;
import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Vector;
import java.util.Enumeration;

import fi.hut.tml.xsmiles.protocol.XURLStreamHandlerFactory;
import fi.hut.tml.xsmiles.protocol.http.XHttpURLConnection;

import javax.media.*;
import javax.media.format.*;
import com.sun.media.util.Registry;
import jmfkaffe.MergeServerRenderer;
import jmfkaffe.JMFKaffe;

//import sdl.core.Main;

/**
 * This is a sample class that shows how to view a smil document and
 * play it using HAVi.
 * <p>
 * NOTE: THIS VIEWER IS NOT COMPLETE AND WON'T WORK CORRECTLY.
 */
public class BrowserViewer extends SMILViewer implements Viewer, HActionListener, HTextListener {
//public class SMILViewer extends Frame implements Viewer, HActionListener {

    
    protected BrowserWindow browserWindow;
    protected Container contentArea;
    static
    {
        Utilities.setJavaVersion(1.1f);
                    //  Create a new instance of our application's frame, and make it visible.
            java.net.URL.setURLStreamHandlerFactory(new XURLStreamHandlerFactory());

    }
    
    public BrowserViewer(String start) {
        super(); // this will create the GUI
        if (start != null) starturl=start;
	else
            {
                //starturl = "file:///home/honkkis/src/xsmiles/demo/smil/xforms/calculator2.smil";
                //starturl = "file:///home/honkkis/src/xsmiles/demo/smil/car/demotv.smil";
                //starturl = "file:///home/honkkis/src/ubik/build/smil/univnew/university.smil";
                //starturl = "http://www.hut.fi/~kpihkala/smils.smil";
                //starturl="file:/home/honkkis/src/xsmiles/demo/smil/xforms/animated/phoneordertv.smil";
                
		try
		    {
			//starturl = "file:../../xsmiles/demo/smil/menutv.smil";
			
			starturl = new java.io.File("../../xsmiles/demo/smil/menutv.smil").getAbsolutePath().toString();
			URL thisURL = new URL("file:/.");
			URL startURL = new URL(thisURL, starturl);
			starturl = startURL.toString();
		    } catch (MalformedURLException e)
			{
			    Log.error(e);
			}
                
            }
	
	
        this.createContentArea();
        //this.testComponent();
        this.createFocusInfoThread();

        /** JMF patch for Kaffe **/
       	new JMFKaffe();
	Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true));
	Registry.set("allowLogging", new Boolean(true));
	Registry.set("secure.logDir", "/home/pcesar/src/ubik/build");
	PlugInManager.removePlugIn("jmfkaffe.MergeServerRenderer",PlugInManager.RENDERER);
	PlugInManager.addPlugIn("jmfkaffe.MergeServerRenderer",(new MergeServerRenderer()).getSupportedInputFormats(),null,PlugInManager.RENDERER);
	//MergeServerRenderer.removeRenderer();
	//MergeServerRenderer.removeHost();
	try{
	    Registry.commit();
	    PlugInManager.commit();
	}catch(Exception e){
	    e.printStackTrace();
	}
	/** **/
	
        //this.addKeyListener(this);
        //this.addFocusListener(this);
    }	
    protected String starturl;
    
    private void testComponent()
    {
        Component c;
            fi.hut.tml.xsmiles.gui.components.havi.HaviSelectBoolean select= new fi.hut.tml.xsmiles.gui.components.havi.HaviSelectBoolean ();
            //fi.hut.tml.xsmiles.gui.components.havi.HaviCaption select= new fi.hut.tml.xsmiles.gui.components.havi.HaviCaption ("comp");
            fi.hut.tml.xsmiles.gui.components.havi.HaviCaption capt= new fi.hut.tml.xsmiles.gui.components.havi.HaviCaption ("test");
            capt.setText("testtest");
            fi.hut.tml.xsmiles.gui.components.havi.HaviLabelCompound compound= new fi.hut.tml.xsmiles.gui.components.havi.HaviLabelCompound(select,capt,"top");
            c = select.getComponent();
            compound.getComponent().setLocation(250,250);
            //compound.getComponent().setSize(250,250);
            //compound.getComponent().setVisible(true);
            //compound.getComponent().setEnabled(true);
            
            //c.setVisible(true);
            //c.setEnabled(true);
            //c.setSize(100,100);
            this.rootlayoutContainer.add(compound.getComponent());
            this.focusManager.addFocusable((HNavigable) c);
        
            /*HVisible guiexit;
            
            Image exitimagen = loadImage("images/exitnormal.gif");
	Image exitimagef = loadImage("images/exitfocused.gif");
	Image exitimagea = loadImage("images/exitactioned.gif");
	guiexit = new HGraphicButton(exitimagen);
	guiexit.setGraphicContent(exitimagef, HVisible.FOCUSED_STATE);
	guiexit.setGraphicContent(exitimagea, HVisible.ACTIONED_STATE);
	guiexit.setBackground(Color.lightGray);
     	guiexit.setEnabled(true);
        Component c=guiexit;*/
/*
            HVisible guiexit;
            
            Image exitimagen = loadImage("images/exitnormal.gif");
	Image exitimagef = loadImage("images/exitfocused.gif");
	Image exitimagea = loadImage("images/exitactioned.gif");
	guiexit = new org.havi.ui.HToggleButton(exitimagen);
	guiexit.setGraphicContent(exitimagef, HVisible.FOCUSED_STATE);
	guiexit.setGraphicContent(exitimagea, HVisible.ACTIONED_STATE);
	guiexit.setBackground(Color.lightGray);
     	guiexit.setEnabled(true);
        c=guiexit;
            c.setLocation(150,150);
            c.setVisible(true);
            c.setEnabled(true);
            c.setSize(100,100);
            this.rootlayoutContainer.add(c);
            this.focusManager.addFocusable((HNavigable) c);*/
    }

    protected GUIListener guiListener;
    protected void createContentArea()
    {
        try
	    {
		
		guiurl.setTextContent(starturl, HVisible.ALL_STATES);
		//browserWindow=new Browser(starturl,"initial");
		browserWindow=new BrowserHavi(starturl,"default",true,"nullGUIHavi");
		System.out.println("Created a browser window");
		Thread.sleep(30);
		this.contentArea = browserWindow.getContentArea();
		guiListener = new GUIListener();
		browserWindow.getGUIManager().getCurrentGUI().addGUIEventListener(guiListener);
            

		this.contentArea.setSize(this.rootlayoutContainer.getSize());
		//this.contentArea.setLocation(3,0);
		rootlayoutContainer.add(this.contentArea);
		this.contentArea.invalidate();
		this.rootlayoutContainer.validate();

		//System.out.println("Contentarea : "+contentArea+" : added to rootLayoutContainer: "+rootlayoutContainer);
		
	    } catch (Throwable t)
		{
		    System.err.println(t);
		    t.printStackTrace(System.err);
		}
    }
    
    public static void main(String[] args) {

	//		// !!!!PERFORMANCE TRACKING!!!!
	//System.gc();
	initTime = System.currentTimeMillis();
	initMem =Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
	System.out.println("H INIT TIME: "+SMILViewer.initTime+"ms MEMORY: "+(double)SMILViewer.initMem/1000+"k");
	//		// !!!!PERFORMANCE TRACKING!!!!
	
	Reader rd = null; 
	URL u = null;
	String starturl = null;//"http://www.hut.fi/~kpihkala/smils.smil";
	
	
	if (args.length > 0) {
	    starturl=args[0];
	}
	
	try {
	    if (starturl!=null)
            {
                if (starturl.startsWith("http:") ||starturl.startsWith("file:")) 
                {
                    u = new URL(starturl);
                    BufferedReader in = new BufferedReader(new InputStreamReader(u.openStream()));
                    rd = in;
                } else {
                    System.out.println("The argument must be a URL!");
                    return;
                }
            }
	} catch (Exception e) {
	    System.out.println("File error"+e);
	}
	System.out.println(starturl);
	BrowserViewer cs = new BrowserViewer(starturl);
	
	//cs.openURL(starturl);
	//		// !!!!PERFORMANCE TRACKING!!!!
	System.out.println("PREFETCH INIT TIME: +"+((System.currentTimeMillis()-SMILViewer.initTime))+
			   "ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILViewer.initMem))/1000 +"k");
	System.gc();
	System.out.println("PREFETCH GC TIME: +"+((System.currentTimeMillis()-SMILViewer.initTime))+
			   "ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILViewer.initMem))/1000 +"k");
	//		// !!!!PERFORMANCE TRACKING!!!!

    }
    private void createFocusInfoThread()
    {
                            Thread t = new Thread()
        {
            public void run()
            {
                try {
                    for (int i=0;i<1000;i++)
                    {
                        Thread.sleep(5000);
                        Component currentfocus = getFocusOwner();
                        System.out.println("current focus: "+currentfocus+" peer:"+currentfocus.getPeer()+" isFocusable"+currentfocus.isFocusTraversable()+" isvisible"+currentfocus.isVisible());//+comp.getFocusable());
                        //System.out.println("peer is focusable: "+currentfocus.getPeer().isFocusable()+" lightweight peer:"+(currentfocus.getPeer() instanceof java.awt.peer.LightweightPeer));
                        //KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                        //Component keyfocus = manager.getFocusOwner();
                        //System.out.println("Key: focusOwner: "+keyfocus);
                        //if (comp!=null) comp.requestFocus();
                        //currentfocus = getFocusOwner();
                        //System.out.println(comp.toString()+" requested focus. current focus: "+currentfocus);
                    }
                } catch (Throwable t)
                {
                }
            }
        };
        t.start();    
    }

    public void Quit(){
	System.exit(0);
    }

    
        /**
     * Open ERROR doc at url, if errormsg != null.
     */
    protected void openURL(String url, String errormsg) {
      	guiback.processHFocusEvent(new HFocusEvent(guiback, HFocusEvent.FOCUS_GAINED));	
        browserWindow.openLocation(url);
    }

        /**
     * Playback buttons.
     */
    public void actionPerformed(ActionEvent e) {
     	String text = e.getActionCommand();
	System.out.println("Action: "+text);

	if (text.equals("back")) {
            this.browserWindow.navigate(NavigationState.BACK);
	}
        else super.actionPerformed(e);
        
        
        // testing diff
    }
    
    public class GUIListener extends GUIEventAdapter
    {
        public  void setLocation(String s) {
            Log.debug("Location changed: "+s);
            guiurl.setTextContent(s,HVisible.ALL_STATES);
        }
    }



    


    
}

