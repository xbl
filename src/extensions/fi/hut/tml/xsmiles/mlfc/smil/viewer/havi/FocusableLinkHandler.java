/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import org.havi.ui.HNavigable;

/**
 */
public interface FocusableLinkHandler 
{
    
    /**
     * Add a link component to a focus list.
     * @param	link	Link component, HNavigable
     */
    public void addFocusableLink(HNavigable link) ;
    /**
     * Remove a link component from a focus list. Causes the whole focus path to be recalculated.
     * @param	link	Link component, HNavigable
     */
    public void removeFocusableLink(HNavigable link);

}

