/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.io.Reader;
import java.io.StringReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.MalformedURLException;
import java.awt.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;


// This will import JMF Manager
import javax.media.Manager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import org.havi.ui.HContainer;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;
import org.havi.ui.HVisible;
import org.havi.ui.HContainer;
import org.havi.ui.HDefaultTextLayoutManager;
import org.havi.ui.HStaticText;
import org.havi.ui.HStaticIcon;
import org.havi.ui.HText;
import org.havi.ui.HTextButton;
import org.havi.ui.HNavigable;
import org.havi.ui.HGraphicButton;
import org.havi.ui.HScene;
import org.havi.ui.HContainer;
import org.havi.ui.HSinglelineEntry;
import org.havi.ui.event.HActionListener;
import org.havi.ui.event.HTextListener;
import org.havi.ui.event.HTextEvent;
import org.havi.ui.event.HFocusEvent;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;

import java.awt.Panel;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.havi.HaviCaption;
import fi.hut.tml.xsmiles.gui.components.havi.HaviLabelCompound;


import java.awt.Image;
import java.awt.Toolkit;
import java.util.Vector;
import java.util.Enumeration;

import javax.swing.*;

//import sdl.core.Main;

/**
 * This is a sample class that shows how to view a smil document and
 * play it using HAVi.
 * <p>
 * NOTE: THIS VIEWER IS NOT COMPLETE AND WON'T WORK CORRECTLY.
 */
public class ContainerTest extends HScene implements KeyListener
{
    
    // Original size: 640x480, application area 420x360
    public static final int screenWidth = 640, screenHeight = 480, screenDepth = 16;
    // Top icon bar height
    public static final int iconHeight = 60;
    
    // Link focus list
    public static void main(String[] args)
    {
        ContainerTest test = new ContainerTest();
    }
    public static HaviFocusManager focusManager=new HaviFocusManager();
    
    
    protected Container rootContainer; // this is usually this
    protected Container rootlayoutContainer = null;
    //protected Main main;
    
    public ContainerTest()
    {
        this.createGUI();
    }
    
    public Container getContentPane()
    {
        return this;
    }
    
    protected void createGUI()
    {
        //rootContainer=fi.hut.tml.xsmiles.gui.components.havi.HaviComponentFactory.createContainer();
        rootContainer=new HContainer();
        rootContainer.setSize(800,600);
        rootContainer.setBackground(Color.yellow);
        this.getContentPane().add(rootContainer);
        
        
        // Create the GUI
        
        
        
        // Top icon boundary
        int iconX = 5, iconY = 25;
        
        this.createComponentCreationThread(this.rootContainer);
        rootContainer.invalidate();
        this.getContentPane().validate();
        this.setSize(new java.awt.Dimension(screenWidth, screenHeight));
        this.setVisible(true);
        
        //this.rootContainer.getFirsttestcomponent.requestFocus();
        //testcomponent.addKeyListener(this);
        //this.createFocusInfoThread();
    }
    protected void createCoupleOfComponents(Container container)
    {
        int maxx=600, minx=100, maxy=500,miny=100;
        
        double random = Math.random(); // 0 -> 1
        int x = (int)((double)minx+((double)maxx-(double)minx)*random);
        random = Math.random(); // 0 -> 1
        int y = (int)((double)miny+((double)maxy-(double)miny)*random);
        
        //this.setLayout(null);
        Component testcomponent = new TestComponent5();
        testcomponent.setEnabled(true);
        testcomponent.setVisible(true);
        testcomponent.setLocation(x,y);
        container.add(testcomponent);
        
        focusManager.addFocusable((HNavigable) testcomponent);
        Image backimagen = loadImage("images/backnormal.gif");
        Image backimagef = loadImage("images/backfocused.gif");
        Image backimaged = loadImage("images/backdisabled.gif");
        Image backimagedf = loadImage("images/backdisabledfocused.gif");
        
        HGraphicButton guiback2 = new TestComponent(backimagen);
        guiback2.setGraphicContent(backimagef, HVisible.FOCUSED_STATE);
        guiback2.setGraphicContent(backimaged, HVisible.DISABLED_STATE);
        guiback2.setGraphicContent(backimagedf, HVisible.DISABLED_FOCUSED_STATE);
        guiback2.setBackground(Color.lightGray);
        guiback2.setEnabled(true);
        guiback2.setSize(50, 50);
        guiback2.setLocation(x+130,y+10);
        guiback2.setActionCommand("back");
        //guiback.addHActionListener(this);
        container.add(guiback2, 0);
        //linkFocusList.addElement(guiback);
        focusManager.addFocusable(guiback2);
        
        /* does not seem to work
        container.invalidate();
        container.getParent().validate();
         */
        // works, but heavy?
        
        container.repaint();
        
        Log.debug("Added couple of components at: "+x+","+y);
    }
    
    private void createComponentCreationThread(Container cont)
    {
        final Container container = cont;
        Thread t = new Thread()
        {
            public void run()
            {
                try
                {
                    for (int i=0;i<1000;i++)
                    {
                        createCoupleOfComponents(container);
                        Thread.sleep(1000);
                        
                    }
                } catch (Throwable t)
                {
                }
            }
        };
        t.start();            
    }
    
    private void createFocusInfoThread()
    {
        Thread t = new Thread()
        {
            public void run()
            {
                try
                {
                    for (int i=0;i<1000;i++)
                    {
                        Thread.sleep(5000);
                        Component currentfocus = getFocusOwner();
                        System.out.println("current focus: "+currentfocus+" peer:"+currentfocus.getPeer()+" isFocusable"+currentfocus.isFocusTraversable()+" isvisible"+currentfocus.isVisible());//+comp.getFocusable());
                        System.out.println("peer is focusable: "+currentfocus.getPeer().isFocusable()+" lightweight peer:"+(currentfocus.getPeer() instanceof java.awt.peer.LightweightPeer));
                        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                        Component keyfocus = manager.getFocusOwner();
                        System.out.println("Key: focusOwner: "+keyfocus);
                        //if (comp!=null) comp.requestFocus();
                        //currentfocus = getFocusOwner();
                        //System.out.println(comp.toString()+" requested focus. current focus: "+currentfocus);
                    }
                } catch (Throwable t)
                {
                }
            }
        };
        t.start();    }
    //this.setBackground(Color.gray);
    public void keyPressed(KeyEvent e)
    {
    }
    public void keyReleased(KeyEvent e)
    {
        
    }
    public void keyTyped(KeyEvent e)
    {
        Log.debug("keyTyped: "+e);
    }
    
    public void Quit()
    {
        System.exit(0);
    }
    public class TestComponent extends org.havi.ui.HGraphicButton
    {
        public TestComponent(Image image)
        {
            super(image);
            //enableEvents(AWTEvent.KEY_EVENT_MASK);
            
            focusTraversable = true;
        }
        public boolean isFocusTraversable()
        {return true;}
        public boolean isLightweight()
        {return true;}
        public boolean isFocusable()
        {return true;}
        
    }
    public class TestComponent5 extends org.havi.ui.HSinglelineEntry
    {
        public TestComponent5()
        {
            super();
            this.setSize(100,100);
            this.setTextContent("test", HVisible.ALL_STATES);
            this.setHorizontalAlignment(HVisible.HALIGN_LEFT);
            this.setVerticalAlignment(HVisible.VALIGN_TOP);
            this.setBackground(new Color(0, 250, 250));
            this.setForeground(Color.black);
            this.processHFocusEvent(new HFocusEvent(this, HFocusEvent.FOCUS_GAINED));
        }
        public boolean isFocusTraversable()
        {return true;}
        public boolean isLightweight()
        {return true;}
        public boolean isFocusable()
        {return true;}
        
    }    public class TestComponent4 extends HText
    {
        public TestComponent4()
        {
            super("text");
            this.setSize(100,100);
            this.setEnabled(true);
            this.setVisible(true);
            //this.setBackgroundColor(Color.yellow);
        }
        public boolean isFocusTraversable()
        {return true;}
        public boolean isLightweight()
        {return true;}
        public boolean isFocusable()
        {return true;}
        
    }
    public class TestComponent3 extends Component
    {
        public TestComponent3()
        {
            super();
            this.setSize(100,100);
            this.setEnabled(true);
            this.setVisible(true);
            //this.setBackgroundColor(Color.yellow);
        }
        public boolean isFocusTraversable()
        {return true;}
        public boolean isLightweight()
        {return true;}
        public boolean isFocusable()
        {return true;}
        
    }    public class TestComponent2 extends JTextField
    {
        public TestComponent2()
        {
            super("test");
            this.setSize(100,100);
            this.setEnabled(true);
            this.setVisible(true);
        }
        
    }
    
    public Image loadImage(String s)
    {
        try
        {
            Image image = null;
            URL resURL=this.getClass().getResource(s);
            Toolkit tk = Toolkit.getDefaultToolkit();
            MediaTracker mt = new MediaTracker(this);
            System.out.println(resURL.getFile());
            // 1st try to find the image in the resources, 2nd from a file
            if (resURL!=null)
                image=tk.getImage(resURL);
            else
                image = tk.getImage(s);
            mt.addImage(image, 1);
            try
            {
                mt.waitForID(1);
            } catch (InterruptedException e)
            {
                System.out.println("Error:" + e);
            }
            return image;
        } catch (Throwable t)
        {
            t.printStackTrace(System.err);
            return null;
        }
    }
/*
    public class MyContainer extends HContainer
    {
        public MyContainer()
        {
            Log.debug(this+" created");
        }
        public void paint(Graphics g)
        {
            Log.debug("paint: "+g);
            if (isShowing())
            {
                g.setColor(Color.yellow);
                g.fillRect(0,0,g.getClipBounds().width,g.getClipBounds().height);
 
            }
            super.paint(g);
        }
        public  void 	paintComponents(Graphics g)
        {
            Log.debug("paintComponents: "+g);
            if (isShowing())
            {
            }
            super.paintComponents(g);
        }
    }*/
}

