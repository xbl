/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.FrameListener;

import java.awt.Container;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.Component;

import org.havi.ui.HContainer;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Element;

/**
 *  Interface to drawing area. 
 */
public class HaviDrawingArea implements DrawingArea {

    // Container having the drawing area (a Frame or Container)
    private Container container;

    // Content Container contains the Components. This is the same as container, 
    // except for JScrollPane
    private Container contentContainer;

    // The left and top coords of the drawing area. 
    // Default values zero for rootlayout and toplayout.
    private int left = 0, top = 0;
    
    // The width and height of the drawing area	
    private int width = 0, height = 0;
    
    // The type of this drawing area
    private int type = DrawingArea.REGION;
    
    /**
     * Creates a new drawing area of type (ROOTLAYOUT or TOPLAYOUT).
     */
    public HaviDrawingArea(int type) {
	if (type == DrawingArea.ROOTLAYOUT) {
	    System.out.println("DrawingArea.ROOTLAYOUT");	    
	    this.type = type;
	} else if (type == DrawingArea.TOPLAYOUT) {
	    container = null;
	    System.out.println("DrawingArea.TOPLAYOUT");	    
	    this.type = type;
	} else if (type == DrawingArea.REGION) {
	    System.out.println("DrawingArea.REGION");
	    Container panel = new RealContainer();
	    // Use absolute coords
	    panel.setLayout(null);
	    container = panel;
	    contentContainer = panel;	
	    panel.setVisible(false);
	    this.type = type;
	    
	    // This is to get a region with scroll bars
	} else if (type == DrawingArea.REGIONSCROLL) {
	    System.out.println("DrawingArea.REGIONSCROLL");
	    Container panel = new RealContainer();
	    // Use absolute coords
	    panel.setLayout(null);
	    container = panel;
	    contentContainer = panel;	
	    panel.setVisible(false);
	    this.type = type;
	}
	
    }
    
    /**
     * Creates a new drawing area using container c - used for root-layout
     * @param c		Container
     */
    public HaviDrawingArea(Container c) {
	container = c;
	container.setLayout(null);	
	contentContainer = c;
	type = DrawingArea.ROOTLAYOUT;
    }
    
    public int getLeft() {
	return left;
    }
    
    public int getTop() {
	return top;
    }
    
    public int getWidth() {
	return width;
    }
    
    public int getHeight() {
	return height;
    }
    
    public void setBounds(int x, int y, int w, int h) {
	//
	left = x;
	top = y;
	width = w;
	height = h;
	
	/**
	   if (height > SMILViewer.screenHeight-SMILViewer.iconHeight) {
	   height = SMILViewer.screenHeight-SMILViewer.iconHeight-5;
	   }
	**/
	// Add 50 to every root layout... this is to fix the upper gui boxes
	// Don't move root layout!

	if (type != DrawingArea.ROOTLAYOUT)
	    container.setLocation(left, top);
	container.setSize(width, height);

	/**
	   if (type == DrawingArea.ROOTLAYOUT)
	   container.setLocation(left, top+SMILViewer.iconHeight);
	   else
	   container.setLocation(left, top);

	   container.setSize(width, height);
	**/
	// Validate to update the scroll bars
	//container.validate();	
    }
    

    /**
     * Set this drawing area visible.
     * @param v    true=visible, false=invisible
     */
    public void setVisible(boolean v) {
	container.setVisible(v);
    }
    
    /**
	 * Set the title for the presentation.
	 * @param title		Title for the presentation
	 */
    public void setTitle(String t) {
    }

    /**
     * For toplayouts (frames), add a close listener.
     * @param FrameListener		listener for closing events.
     */
    public void addFrameListener(FrameListener frameListener)  {
    }


    /**
     * Add a region to this drawing area. A region can be added to root-layout,
     * topLayout, or another region.
     * @param region		Region drawing area to be added.
     */
    public void addRegion(DrawingArea region, int zindex) {
	// Assumes everything is HaviDrawingArea... add to top of other regions with same z-index	
	contentContainer.add(((HaviDrawingArea)region).getContainer(),0);
	//contentContainer.add(((HaviDrawingArea)region).getContainer(), new Integer(zindex),-1);
    }

    /**
     * Bring this region into front of other regions
     * @param drawingArea		Region to be the top most
     */
    public void bringToFront(DrawingArea drawingArea) {
	/**
	   if (contentContainer instanceof HContainer){
	   System.out.println("I hope we are coming here");
	   ((HContainer)contentContainer).popToFront(((HaviDrawingArea)drawingArea).getContainer());
	   }else{
	**/
	contentContainer.remove(((HaviDrawingArea)drawingArea).getContainer());
	contentContainer.add(((HaviDrawingArea)drawingArea).getContainer(),0);
	//}
	
    }

    public void setBackgroundColor(String color) {

	// Special case transparent. Java fails to use alpha color there above.
	if (color == null || color.equals("transparent")) {
	    //			((RealContainer)container).setOpaque(false);
	    return;
	}
	
	Color c = ColorConverter.hexaToRgb(color);
	container.setBackground(c);
    }
	
    /**
     * This is a Swing specific method to return the container to draw media.
     * @return		Container to draw media.
     */
    public Container getContainer() {
	return container;	
    }
    
    /**
     * Dummy class to get a non-abstract transparent Container.
     */	
    private class RealContainer extends HContainer {
	Color background = null;
	int width = 0, height = 0;
	
	public RealContainer()  {
	    System.out.println("Hemos creado RealContainer");
	}      	
    }
    	/**
	 * This is a specific method to return the container to draw media.
	 * Returns either the container or container.getContentPane().
	 * MediaHandler calls this to obtain the container of this DrawingArea.
	 *
	 * @return		Container to draw media.
	 */
	public Container getContentContainer() {
		//if (contentContainer instanceof JFrame)
		//	return ((JFrame)contentContainer).getContentPane();
			
		return contentContainer;	
	}

        public boolean setCSSStretch(String awidth,String aheight, Element origElem)
        {
            return false;
        }

}

