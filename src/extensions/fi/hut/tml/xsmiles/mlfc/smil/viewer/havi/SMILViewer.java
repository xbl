/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.io.Reader;
import java.io.StringReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.MalformedURLException;
import java.awt.Font;
import java.awt.MediaTracker;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import org.w3c.dom.Document;

 // This will import JMF Manager
import javax.media.Manager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import org.havi.ui.HContainer;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;
import org.havi.ui.HVisible;
import org.havi.ui.HContainer;
import org.havi.ui.HDefaultTextLayoutManager;
import org.havi.ui.HStaticText;
import org.havi.ui.HStaticIcon;
import org.havi.ui.HText;
import org.havi.ui.HTextButton;
import org.havi.ui.HNavigable;
import org.havi.ui.HGraphicButton;
import org.havi.ui.HScene;
import org.havi.ui.HContainer;
import org.havi.ui.HSinglelineEntry;
import org.havi.ui.event.HActionListener;
import org.havi.ui.event.HTextListener;
import org.havi.ui.event.HTextEvent;
import org.havi.ui.event.HFocusEvent;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;

import java.awt.Panel;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.havi.HaviCaption;
import fi.hut.tml.xsmiles.gui.components.havi.HaviComponentFactory;
import fi.hut.tml.xsmiles.gui.components.havi.HaviLabelCompound;

import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Decorator;


import java.awt.Image;
import java.awt.Toolkit;
import java.util.Vector;
import java.util.Enumeration;

//import sdl.core.Main;

/**
 * This is a sample class that shows how to view a smil document and
 * play it using HAVi.
 * <p>
 * NOTE: THIS VIEWER IS NOT COMPLETE AND WON'T WORK CORRECTLY.
 */
public class SMILViewer extends HScene implements Viewer, HActionListener, HTextListener, FocusableLinkHandler, WindowListener {
//public class SMILViewer extends Frame implements Viewer, HActionListener {

    protected static String docPath = null;
    protected boolean jmfAvailable = false;
    protected HStaticText statusText = null;

    // Original size: 640x480, application area 420x360
    public static final int screenWidth = 640, screenHeight = 480, screenDepth = 16;
    // Top icon bar height
    public static final int iconHeight = 60;
    
    // Link focus list
    //protected Vector linkFocusList = null;
    
    // History, vector of strings
    protected Vector history = null;
    
    // the focus manager TODO: no statics!
    public static HaviFocusManager focusManager=new HaviFocusManager();
    
    // Performance tracking/debugging...
    public static long initTime, initMem;
    
    public static void main(String[] args) {

	//		// !!!!PERFORMANCE TRACKING!!!!
	System.gc();
	initTime = System.currentTimeMillis();
	initMem =Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
	System.out.println("H INIT TIME: "+SMILViewer.initTime+"ms MEMORY: "+(double)SMILViewer.initMem/1000+"k");
	//		// !!!!PERFORMANCE TRACKING!!!!
	
	SMILViewer cs = new SMILViewer();
	Reader rd = null; 
	URL u = null;
	String starturl = "http://www.hut.fi/~kpihkala/smils.smil";
        starturl = "file:///home/honkkis/src/ubik/build/smil/univnew/university.smil";

	
	
	if (args.length > 0) {
	    starturl=args[0];
	}
	
	try {
	    if (starturl.startsWith("http:") ||starturl.startsWith("file:")) {
		u = new URL(starturl);
		BufferedReader in = new BufferedReader(new InputStreamReader(u.openStream()));
		rd = in;
	    } else {
		System.out.println("The argument must be a URL!");
		return;
	    }
	} catch (Exception e) {
	    System.out.println("File error"+e);
	}
	System.out.println(starturl);
	
	cs.openURL(starturl);
	
	//		// !!!!PERFORMANCE TRACKING!!!!
	System.out.println("PREFETCH INIT TIME: +"+((System.currentTimeMillis()-SMILViewer.initTime))+
			   "ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILViewer.initMem))/1000 +"k");
	System.gc();
	System.out.println("PREFETCH GC TIME: +"+((System.currentTimeMillis()-SMILViewer.initTime))+
			   "ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILViewer.initMem))/1000 +"k");
	//		// !!!!PERFORMANCE TRACKING!!!!

    }
    
    protected Container rootContainer; // this is usually this

    protected HSinglelineEntry guiurl = null;
    protected Container rootlayoutContainer = null;
    protected HGraphicButton guistart = null;
    protected HGraphicButton guistop = null;
    protected HGraphicButton guiexit = null;
    protected HGraphicButton guiback = null;
    protected HVisible guitest = null;
    //protected Main main;
	
    public SMILViewer() {
	
	jmfAvailable = isJMFAvailable();
	//jmfAvailable = false;
	//linkFocusList = new Vector();
        //focusManager=new HaviFocusManager();
	String decorClass= "fi.hut.tml.xsmiles.mlfc.smil.viewer.havi.HaviDecorator";
	SMILMLFC.decorClassName=decorClass;
	
	history = new Vector();
        this.createGUI();
        this.addWindowListener(this);
    }

    
    protected void createGUI()
    {
        String starturl=null;
        rootContainer=this;
        if (starturl==null) starturl="x";
        Log.debug("Starturl: "+starturl);
        //rootContainer=new HContainer();
        //rootContainer.setSize(800,600);
	//this.getContentPane().add(rootContainer);
        
        
	// Create the GUI
	
	// Top icon boundary
	int iconX = 0, iconY = 5;
	
       
      	// URL field
	Font font = new Font("verdana", Font.BOLD, 12);
	guiurl = new HSinglelineEntry(starturl, 100, font, Color.black);
	//guiurl = new HSinglelineEntry();

	guiurl.setTextContent(starturl, HVisible.ALL_STATES);
	guiurl.setHorizontalAlignment(HVisible.HALIGN_LEFT);
	guiurl.setVerticalAlignment(HVisible.VALIGN_TOP);
	guiurl.setBackground(new Color(0, 250, 250));
	guiurl.setForeground(Color.black);
	guiurl.addHTextListener(this);
        guiurl.setEnabled(true);
        guiurl.setVisible(true);
        guiurl.setSize(100,100);
        //guiurl.setLocation(100,100);
	guiurl.setBounds(55+iconX, iconY, screenWidth-225-iconX, 50);
	rootContainer.add(guiurl, 0);
	//linkFocusList.addElement(guiurl);	
        focusManager.addFocusable(guiurl);

        	// Play controls
        Image backimagen = loadImage("images/backnormal.gif");
	Image backimagef = loadImage("images/backfocused.gif");
	Image backimaged = loadImage("images/backdisabled.gif");
	Image backimagedf = loadImage("images/backdisabledfocused.gif");
        guiback = new HGraphicButton(backimagen);
	guiback.setGraphicContent(backimagef, HVisible.FOCUSED_STATE);
	guiback.setGraphicContent(backimaged, HVisible.DISABLED_STATE);
	guiback.setGraphicContent(backimagedf, HVisible.DISABLED_FOCUSED_STATE);
	guiback.setBackground(Color.lightGray);
	guiback.setEnabled(true);
	guiback.setBounds(iconX, iconY, 50, 50);
	guiback.setActionCommand("back");
	guiback.addHActionListener(this);
	rootContainer.add(guiback, 0);
	//linkFocusList.addElement(guiback);
	focusManager.addFocusable(guiback);
        
        
        Image startimagen = loadImage("images/startnormal.gif");
	Image startimagef = loadImage("images/startfocused.gif");
	Image startimagea = loadImage("images/startactioned.gif");
	guistart = new HGraphicButton(startimagen);
	guistart.setGraphicContent(startimagef, HVisible.FOCUSED_STATE);
	guistart.setGraphicContent(startimagea, HVisible.ACTIONED_STATE);
	guistart.setBackground(Color.lightGray);
	guistart.setBounds(screenWidth-160-iconX, iconY, 50, 50);
	guistart.setActionCommand("start");
	guistart.addHActionListener(this);
	guistart.setEnabled(true);
	rootContainer.add(guistart, 0);
	//linkFocusList.addElement(guistart);
        focusManager.addFocusable(guistart);

	
	Image stopimagen = loadImage("images/stopnormal.gif");
	Image stopimagef = loadImage("images/stopfocused.gif");
	Image stopimagea = loadImage("images/stopactioned.gif");
	guistop = new HGraphicButton(stopimagen);
	guistop.setGraphicContent(stopimagef, HVisible.FOCUSED_STATE);
	guistop.setGraphicContent(stopimagea, HVisible.ACTIONED_STATE);
	guistop.setBackground(Color.lightGray);
	guistop.setBounds(screenWidth-105-iconX, iconY, 50, 50);
	guistop.setActionCommand("stop");
	guistop.setEnabled(true);
	guistop.addHActionListener(this);
	rootContainer.add(guistop, 0);
	//linkFocusList.addElement(guistop);	
        focusManager.addFocusable(guistop);

	Image exitimagen = loadImage("images/exitnormal.gif");
	Image exitimagef = loadImage("images/exitfocused.gif");
	Image exitimagea = loadImage("images/exitactioned.gif");
	guiexit = new HGraphicButton(exitimagen);
	guiexit.setGraphicContent(exitimagef, HVisible.FOCUSED_STATE);
	guiexit.setGraphicContent(exitimagea, HVisible.ACTIONED_STATE);
	guiexit.setBackground(Color.lightGray);
     	guiexit.setEnabled(true);

	guiexit.setBounds(screenWidth-50-iconX, iconY,50, 50);
	guiexit.setActionCommand("exit");
	guiexit.addHActionListener(this);
	rootContainer.add(guiexit, 0);
	//linkFocusList.addElement(guiexit);
        focusManager.addFocusable(guiexit);

      	// Play area
        rootlayoutContainer = HaviComponentFactory.createContainer();
	//rootLayoutContainer = new Panel();
        // TODO: for some reason -70 in kaffe sdl does not work
	//rootlayoutContainer.setBounds(0, iconHeight+20, screenWidth, screenHeight-iconHeight);
	//rootlayoutContainer.setBounds(0, 10, 500,500);
	rootlayoutContainer.setBounds(0, iconHeight+iconY, screenWidth, screenHeight-iconHeight-iconY-1);
	rootContainer.add(rootlayoutContainer, BorderLayout.CENTER);
	//this.add(rootlayoutContainer);

        rootContainer.invalidate();
        this.validate();
	//this.getContentPane().validate();
	this.setSize(new java.awt.Dimension(screenWidth, screenHeight));
	/*
	try 
	    {
		Thread.sleep(5000);
	    } catch( Throwable t)
		{
		}
	*/
        this.setVisible(true);
        
        //guiback.requestFocus();
	guistop.processHFocusEvent(new HFocusEvent(guistop, HFocusEvent.FOCUS_GAINED));	
        //estcomponent.addKeyListener(this);
        //this.createFocusInfoThread();
        
    }
        
                public Container getContentPane()
    {
        return this;
    }


    public void Quit(){
        /*
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	main = toolkit.getMain();
	toolkit.terminate();
	main.TTFQuit();
	main.SDLQuit();*/
	System.exit(0);
    }



    public boolean init(SMILViewer viewer, Reader reader, String path) {
	
	if (reader == null) 
	    return false;
	
	// Document URL
	try {
	    docPath = path.substring(0, path.lastIndexOf('/'));
	} catch (StringIndexOutOfBoundsException e) {
	    try {
		docPath = path.substring(0, path.lastIndexOf('\\'));
	    } catch (StringIndexOutOfBoundsException e2) {
	    }
	}


	// Xerces Parser - requires xml-apis.jar and xerces.jar
	SMILDocumentImpl smil = null;
	try {
	    javax.xml.parsers.DocumentBuilderFactory factory=javax.xml.parsers.DocumentBuilderFactory.newInstance(); 
	    
	    factory.setNamespaceAware( true );
	    
	    factory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",
				 "fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl"  );
	    javax.xml.parsers.DocumentBuilder parser = factory.newDocumentBuilder();
	    org.xml.sax.InputSource input = new org.xml.sax.InputSource(reader);
	    Document doc =  parser.parse(input);
	    smil = (SMILDocumentImpl)doc;
	} catch (javax.xml.parsers.ParserConfigurationException e) {
	    smilDoc = null;
	    displayStatusText("Error parsing.");
	    return false;
	} catch(org.xml.sax.SAXException e) {
	    smilDoc = null;
	    displayStatusText("Error parsing.");
	    return false;
	} catch (java.io.IOException e) {
	    smilDoc = null;
	    displayStatusText("Error parsing.");
	    return false;
	}
	// End Xerces Parser


	System.out.println("SMIL PARSED");
	
	viewer.initViewer(smil);
	smil.setViewer(viewer);

	// Initialise the document - this is size and color for root-layout etc.
	smil.initialize(false);

	// Optional prefetch.
	smil.prefetch();
	smil.start();
	return true;
    }


    public void start() {
	smilDoc.start();
    }

    public void stop() {
	smilDoc.stop();
    }

    protected SMILDocumentImpl smilDoc = null;

    protected void initViewer(SMILDocumentImpl s) {
	smilDoc = s;
    }

    /**
     * Return the base URL of the document.
     */	
    public URL getBaseURL()
    {
	// Overridden base url
	//if (baseURI != null) {
	//	return baseURI;
	//}
	
	try {
	    return new URL(docPath);
	} catch (MalformedURLException e) {
	    return null;
	}
    }

    public void gotoExternalLink(String url) {
	//    URL u = URLFactory.getInstance().createURL(url);
	//  EventBroker.getInstance().issueXMLDocRequiredEvent(u, true);
	openURL(url);
    } 
    /**
     * Open external link replacing/opening new target
     * @param url		URL to open
     * @param target	target frame/window
     */
    public void gotoExternalLinkTarget(String url, String target) {
	openURL(url);
    }
    /**
     * Open external link in a new window
     * @param url		URL to open
     */
    public void gotoExternalLinkNewWindow(String url) {
	openURL(url);
    }
    
    public void displayStatusText(String url) {
	if (statusText!=null) statusText.setTextContent(url, HVisible.NORMAL_STATE);		
    }
    
    public MediaHandler getNewMediaHandler() {
	HaviMediaHandler mh = new HaviMediaHandler(docPath);
	mh.setViewer(this);
	return mh;
    }

    /**
     * Returns a new BrushHandler for SMIL core logic.
     */
    public BrushHandler getNewBrushHandler() {
	HaviBrushHandler bh = new HaviBrushHandler();
	bh.setViewer(this);
	return bh;
    }

    public LinkHandler getNewLinkHandler() {
	LinkHandler lh = new HaviLinkHandler();
	lh.setViewer(this);
	return lh;
    }

    public void setDocumentBaseURI(String base) {
    }
	
    public SMILDocument getSMILDoc() {
	return smilDoc;
    }

    /**
     * Returns a container for SMIL region elements.
     * For root-layout, this will also create the frame and all GUI components.
     */
    public DrawingArea getNewDrawingArea(int type, boolean block) {
	
	if (type == DrawingArea.ROOTLAYOUT) {
	    System.out.println("We have created a rootLayoutContainer");
	    return new HaviDrawingArea(rootlayoutContainer);
	}
	else{
	    System.out.println("We have created another drawing area");
	    return new HaviDrawingArea(type);
	}
    }
    
    /**
     * Add a link component to a focus list.
     * @param	link	Link component, HNavigable
     */
    public void addFocusableLink(HNavigable link) {
        focusManager.addFocusable(link);
       /*
	linkFocusList.addElement(link);
	createFocusGrid();
        */
	return;		
    }
    
    /**
     * Remove a link component from a focus list. Causes the whole focus path to be recalculated.
     * @param	link	Link component, HNavigable
     */
    public void removeFocusableLink(HNavigable link) {
        focusManager.removeFocusable(link);
	return;		
    }
    

	/**
	 * Returns a new ForeignHandler for SMIL core logic. NOT IMPLEMENTED IN STANDALONE PLAYER.
	 */
	public MediaHandler getNewForeignHandler(Element e)
	{
		return null;
	}

    public Image loadImage(String s) 
    {
        try
        {
            Image image = null;
            URL resURL=this.getClass().getResource(s);
            //URL resURL=new URL("file:///home/pcesar/src/ubik/build/data/Antonio_Banderas.jpg");
            Toolkit tk = Toolkit.getDefaultToolkit();
            MediaTracker mt = new MediaTracker(this);
            System.out.println(resURL.getFile());
            // 1st try to find the image in the resources, 2nd from a file
            if (resURL!=null) 
                //image=tk.getImage(resURL.getFile());
                image=tk.getImage(resURL);
            else 
                image = tk.getImage(s);
	    /**
	    mt.addImage(image, 1);
	    try { 
		mt.waitForID(1);
	    } catch (InterruptedException e) {
		System.out.println("Error:" + e);
	    }
	    	    **/

	    
	    /*
	    try { 
	        Thread.sleep(3000);
	    } catch (InterruptedException e) {
		System.out.println("Error:" + e);
		}*/

	    System.out.println("Image: "+image);
	    
	    return image;
        } catch (Throwable t)
        {
            t.printStackTrace(System.err);
            return null;
        }
    }


    public void addTimePoint(String elementId) {
	//  	if (elementId != null)
	//	  	timePoints.addItem(elementId);
    }
    
    //// These are methods to configure the SMIL player ////
    
    /**
     * Returns window width for the SMILDocument.
     * This is the playing are width, used to create a default maximum region size.
     */
	public int getWindowWidth() {
		return screenWidth;
	}
	/**
	 * Returns  window height for the SMILDocument.
	 * This is the playing are height, used to create a default maximum region size.
	 */
	public int getWindowHeight() {
		return screenHeight-50; //?
	}
	

	public String getSystemBitrate() {
		return "14400";
	}

	public String getSystemCaptions() {
		return "false";
	}

	public String getSystemLanguage() {
		return "fi";
	}
	public String getSystemOverdubOrCaption() {
		return "caption";
	}
	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemRequired(String prefix)
	{
		return false;
	}

	/**
	 * Returns screen width for the SMILDocument.
	 * This is the system screen width, used in SMIL switch systemScreenSize attribute 
	 */
	public int getSystemScreenWidth() {
		return screenWidth;
	}
	/**
	 * Returns screen height for the SMILDocument.
	 * This is the system screen height, used in SMIL switch systemScreenSize attribute 
	 */
	public int getSystemScreenHeight() {
		return screenHeight;
	}
	/**
	 * Returns screen depth for the SMILDocument.
	 * This is the system screen depth, used in SMIL switch systemScreenSize attribute 
	 */
	public int getSystemScreenDepth() {
		return screenDepth;
	}
	public String getSystemOverdubOrSubtitle() {
		return "overdub";
	}
	public String getSystemAudioDesc() {
		return "on";
	}
	public String getSystemOperatingSystem() {
		return "Linux";
	}
	public String getSystemCPU() {
		return "x386";
	}
	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemComponent(String component)
	{
		return false;
	}

	public boolean getPlayImage() {
		return true;
	}

	public boolean getPlayAudio() {
		return jmfAvailable;
	}

	public boolean getPlayVideo() {
		return jmfAvailable;
	}

	String title = "Havi SMIL viewer";

	/**
	 * Get the title of the presentation. null if no title present.
	 */
	public String getTitle()  {
		return title;
	}
	/**
	 * Set the title for the presentation.
	 * @param title		Title for the presentation
	 */
	public void setTitle(String t) {
		title = t;
//		if (frame != null)
//			frame.setTitle(title);
	}

	public boolean isHost() {
		return true;
	}

    /**
     * Check if JMF class is available. At the same time, set the hint...
     * @return		true if JMF is available, false otherwise
     */
    protected boolean isJMFAvailable() {
	return true;
	/**
	   try {
	   Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true)); 
	   } catch (NoClassDefFoundError e) {
	   System.out.println("JMF not available! It is recommended to have it installed.");
	   System.out.println("Video and audio will not be played.");
	   return false;
	   
	   // This is for kaffe
	   } catch (NoSuchMethodError e) {
	   System.out.println("JMF not available! It is recommended to have it installed.");
	   System.out.println("Video and audio will not be played.");
	   return false;
	   
	   // All other exceptions..
	   } catch (Exception e) {
	   System.out.println("JMF not available! It is recommended to have it installed.");
	   System.out.println("Video and audio will not be played.");
	   return false;
	   }
	   return true;
	**/
    }

	public void textChanged(HTextEvent e) {
	    System.out.println("TEXT CHANGED - opening page");
	    // When the user ends edit mode. What about platforms with automatic
	    // edit mode entry/exit with focus?
	    if (e.getID() == HTextEvent.TEXT_END_CHANGE) {
		openURL(guiurl.getTextContent(HVisible.NORMAL_STATE));
	    }
	}
    
    public void caretMoved(HTextEvent e) {
	// Don't care
    }
    
    /**
     * Playback buttons.
     */
    public void actionPerformed(ActionEvent e) {
	System.out.println("Action: "+e.getActionCommand());
	
	String text = e.getActionCommand();
	if (text.equals("stop")) {
	    if (smilDoc != null) {
		smilDoc.stop();
	    }
	}
	if (text.equals("pause")) {
	    smilDoc.pause();
	}
	
	if (text.equals("start")) {
			if (smilDoc != null) {
			    smilDoc.start();
			}
	}

	if (text.equals("exit")) {
	    if (smilDoc != null)
		smilDoc.stop();
            /*
	    Toolkit toolkit = Toolkit.getDefaultToolkit();
	    main = toolkit.getMain();
	    toolkit.terminate();
	    main.TTFQuit();
	    main.SDLQuit();
             */
	    System.exit(0);
	}
	if (text.equals("go")) {
	    openURL(guiurl.getTextContent(HVisible.NORMAL_STATE));
	}
	
	if (text.equals("back")) {
	    if (history.size() > 1) {
		// Remove the last, get the next last url, remove it and go to it (which will add it)
		history.remove(history.size()-1);
		String url = (String)history.elementAt(history.size()-1);
		history.remove(history.size()-1);
		openURL(url);
	    }
	}
    }
    
    /**
     * Open SMIL doc at url.
     */
    protected void openURL(String url) {
	openURL(url, null);
    }
    
    /**
     * Open ERROR doc at url, if errormsg != null.
     */
    protected void openURL(String url, String errormsg) {
	// Stop the previous presentation
	if (smilDoc != null) {
	    ((SMILDocumentImpl)smilDoc).freeResources(false);
	    smilDoc = null;
	    if (rootlayoutContainer != null) {
		rootlayoutContainer.removeAll();
	    }
	}
	

	// Resolve relative paths	
	if (url.startsWith("http:") == false && url.startsWith("file:") == false)
	    url = docPath+"/"+url;
	
	System.out.println("docPath:"+docPath);
	    
	// Set to the URL field
	guiurl.setTextContent(url, HVisible.ALL_STATES);
	    
	    
	// Add to the history list
	if (errormsg == null)
	    history.addElement(url);
	
	// Enable/Disable back button
	if (history.size() < 2)
            {
		//guiback.setEnabled(false);
            }
	else
	    guiback.setEnabled(true);
	
	// Get focus out of SMIL document, if it is there
	if (guiback.isSelected() != true && guiurl.isSelected() != true &&
	    guistart.isSelected() != true && guistop.isSelected() != true &&
	    guiexit.isSelected() != true)
	    guiurl.processHFocusEvent(new HFocusEvent(guiurl, HFocusEvent.FOCUS_GAINED));
	
	System.out.println("Opening:"+url);
	// Open the SMIL file	
	String filename = url;
	try {
	    System.out.println(filename+" prefetching.");				
	    displayStatusText(filename+" prefetching.");
	    
	    Reader reader = null;
	    // If no errors -> open usual url reader, otherwise string reader from a string			
	    if (errormsg == null) {
		// Initialize, and if failed, just exit
		URL u = null;
		try {
		    u = new URL(filename);
		} catch (java.net.MalformedURLException ex) {
		    System.out.println("URL not found: "+filename);
		    displayStatusText("URL not found: "+filename);
		    displayError(filename, "Invalid Address: "+filename);
		    return;
		}
		InputStream in = null;
		in = u.openStream();
		reader = new InputStreamReader(in);
	    } else {
		//				// Read bytes into a string for Palm parser
		reader = new StringReader(errormsg);
	    }
	    
	    if (init(this, reader, filename) == true) {
		System.out.println(filename+" started.");
		displayStatusText(filename+" started.");
	    } else {
		System.out.println("Error retrieving document.");
		displayStatusText("Error retrieving document.");
		displayError(filename, "Error retrieving document: "+filename);
	    }		
	} catch (java.io.IOException ex) {
	    System.out.println("IOException: "+filename);
	    displayStatusText("IOException: "+filename);
	    displayError(filename, "Error Accessing Address: "+filename);
	    return;
	}
    }
    
    public void displayError(String url, String errMsg) {
	
	String start = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><!DOCTYPE smil PUBLIC \"-//W3C//DTD SMIL 1.0//EN\" \"http://www.w3.org/TR/REC-smil/SMIL10.dtd\"><smil><head><meta name=\"title\" content=\" Error \"/><layout><root-layout background-color=\"#ffffff\" width=\""+screenWidth+"\" height=\""+(screenHeight-50)+"\"/><region id=\"msg\" left=\"40\" top=\"40\" width=\"500\" height=\"300\" z-index=\"3\"/></layout></head><body><text region=\"msg\" src=\"data:,";
	String end = "\" dur=\"indefinite\"/></body></smil>";
	
	openURL(url, start+errMsg+end);
    }

    public void windowActivated(WindowEvent e)
    {
    }
    
    public void windowClosed(WindowEvent e)
    {
    }
    
    public void windowClosing(WindowEvent e)
    {
        System.exit(0);
    }
    
    public void windowDeactivated(WindowEvent e)
    {
    }
    
    public void windowDeiconified(WindowEvent e)
    {
    }
    
    public void windowIconified(WindowEvent e)
    {
    }
    
    public void windowOpened(WindowEvent e)
    {
    }
    
    public class AA extends MouseAdapter implements MouseListener {

	public void mouseClicked(MouseEvent e) {
	    System.out.println("!!!"+e);
	}
    }

    public class BB extends KeyAdapter implements KeyListener {
	
		public void keyTyped(KeyEvent e) {
		    System.out.println("!!!KEY!!!"+e);
		}
    }
    
    public Decorator getDecorator()
    {
        return null;
    }
    
}

