/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.havi;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.util.Vector;
import java.util.Enumeration;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import org.havi.ui.HContainer;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;
import org.havi.ui.HVisible;
import org.havi.ui.HContainer;
import org.havi.ui.HDefaultTextLayoutManager;
import org.havi.ui.HStaticText;
import org.havi.ui.HStaticIcon;
import org.havi.ui.HText;
import org.havi.ui.HTextButton;
import org.havi.ui.HNavigable;
import org.havi.ui.HGraphicButton;
import org.havi.ui.HScene;
import org.havi.ui.HContainer;
import org.havi.ui.HSinglelineEntry;
import org.havi.ui.event.HActionListener;
import org.havi.ui.event.HTextListener;
import org.havi.ui.event.HTextEvent;
import org.havi.ui.event.HFocusEvent;

import fi.hut.tml.xsmiles.Log;



/**
 * Simple focus manager for Havi
 */
public class HaviFocusManager { //extends HScene implements  HActionListener, HTextListener, FocusableLinkHandler {
//public class SMILViewer extends Frame implements Viewer, HActionListener {

    // Link focus list
    protected Vector linkFocusList = null;
    
    
    
    /**
     * Add a link component to a focus list.
     * @param	link	Link component, HNavigable
     */
    public void addFocusable(HNavigable link) {
        if (linkFocusList==null) linkFocusList=new Vector(20);
	linkFocusList.addElement(link);
        Log.debug("Added focusable: "+link+" count:"+linkFocusList.size());
	createFocusGrid();
	return;		
    }
    /**
     * Remove a link component from a focus list. Causes the whole focus path to be recalculated.
     * @param	link	Link component, HNavigable
     */
    public void removeFocusable(HNavigable link) {
	HNavigable nav = null;
        if (linkFocusList==null) linkFocusList=new Vector(20);
	linkFocusList.removeElement(link);
	int navx, navy;
	int linkx = calculateMidX((Component)link);
	int linky = calculateMidY((Component)link);
	
	Component closest = null;
	double closestDistance = 999999, distance;
	
	// Go through all links activate the closest one
	for (Enumeration e = linkFocusList.elements() ; e.hasMoreElements() ;) {
	    nav = (HNavigable)e.nextElement();
	    
	    // Find the closest link, sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
	    navx = calculateMidX((Component)nav);
	    navy = calculateMidY((Component)nav);
	    distance = Math.sqrt( (navx-linkx)*(navx-linkx) + (navy-linky)*(navy-linky));
	    if (distance < closestDistance) {
		closest = (Component)nav;
		closestDistance = distance;
	    }
	}
	
	// If the link to be removed has focus, move the focus to the closest one
	if (link.isSelected() == true) {
	    ((HNavigable)link).processHFocusEvent(
						      new HFocusEvent((Component)link, HFocusEvent.FOCUS_LOST));
	    if (closest != null) {
		((HNavigable)closest).processHFocusEvent(
							     new HFocusEvent(closest, HFocusEvent.FOCUS_GAINED));
	    }
                // WHAT?
                /*
	    else
		guiurl.processHFocusEvent(
					    new HFocusEvent(guiurl, HFocusEvent.FOCUS_GAINED));
                 */
	}
	
	createFocusGrid();
	return;		
    }
    

    /**
     * Searches up for the closest focus traversal.
     * @return HNavigable at the direction. (or null if not found)
     */
    protected HNavigable searchUp(HNavigable origo) {
	HNavigable nav = null;
	HNavigable closest = null;
	int origox = calculateMidX((Component)origo);
	int origoy = calculateMidY((Component)origo);
	int navx,navy;
	double distance = 0, closestDistance = 99999999;
	
	// Go through all links, find the closest 
	for (Enumeration e = linkFocusList.elements() ; e.hasMoreElements() ;) {
	    nav = (HNavigable)e.nextElement();
	    // Skip itself
	    if (nav.equals(origo))
		continue;
	    
	    // If not at upper quadrant -> skip
	    navx = calculateMidX((Component)nav);
	    navy = calculateMidY((Component)nav);
	    if (! ((navx-origox)<-(navy-origoy) && (navx-origox)>(navy-origoy)) )
		continue;
	    
	    // Find the closest link, d = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
	    distance = Math.sqrt( (navx-origox)*(navx-origox) + (navy-origoy)*(navy-origoy));
	    if (distance < closestDistance) {
		closest = nav;
		closestDistance = distance;
	    }
	}
	return closest;
    }

    /**
     * Searches down for the closest focus traversal.
     * @return HNavigable at the direction. (or null if not found)
     */
    protected HNavigable searchDown(HNavigable origo) {
	HNavigable nav = null;
	HNavigable closest = null;
	int origox = calculateMidX((Component)origo);
		int origoy = calculateMidY((Component)origo);
		int navx, navy;
		double distance = 0, closestDistance = 99999999;
		
		// Go through all links, find the closest 
		for (Enumeration e = linkFocusList.elements() ; e.hasMoreElements() ;) {
			nav = (HNavigable)e.nextElement();
			// Skip itself
			if (nav.equals(origo))
				continue;
	
			// If not at down quadrant -> skip
			navx = calculateMidX((Component)nav);
			navy = calculateMidY((Component)nav);
			if (! ((navx-origox)>-(navy-origoy) && (navx-origox)<(navy-origoy)) )
				continue;

			// Find the closest link, d = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
			distance = Math.sqrt( (navx-origox)*(navx-origox) + (navy-origoy)*(navy-origoy));
			if (distance < closestDistance) {
				closest = nav;
				closestDistance = distance;
			}
		}
		return closest;
	}

	/**
	 * Searches left for the closest focus traversal.
	 * @return HNavigable at the direction. (or null if not found)
	 */
	protected HNavigable searchLeft(HNavigable origo) {
		HNavigable nav = null;
		HNavigable closest = null;
		int origox = calculateMidX((Component)origo);
		int origoy = calculateMidY((Component)origo);
		int navx, navy;
		double distance = 0, closestDistance = 99999999;
		
		// Go through all links, find the closest 
		for (Enumeration e = linkFocusList.elements() ; e.hasMoreElements() ;) {
			nav = (HNavigable)e.nextElement();
			// Skip itself
			if (nav.equals(origo))
				continue;
	
			// If not at left quadrant -> skip
			navx = calculateMidX((Component)nav);
			navy = calculateMidY((Component)nav);
			if (! ((navx-origox)<-(navy-origoy) && (navx-origox)<(navy-origoy)) )
				continue;

			// Find the closest link, d = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
			distance = Math.sqrt( (navx-origox)*(navx-origox) + (navy-origoy)*(navy-origoy));
			if (distance < closestDistance) {
				closest = nav;
				closestDistance = distance;
			}
		}
		return closest;
	}

	/**
	 * Searches right for the closest focus traversal.
	 * @return HNavigable at the direction. (or null if not found)
	 */
	protected HNavigable searchRight(HNavigable origo) {
		HNavigable nav = null;
		HNavigable closest = null;
		int origox = calculateMidX((Component)origo);
		int origoy = calculateMidY((Component)origo);
		int navx, navy;
		double distance = 0, closestDistance = 99999999;
		
		// Go through all links, find the closest 
		for (Enumeration e = linkFocusList.elements() ; e.hasMoreElements() ;) {
			nav = (HNavigable)e.nextElement();
			// Skip itself
			if (nav.equals(origo))
				continue;
	
			// If not at right quadrant -> skip
			navx = calculateMidX((Component)nav);
			navy = calculateMidY((Component)nav);
			if (! ((navx-origox)>-(navy-origoy) && (navx-origox)>(navy-origoy)) )
				continue;

			// Find the closest link, d = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
			distance = Math.sqrt( (navx-origox)*(navx-origox) + (navy-origoy)*(navy-origoy));
			if (distance < closestDistance) {
				closest = nav;
				closestDistance = distance;
			}
		}
		return closest;
	}

	protected int calculateMidX(Component comp) {
		int x = comp.getSize().width/2 + comp.getLocation().x;
		Container parent = comp.getParent();
		while (parent != null) {
			x = x + parent.getLocation().x;
			parent = parent.getParent();
		}

		return x;		
	}

	protected int calculateMidY(Component comp) {
		int y = comp.getSize().height/2 + comp.getLocation().y;
		Container parent = comp.getParent();
		while (parent != null) {
			y = y + parent.getLocation().y;
			parent = parent.getParent();
		}

		return y;		
	}
        
            /**
     * Creates the focus grid, for all keys, up, down, left, right.
     */
    public void createFocusGrid() {
	HNavigable nav = null;
	
	// Go through all links, chaining them together
	for (Enumeration e = linkFocusList.elements() ; e.hasMoreElements() ;) {
	    nav = (HNavigable)e.nextElement();
	    nav.setMove(KeyEvent.VK_UP, searchUp(nav));
	    nav.setMove(KeyEvent.VK_DOWN, searchDown(nav));
	    nav.setMove(KeyEvent.VK_LEFT, searchLeft(nav));
	    nav.setMove(KeyEvent.VK_RIGHT, searchRight(nav));
	}

	// Left - right keys move focus
        /*
	guiback.setMove(KeyEvent.VK_RIGHT, guiurl);
	guiurl.setMove(KeyEvent.VK_RIGHT, guistart);
	guistart.setMove(KeyEvent.VK_RIGHT, guistop);
	guistop.setMove(KeyEvent.VK_RIGHT, guiexit);
	guiexit.setMove(KeyEvent.VK_LEFT, guistop);
	guistop.setMove(KeyEvent.VK_LEFT, guistart);
	guistart.setMove(KeyEvent.VK_LEFT, guiurl);
	guiurl.setMove(KeyEvent.VK_LEFT, guiback);
	*/
	return;		
    }


}

