/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.x3d;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AnimationService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.util.Hashtable;
import java.util.Enumeration;

import org.web3d.vrml.j3d.nodes.J3DVRMLNode;
import org.web3d.vrml.nodes.VRMLNodeListener;
import org.web3d.vrml.j3d.nodes.core.J3DTouchSensor;

/**
 * TouchSensorNode Element. This element implements TouchSensor, so it
 * dispatches DOM2 click events to the DOM tree, when mouse is clicked.
 *
 * @author Kari
 */
public class TouchSensorNodeElementImpl extends GenericElementImpl implements VRMLNodeListener
{
	// This DOM node as VRMLNode
	private J3DTouchSensor vrmlNode = null;

	// Index of isActive field
	private int isActiveIndex = -1;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public TouchSensorNodeElementImpl(DocumentImpl owner, String namespace, String tag, X3DMLFC m) {
		super(owner, namespace, tag, m);
	}

	/**
	 * Initialize this element.
	 * This requires that the DOM tree is available.
	 */
	public void init() {
		Node n = this;
		Log.debug("TOUCHSENSOR INIT!");
		
		// Search for X3D root element
		while ((n instanceof X3DElementImpl) == false) {
			n = n.getParentNode();
			if (n == null) {
				Log.error("TouchSensorNodeElement must be under a X3D root element!");
				return;
			}
		}
		
		// Got it - now, ask for VRMLNode J3DTouchSensor
		vrmlNode = (J3DTouchSensor)((X3DElementImpl)n).getVRMLNode(this);
		vrmlNode.addNodeListener(this);
		// Get the field index for isActive
		isActiveIndex = vrmlNode.getFieldIndex("isActive");
	}

	
	public void fieldChanged(int index) {
		Log.debug("field changed:"+index);
		// Check isActive field
		if (index == isActiveIndex) {
			if (vrmlNode.getIsActive() == true)
				dispatch("click");
		}
	}

	/**
	 * Do cleanup 
	 */
	public void destroy() {
	}
}


