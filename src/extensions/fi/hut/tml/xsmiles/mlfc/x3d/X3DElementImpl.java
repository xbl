/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.x3d;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.net.URL;
import java.awt.Component;
import java.awt.Dimension;

import org.web3d.vrml.j3d.nodes.J3DVRMLNode;

/**
 * X3D root element. To obtain the VisualComponent. This element will create a
 * X3DRenderer, if this element is a parasite and getVisualComponent is called.
 *
 * @author Kari
 */
public class X3DElementImpl extends XSmilesElementImpl implements VisualComponentService
{
	// XSmilesDocumentImpl - to create new elements
	DocumentImpl ownerDoc = null;

	// Reference to the MLFC (all parasite X3D elements share the same MLFC)
	X3DMLFC mlfc;

	// Parasite component size ; default is 100x100 (hardcoded!)
	private int width = 100, height = 100;

	// Renderer to render X3D nodes
	private X3DRenderer renderer = null;

	/**
	* Constructor - Set the owner, name and namespace.
	*/
	public X3DElementImpl(DocumentImpl owner, String namespace, String tag, X3DMLFC m) {
		super(owner, namespace, tag);
		ownerDoc = owner;
		mlfc = m;
		
		Log.debug("X3D root element created!");
	}

	private void dispatch(String type) {
		// Dispatch the event
		Log.debug("Dispatching X3D Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}
    
    // currently supports EVENT_STYLECHANGED with object=null, in future there would be more 
    public void visualEvent(int event,Object object)
    {
        
    }

	/**
	 * Initialize this element.
	 * This requires that the DOM tree is available.
	 */
	public void init() {
		Log.debug("X3D.init");
		String href=this.getAttribute("href");
		
		// Secondary - create the renderer here
		if (mlfc.isHost() == false) {
			paraComponent = initParasite();
			if (paraComponent == null) {
				Log.debug("X3D DOM parse error.");
				return;
			}
		}
		super.init();
	}

	/**
	 * Do cleanup - frees renderer, if created.
	 */
	public void destroy() {
		Log.debug("X3D element destroy()");
		if (renderer != null)
			renderer.cleanup();
		setVisible(false);
		super.destroy();
	}

	/**
	 * Get VRMLNode for a DOM element. There is some sort of mapping between the DOM tree
	 * and VRML scene.
	 * @param element	DOM element
	 * @return			VRMLNode
	 */
	protected J3DVRMLNode getVRMLNode(Element element) {
		if (renderer != null)
			return (J3DVRMLNode)renderer.refMap.get(element);
		else
			return (J3DVRMLNode)mlfc.renderer.refMap.get(element);
	}
	
	/**
	 * Get renderer (either X3D element's or MLFC's)
	 * @return			Renderer used in this X3D tree
	 */
	protected X3DRenderer getRenderer() {
		if (renderer != null)
			return renderer;
		else
			return mlfc.renderer;
	}

	/**********************************************************************
	 * VisualComponentService Interface - to use X3D as parasite language.
	 */

	private boolean visualComponentVisible = false;
	private Component paraComponent = null;
	/**
	 * Return the visual component for this extension element. Creates a renderer.
	 */
	public Component getComponent() {
		Log.debug("X3D: getComponent()");

		// If not yet started, get the component
		if (paraComponent == null) {
			Log.error("X3D error");
			return null;
		}
		// XHTML requires this!
		paraComponent.setSize(new Dimension(width, height));
		// XHTML doesn't require this!
		//paraComponent.setPreferredSize(new Dimension(100,100));

		// Set to visible from beginning
		visualComponentVisible=true;
		paraComponent.setVisible(visualComponentVisible);
		Log.debug("X3D: getComponent() returned.");
		return paraComponent;
	}

	private Component initParasite() {
		Component canvas;

		renderer = new X3DRenderer(mlfc);
		canvas = renderer.render(this);

		Log.debug("X3D parasite-DOM converted.");

		return canvas;
	}

	/**
	 * Returns the approximate size of this extension element (WHAT?)
	 */
	public Dimension getSize() {
		Log.debug("X3D GET SIZE!");
		return new Dimension(width, height);
	}
	double zoom = 1.0;
	public void setZoom(double zoom) {
		// SVG requires this!
		paraComponent.setSize(new Dimension((int)((double)width*zoom), 
											(int)((double)height*zoom)));

		return;
	}
	public void setVisible(boolean visible) {
		Log.debug("X3D SET VISIBLE!"+visible);
		visualComponentVisible = visible;
		if (paraComponent != null)
			paraComponent.setVisible(visible);
		return;
	}
	public boolean getVisible() {
		return visualComponentVisible; 
	}

	/**
	 * ECMAScript accessible method to refresh the X3D scene.
	 */
	public void refresh() {
		renderer.refresh(this);
	}

}


