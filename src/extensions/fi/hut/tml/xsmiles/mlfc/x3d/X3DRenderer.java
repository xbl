/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.x3d;

// MLFC specific imports

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.xml.parsers.FactoryConfigurationError;

import javax.media.j3d.GraphicsConfigTemplate3D;

import org.ietf.uri.ResourceConnection;
import org.ietf.uri.URI;
import org.ietf.uri.URIUtils;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;
import org.web3d.vrml.j3d.J3DSceneBuilderFactory;
import org.web3d.vrml.j3d.J3DVRMLScene;
import org.web3d.vrml.j3d.browser.VRMLBrowserCanvas;
import org.web3d.vrml.j3d.browser.VRMLUniverse;
import org.web3d.vrml.j3d.input.LinkSelectionListener;
import org.web3d.vrml.j3d.nodes.J3DViewpointNodeType;
import org.web3d.vrml.nodes.VRMLLinkNodeType;
import org.web3d.vrml.nodes.loader.ExternalLoadManager;
import org.web3d.vrml.nodes.loader.MemCacheLoadManager;
import org.web3d.vrml.nodes.loader.ScriptLoader;
import org.web3d.vrml.nodes.runtime.ExecutionSpaceManager;
import org.web3d.vrml.nodes.runtime.ListsRouterFactory;
import org.web3d.vrml.nodes.runtime.RouteManager;
import org.web3d.vrml.parser.VRMLParserFactory;
import org.web3d.vrml.scripting.ScriptEngine;
import org.web3d.vrml.scripting.jsai.VRML97ScriptEngine;
import org.web3d.x3d.dom.j3d.DOMEventHandler;
import org.web3d.x3d.dom.j3d.DOMtoJ3D;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * X3D Renderer - renders X3D (VRML like) documents - called from X3DMLFC for whole X3D docs
 * and X3DElementImpl for parasite documents.
 */
public class X3DRenderer implements LinkSelectionListener {

	// Viewpoint selector    
	private XSelectOne selectOne = null;
	private ScaleListener selectOneListener;

	// X3DMLFC - to access MLFClistener and such
	private MLFC x3dmlfc = null;

	// vrmlcanvas and universe required in cleanup().
	private VRMLBrowserCanvas vrmlcanvas = null;
	private VRMLUniverse universe = null;

	// All viewpoints, keys=viewpoint names, elements=the J3D nodes
	// Used to resolve node, if a viewpoint selector is clicked
	private Hashtable viewpoints = null;

	// All viewpoints, keys=the J3D nodes, elements=viewpoint names
	// Used to resolve description, if a link to a viewpoint node is clicked
	private Hashtable linkviewpoints = null;

	/** Mapping of def'd Viewpoints to their real implementation */
	// Used to resolve destination node (using DEF) if a link is clicked
	// keys = DEFs, elements = viewpoint J3D nodes
	private HashMap viewpointDefMap = null;

	/** The parser factory that we are going to use. */
	private VRMLParserFactory parserFactory;

	/** Base url for a scene */
	String baseurl = null;

	/** Dom to J3D refMap created from conversion */
	protected HashMap refMap;

	/** Do we support DOM events - current situation */
	private boolean supportEvents=false;

    /** The class which implments DOM events for us */
    private DOMEventHandler deh;
	
	/**
	 * Initializes some private variables.
	 */
	public X3DRenderer(MLFC m) {

		viewpoints = new Hashtable();
		linkviewpoints = new Hashtable();
		viewpointDefMap = new HashMap();
		
		x3dmlfc = m;
	}

	/**
	 * Get local name of a node.
	 * @param tagname		Tag name, including namspace prefix
	 * @return				Tag name without namespace prefix
	 */
	protected String getLocalname(String tagname) {
		int index = tagname.indexOf(':');
		if (index<0) return tagname;
		String localname = tagname.substring(index+1);
		return localname;
	}

	/**
	 * Convert X3D DOM doc into VRML graph and render it.
	 * cleanup() should be called when the document is no longer needed.
	 * @param node	X3D root node
	 * @return 		component containing the X3D document. (rendered as heavy weight).
	 */
	public Component render(Node node) {
		Log.debug("X3DRenderer.render()");

		// Code to try out parsing with Xerces
		//	  try {
		//	  	javax.xml.parsers.DocumentBuilderFactory factory=javax.xml.parsers.DocumentBuilderFactory.newInstance(); 
		//
		//	  	factory.setNamespaceAware( true );
		//
		//	//         	factory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",
		//	//         		"fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl"  );
		//	  	javax.xml.parsers.DocumentBuilder parser = factory.newDocumentBuilder();
		//	  	System.out.println("Xerces parser:"+parser.getClass()+". NSpaces:"+parser.isNamespaceAware());
		//	  	org.xml.sax.InputSource input = new org.xml.sax.InputSource(new FileReader(filename));
		//	  	doc =  parser.parse(input);
		//	  } catch (javax.xml.parsers.ParserConfigurationException e) {
		//	  	System.out.println(e);
		//	  	System.out.println(filename+": Error parsing.");
		//	  } catch(org.xml.sax.SAXException e) {
		//	  	System.out.println(e);
		//	  	System.out.println(filename+": Error parsing.");
		//	  } catch (java.io.IOException e) {
		//	  	System.out.println(e);
		//	  	System.out.println(filename+": Error parsing.");
		//	  }
		// End Xerces Parser

		// Set the ContentHandlers and ProtocolHandlers
		System.setProperty("uri.content.handler.pkgs",
			"vlc.net.content|fi.hut.tml.xsmiles.mlfc.x3d.content");
		System.setProperty("uri.protocol.handler.pkgs",
			"vlc.net.protocol");


		// GET MANAGERS and CREATE UNIVERSE

		//ExternalLoadManager lm = new SimpleLoadManager();
		ExternalLoadManager lm = new MemCacheLoadManager();
		ScriptLoader sl = new ScriptLoader();
		// This constructor has changed...
		RouteManager rm = new ExecutionSpaceManager(lm, sl);
		rm.setRouterFactory(new ListsRouterFactory());

		// This is temporary. Not actually functional until we work out how
		// to integrate DOM/SAI based scripts.
		try {
		    parserFactory = VRMLParserFactory.newVRMLParserFactory();
		} catch(FactoryConfigurationError fce) {
		    throw new RuntimeException("Failed to load factory");
		}

		// Need to load a bunch of script engines here....?
		// Not done - we cannot access some factories/builders (?)
		// We are always going to need a parser and a scene builder to display
		J3DSceneBuilderFactory b_fac =
		    new J3DSceneBuilderFactory(false,
		                               true,
		                               true,
		                               true,
		                               true,
		                               true,
		                               true,
		                               true);

		universe = new VRMLUniverse(rm, lm, sl);	

		ScriptEngine jsai = new VRML97ScriptEngine(b_fac,
		                                           universe,
		                                           parserFactory,
		                                           rm);

		sl.registerScriptingEngine(jsai);

		// CONVERT XML TO VRML //
		// parameters: BaseURI, VRML clock
		baseurl = x3dmlfc.getXMLDocument().getXMLURL().toString();
		// Ugly way to rip of the document path ("http://www.xsmiles.org/demo/x3d/links.x3d")
		baseurl = baseurl.substring(0, baseurl.lastIndexOf('/'));
		Log.debug("X3D PATH: "+baseurl);

		J3DVRMLScene s = convertDOMtoJ3D(node);
		if (s == null)
			return new Container();

		// SETUP UNIVERSE //
		GraphicsConfigTemplate3D template = new GraphicsConfigTemplate3D();
		template.setDoubleBuffer(template.REQUIRED);
		GraphicsEnvironment env =
			GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice dev = env.getDefaultScreenDevice();
		GraphicsConfiguration gfxConfig = dev.getBestConfiguration(template);

		vrmlcanvas = new VRMLBrowserCanvas(gfxConfig);
		vrmlcanvas.setStereoEnable(false);
		vrmlcanvas.setDoubleBufferEnable(true);

		// This will get link selection events.
		universe.setLinkSelectionListener(this);
		universe.addBrowserCanvas(vrmlcanvas);
		Log.debug("X3D Scene:"+s);
		universe.setScene(s);

		resolveViewpoints(s);

		return vrmlcanvas;
	}

	// Called if viewpoint control is changed
	private class ScaleListener implements ItemListener {
		public void itemStateChanged(ItemEvent e) {

			if (e.getStateChange()==ItemEvent.DESELECTED) return;
			String scaleStr=(String)e.getItem();

			J3DViewpointNodeType vp_node = (J3DViewpointNodeType)(viewpoints.get(scaleStr));
			vp_node.setBind(true);
		}
	}

	/**
	 * Convert DOM to J3DScene.
	 */
	private J3DVRMLScene convertDOMtoJ3D(Node node) {
		J3DVRMLScene s = null;
		DOMtoJ3D converter = new DOMtoJ3D(baseurl, universe.getVRMLClock());
		try  {	
		  	// Conversion
			s = converter.convertDoc(node); 
		} catch (org.web3d.x3d.dom.j3d.DOMtoJ3DException d)  {
			Log.error("DOMtoJ3D Exception:"+d);
			d.printStackTrace(System.err);  
			return null;
		} catch(NullPointerException e)  {
		  	Log.error("DOMtoJ3D Null Exception:"+e);
			e.printStackTrace(System.err);  
			return null;
		}

		if (s==null) {
			Log.error("X3D DOCUMENT COULD NOT BE LOADED!");
			throw new IllegalArgumentException("X3D document scene error.");
		}

		if (s.getRootNode() == null) {
		    Log.error("No RootNode.  Invalid URL?");
			return null;
		}

		// Make the DOM dynamic
		refMap = converter.getRefMap();
		setSupportEvents(node, true);

		// Set base url
		s.setWorldRootURL(baseurl);
		return s;
	}

	/**
	 * Initialize viewpoints after a X3D DOM has been converted into a VRMLScene.
	 * @param s 		J3DVRMLScene object
	 */
	private void resolveViewpoints(J3DVRMLScene s) {
		// GET VIEWPOINTS FOR LINK LISTENER and FOR GUI //

		viewpoints.clear();
		linkviewpoints.clear();

		// Grab the list of viewpoints and place them into the toolbar.
		List vp_list = s.getBindableNodes(J3DVRMLScene.VIEWPOINT_BINDABLE);

		if((vp_list != null) && (vp_list.size() != 0)) {
			Iterator itr = vp_list.iterator();

			// Active Viewpoint at start
			J3DViewpointNodeType active_vp = universe.getViewpoint();
			Log.debug("ACTIVE VP:"+active_vp);
			J3DViewpointNodeType vnode;
			int count = 1;
			String desc;
			//	TransformGroup tg;

			Vector viewtexts = new Vector();
			String active_view = null;

			while(itr.hasNext()) {
				vnode = (J3DViewpointNodeType)itr.next();
				desc = vnode.getDescription();

				if((desc == null) || (desc.length() == 0)) {
					desc = "Viewpoint " + count;
				}

				//	    tg = vnode.getPlatformGroup();

				// Activate and view initial viewpoint
				// active_vp seems to be strange viewpoint, so just use the first viewpoint
	//				if(vnode == active_vp) {
	//					active_vp.setBind(true);
				if(count == 1) {
					Log.debug("Activating: "+desc);
					vnode.setBind(true);
					active_view = desc;
				}

				// Save viewpoints, these are used when the user selects a viewpoint
				viewpoints.put(desc, vnode);
				// Save viewpoints, these are used when the user clicks on internal link
				linkviewpoints.put(vnode, desc);
				// Save - this vector goes to GUI
				viewtexts.addElement(desc);

				count++;
				Log.debug("VIEWPOINT FOUND: "+desc);
			}

			// Display the viewpoints only for a primary document with X3D as host
			if (x3dmlfc.isPrimary() && x3dmlfc.isHost() && viewtexts.size() > 0) {
				ComponentFactory cf=x3dmlfc.getMLFCListener().getComponentFactory();
                XPanel cb = x3dmlfc.getMLFCListener().getMLFCControls().getMLFCToolBar();
                cb.removeAll();
				selectOne = cf.getXSelectOne("minimal",false); // minimal will create a menu
				for (int i=0;i<viewtexts.size();i++)
				{
					selectOne.addSelection((String)viewtexts.elementAt(i));
				}
				if (active_view != null)
					selectOne.setSelected(active_view);
				selectOneListener = new ScaleListener();
				//				selectOne.addSelectionListener(selectOneListener);
				selectOne.addItemListener(selectOneListener);
				//				cf.addXComponent(selectOne);
				cb.add(selectOne);
			}

			// Finally set up the viewpoint def name list. Have to start from
			// the list of DEF names as the Viewpoint nodes don't store the DEF
			// name locally.
			viewpointDefMap.clear();
			Map def_map = s.getDEFNodes();
			itr = def_map.keySet().iterator();

			while(itr.hasNext()) {
				String key = (String)itr.next();
				Object vp = def_map.get(key);

				if(vp instanceof J3DViewpointNodeType)
					viewpointDefMap.put(key, vp);
			}
		}

	}

	/**
	 * Whether we should support events for this document
	 * Can be set anytime, before or after conversion
	 *
	 * @param set Should events be supported
	 */
	public void setSupportEvents(Node node, boolean set) {
	    EventTarget et=null;

	    if (node == null) {
	        supportEvents = set;
	        return;
	    }

	    try {
	        et = (EventTarget) node;
	    } catch (ClassCastException cce) {
	        if (set == true)
	            System.out.println("DOMImplementation does not support Events");
	        return;
	    }

	    if (set == true) {
	        deh = new DOMEventHandler(refMap, baseurl);

	        System.out.println("Adding EventListener");
	        et.addEventListener("DOMAttrModified", deh, false);
	        et.addEventListener("DOMNodeInserted", deh, false);
	        et.addEventListener("DOMNodeRemoved", deh, false);
		  //  et.addEventListener("DOMAttrModified", new TestEH(), false);
		} else {
	        if (supportEvents) {
	            et.removeEventListener("DOMAttrModified", deh, false);
	            et.removeEventListener("DOMNodeInserted", deh, false);
	            et.removeEventListener("DOMNodeRemoved", deh, false);
	        }
	    }

	    supportEvents = set;
	}
	
	// Test event handler
	private class TestEH implements org.w3c.dom.events.EventListener {
		public TestEH() {
		}

		public void handleEvent(org.w3c.dom.events.Event evt) {
			System.out.println("X3D EVENT GOT EVENT!!!!!!!!!!!!!!!!!!!!!!!!!!! "+evt);
		}
	}

	/**
	 * Refresh the X3D scene, reloads everything from the node.
	 * @param node		X3D root node (X3DElement)
	 */
	public void refresh(Node node) {

		J3DVRMLScene s = convertDOMtoJ3D(node);
		if (s == null)
			return;
			
		universe.setScene(s);
		resolveViewpoints(s);
	}

	/**
	 * Clean up the X3D renderer - stop timers and renderers.
	 * This method should be called to clean up the system
	 * when the document is no longer needed.
	 */
	public void cleanup() {

		if (x3dmlfc.isHost() == true) {
			// Remove the viewpoint selector
            XPanel cb = x3dmlfc.getMLFCListener().getMLFCControls().getMLFCToolBar();
			cb.removeAll();
		}

		universe.removeBrowserCanvas(vrmlcanvas);
		vrmlcanvas.stopRenderer();

		viewpoints.clear();
		linkviewpoints.clear();
		viewpointDefMap.clear();

		universe = null;
		vrmlcanvas = null;
	}

	//----------------------------------------------------------
	// Methods required by the LinkSelectionListener interface.
	//----------------------------------------------------------

	// Set to true if valid url has been selected
	private boolean skiplink = false;

	/**
	 * Called from X3DAnchorElementImpl to bind (activate) a viewpoint.
	 * @param vpn		J3DViewpointNodeType
	 * @return 			true if everything was ok
	 */
	public boolean bindViewpoint(J3DViewpointNodeType vpn) {
		if(vpn != null) {
			vpn.setBind(true);
			// Change the selector
			String desc = (String)linkviewpoints.get(vpn);
			Log.debug("VIEWPOINT DESC:"+desc);
			if (desc != null && selectOne != null)
				selectOne.setSelected(desc);
		}
		else {
			return false;
		}
		return true;
	}

	/**
	 * Invoked when a link node has been activated. This is the node that has
	 * been selected.
	 *
	 * @param node The selected node
	 */
	public void linkSelected(VRMLLinkNodeType node) {

		// Xj3D seems to call this method twice (???!)
		// Skip every second linkSelected call
		if (skiplink == true) {
			skiplink = false;
			return;
		}
		skiplink = true;

		String[] url_list = node.getUrl();

		for(int i = 0; i < url_list.length; i++) {
			Log.debug("URL ACTIVATED: "+i+" - "+url_list[i]+" node:"+node);
			if(url_list[i].charAt(0) == '#') {
				// move to the viewpoint.
				String def_name = url_list[i].substring(1);
				J3DViewpointNodeType vp =
					(J3DViewpointNodeType)viewpointDefMap.get(def_name);

				if (bindViewpoint(vp) == false) {
					x3dmlfc.getMLFCListener().setStatusText("Unknown Viewpoint " + def_name);
					Log.error("Unknown Viewpoint " + def_name);
				}
			}
			else {
				// load the new URL.
				try {
					Log.debug("...X3D going to "+url_list[i]);
					URL u = new URL(x3dmlfc.getXMLDocument().getXMLURL(), url_list[i]);
					x3dmlfc.getMLFCListener().openLocation(u);

				} catch(MalformedURLException mue) {
					x3dmlfc.getMLFCListener().setStatusText("Invalid URL");
					Log.error("Invalid URL: " + url_list[i] + mue);
				}
			}
		}
	}

	/* This part is not necessary - just some testing how to grab a image of GUI.
	 */
	static public MLFCListener mlfcListener = null;	
	/**
	 * Testing loading XML as texture.
	 */
	 static public void loadXMLtexture(Container container) {
		// Create the components		
		JPanel xmlComp = new JPanel();
		// Set it to transparent, to prevent it from drawing
		xmlComp.setOpaque(false);
		xmlComp.setLayout(new BorderLayout());
		xmlComp.setVisible(true);
	
		//			container.add(p,0);
	
		// create an XLink object
		XLink link = null;
		try {
			link = new XLink(new URL("file:/D:/source/xsmiles/demo/xhtml/box.xhtml"));
		} catch( MalformedURLException e) {
		}
		// call request browser to display a secondary document in 
		// container xmlComp
		mlfcListener.displayDocumentInContainer(link,xmlComp);

		Component comp = xmlComp; // the top-level component
		comp.setSize(200,200);
			                       // you want to capture
		
		// create an image for the capture
		Frame frame = new Frame(); 
		frame.addNotify(); 
		Image img = frame.createImage( 200, 200); 

//		Image img  = comp.createImage(comp.getWidth(), comp.getHeight());
///		comp.addNotify();

		System.out.println("IMAGE:"+img);
		// grab a graphics context for that image
		Graphics g = img.getGraphics();

		// print the GUI into that image
		comp.printAll(g);
		
		Testi j = new Testi();
		j.i = img;
		container.add(j, "Center");
//		comp.removeNotify();
	 }

	 private static class Testi extends JComponent {
	 	public Image i = null;
	 	public void paint(Graphics g) {
	 		g.drawLine(0,0, 100,100);
	 		if (i != null)
	 			g.drawImage(i, 5, 5, this);
	 	}
	 }

	/**
	 * ?????????????
	 */
	private void tryloadinggif() {

	// Try loading gif
	
	// Standard imports
		System.out.println("TADAA1");
		/** The current resource connection if being used */
		ResourceConnection currentConnection;
		String mime_type = null;
		org.ietf.uri.URL[] source_urls = null;
		Object content;
	    try {
	        URI uri = URIUtils.createURI("file:/D:/source/xsmiles/demo/x3d/boxes.xml");
	        source_urls = uri.getURLList();
	    } catch(java.io.IOException ioe) {
	        // ignore and move on
	    }
	    System.out.println("TADAA2");

	    // loop through the list of candidate URLs and look for
	    // something that matches. If it does, set it in the node
	    // for use.
	    for(int j = 0; (j < source_urls.length); j++) {
	        System.out.println("TADAA3 " +source_urls[j]);
	        try {
	            currentConnection = source_urls[j].getResource();
	        } catch(IOException ioe) {
	            continue;
	        }
	        System.out.println("TADAA3.5 " +currentConnection);

	        try {
	            currentConnection.connect();
	            mime_type = currentConnection.getContentType();
	            System.out.println("TADAA3.7 connect4d, mime" +mime_type);
	            content = null;
//				if (mime_type != null) {
					content = currentConnection.getContent();
					System.out.println("GOT CONTETTTII"+content);
//				}
			} catch(IOException ioe) {
		//                System.out.println("IO Error reading external file " + ioe);
		//                ioe.printStackTrace();
				System.out.println("AARGH!");		
					    // ignore and move on
			} catch(IllegalArgumentException iae) {
			    // from the setContent method
			    System.out.println("Error setting external content:" + iae);
			    continue;
			}
	    }
	    System.out.println("TADAA4");


//		org.ietf.uri.URLConnection test = null;
//		try {
//				test = (new URL("file:/D:/source/xsmiles/demo/x3d/boxes.xml")).openConnection();
//		} catch (MalformedURLException e) {
//			Log.error("MALFORMED ORULRLI!");
//		} catch(java.io.IOException ioe) {
//			Log.error("IOIOIOE XPE!");
//		}
//
//		try {
//		    System.out.println("GOT CONTENT:"+test.getContent());
//		} catch(java.io.IOException ioe) {
//			Log.error("IOIOIOE XPE2!");
//		}
//

	}	

}
        



