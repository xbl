/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.x3d;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AnimationService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import java.util.Hashtable;
import java.util.Enumeration;

/**
 * General X3D element. To obtain convert DEF attribute to id attribute and to
 * animate all elements.
 *
 * @author Kari
 */
public class GenericElementImpl extends XSmilesElementImpl implements AnimationService
{
	// XSmilesDocumentImpl - to create new elements
	private DocumentImpl ownerDoc = null;

	// Reference to the MLFC (all parasite X3D elements share the same MLFC)
	protected X3DMLFC mlfc;

	// Animation attributes, value="***REMOVED***" means attr has been removed.
	private Hashtable animAttrs = null;

	// Original DOM attributes - to be restored after animation.
	private Hashtable domAttrs = null;
	
	private static final String REMOVED = "***REMOVED***";

	/**
	* Constructor - Set the owner, name and namespace.
	*/
	public GenericElementImpl(DocumentImpl owner, String namespace, String tag, X3DMLFC m) {
		super(owner, namespace, tag);
		ownerDoc = owner;
		mlfc = m;
		
		animAttrs = new Hashtable();
		domAttrs = new Hashtable();
	}

	public void dispatch(String type) {
		// Dispatch the event
		Log.debug("Dispatching X3D Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	 * Initialize this element.
	 * This requires that the DOM tree is available.
	 */
	public void init() {
		super.init();
	}

	/**
	 * Do cleanup 
	 */
	public void destroy() {
		super.destroy();
	}
	
	/**
	 * Get Id Kludge - get DEF attribute instead!
	 */
	 public String getId() {
	 	return super.getAttribute("DEF");
	 }

	 /**
	  * Get Id Kludge - get DEF attribute instead!
	  */
	  public String getAttribute(String s) {
	  	if (s.equals("id") == true)
			return super.getAttribute("DEF");
		else
			return super.getAttribute(s);
	  }

	//// AnimationService implementation ////
	
	/**
	 * Convert String attribute to an float value
	 */
	public float convertStringToUnitless(String attr, String value)  {
		return 0;
	}
	public String convertUnitlessToString(String attr, float value)  {
		return null;
	}

	/**
	 * Get animation value of an attribute.
	 * @param attr	Animated attribute
	 */
	public String getAnimAttribute(String attr)  {
		return (String)animAttrs.get(attr);
	}

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 * @param attr	Attribute to be animated
	 * @param value	Animation value to be set
	 */
	public void setAnimAttribute(String attr, String value)  {
		animAttrs.put(attr, value);
	}

	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 * @param attr	Attribute to be animated (animation removed)
	 */
	public void removeAnimAttribute(String attr)  {
		animAttrs.put(attr, REMOVED);
	}

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 */
	public void refreshAnimation()  {
		String key, value;
		for (Enumeration e = animAttrs.keys(); e.hasMoreElements() ;) {
			key = (String)e.nextElement();
			value = (String)animAttrs.get(key);
			if (value.equals(REMOVED) == true) {
				// Set original DOM and Remove from Hashtable
				value = (String)domAttrs.get(key);
				if (value.equals("") == true)
					removeAttribute(key);
				else	
					setAttribute(key, value);
				animAttrs.remove(key);
//				System.out.println("Removeing "+key);
			} else {
				// Save old DOM value, if not yet saved
				if (domAttrs.get(key) == null) {
					String dom = getAttribute(key);
					if (dom == null)
						dom = "";
					domAttrs.put(key, dom);
				}

				// Set new animation value
				setAttribute(key, value);
//				System.out.println("Setting "+key+" "+value);
			}
		}
	}
	
}


