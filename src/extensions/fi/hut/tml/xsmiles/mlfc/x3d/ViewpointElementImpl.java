/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.x3d;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AnimationService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.util.Hashtable;
import java.util.Enumeration;

import org.web3d.vrml.j3d.nodes.J3DVRMLNode;
import org.web3d.vrml.nodes.VRMLNodeListener;
import org.web3d.vrml.j3d.nodes.core.J3DTouchSensor;
import org.web3d.vrml.j3d.nodes.J3DViewpointNodeType;

/**
 * ViewpointNode Element. This element has setBind() method,
 * to bring this viewpoint to the top of the stack (viewable).
 * This element might become redundant, if setAttribute("bind", value) works.
 *
 * @author Kari
 */
public class ViewpointElementImpl extends GenericElementImpl
{
	// This DOM node as VRMLNode
	private J3DViewpointNodeType vrmlNode = null;

	// Renderer to activate viewpoints
	private X3DRenderer renderer = null;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public ViewpointElementImpl(DocumentImpl owner, String namespace, String tag, X3DMLFC m) {
		super(owner, namespace, tag, m);
	}

	/**
	 * Initialize this element.
	 * This requires that the DOM tree is available.
	 */
	public void init() {
		Node n = this;
		
		// TODO : MAKE THIS WORK IN A HOST AS WELL
		if (mlfc.isHost() == true)
			return;
		Log.debug("VIEWPOINT ELEMENT PARASITEINIT!");
		
		// Search for X3D root element
		while ((n instanceof X3DElementImpl) == false) {
			n = n.getParentNode();
			if (n == null) {
				Log.error("Viewpoint Element must be under a X3D root element!");
				return;
			}
		}
		
		// Got it - now, ask for VRMLNode J3DViewpointNodeType
		vrmlNode = (J3DViewpointNodeType)((X3DElementImpl)n).getVRMLNode(this);

		renderer = ((X3DElementImpl)n).getRenderer();

	}

	/**
	 * Bind viewpoint, sets it active. NOTE: false value doesn't work yet.
	 * @param bind		true to bind, false to unbind
	 */
	public void setBind(boolean bind) {
		Log.debug(getAttribute("id")+" setBind: "+bind);
		// Call renderer to activate this viewpoint
		if (bind == true)
			renderer.bindViewpoint(vrmlNode);
	}

	/**
	 * Do cleanup 
	 */
	public void destroy() {
	}
}


