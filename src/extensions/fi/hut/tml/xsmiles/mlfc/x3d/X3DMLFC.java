/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.x3d;

// MLFC specific imports

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.awt.Component;
import java.awt.Container;

// If this class exists, then Java3D exists...
import javax.media.j3d.GraphicsConfigTemplate3D;

/**
 * X3D MLFC - renders X3D (VRML like) documents.
 *
 * !! NOTE: This class requires Java3D to compile. If you don't have Java3D installed,<br/>
 * !! just remove this package and try recompiling. The impact of removing this package is <br/>
 * !! that you cannot display X3D documents.<br/>
 *<br/>
 * !! NOTE: This class also requires Java 1.2 (HashMap etc.)<br/>
 */
public class X3DMLFC extends MLFC {

	// Instance of X3D renderer
	protected X3DRenderer renderer = null;
	
	/**
	 * Empty constructor, but checks that Java3D exists.
	 */
	public X3DMLFC() {
		if (isJava3DAvailable() == false) {
			throw new IllegalArgumentException("Java3D not installed! It required to display X3D documents. See X-Smiles installation page for details.");
		}
	}

	/**
	 * Get the version of the MLFC. This version number is updated 
	 * with the browser version number at compilation time. This version number
	 * indicates the browser version this MLFC was compiled with and should be run with.
	 * @return 	MLFC version number.
	 */
	public final String getVersion() {
		return Browser.version;
	}



	/**
	 * Create a DOM element. We need a custom X3D element for X3D parasite language.
	 */
	public Element createElementNS(DocumentImpl doc, String ns,String tagname) {
		if(getLocalname(tagname).equals("X3D")) {
			Log.debug("ROOT X3D, tag: "+tagname );
			Element element = new X3DElementImpl(doc, ns, tagname, this);
			return element;
		} else if(getLocalname(tagname).equals("TouchSensor")) {
			Log.debug("TouchSensor creating, tag: "+tagname );
			Element element = new TouchSensorNodeElementImpl(doc, ns, tagname, this);
			return element;
		} else if(getLocalname(tagname).equals("Viewpoint")) {
			Log.debug("Viewpoint creating, tag: "+tagname );
			Element element = new ViewpointElementImpl(doc, ns, tagname, this);
			return element;
		} else {
			// All other elements are generic X3D elements.
			return new GenericElementImpl(doc, ns, tagname, this);
		}
	}


	/**
	 * Convert X3D DOM doc into VRML graph and render it.
	 */
	public void start( ) {
		// Do not process parasite languages here. X3DElement will take care of them.
		if (isHost() == false) {
			Log.debug("Parasite X3D.start()");
			return;
		}

		Log.debug("X3D.start()");

		  	// Conversion
		Document doc = this.getXMLDocument().getDocument();
		Node node = doc.getDocumentElement();
		renderer = new X3DRenderer(this);
		Component canvas = renderer.render(node);
		if (canvas == null) {
			Log.debug("X3D DOM parse error, vrmlcanvas null.");
			throw new IllegalArgumentException("X3D Parse Error - see the log for details.");
		}

		Log.debug("X3D DOM parsed & doc finished.");

		Container container=this.getContainer();
		container.add(canvas, "Center");
	}

	/**
	 * Stop rendering X3D document.
	 */
	public void stop() {

		// Destroy parasite timers
		if (isHost() == false) {
			Log.debug("X3DMLFC.para-stop()");
			return;
		}

		Log.debug("X3DMLFC.stop()");
		this.getContainer().removeAll();

		if (renderer != null) {
			renderer.cleanup();
		}
	}
	
	/**
	 * Check if Java3D is available.
	 * @return		true if Java3D is available, false otherwise
	 */
	private boolean isJava3DAvailable()
	{
		try
		{
			GraphicsConfigTemplate3D test = new GraphicsConfigTemplate3D();
		} catch (NoClassDefFoundError e)
		{
			Log.error("Java3D not available! It is required to display X3D documents.");
			Log.error("See the X-Smiles installation page for details.");
			return false;

			// This is for kaffe
		} catch (NoSuchMethodError e)
		{
			Log.error("Java3D not available! It is required to display X3D documents.");
			Log.error("See the X-Smiles installation page for details.");
			return false;

			// All other exceptions..
		} 
//		catch (Exception e)
//		{
//			Log.error("Java3D not available! It is required to display X3D documents.");
//			Log.error("See the X-Smiles installation page for details.");
//			return false;
//		}
		return true;
	}

}
        



