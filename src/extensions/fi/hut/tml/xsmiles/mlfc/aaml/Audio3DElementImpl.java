/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.aaml;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.dom.VisualComponentService;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.AnimationService;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;
import java.net.URL;

/**
 * Audio3D element plays audio in a 3d space. The audio file must be in WAVE format,
 * with the following header: RIFF....WAVEfmt ........xx..xx......data
 * Otherwise, the file is not loaded or played!
 *
 * @author Kari Pihkala
 */
public class Audio3DElementImpl extends XSmilesElementImpl 
				implements AudioUpdate, VisualComponentService, Runnable {

	// AAMLFC
	protected AAMLFC aaMLFC = null;
	
	// XSmilesDocumentImpl - to create new elements
	protected DocumentImpl ownerDoc = null;

	protected int width = 10, height = 10;
	protected boolean visible = false;
	protected Component component = null;
	protected Hashtable regions = null;

	protected float front = 0, back = 0, depth = 0, left = 0, top = 0;
	protected float lastfront = 0, lasttop = 0, lastleft = 0;
	protected float last2front = 0, last2top = 0, last2left = 0;
	protected float last3front = 0, last3top = 0, last3left = 0;

	// Sound id for native device
	protected int sid = 0;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public Audio3DElementImpl(DocumentImpl owner, AAMLFC aamlfc, String namespace, String tag)
	{
		super(owner, namespace, tag);
		aaMLFC = aamlfc;
		ownerDoc = owner;
		regions = new Hashtable();
		Log.debug("Audio3D element created!");
	}

	private void dispatch(String type)
	{	 
		// Dispatch the event
		Log.debug("Dispatching Audio3D Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}
    
    // currently supports EVENT_STYLECHANGED with object=null, in future there would be more 
    public void visualEvent(int event,Object object)
    {
        
    }


	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		// Not for Listeners
		if (this instanceof ListenerElementImpl)
			return;
	
		Log.debug("Audio3D.init");

//		component = new Button("Test");
		if (getAttribute("debug").equals("true"))
			component = new InvisibleComponent(true);
		else	
			component = new InvisibleComponent(false);
		component.setSize(width, height);
		
		String src = getAttribute("src");
		if (src == null || src.length() == 0)  {
			Log.error("Audio3D src parameter missing");
			return;
		}
		/// Create a file://xxx from src attribute
		URL u = aaMLFC.createURL(src);
		src = u.toString();
		if (src.startsWith("file:") == false)  {
			Log.error("Audio3D src attribute does not allow http:, use file:");
			return;
		}
		src = src.substring(6);
		// Convert to DOS format.
		src = src.replace('/','\\');
		///
		if (aaMLFC.nativePlayer != null)  {
			sid = aaMLFC.nativePlayer.prefetch(aaMLFC.dso, src);
			
			if (sid == 0)  {
				Log.error("Audio3D URL "+src+" not found or has wrong format. Use WAVE-files with specific headers.");
			} else
				Log.debug("AAML: "+src+" prefetched, sid "+sid);
		}

//		aaMLFC.nativePlayer.play(aaMLFC.dso, sid);
		
		super.init();
	}

	/**
	 * Update the audio data to the native side.
	 */
	public void update()  {
		// Safe check for SVG, which displays this before calling to init()
		if (component == null)
			return;

		// Get loop flag
		boolean loop = false;
		if (getAttribute("loop").equals("true"))
			loop = true;
			
		// Get depth coords (x,y,z) (left, top, front) (region, inline or CSS)
		getRegionCoords();

		// override absolute region coordinates, if coord!="global"
		if (getAttribute("coord").equals("global") == false)  {
			// Get other coords (x,y), from rendering Component
			Dimension coords = getCoords(component.getParent());
			left = coords.width+component.getWidth()/2;
			top = coords.height+component.getHeight()/2;
		}
		
		// Get Listener as Reference Point
		left = left - aaMLFC.getListener().getLeft();
		top = top - aaMLFC.getListener().getTop();
		front = front - aaMLFC.getListener().getFront();
		
		// Apply translation, scaling and rotation
		Point3D p = translate(left, top, front);
		left = p.x;
		top = p.y;
		front = p.z;

		// Remove Listener as Reference Point
		left += aaMLFC.getListener().getLeft();
		top += aaMLFC.getListener().getTop();
		front += aaMLFC.getListener().getFront();

		// Calculate doppler, average of 3 past values
		float dx = ((last2left-last3left) + (lastleft-last2left) + (left-lastleft)) / 3;
		float dy = ((last2top-last3top) + (lasttop-last2top) + (top-lasttop)) / 3;
		float dz = ((last2front-last3front) + (lastfront-last2front) + (front-lastfront)) / 3;

		// Disable doppler effect, if not "true"	
		if (getAttribute("doppler").equals("true") == false)  {
			dx = 0;
			dy = 0;
			dz = 0;
		}
//		Log.debug("Audio "+getId()+" updating data."+left+" "+top+" "+front+" d"+(dx)+" "+dy+" "+(dz));
		
		// Get min/max distance
		String val = getAttribute("minDistance");
		if (val == null || val.length() == 0)  // default value
			val = "100";
		float min = convertToFloat(val);		
		val = getAttribute("maxDistance");
		if (val == null || val.length() == 0)  // default value
			val = "1000";
		float max = convertToFloat(val);		
		val = getAttribute("outLevel");
		if (val == null || val.length() == 0)  // default value
			val = "0";
		int outVol = (int)(100f*convertToFloat(val));		
		
		// Set audio parameters
		if (aaMLFC.nativePlayer != null && sid != 0)
			aaMLFC.nativePlayer.setParams(aaMLFC.dso, sid, left, top, front, 
						dx, dy, dz,
						0, 0, 1, 
						outVol, 0, max, min,
						0, 360, 100, 100, 0, loop);
//    public native boolean setParams(int dso, int id, float x, float y, float z,
//									float vx, float vy, float vz,
//									float coneox, float coneoy, float coneoz, 
//									int coneOutVol, int insideConeAngle, float maxDist, float minDist,
//									int mode, int outsideConeAngle, float freq, float vol, float pan);
		
		// Save values for Doppler effect (=lastleft-left)
		last3left = last2left;
		last3top = last2top;
		last3front = last2front;
		last2left = lastleft;
		last2top = lasttop;
		last2front = lastfront;
		lastleft = left;
		lasttop = top;
		lastfront = front;
	}

	/**
	 * Return the coordinates of this audio element.
	 */
	protected Dimension getCoords(Component parent)  {
		int x = 0;//parent.getLocation().x;
		int y = 0;//parent.getLocation().y;
		while (parent != null)  {
			x += parent.getLocation().x;
			y += parent.getLocation().y;
			parent = parent.getParent();
		}
		return new Dimension(x,y);
	}

	/**
	 * Get the front, back, depth values of the element.
	 * Returns either region, inline or CSS coords (left, top, front).
	 * Note, this cannot handle SMIL right & width attribute combination!
	 * @return true if depth information was found.
	 */
	protected boolean getRegionCoords()  {
		// Try to get region, with aaml namespace or smil namespace.
		String region = getAttribute("region");
		if (region == null)
			region = getAttributeNS(AAMLFC.namespace, "region");
		if (region == null)
			region = getAttributeNS(AAMLFC.smilnamespace, "region");
		if (region != null && region.length() != 0)  {
			// Get region element's depth info
			Element e = getRegionElement(region);
			if (e == null)  {
				Log.error("Region "+region+" not found for Audio Element!");
				return false;
			}
			front = convertToFloat(getNSAttr(e, "front"));
			back = convertToFloat(getNSAttr(e, "back"));
			depth = convertToFloat(getNSAttr(e, "depth"));
			left = convertToFloat(getNSAttr(e, "left"));
			top = convertToFloat(getNSAttr(e, "top"));
			return true;
		}
		
		// Try to get inline attribute
		if (getNSAttr(this, "front").equals("") == false)  {
				front = convertToFloat(getNSAttr(this, "front"));
				back = convertToFloat(getNSAttr(this, "back"));
				depth = convertToFloat(getNSAttr(this, "depth"));
				left = convertToFloat(getNSAttr(this, "left"));
				top = convertToFloat(getNSAttr(this, "top"));
				return true;
		}
		
		// Try to get CSS value
		front = convertToFloat(getStyle(this, "front"));
		back = convertToFloat(getStyle(this, "back"));
		depth = convertToFloat(getStyle(this, "depth"));
		depth = convertToFloat(getStyle(this, "left"));
		depth = convertToFloat(getStyle(this, "top"));
		
		// If CSS not found, this should use language specific way... (x3d, svg..)
		
		return true;
	}

	/**
	 * Get either animated or normal attribute (AAML, SMIL or no namespace).
	 */
	protected String getNSAttr(Element e, String a)  {
		if (e instanceof AnimationService)  {
			return ((AnimationService)e).getAnimAttribute(a);
		} else  {
			String attr = e.getAttribute(a);
			if (attr == null || attr.equals(""))  {
				attr = getAttributeNS(AAMLFC.namespace, a);
				if (attr == null || attr.equals(""))  {
					attr = getAttributeNS(AAMLFC.smilnamespace, a);
				}
			}
			return attr;
		}
	}

	/**
	 * Convert string val to float.
	 * returns 0 in errors.
	 */
	protected float convertToFloat(String val)  {
		try  {
			return Float.parseFloat(val);
		} catch (NumberFormatException e)  {
			return 0.0f;
		}
	}

	/**
	 * Search the region element. Use cache to find it quickly.
	 */
	private Element getRegionElement(String name)  {
		Element reg = null;
			
		reg = (Element)regions.get(name);
		if (reg != null)
			return reg;
			
		// Go through all elements until the matching one is found.
		// Searches through all children under root element in a depth-first order.
		if (name != null && name.length() != 0)  {
			reg = searchBasicRegion(name, getOwnerDocument().getDocumentElement());
			if (reg != null)  {
				regions.put(name, reg);
				return reg;
			}
		}
		
		// Not found.
		return null;
	}

	private Element searchBasicRegion(String name, Node se)  {
		Element reg = null;
		NodeList children = se.getChildNodes();
		Node e;

		// Go through all regions until the matching one is found.
		// Searches through all children in a depth-first order.
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);
			reg = searchBasicRegion(name, e);
			if (reg != null)
				return reg;

			if (e instanceof Element)  {
				if (((Element)e).getAttribute("regionName").equals(name)) {
					return (Element)e;
				}
				if (((Element)e).getAttribute("id").equals(name)) {
					return (Element)e;
				}
			}
		}
		return null;
	}

	protected String getLocalname(String tagname)
	{
		int index = tagname.indexOf(':');
		if (index<0) return tagname;
		String localname = tagname.substring(index+1);
		return localname;
	}

	/**
	 * Calculate matrix translation.
	 * Uses child elements translate, rotate, scale, in the order in the DOM.
	 */
	protected Point3D translate(float left, float top, float front)  {
		Element reg = null;
		NodeList children = this.getChildNodes();
		Node e;
		float val;

		// Go through all children.
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);

			if (e instanceof Element)  {
				String tag = getLocalname(((Element)e).getNodeName());
				if (tag.equals("scale")) {
					String sx = ((Element)e).getAttribute("x");
					if (!sx.equals(""))  {
						val = convertToFloat(sx);
						left = left * val;						
					}
					String sy = ((Element)e).getAttribute("y");
					if (!sy.equals(""))  {
						val = convertToFloat(sy);
						top = top * val;						
					}
					String sz = ((Element)e).getAttribute("z");
					if (!sz.equals(""))  {
						val = convertToFloat(sz);
						front = front * val;						
					}
				}
				if (tag.equals("rotate")) {
					String sx = ((Element)e).getAttribute("x");
					String sy = ((Element)e).getAttribute("y");
					String sz = ((Element)e).getAttribute("z");
					String sa = ((Element)e).getAttribute("angle");
					// rotate (fake - changes top to front)
					float temp = front;
					front = top;
					top = temp;	
					
				}
				if (tag.equals("translate")) {
					String sx = ((Element)e).getAttribute("x");
					if (!sx.equals(""))  {
						val = convertToFloat(sx);
						left = left + val;						
					}
					String sy = ((Element)e).getAttribute("y");
					if (!sy.equals(""))  {
						val = convertToFloat(sy);
						top = top + val;						
					}
					String sz = ((Element)e).getAttribute("z");
					if (!sz.equals(""))  {
						val = convertToFloat(sz);
						front = front + val;						
					}
				}
			}
		}
		return new Point3D(left, top, front);
		
	}

	/**
	 * Destroy this element.
	 */
	public void destroy()
	{
		setVisible(false);
	}
	
	//// CSS STUFF ///

	/**
	 * CSS: Returns the CSS property for an element. If the element doesn't support
	 * CSS, then "" will be returned.
	 *
	 * @param	The style of element e is asked for.
	 * @param	prop is the CSS property asked for.
	 * @return 	the value of CSS property.
	 */	
	private String getStyle(Element e, String prop) {
		if (e instanceof StylableElement) {
			CSSStyleDeclaration style = ((StylableElement)e).getStyle();
			CSSValue val = style.getPropertyCSSValue(prop);
			if (val != null)
				return val.getCssText();
		}
		
		return "";
	}
	
	/**
	 *  The class attribute. This method should be called getClass, but it is 
	 * already reserved by java.lang.Object.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getClassName() {
		return getAttribute("class");
	}
	
	public void setClassName(String cl) throws DOMException {
		if (cl != null && cl.length() > 0) {
		  setAttribute("class", cl);
		} else {
		  removeAttribute("class");
		}
	}
	
	
	//////// VISUALCOMPONENTSERVICE /////////
	
	/**
	 * Return the visual component for this extension element
	 */
	public Component getComponent()  {
		return component;
	}
	
	    /**
	 * Returns the approximate size of this extension element
	 */
	public Dimension getSize()  {
		return new Dimension(width, height);
	}
	public void setZoom(double zoom)  {
		return;
	}
	public void run()  {
		// play sound
		boolean f = false;
		if (aaMLFC.nativePlayer != null && sid != 0)  {
			f = aaMLFC.nativePlayer.play(aaMLFC.dso, sid);
			Log.debug ("dso:"+aaMLFC.dso+" sid:"+sid+" flag:"+f);
		} else  {
			Log.debug("Trying to play..."+sid);
		}
		while (true)  {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {}
		
		}
	}
	public void setVisible(boolean v)  {
	
		if (visible == false && v == true)  {
			Log.debug("Audio3D Visible - playing");
			// Set position
			update();
			// Remove averaging effect (old coords)
			last3left = left;
			last3top = top;
			last3front = front;
			last2left = left;
			last2top = top;
			last2front = front;
			lastleft = left;
			lasttop = top;
			lastfront = front;
			// Set velocity with the correct averaging
			update();

			// play sound
//			boolean f = false;
//			if (aaMLFC.nativePlayer != null && sid != 0)  {
//				f = aaMLFC.nativePlayer.play(aaMLFC.dso, sid);
//				Log.debug ("dso:"+aaMLFC.dso+" sid:"+sid+" flag:"+f);
//			} else  {
//				Log.debug("Trying to play..."+sid);
//			}
			Log.debug("SPAWNED A NEW THREAD!");
			new Thread(this).start();


		} else if (visible == true && v == false)  {
			Log.debug("Audio3D Invisible - stopping");
			// stop sound
			if (aaMLFC.nativePlayer != null && sid != 0)  {
				aaMLFC.nativePlayer.stop(aaMLFC.dso, sid);
			}
		}

		visible = v;
		if (component != null)
			component.setVisible(v);
	}
	public boolean getVisible()  {
		return visible;
	}
	
	public class InvisibleComponent extends Component  {
		boolean debug = false;
	
		public InvisibleComponent(boolean d)  {
			debug = d;
		}
		
		// No paint
		public void paint(Graphics g)  {
		// for debugging - draws the borders of the component.
			if (debug == true)  {
		     g.setColor(Color.blue);
		     g.drawRect(0, 0, getWidth()-1, getHeight()-1);
			}
		}
	}
}


