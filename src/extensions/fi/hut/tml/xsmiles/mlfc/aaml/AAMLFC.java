/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

/**
 * To build this AAML, you need to
 * 1. Compile all these classes under aaml folder.
 * 2. Copy AdvancedAudioML.dll to the xsmiles/bin folder (required native library for Win32)
 *
 * If you have problems hearing the sounds, try reactivating the window that was
 * active when X-Smiles was launched (can be console window or IDE window).
 * If the sound is then heard, the problem is that windows associates sounds to
 * a wrong window. You can get correct association by removing the initializeAAML() call
 * in the Xsmiles.java class. However, this will disable EAX environmental effects.
 */
package fi.hut.tml.xsmiles.mlfc.aaml;

import java.net.URL;
import java.awt.Dimension;
import java.awt.Container;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.XMLDocument;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import java.util.*;

import java.awt.*;

// Native Class for DirectX
import fi.hut.tml.xsmiles.mlfc.aaml.nai.NativeAudioDirectX;

/**
 * Advanced Audio Markup Language (AAML). 
 * Namespace: http://www.xsmiles.org/2002/aaml
 *
 * @author Kari Pihkala
 */
public class AAMLFC extends MLFC {

	static public boolean forceInitialization() {
	  nativePlayer = new NativeAudioDirectX();
	  if (nativePlayer.isLibraryValid() == false)  {
		  Log.error("AdvancedAudioML.dll not found - AAML disabled.");  
		  return false;
	  }
	  
	  dso = nativePlayer.init();

	  if (nativePlayer.isEAXAvailable(dso) == false) 
	  	Log.debug("EAX NOT SUPPORTED!");
	  else
	  	Log.debug("EAX _SUPPORTED_!");
	
		return true;
	}	
////////////////////////////////


	public static final String namespace = "http://www.x-smiles.org/2002/aaml";
	public static final String smilnamespace = "http://www.w3.org/2001/SMIL20/Language";

	private static int SampleInterval = 50;

	// DocumentImpl
	private DocumentImpl docImpl = null;

	// List of elements in this language
	private Vector aamlElements;

	// Set to true if listener found
	private ListenerElementImpl listenerElement = null;

	// Samples locations
	private AAScheduler scheduler = null;

	// The Environment element - has global scaling, translation, rotation
//	private Environment environment;

	// DirectSoundObject id for native device
	public static int dso = 0;

	// Native class reference
	public static NativeAudioDirectX nativePlayer = null;

	// Window size
	Dimension windowSize;
	
	/**
	* Constructor.
	*/
	public AAMLFC()
	{
		aamlElements=new Vector();

		// Initialize native device
		if (nativePlayer == null)
			if (forceInitialization() == false)
				return;
		try  {
//			nativePlayer = new NativeAudioDirectX();
			Log.debug("NativeAudioDirectX instantiated.");
			if (nativePlayer != null)  {
			//	dso = nativePlayer.init();
				Log.debug("NativeAudioDirectX init() has been called, got dso "+dso);
				if (nativePlayer.isEAXAvailable(dso) == false) 
					Log.error("EAX NOT SUPPORTED!");
				else
					Log.debug("EAX SUPPORTED!");
			}

		} catch (java.lang.UnsatisfiedLinkError e)  {
			Log.error("Native library for AAMLFC not installed!");
			throw e;
		}
	}

	/*
	protected String getLocalname(String tagname)
	{
	int index = tagname.indexOf(':');
	if (index<0) return tagname;
	String localname = tagname.substring(index+1);
	return localname;
	}
	*/

	/**
	 * Get the version of the MLFC. This version number is updated 
	 * with the browser version number at compilation time. This version number
	 * indicates the browser version this MLFC was compiled with and should be run with.
	 * @return 	MLFC version number.
	 */
	public final String getVersion() {
		return Browser.version;
	}

	/**
	* Create a DOM element.
	*/
	public Element createElementNS(DocumentImpl doc, String ns, String tag)
	{
		Element element = null;
		docImpl = doc;
		Log.debug("AAML : "+ns+":"+tag);

		if (tag == null)
		{
			Log.error("Invalid null tag name!");
			return null;
		}
		String localname = getLocalname(tag);

		if (localname.equals("audio3d"))
		{
			Log.debug("**** Creating Audio3D element");
			element = new Audio3DElementImpl(doc, this, ns, tag);
			aamlElements.addElement(element);
		} else if (localname.equals("listener"))
		{
			Log.debug("**** Creating listener element");
			element = new ListenerElementImpl(doc, this, ns, tag);
			aamlElements.addElement(element);
			listenerElement = (ListenerElementImpl)element;
		} else if (localname.equals("environment"))
		{
			Log.debug("**** Creating environment element");
			element = new EnvironmentElementImpl(doc, this, ns, tag);
			aamlElements.addElement(element);
		} else if (localname.equals("scale") || localname.equals("rotate") || localname.equals("translate"))
		{
			return null;
		}

		// Other tags go back to the XSmilesDocument as null...
		if (element == null)
			Log.error("Advanced Audio Language doesn't include element "+tag);

		return element;
	} 

	public ListenerElementImpl getListener()  {
		return listenerElement;
	}

	public void start()
	{ 
		XMLDocument doc = this.getXMLDocument(); 

		// Set default listener, if not specified in the document
		if (listenerElement == null)  {
//			ListenerElementImpl.setDefaultListenerPosition(this);
			ListenerElementImpl defaultListener = new ListenerElementImpl(docImpl, this, "ns", "listener");	
			aamlElements.addElement(defaultListener);
			listenerElement = defaultListener;
		}

		scheduler = new AAScheduler();
		scheduler.start();

//		if (listenerElement.getAttribute("start").equals("immediate") == true)  {
			Enumeration e = aamlElements.elements();
			Element elem;
			while (e.hasMoreElements())   {
				elem = (Element)e.nextElement();
				if (elem instanceof Audio3DElementImpl && !(elem instanceof ListenerElementImpl))
					((Audio3DElementImpl)elem).setVisible(true);
			}
//		}
		
		Log.debug("AAMLFC started.");
	}

	/**
	* Append the given URL to be a full URL.
	* @param partURL		Partial URL, e.g. fanfaari.wav
	* @return  Full URL, e.g. http://www.x-smiles.org/demo/fanfaari.wav
	*/
	public URL createURL(String partURL)
	{
		try
		{
			return new URL(this.getXMLDocument().getXMLURL(),partURL);
		} catch (java.net.MalformedURLException e)
		{
			Log.error(e);
			return null;
		}
	}

	/**
	* Not implemented method.
	*/
	public void setSize(Dimension d)
	{
	}

	/**
	* Called from the LinkHandler - this method asks the browser to go to this external link.
	* @param url		URL to jump to.
	*/
	public void gotoExternalLink(String url)
	{
		Log.debug("...going to "+url);
		URL u = createURL(url);
		getMLFCListener().openLocation(u);
	} 

	/**
	* Display status text in the broswer. Usually shows the link destination.
	*/
	public void displayStatusText(String url)
	{
		getMLFCListener().setStatusText(url);
	}

	/**
	* The opposite of init()
	* deactivate is only called for displayable MLFCs
	*/
	public void stop()
	{
		if (scheduler != null)
			scheduler.stop();
		
		// Terminate Native Device
		if (nativePlayer != null && dso != 0)
			nativePlayer.terminate(dso);
	
		//super.destroy();
		Log.debug("Destroying objects related with AAMLFC");
		Enumeration e = aamlElements.elements();
		while(e.hasMoreElements()) 
			((XSmilesElementImpl)e.nextElement()).destroy();
		aamlElements=null;
	}

	public Dimension getWindowSize()  {
		Component parent = this.getContainer();
		windowSize = parent.getSize();
		return windowSize;
//		int x = component.getLocation().x;
//		int y = component.getLocation().y;
//		while (parent != null)  {
//			x += parent.getLocation().x;
//			y += parent.getLocation().y;
//			parent = parent.getParent();
//		}
//		return new Dimension(x,y);
	}

	/**
	 * Scheduler to sample audio / listener locations every 50 ms.
	 */
	public class AAScheduler implements Runnable
	{

		private boolean running = false;

		public AAScheduler()
		{
		}

		public void start()
		{
			// Has laready started?
			if (running == true)
				return;

				running = true;
			new Thread(this).start();
		}

		public void stop()
		{
			// Stop it
			running = false;
		} 

		public void run()
		{
			while (running == true)
			{
				Enumeration e = aamlElements.elements();
//				Log.debug("AA sampling Audio3D elements");
				while(e.hasMoreElements()) 
					((AudioUpdate)e.nextElement()).update();

				try
				{
					Thread.sleep(SampleInterval);
				} catch (InterruptedException ex) {
				}
			}
		}
	}

}

 


