/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.aaml.nai;

import java.io.IOException;

/**
 * OS independent interface to implement native 3d-audio.
 */
public interface NativeAudio {

	/**
	 * Initialize audio lib.
	 */
	public boolean init();

	
	/**
	 * Prefetch audio, create buffers etc.
	 * @return 		Sound id - this id is used to play the sound.
	 */
	public int prefetch(String URL);

	/**
	 * Play audio.
	 * @param id	audio id, returned by prefetch().
	 */
	public boolean play(int id);

	/**
	 * Stop audio.
	 * @param id	audio id, returned by prefetch().
	 */
	public boolean stop(int id);
	
	/**
	 * Terminate audio lib.
	 */
	public void terminate();
}

