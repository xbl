/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.aaml;

public class Point3D  {
	float x;
	float y;
	float z;
	
	public Point3D(float x, float y, float z)  {
		this.x = x;
		this.y = y;
		this.z = z;
	}
}