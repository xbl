/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.aaml;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.dom.AnimationService;

import fi.hut.tml.xsmiles.Log;

import java.awt.*;

/**
 * Listener in a 3d space.
 *
 * @author Kari
 */
public class ListenerElementImpl extends Audio3DElementImpl implements AudioUpdate, AnimationService {

	private float left = 0, top = 0;
	private String aleft = null, atop = null, afront = null;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public ListenerElementImpl(DocumentImpl owner, AAMLFC aamlfc, String namespace, String tag)
	{
		super(owner, aamlfc, namespace, tag);
		Log.debug("Listener element created!");
	}

	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		Log.debug("Listener.init");

//		try  {
//			joy = DirectDevice.createInstance();
//		} catch (java.lang.UnsatisfiedLinkError e)  {
//			Log.error("Native library for AAMLFC not installed!");
//			throw e;
//		}

		String rate=getAttribute("sampleRate");
		if(rate != null && rate.equals("")==false) {
//			int valrate=Integer.parseInt(rate);
//			joy.setSamplingRate(valrate);
		}

		super.init();
	}

	/** Return left coord
	 */
	public float getLeft()  {
		return left;
	}
	/** Return top coord
	 */
	public float getTop()  {
		return top;
	}
	/** Return front coord
	 */
	public float getFront()  {
		return front;
	}

	/**
	 * Update the audio data to the native side.
	 */
	public void update()  {
		// Get coordinates
		left = convertToFloat(getNSAttr(this, "left"));
		top = convertToFloat(getNSAttr(this, "top"));
		front = convertToFloat(getNSAttr(this, "front"));

		// Use default values if attribute was missing		
		Dimension size = aaMLFC.getWindowSize();
		if (getAttribute("left").length() == 0)
			left = size.width/2;
		if (getAttribute("top").length() == 0)
			top = size.height/2;
		if (getAttribute("front").length() == 0)
			front = 30;
		
		// Coords of the window - listener is relative to window if not "global"
		Dimension w = getCoords(aaMLFC.getContainer());
		if (!getAttribute("coord").equals("global"))  {
			left += w.width;
			top += w.height;
		}
		
		// Apply percentages - not implemented
//		left = windowWidthPercentage(left);
//		top = windowHeightPercentage(top);

		// Apply translation, scaling and rotation
		Point3D p = translate(left, top, front);
		left = p.x;
		top = p.y;
		front = p.z;

		// Calculate doppler, average of 3 past values
		float dx = ((last2left-last3left) + (lastleft-last2left) + (left-lastleft)) / 3;
		float dy = ((last2top-last3top) + (lasttop-last2top) + (top-lasttop)) / 3;
		float dz = ((last2front-last3front) + (lastfront-last2front) + (front-lastfront)) / 3;

		// Disable doppler effect, if not "true"	
		if (getAttribute("doppler").equals("true") == false)  {
			dx = 0;
			dy = 0;
			dz = 0;
		}
//		Log.debug("Listener "+getId()+" updating data."+left+" "+top+" "+front+" d"+(dx)+" "+dy+" "+(dz));

		// Get min/max distance
		String val = getAttribute("distanceFactor");
		if (val == null || val.length() == 0)  // default value
			val = "1";
		float distance = convertToFloat(val);		
		val = getAttribute("dopplerFactor");
		if (val == null || val.length() == 0)  // default value
			val = "1";
		float doppler = convertToFloat(val);		
		val = getAttribute("rolloffFactor");
		if (val == null || val.length() == 0)  // default value
			val = "1";
		float rolloff = convertToFloat(val);		
		
		// Set listener parameters
		if (aaMLFC.nativePlayer != null)
			aaMLFC.nativePlayer.setListenerParams(aaMLFC.dso, left, top, front, 
						dx, dy, dz,
						0, 0, 1, 0, 1, 0,
						distance, doppler, rolloff);
//		win.setListenerParams(dso, 0, 20, 0, 0, 4100, 0,
//						0, 1, 0, 1, 0, 0,
//						1, 1, 1);

//    public native boolean setListenerParams(int dso, float x, float y, float z,
//									float vx, float vy, float vz,
//									float ofx, float ofy, float ofz, float otx, float oty, float otz,
//									float distancefactor, float dopplerfactor, float rollofffactor);
		
		// Save values for Doppler effect (=lastleft-left)
		last3left = last2left;
		last3top = last2top;
		last3front = last2front;
		last2left = lastleft;
		last2top = lasttop;
		last2front = lastfront;
		lastleft = left;
		lasttop = top;
		lastfront = front;
	}

	/**
	 * Destroy this element.
	 */
	public void destroy()
	{
	}
	
	// AnimationService implementation
	
	/**
	 * Convert String attribute to an float value
	 */
	public float convertStringToUnitless(String attr, String value)  {
		return 0;
	}
	public String convertUnitlessToString(String attr, float value)  {
		return null;
	}

	/**
	 * The attribute value got with this method takes precedence over
	 * the DOM attribute value.
	 * @param attr	Animated attribute
	 */
	public String getAnimAttribute(String attr)  {
		if (attr.equals("left"))  {
			if (aleft != null)
				return aleft;
			return getAttribute("left");	
		}
		if (attr.equals("top"))  {
			if (atop != null)
				return atop;
			return getAttribute("top");	
		}
		if (attr.equals("front"))  {
			if (afront != null)
				return afront;
			return getAttribute("front");	
		}
		return "";	
	}

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 * @param attr	Attribute to be animated
	 * @param value	Animation value to be set
	 */
	public void setAnimAttribute(String attr, String value)  {
		if (attr.equals("left"))
			aleft = value;
		if (attr.equals("top"))
			atop = value;
		if (attr.equals("front"))
			afront = value;
	}

	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 * @param attr	Attribute to be animated (animation removed)
	 */
	public void removeAnimAttribute(String attr)  {
		if (attr.equals("left"))
			aleft = null;
		if (attr.equals("top"))
			atop = null;
		if (attr.equals("front"))
			afront = null;
	}

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 */
	public void refreshAnimation()  {
	}
	
//	static public void setDefaultListenerPosition(AAMLFC mlfc)  {
//		// Get size of the window
//		Dimension w = mlfc.getWindowSize();
//		Log.debug("Default Listener at "+(w.width/2)+" "+(w.height/2));
//		// Set listener to coordinates (50%, 50%)
//		if (mlfc.nativePlayer != null)
//			mlfc.nativePlayer.setListenerParams(mlfc.dso, 0, w.height/2, w.width/2,
//						0, 0, 0,
//						0, 1, 0, 1, 0, 0,
//						1, 1, 1);	
//	}
	
}


