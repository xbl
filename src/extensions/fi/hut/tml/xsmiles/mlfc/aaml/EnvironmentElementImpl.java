/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.aaml;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import java.util.Hashtable;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;
import fi.hut.tml.xsmiles.dom.AnimationService;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.Log;

/**
 * Listener in a 3d space.
 *
 * @author Kari
 */
public class EnvironmentElementImpl extends XSmilesElementImpl 
							implements AudioUpdate, AnimationService {

	// Animation attributes, value="***REMOVED***" means attr has been removed.
	private Hashtable animAttrs = null;
	
	// Hash of all preset environments
	private static Hashtable envs = null;
    static {
		envs = new Hashtable();
        envs.put("generic", new Integer(1));
	    envs.put("room", new Integer(2));
	    envs.put("bathroom", new Integer(3));
	    envs.put("livingroom", new Integer(4));
	    envs.put("stoneroom", new Integer(5));
	    envs.put("auditorium", new Integer(6));
	    envs.put("concerthall", new Integer(7));
	    envs.put("cave", new Integer(8));
	    envs.put("arena", new Integer(9));
	    envs.put("hangar", new Integer(10));
	    envs.put("carpetedhallway", new Integer(11));
	    envs.put("hallway", new Integer(12));
	    envs.put("stonecorridor", new Integer(13));
	    envs.put("alley", new Integer(14));
	    envs.put("forest", new Integer(15));
	    envs.put("city", new Integer(16));
	    envs.put("mountains", new Integer(17));
	    envs.put("quarry", new Integer(18));
	    envs.put("plain", new Integer(19));
	    envs.put("parkinglot", new Integer(20));
	    envs.put("sewerpipe", new Integer(21));
	    envs.put("underwater", new Integer(22));
	    envs.put("drugged", new Integer(23));
	    envs.put("dizzy", new Integer(24));
	    envs.put("psychotic", new Integer(25));
	    envs.put("", new Integer(1));
    }

	// AAMLFC
	private AAMLFC aaMLFC = null;
	
	// XSmilesDocumentImpl - to create new elements
	private DocumentImpl ownerDoc = null;

	// true if eax supported
	boolean eaxSupported = false;

	String room = "";
	String roomHF = "";
	String roomRolloff = "";
	String decayTime = "";
	String decayHFRatio = "";
	String reflections = "";
	String reflectionsDelay = "";
	String reverb = "";
	String reverbDelay = "";
	String environment = "generic";
	String environmentSize = "";
	String environmentDiffusion = "";
	String airAbsorption = "";
	String scaleDecayTime = "";
	String scaleReflections = "";
	String scaleReflectionsDelay = "";
	String scaleReflectionsDiffusion = "";
	String scaleReverb = "";
	String scaleReverbDelay = "";
	String clipDecayHF = "";
	
	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public EnvironmentElementImpl(DocumentImpl owner, AAMLFC aamlfc, String namespace, String tag)
	{
		super(owner, namespace, tag);
		aaMLFC = aamlfc;
		ownerDoc = owner;
		animAttrs = new Hashtable();
		Log.debug("Environment element created!");
	}

	private void dispatch(String type)
	{	 
		// Dispatch the event
		Log.debug("Dispatching Audio3D Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		Log.debug("Environment.init");

		eaxSupported = aaMLFC.nativePlayer.isEAXAvailable(aaMLFC.dso);
		if (eaxSupported == false)  {
			Log.debug("EAX 2.0 is not supported by your system.");
			Log.debug("Environmental effects disabled.");
			return;
		} else {
			Log.debug("EAX 2.0 happily supported!");
		}

//		boolean f = aaMLFC.nativePlayer.setEnvironmentParams(aaMLFC.dso, -789,-476,0.0f,4.32f,0.59f,0,0.020f,8,0.030f,
//										6,21.6f,1.00f,-5.00f,0x0000002F);
//		System.out.println("ENVIRONEMNT SET:"+f+" dso"+aaMLFC.dso);
//		System.out.println("PARAM SET:"+aaMLFC.nativePlayer.getRoom(aaMLFC.dso));
//		// If environment changed -> Set environment, set all non-null singles
//		// else if single changed -> Set single
//		f = aaMLFC.nativePlayer.setRoom(aaMLFC.dso, -100);
//		System.out.println("PARAM SET 2:"+aaMLFC.nativePlayer.getRoom(aaMLFC.dso));


		//boolean flag = aaMLFC.nativePlayer.setEnvironmentParams(aaMLFC.dso, -789,-476,0.0f,4.32f,0.59f,0,0.020f,8,0.030f,
		//6,21.6f,1.00f,-5.00f,0x0000002F);

		// If environment changed -> Set environment, set all non-null singles
		// else if single changed -> Set single
//		boolean flag = aaMLFC.nativePlayer.setRoom(aaMLFC.dso, -100);
//		System.out.println("PARAM SET 2:"+aaMLFC.nativePlayer.getRoom(aaMLFC.dso));

//		try  {
//			joy = DirectDevice.createInstance();
//		} catch (java.lang.UnsatisfiedLinkError e)  {
//			Log.error("Native library for AAMLFC not installed!");
//			throw e;
//		}

		super.init();
	}

	/**
	 * Update the audio data to the native side.
	 */
	public void update()  {
		if (eaxSupported == false)
			return;
		
		// Has environment changed?
		String attr = getAAttribute("preset");
		if (attr != null && attr.equals(environment) == false)  {
			environment = attr;
			Integer i = (Integer)envs.get(attr);
			aaMLFC.nativePlayer.setEnvironment(aaMLFC.dso, i.intValue());
//			boolean f = aaMLFC.nativePlayer.setEnvironmentParams(aaMLFC.dso, -789,-476,0.0f,4.32f,0.59f,0,0.020f,8,0.030f,
//											6,21.6f,1.00f,-5.00f,0x0000002F);
//			System.out.println("ENVIRONEMNT SET:"+f+" dso"+aaMLFC.dso);
//			System.out.println("PARAM SET:"+aaMLFC.nativePlayer.getRoom(aaMLFC.dso));
			// If environment changed -> Set environment, set all non-null singles
			// else if single changed -> Set single
//			f = aaMLFC.nativePlayer.setRoom(aaMLFC.dso, -100);
//			System.out.println("PARAM SET 2:"+aaMLFC.nativePlayer.getRoom(aaMLFC.dso));
//			System.out.println("ENVI:"+aaMLFC.nativePlayer.getEnvironment(aaMLFC.dso));
			room = "";
			roomHF = "";
			roomRolloff = "";
			decayTime = "";
			decayHFRatio = "";
			reflections = "";
			reflectionsDelay = "";
			reverb = "";
			reverbDelay = "";
			environmentSize = "";
			environmentDiffusion = "";
			airAbsorption = "";
			scaleDecayTime = "";
			scaleReflections = "";
			scaleReflectionsDiffusion = "";
			scaleReverb = "";
			scaleReverbDelay = "";
			clipDecayHF = "";
			Log.debug("Environment environment changed to "+environment+" "+aaMLFC.nativePlayer.getEnvironment(aaMLFC.dso));
		}
		// This cannot handle situations when an attribute is removed
		attr = getAAttribute("room");
		if (attr != null && attr.equals(room) == false)  {
			room = attr;
			float l = convertToFloat(attr)*100;
			aaMLFC.nativePlayer.setRoom(aaMLFC.dso, (long)l);
			Log.debug("ENV ROOM CHECK:"+aaMLFC.nativePlayer.getRoom(aaMLFC.dso));
		}
		attr = getAAttribute("roomHF");
		if (attr != null && attr.equals(roomHF) == false)  {
			roomHF = attr;
			float l = convertToFloat(attr)*100;
			aaMLFC.nativePlayer.setRoomHF(aaMLFC.dso, (long)l);
			Log.debug("ENV ROOMHF CHECK:"+aaMLFC.nativePlayer.getRoomHF(aaMLFC.dso));
		}
		attr = getAAttribute("roomRolloff");
		if (attr != null && attr.equals(roomRolloff) == false)  {
			roomRolloff = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setRoomRolloff(aaMLFC.dso, l);
			Log.debug("ENV ROOMROLLOFF CHECK:"+aaMLFC.nativePlayer.getRoomRolloff(aaMLFC.dso));
		}
		attr = getAAttribute("decayTime");
//		Log.debug("Environment "+getId()+" updating data:"+attr);
		if (attr != null && attr.equals(decayTime) == false)  {
			decayTime = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setDecayTime(aaMLFC.dso, l);
			Log.debug("ENV DECAYTIME CHECK:"+aaMLFC.nativePlayer.getDecayTime(aaMLFC.dso));
		}
		attr = getAAttribute("decayHFRatio");
		if (attr != null && attr.equals(decayHFRatio) == false)  {
			decayHFRatio = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setDecayHFRatio(aaMLFC.dso, l);
			Log.debug("ENV DECAYHFRATIO CHECK:"+aaMLFC.nativePlayer.getDecayHFRatio(aaMLFC.dso));
		}
		attr = getAAttribute("reflections");
		if (attr != null && attr.equals(reflections) == false)  {
			reflections = attr;
			float l = convertToFloat(attr)*100;
			aaMLFC.nativePlayer.setReflections(aaMLFC.dso, (long)l);
			Log.debug("ENV REFLECTIONS CHECK:"+aaMLFC.nativePlayer.getReflections(aaMLFC.dso));
		}
		attr = getAAttribute("reflectionsDelay");
		if (attr != null && attr.equals(reflectionsDelay) == false)  {
			reflectionsDelay = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setReflectionsDelay(aaMLFC.dso, l);
			Log.debug("ENV REFLECTIONSDELAY CHECK:"+aaMLFC.nativePlayer.getReflectionsDelay(aaMLFC.dso));
		}
		attr = getAAttribute("reverb");
		if (attr != null && attr.equals(reverb) == false)  {
			reverb = attr;
			float l = convertToFloat(attr)*100;
			aaMLFC.nativePlayer.setReverb(aaMLFC.dso, (long)l);
			Log.debug("ENV REVERB CHECK:"+aaMLFC.nativePlayer.getReverb(aaMLFC.dso));
		}
		attr = getAAttribute("reverbDelay");
		if (attr != null && attr.equals(reverbDelay) == false)  {
			reverbDelay = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setReverbDelay(aaMLFC.dso, l);
			Log.debug("ENV REVERBDELAY CHECK:"+aaMLFC.nativePlayer.getReverbDelay(aaMLFC.dso));
		}
		attr = getAAttribute("environmentSize");
		if (attr != null && attr.equals(environmentSize) == false)  {
			environmentSize = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setEnvironmentSize(aaMLFC.dso, l);
			Log.debug("ENV ENVSIZE CHECK:"+aaMLFC.nativePlayer.getEnvironmentSize(aaMLFC.dso));
		}
		attr = getAAttribute("environmentDiffusion");
		if (attr != null && attr.equals(environmentDiffusion) == false)  {
			environmentDiffusion = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setEnvironmentDiffusion(aaMLFC.dso, l);
			Log.debug("ENV ENVDIFF CHECK:"+aaMLFC.nativePlayer.getEnvironmentDiffusion(aaMLFC.dso));
		}
		attr = getAAttribute("airAbsorption");
		if (attr != null && attr.equals(airAbsorption) == false)  {
			airAbsorption = attr;
			float l = convertToFloat(attr);
			aaMLFC.nativePlayer.setAirAbsorption(aaMLFC.dso, l);
			Log.debug("ENV AIRABS CHECK:"+aaMLFC.nativePlayer.getAirAbsorption(aaMLFC.dso));
		}

		attr = getAAttribute("scaleDecayTime");
		if (attr != null && attr.equals(scaleDecayTime) == false)  {
			scaleDecayTime = attr;
			boolean l = attr.equals("true");
			aaMLFC.nativePlayer.setScaleDecayTime(aaMLFC.dso, l);
		}
		attr = getAAttribute("scaleReflections");
		if (attr != null && attr.equals(scaleReflections) == false)  {
			scaleReflections = attr;
			boolean l = attr.equals("true");
			aaMLFC.nativePlayer.setScaleReflections(aaMLFC.dso, l);
		}
		attr = getAAttribute("scaleReflectionsDelay");
		if (attr != null && attr.equals(scaleReflectionsDelay) == false)  {
			scaleReflectionsDelay = attr;
			boolean l = attr.equals("true");
			aaMLFC.nativePlayer.setScaleReflectionsDelay(aaMLFC.dso, l);
		}
		attr = getAAttribute("scaleReverb");
		if (attr != null && attr.equals(scaleReverb) == false)  {
			scaleReverb = attr;
			boolean l = attr.equals("true");
			aaMLFC.nativePlayer.setScaleReverb(aaMLFC.dso, l);
		}
		attr = getAAttribute("scaleReverbDelay");
		if (attr != null && attr.equals(scaleReverbDelay) == false)  {
			scaleReverbDelay = attr;
			boolean l = attr.equals("true");
			aaMLFC.nativePlayer.setScaleReverbDelay(aaMLFC.dso, l);
		}
		attr = getAAttribute("clipDecayHF");
		if (attr != null && attr.equals(clipDecayHF) == false)  {
			clipDecayHF = attr;
			boolean l = attr.equals("true");
			aaMLFC.nativePlayer.setClipDecayHF(aaMLFC.dso, l);
		}
///		Log.debug("Environment updated!");
	}

	/**
	 * Convert string val to float.
	 * returns 0 in errors.
	 */
	protected float convertToFloat(String val)  {
		try  {
			return Float.parseFloat(val);
		} catch (NumberFormatException e)  {
			return 0.0f;
		}
	}

	/**
	 * Convert string val to long.
	 * returns 0 in errors.
	 */
	protected long convertToLong(String val)  {
		try  {
			return Long.parseLong(val);
		} catch (NumberFormatException e)  {
			return 0;
		}
	}

	/**
	 * Destroy this element.
	 */
	public void destroy()
	{
	}
	
	
	//// AnimationService implementation ////
	
	/**
	 * Convert String attribute to an float value
	 */
	public float convertStringToUnitless(String attr, String value)  {
		return 0;
	}
	public String convertUnitlessToString(String attr, float value)  {
		return null;
	}

	/**
	 * Get animation value of an attribute.
	 * @param attr	Animated attribute
	 */
	public String getAnimAttribute(String attr)  {
		return (String)animAttrs.get(attr);
	}

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 * @param attr	Attribute to be animated
	 * @param value	Animation value to be set
	 */
	public void setAnimAttribute(String attr, String value)  {
		animAttrs.put(attr, value);
	}

	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 * @param attr	Attribute to be animated (animation removed)
	 */
	public void removeAnimAttribute(String attr)  {
		animAttrs.remove(attr);
	}

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 * Not required.
	 */
	public void refreshAnimation()  {
	}

	/**
	 * Get either animated value of an attribute or the real dom attribute value.
	 */
	private String getAAttribute(String attr) {
		String val = getAnimAttribute(attr);
		if (val == null || val.length() == 0)
			return getAttribute(attr);
		
		return val;	
	}
	
}


