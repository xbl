/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.aaml.nai;

import java.io.IOException;
import fi.hut.tml.xsmiles.Log;

/**
 */
public class NativeAudioDirectX extends java.awt.Canvas { //implements NativeAudio {

	static boolean ok = false;

    public NativeAudioDirectX()  {
        try {
            System.loadLibrary("AdvancedAudioML");
        } catch (UnsatisfiedLinkError e) {
			Log.debug("AdvancedAudioML.dll not found!!");
			return;
        }
		Log.debug("NativeAudioDirectX class instantiated!");
		ok = true;
    }

	public boolean isLibraryValid()  {
		return ok;
	}

    /**
     * Initialize audio lib.
	 * @returns dso number
     */
    public native int init();
    
    /**
     * Prefetch audio, create buffers etc.
	 * @param dso	DirectSoundObject id, returned by init()
	 * @param url	URL to be prefetched (currently, filename in local dir)
     * @return 		Sound id - this id is used to play the sound.
     */
    public native int prefetch(int dso, String url);

    /**
     * Play audio.
	 * @param dso	DirectSoundObject id, returned by init()
     * @param id	audio id, returned by prefetch().
     */
    public native boolean play(int dso, int id);

    /**
     * Stop audio.
	 * @param dso	DirectSoundObject id, returned by init()
     * @param id	audio id, returned by prefetch().
     */
    public native boolean stop(int dso, int id);

    /**
     * Set sound position.
	 * @param dso	DirectSoundObject id, returned by init()
     * @param id	audio id, returned by prefetch().
     */
    public native boolean setPosition(int dso, int id, float x, float y, float z);


    /**
     * Set sound parameters.
	 * @param dso	DirectSoundObject id, returned by init()
     * @param id	audio id, returned by prefetch().
     */
    public native boolean setParams(int dso, int id, float x, float y, float z,
									float vx, float vy, float vz,
									float coneox, float coneoy, float coneoz, 
									int coneOutVol, int insideConeAngle, float maxDist, float minDist,
									int mode, int outsideConeAngle, float freq, float vol, float pan,
									boolean loop);

    /**
     * Set listener parameters.
	 * @param dso	DirectSoundObject id, returned by init()
     */
    public native boolean setListenerParams(int dso, float x, float y, float z,
									float vx, float vy, float vz,
									float ofx, float ofy, float ofz, float otx, float oty, float otz,
									float distancefactor, float dopplerfactor, float rollofffactor);

								
    /**
     * Get Last Error Message.
     */
    public native String getErrorMsg(int dso);
    
    /**
     * Terminate audio lib.
     */
    public native void terminate(int dso);

	////////// EAX 2.0 /////////////

	/**
	 * Check if EAX 2.0 is available. Returns valid values after init().
	 */
	 public native boolean isEAXAvailable(int dso);

	 /**
	  * Set environment parameters.
	  * @param dso	DirectSoundObject id, returned by init()
	  */
	public native boolean setEnvironmentParams(int dso, long lRoom, long lRoomHF, float flRoomRolloffFactor,
										float flDecayTime, float flDecayHFRatio,
										long lReflections, float flReflectionsDelay,
										long lReverb, float flReverbDelay, long dwEnvironment,
										float flEnvironmentSize, float flEnvironmentDiffusion,
										float flAirAbsorptionHF, long dwFlags);

	/**
	 * Get EAX values
	 */
	public native long getRoom(int dso);
	public native long getRoomHF(int dso);
	public native float getRoomRolloff(int dso);
	public native float getDecayTime(int dso);
	public native float getDecayHFRatio(int dso);
	public native long getReflections(int dso);
	public native float getReflectionsDelay(int dso);
	public native long getReverb(int dso);
	public native float getReverbDelay(int dso);
	public native int getEnvironment(int dso);
	public native float getEnvironmentSize(int dso);
	public native float getEnvironmentDiffusion(int dso);
	public native float getAirAbsorption(int dso);
	public native int getFlags(int dso);

	public native boolean setRoom(int dso, long lValue);
	public native boolean setRoomHF(int dso, long lValue);
	public native boolean setRoomRolloff(int dso, float fValue);
	public native boolean setDecayTime(int dso, float fValue);
	public native boolean setDecayHFRatio(int dso, float fValue);
	public native boolean setReflections(int dso, long lValue);
	public native boolean setReflectionsDelay(int dso, float fValue);
	public native boolean setReverb(int dso, long lValue);
	public native boolean setReverbDelay(int dso, float fValue);
	public native boolean setEnvironment(int dso, int dwValue);
	public native boolean setEnvironmentSize(int dso, float fValue);
	public native boolean setEnvironmentDiffusion(int dso, float fValue);
	public native boolean setAirAbsorption(int dso, float fValue);
	public native boolean setFlags(int dso, int dwValue);
	public native boolean setScaleDecayTime(int dso, boolean bValue);
	public native boolean setScaleReflections(int dso, boolean bValue);
	public native boolean setScaleReflectionsDelay(int dso, boolean bValue);
	public native boolean setScaleReverb(int dso, boolean bValue);
	public native boolean setScaleReverbDelay(int dso, boolean bValue);
	public native boolean setClipDecayHF(int dso, boolean bValue);

}
