/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.media;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.Container;
import java.awt.Component;
import java.awt.Dimension;

import java.net.URL;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

// These will import JSR-135
import javax.microedition.media.*;
import javax.microedition.media.control.*;

import fi.hut.tml.xsmiles.JSRMedia;

import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import fi.hut.tml.xsmiles.Log;

/**
 * This is the implementation of JMF media (audio/video...).
 */
public class JSRMedia implements Media, MouseListener, Runnable {

  	// Hashtable holding loaded images - this is a proxy
	private Hashtable loadedImages;
  
	// Label and ImageIcon for current image
	private Component comp = null;

	// Container for media (DrawingArea)
	private Container container = null;

	private Player player = null;

	// MediaListener - called after prefetch and endOfMedia.
	MediaListener mediaListener = null;

	// Coords for the media
	private int x=0, y=0, width=0, height=0;

	// Media URL
	private URL url = null;

	// ControllerListener to listen for JSR-135
	PlayerListenerImpl playerListener = null;

	// Player state set by this
	boolean playing = false;

	public CanaryJMFMedia() {
		// Create the proxy hashtable
		loadedImages = new Hashtable();			
	}

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static.
	 */
	public boolean isStatic() {
		return false;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public synchronized void prefetch() {
		Log.debug("swing.media.JMFMedia prefetch()");				
		Player newPlayer = null;

		try {
		    try  {
		        newPlayer = Manager.createPlayer(url.toString());
		    } catch (MediaException e) {
		        Log.error("ElementPlayer:" + e);
		        throw e;
		    }		    
			if (newPlayer != null) {
				player = newPlayer;
				playerListener = new PlayerListenerImpl();
				player.addPlayerListener(playerListener);
				try {
					player.realize();
				}
				catch (MediaException ex) {
					ex.printStackTrace();
					Log.error("player.realize() -- " + ex.getMessage());
				}

				GUIControl gc = (GUIControl)newPlayer.getControl("GUIControl");
			    if (gc != null) {
					// Save the visual component so that it can be shown
					// Audio will return value null
			    	comp =(Component)gc.initDisplayMode(GUIControl.USE_GUI_PRIMITIVE,
			                                   						"java.awt.Component");
					Log.debug("Realized + prefetched "+url.toString()+" comp: "+comp);

					// This will get all mouse events, and pass them to the MediaListener
					if (comp != null) {
						comp.addMouseListener(this);
						comp.setVisible(false);
					}								
			    }		    
			}
		} catch (Exception e) {
			// Loading of media failed -> display broken image.
			Log.debug("File not found, fetching broken image.");
		}
	}
	
	public void setContainer(Container container) {
		if (this.container != container) {

			// Change container
			if (comp != null && comp.isVisible() == true) {
				comp.setVisible(false);
				this.container.remove(comp);
				container.add(comp, 0);
				comp.setVisible(true);
			}

			this.container = container;
		}
	}

	public void play() {
		Log.debug("swing.media.JMFMedia play()");
		// Set to true, mediaEnded() will be called
		playing = true;

		if (container != null && comp != null) {
			container.add(comp);
			comp.setLocation(x, y);
			comp.setSize(width, height);
			comp.setVisible(true);
		} else if (container == null) 
			Log.error("Region container not set for media "+url.toString());
		else	
			Log.debug("Component missing for media "+url.toString());

		// Play it!
		if (player != null)
			try {
				player.start();
			}
			catch (MediaException ex) {
				ex.printStackTrace();
				Log.error("player.start() -- " + ex.getMessage());
			}
		else {
			Log.error("Player not initialized for "+url.toString()+"!");
			// End this media immediately
			Thread t = new Thread(this);
			t.start();
		}
	}

	/**
	 * This media ends immediately, if the player was not found. Actually,
	 * this should be replaced by an alt-text component!!!! (ending immediately)
	 */
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		// Media ended immediately (almost...)
		if (mediaListener != null)
			mediaListener.mediaEnded();
	}

	public void pause() {
		// Pause it
		if (player != null)
			try {
				player.stop();
			}
			catch (MediaException ex) {
				ex.printStackTrace();
				Log.error("player.pause() -- " +ex.getMessage());
			}
	}

	public void stop() {
	
		// Set to false, mediaEnded() will not be called
		playing = false;

		if (comp != null)
			comp.setVisible(false);


		if (container != null && comp != null)
			container.remove(comp);
	
		// Stop it!
		if (player != null) {
			try {
				try {
					player.stop();
					player.setMediaTime((long)0/*new Time(0)*/);
				}
				catch (MediaException ex) {
					ex.printStackTrace();
					Log.error("player.stop() -- " +ex.getMessage());					
				}
			} catch (javax.media.NotRealizedError e) {
				// Catch if time is set for unrealized player
			}
		}

		// Clear media listeners
		// This is also needed to prevent JMF from sending MediaEndedEvent 
		// to activate the next element.
		if (mediaListener != null)
			mediaListener = null;	
	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		// If the media is shown on the screen, move it immediately
		if (comp != null && comp.isVisible() == true) {
			// Use the given coordinates
			comp.setLocation(x, y);
			comp.setSize(width, height);
		}			
	}

	/**
	 * Close this media and free all resources.
	 */
	public void close() {
		// close the player and set it to null to free memory
		if (player != null) {
			stop();
			player.removePlayerListener(playerListener);
			player.close();
		}
		player = null;

		// Free the listener		
		if (playerListener != null) {
			playerListener = null;
		}
		
		// Flush all images
		if (loadedImages != null) {
			Enumeration i = loadedImages.elements();
			while(i.hasMoreElements()) {
//				((Image)i.nextElement()).flush();
			}
		 
			loadedImages.clear();
		}
		loadedImages = null;
		
		if (container != null && comp != null)
			container.remove(comp);
		container = null;
		comp = null;
		mediaListener = null;
	}
	
	/**
	 * This moves the time position in media. JMF supports this for audio/video.
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs) {
	 	try {
	 		if (player != null)
	 			player.setMediaTime(millisecs*1000);
	 	}
	 	catch (MediaException ex) {
			ex.printStackTrace();
			Log.error("setMediaTime() -- " +ex.getMessage());	 		
	 	}
	 }

	 /** 
	  * Get the real width of the media. It should be known after prefetch.
	  */
	 public int getOriginalWidth() {
	 	if (comp != null) {
			Dimension d = comp.getPreferredSize();
	 		return d.width;
	 	} else
	 		return -1;	
	 }
	
	 /** 
	  * Get the real height of the media. It should be known after prefetch.
	  */
	 public int getOriginalHeight() {
 		if (comp != null) {
 			Dimension d = comp.getPreferredSize();
 			return d.height;
 		} else
	 		return -1;
	 }

	 /**
	  * Set the sound volume for media. Only applicable for sound media formats.
	  * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
	  * dB change in signal level = 20 log10(percentage / 100) )
	  */
	  public void setSoundVolume(int percentage) {
	  }

	/**
	 * Add a listener for this media.
	 */	
	public void addMediaListener(MediaListener listener) {
		mediaListener = listener;
	}
	
	/**
	 * A class to listen to JMF events and pass them to either JMFMedia or
	 * MediaListener (which should be XElementBasicTimeImpl).
	 */
	class PlayerListenerImpl implements PlayerListener {
		public PlayerListenerImpl() {
		}
		public void playerUpdate(Player player, String event, Object eventData) { 
			Log.debug("playerUpdate: " + event);
		    if (event.equals(PlayerListener.DEVICE_AVAILABLE)) {
		    	Log.debug("playerUpdate: DEVICE_AVAILABLE");
		    	// automatically prefetch after realized
		    	try {
		    		player.prefetch();
		    	}
		    	catch (MediaException ex) {
		    		ex.printStackTrace();
		    		Log.error("playerUpdate -- player.prefetch() " +ex.getMessage());	 		
		    	}
		    } else if (event.equals(PlayerListener.DEVICE_UNAVAILABLE)) { // instanceof ResourceUnavailableEvent) {
			    Log.error("Error prefetching "+url.toString()+": " + "resource unavaible");
				// Close the invalid player
				player.close();
				player = null;
		    } else if (event.equals(PlayerListener.END_OF_MEDIA)) {
				// Media ended - inform the SMIL player.
				// Calling stop() will not cause mediaEnded() to be called.
				if (mediaListener != null && playing == true)
					mediaListener.mediaEnded();
		    }
			return;
	    }
	}

	/**
	 * Mouse listener...
	 */
	public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}
	
	public void mouseEntered(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseEntered();
	}

	public void mouseExited(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseExited();
	}
	
	public void mousePressed(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mousePressed();
	}
	
	public void mouseReleased(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseReleased();
	}	
        
                	/**
	 * Requests the media player to display a control panel for media.
	 * For audio and video, these can be a volume/play/stop controls,
	 * for images, these can be zoom controls.
	 * The controls are GUI dependent, generated through ComponentFactory.
	 * @param visible	true=Display controls, false=don't display controls.
	 */
	public void showControls(boolean visible) {
            Log.error("Smil media showControls() not supported.");
	}
        
     /**
	  * Get the duration of media. Only applicable for continuous media (audio, video).
	  * @return The duration of media in millisecs.
	  */
	 public int getOriginalDuration() {
	 	return 0;
	 }
         
    /**
	 * All traffic to the browser, such as openLocation, etc goes through this listener.
	 * If no listener supplied media players should still function with some basic level. 
	 * 
	 * @param listener The MLFCListener supplied by the browser
	 * @see MLFCListener
	 */
	public void setMLFCListener(MLFCListener listener) {
            Log.error("Smil media setMLFCListener() not supported.");
	}
}

