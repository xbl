/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.transform.TransformerException;

import org.apache.xerces.dom.CoreDocumentImpl;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.dom.XSmilesPI;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.ecma.ECMAScripterFactory;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.general.MediaQuery;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.xslt.XSLTEngine;

/**
 * The XMLdocument is an object that can retrieve and parse a XML document when
 * given the URL of the document as an argument in the constructor. The
 * document can then be accessed through the DOM-interface that it provides.
 * The document also saves the original source of the document, so that the
 * source can be given to any component that is interested of it (for example
 * the SourceViewMLFC)
 * 
 * @author Kreivi
 * @author Mikko Honkala
 * @author Juha
 * @version $Revision: 1.55 $
 */
public class XSmilesXMLDocument implements XMLDocument, MediaQueryEvaluator {

    protected MLFC mlfc;
    protected XLink m_link;
    protected InputStream m_stream;
    protected XMLParser m_parser;
    protected XSLTEngine m_engine;
    protected BrowserWindow m_browser;
    protected URL XSLURL = null, sourceXMLURL;
    protected String currentStylesheetTitle = "";
    protected String preferredTitle = "";
    protected Document sourceXMLDoc, XSLDoc = null, destXMLDoc;
    protected String xslProcessor;
    protected String parserClass;
    protected Vector stylesheetTitles;
    protected boolean trueDocument; // false if only

    protected ECMAScripter currentScripter;

    /**
     * This constructor is used by the Browser when it requests a new
     * XMLdocument. <br>
     * The DOM-implementation is set here.
     * 
     * @param Browser
     *                   browser
     * @param XLink
     *                   xlink the source of the document.
     *  
     */
    public XSmilesXMLDocument(BrowserWindow browser, XLink link) {
        init(browser);
        m_link = link;
        sourceXMLURL = m_link.getURL();
        //URLFactory.getInstance().setContextURL(m_link.getURL());
    }

    /**
     * This constructor is used for the docs gotten via sockets etc.
     * (not http, i.e., no URI available). <br>
     * The DOM-implementation is set here.
     * 
     * @param Browser browser
     * @param InputStream document stream
     *  
     */
    public XSmilesXMLDocument(BrowserWindow browser, InputStream stream)
    {
        init(browser);
        m_link = null;
        sourceXMLURL = null;
        m_stream = stream;
        //URLFactory.getInstance().setContextURL(m_link.getURL());
    }

    /**
     * This constructor is used by the Browser when it requests a new
     * XMLdocument. <br>
     * The DOM-implementation is set here.
     * 
     * @param Browser
     *                   browser
     * @param XLink
     *                   xlink the source of the document.
     *  
     */
    public XSmilesXMLDocument(BrowserWindow browser, InputStream stream, XLink link) {
        init(browser);
        m_link = link;
        sourceXMLURL = m_link.getURL();
        m_stream = stream;
        //URLFactory.getInstance().setContextURL(m_link.getURL());
    }

    /**
     * An alternative constructor, where the source can be given as a string.
     * 
     * @param Browser
     *                   browser
     * @param String
     *                   s the source of the document, given as a string.
     */
    // NOTE: Not implemented.. try the XMLDocument(BrowserWindow, domDocument)
    public XSmilesXMLDocument(BrowserWindow browser, String s) {
        init(browser, s);

        m_link = new XLink(null, XLink.SIMPLE);
        sourceXMLURL = m_link.getURL();
        // in this case, the xmlURL is still null.,.
        init(browser);
    }

    /**
     * Another alternative constructor
     * 
     * @param browser
     *                   The BrowserWindow
     * @param doc
     *                   The Document
     */
    public XSmilesXMLDocument(BrowserWindow browser, Document doc) {
        sourceXMLDoc = doc;
        init(browser);
    }

    public void init(BrowserWindow browser) {
        if (browser != null)
            m_parser = browser.getXMLParser();
        m_browser = browser;
        //    this.getECMAScripter();
        stylesheetTitles = new Vector();
        if (browser!=null)this.preferredTitle = browser.getPreferredStylesheetTitle();

        trueDocument = true;
        //if(m_browser!=null)

    }

    public void init(BrowserWindow browser, String s) {
        init(browser);
        trueDocument = false;
    }

    /**
     * This is the method that performs all the actual work of the XMLDocument.
     * It fetches and parses the document (determined in the constructor) and
     * makes the document available for use by the other browser components.
     * The handle to the DOM-interface is kept in a variable called doc.
     * 
     * Run by a separate thread, so that fetching can be interrupted, for
     * example in error situations.
     */
    public void retrieveDocument() throws Exception {
        if (this.m_link != null)
        this.sourceXMLURL = this.m_link.getURL();

        if (m_stream == null)
            this.sourceXMLDoc = fetchXMLDOM(this.sourceXMLURL);
        else if (this.m_link != null)
            this.sourceXMLDoc = fetchXMLDOM(m_stream, this.sourceXMLURL);
        else
            this.sourceXMLDoc = fetchXMLDOM(m_stream);
        transformDocument();
    }

    /**
     * Just transform existing document again
     * 
     * @throws TransformerException,
     *                    if something went wrong.
     */
    public void transformDocument() throws Exception {
        try {
            /* This is fucked up... findXSL also looks for xml-edit PIs */
            XSmilesPI currentPI = findXSL(sourceXMLDoc);

            if (currentPI == null) {
                Log.debug("Couldn't find a editor document, using source document as renderable document");
                this.destXMLDoc = this.sourceXMLDoc;
            } else {
                currentStylesheetTitle = currentPI.getTitle();
                String target = currentPI.getPINode().getNodeName();

                // XSLT or inject instance into form
                if (target.equals("xml-edit")) {
                    // inject sourcedoc into instance
                    destXMLDoc = insertSourceIntoEditor(sourceXMLDoc, currentPI);
                    Log.debug("Destination document set to new editor document");
                } else if (target.equals("xml-stylesheet")) {
                    this.XSLURL = currentPI.getHREF(); //new
                                                                             // URL(getXMLURL(),
                                                                             // getAttribute(currentPI.getPINode().getData(),"href"));
                    // XSLT process
                    Log.info("A stylesheet reference was found: " + this.XSLURL);
                    this.destXMLDoc = processStyleSheet(this.sourceXMLDoc, this.XSLURL, this.sourceXMLURL);
                }
            }
        } catch (TransformerException e) {
            // FIND the innermost exception
            throw XSmilesXMLDocument.findInnermostException(e);
        }
    }

    /**
     * @param sourceXMLDoc
     *                   the source document instance
     * @param pi
     *                   The processing instruction, which has been chosen to be used.
     */
    public Document insertSourceIntoEditor(Document d, XSmilesPI pi) {
        Log.debug("Inserting source into editor: " + pi.getHREF() + " : " + pi.getTitle());
        // find editor doc
        String piData = pi.getPINode().getData();
        // example:
        // <?xml-edit type="application/xhtml+xml" title="Header editor"
        // href="./header_edit.xhtml" alternate="yes"?>

        String editHREF = getAttribute(piData, "href");
        String targetID = getAttribute(piData, "target_id");

        if (editHREF == null || editHREF.equals("")) {
            Log.debug("Inserting source into editor: Aborting href null");
            return d;
        }

        if (targetID == null || targetID.equals("")) {
            Log.debug("Inserting source into editor: Aborting target_id null");
            return d;
        }

        Document doc;
        // so that relative URIs also work
        try {
            URL editURL = new URL(getXMLURL(), editHREF);

            if (editURL == null) {
                Log.debug("Inserting source into editor: Aborting editURL null");
                return d;
            }

            Log.debug("Inserting source into editor: Retrieving editor document");
            doc = m_parser.openDocument(editURL);

        } catch (Exception e) {
            Log.error(e);
            Log.error("Couldn't find editor document");
            return d;
        }

        Log.debug("Inserting source into editor: Injecting instance into xforms editor");
        NodeList instances = doc.getElementsByTagNameNS("http://www.w3.org/2002/xforms", "instance");
        if (instances.getLength() == 0) {
            Log
                    .debug("Inserting source into editor: no instance nodes found. Most likely this is not a working document with xForms, reverting to original doc.");
            return (d);
        }
        boolean insertedInstance = false;
        for (int i = 0; i < instances.getLength(); i++) {
            if (((Element) instances.item(i)).getAttribute("id").equals(targetID)) {
                Element instance = (Element) instances.item(i);
                Node child = instance.getFirstChild();
                if (child == null) {
                    // inject source into instance
                    instance.appendChild(doc.importNode(d.getDocumentElement(), true));
                } else {
                    instance.replaceChild(doc.importNode(d.getDocumentElement(), true), instance.getFirstChild());
                }
                insertedInstance = true;
                /* remove src attribute, if there is one */
                instance.removeAttribute("src");
            }
        }

        // doc is the real presentation document
        return doc;
    }

    static Exception findInnermostException(Exception e) {
        //Log.debug("***"+e.getClass());
        if (e instanceof TransformerException) {
            Exception temp = (Exception) ((TransformerException) e).getCause();
            if (temp == null)
                return e;
            return findInnermostException(temp);
        } else if (e instanceof SAXException) {
            Exception temp = (Exception) ((SAXException) e).getException();
            if (temp == null)
                return e;
            return findInnermostException(temp);
        }
        return (Exception) e;
    }

    public boolean isTrueDocument() {
        return trueDocument;
    }

    /**
     * This method is alternative for fetchXMLDOM(). Creates the DOM tree for
     * the document defined by
     * 
     * @param URL
     *                   sourceURL Sets the variable sourceXMLDoc to refer to the
     *                   document.
     *                   <p>
     *                   If the document is a HTML document, it is handled separately
     *                   by the util.XHTMLTool.
     *                   </p>
     */
    protected Document fetchAndSetDOM(URL sourceURL) throws Exception {
        Document sourceDoc = null;

        if (m_stream == null)
            return fetchXMLDOM(this.sourceXMLURL);
        else
            return fetchXMLDOM(m_stream, this.sourceXMLURL);
    }

    protected Document fetchXMLDOM(URL sourceURL) throws Exception {
        Log.info("parsing new document: " + sourceURL.toString());
        try {
            /*
             * BufferedReader in =
             */
            Document document = m_parser.openDocument(sourceURL);
            return document;
        } catch (Exception e) {
            Log.error(e);
            throw e;
        }
    }

    protected Document fetchXMLDOM(InputStream stream, URL sourceURL) throws Exception {
        Log.info("parsing new document from stream: " + sourceURL.toString());
        try {
            /*
             * BufferedReader in =
             */
            Document document = m_parser.openDocument(stream, true, sourceURL.toString());
            return document;
        } catch (Exception e) {
            Log.error(e);
            throw e;
        }
    }

    protected Document fetchXMLDOM(InputStream stream) throws Exception {
        Log.info("parsing new document from stream gotten from socket.");
        try {
            /*
             * BufferedReader in =
             */
            Document document = m_parser.openDocument(stream, true);
            return document;
        } catch (Exception e) {
            Log.error(e);
            throw e;
        }
    }

    protected Document processStyleSheet(Document xml_dom, URL xsl_url, URL xml_url) throws Exception {
        Log.info("Running the XSL transform.");
        Document doc;
        String xslproc = m_browser.getBrowserConfigurer().getProperty("main/xslprocessor");
        if (xslproc.equals("XT")) {
            String Lines = m_browser.getXSLEngine().transformToString(sourceXMLURL, xsl_url); //Use
                                                                                                                                                    // XT
                                                                                                                                                    // Engine
            doc = createDomFromString(Lines);
        } else
            doc = m_browser.getXSLEngine().transform(xml_dom, xsl_url, xml_url);
        // to make xml:base and element.getBaseURI to work -MH
        if (doc instanceof CoreDocumentImpl)
            ((CoreDocumentImpl) doc).setDocumentURI(xml_url.toString());
        return doc;

    }

    /**
     * This method gets the first processing instruction from doc, which
     * contains the target 'xml-stylesheet'. It returs the href - attribute of
     * that processing instruction as an URL.
     * 
     * Additions: Read all stylsheets, and choose the correct one. (juha)
     */
    protected XSmilesPI findXSL(Document doc) throws Exception {
        stylesheetTitles.removeAllElements();

        // If preferred stylesheet title not selected, than use default from
        // configuration

        if (preferredTitle == null || preferredTitle.equals(""))
            preferredTitle = m_browser.getBrowserConfigurer().getGUIProperty(m_browser, "preferredStylesheetTitle");
        //Log.debug("The preferred stylesheet title: "+ preferredTitle);
        String href = null;

        /*
         * String preferredHref=null; String firstPrimaryHref=null; String
         * firstAlternativeHref=null; String currentTitle=null; String
         * firstPrimaryTitle=null;
         */

        XSmilesPI firstPrimaryPI = null;
        XSmilesPI firstAlternativePI = null;
        XSmilesPI preferredPI = null;

        ProcessingInstruction pi;

        Node child = doc.getFirstChild(); // Get the first
                                                            // node
        // Log.debug("PreferredTitle: " + preferredTitle);

        while (child != null) { // Loop xslt processing instructions
                                          // 'till no more nodes
            if (child.getNodeType() == Node.PROCESSING_INSTRUCTION_NODE) { // Handle
                                                                                                                            // PI
                                                                                                                            // nodes
                pi = (ProcessingInstruction) child; // Output
                                                                       // node
                if (pi.getTarget().equals("xml-stylesheet") || pi.getTarget().equals("xml-edit")) {
                    String data = pi.getData();
                    String alternate = getAttribute(data, "alternate");
                    String type = getAttribute(data, "type");
                    String title = getAttribute(data, "title");
                    String media = getAttribute(data, "media");

                    if (type == null || !(type.equals("text/xsl") || type.equals("application/xml"))) {
                        Log.debug("No XSLT or XForms editor found: " + type);
                    } else if (evalMediaQuery(media) == false) {
                        //Log.debug("Stylesheet not for this device:
                        // "+getAttribute(data,"href"));
                    } else {
                        href = getAttribute(data, "href");

                        if (href == null) {
                            Log.error("Stylesheet title or href null");
                        }
                        if (title == null)
                            title = href;
                        XSmilesPI tempPI = new XSmilesPI(pi, title, new URL(this.getXMLURL(), href));
                        if ((alternate != null && alternate.equals("yes")) && firstAlternativePI == null) {
                            firstAlternativePI = tempPI;
                        }

                        if ((alternate == null || !alternate.equals("yes")) && firstPrimaryPI == null) {
                            firstPrimaryPI = tempPI;
                        }

                        if (title != null && title.equals(preferredTitle))
                            preferredPI = tempPI;

                        stylesheetTitles.addElement(title);
                        //Log.debug("Adding Stylesheet reference: "+ title);
                    }

                }
            }
            child = child.getNextSibling(); // Get next node
        }

        if (preferredPI != null) {
            return preferredPI;
        } else if (firstPrimaryPI != null) {
            return firstPrimaryPI;
        } else if (firstAlternativePI != null) {
            return firstAlternativePI;
        }

        return null;
    }

    /**
     * Public method accessible from MLFCs and used above in findXSL().
     * 
     * @param media
     *                   Media attribute string
     * @return true if media query evaluates to true
     */
    public boolean evalMediaQuery(String media) {
        XMLConfigurer config = m_browser.getBrowserConfigurer();
        // A fast kludge to send guiname to MediaQuery.
        // It would be much better to add them the the method calls
        MediaQuery.guiName = m_browser.getGUIManager().getCurrentGUIName();
        return MediaQuery.evalMedia(media, config);
    }

    /**
     * return an xml attribute from a data String
     * 
     * @param data
     *                   The datastring
     * @param att
     *                   The attribute to look for
     * @return the value of attribute, (null if not found)
     */
    public static String getAttribute(String data, String att) {
        int indexAtt = data.indexOf(att);
        if (indexAtt == -1)
            return null;
        int start = data.indexOf("\"", indexAtt) + 1;
        if (start == -1)
            return null;
        int end = data.indexOf("\"", start);
        if (end == -1)
            return null;
        String attribute = data.substring(start, end);
        //Log.debug(attribute);
        return attribute;
    }

    /**
     * @return A Vector containing titles of all stylesheets assosiated with
     *               browser
     */
    public Vector getStylesheetTitles() {
        return stylesheetTitles;
    }

    /**
     * @return the title of the current stylesheet
     */
    public String getCurrentStylesheetTitle() {
        return currentStylesheetTitle;
    }

    protected Vector sourceStringToVector(String sourcestring) {

        StringTokenizer tokenizer = new StringTokenizer(sourcestring, "\n");
        Vector vector = new Vector();
        while (tokenizer.hasMoreTokens()) {
            vector.addElement(tokenizer.nextElement());
        }
        return vector;
    }

    protected Document createDomFromString(String source) {
        Document document;
        //Log.debug(source);
        java.io.StringReader sourceReader;
        if (source != null && source != "")
            sourceReader = new java.io.StringReader(source);
        else
            return null;
        //Log.debug("sourceReader olemassa");
        if (sourceReader != null) {
            try {
                document = m_parser.openDocument(sourceReader, true);
                // Log.debug("Doc parsed.");
                sourceReader.close();
                return document;
            } catch (Exception e) {
                Log.error(e);
                return null;
            }
        } else {
            Log.debug("Failed to open document.");
            sourceReader.close();
            return null;
        }
    }

    /**
     * Accessor method.
     * 
     * @return an object providing the DOM-interface to the document.
     */
    public Document getDocument() {
        return this.destXMLDoc;
    }

    /**
     * Accessor method.
     * 
     * @return an object providing the DOM-interface to the XML part of the
     *               document.
     */
    public Document getXMLDocument() {
        return sourceXMLDoc;
    }

    /**
     * Accessor method.
     * 
     * @return an object providing the DOM-interface to the XSL part of the
     *               document.
     */
    public Document getXSLDocument() {
        try {
            if (this.XSLDoc == null && this.XSLURL != null)
                this.XSLDoc = fetchXMLDOM(this.XSLURL);
        } catch (Exception e) {
            Log.error(e);
        }

        return XSLDoc;
    }

    /**
     * Accessor method.
     * 
     * @return String the name of the markup-language of the current document.
     *               !NOTE! Do not trust this blindly. The implementation is
     *               non-standard.
     */
    public String getMLName() {
        if (destXMLDoc != null) {
            /*
             * Note : There is not a clear interface to the <!DOCTYPE....>
             * element The first TAG is used to determine the MLName of the
             * Document
             *  
             */
            String tagName = this.destXMLDoc.getDocumentElement().getTagName();
            //Log.debug(tagName);
            if (tagName.indexOf(":") != -1) // it's e.g.fo:
                //return "XML";
                return "XSLFO";
            else {
                return this.destXMLDoc.getDocumentElement().getTagName();
            }
        } else
            return null;
    }

    /**
     * Used for locking/unlocking the document for mutex-reasons. !Note! Not
     * implemented yet!
     */
    public void acquireToken() {
    }

    /**
     * Used for locking/unlocking the document for mutex-reasons. !Note! Not
     * implemented yet!
     */
    public void releaseToken() {
    }

    /**
     * Accessor method.
     * 
     * @return the source of the destination XML document as one String.
     *  
     */
    public String getSourceText(Document doc) {
        return this.getSourceText(doc, false);
    }

    public String getSourceText(Document doc, boolean prettyPrinting) {
        String orig = m_parser.write(doc, prettyPrinting);
        return orig;
    }

    /**
     * Accessor method.
     * 
     * @return the source of the destination XML document as a Enumeration.
     *  
     */
    public Enumeration getSourceEnumeration() {
        return getSourceVector().elements();
    }

    /**
     * Accessor method.
     * 
     * @return the source of the destination XML document as a Vector.
     *  
     */
    public Vector getSourceVector() {
        return sourceStringToVector(this.getSourceText(this.destXMLDoc, (XSLURL != null)));

    }

    /**
     * Accessor method.
     * 
     * @return returns the source of the source XML document as a Vector.
     */
    public Vector getXMLVector() {
        return sourceStringToVector(getSourceText(this.sourceXMLDoc, false));
    }

    /**
     * Accessor method.
     * 
     * @return returns the source of XSL-stylesheet as a Vector.
     */

    public Vector getXSLVector() {
        return sourceStringToVector(getSourceText(this.getXSLDocument(), false));
    }

    /**
     * Accessor method.
     * 
     * @return the XLink object associated with the document.
     *  
     */
    public XLink getLink() {
        return m_link;
    }

    /**
     * Accessor method
     * 
     * @return the URL of the source xml document
     *  
     */
    public URL getXMLURL() {
        return sourceXMLURL;
    }

    /**
     * Accessor method
     * 
     * @return the URL of the xsl document
     *  
     */
    public URL getXSLURL() {
        return XSLURL;
    }

    public ECMAScripter getECMAScripter() {
        if (this.currentScripter == null) {
            //Log.debug("* * * * * requesting for ecmascripter,
            // browser="+(m_browser==null));
            if(this.getBrowser().getBrowserConfigurer().getBooleanProperty("security/javascript/enabled"))
                this.currentScripter = ECMAScripterFactory.getScripter("rhino",this.m_browser);
            else this.currentScripter = ECMAScripterFactory.getScripter("dummy",this.m_browser);
        }
        return this.currentScripter;
    }

    /**
     * @param title
     *                   Sets the preferred stylesheet title.
     */
    public void setPreferredStylesheetTitle(String title) {
        preferredTitle = title;
    }

    public void deactivate() {
        if (currentScripter != null)
            currentScripter.destroy();

        // some cleanup of the document for memory leaks
        //		ExtendedDocument doci = (ExtendedDocument)this.getDocument();
        //	if (doci!=null) doci.destroy();

        currentScripter = null;
    }

    public BrowserWindow getBrowser() {
        return m_browser;
    }
}
