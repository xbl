/*
 * Created on Jul 7, 2004
 * 
 * TODO To change the template for this generated file go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.csslayout;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import fi.hut.tml.xsmiles.gui.components.XContainer;


import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.EventFactory;

/**
 * @author mpohja
 * 
 */
public class ExternalWindow 
{
    Window myFrame;
    CSSRenderer renderer;
    Image logo;
    MediaTracker tracker;
    ImageObserver imageObserver;
    Container scrollPane;
    Node node;

    /**
     * @param title
     * @param containertype
     * @param node
     * @throws java.awt.HeadlessException
     */
    public ExternalWindow(String title, CSSRenderer ren, String containertype, Node n) throws HeadlessException
    {
        super();
        this.node=n;
        myFrame=null;
        if (containertype!=null)
        {
            if (containertype.equalsIgnoreCase("ephemeral"))
	        {
	            Frame parent = this.findParent(ren.mlfc.getMLFCListener().getContentArea());
	            if (parent!=null)
	                myFrame=new Window(parent);
	        } 
            else if (containertype.equalsIgnoreCase("modal"))
	        {
	            Frame parent = this.findParent(ren.mlfc.getMLFCListener().getContentArea());
	            if (parent!=null)
	                myFrame=new Dialog(parent);
	        } 
        }	       
        if (myFrame==null)    myFrame=new Frame();
        //scrollPane = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
        init(title, ren);
        this.myFrame.add(ren.getComponent(), BorderLayout.CENTER);
        this.myFrame.addWindowListener(new RendererWindowListener());

        // Set window logo
        logo = Toolkit.getDefaultToolkit().getImage("img/browserlogo.gif");
        tracker = new MediaTracker(this.myFrame);
        imageObserver = this.myFrame;
        if (logo != null)
            loadImage(logo);
        if (this.myFrame instanceof Frame ) ((Frame)this.myFrame).setIconImage(logo);
    }
    
    protected Frame findParent(Component comp)
    {
        if (comp==null) return null;
        if (comp instanceof Frame) return (Frame )comp;
        return findParent(comp.getParent());
    }

    public void init(String title, CSSRenderer ren)
    {
        if (scrollPane!=null&&renderer==ren) return;
        scrollPane = ren.getComponentFactory().createScrollPane(ren.getComponent(),0);
        this.myFrame.removeAll();
        this.myFrame.add(scrollPane);
        //removeAll();
        /*
        if (renderer != null)
            scrollPane.remove(renderer);*/
        if (this.myFrame instanceof Frame) ((Frame)this.myFrame).setTitle(title);
        renderer = ren;
        //this.setLayout(new BorderLayout());
        //scrollPane.add(ren);
    }
    
    /**
     * Loads the image, returning only when the image is loaded.
     * 
     * @param image
     *            the image
     */
    void loadImage(Image image)
    {
        tracker.addImage(image, 1);
        try
        {
            tracker.waitForID(1, 0);
        }
        catch (InterruptedException e)
        {
            Log.error("No logo " + e);
        }
        //loadStatus = tracker.statusID(id, false);
        tracker.removeImage(image, 1);
    }
    /** dispatch a custom DOM event, so that xforms message can listen to window closing and
     * set the pseudoclass correctly
     */
    protected void dispatchCloseDOMEvent()
    {
        
        EventTarget target = ((EventTarget)this.node);
        if (target!=null)
        {
            target.dispatchEvent(EventFactory.createEvent(EventFactory.XSMILES_MESSAGE_CLOSE_EVENT,false,false));
        }
    }

    private class RendererWindowListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent e)
        {
            ExternalWindow.this.dispatchCloseDOMEvent();
            dispose();
            Log.debug("Window closed "+ ExternalWindow.this);
        }
    }

    public void dispose()
    {
        this.myFrame.dispose();
        
    }

    public void setLocationRelativeTo(Container container)
    {
      myFrame.setLocationRelativeTo(container);
        
    }

    public void setSize(int i, int j)
    {
        // TODO Auto-generated method stub
        myFrame.setSize(i,j);
        
    }
    public void show()
    {
        myFrame.show();
    }

    public void pack()
    {
        myFrame.pack();
    }

    /**
     * 
     */
    public void repaint()
    {
        // TODO Auto-generated method stub
        myFrame.repaint();
        
    }
}
