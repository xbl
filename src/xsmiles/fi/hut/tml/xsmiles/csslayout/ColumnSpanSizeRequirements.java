/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 23, 2006
 */
package fi.hut.tml.xsmiles.csslayout;

public class ColumnSpanSizeRequirements extends SizeRequirements
{
    int firstColumn, columnCount;
    
    public ColumnSpanSizeRequirements(int col, int cols)
    {
        super();
        firstColumn = col;
        columnCount = cols;
    }
    
    public int getFirstColumn()
    {
        return firstColumn;
    }
    
    public int getColumnCount()
    {
        return columnCount;
    }
}
