/*
 * CSS_Property.java
 *
 * Created on October 10, 2003, 11:58 AM
 */

package fi.hut.tml.xsmiles.csslayout;

/**
 *
 * @author  alessandro
 */
public interface CSS_Property
{
    //    String DEFAULT_FONT = "serif";
    String FOREGROUND_COLOR = "color";
    String BACKGROUND_COLOR = "background-color";
    String REAL_BACKGROUND_COLOR = "real-background-color";
    String BORDER_COLOR = "border-color";
    String BORDER_TOP_COLOR = "border-top-color";
    String BORDER_RIGHT_COLOR = "border-right-color";
    String BORDER_LEFT_COLOR = "border-left-color";
    String BORDER_BOTTOM_COLOR = "border-bottom-color";
    String BORDER_STYLE = "border-style";
    String BORDER_TOP_STYLE = "border-top-style";
    String BORDER_RIGHT_STYLE = "border-right-style";
    String BORDER_LEFT_STYLE= "border-left-style";
    String BORDER_BOTTOM_STYLE = "border-bottom-style";
    String BORDER_WIDTH = "border-width";
    String BORDER_TOP_WIDTH = "border-top-width";
    String BORDER_RIGHT_WIDTH = "border-right-width";
    String BORDER_LEFT_WIDTH = "border-left-width";
    String BORDER_BOTTOM_WIDTH = "border-bottom-width";
    String CLEAR = "clear";
    String FONT = "font";
    String FLOAT = "float";
    String FONT_FAMILY = "font-family";
    String FONT_STYLE = "font-style";
    String FONT_WEIGHT = "font-weight";
    String FONT_SIZE = "font-size";
    String BACKGROUND_IMAGE = "background-image";
    String BACKGROUND_POSITION = "background-position";
    String H_BACK_POSITION = "h-back-position";
    String V_BACK_POSITION = "v-back-position";
    String BACKGROUND_REPEAT = "background-repeat";
    String MARGIN = "margin";
    String MARGIN_TOP = "margin-top";
    String MARGIN_BOTTOM = "margin-bottom";
    String MARGIN_RIGHT = "margin-right";
    String MARGIN_LEFT = "margin-left";
    String PADDING = "padding";
    String PADDING_TOP = "padding-top";
    String PADDING_BOTTOM = "padding-bottom";
    String PADDING_RIGHT = "padding-right";
    String PADDING_LEFT = "padding-left";
    String TEXT_ALIGN = "text-align";
    String TEXT_DECORATION = "text-decoration";
    String TEXT_INDENT = "text-indent";
    String TEXT_TRANSFORM = "text-transform";
    String WIDTH = "width";
    String HEIGHT = "height";
    String CAPTION_SIDE = "caption-side";
    String LIST_STYLE_TYPE = "list-style-type";
    String LIST_STYLE_IMAGE = "list-style-image";
    String LIST_STYLE_POSITION = "list-style-position";
    String TOP = "top";
    String BOTTOM = "bottom";
    String LEFT = "left";
    String RIGHT = "right";
    String VISIBILITY = "visibility";
    String DISPLAY = "display";
    String OVERFLOW = "overflow";
    String POSITION = "position";
    String WHITESPACE = "white-space";
    String VERTICAL_ALIGN = "vertical-align";
    String CONTAINER = "xsm-container";
    String CONTAINERTYPE = "xsm-containertype";
}
