/*
 * Created on 21-Jul-2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fi.hut.tml.xsmiles.csslayout;

import java.awt.Color;
import java.awt.Font;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import fi.hut.tml.xsmiles.csslayout.view.*;
/**
 * @author Alessandro
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */




public class Adapter {
	
	protected CSSRenderer renderer;
	protected int widthCont;
	protected int heightCont;
	private final int defaultWidth = 1024;
	private final int defaultheight = 768;
	private final short level1 = 1;
	private final short level2 = 2;
	private final short level3 = 3;
	private float adapterZoom = 1 ;
	

	public Adapter(CSSRenderer renderer)
    {
		this.renderer = renderer;
	
    }
	public void setFontZoom(double zoom){
		
		StyleGenerator.setFontZoom(zoom);
		
	}
	public void setWSize()
	{
		widthCont = renderer.getMLFC().getContainer().getSize().width - 30;
	    heightCont = renderer.getMLFC().getContainer().getSize().height;
	    System.out.println(widthCont +"-----------"+ heightCont);
	    adapterZoom = ((widthCont * 100)  / defaultWidth );
	    adapterZoom = adapterZoom / 100;
	    System.out.println(adapterZoom +"------cazzo-----");
	}
	public String getStringValue(View view, CSSStyleDeclaration style, String what)
	{
		return StyleGenerator.getStringValue(style, what);
	}
	
	public int getMeasure(CSSValue v)
	{
		return (int)(StyleGenerator.getMeasure(v) * adapterZoom) ;
	}
	
	public int getMeasure(View view, CSSStyleDeclaration style, String what)
	{
		return (int)(StyleGenerator.getMeasure(style, what)* adapterZoom) ;
	}
	
	public int getImageSize(int orig)
	{
	    return (int)(StyleGenerator.getImageSize(orig)* adapterZoom);
	}
	
	public int getScaledSize(View view, int orig)
	{
		return StyleGenerator.getScaledSize(orig);
	}
	
	public boolean isPercentageValue(View view, CSSStyleDeclaration style, String property)
	{
		return StyleGenerator.isPercentageValue(style, property);
	}
	public Font getCSSFontObj(View view, CSSStyleDeclaration style)
    {
		Font font = StyleGenerator.getCSSFontObj(style);
		System.out.println(font.getSize2D() +"-----font2D------");
		System.out.println(adapterZoom +"-----zoom------");
		float size = (font.getSize2D());
		System.out.println(size +"-----size------");
		size = size * adapterZoom;
		size = size * 1.2f ;
		System.out.println(size +"-----sizepost------");
		font = font.deriveFont(size);
		System.out.println(font.getSize2D() +"-----fontsize------");
		
		
        return font;
    }

    public Color parseColor(View view, CSSValue color)
    {
        return StyleGenerator.parseColor(color);
    }
}