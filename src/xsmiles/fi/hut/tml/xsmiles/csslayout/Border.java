/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.csslayout;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author  Alessandro
 */
public class Border {
    
    protected int thickness = 1;
    protected Color borderColor = Color.gray; 
    
    /** Creates a new instance of Border */
    public Border() {        
    }
    
    public Border(Color color) {
        
        borderColor = color;
    }
    
    public Border(int thick) {
        
        thickness = thick;
    }
    
    public Border(Color color, int thick) {
        
        borderColor = color;
        thickness = thick;
    }
    
    public void paintBorder(Graphics g, int x, int y, int width, int height) {
        
        Color oldColor = g.getColor();
        int i;
        g.setColor( borderColor );
        for(i=0; i<thickness; i++){
            g.drawRect(x+i, y+i, width-i-i-1, height-i-i-1);        
        }
        g.setColor(oldColor);
    }    
     public void setThickness(int thick){
     
        thickness = thick;
     }
     
      public void setColor(Color color){
     
        borderColor = color;
     }
    
}
