/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.csslayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.csslayout.view.BaseView;
import fi.hut.tml.xsmiles.csslayout.view.BlockView;
import fi.hut.tml.xsmiles.csslayout.view.RootView;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.dom.AsyncChangeHandler;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.PopupHandler;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xmlcss.XMLCSSMLFC;
import fi.hut.tml.xsmiles.util.EventUtil;

/**
 * @author
 */
public class CSSRenderer implements LayoutManager, PopupHandler, MouseListener, MouseMotionListener
{
    /*
     * Cannot extend a component directly, since in J9 AWT a Panel is needed, in
     * order to scrolling to work, while in Swing a Container is enough
     */

    /** the DOM document that should be rendered */
    Document doc;

    RootView rootView;

    protected RendererComponent renderer;

    //Rectangle repaintArea;

    protected MLFC mlfc;

    boolean inWindow;
    
    public int layouts = 0;

    /**
     * this is the root, which is e.g. a Panel in AWT J9 and Container in Swing
     * It's size is handled by the implemented LayoutManager The size depends on
     * the size of the rootView, which depends on the size of the parent
     * container...
     */
    protected Container rootContainer;

    protected Dimension size;// = new Dimension(200, 100);

    /**
     * this is used if this renderer is part of anouther renderer, which wants
     * to show part of DOM in external window
     */
    protected ExternalWindow window;

    /** should we do antializing */
    public static boolean antiAlias = true;

    /* minimum font size to anti-alias */
    public static int minAlias = 16;

    /* when dirty is false, refreshImmediately exits immediately */
    protected boolean dirty = false;

    /** only one refreshImmediately will be scheduled */
    protected boolean refreshScheduled = false;

    protected ContentFocusPointsProvider fpProvider;
    private Runnable refreshRunnable = new Runnable()
    {

        public void run()
        {
            refreshImmediately();
        }
    };

    private URL currentURL = null;
    private Node lastNode = null;
    /**
     * ComponentView will do delayed removal using this vector. Reason: when
     * style is changed, old views are removed from view and new ones created.
     * If the component is always first removed and then re-added, it will lose
     * focus.
     */
    protected Vector componentsToBeRemoved;
    private static int counter=0;
    /** Creates a new instance of CSSRenderer */
    public CSSRenderer(MLFC m, boolean isInWindow)
    {
        //MH: to avoid mem leaks in the long run
        counter++;
        if (counter==10 )
        {
            StyleGenerator.clearCaches();
            counter=0;
        }
        // this.setLayout(null);
        mlfc = m;
        inWindow = isInWindow;
        this.createRootContainer();
        fpProvider = new ContentFocusPointsProvider(this);
        mlfc.getXMLDocument().getBrowser().registerFocusPointProvider(fpProvider);
        // repainter = new ShowThread();
        componentsToBeRemoved = new Vector(20);
    }

    public void removeComponentDelayed(Component comp)
    {
        if (!this.componentsToBeRemoved.contains(comp))
            this.componentsToBeRemoved.addElement(comp);
    }

    public void cancelComponentRemoval(Component comp)
    {
        this.componentsToBeRemoved.remove(comp);
    }

    public void removeAllDelayedComponents()
    {
        //Log.debug("CSSRenderer: removing delayed components (" + this.componentsToBeRemoved.size()
          //      + ").");
        Enumeration comps = this.componentsToBeRemoved.elements();
        while (comps.hasMoreElements())
        {
            Component component = (Component) comps.nextElement();
            if (component.getParent() != null)
            {
                component.getParent().remove(component);
                // Log.debug("Removed component: "+component);
            }

        }
        this.componentsToBeRemoved.removeAllElements();
    }

    /** to set the zoom, for instance by the UI */
    public void setZoom(double zoom)
    {
        StyleGenerator.setFontZoom(zoom);
        /*
         * if ((this.getRootView()) != null) {
         * System.out.println("((((((((((((((((((((--"+zoom+"---cssRender");
         * this.getRootView().setZoom(zoom); }
         */
    }

    public ComponentFactory getComponentFactory()
    {
        return mlfc.getMLFCListener().getComponentFactory();
    }

    public Container getComponent()
    {
        return this.rootContainer;
    }
    
    public void createRootContainer()
    {
        this.rootContainer = this.getComponentFactory().createContentPanel();
        /*
         * It's size is handled by the implemented LayoutManager The size
         * depends on the size of the rootView, which depends on the size of the
         * parent container...
         */
        this.rootContainer.setLayout(this);
    }

    protected Dimension lastSize = null;

    /** this is called from the MLFC when the zoom is changed */
    public void recreateViews()
    {
        this.setDocument(this.doc);
    }

    /**
     * this is called internally to layout the rootView and all other views e.g.
     * when the parent container size has changed
     */
    public void layoutRenderer()
    {
        if (rootView != null)
        {
            Log.debug("Doing full layout");
            rootView.doLayout();
            Rectangle pos = rootView.getRectangle();
            this.size = new Dimension(pos.width, pos.height);
            // this.renderer.setPreferredSize(size); not supported in
            // personaljava
            if (this.renderer != null)
            {
                this.renderer.setSize(size);
            }
            /*
             * Log.debug("layoutRenderer, size: "+size);
             * Log.debug("rendererComponent, size: "+this.renderer.getSize());
             * Log.debug("rootContainer, size: "+this.rootContainer.getSize());
             */
        }
        else
        {
            Log.error("LayoutRenderer: Rootview was null");
        }
    }

    public void setDocument(Document adoc)
    {
        // StackedLayout layout = new StackedLayout();
        // this.setLayout(layout);
        this.doc = adoc;
        this.dirty = true;

        // long start = System.currentTimeMillis();

        this.rootContainer.removeAll();
        this.createViews();
        this.renderer = new RendererComponent(this.rootView);
        this.renderer.addMouseListener(this);
        this.renderer.addMouseMotionListener(this);
        
        // We need an extra component to have a white background behind everything.
/*        Component background = new Component()
        {
            private static final long serialVersionUID = 1L;

            public void paint(Graphics g)
            {
                super.paint(g);
                g.setColor(Color.white);
                Rectangle rect = getRootView().getRectangle();
                Rectangle clip = g.getClipBounds();
                Rectangle intersect = rect.intersection(clip);
                g.fillRect(intersect.x, intersect.y, intersect.width, intersect.height);
            }
        };
        background.setVisible(true);
        background.validate();
        this.rootContainer.add(background);*/
        this.rootContainer.add(renderer, 0);

        /*
         * long time = System.currentTimeMillis() - start;
         * System.out.println("***** createViews() took " + Long.toString(time) + "
         * ms");
         */
        // Log.debug("Dumping views before layout...");
        // rootView.dump();
        // start = System.currentTimeMillis();
        this.layoutRenderer();
        Rectangle bg = getRootView().getRectangle();
        //background.setSize(new Dimension(bg.width, bg.height));
        /*
         * time = System.currentTimeMillis() - start; System.out.println("*****
         * layoutRenderer() took " + Long.toString(time) + " ms");
         */
        //Log.debug("Dumping views after layout...");
        //rootView.dump("");
        setFocusComponent();
        this.setInitialSize(size);
    }

    public Document getDocument()
    {
        return doc;
    }

    protected void setFocusComponent()
    {
    }

    public void setInitialSize(Dimension size)
    {
        if (this.renderer != null)
            this.renderer.setSize(size);
        this.rootContainer.setSize(size);
    }

    public RootView getRootView()
    {
        return rootView;
    }

    public void setRootView(RootView root)
    {
        rootView = root;
        // Log.debug("setRootView: " + this.renderer.hashCode() + "rootView:" +
        // this.rootView);
    }

    public void createViews()
    {
        // Log.debug("createViews:
        // "+this.renderer.hashCode()+"rootView:"+this.rootView);
        Element docElem = this.doc.getDocumentElement();
        rootView = new RootView(this, null, null);
        BlockView docBlock = new BlockView(this, docElem, rootView);
        rootView.addChildView(docBlock);
        docBlock.createChildViews(docElem);
    }

    public void createWindow(final Node node, String display, final String containertype)
    {
        Log.debug("CSSR: createWindow");
        CSSRenderer ren = new CSSRenderer(getMLFC(), true);

        RootView root = new RootView(ren, null, null);
        ren.renderer = new RendererComponent(root);

        ren.getComponent().removeAll();
        ren.getComponent().add(ren.renderer);
        ren.setRootView(root);

        View view = root.createView(node, display);

        // BlockView docBlock = new BlockView(ren, node, root);
        root.addChildView(view);
        view.createChildViews(node);

        String title = "X-Smiles message";
        if (node instanceof Element && node.getAttributes().getNamedItem("title") != null)
            title = ((Element) node).getAttribute("title");

        org.apache.xerces.dom.NodeImpl dom3Node = (org.apache.xerces.dom.NodeImpl) node;
        ExternalWindow window = (ExternalWindow) dom3Node.getUserData("window");
        if (window != null)
        {
            window.dispose();
            /*
             * window.init(title, ren); ren.layoutRenderer();
             * ren.setSize(ren.size); //window.setSize(ren.size.width + 30,
             * ren.size.height + 0); ren.invalidate(); //window.show();
             */}
        // else
        {
            final String fTitle = title;
            final CSSRenderer fRen = ren;
            AccessController.doPrivileged(new PrivilegedAction()
            {

                public Object run()
                {
                    // RUN WITH FULL PRIVS TO REMOVE APPLET HEADER FROM WINDOW
                    ExternalWindow win = new ExternalWindow(fTitle, fRen, containertype, node);
                    win.setLocationRelativeTo(mlfc.getContainer());
                    fRen.addWindow(win);

                    fRen.layoutRenderer();
                    fRen.setInitialSize(fRen.size);
                    win.setSize(fRen.size.width + 30, fRen.size.height + 0);

                    win.show();
                    win.pack();
                    return null; // nothing to return
                }
            });

            // Set window instance into the corresponding node.
            dom3Node.setUserData("window", fRen.window, null);
        }
    }

    public void addWindow(ExternalWindow frame)
    {
        if (((XMLCSSMLFC) mlfc).windows == null)
            ((XMLCSSMLFC) mlfc).windows = new Vector(15);
        ((XMLCSSMLFC) mlfc).windows.addElement(frame);
        // Log.debug("View, added child.");
        window = (ExternalWindow) frame;
    }

    public MLFC getMLFC()
    {
        return mlfc;
    }

    public boolean isInWindow()
    {
        return inWindow;
    }

    public Graphics getGraphicsContext()
    {
        return this.getComponent().getGraphics();
        // return renderer.getGraphics();
    }

    // protected ShowThread repainter = null;

    public final static short LAYOUT_DONE = 5;
    public final static short LAYOUT_IN_PROGRESS = 50;

    protected short layoutState = LAYOUT_DONE;

    protected boolean redoLayout = false;

    protected boolean transactionInProgress = false;

    public void startTransaction()
    {
        // repainter.startTransaction();
    }

    public void commit()
    {
        // repainter.endTransaction();
    }

    protected void invokeLater(Runnable run)
    {
        if (this.doc != null && this.doc instanceof AsyncChangeHandler)
        {
            ((AsyncChangeHandler) this.doc).invokeLater(run);
        }
        else
        {
            EventQueue.invokeLater(run);
        }
    }
    
    public void repaint(Rectangle rect)
    {
        if (renderer != null)
            renderer.repaint(rect.x, rect.y, rect.width, rect.height);
    }

    public void refresh()
    // public void refresh(int x, int y, int width, int height)
    {
        this.dirty = true;
        this.redoLayout = true;
        if (!refreshScheduled)
        {
            this.refreshScheduled = true;
            this.invokeLater(refreshRunnable);
        }
        // repainter.needsLayout();
        // if (getMLFC() instanceof XMLCSSMLFC)
        // ((XMLCSSMLFC)getMLFC()).repaintExternalWindows();
    }

    /**
     * Refresh only given view if its size hasn't been changed. Otherwise calls
     * general refresh. This is an optimized version of the refresh, which is
     * called e.g. when a style is changed for mouseovers
     * 
     * @param view
     */
    public void refresh(View view)
    {
        dirty = true;
        Rectangle origView = new Rectangle(view.getAbsolutePositionX(), view.getAbsolutePositionY(),
                view.getViewWidth(), view.getViewHeight());
        view.doLayout(); // should we make sure that this is also run in AWT
        Log.debug("partial layout:"+this.layouts);
        this.layouts=0;
                            // Thread?
        Rectangle newView = new Rectangle(view.getAbsolutePositionX(), view.getAbsolutePositionY(),
                view.getViewWidth(), view.getViewHeight());

        // Absolute positioned views should go through this. Otherwise
        // whole document is laid out again.
        if ((origView.equals(newView)) //|| ((BaseView) view).hasAbsolutePosition())
                && !view.hasAbsoluteChildren())
        {
            // Log.debug("Partial relayout, view: " + view);
            // if (!((BaseView) view).hasAbsolutePosition())
            // ;// repaintArea = view.getRectangle();
            // repainter.needsLayout();
            if (!this.redoLayout) // if full layout is scheduled, don't do this
            {
                Rectangle repaintArea = view.getRectangle();
                //Log.debug("Repaint area: " + repaintArea);
                // Following is needed to remove components. Not sure if causes other bugs, though.
                removeAllDelayedComponents();
                this.rootContainer.repaint(repaintArea.x, repaintArea.y, repaintArea.width,
                        repaintArea.height);
                repaintArea=null;
            }
            /*
            if (!refreshScheduled)
            {
                this.refreshScheduled = true;
                this.invokeLater(refreshRunnable);
            }*/
            // if (getMLFC() instanceof XMLCSSMLFC)
            // ((XMLCSSMLFC)getMLFC()).repaintExternalWindows();
        }
        else
        {
                refresh();
        }
    }

    /*
     * public Component getFocusOwner() { Container cont = this; while (!(cont
     * instanceof java.awt.Window)&&(cont!=null)) { cont=cont.getParent(); } if
     * (cont instanceof java.awt.Window) { return
     * ((java.awt.Window)cont).getFocusOwner(); } return null; }
     */

    /**
     * This does full re-layout for the document. IT SHOULD BE CALLED ONLY BY
     * THE REFRESHTHREAD
     * 
     */
    private synchronized void refreshImmediately()
    {
        refreshScheduled = false; // next one will schedule a new one.
        boolean inEventThread = EventQueue.isDispatchThread();
        if (!inEventThread)
            Log.error("refreshImmediately running in a wrong thread!");
        this.layoutState = LAYOUT_IN_PROGRESS;
        if (dirty == false)
        {
            Log.debug("*** refresh NO-OP since not dirty");
        }
        else
        {
            try
            {
                Log.debug("*****  CSS Layout start");
                // force new layout
                if (redoLayout)
                {
                    this.layoutRenderer();
//                     Log.debug("Dumping views after refresh layout...");
//                     rootView.dump("  ");
                    redoLayout = false;
                    dirty = false;
                }
                /*
                if (repaintArea != null)
                {
                    Log.debug("Repaint area: " + repaintArea);
                    this.rootContainer.repaint(repaintArea.x, repaintArea.y, repaintArea.width,
                            repaintArea.height);
                    repaintArea = null;
                }else*/
                
                {
                    this.rootContainer.repaint();
                }
                // FOR COMPONENTS WE HAVE TO CALL VALIDATE
                removeAllDelayedComponents();
                this.rootContainer.invalidate();
                this.rootContainer.getParent().validate();
            }
            catch (Throwable t)
            {
                Log.error(t);
            }
            Log.debug("********* CSS Layout done: laid out:"+layouts);
            layouts=0;
        }
        this.layoutState = LAYOUT_DONE;
    }

    /*
     * private class ShowThread implements Runnable {
     * 
     * private long wait = 500; private Object transactionLock = new Object();
     * private Object runLock = new Object(); private int runners = 0; private
     * boolean needsLayout = false;
     * 
     * private Runnable refresh = new Runnable() {
     * 
     * public void run() { refreshImmediately(); } };
     * 
     * public void needsLayout() { synchronized (runLock) { needsLayout = true;
     * if (runners == 0) { runners++; (new Thread(this, "ShowThread")).start(); }
     * runLock.notifyAll(); } }
     * 
     * public void startTransaction() { synchronized (transactionLock) {
     * transactionInProgress = true; transactionLock.notifyAll(); } }
     * 
     * public void endTransaction() { synchronized (transactionLock) {
     * transactionInProgress = false; transactionLock.notifyAll(); } }
     * 
     * private boolean layoutNeeded() { synchronized (runLock) { if
     * (needsLayout) { return true; } else { runners--; return false; } } }
     * 
     * private void layoutDone() { synchronized (runLock) { needsLayout = false; } }
     * 
     * private void waitForTransactionEnd() { synchronized (transactionLock) {
     * while (transactionInProgress) { try { transactionLock.wait(); } catch
     * (InterruptedException e) { } } } }
     * 
     * public void run() { while (layoutNeeded()) { waitForTransactionEnd();
     * 
     * if (layoutState != LAYOUT_IN_PROGRESS) { try { layoutDone();
     * CompatibilityFactory.getCompatibility().invokeAndWait(refresh); } catch
     * (InterruptedException e2) { } catch (InvocationTargetException e1) {
     * Log.error(e1); } }
     * 
     * Log.debug("Waiting before checking need for repaint."); synchronized
     * (runLock) { try { runLock.wait(wait); } catch (InterruptedException e) { } } } } }
     */
    public void mouseMoved(MouseEvent event)
    {
        if (rootView == null)
            return;
        Point pt = new Point(event.getX(), event.getY());
        View view = rootView.getViewAtPoint(pt);
        mouseMoved(view, event);
    }

    public void mouseMoved(View view, MouseEvent event)
    {
        if (view == null || view instanceof RootView)
        {
            if (currentURL != null)
            {
                currentURL = null;
                mlfc.getMLFCListener().setStatusText("Ready.");
            }
            CSSRenderer.this.getComponent()
                    .setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            return;
        }
        Node node = null;
        node = view.getDOMElement();
        while (node == null && view != null)
        {
            view = view.getParentView();
            node = view.getDOMElement();
        }
        if (!node.equals(lastNode))
        {
            // Log.debug("Mouseout: "+lastNode+" Mouseover: "+node);
            boolean cancelledToo;
            if (lastNode != null)
            {
                org.w3c.dom.events.MouseEvent outEvent =
                    (org.w3c.dom.events.MouseEvent)EventFactory
                    .createEvent("mouseout");
                outEvent.initMouseEvent("mouseout", true, true, null, 0,
                        0, 0, event.getX(),
                        event.getY(), event.isControlDown(), event.isAltDown(),
                        event.isShiftDown(), event.isMetaDown(),
                        (short)(event.getButton() - 1), null);
                cancelledToo = ((EventTarget) lastNode).dispatchEvent(outEvent);
            }
            org.w3c.dom.events.MouseEvent overEvent =
                (org.w3c.dom.events.MouseEvent)EventFactory
                .createEvent("mouseover");
            overEvent.initMouseEvent("mouseover", true, true, null, 0,
                    0, 0, event.getX(),
                    event.getY(), event.isControlDown(), event.isAltDown(),
                    event.isShiftDown(), event.isMetaDown(),
                    (short)(event.getButton() - 1), null);
            boolean cancelled = ((EventTarget) node).dispatchEvent(overEvent);
            lastNode = node;
        }
        String location = null;
        while (node != null)
        {
            if (node.getAttributes() != null && node.getAttributes().getNamedItem("href") != null)
                break;
            node = node.getParentNode();
        }
        if (node != null)
        {
            URL url = null;
            try
            {
                url = new URL(mlfc.getXMLDocument().getXMLURL(), node.getAttributes().getNamedItem(
                        "href").getNodeValue());
            }
            catch (Exception e)
            {
                Log.error(e);
            }
            if (!url.equals(currentURL))
            {
                rootContainer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                mlfc.getMLFCListener().setStatusText(url.toString());
                currentURL = url;
            }
        }
        else
        {
            if (currentURL != null)
            {
                currentURL = null;
                mlfc.getMLFCListener().setStatusText("Ready.");
            }
            rootContainer.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void mouseClicked(View view, MouseEvent event)
    {
        Log.debug("Clicked in " + view);
        event.consume();
        if (view == null || view instanceof RootView)
            return;
        Node node = null;
        node = view.getDOMElement();
        while (node == null)
        {
            view = view.getParentView();
            node = view.getDOMElement();
        }
        // Log.debug("click: " + node);
        org.w3c.dom.events.MouseEvent mEvent =
            (org.w3c.dom.events.MouseEvent)EventFactory.createEvent("click");
        mEvent.initMouseEvent("click", true, true, null, event.getClickCount(),
                0, 0, event.getX(),
                event.getY(), event.isControlDown(), event.isAltDown(),
                event.isShiftDown(), event.isMetaDown(),
                (short)(event.getButton() - 1), null);
        boolean cancelled = ((EventTarget) node).dispatchEvent(mEvent);
        if (cancelled)
        {
            return;
        }
        String location = null;
        while (node != null)
        {
            if (node.getAttributes() != null && node.getAttributes().getNamedItem("href") != null)
                break;
            node = node.getParentNode();
        }
        if (node != null)
        {
            // TODO: I don't think this method uses xml:base... Probably
            // should use elem.getURL, as in XHTMLMLFC
            location = node.getAttributes().getNamedItem("href").getNodeValue();
            try
            {
                URL url = new URL(mlfc.getXMLDocument().getXMLURL(), location);
                if (EventUtil.isPropertyRequest(event))
                {
                    mlfc.getMLFCListener().getComponentFactory().showLinkPopup(url,
                            mlfc.getXMLDocument(), event, mlfc.getMLFCListener());
                }
                else
                {
                    // Log.debug("DOMActivate: " + node);
                    cancelled = ((EventTarget) node).dispatchEvent(EventFactory
                            .createEvent("DOMActivate"));
                    if (cancelled)
                    {
                        return;
                    }
                    if (mlfc.getMLFCListener().getIsTabbedGUI()
                            && EventUtil.isFollowAlternativeRequest(event))
                    {
                        Log.debug("Middle click");
                        mlfc.getMLFCListener().openInNewTab(new XLink(url), null);
                        // mlfc.getMLFCListener().openInNewTab(new
                        // XLink(mlfc.getXMLDocument().getXMLURL())),
                        // location);
                    }
                    else
                    {
                        // TODO: do this only for HTML family!
                        Attr target = (Attr) node.getAttributes().getNamedItem("target");
                        if (target != null)
                        {
                            rootContainer.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                            mlfc.getMLFCListener().openLocationTop(new XLink(url.toString()),
                                    target.getNodeValue());
                        }
                        else
                        {
                            // Open link in same window
                            if (url.sameFile(mlfc.getXMLDocument().getXMLURL()))
                            {
                                // ((XMLCSSMLFC)
                                // mlfc).setScrollBar(url.getRef()); // removed
                                // for j9 test
                                mlfc.getXMLDocument().getBrowser().getGUIManager().getCurrentGUI()
                                        .setLocation(url.toString());
                            }
                            else
                            {
                                rootContainer.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                                mlfc.getMLFCListener().openLocation(url);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.error(e);
            }
        }
    }

    
    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent event)
    {
        Point pt = new Point(event.getX(), event.getY());
        View view = rootView.getViewAtPoint(pt);
        mouseClicked(view, event);
    }

    /*
     * public void elementActivate(Element e, Dimension coord, Object evt) { }
     */

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent arg0)
    {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent arg0)
    {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent event)
    {
        Point pt = new Point(event.getX(), event.getY());
        View view = rootView.getViewAtPoint(pt);

        event.consume();
        if (view == null || view instanceof RootView)
            return;
        Node node = null;
        node = view.getDOMElement();
        while (node == null)
        {
            view = view.getParentView();
            node = view.getDOMElement();
        }
        // Log.debug("click: " + node);
        org.w3c.dom.events.MouseEvent mEvent =
            (org.w3c.dom.events.MouseEvent)EventFactory.createEvent("mousedown");
        mEvent.initMouseEvent("mousedown", true, true, null, event.getClickCount(),
                0, 0, event.getX(),
                event.getY(), event.isControlDown(), event.isAltDown(),
                event.isShiftDown(), event.isMetaDown(),
                (short)(event.getButton() - 1), null);
        boolean cancelled = ((EventTarget) node).dispatchEvent(mEvent);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent event)
    {
        Point pt = new Point(event.getX(), event.getY());
        View view = rootView.getViewAtPoint(pt);

        event.consume();
        if (view == null || view instanceof RootView)
            return;
        Node node = null;
        node = view.getDOMElement();
        while (node == null)
        {
            view = view.getParentView();
            node = view.getDOMElement();
        }
        // Log.debug("click: " + node);
        org.w3c.dom.events.MouseEvent mEvent =
            (org.w3c.dom.events.MouseEvent)EventFactory.createEvent("mouseup");
        mEvent.initMouseEvent("mouseup", true, true, null, event.getClickCount(),
                0, 0, event.getX(),
                event.getY(), event.isControlDown(), event.isAltDown(),
                event.isShiftDown(), event.isMetaDown(),
                (short)(event.getButton() - 1), null);
        boolean cancelled = ((EventTarget) node).dispatchEvent(mEvent);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
     */
    public void mouseDragged(MouseEvent arg0)
    {
        // TODO Auto-generated method stub

    }

    /**
     * THESE METHODS ARE FOR LAYOUTING THE CONTAINER WHEN THE PARENT SIZE
     * CHANGES
     */

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.LayoutManager#addLayoutComponent(java.lang.String,
     *      java.awt.Component)
     */
    public void addLayoutComponent(String arg0, Component arg1)
    {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.LayoutManager#removeLayoutComponent(java.awt.Component)
     */
    public void removeLayoutComponent(Component arg0)
    {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.LayoutManager#preferredLayoutSize(java.awt.Container)
     */
    public Dimension preferredLayoutSize(Container arg0)
    {
        return size;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.LayoutManager#minimumLayoutSize(java.awt.Container)
     */
    public Dimension minimumLayoutSize(Container arg0)
    {
        return size;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.LayoutManager#layoutContainer(java.awt.Container)
     */
    public void layoutContainer(Container arg0)
    {
        // TODO Auto-generated method stub
        if (this.layoutState!=this.LAYOUT_IN_PROGRESS)
            this.layoutRenderer();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.PopupHandler#showAsPopup(fi.hut.tml.xsmiles.dom.StylableElement)
     */
    public void showAsPopup(StylableElement elem, String containerType)
    {
        // TODO Auto-generated method stub
        this.createWindow(elem, "block", containerType);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.PopupHandler#closePopup(fi.hut.tml.xsmiles.dom.StylableElement)
     */
    public void closePopup(StylableElement elem)
    {
        // TODO Auto-generated method stub
        ExternalWindow win = (ExternalWindow) ((org.apache.xerces.dom.NodeImpl) elem)
                .getUserData("window");
        if (win != null)
            win.dispose();

    }
}
