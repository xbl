/*
 * CSSBackgroundImage.java
 *
 * Created on 10 February 2004, 14:54
 */

package fi.hut.tml.xsmiles.csslayout;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.net.MalformedURLException;
import java.net.URL;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;
/**
 * 
 * @author Alessandro
 */
public class CSSBackgroundImage
{
    Image image;
    MediaTracker tracker;
    ImageObserver imageObserver;
    CSSRenderer renderer;
    CSSStyleDeclaration style;
    View view;

    int width, height;
    float HorizontalBPFactor;
    float VerticalBPFactor;
    float CSSPosX;
    float CSSPosY;

    boolean repeat, repeat_x, repeat_y;
    boolean isBGPropertySet = false;

    static final short FIRST = 0;
    static final short UP = 1;
    static final short RIGHT = 2;
    static final short LEFT = 3;
    static final short DOWN = 4;

    Component svgComp;

    /** Creates a new instance of CSSBackgroundImage */
    public CSSBackgroundImage(CSSStyleDeclaration s, CSSRenderer r, View view)
    {
        renderer = r;
        style = s;
        this.view = view;
        URL url = null;
        if (style.getPropertyCSSValue(CSS_Property.BACKGROUND_IMAGE) != null)
        {
            CSSValue value = style.getPropertyCSSValue(CSS_Property.BACKGROUND_IMAGE);
            if (value != null)
            {
                try
                {
                    if (value instanceof XSmilesCSSValueImpl)
                    {
                        XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl) value;
                        if (xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_URI)
                        {
                            try
                            {
                                url = new URL(xValue.getStringValue());
                            }
                            catch (MalformedURLException mue)
                            {
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.error(e);
                }
                if (url.toString().endsWith(".svg"))
                {
                    // image =
                    // CompatibilityFactory.getCompatibility().getSVGImage(url.toString());
                    // if (image != null)
                    // this.setBackgroundProperty();
                    svgComp = CompatibilityFactory.getGraphicsCompatibility().getSVGComponent(r.mlfc, url, view);
                    //r.getComponent().add(svgComp);
                    svgComp.setLocation(0, 0);
                    svgComp.setSize(200, 300);
                    svgComp.setVisible(true);
                }
                else
                {
                    image = Toolkit.getDefaultToolkit().getImage(url);
                    tracker = new MediaTracker(renderer.getComponent());
                    imageObserver = renderer.getComponent();
                    if (image != null)
                        loadImage(image);
                }
            }
        }
    }

    void loadImage(Image image)
    {

        tracker.addImage(image, 1);
        try
        {
            tracker.waitForID(1, 0);
        }
        catch (InterruptedException e)
        {
            Log.error("INTERRUPTED while loading BackgroundImage " + e);
        }
        // loadStatus = tracker.statusID(id, false);
        tracker.removeImage(image, 1);

        width = image.getWidth(imageObserver);
        height = image.getHeight(imageObserver);

        this.setBackgroundProperty();
        Log.debug("Size of the image: " + width + ", " + height);
    }
    
    public void setImage(Image img)
    {
        image = img;
        if (!isBGPropertySet)
            setBackgroundProperty();
    }

    private void setBackgroundProperty()
    {
        int imgOrigW=image.getWidth(imageObserver);
        int imgOrigH=image.getHeight(imageObserver);

        width = ((int) StyleGenerator.getImageSize(imgOrigW));
        height = ((int) StyleGenerator.getImageSize(imgOrigH));
//        Log.debug("width: " + width + "height:" + height);
        if (width > 0 || height > 0)
        {
        	if (width!=imgOrigW||height!=imgOrigH) image = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);

            CSSValue value = style.getPropertyCSSValue(CSS_Property.BACKGROUND_POSITION);
            if (value != null)
            {

                HorizontalBPFactor = 0.0f;
                VerticalBPFactor = 0.0f;
                CSSPosX = 0;
                CSSPosY = 0;

                if (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)
                {
                    HorizontalBPFactor = getFloatBackPosition(value, (short) 1);
                    VerticalBPFactor = getFloatBackPosition(value, (short) 2);

                }
                else if (value.getCssValueType() == CSSValue.CSS_VALUE_LIST)
                {
                    CSSValueList vlValue = (CSSValueList) value;
                    if (vlValue.getLength() == 2)
                    {
                        value = vlValue.item(0);
                        if (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)
                        {
                            CSSPrimitiveValue pv = (CSSPrimitiveValue) value;

                            if (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE)
                            {
                                HorizontalBPFactor = getFloatBackPosition(value, (short) 1);
                                VerticalBPFactor = getFloatBackPosition(vlValue.item(1), (short) 2);

                            }
                            else if (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT)
                            {
                                String sValue = pv.getStringValue();
                                if (sValue.indexOf("left") != -1 || (sValue.indexOf("right")) != -1)
                                {
                                    HorizontalBPFactor = getFloatBackPosition(value, (short) 1);
                                    VerticalBPFactor = getFloatBackPosition(vlValue.item(1), (short) 2);
                                }
                                else if (sValue.indexOf("top") != -1
                                        || (sValue.indexOf("bottom")) != -1)
                                {
                                    VerticalBPFactor = getFloatBackPosition(value, (short) 2);
                                    HorizontalBPFactor = getFloatBackPosition(vlValue.item(1),
                                            (short) 1);
                                }
                                else if (sValue.indexOf("center") != -1)
                                {
                                    value = vlValue.item(1);
                                    if (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)
                                    {
                                        CSSPrimitiveValue pv2 = (CSSPrimitiveValue) value;
                                        String s2Value = pv2.getStringValue();
                                        if (s2Value.indexOf("left") != -1
                                                || (s2Value.indexOf("right")) != -1)
                                        {
                                            HorizontalBPFactor = getFloatBackPosition(value, (short) 1);
                                            VerticalBPFactor = getFloatBackPosition(value, (short) 2);
                                        }
                                        else if (s2Value.indexOf("top") != -1
                                                || (s2Value.indexOf("bottom")) != -1)
                                        {
                                            VerticalBPFactor = getFloatBackPosition(value, (short) 2);
                                            HorizontalBPFactor = getFloatBackPosition(value, (short) 1);
                                        }
                                        else if (s2Value.indexOf("center") != -1)
                                        {
                                            VerticalBPFactor = getFloatBackPosition(value, (short) 2);
                                            HorizontalBPFactor = getFloatBackPosition(value, (short) 1);
                                        }
                                    }
                                }
                            }
                            else if ((pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PX)
                                    | (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_CM)
                                    | (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_MM)
                                    | (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_IN)
                                    | (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PT)
                                    | (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PC))
                            {

                                CSSPosX = StyleGenerator.getMeasure(value);
                                CSSPosY = StyleGenerator.getMeasure(vlValue.item(1));
                            }
                        }
                    }
                }
            }

            value = style.getPropertyCSSValue(CSS_Property.BACKGROUND_REPEAT);
            if (value != null)
            {
                String xStringValue = new String();
                if (value instanceof XSmilesCSSValueImpl)
                {
                    XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl) value;
                    if (xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT)
                    {
                        if ((xValue.getStringValue()).equals("repeat"))
                        {
                            repeat = true;
                        }
                        if ((xValue.getStringValue()).equals("repeat-x"))
                        {
                            repeat_x = true;
                        }
                        if ((xValue.getStringValue()).equals("repeat-y"))
                        {
                            repeat_y = true;
                        }
                    }
                }
            }
        }
        isBGPropertySet = true;
    }

    private float getFloatBackPosition(CSSValue v, short flag)
    {

        float floatValue = 0.5f;

        if (v.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)
        {
            CSSPrimitiveValue pv = (CSSPrimitiveValue) v;
            // System.out.println("**********"+pv.getPrimitiveType());
            if (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE)
            {
                floatValue = (pv.getFloatValue(CSSPrimitiveValue.CSS_PERCENTAGE)) / 100;
            }
            else if (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT)
            {
                if (flag == 1)
                {
                    if ((pv.getStringValue()).equals("left"))
                    {
                        floatValue = 0.0f;
                    }
                    else if ((pv.getStringValue()).equals("right"))
                    {
                        floatValue = 1.0f;
                    }
                    else if ((pv.getStringValue()).equals("center"))
                    {
                        floatValue = 0.5f;
                    }
                }
                else if (flag == 2)
                {
                    if ((pv.getStringValue()).equals("top"))
                    {
                        floatValue = 0.0f;
                    }
                    else if ((pv.getStringValue()).equals("bottom"))
                    {
                        floatValue = 1.0f;
                    }
                    else if ((pv.getStringValue()).equals("center"))
                    {
                        floatValue = 0.5f;
                    }
                }
            }
        }
        return floatValue;
    }

    public void paint(Graphics g, float x, float y, float w, float h)
    {
        Rectangle clip = g.getClipBounds();
        g.clipRect((int) x, (int) y, (int) w, (int) h);
        if (image != null)
        {
            float xPosition = (int) (x + CSSPosX + w * HorizontalBPFactor - (float) width
                    * HorizontalBPFactor);
            float yPosition = (int) (y + CSSPosY + h * VerticalBPFactor - (float) height
                    * VerticalBPFactor);
            this.paintImage(g, clip, x, y, w, h, FIRST, xPosition, yPosition);
        }
        g.setClip(clip.x, clip.y, clip.width, clip.height);
    }

    public void paintImage(Graphics g, Rectangle clip, float x, float y, float w, float h,
            short direction, float xPosition, float yPosition)
    {

        if (!((xPosition + width <= x) || (yPosition + height <= y) || (xPosition >= x + w) || (yPosition >= y
                + h)))
        {
            if (direction == FIRST)
            {
                this.drawImage(g, clip, xPosition, yPosition);
                if (repeat)
                {
                    this.paintImage(g, clip, x, y, w, h, UP, xPosition, (yPosition - height));
                    this.paintImage(g, clip, x, y, w, h, LEFT, (xPosition - width), yPosition);
                    this.paintImage(g, clip, x, y, w, h, RIGHT, (xPosition + width), yPosition);
                    this.paintImage(g, clip, x, y, w, h, DOWN, xPosition, (yPosition + height));
                }
                else if (repeat_x)
                {
                    this.paintImage(g, clip, x, y, w, h, LEFT, (xPosition - width), yPosition);
                    this.paintImage(g, clip, x, y, w, h, RIGHT, (xPosition + width), yPosition);
                }
                else if (repeat_y)
                {
                    this.paintImage(g, clip, x, y, w, h, UP, xPosition, (yPosition - height));
                    this.paintImage(g, clip, x, y, w, h, DOWN, xPosition, (yPosition + height));
                }
            }
            else if (direction == UP)
            {
                this.drawImage(g, clip, xPosition, yPosition);
                if (repeat)
                {
                    this.paintImage(g, clip, x, y, w, h, UP, xPosition, (yPosition - height));
                    this.paintImage(g, clip, x, y, w, h, LEFT, (xPosition - width), yPosition);
                    this.paintImage(g, clip, x, y, w, h, RIGHT, (xPosition + width), yPosition);
                }
                else if (repeat_y)
                {
                    this.paintImage(g, clip, x, y, w, h, UP, xPosition, (yPosition - height));
                }
            }
            else if (direction == LEFT)
            {
                this.drawImage(g, clip, xPosition, yPosition);
                if (repeat || repeat_x)
                {
                    this.paintImage(g, clip, x, y, w, h, LEFT, (xPosition - width), yPosition);
                }
            }
            else if (direction == RIGHT)
            {
                this.drawImage(g, clip, xPosition, yPosition);
                if (repeat || repeat_x)
                {
                    this.paintImage(g, clip, x, y, w, h, RIGHT, (xPosition + width), yPosition);
                }
            }
            else if (direction == DOWN)
            {
                this.drawImage(g, clip, xPosition, yPosition);
                if (repeat)
                {
                    this.paintImage(g, clip, x, y, w, h, LEFT, (xPosition - width), yPosition);
                    this.paintImage(g, clip, x, y, w, h, RIGHT, (xPosition + width), yPosition);
                    this.paintImage(g, clip, x, y, w, h, DOWN, xPosition, (yPosition + height));
                }
                else if (repeat_y)
                {
                    this.paintImage(g, clip, x, y, w, h, DOWN, xPosition, (yPosition + height));
                }
            }
        }
    }

    private void drawImage(Graphics g, Rectangle clip, float xPosition, float yPosition)
    {
        if (!((xPosition + width <= clip.x) || (yPosition + height <= clip.y)
                || (xPosition >= clip.x + clip.width) || (yPosition >= clip.y + clip.height)))
        {

            g.drawImage(image, (int) xPosition, (int) yPosition, imageObserver);
        }

    }

}
