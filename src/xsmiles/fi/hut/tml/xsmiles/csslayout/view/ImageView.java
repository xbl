/*
 * ImageView.java
 * 
 * Created on October 6, 2003, 3:38 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.net.URL;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.image.ImageObserver;

import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.w3c.dom.Node;

/**
 * @author mpohja
 */
public class ImageView extends BaseView
{
    Image image;
    MediaTracker tracker;
    ImageObserver imageObserver;

    /** Creates a new instance of ImageView */
    public ImageView(CSSRenderer renderer, Node node, View parent,
            String location)
    {
        super(renderer, node, parent);
        try
        {
            URL u = null;
            if (node instanceof XSmilesElementImpl)
            {
                u = ((XSmilesElementImpl) node).resolveURI(location);
                try
                {
                    // add this to the list of resources, so that XML signature can get all references
                    Resource r = new Resource(u,Resource.RESOURCE_IMAGE,null);
                    ((XSmilesElementImpl) node).getResourceReferencer().addResource(r);
                } catch (Exception e)
                {
                    Log.error(e,"while trying to image URLadd to reference list");
                }
            }
            else
            {
                u = new URL(location);
            }
            image = Toolkit.getDefaultToolkit().getImage(u);
        }
        catch (Exception e)
        {
            Log.error(e);
        }
        tracker = new MediaTracker(renderer.getComponent());
        imageObserver = renderer.getComponent();
        if (image != null)
            loadImage(image);
        
        int width, height;
        int imgOrigW=image.getWidth(imageObserver);
        int imgOrigH=image.getHeight(imageObserver);
        width = ((int) StyleGenerator.getImageSize(imgOrigW));
        height = ((int)StyleGenerator.getImageSize(imgOrigH));
        width=(width==0?1:width);
        height=(height==0?1:height);
        setContentWidth(width);
        setContentHeight(height);
        if (width!=imgOrigW||height!=imgOrigH)image = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        
        // Scale the image if width or height is defined in ss.
        if (dimensions.isWidthDefined() || dimensions.isHeightDefined())
        {
       //     int width, height;
            if (dimensions.isWidthDefined())
                width = dimensions.getMaxContentSize(View.X_AXIS);
            else
                width = -1;
            
            if (dimensions.isHeightDefined())
                height = dimensions.getMaxContentSize(View.Y_AXIS);
            else
                height = -1;
            
            image = image.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        }
    }

    /**
     * Loads the image, returning only when the image is loaded.
     * 
     * @param image
     *            the image
     */
    void loadImage(Image image)
    {
        tracker.addImage(image, 1);
        try
        {
            tracker.waitForID(1, 0);
        }
        catch (InterruptedException e)
        {
            Log.error("INTERRUPTED while loading Image " + e);
        }
        //loadStatus = tracker.statusID(id, false);
        tracker.removeImage(image, 1);
    }

    public void paint(Graphics g)
    {
        this.paintBorder(g);
        if (image != null && visible)
            g.drawImage(image, this.getDimensions().getContentOrigin(
                    View.X_AXIS), this.getDimensions().getContentOrigin(
                    View.Y_AXIS), renderer.getComponent());
        super.paint(g);
    }
    public void setSizeRequirements()
    {
        int width = dimensions.getMaxContentSize(View.X_AXIS);
        int height = dimensions.getMaxContentSize(View.Y_AXIS);
        sizeRequirements.setMaximumSpan(new Dimension(width, height));
        sizeRequirements.setPreferredSpan(new Dimension(width, height));
        sizeRequirements.setMinimumSpan(new Dimension(width, height));
    }
}