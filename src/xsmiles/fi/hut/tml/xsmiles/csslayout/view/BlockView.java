/*
 * BlockView.java
 * 
 * Created on September 17, 2003, 6:35 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.util.Vector;
import java.util.Enumeration;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Color;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.CSSBorder;

import org.w3c.dom.Node;

/**
 * @author honkkis
 */
public class BlockView extends BaseView
{

    /** Creates a new instance of BlockView */
    public BlockView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        super.doLayout();
        int y = 0;//(int)this.getPreferredSpan(View.Y_AXIS);
        int x = 0;//(int)this.getPreferredSpan(View.X_AXIS);
        if (childViews != null && childViews.size() > 0)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                BaseView v = (BaseView) e.nextElement();
                if (v.hasAbsolutePosition())
                {
                    v.setAbsoluteCoordinates();
                    if (this instanceof RootView)
                    {
                        x = Math.max(x, (v.getAbsolutePositionX() + v.getViewWidth()));
                        y = Math.max(y, (v.getAbsolutePositionY() + v.getViewHeight()));
                    }
                    v.doLayout();
                }
                else
                {
                    v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS), this
                            .getDimensions().getContentOrigin(View.Y_AXIS)
                            + y);
                    v.doLayout();
                    y = y + v.getViewHeight();
                    x = Math.max(x, v.getViewWidth());
                    if (v.hasRelativePosition())
                        v.handleRelativeView();
                }
            }
            if (!this.hasAbsolutePosition() || (getViewWidth() == 0 && getViewHeight() == 0))
            {
                // Block's width should be as wide as possible except if
                // it's inside table or it has relative width. Note: if view has
                // absolute width, the dimension change is denied by
                // ViewDimensions.
                if (getParentView() != null && !this.isInTable() && !hasRelativeWidth)
                {
                    int max = ((BaseView) getParentView()).getMaximumSpan(View.X_AXIS)
                            - this.getDimensions().getOuterSize(View.X_AXIS);
                    x = Math.max(x, max);
                }
                setContentWidth(x);
                setContentHeight(y);
            }
            // TODO: For absolute position with height = 0
            if (this.getViewHeight() == 0)
                setContentHeight(y);
        }
        else
        {
            setContentWidth(0);
            setContentHeight(0);
        }
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (visible)
        {
            this.paintBackground(g);
            this.paintBorder(g);
            if (childViews != null)
            {
                Rectangle clip = g.getClipBounds();
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    View v = (View) e.nextElement();
                    if (insideClip(clip, v))
                    {
                        v.paint(g);
                    }
                }
            }
            super.paint(g);
        }
    }

    public static boolean insideClip(Rectangle clip, View v)
    {
        //return clip.contains(v.getRectangle());
        Rectangle rect = v.getRectangle();
        return clip.intersects(rect);
    }

}