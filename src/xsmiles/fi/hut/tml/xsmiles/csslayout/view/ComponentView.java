/*
 * BlockView.java
 * 
 * Created on September 17, 2003, 6:35 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.dom.VisualComponentService;

/**
 * @author honkkis
 */
public class ComponentView extends BaseView
{
    protected VisualComponentService visual;

    /** Creates a new instance of BlockView */
    public ComponentView(CSSRenderer r, Node node, VisualComponentService visComp,View parent)
    {
        super(r, node, parent);
        visual=visComp;
        //this.container = r.getComponent();
        this.setComponent();
    }
    
    /** @return the container where this component should be added. */
    protected Container getContainer()
    {
        // tries to find a container view in the hierarchy, but if it does not, it will return the main container
        View pview = this.getParentView();
        while (pview!=null)
        {
            if (pview instanceof ContainerView)
            {
                ContainerView scrollv = (ContainerView)pview;
                if (scrollv.hasContainer())
                    return scrollv.getContainerForDescendants();
            }
            pview=pview.getParentView();
        }
        return this.getCSSRenderer().getComponent();
    }

    public void setComponent()
    {
        if (visual!=null)
        {
            // when this function is called, the style is usually changed, so let's notify the component as well
            visual.visualEvent(VisualComponentService.EVENT_STYLECHANGED, null);
            Component component = visual.getComponent();
            if (component != null)
            {
                Dimension dim = component.getSize();
                setContentHeight(dim.height);
                setContentWidth(dim.width);
            }
        }
    }

    public void doLayout()
    {
        super.doLayout();
        Component component = (visual==null?null:visual.getComponent());
        if (component == null)
        {
            return;
        }
        if (component.getLocation().x != this.getDimensions().getContentOrigin(View.X_AXIS)
                || component.getLocation().y != this.getDimensions().getContentOrigin(View.Y_AXIS))
            component.setLocation(this.getDimensions().getContentOrigin(View.X_AXIS), this
                    .getDimensions().getContentOrigin(View.Y_AXIS));
        if (component.getParent() != this.getContainer())
        {
            component.setVisible(true);
            //Log.debug("Adding component: "+component);
            this.getContainer().add(component, 0);
        }

    }
    
    public void viewRemoved()
    {
        Component component = (visual==null?null:visual.getComponent());
        if (component != null && component.getParent() != null)
        {
            //component.getParent().remove(component);
            this.getCSSRenderer().removeComponentDelayed(component);
        }
    }
/*
    public void removeComponent()
    {
        // hack to reduce blinking
        //this.renderer.addingRemovingComponents();
        Log.debug("Removing component: " + component.getClass());
        if (component != null && component.getParent() != null)
            component.getParent().remove(component);
    }
*/
    /** paints this view */
    public void paint(Graphics g)
    {
        //        component.setLocation(this.posX, this.posY);
        super.paint(g);
    }

    public void setSizeRequirements()
    {
        Component component = (visual==null?null:visual.getComponent());
        if (component != null)
        {
            Dimension dim = component.getSize();
            sizeRequirements.setMaximumSpan(dim);
            sizeRequirements.setPreferredSpan(dim);
            sizeRequirements.setMinimumSpan(dim);
        }
    }
}