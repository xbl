/*
 * InlineView.java
 * 
 * Created on December 11, 2003, 11:53 AM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.util.Enumeration;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

/**
 * This view is for inline elements, who don't have their own view (e.g. links).
 * Note: text, images, and components have their own view.
 * 
 * @author mpohja
 */
public class InlineView extends BlockView
{

    /** Creates a new instance of InlineView */
    public InlineView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        if (parent != null)
        {
            setMaxContentSize();
        }
        int y = 0;//(int)this.getPreferredSpan(View.Y_AXIS);
        int x = 0;//(int)this.getPreferredSpan(View.X_AXIS);
        if (childViews != null && childViews.size() > 0)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                BaseView v = (BaseView) e.nextElement();
                v.setAbsolutePosition(this.getDimensions()
                                .getContentOrigin(View.X_AXIS)
                                + x, this.getDimensions().getContentOrigin(
                                View.Y_AXIS));
                v.doLayout();

                y = Math.max(y, v.getViewHeight());
                x = x + v.getViewWidth();
                if (v.hasRelativePosition())
                    v.handleRelativeView();
            }
            setContentWidth(x);
            setContentHeight(y);
        }
        else
        {
            setContentWidth(0);
            setContentHeight(0);
        }
    }

    public void calculateSize()
    {
        int width = 0, height = 0;
        View view;
        if (childViews != null)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                view = (View) e.nextElement();
                if (view instanceof InlineView)
                {
                    ((InlineView) view).calculateSize();
                }
                width += view.getViewWidth();
                height += view.getViewHeight();
            }
        }
        setContentWidth(width);
        setContentHeight(height);
    }

}