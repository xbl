/*
 * BaseView.java
 *
 * Created on September 17, 2003, 6:34 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import fi.hut.tml.xsmiles.csslayout.SizeRequirements;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Vector;

import org.w3c.dom.Node;
import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

/**
 *
 * @author  honkkis
 */
public interface View
{
    public static final int X_AXIS = 1;
    public static final int Y_AXIS = 10;
    
    /** paints this view */
    public void paint(Graphics g);
    
    /** get minimum span of this view that corresponds to an axis */
    public int getMinimumSpan(int axis);
    
    /** get maximum span of this view that corresponds to an axis */
    public int getMaximumSpan(int axis);

    /** get maximum span of this view that corresponds to an axis */
    public int getPreferredSpan(int axis);
    
    public int getViewWidth();
    
    public int getViewHeight();
    
    public CSSStyleDeclaration getStyle();
    
    /** set the absolute position of this view, only the parent is allowed to call this */
    public void setAbsolutePosition(int x, int y);
    public int getAbsolutePositionX();
    public int getAbsolutePositionY();
    public Rectangle getRectangle();
    /**
     * Returns true if view is absolute positioned.
     */
    public boolean hasAbsolutePosition();
    
    public View getParentView();
    public void setParentView(View parent);
    
    public Node getDOMElement();
    
    public Vector getChildren();
    /** this method is called when a view is removed from the hierarchy.
     * for instance, components can then be removed from the containers
     *
     */
    public void viewRemoved();

    
    
    /** do the layout for this and its children */
    public void doLayout();

    public void addChildView(View v);
    
    public void removeFromTree();
    
    public void removeChild(View child);
    
    public void removeChildren();
    
    public void repaintDocument();
    
    public void repaintDocument(View view);
    
    public void repaintView();
    
    public void createChildViews(Node parent);
    
    public View createChildView(Node child, Node parent);
    
    public void setSizeRequirements();
    
    public void setStyle(CSSStyleDeclaration s);
    
    public SizeRequirements getSizeRequirements();
    
    public View getFirstChild();
 
    public CSSRenderer getCSSRenderer();

    public void repaint(Image img);
    
	/**
	 * This is called by XSmilesVisualElement when a node is removed from the DOM,
	 * let's remove all components from view
	 * @param removed
	 */
	//public void nodeRemoved(Node removed); //now viewRemoved 
    
    public boolean hasAbsoluteChildren();
}
