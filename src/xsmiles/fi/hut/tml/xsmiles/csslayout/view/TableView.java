/*
 * TableView.java
 * 
 * Created on January 14, 2004, 12:05 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.ColumnSpanSizeRequirements;
import fi.hut.tml.xsmiles.csslayout.SizeRequirements;

/**
 * @author mpohja
 */
public class TableView extends BaseView
{
    private SizeRequirements[] columnRequirements;
    // SizeRequirements totalColumnRequirements;
    private int[] columnSpans;
    private Vector columnSpanRequirements = new Vector();

    /** Creates a new instance of TableView */
    public TableView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
    }

    public View createView(Node node, String display)
    {
        View view = null;
        if (node.getNodeType() == Node.ELEMENT_NODE && (display.equals("table-row")))
        {
            view = new RowView(renderer, node, this);
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && (display.equals("table-cell")))
        {
            view = new CellView(renderer, node, this);
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && isRowGroup(display))
        {
            view = new RowGroupView(renderer, node, this);
        }
        else
            view = super.createView(node, display);
        return view;
    }

    protected static boolean isRowGroup(String display)
    {
        return ((display.equals("table-row-group") || display.equals("table-header-group") || display
                .equals("table-footer-group")));
    }

    protected void addView(Node node, View view, View parentView, String display)
    {
        if (display.equals("table-row"))
        {
            parentView.addChildView(view);
            if (view instanceof RowView)
                view.createChildViews(node);
        }
        else
            super.addView(node, view, parentView, display);
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        try
        {
            setSizeRequirements();
            super.doLayout();
            calculateColumnRequirements();
            calculateColumnSizes();

            int y = 0;// (int)this.getPreferredSpan(View.Y_AXIS);
            int x = 0;// (int)this.getPreferredSpan(View.X_AXIS);
            if (childViews != null && childViews.size() > 0)
            {
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    BaseView v = (BaseView) e.nextElement();
                    if (v.hasAbsolutePosition())
                    {
                        v.setAbsoluteCoordinates();
                        y = Math.max(y, (v.getAbsolutePositionY() + v.getViewHeight()));
                        x = Math.max(x, (v.getAbsolutePositionX() + v.getViewWidth()));
                        v.doLayout();
                    }
                    else
                    {
                        v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS), this
                                .getDimensions().getContentOrigin(View.Y_AXIS)
                                + y);
                        v.doLayout();
                        y = y + v.getViewHeight();
                        x = Math.max(x, v.getViewWidth());
                        if (v.hasRelativePosition())
                            v.handleRelativeView();
                    }
                }
                if (!this.hasAbsolutePosition())
                {
                    // Add paddings
                    // x = x + this.getDimensions().getXPadding();
                    // Block's width should be as wide as possible.
                    // if (getParentView() != null)
                    // x = ((BaseView)
                    // getParentView()).getMaximumSpan(View.X_AXIS);
                    setContentWidth(x);
                    setContentHeight(y);
                }
            }
            else
            {
                setContentWidth(0);
                setContentHeight(0);
            }
            if (childViews != null)
            {
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    BaseView row = (BaseView) e.nextElement();
                    Enumeration rows = row.childViews.elements();
                    while (rows.hasMoreElements())
                    {
                        BaseView v = (BaseView) rows.nextElement();
                        v.setContentHeight(row.getViewHeight());
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.error(e);
        }
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (childViews != null && visible)
        {
            Rectangle clip = g.getClipBounds();
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                View v = (View) e.nextElement();
                if (BlockView.insideClip(clip, v))
                {
                    v.paint(g);
                }
            }
        }
        super.paint(g);
    }

    private void calculateColumnRequirements()
    {
        int maxColumns = 0, columns = 0;
        if (childViews != null)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                View row = (View) e.nextElement();
                if (row instanceof RowGroupView)
                {
                    if (row.getChildren().size() > 0)
                        columns = ((View) row.getChildren().elementAt(0)).getChildren().size();
                }
                else
                    columns = row.getChildren().size();
                maxColumns = Math.max(maxColumns, columns);
            }
            columnSpans = new int[maxColumns];
            columnRequirements = new SizeRequirements[maxColumns];

            Enumeration children = childViews.elements();
            while (children.hasMoreElements())
            {
                View child = (View) children.nextElement();
                if (child instanceof RowGroupView)
                {
                    Enumeration rows = child.getChildren().elements();
                    while (rows.hasMoreElements())
                    {
                        View row = (View) rows.nextElement();
                        cellSizes(row.getChildren().elements());
                    }
                }
                else
                    cellSizes(child.getChildren().elements());
            }

            // Adjust reqs with column spans
            if (columnSpanRequirements.size() > 0)
            {
                Enumeration enume = columnSpanRequirements.elements();
                ColumnSpanSizeRequirements colSizeReqs;
                while (enume.hasMoreElements())
                {
                    colSizeReqs = (ColumnSpanSizeRequirements) enume.nextElement();
                    increaseColumnReqs(colSizeReqs);
                }
            }
        }
    }

    private void cellSizes(Enumeration cells)
    {
        int column = 0;
        while (cells.hasMoreElements())
        {
            View cell = (View) cells.nextElement();
            if (cell instanceof CellView)
            {
                // TODO: this is not necessary a cell always...
                if (((CellView) cell).getColSpan() == 1)
                    updateColumnRequirements(cell.getSizeRequirements(), column);
                else
                {
                    if (columnRequirements[column] == null)
                        columnRequirements[column] = new SizeRequirements();

                    updateColumnSpanRequirements(((CellView) cell), column);
                }
            }
            column++;
        }
    }

    private void updateColumnRequirements(SizeRequirements reqs, int column)
    {
        if (columnRequirements[column] == null)
            columnRequirements[column] = new SizeRequirements();

        // Adding 10 pixels to max for smoother layout
        if (columnRequirements[column].getMaximumSpan(View.X_AXIS) < reqs.getMaximumSpan(View.X_AXIS) + 10)
            columnRequirements[column].setMaximumSpan(new Dimension(
                    reqs.getMaximumSpan(View.X_AXIS) + 10, 1));

        if (columnRequirements[column].getMinimumSpan(View.X_AXIS) < reqs.getMinimumSpan(View.X_AXIS))
            columnRequirements[column]
                    .setMinimumSpan(new Dimension(reqs.getMinimumSpan(View.X_AXIS), 1));
    }

    private void updateColumnSpanRequirements(CellView cell, int column)
    {
        ColumnSpanSizeRequirements spanReqs = new ColumnSpanSizeRequirements(column, cell.getColSpan());
        spanReqs.setMaximumSpan(new Dimension(
                cell.getSizeRequirements().getMaximumSpan(View.X_AXIS) + 10, 1));
        spanReqs
                .setMinimumSpan(new Dimension(cell.getSizeRequirements().getMinimumSpan(View.X_AXIS), 1));
        columnSpanRequirements.add(spanReqs);
    }

    /**
     * Cells, which span on multiple columns may require larger size than
     * single column cells. This method updates columns size requirements if needed.
     * 
     * @param span
     */
    private void increaseColumnReqs(ColumnSpanSizeRequirements span)
    {
        int firstColumn, colCount;
        firstColumn = span.getFirstColumn();
        colCount = span.getColumnCount();
        int max = 0, min = 0;
        for (int i = firstColumn; i < firstColumn + colCount; i++)
        {
            try
            {
                max += columnRequirements[i].getMaximumSpan(View.X_AXIS);
                min += columnRequirements[i].getMinimumSpan(View.X_AXIS);
            }
            catch (ArrayIndexOutOfBoundsException aiobe)
            {
                Log.error("Table has td element with colspan=\""+colCount+"\" which is bigger than the total amount of columns. "+aiobe);
                colCount = i - 1;
            }
        }

        int diff = 0, portion = 0;
        if (span.getMaximumSpan(View.X_AXIS) > max && colCount > 0)
        {
            diff = span.getMaximumSpan(View.X_AXIS) - max;
            portion = diff / colCount;
            for (int i = firstColumn; i < firstColumn + colCount; i++)
            {
                columnRequirements[i].increaseMaximumSpan(View.X_AXIS, portion);
            }
        }
        if (span.getMinimumSpan(View.X_AXIS) > min && colCount > 0)
        {
            diff = span.getMinimumSpan(View.X_AXIS) - min;
            portion = diff / colCount;
            for (int i = firstColumn; i < firstColumn + colCount; i++)
            {
                columnRequirements[i].increaseMinimumSpan(View.X_AXIS, portion);
            }
        }
    }

    private void calculateColumnSizes()
    {
        int columns, freeSpace, extra = 0, needForSpace = 0;
        if (columnRequirements == null || columnRequirements[0] == null)
            columns = 0;
        else
            columns = Array.getLength(columnRequirements);
        int minWidth = 0;
        // Minimum width of the table.
        for (int i = 0; i < columns; i++)
        {
            minWidth += columnRequirements[i].getMinimumSpan(View.X_AXIS);
        }
        // if (minWidth < this.getMaximumSpan(View.X_AXIS))
        freeSpace = this.getMaximumSpan(View.X_AXIS) - minWidth;
        // freeSpace = this.dimensions.getMaxContentSize(View.X_AXIS) -
        // minWidth;

        // Space left per column.
        if (freeSpace > 0 && columns > 0)
            extra = freeSpace / columns;

        for (int i = 0; i < columns; i++)
        {
            if (columnRequirements[i].getMaximumSpan(View.X_AXIS) >= columnRequirements[i]
                    .getMinimumSpan(View.X_AXIS)
                    + extra)
            {
                columnSpans[i] = columnRequirements[i].getMinimumSpan(View.X_AXIS) + extra;
                freeSpace -= extra;
                needForSpace++;
            }
            else
            {
                columnSpans[i] = columnRequirements[i].getMaximumSpan(View.X_AXIS);
            }
        }

        // Repeat the for-loop to dish out the remaining space
        // for columns in need.
        if (freeSpace > 0 && needForSpace > 0)
            extra = freeSpace / needForSpace;
        else
            extra = 0;

        for (int i = 0; i < columns; i++)
        {
            if (columnRequirements[i].getMaximumSpan(View.X_AXIS) >= columnSpans[i] + extra)
            {
                columnSpans[i] += extra;
                freeSpace -= extra;
                needForSpace++;
            }
            else
            {
                columnSpans[i] = columnRequirements[i].getMaximumSpan(View.X_AXIS);
            }
        }

    }

    public int getColumnSpan(int column)
    {
        if (columnSpans.length > column)
            return columnSpans[column];
        else
            return 1;
    }

    public class RowGroupView extends BaseView
    {
        public RowGroupView(CSSRenderer r, Node node, View parent)
        {
            super(r, node, parent);
        }

        public View createView(Node node, String display)
        {
            View view = null;
            if (node.getNodeType() == Node.ELEMENT_NODE && (display.equals("table-row")))
            {
                view = new RowView(renderer, node, this);
            }
            else if (node.getNodeType() == Node.ELEMENT_NODE && (display.equals("table-cell")))
            {
                view = new CellView(renderer, node, this);
            }
            else if (node.getNodeType() == Node.ELEMENT_NODE && isRowGroup(display))
            {
                view = new RowGroupView(renderer, node, this);
            }
            else
                view = super.createView(node, display);
            return view;
        }

        /** do, or redo layout for this and children */
        public void doLayout()
        {
            try
            {
                // setSizeRequirements();
                super.doLayout();

                int y = 0;// (int)this.getPreferredSpan(View.Y_AXIS);
                int x = 0;// (int)this.getPreferredSpan(View.X_AXIS);
                if (childViews != null && childViews.size() > 0)
                {
                    Enumeration e = childViews.elements();
                    while (e.hasMoreElements())
                    {
                        BaseView v = (BaseView) e.nextElement();
                        if (v.hasAbsolutePosition())
                        {
                            v.setAbsoluteCoordinates();
                            y = Math.max(y, (v.getAbsolutePositionY() + v.getViewHeight()));
                            x = Math.max(x, (v.getAbsolutePositionX() + v.getViewWidth()));
                            v.doLayout();
                        }
                        else
                        {
                            v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS),
                                    this.getDimensions().getContentOrigin(View.Y_AXIS) + y);
                            v.doLayout();
                            y = y + v.getViewHeight();
                            x = Math.max(x, v.getViewWidth());
                        }
                    }
                    if (!this.hasAbsolutePosition())
                    {
                        // Add paddings
                        // x = x + this.getDimensions().getXPadding();
                        // Block's width should be as wide as possible.
                        // if (getParentView() != null)
                        // x = ((BaseView)
                        // getParentView()).getMaximumSpan(View.X_AXIS);
                        setContentWidth(x);
                        setContentHeight(y);
                    }
                }
                else
                {
                    setContentWidth(0);
                    setContentHeight(0);
                }
                if (childViews != null)
                {
                    Enumeration e = childViews.elements();
                    while (e.hasMoreElements())
                    {
                        BaseView row = (BaseView) e.nextElement();
                        Enumeration rows = row.childViews.elements();
                        while (rows.hasMoreElements())
                        {
                            BaseView v = (BaseView) rows.nextElement();
                            v.setContentHeight(row.getViewHeight());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.error(e);
            }
        }

        protected void addView(Node node, View view, View parentView, String display)
        {
            if (display.equals("table-row"))
            {
                parentView.addChildView(view);
                if (view instanceof RowView)
                    view.createChildViews(node);
            }
            else
                super.addView(node, view, parentView, display);
        }

        /** paints this view */
        public void paint(Graphics g)
        {
            if (childViews != null && visible)
            {
                Rectangle clip = g.getClipBounds();
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    View v = (View) e.nextElement();
                    if (BlockView.insideClip(clip, v))
                    {
                        v.paint(g);
                    }
                }
            }
            super.paint(g);
        }
    }

    public class RowView extends BaseView
    {
        /** Creates a new instance of RowView */
        public RowView(CSSRenderer r, Node node, View parent)
        {
            super(r, node, parent);
        }

        public View createView(Node node, String display)
        {
            View view = null;
            if (node.getNodeType() == Node.ELEMENT_NODE && (display.equals("table-cell")))
            {
                view = new CellView(renderer, node, this);
            }
            else
                view = super.createView(node, display);
            return view;
        }

        protected void addView(Node node, View view, View parentView, String display)
        {
            if (display.equals("table-cell"))
            {
                parentView.addChildView(view);
                view.createChildViews(node);
            }
            else
                super.addView(node, view, parentView, display);
        }

        /** do, or redo layout for this and children */
        public void doLayout()
        {
            super.doLayout();
            int y = 0;// (int)this.getPreferredSpan(View.Y_AXIS);
            int x = 0;// (int)this.getPreferredSpan(View.X_AXIS);
            if (childViews != null && childViews.size() > 0)
            {
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    BaseView v = (BaseView) e.nextElement();
                    v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS) + x, this
                            .getDimensions().getContentOrigin(View.Y_AXIS));
                    v.doLayout();
                    y = Math.max(y, v.getViewHeight());
                    x = x + v.getViewWidth();
                }
                setContentWidth(x);
                setContentHeight(y);
            }
            else
            {
                setContentWidth(0);
                setContentHeight(0);
            }
        }

        /** paints this view */
        public void paint(Graphics g)
        {
            if (childViews != null && visible)
            {
                Rectangle clip = g.getClipBounds();
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    View v = (View) e.nextElement();
                    if (BlockView.insideClip(clip, v))
                    {
                        v.paint(g);
                    }
                }
            }
            super.paint(g);
        }

    }

    public class CellView extends BlockView
    {

        int rowspan = 1, colspan = 1;

        /** Creates a new instance of CellView */
        public CellView(CSSRenderer r, Node node, View parent)
        {
            super(r, node, parent);

            String attr = ((Element) node).getAttribute("rowspan");
            int span;
            if (!attr.equals(""))
            {
                span = Integer.valueOf(attr).intValue();
                if (span > 1)
                    rowspan = span;
            }

            attr = ((Element) node).getAttribute("colspan");
            if (!attr.equals(""))
            {
                span = Integer.valueOf(attr).intValue();
                if (span > 1)
                    colspan = span;
            }
        }

        public int getColSpan()
        {
            return colspan;
        }

        public int getRowSpan()
        {
            return rowspan;
        }

        public void doLayout()
        {
            super.doLayout();
            int column = getParentView().getChildren().indexOf(this), width = 0;
            for (int i = 0; i < colspan; i++, column++)
            {
                width += getColumnSpan(column);
            }
            setViewWidth(width);
        }

        protected void setMaxContentSize()
        {
            int column = getParentView().getChildren().indexOf(this), width = 0;
            for (int i = 0; i < colspan; i++, column++)
            {
                width += getColumnSpan(column);
            }

            width = width - dimensions.getOuterSize(View.X_AXIS);

            int y = ((BaseView) parent).getMaximumSpan(View.Y_AXIS)
                    - dimensions.getOuterSize(View.Y_AXIS);

            sizeRequirements.setMaximumSpan(new Dimension(width, y));
        }
    }

}