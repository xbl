/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Feb 3, 2005
 */
package fi.hut.tml.xsmiles.csslayout.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

/**
 * @author mpohja
 */
public class FloatContainer extends BaseView
{
    int tempMaximumSpan = 0;
    /**
     * @param r
     * @param node
     * @param parent
     */
    public FloatContainer(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
        // TODO Auto-generated constructor stub
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        super.doLayout();
        int y = 0;//(int)this.getPreferredSpan(View.Y_AXIS);
        int x = (int) this.getMaximumSpan(View.X_AXIS);
        int childWidth;
        Enumeration origins;
        Location high, low;
        Vector originLocations = new Vector(6, 2);
        originLocations.add(new Location(this.getDimensions().getContentOrigin(View.X_AXIS), this
                .getDimensions().getContentOrigin(View.Y_AXIS), this.getMaximumSpan(View.X_AXIS)));
        if (childViews != null && childViews.size() > 0)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                BaseView v = (BaseView) e.nextElement();
                Location location = null;

                if (v.isLeftFloated() || v.isRightFloated() || v.getDimensions().widthDefined)
                {
                    while (!originLocations.isEmpty())
                    {
                        location = (Location) originLocations.get(0);
                        originLocations.remove(0);
                        if (location.getWidth() > v.getViewWidth())
                            break;
                    }
                }
                else
                {
                    location = (Location)originLocations.firstElement();
                    tempMaximumSpan = location.getWidth();
                }

                if (v.isRightFloated())
                {
                    v.setAbsolutePosition(location.getX() + location.getWidth() - v.getViewWidth(),
                            location.getY());
                    high = new Location(location.getX(), location.getY(), location.getWidth()
                            - v.getViewWidth());
                    low = new Location(location.getX(), location.getY() + v.getViewHeight(), location
                            .getWidth());
                }
                else
                {
                    v.setAbsolutePosition(location.getX(), location.getY());
                    high = new Location(location.getX() + v.getViewWidth(), location.getY(), location
                            .getWidth()
                            - v.getViewWidth());
                    low = new Location(location.getX(), location.getY() + v.getViewHeight(), location
                            .getWidth());
                }
                v.doLayout();
                tempMaximumSpan = 0;
                int yAddition = v.getAbsolutePositionY() + v.getViewHeight()
                        - this.getAbsolutePositionY() - y;
                if (yAddition > 0)
                    y += yAddition;
                //x = Math.max(x, v.getAbsolutePositionX() + v.getViewWidth());
                //originLocations.add(0, high);
                //originLocations.add(1, low);
                addLocation(originLocations, low);
                addLocation(originLocations, high);
            }
            setContentWidth(x);
            setContentHeight(y);
        }
        else
        {
            setContentWidth(0);
            setContentHeight(0);
        }
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (visible)
        {
            this.paintBackground(g);
            this.paintBorder(g);
            if (childViews != null)
            {
                Rectangle clip = g.getClipBounds();
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    View v = (View) e.nextElement();
                    if (BlockView.insideClip(clip, v))
                    {
                        v.paint(g);
                    }
                }
            }
            super.paint(g);
        }
    }

    private void addLocation(Vector locations, Location location)
    {
        int size = locations.size();
        Location loc;
        if (size == 0)
            locations.add(location);
        else
        {
            for (int i = 0; i < size; i++)
            {
                loc = (Location) locations.get(i);
                if (location.getY() < loc.getY())
                {
                    locations.add(i, location);
                    if (i > 0)
                        location.setWidth(((Location) locations.get(i)).getWidth());
                }
                else if (location.getX() > loc.getX())
                {
                    loc.setY(location.getY());
                }
            }
        }
    }

    public int getMaximumSpan(int axis)
    {
        if (tempMaximumSpan > 0)
            return tempMaximumSpan;
        else
            return super.getMaximumSpan(axis);
    }
    
    class Location
    {
        int x, y, width;

        public Location(int xArg, int yArg, int widthArg)
        {
            x = xArg;
            y = yArg;
            width = widthArg;
        }

        public int getWidth()
        {
            return width;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public void setX(int xArg)
        {
            x = xArg;
        }

        public void setY(int yArg)
        {
            y = yArg;
        }

        public void setWidth(int d)
        {
            width = d;
        }
    }
}