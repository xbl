/*
 * BlockView.java
 * 
 * Created on September 17, 2003, 6:35 PM
 */
package fi.hut.tml.xsmiles.csslayout.view;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Node;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.CSS_Property;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
/**
 * @author honkkis
 */
public class ParagraphView extends BaseView
{
    Vector originalTextViews;
    CSSValue textAlign;
    int indent = 0;

    /** Creates a new instance of BlockView */
    public ParagraphView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
        textAlign = parent.getStyle().getPropertyCSSValue(CSS_Property.TEXT_ALIGN);
        indent = StyleGenerator.getMeasure(parent.getStyle(), CSS_Property.TEXT_INDENT);
    }

    public Vector getOriginalViews()
    {
        return originalTextViews;
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        super.doLayout();
        //	View child;
        //	child = (View)childViews.elementAt(0);
        // First time doing layout. Store original content to another vector.
        if (!childViews.isEmpty() && !(childViews.elementAt(0) instanceof RowView))
            originalTextViews = (Vector) childViews.clone();
        createRowViews(originalTextViews);
        int y = 0;//(int)this.getPreferredSpan(View.Y_AXIS);
        int x = 0;//(int)this.getPreferredSpan(View.X_AXIS);
        if (childViews != null && childViews.size() > 0)
        {
            ((BaseView)childViews.elementAt(0)).getDimensions().setLeftMargin(indent);
            Enumeration e = childViews.elements();
            BaseView v = null;
            while (e.hasMoreElements())
            {
                v = (BaseView) e.nextElement();
                v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS), this
                        .getDimensions().getContentOrigin(View.Y_AXIS)
                        + y);
                v.doLayout();
                y = y + v.getViewHeight();
                x = Math.max(x, v.getViewWidth());
            }
            setContentWidth(x);
            setContentHeight(y);

            // Align text
            if (textAlign != null && !textAlign.getCssText().equals("left"))
            {
                e = childViews.elements();
                int size = ((BaseView) this.getParentView()).getSizeRequirements().getMaximumSpan(
                        View.X_AXIS)
                        - ((BaseView) this.getParentView()).getDimensions().getOuterSize(View.X_AXIS);
                int space = 0;
                while (e.hasMoreElements())
                {
                    v = (BaseView) e.nextElement();
                    if ((space = (size - v.getDimensions().getViewWidth())) > 0)
                    {
                        if (textAlign.getCssText().equals("center"))
                            space = space / 2;
                        //v.moveView(View.X_AXIS, space);
                        v.setAbsolutePosition(space + v.getAbsolutePositionX(), v
                                .getAbsolutePositionY());
                        v.doLayout();
                        x = Math.max(x, space + v.getViewWidth());
                    }
                }
                setContentWidth(x);
            }
        }
        else
        {
            setContentWidth(0);
            setContentHeight(0);
        }
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (childViews != null)
        {
            Rectangle clip = g.getClipBounds();
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                View v = (View) e.nextElement();
                if (BlockView.insideClip(clip, v))
                {
                    v.paint(g);
                }
            }
        }
        super.paint(g);
    }
    
    public void setIndent(int i)
    {
        this.indent = i;
    }

    public void drawBorder(Graphics g)
    {
        /*
         * g.setColor(Color.white); Rectangle rect=this.getRectangle();
         * //Log.debug("drawing borders at"+rect);
         * g.drawRect(rect.x,rect.y,rect.width,rect.height);
         */
        //super.drawBorder(g);
    }

    /**
     * Creates row views for the text views. Text is divided to rows to fit in
     * window. Rows have to be recreated when window is resized etc.
     * 
     * @param views
     *            Vector of text views
     */
    void createRowViews(Vector views)
    {
        childViews.removeAllElements();
        if (views == null)
            return;
        Enumeration content = views.elements();
        View view;
        /*
         * Goes through all the original text views and creates row views for
         * them. Also text views are recreated to fit in the rows.
         */
        while (content.hasMoreElements())
        {
            view = (View) content.nextElement();
            if (view instanceof InlineView)
                ((InlineView) view).calculateSize();
            // Width of the inline element.
            int width = view.getViewWidth();
            Vector splitted;
            if (view instanceof InlineView)
                splitted = splitInline(width, view, this);
            else
                splitted = splitToViews(width, view);
            insertRows(splitted);
        }
    }

    private Vector splitInline(int width, View view, View parent)
    {
        Vector splitted;
        Vector finalViews = new Vector();
        if (((InlineView) view).childViews != null)
        {
            View child;
            Enumeration inlines = ((InlineView) view).childViews.elements();
            while (inlines.hasMoreElements())
            {
                child = (View) inlines.nextElement();
                if (child instanceof InlineView)
                {
                    ((InlineView) child).calculateSize();
                    Vector someViews = splitInline(child.getViewWidth(), child, view);
                    Enumeration sp = someViews.elements();
                    // Remove old views from DOM element.
                    if (view.getDOMElement() instanceof VisualElementImpl)
                        ;//((VisualElementImpl) view.getDOMElement()).removeAllViews();
                    while (sp.hasMoreElements())
                    {
                        View contentView = (View) sp.nextElement();
                        InlineView inline = new InlineView(renderer, view.getDOMElement(), parent);
                        inline.addChildView(contentView);
                        contentView.setParentView(inline);
                        finalViews.addElement(inline);
                    }
                }
                else
                {
                    splitted = splitToViews(width, child);
                    Enumeration sp = splitted.elements();
                    // Remove old views from DOM element.
                    if (view.getDOMElement() instanceof VisualElementImpl)
                        ((VisualElementImpl) view.getDOMElement()).removeAllViews();
                    while (sp.hasMoreElements())
                    {
                        View contentView = (View) sp.nextElement();
                        InlineView inline = new InlineView(renderer, view.getDOMElement(), parent);
                        inline.addChildView(contentView);
                        contentView.setParentView(inline);
                        finalViews.addElement(inline);
                    }
                }
            }
        }
        return finalViews;
    }

    /**
     * Divides text view to several views to fit in the containing block.
     */
    protected Vector splitToViews(int width, View view)
    {
        Vector views = new Vector();
        int previousTextWidth = getLastRowWidth();
        // fetch the last row, if it exist.
        // Free space for the text.
        int size = this.getMaximumSpan(View.X_AXIS) - previousTextWidth;
        /*
         * If width is bigger than the size of the free space, text view have to
         * divided to several views.
         */
        if (width >= size && view instanceof TextView)
        {
            TextView textView = (TextView) view;
            CSSStyleDeclaration style = textView.getStyle();
            FontMetrics metrics = renderer.getComponent().getFontMetrics(textView.getFont());
            char[] text = textView.getText().toCharArray();
            childViews.removeElement(textView);
            int start = 0, end = 0;
            // Goes through text character by character.
            for (; end < Array.getLength(text); end++)
            {
                // Search offset when string wider than free space.
                if (metrics.charsWidth(text, start, 1 + end - start) >= size)
                {
                    int initial = end;
                    // Set end offset to white space character.
                    while (!Utilities.isHTMLWhiteSpace(text[end]) && end > start) //(!Character.isSpace(text[end])
                    // &&
                    // end > start)//
                    {
                        end--;
                    }
                    if (end == start)
                    {
                        end = initial;
                        while (end < Array.getLength(text) && !Utilities.isHTMLWhiteSpace(text[end]))
                            end++;
                    }
                    // Create new text view.
                    TextView tv = new TextView(renderer, textView.getDOMElement(), this);
                    tv.setStyle(style);
                    int length = end - start;
                    tv.setText(new String(text, start, end - start));
                    views.addElement(tv);
                    //                    row.addChildView(tv);
                    start = ++end;
                    size = this.getMaximumSpan(View.X_AXIS);//renderer.getMLFC().getContainer().getWidth();
                }
            }
            // Creates last text view for the rest of the text.
            if (end <= Array.getLength(text))
            {
                //                RowView row = new RowView(renderer, null, this);
                TextView tv = new TextView(renderer, textView.getDOMElement(), this);
                tv.setStyle(style);
                tv.setText(new String(text, start, end - start));
                views.addElement(tv);
                //                addChildView(row);
                //                row.addChildView(tv);
            }
        }
        else if (width >= size && view instanceof ImageView && view instanceof ComponentView)
        {
            views.addElement(view);
        }
        /*
         * If width is smaller than the size of the free space, text view
         * doesn't have to divided.
         */
        else
        {
            views.addElement(view);
        }
        return views;
    }

    protected void insertRows(Vector content)
    {
        View view;
        Enumeration e = content.elements();
        while (e.hasMoreElements())
        {
            view = (View) e.nextElement();
            if (view instanceof InlineView)
                ((InlineView) view).calculateSize();
            int viewWidth = view.getViewWidth();
            if (viewWidth + getLastRowWidth() <= this.getMaximumSpan(View.X_AXIS)
                    && !childViews.isEmpty())
            {
                View previousRow = (RowView) childViews.lastElement();
                previousRow.addChildView(view);
                view.setParentView(previousRow);
            }
            else
                insertRow(view);
        }
    }

    protected void insertRow(View view)
    {
        RowView row = new RowView(renderer, null, this);
        addChildView(row);
        row.addChildView(view);
        view.setParentView(row);
    }

    private int getLastRowWidth()
    {
        if (!childViews.isEmpty())
        {
            View previousRow = (RowView) childViews.lastElement();
            if (previousRow != null)
            {
                ((RowView) previousRow).calculateSize();
                return (int) previousRow.getViewWidth();
            }
        }
        // First row, return text indent.
        return indent;
    }
    class RowView extends BlockView
    {
        public RowView(CSSRenderer r, Node node, View parent)
        {
            super(r, node, parent);
        }

        /** do, or redo layout for this and children */
        public void doLayout()
        {
            if (parent != null)
            {
                setMaxContentSize();
            }
            int y = 0;//(int)this.getPreferredSpan(View.Y_AXIS);
            int x = 0;//(int)this.getPreferredSpan(View.X_AXIS);
            if (childViews != null && childViews.size() > 0)
            {
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    BaseView v = (BaseView) e.nextElement();
                    v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS) + x, this
                            .getDimensions().getContentOrigin(View.Y_AXIS));
                    v.doLayout();
                    y = Math.max(y, v.getViewHeight());
                    x = x + v.getViewWidth();
                }
                setContentWidth(x);
                setContentHeight(y);
            }
            else
            {
                setContentWidth(0);
                setContentHeight(0);
            }
        }

        public void drawBorder(Graphics g)
        {
            g.setColor(Color.yellow);
            Rectangle rect = this.getRectangle();
            //Log.debug("drawing borders at"+rect);
            g.drawRect(rect.x, rect.y, rect.width, rect.height);
        }

        /*
         * public void paint(Graphics g) { g.setColor(Color.yellow); Rectangle
         * rect = this.getRectangle(); //Log.debug("drawing borders at"+rect);
         * g.drawRect(rect.x, rect.y, rect.width, rect.height); super.paint(g); }
         */
        public void calculateSize()
        {
            int width = 0, height = 0;
            View view;
            if (childViews != null)
            {
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    view = (View) e.nextElement();
                    if (view instanceof InlineView)
                    {
                        ((InlineView) view).calculateSize();
                    }
                    width += view.getViewWidth();
                    height += view.getViewHeight();
                }
            }
            setContentWidth(width);
            setContentHeight(height);
        }
    }
}