/*
 * PreView.java
 *
 * Created on May 5, 2004, 12:58 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import org.w3c.dom.Node;
import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

/**
 *
 * @author  mpohja
 */
public class PreView extends ParagraphView
{

    private boolean newrow = false;

    /** Creates a new instance of PreView */
    public PreView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
    }
    
    /**
     * Divides text view to several views according to line breaks.
     */
    protected Vector splitToViews(int width, View view)
    {
        Vector views = new Vector();
        
        
        if (view instanceof TextView)
        {
            TextView textView = (TextView)view;
            CSSStyleDeclaration style = textView.getStyle();
            String text = textView.getText();
            StringTokenizer tokens = new StringTokenizer(text, "\n", true);
            
            boolean newline = false;
            
            while (tokens.hasMoreTokens())
            {
                TextView tv = null;
                String token = tokens.nextToken();
                tv = new TextView(renderer, textView.getDOMElement(), this);
                tv.setStyle(style);
                tv.setText(token);
                views.addElement(tv);
            }
        }
        else
        {
            views.addElement(view);
        }
        return views;
    }
   
    protected void insertRows(Vector content)
    {
        View view;
        if (childViews.isEmpty())
            newrow = true;
        Enumeration e = content.elements();
        while (e.hasMoreElements())
        {
            view = (View)e.nextElement();
            if (view instanceof TextView)
            {
                if (newrow)
                    insertRow(view);
                else
                {
                    View previousRow = (RowView)childViews.lastElement();
                    previousRow.addChildView(view);
                    view.setParentView(previousRow);
                }
                if (((TextView)view).getText().equals("\n"))
                    newrow = true;
                else
                    newrow = false;
            }
            else
            {
                if (newrow)
                    insertRow(view);
                else
                {
                    View previousRow = (RowView)childViews.lastElement();
                    previousRow.addChildView(view);
                    view.setParentView(previousRow);
                }
                newrow = false;
                // Check if there is a line break within inline.
                if (view instanceof InlineView)
                {
                    View child = (View)view.getChildren().firstElement();
                    if (child instanceof TextView && ((TextView)child).getText().equals("\n"))
                        newrow = true;
                }
            }
        }
    }
}
