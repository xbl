/*
 * BlockView.java
 * 
 * Created on September 17, 2003, 6:35 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.util.Vector;
import java.util.Enumeration;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.CSSBorder;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;

import org.w3c.dom.Node;
import org.w3c.dom.css.CSSStyleDeclaration;

/**
 * @author honkkis
 */
public class ScrollBlockView extends BlockView implements ScrollView, MouseListener, MouseMotionListener
{
    protected Container container;
    protected Container scrollpane;
    protected Container midcontainer; // we should do without this, but for now
                                      // it is ok.
    protected Container content;

    /** Creates a new instance of ScrollBlockView */
    public ScrollBlockView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
        this.container = r.getComponent();
        this.createScrollPane();
    }

    protected void createScrollPane()
    {
        content = new ContentComponent();
        midcontainer = new ContentContainer();
        midcontainer.setLayout(null);
        midcontainer.add(content);
        scrollpane = this.getCSSRenderer().getComponentFactory().createScrollPane(midcontainer,
                ComponentFactory.VERTICAL_SCROLLBAR_ALWAYS);
        content.addMouseListener(this);
        content.addMouseMotionListener(this);
        content.setBackground(Color.red);
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        super.doLayout();
        int contentHeight = 0, viewContentHeight = 0;
        int contentWidth = 0, viewContentWidth = 0;
        if (childViews != null && childViews.size() > 0)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                BaseView v = (BaseView) e.nextElement();
                if (v.hasAbsolutePosition())
                {
                    v.setAbsoluteCoordinates();
                    v.doLayout();
                }
                else
                {
                    v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS), this
                            .getDimensions().getContentOrigin(View.Y_AXIS)
                            + contentHeight);
                    v.doLayout();
                    contentHeight = contentHeight + v.getViewHeight();
                    contentWidth = Math.max(contentWidth, v.getViewWidth());
                    if (v.hasRelativePosition())
                        v.handleRelativeView();
                }
            }
            if (!this.hasAbsolutePosition() || (getViewWidth() == 0 && getViewHeight() == 0))
            {
                // Block's width should be as wide as possible except if
                // it's inside table or it has relative width. Note: if view has
                // absolute width, the dimension change is denied by
                // ViewDimensions.
                if (getParentView() != null && !this.isInTable() && !hasRelativeWidth)
                {
                    int max = ((BaseView) getParentView()).getMaximumSpan(View.X_AXIS)
                            - this.getDimensions().getOuterSize(View.X_AXIS);
                    viewContentWidth = Math.max(contentWidth, max);
                }
                setContentWidth(viewContentWidth);
                setContentHeight(contentHeight);
            }
            // TODO: For absolute position with height = 0
            if (this.getViewHeight() == 0)
                setContentHeight(contentHeight);
        }
        else
        {
            setContentWidth(0);
            setContentHeight(0);
        }
        if (scrollpane.getParent() != container)
        {
            //Log.debug("Adding container to scrollpane in scrollblockview.");
            content.setVisible(true);
            container.add(scrollpane, 0);
            //scrollpane.setLayout(null); // does not seem to work...;
            //midcontainer.setPreferredSize(dim);
            //midcontainer.setMinimumSize(dim);

            //scrollpane.setSize(100,100);
        }
        scrollpane.setSize(this.dimensions.getMaxContentSize(View.X_AXIS), this.dimensions
                .getMaxContentSize(View.Y_AXIS));
        content.setLocation(0 - this.getDimensions().getContentOrigin(View.X_AXIS), 0 - this
                .getDimensions().getContentOrigin(View.Y_AXIS));
        scrollpane.setLocation(this.getDimensions().getContentOrigin(View.X_AXIS), this
                .getDimensions().getContentOrigin(View.Y_AXIS));
        Dimension dim = new Dimension(contentWidth
                + this.getDimensions().getContentOrigin(View.X_AXIS), contentHeight
                + this.getDimensions().getContentOrigin(View.Y_AXIS));
        Dimension contentDim = new Dimension(contentWidth, contentHeight);
        content.setSize(dim);
        midcontainer.setSize(contentDim);
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (visible)
        {
            this.paintBackground(g);
            this.paintBorder(g);
            // dont paint children
            /*
             * if (childViews != null) { Rectangle clip = g.getClipBounds();
             * Enumeration e = childViews.elements(); while
             * (e.hasMoreElements()) { View v = (View) e.nextElement(); if
             * (insideClip(clip, v)) { v.paint(g); } } } super.paint(g);
             */
        }
    }

    public class ContentComponent extends Container
    {
        public void paint(Graphics g)
        {
            if (visible)
            {
                if (CSSRenderer.antiAlias)
                {
                    {
                        CompatibilityFactory.getGraphicsCompatibility().setAntialias(CSSRenderer.antiAlias, g);
                    }
                }
                paintBackground(g);
                //paintBorder(g);
                if (childViews != null)
                {
                    Rectangle clip = g.getClipBounds();
                    Enumeration e = childViews.elements();
                    while (e.hasMoreElements())
                    {
                        View v = (View) e.nextElement();
                        //if (insideClip(clip, v)) // now we just paint
                        // everything
                        {
                            v.paint(g);
                        }
                    }
                }
                super.paint(g);
            }
        }
        /*
         * public Dimension getSize() { return new Dimension(1000,1000); }
         */
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.csslayout.view.ScrollView#hasContainer()
     */
    public boolean hasContainer()
    {
        // TODO Auto-generated method stub
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.csslayout.view.ScrollView#getContainerForDescendants()
     */
    public Container getContainerForDescendants()
    {
        // TODO Auto-generated method stub
        return this.content;
    }

    public class ContentContainer extends Container
    {
        public Dimension getPreferredSize()
        {
            return this.getSize();
        }

        public Dimension getMinimumSize()
        {
            return this.getSize();
        }
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.csslayout.view.ScrollView#removeContainerFromView()
     */
    public void removeContainerFromView()
    {
        Container parent = this.scrollpane.getParent();
        if (parent!=null)
        {
            parent.remove(scrollpane);
        }
        
    }
    
    public void viewRemoved()
    {
            this.removeContainerFromView();
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent event)
    {
        Point pt = new Point(event.getX(), event.getY());
        View view = this.getViewAtPoint(pt);
        renderer.mouseClicked(view, event);
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent arg0)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent arg0)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent arg0)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent arg0)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
     */
    public void mouseDragged(MouseEvent arg0)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
     */
    public void mouseMoved(MouseEvent event)
    {
        Point pt = new Point(event.getX(), event.getY());
        View view = this.getViewAtPoint(pt);
        renderer.mouseMoved(view, event);
    }

}