/*
 * BlockView.java
 * 
 * Created on September 17, 2003, 6:35 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Dimension;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

import org.w3c.dom.Node;

/**
 * @author honkkis
 */
public class RootView  extends BlockView implements ContainerView  
{

    /** Creates a new instance of BlockView */
    public RootView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
    }

    public void doLayout()
    {
        int widthCont, heightCont;
        if (renderer.isInWindow())
        {
            BaseView windowView = (BaseView)childViews.firstElement();
            windowView.setAbsoluteCoordinates();
            windowView.setAbsolutePosition(0, 0);
            if ((widthCont = windowView.getViewWidth()) == 0)
                widthCont = 500; // Default width
            heightCont = windowView.getViewHeight();
        }
        else
        {
            widthCont = renderer.getMLFC().getContainer().getSize().width - 30;
            heightCont = renderer.getMLFC().getContainer().getSize().height;
        }
        /*Log.debug("RootView.doLayout(). View: " + this + " Container size: "
                + widthCont +", "+heightCont);
                */
        Dimension rootSize = new Dimension(widthCont, heightCont);
        sizeRequirements.setMaximumSpan(rootSize);
        sizeRequirements.setMinimumSpan(rootSize);
        sizeRequirements.setPreferredSpan(rootSize);
        super.doLayout();

        // If content is wider than the size of the window, layout has to
        // be done again with new size requirements.
        if (widthCont < dimensions.getViewWidth())
        {
            rootSize.setSize(dimensions.getViewWidth() - 30, dimensions.getViewHeight());
            sizeRequirements.setMaximumSpan(rootSize);
            sizeRequirements.setMinimumSpan(rootSize);
            sizeRequirements.setPreferredSpan(rootSize);
            super.doLayout();
        }
        
        
        if (heightCont > dimensions.getViewHeight())
        {
            rootSize.setSize(dimensions.getViewWidth(), heightCont - 30);
            sizeRequirements.setMaximumSpan(rootSize);
            sizeRequirements.setMinimumSpan(rootSize);
            sizeRequirements.setPreferredSpan(rootSize);
            //super.doLayout();
            dimensions.setViewHeight(heightCont - 30);
            BaseView child = (BaseView)this.getChildren().get(0);
            child.getDimensions().setViewHeight(heightCont - 30);
        }
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        //paintBackground(g);
        //Log.debug("RootView.paint() " + this);
        //long start = System.currentTimeMillis();
        super.paint(g);
        //long time = System.currentTimeMillis() - start;
        //System.out.println("***** RootView.paint() took " + Long.toString(time) + " ms");
    }

    public boolean hasContainer()
    {
        // TODO Auto-generated method stub
        return this.getCSSRenderer().getComponent()!=null;
    }

    public Container getContainerForDescendants()
    {
        // TODO Auto-generated method stub
        return this.getCSSRenderer().getComponent();
    }

    public void removeContainerFromView()
    {
        // TODO Auto-generated method stub
        
    }
}