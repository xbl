/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 8, 2005
 *
 */
package fi.hut.tml.xsmiles.csslayout.view;

import java.awt.Container;


/**
 * @author honkkis
 *
 */
public interface ScrollView extends ContainerView
{
}
