/*
 * BaseView.java
 * 
 * Created on September 17, 2003, 6:34 PM
 */
package fi.hut.tml.xsmiles.csslayout.view;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.csslayout.CSSBackgroundImage;
import fi.hut.tml.xsmiles.csslayout.CSSBorder;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.CSS_Property;
import fi.hut.tml.xsmiles.csslayout.ExternalWindow;
import fi.hut.tml.xsmiles.csslayout.SizeRequirements;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
/**
 * @author mpohja,honkkis
 */
public class BaseView implements View
{
    protected int posX, posY;
    protected Vector childViews, absoluteChildViews, relativeChildViews;
    protected CSSRenderer renderer;
    protected Node domElement;
    protected CSSStyleDeclaration style;
    protected View parent;
    protected BaseView.ViewDimensions dimensions;
    protected boolean absolutePosition = false, relativePosition = false;
    protected boolean visible = true;
    /** cache the bg color */
    protected Color bgColor = null;
    protected SizeRequirements sizeRequirements;
    protected CSSBorder border = null;
    protected CSSBackgroundImage backImage = null;
    protected double zoom = 1;
    protected boolean hasRelativeWidth = false, hasRelativeHeight = false, floatLeft = false,
            floatRight = false, closeFloat = false;
    


    /** Creates a new instance of BaseView */
    public BaseView(CSSRenderer r, Node node, View parent)
    {
        renderer = r;
        domElement = node;
        this.parent = parent;

        if (node instanceof StylableElement)
        {
            this.setStyle(((StylableElement) node).getStyle());
            if ((this.style != null)
                    && ((style.getPropertyCSSValue("border-top-style") != null)
                            || (style.getPropertyCSSValue("border-right-style") != null)
                            || (style.getPropertyCSSValue("border-left-style") != null) || (style
                            .getPropertyCSSValue("border-bottom-style") != null)))
                border = new CSSBorder(style);
            if (style == null)
                Log.error("Style for " + node + " null!");

            dimensions = new BaseView.ViewDimensions(style);

            CSSValue position = style.getPropertyCSSValue(CSS_Property.POSITION);
            if (position != null)
            {
                String posString = position.toString();
                if (posString.equals("absolute"))
                    absolutePosition = true;
                else if (posString.equals("relative"))
                    relativePosition = true;
            }
            CSSValue vis = style.getPropertyCSSValue(CSS_Property.VISIBILITY);
            if (vis != null
                    && (vis.getCssText().equals("hidden") || vis.getCssText().equals("collapse")))
                visible = false;
            CSSValue floatValue = style.getPropertyCSSValue(CSS_Property.FLOAT);
            if (floatValue != null)
            {
                if (floatValue.getCssText().equals("left"))
                    floatLeft = true;
                else if (floatValue.getCssText().equals("right"))
                    floatRight = true;
            }
        }
        else
            dimensions = new BaseView.ViewDimensions();
        if (style != null)
            sizeRequirements = new SizeRequirements(style);
        else
            sizeRequirements = new SizeRequirements();

        if ((style != null) && (style.getPropertyCSSValue("background-image") != null))
            backImage = new CSSBackgroundImage(style, renderer, this);

        // Set this view to node it represents as user data.
        if (node != null && node instanceof VisualElementImpl)
        {
            ((VisualElementImpl) node).addView(this);
            /*
             * org.apache.xerces.dom.NodeImpl dom3Node =
             * (org.apache.xerces.dom.NodeImpl) node; Vector views = (Vector)
             * dom3Node.getUserData("views"); if (views == null) views = new
             * Vector(); views.addElement(this); dom3Node.setUserData("views",
             * views, null);
             */
        }
    }
    

    public void addChildView(View v)
    {
        if (childViews == null)
            childViews = new Vector(15);
        this.childViews.addElement(v);
        //Log.debug("View, added child.");
    }

    public void addAbsoluteChildView(View v)
    {
        if (absoluteChildViews == null)
            absoluteChildViews = new Vector(15);
        this.absoluteChildViews.addElement(v);
        //Log.debug("View, added child.");
    }

    public void addRelativeChildView(View v)
    {
        if (relativeChildViews == null)
            relativeChildViews = new Vector(15);
        this.relativeChildViews.addElement(v);
        //Log.debug("View, added child.");
    }

    public View getChildView(int index)
    {
        if (childViews != null)
        {
            return (View) childViews.elementAt(index);
        }
        return null;
    }

    public Node getDOMElement()
    {
        return domElement;
    }

    public CSSStyleDeclaration getStyle()
    {
        return style;
    }

    public int getMaximumSpan(int axis)
    {
        return sizeRequirements.getMaximumSpan(axis);
        //        if (hasAbsolutePosition())
        //            return this.getPreferredSpan(axis);
        //        else
        //        {
        //            if (maxSize == null)
        //            {
        //                Log.error("View "+this+" max size is null. DOM element: "+
        // domElement+". 20 returned");
        //                return 20;
        //            }
        //            if (axis==View.Y_AXIS) return (float)this.maxSize.height;
        //            else return (float)this.maxSize.width;
        //        }
    }

    public int getMinimumSpan(int axis)
    {
        return sizeRequirements.getMinimumSpan(axis);
    }

    public int getPreferredSpan(int axis)
    {
        return sizeRequirements.getPreferredSpan(axis);
    }

    public void setAbsolutePosition(int x, int y)
    {
        posX = x;
        posY = y;
    }

    public int getAbsolutePositionX()
    {
        return posX;
    }

    public int getAbsolutePositionY()
    {
        return posY;
    }

    public int getViewWidth()
    {
        return dimensions.getViewWidth();
    }

    public int getViewHeight()
    {
        return dimensions.getViewHeight();
    }

    public void setContentWidth(int width)
    {
        dimensions.setContentWidth(width);
    }

    public void setContentHeight(int height)
    {
        dimensions.setContentHeight(height);
    }

    public void setViewWidth(int width)
    {
        dimensions.setViewWidth(width);
    }

    public void setViewHeight(int height)
    {
        dimensions.setViewHeight(height);
    }

    public BaseView.ViewDimensions getDimensions()
    {
        return dimensions;
    }

    public CSSRenderer getCSSRenderer()
    {
        return renderer;
    }

    public void doLayout()
    {
        this.renderer.layouts++;
        if (parent != null)
        {
            setMaxContentSize();
            if (hasRelativeWidth)
            {
                sizeRequirements.setMaximumSpan(View.X_AXIS, getMaximumSpan(View.X_AXIS)
                        * dimensions.getWidthPercentage() / 100);
                dimensions.setContentWidth(sizeRequirements.getMaximumSpan(View.X_AXIS));
            }
        }
    }

    protected void setMaxContentSize()
    {
        int x, y;
        if (!hasAbsolutePosition())
        {
            x = ((BaseView) parent).getMaximumSpan(View.X_AXIS) - dimensions.getOuterSize(View.X_AXIS);
            y = ((BaseView) parent).getMaximumSpan(View.Y_AXIS) - dimensions.getOuterSize(View.Y_AXIS);
            sizeRequirements.setMaximumSpan(new Dimension(x, y));
        }
    }

    /**
     * Returns true if view is absolute positioned.
     */
    public boolean hasAbsolutePosition()
    {
        return absolutePosition;
    }

    /**
     * Returns true if view is relative positioned.
     */
    public boolean hasRelativePosition()
    {
        return relativePosition;
    }
    
    /**
     * Returns true if view has float left.
     */
    public boolean isLeftFloated()
    {
        return floatLeft;
    }

    /**
     * Returns true if view has float right.
     */
    public boolean isRightFloated()
    {
        return floatRight;
    }

    /**
     * This is called for absolute positioned views.
     */
    public void setAbsoluteCoordinates()
    {
        int x = 0, y = 0, left, top, bottom, right, height = 0, width = 0, heightProperty, widthProperty;
        boolean hasLeft = false, hasRight = false, hasBottom = false, hasTop = false, hasHeight = false, hasWidth = false;

        left = StyleGenerator.getMeasure(style, CSS_Property.LEFT);
        top = StyleGenerator.getMeasure(style, CSS_Property.TOP);
        bottom = StyleGenerator.getMeasure(style, CSS_Property.BOTTOM);
        right = StyleGenerator.getMeasure(style, CSS_Property.RIGHT);
        heightProperty = StyleGenerator.getMeasure(style, CSS_Property.HEIGHT);
        widthProperty = StyleGenerator.getMeasure(style, CSS_Property.WIDTH);

        if (style.getPropertyCSSValue(CSS_Property.HEIGHT) != null)
            hasHeight = true;
        if (style.getPropertyCSSValue(CSS_Property.WIDTH) != null)
            hasWidth = true;
        if (style.getPropertyCSSValue(CSS_Property.LEFT) != null)
        {
            hasLeft = true;
            x = left;
        }
        if (style.getPropertyCSSValue(CSS_Property.TOP) != null)
        {
            hasTop = true;
            y = top;
        }
        if (style.getPropertyCSSValue(CSS_Property.BOTTOM) != null)
            hasBottom = true;
        if (style.getPropertyCSSValue(CSS_Property.RIGHT) != null)
            hasRight = true;

        // Following calculations have to be changed after View modification.
        if (hasLeft && hasRight)
            width = ((BaseView) getParentView()).getDimensions().getMaxContentSize(View.X_AXIS) - left
                    - right - getDimensions().getOuterSize(View.X_AXIS);
        else
            width = widthProperty;

        if (hasTop && hasBottom)
            height = ((BaseView) getParentView()).getDimensions().getMaxContentSize(View.Y_AXIS) - top
                    - bottom - getDimensions().getOuterSize(View.Y_AXIS);
        else
            height = heightProperty;

        if (!hasLeft)
            x = right + widthProperty + dimensions.getOuterSize(View.X_AXIS);

        if (!hasTop)
            y = bottom + heightProperty + dimensions.getOuterSize(View.Y_AXIS);

        x = x + this.getParentView().getAbsolutePositionX();
        y = y + this.getParentView().getAbsolutePositionY();

        setAbsolutePosition(x, y);
        setContentWidth(width);
        setContentHeight(height);
    }

    public void handleRelativeView()
    {
        ((BaseView) this.getParentView()).addRelativeChildView(this);
        /*
         * this.removeFromTree(); RootView root =
         * getCSSRenderer().getRootView(); this.setParentView(root);
         * root.addChildView(this);
         */this.setRelativePosition();
    }

    protected void setRelativePosition()
    {
        int left = 0, right = 0, top = 0, bottom = 0, x, y;
        left = StyleGenerator.getMeasure(style, CSS_Property.LEFT);
        top = StyleGenerator.getMeasure(style, CSS_Property.TOP);
        bottom = StyleGenerator.getMeasure(style, CSS_Property.BOTTOM);
        right = StyleGenerator.getMeasure(style, CSS_Property.RIGHT);

        x = left - right;
        y = top - bottom;
        setAbsolutePosition(posX + x, posY + y);
        moveChildViews(x, y);
    }

    protected void moveChildViews(int x, int y)
    {
        if (childViews != null)
        {
            Enumeration e = childViews.elements();
            BaseView view = null;
            while (e.hasMoreElements())
            {
                view = (BaseView) e.nextElement();
                view.setAbsolutePosition(view.posX + x, view.posY + y);
                view.moveChildViews(x, y);
            }
        }
    }

    public Rectangle getRectangle()
    {
        // TODO: cache
        return new Rectangle(posX, posY, getViewWidth(), getViewHeight());
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (relativeChildViews != null)
        {
            Enumeration e = relativeChildViews.elements();
            while (e.hasMoreElements())
            {
                ((BaseView) e.nextElement()).paint(g);
            }
        }
    }

    public void paintBorder(Graphics g)
    {
        Rectangle rect = this.dimensions.getBorderRectangle();
        if (border != null)
            border.paintBorder(g, (int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
    }

    public void paintBackground(Graphics g)
    {
        // calculate clipping intersection here to optimize
        Rectangle rect = this.dimensions.getMarginRectangle();
        Rectangle clip = g.getClipBounds();
        Rectangle intersect = this.calculateIntersection(clip, rect);
        Color old = g.getColor();
        Color bgColor = this.getCSSBackgroundColor();
        if (!CompatibilityFactory.getCompatibility().isTransparentColor(bgColor))
        {
            g.setColor(bgColor);
            g.fillRect(intersect.x, intersect.y, intersect.width, intersect.height);
            g.setColor(old);
        }

        rect = this.dimensions.getBorderRectangle();
        if (backImage != null)
            backImage.paint(g, (int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
    }
    
    public void repaint(Image img)
    {
        backImage.setImage(img);
        renderer.repaint(this.getRectangle());
    }

    public Rectangle calculateIntersection(Rectangle rect1, Rectangle rect2)
    {
        Rectangle intersect = (Rectangle) rect1.intersection(rect2);
        return intersect;
    }

    public void createChildViews(Node parent)
    {
        NodeList children;
        if (parent instanceof XSmilesElementImpl)
            children = ((XSmilesElementImpl) parent).getChildNodes(true); // this
        // returns
        // pseudoelements
        // as
        // well
        else
            children = parent.getChildNodes();
        for (int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            if (child != null)
                this.createChildView(child, parent);
        }
    }

    public View createChildView(Node child, Node parent)
    {
        //Log.debug("Creating child view for: "+child.getNodeName());
        String display = "", position = "", container = "", containertype = "";
        View parentView;
        if (child instanceof StylableElement)
        {
            CSSStyleDeclaration style = ((StylableElement) child).getStyle();
            if (style == null)
            {
                display = "inline"; // inline is initial value
                Log.error("STYLE WAS NULL FOR NODE: " + child);
            }
            else
            {
                display = style.getPropertyValue(CSS_Property.DISPLAY);
                if (display == null || display.length() == 0)
                    display = "inline";
                position = style.getPropertyValue(CSS_Property.POSITION);
                container = style.getPropertyValue(CSS_Property.CONTAINER);
                containertype = style.getPropertyValue(CSS_Property.CONTAINERTYPE);
            }
        }
        if (container.equals("window") && !display.equals("none"))
        {
            renderer.createWindow(child, display, containertype);
            return null;
        }
        View view=null;
        if (!display.equals("none"))
        {
                view = createView(child, display);
        }
        if (view != null)
        {
            if (position.equals("absolute"))
            {
                parentView = findContainingBlock(this);
                addAbsoluteChildView(view);
                view.setParentView(parentView);
            }
            else
                parentView = this;
            addView(child, view, parentView, display);
        }
        return view;
    }

    protected void addView(Node node, View view, View parentView, String display)
    {
        if (display.equals("none") || view == null)
        {
            //Log.debug("*********************** Display none for node:"+node);
            // let's remove all components that have been visible
            // note that this is probably called many times unnecessarily, for
            // instance at init time
            // or when the status has not been changed.
            
            // no longer used, now views get informed through viewRemoved
            //if (node instanceof Element)
               // this.removeComponentsRecursively(node);
        }
        else if (((BaseView)view).isLeftFloated() || ((BaseView)view).isRightFloated() || closeFloat)
        {
            addFloatView(view);
            view.createChildViews(node);
        }
        else if (display.equals("block") || display.equals("table")
                || display.equals("table-row-group") || display.equals("table-header-group")
                || display.equals("table-footer-group"))
        {
            parentView.addChildView(view);
            if (view instanceof BlockView || view instanceof TableView
                    || view instanceof TableView.RowGroupView)
                view.createChildViews(node);
        }
        else if (display.equals("list-item"))
        {
            addListItem(view);
            view.createChildViews(node);
        }
        else if (view instanceof TextView)
        {
            if (this instanceof InlineView)
                addChildView(view);
            else
                addInlineView(view);
        }
        else
        {
            addInlineView(view);
            if (!(node instanceof VisualComponentService))
                view.createChildViews(node);
        }
    }

    /**
     * Add inline view to view tree. Inline has to be always inside Paragraph or
     * Pre view.
     */
    void addInlineView(View view)
    {
        CSSValue whitespace = view.getStyle().getPropertyCSSValue(CSS_Property.WHITESPACE);
        if (whitespace != null
                && (whitespace.getCssText().equals("pre") || whitespace.getCssText().equals("nowrap")))
        {
            if (childViews != null && childViews.size() > 0
                    && childViews.lastElement() instanceof PreView)
            {
                ((PreView) childViews.lastElement()).addChildView(view);
                view.setParentView((View) childViews.lastElement());
            }
            else if (this instanceof InlineView)
            {
                addChildView(view);
            }
            else
            {
                PreView p = new PreView(renderer, null, this);
                addChildView(p);
                p.setParentView(this);
                p.addChildView(view);
                view.setParentView(p);
            }
        }
        else
        {
            if (childViews != null && childViews.size() > 0
                    && childViews.lastElement() instanceof ParagraphView)
            {
                ((ParagraphView) childViews.lastElement()).addChildView(view);
                view.setParentView((View) childViews.lastElement());
            }
            else if (this instanceof InlineView)
            {
                addChildView(view);
            }
            else
            {
                ParagraphView p = new ParagraphView(renderer, null, this);
                addChildView(p);
                p.setParentView(this);
                p.addChildView(view);
                view.setParentView(p);
            }
        }
    }

    void addListItem(View view)
    {
        if (childViews != null && childViews.lastElement() != null
                && childViews.lastElement() instanceof ListView)
            ((ListView) childViews.lastElement()).addChildView(view);
        else
        {
            ListView lv = new ListView(renderer, null, this);
            addChildView(lv);
            lv.setParentView(this);
            lv.addChildView(view);
            view.setParentView(lv);
        }
    }
    
    void addFloatView(View view)
    {
        if (childViews != null && childViews.lastElement() != null
                && childViews.lastElement() instanceof FloatContainer && closeFloat)
        {
            ((FloatContainer) childViews.lastElement()).addChildView(view);
            view.setParentView((FloatContainer) childViews.lastElement());
        }
        else
        {
            FloatContainer fc = new FloatContainer(renderer, null, this);
            addChildView(fc);
            fc.setParentView(this);
            fc.addChildView(view);
            view.setParentView(fc);
        }
        if (((BaseView)view).isLeftFloated() || ((BaseView)view).isRightFloated())
            closeFloat = true;
        else
            closeFloat = false;
    }

    public View createComponentView(VisualComponentService e)
    {
        ComponentView c = new ComponentView(renderer, (Node) e, e,this);
        this.getCSSRenderer().cancelComponentRemoval(e.getComponent());
        return c;
    }

    public View createView(Node node, String display)
    {
        String overflow = "";
        if (node instanceof StylableElement)
            overflow = ((StylableElement) node).getStyle().getPropertyValue(CSS_Property.OVERFLOW);

        View view = null;
        if (node instanceof VisualComponentService)
        {
            view = createComponentView((VisualComponentService) node);
        }
        // TODO: SOME OTHER WAY OF TESTING THAN LOCAL NAME. ALSO NAMESPACE
        // MATTERS
        else if (node.getNodeType() == Node.ELEMENT_NODE && node.getLocalName().equals("img"))
        {
            view = new ImageView(renderer, node, this, ((Element) node).getAttribute("src"));
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && display.equals("block"))
        {
            if ("scroll".equals(overflow))
            {
                view = new ScrollBlockView(renderer, node, this);
                
            }
            else
            {
                view = new BlockView(renderer, node, this);
            }
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && display.equals("list-item"))
        {
            view = new ListItemView(renderer, node, this);
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE && (display.equals("table")))
        {
            view = new TableView(renderer, node, this);
        }
        /*
         * else if (node.getNodeType()==Node.ELEMENT_NODE &&
         * (display.equals("table-row"))) { view = new
         * TableView.RowView(renderer, node, this); } else if
         * (node.getNodeType()==Node.ELEMENT_NODE &&
         * (display.equals("table-cell"))) { view = new
         * TableView.CellView(renderer, node, this); }
         */
        else if (node.getNodeType() == Node.ELEMENT_NODE)
        {
            view = new InlineView(renderer, node, this);
        }
        else if (node.getNodeType() == Node.TEXT_NODE || node.getNodeType() == Node.CDATA_SECTION_NODE)
        {
            String text = node.getNodeValue();// getText((Element)child);
            if (text != null && text.length() > 0 && testValue(text))
            {
                view = new TextView(renderer, node, this);
                if (node.getParentNode() instanceof StylableElement)
                {
                    CSSStyleDeclaration style = ((StylableElement) node.getParentNode()).getStyle();
                    if (style != null)
                        ((TextView) view).setStyle(style);
                }
                ((TextView) view).setText(text);
            }
        }
        if (view == null)
            ;
        //            Log.debug("No view creted for "+ node);
        return view;
    }

    public void insertBefore(Node newChild, Node refChild)
    {
        CSSValue displayValue, position = null;
        String display = "";
        View parentView;
        if (newChild instanceof StylableElement)
        {
            CSSStyleDeclaration style = ((StylableElement) newChild).getStyle();
            if (style == null)
            {
                display = "inline"; // inline is initial value
                Log.error("STYLE WAS NULL FOR NODE: " + newChild);
            }
            else
            {
                displayValue = style.getPropertyCSSValue("display");
                if (displayValue == null)
                    display = "inline";
                position = style.getPropertyCSSValue("position");
            }
        }
        if (position != null && position.toString().equals("absolute"))
        {
            parentView = getCSSRenderer().getRootView();
        }
        else
            parentView = this;
        View newView = createView(newChild, display);
        Vector refViews = ((VisualElementImpl) refChild).getViews();
        View refView = (View) refViews.firstElement();
        View parent = refView.getParentView();
        // TODO: add newView to correct place...
    }

    public static String getText(Element elem)
    {
        String text = "";
        NodeList children = elem.getChildNodes();
        for (int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            if (child.getNodeType() == Node.TEXT_NODE)
            {
                text += child.getNodeValue();
            }
        }
        return text;
    }

    /**
     * Returns a view, which embeds the given point.
     * 
     * @param point
     */
    public View getViewAtPoint(Point pt)
    {
        //Log.debug("get view at point: "+pt+". this view:
        // "+posX+"-"+posX+prefSize.getWidth()+" and
        // "+posY+"-"+posY+prefSize.getHeight());
        BaseView child, view = null, asd = null;
        if (posX <= pt.x && pt.x <= posX + getViewWidth() && posY <= pt.y
                && pt.y <= posY + getViewHeight())
        {
            view = this;
            if (childViews != null)
            {
                for (int i = childViews.size() - 1; i >= 0; i--)
                {
                    child = (BaseView) childViews.elementAt(i);
                    asd = (BaseView) child.getViewAtPoint(pt);
                    if (asd != null)
                    {
                        view = asd;
                        break;
                    }
                }
            }
        }
        else if (this.hasAbsolutePosition() || this.hasRelativePosition())
        {
            if (childViews != null)
            {
                for (int i = childViews.size() - 1; i >= 0; i--)
                {
                    child = (BaseView) childViews.elementAt(i);
                    asd = (BaseView) child.getViewAtPoint(pt);
                    if (asd != null)
                    {
                        view = asd;
                        break;
                    }
                }
            }

        }
        return view;
    }

    protected View findContainingBlock(BaseView view)
    {
        BaseView cBlock = view;
        while (!cBlock.hasAbsolutePosition() && !cBlock.hasRelativePosition()
                && !(cBlock instanceof RootView))
        {
            cBlock = (BaseView) cBlock.getParentView();
        }
        return cBlock;
    }

    /**
     * Check if the text node is real text or just indentation in original xhtml
     * document.
     */
    protected boolean testValue(String text)
    {
        if (text == null)
            return false;
        //if (parent instanceof VisualComponentService) return false; // Was
        // true!!
        // TODO: check punctuation marks etc.!
        try
        {
            int length = text.length();
            for (int i = 0; i < length; i++)
            {
                if (!Utilities.isHTMLWhiteSpace(text.charAt(i)))
                    return true;
                //if (Character.getNumericValue(text.charAt(i)) != -1) return
                // true;
            }
            return false;
        }
        catch (NullPointerException npe)
        {
            Log.debug("XHTMLDocument.testValue: text: " + text);
            Log.error(npe);
        }
        return true;
    }

    public void dump(String indent)
    {
        if (this instanceof TextView)
            Log.debug(indent + this.toString() + ", pos:(" + posX + ", " + posY + "), size: (" + getViewWidth()
                    + ", " + getViewHeight() + ") : " + " text: " + ((TextView) this).getText() + ", "
                    + this.style);
        else
        {
            String hash = "";
            if (this.domElement!=null) hash+=this.domElement.hashCode();
            Log.debug(indent + this.toString() + this.domElement + hash+", pos:(" + posX + ", " + posY + "), size: ("
                    + getViewWidth() + ", " + getViewHeight() + ")" + ". Parent: "
                    + this.getParentView() + ", " + this.style);
            BaseView child;
            if (childViews != null)
            {
                for (int i = 0; i < childViews.size(); i++)
                {
                    child = (BaseView) childViews.elementAt(i);
                    child.dump(indent + "  ");
                }
            }
        }
    }
 /*   public void setZoom(double zoom)
    {
        System.out.println("((((((((((((((((((((--"+zoom+"---baseView1");
        if (border != null){
        	dimensions.setZoom(zoom);
        	border.setZoom(zoom);
        }
        BaseView child;
        if (childViews != null)
        {
            for (int i = 0; i < childViews.size(); i++)
            {
                child = (BaseView) childViews.elementAt(i);
                child.setZoom(zoom);
            }
        }
        
    }
    */
    public View getParentView()
    {
        return parent;
    }

    public void setParentView(View parent)
    {
        this.parent = parent;
    }

    public void setStyle(CSSStyleDeclaration s)
    {
        this.style = s;
    }

    private Color getCSSBackgroundColor()
    {
        // Root view must have transparent background in order to be possible to add
        // components behind it.
        if (this instanceof RootView)
            return new Color(255,255,255);
        if (bgColor == null)
        {
            CSSStyleDeclaration s = this.style;
            View v = this;
            while (v != null && s == null)
            {
                v = v.getParentView();
                s = v.getStyle();
            }
            if (s == null)
                return Color.red;
            bgColor = StyleGenerator.parseColor(s.getPropertyCSSValue(CSS_Property.BACKGROUND_COLOR),
                    Color.yellow);
        }
        return bgColor;
    }

    /**
     * Removes this view from the view tree.
     */
    public void removeFromTree()
    {
        // NOTE: If inside paragraph view, should be removed from there, too.
        parent.removeChild(this);
        //        View par = parent;
        //        while (!(par instanceof RootView))
        //        {
        //            par = par.getParentView();
        //            if (par instanceof ParagraphView)
        //            {
        //                ((ParagraphView)par).getOriginalViews().remove(this);
        //                break;
        //            }
        //        }
    }

    /**
     * This is called by XSmilesVisualElement when a node is removed from the
     * DOM, let's remove all components from view
     * 
     * @param removed
     */
    /*
    public void nodeRemoved(Node removed)
    {
        // now views get informed through viewRemoved
        //this.removeComponentsRecursively(removed);
    }*/


    public View getFirstChild()
    {
        return (View) childViews.firstElement();
    }

    /** this method is called when a view is removed from the hierarchy.
     * for instance, components can then be removed from the containers
     *
     */
    public void viewRemoved()
    {
    }
    
    /** go through all descendant views and notify them of removal */
    protected void notifyViewRemovedRecursively(View child)
    {
        child.viewRemoved();
        // TODO: for some reason textviews hang if their children is begin enumerated etc.
        if (child.getChildren()!=null&& child.getChildren().size()>0)
        {
            Enumeration e = child.getChildren().elements();
            while (e.hasMoreElements())
            {
                BaseView nv=(BaseView)e.nextElement();
                this.notifyViewRemovedRecursively(nv); // recursion step
            }
        }
    }

    public void removeChild(View child)
    {
        notifyViewRemovedRecursively(child);
        if (childViews != null)
        {
            childViews.removeElement(child);
        }
        //if (child instanceof BaseView) this.removeComponentsFromSubtree(
        // (BaseView)child);
    }

    public void removeChildren()
    {
        //this.removeComponentsFromSubtree(this);
        if (childViews != null)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                View nv=(View)e.nextElement();
                this.notifyViewRemovedRecursively(nv);
            }
            childViews.removeAllElements();
            /*
            while (childViews.size()>0)
            {
                this.removeChild((View)childViews.elementAt(0)); // removechild notifies descendant views of removal
            }*/
        }
        if (absoluteChildViews != null)
        {
            Enumeration e = absoluteChildViews.elements();
            while (e.hasMoreElements())
            {
                ((View) e.nextElement()).removeFromTree();
            }
            absoluteChildViews.removeAllElements();
        }
    }

    /*
     * NOT IN USE ANYMORE protected void removeComponentsFromSubtree(BaseView
     * view) { if (this instanceof ComponentView) {
     * ((ComponentView)this).removeComponent(); } if (view.childViews!=null) {
     * View ab; Enumeration e = view.childViews.elements(); while
     * (e.hasMoreElements()) { View v = (View)e.nextElement(); if (v instanceof
     * ComponentView) { ((ComponentView)v).removeComponent(); } else if (v
     * instanceof BaseView) { // go thru children
     * this.removeComponentsFromSubtree((BaseView)v); } // Removes absolute
     * positioned children from the root view. if
     * (((BaseView)v).absoluteChildViews != null) { Enumeration abs =
     * ((BaseView)v).absoluteChildViews.elements(); while
     * (abs.hasMoreElements()) { ab = (View)abs.nextElement();
     * ab.removeFromTree(); } } } } }
     */
    public void repaintDocument()
    {
        //Log.debug("Repainting document!");
        renderer.refresh();
        //        renderer.refresh(posX, posY, (int)prefSize.getWidth(),
        // (int)prefSize.getHeight());
    }

    public void repaintDocument(View view)
    {
        dimensions.updateViewDimensions(style);
        if (((BaseView) view).hasAbsolutePosition())
            ((BaseView) view).setAbsoluteCoordinates();
        ((BaseView) view).bgColor = null; // Otherwise bg color won't change.
        if ((style != null) && (style.getPropertyCSSValue("border-top-style") != null))
            border = new CSSBorder(style);
        renderer.refresh(view);
    }

    public void repaintView()
    {
        this.paint(renderer.getGraphicsContext());
    }

    public Vector getChildren()
    {
        return childViews;
    }

    public boolean hasAbsoluteChildren()
    {
        if (absoluteChildViews == null)
            return false;
        return !absoluteChildViews.isEmpty();
    }

    public void setSizeRequirements()
    {
        int maxWidth = 0, maxHeight = 0, minWidth = 0, minHeight = 0;
        if (childViews != null)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                View view = (View) e.nextElement();
                view.setSizeRequirements();
                int outerWidth = ((BaseView) view).getDimensions().getOuterSize(View.X_AXIS);
                int outerHeight = ((BaseView) view).getDimensions().getOuterSize(View.Y_AXIS);
                maxWidth += view.getMaximumSpan(View.X_AXIS) + outerWidth;
                maxHeight += view.getMaximumSpan(View.Y_AXIS) + outerHeight;
                minWidth = Math.max(minWidth, view.getMinimumSpan(View.X_AXIS)) + outerWidth;
                minHeight = Math.max(minHeight, view.getMinimumSpan(View.Y_AXIS)) + outerHeight;
            }
        }
        sizeRequirements.setMaximumSpan(new Dimension(maxWidth, maxHeight));
        sizeRequirements.setPreferredSpan(new Dimension(maxWidth, maxHeight));
        sizeRequirements.setMinimumSpan(new Dimension(minWidth, minHeight));
    }

    public SizeRequirements getSizeRequirements()
    {
        return sizeRequirements;
    }

    /**
     * Moves view and its children. Negative values moves to up or left.
     */
    public void moveView(int axis, int pixels)
    {
        if (axis == View.X_AXIS)
            posX = posX + pixels;
        else
            posY = posY + pixels;

        BaseView child;
        Enumeration e = childViews.elements();
        while (e.hasMoreElements())
        {
            child = (BaseView) e.nextElement();
            child.moveChildViews(axis, pixels);
        }
    }

    /**
     * Checks if this view is inside TableView. This is called for every block
     * at the moment. Optimization wanted.
     * 
     * @return boolean
     */
    public boolean isInTable()
    {
        if (getParentView() == null)
            return false;
        else if (getParentView() instanceof TableView.CellView)
            return true;
        else
            return ((BaseView) getParentView()).isInTable();
    }
    /**
     * Contains all the dimensions of a view.
     *  
     */
    protected class ViewDimensions
    {
        private int marginLeft = 0, marginRight = 0, marginTop = 0, marginBottom = 0, paddingLeft = 0,
                paddingRight = 0, paddingTop = 0, paddingBottom = 0, borderLeftWidth = 0,
                borderRightWidth = 0, borderTopWidth = 0, borderBottomWidth = 0, width = 0, height = 0,
                widthPercentage = 100, heightPercentage = 100;
        protected boolean widthDefined = false;
        protected boolean heightDefined = false;

        public ViewDimensions()
        {
        }

        public ViewDimensions(CSSStyleDeclaration style)
        {
            updateViewDimensions(style);
        }

        public void updateViewDimensions(CSSStyleDeclaration style)
        {
            if (style != null)
            {
                CSSValue value = null;
                // TODO: px and pt etc. values... plus CSS width and height
                marginLeft = (int)(StyleGenerator.getMeasure(style, CSS_Property.MARGIN_LEFT) );
                marginRight = (int)(StyleGenerator.getMeasure(style, CSS_Property.MARGIN_RIGHT) );
                if (!(BaseView.this instanceof InlineView))
                    marginTop = (int)(StyleGenerator.getMeasure(style, CSS_Property.MARGIN_TOP) );
                if (!(BaseView.this instanceof InlineView))
                    marginBottom = (int)(StyleGenerator.getMeasure(style, CSS_Property.MARGIN_BOTTOM) );
                if (border != null)
                {
                    borderLeftWidth = (border.getBorderWidth(CSSBorder.LEFT));
                    borderRightWidth = (border.getBorderWidth(CSSBorder.RIGHT));
                    borderTopWidth = (border.getBorderWidth(CSSBorder.TOP));
                    borderBottomWidth = (border.getBorderWidth(CSSBorder.BOTTOM));
                }
                paddingLeft = (int)(StyleGenerator.getMeasure(style, CSS_Property.PADDING_LEFT));
                paddingRight = (int)(StyleGenerator.getMeasure(style, CSS_Property.PADDING_RIGHT));
                paddingTop = (int)(StyleGenerator.getMeasure(style, CSS_Property.PADDING_TOP));
                paddingBottom = (int)(StyleGenerator.getMeasure(style, CSS_Property.PADDING_BOTTOM));
                int width;
                int height;
                if (StyleGenerator.isPercentageValue(style, CSS_Property.WIDTH))
                    hasRelativeWidth = true;
                if (StyleGenerator.isPercentageValue(style, CSS_Property.HEIGHT))
                    hasRelativeHeight = true;
                if ((width = StyleGenerator.getMeasure(style, CSS_Property.WIDTH)) > 0)
                {
                    if (hasRelativeWidth)
                        widthPercentage = width;
                    else
                    {
                        this.width = width + getOuterSize(View.X_AXIS);
                        widthDefined = true;
                    }
                }
                if ((height = StyleGenerator.getMeasure(style, CSS_Property.HEIGHT)) > 0)
                {
                    if (hasRelativeHeight)
                        heightPercentage = height;
                    else
                        this.height = height + getOuterSize(View.Y_AXIS);
                    heightDefined = true;
                }
            }
        }
/*        public void setZoom(double zoom)
        { 
        	System.out.println("((((((((((((((((((((--"+zoom+"---dimension");
        	borderLeftWidth = (int)(borderLeftWidth * zoom);
            borderRightWidth = (int)(borderRightWidth* zoom);
            borderTopWidth = (int)(borderTopWidth* zoom);
            borderBottomWidth = (int)(borderBottomWidth * zoom);
        }
        */
        public int getViewWidth()
        {
            return width;
        }

        public int getViewHeight()
        {
            return height;
        }

        public void setContentWidth(int w)
        {
            if (!widthDefined)
                width = w + getOuterSize(View.X_AXIS);
        }

        public void setContentHeight(int h)
        {
            if (heightDefined && !(BaseView.this instanceof ScrollBlockView))
                height = Math.max(height, h + getOuterSize(View.Y_AXIS));
            else if (!(BaseView.this instanceof ScrollBlockView))
                height = h + getOuterSize(View.Y_AXIS);
            else if (!heightDefined && BaseView.this instanceof ScrollBlockView)
                height = h + getOuterSize(View.Y_AXIS);
        }

        public void setViewWidth(int w)
        {
            if (!widthDefined)
                width = w;
        }

        public void setViewHeight(int h)
        {
            if (heightDefined)
                height = Math.max(height, h);
            else
                height = h;
        }

        public int getWidthPercentage()
        {
            return widthPercentage;
        }

        public int getMaxContentSize(int axis)
        {
            if (axis == View.X_AXIS)
                return width - getOuterSize(View.X_AXIS);
            else
                return height - getOuterSize(View.Y_AXIS);
        }

        public int getXPadding()
        {
            return paddingLeft + paddingRight;
        }

        public int getYPadding()
        {
            return paddingTop + paddingBottom;
        }

        public int getTopPadding()
        {
            return paddingTop;
        }

        public int getBottomPadding()
        {
            return paddingBottom;
        }

        public int getLeftPadding()
        {
            return paddingLeft;
        }

        public void setLeftPadding(int value)
        {
            paddingLeft = value;
        }

        public void setLeftMargin(int value)
        {
            marginLeft = value;
        }

        public int getOuterSize(int axis)
        {
            if (axis == View.X_AXIS)
                return marginLeft + marginRight + borderLeftWidth + borderRightWidth + paddingLeft
                        + paddingRight;
            else
                return marginTop + marginBottom + borderTopWidth + borderBottomWidth + paddingTop
                        + paddingBottom;
        }

        public int getContentOrigin(int axis)
        {
            if (axis == View.X_AXIS)
                return posX + marginLeft + borderLeftWidth + paddingLeft;
            else
                return posY + marginTop + borderTopWidth + paddingTop;
        }

        public Rectangle getMarginRectangle()
        {
            return new Rectangle(posX + marginLeft, posY + marginTop, width - marginLeft - marginRight,
                    height - marginTop - marginBottom);
        }

        public Rectangle getBorderRectangle()
        {
            int x = posX + marginLeft + borderLeftWidth;
            int y = posY + marginTop + borderTopWidth;
            int rWidth = width - marginLeft - marginRight - borderLeftWidth - borderRightWidth;
            int rHeight = height - marginTop - marginBottom - borderBottomWidth - borderTopWidth;
            return new Rectangle(x, y, rWidth, rHeight);
        }

        public int getLeftMargin()
        {
            return marginLeft;
        }

        public int getTopMargin()
        {
            return marginTop;
        }

        public int getLeftOuterSize()
        {
            return marginLeft + borderLeftWidth + paddingLeft;
        }

        public int getTopOuterSize()
        {
            return marginTop + borderTopWidth + paddingTop;
        }

        public int getBorderLeftWidth()
        {
            return borderLeftWidth;
        }

        public int getBorderRightWidth()
        {
            return borderRightWidth;
        }

        public int getBorderTopWidth()
        {
            return borderTopWidth;
        }

        public int getBorderBottomWidth()
        {
            return borderBottomWidth;
        }

        public boolean isWidthDefined()
        {
            return widthDefined;
        }

        public boolean isHeightDefined()
        {
            return heightDefined;
        }
    }
}
