/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jan 21, 2005
 */
package fi.hut.tml.xsmiles.csslayout.view;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.CSS_Property;

/**
 * @author mpohja
 */
public class ListItemView extends BlockView
{
    boolean markerInside = false;

    /**
     * @param r
     * @param node
     * @param parent
     */
    public ListItemView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
        if (getStyle().getPropertyCSSValue(CSS_Property.LIST_STYLE_POSITION) != null
                && getStyle().getPropertyValue(CSS_Property.LIST_STYLE_POSITION).equals("inside"))
            markerInside = true;
    }

    public void doLayout()
    {
        if (markerInside && childViews.get(0) instanceof ParagraphView)
            ((ParagraphView) childViews.get(0)).setIndent(30);
        super.doLayout();
    }

    public int getLetterPosition()
    {
        int pos = 0;
        ParagraphView.RowView row = getFirstRow();
        if (row != null)
            pos = row.posY;
        return pos;
    }

    public int getBulletPosition()
    {
        int pos = 0;
        ParagraphView.RowView row = getFirstRow();
        if (row != null)
            pos = row.posY + (row.getViewHeight() / 2);
        return pos;
    }

    ParagraphView.RowView getFirstRow()
    {
        View view = getFirstChild();
        while (!(view instanceof ParagraphView.RowView) && view != null)
            view = (View) view.getFirstChild();
        return (ParagraphView.RowView) view;
    }
}