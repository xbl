/*
 * ListView.java
 * 
 * Created on October 16, 2003, 1:48 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.awt.FontMetrics;
import java.awt.image.ImageObserver;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.CSS_Property;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import org.w3c.dom.Node;

/**
 * @author mpohja
 */
public class ListView extends BaseView
{
    private int bulletgap = 2;
    private String markertype = "disc";

    Image image;
    MediaTracker tracker;
    ImageObserver imageObserver;

    /** Creates a new instance of ListView */
    public ListView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
        dimensions.setLeftPadding(30);
        if (parent.getStyle() != null)
        {
            String type = parent.getStyle().getPropertyValue(CSS_Property.LIST_STYLE_TYPE);
            if (!type.equals(""))
                markertype = type;

            URL url = null;
            if (parent.getStyle().getPropertyCSSValue(CSS_Property.LIST_STYLE_IMAGE) != null)
            {
                CSSValue value = parent.getStyle().getPropertyCSSValue(CSS_Property.LIST_STYLE_IMAGE);
                if (value != null)
                {
                    try
                    {
                        if (value instanceof XSmilesCSSValueImpl)
                        {
                            XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl) value;
                            if (xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_URI)
                            {
                                try
                                {
                                    url = new URL(xValue.getStringValue());
                                    // add this to the list of resources, so
                                    // that XML
                                    // signature can get all references
                                    Resource resource = new Resource(url, Resource.RESOURCE_IMAGE, null);
                                    ((XSmilesElementImpl) parent.getDOMElement())
                                            .getResourceReferencer().addResource(resource);
                                }
                                catch (MalformedURLException mue)
                                {
                                    Log.error(mue);
                                }
                            }
                        }

                        image = Toolkit.getDefaultToolkit().getImage(url);
                    }
                    catch (Exception e)
                    {
                        Log.error(e);
                    }
                    tracker = new MediaTracker(renderer.getComponent());
                    imageObserver = renderer.getComponent();
                    if (image != null)
                        loadImage(image);

                }
            }
        }
    }

    /**
     * Loads the image, returning only when the image is loaded.
     * 
     * @param image
     *            the image
     */
    void loadImage(Image image)
    {
        tracker.addImage(image, 1);
        try
        {
            tracker.waitForID(1, 0);
        }
        catch (InterruptedException e)
        {
            Log.error("INTERRUPTED while loading Image " + e);
        }
        //loadStatus = tracker.statusID(id, false);
        tracker.removeImage(image, 1);
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        super.doLayout();
        int y = 0;//(int)this.getPreferredSpan(View.Y_AXIS);
        int x = 0;//(int)this.getPreferredSpan(View.X_AXIS);
        if (childViews != null && childViews.size() > 0)
        {
            Enumeration e = childViews.elements();
            while (e.hasMoreElements())
            {
                BaseView v = (BaseView) e.nextElement();
                v.setAbsolutePosition(this.getDimensions().getContentOrigin(View.X_AXIS), this
                        .getDimensions().getContentOrigin(View.Y_AXIS)
                        + y);
                v.doLayout();
                y = y + v.getViewHeight();
                x = Math.max(x, v.getViewWidth());
                if (v.hasRelativePosition())
                    v.handleRelativeView();
            }
            setContentWidth(x);
            setContentHeight(y);
        }
        else
        {
            setContentWidth(0);
            setContentHeight(0);
        }
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (visible)
        {
            if (childViews != null)
            {
                int i = 0;
                Rectangle clip = g.getClipBounds();
                Enumeration e = childViews.elements();
                while (e.hasMoreElements())
                {
                    View v = (View) e.nextElement();
                    i++;
                    if (BlockView.insideClip(clip, v))
                    {
                        v.paint(g);
                        drawMarker(g, v, i);
                    }
                }
            }
            super.paint(g);
        }
    }

    private void drawMarker(Graphics g, View view, int index)
    {
        String type = "";
        CSSStyleDeclaration style = view.getStyle();
        type = style.getPropertyValue("list-style-type");
        if (type.equals(""))
            type = markertype;
        int x = posX;
        if (parent.getStyle().getPropertyCSSValue(CSS_Property.LIST_STYLE_POSITION) != null
                && parent.getStyle().getPropertyValue(CSS_Property.LIST_STYLE_POSITION)
                        .equals("inside"))
            x += 30;
        int y = ((ListItemView) view).getLetterPosition();

        if (image != null)
            drawImage(g, x, y);
        else if (type.equals("none"))
            return;
        else if (type.equals("decimal"))
            drawLetter(g, '1', x, y, index);
        else if (type.equals("lower-roman"))
            drawLetter(g, 'i', x, y, index);
        else if (type.equals("upper-roman"))
            drawLetter(g, 'I', x, y, index);
        else if (type.equals("lower-alpha"))
            drawLetter(g, 'a', x, y, index);
        else if (type.equals("upper-alpha"))
            drawLetter(g, 'A', x, y, index);
        else
        {
            y = ((ListItemView) view).getBulletPosition();
            drawBullet(g, x, y, type);
        }
    }

    void drawBullet(Graphics g, int x, int y, String type)
    {
        if (type == null)
            type = "disk";
        g.setColor(Color.black);
        // y is middle of the row. We lift it up 3 pixels to draw bullet into
        // right position.
        y = y - 3;
        if (type.equals("square"))
            g.fillRect(x, y, 7, 7);
        else if (type.equals("circle"))
            g.drawOval(x, y, 7, 7);
        else
            g.fillOval(x, y, 7, 7);
    }

    void drawLetter(Graphics g, char letter, int ax, int ay, int index)
    {
        g.setColor(Color.black);
        String str = formatItemNum(index, letter) + ".";
        FontMetrics fm = g.getFontMetrics();
        //			int width = fm.stringWidth(str);
        //			int x = ax - bulletgap - width;
        int y = ay + fm.getHeight() - fm.getDescent();// + fm.getLeading()+0;
        g.drawString(str, ax, y);
    }

    void drawImage(Graphics g, int x, int y)
    {
        g.drawImage(image, x, y, renderer.getComponent());
    }

    //		private int getItemIndex(Element element, int childIndex)
    //		{
    //			int itemIndex = childIndex;
    //			for (int counter = childIndex; counter >= 0; counter--)
    //			{
    //				if (!element.getElement(counter).getName().equals("li"))
    //					itemIndex--;
    //			}
    //			return itemIndex + 1;
    //		}

    /**
     * Converts the item number into the ordered list number (i.e. 1 2 3, i ii
     * iii, a b c, etc.
     * 
     * @param itemNum
     *            number to format
     * @param type
     *            type of ordered list
     */
    String formatItemNum(int itemNum, char type)
    {
        String numStyle = "1";
        boolean uppercase = false;
        String formattedNum;
        switch (type)
        {
            case '1' :
            default :
                formattedNum = String.valueOf(itemNum);
                break;
            case 'A' :
                uppercase = true;
            // fall through
            case 'a' :
                formattedNum = formatAlphaNumerals(itemNum);
                break;
            case 'I' :
                uppercase = true;
            // fall through
            case 'i' :
                formattedNum = formatRomanNumerals(itemNum);
        }
        if (uppercase)
        {
            formattedNum = formattedNum.toUpperCase();
        }
        return formattedNum;
    }

    /**
     * Converts the item number into an alphabetic character
     * 
     * @param itemNum
     *            number to format
     */
    String formatAlphaNumerals(int itemNum)
    {
        String result = "";
        if (itemNum > 26)
        {
            result = formatAlphaNumerals(itemNum / 26) + formatAlphaNumerals(itemNum % 26);
        }
        else
        {
            // -1 because item is 1 based.
            result = String.valueOf((char) ('a' + itemNum - 1));
        }
        return result;
    }

    /* list of roman numerals */

    static final char[][] romanChars = {{'i', 'v'}, {'x', 'l'}, {'c', 'd'}, {'m', '?'},};

    /**
     * Converts the item number into a roman numeral
     * 
     * @param num
     *            number to format
     */
    String formatRomanNumerals(int num)
    {
        return formatRomanNumerals(0, num);
    }

    /**
     * Converts the item number into a roman numeral
     * 
     * @param num
     *            number to format
     */
    String formatRomanNumerals(int level, int num)
    {
        if (num < 10)
        {
            return formatRomanDigit(level, num);
        }
        else
        {
            return formatRomanNumerals(level + 1, num / 10) + formatRomanDigit(level, num % 10);
        }
    }

    /**
     * Converts the item number into a roman numeral
     * 
     * @param level
     *            position
     * @param num
     *            digit to format
     */
    String formatRomanDigit(int level, int digit)
    {
        String result = "";
        if (digit == 9)
        {
            result = result + romanChars[level][0];
            result = result + romanChars[level + 1][0];
            return result;
        }
        else if (digit == 4)
        {
            result = result + romanChars[level][0];
            result = result + romanChars[level][1];
            return result;
        }
        else if (digit >= 5)
        {
            result = result + romanChars[level][1];
            digit -= 5;
        }
        for (int i = 0; i < digit; i++)
        {
            result = result + romanChars[level][0];
        }
        return result;
    }

    public void drawBorder(Graphics g)
    {
        g.setColor(Color.black);
        Rectangle rect = this.getRectangle();
        //Log.debug("drawing borders at"+rect);
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
    }
}