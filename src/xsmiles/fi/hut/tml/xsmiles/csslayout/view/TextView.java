/*
 * TextView.java
 * 
 * Created on September 22, 2003, 2:57 PM
 */

package fi.hut.tml.xsmiles.csslayout.view;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.Node;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.font.TextAttribute;

import java.text.AttributedString;
import java.text.BreakIterator;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.*;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility;
/**
 * @author mpohja
 */
public class TextView extends BaseView
{
    protected String text;
    protected Font font;
    protected Color color;
    private int descent = 0;
    private JDKCompatibility compability;
    private Object attributedString;

    public static short UNDERLINE = 10;
    public static short LINETHROUGH = 15;
    public static short ALIGNSUB = 20;
    public static short ALIGNSUPER = 25;

    /** Creates a new instance of TextView */
    public TextView(CSSRenderer r, Node node, View parent)
    {
        super(r, node, parent);
    }

    public void setText(String atext)
    {
        // Check whitespace value before trim!
        CSSValue whitespace = this.style.getPropertyCSSValue(CSS_Property.WHITESPACE);
        if (whitespace != null)
        {
            String ws = whitespace.getCssText();
            if (ws.equals("pre") || ws.equals("pre-wrap"))
                this.text = atext;
            else if (ws.equals("pre-line"))
                this.text = Utilities.prelineHTMLContent(atext);
            else
                this.text = Utilities.trimHTMLContent(atext);
        }
        else
            this.text = Utilities.trimHTMLContent(atext);

        CSSValue transform = this.style.getPropertyCSSValue(CSS_Property.TEXT_TRANSFORM);
        if (transform != null)
        {
            String tf = transform.getCssText();
            if (tf.equals("uppercase"))
                this.text = this.text.toUpperCase();
            else if (tf.equals("lowercase"))
                this.text = this.text.toLowerCase();
        }
        //Log.debug("TextView, added text: "+atext);
        FontMetrics metrics = renderer.getComponent().getFontMetrics(font);
        int width = metrics.stringWidth(text) + getDimensions().getXPadding();
        int height = metrics.getHeight() + getDimensions().getYPadding();
        descent = metrics.getDescent();
        //this.prefSize=new Dimension(width, height);
        setContentHeight(height);
        setContentWidth(width);
        
        setDecorations();
    }

    public String getText()
    {
        return text;
    }

    public Font getFont()
    {
        return font;
    }

    private Font getCSSFont()
    {
        return StyleGenerator.getCSSFontObj(style);
    }

    private Color getCSSColor()
    {
        return StyleGenerator.parseColor(style.getPropertyCSSValue("color"));
    }

    public void setStyle(CSSStyleDeclaration s)
    {
        super.setStyle(s);
        font = getCSSFont();
        color = getCSSColor();
    }

    public CSSStyleDeclaration getStyle()
    {
        return style;
    }

    public void setSizeRequirements()
    {
        FontMetrics metrics = renderer.getComponent().getFontMetrics(font);
        int width = metrics.stringWidth(text) + getDimensions().getXPadding();
        int height = metrics.getHeight() + getDimensions().getYPadding();

        sizeRequirements.setMaximumSpan(new Dimension(width, height));
        sizeRequirements.setPreferredSpan(new Dimension(width, height));
        sizeRequirements.setMinimumSpan(new Dimension(getLongestWordSpan(), height));
    }

    private int getLongestWordSpan()
    {
        int maxStart = 0, maxEnd = 0;
        BreakIterator boundary = BreakIterator.getWordInstance();
        boundary.setText(text);
        int start = boundary.first();
        for (int end = boundary.next(); end != BreakIterator.DONE; start = end, end = boundary.next())
        {
            if ((end - start) > (maxEnd - maxStart))
            {
                maxStart = start;
                maxEnd = end;
            }
        }
        FontMetrics metrics = renderer.getComponent().getFontMetrics(font);
        int span = metrics.stringWidth(text.substring(maxStart, maxEnd));
        return span;

    }

    private void setDecorations()
    {
        CSSValue decoration = this.style.getPropertyCSSValue(CSS_Property.TEXT_DECORATION);
        CSSValue vertAlign = this.style.getPropertyCSSValue(CSS_Property.VERTICAL_ALIGN);
        short[] properties = new short[2];
        if (decoration != null || vertAlign != null)
        {
            String deco = null, valign = null;
            if (decoration != null)
                deco = decoration.getCssText();
            if (vertAlign != null)
            valign = vertAlign.getCssText();
            compability = CompatibilityFactory.getCompatibility();
            attributedString = compability.createAttributedString(text, font);

            if (deco != null && deco.indexOf("underline") >= 0)
                compability.setTextAttribute(attributedString, UNDERLINE);
            if (deco != null && deco.indexOf("line-through") >= 0)
                compability.setTextAttribute(attributedString, LINETHROUGH);
            if (valign != null && valign.equals("sub"))
                compability.setTextAttribute(attributedString, ALIGNSUB);
            else if (valign != null && valign.equals("super"))
                compability.setTextAttribute(attributedString, ALIGNSUPER);
        }
    }

    /** do, or redo layout for this and children */
    public void doLayout()
    {
        if (style != null)
        {

        }
        // use font metrix here
        FontMetrics metrics = renderer.getComponent().getFontMetrics(font);
        int width = metrics.stringWidth(text) + getDimensions().getXPadding();
        int height = metrics.getHeight() + getDimensions().getYPadding();
        descent = metrics.getDescent();
        //this.prefSize=new Dimension(width, height);
        setContentHeight(height);
        setContentWidth(width);
        //Log.debug("Size: "+ renderer.getMLFC().getContainer().getWidth());
    }

    /** paints this view */
    public void paint(Graphics g)
    {
        if (this.text != null)
        {
            /*
             * if (this.renderer.antiAlias) { if
             * (font.getSize()>this.renderer.minAlias){
             * ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, //
             * Anti-alias! RenderingHints.VALUE_ANTIALIAS_ON); } }
             */
            g.setColor(color);
            g.setFont(font);
            //Log.debug("TextView draw string at"+posX+" :"+posY+": "+text);
            int x = posX + getDimensions().getLeftPadding();
            int y = posY + (int) this.getViewHeight() - getDimensions().getBottomPadding() - descent;

            if (compability != null)
                compability.drawString(text, font, g, x, y, attributedString);
            else
                g.drawString(this.text, x, y);
        }
        super.paint(g);
    }

    public void drawBorder(Graphics g)
    {
        g.setColor(Color.blue);
        Rectangle rect = this.getRectangle();
        //Log.debug("drawing borders at"+rect);
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
    }

}