/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.csslayout;

import java.io.*;
import java.lang.String;
import java.lang.Float;
import java.net.URL;
import java.net.MalformedURLException;
import java.lang.reflect.Array;
import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;
import fi.hut.tml.xsmiles.Log;

import com.steadystate.css.*;
import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.parser.LexicalUnitImpl;
import org.w3c.dom.css.*;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;
import fi.hut.tml.xsmiles.csslayout.view.*;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;

/**
 * @author alessandro
 */
public class StyleGenerator
{

    public static final String ATTRIBUTE_FONT_FAMILY = "font-family";

    public static final String DEFAULT_FONT_STR = "serif";
    public static final int DEFAULT_FONT_SIZE = 16;
    public static final int DEFAULT_FONT_WEIGHT = Font.PLAIN;
    public static final String DEFAULT_FONT_STYLE = "normal";
    public static final Font DEFAULT_FONT = new Font(DEFAULT_FONT_STR, DEFAULT_FONT_WEIGHT,
            DEFAULT_FONT_SIZE);
    /** all fonts will be zoomed by this factor */
    private static double fontzoom = 1.0;
    public static Color transparentColor;

    private static Hashtable fontMapping;
    private static Hashtable bh;
    private static Hashtable createdFonts = new Hashtable(20);
    private static Hashtable createdColor = new Hashtable(20);
    private static Hashtable fontsFromSameStyle = new Hashtable(20);
    
    private static int measure;
    private static CSSValue value;

    static
    {
        try
        {
            transparentColor = CompatibilityFactory.getCompatibility().createTransparentColor();
        }
        catch (Exception e)
        {
            // Fallback
            transparentColor = new Color(0, 0, 0);
        }
    }
    
    public static void clearCaches()
    {
        if (fontMapping!=null) fontMapping.clear();
        if (bh!=null) bh.clear();
        if (createdFonts!=null) createdFonts.clear();
        if (createdColor!=null) createdColor.clear();
        if (fontsFromSameStyle!=null) fontsFromSameStyle.clear();
    }

    /** to set the zoom, for instance by the UI */
    public static void setFontZoom(double zoom)
    {
        fontzoom = zoom;
        createdFonts.clear();
    }
    public static double getZoom()
    {
        return fontzoom;
    }

    static Hashtable getValidFontNameMapping()
    {
        if (fontMapping == null)
        {
            String[] names = null;

            // Use the 1.2 GraphicsEnvironment for getting font names,
            // if we can.
            try
            {
                Class ge = Class.forName("java.awt.GraphicsEnvironment");
                if (ge != null)
                {
                    Method method = ge.getDeclaredMethod("getLocalGraphicsEnvironment", null);
                    if (method != null)
                    {
                        Object instance = method.invoke(ge, null);
                        if (instance != null)
                        {
                            method = ge.getMethod("getAvailableFontFamilyNames", null);
                            if (method != null)
                            {
                                names = (String[]) method.invoke(instance, null);
                            }
                        }
                    }
                }
            }
            catch (Throwable th)
            {
            }
            if (names == null)
            {
                // We can't, use the Toolkit method.
                names = Toolkit.getDefaultToolkit().getFontList();
            }
            if (names != null)
            {
                fontMapping = new Hashtable(names.length * 2);
                for (int counter = names.length - 1; counter >= 0; counter--)
                {
                    // Put both lowercase and case value in table.
                    fontMapping.put(names[counter].toLowerCase(), names[counter]);
                    fontMapping.put(names[counter], names[counter]);
                }
            }
            else
            {
                fontMapping = new Hashtable(1);
            }
        }
        return fontMapping;
    }

    static Hashtable getBasicFonts()
    {

        bh = new Hashtable();
        bh.put("serif", "Serif");
        bh.put("times", "Times New Roman");
        bh.put("sans-serif", "SansSerif");
        bh.put("helvetica", "SansSerif");
        bh.put("geneva", "SansSerif");
        bh.put("verdana", "Verdana");
        bh.put("monospace", "Monospaced");
        bh.put("monaco", "Monospaced");
        bh.put("courier", "Courier New");
        bh.put("courier new", "Courier New");
        bh.put("arial", "Arial");
        bh.put("fantasy", "Blacksmith Delight SemiWide");
        bh.put("cursive", "Monotype Corsiva");

        return bh;
    }

    /** Creates a new instance of styleGenerator */
    public StyleGenerator()
    {
    }

    private static class AwtFontInfo
    {
        protected int style = DEFAULT_FONT_WEIGHT;
        protected int size = DEFAULT_FONT_SIZE;
        protected String name = DEFAULT_FONT_STR;

        protected AwtFontInfo()
        {
        }

        protected AwtFontInfo(Font f)
        {
            style = f.getStyle();
            size = f.getSize();
            name = f.getName();
        }

        protected void setFontFamily(CSSStyleDeclaration d)
        {
            CSSValue font_family = d.getPropertyCSSValue(CSS_Property.FONT_FAMILY);
            String checkedFont = checkFontName(font_family);
            if (!checkedFont.equals(""))
            {
                name = checkedFont;
            }
        }

        protected void setFontSize(CSSStyleDeclaration d)
        {
            setFontSize(d, 1.0);
        }

        protected void setFontSize(CSSStyleDeclaration d, double zoom)
        {
            size = (int) (zoom * (double) getFontSize(d.getPropertyCSSValue(CSS_Property.FONT_SIZE)));
        }

        protected void setFontWeight(CSSStyleDeclaration d)
        {
            int weight = (int) getFontWeight(d.getPropertyCSSValue(CSS_Property.FONT_WEIGHT));
            if (weight <= 400)
            {
                style &= ~Font.BOLD;
            }
            else if (weight > 400)
            {
                style |= Font.BOLD;
            }
        }

        protected void setFontStyle(CSSStyleDeclaration d)
        {
            String newStyle = DEFAULT_FONT_STYLE;

            newStyle = getFontStyle(d.getPropertyCSSValue(CSS_Property.FONT_STYLE));
            if (newStyle.equals("normal"))
            {
                style &= ~Font.ITALIC;
            }
            else if (newStyle.equals("italic"))
            {
                style |= Font.ITALIC;
            }
        }

        protected Font getFont()
        {
            int hashSize = createdFonts.size();
            String fontString = new String(name + style + size);
            if (createdFonts.containsKey(fontString))
            {
                return ((Font) createdFonts.get(fontString));
            }
            else
            {
                Font newFont = new Font(name, style, (int) ((double) size * (double) fontzoom));
                createdFonts.put(fontString, newFont);
                return newFont;
            }
        }
    }

    public static Font getCSSFontObj(CSSStyleDeclaration dec)
    {
        return getCSSFontObj(dec, 1.0);
    }

    public static Font getCSSFontObj(CSSStyleDeclaration dec, double zoom) // zoom
                                                                           // for
                                                                           // SVG
    {
    	if (fontsFromSameStyle.containsKey(dec)){
    		return ((Font)fontsFromSameStyle.get(dec));
    	}else{
	        AwtFontInfo font = new AwtFontInfo();
	        font.setFontFamily(dec);
	        font.setFontSize(dec,zoom);
	        font.setFontWeight(dec);
	        font.setFontStyle(dec);
	        
	        Font f = font.getFont();
	        fontsFromSameStyle.put(dec, f);
	        return f;
//	        return font.getFont();
    	}
    }

    public static Font setFontFamily(Font f, CSSStyleDeclaration d)
    {
        AwtFontInfo font = new AwtFontInfo(f);
        font.setFontFamily(d);
        return font.getFont();
    }

    public static Font setFontSize(Font f, CSSStyleDeclaration d)
    {
        return setFontSize(f, d, 1.0);
    }

    public static Font setFontSize(Font f, CSSStyleDeclaration d, double zoom)
    {
        AwtFontInfo font = new AwtFontInfo(f);
        font.setFontSize(d);
        return font.getFont();
    }

    public static Font setFontWeight(Font f, CSSStyleDeclaration d)
    {
        AwtFontInfo font = new AwtFontInfo(f);
        font.setFontWeight(d);
        return font.getFont();
    }

    public static Font setFontStyle(Font f, CSSStyleDeclaration d)
    {
        AwtFontInfo font = new AwtFontInfo(f);
        font.setFontStyle(d);
        return font.getFont();
    }

    public static int getFontSize(CSSValue sizeValue)
    {
        int font_size = DEFAULT_FONT_SIZE;
        if (sizeValue != null && sizeValue instanceof CSSPrimitiveValue)
        {
            CSSPrimitiveValue pSizeValue = (CSSPrimitiveValue) sizeValue;
            font_size = (int) (pSizeValue.getFloatValue(CSSPrimitiveValue.CSS_PX) + .5);
        }
        return font_size;
    }

    public static int getFontWeight(CSSValue weightValue)
    {
        int font_weight = DEFAULT_FONT_WEIGHT;
        if (weightValue != null && weightValue instanceof CSSPrimitiveValue)
        {
            CSSPrimitiveValue pWeightValue = (CSSPrimitiveValue) weightValue;
            font_weight = (int) (pWeightValue.getFloatValue(CSSPrimitiveValue.CSS_NUMBER) + .5);
        }
        return font_weight;
    }

    public static String getFontStyle(CSSValue styleValue)
    {
        String font_style = DEFAULT_FONT_STYLE;
        if (styleValue != null && styleValue instanceof CSSPrimitiveValue)
        {
            CSSPrimitiveValue pStyleValue = (CSSPrimitiveValue) styleValue;
            font_style = pStyleValue.getStringValue();
        }
        return font_style;
    }

    static String checkFontName(CSSValue valueToCheck)
    {
        if (valueToCheck == null)
            return "";
        String family = "";
        String familyToCheck = "";

        Hashtable JDKMap = getValidFontNameMapping();
        StringTokenizer tokenizer = new StringTokenizer(valueToCheck.getCssText(), ",");

        while (tokenizer.hasMoreTokens())
        {
            familyToCheck = tokenizer.nextToken().replace('"', ' ').trim().toLowerCase();
            if (JDKMap.containsKey(familyToCheck))
            {
                return (String) JDKMap.get(familyToCheck);
            }
            else
            {
                familyToCheck = mapAlternativeFont(familyToCheck);
                if (!familyToCheck.equals(""))
                {
                    return familyToCheck;
                }
            }
        }
        return "";
    }

    static String mapAlternativeFont(String stringToMap)
    {
        Hashtable myMap = getBasicFonts();
        if (myMap.containsKey(stringToMap))
        {
            return (String) myMap.get(stringToMap);
        }
        return "";
    }

    public static Color parseColor(CSSValue value, Color defaultColor)
    {
        if (value == null)
            return defaultColor;
        return parseColor(value);
    }
    public static boolean isTransparent(Color color)
    {
        return CompatibilityFactory.getCompatibility().isTransparentColor(color);    
    }
    
    public static Color getTransparentColor()
    {
        return transparentColor;
    }
    public static Color parseColor(CSSValue value)
    {
        String rgbString = null;
        Color rgb = null;
        if (value instanceof XSmilesCSSValueImpl)
        {
            XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl)value;
            rgbString = xValue.getCssText();
            if (createdColor.containsKey(rgbString))
            {
                rgb = ((Color) createdColor.get(rgbString));
            }
            else
            {
                if (xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_STRING
                        || xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT)
                {

                    rgbString = xValue.getCssText();

                    if (rgbString.equals("transparent"))
                    {
                        rgb = transparentColor;
                    }
                    else if (rgbString.equals("aqua"))
                    {
                        rgb = new Color(0x00, 0xff, 0xff);
                    }
                    else if (rgbString.equals("black"))
                    {
                        rgb = Color.black;
                    }
                    else if (rgbString.equals("blue"))
                    {
                        rgb = Color.blue;
                    }
                    else if (rgbString.equals("fuchsia"))
                    {
                        rgb = new Color(0xff, 0x00, 0xff);
                    }
                    else if (rgbString.equals("gray"))
                    {
                        rgb = Color.gray;
                    }
                    else if (rgbString.equals("green"))
                    {
                        rgb = new Color(0x00, 0x80, 0x00);
                    }
                    else if (rgbString.equals("lime"))
                    {
                        rgb = new Color(0x00, 0xff, 0x00);
                    }
                    else if (rgbString.equals("maroon"))
                    {
                        rgb = new Color(0x80, 0x00, 0x00);
                    }
                    else if (rgbString.equals("navy"))
                    {
                        rgb = new Color(0x00, 0x00, 0x80);
                    }
                    else if (rgbString.equals("olive"))
                    {
                        rgb = new Color(0x80, 0x80, 0x00);
                    }
                    else if (rgbString.equals("purple"))
                    {
                        rgb = new Color(0x80, 0x00, 0x80);
                    }
                    else if (rgbString.equals("red"))
                    {
                        rgb = Color.red;
                    }
                    else if (rgbString.equals("silver"))
                    {
                        rgb = new Color(0xc0, 0xc0, 0xc0);
                    }
                    else if (rgbString.equals("teal"))
                    {
                        rgb = new Color(0x00, 0x80, 0x80);
                    }
                    else if (rgbString.equals("white"))
                    {
                        rgb = Color.white;
                    }
                    else if (rgbString.equals("yellow"))
                    {
                        rgb = Color.yellow;
                    }
                }
                else if (xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_RGBCOLOR)
                {
                    rgbString = xValue.getCssText();
                    org.w3c.dom.css.RGBColor rgbValue = xValue.getRGBColorValue();

                    if ((rgbValue.getRed()).getPrimitiveType() == CSSPrimitiveValue.CSS_NUMBER
                            && (rgbValue.getGreen()).getPrimitiveType() == CSSPrimitiveValue.CSS_NUMBER
                            && (rgbValue.getBlue()).getPrimitiveType() == CSSPrimitiveValue.CSS_NUMBER)
                    {

                        rgb = new Color((int) (rgbValue.getRed().getFloatValue((short) 0) + .5),
                                (int) (rgbValue.getGreen().getFloatValue((short) 0) + .5),
                                (int) (rgbValue.getBlue().getFloatValue((short) 0) + .5));

                    }
                    else if ((rgbValue.getRed()).getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE
                            && (rgbValue.getGreen()).getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE
                            && (rgbValue.getBlue()).getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE)
                    {

                        rgb = new Color(
                                ((int) (rgbValue.getRed().getFloatValue((short) 0) + .5) * 255) / 100,
                                ((int) (rgbValue.getGreen().getFloatValue((short) 0) + .5) * 255) / 100,
                                ((int) (rgbValue.getBlue().getFloatValue((short) 0) + .5) * 255) / 100);

                    }
                }
                if (rgb != null)
                {
                    createdColor.put(rgbString, rgb);
                }
            }
        }
        // System.out.println("(((((((((((((((((((("+rgbString+"))))))))))))))))))))");
        return rgb;
    }

    public static String getStringValue(CSSStyleDeclaration style, String what)
    {

        if (!style.getPropertyValue(what).equals(""))
        {
            value = style.getPropertyCSSValue(what);
            if (value instanceof XSmilesCSSValueImpl)
            {
                XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl) value;
                if (xValue.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT)
                {
                    return xValue.getStringValue();
                }
            }
        }
        return null;
    }

    public static int getMeasure(CSSValue v)
    {
        // This isn't correct in all cases. A property can be greater than 0
        // even if
        // it isn't defined in stylesheet (e.g. width and height).
        measure = 0;
        try
        {
            value = v;
            //Log.debug("Style for :"+what+" _ : "+value);
            //Log.debug("Style: "+style);
            if (value instanceof XSmilesCSSValueImpl)
            {
                XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl) value;
                try
                {
                    measure = (int) (xValue.getFloatValue(CSSPrimitiveValue.CSS_PX)* getZoom());
                } catch (Exception e)
                {
                    Log.error("getMeasure:"+e);
                    measure=0;  
                }
            }   
        }
        catch (NumberFormatException e)
        {
            Log.error("ViewDimensions, unknown style. " + e.getMessage());
        }
        
        return measure;
    }
    
    public static int getMeasure(CSSStyleDeclaration style, String what)
    {
        // This isn't correct in all cases. A property can be greater than 0
        // even if
        // it isn't defined in stylesheet (e.g. width and height).
        measure = 0;

            if (!style.getPropertyValue(what).equals(""))
            {
                value = style.getPropertyCSSValue(what);
                measure = StyleGenerator.getMeasure(value);
            }      
        return measure;
    }
    
    public static int getImageSize(int orig)
    {
        return ((int)(orig * getZoom()));
    }
    
    public static int getScaledSize(int orig)
    {
        return ((int)(orig * getZoom()));
    }
 
    public static boolean isPercentageValue(CSSStyleDeclaration style, String property)
    {
        if (!style.getPropertyValue(property).equals(""))
        {
            value = style.getPropertyCSSValue(property);
            if (((CSSValueImpl)value).getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE)
            {
                Log.debug(property+" has percentage value... style: "+ style);
                return true;
            }
        }
        return false;
    }
}

