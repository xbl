/*
 * SizeRequirements.java
 * 
 * Created on January 16, 2004, 2:15 PM
 */

package fi.hut.tml.xsmiles.csslayout;

import java.awt.Dimension;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.csslayout.view.*;

/**
 * Contains information about view's size requirements. All the requirements are
 * view's content size requirements.
 * 
 * @author mpohja
 */
public class SizeRequirements
{
    protected Dimension minSize = new Dimension(), maxSize = new Dimension(),
            prefSize = new Dimension();

    protected boolean widthDefined = false;

    /** Creates a new instance of SizeRequirements */
    public SizeRequirements(CSSStyleDeclaration style)
    {
        int width, height;
        if ((width = StyleGenerator.getMeasure(style, CSS_Property.WIDTH)) > 0
                && !StyleGenerator.isPercentageValue(style, CSS_Property.WIDTH))
        {
            minSize.width = maxSize.width = prefSize.width = width;
            widthDefined = true;
        }

        if ((height = StyleGenerator.getMeasure(style, CSS_Property.HEIGHT)) > 0)
            minSize.height = maxSize.height = prefSize.height = height;
    }

    /** Creates a new instance of SizeRequirements */
    public SizeRequirements()
    {
    }

    public int getPreferredSpan(int axis)
    {
        if (axis == View.Y_AXIS)
            return this.prefSize.height;
        else
            return this.prefSize.width;
    }

    public int getMinimumSpan(int axis)
    {
        if (axis == View.Y_AXIS)
            return this.minSize.height;
        else
            return this.minSize.width;
    }

    public int getMaximumSpan(int axis)
    {
        if (axis == View.Y_AXIS)
            return this.maxSize.height;
        else
            return this.maxSize.width;
    }

    public void setPreferredSpan(Dimension spans)
    {
        if (!widthDefined)
            prefSize.width = spans.width;
        prefSize.height = spans.height;
    }

    public void setMinimumSpan(Dimension spans)
    {
        if (!widthDefined)
            minSize.width = spans.width;
        minSize.height = spans.height;
    }

    public void setMaximumSpan(Dimension spans)
    {
        if (!widthDefined)
            maxSize.width = spans.width;
        maxSize.height = spans.height;
    }

    public void setMaximumSpan(int axis, int span)
    {
        if (axis == View.X_AXIS)
            maxSize.width = span;
        else
            maxSize.height = span;
    }

    public void increaseMaximumSpan(int axis, int span)
    {
        if (axis == View.X_AXIS)
            maxSize.width += span;
        else
            maxSize.height += span;
    }
    
    public void increaseMinimumSpan(int axis, int span)
    {
        if (axis == View.X_AXIS)
            minSize.width += span;
        else
            minSize.height += span;
    }
    
    public boolean isWidthDefined()
    {
        return widthDefined;
    }
}