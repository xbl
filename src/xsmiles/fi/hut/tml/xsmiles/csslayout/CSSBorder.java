/*
 * Border.java
 * 
 * Created on 8. joulukuuta 2003, 13:13
 */

package fi.hut.tml.xsmiles.csslayout;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/**
 * @author Alessandro
 */
public class CSSBorder
{

    public static final short TOP = 1;
    public static final short RIGHT = 2;
    public static final short LEFT = 3;
    public static final short BOTTOM = 4;

    private CSSBackgroundImage backgroundImage = null;

    protected String border_top_style = "solid";
    protected String border_right_style = "solid";
    protected String border_left_style = "solid";
    protected String border_bottom_style = "solid";

    protected int border_top_width = 0;
    protected int border_right_width = 0;
    protected int border_left_width = 0;
    protected int border_bottom_width = 0;

    protected Color border_top_color = Color.gray;
    protected Color border_right_color = Color.gray;
    protected Color border_left_color = Color.gray;
    protected Color border_bottom_color = Color.gray;

//    protected double zoom = 1;
    protected int thickness = 1;
    protected Color borderColor = Color.gray;
    XSmilesCSSStyleDeclarationImpl s = null;
    //int[] ascisse ;
    //int[]ordinate ;

    int x1, y1, x2, y2, x3, x4, y5, y6, x12, y12, x34, y56;

    /** Creates a new instance of Border */
    public CSSBorder()
    {
    }

    public CSSBorder(CSSStyleDeclaration style)
    {

        if (style instanceof XSmilesCSSStyleDeclarationImpl)
        {
            this.setCSSBorderStyle((XSmilesCSSStyleDeclarationImpl) style);
            this.setCSSBorderWidth((XSmilesCSSStyleDeclarationImpl) style);
            this.setCSSBorderColor((XSmilesCSSStyleDeclarationImpl) style);
            if (style.getPropertyCSSValue("background-image") != null)
            {
                // backgroundImage = new CSSBackgroundImage(style);
            }
        }
    }

    public CSSBorder(Color color)
    {

        borderColor = color;
    }

    public CSSBorder(int thick)
    {

        thickness = thick;
    }

    public CSSBorder(Color color, int thick)
    {

        borderColor = color;
        thickness = thick;
    }

    public int getBorderWidth(short border)
    {
        switch (border)
        {
            case TOP :
                return border_top_width;
            case RIGHT :
                return border_right_width;
            case LEFT :
                return border_left_width;
            case BOTTOM :
                return border_bottom_width;
        }
        return 0;
    }
 /*   public void setZoom(double zoom){
    	System.out.println("((((((((((((((((((((--"+this.zoom+"---border1-I");
    	this.zoom = zoom;
    	System.out.println("((((((((((((((((((((--"+this.zoom+"---border1-II");
    	System.out.println("((((((((((((((((((((--"+border_top_width+"---border_top_width-I");
    	border_top_width = (int)(border_top_width*zoom );
    	System.out.println("((((((((((((((((((((--"+border_top_width+"---border_top_width-II");
        border_right_width = (int)(border_right_width * zoom);
    	border_left_width = (int)(border_left_width * zoom);
    	border_bottom_width = (int)(border_bottom_width * zoom);
    }
*/
    public void paintBorder(Graphics g, int x, int y, int width, int height)
    {

        Color oldColor = g.getColor();
//        System.out.println("((((((((((((((((((((--"+border_top_width+"---border_top_width-paint");
        x1 = x;
        y1 = y;
        x2 = x - border_left_width;
        y2 = y - border_top_width;
        x3 = x + width;
        x4 = x3 + border_right_width;
        y5 = y + height;
        y6 = y5 + border_bottom_width;

        if (border_top_style.equals("ridge") || border_top_style.equals("groove"))
        {
            x12 = x - ((int) (border_left_width / 2));
            y12 = y - ((int) (border_top_width / 2));
            x34 = x3 + ((int) (border_right_width / 2));
            y56 = y5 + ((int) (border_bottom_width / 2));

            g.setColor(border_left_color);
            int[] OutXAxisLeft = {x12, x2, x2, x12};
            int[] OutYAxisLeft = {y56, y6, y2, y12};
            int[] InXAxisLeft = {x1, x12, x12, x1};
            int[] InYAxisLeft = {y5, y56, y12, y1};
            paintBorderSideCombi(LEFT, border_left_style, g, OutXAxisLeft, OutYAxisLeft, InXAxisLeft,
                    InYAxisLeft);

            g.setColor(border_top_color);
            int[] OutXAxisTop = {x12, x2, x4, x34};
            int[] OutYAxisTop = {y12, y2, y2, y12};
            int[] InXAxisTop = {x1, x12, x34, x3};
            int[] InYAxisTop = {y1, y12, y12, y1};
            paintBorderSideCombi(TOP, border_top_style, g, OutXAxisTop, OutYAxisTop, InXAxisTop,
                    InYAxisTop);

            g.setColor(border_right_color);
            int[] OutXAxisRight = {x34, x4, x4, x34};
            int[] OutYAxisRight = {y12, y2, y6, y56};
            int[] InXAxisRight = {x3, x34, x34, x3};
            int[] InYAxisRight = {y1, y12, y56, y5};
            paintBorderSideCombi(RIGHT, border_right_style, g, OutXAxisRight, OutYAxisRight,
                    InXAxisRight, InYAxisRight);

            g.setColor(border_bottom_color);
            int[] OutXAxisBottom = {x34, x4, x2, x12};
            int[] OutYAxisBottom = {y56, y6, y6, y56};
            int[] InXAxisBottom = {x3, x34, x12, x1};
            int[] InYAxisBottom = {y5, y56, y56, y5};
            paintBorderSideCombi(BOTTOM, border_bottom_style, g, OutXAxisBottom, OutYAxisBottom,
                    InXAxisBottom, InYAxisBottom);

        }
        else
        {

            g.setColor(border_left_color);
            int[] XAxisLeft = {x1, x2, x2, x1};
            int[] YAxisLeft = {y5, y6, y2, y1};
            paintBorderStyle(LEFT, border_left_style, g, XAxisLeft, YAxisLeft);

            g.setColor(border_top_color);
            int[] XAxisTop = {x1, x2, x4, x3};
            int[] YAxisTop = {y1, y2, y2, y1};
            paintBorderStyle(TOP, border_top_style, g, XAxisTop, YAxisTop);

            g.setColor(border_right_color);
            int[] XAxisRight = {x3, x4, x4, x3};
            int[] YAxisRight = {y1, y2, y6, y5};
            paintBorderStyle(RIGHT, border_right_style, g, XAxisRight, YAxisRight);

            g.setColor(border_bottom_color);
            int[] XAxisBottom = {x3, x4, x2, x1};
            int[] YAxisBottom = {y5, y6, y6, y5};
            paintBorderStyle(BOTTOM, border_bottom_style, g, XAxisBottom, YAxisBottom);

        }

        g.setColor(oldColor);

    }

    public void paintBorderStyle(int side, String bs, Graphics g, int[] XAxis, int[] YAxis)
    {

        if (bs.equals("solid"))
        {
            paintBorderSolidSide(side, bs, g, XAxis, YAxis);
        }
        else if (bs.equals("inset"))
        {
            paintBorderSideInset(side, bs, g, XAxis, YAxis);
        }
        else if (bs.equals("outset"))
        {
            paintBorderSideOutset(side, bs, g, XAxis, YAxis);
        }
    }

    public void paintBorderSolidSide(int side, String bs, Graphics g, int[] XAxis, int[] YAxis)
    {

        g.fillPolygon(new Polygon(XAxis, YAxis, 4));
    }

    public void paintBorderSideInset(int side, String bs, Graphics g, int[] XAxis, int[] YAxis)
    {
        //  System.out.println("((((((((((((((((((((-----))))))))))))))))))))");
        Color oldColor = g.getColor();
        if (side == LEFT || side == TOP)
        {
            g.setColor(g.getColor().darker());
        }
        else if (side == RIGHT || side == BOTTOM)
        {
            g.setColor(g.getColor().brighter());
        }
        g.fillPolygon(new Polygon(XAxis, YAxis, 4));
        g.setColor(oldColor);
    }

    public void paintBorderSideOutset(int side, String bs, Graphics g, int[] XAxis, int[] YAxis)
    {

        Color oldColor = g.getColor();
        if (side == LEFT || side == TOP)
        {
            g.setColor(g.getColor().brighter());
        }
        else
        {
            g.setColor(g.getColor().darker());
        }
        g.fillPolygon(new Polygon(XAxis, YAxis, 4));
        g.setColor(oldColor);
    }

    public void paintBorderSideCombi(int side, String bs, Graphics g, int[] OutXAxis, int[] OutYAxis,
            int[] InXAxis, int[] InYAxis)
    {

        if (bs.equals("groove"))
        {
            paintBorderSideInset(side, bs, g, OutXAxis, OutYAxis);
            paintBorderSideOutset(side, bs, g, InXAxis, InYAxis);
        }
        else if (bs.equals("ridge"))
        {
            paintBorderSideInset(side, bs, g, InXAxis, InYAxis);
            paintBorderSideOutset(side, bs, g, OutXAxis, OutYAxis);
        }
    }

    public void setThickness(int thick)
    {

        thickness = thick;
    }

    public void setColor(Color color)
    {

        borderColor = color;
    }

    public void setCSSBorderStyle(XSmilesCSSStyleDeclarationImpl attributes)
    {

        if (StyleGenerator.getStringValue(attributes, CSS_Property.BORDER_TOP_STYLE) != null)
        {
            border_top_style = StyleGenerator.getStringValue(attributes, CSS_Property.BORDER_TOP_STYLE);
        }
        if (StyleGenerator.getStringValue(attributes, CSS_Property.BORDER_RIGHT_STYLE) != null)
        {
            border_right_style = StyleGenerator.getStringValue(attributes,
                    CSS_Property.BORDER_RIGHT_STYLE);
        }
        if (StyleGenerator.getStringValue(attributes, CSS_Property.BORDER_LEFT_STYLE) != null)
        {
            border_left_style = StyleGenerator.getStringValue(attributes,
                    CSS_Property.BORDER_LEFT_STYLE);
        }
        if (StyleGenerator.getStringValue(attributes, CSS_Property.BORDER_BOTTOM_STYLE) != null)
        {
            border_bottom_style = StyleGenerator.getStringValue(attributes,
                    CSS_Property.BORDER_BOTTOM_STYLE);
        }
    }

    public void setCSSBorderWidth(XSmilesCSSStyleDeclarationImpl attributes)
    {
    	//System.out.println("((((((((((((((((((((--"+StyleGenerator.getZoom()+"---border");
        if (StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_TOP_WIDTH) != 0)
        {
            border_top_width = (int)((StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_TOP_WIDTH)));
        }
        if (StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_RIGHT_WIDTH) != 0)
        {
            border_right_width = (int)((StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_RIGHT_WIDTH)));
        }
        if (StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_LEFT_WIDTH) != 0)
        {
            border_left_width = (int)((StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_LEFT_WIDTH)));
        }
        if (StyleGenerator.getMeasure(attributes, CSS_Property.BORDER_BOTTOM_WIDTH) != 0)
        {
            border_bottom_width = (int)((StyleGenerator.getMeasure(attributes,CSS_Property.BORDER_BOTTOM_WIDTH)));
        }
    }

    public void setCSSBorderColor(XSmilesCSSStyleDeclarationImpl attributes)
    {

        if (StyleGenerator.parseColor(attributes.getPropertyCSSValue(CSS_Property.BORDER_TOP_COLOR)) != null)
        {
            border_top_color = StyleGenerator.parseColor(attributes
                    .getPropertyCSSValue(CSS_Property.BORDER_TOP_COLOR));
        }
        if (StyleGenerator.parseColor(attributes.getPropertyCSSValue(CSS_Property.BORDER_RIGHT_COLOR)) != null)
        {
            border_right_color = StyleGenerator.parseColor(attributes
                    .getPropertyCSSValue(CSS_Property.BORDER_RIGHT_COLOR));
        }
        if (StyleGenerator.parseColor(attributes.getPropertyCSSValue(CSS_Property.BORDER_LEFT_COLOR)) != null)
        {
            border_left_color = StyleGenerator.parseColor(attributes
                    .getPropertyCSSValue(CSS_Property.BORDER_LEFT_COLOR));
        }
        if (StyleGenerator.parseColor(attributes.getPropertyCSSValue(CSS_Property.BORDER_BOTTOM_COLOR)) != null)
        {
            border_bottom_color = StyleGenerator.parseColor(attributes
                    .getPropertyCSSValue(CSS_Property.BORDER_BOTTOM_COLOR));
        }
    }

}