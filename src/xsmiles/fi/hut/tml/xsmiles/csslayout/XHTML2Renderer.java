/*
 * Created on Jun 24, 2004
 * 
 * TODO To change the template for this generated file go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.csslayout;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml2.XHTML2NavigationListElementImpl;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.Point;

import org.w3c.dom.Node;

/**
 * @author mpohja
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class XHTML2Renderer extends CSSRenderer
{

    /**
     * @param m
     */
    public XHTML2Renderer(MLFC m, boolean isInWindow)
    {
        super(m, isInWindow);
        //this.addMouseMotionListener(new XHTML2Renderer.XHTML2MouseMotionListener());
    }

    public Node findAncestor(Class ancestor, Node descendant)
    {
        if (descendant == null)
            return null;
        else if (ancestor.isInstance(descendant))
            return descendant;
        else
            return findAncestor(ancestor, descendant.getParentNode());
    }

    private class XHTML2MouseMotionListener extends MouseMotionAdapter
    {
        XHTML2NavigationListElementImpl lastNL = null;

        public void mouseMoved(MouseEvent event)
        {
            if (rootView == null)
                return;
            Point pt = new Point(event.getX(), event.getY());
            View view = rootView.getViewAtPoint(pt);
            Node node = null;
            if (view == null)
                return;
            node = view.getDOMElement();
            while (node == null && view != null)
            {
                view = view.getParentView();
                node = view.getDOMElement();
            }
            String location = null;
            XHTML2NavigationListElementImpl nlElement = null;

            if ((nlElement = (XHTML2NavigationListElementImpl) findAncestor(
                    XHTML2NavigationListElementImpl.class, node)) != null)
            {
                nlElement.setStatus(true);

                XHTML2NavigationListElementImpl parentNL = (XHTML2NavigationListElementImpl) findAncestor(
                        XHTML2NavigationListElementImpl.class, nlElement.getParentNode());
                if (lastNL != null
                        && ((parentNL == null && !nlElement.equals(lastNL)) || (parentNL != null
                                && !parentNL.equals(lastNL) && !nlElement.equals(lastNL))))
                    lastNL.setStatus(false);
                lastNL = nlElement;
            }
            else
            {
                while (lastNL != null)
                {
                    lastNL.setStatus(false);
                    lastNL = (XHTML2NavigationListElementImpl) findAncestor(
                            XHTML2NavigationListElementImpl.class, lastNL.getParentNode());
                }
            }
        }
    }
}