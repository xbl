/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Dec 22, 2004
 */
package fi.hut.tml.xsmiles.csslayout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.util.Vector;
import java.awt.Point;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.view.BaseView;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;
import fi.hut.tml.xsmiles.BrowserWindow;

/**
 * @author mpohja
 */
public class ContentFocusPointsProvider implements FocusPointsProvider
{

    protected Vector focusNodes;
    public AWTGUI root;
    public CSSRenderer renderer;
    public Container scrollpane;

    /**
     *  
     */
    public ContentFocusPointsProvider(AWTGUI gui)
    {
        root = gui;
    }

    public ContentFocusPointsProvider(CSSRenderer r)
    {
        renderer = r;
    }

    /*
     * (non-Javadoc)
     * It returns the focus points (its location is related to the Document 
     * View)
     * @see fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider#getFocusPoints()
     */
    public FocusPoint[] getFocusPoints()
    {
	// Document doc = renderer.getDocument();
        Document doc = root.getBrowser().getXMLDocument().getDocument();
        Node rootElement = doc.getDocumentElement();
        focusNodes = new Vector();
        getChildrenFocus(rootElement);
	
        int i, numComponents = focusNodes.size();
        FocusPoint[] vec;// = new FocusPoint[numComponents];
        Vector elemViews = null, tmpVec = new Vector();
        int views;
        Rectangle[] recs;
        VisualElementImpl elem;
	
	
        for (i = 0; i < numComponents; i++)
	    {
		elem = (VisualElementImpl) focusNodes.elementAt(i);
		elemViews = elem.getViews();
		if (elemViews != null)
		    {
			/** We add the Visual Element **/
			views = elemViews.size();
			recs = new Rectangle[views];
			for (int viewIndex = 0; viewIndex < views; viewIndex++)
			    {
				recs[viewIndex] = ((View) elemViews.get(viewIndex)).getRectangle();
			    }
			tmpVec.add(new FocusPoint(recs, elem));
			Log.debug("Another focus element: " + elem.toString() + ", recs: "+ recs[0] + ", views: "+ elem.getViews());
		    }
	    }
	vec = (FocusPoint[])tmpVec.toArray(new FocusPoint[0]);
	return vec;
    }


    private void getChildrenFocus(Node node)
    {
        if (node instanceof VisualElementImpl && ((VisualElementImpl) node).isFocusPoint())
            focusNodes.add(node);
        NodeList children = node.getChildNodes();
        int length = children.getLength();
        for (int i = 0; i < length; i++)
        {
            getChildrenFocus(children.item(i));
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider#getNextFocusPoint()
     */
    public FocusPoint getNextFocusPoint()
    {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider#getPreviousFocusPoint()
     */
    public FocusPoint getPreviousFocusPoint()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void addFocusNode(VisualElementImpl elem)
    {
        focusNodes.add(elem);
    }

    public AWTGUI getAWTGUI()
    {
        return root;
    }

    public CSSRenderer getRenderer()
    {
        return renderer;
    }
}
