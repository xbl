    /* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Dec 29, 2004
 */
package fi.hut.tml.xsmiles.csslayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Point;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;

/**
 * @author mpohja
 */
public class FocusComponent extends Component
{
    
    Rectangle focusArea;
    Rectangle[] recs = null;

    /**
     * 
     */
    public FocusComponent()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public void setCurrentFocus(FocusPoint point)
    {
        
	Rectangle tmp = null;
	FocusPoint currentFocus = point;
        focusArea = new Rectangle(currentFocus.getRectangleAt(0));
	int rectangles = currentFocus.getNumRectangles();
	recs = new Rectangle[rectangles];
	recs[0] = focusArea;
        
	
	for (int i = 1; i < rectangles; i++)
	    {
		tmp = new Rectangle(currentFocus.getRectangleAt(i));
		recs[i] = tmp;
		focusArea = focusArea.union(tmp);
	    }

        this.setBounds(focusArea);
	this.setVisible(true);
    }

    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(Color.red);
	if (recs != null){
	    for (int i = 0; i < recs.length; i++){
		g.drawRect(recs[i].x, recs[i].y, recs[i].width, recs[i].height);
	    }
	}
        //g.fillRect(focusArea.x, focusArea.y, focusArea.width, focusArea.height);
    }   
}
