/*
 * Created on Jul 5, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.csslayout;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;


public class RendererComponent extends Component
{
    protected View myRootView;
    protected Image bgImage;
    
    public RendererComponent(View rw)
    {
        myRootView=rw;
    }
    public void paint(Graphics g)
    {
        if (CSSRenderer.antiAlias)
        {
            {
                CompatibilityFactory.getGraphicsCompatibility().setAntialias(CSSRenderer.antiAlias, g);
            }
        }
        /*
         * if (!paintList.isEmpty()) { Log.debug("Paint list: " +
         * paintList); View view = ((View) paintList.get(0)); Rectangle
         * viewBounds = view.getRectangle(); Rectangle newPaintArea =
         * viewBounds.union(g.getClipBounds()); g.setClip(newPaintArea);
         * //paintList.remove(view); }
         */
        this.myRootView.paint(g);
        //Log.debug("renderer.paint "+ this.hashCode()+ " "+ this.myRootView);
    }
    
    public void setImage(Image img)
    {
        bgImage = img;
    }

    public boolean isFocusTraversable()
    {
        return true;
    }

    public boolean isLightweight()
    {
        return true;
    }

    public boolean isFocusable()
    {
        return true;
    }
}