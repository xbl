/* X-Smiles
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package fi.hut.tml.xsmiles;
import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.event.BrowserEventListener;
import fi.hut.tml.xsmiles.event.GUIEventListener;

/**
 * Broadcasts events to the all parties attached to when an event is issued.
 *
 * When an issue method is called the EventBroker invokes the corresponding
 * event method in all the static and MLFC components attached to it. All components
 * attached to it must extend the BrowserEventListener.
 * <br>See Technical Requirements Specification for further details on
 * events.
 *
 * @author       Juha
 * @version	$Revision: 1.27 $
 * @see          BrowserEventListener
 */
public class EventBroker 
{ 
    private BrowserWindow browser;
    private Vector  browserEventListeners;
    private Vector  guiEventListeners;
  
    public EventBroker(BrowserWindow myBrowser)
    {
        browser = myBrowser;
        browserEventListeners=new Vector();
        guiEventListeners=new Vector();
        return;
    }

    /**
     * @param l Attach GUIEventListener to EventBroker
     */
    public void addGUIEventListener(GUIEventListener l) 
    {
        guiEventListeners.addElement(l);
    }
  
    public void removeGUIEventListener(GUIEventListener l)  
    {
        guiEventListeners.removeElement(l);
    }
  
    /**
     * @param l Attach BrowserEventListener to EventBroker.
     */
    public void addBrowserEventListener(BrowserEventListener l) {
        browserEventListeners.addElement(l);
        Log.debug("AddBrowserEventListener: count: "+browserEventListeners.size());
    }

    /**
     * @param l Attach BrowserEventListener to EventBroker.
     */
    public void removeBrowserEventListener(BrowserEventListener l) {
        browserEventListeners.removeElement(l);
    }
    
    /** 
     * Remove all listeners added to eventbroker
     */
    public void removeAllListeners() {
        browserEventListeners.removeAllElements();
        //guiEventListeners.removeAllElements();
    }
  
  
    /*    public void issueBrowserActionEvent(BrowserEvent e) {
          BrowserEventListener reactee;
          Enumeration e = browserEventListeners.elements();
          while(e.hasMoreElements())  {
          reactee = (BrowserEventListener)e.nextElement();
          reactee.browserActionPerformed(e);
          }
          }
    */
    
    /**
     * @see BrowserLogic
     */
    public synchronized void issueBrowserStateChangedEvent(int state, String text)
    {
        BrowserEventListener reactee;
        Enumeration e = browserEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (BrowserEventListener)e.nextElement();
            reactee.browserStateChanged(state, text);
            Log.debug("BROWSER STATE: " + state +" "+ text);
        }
    }


    /* GUIEVENTLISTENER METHODS */
  
    /**
     * After browser is ready, then start is called. 
     * Not neccesary to implement
     */
    public void start()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.start();
        }
    }

  
    /**
     * Destroy The GUI (delete frame, etc)
     */
    public void destroy()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.destroy();
        }
    }

    /**
     * Informs the gui that browser is working
     */
    public void browserWorking()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.browserWorking();
        }
    }

    /**
     * Informs the gui that browser is resting
     */
    public void browserReady()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.browserReady();
        }
    }

    /**
     * @param s The location that is beeing loaded
     */
    public void setLocation(String s)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setLocation(s);
        }
    }


    /**
     * @param statusText The text to put in status bar
     */
    public void setStatusText(String statusText)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setStatusText(statusText);
        }
    }    
}
