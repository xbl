/* X-Smiles
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * SUCH DAMAGE. Full license in licenses/XSMILES_LICENSE
 */

package fi.hut.tml.xsmiles;

import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.xml.XMLParserFactory;

import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.*;
import java.net.URL;
import java.net.MalformedURLException;


/* CONFIG IS OF THE FOLLOWING TYPE:
	<main>
		<homepage>http://www.xsmiles.org/demo/demos.xml</homepage>
		<parser>Docuverse</parser>
		<xslprocessor>XT</xslprocessor>
		<broker>tcp://127.0.0.1:80</broker>
		<publishtopic>XSmilesTopic</publishtopic>
		<subscribetopic>XSmilesPublish</subscribetopic>
	</main>
	<gui>
		<initialgui>2</initialgui>
		<skinsenabled>true</skinsenabled>
		<themepack>./lib/skinlf/mozilla.zip</themepack>
	</gui>
	<smil>
		<system-bitrate>28800</system-bitrate>
		<system-captions>off</system-captions>
		<system-language>en</system-language>
		<system-overdub-or-caption>caption</system-overdub-or-caption>
		<play-image>true</play-image>
		<play-video>true</play-video>
		<play-audio>true</play-audio>
		<use-styles>true</use-styles>
	</smil>
*/


/**
 * Stores general browser configuration and provides access to read 
 * values.
 *
 *
 * @author	Mikko Honkala
 * @version	$Revision: 1.45 $
 */
public class XMLConfigurer
{ 
  protected boolean changed=false;
  protected Element root;
  protected Document doc;
  protected URL fileurl;
  protected XMLParser parser;
  protected String parserName="Jaxp";

  /**
   * A XML tree based configuration handler which 
   * gives back properties to XPath queries
   * @see cfg/config.xml
   */
  public XMLConfigurer()
  {
    try
      {
	  /* file:/// did not work in kaffe */
	this.fileurl = Resources.getResourceURL("xsmiles.config"); 
	    //new URL("file","",new File(Browser.optionsDocument).getAbsolutePath());
	readConfig(fileurl);
	//if (changed) writeConfig(); WHY TO WRITE HERE?
      } catch (Exception e)
	{
          try
          {
              Log.error(e);
              Log.error("trying to read from: "+fileurl);
              this.setDefaults();
              //this.writeConfig(); // let's not try to be more intelligent than we are
          } catch (Exception ex)
          {
              Log.error(ex);
          }
	}
		
  }
  

  private void setDefaults()
  {
    try {
	if (this.parser==null) this.parser=XMLParserFactory.createXMLParser(this.parserName);

  	Log.debug("XMLConfigurer.setDefaults called:"+this.doc+this.parser);
    if (doc==null) {
	this.doc=parser.openDocument(Resources.getResourceURL("xsmiles.config"),null);
	this.root=doc.getDocumentElement();
      }
    } catch (Exception e) {
    	Log.error(e);
    	return;
    }
  }
  
  
	/**
	* Config has chaged, change global options
	*/
	public void configChanged() 
	{
		
		/*
		 	<logging>
		<target>file</target>
		<logfile>fgsdfgsdfgsdfgsdfg</logfile>
		<debuglevel>
			<info>true</info>
			<debug>false</debug>
			<eventsdebug>false</eventsdebug>
		</debuglevel>
	</logging>
		 */
		boolean level_debug=this.getBooleanProperty("logging/debuglevel/debug");
		boolean level_info=this.getBooleanProperty("logging/debuglevel/info");
		boolean source_error=this.getBooleanProperty("logging/debuglevel/errmsgsource");
		boolean source_debug=this.getBooleanProperty("logging/debuglevel/debugmsgsource");
		Log.enable_debug=level_debug;
		Log.enable_info=level_info;
		Log.enable_error_source=source_error;
		Log.enable_debug_source=source_debug;
		boolean antialias=this.getBooleanProperty("gui/antialias/enabled");
		CSSRenderer.antiAlias=antialias;
		int minalias = this.getIntProperty("gui/antialias/minalias");
		CSSRenderer.minAlias=minalias;
		try
		{
			boolean level_http=this.getBooleanProperty("logging/debuglevel/httpdebug");
			boolean httpclient_enabled=this.getBooleanProperty("main/httpclient/enabled");
			HTTP.enableDebug(level_http);
			HTTP.enableHTTPClient(httpclient_enabled);
		}
		catch (Throwable t)
		{
			Log.error(t);
		}
		try
		{
			String target = this.getProperty("logging/target");
			if (target.equalsIgnoreCase("window"))
			{
				Log.disableLogFile();
				Utilities.openStaticLog();
			}
			else if (target.equalsIgnoreCase("file"))
			{
				String filename = this.getProperty("logging/logfile");
				if (filename!=null && filename.length()>0)
				{
					Utilities.closeStaticLog();
					Log.disableLogFile();
					Log.enableLogFile(filename);
				}
			}
			else
			{
				Utilities.closeStaticLog();
				Log.disableLogFile();
			}
		} catch (IOException e)
		{
			Log.error(e);
		}

		String host="";
		String port="";
		if ("true".equals(this.getProperty("main/useproxy")))
		{
			host=this.getProperty("main/proxyhost");
			port=this.getProperty("main/proxyport");
		}
		Properties props = System.getProperties();
		props.put("http.proxyHost", host);
		props.put("http.proxyPort", port);
		Log.debug("Using HTTP proxy: "+host+":"+port);
		
	}
  /**
   * Read the config file specified by the url
   */
  public void readConfig(URL m_url) throws MalformedURLException, IOException, SAXException,ClassNotFoundException
  {
    URL url=m_url;
    if(url==null) {
      this.fileurl = Resources.getResourceURL("xsmiles.config");
      //;new URL("file:///"+new File(Browser.optionsDocument).getAbsolutePath());
      url=fileurl;
      Log.debug("OH CRAP!!! config file url null, trying to fix it");
    }
    
    if (new File(url.getFile()).exists())
      {
	Reader temp=this.readFile(url);
	parser=XMLParserFactory.createXMLParser(this.parserName);
	this.doc=parser.openDocument(temp,false);
	this.root=doc.getDocumentElement();
	this.setDefaults();
	//Log.debug(parser.write(doc,true));
      } else parser=XMLParserFactory.createXMLParser(this.parserName);
    this.configChanged();
  }
  
  /**
   * Write the config file specified by the url
   */
  public void writeConfig()
  {
    /// TODO: check stylesheet reference
    try
      {
        Log.info("******* WRITING CONFIGURATION FILE TO: "+fileurl.getFile());

	File file=new File(fileurl.getFile());
	FileWriter writer=new FileWriter(file);
	this.parser.write(writer,doc,true);
	changed=false;
      } catch (IOException e)
	{
	  Log.error(e);
	}
  }
	
  /**
   * Set a config property
   */
  public void setProperty(String path, String value)
  {
    Element e=this.findElement(path);
    this.setValue(e,value);
    changed=true;
  }

  /**
   * @param path Get an integer value config property (e.g "gui/screendepth") 
   * @return the value
   */
  public int getIntProperty(String path) {
    return Integer.parseInt(getProperty(path));
  }

  
    /**
   * @param path Get an boolean value config property (e.g "gui/screendepth") 
   * @return the value
   */
  public boolean getBooleanProperty(String path) {
	String val = this.getProperty(path);
	if (val.equals("true")||val.equals("1")) return true;
	else return false;
  }

  /**
   * @param prop The gui property found under gui/properties/<currentgui>/
   *             Minimizes the need of doing extensive hunting for the name of the
   *             current GUI every time a property is requested.
   * @return The value. e.g., if screenDepth  is requested, and the current GUI is 
   *         desktop, then, the property is translated into into the full xpath:
   *         gui/properties/desktop/screenDepth
   */
  public String getGUIProperty(BrowserWindow browser, String prop) {
    String guiName=(String)browser.getGUIManager().getCurrentGUIName();
    return getProperty("gui/devices/"+guiName+"/"+prop);
  }

  /**
   * @param guiName The title of the GUI
   * @param prop The gui property found under gui/properties/<currentgui>/
   *             Minimizes the need of doing extensive hunting for the name of the
   *             current GUI every time a property is requested.
   * @return The value. e.g., if screenDepth  is requested, and the current GUI is 
   *         desktop, then, the property is translated into into the full xpath:
   *         gui/properties/desktop/screenDepth
   */
  public String getGUIProperty(String guiName, String prop) {
    return getProperty("gui/devices/"+guiName+"/"+prop);
  }


  /**
   * Set a GUI property
   */
  public void setGUIProperty(BrowserWindow browser, String prop, String value)
  {
    String path="gui/devices/"+browser.getGUIManager().getCurrentGUIName()+"/"+prop;
    Element e=this.findElement(path);
    this.setValue(e,value);
    changed=true;
  }

  /**
   * @param path Get a config property (e.g "main/homepage")
   * @return the value
   */
  public String getProperty(String path)
  {
    Element e=this.findElement(path);
    if (e==null) 
      {
	Log.error("XMLConfigurer: config file "+fileurl+" does not have the value :"+path);
	return null;
      }
    else {
		// Value is blank, or null.
		if (e.getFirstChild() == null)
			return null;

		// Value found			
		return e.getFirstChild().getNodeValue();
    }
  }

  private void setDefault(String path, String defvalue)
  {
    Log.error("YOU ARE RUNNING A ");
    Element n = this.findElement(path);
    if (n==null) 
      {
	n=this.createNode(path);
	this.setValue(n,defvalue);
      }
  }
	
  /**
   * Creates the node represented by path
   */
  public Element createNode(String path)
  {
    Element curr=this.root;
    StringTokenizer t = new StringTokenizer(path,"/");
    while (t.hasMoreTokens())
      {
	String token=t.nextToken();
	Element child = findChild(curr,token);
	if (child==null)
	  {
				// Create the node
	    child=curr.getOwnerDocument().createElement(token);
	    curr.appendChild(child);
	  }
	curr=child;
      }
    return curr;
  }
  private Element findChild(Element root,String name)
  {
    int sibling;
		
    NodeList list=root.getChildNodes();
		
    if (name.indexOf('[') != -1 )
      {
	sibling = Integer.parseInt(name.substring(name.indexOf('[') + 1, name.indexOf(']') ) );
	name = name.substring(0, name.indexOf('[') );
      }
    else
      sibling = 0;
		
		
    int s_count=0;
		
    for (int i=0;i<list.getLength();i++)
      {
			
	if (list.item(i).getNodeType()==Node.ELEMENT_NODE) 
	  {
	    Element elem=(Element)list.item(i);
	    if (elem.getNodeName().equals(name)) 
	      {
		if(s_count == sibling)
		  return elem;
		else
		  s_count++;
	      }	
	  }		
      }
    return null;
  }
	
  public Element findElement(String path)
  {
    Element curr=this.root;
    StringTokenizer t = new StringTokenizer(path,"/");
    while (t.hasMoreTokens())
      {
	String token=t.nextToken();
	Element child = findChild(curr,token);
	if (child==null) return null;
	curr=child;
      }
    return curr;
  }
	
  private void setValue(Element e,String value)
  {
    Node textn=e.getFirstChild();
    if (textn==null)
      {
	// Create the text node
	e.appendChild(e.getOwnerDocument().createTextNode(value));
	changed=true;
	return;
      }
    if (textn.getNodeType()!=Node.TEXT_NODE) 
      {
	Log.error("XMLConfigurer.setValue, "+e);
	return;
      }
    textn.setNodeValue(value);
    changed=true;
  }
  
  public Document getConfigDocument()
  {
      return doc;
  }
	
  /**
   * Reads the file into a temporary StringReader
   * This is done, because we need to search for the browser
   * option to know which XML parser the user wants to use.
   */
  private Reader readFile(URL file) throws IOException
  {
    StringBuffer buffer=new StringBuffer();
    BufferedReader r = new BufferedReader(new FileReader(file.getFile()));
    String line;
    String parser=null;
    while ((line=r.readLine())!=null)
      {
	buffer.append(line);
	buffer.append('\n');
			
	//------ try to find the parser tag
	int s=line.indexOf("<parser>");
	if (s>-1)
	  {
	    String rest=line.substring(s+8);
	    int e=rest.indexOf("<");
	    if (e<0) 
	      {
		parser=rest;
	      }
	    else
	      {
		String p = rest.substring(0,e);
		parser =p;
	      }
	  }
	// ------- end of parser hunt
      }
    Log.debug("*** PARSER: '"+parser+"'");
    if (parser!=null) this.parserName=parser;

    return new StringReader(buffer.toString());
  }

  
  
}
