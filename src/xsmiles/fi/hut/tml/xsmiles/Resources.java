/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 2, 2004
 *
 */
package fi.hut.tml.xsmiles;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;


/**
 * This class is used to get an URL to a resource, which can be either in 
 * the jar or in the development environment file
 * @author honkkis
 *
 */
public class Resources
{
    protected static Hashtable resourceTable;
    static {
        resourceTable = new Hashtable();
        // TODO: read these from a properties file in the jar
        addResourceURL("xsmiles.sphinx.config", "cfg/sphinxconfig.xml");
        addResourceURL("xsmiles.smil.stylesheet", "cfg/smil.css");
        addResourceURL("xsmiles.xhtml.stylesheet", "cfg/xhtml.css");
        addResourceURL("xsmiles.xforms.stylesheet", "cfg/xforms.css");
        addResourceURL("xsmiles.error.stylesheet", "cfg/error.xsl");
        addResourceURL("xsmiles.search.page", "cfg/search.xhtml");
        addResourceURL("xsmiles.dir.stylesheet", "cfg/directory.xsl");
        addResourceURL("xsmiles.bookmarks.document", "cfg/bookmarks_recursive.xhtml");
        addResourceURL("xsmiles.bookmarks.view", "cfg/bookmarks_xhtml.xml");
        addResourceURL("xsmiles.bookmarks.view.frame", "cfg/bookmarks.xfm");
        addResourceURL("xsmiles.config", "cfg/config.xml");
        addResourceURL("xsmiles.config.editor", "cfg/config.xhtml");
        addResourceURL("xsmiles.about", "cfg/about.smil");
        addResourceURL("xsmiles.default.key", "cfg/xsmiles.jks");
        addResourceURL("xsmiles.default.symmetrickey", "cfg/xsmiles.jceks");
        // to get an empty page
        addResourceURL("xsmiles.empty", "cfg/empty.txt"); 
        addResourceURL("xforms.schema.datatypes", "cfg/xforms-schema-datatypes.xsd");
        addResourceURL("xsmiles.dtd.xhtml", "cfg/dtd/xhtml-entities.ent");
        
        addResourceString("xsmiles.default.keystore.pass","xsmiles"); // the password for the default key store
        addResourceString("xsmiles.default.key.pass","xsmiles"); // the password for the default key store
        addResourceString("xsmiles.default.key.alias","xsmiles"); // the password for the default key store
        addResourceString("xsmiles.default.certificate.alias","xsmiles"); // the password for the default key store
    }
    

    
    
    /**
     * @return url to a browser resource
     */
    public static URL getResourceURL(String id) throws MalformedURLException {
        URL res = (URL) resourceTable.get(id);
        if (res == null) {
            throw new MalformedURLException("No XResource found with this id: " + id);
        }
        return res;
    }
    public static String getResourceString(String id) throws XSmilesException {
        Object res = resourceTable.get(id);
        if (res == null||(!(res instanceof String)) ) {
            throw new XSmilesException("No XResource found with this id: " + id);
        }
        return (String )res;
    }

    /**
     * @return url to a browser resource
     */
    protected static void addResourceURL(String id, String file) {
        URL res = Resources.findResourceURL(file);
        Log.debug("Resource: " + file + " URL:" + res);
        if (res == null) {
            Log
                    .error("Could not find critical configuration/resource file: "
                            + res
                            + "\n\n You might be missing the critical files from the cfg directory. Update from CVS, or if you are running from a jar contact the developers mailinglist: developers@xsmiles.org");
            System.exit(0);
        }
        resourceTable.put(id, res);
    }
    protected static void addResourceString(String id, String file) {
        Object res = resourceTable.get(id);
        if (res==null||res instanceof String)
        {
            resourceTable.put(id,file);
        }
        else
        {
            Log
                    .error("Error in resource string: "
                            + res);
            System.exit(0);
        }
    }

    /**
     * @return url to a browser resource
     */
    protected static URL findResourceURL(String file) {
        try {
            if (file == null || file.length() == 0)
                return null;
            try
			{
	            File f = new File(file);
	            if (!f.exists()) {
	                return Browser.class.getResource(file);
	            } else {
	                return new URL("file", "", f.getAbsolutePath());
	            }
			} catch (java.security.AccessControlException e)
			{
				return Browser.class.getResource(file);
			}

        } catch (MalformedURLException e) {
            Log.error(e);
        }

        return null;
    }


}
