/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui;

import java.awt.Container;
import java.awt.Window;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;

/**
 * A stub GUI. The main function is to send events to third party GUIs and also to provide default implementations of the ComponentFactory and ContentHandler
 * classes.
 * 
 * @author Juha, Mikko Honkala
 */
public class NullGUIAWT extends XSmilesUIAWT {

    //  private MLFCController mlfcController;
    private BrowserWindow browser;
    private BrowserConstraints constraints;
    private ContentHandlerFactory contentHandlerFactory;
    private MLFCControls mlfcControls;

    /**
     * @param b the BrowserWindow
     * @param c componentContainer (optional)
     */
    public NullGUIAWT(BrowserWindow b, Container c) {
        super(b, c);
        browser = b;

    }

    /**
     * @param b the BrowserWindow
     */
    public NullGUIAWT(BrowserWindow b) {
        super(b, null);
        browser = b;
    }

    public BrowserConstraints getBrowserConstraints() {
        return constraints;
    }

    public void setLocation(String s) {
        super.setLocation(s);
    }

    public ContentHandlerFactory getContentHandlerFactory() {
        if (contentHandlerFactory == null)
                // for gcj
            //contentHandlerFactory = new JMFContentHandlerFactory(browser);
            contentHandlerFactory = new ContentHandlerFactory(browser);
        return contentHandlerFactory;
    }

    public boolean isTabbed() {
        // To get the events for opening in new tab...
        return true;
    }

    /**
     * Return mainFrame
     * 
     * @return mainFrame
     */

    protected Window window;
    private ComponentFactory componentFactory;

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window win) {
        this.window = win;
    }

    public void destroy() {
        browser = null;
        super.destroy();
    }

    public ComponentFactory getComponentFactory() {
        if (componentFactory == null) {
            componentFactory = new AWTComponentFactory(browser);
        }
        return componentFactory;
    }

    public MLFCControls getMLFCControls() {
        if (mlfcControls == null) {
            mlfcControls = new DefaultMLFCControls(getComponentFactory());
        }
        return mlfcControls;
    }
}