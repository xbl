package fi.hut.tml.xsmiles.gui.imode;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.XAButton;
import fi.hut.tml.xsmiles.gui.components.swing.XAImageIcon;
import fi.hut.tml.xsmiles.gui.components.swing.XAPanel;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.gui.swing.XSmilesUI;
import fi.hut.tml.xsmiles.mlfc.MLFCController;

/**
 * A imode mockup
 * 
 * @author Juha
 * @version 0.1
 */
public class IMode extends XSmilesUI {
    
  private BrowserConstraints constraints;
  private MLFCController mlfcController;
  private BrowserWindow browser;
  private JPanel presentationPanel; 
  private JFrame mainFrame;	
  private Container contentPane; 
  private JPanel upperBar;	
  private JPanel lowerBar;
  private JButton hide;
  private JTextField statusBar;
  private JFrame deviceFrame;
  private boolean covers=false;             // Virtual Prototype mode 
  private JRootPane rp;
  private Container devicePane;
  private XAButton devicePicture, luuri,hangup,back,forward,home,menu,up,down,left,right,onoff,light; 
  private Valo blink;
  private XAImageIcon lighton, lightoff;
  private XAPanel toolbar;
  private XAImageIcon bback, bforw,bhome,bmenu;
  private MLFCControls mlfcControls;
private ComponentFactory componentFactory;
    

  /**
   * Constructor for switching to this GUI
   *
   * @param b the BrowserWindow
   * @param c the presentationPanel
   */
  public IMode(BrowserWindow b, Container c) {
    super(b,c);
    browser=b;
    if(c==null) {		
      presentationPanel=new JPanel();
      Log.error("IMode: null container passed, expecting something more!!!");
    }
    else 
      presentationPanel=(JPanel)c;
    init();
  }
    
  /**
   * Contructor for creating a new GUI
   *
   * @param b the BrowserWindow
   */
  public IMode(BrowserWindow b) {
    super(b,null);
    browser=b;
    presentationPanel=new JPanel();
    init();
  }
    
  /**
   * plain vanilla init()
   */
  private void init() {
    setSkinsIfNeeded();
    createGraphicalElements();
    attachListeners();
    constraints=new BrowserConstraints("phone");
    constraints.setTitle("phone");
    mlfcController=null;
  }  
    
  /**
   * Set the skin in browser configuration file
   *
   * @param the skin
   */
    //public void setSkinsIfNeeded(String skin) {
    // browser.getBrowserConfigurer().setProperty("gui/theme",skin);
    //}
    
  /**
   * All this stuph is needed for modular skins plugin. 
   * The skin library doesn't need to be resident, if
   * skins aren't used
   *
   * @param t The themePack location
   */
    /*    private void setSkinsIfNeeded() {
          String themePack=null;
          String themePath=null;
          String theme=null;
          if(browser.getBrowserConfigurer().getProperty("gui/skinsenabled").equalsIgnoreCase("true")) {
          try{
          themePath=browser.getBrowserConfigurer().getProperty("gui/themepacklocation");
          theme=browser.getBrowserConfigurer().getProperty("gui/theme");
          themePack=themePath+theme;
          
          Object[] initArgs, initArgs2;
		
          Class skinClass=Class.forName("com.l2fprod.gui.plaf.skin.Skin");
          Class skinLookAndFeel=Class.forName("com.l2fprod.gui.plaf.skin.SkinLookAndFeel");
          
          Class[] args;
          args=new Class[1];
          args[0]=Class.forName("java.lang.String");
          Method loadThemePack=skinLookAndFeel.getMethod("loadThemePack", args);
		
          args[0]=skinClass;
          Method setSkin=skinLookAndFeel.getMethod("setSkin", args);
          
          initArgs=new Object[1];
          initArgs2=new Object[1];
          initArgs2[0]=themePack;
          initArgs[0]=loadThemePack.invoke(skinLookAndFeel, initArgs2);
          
          setSkin.invoke(skinLookAndFeel, initArgs);
          
          // the SkinLookAndFeel.loadSkin tries to determine
          // if the theme is GTK (gtkrc) or KDE (ends with .themerc)
          // by looking at the filename.
          // You can also use GtkSkin or KdeSkin classes.
          } catch(ClassNotFoundException e) {
          Log.debug("Class not found " + e.toString());
          Log.error(e);
          } catch(NoSuchMethodException e) {
          Log.debug("No such method " + e.toString());
          Log.error(e);
          } catch(Exception e) {
          Log.debug("Some other exception " + e.toString());
          Log.error(e);
          }
          try {
          // ask Swing to use Skin Look And Feel
          UIManager.setLookAndFeel("com.l2fprod.gui.plaf.skin.SkinLookAndFeel");
          } catch(Exception e)
          {
          Log.debug("MobileGUI: problems with setting up skinLF");
          Log.error(e);
          }
          }
          }
    */
  public ContentHandlerFactory getContentHandlerFactory()
  {
      if (contentHandlerFactory==null) contentHandlerFactory=new SwingContentHandlerFactory(browser);
      return contentHandlerFactory;
  }

    
  /**
   * create Graphical elements
   */
  private void createGraphicalElements() { 
    GuiListener gl=new GuiListener();
    mainFrame=new JFrame("imodegui");
    if(browser.getBrowserConfigurer().getProperty("gui/virtualproto").equalsIgnoreCase("true"))
      covers=true;
    else 
      covers=false;
	
    if(covers) {
      blink=new Valo();
      deviceFrame=new JFrame("imode - device"); 
      devicePicture=new XAButton("./img/imode/imodegui.jpg");
      luuri=new XAButton("./img/imode/luuri_ylos.jpg","./img/imode/luuri_ylos_hl.jpg","./img/imode/luuri_ylos.jpg",false);
      hangup=new XAButton("./img/imode/luuri.jpg","./img/imode/luuri_hl.jpg","./img/imode/luuri.jpg",false);
      back=new XAButton("./img/imode/back.jpg","./img/imode/back_hl.jpg","./img/imode/back.jpg",false);
      forward=new XAButton("./img/imode/forw.jpg","./img/imode/forw_hl.jpg","./img/imode/forw.jpg",false);
      home=new XAButton("./img/imode/home.jpg","./img/imode/home_hl.jpg","./img/imode/home.jpg",false);
      menu=new XAButton("./img/imode/menu.jpg","./img/imode/menu_hl.jpg","./img/imode/menu.jpg",false);
      up=new XAButton("./img/imode/ylos.jpg","./img/imode/ylos_hl.jpg","./img/imode/ylos.jpg",false);
      down=new XAButton("./img/imode/alas.jpg","./img/imode/alas_hl.jpg","./img/imode/alas.jpg",false);
      left=new XAButton("./img/imode/vasen.jpg","./img/imode/vasen_hl.jpg","./img/imode/vasen.jpg",false);
      right=new XAButton("./img/imode/oikea.jpg","./img/imode/oikea_hl.jpg","./img/imode/oikea.jpg",false);
      onoff=new XAButton("./img/imode/onoff.jpg","./img/imode/onoff_hl.jpg","./img/imode/onoff.jpg",true);
      light=new XAButton("./img/imode/valo.jpg");

      lighton=new XAImageIcon("./img/imode/valo.jpg");
      lightoff=new XAImageIcon("./img/imode/valo_off.jpg");
      hangup.set3D(false);
      back.set3D(false);
      forward.set3D(false);
      home.set3D(false);
      menu.set3D(false);
      up.set3D(false);
      down.set3D(false);
      left.set3D(false);
      right.set3D(false);
      onoff.set3D(false);
      luuri.set3D(false);
      light.set3D(false);
      light.setImage(lightoff);

      hangup.setLabel("hangup");
      back.setLabel("back");
      forward.setLabel("forward");
      home.setLabel("home");
      menu.setLabel("menu");
      up.setLabel("up");
      down.setLabel("down");
      left.setLabel("left");
      right.setLabel("right");
      onoff.setLabel("onoff");
      luuri.setLabel("answer");

      hangup.addActionListener(gl);
      back.addActionListener(gl);
      forward.addActionListener(gl);
      home.addActionListener(gl);
      menu.addActionListener(gl);
      up.addActionListener(gl);
      down.addActionListener(gl);
      left.addActionListener(gl);
      right.addActionListener(gl);
      onoff.addActionListener(gl);
      luuri.addActionListener(gl);

	    
      hangup.setBounds(39,373,66,66);
      forward.setBounds(109,316,42,42);
      back.setBounds(44,310,53,55);
      home.setBounds(150,314,50,45);
      menu.setBounds(199,312,59,56);
      up.setBounds(120,356,55,30);
      down.setBounds(122,401,40,50);
      left.setBounds(101,369,40,50);
      right.setBounds(160,361,55,30);
      onoff.setBounds(185,617,68,67);
      luuri.setBounds(192,370,63,69);
      light.setBounds(116,0,80,68);
	    
      devicePane=deviceFrame.getContentPane();
    }
	
    contentPane=mainFrame.getContentPane();
	
    presentationPanel.setBorder(new EmptyBorder(0,0,0,0));
    contentPane.setLayout(new BorderLayout());
	
    contentPane.add(presentationPanel, BorderLayout.CENTER);
	
    toolbar=new XAPanel();
    toolbar.setSize(163,31);
    toolbar.setPreferredSize(new Dimension(163,31));
    toolbar.setBackgroundImage(new ImageIcon("./img/imode/toolbar_back.jpg"));

    toolbar.setLayout(new BulletinLayout());

    bback=new XAImageIcon("./img/imode/browserback.gif");
    bforw=new XAImageIcon("./img/imode/browserforward.gif");
    bhome=new XAImageIcon("./img/imode/browserhome.gif");
    bmenu=new XAImageIcon("./img/imode/browsermenu.gif");
	
    bback.setBounds(8,7,30,30);
    bforw.setBounds(43,7,30,30);
    bhome.setBounds(95,7,30,30);
    bmenu.setBounds(120,7,43,30);
	
    toolbar.add(bback);
    toolbar.add(bforw);
    toolbar.add(bhome);
    toolbar.add(bmenu);
	
    contentPane.add(toolbar, BorderLayout.SOUTH);
	
    //mainFrame.pack();
    presentationPanel.setPreferredSize(new Dimension(162,245-toolbar.getPreferredSize().height));
    mainFrame.pack();
    mainFrame.doLayout();
    mainFrame.setResizable(false);
	
    if(!covers) {
      mainFrame.show();
      mainFrame.setVisible(true);
    } else {
      devicePane.setLayout(new BulletinLayout());
      rp=mainFrame.getRootPane(); // get rootpane from real Frame
      rp.setLayout(new BorderLayout());
	    
      rp.setBounds(68,68,162,245);
      devicePane.add(rp);
	    
      devicePicture.setBounds(0,0,300,745);
      devicePane.add(hangup);
      devicePane.add(back);
      devicePane.add(forward);
      devicePane.add(home);
      devicePane.add(menu);
      devicePane.add(up);
      devicePane.add(down);
      devicePane.add(left);
      devicePane.add(right);
      devicePane.add(onoff);
      devicePane.add(luuri);
      devicePane.add(light);
      devicePane.add(devicePicture);
	    
      deviceFrame.pack();
      deviceFrame.setResizable(false);
      deviceFrame.show();
    }
  }

  public void reValidateDocumentArea() {
    mainFrame.validate();
  }

  public void validate() {
    mainFrame.validate();
  }
    
  public Dimension getPreferredSize() {
    return new Dimension(300,745);
  }
    
  /**
   * Add a window listener for window closing events
   * 
   */
  private void attachListeners() {
    if(covers) {
      deviceFrame.addWindowListener(new java.awt.event.WindowAdapter() 
	{
	  public void windowClosing(java.awt.event.WindowEvent e)
	  {
	    browser.closeBrowser();
	    System.exit(0);
	  }
	});
    } else {
      mainFrame.addWindowListener(new java.awt.event.WindowAdapter() 
	{
	  public void windowClosing(java.awt.event.WindowEvent e)
	  {
	    browser.closeBrowser();
	    System.exit(0);
	  }
	});
    }
  }
    
  public JMenu getMLFCMenu() {
    return null;
  }

  public void navigate(int navigationCommand){
  }
    
  public void openFile(String fileName){
  }

  public void openLocation(URL url){
  }

  public void openLocation(String urlName){
  }
    
  public void exit(){
  }

  public void changeViewEvent(int viewType){
  }

  public void resizeEvent(Dimension size){
  }
    
  /**
   * Set statusbar text
   * 
   * @param the text
   */
  public void setStatusText(String statusText){
    //statusBar.setText(statusText);
  }
    
  public void setEnabledBack(boolean value) {
    back.setEnabled(value);
  }

  public void setEnabledForward(boolean value) {
    forward.setEnabled(value);
  }

  public void setEnabledHome(boolean value){
    home.setEnabled(value);
  }

  public void setEnabledStop(boolean value){
  }
    
  public void setEnabledReload(boolean value){
    back.setEnabled(value);
  }
    
  public void setEnabledAllMenus(boolean value){
  }

  public void setEnabledLocationCombo(boolean value){
  }

  public void setTitle(String title){
    mainFrame.setTitle(title);
  }

  public void setEnabledOpenFile(boolean value){
  }


  public void setLocation(String s) {
    //toolBar.setText(s);
  }
  
  /**
   * @return The browserConstraints of this gui
   */
  public BrowserConstraints getBrowserConstraints() {
    constraints.setDisplaySize(new Dimension(162,245));
    return constraints;
  }
    
  /** 
   * Browser is working, animate the animator
   */
  public void browserWorking() {
    blink.play();
  }

  /** 
   * Browser is finished, stop the animator
   */
  public void browserReady() {
    blink.pause();
    presentationPanel.invalidate();
    presentationPanel.validate();
    presentationPanel.repaint();
    //toolBar.doLayout();
    mainFrame.doLayout();
    mlfcController=browser.getMLFCController();
  }

  public Container getContentPanel() {

    return (Container)presentationPanel;
  }
  
  public Window getWindow() {
    return mainFrame;
  }
  
  public void destroy() {
    mainFrame.dispose();
    if(covers)
      deviceFrame.dispose();
  }

  private class GuiListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      String cmd=e.getActionCommand();
      Log.debug(cmd + " " + mlfcController);
      if(cmd.equals("onoff")) {
	browser.getGUIManager().createMainGUI(true);

      } else if(cmd.equals("left")) {
	mlfcController.pageBack();

      } else if(cmd.equals("right")) {
	mlfcController.pageForward();

      } else if(cmd.equals("down")) {
	if(!mlfcController.scrollDown())
	  mlfcController.pageForward();

      } else if(cmd.equals("up")) {
	if(!mlfcController.scrollUp())
	  mlfcController.pageBack();

      } else if(cmd.equals("back")) {
	browser.navigate(NavigationState.BACK);

      } else if(cmd.equals("forward")) {
	browser.navigate(NavigationState.FORWARD);

      } else if(cmd.equals("home")) {
	browser.navigate(NavigationState.HOME);
      }
	    
    }
  }
    
  private class Valo extends Thread {
    private boolean play=false;
    private Thread valo;
    private int delay=300;
	
    public void run() {
      while(play==true) {
	Log.debug("blink");
	try{Thread.sleep(delay);}catch(Exception e) {	
	}
	light.setImage(lighton);
	light.repaint();
	try{Thread.sleep(delay);}catch(Exception e) {	
	}
	light.setImage(lightoff);
	light.repaint();
      }

    }
	
    public void play() {
      if (play==false) {
	play=true;
	valo=new Thread(this);    
	valo.start();
      }
    }
	
    public void pause() {
      play=false;
      light.setImage(lightoff);
      light.repaint();
      valo=null;
    }
  }
  
  public ComponentFactory getComponentFactory() {
      if (componentFactory ==null) {
          componentFactory = new DefaultComponentFactory(browser);
      }
    return componentFactory;
  }
  
  public MLFCControls getMLFCControls() {
      if (mlfcControls==null) {
          mlfcControls = new DefaultMLFCControls(getComponentFactory());
      }
      return mlfcControls;
  }

}
