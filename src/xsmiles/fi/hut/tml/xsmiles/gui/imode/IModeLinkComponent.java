/* X-Smiles
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * PLEASE CHECK LICENSE_XSMILES THAT COMES WITH THE DISTRIBUTION FOR
 * MORE FURTHER INFO IN licenses/ DIRECTORY.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.imode;

import java.awt.*;
import fi.hut.tml.xsmiles.gui.components.awt.LinkComponent;

public class IModeLinkComponent extends LinkComponent {
    
  /**
   * Draw a rectangle around the active link, if in Digitv GUI
   *
   * @param g ...
   */ 
  public void paint(Graphics g) {
    super.paint(g);
    if(isActive()) {
      g.setColor(Color.black);
      for(int i=0;i<1; i++)
	g.drawRect(i, i, getSize().width-i, getSize().height-i);
    }
  }
  
}
