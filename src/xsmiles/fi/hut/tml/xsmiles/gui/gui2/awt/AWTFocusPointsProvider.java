
package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.util.Vector;

import java.awt.*;
import java.awt.event.*;

import fi.hut.tml.xsmiles.gui.components.XFocusManager;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;

public class AWTFocusPointsProvider implements FocusPointsProvider{

    /** Returns the actual potential focusable components. The actual location of those is related to their parent container **/

    public AWTGUI root;
    public Vector components = null;
    
    public AWTFocusPointsProvider(AWTGUI gui){
	root = gui;
	components = new Vector();
    }

    public Vector getComponents(Vector components, Container cont){
	int numComponents = cont.getComponentCount();

	for (int i= 0; i<numComponents; i++){
	    Component component = cont.getComponent(i);
	    if (isFocusablePoint(component)){
		components.addElement(component);
		// Just in case, this component has already the focuslistener
		component.removeKeyListener(root);
		component.addKeyListener(root);
	    }else if (component instanceof Container)
		// This case, we continue serching
		components = getComponents(components, (Container)component);
	}
	return components;
    }


    public boolean isFocusablePoint(Component component){
	return (!(component instanceof Container) && (component.isVisible()) && (component.isEnabled()) );
    }
    
    public FocusPoint[] getFocusPoints(){
	int i,numComponents;
	components.removeAllElements();
	components = getComponents(components, root.getRootContainer());
	numComponents = components.size();
	FocusPoint[] vec = new FocusPoint[numComponents];
	for (i=0;i<numComponents;i++){	    
	    Component component = (Component)components.elementAt(i);	
	    vec[i] = new FocusPoint(component.getLocation(),component.getSize(), component);
	}
	return vec;
    }

    public FocusPoint getNextFocusPoint(){
	return null;
    }
    
    public FocusPoint getPreviousFocusPoint(){
	return null;
    }

}
