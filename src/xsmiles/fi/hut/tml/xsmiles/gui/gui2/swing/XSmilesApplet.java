/*
 * Created on Apr 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.gui2.swing;

import java.applet.Applet;

import javax.swing.JButton;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.gui2.KickStart;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class XSmilesApplet extends Applet {
    
	public String paramStartURL;
    public void init() {
        /*
        width = getSize().width;
        height = getSize().height;
        setBackground( Color.black );
        */
        instance = new AppletKickStart(this);
        String[] args;
        
        this.paramStartURL = this.getParameter("startpage");
        if (paramStartURL!=null) args=new String[]{paramStartURL};
        else args = new String[]{};
        Log.debug("Applet specified startpage: "+paramStartURL);
        instance.initSequence(args);
        //this.add(new JButton("test"));
     }
    
    public static KickStart instance;
    
    
    
    
    
    public class AppletKickStart extends KickStart
    {
        Applet applet;
        public AppletKickStart(Applet ap)
        {
            super();
            applet=ap;
        }
        /**
         * This method launches our own Frame instead of the normal Browser.
         */
        public Browser launchBrowser(String initialURL) throws Throwable
        {
            this.gui=new SwingAppletGUI(applet);
            if (paramStartURL!=null) initialURL=paramStartURL;
            if (initialURL==null) initialURL="http://www.xsmiles.org/";
            
            gui.setInitialURL(new XLink(initialURL));
            //gui.setInitialURL("http://sinex.tml.hut.fi/nightly/demo/demos.xml");
            gui.show();
            Browser b=gui.getBrowser();
            return b;
        }
        
        public String getGUIClass()
        {
             return "fi.hut.tml.xsmiles.gui.gui2.swing.SwingAppletGUI";
            //return "fi.hut.tml.xsmiles.gui.gui2.swing.SwingGUI";
        }    
        // this can be overridden with an empty function for instance in applet
        protected void applySecurityPolicy(boolean enabled)
        {
            super.applySecurityPolicy(false);
        }

    }

}
