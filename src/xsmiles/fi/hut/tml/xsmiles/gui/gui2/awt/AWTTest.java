/*
 * (c) X-Smiles.org, TML, HUT
 */
package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.ScrollPane;


/**
 * @author honkkis
 *
 * Test AWT's capability to use a ScrollPane and inside
 * layering heavyweights on top of lightweights...
 * 
 * This works in J2ME PP reference impl, but not in Nokias
 * 9500 series 80 PP emulator.
 */
public class AWTTest extends Frame {

	public Container topLevelContainer;
	public Container scrollPane;
	public ContainerEx contentContainer;
	public Component lightweightBackground;
	public Button button1;
	public Button button2;
	
	public static void main(String[] args) {
		AWTTest test = new AWTTest();
		test.init();
	}
	
	public void init()
	{
		this.createUI();
	}
	
	public void createUI()
	{
		this.setSize(400,400);
		topLevelContainer = this.createContainer(new BorderLayout(),null);
		this.scrollPane=new ScrollPane(ScrollPane.SCROLLBARS_ALWAYS);
		this.lightweightBackground=new LightweightBackground();
		Dimension contentSize=new Dimension(800,1000);
		this.lightweightBackground.setSize(contentSize);
		this.contentContainer=this.createContainer(null,contentSize);
		
		this.contentContainer.add(lightweightBackground);
		this.contentContainer.setSize(contentSize);
		this.contentContainer.setSize(contentSize);
		this.scrollPane.add(this.contentContainer);
		this.topLevelContainer.add(this.scrollPane,BorderLayout.CENTER);
	
		this.createButtons();
		this.setLayout(new BorderLayout());
		this.add(topLevelContainer,BorderLayout.CENTER);
		
		
		this.show();
	}
	
	public void createButtons()
	{
		button1=new Button("Button 1");
		button1.setBounds(100,100,80,30);
		this.contentContainer.add(button1);
		button2=new Button("Button 2");
		button2.setBounds(200,400,80,40);
		this.contentContainer.add(button2);
	}
	
	/**
	 * extends the normal container with a sense of preferred size, which can be set
	 */
	public class ContainerEx extends Container
	{
	    Dimension p_size;
	    public void mySetSize(Dimension d)
	    {
	        p_size=d;
	    }
	    
	    public Dimension getSize()
	    {
	        if (p_size!=null) return p_size;
	        return super.getSize();
	    }
	    
	    public Dimension getPreferredSize()
	    {
	        if (p_size!=null) return p_size;
	        return super.getPreferredSize();
	        
	    }
	    	
	}
	
	public ContainerEx createContainer(LayoutManager layout, Dimension x)
	{
		ContainerEx c = new ContainerEx();
		c.setLayout(layout);
		if (x!=null) c.mySetSize(x);
		return c;
	}
	
	public class LightweightBackground extends Component
	{
		public LightweightBackground()
		{
		}
		// paint half-ref half-blue
		public void paint(Graphics g)
		{
		    int w = this.getSize().width;
		    int h = this.getSize().height;
			Rectangle clip = g.getClipBounds();
			g.setColor(Color.red);
			g.fillRect(0,0,w,h/2);
			g.setColor(Color.blue);
			g.fillRect(0,h/2+1,w,h);
			System.out.println("background size: "+this.getSize()+" parent size:"+this.getParent().getSize());
			super.paint(g);
		}
	}
}
