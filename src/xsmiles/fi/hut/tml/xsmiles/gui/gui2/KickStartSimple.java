/*
 * Created on 16.3.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fi.hut.tml.xsmiles.gui.gui2;

import fi.hut.tml.xsmiles.gui.gui2.KickStart;

/**
 * @author Mikko Honkala
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class KickStartSimple extends KickStart {
    public static void main(String args[])
    {
        instance = new KickStartSimple();
        instance.initSequence(args);
    }
    public String getGUIClass()
    {
    	// select either swing or awt version
        //return "fi.hut.tml.xsmiles.gui.gui2.awt.SimpleAWTGUI";
        return "fi.hut.tml.xsmiles.gui.gui2.swing.SimpleSwingGUI";
    }

}
