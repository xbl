/*
 * (c) X-Smiles.org, TML, HUT
 */
package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import fi.hut.tml.xsmiles.Log;


/**
 * @author honkkis
 *
 * Test AWT's capability to use a ScrollPane and inside
 * layering heavyweights on top of lightweights...
 * 
 * This works in J2ME PP reference impl, but not in Nokias
 * 9500 series 80 PP emulator.
 */
public class AWTEvents extends Frame {

	public Container topLevelContainer;
	public Button button1;
	
	public static void main(String[] args) {
		AWTEvents test = new AWTEvents();
		test.init();
	}
	
	public void init()
	{
		this.createUI();
	}
	
	public void createUI()
	{
		this.setSize(400,400);
		topLevelContainer = this.createContainer(null,null);
		this.createButtons();
		//this.setLayout(new BorderLayout());
		this.add(topLevelContainer,BorderLayout.CENTER);
		this.show();
	}
	
	public void createButtons()
	{
		button1=new Button("Button 1");
		button1.addKeyListener(new KeyAdapter(){
		    
            /* (non-Javadoc)
             * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
             */
            public void keyPressed(KeyEvent arg0)
            {
                // TODO Auto-generated method stub
                super.keyPressed(arg0);
                Log.info("Button: Keypressed: "+arg0);
            }
		    });
		button1.setBounds(100,100,80,30);
		this.topLevelContainer.add(button1);
	}
	
	/**
	 * extends the normal container with a sense of preferred size, which can be set
	 */
	public class ContainerEx extends Container
	{
	    protected void processKeyEvent(KeyEvent e)
	    {
	        Log.info("processEvent:"+e);
	    }
	}
	
	public ContainerEx createContainer(LayoutManager layout, Dimension x)
	{
		ContainerEx c = new ContainerEx();
		c.setLayout(layout);
		c.addKeyListener(new KeyAdapter(){
		    
            /* (non-Javadoc)
             * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
             */
            public void keyPressed(KeyEvent arg0)
            {
                // TODO Auto-generated method stub
                super.keyPressed(arg0);
                Log.info("Keypressed: "+arg0);
            }
		    });
		return c;
	}
	
}
