package fi.hut.tml.xsmiles.gui.gui2.swing;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

public class SimpleNoGUI extends SimpleSwingGUI
{
    public void createMenus()
    {
        
    }
    
    public void createActionContainer()
    {

    }
    
    public void decorateGUI()
    {
        this.getRootContainer().setLayout(new BorderLayout());
        Container contentArea = this.getContentArea();
        this.createListeners();
        this.getRootContainer().add(contentArea, BorderLayout.CENTER);
//        this.createActionContainer();
//        this.createStatusContainer();
//        this.getRootContainer().add(this.statusContainer, BorderLayout.SOUTH);
//        this.getRootContainer().add(this.topContainer, BorderLayout.NORTH);

        this.createMenus();
        //contentArea.setSize(800, 600);
        this.setDefaultWindowSize();
        //contentArea.setSize(780,580);
        
    }

}
