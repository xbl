/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.MenuBar;
import java.awt.Panel;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;


import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.gui.XSmilesDialog;
import fi.hut.tml.xsmiles.gui.XSmilesUIAWT;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;
import fi.hut.tml.xsmiles.gui.gui2.GUIInterface;
import fi.hut.tml.xsmiles.util.URLUtil;
import fi.hut.tml.xsmiles.event.GUIEvent;
import fi.hut.tml.xsmiles.event.GUIEventListener;

/**
 * 
 * This is a simple ABSTRACT GUI that does not use the OLD gui system.
 * 
 * The idea is that it creates a browser window without a OLD gui system and
 * then just gets the content area and adds it to itself.
 * 
 * Actually the old GUI system is still created under the hoods, but it is only
 * used to get events back...
 * 
 * Anyway, this should show that it is easy to use the browser inside a
 * container in a Java project.
 * 
 * Note that some settings (e.g. the ComponentFactory) are set in the
 * config.xml file.
 * 
 * Currently this does not support tabs, so it is always one main BrowserWindow
 * inside a AWTGUI.
 * 
 * This should be Swing-independent.
 * 
 * NOTE: SwingGUI extends this GUI, so that all changes here will be incorporated in both
 * versions
 * 
 * @author Mikko Honkala
 * 
 *  
 */
//public class AWTGUI extends Frame implements GUIEventListener
public class SimpleAWTGUI implements GUIEventListener, GUIInterface
{
    public static final String dialogClass="fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogHandler";
    public static final String title = "X-Smiles";

    public XLink initialURL;
    protected XSmilesDialog dialog ;

    public Browser browserWindow;

    /**
     * The structure of the containers :
     * 
     * ------------------------------------------------- 
	*          Menu 
     * ------------------------------------------------- 
	* actionContainer
     * (BorderLayout.NORTH) 
	*------------------------------------------------
     * 
     * BrowserWindow.contentArea() (BorderLayout.CENTER)
     * 
     * ------------------------------------------------ 
	* statusContainer
     * (BorderLayout.SOUTH)
     *  
     */

    public Container actionContainer;
    public Container statusContainer;

    public XCaption status;

    public static final String backCommand = "back";
    public static final String forwardCommand = "forward";
    public static final String homeCommand = "home";
    public static final String reloadCommand = "reload";
    public static final String xsmilesCommand = "xsmiles";

    protected XMenuBar menuBar;
    protected XMenu editMenu,fileMenu,bookmarksMenu, viewMenu, helpMenu, goMenu;
    
    // this is not necessary used in extending classes, there might not be a frame, use rootContainer instead
    protected Frame frame;
    
    protected Container rootContainer;
    
    public static final String configMenu = "edit_config";
    public static final String sethomepageMenu = "edit_homepage";
    public static final String exitMenu = "file_exit";
    public static final String openFileMenu = "file_open";
    public static final String aboutHelpMenu = "help_about";
    public static final String viewBookmarksMenu = "bookmarks_view";
    public static final String viewSpeechMenu = "speech_view";
    public static final String viewXMLSourceMenu = "xmlsource_view";
    public static final String viewShowGUIMenu = "view_showgui";
    public static final String viewHideGUIMenu = "view_hidegui";
    public static final String editBookmarksMenu = "bookmarks_edit";
    public static final String zoomPlusMenu = "view_zoomplus";
    public static final String zoomMinusMenu = "view_zoomminus";
    
    
    public SimpleAWTGUI()
    {
        super();
		setDoubleBuffering(true);
		this.rootContainer = this.createRootContainer();
    }
    
    public void openInNewWindow(XLink l, String id) {}

    /*
    protected GUITab createGUITab(String title, String id, URL url)
    {
        GUITab tab =  new GUITab();
        //tab.setURL(url);
        //tab.init();
    }*/
    
    protected Container createRootContainer()
    {
        this.frame=new Frame(title);
        listenForWindowClose(this.frame);
        return this.frame;
    }
    protected void listenForWindowClose(Window w)
    {
        w.addWindowListener(new WindowAdapter(){
            public void windowClosed(WindowEvent we)
            {
          	  browserWindow.closeBrowserWindow();
            }
        }
            );
        
    }
    
    protected Container getRootContainer()
    {
        return this.rootContainer;
    }

	protected void setDoubleBuffering(boolean b)
	{
		//CSSRenderer.doubleBuffering=b;
	}	
	
	public void show()
	{
	    this.getRootContainer().setVisible(true);
	    if (this.frame!=null) this.frame.show();
	}
	
	/** subclasses can override this */
	public void setDefaultWindowSize()
	{
        this.getRootContainer().setSize(850, 640);
	    
	}

    public void decorateGUI()
    {
        this.getRootContainer().setLayout(new BorderLayout());
        Container contentArea = this.getContentArea();
        this.createListeners();
        this.getRootContainer().add(contentArea, BorderLayout.CENTER);
        this.createActionContainer();
        this.createStatusContainer();
        this.getRootContainer().add(this.statusContainer, BorderLayout.SOUTH);
        this.getRootContainer().add(this.topContainer, BorderLayout.NORTH);

        this.createMenus();
        //contentArea.setSize(800, 600);
        this.setDefaultWindowSize();
        //contentArea.setSize(780,580);
        
    }
    
    protected void addMenuBar()
    {
        if (this.frame!=null)
            menuBar.addToFrame(this.frame);
        
    }
    
    /** menus */
    
    public void createMenus()
    {
        menuBar= this.browserWindow.getComponentFactory().getXMenuBar();
        
        fileMenu=this.browserWindow.getComponentFactory().getXMenu("File");
        fileMenu.add(this.createMenuItem("Open",this.openFileMenu));
        fileMenu.add(this.createMenuItem("Exit",this.exitMenu));
        menuBar.addMenu(fileMenu);
        
        editMenu=this.browserWindow.getComponentFactory().getXMenu("Edit");
        editMenu.add(this.createMenuItem("Set as Homepage",this.sethomepageMenu));
        editMenu.add(this.createMenuItem("Configuration",this.configMenu));
        menuBar.addMenu(editMenu);
        
        goMenu=this.browserWindow.getComponentFactory().getXMenu("Go");
        goMenu.add(this.createMenuItem("Back",this.backCommand));
        goMenu.add(this.createMenuItem("Forward",this.forwardCommand));
        goMenu.add(this.createMenuItem("Reload",this.reloadCommand));
        goMenu.add(this.createMenuItem("Home",this.homeCommand));
        menuBar.addMenu(goMenu);

        viewMenu=this.browserWindow.getComponentFactory().getXMenu("View");
        viewMenu.add(this.createMenuItem("Zoom In  +",this.zoomPlusMenu));
        viewMenu.add(this.createMenuItem("Zoom Out -",this.zoomMinusMenu));
        viewMenu.add(this.createMenuItem("View XML Source",this.viewXMLSourceMenu));
        viewMenu.add(this.createMenuItem("Speech Dialog",this.viewSpeechMenu));
        viewMenu.add(this.createMenuItem("Show GUI",this.viewShowGUIMenu));
        viewMenu.add(this.createMenuItem("Hide GUI",this.viewHideGUIMenu));
        menuBar.addMenu(viewMenu);
        
        bookmarksMenu=this.browserWindow.getComponentFactory().getXMenu("Bookmarks");
        bookmarksMenu.add(this.createMenuItem("View Bookmarks",this.viewBookmarksMenu));
        bookmarksMenu.add(this.createMenuItem("Edit Bookmarks",this.editBookmarksMenu));
        menuBar.addMenu(bookmarksMenu);
        
        helpMenu=this.browserWindow.getComponentFactory().getXMenu("Help");
        helpMenu.add(this.createMenuItem("About",this.aboutHelpMenu));
        menuBar.addMenu(helpMenu);
        
        this.addMenuBar();
    }
    
    protected XMenuItem createMenuItem(String name,String actionCommand)
    {
        final String command = actionCommand;
        XMenuItem item = this.browserWindow.getComponentFactory().getXMenuItem(name);
        item.addActionListener(new ActionListener()
                {
            // Actions from the buttons, back, forward, etc
            public void actionPerformed(ActionEvent e)
            {
                Log.debug("menu actionPerformed: " + e);
                action(command);
            }
        });
        return item;
        
    }
    
    protected Container createContainer()
    {
        return new Panel();
    }
    protected Component createLocation()
    {
        locationField = this.browserWindow.getComponentFactory().getXInput();
        locationField.addActionListener(new LocationFieldListener());
        return locationField.getComponent();
    }
    protected class LocationFieldListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String s=locationField.getText();
            if (s.indexOf(':') < 0 && s.startsWith(".") == false)
            {
                s = "http://"+s;
            }
            loadLocation(s);
        }
    }
    public void loadLocation(String location)
    {
        this.browserWindow.openLocation(location);
    }
    protected XInput locationField;
    protected Container topContainer;
    public void createActionContainer()
    {
        this.topContainer=this.createContainer();
        this.topContainer.setLayout(new BorderLayout());
        this.actionContainer=this.createContainer();
        Font acFont = this.getActionFont();
        if (acFont!=null)
        {
            this.topContainer.setFont(acFont);
            this.actionContainer.setFont(acFont);
        }
        this.actionContainer.setLayout(new FlowLayout(FlowLayout.LEFT));
	 this.actionContainer.add(createActionButton(backCommand, null, backCommand).getComponent());
	 this.actionContainer.add(createActionButton(forwardCommand, null, forwardCommand).getComponent());
        this.actionContainer.add(createActionButton(homeCommand, null, homeCommand).getComponent());
	 this.actionContainer.add(createActionButton(reloadCommand, null, reloadCommand).getComponent());
        //locationField.setSize(400,20);
        this.topContainer.add(actionContainer,BorderLayout.WEST);
        this.topContainer.add(createLocation(),BorderLayout.CENTER);
        this.topContainer.add(createActionButton("X", null, xsmilesCommand).getComponent(),BorderLayout.EAST);
    }
    
    protected Font getActionFont()
    {
        return new Font("SansSerif",Font.PLAIN,8);
    }
    protected Font getStatusFont()
    {
        return this.getActionFont();
    }

    public void createStatusContainer()
    {
        this.status = this.browserWindow.getComponentFactory().getXCaption("Browser ready.");
        this.statusContainer = this.createContainer();
        this.statusContainer.setLayout(new BorderLayout());
        this.statusContainer.add(status.getComponent(), BorderLayout.CENTER);
        Font acFont = this.getActionFont();
        if (acFont!=null) this.statusContainer.setFont(acFont);
        
    }

    protected XComponent createActionButton(String name, String icon, String actionCommand)
    {
        final String command = actionCommand;
        XButton button = this.browserWindow.getComponentFactory().getXButton(name, icon);
        button.addActionListener(new ActionListener()
        {
            // Actions from the buttons, back, forward, etc
            public void actionPerformed(ActionEvent e)
            {
                Log.debug("actionPerformed: " + e);
                action(command);
            }
        });
        return button;
    }

    public void setInitialURL(XLink url)
    {
        this.initialURL = url;
    }
    protected String getComponentFactoryClassName()
    {
    	return "fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory";
    }
    protected String getGUIName()
    {
        return "nullGUIAWT";
    }
    
    protected void createBrowserWindow()
    {
        String id = "initial";
        // this is necessary for the initial content pane creation...
        Browser.componentFactoryClassName = this.getComponentFactoryClassName();

        browserWindow = new Browser(initialURL, id, true,this.getGUIName());
        if (this.initialURL!=null) this.setLocation(initialURL.getURLString());
        this.createBrowserListeners();
        Container contentArea = browserWindow.getContentArea();
        decorateGUI();
        Log.debug(
            "Added the content area to the frame. Content area size: " + browserWindow.getContentArea().getSize());
    }

    public Browser getBrowser()
    {
        if (browserWindow == null)
            createBrowserWindow();
        return browserWindow;
    }

    public Container getContentArea()
    {
        return this.getBrowser().getContentArea();
    }

    public void createBrowserListeners()
    {
        this.getBrowser().getCurrentGUI().addGUIEventListener(this);
    }

    protected void createListeners()
    {
        Frame f = this.frame;
        if (this.rootContainer instanceof Frame) // for PC havi
            f=(Frame)rootContainer;
        final Frame fr=f;
        if (fr!=null)
        {
	        fr.addWindowListener(new java.awt.event.WindowAdapter()
	        {
	            public void windowClosing(java.awt.event.WindowEvent e)
	            {
	                fr.dispose();
	                System.exit(0);
	            }
	        });
        }
    }
    
    protected double zoomFactor=0.25;

    // Actions from the buttons, back, forward, etc
    public void action(Object action)
    {
        Log.debug("actionPerformed: " + action);
        try
        {
	        if (action.equals(backCommand))
	        {
	            this.browserWindow.navigate(NavigationState.BACK);
	        } 
	        else if (action.equals(forwardCommand))
	        {
	            this.browserWindow.navigate(NavigationState.FORWARD);
	        } 
	        else if (action.equals(reloadCommand))
	        {
	            this.browserWindow.navigate(NavigationState.RELOAD);
	        } 
	        else if (action.equals(homeCommand))
	        {
	            this.browserWindow.navigate(NavigationState.HOME);
	        } 
	        else if (action.equals(xsmilesCommand))
	        {
	            this.browserWindow.openLocation("http://www.xsmiles.org/");
	        }
	        else if (action.equals(this.configMenu))
	        {
	            this.browserWindow.openLocation(browserWindow.getXResource("xsmiles.config.editor").toString());
	        }
	        else if (action.equals(this.exitMenu))
	        {
	            System.exit(0);
	        }
	        else if (action.equals(this.openFileMenu))
	        {
	            this.openFileDialog();
	        }
	        
	        else if (action.equals(this.viewBookmarksMenu))
	        {
	            this.browserWindow.openLocation(browserWindow.getXResource("xsmiles.bookmarks.view").toString());
	        }
	        else if (action.equals(this.viewShowGUIMenu))
	        {
	            this.showGUIButtons();
	        }
	        else if (action.equals(this.viewHideGUIMenu))
	        {
	            this.hideGUIButtons();
	        }
	        else if (action.equals(this.editBookmarksMenu))
	        {
	            this.browserWindow.openLocation(browserWindow.getXResource("xsmiles.bookmarks.document").toString());
	        }
	        else if (action.equals(this.zoomPlusMenu))
	        {
	            browserWindow.setZoom(browserWindow.getZoom()*(1+zoomFactor));
	        }
	        else if (action.equals(this.zoomMinusMenu))
	        {
	            browserWindow.setZoom(browserWindow.getZoom()*(1-zoomFactor));
	        }
	        else if (action.equals(this.aboutHelpMenu))
	        {
	            this.browserWindow.openLocation(browserWindow.getXResource("xsmiles.about"));
	        }
	        else if (action.equals(this.sethomepageMenu))
	        {
	            
	            String urli=this.browserWindow.getDocumentHistory().getLastDocument().getURLString();
	            if(urli!=null || ! urli.equals(""))
	            {
	                this.browserWindow.getBrowserConfigurer().setProperty("main/homepage",urli);
	                this.browserWindow.getBrowserConfigurer().writeConfig();
	            }
	        }
	        else if (action.equals(this.viewSpeechMenu))
	        {
	            try
	            {
		            dialog = (XSmilesDialog)Class.forName(this.dialogClass).newInstance();
		            dialog.setBrowserWindow(this.browserWindow);
		            dialog.start();
	            } catch (Throwable t)
	            {
	                Log.error(t);
	            }
	        }
	        else if (action.equals(this.viewXMLSourceMenu))
	        {
	            /** The modes are from XSmilesView class */
	            //public static void showSourceStatic(XMLDocument doc, int mode, String heading)
	            XSmilesUIAWT.showSourceStatic(this.browserWindow.getXMLDocument(), 
	                    XsmilesView.SOURCEMLFC,
	                    this.browserWindow.getCurrentPage().getURLString()+" : Source");
	        }

        } catch (MalformedURLException e)
        {
            Log.error(e);
        }
    }
    
    /**
     * 
     */
    private void hideGUIButtons()
    {
        try
        {
            Log.debug("Removing button container");
            //this.actionContainer.setVisible(false);
            this.getRootContainer().remove(this.topContainer);
            this.getRootContainer().validate();
        } catch (Exception e)
        {
            Log.error(e);
        }
        
    }

    /**
     * 
     */
    private void showGUIButtons()
    {
        try
        {
            Log.debug("Re-adding button container");
            this.getRootContainer().add(this.topContainer, BorderLayout.NORTH);
            this.getRootContainer().validate();
        } catch (Exception e)
        {
            Log.error(e);
        }
    }

    private void openFileDialog()
    {
        URL requiredDoc=null;
        XFileDialog fc=this.browserWindow.getComponentFactory().getXFileDialog(false);
        int result=fc.select();
        
        if (result==fc.SELECTION_FAILED) return;
        File file = fc.getSelectedFile();
        try
        {
            requiredDoc =  URLUtil.fileToURL(file);
        } catch(Exception e)
        {
            // Log.error(e);
        }
        if(requiredDoc!=null)
            this.browserWindow.openLocation(requiredDoc, true);
    }
    

    /** the GUI event listener interface */
    public void start()
    {
    };
    public void destroy()
    {
        if (dialog!=null) this.dialog.destroy();
        this.browserWindow.getCurrentGUI().destroy();
    };
    public void openInNewTab(XLink l, String id)
    {
    };
    public void setStatusText(String statusText)
    {
    	//Log.debug("Status: "+statusText);
		if (status!=null)this.status.setText(statusText);
    };
    public void setEnabledBack(boolean value)
    {
    };
    public void setEnabledForward(boolean value)
    {
    };
    public void setEnabledHome(boolean value)
    {
    };
    public void setEnabledStop(boolean value)
    {
    };
    public void setEnabledReload(boolean value)
    {
    };
    public void setTitle(String title)
    {
    };
    public void setLocation(String s)
    {
        if (locationField!=null)
            this.locationField.setText(s);
    };
    public void browserWorking()
    {
    };
    public void GUIEvent(GUIEvent ev)
    {
    };

    public void browserReady()
    {
        Log.debug("Browser finished loading document.");
        this.setLocation(this.browserWindow.getCurrentPage().getURL().toString());
    }

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.gui.gui2.GUIInterface#setXMLConfigurer(fi.hut.tml.xsmiles.XMLConfigurer)
	 */
	public void setXMLConfigurer(XMLConfigurer c) {
		// TODO Auto-generated method stub
		
	};
}
