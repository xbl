/*
 * (c) X-Smiles.org, TML, HUT
 */
package fi.hut.tml.xsmiles.gui.gui2.swing;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * @author honkkis
 *
 * Test AWT's capability to use a ScrollPane and inside
 * layering heavyweights on top of lightweights...
 * 
 * This works in J2ME PP reference impl, but not in Nokias
 * 9500 series 80 PP emulator.
 */
public class SwingTest extends JFrame {

	public Container topLevelContainer,contentContainer;
	public Container scrollPane;
	public Component lightweightBackground;
	public JButton button1;
	public JButton button2;
	
	public static void main(String[] args) {
		SwingTest test = new SwingTest();
		test.init();
	}
	
	public void init()
	{
		this.createUI();
	}
	
	public void createUI()
	{
		this.setSize(400,400);
		topLevelContainer = this.createContainer(new BorderLayout(),null);
		this.scrollPane=new ScrollPane(ScrollPane.SCROLLBARS_ALWAYS);
		this.lightweightBackground=new LightweightBackground();
		Dimension contentSize=new Dimension(800,1000);
		this.lightweightBackground.setSize(contentSize);
		this.contentContainer=this.createContainer(null,contentSize);
		
		//this.contentContainer.add(lightweightBackground);
		this.contentContainer.setSize(contentSize);
		this.contentContainer.setSize(contentSize);
		this.scrollPane.add(this.contentContainer);
		this.topLevelContainer.add(this.scrollPane,BorderLayout.CENTER);
	
		this.createButtons();
		this.setLayout(new BorderLayout());
		this.add(topLevelContainer,BorderLayout.CENTER);
		
		
		this.show();
	}
    int size=10;
	
	public void createButtons()
	{
		button1=new JButton("Change font of button2");
		button1.setLocation(100,100);
        button1.setSize(button1.getPreferredSize());
		this.contentContainer.add(button1);
		button2=new JButton("Button 2");
		button2.setLocation(100,160);
        button2.setSize(button2.getPreferredSize());
		this.contentContainer.add(button2);
        
        button1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent arg0)
            {
                button2.setFont(new Font("sans-serif",Font.BOLD,size++));
                Dimension pref = button2.getPreferredSize();
                System.out.println("button2 pref size:"+pref);
                button2.setSize(button2.getPreferredSize());
            }
        });
	}
	
	/**
	 * extends the normal container with a sense of preferred size, which can be set
	 */
	
	public Container createContainer(LayoutManager layout, Dimension x)
	{
		JPanel c = new JPanel();
		c.setLayout(layout);
		if (x!=null) c.setSize(x);
		return c;
	}
	
	public class LightweightBackground extends Component
	{
		public LightweightBackground()
		{
		}
		// paint half-ref half-blue
		public void paint(Graphics g)
		{
		    int w = this.getSize().width;
		    int h = this.getSize().height;
			Rectangle clip = g.getClipBounds();
			g.setColor(Color.red);
			g.fillRect(0,0,w,h/2);
			g.setColor(Color.blue);
			g.fillRect(0,h/2+1,w,h);
			System.out.println("background size: "+this.getSize()+" parent size:"+this.getParent().getSize());
			super.paint(g);
		}
	}
}
