package fi.hut.tml.xsmiles.gui.gui2.swing;

public class KickStartNoGUI extends KickStartSwing
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        instance = new KickStartNoGUI();
        instance.initSequence(args);
    }
    
    public String getGUIClass()
    {
        return "fi.hut.tml.xsmiles.gui.gui2.swing.SimpleNoGUI";
    }
}
