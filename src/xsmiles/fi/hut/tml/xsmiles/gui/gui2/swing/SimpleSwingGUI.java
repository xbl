/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.gui2.swing;

import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fi.hut.tml.xsmiles.gui.gui2.awt.SimpleAWTGUI;

/**
 * 
 * This is a simple ABSTRACT GUI that does not use the OLD gui system.
 * 
 * The idea is that it creates a browser window without a OLD gui system and
 * then just gets the content area and adds it to itself.
 * 
 * Actually the old GUI system is still created under the hoods, but it is only
 * used to get events back...
 * 
 * Anyway, this should show that it is easy to use the browser inside a
 * container in a Java project.
 * 
 * Note that some settings (e.g. the ComponentFactory) are set in the
 * config.xml file.
 * 
 * Currently this does not support tabs, so it is always one main BrowserWindow
 * inside a AWTGUI.
 * 
 * This should be Swing-independent.
 * 
 * NOTE: SwingGUI extends this GUI, so that all changes here will be incorporated in both
 * versions
 * 
 * @author Mikko Honkala
 * 
 *  
 */
//public class AWTGUI extends Frame implements GUIEventListener
public class SimpleSwingGUI extends SimpleAWTGUI 
{
  
    public SimpleSwingGUI()
    {
        super();
    }
    
    protected String getComponentFactoryClassName()
    {
    	return "fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory";
    }
    protected String getGUIName()
    {
        return "nullGUI";
    }
    protected Container createRootContainer()
    {
        this.frame=new JFrame(title);
        listenForWindowClose(this.frame);
        return this.frame;
    }
    protected Container createContainer()
    {
        return new JPanel();
    }

    
}
