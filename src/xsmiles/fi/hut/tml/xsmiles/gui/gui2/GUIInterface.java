/*
 * Created on 16.3.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fi.hut.tml.xsmiles.gui.gui2;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLConfigurer;

/**
 * @author Mikko Honkala
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface GUIInterface {
    public void setXMLConfigurer(XMLConfigurer c);
    public void setInitialURL(XLink url);
	public void show();
	public Browser getBrowser();


}
