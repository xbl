/*
 * Created on 16.3.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.util.Enumeration;
import java.util.Hashtable;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.xml.serializer.SerializerFactory;
import fi.hut.tml.xsmiles.xml.serializer.XMLSerializerInterface;

/**
 * @author Mikko Honkala
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PlatformDetails {
	
	protected BrowserWindow iBW;
	public PlatformDetails(BrowserWindow aBW)
	{
		iBW=aBW;
	}
	
	protected Hashtable getPlatformDetailsHash()
	{
		return System.getProperties();
	}
    String ns = "http://www.w3.org/1999/xhtml";
	protected Document getPlatformDetails()
	{
		Document doc = iBW.getXMLParser().createEmptyDocument();
		Element root = doc.createElementNS(ns,"html");
		doc.appendChild(root);
        Element head = doc.createElementNS(ns,"head");
        root.appendChild(head);

        Element style = doc.createElementNS(ns,"style");
        head.appendChild(style);
        style.setAttribute("type","text/css");
        style.appendChild(doc.createTextNode("body {padding-left:20px;} \n h2 {border-style:solid;} \np {margin:0px;padding:0px;} \n h1 {border-style:solid;background-color:blue;color:white}\n"));
        
        Element body = doc.createElementNS(ns,"body");
		root.appendChild(body);
		Element h1 = doc.createElementNS(ns,"h1");
		body.appendChild(h1);
		h1.appendChild(doc.createTextNode("X-Smiles platform runtime details"));
		Element div = doc.createElementNS(ns,"div");
		body.appendChild(div);
		//Element p = doc.createElementNS(ns,"p");
		//div.appendChild(p);
		//p.appendChild(doc.createTextNode("Java vendor: "));
		Element table = doc.createElementNS(ns,"table");
		div.appendChild(table);
		Element tbody = doc.createElementNS(ns,"tbody");
		table.appendChild(tbody);
		Hashtable detailshash = this.getPlatformDetailsHash();
		for (Enumeration e=detailshash.keys();e.hasMoreElements();)
		{
			String key = (String)e.nextElement();
			String value = (String)detailshash.get(key);
			Element tr = doc.createElementNS(ns,"tr");
			tbody.appendChild(tr);
			Element td = doc.createElementNS(ns,"td");
			tr.appendChild(td);
			td.appendChild(doc.createTextNode(key));
			td = doc.createElementNS(ns,"td");
			tr.appendChild(td);
			td.appendChild(doc.createTextNode(value));
		}
        
        Document configDoc = iBW.getBrowserConfigurer().getConfigDocument();
        if (configDoc!=null)
        {
                Element h12 = doc.createElementNS(ns,"h1");
                body.appendChild(h12);
                h12.appendChild(doc.createTextNode("X-Smiles configuration"));
                Element div2 = doc.createElementNS(ns,"div");
                body.appendChild(div2);
                
                addBrowserConfig(div2,configDoc.getDocumentElement());
        }
        
		return doc;
	}
    
    public boolean containsElements(Element parent)
    {
        Node child = parent.getFirstChild();
        while (child!=null)
        {
            if (child.getNodeType()==Node.ELEMENT_NODE)
            {
                return true;
            }
            child=child.getNextSibling();
        }
        return false;
    }
	
    public void addBrowserConfig(Element parent, Element config)
    {
          Node child = config.getFirstChild();
          while (child!=null)
          {
              if (child.getNodeType()==Node.ELEMENT_NODE)
              {
                  if (containsElements((Element)child))
                  {
                      String name = ((Element)child).getLocalName();
                      Element h12 = parent.getOwnerDocument().createElementNS(ns,"h2");
                      parent.appendChild(h12);
                      h12.appendChild(parent.getOwnerDocument().createTextNode("Group: '"+name+"'"));
                      
                  }
                  addBrowserConfig(parent,(Element)child);
              }
              else if (child.getNodeType()==Node.TEXT_NODE)
              {
                  String value=child.getNodeValue();
                  String name=child.getParentNode().getLocalName();
                  Element p = parent.getOwnerDocument().createElementNS(ns,"p");
                  parent.appendChild(p);
                  p.appendChild(parent.getOwnerDocument().createTextNode(name+" = "+value));
              }
              child=child.getNextSibling();
          }
    }
    
	public String getPlatformDetailsString()
	{
		try
		{
			Document doc =this.getPlatformDetails();
			SerializerFactory fact = new SerializerFactory();
			XMLSerializerInterface ser = (XMLSerializerInterface)fact.getSerializer(SerializerFactory.SERIALIZER_XML);
			String ret = ser.writeToString(doc);
			return ret;
		} catch (Exception e)
		{
			Log.error(e);
			return "error: "+e.toString();
		}
		// TODO: serialize the platform details
	}
	
	
}
