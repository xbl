/*
 * Created on Mar 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.gui2.swing;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.swing.SwingMenuBar;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SwingAppletGUI extends SwingGUI
{
    public Container appletContainer;
    public SwingAppletGUI()
    {
        Log.error("NEVER CALL THIS");
    }
    public SwingAppletGUI(Container root)
    {
        super();
        this.appletContainer=root;
    }
    protected Container createRootContainer()
    {
        return this.appletContainer;
    }
    public Container getRootContainer()
    {
        if (rootContainer==null) rootContainer=appletContainer;
        return this.rootContainer;
    }
    protected void addMenuBar()
    {
            Object bar = menuBar.getObject();
            if (bar instanceof JMenuBar && this.getRootContainer()!=null)
            {
            	JMenuBar menubar = (JMenuBar)bar;
            	this.getRootContainer().add(menubar,BorderLayout.NORTH);
            }
    }

}
