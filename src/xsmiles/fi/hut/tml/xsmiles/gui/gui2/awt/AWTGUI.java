/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.awt.event.MouseEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.Resources;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XLinkWithContent;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.csslayout.*;
import fi.hut.tml.xsmiles.gui.XSmilesDialog;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.gui.components.XFocusManager;
import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;
import fi.hut.tml.xsmiles.gui.components.XTabbedPane;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.gui2.GUIInterface;
import fi.hut.tml.xsmiles.mlfc.general.Helper;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;
import fi.hut.tml.xsmiles.util.URLUtil;

import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

/**
 * 
 * This is a simple ABSTRACT GUI that does not use the OLD gui system.
 * 
 * The idea is that it creates a browser window without a OLD gui system and
 * then just gets the content area and adds it to itself.
 * 
 * Actually the old GUI system is still created under the hoods, but it is only
 * used to get events back...
 * 
 * Anyway, this should show that it is easy to use the browser inside a
 * container in a Java project.
 * 
 * Note that some settings (e.g. the ComponentFactory) are set in the
 * config.xml file.
 * 
 * Tabs are also supported through GUITab and XTabbedPane classes.
 * 
 * This class is completely Swing-independent.
 * 
 * NOTE: SwingGUI extends this GUI, so that all changes here will be incorporated in both
 * versions
 * 
 * @author Mikko Honkala
 * 
 *  
 */
//public class AWTGUI extends Frame implements GUIEventListener
public class AWTGUI implements XChangeListener, KeyListener, GUIInterface
{
    // the new system for speech
    public static final String dialogClass="fi.hut.tml.xsmiles.mlfc.xforms.dialog.DynamicDialogHandler";
    
    // the old system for speech
    //public static final String dialogClass="fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogHandler";
    public static final String title = "X-Smiles";
    protected int defaultSizeX=800, defaultSizeY=800;

    public XLink initialURL;
    String id; 
    protected XSmilesDialog dialog ;

    public Vector tabs;
    
    public GUITab currentTab;

    public FocusPointsProvider gui_provider;
    public FocusPointsProvider content_provider;
    public XFocusManager focusManager;
    
    protected XMLConfigurer config;
    
    XTabbedPane tabbedPane;

    public static final String backCommand = "back";
    public static final String forwardCommand = "forward";
    public static final String homeCommand = "home";
    public static final String stopCommand = "stop";
    public static final String reloadCommand = "reload";
    public static final String xsmilesCommand = "xsmiles";

    protected XMenuBar menuBar;
    protected XMenu editMenu,fileMenu,bookmarksMenu, viewMenu, helpMenu, goMenu, stylesheetMenu;
    
    // this is not necessary used in extending classes, there might not be a frame, use rootContainer instead
    private Frame frame;
    
    protected Container rootContainer;
    
    public static final String changeGUIMenu = "CHANGEGUI:";// this will be appended with the gui name
    public static final String changeStyleMenu = "CHANGESTYLE:";// this will be appended with the gui name
    public static final String changeMediaMenu = "CHANGEMEDIA:"; // will be appended with the media name
    public static final String changeZoomMenu = "CHANGEZOOM:"; // appended with the zoom percentage
    public static final String configMenu = "edit_config";
    public static final String sethomepageMenu = "edit_homepage";
    public static final String exitMenu = "file_exit";
    public static final String fileNewTabMenu = "file_newtab";
    public static final String fileCloseTabMenu = "file_closetab";
    public static final String fileNewWindowMenu = "file_newwindow";
    public static final String openFileMenu = "file_open";
    public static final String saveFileMenu = "file_save";
    public static final String aboutHelpMenu = "help_about";
    public static final String platformHelpMenu = "help_platform";
    public static final String viewBookmarksMenu = "bookmarks_view";
    public static final String viewSpeechMenu = "speech_view";
    public static final String viewXMLSourceMenu = "xmlsource_view";
    public static final String viewXSLSourceMenu = "xslsource_view";
    public static final String viewSourceMenu = "source_view";
    public static final String viewTreeMenu = "tree_view";
    public static final String viewShowGUIMenu = "view_showgui";
    public static final String viewHideGUIMenu = "view_hidegui";
    public static final String viewLogMenu = "view_log";
    public static final String editBookmarksMenu = "bookmarks_edit";
    public static final String editWebsearchMenu = "edit_websearch";
    public static final String zoomPlusMenu = "view_zoomplus";
    public static final String zoomMinusMenu = "view_zoomminus";
    
    public static final String searchURL="http://www.google.com/";
    
    public AWTGUI()
    {
        super();
        this.tabs=new Vector();
	this.rootContainer = this.createRootContainer();
        // this is necessary for the initial content pane creation...
        Browser.componentFactoryClassName = this.getComponentFactoryClassName();
    }
    
    
    protected GUITab getCurrentTab()
    {
        return this.currentTab;
    }
    
    public XTabbedPane getTabbedPane()
    {
        return this.tabbedPane;
    }
    
    public void setXMLConfigurer(XMLConfigurer c)
    {
        this.config=c;
    }
    
    protected Container createRootContainer()
    {
        this.frame=new Frame(title);
        listenForWindowClose(this.frame);
        return this.frame;
    }
    protected void listenForWindowClose(Window w)
    {
        w.addWindowListener(new WindowAdapter(){
            public void windowClosed(WindowEvent we)
            {
                closeAllTabs();
            }
        }
            );
        
    }
    
    protected ComponentFactory getComponentFactory()
    {
        return this.getBrowserWindow().getComponentFactory();
    }
    
    public BrowserWindow getBrowserWindow()
    {
        BrowserWindow w=this.currentTab.getBrowserWindow();
        if (w==null)
        {
            Log.error("AWTGUI:BrowserWindow is null.");
        }
        return w;
    }
    
    protected void closeCurrentTab()
    {
        if (this.tabbedPane.getMaxTabCount()>1)
        {
            GUITab tabToBeRemoved=this.currentTab;
	        this.tabbedPane.remove(tabToBeRemoved.getComponent());
	        this.tabs.removeElement(tabToBeRemoved);
	        tabToBeRemoved.destroy();
	        if (this.tabs.size()==0)
	        {
	            this.disposeWindow();
	            //System.exit(0); // TODO: multiple windows
	        }
	        // change the current tab
	        GUITab tab = this.getGUITabForComponent(this.tabbedPane.getSelectedComponent());
	        if (tab!=null) this.currentTab=tab;
        }
    }
    
    
    protected void disposeWindow()
    {
        this.frame.dispose();
    }
    
    protected void closeAllTabs()
    {
        for (Enumeration e=this.tabs.elements();e.hasMoreElements();)
        {
            GUITab tab = (GUITab)e.nextElement();
            tab.destroy();
        }
        this.tabs.clear();
    }
    
    public Container getRootContainer()
    {
        return this.rootContainer;
    }
    
    protected void saveContent() throws FileNotFoundException, IOException
    {
        XFileDialog saved;
        URL requiredDoc=null;
        saved=this.getBrowserWindow().getComponentFactory().getXFileDialog(true);
        int result=saved.select();
        
        if (result==saved.SELECTION_FAILED) return;
        File file = saved.getSelectedFile();
        XSmilesContentHandler handler=this.getBrowserWindow().getContentManager().getPrimaryContentHandler();
        if (handler!=null&&file!=null)
        {
            Log.info("Saving file into: "+file.getAbsolutePath());
            FileOutputStream os = new FileOutputStream(file);
            InputStream contentStream=handler.getContentStream();
            Helper.copyStream(contentStream,os,2000);
        }
    }

    
    public void show()
    {
	this.decorateGUI();
	this.getRootContainer().setVisible(true);
	if (this.frame!=null) this.frame.show();
	if (this.getComponentFactory() instanceof AWTComponentFactory)
	    ((AWTComponentFactory)this.getComponentFactory()).findTopLevelFrame(this.getRootContainer());
	// LOAD THE FIRST DOC,  if initial url == null, go to home page
	if (initialURL==null) this.getCurrentTab().getBrowserWindow().navigate(NavigationState.HOME); 
	else this.getCurrentTab().getBrowserWindow().openLocation(this.initialURL,true);
    }
	
    /** subclasses can override this */
    public void setDefaultWindowSize()
    {
	this.getRootContainer().setSize(defaultSizeX, defaultSizeY);
    }

    public void decorateGUI()
    {
        try
        {
        this.getRootContainer().setLayout(new BorderLayout());
        } catch (Throwable t)
        {
            Log.error(t);
        }
        GUITab tab = this.createGUITab(this.initialURL, this.id,true);
        this.tabbedPane = this.getComponentFactory().getXTabbedPane();
        this.tabbedPane.setChangeListener(this);
        // TODO: add tabbed pane listener to change to current Tab
        this.tabbedPane.addTab("Initial",tab.getComponent());
        this.getRootContainer().add(this.tabbedPane.getComponent(),BorderLayout.CENTER);
        
        this.createMenus();
        this.setDefaultWindowSize();
	this.createListeners();
	Thread.yield();
    this.initFocusManager();
    }
    
    protected void initFocusManager()
    {
        focusManager = this.getBrowserWindow().getComponentFactory().getXFocusManager();
        //Log.debug("Focusmanager: "+focusManager+ " ComponentFactory: "+this.getBrowserWindow().getComponentFactory());
        if (focusManager!=null)
            {
            gui_provider = createFocusPointsProvider();
            focusManager.addProvider(gui_provider);
            
            // Content focus
            content_provider = createContentFocusProvider();
            focusManager.addProvider(content_provider);
            
            focusManager.addContentArea(this.getBrowserWindow().getContentArea());
            focusManager.addRootContainer(this.getRootContainer());

            this.getRootContainer().addKeyListener(this);
            this.getRootContainer().requestFocus();     
            }
        
    }

    protected FocusPointsProvider createFocusPointsProvider()
    {
        FocusPointsProvider fp = new AWTFocusPointsProvider(this);
        return fp;
    }
    
    protected FocusPointsProvider createContentFocusProvider()
    {
        FocusPointsProvider fp = new ContentFocusPointsProvider(this);
        return fp;
    }
    
    protected GUITab createGUITab(XLink urli, String id,boolean activate)
    {
	GUITab tab = this.createGUITabInternal();
        if (activate) this.currentTab=tab;
        this.tabs.addElement(tab);
        tab.setInitialURL(urli!=null?urli:this.initialURL);
        tab.setID(id);
        tab.init();
        return tab;
    }

    /* override this if you need to extend GUITab */
    protected GUITab createGUITabInternal()
    {
        GUITab tab = new GUITab(this);
	return tab;
    }
    
    
    protected void addMenuBar()
    {
        if (this.frame!=null)
            menuBar.addToFrame(this.frame);
        
    }
    
    /** menus */
    
    public void createMenus()
    {
        menuBar= this.getBrowserWindow().getComponentFactory().getXMenuBar();
        
        fileMenu=this.getBrowserWindow().getComponentFactory().getXMenu("File");
        fileMenu.add(this.createMenuItem("Open",this.openFileMenu));
        fileMenu.add(this.createMenuItem("Save",this.saveFileMenu));
        fileMenu.add(this.createMenuItem("New Window",this.fileNewWindowMenu));
        fileMenu.addSeparator();
        fileMenu.add(this.createMenuItem("New Tab",this.fileNewTabMenu));
        fileMenu.add(this.createMenuItem("Close Tab",this.fileCloseTabMenu));
        fileMenu.addSeparator();
        fileMenu.add(this.createMenuItem("Exit",this.exitMenu));
        menuBar.addMenu(fileMenu);
        
        editMenu=this.getBrowserWindow().getComponentFactory().getXMenu("Edit");
        editMenu.add(this.createMenuItem("Set as Homepage",this.sethomepageMenu));
        editMenu.add(this.createMenuItem("Configuration",this.configMenu));
        editMenu.add(this.createGUIMenu());
        editMenu.add(this.createMediaMenu());
        menuBar.addMenu(editMenu);
        
        goMenu=this.getBrowserWindow().getComponentFactory().getXMenu("Go");
        goMenu.add(this.createMenuItem("Back",this.backCommand));
        goMenu.add(this.createMenuItem("Forward",this.forwardCommand));
        goMenu.add(this.createMenuItem("Reload",this.reloadCommand));
        goMenu.add(this.createMenuItem("Home",this.homeCommand));
        goMenu.add(this.createMenuItem("Stop",this.stopCommand));
        goMenu.addSeparator();
        goMenu.add(this.createMenuItem("Web search",this.editWebsearchMenu));
        menuBar.addMenu(goMenu);

        viewMenu=this.getBrowserWindow().getComponentFactory().getXMenu("View");
        viewMenu.add(this.createMenuItem("Zoom In  +",this.zoomPlusMenu));
        viewMenu.add(this.createMenuItem("Zoom Out -",this.zoomMinusMenu));
        viewMenu.add(this.createZoomMenu());
        viewMenu.addSeparator();
        viewMenu.add(this.createMenuItem("View XML Source",this.viewXMLSourceMenu));
        viewMenu.add(this.createMenuItem("View XSL Source",this.viewXSLSourceMenu));
        viewMenu.add(this.createMenuItem("View Complete Source: Current DOM",this.viewSourceMenu));
        viewMenu.add(this.createMenuItem("View Tree: Current DOM",this.viewTreeMenu));
        viewMenu.addSeparator();
        viewMenu.add(this.createMenuItem("Speech Dialog",this.viewSpeechMenu));
        viewMenu.add(this.createMenuItem("Show GUI",this.viewShowGUIMenu));
        viewMenu.add(this.createMenuItem("Hide GUI",this.viewHideGUIMenu));
        viewMenu.addSeparator();
        viewMenu.add(this.createMenuItem("Log",this.viewLogMenu));
        menuBar.addMenu(viewMenu);
        
        bookmarksMenu=this.getBrowserWindow().getComponentFactory().getXMenu("Bookmarks");
        bookmarksMenu.add(this.createMenuItem("View Bookmarks",this.viewBookmarksMenu));
        bookmarksMenu.add(this.createMenuItem("Edit Bookmarks",this.editBookmarksMenu));
        menuBar.addMenu(bookmarksMenu);
        
        helpMenu=this.getBrowserWindow().getComponentFactory().getXMenu("Help");
        helpMenu.add(this.createMenuItem("About",this.aboutHelpMenu));
        helpMenu.add(this.createMenuItem("Platform details",this.platformHelpMenu));
        menuBar.addMenu(helpMenu);
        
        this.addMenuBar();
    }
    
    protected XMenu createGUIMenu()
    {
        BrowserWindow browser =this.getBrowserWindow(); 
        XMenu guimenu = browser.getComponentFactory().getXMenu("Change gui");
        for(int i=0;i<browser.getGUIManager().getNumGUIs();i++) {
            XMenuItem it = createMenuItem(
                    (String)browser.getGUIManager().getGUINames().elementAt(i),
                                         this.changeGUIMenu+browser.getGUIManager().getGUINames().elementAt(i)
                                         );
            guimenu.add(it);
        }
        return guimenu;
    }
    
    protected XMenu createStylesheetMenu()
    {
        BrowserWindow b =this.getBrowserWindow(); 
        XMenu smenu = b.getComponentFactory().getXMenu("Change stylesheet");
        if(b.getXMLDocument()!=null) {
            int i;
            Enumeration e=b.getXMLDocument().getStylesheetTitles().elements();
            String title=b.getXMLDocument().getCurrentStylesheetTitle();
	
            for(i=0;e.hasMoreElements();i++) {
                String s=(String)e.nextElement();
                XMenuItem it = createMenuItem(
                       s, this.changeStyleMenu+s );

                if(s.equals(title)) {
                    //it.setEnabled(false);
                    //dissed=it;
                }
                //it.addActionListener(styleListener);
                smenu.add(it);
            }
            /*
            if(i<2)
                setEnabled(false);
            else
                setEnabled(true);
                */
        }
        return smenu;
    }

    protected XMenu createMediaMenu()
    {
        BrowserWindow browser =this.getBrowserWindow(); 
        XMenu guimenu = browser.getComponentFactory().getXMenu("Media");
        for (int i=0;i<MediaQueryEvaluator.MEDIA_TYPES.length;i++) {
            XMenuItem it = createMenuItem(
                    MediaQueryEvaluator.MEDIA_TYPES[i],
                                         this.changeMediaMenu+MediaQueryEvaluator.MEDIA_TYPES[i]
                                         );
            guimenu.add(it);
        }
        return guimenu;
    }
    protected XMenu createZoomMenu()
    {
        BrowserWindow browser =this.getBrowserWindow(); 
        XMenu guimenu = browser.getComponentFactory().getXMenu("Text zoom");
        for (int i=25;i<320;i+=25) {
            
            XMenuItem it = createMenuItem(
                    ""+i+" %",
                                         this.changeZoomMenu+i
                                         );
            guimenu.add(it);
        }
        return guimenu;
    }

    
    protected XMenuItem createMenuItem(String name,String actionCommand)
    {
        final String command = actionCommand;
        XMenuItem item = this.getBrowserWindow().getComponentFactory().getXMenuItem(name);
        item.addActionListener(new ActionListener()
                {
            // Actions from the buttons, back, forward, etc
            public void actionPerformed(ActionEvent e)
            {
                Log.debug("menu actionPerformed: " + e);
                action(command);
            }
        });
        return item;
        
    }
    
    protected Container createContainer()
    {
        return new Panel();
    }




    public void setInitialURL(XLink url)
    {
        this.initialURL = url;
    }
    public void setID(String i)
    {
        this.id = i;
    }
    protected String getComponentFactoryClassName()
    {
    	return "fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory";
    }
    protected String getGUIName()
    {
        return "nullGUIAWT";
    }
    

    public Browser getBrowser()
    {
        //if (browserWindow == null)
            //createBrowserWindow();
        return (Browser)this.getBrowserWindow();
    }


    protected void createListeners()
    {
        Frame f = this.frame;
        if (this.rootContainer instanceof Frame) // for PC havi
            f=(Frame)rootContainer;
        final Frame fr=f;
        if (fr!=null)
        {
	        fr.addWindowListener(new java.awt.event.WindowAdapter()
	        {
	            public void windowClosing(java.awt.event.WindowEvent e)
	            {
	                fr.dispose();
	                System.exit(0);
	            }
	        });
        }
    }
    
    protected Container getWindow()
    {
        return this.frame;
    }
    
    protected void setFullScreen(boolean fs)
    {
	    CompatibilityFactory.getCompatibility().setFullScreen(this.getWindow(),fs); // set full screen if supported
    }
    
    protected double zoomFactor=0.25;

    // Actions from the buttons, back, forward, etc
    public void action(Object action)
    {
        BrowserWindow browser=this.getBrowserWindow();
        Log.debug("actionPerformed: " + action);
	if (this.focusManager!=null) focusManager.cleanFocusPoint(focusManager.getCurrentFocusPoint());
	try
        {
	    if (action.equals(backCommand))
	        {
	            browser.navigate(NavigationState.BACK);
	        } 
	    else if (action.equals(forwardCommand))
	        {
	            browser.navigate(NavigationState.FORWARD);
	        } 
	    else if (action.equals(reloadCommand))
	        {
	            browser.navigate(NavigationState.RELOAD);
	        } 
	    else if (action.equals(homeCommand))
	        {
	            browser.navigate(NavigationState.HOME);
	        } 
        else if (action.equals(stopCommand))
        {
            browser.navigate(NavigationState.STOP);
        } 
	        else if (action.equals(xsmilesCommand))
		    {
	            browser.openLocation("http://www.xsmiles.org/");
	        }
	        else if (action.equals(this.configMenu))
	        {
	            this.newBrowserWindow(new XLink(Resources.getResourceURL("xsmiles.config.editor").toString()),null);
	        }
	        else if (action.equals(this.exitMenu))
	        {
	            System.exit(0);
	        }
	        else if (action.equals(this.openFileMenu))
	        {
	            this.openFileDialog();
	        }
	        else if (action.equals(this.saveFileMenu))
	        {
	            try
	            {
	                this.saveContent();
	            } catch (Exception e)
	            {
	                Log.error(e);
	            }
	        }
	        
	        else if (action.equals(this.viewBookmarksMenu))
	        {
	            this.newBrowserWindow(new XLink(Resources.getResourceURL("xsmiles.bookmarks.view").toString()),null);
	        }
	        else if (action.equals(this.viewShowGUIMenu))
	        {
	            this.getCurrentTab().showGUIButtons();
	        }
	        else if (action.equals(this.viewHideGUIMenu))
	        {
	            this.setFullScreen(true);
	            this.getCurrentTab().hideGUIButtons();
	        }
	        else if (action.equals(this.editBookmarksMenu))
	        {
	            this.newBrowserWindow(new XLink(Resources.getResourceURL("xsmiles.bookmarks.document").toString()),null);
	        }
	        else if (action.equals(this.zoomPlusMenu))
	        {
	            browser.setZoom(browser.getZoom()*(1+zoomFactor));
	        }
	        else if (action.equals(this.zoomMinusMenu))
	        {
	            browser.setZoom(browser.getZoom()*(1-zoomFactor));
	        }
	        else if (action.equals(this.editWebsearchMenu))
	        {
	            this.newBrowserWindow(new XLink(this.searchURL),null);
	        }
	        else if (action.equals(this.fileNewWindowMenu))
	        {
	            this.newBrowserWindow(null,null);
	        }
	        else if (action.equals(this.fileNewTabMenu))
	        {
	            this.createNewTab(null,null,false);
	        }
	        else if (action.equals(this.fileCloseTabMenu))
	        {
	            this.closeCurrentTab();
	        }
	        else if (action.equals(this.aboutHelpMenu))
	        {
	            browser.newBrowserWindowWithGUI(
	                    Resources.getResourceURL("xsmiles.about").toString(),
	                    "about",
	                    true);
	        }
	        else if (action.equals(this.platformHelpMenu))
	        {
	        	PlatformDetails details= new PlatformDetails(this.getBrowserWindow());
	        	XLink xlink = new XLinkWithContent("http://www.xsmiles.org/",details.getPlatformDetailsString());
	            browser.newBrowserWindow(xlink);
	        }
	        else if (action.equals(this.sethomepageMenu))
	        {
	            
	            String urli=browser.getDocumentHistory().getLastDocument().getURLString();
	            if(urli!=null || ! urli.equals(""))
	            {
	                browser.getBrowserConfigurer().setProperty("main/homepage",urli);
	                browser.getBrowserConfigurer().writeConfig();
	            }
	        }
	        else if (action.equals(this.viewSpeechMenu))
	        {
	            try
	            {
		            dialog = (XSmilesDialog)Class.forName(this.dialogClass).newInstance();
		            dialog.setBrowserWindow(browser);
		            dialog.start();
	            } catch (Throwable t)
	            {
	                Log.error(t);
	            }
	        }
	        else if (action.equals(this.viewXMLSourceMenu))
	        {
	            /** The modes are from XSmilesView class */
	            this.showSource(
	                    XsmilesView.SOURCEMLFC,
	                    " : Source");
	        }
	        else if (action.equals(this.viewXSLSourceMenu))
	        {
	            /** The modes are from XSmilesView class */
	            this.showSource(
	                    XsmilesView.XSL_MLFC,
	                    " : XSL Source");
	        }
	        else if (action.equals(this.viewSourceMenu))
	        {
	            /** The modes are from XSmilesView class */
	            this.showSource(
	                    XsmilesView.SOURCE_AND_XSL_MLFC,
	                    " : Complete source - current DOM");
	        }
	        else if (action.equals(this.viewTreeMenu))
	        {
	            /** The modes are from XSmilesView class */
	            //public static void showSourceStatic(XMLDocument doc, int mode, String heading)
	            this.showSource(
	                    XsmilesView.TREEMLFC,
	                    " : Tree - current DOM");
	        }
	        else if (action.equals(this.viewLogMenu))
	        {
	            browser.openLog();
	        }
	        else if (((String)action).startsWith(changeGUIMenu))
	        {
	            String com=((String)action).substring(changeGUIMenu.length());
	            browser.newBrowserWindowWithGUI(
	                    this.getCurrentTab().getBrowserWindow().getCurrentPage().getURLString(),
	                    com,
	                    true);
                /*
                Vector guiNames=browser.getGUIManager().getGUINames();
                if(com!=null && !com.equals(""))
                    browser.getGUIManager().showGUI(com,true,false);
                    */
	        }
	        else if (((String)action).startsWith(changeZoomMenu))
	        {
	            String com=((String)action).substring(changeZoomMenu.length());
	            float f = Float.parseFloat(com);
	            browser.setZoom(f/100.0f);
	        }
	        else if (((String)action).startsWith(changeMediaMenu))
	        {
	            String com=((String)action).substring(changeMediaMenu.length());
	            XMLConfigurer config = browser.getBrowserConfigurer();
	            config.setGUIProperty(browser, "mediaType", com);
	            browser.navigate(NavigationState.RELOAD);
	        }
            else if (((String)action).startsWith(this.changeStyleMenu))
            {
                String com=((String)action).substring(changeStyleMenu.length());
                Log.debug("Reloading the doc with the style: "+com);
                // change the stylesheet and reload
                browser.setPreferredStylesheetTitle(com);   
                // Tell browser window to reload
                browser.navigate(NavigationState.RELOAD);
            }
        } catch (MalformedURLException e)
        {
            Log.error(e);
        }
    }
    
    protected void showSource(int type, String addtitle)
    {
        this.getBrowserWindow().getCurrentGUI().showSource(
                this.getBrowserWindow().getXMLDocument(), 
                type,
                this.getBrowserWindow().getCurrentPage().getURLString()+addtitle);
    }
    
    public void newBrowserWindow(XLink urli, String id)
    {
        // TODO: support for url and id
        try
        {
            XLink url;
            if (urli!=null) url =urli;
            else url=initialURL;
            AWTGUI gui=(AWTGUI)this.getClass().newInstance();
            gui.setInitialURL(url);
            gui.setID(id);
            gui.show();
        } catch (Throwable e)
        {
            Log.error(e);
        }
    }
    
    public void openInNewTab(XLink l, String id)
    {
        this.createNewTab(l.getURL(),id,false);
    };

    public void openInNewWindow(XLink l, String id) {
        this.newBrowserWindow(l,id);
    }

    

    /**
     * @param object
     * @param object2
     */
    protected synchronized void createNewTab(URL url, String id,boolean activate)
    {
        // TODO: support for url and id
        if (this.tabbedPane.getMaxTabCount()>this.tabbedPane.getTabCount())
        {
            GUITab tab = this.createGUITab(url!=null?new XLink(url):null,id,activate);
            this.tabbedPane.addTab("new",tab.getComponent());
            if (url!=null) tab.getBrowserWindow().openLocation(url);
        }
    }

    XFileDialog fc;

    private void openFileDialog()
    {
        URL requiredDoc=null;
        if (fc==null)
        {
            fc=this.getBrowserWindow().getComponentFactory().getXFileDialog(false);
            fc.setFile(new File ("../demo"));
        }
        int result=fc.select();
        
        if (result==fc.SELECTION_FAILED) return;
        File file = fc.getSelectedFile();
        try
        {
            requiredDoc =  URLUtil.fileToURL(file);
        } catch(Exception e)
        {
            // Log.error(e);
        }
        if(requiredDoc!=null)
            this.getBrowserWindow().openLocation(requiredDoc, true);
    }
    
    protected void updateStylesheetMenu()
    {
        if (this.editMenu==null) return;
        XMenu smenu = this.createStylesheetMenu();
        if (smenu!=null)
        {
            if (this.stylesheetMenu!=null)
            {
                // remove the old stylesheet menu
                this.editMenu.remove(stylesheetMenu);
            }
            this.stylesheetMenu=smenu;
            this.editMenu.add(smenu);
            Log.debug("Stylesheet menu updated");
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XChangeListener#valueChanged(boolean, java.lang.Object)
     * This method is called when a tab is changed
     */
    public void valueChanged(boolean valueChanging, Object source)
    {
        GUITab tab = this.getGUITabForComponent(this.tabbedPane.getSelectedComponent());
        if (tab!=null) this.currentTab=tab;
        this.updateStylesheetMenu();
        //Log.debug("Current tab is tab:"+this.tabs.indexOf(this.currentTab));
    }
    protected GUITab getGUITabForComponent(Component c)
    {
        for (Enumeration e=this.tabs.elements();e.hasMoreElements();)
        {
            GUITab t = (GUITab)e.nextElement();
            if (t.getComponent()==c) return t;
        }
        return null;
    }


    /**
     * @param title2
     */
    public void setTitle(String title2)
    {
        if (this.frame!=null) this.frame.setTitle(title2);
    }


    /**
     * @return
     */
    public boolean isTabbed()
    {
        // TODO Auto-generated method stub
        return false;
    }
    

    public void keyReleased (KeyEvent evt) {}

    public void keyTyped (KeyEvent evt) {}

    public void keyPressed (KeyEvent evt) {	
	if (this.focusManager!=null)
    {
    	if (evt.getKeyCode()==KeyEvent.VK_LEFT) {
    	    focusManager.changeFocus(XFocusManager.LEFT_FOCUS_POINT);
    	}else if (evt.getKeyCode()==KeyEvent.VK_RIGHT) {
    	    focusManager.changeFocus(XFocusManager.RIGHT_FOCUS_POINT);
    	}else if (evt.getKeyCode()==KeyEvent.VK_UP) {
    	    focusManager.changeFocus(XFocusManager.UP_FOCUS_POINT);
    	}else if (evt.getKeyCode()==KeyEvent.VK_DOWN) {
    	    focusManager.changeFocus(XFocusManager.DOWN_FOCUS_POINT);
    	}else if (evt.getKeyCode()==KeyEvent.VK_ENTER) {
    	    if (focusManager.getCurrentFocusPoint().getVisualElement()!=null){
    		VisualElementImpl elem = focusManager.getCurrentFocusPoint().getVisualElement();
    		//focusManager.cleanFocusPoint(focusManager.getCurrentFocusPoint());
    		Vector elemViews = elem.getViews();		
    		if (elemViews.get(0) != null){
    		    View view = (View)elemViews.get(0);
    		    if (view.getCSSRenderer() != null){
    			CSSRenderer renderer = view.getCSSRenderer();
    			renderer.mouseClicked(new MouseEvent(rootContainer,MouseEvent.MOUSE_CLICKED,0,0,focusManager.getCurrentFocusRectangle().x,focusManager.getCurrentFocusRectangle().y,1,false));
    		    }
    		}
    		
    	    }
    	}else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE){
    	    System.exit(0);
    	}	
    }
    }


    /**
     * 
     */
    public void browserReady()
    {
        if (this.dialog!=null) this.dialog.setBrowserWindow(this.getBrowserWindow());
        this.updateStylesheetMenu();
    }    
}
