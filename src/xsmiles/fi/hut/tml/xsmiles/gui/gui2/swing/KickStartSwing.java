/*
 * Created on Mar 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.gui2.swing;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.Xsmiles;
import fi.hut.tml.xsmiles.gui.gui2.KickStart;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;
import fi.hut.tml.xsmiles.gui.swing.LookAndFeelManager;

/**
 * An easy way to kickstart the browser with a GUI wrapper
 * This does not use the old GUI framework in X-Smiles, but
 * rather just creates a frame and creates a BrowserWindow without an 
 * old - type of GUI and puts that into the frame.
 * 
 * This class shows that it is easy to create a project that launches X-Smiles inside it.
 * @author honkkis
 *
 */
public class KickStartSwing extends KickStart
{
    public static KickStartSwing instance;
    
    public AWTGUI gui;
    
    public static void main(String args[])
    {
        instance = new KickStartSwing();
        instance.initSequence(args);
    }
    

    
    /**
     * This method launches our own Frame instead of the normal Browser.
     */
    /*
    public Browser launchBrowser(String initialURL) throws Throwable
    {
        gui=(AWTGUI)Class.forName(getGUIClass()).newInstance();
        gui.setInitialURL(initialURL);
        Browser b=gui.getBrowser();
        gui.show();
        return b;
    }*/
    
    protected void setXMLConfigurer(XMLConfigurer conf)
    {
        LookAndFeelManager.setStyle(conf);
        super.setXMLConfigurer(conf);
    }

    
    public String getGUIClass()
    {
        return "fi.hut.tml.xsmiles.gui.gui2.swing.SwingGUI";
    }

}
