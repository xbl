/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Oct 19, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.gui2.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.event.GUIEvent;
import fi.hut.tml.xsmiles.event.GUIEventListener;
import fi.hut.tml.xsmiles.gui.XSmilesDialog;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.awt.Animation;
import fi.hut.tml.xsmiles.gui.components.XFocusManager;

/**
 * The GUITab contains one tab.
 * Each tab has 
 * - the action container (back, forward buttons, the url area)
 * - one browser window in the center
 * - the status area in the bottom
 * 
 * The GUITab can be extended by subclasses to suit different platforms
 * GUITab is created with the method AWTGUI.createTab() 
 * 
 * @author honkkis
 *
 */
public class GUITab  implements GUIEventListener
{
    public XLink initialURL;
    String id; 
    protected XSmilesDialog dialog ;
    protected AWTGUI gui;

    public Browser browserWindow;

    /**
     * The structure of the containers :
     * 
     * ------------------------------------------------- 
	*          Menu 
     * ------------------------------------------------- 
	* actionContainer
     * (BorderLayout.NORTH) 
	*------------------------------------------------
     * 
     * BrowserWindow.contentArea() (BorderLayout.CENTER)
     * 
     * ------------------------------------------------ 
	* statusContainer
     * (BorderLayout.SOUTH)
     *  
     */

    public Container actionContainer;
    public Container statusContainer;
    public Container rootContainer;

    public XCaption status;


    //The actual iconsUrl and animation size should be variables, so different devices can show different GUIs more suitable for their characteritics
    public String backUrl="img/met/back.gif";
    public String forwardUrl="img/met/forward.gif";
    public String homeUrl="img/met/home.gif";
    public String reloadUrl="img/met/reload.gif";
    public String stopUrl="img/met/stop.gif";
    public int animationWidth=30;
    public int animationHeight=30;

    public void changeGUIElements(String backUrl, String forwardUrl, String homeUrl, String reloadUrl, int animationWidth, int animationHeight){
	if (backUrl!=null)
	    this.backUrl = backUrl;
	if (forwardUrl!=null)
	    this.forwardUrl = forwardUrl;
	if (homeUrl!=null)
	    this.homeUrl = homeUrl;
	if (reloadUrl!=null)
	    this.reloadUrl = reloadUrl;
	if (animationWidth!=0)
	    this.animationWidth=animationWidth;
	if (animationHeight!=0)
	    this.animationHeight=animationHeight;
    // TODO: stop button
    }
	    
    public GUITab(AWTGUI agui)
    {
        this.gui=agui;
    }
    
    protected void createRootContainer()
    {
        this.rootContainer=this.createContainer();
    }


    public void setInitialURL(XLink url)
    {
        this.initialURL = url;
    }

    public void setID(String i)
    {
        this.id = i;
    }

    public Component getComponent()
    {
        if (this.rootContainer==null) this.init();
        return this.rootContainer;
    }
    
    public BrowserWindow getBrowserWindow()
    {
        return this.browserWindow;
    }
    
    public void init()
    {
        this.createRootContainer();
        this.createBrowserWindow();
        this.decorateGUI();
        Log.debug(
                "Added the content area to the frame. Content area size: " + browserWindow.getContentArea().getSize());
    }
    
    public void destroy()
    {
	  browserWindow.closeBrowserWindow();
    }
    
    protected void createBrowserWindow()
    {
        //browserWindow = new Browser(initialURL, id, true,this.gui.getGUIName());
        browserWindow = new Browser((XLink)null, id, true,this.gui.getGUIName(),false);
        //XLink uri, String id, boolean showGUI, String name, boolean loadInitialDoc
        if (this.initialURL!=null) this.setLocation(initialURL.getURLString());
        this.createBrowserListeners();
        Container contentArea = browserWindow.getContentArea();
    }
    
    public void createBrowserListeners()
    {
        this.getBrowserWindow().getCurrentGUI().addGUIEventListener(this);
    }
    
    public void decorateGUI()
    {
        this.getRootContainer().setLayout(new BorderLayout());
        Container contentArea = this.getContentArea();
        //this.createListeners();
        this.getRootContainer().add(contentArea, BorderLayout.CENTER);
        this.createActionContainer();
        this.createStatusContainer();
        this.getRootContainer().add(statusContainer, BorderLayout.SOUTH);

        this.getRootContainer().add(this.topContainer, BorderLayout.NORTH);
    }
    
    protected XInput locationField;
    protected Container topContainer;
    protected XButton backButton, forwardButton, homeButton, reloadButton,stopButton;
    public void createActionContainer()
    {
        this.topContainer=this.createContainer();
        this.topContainer.setLayout(new BorderLayout(0,0)); // hgap,vgap
        this.actionContainer=this.createContainer();
        this.actionContainer.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));//align, hgap,vgap
        Font acFont = this.getActionFont();
        if (acFont!=null)
        {
            this.topContainer.setFont(acFont);
            this.actionContainer.setFont(acFont);
        }

	this.backButton=createActionButton(gui.backCommand, backUrl, gui.backCommand);
	this.forwardButton=createActionButton(gui.forwardCommand, forwardUrl, gui.forwardCommand);
	this.homeButton=createActionButton( gui.homeCommand, homeUrl, gui.homeCommand);
    this.stopButton=createActionButton("", stopUrl, gui.stopCommand);
	this.reloadButton=createActionButton(gui.reloadCommand, reloadUrl, gui.reloadCommand);
	

	/**
	   this.backButton=createActionButton(gui.backCommand, "img/met/back.gif", gui.backCommand);
	   this.forwardButton=createActionButton(gui.forwardCommand, "img/met/forward.gif", gui.forwardCommand);
	   this.homeButton=createActionButton( gui.homeCommand, "img/met/home.gif", gui.homeCommand);
	   this.reloadButton=createActionButton(gui.reloadCommand, "img/met/reload.gif", gui.reloadCommand);
	**/

	this.actionContainer.add(backButton.getComponent());
	this.actionContainer.add(forwardButton.getComponent());
    this.actionContainer.add(stopButton.getComponent());
	this.actionContainer.add(homeButton.getComponent());
	this.actionContainer.add(reloadButton.getComponent());
        //locationField.setSize(400,20);
        this.topContainer.add(actionContainer,BorderLayout.WEST);
	
	//this.topContainer.add(createActionButton("X", null, gui.xsmilesCommand).getComponent(),BorderLayout.EAST);
	
	XPanel cont = this.getBrowserWindow().getComponentFactory().getXPanel();
	((Container)cont.getComponent()).setLayout(new FlowLayout(FlowLayout.LEFT,0,0));//align, hgap,vgap
	if (this.gui.isTabbed())
	    {
		cont.add(createActionButton(null, "img/met/nutab.gif", gui.fileNewTabMenu));
		cont.add(createActionButton(null, "img/met/closetab.gif", gui.fileCloseTabMenu));
	    }
	
	this.topContainer.add(createLocation(),BorderLayout.CENTER);
	
	if (!Utilities.isEmbedded())
	    {
		//this.animation = new Animation(30,30,500);
		this.animation = new Animation(animationWidth,animationHeight,500);
                this.animation.addMouseListener(new MouseAdapter() 
		    {
			public void    mouseClicked(MouseEvent e) 
			{
			    Log.debug("Mouse clicked in animation");
			    getBrowserWindow().openLocation("http://www.xsmiles.org/");
			}
		    });
		cont.addComp(animation);
	    }
	this.topContainer.add(cont.getComponent(),BorderLayout.EAST);      
    }
    protected Animation animation;
    
    protected Container createContainer()
    {
        return gui.createContainer();
    }
    
    protected Font getActionFont()
    {
        return new Font("SansSerif",Font.PLAIN,8);
    }
    protected Font getStatusFont()
    {
        return this.getActionFont();
    }

    public void createStatusContainer()
    {
        this.status = this.getBrowserWindow().getComponentFactory().getXCaption("Browser ready.");
        this.statusContainer = this.createContainer();
        this.statusContainer.setLayout(new BorderLayout());
        this.statusContainer.add(status.getComponent(), BorderLayout.CENTER);
        this.addMLFCControls();
        Font acFont = this.getActionFont();
        if (acFont!=null) this.statusContainer.setFont(acFont);
        
    }
    
    protected void addMLFCControls()
    {
        this.statusContainer.add(this.getBrowserWindow().getGUIManager().getCurrentGUI().getMLFCControls().getMLFCToolBar().getComponent(), BorderLayout.NORTH);
    }
    
    protected void removeMLFCControls()
    {
        this.statusContainer.remove(this.getBrowserWindow().getGUIManager().getCurrentGUI().getMLFCControls().getMLFCToolBar().getComponent());
    }
    
    protected Component createLocation()
    {
        locationField = this.getBrowserWindow().getComponentFactory().getXInput();
        locationField.addActionListener(new LocationFieldListener());
        return locationField.getComponent();
    }
    protected class LocationFieldListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String s=locationField.getText();
            if (s.indexOf(':') < 0 && s.startsWith(".") == false)
            {
                s = "http://"+s;
            }
            loadLocation(s);
        }
    }
    
    public void loadLocation(String location)
    {
        this.getBrowserWindow().openLocation(location);
    }

    protected XButton createActionButton(String name, String icon, String actionCommand)
    {
        final String command = actionCommand;
        // AWT component factory does not yet support image buttons
        // For swing, we only want the icon to be shown...
        XButton button = this.getBrowserWindow().getComponentFactory().getXButton(name, icon);

        button.addActionListener(new ActionListener()
        {
            // Actions from the buttons, back, forward, etc
            public void actionPerformed(ActionEvent e)
            {
                Log.debug("actionPerformed: " + e);
                gui.action(command);
            }
        });
        return button;
    }
    
    public Container getContentArea()
    {
        return this.getBrowserWindow().getContentArea();
    }
    
    protected Container getRootContainer()
    {
        return this.rootContainer;
    }

    
    /** the GUI event listener interface */
    public void start()
    {
    };
    public void openInNewTab(XLink l, String id)
    {
        gui.openInNewTab(l,id);
    };
    
    public void openInNewWindow(XLink l, String id) {
        gui.openInNewWindow(l,id);
    }

    public void setStatusText(String statusText)
    {
		if (status!=null)this.status.setText(statusText);
    };
    public void setEnabledBack(boolean value)
    {
        if (backButton!=null) backButton.setEnabled(value);
    };
    public void setEnabledForward(boolean value)
    {
        if (forwardButton!=null) forwardButton.setEnabled(value);
    };
    public void setEnabledHome(boolean value)
    {
        if (homeButton!=null)homeButton.setEnabled(value);
    };
    public void setEnabledStop(boolean value)
    {
        if (stopButton!=null)stopButton.setEnabled(value);
    };
    public void setEnabledReload(boolean value)
    {
        if (reloadButton!=null)reloadButton.setEnabled(value);
    };
    public void setTitle(String title) {
        try
        {
	        gui.getTabbedPane().setTitleAt(gui.getTabbedPane().indexOfComponent(this.getComponent()), title);
	        this.gui.setTitle(title);
        } catch (NullPointerException e)
        {
        }
    }
    public void setLocation(String s)
    {
        if (locationField!=null)
            this.locationField.setText(s);
    };
    public void browserWorking()
    {
        if (this.animation!=null) this.animation.play();
    };
    public void GUIEvent(GUIEvent ev)
    {
    };

    public void browserReady()
    {
        Log.debug("Browser finished loading document.");
        if (this.animation!=null) this.animation.pause();
        if (this.getBrowserWindow().getCurrentPage() !=null)
	    {
	        this.setLocation(this.getBrowserWindow().getCurrentPage().getURL().toString());
	        String title = this.browserWindow.getContentManager().getPrimaryContentHandler().getTitle();
	        title = createTabTitleString(title!=null?title:this.browserWindow.getCurrentPage().getURLString());
	        this.setTitle(title);
            this.gui.browserReady();
        }
	
	/** Set the first element as focused **/
	if (gui.focusManager!=null) gui.focusManager.setFirstFocusPoint();
    };
    
    /**
     * 
     */
    protected void hideGUIButtons()
    {
        try
        {
            Log.debug("Removing button container");
            //this.actionContainer.setVisible(false);
            this.getRootContainer().remove(this.topContainer);
            this.removeMLFCControls();
            this.getRootContainer().validate();
        } catch (Exception e)
        {
            Log.error(e);
        }
        
    }

    /**
     * 
     */
    protected void showGUIButtons()
    {
        try
        {
            Log.debug("Re-adding button container");
            this.getRootContainer().add(this.topContainer, BorderLayout.NORTH);
            this.addMLFCControls();
            this.getRootContainer().validate();
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    private String createTabTitleString(String title) {
        String fin = "";
        if (title.length() > 25) {
            fin = title.substring(0, 11) + "..." + title.substring(title.length() - 11, title.length());
        } else {
            fin = title;
        }
        return fin;
    }
}
