/*
 * Created on Mar 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.gui2;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.Xsmiles;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;

/**
 * An easy way to kickstart the browser with a GUI wrapper
 * This does not use the old GUI framework in X-Smiles, but
 * rather just creates a frame and creates a BrowserWindow without an 
 * old - type of GUI and puts that into the frame.
 * 
 * This class shows that it is easy to create a project that launches X-Smiles inside it.
 * @author honkkis
 *
 */
public class KickStart extends Xsmiles
{
    public static KickStart instance;
    
    public GUIInterface  gui;
    
    public static void main(String args[])
    {
        instance = new KickStart();
        instance.initSequence(args);
    }
    
    /**
     * This method launches our own Frame instead of the normal Browser.
     */
    public Browser launchBrowser(String initialURL) throws Throwable
    {
        gui=(GUIInterface)Class.forName(getGUIClass()).newInstance();
        XMLConfigurer conf = this.getXMLConfigurer();
        this.setXMLConfigurer(conf);
        if (initialURL!=null)gui.setInitialURL(new XLink(initialURL));
        gui.show();
        Browser b=gui.getBrowser();
        return b;
    }
    
    protected void setXMLConfigurer(XMLConfigurer conf)
    {
        Browser.setXMLConfigurerStatic(conf);
        gui.setXMLConfigurer(conf);
    }
    
    
    public String getGUIClass()
    {
        return "fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI";
    }

}
