/*
 */
package fi.hut.tml.xsmiles.gui.gui2.swing;

import java.applet.Applet;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.swing.XAFrame;
import fi.hut.tml.xsmiles.gui.gui2.awt.AWTGUI;
import fi.hut.tml.xsmiles.gui.swing.LookAndFeelManager;

/**
 * @author honkkis
 */
public class SwingGUI extends AWTGUI
{
	JFrame jframe;
    protected String getComponentFactoryClassName()
    {
        return "fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory";
    }
    protected String getGUIName()
    {
        return "nullGUI";
    }
    protected void initFocusManager()
    {
    }
    
    protected Container createContainer()
    {
        return new JPanel();
    }
    
    public void decorateGUI()
    {
        super.decorateGUI();
    }    
    protected Container getWindow()
    {
        return this.jframe;
    }
    protected Container createRootContainer()
    {
        //this.jframe= new JFrame(title);
        this.jframe = new XAFrame(title); // provides the icon
        this.jframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        listenForWindowClose(this.jframe);

        return jframe.getContentPane();
    }
    
    protected void disposeWindow()
    {
        this.jframe.dispose();
    }
    
    protected void addMenuBar()
    {
        if (this.jframe!=null)
            menuBar.addToFrame(this.jframe);
        this.jframe.setSize(this.defaultSizeX,this.defaultSizeY);
        this.getRootContainer().invalidate();
        this.jframe.validate();
        this.jframe.show();
    }
    
    protected void createListeners()
    {
    	super.createListeners();
    	this.registerKeyboardAction(
    			KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, InputEvent.CTRL_MASK)
    			,zoomPlusMenu);
    	this.registerKeyboardAction(
    			KeyStroke.getKeyStroke(KeyEvent.VK_ADD,InputEvent.CTRL_MASK)
    			,zoomPlusMenu);
    	this.registerKeyboardAction(
    			KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_MASK)
    			,zoomMinusMenu);
    	this.registerKeyboardAction(
    			KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT,InputEvent.CTRL_MASK)
    			,zoomMinusMenu);
    	this.registerKeyboardAction(
    			KeyStroke.getKeyStroke(KeyEvent.VK_T,InputEvent.CTRL_MASK)
    			,fileNewTabMenu);
    }
    
    protected JComponent getFirstJComponent(Container c)
    {
        if (c instanceof JComponent) return (JComponent)c;
        else
        {
            for (int i =0;i<c.getComponentCount();i++)
            {
                if (c.getComponent(i) instanceof JComponent) return (JComponent)c.getComponent(i);
            }
            for (int i =0;i<c.getComponentCount();i++)
            {
                if (c.getComponent(i) instanceof Container) 
                {
                    JComponent jc = getFirstJComponent((Container)c.getComponent(i));
                    if (jc!=null) return jc;
                }
            }
        }
           
        return null;
    }

    protected void registerKeyboardAction(KeyStroke stroke, String cmd)
    {
    	JComponent root=null;
    	if (this.jframe!=null) root=(JComponent) jframe.getContentPane();
    	else if (this.getRootContainer() instanceof Applet) root=this.getFirstJComponent(this.getBrowserWindow().getContentArea());
    	if (root!=null)
    	{
	    	final String command = cmd;
	    	root.registerKeyboardAction(
	    		new ActionListener()
	            {
			        // Actions from the buttons, back, forward, etc
			        public void actionPerformed(ActionEvent e)
			        {
			            Log.debug("menu actionPerformed: " + e);
			            action(command);
			        }
			    }
	    		,cmd,
	            stroke,JComponent.WHEN_IN_FOCUSED_WINDOW);
    	}
    }
    
    /**
     * @param title2
     */
    public void setTitle(String title2)
    {
        if (this.jframe!=null) this.jframe.setTitle(title2);
    }
    /**
     * @return
     */
    public boolean isTabbed()
    {
        // TODO Auto-generated method stub
        return true;
    }


}
