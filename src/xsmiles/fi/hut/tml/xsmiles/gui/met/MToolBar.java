package fi.hut.tml.xsmiles.gui.met;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.swing.XAAddressCombo;

public class MToolBar extends JPanel {

    private XAAddressCombo cb;
    private JButton back, forward, reload, stop, home, closetab, nutab;
    private MAnimate an;

    public MToolBar() {
        init();
    }

    private void init() {
        an = new MAnimate(30, 30);

        setLayout(new BorderLayout());
        JToolBar butPane = new JToolBar();

        back = new JButton(loadImage("img/met/back.gif"));
        back.setActionCommand("back");

        forward = new JButton(loadImage("img/met/forward.gif"));
        forward.setActionCommand("forward");

        reload = new JButton(loadImage("img/met/reload.gif"));
        reload.setActionCommand("reload");

        stop = new JButton(loadImage("img/met/stop.gif"));
        stop.setActionCommand("stop");

        home = new JButton(loadImage("img/met/home.gif"));
        home.setActionCommand("home");

        nutab = new JButton(loadImage("img/met/nutab.gif"));
        nutab.setActionCommand("nutab");

        closetab = new JButton(loadImage("img/met/closetab.gif"));
        closetab.setActionCommand("closetab");

        butPane.add(back);
        butPane.add(forward);
        butPane.add(stop);
        butPane.add(reload);
        butPane.add(home);

        JToolBar rightToolbar = new JToolBar();
        rightToolbar.add(nutab);
        rightToolbar.add(closetab);

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout());
        rightPanel.add(rightToolbar, BorderLayout.WEST);
        rightPanel.add(an, BorderLayout.EAST);

        JToolBar comboBar = new JToolBar();
        cb = new XAAddressCombo();
        cb.setEditable(true);
        //cb.setActionCommand("COMBO!");
        comboBar.add(cb);

        add(butPane, BorderLayout.WEST);
        add(comboBar, BorderLayout.CENTER);
        add(rightPanel, BorderLayout.EAST);
    }

    public XAAddressCombo getCombo() {
        return cb;
    }

    public void play() {
        an.play();
    }

    public void pause() {
        an.pause();
    }

    public void setEnabledBack(boolean value) {
        back.setEnabled(value);
    }

    public void setEnabledForward(boolean value) {
        forward.setEnabled(value);
    }

    public void setEnabledHome(boolean value) {
        home.setEnabled(value);
    }

    public void setEnabledStop(boolean value) {
        stop.setEnabled(value);
    }
    
    public void setEnabledReload(boolean value) {
        reload.setEnabled(value);
    }

    /**
     * Add an actionListener to all buttions in the toolbar
     */
    public void addButtonActionListener(ActionListener al) {
        back.addActionListener(al);
        forward.addActionListener(al);
        stop.addActionListener(al);
        reload.addActionListener(al);
        home.addActionListener(al);
        an.addActionListener(al);
        nutab.addActionListener(al);
        closetab.addActionListener(al);
        cb.addActionListener(al);
    }

    private static int no = 0;

    public ImageIcon loadImage(String s) {
        URL resURL = this.getClass().getResource(s);
        //Log.debug("*** resourceURL: "+resURL);
        Toolkit tk = Toolkit.getDefaultToolkit();
        MediaTracker mt = new MediaTracker(this);
        Image image;
        // 1st try to find the image in the resources, 2nd from a file
        if (resURL != null)
            image = tk.getImage(resURL);
        else
            image = tk.getImage(s);
        mt.addImage(image, ++no);
        try {
            mt.waitForID(no);
        } catch (InterruptedException e) {
            Log.error(e);
        }
        return new ImageIcon(image);
    }

}
