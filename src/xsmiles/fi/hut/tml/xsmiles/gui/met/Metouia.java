package fi.hut.tml.xsmiles.gui.met;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.components.swing.XAFrame;
import fi.hut.tml.xsmiles.gui.swing.LookAndFeelManager;

/**
 * A GUI using the BrowserWindow as a toolkit, instead of integrating directly
 * to it 100% Swing, currently using the metouia style
 * 
 * @author juha
 */
public class Metouia extends GUIImpl
{
    private XAFrame frame;
    private JPanel contentPane;
    private JTabbedPane tabs;
    private Vector browserTabs; // a vector containing all
                                                // browsertab objects
    protected MMenuBar menuBar;

    // private BrowserWindow selectedBrowser; // The BrowserWindow, which is
    // currently selected

    private BrowserWindow initialBrowser; // The
                                                              // browserWindow
                                                              // instance with
                                                              // a reference to
                                                              // the
    // GUI, so that it can change GUIs (problem: the
    // initialbrowser is the only one with a correct
    // reference to the GUI, how can this be handled?)
    // private DefaultComponentFactory cf;

    private fi.hut.tml.xsmiles.content.ContentHandlerFactory contentHandlerFactory;

    /**
     * Constructor
     */
    public Metouia(BrowserWindow b)
    {
        super(b);
        initialBrowser = b;
        // super(b);
        setStyle();
        createComponents();
    }

    /**
     * Constructor (ignores c)
     */
    public Metouia(BrowserWindow b, Container c)
    {
        super(b);
        initialBrowser = b;
        setStyle();
        createComponents();
        createListeners();
    }

    private void setStyle()
    {
        LookAndFeelManager.setStyle(initialBrowser.getBrowserConfigurer());
    }

    private void createComponents()
    {
        frame = new XAFrame("Metouia GUI");
        contentPane = new JPanel();
        frame.setContentPane(contentPane);
        menuBar = new MMenuBar(this);
        frame.setJMenuBar(menuBar);
        contentPane.setLayout(new BorderLayout());

        tabs = new JTabbedPane();
        tabs.addChangeListener(new TBListener());

        contentPane.add(tabs, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.show();
    }

    public void start()
    {
        newTab("home", initialBrowser);
    }

    public static void main(String args[])
    {
        Log.info("Java version: " + System.getProperty("java.version"));
        BrowserWindow b = null;
        try
        {
            b = new Browser(Browser.getXResource("xsmiles.empty").toString(), "default", true, "tabguitab");
        } catch (Exception e)
        {
        }

        Metouia m = new Metouia(b);
    }

    public void newTab()
    {
        tabs.add("empty", new BrowserTab(this));
    }

    public void openInNewTab(XLink link, String id)
    {
        tabs.add("Nu Tab", new BrowserTab(this, link, id));
    }

    public void newTab(String title)
    {
        tabs.add(title, new BrowserTab(this));
    }

    public void newTab(String title, BrowserWindow bw)
    {
        tabs.add(title, new BrowserTab(bw, this));
    }

    /**
     * only remove a tab if it is not the last
     */
    public void delTab(BrowserTab c)
    {
        if (tabs.getTabCount() > 1)
            tabs.remove(c);
    }

    private void delCurrentTab()
    {
        BrowserTab tab = (BrowserTab) getTabs().getComponentAt(getTabs().getSelectedIndex());
        if (getInitialBrowser() != tab.getBrowserWindow())
        {
            tab.getBrowserWindow().closeBrowserWindow();
        }
        if (getTabs().getTabCount() == 1)
        {
            getInitialBrowser().closeBrowserWindow();
        }
        delTab(tab);
    }

    public void destroy()
    {
        frame.dispose();
    }
    /**
     * Create Listeners
     *  
     */
    private void createListeners()
    {
        frame.addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent e)
            {
                for (int i = 0; i < getTabs().getTabCount(); i++)
                {
                    ((BrowserTab) getTabs().getComponentAt(i)).getBrowserWindow().closeBrowserWindow();
                }
                browser.closeBrowserWindow();
            }
        });
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
            new TabListener(),
            "zoomplus",
            KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, InputEvent.CTRL_MASK),
            JComponent.WHEN_IN_FOCUSED_WINDOW);
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
            new TabListener(),
            "zoomminus",
            KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_MASK),
            JComponent.WHEN_IN_FOCUSED_WINDOW);
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
            new TabListener(),
            "zoomplus",
            KeyStroke.getKeyStroke(KeyEvent.VK_ADD,InputEvent.CTRL_MASK),
            JComponent.WHEN_IN_FOCUSED_WINDOW);
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
                new TabListener(),
                "zoomminus",
                KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT,InputEvent.CTRL_MASK),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        
        
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
            new TabListener(),
            "newtab",
            KeyStroke.getKeyStroke("ctrl T"),
            JComponent.WHEN_IN_FOCUSED_WINDOW);
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
            new TabListener(),
            "closetab",
            KeyStroke.getKeyStroke("ctrl W"),
            JComponent.WHEN_IN_FOCUSED_WINDOW);
        ((JComponent) frame.getContentPane()).registerKeyboardAction(
            new TabListener(),
            "quit",
            KeyStroke.getKeyStroke("ctrl Q"),
            JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    /**
     * @return the browserWindow instance connected to the currently selected
     *              tab
     */
    public BrowserWindow getSelectedBrowser()
    {
        return ((BrowserTab) tabs.getSelectedComponent()).getBrowserWindow();
    }

    /**
     * The initial browserWindow, which is associated with the GUI
     */
    public BrowserWindow getInitialBrowser()
    {
        return initialBrowser;
    }

    public Window getWindow()
    {
        return frame;
    }
    public boolean isTabbed()
    {
        return true;
    }

    private class TBListener implements javax.swing.event.ChangeListener
    {
        public void stateChanged(ChangeEvent e)
        {
            // get stylesheets associated with this tab to stylesheetmenu
            menuBar.setStyleSheets();
        }
    }

    protected JTabbedPane getTabs()
    {
        return tabs;
    }
    private final double zoomModifier = 0.2;
    private class TabListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String cmd = e.getActionCommand();
            Log.debug("TabListener: " + cmd);
            if (cmd.equals("newtab"))
            {
                newTab();
            } else if (cmd.equals("closetab"))
            {
                delCurrentTab();
            } else if (cmd.equals("quit"))
            {
                // Log.debug("QUIT");
                System.exit(0);
            } else if (cmd.equals("zoomplus"))
            {
                //Log.debug("Zoomplus" +e.);
                //getInitialBrowser().setZoom(1.5);
                setZoomForBrowser(1.0 + zoomModifier);
            } else if (cmd.equals("zoomminus"))
            {
                Log.debug("Zoomplus");
                //getInitialBrowser().setZoom(1.5);
                setZoomForBrowser(1.0 - zoomModifier);
            }
        }
    }

    private void setZoomForBrowser(double zoomFactor)
    {
        for (int i = 0; i < getTabs().getTabCount(); i++)
        {
            double oldZoom = ((BrowserTab) getTabs().getComponentAt(i)).getBrowserWindow().getZoom();
            ((BrowserTab) getTabs().getComponentAt(i)).getBrowserWindow().setZoom((zoomFactor) * oldZoom);
        }
    }
    /**
     * @return whether or not this gui should be instructed to reload. XMLGUI
     *              should return false, since it creates an extra content area and
     *              browser for the content
     */
    public boolean shouldReloadAtStartup()
    {
        return true;
    }

}
