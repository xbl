package fi.hut.tml.xsmiles.gui.met;

import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.GUIManager;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.BrowserWindow;
import javax.swing.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;

/** 
 * A Menu for Metouia tabbed GUI for switching Stylesheets
 *
 * @author Juha
 */
public class MetStylesheetMenu extends JMenu 
{    
    ListenerWithStyle styleListener;
    Metouia gui;
    JMenuItem dissed;
    
    /**
     * @param The gui, so that menu can mess around with gui's
     * BrowserConstraints
     */
    public MetStylesheetMenu(Metouia g) 
    {
        super(Language.STYLESHEET);
        gui=g;
        styleListener=new ListenerWithStyle();
    }
    
    /**
     * Set the stylesheet titles to put in stylesheet menu
     */
    public void setStylesheets() 
    {
        if(gui.getSelectedBrowser().getXMLDocument()!=null) {
            int i;
            removeAll();
            Enumeration e=gui.getSelectedBrowser().getXMLDocument().getStylesheetTitles().elements();
            String title=gui.getSelectedBrowser().getXMLDocument().getCurrentStylesheetTitle();
	
            for(i=0;e.hasMoreElements();i++) {
                String s=(String)e.nextElement();
                JMenuItem it=new JMenuItem(s);
                if(s.equals(title)) {
                    it.setEnabled(false);
                    dissed=it;
                }
                it.addActionListener(styleListener);
                add(it);
            }
            if(i<2)
                setEnabled(false);
            else
                setEnabled(true);
        }
    }
    
    private class ListenerWithStyle implements ActionListener 
    {
        /**
         * @param ActionEvent encapsulating the title of stylesheet
         * change to disabled one to enabled
         * reload page to make changes effect
         */
        public void actionPerformed(ActionEvent e) {
            dissed.setEnabled(true);
            dissed=(JMenuItem)e.getSource();
            dissed.setEnabled(false);
            gui.getSelectedBrowser().setPreferredStylesheetTitle(e.getActionCommand());	  
            // Tell browser window to reload
            gui.getSelectedBrowser().navigate(NavigationState.RELOAD);
        }
    }
    
}
