package fi.hut.tml.xsmiles.gui.met;

import javax.swing.*;
import java.awt.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import fi.hut.tml.xsmiles.Log;

/**
 * A component for displaying a the nice animation that indicates document
 * being loaded. .
 * 
 * @author Juha
 */
public class MAnimate extends JButton implements Runnable
{
    private int delay, width, height;
    private boolean play;
    private int frame;
    private Thread animator;
    private boolean resizable=false;
    
    /**
     * The old constructor. Animate until stopped
     *
     * @param width in pixels
     *?@param height in pixels
     * @param delay betwean randomization
     */
    public MAnimate(int width, int height, int delay) 
    {
        super();
        this.delay=delay;
        init(width,height);
    }

    public MAnimate(int delay) 
    {
        super();
    	this.delay=delay;
        play=false;
        resizable=true;
    }
    
    /**
     * Animate with default delay.
     *
     * @param width in pixels
     *?@param height in pixels
     * @param delay betwean randomization
     */
    public MAnimate(int width, int height) {
        super();
        this.delay=700;
        init(width,height);
    }
    
    /**
     * Init
     *
     */
    private void init(int width, int height) {
        this.width=width;
        this.height=height;
        setSize(width, height);
        setPreferredSize(new Dimension(width, height));
        setMinimumSize(new Dimension(width, height));
        play=false;
        setActionCommand("xsmilesHome");
    }
    
    
    /**
     * Repaint component on a regular basis, until stopped
     */
    public void run() {
        while(play==true) {
            if(resizable) {
                //    width=(int)getSize().getWidth();
                //    height=(int)getSize().getHeight();
            }
            repaint();
            try{Thread.sleep(delay);}catch(Exception e) {	
            }
        }
        Log.debug("Exiting animation hread");
        repaint();
    }
    
    /**
     * Stop Animation
     * 
     */
    public synchronized void pause() {
        if(resizable) {
            // width=(int)getSize().getWidth();
            // height=(int)getSize().getHeight();
        }
        play=false;
    }
    
    /**
     * Stop Animation
     * 
     */
    public synchronized void play() {
        if (play==false) {
            play=true;
            animator=new Thread(this);    
            animator.start();
        }
    }

    public Dimension getPreferredSize() 
    {
        //Log.debug("PREF SIZE REQUESTED");
        return new Dimension(width,height);
    }
    
    public Dimension getSize() 
    {
        //Log.debug("GET SIZE REQUESTED");
        return new Dimension(width,height);
    }
    
    public Dimension getMinumumSize() 
    {
        //Log.debug("MIN SIZE REQUESTED");
        return new Dimension(width,height);
    }
    
    /**
     *  Do some fancy animation thingies..
     *
     */
    public void paint(Graphics g) {
        super.paint(g);
        int rowspan=(int)height/3;
        int colspan=(int)width/3;
        if (play) {
            //super.paint(g);
            g.setColor(Color.white);
            g.fillRect(0,0,width, height);
            g.setColor(Color.black);
	    
            for(int i=0;i<3;i++) 
                for(int j=0;j<3;j++) {
                    //int y1=gen.nextInt(3);
                    int y1 = this.getRandomInteger(3);
                    if(y1==j)
                        g.fillRect(i*colspan,j*rowspan,colspan, rowspan); 
                }
        } else {
            g.setColor(Color.white);
            g.fillRect(0,0,width,height);
            g.setColor(Color.black);
            g.fillRect(colspan,rowspan,colspan,rowspan);
        }
        g.setColor(Color.black);
        g.drawRect(0,0,width-1,height-1);
    }
    
	private int getRandomInteger(int range) 
    {
        int i = (int)(range * Math.random());
        return i;
    }
    
}
