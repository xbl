package fi.hut.tml.xsmiles.gui.met;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.gui.components.swing.LWGuiMenu;
import fi.hut.tml.xsmiles.gui.components.swing.LWMediatypeMenu;
import fi.hut.tml.xsmiles.gui.components.swing.SwingZoomMenu;

/**
 * MenuBar for Metouia GUI
 *
 * @author Juha Vierinen
 */
public class MMenuBar extends JMenuBar
{
    JMenu fileMenu, editMenu, viewMenu, helpMenu, bookmarksMenu, navigateMenu;
    JFileChooser fc;
    LWGuiMenu guiMenu;
    LWMediatypeMenu mediaMenu;
    SwingZoomMenu zoomMenu;
    // LWThemeMenu themeMenu;
    MetStylesheetMenu styleMenu;
    Metouia gui;

                    JTextField uriTextField;
                    JDialog openURIDialog;

    
    public static final String BOOKMARKS_VIEW="View";
    public static final String BOOKMARKS_VIEW_FRAME="View Frames";
    
    public MMenuBar(Metouia g)
    {
        super();
        gui=g;
        
        fileMenu = new JMenu(Language.FILE);
        viewMenu = new JMenu(Language.VIEW);
        helpMenu = new JMenu(Language.HELP);
        editMenu = new JMenu(Language.EDIT);
        navigateMenu = new JMenu("Navigate");
        bookmarksMenu = new JMenu(Language.BOOKMARKS);

        fileMenu.setMnemonic(java.awt.event.KeyEvent.VK_F);
        viewMenu.setMnemonic(java.awt.event.KeyEvent.VK_V);
        helpMenu.setMnemonic(java.awt.event.KeyEvent.VK_H);
        editMenu.setMnemonic(java.awt.event.KeyEvent.VK_E);
        navigateMenu.setMnemonic(java.awt.event.KeyEvent.VK_N);
        bookmarksMenu.setMnemonic(java.awt.event.KeyEvent.VK_B);

        // themeMenu= new LWThemeMenu(gui.getInitialBrowser(), gui);
        
        guiMenu=new LWGuiMenu(gui.getInitialBrowser());
		zoomMenu=new SwingZoomMenu(gui.getInitialBrowser());
        styleMenu=new MetStylesheetMenu(gui);
        mediaMenu=new LWMediatypeMenu(gui.getInitialBrowser());
        
        //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        fc = new JFileChooser("../demo");
        //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        
        ActionListener al = new MyListener();
        
        createMenuItem(fileMenu,Language.NEW,KeyEvent.VK_N,al);
        createMenuItem(fileMenu, "New Tab", KeyEvent.VK_T,al);
        createMenuItem(fileMenu,Language.OPEN,KeyEvent.VK_O,al);
        createMenuItem(fileMenu,"Open URI",KeyEvent.VK_U,al);
        createMenuItem(fileMenu,Language.SAVE,KeyEvent.VK_S,al);
        fileMenu.addSeparator();
        createMenuItem(fileMenu,Language.CLOSE,KeyEvent.VK_C,al);
        createMenuItem(fileMenu,Language.EXIT,KeyEvent.VK_X,al);
        
        
        createMenuItem(helpMenu,Language.ABOUT,KeyEvent.VK_A,al);

        createMenuItem(navigateMenu,"Back",KeyEvent.VK_B,al);
        createMenuItem(navigateMenu,"Forward",KeyEvent.VK_F,al);
        createMenuItem(navigateMenu,"Stop",KeyEvent.VK_S,al);
        createMenuItem(navigateMenu,"Reload",KeyEvent.VK_R,al);
        createMenuItem(navigateMenu,"Home",KeyEvent.VK_H,al);
        
        
        //	createMenuItem(viewMenu,Language.DOCUMENT,KeyEvent.VK_D,al);
        createMenuItem(viewMenu,Language.TREE,KeyEvent.VK_T,al);
		viewMenu.addSeparator();
		viewMenu.add(zoomMenu);
        viewMenu.addSeparator();
        createMenuItem(viewMenu,Language.SOURCEXMLXSL,KeyEvent.VK_C,al);
        createMenuItem(viewMenu,Language.SOURCEXML,KeyEvent.VK_M,al);
        createMenuItem(viewMenu,Language.SOURCEXSL,KeyEvent.VK_L,al);
        
        viewMenu.addSeparator();
        createMenuItem(viewMenu,Language.LOG,KeyEvent.VK_O,al);
        
        createMenuItem(editMenu,"Set as homepage",KeyEvent.VK_S,al);
        createMenuItem(editMenu,"Web Search",KeyEvent.VK_W,new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                //Log.debug("View bookmark: "+e.getActionCommand());
                try
                {
                    gui.getSelectedBrowser().newBrowserWindow(Browser.getXResource("xsmiles.search.page").toString(), true);
                }
                catch (Exception ex)
                {
                    Log.error(ex);
                }
                
            }}
        );
        
        // editMenu.add(themeMenu);
        editMenu.add(guiMenu);
        editMenu.add(styleMenu);
        editMenu.add(mediaMenu);
        editMenu.addSeparator();
        
        createMenuItem(editMenu,Language.CONFIGURATION,KeyEvent.VK_O,al);
        
        ActionListener editBookmarksListener = new EditBookmarksListener();
        createMenuItem(bookmarksMenu,"Edit",KeyEvent.VK_E,editBookmarksListener);
        ActionListener viewBookmarksListener = new ViewBookmarksListener();
        createMenuItem(bookmarksMenu,BOOKMARKS_VIEW,KeyEvent.VK_V,viewBookmarksListener);
        createMenuItem(bookmarksMenu,BOOKMARKS_VIEW_FRAME,KeyEvent.VK_F,viewBookmarksListener);
        
        add(fileMenu);
        add(editMenu);
        add(viewMenu);
        add(navigateMenu);
        add(bookmarksMenu);        
        add(helpMenu);
        //setHelpMenu(helpMenu);
    }
    
    private void createMenuItem(JMenu parent,String text, int shortcut, ActionListener list)
    {
        JMenuItem item;
        if (shortcut!=-1)
            item = new JMenuItem(text, shortcut);
        else
            item = new JMenuItem(text);
        parent.add(item);
        item.addActionListener(list);
    }
    
    public File showFileDialog(boolean save)
    {
        File file=null;
        URL requiredDoc=null;
        int returnVal=0;
        try
        {
            if(save)
                returnVal = fc.showSaveDialog(gui.getWindow());
            else
                returnVal = fc.showOpenDialog(gui.getWindow());
            if (returnVal == JFileChooser.APPROVE_OPTION)
                file = fc.getSelectedFile();
        } catch(Exception e)
        {
            Log.error(e);
        }
        return file;
    }
    
    /**
     * Update stylesheet references
     */
    public void setStyleSheets()
    {
        styleMenu.setStylesheets();
    }
    
    private class MyListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String com = e.getActionCommand();
            if(com.equals(Language.OPEN))
            {
                openFileDialog();
            } else if(com.equals(Language.NEW))
            {
                gui.getInitialBrowser().newBrowserWindow();
            } else if(com.equals(Language.CLOSE))
            {
                BrowserWindow browser=gui.getSelectedBrowser();
                if(gui!=null)
                {
                    if(gui.getInitialBrowser() != browser)
                    {
                        browser.closeBrowserWindow();
                    }
                    if(gui.getTabs().getTabCount() == 1)
                    {
                        gui.getInitialBrowser().closeBrowserWindow();
                    }
                    gui.delTab((BrowserTab)gui.getTabs().getSelectedComponent());
                } else
                {
                    Log.debug("Sorry, tab couldn't be closed. Removing content area anyway..");
                }
            } else if(com.equals(Language.SAVE))
            {
                saveFileDialog();
            } else if(com.equals(Language.EXIT))
            {
                exitDialog();
            } else if(com.equals("Instructions"))
            {
                instructions();
            } else if(com.equals(Language.ABOUT))
            {
                about();
            }
            //		 else if(com.equals(Language.DOCUMENT))
            //	      browser.changeViewType(XsmilesView.PRIMARYMLFC);
            else if(com.equals(Language.TREE))
                gui.showSource(gui.getSelectedBrowser().getXMLDocument(),XsmilesView.TREEMLFC,com+" : "+gui.getSelectedBrowser().getXMLDocument().getXMLURL());
            else if (com.equals(Language.SOURCEXML))
                gui.showSource(gui.getSelectedBrowser().getXMLDocument(),XsmilesView.SOURCEMLFC,com+" : "+gui.getSelectedBrowser().getXMLDocument().getXMLURL());
            else if(com.equals(Language.SOURCEXMLXSL))
                gui.showSource(gui.getSelectedBrowser().getXMLDocument(),XsmilesView.SOURCE_AND_XSL_MLFC,com+" : "+gui.getSelectedBrowser().getXMLDocument().getXMLURL());
            else if(com.equals(Language.SOURCEXSL))
                gui.showSource(gui.getSelectedBrowser().getXMLDocument(),XsmilesView.XSL_MLFC,com+" : "+gui.getSelectedBrowser().getXMLDocument().getXSLURL());
            else if(com.equals(Language.CONFIGURATION))
            {
                optionsDialog();
            }
            else if(com.equals(Language.LOG))
            {
                openLog();
            }
            else if(com.equals("Set as homepage"))
            {
                String urli=gui.getSelectedBrowser().getDocumentHistory().getLastDocument().getURLString();
                if(urli!=null || ! urli.equals(""))
                {
                    gui.getSelectedBrowser().getBrowserConfigurer().setProperty("main/homepage",urli);
                    gui.getSelectedBrowser().getBrowserConfigurer().writeConfig();
                }
            } else if(com.equals("New Tab"))
            {
                gui.newTab();
            } else if(com.equals("Back")) 
                {
                    gui.getSelectedBrowser().navigate(NavigationState.BACK);
                }
            else if(com.equals("Forward")) 
                {
                    gui.getSelectedBrowser().navigate(NavigationState.FORWARD);
                }
            else if(com.equals("Stop")) 
                {
                    gui.getSelectedBrowser().navigate(NavigationState.STOP);
                }

            else if(com.equals("Reload")) 
                {
                    gui.getSelectedBrowser().navigate(NavigationState.RELOAD);
                }

            else if(com.equals("Home")) 
                {
                    gui.getSelectedBrowser().navigate(NavigationState.HOME);
                }
            else if(com.equals("Open URI")) 
                {
                    //                    Thread t = new Thread("Open URI Dialog")
                    //    {
                    //public void run()
                    //        {
                                
                    openURIDialog = new JDialog((JFrame)gui.getWindow(),"Open URI");
                    
                    uriTextField = new JTextField("http://",30);
                    JButton doit = new JButton("Do it");
                    URIListener ul = new URIListener();
                    doit.addActionListener(ul);
                    uriTextField.addActionListener(ul);
                    openURIDialog.setContentPane(new JPanel());
                    openURIDialog.getContentPane().setLayout(new FlowLayout());
                    openURIDialog.getContentPane().add(new JLabel("Open URI"));
                    openURIDialog.getContentPane().add(uriTextField);
                    openURIDialog.getContentPane().add(doit);
                    openURIDialog.setSize(450,100);
                    openURIDialog.show();
                    //}
                    
                            
                    //};
                    //    t.start();
                }
        }

                    class URIListener implements ActionListener 
                    {
                        public void actionPerformed(ActionEvent ee) 
                        {
                            String cmd = ee.getActionCommand();
                            gui.getSelectedBrowser().openLocation(uriTextField.getText());
                            openURIDialog.dispose();
                        }
                    }
        

        private void openLog()
        {
            gui.getSelectedBrowser().openLog();
        }
        
        /**
         * Show about dialog
         */
        private void about()
        {
            try
            {
                gui.getSelectedBrowser().newBrowserWindowWithGUI(Browser.getXResource("xsmiles.about").toString(), "about", false);
            } catch(MalformedURLException e)
            {
                Log.error("Error about dialog cannot be shown.");
            }
        }
        
        private void exitDialog()
        {
            gui.getInitialBrowser().closeBrowser();
        }
        
        private void optionsDialog()
        {
            try
            {
                gui.getSelectedBrowser().newBrowserWindow(Browser.getXResource("xsmiles.config.editor").toString());
            } catch (Exception e)
            {
                Log.error(e);
            }
        }
        
        private void saveFileDialog()
        {
            File file=null;
            URL requiredDoc=null;
            int returnVal=0;
            int viewType = gui.getSelectedBrowser().getViewType();
            PrintWriter out = null;
            XMLDocument doc = gui.getSelectedBrowser().getXMLDocument();
            returnVal = fc.showSaveDialog(gui.getWindow());
            if (returnVal == JFileChooser.APPROVE_OPTION)
                file = fc.getSelectedFile();
            try
            {
                requiredDoc = toURL(file);
            } catch(Exception e)
            {
            }
            if (file != null)
            {
                try
                {
                    out = new PrintWriter(new FileWriter(file));
                } catch(Exception e)
                {
                }
                if(viewType == XsmilesView.XSL_MLFC)
                {
                    Vector sourceVector = new Vector();
                    sourceVector        = doc.getXSLVector() ;
                    for (int i=0; i < sourceVector.size(); i++)
                    {
                        String vectorElement = sourceVector.elementAt(i).toString();
                        out.print(vectorElement);
                        out.print("\n");
                    }
                } else if(viewType == XsmilesView.SOURCEMLFC)
                {
                    Vector sourceVector = new Vector();
                    sourceVector        = doc.getXMLVector();
                    
                    for (int i=0; i < sourceVector.size(); i++)
                    {
                        String vectorElement = sourceVector.elementAt(i).toString();
                        out.print(vectorElement);
                        out.print("\n");
                    }
                } else if(viewType == XsmilesView.PRIMARYMLFC || viewType == XsmilesView.SOURCE_AND_XSL_MLFC)
                {
                    Vector sourceVector = new Vector();
                    sourceVector        = doc.getSourceVector();
                    for (int i=0; i < sourceVector.size(); i++)
                    {
                        String vectorElement = sourceVector.elementAt(i).toString();
                        out.print(vectorElement);
                        //out.print("\n");
                    }
                }
                out.close();
            }
        }
        
        private void openFileDialog()
        {
            URL requiredDoc=null;
            File file=showFileDialog(false);
            try
            {
                requiredDoc = toURL(file);
            } catch(Exception e)
            {
                // Log.error(e);
            }
            if(requiredDoc!=null)
                gui.getSelectedBrowser().openLocation(requiredDoc, true);
        }
        
        
        
        private void instructions()
        {
        }
    }
    
    private class ChangeGuiListener implements ActionListener
    {
        
        public void actionPerformed(ActionEvent e)
        {
            String com = e.getActionCommand();
            Vector guiNames=gui.getInitialBrowser().getGUIManager().getGUINames();
            gui.getInitialBrowser().getGUIManager().showGUI(com);
        }
    }
    
    public static URL toURL(File file) throws java.net.MalformedURLException
    {
        String path = file.getAbsolutePath();
        if (File.separatorChar != '/')
        {
            path = path.replace(File.separatorChar, '/');
        }
        if (!path.startsWith("/"))
        {
            path = "/" + path;
        }
        if (!path.endsWith("/") && file.isDirectory())
        {
            path = path + "/";
        }
        return new URL("file", "", path);
    }
    
    private class EditBookmarksListener implements ActionListener
    {
        public EditBookmarksListener()
        {
            super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
            Log.debug("Edit bookmark");
            try
            {
                gui.getSelectedBrowser().newBrowserWindow(Browser.getXResource("xsmiles.bookmarks.document").toString(), true);
            } catch(Exception e1)
            {
                Log.error(e1);
            }
            Log.debug("done");
        }
        
    }
    private class ViewBookmarksListener implements ActionListener
    {
        public ViewBookmarksListener()
        {
            super();
        }
        
        public void actionPerformed(ActionEvent e)
        {
            Log.debug("View bookmark: "+e.getActionCommand());
            try
            {
                if (e.getActionCommand().equals(BOOKMARKS_VIEW_FRAME))
                    gui.getSelectedBrowser().newBrowserWindow(Browser.getXResource("xsmiles.bookmarks.view.frame").toString(), true);
                else
                    gui.getSelectedBrowser().newBrowserWindow(Browser.getXResource("xsmiles.bookmarks.view").toString(), true);
            } catch(Exception e1)
            {
                Log.error(e1);
            }
            Log.debug("done");
        }
        
    }
    
}

