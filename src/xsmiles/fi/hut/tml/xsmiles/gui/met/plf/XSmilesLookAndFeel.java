package fi.hut.tml.xsmiles.gui.met.plf;

import java.awt.*;
import javax.swing.*;

import net.sourceforge.mlf.metouia.MetouiaLookAndFeel;

/**
 *
 */
public class XSmilesLookAndFeel extends MetouiaLookAndFeel
{
    /**
     * Initializes the uiClassID to BasicComponentUI mapping.
     * The JComponent classes define their own uiClassID constants. This table
     * must map those constants to a BasicComponentUI class of the appropriate
     * type.
     *
     * @param table The ui defaults table.
     */
    protected void initClassDefaults(UIDefaults table)
    {
        super.initClassDefaults(table);
        try
        {
            String windowsFileChooserUIClass = "com.sun.java.swing.plaf.windows.WindowsFileChooserUI";
            Class lf = Class.forName(windowsFileChooserUIClass);
            table.putDefaults(new Object[]
            {
                "FileChooserUI", windowsFileChooserUIClass
            });
        } catch (Throwable exception)
        {
            // Log.error(e);
        }
    }
    
}
