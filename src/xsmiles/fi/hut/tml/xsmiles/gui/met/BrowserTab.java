package fi.hut.tml.xsmiles.gui.met;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.event.GUIEvent;
import fi.hut.tml.xsmiles.event.GUIEventAdapter;
import fi.hut.tml.xsmiles.event.GUIEventListener;
import fi.hut.tml.xsmiles.gui.components.swing.XAFrame;

/**
 * A Component that goes into the tab of the Met GUI
 * 
 * Basically it attached a listener to the browserwindow provides an area for
 * the mlfc controls, and shows if the browser is loading/resting
 * 
 * @author juha
 */
public class BrowserTab extends JPanel implements GUIEventListener {

    private BrowserWindow browser;
    private Metouia gui;
    private MToolBar toolBar;
    private JTextField statusField;
    private boolean browserFired = true;
    private boolean titleSet = false;
    private String loc = "";
    
    protected ButtonListener buttonListener;

    /**
     * Init with or without browser as paramter if browser not given, one will
     * be created
     */
    public BrowserTab(Metouia g) {
        super();
        gui = g;
        try {
            init(new XLink(Browser.getXResource("xsmiles.empty")), "default");
        } catch (Exception e) {
            Log.error("Couldn't create empty page. sad.");
        }
    }

    /**
     * Init with or without browser as paramter if browser not given, one will
     * be created
     */
    public BrowserTab(Metouia g, XLink l, String id) {
        super();
        gui = g;
        try {
            init(l, id);
        } catch (Exception e) {
            Log.error("Couldn't create empty page. sad.");
        }
    }
    
    public void openInNewWindow(XLink l, String id) {
        this.browser.newBrowserWindow(l.getURL().toString(),id,true);
        }


    /**
     * Init with or without browser as paramter if browser not given, one will
     * be created
     */
    public BrowserTab(BrowserWindow b) {
        super();
        if (b != null)
            browser = b;

        try {
            init(new XLink(Browser.getXResource("xsmiles.empty")), "default");
        } catch (Exception e) {
            Log.error("Couldn't create empty page. sad.");
        }
    }

    public BrowserTab(BrowserWindow b, Metouia g) {
        super();
        if (b != null)
            browser = b;
        if (g != null)
            gui = g;
        try {
            init(new XLink(Browser.getXResource("xsmiles.empty")), "default");
        } catch (Exception e) {
            Log.error("Couldn't create empty page. sad.");
        }
    }

    private void init(XLink initialURI, String id) {
        if (browser == null) {
            // the browser is not handed to us, thus it has to be created.
            // assume that this is a tabgui pane
            browser = new Browser(initialURI, id, true, "tabguitab");
            ((GUIImpl) browser.getGUIManager().getCurrentGUI()).setGUIWindow(gui);
        }
        Log.debug("Created browser");
        Log.debug("Set GUI Window browser");

        setLayout(new BorderLayout());

        // Lowerbar
        JPanel lower = new JPanel();
        lower.setLayout(new BorderLayout());

        statusField = new JTextField("");
        lower.add(statusField, BorderLayout.CENTER);

        // mlfc toolbar
        JPanel pan = new JPanel();
        pan.setLayout(new BorderLayout());
        pan.add(browser.getMLFCControls().getMLFCToolBar().getComponent(), BorderLayout.WEST);
        lower.add(pan, BorderLayout.NORTH);

        // Toolbar
        toolBar = new MToolBar();
        buttonListener = new ButtonListener(this);
        toolBar.addButtonActionListener(buttonListener);

        add(lower, BorderLayout.SOUTH);
        add(toolBar, BorderLayout.NORTH);
        add(browser.getContentArea(), BorderLayout.CENTER);

        /*
         * toolBar.getCombo().registerKeyboardAction(new
         * ButtonListener(this),"comboBoxEdited",KeyStroke.getKeyStroke("ENTER"),JComponent.WHEN_FOCUSED);
         */
        toolBar.getCombo().registerKeyboardAction(new ButtonListener(this), "comboBoxEditedNewTab", KeyStroke.getKeyStroke("ctrl ENTER"),
                JComponent.WHEN_FOCUSED);

        Log.debug("GUIManager: " + browser.getGUIManager().toString());
        // Log.debug("GUI: " +
        // browser.getGUIManager().getCurrentGUI().toString());
        browser.getGUIManager().getCurrentGUI().addGUIEventListener(this);
        // fire last events kludge, to ensure that the events that might have
        // come from the browser while doing other stuff come through.
        browser.getGUIManager().getCurrentGUI().fireLatestEvents();
    }

    public BrowserWindow getBrowserWindow() {
        return browser;
    }

    /**
     * Listen for back forward etc events.
     */
    private class ButtonListener implements ActionListener {

        BrowserTab tab;

        public ButtonListener(BrowserTab bt) {
            tab = bt;
        }

        public void actionPerformed(ActionEvent e) {
            String cmd = e.getActionCommand();

            //Log.debug(cmd);
            if (cmd.equals("back")) {
                browser.navigate(NavigationState.BACK);
            } else if (cmd.equals("comboBoxChanged")) {
                if (browserFired) {
                    browserFired = false;
                } else {
                    String s = (String) toolBar.getCombo().getSelectedItem();
                    if (s.indexOf(':') < 0 && s.startsWith(".") == false) {
                        s = "http://" + s;
                    }
                    browser.openLocation(s);
                    browserFired = false;
                }
            } else if (cmd.equals("forward")) {
                browser.navigate(NavigationState.FORWARD);
            } else if (cmd.equals("stop")) {
                browser.navigate(NavigationState.STOP);
            } else if (cmd.equals("reload")) {
                browser.navigate(NavigationState.RELOAD);
            } else if (cmd.equals("home")) {
                browser.navigate(NavigationState.HOME);
            } else if (cmd.equals("xsmilesHome")) {
                browser.openLocation("http://www.xsmiles.org");
            } else if (cmd.equals("nutab")) {
                if (gui != null)
                    gui.newTab();
                else
                    Log.debug("Sorry, gui doesn't support tabs");
            } else if (cmd.equals("closetab")) {
                if (gui != null) {
                    if (gui.getInitialBrowser() != browser) {
                        browser.closeBrowserWindow();
                    }
                    if (gui.getTabs().getTabCount() == 1) {
                        gui.getInitialBrowser().closeBrowserWindow();
                    }
                    gui.delTab(tab);
                } else {
                    Log.debug("Sorry, tab couldn't be closed. Removing content area anyway..");
                }
            } else if (cmd.equals("comboBoxEdited")) {
                String s = (String) toolBar.getCombo().getSelectedItem();
                if (s.indexOf(':') < 0 && s.startsWith(".") == false) {
                    s = "http://" + s;
                }
                browser.openLocation(s);
            } else if (cmd.equals("comboBoxEditedNewTab")) {
                Log.debug("SHOULD OPEN IN NEW TAB");
                String s = (String) toolBar.getCombo().getSelectedItem();
                if (s.indexOf(':') < 0 && s.startsWith(".") == false) {
                    s = "http://" + s;
                }
                gui.openInNewTab(new XLink(s), "default");
            }
        }
    }

    public void start() {
    }

    public void destroy() {
    }

    public void openInNewTab(XLink l, String id) {
        Log.debug("DIU");
        gui.openInNewTab(l, id);
    }

    public void setStatusText(String statusText) {
        //Log.debug("set statusfield: "+statusText);
        statusField.setText(statusText);
    }

    public void setEnabledBack(boolean value) {
        toolBar.setEnabledBack(value);
    }

    public void setEnabledForward(boolean value) {
        toolBar.setEnabledForward(value);
    }

    public void setEnabledHome(boolean value) {
        toolBar.setEnabledHome(value);
    }

    public void setEnabledStop(boolean value) {
        toolBar.setEnabledStop(value);
    }

    public void setEnabledReload(boolean value) {
        toolBar.setEnabledReload(value);
    }

    public void setTitle(String title) {
        String tit = createTabTitleString(title);
        ((XAFrame) gui.getWindow()).setTitle(title);
        gui.getTabs().setTitleAt(gui.getTabs().indexOfComponent(this), tit);
        titleSet = true;
    }

    public void setLocation(String s) {
        loc = s;
        browserFired = true;
        toolBar.getCombo().removeActionListener(this.buttonListener);
        
        toolBar.getCombo().setText(s);
        toolBar.getCombo().addActionListener(this.buttonListener);
    }

    public void browserWorking() {
        titleSet = false;
        toolBar.play();
    }

    public void browserReady() {
        this.gui.menuBar.setStyleSheets();
        toolBar.pause();
        if (titleSet == false) {
            setTitle(loc);
            titleSet = true;
        }
    }

    /**
     * receive events from the gui that are intended for a GUI. This basically
     * does what the GUI interface did once upon a time
     */
    private class TabListener extends GUIEventAdapter {

        /**
         * After browser is ready, then start is called. Not neccesary to
         * implement
         */
        public void start() {
        }

        /**
         * Destroy The GUI (delete frame, etc)
         */
        public void destroy() {
            // dispose stuff?
        }

        /**
         * Informs the gui that browser is working
         */
        public void browserWorking() {
            //toolBar.play();
        }

        /**
         * Informs the gui that browser is resting
         */
        public void browserReady() {
            // toolBar.pause();
        }

        /**
         * @param s
         *                   The location that is beeing loaded
         */
        public void setLocation(String s) {
            // toolBar.getCombo().setText(s);
        }

        /**
         * @param statusText
         *                   The text to put in status bar
         */
        public void setStatusText(String statusText) {
            // statusField.setText(statusText);
        }
    }



    public void GUIEvent(GUIEvent ev) {
    }

    private String createTabTitleString(String title) {
        String fin = "";
        if (title.length() > 25) {
            fin = title.substring(0, 11) + "..." + title.substring(title.length() - 11, title.length());
        } else {
            fin = title;
        }
        return fin;
    }
}
