package fi.hut.tml.xsmiles.gui.met;

import java.awt.Container;
import java.awt.Window;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.gui.swing.XSmilesUI;

/**
 * Base class for both MetouiaGUI and BrowserTab (both offer the same 
 * contentHandlers etc, but the BrowserTab doesn't do the Tabbing or frame
 *
 * also this takes care of adding and removing tabs
 */
public class GUIImpl extends XSmilesUI
{
    private Metouia guiWindow;
    protected BrowserWindow browser;
    private ComponentFactory cf;
    private MLFCControls mlfcControls;

    /**
     * Constructor
     */
    public GUIImpl(BrowserWindow b)
    {
        super(b);
        browser=b;
        cf=new DefaultComponentFactory(b);
    }
    
    public GUIImpl(BrowserWindow b, Container c)
    {
        super(b);
        browser=b;
        cf=new DefaultComponentFactory(b);
    }
    
    public void setGUIWindow(Metouia g) 
    {
        guiWindow=g;
    }
    
    public Metouia getGUIWindow() 
    {
        return guiWindow;
    }
    
        public boolean isTabbed()
    {
        return true;
    }


    // GUI interface methods
    public ComponentFactory getComponentFactory() 
    {
        if(cf == null) {
            cf=new DefaultComponentFactory(browser);
        }
        return cf;
    }  
    
    public MLFCControls getMLFCControls() {
        if (mlfcControls==null) {
            mlfcControls = new DefaultMLFCControls(getComponentFactory());
        }
        return mlfcControls;
    }

    public fi.hut.tml.xsmiles.content.ContentHandlerFactory getContentHandlerFactory()
    {
        if (contentHandlerFactory==null) contentHandlerFactory=new SwingContentHandlerFactory(browser);
        return contentHandlerFactory;
    }
    
    public Window getWindow() 
    {
        return guiWindow.getWindow();
    }
    
        /** @return whether or not this gui should be instructed to reload.
     * XMLGUI should return false, since it creates an extra content area and
     * browser for the content
     */
    public boolean shouldReloadAtStartup()
    {
        return false;
    }
}
