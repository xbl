package fi.hut.tml.xsmiles.gui;

import java.awt.Component;
import java.awt.Window;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.event.GUIEventListener;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;

/**
 * This is an interface for all user interfaces that are used with the
 * Browser. 
 * testing..... kolmas....
 * @author Niklas von Knorrin, Juha, MH
 * @version 0
 */
public interface GUI 
{
    /**
     * @return A ComponentFactory which is capable of returning all components needed
     *         by MLFCs, or any other non-GUI package classes.
     * @see ComponentFactory
     */
    public ComponentFactory getComponentFactory();
    
    public MLFCControls getMLFCControls();

    /**
     * @return ContentHandler creates players for certain mime types
     * @see ContentHandlerFactory
     */
    public ContentHandlerFactory getContentHandlerFactory();

    /**
     * After browser is ready, then start is called. 
     * Not neccesary to implement
     */
    public void start();

    /**
     * Destroy The GUI (delete frame, etc)
     */
    public void destroy();

    /**
     * @return the Frame of current GUI
     */
    public Window getWindow();

    /**
     * Can gui accommodate several content areas within one frame
     */
    public boolean isTabbed();

    /**
     * Open up a link in a new tab
     * @param id the id of the browser to be
     */ 
    public void openInNewTab(XLink link, String id);

    /**
     * Open up a link in a new tab
     * @param id the id of the browser to be
     */ 
    public void openInNewWindow(XLink link, String id);
    
        /**
         * @return The container where content is rendered
         * @deprecated The contentArea lives in the Browser class.
         */
        //public Container getContentPanel();

        /** 
         * Move the highlight of the selected link up
         */
        public void moveActiveLinkUp();

    /** 
     * Move the highlight of the selected link down
     */
    public void moveActiveLinkDown();

    /**
     * Informs the gui that browser is working
     */
    public void browserWorking();

    /**
     * Informs the gui that browser is resting
     */
    public void browserReady();

    /**
     * @param s The location that is beeing loaded
     */
    public void setLocation(String s);

    /**
     * @param n Set the skin to n, if possible
     */
    public void setSkinsIfNeeded(String n);

    /**
     * @param statusText The text to put in status bar
     */
    public void setStatusText(String statusText);
    
    /**
     * @param value Set state of back widget
     */
    public void setEnabledBack(boolean value);

    /**
     * @param value Set state of forward widget
     */
    public void setEnabledForward(boolean value);

    /**
     * @param value Set state of home widget
     */
    public void setEnabledHome(boolean value);

    /**
     * @param value Set state of top widget
     */
    public void setEnabledStop(boolean value);

    /** 
     * @param value Set state of reload widget
     */
    public void setEnabledReload(boolean value);

    /**
     * @param title Set title of UI frame
     */
    public void setTitle(String title);
  
    /**
     * @param target JTextComponent to receive "virtual" key presses
     * @param mode inputMode for text element
     */
    public void displayKeypad(Component target, String mode);
    public void hideKeypad();

    /**
     * GUI shows a popup error dialog. ErrorHandling should call the Browser's 
     * showErrorDialog instead of this.
     *
     * @param isModal Modality
     * @param heading Headline of error
     *?@param description Description of error
     */
    public void showMessageDialog(boolean isModal, String heading, String description,long timeToLiveMillis);

    /**
     * GUI shows a popup error dialog. ErrorHandling should call the Browser's 
     * showErrorDialog instead of this.
     *
     * @param isModal Modality
     * @param heading Headline of error
     *?@param description Description of error
     */
    public void showErrorDialog(boolean isModal, String heading, String description);
  
    /** The modes are from XSmilesView class */
    public void showSource(XMLDocument doc, int mode, String heading);
  
    /** show the file open dialog */
    //public File showFileDialog(boolean save);

    /**
     *
     */
    public void reDrawGUI();

    public void addGUIEventListener(GUIEventListener e);
    public void removeGUIEventListener(GUIEventListener e);
    
    /** @return whether or not this gui should be instructed to reload.
     * XMLGUI should return false, since it creates an extra content area and
     * browser for the content
     */
    public boolean shouldReloadAtStartup();

    // KLUDGE: in the initialization phase, the GUI might be left without the first events, if using a GUIEventListener. This fires the events again.
    public void fireLatestEvents();
    
    public void registerFocusPointProvider(FocusPointsProvider fpp, BrowserWindow bw);

    public void unRegisterFocusPointProvider(FocusPointsProvider fpp, BrowserWindow bw);

}
