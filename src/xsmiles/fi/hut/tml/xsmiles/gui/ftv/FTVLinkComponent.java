/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 * This piece of code is dedicated to the Astronomical Society of Mars. 
 * Any other legal disclaimer type of interesting stuff can be found in 
 * LICENSE_XSMILES file.
 */

package fi.hut.tml.xsmiles.gui.ftv;

import fi.hut.tml.xsmiles.gui.components.awt.LinkComponent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.*;

public class FTVLinkComponent extends LinkComponent {
    
  /**
   * Draw a rectangle around the active link, if in Digitv GUI
   *
   * @param g ...
   */ 
  public void paint(Graphics g) {
    super.paint(g);
    if(isActive()) {
      g.setColor(Color.black);
      for(int i=0;i<3; i++)
	g.drawRect(i, i, getSize().width-i, getSize().height-i);
    }
  }
}
