/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/**
 * DoubleBufferedPanel for eliminating the flash whilest drawing 
 * lightweight components.
 *
 * See Graphic Java pages 482-> for more info
 * 
 * @author Juha
 * @version 0.1
 */

package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;

public class DoubleBufferedPanel extends Panel {
    Image offscreen;
    
    /**
     * Null out the offsdcreen buffer as part of the invalidation
     */
    public void invalidate() {
	super.invalidate();  
	offscreen = null;
    }
    
    /**
     * Override udate to not erase the background before painting
     */
    public void update(Graphics g) {
	paint(g);
    }
    
    /**
     * Paint children into an offscreen buffer, then blast entire 
     * image at once
     */
    public void paint(Graphics g) {
	try{
	    if(offscreen==null) {
		offscreen = createImage(getSize().width, getSize().height);
	    }
	    
	    Graphics og = offscreen.getGraphics();	
	    
	    og.setClip(0,0,getSize().width, getSize().height);
	    super.paint(og);
	    g.drawImage(offscreen, 0, 0, this);
	    
	    if(!(og==null))
		og.dispose();
	} catch(Exception e){
	    System.err.println("DoubleBufferedPanel:Something wrong with paint(g)");
	}
    }
    
    /**
     * Override method remove, for removing component also from the offscreen buffer
     */
    public void remove(Component c) {
	offscreen=null; 
	super.remove(c);
    }
}
