/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import fi.hut.tml.xsmiles.Log;

/**
 * This is as configuration menu component, similar to the ColorMenu.
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class ConfigMenu extends Container implements FakeRemoteListener {
  // Heavyweight
  //
  private int width, height;
  private Color color;
  private Vector items;
  private boolean[] values;
  private String[] types;
  private int size;
  private static int FONTSIZE=20;
  private static int SPACING=30;
  private Double CHARLENGTH;
  private String FONT="Tiresias";
  private int selected=0;
  public MenuKeyListener keyListener;
  private String lista;
  // vakio liittyy komponentin leveyteen. 
  private int VAKIO=70;
  private Painter painter;
    
  /**
   * ConfigMenu, with default color=green
   */
  public ConfigMenu() {
    this.color=Color.green;
    init();
  } 
    
  /**
   * Create a custom colored menu
   */
  public ConfigMenu(Color color) {
    this.color=color;
    init();
  }
    
  /**
   * Initalization
   */
  private void init() {
    this.values=new boolean[20];
    this.types=new String[20];
    CHARLENGTH=new Double(0.75*FONTSIZE);
    items=new Vector();
    width=100;
    selected=0;
    keyListener=new MenuKeyListener();
    addKeyListener(keyListener);
    requestFocus();
    painter=new Painter(this);
    painter.setSize(width,height);
    add(painter);
  }
    
  public void setSize(int width, int height) {
    this.width=width;
    this.height=height;
    super.setSize(width,height);
    painter.setSize(width,height);
  }
    
  /**
   * Add a config_item to list
   */
  public void add(String item) {
    size=items.size();
    items.addElement(item);
    values[size]=false;
    types[size]="CHECKABLE";
    lista=lista+item+"\n";
	
    if(item.length()*CHARLENGTH.intValue()>width)
      width=item.length()*CHARLENGTH.intValue()+VAKIO;
    height=(size+1)*SPACING+28;
    setSize(width, height);
  }
    
  /** 
   * Add a config_item and a value for the item
   */
  public void add(String item, boolean value) {
    size=items.size();
    items.addElement(item);
    values[size]=value;
    types[size]="CHECKABLE";
    lista=lista+item+"\n";
    if(item.length()*CHARLENGTH.intValue()>width)
      width=item.length()*CHARLENGTH.intValue()+VAKIO;
    height=(size+1)*SPACING+28;
    setSize(width, height);
  }
    
  /**
   * Add a normal item to config menu
   * 
   * @param item an item
   * @param type Type of item only choice is: NORMAL 
   */
  public void add(String item, String type) {
    size=items.size();
    items.addElement(item);
    values[size]=false;
    types[size]="NORMAL";
    if(item.length()*CHARLENGTH.intValue()>width)
      width=item.length()*CHARLENGTH.intValue()+70;
    height=(size+1)*SPACING+28;
    setSize(width, height);	
  }
    
  
    
  /**
   * Gets the selected item
   *
   * @return The selected item
   */
  public String getSelected() {
    return (String) items.elementAt(selected);
  }
    
  /**
   * returns the number of the selected item
   *
   * @return int The number of the selected item
   */
  public int getSelectedNum() {
    return selected;
  }
    
  /**
   * Move selection head upwards
   */
  public void moveUp() {
    if (selected>0) 
      selected--;
    repaint();
  }
    
  /**
   * Move selectionhead downwards
   */
  public void moveDown() {
    if (selected<size-1)
      selected++;
    repaint();
  }
    
  /**
   * Change the state of an element at a certain position
   * 
   * @param position position of item
   */
  public void changeValue(int position) {
    values[position]=!(values[position]);
    repaint();
  }
	
  /**
   * Because we have coupled a keylistener with the menu
   * to do all listening and event handling we must therefore
   * pass ActionListener forward to the keyListener;
   */
  public void addActionListener(ActionListener listener) {
    keyListener.addActionListener(listener);
  }
    
  /**
   * Tell layoutmanagaer what the size of this applet should be
   *
   * @return Dimension
   */
  public Dimension getPreferredSize() {
    return(new Dimension(width, height));
  }    
    
  public boolean isFocusTraversable() {
    return true;
  }
    
  public boolean isLightWeight() {
    return true;
  }
    
  public boolean isOpague() {
    return true;
  }

  public String getColor() {
    return color.toString();
  }

  public void keyPressed(Component c, int i) {
    keyListener.keyPressed(new KeyEvent(this,0,0,0,i)); 
  }

  private class MenuKeyListener extends KeyAdapter {
	
    private ConfigMenu referer;
    private ActionListener listener;
	
    /** 
     * A standard method for processing incoming keyboard events.
     * Some are sent forward to an actionListener assosiated w 
     * the ColorMenu
     *
     * @param e The keyevent
     */
    public void keyPressed(KeyEvent e) {
      referer=(ConfigMenu) e.getComponent();
	    
      //	    if(e.getKeyCode()==KeyEvent.VK_UP || e.getKeyCode()==KeyEvent.VK_KP_UP)
      if(e.getKeyCode()==KeyEvent.VK_UP)

	referer.moveUp();
      //	    else if (e.getKeyCode()==KeyEvent.VK_DOWN || e.getKeyCode()==KeyEvent.VK_KP_DOWN)
      else if (e.getKeyCode()==KeyEvent.VK_DOWN)

	referer.moveDown();
      else if (e.getKeyCode()==KeyEvent.VK_ENTER) {
	referer.changeValue(referer.getSelectedNum());
	processActionEvent(referer.getSelected());
      }
    }
	
    /**
     * This is for sending actionEvents to actionlistener 
     * associated with this ColorMenu.
     *
     * @str The command to bundle with actionEvent
     */
    public void processActionEvent(String str) {
      if(!(listener==null))
	listener.actionPerformed(new ActionEvent(this, 1, referer.getSelected()));
      Log.debug("Sending to ActionListener: " + referer.getSelected());
    }
	
    /** 
     * Adds an actionListener
     *
     * @param listener The ActionListener
     */
    public void addActionListener(ActionListener listener) {
      this.listener=listener;
    }
  }
    
  private class Painter extends Component {

    private MenuKeyListener keyListener;
	
    public Painter(ConfigMenu cm) 
    {
      keyListener = cm.keyListener;
    }	

    /**
     * Because we have coupled a keylistener with the menu
     * to do all listening and event handling we must therefore
     * pass ActionListener forward to the keyListener;
     */
    public void addActionListener(ActionListener listener) {
      keyListener.addActionListener(listener);
    }
	
    /**
     * Tell layoutmanagaer what the size of this applet should be
     *
     * @return Dimension
     */
    public Dimension getPreferredSize() {
      //return(new Dimension(width, height));
      return this.getSize();
    }    
	
    public boolean isFocusTraversable() {
      return true;
    }
	
    public boolean isLightWeight() {
      return true;
    }
	
    /**
     * Here we do some fancy drawing
     * using the java 2d api.
     * When we want 1.1 compatibility then when downgrade to normal graphics
     * 
     * @param g0 The graphics that is passed to the component by the system
     */
    public void paint(Graphics g0) {
      int y;
      Graphics g;
      g = g0;
      //g=(Graphics2D) g0;
      //g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g.setFont(new Font(FONT, Font.BOLD, FONTSIZE));
	    
      int w = (int)this.getSize().width;
      int h = (int)this.getSize().height;
		
      // Frame & background
      g.setColor(Color.black);
      g.fillRect(0, 20, w, h);
      g.setColor(new Color(50,50,50));
      g.fillRect(4,24,w-8,h-28);
      g.setColor(Color.black);
      g.fillOval(50,0,40,40);
      g.setColor(color);
	    
      g.fillOval(54,4,32,32);
	    
      // Highlight
      g.setColor(new Color(0,130,0));
      g.fillRect(4, (selected*SPACING)+48, w-8, 34);
	    
      // Text
      g.setColor(Color.white);
      for(y=0;y<size;y++) {
	String tmp = (String) items.elementAt(y);
	g.drawString(tmp, 45, y*SPACING + 72);
      }
	    
      // Settings
      for(y=0;y<size;y++) {
	if(!(types[y]==null))
	  if(values[y] && types[y].equals("CHECKABLE"))
	    g.fillOval(13,y*SPACING+56, 16,16);
      }
	    
      super.paint((Graphics) g);
    }
  }
}     

