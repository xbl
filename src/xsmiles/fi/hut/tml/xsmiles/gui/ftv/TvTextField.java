/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/**
 * A lightwieght TextField for digital tv environment.
 * Methods resemble the methods of 
 *
 * @author Juha Vierinen
 * @version 0.1
 */
package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;

public class TvTextField extends Container implements FocusListener {

    protected String contents;
    private Color background;
    private Color foreground;
    private int FONTSIZE=21;
    private String FONT="Tiresias";
    private Double CHARLENGTH;
    private int width;
    private int height;
    private int length;
    private int LINEWIDTH=4;
    private TListener kListener;
    private ActionListener listener;
    private Painter painter;
    
    /**
     * Plain vanilla constructor with nothing special
     */
    public TvTextField(String str) {
	this.length=100;
	this.contents=str;
	init();
    }
    
    /** 
     * Contructor to set textfield length 
     *
     * @param length
     */
    public TvTextField(int length) {
	this.length=length;
    }
    
    /**
     * Plain vanilla init()
     *
     * - Sets the approximate length of text
     * - Sets the initial width & height of the component
     * - Creates a focusListener of a inner class and adds it 
     *   to this component.
     */
    private void init() {
	CHARLENGTH=new Double(1.2*FONTSIZE);
	width=20;
	height=(int)getSize().height;
	addFocusListener(this);
	kListener=new TListener(this);
	addKeyListener(kListener);
	painter=new Painter();
	add(painter);
    }
    
    public void setSize(int width, int height) {
	super.setSize(width, height);
	this.width=width;
	this.height=height;
	painter.setSize(width,height);
    }
    
    /**
     * Set the text in the textField to something
     *
     * @param text The text to be set
     */
    public void setText(String text) {
	this.contents=text;
    Log.debug("TvTextField: " + contents);
    repaint();
    }
    
    /**
     * fire a actionevent
     * 
     * @param str the actionevent command
     */
    public void actionPerformed(String str) {
	if(!(listener==null))
	    listener.actionPerformed(new ActionEvent(this, 1, str));
      //     Log.debug("TvTextField: actionPerformed");
    }
    
    /**
     * Fetches the text from the textfield
     * 
     * @return The text
     */
    public String getText() {
	return(this.contents);
    }
   
    public void addActionListener(ActionListener listener) {
	this.listener=listener;
    }
    
    private class TListener extends KeyAdapter 
    {
        private TvTextField c;
        private boolean ucase=false;
        private String oldStr;
    
     public TListener(TvTextField c) {
         this.c=c;
     }

     public void keyTyped(KeyEvent event)
     {
         char keyChar = event.getKeyChar();
         oldStr=c.getText();
         if (!Character.isISOControl(keyChar))     
             c.setText(oldStr + String.valueOf(keyChar));
             event.consume();   
     }
     
     public void keyPressed(KeyEvent e) {
	    String str;
	    String oldStr=c.getText();
            
        str=e.getKeyText(e.getKeyCode());
        oldStr=c.getText();
       	
        if(str.length()==1)
            return;
       
        if(str.equals("Backspace")) {
           Log.debug("TvTextField: Fukking backspace");
           c.setText(c.getText().substring(0,c.getText().length()-1));
        }
        if(str.equals("Caps Lock"))
          ucase=!ucase;
        if(str.equals("Enter"))
          c.actionPerformed(c.getText());
        if(str.equals("F1"))
          c.actionPerformed("red");
        if(str.equals("F2"))
          c.actionPerformed("green");
        if(str.equals("F3"))
          c.actionPerformed("yellow");
        if(str.equals("F4"))
          c.actionPerformed("blue");
     }
    }
    
   
    
    
    /**
     * This component is focusable
     */
    public boolean isFocusTraversable() {
	return true;
    }
    
    /**
     * This component is Lightweight
     */
    public boolean isLightWeight() {
	return true;
    }
    
    /**
     * When focus is gained, then repaint the component
     * because there is stuff to draw
     */ 
    public void focusGained(FocusEvent e) {
	painter.focusGained();
	repaint();
    }
    
    /**
     * Jesus said, uh, what did he say...  
     * @param e the pramaaaaaaresrfgfljk
     */
    public void focusLost(FocusEvent e) {
	painter.focusLost();
	repaint();
    }
    
    private class Painter extends Component {
	private boolean focus;
	
	private int h;	
	private int w;
	
	public void focusLost() {
	    
	    focus=false;
	}
	
	public void focusGained() {
	    focus=true;
	}
	
	public void paint(Graphics g0) {
	    //Graphics2D g=(Graphics2D) g0;
	    Graphics g = g0;
		h = (int)this.getSize().height;
		//height = (int)this.getSize().height;
	    //width = (int)this.getSize().width;
	    w = (int)this.getSize().width;
		g.setFont(new Font(FONT, Font.BOLD, FONTSIZE));
	    //g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    if(focus)
		g.setColor(new Color(0,130,0));
	    else
		g.setColor(Color.lightGray);
	    
	    g.fillRect(1,1,w-1, h-1);
	    
	    g.setColor(Color.black);
	    for(int i=0;i<LINEWIDTH;i++)
		g.drawRect(i,i,w-i-i, h-i-i);
	    
	    if(focus)
		g.setColor(Color.white);
	    else
		g.setColor(Color.black);
	    g.drawString(contents, 13, FONTSIZE+8);
	    
	    // The caret.. fuck the blinking caret for now..
	}
	/**
	 * This component is focusable
	 */
	public boolean isFocusTraversable() {
	    return true;
	}
	
	/**
	 * This component is Lightweight
	 */
	public boolean isLightWeight() {
	    return true;
	}
    }
}
