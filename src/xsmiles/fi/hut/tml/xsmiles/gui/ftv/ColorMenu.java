/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
import java.util.Vector;
import java.awt.event.*;

/**
 * Lightweight menu-component for a menu
 * FTV-XML-Browser
 * The component extends DoubleBuffered container to prevent flickering. The component
 * itself is drawn by a inner class that overrides the paint-method
 * 
 * Note: Figure out if there is any idea in this
 *
 * @author Juha Vierinen
 * @version 0.1 
 */
public class ColorMenu extends Container implements FakeRemoteListener {
    // Due to The incredibly mystical and otherwise so beautiful Lightweight 
    // Framework, I'll just stick to the heavyweight components.. 
    // This should extend DoubleBufferedContainer
    private Color color;
    private Vector items; // items in vector
    private String lista=""; // items in a String (for paint())
    private int size;
    private String selectedStr;
    private int width, height;
    private static int FONTSIZE = 20;
    private static int SPACING = 30;
    private Double CHARLENGTH;
    private String FONT="Tiresias";
    private int selected=0;
    private MenuKeyListener keyListener;
    private Painter painter;
    //    private DoubleBufferdPanel buffer;
    
    /**
     * Creates a menu that can be launched by default with
     * the red button.
     */
    public ColorMenu() {
	this.color=Color.red;
	init();
    }
    
    public ColorMenu(Color color) {
	this.color=color;
	init();
    }

  /**
   * @param c mainframe for fake event
   * @param i the keycode of fake remote control
   */
  public void keyPressed(Component c,  int i) {
    keyListener.keyPressed(new KeyEvent(this,0,0,0,i));
  }

  public void addKL() {
    addKeyListener(keyListener);
  }
  

  public void removeKL() {
    removeKeyListener(keyListener);
  }
    
    private void init() {
	CHARLENGTH= new Double(0.75*FONTSIZE);
	items=new Vector();
	width=100;
	selected=0;
	keyListener = new MenuKeyListener();
	requestFocus();
	painter=new Painter();
	add(painter);
    }

  
    public void setSize(int width, int height) {
	super.setSize(width, height);
	this.width=width;
	this.height=height;
    painter.setSize(width,height);
    }
    
    /**
     * Add an item to list
     */
    public void add(String item) {
	items.addElement(item);
	size=items.size();
	lista=lista + item + "\n";
	
	// The length according to the longest word
	if (item.length()*CHARLENGTH.intValue()>width)
	    width=item.length()*CHARLENGTH.intValue();
	height=(size+1)*SPACING+28;
	painter.setSize(width, height);
	setSize(width, height);
    }
    
 

    /**
     * Gets the selected item
     *
     * @return The selected item
     */
    public String getSelected() {
	return (String)items.elementAt(selected);
    }

    /**
     * Move selection head upwards
     */
    public void moveUp() {
	if (selected>0) 
	  selected--;
	repaint();
    }
    
    /**
     * Move selectionhead downwards
     */
    public void moveDown() {
	if (selected<size-1)
	    selected++;
	repaint();
    }

    /**
     * Because we have coupled a keylistener with the menu
     * to do all listening and event handling we must therefore
     * pass ActionListener forward to the keyListener;
     */
    public void addActionListener(ActionListener listener) {
	keyListener.addActionListener(listener);
    }
    
    /**
     * Tell layoutmanagaer what the size of this component should be
     *
     *
     */
    public Dimension getPreferredSize() {
	return(new Dimension(width, height));
    }    
    
    public boolean isFocusTraversable() {
	return true;
    }
    
    // public boolean isLightWeight() {
    //	return true;
    //}
    
    public String getColor() {
	return color.toString().toLowerCase();
    }
    
    private class MenuKeyListener extends KeyAdapter {
	
	private ColorMenu referer;
	private ActionListener listener;
	
	/** 
	 * A standard method for processing incoming keyboard events.
	 * Some are sent forward to an actionListener assosiated w 
	 * the ColorMenu
	 *
	 * @param e The keyevent
	 */
	public void keyPressed(KeyEvent e) {

	  //Log.debug(e.getKeyCode());
	  referer=(ColorMenu) e.getComponent();
	  if(e.getKeyCode()==KeyEvent.VK_UP)
	    referer.moveUp();
	  else if (e.getKeyCode()==KeyEvent.VK_DOWN)
	    referer.moveDown();
	  else if (e.getKeyCode()==KeyEvent.VK_ENTER) 
	    processActionEvent(referer.getSelected());
	  else if(e.getKeyCode()==KeyEvent.VK_F1)
	    processActionEvent("red");
	  else if(e.getKeyCode()==KeyEvent.VK_F2)
	    processActionEvent("green");
	  else if(e.getKeyCode()==KeyEvent.VK_F3)
	    processActionEvent("yellow");
	  else if(e.getKeyCode()==KeyEvent.VK_F4)
	    processActionEvent("blue");
	  e.consume();
	}
      
     
	/**
	 * This is for sending actionEvents to actionlistener 
	 * associated with this ColorMenu.
	 *
	 * @str The command to bundle with actionEvent
	 */
      public void processActionEvent(String str) {
	if(!(listener==null))
	  listener.actionPerformed(new ActionEvent(this, 1, referer.getSelected()));
	// Log.debug("Sending to ActionListener: " + referer.getSelected());
      }
	
	/** 
	 * Adds an actionListener
	 *
	 * @param listener The ActionListener
	 */
	public void addActionListener(ActionListener listener) {
	    this.listener=listener;
	}
    }
    
    private class Painter extends Component {
	/**
	 * Here we do some fancy drawing 
	 * using the java 2d api. 
	 * When we want 1.1 compatibility then when downgrade to normal graphics
	 * 
	 * @param g0 The graphics that is passed to the component by the system
	 */

	private int w;
	private int h;



	public void paint(Graphics g0) {
	    //super.paint(g0);
	    int y;
//	    Graphics2D g;
//	    g = (Graphics2D) g0;
//	    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	    Graphics g;
	    g = g0;

	    g.setFont(new Font(FONT, Font.BOLD, FONTSIZE));
	    
		w = (int)this.getSize().width;
		h =  (int)this.getSize().height;
		
	    // Frame & background
	    g.setColor(Color.black);
	    g.fillRect(0, 20, w, h);
	    g.setColor(new Color(50,50,50));
	    g.fillRect(4,24,w-8, h-28 ); 
	    g.setColor(Color.black);
	    g.fillOval(50,0,40,40);
	    
	    g.setColor(color);
	    
	    g.fillOval(54,4,32,32);
	    
	    // Highlight
	    g.setColor(new Color(0,130,0));
	    g.fillRect(4, (selected*SPACING)+48, w-8, 34);
	    
	    // Text
	    g.setColor(Color.white);
	    for(y=0;y<size;y++) {
		String tmp = (String) items.elementAt(y);
		g.drawString(tmp, 10, y*SPACING + 72);
	    }
	} 
	
	/**
	 * Tell layoutmanagaer what the size of this applet should be
	 *
	 *
	 */
	public Dimension getPreferredSize() {
	    //return(new Dimension(width, height));
		return this.getSize();
	}    
	
	public boolean isFocusTraversable() {
	    return true;
	}
	
	public boolean isLightWeight() {
	    return true;
	}
    }
}     


