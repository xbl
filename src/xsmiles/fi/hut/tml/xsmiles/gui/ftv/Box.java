/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/**
 * A simple lightweight component called the box.
 * It only draws a box. It does not even have container properties.
 */
package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
public class Box extends Container {
    private int width, height, linewidth;
    
    /**
     * Contructs the object that draws a box
     *
     * @param linewidth The width of the surrounding line
     */
    public Box(int linewidth) {
	this.linewidth=linewidth;
    }

    /**
     * Override the paint to get some fancier output
     *
     * @param g1 The graphics
     */
     
    public void paint(Graphics g1) {
	//super.paint(g1);
	//Graphics2D g = (Graphics2D) g1;
	Graphics g = g1;
	height=(int)getSize().height;
	width=(int)getSize().width;
	//g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	for(int i=0;i<linewidth;i++)
	    g.drawRect(i,i,width-i-i, height-i-i);
    }
    
    /**
     * This seems to work, I'm not sure if this is needed. 
     * Have to do some testing
     *
     * @return The Dimension that this component consists of.
     */
    public Dimension getPreferredSize() {
	return(new Dimension(width, height));
    }
    
    /**
     * Tell awt not to focus on this component
     *
     * @return false, becauase label is not focus-traversable
     */
    public boolean isFocusTraversable() {
	return false;
    }
    
    /**
     * Yes 
     *
     * @return yes
     */
    public boolean isLightWeight() {
	return true;
    }
    
    /**
     * Wonder if this helps at all
     *  
     * @return yesyesyes
     */
    public boolean isOpague() {
	return true;
    }
}
