/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;

import java.awt.event.*;
import fi.hut.tml.xsmiles.gui.*;
import fi.hut.tml.xsmiles.event.GUIEvent;
import java.awt.*;
import fi.hut.tml.xsmiles.EventBroker;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.Log;


/**
 * GuiHandler is the main KeyEvent handler for the gui. It is 
 * activated in the normal state, (no menus open & no textfield activated)
 * 
 * @author Juha Vierinen
 * @version 0.1
 */
public class GuiHandler extends KeyAdapter implements FakeRemoteListener {
    
  private FtvGUI gui;
  private static boolean debug = false;
  private boolean buffer=false;
    
  /** 
   * We need the gui for operating with gui widgets from withing GuiHandller
   * 
   * @param gui The gui
   */
  public GuiHandler(FtvGUI gui) {
    this.gui=gui;
  }

  /**
   * When a key is pressed, we check what we should do in the normal state of the browser. 
   * Move highlight up or down, launch menus, follow a link or set textfield active.
   * NOTE: The remotecontrol is simulated by keyboard keys in this version. Refer to the MHP
   * specification for info on remote control key-events.
   * 
   * @param e info abt the event
   */
  public void keyPressed(KeyEvent e) {
	
    switch(e.getKeyCode()) { 
    case KeyEvent.VK_F1:                            
      // Red button: Set focus to TextField
      gui.setColorMenu("red", true);
      break;
    case KeyEvent.VK_F2:                            
      gui.setColorMenu("yellow", true);
      break;
    case KeyEvent.VK_F3:                            
      // Yellow button: navigate home
      gui.navigateHomeEvent();
      break;
    case KeyEvent.VK_F4:                            
      // Blue button: Exit program
      gui.removeComponents();
      gui.browser.closeBrowserWindow();
      break;
    case KeyEvent.VK_F5:                            
      // Blue button: Exit program
      gui.changeGui();
      break;
    case KeyEvent.VK_LEFT:
      gui.navigateBack();
      break;
    case KeyEvent.VK_RIGHT:
      gui.navigateForward();
      break;
    case KeyEvent.VK_UP:
      // Move highlight up in browser window
      //gui.issueGUIActionEvent(new GUIEvent(gui, GUIEvent.moveActiveLinkUp));
      break;
    case KeyEvent.VK_DOWN:
      // Move highlight down in browser window
      // This is delegated to the browser, because GUI doesn't do 
      // rendering
      // gui.issueGUIActionEvent(new GUIEvent(gui, GUIEvent.moveActiveLinkDown));
      break;
    case KeyEvent.VK_ENTER:
      if(gui.state==FtvGUI.NORMAL)
	// Move highlight down in browser window
	//gui.issueGUIActionEvent(new GUIEvent(gui, GUIEvent.followActiveLink));
      break;
    }
    e.consume();
  }

  public void keyPressed(Component c,int i) {
    keyPressed(new KeyEvent(c , 0, 0, 0, i));
  }
  
  private void debug(String str) {
    if(debug)
      Log.debug(str+"\n");
  }
}
