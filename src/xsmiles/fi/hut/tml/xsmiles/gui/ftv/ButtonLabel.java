/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/**
 * The ButtonLabel is a DigitalTv specific Lightweight component 
 * which consists of a "Colorbutton" and a testlabel indicating what it does.
 * 
 * @author Juha Vierinen
 * @version 0.1
 */
package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
import java.awt.event.*;


public class ButtonLabel extends Container {
    
    private String label;
    private Color color;
    private ActionListener listener;
    // The font to use in this component 
    private String FONT="Tiresias";
    // The radius of the graphical "button
    private int FONTSIZE=20;
    private static int RADIUS=15;

    private Double CHARLENGTH;
    private int width, height;
    private boolean focusable=false;
    
    /** 
     * The constructor sets the label and color of the "colorbuttonlabel"
     *
     * @param label The label of this component
     * @param color The Color of the button on front of this label
     * @param key The key to be associated with this Menu (which key fires actionEvent)
     */
    public ButtonLabel(String label, Color color) {
	this.label=label;
	this.color=color;
	CHARLENGTH=new Double(1.2*FONTSIZE);
	width=label.length()*CHARLENGTH.intValue()+30;
	height=RADIUS*2+4;
    } 
    
    /**
     * Needed by awt rendering.. 
     * 
     * @return a Dimension indicating borders of component
     */
    public Dimension getPreferredSize() {
	return(new Dimension(width,height));
    }
    
    /**
     * We override the paint method to make the lightweight component
     * Again, note that we are using Graphics2D instead of Graphics
     * to achieve nice rendering for graphics
     * 
     * @param the graphics
     */
    public void paint(Graphics g1) {
	//Graphics2D g = (Graphics2D) g1;
	Graphics g = g1;
	//g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	g.setFont(new Font(FONT, Font.BOLD, FONTSIZE));	
	g.setColor(Color.black);
	g.fillOval(0,0, RADIUS*2, RADIUS*2);
	g.setColor(color);
	g.fillOval(4,4, RADIUS*2-8, RADIUS*2-8);
	//Log.debug(label + "\n");
	g.setColor(Color.white);
	g.drawString(label, RADIUS*2 + 10, (int) (RADIUS-(FONTSIZE/2)+FONTSIZE)-3);
    }
    
    /**
     * Tell awt not to focus on this component
     *
     * @return false, becauase label is not focus-traversable
     */
    public boolean isFocusTraversable() {
	return focusable;
    }
    
    /**
     * Yes 
     *
     * @return yes
     */
    public boolean isLightWeight() {
	return true;
    }
    
    /**
     * Wonder if this helps at all
     *  
     * @return yesyesyes
     */
    public boolean isOpaque() {
	return false;
    }
    
    /**
     * Sets the button focusable
     */
    public void setFocusable(boolean focusable) {
	this.focusable=true;
    }
}
