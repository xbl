/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
import fi.hut.tml.xsmiles.Log;


/**
 * An arrow BACK or FORWARD indicating which key (which functionality) 
 * is pressed. 
 *
 * @author Juha Vierinen
 * @version 0.1 
 */
public class Arrow extends Container {
    // Note: Because of the awsom Lightweight Framework
    // We use HEAVYweight panels..
    //
    private String direction;
    public int[] x=new int[4];
    public int[] y=new int[4];
    private int np;
    private int height=200;
    private int width=160;
    private Painter painter;
    
    /**
     * Creates the points for the polygon
     * 
     * @param direction The direction of the arrow. "BACK" or "FORWARD"
     */
    public Arrow(String direction) {
	this.width=160;
	this.height=200;
	this.direction=direction;
	if(direction.equals("FORWARD")) {
	    x[0]=0;
	    y[0]=0;
	    x[1]=width;
	    y[1]=(int)height/2;
	    x[2]=0;
	    y[2]=height;
	} else {
	    //Log.debug("tanne4");
	    x[0]=width;
	    y[0]=0;
	    x[1]=width;
	    y[1]=height;
	    x[2]=0;
	    y[2]=(int)height/2;
	}
	requestFocus();
	setSize(width+1,height);
	painter=new Painter(this);
	painter.setSize(width+1,height);
	add(painter);
    }    
    

    /**
     * @return Is component is focusable
     */
    public boolean isFocusTraversable() {
	return true;
    }
    
    /**
     * @return Is component is Lightweight
     */
    public boolean isLightWeight() {
	return true;
    }
    
    /**
     * @return The dimension of component
     */
    public Dimension getPreferredSize() {
	return(new Dimension(this.width, this.height));
    }   

    private class Painter extends Component {
	/**
	 * Overrides paint() as a good lightweight should
	 * 
	 * @param g Graphics
	 */
	 
	 int[] y;
	 int[] x;

	 
	 public Painter(Arrow arrow) 
	 {
	 	super();
		x = arrow.x;
		y = arrow.y;
	 }
	 
	public void paint(Graphics g0) {
	    //super.paint(g0);
//	    Graphics2D g=(Graphics2D)g0;
//	    g = (Graphics2D) g0;
	    Graphics g= g0;
	    //g = (Graphics2D) g0;
	    //g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g.setColor(Color.green);

	    g.fillPolygon(x, y, 3);
	    g.setColor(Color.black);
	    g.drawPolygon(x, y, 3);
	}    
	
	/**
	 * This component is focusable
	 */
	public boolean isFocusTraversable() {
	    return true;
	}
	
	/**
	 * This component is Lightweight
	 */
	public boolean isLightWeight() {
	    return true;
	}
    }
}
