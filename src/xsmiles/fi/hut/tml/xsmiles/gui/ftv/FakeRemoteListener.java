package fi.hut.tml.xsmiles.gui.ftv;
/**
 * Interface for listeners, which can take keyPressed(Component c, int i)
 * @author juha
 */
import java.awt.event.KeyEvent;
import java.awt.Component;

public interface FakeRemoteListener {
  public void keyPressed(Component c, int code); 
}
