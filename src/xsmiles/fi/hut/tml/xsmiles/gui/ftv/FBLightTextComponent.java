/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;

//import fbform.FBDebug;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
//import java.awt.geom.Rectangle2D;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FBLightTextComponent
    extends Component
    implements KeyListener, FocusListener
{

    protected Insets borderInsets;
    protected int caretPos;
    
    // whether the component currently has the focus
    private transient boolean haveFocus;
    
    // the component name that's displayed at the top of the component's area
    private String name;
    
    // The text the user has entered. The term "committed text"
    // follows the usage in the input method framework.
    private StringBuffer committedText = new StringBuffer();

    // members that determine where the text is drawn
    private static final int LINE_OFFSET = 8;
    private int textOriginX;
    private int nameOriginY;
    private int textOriginY;

    private int charsWidth;
    
    /**
     * Constructs a LWTextComponent.
     * @param name the component name to be displayed above the text
     * @param enableInputMethods whether to enable input methods for this component
     */
    public FBLightTextComponent(String name, boolean enableInputMethodss)
    {
        super();
        this.name = name;
	charsWidth = 20;
	setSize(150, 40);
	// we have to set the foreground color because otherwise
	// text may not display correctly when we use an input
	// method highlight that swaps background and foreground
	// colors
	setForeground(Color.black);
	setBackground(Color.white);
	setEnabled(true);
	addKeyListener(this);
	addFocusListener(this);
	addMouseListener(new MouseFocusListener(this));
	//enableInputMethods(enableInputMethodss);
	caretPos = 0;
	borderInsets = new Insets(2,2,2,2);
    }

    public void setCharsWidth(int newWidth)
    {
	charsWidth = newWidth;
    }
    
    /*
      public void setFontSize(int size) {
      setFont(new Font("Dialog", Font.PLAIN, size));
      //nameOriginY = LINE_OFFSET + size;
      textOriginX = 10;
      textOriginY = 2 * (LINE_OFFSET + size);
      }
    */
    
    //////////////////////////////////////////////////
    // Component overrides
    //////////////////////////////////////////////////

    public boolean isFocusTraversable()
    {
	return true;
    }
    
    public synchronized void paint(Graphics g)
    {

        // draw the background
	g.setColor(getBackground());
	Dimension size = getSize();
	g.fillRect(0, 0, size.width, size.height);
	
	// draw the frame, thicker if the component has the focus
	g.setColor(Color.black);
	g.drawRect(0, 0, size.width - 1, size.height - 1);
	if (haveFocus) {
	    g.drawRect(1, 1, size.width - 3, size.height - 3);
	}

	g.setColor(getForeground());

	FontMetrics metrics = g.getFontMetrics();

	textOriginX = borderInsets.left;
	textOriginY = borderInsets.top + metrics.getAscent();
	
	g.drawString(committedText.toString(), textOriginX, textOriginY); 
        
        // draw the caret, if the component has the focus
	Rectangle rectangle = getCaretRectangle();
	if (haveFocus && rectangle != null) {
	    g.setXORMode(getBackground());
            g.fillRect(rectangle.x, rectangle.y, 1, rectangle.height);
	    g.setPaintMode();
        }
    }

    public Dimension getPreferredSize()
    {
        FontMetrics metrics = getGraphics().getFontMetrics();
	int height = metrics.getAscent() + metrics.getDescent() +
	    borderInsets.top + borderInsets.bottom;
	int width = charsWidth * metrics.getMaxAdvance();
	return new Dimension(width,height);
    }

    public int getCaretPos()
    {
	return caretPos;
    }
    
    public Rectangle getCaretRectangle()
    {
        int caretLocation = getCaretPos();
        FontMetrics metrics = getGraphics().getFontMetrics();
	//int strWidth = metrics.stringWidth(committedText.substring(0, caretLocation));
	int strWidth = metrics.stringWidth(committedText.toString());
	//Rectangle2D rec = metrics.getStringBounds(committedText.substring(0, caretLocation),
	//					getGraphics());
	return new Rectangle(textOriginX + strWidth,
                             textOriginY - metrics.getAscent() + 1,
                             0, metrics.getAscent() + metrics.getDescent() - 2);
    }

    public String getText()
    {
	return new String(committedText);
    }


    /**
     * Returns the origin of the text. This is the leftmost point
     * on the baseline of the text.
     * @return the origin of the text
     */
    public Point getTextOrigin()
    {
        return new Point(textOriginX, textOriginY);
    }
    
    /**
     * Inserts the given character at the end of the text.
     * @param c the character to be inserted
     */
    public void insertCharacter(char c)
    {
        committedText.insert(caretPos, c);
	caretPos++;
	repaint();
    }

    public void deleteBackwardCharacter()
    {
	int len = committedText.length();
	if (len > 0 && caretPos > 0) {
	    //committedText.deleteCharAt(caretPos - 1);
	    caretPos--;
	    repaint();
	}
    }

    public void deleteForwardCharacter()
    {
	int len = committedText.length();
	if (len > 0 && caretPos < len) {
	    //committedText.deleteCharAt(caretPos);
	    repaint();
	}
    }

    public void moveCaretLeft()
    {
	if (caretPos > 0) {
	    caretPos--;
	    repaint();
	}
    }

    public void moveCaretRight()
    {
	int len = committedText.length();
	if (caretPos < len) {
	    caretPos++;
	    repaint();
	}
    }

    public void moveCaretTo(int pos)
    {
	if (caretPos == pos)
	    return;
	int len = committedText.length();
	if (pos < 0 || pos > len) {
//	    FBDebug.error("Invalid caret position specified: " + pos);
	    return;
	}
	caretPos = pos;
	repaint();
    }
    
    /////////////////////////////////////////////////////////
    // KeyListener implementation
    /////////////////////////////////////////////////////////
    
    /**
     * Handles the key typed event. If the character is backspace,
     * the last character is removed from the text that the user
     * has entered. Otherwise, the character is appended to the text.
     * Then, the text is redrawn.
     * @event the key event to be handled
     */
    public void keyTyped(KeyEvent event)
    {
	char keyChar = event.getKeyChar();
	switch (keyChar) {
	    /*	case '\b':
		deleteBackwardCharacter();
		event.consume();
		break;
		//	case '\n':
		// do nothing
		//break;
		case '\d':
		deleteForwardCharacter();
		event.consume();
		break;*/
	default:
	    if (!Character.isISOControl(keyChar)) {
		insertCharacter(keyChar);
		event.consume();
	    }
	    else {
//		FBDebug.println("ISO control in FBLightTextComponent.");
        }
	}
    }

    public void keyPressed(KeyEvent event)
    {
	int keyCode = event.getKeyCode();
	switch (keyCode) {
	case KeyEvent.VK_LEFT:
	    moveCaretLeft();
	    event.consume();
	    break;
	case KeyEvent.VK_RIGHT:
	    moveCaretRight();
	    event.consume();
	    break;
	case KeyEvent.VK_BACK_SPACE:
	    deleteBackwardCharacter();
	    event.consume();
	    break;
	case KeyEvent.VK_DELETE:
	    deleteForwardCharacter();
	    event.consume();
	    break;
	case KeyEvent.VK_HOME:
	    moveCaretTo(0);
	    event.consume();
	    break;
	case KeyEvent.VK_END:
	    moveCaretTo(committedText.length());
	    event.consume();
	    break;
	}
    }

    /** Ignores key released events. */
    public void keyReleased(KeyEvent event)
    {
    }
    
    /** Turns on drawing of the component's thicker frame and the caret. */
    public void focusGained(FocusEvent event) {
        haveFocus = true;
	repaint();
    }
    
    /** Turns off drawing of the component's thicker frame and the caret. */
    public void focusLost(FocusEvent event) {
        haveFocus = false;
	repaint();
    }    

}

class MouseFocusListener extends MouseAdapter {

    private Component target;
    
    MouseFocusListener(Component target) {
        this.target = target;
    }

    public void mouseClicked(MouseEvent e) {
	//FBDebug.println("Mouse clicked");
	target.requestFocus();
    } 

}
