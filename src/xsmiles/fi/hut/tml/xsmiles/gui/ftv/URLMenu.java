/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.*;
import java.awt.event.*;

/**
 * Open an URL menu. It contains a Cancel and a TextField.
 * 
 * @author Juha Vierinen
 * @version 0.1
 */
public class URLMenu extends Component {
    private String color;
    private String selectedStr;
    private int width, height;
    private static int FONTSIZE = 20;
    private static int SPACING = 30;
    private Double CHARLENGTH;
    private String FONT="Tiresias";
    private int selected=0;


    /**
     * Nothing special
     */
    public URLMenu() {
	init();
    }
    
    /** 
     * Initialize component
     */
    private void init() {
	color="red";
    }
    
    public void paint(Graphics g0) {
	int y;

	Graphics g;
	g = g0;
	
	g.setFont(new Font(FONT, Font.BOLD, FONTSIZE));
	
	// Frame & background
	g.setColor(Color.black);
	g.fillRect(0, 20, width, height);
	g.setColor(new Color(50,50,50));
	g.fillRect(4,24,width-8, height-28 ); 
	g.setColor(Color.black);
	g.fillOval(50,0,40,40);
	if (this.color.equals("red"))
	    g.setColor(Color.red);
	else 
	    g.setColor(Color.yellow);
	g.fillOval(54,4,32,32);

	// Highlight
	g.setColor(new Color(0,130,0));
	g.fillRect(4, (selected*SPACING)+48, width-8, 34);

	// Text
	g.setColor(Color.white);
	g.drawString("Under Construction", 10, 72);
    
	
	super.paint((Graphics) g);
    }
    
    public Dimension getPreferredSize() {
	return new Dimension(width, height);
    }
}
