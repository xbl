/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.ftv;
import java.awt.event.*;
import java.awt.*;
import fi.hut.tml.xsmiles.Log;

/**
 * This handler handles events from ColorMenus. At the moment it a klugy tailored 
 * handler for only the red & yellow menu. Have to come up with something more elegant.
 * 
 * @author Juha Vierinen
 * @version 0.1
 */
public class MenuHandler implements ActionListener {
    private FtvGUI gui;
    private String color;
    private boolean debug=true;
    
    /**
     * Kludgy contructor wants the color to know what to do with
     * incoming actionEvents.
     * 
     * @param color The color of the menu.
     */
    public MenuHandler(String color, FtvGUI gui) {
	this.color=color;
	this.gui=gui;
    }
    
    /** 
     * The part that handles incoming events from the menu.
     * 
     * @param e An actionEvent bundled with an String item of the menu fired. 
     */
    public void actionPerformed(ActionEvent e) {
	String temp=e.getActionCommand(); 
	Log.debug("MenuHandler ActionPerformed: " + temp);
	if (color.equals("red")) {
	    if (temp.equals("Cancel")) {
	      gui.setColorMenu("red",false);
	      return; 
	    } else if (temp.equals("Go to URL")) {
		gui.setColorMenu("red", false);
		gui.setURLMenu(true);
		return;
	    } else if (temp.equals("Add to Bookmarks")) {
		gui.setColorMenu("red", false);
		gui.navigateAddtoBookmarks();
		return;
	    } else if (temp.equals("Exit Browser")) {
		gui.setColorMenu("red", false);
		System.exit(0);
	    }
	} else if(color.equals("yellow")) {
	    if (temp.equals("Cancel")) {
		gui.setColorMenu("yellow", false);
		return;
	    } else if (temp.equals("Go to URL")) {
		gui.setColorMenu("yellow", false);
		gui.setURLMenu(true);
		return;
	    } else if (temp.equals("Go to bookmarks")) {
		gui.setColorMenu("yellow", false);
		gui.navigateToBookmarks();
		return;
	    } else if (temp.equals("Add to bookmarks")) {
		gui.setColorMenu("yellow", false);
		gui.navigateAddtoBookmarks();
		return;
	    } else if (temp.equals("Configure browser")) {
		gui.setColorMenu("yellow", false);
		gui.setConfigMenu(true);
		return;
	    } else if (temp.equals("Exit browser")) {
		// Check if there are other exit procedures
		gui.setColorMenu("yellow", false);
		System.exit(0);
		return;
	    }
	} else if(color.equals("config")) {
	    if (temp.equals("Cancel")) {
		gui.setConfigMenu(false);
		return;
	    }
	    // Add the different possibilities of configmenu here!
	} else if(color.equals("text"))
	    if(temp.equals("red")) {
		gui.setColorMenu("red", true);
		return;
	    } else if(temp.equals("green")) {
		gui.setColorMenu("yellow", true);
		return;
	    } else if(temp.equals("yellow")) {
		gui.navigateHomeEvent();
		return;
	    } else if(temp.equals("blue")) {
		System.exit(0);
		return;
	    } else {
		gui.navigateToURL(temp);
		return;
	    }
    }
    
    private void debug(String str) {
	if(debug)
	    Log.debug(str+"\n");
    }
}
