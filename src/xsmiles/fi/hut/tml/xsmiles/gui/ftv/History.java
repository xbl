/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/**
 * A class that represents the page history of the browser
 * 
 * @author Juha Vierinen
 * @version 0.1
 */
package fi.hut.tml.xsmiles.gui.ftv;
import java.util.*;
import java.net.*;

public class History {

    //private LinkedList history;
    private Vector history;
	private int HISTORY_SIZE;
    private int size;
    
    /**
     * Creates a new History object
     * using a Enumeration.
     * 
     */
    public History() {
	this.HISTORY_SIZE = 10;
	this.history = new Vector();
	this.size = 0;
    }
    
    /**
     * The size of the history can also be speicified
     *
     * @param size
     */
    public History(int size) {
	this.HISTORY_SIZE=size;
	this.history = new Vector();
	this.size = 0;
    }
    
    /**
     * Adds an url to history
     *
     * @param url The URL to add to history. (goes on top)
     */
    public void addURL(URL url) {
	if (size<HISTORY_SIZE) { 
	    history.insertElementAt(url, 0);
	    size++;
	} else {
		history.removeElement(history.lastElement());
	    history.insertElementAt(url, 0);
	}
    }
    
    /**
     * Gets the N:th url from the history
     * (For history-menu)
     *
     * @return The URL!
     */
    public URL get(int pos) {
	return (URL) history.elementAt(pos);
    }

    /**
     * The last page visited 
     * (For back-functionality)
     *
     * @return The last visited URL
     */
    public URL getFirst() {
	return (URL) history.elementAt(0);
    }
}
