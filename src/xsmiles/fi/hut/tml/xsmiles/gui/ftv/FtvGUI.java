/* X-Smiles
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE. License can be found in licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles.gui.ftv;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.awt.Animation;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.XAButton;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.gui.swing.XSmilesUI;

/**
 * FtvGUI is a TV GUI for the XSmiles browser.
 * 
 * @author Juha Vierinen
 */
public class FtvGUI extends XSmilesUI {

    private ColorMenu redMenu, yellowMenu;
    private ButtonLabel buttons, green, yellow, blue;
    private Arrow back, forward;
    private Box mainBox, leftBox, rightBox;
    private Container componentContainer;
    private Container presentationPanel;
    private JScrollPane pane;
    private JPanel kontti;
    private TvTextField statusBar;
    private URLMenu urlMenu; // For opening a specific URL
    private ConfigMenu configMenu; // configuration menu
    private Animation animation; // the animated loading indicator
    private Thread animator;
    private FakeRemoteListener keyBoardFocus;

    private History history;
    protected BrowserWindow browser;
    private LayoutManager bulletin;
    private JFrame deviceFrame;
    private JRootPane rp;
    private Container devicePane;
    private JFrame mainFrame;
    private int width, height;
    private int scrollPolicy = java.awt.ScrollPane.SCROLLBARS_AS_NEEDED; // see java.awt.ScollPane for values
    private boolean debug = false;
    private static String title = "Future-tv xml browser";
    private MenuHandler redHandler, yellowHandler, configHandler, textHandler;
    private GuiHandler guiHandler;
    private FocusGrabber grabber;
    private MLFCControls mlfcControls;
    protected boolean backEnabled = false;
    protected boolean forwardEnabled = false;
    protected int state = NORMAL;
    protected final static int NORMAL = 0;
    protected final static int RED = 1;
    protected final static int GREEN = 2;
    protected final static int TEXT = 3;
    private BrowserConstraints constraints;
    private boolean covers = true;
    private Vector butts;

    private XAButton devicePicture, onoff, up, down, left, right, ok, b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, gb, rb, yb, bb;
    private ComponentFactory componentFactory;

    /**
     * Initial gui contructor
     * 
     * @param b The Browser
     */
    public FtvGUI(BrowserWindow b) {
        super(b, null);
        presentationPanel = browser.getContentArea();
        presentationPanel.setLayout(new BorderLayout());
        presentationPanel.setBackground(Color.lightGray);
        init(b, 810, 670);
    }

    /**
     * For switching gui on the fly
     * 
     * @param b The browser
     * @param c the browser container of previous gui
     */
    public FtvGUI(BrowserWindow b, Container c) {
        super(b, c);
        if (c != null)
            presentationPanel = c;
        else
            presentationPanel = b.getContentArea();
        init(b, 810, 670);
    }

    private void init(BrowserWindow b, int x, int y) {
        setInitialValues(b, x, y);
        createGraphicalElements();
        createListeners();
        addListeners();

    }

    public ContentHandlerFactory getContentHandlerFactory() {
        if (contentHandlerFactory == null)
            contentHandlerFactory = new SwingContentHandlerFactory(browser);
        return contentHandlerFactory;
    }

    /**
     * Sets variable to their initial values
     */
    private void setInitialValues(BrowserWindow b, int x, int y) {
        this.width = x;
        this.height = y;
        this.browser = b;
        //constraints=new BrowserConstraints("digitv");
        //constraints.setTitle("digitv");
    }

    public Window getWindow() {
        return (mainFrame);
    }

    public BrowserConstraints getBrowserConstraints() {
        return constraints;
    }

    /*
     * The component structure is mainFrame -kontti --leftBox --rightBox --buttons --animation --pane (scrollpane) ---presentationPanel
     */

    /**
     * Creates Elements
     *  
     */
    private void createGraphicalElements() {
        mainFrame = new JFrame(title); // Different Frame size
        mainFrame.getContentPane().setLayout(new BorderLayout());

        if (browser.getBrowserConfigurer().getProperty("gui/virtualproto").equalsIgnoreCase("true"))
            covers = true;
        else
            covers = false;

        if (covers) {
            deviceFrame = new JFrame("Digital TV -- Virtual Device");
            devicePicture = new XAButton("./img/ftv/tv.jpg");
            up = new XAButton("./img/ftv/ny.gif", "./img/ftv/nyh.gif", "./img/ftv/ny.gif");
            down = new XAButton("./img/ftv/na.gif", "./img/ftv/nah.gif", "./img/ftv/na.gif");
            left = new XAButton("./img/ftv/nv.gif", "./img/ftv/nvh.gif", "./img/ftv/nv.gif");
            right = new XAButton("./img/ftv/no.gif", "./img/ftv/noh.gif", "./img/ftv/no.gif");
            onoff = new XAButton("./img/ftv/onoff.gif", "./img/ftv/onoffh.gif", "./img/ftv/onoff.gif");
            ok = new XAButton("./img/ftv/ok.gif", "./img/ftv/okh.gif", "./img/ftv/ok.gif");
            b1 = new XAButton("./img/ftv/1.gif", "./img/ftv/1h.gif", "./img/ftv/1.gif");
            b2 = new XAButton("./img/ftv/2.gif", "./img/ftv/2h.gif", "./img/ftv/2.gif");
            b3 = new XAButton("./img/ftv/3.gif", "./img/ftv/3h.gif", "./img/ftv/3.gif");
            b4 = new XAButton("./img/ftv/4.gif", "./img/ftv/4h.gif", "./img/ftv/4.gif");
            b5 = new XAButton("./img/ftv/5.gif", "./img/ftv/5h.gif", "./img/ftv/5.gif");
            b6 = new XAButton("./img/ftv/6.gif", "./img/ftv/6h.gif", "./img/ftv/6.gif");
            b7 = new XAButton("./img/ftv/7.gif", "./img/ftv/7h.gif", "./img/ftv/7.gif");
            b8 = new XAButton("./img/ftv/8.gif", "./img/ftv/8h.gif", "./img/ftv/8.gif");
            b9 = new XAButton("./img/ftv/9.gif", "./img/ftv/9h.gif", "./img/ftv/9.gif");
            b0 = new XAButton("./img/ftv/0.gif", "./img/ftv/0h.gif", "./img/ftv/0.gif");
            rb = new XAButton("./img/ftv/red.gif", "./img/ftv/redh.gif", "./img/ftv/red.gif");
            gb = new XAButton("./img/ftv/green.gif", "./img/ftv/greenh.gif", "./img/ftv/green.gif");
            yb = new XAButton("./img/ftv/yellow.gif", "./img/ftv/yellowh.gif", "./img/ftv/yellow.gif");
            bb = new XAButton("./img/ftv/blue.gif", "./img/ftv/blueh.gif", "./img/ftv/blue.gif");
            b1.setBounds(876, 465, 22, 20);
            b2.setBounds(905, 463, 24, 19);
            b3.setBounds(935, 462, 23, 20);
            b4.setBounds(873, 488, 23, 19);
            b5.setBounds(905, 486, 23, 19);
            b6.setBounds(937, 486, 23, 20);
            b7.setBounds(873, 511, 23, 20);
            b8.setBounds(906, 509, 22, 20);
            b9.setBounds(938, 509, 22, 19);
            b0.setBounds(905, 532, 23, 20);
            rb.setBounds(865, 568, 29, 15);
            gb.setBounds(899, 566, 31, 16);
            yb.setBounds(935, 565, 30, 18);
            bb.setBounds(969, 564, 30, 19);
            ok.setBounds(915, 391, 35, 22);
            onoff.setBounds(875, 431, 23, 31);
            up.setBounds(910, 366, 42, 25);
            down.setBounds(911, 413, 42, 31);
            left.setBounds(878, 388, 28, 31);
            right.setBounds(958, 387, 28, 31);
            DeviceListener dl = new DeviceListener();

            butts = new Vector();
            butts.addElement(up);
            butts.addElement(down);
            butts.addElement(left);
            butts.addElement(right);
            butts.addElement(ok);
            butts.addElement(onoff);
            butts.addElement(b1);
            butts.addElement(b2);
            butts.addElement(b3);
            butts.addElement(b4);
            butts.addElement(b5);
            butts.addElement(b6);
            butts.addElement(b7);
            butts.addElement(b8);
            butts.addElement(b9);
            butts.addElement(b0);
            butts.addElement(rb);
            butts.addElement(gb);
            butts.addElement(yb);
            butts.addElement(bb);

            Enumeration e = butts.elements();
            while (e.hasMoreElements()) {
                XAButton bu = (XAButton) e.nextElement();
                bu.addActionListener(dl);
                bu.set3D(false);
            }

            onoff.setLabel("onoff");
            up.setLabel("up");
            down.setLabel("down");
            left.setLabel("left");
            right.setLabel("right");
            ok.setLabel("ok");
            b1.setLabel("1");
            b2.setLabel("2");
            b3.setLabel("3");
            b4.setLabel("4");
            b5.setLabel("5");
            b6.setLabel("6");
            b7.setLabel("7");
            b8.setLabel("8");
            b9.setLabel("9");
            b0.setLabel("0");
            rb.setLabel("red");
            gb.setLabel("green");
            yb.setLabel("yellow");
            bb.setLabel("blue");

            devicePane = deviceFrame.getContentPane();
        }

        kontti = new JPanel();
        kontti.setLayout(new BulletinLayout());
        kontti.setPreferredSize(new Dimension(800, 600));
        kontti.setLayout(new BulletinLayout());
        kontti.setBackground(Color.darkGray);

        mainFrame.getContentPane().add(kontti, BorderLayout.CENTER, 0);
        mainFrame.pack();
        mainFrame.requestFocus();
        mainFrame.getContentPane().setBackground(Color.darkGray);
        mainFrame.setResizable(false);

        makeColorMenus();
        makeGuiCrap();

        kontti.setLayout(new BulletinLayout());
        kontti.add(leftBox);
        kontti.add(rightBox);
        kontti.add(buttons);
        kontti.add(animation);

        presentationPanel.setBackground(Color.white);
        presentationPanel.setSize(new Dimension(800, 576));

        this.setPresentationPanelSize();
        kontti.add(presentationPanel, 0);
        kontti.add(statusBar);
        kontti.add(yellow);
        kontti.add(blue);
        kontti.add(green);
        kontti.add(mainBox);
        
        setStatusText("Browser ready.");

        if (!covers) {
            mainFrame.setVisible(true);
            mainFrame.requestFocus();
        } else {
            devicePane.setLayout(new BulletinLayout());
            rp = mainFrame.getRootPane();

            rp.setBounds(45, 46, 800, 600);
            devicePane.add(rp);

            Enumeration e = butts.elements(); //Add all buttons to remote
            while (e.hasMoreElements()) {
                devicePane.add((XAButton) e.nextElement());
            }

            devicePicture.setBounds(0, 0, 1020, 735);
            devicePane.add(devicePicture);
            deviceFrame.pack();
            deviceFrame.setResizable(false);
            deviceFrame.show();
        }
    }

    private void setPresentationPanelSize() {
        presentationPanel.setSize(width - 17, height - 161);
        //presentationPanel.setPreferredSize(new Dimension(width-17, height-161));
        presentationPanel.setLocation(3, 3);
    }

    /**
     * @param str sets the location text
     */
    public void setLocation(String str) {
        setStatusText(str);
    }

    /**
     * Create the menus: red, yellow & config menu and set location.
     *  
     */
    private void makeColorMenus() {
        redMenu = new ColorMenu(Color.red);
        redMenu.add("Cancel");
        redMenu.add("Go to URL");
        redMenu.add("Add to Bookmarks");
        redMenu.add("Exit Browser");
        redMenu.setLocation((int) (width - redMenu.getPreferredSize().getSize().width) / 2,
                (int) ((height - 161) - redMenu.getPreferredSize().getSize().height) / 2);

        yellowMenu = new ColorMenu(Color.green);
        yellowMenu.add("Cancel");
        yellowMenu.add("Go to URL");
        yellowMenu.add("Go to bookmarks");
        yellowMenu.add("Add to bookmarks");
        yellowMenu.add("Configure browser");
        yellowMenu.add("Exit browser");
        yellowMenu.setLocation((int) (width - yellowMenu.getPreferredSize().getSize().width) / 2, (int) ((height - 161) - yellowMenu.getPreferredSize()
                .getSize().height) / 2);

        configMenu = new ConfigMenu();
        configMenu.add("Cancel", "NORMAL");
        configMenu.add("Save Changes", "NORMAL");
        configMenu.add("Easy mode");
        configMenu.add("Enable Buttons");
        configMenu.add("Disable fuckups");
        configMenu.add("Remove flicker");
        configMenu.add("Finish the gui!");
        configMenu.setLocation((int) (width - configMenu.getPreferredSize().getSize().width) / 2, (int) ((height - 161) - configMenu.getPreferredSize()
                .getSize().height) / 2);
    }

    /**
     * Make rest of GUI crap
     *  
     */
    private void makeGuiCrap() {
        statusBar = new TvTextField("");
        statusBar.setSize(716, 46);
        statusBar.setLocation(43, 512);

        green = new ButtonLabel("Menu", Color.green);
        green.setLocation(338, 562);

        yellow = new ButtonLabel("Home", Color.yellow);
        yellow.setLocation(488, 562);

        blue = new ButtonLabel("Exit", Color.blue);
        blue.setLocation(644, 562);

        buttons = new ButtonLabel("", Color.red);
        buttons.setLocation(9, 520);
        buttons.setFocusable(true);

        mainBox = new Box(5);
        mainBox.setSize(800, 600);
        mainBox.setLocation(0, 0);

        leftBox = new Box(4);
        leftBox.setSize(46, 46);
        leftBox.setLocation(1, 512);

        rightBox = new Box(4);
        rightBox.setSize(46, 46);
        rightBox.setLocation(755, 512);

        animation = new Animation(40, 40, 200);
        animation.setLocation(758, 516);
        animator = new Thread(animation);
        animator.start();

        back = new Arrow("BACK");
        back.setSize(back.getPreferredSize());
        back.setLocation(300, 150);
        forward = new Arrow("FORWARD");
        forward.setSize(forward.getPreferredSize());
        forward.setLocation(300, 150);
    }

    /**
     * Create listeners needed in gui
     */
    private void createListeners() {
        redHandler = new MenuHandler("red", this);
        yellowHandler = new MenuHandler("yellow", this);
        configHandler = new MenuHandler("config", this);
        guiHandler = new GuiHandler(this);
        textHandler = new MenuHandler("text", this);
        grabber = new FocusGrabber();
    }

    /**
     * Add listeners to corresponding components
     */
    private void addListeners() {
        if (!covers)
            mainFrame.addKeyListener(guiHandler); // Add a keyListener to Fr
        else
            deviceFrame.addKeyListener(guiHandler); // Add a keyListener to

        yellowMenu.addActionListener(yellowHandler);
        redMenu.addActionListener(redHandler);
        configMenu.addActionListener(configHandler);
        statusBar.addActionListener(textHandler);
        presentationPanel.addFocusListener(grabber);
        keyBoardFocus = guiHandler;
    }

    /**
     * Back
     */
    public void navigateBack() {
        if (backEnabled) {
            browser.navigate(NavigationState.BACK);
            ComponentFlash flash = new ComponentFlash();
            flash.flash(500, back, kontti);
        } else
            setStatusText("Cannot go back");
    }

    /**
     * Forward
     */
    public void navigateForward() {
        if (forwardEnabled) {
            browser.navigate(NavigationState.FORWARD);
            ComponentFlash flash = new ComponentFlash();
            flash.flash(500, forward, kontti);
        } else
            setStatusText("Cannot go forward");
    }

    /**
     * In the tv ui, the reload will be left to the program.
     */
    protected void navigateReload() {
        browser.navigate(NavigationState.RELOAD);
    }

    /**
     * Home will be specified to a colorbutton
     */
    public void navigateHomeEvent() {
        browser.navigate(NavigationState.HOME);
        setStatusText("Going Home");
        buttons.requestFocus();

    }

    /**
     * Navigates to bookmarks
     */
    public void navigateToBookmarks() {
        browser.navigate(NavigationState.HOME);
        setStatusText("Going to My bookmarks.");
    }

    /**
     * Navigates to an URL
     * 
     * @param url The url
     */
    public void navigateToURL(String urlString) {
        URL url = null;
        try {
            url = createWellformedURL(urlString);
            browser.openLocation(url);
        } catch (java.net.MalformedURLException e) {
        }
        browser.openLocation(url);
        setStatusText("loading: " + url);
        buttons.requestFocus();
    }

    // XXX cut'n'paste from LocationCombo.java
    private URL createWellformedURL(String s) throws java.net.MalformedURLException {
        URL url;
        int delim = s.indexOf("/");
        if (delim != -1) {
            url = new URL("http", s.substring(0, delim), s.substring(delim));
        } else {
            url = new URL("http", s, "");
        }
        return url;
    }

    /**
     * Needed by browser for rendering the contents of the page.
     * 
     * @return componentContainer A scrollbarless ScrollPane
     */
    public Container getContentPanel() {
        return presentationPanel;
    }

    /**
     * @param str the text to set in statusbar
     */
    public void setStatusText(String str) {
        statusBar.setText(str);
        Log.debug(presentationPanel.getSize().toString());
        if (presentationPanel != null)
            presentationPanel.setVisible(true);
        presentationPanel.setSize(width - 17, height - 161);
        presentationPanel.setLocation(3, 3);
        presentationPanel.validate();
        presentationPanel.doLayout();
    }

    /**
     * This hides and opens the colormenus. Also take care of moving focus to the right place.
     * 
     * @param color Which menu is to be set.
     * @param state true=visible false=hidden
     */
    public void setColorMenu(String color, boolean state) {
        if (color.equals("red")) {
            if (state == false) {
                keyBoardFocus = guiHandler;
                kontti.remove(redMenu);
                flushContainers();
                if (!covers)
                    mainFrame.addKeyListener(guiHandler);
                else
                    deviceFrame.addKeyListener(guiHandler);
                mainFrame.requestFocus();
                this.state = NORMAL;
            } else {
                this.state = RED;
                keyBoardFocus = redMenu;
                if (!covers)
                    mainFrame.removeKeyListener(guiHandler);
                else
                    deviceFrame.removeKeyListener(guiHandler);
                kontti.add(redMenu, 0);
                redMenu.requestFocus();
                redMenu.repaint();
            }
        }
        if (color.equals("yellow"))
            if (state == false) {
                keyBoardFocus = guiHandler;
                kontti.remove(yellowMenu);
                mainFrame.requestFocus();
                flushContainers();

                if (!covers)
                    mainFrame.addKeyListener(guiHandler);
                else
                    deviceFrame.addKeyListener(guiHandler);

                this.state = NORMAL;
            } else {
                if (!covers)
                    mainFrame.removeKeyListener(guiHandler);
                else
                    deviceFrame.removeKeyListener(guiHandler);

                this.state = GREEN;
                keyBoardFocus = yellowMenu;
                kontti.add(yellowMenu, 0);
                yellowMenu.requestFocus();
                yellowMenu.repaint();
            }
    }

    public void documentReady() {
        //presentationPanel.setBackground(Color.white);
        Log.debug(presentationPanel.getSize().toString());
    }

    /**
     * This hides and opens the URLMenu, setting the focus in the right place also.
     * 
     * @param state visible or not
     */
    public void setURLMenu(boolean state) {
        if (state == false) {
            flushContainers();
        } else {
            statusBar.requestFocus();
            statusBar.setText("");
            statusBar.repaint();
        }
    }

    /**
     * This open and hides the configuration menu
     * 
     * @param state visible or not
     */
    public void setConfigMenu(boolean state) {
        if (state == false) {
            kontti.remove(configMenu);
            flushContainers();
            keyBoardFocus = guiHandler;
            mainFrame.requestFocus();
        } else {
            kontti.add(configMenu, 0);
            keyBoardFocus = configMenu;
            configMenu.requestFocus();
        }
    }

    /**
     * This adds a URL to the bookmarks. Just haven't figured out how to implement is yet
     */
    public void navigateAddtoBookmarks() {
        Log.debug("Kludge: Adding current page to bookmarks, not really adding");
        setStatusText("Current page added to bookmarks.");
    }

    /**
     * Used for debuggin
     * 
     * @param str The debuggin message
     */
    private void debug(String str) {
        if (debug)
            Log.debug(str + "\n");
    }

    /**
     * Removes all grpahical components of the gui
     */
    protected void removeComponents() {
        mainFrame.getContentPane().remove(kontti);
    }

    /**
     * Flush containers
     */
    private void flushContainers() {
        kontti.invalidate();
        kontti.repaint();
        presentationPanel.invalidate();
        presentationPanel.repaint();
        mainFrame.requestFocus();
    }

    public void changeGui() {
        browser.getGUIManager().createMainGUI(true);
    }

    /**
     * A small class thingie that removes an arrow after a specified time. Needed by showArrow..
     * 
     * kludge..
     */
    private class ComponentPainter extends Thread {

        private FtvGUI gui;
        private int duration;
        private String command;

        public ComponentPainter(FtvGUI gui, int duration, String command) {
            this.gui = gui;
            this.duration = duration;
            this.command = command;
        }

        public void run() {
            Date date = new Date();
            long start = date.getTime();
            long now = date.getTime() + 1;
            try {
                sleep(duration);
            } catch (InterruptedException e) {
                System.err.println("FtvGUI: ComponenFlash interrupted");
            }
        }
    }

    /**
     * A Kludge to ensure that the mainFrame has focus
     */
    private class FocusGrabber extends FocusAdapter {

        public void FocusGained(FocusEvent e) {
            mainFrame.requestFocus();
        }
    }

    /**
     * A private class to Write a specific text in the statusfield for a specified time.After the time, the textfield is erased.
     */
    private class StatusFlash extends Thread {

        private FtvGUI gui;
        private int duration;
        private String text;

        public StatusFlash(FtvGUI gui, String text, int duration) {
            this.gui = gui;
            this.text = text;
            this.duration = duration;
        }

        public void run() {
            gui.statusBar.setText(text);

            try {
                sleep(duration);
            } catch (InterruptedException e) {
                Log.error("FtvGUI: StatusFlash interrupted");
            }
            gui.statusBar.setText("");
            animation.pause();
        }
    }

    /**
     * Sets the document area size
     * 
     * @param size as a Dimension
     */
    public void setDocumentAreaSize(Dimension size) {
        presentationPanel.setSize(new Dimension(800, 576));
    }

    /**
     * Revalidates (redraws) the document area.
     *  
     */
    public void reValidateDocumentArea() {
        Container c = this.getContentPanel();
        c.invalidate();
        c.getParent().invalidate();
        c.getParent().getParent().validate();
    }

    public void validate() {
        presentationPanel.repaint();
    }

    public void destroy() {
        if (covers) {
            deviceFrame.setEnabled(false);
            deviceFrame.setVisible(false);
        } else {
            mainFrame.setEnabled(false);
            mainFrame.setVisible(false);
        }

    }

    /**
     * Is back enabled
     * 
     * @param set false or true
     */
    public void setEnabledBack(boolean v) {
        Log.debug("BACKENABLED SET TO:" + v);
        backEnabled = v;
    }

    /**
     * @return The componentFactory
     */
    public ComponentFactory getComponentFactory() {
        if (componentFactory == null) {
            componentFactory = new DefaultComponentFactory(browser);
        }
        return componentFactory;
    }

    public MLFCControls getMLFCControls() {
        if (mlfcControls == null) {
            mlfcControls = new DefaultMLFCControls(getComponentFactory());
        }
        return mlfcControls;
    }

    /**
     * Is forward enabled
     * 
     * @param set false or true
     */
    public void setEnabledForward(boolean v) {
        Log.debug("FORWARDENABLED SET TO:" + v);
        forwardEnabled = v;
    }

    private class ComponentFlash implements Runnable {

        Thread t;
        Container dest;
        Component c;
        int time;

        public void flash(int time, Component c, Container dest) {
            t = new Thread(this);
            this.time = time;
            this.c = c;
            this.dest = dest;
            t.start();
            t = null;
        }

        public void run() {
            dest.add(c, 0);
            dest.invalidate();
            dest.repaint();
            c.requestFocus();
            try {
                Thread.sleep(time);
            } catch (Exception e) {
            }
            dest.remove(c);
        }
    }

    private class DeviceListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String com = e.getActionCommand();
            Log.debug(com);
            if (com.equals("onoff"))
                browser.getGUIManager().createMainGUI(true);
            if (com.equals("ok"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_ENTER);
            if (com.equals("up"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_UP);
            if (com.equals("down"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_DOWN);
            if (com.equals("left"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_LEFT);
            if (com.equals("right"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_RIGHT);
            if (com.equals("red"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_F1);
            if (com.equals("green"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_F2);
            if (com.equals("yellow"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_F3);
            if (com.equals("blue"))
                keyBoardFocus.keyPressed(mainFrame, KeyEvent.VK_F4);
        }
    }
}