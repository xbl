/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.mlfc;

import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XPanel;


/**
 * Class DefaultMLFCControls
 * 
 * @author tjjalava
 * @since Oct 22, 2004
 * @version $Revision: 1.1 $, $Date: 2004/10/22 12:59:56 $
 */
public class DefaultMLFCControls implements MLFCControls {
    
    private ComponentFactory componentFactory;
    private XPanel controlBar;

    public DefaultMLFCControls(ComponentFactory cf) {
        this.componentFactory = cf;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.mlfc.MLFCControls#getControlBar()
     */
    public XPanel getMLFCToolBar() {
        if (controlBar==null) {
            controlBar = componentFactory.getXPanel();
        }
        return controlBar;
    }

}
