/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.mlfc;

import fi.hut.tml.xsmiles.gui.components.XPanel;



/**
 * Class GUIPortions
 * 
 * @author tjjalava
 * @since Oct 21, 2004
 * @version $Revision: 1.1 $, $Date: 2004/10/22 12:59:56 $
 */
public interface MLFCControls {
    
    public XPanel getMLFCToolBar();

}
