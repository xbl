/* X-Smiles
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SUCH DAMAGE. Complete license in licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.event.GUIEventListener;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.mlfc.SourceMLFC;


/** 
 * very stripped down GUI base class that does not use swing at all
 * All it does, is dispatch events to possible listeners
 */
public abstract class XSmilesUIAWT implements GUI
{
    protected ContentHandlerFactory contentHandlerFactory=null;
    protected BrowserWindow browserWindow;
    protected Vector guiEventListeners;
    protected String latestLoc="untitled";
    protected String latestTitle="untitled";
    protected Hashtable focusPointProviders = new Hashtable();

    public XSmilesUIAWT(BrowserWindow b, Container c) 
    {
        browserWindow = b;
        initx();
  
    }


    public XSmilesUIAWT(BrowserWindow b)
    {
        browserWindow = b;
        initx();

    }
    
    private void initx() 
    {
        guiEventListeners = new Vector();
    }

    public void start()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.start();
        }
    }

    public void destroy()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.destroy();
        }        
    }

    public boolean isTabbed()
    {
        return false;
    }
    
    public void openInNewTab(XLink l, String id)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.openInNewTab(l, id);
        }        
    }
    
    /**
     * Open up a link in a new tab
     * @param id the id of the browser to be
     */ 
    public void openInNewWindow(XLink link, String id)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.openInNewWindow(link, id);
        }        
        //((Browser)this.browserWindow).newBrowserWindow(link.getURL().toString(),id);
    }

  
    /**
     * Default components. If GUI has a special component deployment sceme, then
     * they should override the fi.hut.tml.xsmiles.gui.components.ComponentFactory
     * in neccecary places.
     *
     * @return A ComponentFactory which is capable of returning all components needed
     *         by MLFCs, or any other non-GUI package classes.
     * @see ComponentFactory
     */
    public abstract ComponentFactory getComponentFactory();
    
    public abstract MLFCControls getMLFCControls();
  
    /**
     * @return ContentHandler creates players for certain mime types
     * @see ContentHandlerFactory
     */
    public ContentHandlerFactory getContentHandlerFactory()
    {
        if (contentHandlerFactory==null) contentHandlerFactory=new ContentHandlerFactory(browserWindow);
        return contentHandlerFactory;
    }

  
    /**
     * These are only to be used by the Browser class. Add eventListeners 
     * with the BrowserWindow addGUIEventListener method. 
     * 
     * @param e Attach GUIEventListener to the EventBroker.
     */
    public void addGUIEventListener(GUIEventListener e)
    {
        guiEventListeners.addElement(e);
    //		Log.debug("AddGUIEventListener: count: "+guiEventListeners.size());
    }
    
    public void removeGUIEventListener(GUIEventListener e)
    {
        guiEventListeners.removeElement(e);
    }


    /**
     * @param e GUIEventObject to be sent to everyone
     */
    //public void issueGUIActionEvent(GUIEvent e)
    //{
    //	GUIEventListener reactee;
    //	Enumeration e = guiEventListeners.elements();
    //	while(e.hasMoreElements())
    //	{
    //		reactee = (GUIEventListener)e.nextElement();
    //		reactee.guiActionPerformed(e);
    //	}
    //}

    /**
     * The bookmarks have changed
     *
     * @param GUIEvent to be bundled with event
     */
    //public void issueBookmarksChangedEvent(GUIEvent e)
    //{
    //	GUIEventListener reactee;
    //
    ////			Enumeration e = guiEventListeners.elements();
    //		GUIEventListener bl;

    //		while(e.hasMoreElements())
    //		{
    //			bl = (GUIEventListener)e.nextElement();
    //			bl.bookmarksChangedEvent(e);
    //		}	
    //	}

    /**
     * @param l Attach GUIEventListener to the EventBroker.
     */
    //public void removeGUIEventListener(GUIEventListener l)
    //{
    //	guiEventListeners.removeElement(l);
    //}


    //    private JDialog errorDialog = new JDialog();


    /**
     * A refrence to the MLFCMenu (i.e. the "Document"-menu of the GUI)
     *
     * @return null if no menu exists (mlfc's will handle the null checking)
     * @depracated We will go to a MLFC-draws-it's-own-controls-on-it's-own-area-based
     *             solution in the future. 
     */
    //public JMenu getMLFCMenu()
    //{
    //	return null;
    //}

    /* 
     * The names of the methods stand for themselves here.
     */
    public void setStatusText(String statusText)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setStatusText(statusText);
        }        

    }

    public void setEnabledBack(boolean value)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setEnabledBack(value);
        }        

    }

    public void setEnabledForward(boolean value)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setEnabledForward(value);
        }        

    }

    public void setEnabledHome(boolean value)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setEnabledHome(value);
        }        

    }

    public void setEnabledStop(boolean value)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setEnabledStop(value);
        }        

    }

    public void setEnabledReload(boolean value)
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setEnabledReload(value);
        }        

    }

    //public void setEnabledAllMenus(boolean value)
    //	{
    //	}
  
    //public void setEnabledLocationCombo(boolean value)
    //	{
    //	}
  
    public void setTitle(String title)
    {
	latestTitle=title;
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setTitle(title);
        }        
    }

    public  void setLocation(String s) 
    {
	latestLoc = s;
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.setLocation(s);
        }        
    }
  
    //    public abstract void reValidateDocumentArea();
  
    public void moveActiveLinkUp()
    {
    }

    public void moveActiveLinkDown()
    {
    }

    public void displayKeypad(Component target, String mode)
    {
    }
  
    public void hideKeypad()
    {
    }

    public void showMessageDialog(boolean isModal, String title, String message, long timeToLiveMillis) 
    {
        Log.info(this+".showMessageDialog: "+title+" : "+message);
        this.getComponentFactory().showMessageDialog(isModal,title,message,timeToLiveMillis);
        
        /*
        int messageType = JOptionPane.INFORMATION_MESSAGE;
        int optionType = JOptionPane.DEFAULT_OPTION;
        JOptionPane pane = new JOptionPane(message, messageType,
                                           optionType, null,null, null);

        //pane.setInitialValue(initialValue);

        final JDialog dialog = pane.createDialog(null, title);
        dialog.setModal(isModal);
        pane.selectInitialValue();
        dialog.show();
        if (timeToLiveMillis>0)
            {
                final long time = timeToLiveMillis;
                Thread t= new Thread(title) {
                        public void run() {
                            try
                                {
                                    Thread.sleep(time);
                                    dialog.dispose();
						
                                } catch (Exception e)
                                    {
                                    }
                        }
                    };
                t.start();
			
            }*/
    }

    /**
     * Show a popup dialog, whenever something fatal occurs.. 
     * (with xsmiles this happens quite often ;)
     */
    public void showErrorDialog(boolean isModal, String heading, String description)
    {
        this.showMessageDialog(isModal, heading, description, 5000); 
        /*
        JOptionPane.showMessageDialog(null, 
                                      description,heading,JOptionPane.ERROR_MESSAGE,null);*/
    }	  

    /**
     * @return the Frame of the GUI.
     */
    public abstract Window getWindow();


    /**
     * If a GUI has some "i'm-working-please-wait-animator", then inform the GUI, 
     * that now is a good time to start animating. You might want to also use 
     * some kind of semaphore system, i.e. workCount. 
     */
    public void browserWorking()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.browserWorking();
        }        

        Log.debug("Browser working");
    }

    /**
     * Stop animating. 
     */
    public void browserReady()
    {
        GUIEventListener reactee;
        Enumeration e = guiEventListeners.elements();
        while(e.hasMoreElements())  {
            reactee = (GUIEventListener)e.nextElement();
            reactee.browserReady();
        }        
        Container contentArea =this.browserWindow.getContentArea(); 
        contentArea.invalidate();
        if (contentArea!=null && contentArea.getParent()!=null) {
            contentArea.getParent().validate();
        }
        Log.debug("Browser resting");
    }

    /** The modes are from XSmilesView class */
    public void showSource(XMLDocument doc, int mode, String heading)
    {
        showSourceStatic(doc,mode,heading);
    }
    /** The modes are from XSmilesView class */
    public static void showSourceStatic(XMLDocument doc, int mode, String heading)
    {
        SourceFrame frame = new SourceFrame(heading);
        SourceMLFC mlfc = new SourceMLFC();
        mlfc.setMLFCListener(((ExtendedDocument)doc.getDocument()).getHostMLFC().getMLFCListener());
        mlfc.display(doc,mode,frame);
            //public void display(XMLDocument doc,int mode, Container c )
        frame.setVisible( true );
    }

	
    /**
     * Cleanup GUI
     */
    public void reDrawGUI()
    {
    }  

    /**
     * Set the skin in browser configuration file
     *
     * @param the skin
     */
    public void setSkinsIfNeeded(String skin) 
    {
        browserWindow.getBrowserConfigurer().setProperty("gui/theme",skin);
        setSkinsIfNeeded();
    }
    
    /**
     * All this stuph is needed for modular skins plugin. 
     * The skin library doesn't need to be resident, if
     * skins aren't used
     *
     * @param t The themePack location
     */
    public void setSkinsIfNeeded() 
    {
    }
    
        /** @return whether or not this gui should be instructed to reload.
     * XMLGUI should return false, since it creates an extra content area and
     * browser for the content
     */
    public boolean shouldReloadAtStartup()
    {
        return true;
    }

    public void fireLatestEvents()
    {
	setLocation(latestLoc);
	setTitle(latestTitle);
    }

    public void registerFocusPointProvider(FocusPointsProvider fpp, BrowserWindow bw)
    {
        //MH: commented out in 23.1.2006 since it was never unregistered resulting into HUGE memory leak
        /*
        Vector providers;
        providers = (Vector)focusPointProviders.get(bw);
        if (providers != null)
            providers.add(fpp);
        else
        {
            providers = new Vector();
            providers.add(fpp);
            focusPointProviders.put(bw, providers);
        }
        */
    }

    public void unRegisterFocusPointProvider(FocusPointsProvider fpp, BrowserWindow bw)
    {
        ((Vector)focusPointProviders.get(bw)).remove(fpp);
    }
}


