/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.gui.swing;

import java.awt.Container;
import java.awt.Window;

import javax.swing.JFrame;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;

/**
 * A stub GUI. The main function is to send events to third party GUIs and also
 * to provide default implementations of the ComponentFactory and ContentHandler
 * classes.
 * 
 * @author Juha
 */
public class NullGUI extends XSmilesUI 
{
    //  private MLFCController mlfcController;
    private BrowserWindow browser;
    private BrowserConstraints constraints;
    private ContentHandlerFactory contentHandlerFactory;
    private MLFCControls mlfcControls;
    private ComponentFactory componentFactory;

    /**
     * @param b the BrowserWindow
     * @param c componentContainer (optional)
     */
    public NullGUI(BrowserWindow b, Container c) {
        super(b,c);
        browser=b;
    
    }
    
    /**
     * @param b the BrowserWindow
     */
    public NullGUI(BrowserWindow b) {
        super(b,null);
        browser=b;
    }
/*
    public void validate() {

    }
*/
    
    public BrowserConstraints getBrowserConstraints() {
        return constraints;
    }
  /*
    
    public void navigate(int navigationCommand)
    {
    }

    public void openFile(File file)
    {
    }

    public void openFile(String fileName)
    {
    }

    public void openLocation(URL url)
    {
    }

    public void openLocation(String urlName)
    {

    }

    public void exit()
    {
        
    }

    public void changeViewEvent(int viewType)
    {

    }

    public void resizeEvent(Dimension size)
    {

    }

    public void setStatusText(String statusText)
    {

    }
    
    public void setEnabledBack(boolean value) 
    {

    }

    public void setEnabledForward(boolean value) 
    {

    }

    public void setEnabledHome(boolean value)
    {

    }

    public void setEnabledStop(boolean value)
    {

    }

    public void setEnabledReload(boolean value)
    {

    }

    public void setEnabledAllMenus(boolean value)
    {
        
    }
*/
  
    public ContentHandlerFactory getContentHandlerFactory()
    {
        if (contentHandlerFactory==null) contentHandlerFactory=new SwingContentHandlerFactory(browser);
        return contentHandlerFactory;
    }
  
    /**
     * Return mainFrame
     * @return mainFrame
     */
    public Window getWindow() 
    {
        return new JFrame();
    }
  
    public void destroy() 
    {
        browser=null;
    }
    public boolean isTabbed()
    {
        // To get the events for opening in new tab...
        return true;
    }
    
    public MLFCControls getMLFCControls() {
        if (mlfcControls == null) {
            mlfcControls = new DefaultMLFCControls(getComponentFactory());
        }
        return mlfcControls;
    }
  
    public ComponentFactory getComponentFactory() {
        if (componentFactory == null) {
            componentFactory = new DefaultComponentFactory(browser);
        }
        return componentFactory;
    }  
}
