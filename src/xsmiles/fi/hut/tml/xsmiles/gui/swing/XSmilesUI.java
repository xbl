/* X-Smiles
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SUCH DAMAGE. Complete license in licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles.gui.swing;

import java.awt.Container;

import javax.swing.JOptionPane;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.SourceFrame;
import fi.hut.tml.xsmiles.gui.XSmilesUIAWT;
import fi.hut.tml.xsmiles.mlfc.swing.TreeView;



public abstract class XSmilesUI extends XSmilesUIAWT implements GUI
{

    public XSmilesUI(BrowserWindow b, Container c) 
    {
        super(b,c);
    }


    public XSmilesUI(BrowserWindow b)
    {
        super(b);
    }
    

    
    public void showMessageDialog(boolean isModal, String title, String message, long timeToLiveMillis) 
    {
        this.getComponentFactory().showMessageDialog(isModal,title,message,timeToLiveMillis);
    }

    /**
     * Show a popup dialog, whenever something fatal occurs.. 
     * (with xsmiles this happens quite often ;)
     */
    public void showErrorDialog(boolean isModal, String heading, String description)
    {

        JOptionPane.showMessageDialog(null, 
                description,heading,JOptionPane.ERROR_MESSAGE,null);
    }	  

    
    /** The modes are from XSmilesView class */
    public void showSource(XMLDocument doc, int mode, String heading)
    {
        if (mode==XsmilesView.TREEMLFC)
        {
            SourceFrame frame = new SourceFrame(heading);
            Log.debug("The tree should start now");
            TreeView view = new TreeView();
            view.view(frame,doc);
            frame.setVisible( true );
        }
        else
        {
            super.showSource(doc,mode,heading);
            /*
            SourceMLFC mlfc = new SourceMLFC();
            mlfc.setMLFCListener(((ExtendedDocument)doc.getDocument()).getHostMLFC().getMLFCListener());
            mlfc.display(doc,mode,frame);
            //public void display(XMLDocument doc,int mode, Container c )
            */

        }

    }
    
 }


