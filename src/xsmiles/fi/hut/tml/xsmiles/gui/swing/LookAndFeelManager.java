/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 27, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.swing;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.MLFCLoader;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.XMLConfigurer;


/**
 * @author honkkis
 *
 */
public class LookAndFeelManager
{
	protected static String METOUIA_CLASS="fi.hut.tml.xsmiles.gui.met.plf.XSmilesLookAndFeel";
    
    public static void setStyle(XMLConfigurer configurer)
    {
        Hashtable styleClasses = new Hashtable();
        styleClasses.put("System",UIManager.getSystemLookAndFeelClassName());
        styleClasses.put("Crossplatform",UIManager.getCrossPlatformLookAndFeelClassName());
        //put("net.sourceforge.mlf.metouia.MetouiaLookAndFeel");
        styleClasses.put("Metouia",METOUIA_CLASS);
        styleClasses.put("GTK+","com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        styleClasses.put("Liquid","com.birosoft.liquid.LiquidLookAndFeel");
        styleClasses.put("Tonic","com.digitprop.tonic.TonicLookAndFeel");
        styleClasses.put("TinyLaF","de.muntjak.tinylookandfeel.TinyLookAndFeel");
        styleClasses.put("Substance","org.jvnet.substance.SubstanceLookAndFeel");
        // In windows XP and JDK 1.5 the default LF is systemlookandfeel, since it's great!
        String defaultLookandFeel = getDefaultLF();
        styleClasses.put("Default",defaultLookandFeel);
        
        

        String initialTheme = configurer.getProperty("gui/theme");
        String cn = (String)styleClasses.get(initialTheme);
        if (cn==null) cn=initialTheme;
        Log.debug("Setting Swing Look And Feel class: " + cn);
        final String klassName=cn;
        try
        {
            Class klass = MLFCLoader.loadClass(klassName);
            LookAndFeel lf = (LookAndFeel) klass.newInstance();
            UIManager.setLookAndFeel(lf);
        } catch (Throwable exception)
        {
             Log.error(exception);
             Log.error("Note: for liquid L&F, you need liquidlnf.jar in the classpath. \nFor metouia metouia.jar must be present. For tinylaf, tinylaf.jar needs to be in the classpath. Additionally you might need Default.theme in your home dir for tinylaf. Also, you need to grant permissions for the jars in xsmiles.policy file.");
        }
    }

	/**
	 * @return
	 */
	private static String getDefaultLF() {
		// TODO Auto-generated method stub
		try
		{
			if (isWindowsXP() && isJava15OrHigher())
            {
                return UIManager.getSystemLookAndFeelClassName();
            }  else if (isMacOs() && isJava14OrHigher())
            {
                return UIManager.getSystemLookAndFeelClassName(); // this brings the mac os look and feel
            }
			else if (isJava15OrHigher()) 
            {
                return UIManager.getCrossPlatformLookAndFeelClassName();
            }
		} catch (Exception e)
		{
			Log.debug("getDefaultLF() "+e.getMessage());
		}
		return METOUIA_CLASS;
	}
	
	private static boolean isWindowsXP() throws Exception
	{
		String osname=System.getProperty("os.name");
		return osname.equalsIgnoreCase("Windows XP");
	}

	private static boolean isJava15OrHigher() throws Exception
	{
		double ver =Utilities.getJavaVersion(); 
		return ver>1.4999;
	}
    
    private static boolean isMacOs() throws Exception
    {
        String osname=System.getProperty("os.name").toLowerCase();
        return osname.indexOf("mac")!=-1;
    }

    private static boolean isJava14OrHigher() throws Exception
    {
        double ver =Utilities.getJavaVersion(); 
        return ver>1.3999;
    }

}

