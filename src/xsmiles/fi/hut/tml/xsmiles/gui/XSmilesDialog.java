/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 7, 2004
 *
 */
package fi.hut.tml.xsmiles.gui;

import fi.hut.tml.xsmiles.BrowserWindow;


/**
 * @author honkkis
 *
 */
public interface XSmilesDialog
{
    public void setBrowserWindow(BrowserWindow bw);
    public BrowserWindow getBrowserWindow();
    public void start();
    /**
     * 
     */
    public void destroy();
}
