/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.gui.media.general;


import java.awt.Component;
 
// For CSS2

import org.w3c.dom.css.*;
import java.lang.Float; 


 
 /**
  * Reads in a style string and formats a text component (font, color).
  *
  * @author Kari Pihkala
  * @author Mikko Honkala
  */
 public interface CSSTextFormatterInterface
 {
	public void setStyle(String s);
	public void formatComponent(Component comp);
 }
