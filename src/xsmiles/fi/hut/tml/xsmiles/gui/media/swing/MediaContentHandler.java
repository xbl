package fi.hut.tml.xsmiles.gui.media.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.gui.media.general.AWTMediaContentHandler;
import fi.hut.tml.xsmiles.gui.media.general.Media;

/**
 * Handles application/xml
 * 
 * This class should be reusable for optimization purposes
 * 
 * @author Mikko Honkala
 */
public class MediaContentHandler extends AWTMediaContentHandler implements XSmilesContentHandler {

    public MediaContentHandler(Media aMedia) {
        super(aMedia);
    }

    public void playPrimary() throws Exception {
        // primary content
        Container c = this.getContainer();
        JPanel p = new JPanel();
        p.setBackground(Color.white);
        JScrollPane scroll = new JScrollPane(p);
        //scroll.setLayout(new BorderLayout());
        p.setLayout(new BorderLayout());
        // Get rid of borders coming thanks to FlowLayout
        Border empty = BorderFactory.createEmptyBorder();
        p.setBorder(empty);
        // Get rid of borders coming thanks to ScrollLayout
        scroll.setBorder(empty);
        c.add(scroll);
        // TODO: move media prefetch to prefetch stage!
        media.setContainer(p);
        media.setBounds(0, 0, media.getOriginalWidth(), media.getOriginalHeight());
        media.play();
    }
}
