/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.general.jmfxine;

import java.awt.Component;

import javax.media.Player;


/**
 * Class JMFPlayer
 * 
 * @author tjjalava
 * @since Jan 19, 2005
 * @version $Revision: 1.2 $, $Date: 2005/01/26 12:28:54 $
 */
public interface JMFPlayer extends Player {
    
    public void setVisualComponent(Component c);

}
