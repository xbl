/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.general;

import java.awt.Container;
import java.net.URL;

import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

/**
 * This is the interface for binary media. The methods should be called in the following
 * order: <ol>
 * <li>setUrl(url to media)
 * <li>setContainer(myContainer)  [not necessary for audio]
 * <li>setBounds(0,0, 100, 100)
 * <li>addMediaListener(listener) [optional]
 * <li>prefetch()
 * <li>play()
 * <li>pause() [optional]
 * <li>play()
 * <li>close()
 * </ol>
 */
public interface Media //extends fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.media.Media 
{

	/**
	 * All traffic to the browser, such as ComponentFactory, etc goes through this listener.
	 * If no listener supplied media players should still function with some basic level. 
	 * 
	 * @param listener The MLFCListener supplied by the browser
	 * @see MLFCListener
	 */
	public void setMLFCListener(MLFCListener listener);

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static (duration is zero, see getOriginalDuration().
	 */
	public boolean isStatic();

	/**
	 * Get the duration of media. Only applicable for continuous media (audio, video).
	 * @return The duration of media in millisecs. zero for static media (images, text)
	 * 			-1 means indefinite (infinite streamed media or unknown duration).
	 */
	public int getOriginalDuration();
	
	/** 
	 * Get the real width of the media. If not visible, or size is unknown, then returns -1.
	 * @return Original width of the media. -1 means no particular width (audio)
	 * or unknown width (text, XML media...)
	 */
	public int getOriginalWidth();

	/** 
	 * Get the real height of the media. If not visible, or size is unknown, then returns -1.
	 * @return Original height of the media. -1 means no particular height (audio)
	 * or unknown height (text, XML media...)
	 */
	public int getOriginalHeight();

	/** 
	 * Sets the URL for this media. This method will only set the URL for the media.
	 * To actually download the data, prefetch() or play() should be called.
	 * @param url		URL for media
	 */
	public void setUrl(URL url);

	/**
	 * Sets the container the media will be rendered in. If media is audio, this
	 * can be null. Can be called if media is already visible in one container,
	 * it will then move to the new container.
	 * @param container   This container will contain the media.
	 */
	public void setContainer(Container container);

	/**
	 * Set the coordinates for the media. These are relative to the given container,
	 * set using setContainer(). Setting the bounds will always immediately move
	 * the media to a new location, if it is visible.
	 */
	public void setBounds(int x, int y, int width, int height);

	/**
	 * Set the sound volume for media. Only applicable for sound media formats.
	 * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
	 * dB change in signal level = 20 log10(percentage / 100) )
	 */
	 public void setSoundVolume(int percentage);

	/**
	 * Prefetches media. The URL must have been set using setUrl(). 
	 * The data will be downloaded from the URL. After calling this method,
	 * the media will be in memory and can be played. This is a blocking method.
	 */
	public void prefetch() throws Exception;

	/**
	 * Plays the media. The media will be added to the container set using setContainer().
	 * It will be visible. It will also play any animation it possibly has. Also,
	 * audio media is started using this method.
	 * <p>If the media is not yet prefetched, it will first be prefetched.
	 */
	public void play() throws Exception;

	/**
	 * Pauses the media. The media will stay visible, but any animations will be paused.
	 * Audio media will be silent. NOT IMPLEMENTED YET.
	 * ?How to restart paused media?
	 */
	public void pause();

	/**
	 * Stops the media. The media will be stopped and it will be invisible. Audio will be
	 * silent.
	 */
	public void stop();

	/**
	 * This will freeze all memory and references to this media. If the media is not
	 * yet stopped, it will first be stopped.
	 */
	public void close();

	/**
	 * This moves the time position in media. Works only for continuous media (video/audio).
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs);

	/**
	 * Adds a MediaListener for this media. The listener will be called <ol>
	 * <li>When the media has been prefetched. (NOT IMPLEMENTED YET).</li>
	 * <li>When the media ends. (continuous media, video/audio) </li></ol>
	 * Static media, such as text and images will end immediately, notifying
	 * immediately about the end of media.
	 */
	public void addMediaListener(MediaListener listener);
	
	/**
	 * Requests the media player to display a control panel for media.
	 * For audio and video, these can be a volume/play/stop controls,
	 * for images, these can be zoom controls etc.
	 * The controls are GUI dependent, generated through ComponentFactory.
	 * @param visible	true=Display controls, false=don't display controls.
	 */
	public void showControls(boolean visible);	
}