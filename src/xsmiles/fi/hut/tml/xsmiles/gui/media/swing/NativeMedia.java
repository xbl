/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.swing;

// All possible info to get peers and stuff..
import java.awt.Canvas;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * This is the implementation of native media - currently working only in Win32.
 * This displays the IE ActiveX Component in a canvas (in media element).
 * Useful for certain media types, such as .ppt, .doc .pdf, etc.
 */
public class NativeMedia implements Media, Runnable, MouseListener {

	// NativeCanvas for current media
	NativeCanvas canvas;

	// Container for media (DrawingArea)
	Container container = null;

	// MediaListener - called after prefetch and endOfMedia.
	MediaListener mediaListener = null;

	// Location and coords for the media
	int x=0, y=0, width=0, height=0;

	// URL to be loaded
	URL url = null;

	static {
		// Load the library that contains the paint code.
		System.loadLibrary("SMILNativeMedia");
	}
	
	/**
	 * Constructor
	 */
	public NativeMedia() {
		// Create the components		
		canvas=new NativeCanvas();
		canvas.setSize(300, 300);

		// This will get all mouse events, and pass them to the MediaListener
		canvas.addMouseListener(this);			
	}

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static.
	 */
	public boolean isStatic() {
		return true;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public void prefetch() {
		canvas.loadURL(url);
		canvas.setVisible(false);
	}

	/**
	 * Set the container. Must be dynamic, the media must move if container changes.
	 */
	public void setContainer(Container container) {
		if (this.container != container) {

			// Change container
			if (this.container!=null && canvas.isVisible() == true) {
				canvas.setVisible(false);
				this.container.remove(canvas);
				container.add(canvas, 0);
				canvas.setVisible(true);
			}

			this.container = container;
		}
	}

	/** 
	 * Displays the native media. Also calls mediaEnded() immediately - this is static media,
	 * and therefore will "freeze" immediately.
	 */
	public void play() {
		if (container != null) {
			canvas.setSize(width, height);
			container.add(canvas, 0);
			canvas.setLocation(x, y);
			canvas.setSize(width, height);
			canvas.setVisible(true);
		} else
			Log.error("Container not set for media "+url.toString());
		
		// Media ends immediately for static media (almost..).
		// Media ended - inform the SMIL player.
		if (mediaListener != null) {
			Thread t = new Thread(this);
			t.start();
		}
		
	}

	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		// Media ended immediately (almost...)
		if (mediaListener != null)
			mediaListener.mediaEnded();
	}
	

	public void pause() {
	}

	public void stop() {
		if (canvas != null) {
			canvas.setVisible(false);
			if (container != null)
				container.remove(canvas);
		}
		
		// Clear media listeners
		if (mediaListener != null)
			mediaListener = null;

	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;

		this.width = width;
		this.height = height;	
		
		// If the image is shown on the screen, move it immediately
		if (canvas.isVisible() == true) {
			// Use the given coordinates
			canvas.setLocation(x, y);
			canvas.setSize(width, height);     
		}			
	}

	/** 
	 * Get the real width of the media.
	 */
	public int getOriginalWidth() {
		return -1;	
	}

	/** 
	 * Get the real height of the media.
	 */
	public int getOriginalHeight() {
		return -1;	
	}

	public void close() {
		stop();
	
		if (container != null)
			container.remove(canvas);
	
		// Free native resources
		canvas = null;
		mediaListener = null;
	}

	/**
	 * This moves the time position in media. Not effective for this media.
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs) {
	 }

	 /**
	  * Set the sound volume for media. Does nothing for this media player.
	  * @param percentage	Not used.
	  */
	 public void setSoundVolume(int percentage) {
	 }

	 /**
	  * Get the duration of media. Only applicable for continuous media (audio, video).
	  * @return The duration of media in millisecs.
	  */
	 public int getOriginalDuration() {
	 	return 0;
	 }
	
	public void addMediaListener(MediaListener listener) {
		mediaListener = listener;
	}

	/**
	 * Requests the media player to display a control panel for media.
	 * For audio and video, these can be a volume/play/stop controls,
	 * for images, these can be zoom controls.
	 * The controls are GUI dependent, generated through ComponentFactory.
	 * @param visible	true=Display controls, false=don't display controls.
	 */
	public void showControls(boolean visible) {
		if (visible == true) {
		    ComponentFactory cf=mlfcListener.getComponentFactory();
		    XPanel cb = mlfcListener.getMLFCControls().getMLFCToolBar();
		    cb.removeAll();
		    selectOne = cf.getXSelectOne("minimal",false);
		    selectOne.addSelection("100%");
		    selectOne.addSelection("200%");
		    selectOne.setSelected("100%");
			selectOneListener = new ScaleListener();
			selectOne.addItemListener(selectOneListener);
		    cb.add(selectOne);
			Log.debug("DISPLAYING CONTROLS!");
		}
	}
	private ScaleListener selectOneListener;
	private XSelectOne selectOne;

	private class ScaleListener implements ItemListener {
	  public void itemStateChanged(ItemEvent e) {
			  if (e.getStateChange()==ItemEvent.DESELECTED) return;
			    String scaleStr=(String)e.getItem();
				if (scaleStr.equals("100%"))
					setBounds(0,0,getOriginalWidth(),getOriginalHeight());
				if (scaleStr.equals("200%"))
					setBounds(0,0,getOriginalWidth()*2,getOriginalHeight()*2);
			  }
	}

	/**
	 * All traffic to the browser, such as openLocation, etc goes through this listener.
	 * If no listener supplied media players should still function with some basic level. 
	 * 
	 * @param listener The MLFCListener supplied by the browser
	 * @see MLFCListener
	 */
	public void setMLFCListener(MLFCListener listener) {
		mlfcListener = listener;
	}
	private MLFCListener mlfcListener;

	/**
	 * Mouse listener...
	 */
	public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}
	
	public void mouseEntered(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseEntered();
	}

	public void mouseExited(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseExited();
	}
	
	public void mousePressed(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mousePressed();
	}
	
	public void mouseReleased(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseReleased();
	}	
	
	/**
	 * Simple class to draw images. This is required to scale the images.
	 * (ImageIcon couldn't handle it)
	 */
	public class NativeCanvas extends Canvas {
		int width = 0;
		int height = 0;
		URL url = null;
		int    m_hWnd   = 0;

		public NativeCanvas() {
		}

		public void loadURL(URL u) {
			url = u;
		}

		public void setSize(int w, int h) {
			super.setSize(w, h);
			if (width != w || height != h)  {
				width = w;
				height = h;
				//super.setPreferredSize(new Dimension(w, h));
				if (m_hWnd!=0)  {
				    resizeControl(m_hWnd, width, height);
				}
			}
		}

		public void setSize( Dimension d ) 
		{
			super.setSize(d);
			if (width != d.width || height != d.height)  {
				width = d.width;
				height = d.height;
			    if(m_hWnd!=0)
			        resizeControl(m_hWnd, d.width, d.height);
			}	
		}
	
		public void setBounds( int x, int y, int w, int h ) 
		{
		    super.setBounds(x,y,w,h);
		    if (width != w || height != h)  {
			    width = w;
			    height = h;
			    if(m_hWnd!=0)
			        resizeControl(m_hWnd, w, h);
		    }	
		}
	
		public void setBounds( Rectangle r ) 
		{
		    super.setBounds(r);
		    if (width != r.width || height != r.height)  {
		    	width = r.width;
		    	height = r.height;
			    if(m_hWnd!=0)
			        resizeControl(m_hWnd, r.width, r.height);
		    }	
		}

		// native entry point for initializing the IE control.
		public native void initialize(int hwnd, String strURL, int width, int height);
	
		// native entry point for resizing.
		public native void resizeControl(int hwnd, int nWidth, int nHeight);

		// native entry point for closing resources.
		// Note this must be called to free MS Word Doc lock files. If X-Smiles
		// crashes, this will be a problem.
		public native void destroy(int hwnd);

		// The native method is for JDK1.3 and above - the official way to obtain hwnd.
		public native int getHWND();
		// Returns the HWND for panel. This is a hack which works with
		// JDK1.1.8, JDK1.2.2 and JDK1.3. This is undocumented. Also 
		// you should call this only after addNotify has been called.
//		public int getHWND() 
//		{
//		    int hwnd = 0;
//		// This stuff is system dependent and should be commented out. 
//		// Otherwise, won't compile with 1.1 or Linux.
//		    DrawingSurfaceInfo drawingSurfaceInfo = ((DrawingSurface)(getPeer())).getDrawingSurfaceInfo();
//		    if (null != drawingSurfaceInfo) 
//			{
//		        drawingSurfaceInfo.lock();
//		        Win32DrawingSurface win32DrawingSurface = (Win32DrawingSurface)drawingSurfaceInfo.getSurface();
//		        hwnd = win32DrawingSurface.getHWnd();
//		        drawingSurfaceInfo.unlock();
//		    }
//		    return hwnd;
//		}

		/**
		 * Initialize the native component.
		 */
		public void addNotify()
		{
		    super.addNotify();
		    m_hWnd = getHWND();
		    initialize(m_hWnd, url.toString(), width, height);
		}
	
		/**
		 * Uninitialize the native component.
		 */
		public void removeNotify()  {
			destroy(m_hWnd);
			super.removeNotify();
		}

	} 
	
}