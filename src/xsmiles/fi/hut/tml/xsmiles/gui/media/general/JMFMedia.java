/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.general;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.media.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * This is the implementation of JMF media (audio/video...).
 */
public class JMFMedia implements Media, MouseListener, Runnable {

    // Hashtable holding loaded images - this is a proxy
    private Hashtable loadedImages;
  
    // Label and ImageIcon for current image
    private Component comp = null;

    // Container for media (DrawingArea)
    private Container container = null;

    private javax.media.Player  player = null;

    // MediaListener - called after prefetch and endOfMedia.
    MediaListener mediaListener = null;

    // Coords for the media
    private int x=0, y=0, width=0, height=0;

    // Media URL
    private URL url = null;

    // ControllerListener to listen for JMF
    PlayerListener playerListener = null;

    // Player state set by this
    boolean playing = false;

    private boolean paused = false;

    public JMFMedia() {
        // Create the proxy hashtable
        loadedImages = new Hashtable();         
    }

    /**
     * Checks if this media is static or continuous.
     * @return true if media is static.
     */
    public boolean isStatic() {
        return false;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public synchronized void prefetch() {
        Log.debug("JMFMedia prefetch: "+this.url);      
        MediaLocator ml;

        try {
            if ((ml = new MediaLocator(url)) == null) {
                throw new Exception("Can't build URL for " + url.toString());
            }
            
            try  {          
                if (player==null) {
                    player = Manager.createPlayer(ml);
                } 
            } catch (NoPlayerException e) {
                Log.error(e,"ElementPlayer:");
                throw e;
            }
        } catch (Exception e) {
            Log.error("Error loading URL "+url.toString());
            return;
        }
        Log.debug("Created player: "+player.getClass().getName());
        
        if (player != null) {           
            playerListener = new PlayerListener(this);
            player.addControllerListener(playerListener);
            player.realize();

            // Wait until notified, after realization.
            try {
                this.wait();
            } catch (InterruptedException e) {
            }
        }
    }
    
    public void setContainer(Container container) {
        if (this.container != container) {

            // Change container
            if (comp != null && comp.isVisible() == true) {
                comp.setVisible(false);
                this.container.remove(comp);
                container.add(comp, 0);
                comp.setVisible(true);
            }

            this.container = container;
        }
    }

    public void play() {
        // Set to true, mediaEnded() will be called
        playing = true;

        if (container != null && comp != null) {
            container.add(comp);
            comp.setLocation(x, y);
            comp.setSize(width, height);
            comp.setVisible(true);
        } else if (container == null) 
            Log.error("Container not set for media "+url.toString());
        else    
            Log.debug("Component missing for media "+url.toString());

        // Play it!
        if (player != null)
            player.start();
        else {
            Log.error("Player not initialized for "+url.toString()+"!");
            // End this media immediately
            Thread t = new Thread(this);
            t.start();
        }

    }

    /**
     * This media ends immediately, if the player was not found. Actually,
     * this should be replaced by an alt-text component!!!! (ending immediately)
     */
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        // Media ended immediately (almost...)
        if (mediaListener != null)
            mediaListener.mediaEnded();
    }

    public void pause() {
        Log.debug("JMFMEDIA: pause("+url.toString()+")");
        // Pause it      
        if (player == null) {
            return;
        }
        
        if (paused) {
            paused = false;
            player.start();
        } else {
            paused = true;
            player.stop();
        }
    }

    public void stop() {
    
        // Set to false, mediaEnded() will not be called
        playing = false;

        if (comp != null)
            comp.setVisible(false);


        if (container != null && comp != null)
            container.remove(comp);
    
        // Stop it!
        if (player != null) {
            try {
                player.stop();
                player.setMediaTime(new Time(0));
            } catch (javax.media.NotRealizedError e) {
                // Catch if time is set for unrealized player
            }
        }

        // Clear media listeners
        // This is also needed to prevent JMF from sending MediaEndedEvent 
        // to activate the next element.
        if (mediaListener != null)
            mediaListener = null;   
    }
    
    public void setBounds(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        // If the media is shown on the screen, move it immediately
        if (comp != null && comp.isVisible() == true) {
            // Use the given coordinates
            comp.setLocation(x, y);
            comp.setSize(width, height);
        }           
    }

    /**
     * Close this media and free all resources.
     */
    public void close() {
        // close the player and set it to null to free memory
        if (player != null) {
            stop();
            player.removeControllerListener(playerListener);
            player.close();
        }
        player = null;

        // Free the listener        
        if (playerListener != null) {
            playerListener.free();
            playerListener = null;
        }
        
        Log.debug("JMFMedia closing "+url);
    
        // Flush all images
        if (loadedImages != null) {
            Enumeration i = loadedImages.elements();
            while(i.hasMoreElements()) {
//              ((Image)i.nextElement()).flush();
            }
         
            loadedImages.clear();
        }
        loadedImages = null;
        
        if (container != null && comp != null)
            container.remove(comp);
        container = null;
        comp = null;
        mediaListener = null;
    }
    
    public synchronized void prefetched() {
        notify();
    }

    /**
     * This moves the time position in media. JMF supports this for audio/video.
     * @param millisecs     Time in millisecs
     */
     public void setMediaTime(int millisecs) {
        if (player != null)
            player.setMediaTime(new Time((float)millisecs/1000));
     }

     /**
      * Set the sound volume for media. Only applicable for sound media formats.
      * @param percentage   Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
      * dB change in signal level = 20 log10(percentage / 100) )
      */
     public void setSoundVolume(int percentage) {
        GainControl gain = null;
        
        if (player != null)
            gain = player.getGainControl();
        if (gain == null)
            return;

        gain.setLevel(((float)percentage)/100);
     }

     /**
      * Get the duration of media. Only applicable for continuous media (audio, video).
      * @return The duration of media in millisecs, -1 is unknown.
      */
     public int getOriginalDuration() {
        if (player == null)
            return -1;
            
        Time duration = player.getDuration();
        if (duration.equals(Duration.DURATION_UNKNOWN))
            return -1;
            
        return (int)(duration.getNanoseconds()/1000);
     }

     /** 
      * Get the real width of the media. It should be known after prefetch.
      */
     public int getOriginalWidth() {
        if (comp != null) {
            Dimension d = comp.getPreferredSize();
            return d.width;
        } else
            return -1;  
     }
    
     /** 
      * Get the real height of the media. It should be known after prefetch.
      */
     public int getOriginalHeight() {
        if (comp != null) {
            Dimension d = comp.getPreferredSize();
            return d.height;
        } else
            return -1;
     }

    /**
     * Add a listener for this media.
     */ 
    public void addMediaListener(MediaListener listener) {
        mediaListener = listener;
    }

    /**
     * Requests the media player to display a control panel for media.
     * For audio and video, these can be a volume/play/stop controls,
     * for images, these can be zoom controls.
     * The controls are GUI dependent, generated through ComponentFactory.
     * @param visible   true=Display controls, false=don't display controls.
     */
    public void showControls(boolean visible) {
    }

    /**
     * All traffic to the browser, such as openLocation, etc goes through this listener.
     * If no listener supplied media players should still function with some basic level. 
     * 
     * @param listener The MLFCListener supplied by the browser
     * @see MLFCListener
     */
    public void setMLFCListener(MLFCListener listener) {
    }

    /**
     * A class to listen to JMF events and pass them to either JMFMedia or
     * MediaListener (which should be XElementBasicTimeImpl).
     */
    class PlayerListener implements ControllerListener {
        JMFMedia jmfMedia = null;
        
        public PlayerListener(JMFMedia m) {
            jmfMedia = m;
        }
        
        public void free() {
            jmfMedia = null;
        }
        
        public synchronized void controllerUpdate(ControllerEvent event) {

            Log.debug(event.toString());
            if (event instanceof RealizeCompleteEvent) {
                // automatically prefetch after realized
                player.prefetch();
            } else if (event instanceof PrefetchCompleteEvent) {
                // Save the visual component so that it can be shown
                // Audio will return value null
                comp = player.getVisualComponent();
                Log.debug("Realized + prefetched "+url.toString()+" comp: "+comp);

                // This will get all mouse events, and pass them to the MediaListener
                if (comp != null) {
                    comp.addMouseListener(jmfMedia);
                    comp.setVisible(false);
                }
                                
                // Inform the JMFMedia
                jmfMedia.prefetched();
            } else if (event instanceof ResourceUnavailableEvent) {
                Log.error("Error prefetching "+url.toString()+": "+((ResourceUnavailableEvent)event).getMessage());
                // Close the invalid player
                player.close();
                player = null;
                // Inform the JMFMedia
                jmfMedia.prefetched();
            } else if (event instanceof EndOfMediaEvent) {
                // Media ended - inform the SMIL player.
                // Calling stop() will not cause mediaEnded() to be called.
                if (mediaListener != null && playing == true)
                    mediaListener.mediaEnded();
            }
            return;
        }
    }

    /**
     * Mouse listener...
     */
    public void mouseClicked(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseClicked(e);
    }
    
    public void mouseEntered(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseEntered();
    }

    public void mouseExited(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseExited();
    }
    
    public void mousePressed(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mousePressed();
    }
    
    public void mouseReleased(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseReleased();
    }   

}