/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.general;

import java.awt.event.MouseEvent;

public interface MediaListener {

	/**
	 * Called when the media has been prefetched.
	 */
    public void mediaPrefetched();

    /**
     * Called when the media has ended.
     */
    public void mediaEnded();

	/**
	 * Mouse events.
	 */
	public void mouseClicked(MouseEvent e);
	
	public void mouseEntered();

	public void mouseExited();
	
	public void mousePressed();
	
	public void mouseReleased();

}