package fi.hut.tml.xsmiles.gui.media.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.content.BaseContentHandler;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;

//import javax.swing.border.Border;
/**
 * Handles application/xml
 * 
 * This class should be reusable for optimization purposes
 * 
 * @author Mikko Honkala
 */
public class AWTMediaContentHandler extends BaseContentHandler implements XSmilesContentHandler {

    protected Media media;

    public AWTMediaContentHandler(Media aMedia) {
        media = aMedia;
    }

    /**
     * fetches an resource
     * 
     * @param url
     *                   the documents url, used for relative references
     * @param stream
     *                   the input stream of the content
     * @param manager
     *                   the MLFCManager that could be used for this content
     */
    public void setURL(XLink url) {
        super.setURL(url);
        //Log.debug(this.toString()+": setting URL");
        //media = new TextMedia();
        //media.useStyles="false";
        media.setUrl(url.getURL());
    }
    /**
     * sets the browser window instance. This is needed mainly by the XMLContentHandler
     */
      public void setBrowserWindow(BrowserWindow browser)
      {
          super.setBrowserWindow(browser);
          this.media.setMLFCListener(browser.getMLFCListener());
      }


    public void prefetch() throws Exception {
        super.prefetch();
        media.prefetch();
    }

    public void play() throws Exception {
        super.play();
        if (this.getPrimary()) {
            this.playPrimary();
        } else {
            // secondary content
            media.play();
        }
    }

    public void stop() {
        media.stop();
        super.stop();
    }

    public void playPrimary() throws Exception {
        // primary content
        Container c = this.getContainer();
        if (c != null) {
            //Container p = this.getContainer();
            c.setBackground(Color.white);
            //JScrollPane scroll = new JScrollPane(p);
            //scroll.setLayout(new BorderLayout());
            c.setLayout(new BorderLayout());
            // Get rid of borders coming thanks to FlowLayout
            //Border empty = BorderFactory.createEmptyBorder();
            //p.setBorder(empty);
            // Get rid of borders coming thanks to ScrollLayout
            //scroll.setBorder(empty);
            //c.add(scroll);
            // TODO: move media prefetch to prefetch stage!
            media.setContainer(c);
            media.setBounds(0, 0, media.getOriginalWidth(), media.getOriginalHeight());
        }
        media.play();
    }

    /**
     * Sets the container the media will be rendered in. If media is audio,
     * this can be null. Can be called if media is already visible in one
     * container, it will then move to the new container.
     * 
     * @param container
     *                   This container will contain the media.
     */
    public void setContainer(Container container) {
        super.setContainer(container);
        media.setContainer(container);
    }

    public void close() {
        super.close();
        media.close();
    }

    /**
     * This moves the time position in media. Works only for continuous media
     * (video/audio).
     * 
     * @param millisecs
     *                   Time in millisecs
     */
    public void setMediaTime(int millisecs) {
        if (this.media != null)
            this.media.setMediaTime(millisecs);
    }

    /**
     * Adds a MediaListener for this media. The listener will be called
     * <ol>
     * <li>When the media has been prefetched. (NOT IMPLEMENTED YET).</li>
     * <li>When the media ends. (continuous media, video/audio)</li>
     * </ol>
     * Static media, such as text and images will end immediately, notifying
     * immediately about the end of media.
     */
    public void addMediaListener(MediaListener listener) {
        if (this.media != null)
            this.media.addMediaListener(listener);
    }

    /**
     * Checks if this media is static or continuous.
     * 
     * @return true if media is static (duration is zero, see
     *               getOriginalDuration().
     */
    public boolean isStatic() {
        return this.media.isStatic();
    }

    /**
     * Get the duration of media. Only applicable for continuous media (audio,
     * video).
     * 
     * @return The duration of media in millisecs. zero for static media
     *               (images, text) -1 means indefinite (infinite streamed media or
     *               unknown duration).
     */
    public int getOriginalDuration() {
        return this.media.getOriginalDuration();
    }

    /**
     * Get the real width of the media. If not visible, or size is unknown,
     * then returns -1.
     * 
     * @return Original width of the media. -1 means no particular width
     *               (audio) or unknown width (text, XML media...)
     */
    public int getOriginalWidth() {
        return this.media.getOriginalWidth();
    }

    /**
     * Get the real height of the media. If not visible, or size is unknown,
     * then returns -1.
     * 
     * @return Original height of the media. -1 means no particular height
     *               (audio) or unknown height (text, XML media...)
     */
    public int getOriginalHeight() {
        return this.media.getOriginalHeight();
    }

    /**
     * Set the coordinates for the media. These are relative to the given
     * container, set using setContainer(). Setting the bounds will always
     * immediately move the media to a new location, if it is visible.
     */
    public void setBounds(int x, int y, int width, int height) {
        if (this.media != null)
            this.media.setBounds(x, y, width, height);
    }

    /**
     * Set the sound volume for media. Only applicable for sound media formats.
     * 
     * @param percentage
     *                   Sound volume, 0-100- (0 is quiet, 100 is original loudness,
     *                   200 twice as loud; dB change in signal level = 20
     *                   log10(percentage / 100) )
     */
    public void setSoundVolume(int percentage) {
        if (this.media != null)
            this.media.setSoundVolume(percentage);
    }

    /**
     * Pauses the media. The media will stay visible, but any animations will
     * be paused. Audio media will be silent. NOT IMPLEMENTED YET. ?How to
     * restart paused media?
     */
    public void pause() {
        if (this.media != null)
            this.media.pause();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.XSmilesContentHandler#getTitle()
     */
    public String getTitle()
    {
        // TODO Auto-generated method stub
        return null;
    }
}
