package fi.hut.tml.xsmiles.gui.media.general;

import javax.media.Manager;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.jmfxine.RegisterPlayer;

/**
 * ContentHandlerFactory creates content handlers based on the content-type and
 * possibly the URI This class adds JMF dependand media handlers JMF
 * 
 * @author Mikko Honkala
 * @version $Revision: 1.5 $, $Date: 2005/01/26 12:28:55 $
 */
public class JMFContentHandlerFactory extends ContentHandlerFactory
{

    public static final String JMF_BASIC_MEDIA = "fi.hut.tml.xsmiles.gui.media.general.JMFMedia";
    public static final String JMF_XINE_MEDIA = "fi.hut.tml.xsmiles.gui.media.general.jmfxine.JMFXineMedia";
    public static String JMFMEDIA = JMF_BASIC_MEDIA;
    public final static String AWTTEXTMEDIA = "fi.hut.tml.xsmiles.gui.media.general.AWTTextMedia";
    static Class AWTTEXTMEDIA_CLASS;

    // Check if the xine classes are present
    static {
        if (RegisterPlayer.register()) {
            JMFMEDIA = JMF_XINE_MEDIA;
            Log.debug("Using JMFXine");
        } else {
            Log.debug("Using basic JMF");
        }
    }

    public JMFContentHandlerFactory(BrowserWindow bw)
    {
        super(bw);
    }

    protected void createMappings()
    {
        // the superclass creates the basic mappings, such as XML and HTML
        super.createMappings();
        Class JMFMEDIA_CLASS = null;
        try
        {
            JMFMEDIA_CLASS = Class.forName(JMFMEDIA);
            createMIMEMapping("video/mpeg", JMFMEDIA_CLASS);
            createEndingMapping("mpg", JMFMEDIA_CLASS);
            createEndingMapping("mpeg", JMFMEDIA_CLASS);
            createEndingMapping("avi", JMFMEDIA_CLASS);
            createMIMEMapping("video/*", JMFMEDIA_CLASS);
            // audio
            //createMIMEMapping("audio/mpeg",JMFMEDIA_CLASS);
            //createEndingMapping("mp3",JMFMEDIA_CLASS);
            createEndingMapping("mid", JMFMEDIA_CLASS);
            createEndingMapping("mp2", JMFMEDIA_CLASS);
            createMIMEMapping("audio/*", JMFMEDIA_CLASS);
            /*
             * createMIMEMapping("audio/x-wav",JMFMEDIA_CLASS);
             * createMIMEMapping("audio/wav",JMFMEDIA_CLASS);
             * createEndingMapping("wav",JMFMEDIA_CLASS);
             */
        } catch (Throwable e)
        {
            Log.error("No JMF found. (" + e.toString()
                    + ") .Running without it.");
        }

        // Set JMF light/heavy weight video MPEG player
        isJMFAvailable();
    }
    
    protected void createTextMappings()
    {
        // the superclass creates the basic mappings, such as XML and HTML
        try
        {
            AWTTEXTMEDIA_CLASS = Class.forName(AWTTEXTMEDIA);

            //text
            createMIMEMapping("text/*", AWTTEXTMEDIA_CLASS);
            createEndingMapping("txt", AWTTEXTMEDIA_CLASS);
            createEndingMapping("css", AWTTEXTMEDIA_CLASS);
        } catch (ClassNotFoundException e)
        {
            Log
                    .error(e,
                            "ContentHandlerFactory could not find content handler class");
        }
    }



    /**
     * Check if JMF class is available. At the same time, set the hint...
     * 
     * @return true if JMF is available, false otherwise
     */
    private boolean isJMFAvailable()
    {
        try
        {
            Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true));
            // If we are in lightweight mode, then enable modular
            // plugin-players
            if (browserWindow.getBrowserConfigurer().getProperty(
                    "media/lightweight").equals("true"))
            {
                Manager.setHint(Manager.PLUGIN_PLAYER, new Boolean(true));
            } else
            {
                Manager.setHint(Manager.PLUGIN_PLAYER, new Boolean(false));
            }
        } catch (Throwable e)
        {
            Log
                    .info("JMF not available! It is recommended to have it installed.");
            Log.info("Video will not be played.");
            return false;
        }
        return true;
    }

    /** copied from SwingContentHandlerFactory */
    protected XSmilesContentHandler createContentHandler(Class a_class,
            String contentType, XLink url) throws InstantiationException,
            IllegalAccessException
    {
        Class cl = a_class;
        // KLUDGE
        // Handle "data:" - protocol differently if type is text/html (use
        // textplayer)
        /*
         * if (url.getURL().toString().startsWith("data:") &&
         * contentType.equals("text/html")) { cl = TEXTMEDIA_CLASS; Object
         * instance = cl.newInstance(); return new
         * AWTMediaContentHandler((Media) instance); }
         */
        try
        {
            // the super class handles basic types, such as XML and HTML
            return super.createContentHandler(cl, contentType, url);
        } catch (IllegalAccessException e)
        {
            Object instance = cl.newInstance();
            // TODO: move media to content package!
            if (instance instanceof Media)
            {
                return this.createMediaContentHandler((Media)instance);
            } else
            {
                throw new IllegalAccessException("Illegal content handler");
                //return null;
            }
        }
    }
    
    protected XSmilesContentHandler createMediaContentHandler(Media instance)
    {
        return new AWTMediaContentHandler((Media) instance);
    }

}
