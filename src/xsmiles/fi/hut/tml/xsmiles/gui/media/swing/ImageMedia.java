/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.swing;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JComponent;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * This is the implementation of image media.
 */
public class ImageMedia implements Media, Runnable, MouseListener {

	// Hashtable holding loaded images - this is a proxy
	private Hashtable loadedImages;

	// JImageCanvas for current image
	JImageCanvas jcanvas;

	// Container for media (DrawingArea)
	Container container = null;

	// MediaListener - called after prefetch and endOfMedia.
	MediaListener mediaListener = null;

	// Location and coords for the media
	int x = 0, y = 0, width = 0, height = 0;

	Image pic = null;
	URL url = null;

	public ImageMedia() {
		// Create the components
		jcanvas = new JImageCanvas();
		jcanvas.setSize(300, 300);

		// Create the proxy hashtable
		loadedImages = new Hashtable();

		// This will get all mouse events, and pass them to the MediaListener
		jcanvas.addMouseListener(this);
	}

	/**
	 * Checks if this media is static or continuous.
	 * 
	 * @return true if media is static.
	 */
	public boolean isStatic() {
		return true;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public void prefetch() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		pic = tk.getImage(url);
		MediaTracker mt = new MediaTracker(jcanvas);
		mt.addImage(pic, 1);
		try {
			mt.waitForID(1);
		} catch (InterruptedException e) {
			Log.error(e);
		}
		// Add image to the proxy
		loadedImages.put(url.toString(), pic);

		jcanvas.setImage(pic);
		jcanvas.setVisible(false);
	}

	/**
	 * Set the container. Must be dynamic, the media must move if container
	 * changes.
	 */
	public void setContainer(Container container) {
		if (this.container != container) {

			// Change container
			if (jcanvas.isVisible() == true) {
				jcanvas.setVisible(false);
				if (this.container != null)
					this.container.remove(jcanvas);
				container.add(jcanvas, 0);
				jcanvas.setVisible(true);
				if (this.container != null)
					this.container.doLayout(); // CSS requires layout
														 // to be reapplied.
				container.doLayout(); // CSS requires layout to be
											   // reapplied.
			}

			this.container = container;
		}
	}

	/**
	 * Displays the image media. Also calls mediaEnded() immediately - this is
	 * static media, and therefore will "freeze" immediately.
	 */
	public void play() {
		if (container != null) {
			container.add(jcanvas, 0); // CSS: Was ZERO to bring the
												  // latest image on top!!
			jcanvas.setLocation(x, y);
			jcanvas.setSize(width, height);
			jcanvas.setVisible(true);
			container.doLayout(); // CSS requires layout to be
										   // reapplied.
		} else
			Log.error("Container not set for media " + url.toString());

		// Media ends immediately for static media (almost..).
		// Media ended - inform the SMIL player.
		if (mediaListener != null) {
			Thread t = new Thread(this);
			t.start();
		}

	}

	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		// Media ended immediately (almost...)
		if (mediaListener != null)
			mediaListener.mediaEnded();
	}

	public void pause() {
	}

	public void stop() {
		if (jcanvas != null) {
			jcanvas.setVisible(false);
			if (container != null) {
				container.remove(jcanvas);
				container.doLayout(); // CSS requires layout to be
											   // reapplied.
			}
		}

		// Clear media listeners
		if (mediaListener != null)
			mediaListener = null;

	}

	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;

		this.width = width;
		this.height = height;

		// If the image is shown on the screen, move it immediately
		if (jcanvas!=null && jcanvas.isVisible()) {
			// Use the given coordinates
			jcanvas.setLocation(x, y);
			jcanvas.setSize(width, height);
		}
	}

	/**
	 * Get the real width of the media.
	 */
	public int getOriginalWidth() {
		if (pic != null)
			return pic.getWidth(jcanvas);
		else
			return -1;
	}

	/**
	 * Get the real height of the media.
	 */
	public int getOriginalHeight() {
		if (pic != null)
			return pic.getHeight(jcanvas);
		else
			return -1;
	}

	public void close() {
		stop();

		if (container != null) {
			container.remove(jcanvas);
			container.doLayout(); // CSS requires layout to be
										   // reapplied.
		}

		// Flush all images
		if (loadedImages != null) {
			Enumeration i = loadedImages.elements();
			while (i.hasMoreElements()) {
				((Image) i.nextElement()).flush();
			}

			loadedImages.clear();
		}
		loadedImages = null;
		jcanvas = null;
		mediaListener = null;
	}

	/**
	 * This moves the time position in media. Not effective for this media.
	 * 
	 * @param millisecs
	 *                  Time in millisecs
	 */
	public void setMediaTime(int millisecs) {
	}

	/**
	 * Set the sound volume for media. Does nothing for this media player.
	 * 
	 * @param percentage
	 *                  Not used.
	 */
	public void setSoundVolume(int percentage) {
	}

	/**
	 * Get the duration of media. Only applicable for continuous media (audio,
	 * video).
	 * 
	 * @return The duration of media in millisecs.
	 */
	public int getOriginalDuration() {
		return 0;
	}

	public void addMediaListener(MediaListener listener) {
		mediaListener = listener;
	}

	/**
	 * Requests the media player to display a control panel for media. For
	 * audio and video, these can be a volume/play/stop controls, for images,
	 * these can be zoom controls. The controls are GUI dependent, generated
	 * through ComponentFactory.
	 * 
	 * @param visible
	 *                  true=Display controls, false=don't display controls.
	 */
	public void showControls(boolean visible) {
		if (visible == true) {
			XPanel cb = mlfcListener.getMLFCControls().getMLFCToolBar();
			ComponentFactory cf = mlfcListener.getComponentFactory();
			cb.removeAll();
			Vector v = new Vector();
			selectOne = cf.getXSelectOne("minimal", false);
			selectOne.addSelection("100%");
			selectOne.addSelection("200%");
			selectOne.setSelected("100%");
			selectOneListener = new ScaleListener();
			selectOne.addItemListener(selectOneListener);
			cb.add(selectOne);
			Log.debug("DISPLAYING CONTROLS!");
		}
	}
	private ScaleListener selectOneListener;
	private XSelectOne selectOne;

	private class ScaleListener implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.DESELECTED)
				return;
			String scaleStr = (String) e.getItem();
			if (scaleStr.equals("100%"))
				setBounds(0, 0, getOriginalWidth(), getOriginalHeight());
			if (scaleStr.equals("200%"))
				setBounds(0, 0, getOriginalWidth() * 2, getOriginalHeight() * 2);
		}
	}

	/**
	 * All traffic to the browser, such as openLocation, etc goes through this
	 * listener. If no listener supplied media players should still function
	 * with some basic level.
	 * 
	 * @param listener
	 *                  The MLFCListener supplied by the browser
	 * @see MLFCListener
	 */
	public void setMLFCListener(MLFCListener listener) {
		mlfcListener = listener;
	}
	private MLFCListener mlfcListener;

	/**
	 * Mouse listener...
	 */
	public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}

	public void mouseEntered(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseEntered();
	}

	public void mouseExited(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseExited();
	}

	public void mousePressed(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mousePressed();
	}

	public void mouseReleased(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseReleased();
	}

	/**
	 * Simple class to draw images. This is required to scale the images.
	 * (ImageIcon couldn't handle it)
	 */
	public class JImageCanvas extends JComponent {
		int width = 0;
		int height = 0;
		Image image = null;

		public JImageCanvas() {
		}

		public void setImage(Image i) {
			image = i;
		}

		public void setSize(int w, int h) {
			width = w;
			height = h;
			super.setSize(w, h);
			super.setPreferredSize(new Dimension(w, h));
		}

		public void paint(Graphics g) {
			// Draw image
			if (image != null)
				g.drawImage(image, 0, 0, width, height, null, this);

			return;
		}
	}

}
