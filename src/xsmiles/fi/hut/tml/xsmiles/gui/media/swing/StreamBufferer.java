/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution please refer to the LICENSE_XSMILES file
 * included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.swing;

import java.io.*;
import java.math.BigDecimal;

import fi.hut.tml.xsmiles.Log;

/**
 * Class StreamBufferer
 * 
 * @author tjjalava
 * @since Dec 17, 2003
 * @version $Revision: 1.10 $, $Date: 2004/05/18 12:09:20 $
 */
public class StreamBufferer extends InputStream {

    private StreamReader reader;
    protected int bytesRead = 0;
    protected static final int MAX_BUFFER_SIZE = 200000;
    protected static final int MIN_BUFFER_SIZE = 64000;
    protected static final int START_LIMIT = 64000;
    private ByteBuffer byteBuffer;
    private ByteArrayInputStream savedBufferStream = new ByteArrayInputStream(new byte[0]);
    private ByteArrayOutputStream saveStream = new ByteArrayOutputStream();
    private byte[] savedBuffer = new byte[MAX_BUFFER_SIZE];
    private int savedBufferSize = 0;
    private ByteArrayInputStream markBufferStream = new ByteArrayInputStream(new byte[0]);
    private ByteArrayOutputStream markOutBuffer;
    private int readPosition = 0;
    private boolean markSet = false;
    private int markPosition;
    private int markLimit;
    private boolean running = true;
    protected PrintStream debugStream = null;
    private Thread debug = null;
    private boolean closed = false;

    private class ByteBuffer {

        private byte[] buffer;
        private int in = 0;
        private int out = 0;
        private int startLimitCounter = 0;
        private boolean startLimitRead = false;
        private boolean writerFinished = false;
        private boolean bufferFinished = false;
        private int errorcount = 0;

        public ByteBuffer() {
            buffer = new byte[MAX_BUFFER_SIZE];
        }

        public synchronized int getBufferSize() {
            int size = out - in;
            if (size < 0) {
                size = out + MAX_BUFFER_SIZE - in;
            }
            return size;
        }

        public String getStatus() {
            return "size: " + getBufferSize() + " in=" + in + " out=" + out;
        }

        public synchronized void finishWriting() {
            writerFinished = true;
            notifyAll();
        }

        public synchronized void write(int b) {
            while (running && getBufferSize() == MAX_BUFFER_SIZE - 1) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            buffer[out] = (byte) b;
            out = (++out) % MAX_BUFFER_SIZE;
            if (!startLimitRead) {
                startLimitRead = (++startLimitCounter) >= START_LIMIT;
            }
            notifyAll();
        }

        public synchronized void write(byte[] b, int off, int len) {
            while (len-- > 0) {
                write(b[off++]);
            }
        }

        private synchronized int readByte() {
            return -1;
        }

        public synchronized void fillBuffer() {
            while (!writerFinished && getBufferSize() < MIN_BUFFER_SIZE) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }

        public synchronized int read() {
            if (bufferFinished) {
                return -1;
            }
            while (startLimitRead && !writerFinished && getBufferSize() < MIN_BUFFER_SIZE) {
                try {
                    if ((errorcount++) % 20 == 0) {
                        System.err.println("Audio buffer underrun! Increase buffer size");
                    }
                    wait();
                } catch (InterruptedException e) {
                }
            }

            while (!writerFinished && in == out) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }

            if (writerFinished && in == out) {
                bufferFinished = true;
                return -1;
            }

            int read = buffer[in] & 0xFF;
            in = (++in) % MAX_BUFFER_SIZE;
            notifyAll();
            return read;
        }
    }

    private class StreamReader extends Thread {

        private InputStream stream;
        private boolean running = false;
        private long toBeSkipped = 0;
        private boolean contentsFetched = false;

        public StreamReader(InputStream input) {
            super("StreamReader");
            stream = input;
        }

        public void skip(long n, long wholeContents) {
            if (!running) {
                if (wholeContents != -1 && n >= wholeContents) {
                    contentsFetched = true;
                } else {
                    toBeSkipped = n;
                }
            }
        }

        public int available() throws IOException {
            return stream.available();
        }

        private int lastReadBytes = 0;
        private long lastTime = -1;

        public void printReadBytes() {
            if (debugStream != null) {
                int size = byteBuffer.getBufferSize();
                long timeNow = -1;
                int bytesNow = -1;
                int upper = (bytesNow = bytesRead) - lastReadBytes;
                long lower = ((timeNow = System.currentTimeMillis()) - lastTime);
                String speed = new BigDecimal((double) upper / lower).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                debugStream.print("\rBytes Read: " + bytesRead + " - Buffer usage: size: " + size + " Min: " + (int) ((double) size / MIN_BUFFER_SIZE * 100)
                        + "% Max: " + +(int) ((double) size / MAX_BUFFER_SIZE * 100) + "% speed: " + speed + " KB/s   ");
                lastReadBytes = bytesNow < 0 ? bytesRead : bytesNow;
                lastTime = timeNow < 0 ? System.currentTimeMillis() : timeNow;
            }
        }

        public boolean isRunning() {
            return running;
        }

        public void stopReader() {
            running = false;
            interrupt();
        }

        public void run() {
            running = true;
            if (!contentsFetched) {
                try {
                    int read = 0;
                    byte[] b = new byte[128000];
                    while (running && toBeSkipped > 0) {
                        read = stream.read(b, 0, (int) Math.min(b.length, toBeSkipped));
                        if (read == -1) {
                            break;
                        } else {
                            toBeSkipped -= read;
                        }
                    }
                    while (running && (read = stream.read(b, 0, b.length)) >= 0) {
                        synchronized (this) {
                            byteBuffer.write(b, 0, read);
                            notifyAll();
                        }
                        bytesRead += read;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            running = false;
            byteBuffer.finishWriting();
        }
    }

    public StreamBufferer(InputStream input) throws IOException {
        byteBuffer = new ByteBuffer();
        running = true;
        reader = new StreamReader(input);
        reader.start();
    }

    public StreamBufferer(InputStream input, boolean block) throws IOException {
        this(input);
        if (block) {
            byteBuffer.fillBuffer();
        }
    }

    public void restart(InputStream input, int contentLength) throws IOException {
        markBufferStream = new ByteArrayInputStream(new byte[0]);
        readPosition = 0;
        markSet = false;
        running = true;
        closed = false;
        reader = new StreamReader(input);
        byteBuffer = new ByteBuffer();
        if (contentLength<=0) {
            Log.debug("ContentLength: "+contentLength+" no buffer");
            savedBufferStream = new ByteArrayInputStream(new byte[0]);
            saveStream = new ByteArrayOutputStream();
        } else {
            Log.debug("ContentLength: "+contentLength+" read from buffer");
            savedBufferStream = new ByteArrayInputStream(saveStream.toByteArray());
            reader.skip(savedBufferStream.available(), contentLength);
        }
        running = true;
        reader.start();
    }
    
    public int read() throws IOException {
        int read;
        if (markBufferStream.available() > 0) {
            read = markBufferStream.read();            
        } else {
            if (savedBufferStream.available() > 0) {
                read = savedBufferStream.read();
            } else {
                read = byteBuffer.read();
                if (read != -1 && readPosition < MAX_BUFFER_SIZE) {
                    saveStream.write(read);
                }
            }
        }
        readPosition++;
        if (markSet) {
            if (markPosition >= markLimit) {
                markSet = false;
            } else {
                markOutBuffer.write(read);
                markPosition++;
            }
        }
        return read;
    }

    public synchronized void mark(int readlimit) {
        markSet = true;
        markOutBuffer = new ByteArrayOutputStream();
        markPosition = 0;
        markLimit = readlimit;

    }

    public synchronized void reset() throws IOException {
        if (markSet) {
            int avail = markBufferStream.available();
            byte[] b = markOutBuffer.toByteArray();
            byte[] tmp = new byte[b.length + avail];
            System.arraycopy(b, 0, tmp, 0, b.length);
            markBufferStream.read(tmp, b.length, avail);
            markBufferStream = new ByteArrayInputStream(tmp);
            markSet = false;
            if (readPosition == savedBufferSize) {
                savedBufferSize -= markPosition;
            }
            readPosition -= markPosition;
        }
    }

    public int available() throws IOException {
        return reader.available();
    }

    public void close() throws IOException {
        reader.stopReader();
        running = false;
        if (debug != null) {
            debug.interrupt();
        }
        closed = true;
    }

    public boolean markSupported() {
        return true;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (len == 0) {
            return 0;
        }
        int read = -1;
        int count = 0;
        while (len-- > 0 && (read = read()) >= 0) {
            b[off++] = (byte) read;
            count++;
        }
        if (read == -1 && count == 0) {
            return -1;
        } else {
            return count;
        }
    }

    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    public long skip(long n) throws IOException {
        long count = 0;
        while (n-- > 0 && read() >= 0) {
            count++;
        }
        return count;
    }
}