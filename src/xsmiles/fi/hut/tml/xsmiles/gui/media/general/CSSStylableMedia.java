/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.general;

import org.w3c.dom.css.CSSStyleDeclaration;

/**
 * This is the interface for CSS stylable media. The methods should be called in the following
 */
public interface CSSStylableMedia
{
	/** set the resolved CSS style for this element */
	public void setStyle(CSSStyleDeclaration style);
}