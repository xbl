/*
 * Created on 08.02.2003
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code Template
 */
package fi.hut.tml.xsmiles.gui.media.general.jmfxine;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.media.PackageManager;

import com.sun.media.MimeManager;

import fi.hut.tml.xsmiles.Log;

/**
 * @author benjes
 */
public class RegisterPlayer {

    private static final String XINE_PLAYER = "org.limmbo.jmf.media.content.JMFXinePlayer";

    static Hashtable mimeTypes;

    private static boolean registered = false;

    public static boolean register() {
        if (!registered) {
            try {
                Class.forName(XINE_PLAYER);
            } catch (ClassNotFoundException e1) {
                Log.debug("No JMFXinePlayer found");
                return false;
            }
            
            Vector content = PackageManager.getContentPrefixList();
            Vector source = PackageManager.getProtocolPrefixList();
            String prefix = new String("org.limmbo.jmf");
            Enumeration e = content.elements();
            while (e.hasMoreElements())
                Log.debug((String) e.nextElement());
            if (!content.contains(prefix)) {
                Log.debug("Adding org.limmbo.jmf to Content Prefix list");
                content.add(0, prefix);
                PackageManager.setContentPrefixList(content);
                PackageManager.commitContentPrefixList();
            }
            if (!source.contains(prefix)) {
                Log.debug("Adding org.limmbo.jmf to Protocol Prefix list");
                source.add(0, prefix);
                PackageManager.setProtocolPrefixList(source);
                PackageManager.commitProtocolPrefixList();
            }
            addMimeTypes();
            registered = true;
        }
        return registered;
    }

    private static void addExtension(String ext, String mimeType) {
        Log.debug("Adding extension " + ext + " mime type " + mimeType);
        if (!mimeTypes.containsKey(ext))
            if (!MimeManager.addMimeType(ext, mimeType))
                Log.debug("Adding mime type " + ext + " " + mimeType + " failed");

    }

    private static void listTypes(Hashtable table) {
        Enumeration e = table.keys();
        while (e.hasMoreElements()) {
            String ext = (String) e.nextElement();
            Log.debug("Ext " + ext + " mime type " + table.get(ext));
        }

    }

    private static void addMimeTypes() {
        mimeTypes = MimeManager.getMimeTable();
        // listTypes(mimeTypes);
        addExtension("ogg", "audio/x_ogg");
        addExtension("vob", "video/mpeg");
        addExtension("mpv", "video/mpeg");
        addExtension("mpg", "video/mpeg");
        addExtension("divx", "video/x_msvideo");
        MimeManager.commit();
        listTypes(MimeManager.getMimeTable());

    }

}
