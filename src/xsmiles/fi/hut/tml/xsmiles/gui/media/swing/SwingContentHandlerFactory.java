package fi.hut.tml.xsmiles.gui.media.swing;

import javax.media.Manager;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.gui.media.general.JMFContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.media.general.Media;

/**
 * ContentHandlerFactory creates content handlers based on the content-type and possibly the URI This class adds swing dependand media handlers
 * 
 * @author Mikko Honkala
 * @version $Revision: 1.17 $, $Date: 2005/01/26 12:28:55 $
 */
public class SwingContentHandlerFactory extends JMFContentHandlerFactory {

    public final static String TEXTMEDIA = "fi.hut.tml.xsmiles.gui.media.swing.TextMedia";
    public final static String IMAGEMEDIA = "fi.hut.tml.xsmiles.gui.media.swing.ImageMedia";
    public final static String JMFMEDIA = "fi.hut.tml.xsmiles.gui.media.general.JMFMedia";
    // public final static String OGGMEDIA =
    // "fi.hut.tml.xsmiles.gui.media.swing.OggMedia";
    public final static String NATIVEMEDIA = "fi.hut.tml.xsmiles.gui.media.swing.NativeMedia";
    public final static String AUDIOMEDIA = "fi.hut.tml.xsmiles.gui.media.swing.AudioMedia";

    public SwingContentHandlerFactory(BrowserWindow bw) {
        super(bw);
    }

    static Class TEXTMEDIA_CLASS;

    protected void createMappings() {
        // the superclass creates the basic mappings, such as XML and HTML
        super.createMappings();
        try {
            final Class IMAGEMEDIA_CLASS = Class.forName(IMAGEMEDIA);

            // image
            createMIMEMapping("image/jpeg", IMAGEMEDIA_CLASS);
            createMIMEMapping("image/gif", IMAGEMEDIA_CLASS);
            createMIMEMapping("image/png", IMAGEMEDIA_CLASS);
            createEndingMapping("png", IMAGEMEDIA_CLASS);
            createEndingMapping("gif", IMAGEMEDIA_CLASS);
            createEndingMapping("jpg", IMAGEMEDIA_CLASS);
            createEndingMapping("jpeg", IMAGEMEDIA_CLASS);

            // video is done in superclass

            try {
                final Class AUDIOMEDIA_CLASS = Class.forName(AUDIOMEDIA);
                createMIMEMapping("audio/mpeg", AUDIOMEDIA_CLASS);
                createEndingMapping("mp3", AUDIOMEDIA_CLASS);
                createMIMEMapping("audio/x-vorbis", AUDIOMEDIA_CLASS);
                createMIMEMapping("application/x-ogg", AUDIOMEDIA_CLASS);
                createEndingMapping("ogg", AUDIOMEDIA_CLASS);
                createMIMEMapping("audio/x-wav", AUDIOMEDIA_CLASS);
                createMIMEMapping("audio/wav", AUDIOMEDIA_CLASS);
                createEndingMapping("wav", AUDIOMEDIA_CLASS);
            } catch (Throwable e) {
                Log.error("Couldn't start audio. (" + e.toString() + ") .Running without it.");
                /*
                 * if (JMFMEDIA_CLASS != null) { createMIMEMapping("audio/x-wav", JMFMEDIA_CLASS); createMIMEMapping("audio/wav", JMFMEDIA_CLASS);
                 * createEndingMapping("wav", JMFMEDIA_CLASS); }
                 */
            }

            // OGG
            /*
             * try { final Class OGGMEDIA_CLASS=Class.forName(OGGMEDIA); createMIMEMapping("audio/x-vorbis",OGGMEDIA_CLASS);
             * createMIMEMapping("application/x-ogg",OGGMEDIA_CLASS); createEndingMapping("ogg",OGGMEDIA_CLASS); } catch (Throwable e) { Log.error("No OGG
             * found. ("+e.toString()+") .Running without it.");
             */

            // native
            try {
                final Class NATIVEMEDIA_CLASS = Class.forName(NATIVEMEDIA);
                createMIMEMapping("application/pdf", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/powerpoint", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/word", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/excel", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/mspowerpoint", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/msword", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/msexel", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/x-msword", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/x-msexcel", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/vnd.ms-word", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/vnd.ms-excel", NATIVEMEDIA_CLASS);
                createMIMEMapping("application/vnd.ms-powerpoint", NATIVEMEDIA_CLASS);

                createMIMEMapping("application/rtf", NATIVEMEDIA_CLASS);
                createEndingMapping("doc", NATIVEMEDIA_CLASS);
                createEndingMapping("ppt", NATIVEMEDIA_CLASS);
                createEndingMapping("pdf", NATIVEMEDIA_CLASS);
            } catch (Throwable e) {
                Log.error("No native media on this platform.");
            }

        } catch (ClassNotFoundException e) {
            Log.error(e, "ContentHandlerFactory could not find content handler class");
        }

        // Set JMF light/heavy weight video MPEG player
        isJMFAvailable();
    }

    protected void createTextMappings() {
        // the superclass creates the basic mappings, such as XML and HTML
        try {
            TEXTMEDIA_CLASS = Class.forName(TEXTMEDIA);

            // text
            createMIMEMapping("text/*", TEXTMEDIA_CLASS);
            createEndingMapping("txt", TEXTMEDIA_CLASS);
            createEndingMapping("css", TEXTMEDIA_CLASS);
        } catch (ClassNotFoundException e) {
            Log.error(e, "ContentHandlerFactory could not find content handler class");
        }
    }

    /**
     * Check if JMF class is available. At the same time, set the hint...
     * 
     * @return true if JMF is available, false otherwise
     */
    private boolean isJMFAvailable() {
        try {
            Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true));
            // If we are in lightweight mode, then enable modular
            // plugin-players
            if (browserWindow.getBrowserConfigurer().getProperty("media/lightweight").equals("true")) {
                Manager.setHint(Manager.PLUGIN_PLAYER, new Boolean(true));
            } else {
                Manager.setHint(Manager.PLUGIN_PLAYER, new Boolean(false));
            }
        } catch (Throwable e) {
            Log.info("JMF not available! It is recommended to have it installed.");
            Log.info("Video will not be played.");
            return false;
        }
        return true;
    }

    /**
     * This returns just the bare content type. e.g. for "text/html; charset = xxx", it would return "text/html"
     */
    protected String getBareContentType(String contentType) {
        int pos = contentType.indexOf(';');
        if (pos < 0)
            return contentType;
        return contentType.substring(0, pos);
    }

    protected XSmilesContentHandler createContentHandler(Class cl, String contentType, XLink url) throws InstantiationException, IllegalAccessException {
        // KLUDGE
        // Handle "data:" - protocol differently if type is text/html (use
        // textplayer)
        if (url.getURL().toString().startsWith("data:") && contentType.equals("text/html")) {
            cl = TEXTMEDIA_CLASS;
            Object instance = cl.newInstance();
            return new MediaContentHandler((Media) instance);
        }

        Object instance = cl.newInstance();
        try {
            //  the super class handles basic types, such as XML and HTML
            return super.createContentHandler(instance);
        } catch (IllegalAccessException e) {            
            if (instance instanceof Media) {
                return new MediaContentHandler((Media) instance);
            } else {                
                throw new IllegalAccessException("Illegal content handler");
            }
        }
    }

    protected XSmilesContentHandler createMediaContentHandler(Media instance) {
        return new MediaContentHandler(instance);
    }

}
