/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.swing;

import java.awt.Container;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Vector;

import javax.sound.sampled.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * General implementation for playing audio media, utilizing the
 * javax.sound-technology. Different audio formats can be added by implementing
 * the correct provider and registering AudioMedia as the contents content
 * handler.
 * 
 * @author tjjalava
 * @since Dec 16, 2003
 * @version $Revision: 1.13 $, $Date: 2004/05/17 15:28:11 $
 */
public class AudioMedia implements Media, Runnable {

    private boolean initDone = false;

    /**
     * Size of the byte buffer used while reading the source.
     */
    protected static final int BUFFER_SIZE = 1280;

    /**
     * When CACHE_MIXER is set, retrieve the default mixer when class is
     * instantiated. It can speed up the initialization of the audio system.
     */
    private static final boolean CACHE_MIXER = true;
    private static Mixer mixer;    
    private Thread pauseThread;

    // The url to the media
    private URL url;

    // For the listeners
    private Vector mediaListeners = new Vector();

    // Stream from which the audio is read
    private AudioInputStream audioInput;

    // Connection to the url
    private InputStream urlStream;

    // Format of the media
    private AudioFormat format;

    // Dataline for the media source
    private SourceDataLine line;

    // state of the player
    private boolean playing = false;
    private boolean paused = false;

    // thread handling the playback of the media
    private Thread player;

    private InputStream input;

    private boolean prefetchDone;
    
    private PauseMutex pauseMutex = new PauseMutex();

    private boolean inputReady;

    private int contentLength;
    
    private class PauseMutex {
        
        private boolean paused = false;
        
        public synchronized void checkPaused() {
            while (paused) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
        
        public synchronized void setPaused(boolean paused) {
            this.paused = paused;
            notifyAll();
        }
    }

    public AudioMedia() {
        if (CACHE_MIXER && mixer == null) {
            mixer = AudioSystem.getMixer(null);
        }
    }

    public void setMLFCListener(MLFCListener listener) {
    }

    public boolean isStatic() {
        return false;
    }

    public int getOriginalDuration() {
        return 0;
    }

    public int getOriginalWidth() {
        return 0;
    }

    public int getOriginalHeight() {
        return 0;
    }

    public void setUrl(URL url) {
        //Log.enable_debug = true;
        this.url = url;
    }

    public void setContainer(Container container) {
    }

    public void setBounds(int x, int y, int width, int height) {
    }

    public void setSoundVolume(int percentage) {
    }

    private void createInputStream(boolean block) throws IOException {
        if (inputReady) {
            return;
        }
        
        if (urlStream == null) {
            try {
                URLConnection conn = url.openConnection();
                contentLength = conn.getContentLength();
                urlStream = conn.getInputStream();                
            } catch (Throwable t) {
                throw new IOException("Can't connect to: " + url);
            }
        }

        if (input != null) {
            ((StreamBufferer)input).restart(urlStream, contentLength);
        } else {
            input = new StreamBufferer(urlStream, block);
        }
        inputReady = true;
    }

    public void prefetch() throws Exception {
        if (prefetchDone) {
            return;
        }
        createInputStream(true);
        prefetchDone = true;
        for (Enumeration e = mediaListeners.elements(); e.hasMoreElements();) {
            try {
                ((MediaListener) e.nextElement()).mediaPrefetched();
            } catch (Throwable t) {
            }
        }
    }

    public synchronized void play() throws IOException {        
        Log.debug("play() called");
        Log.debug("Playing " + url.toString());
        try {
            initAudio();
        } catch (Exception e) {
            Log.error(e.getMessage());
            return;
        }
        player = new Thread(this, "AudioThread");
        player.start();
    }

    public void pause() {        
        paused = !paused;
        pauseMutex.setPaused(paused);
    }

    public synchronized void stop() {
        Log.debug("stop() called");
        playing = false;
        if (paused) {
            pause();
        }                
        //finalizeAudio();        
        //mediaListeners = new Vector();
        notifyAll();
    }

    public void close() {
        Log.debug("close() called");
        stop();
        try {
            player.join();
        } catch (Throwable e) {
        }
    }

    public void setMediaTime(int millisecs) {
    }

    public void addMediaListener(MediaListener listener) {
        mediaListeners.add(listener);
    }

    public void showControls(boolean visible) {
    }

    /**
     * Initialized the playback engine by converting the input to an
     * appropriate format and creating an AudioInputStream from the input.
     * 
     * @param input
     *                   Stream from which the audio is read
     * @throws UnsupportedAudioFileException
     * @throws IOException
     * @throws LineUnavailableException
     */
    protected synchronized void initAudio() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        //Log.enable_debug = true;
        // Create a buffer for the stream
        if (initDone) {
            return;
        }

        createInputStream(false);

        audioInput = AudioSystem.getAudioInputStream(input);
        format = audioInput.getFormat();
        Log.debug("Play input audio format=" + format);
        int sampleSize = format.getSampleSizeInBits();
        if (sampleSize == AudioSystem.NOT_SPECIFIED) {
            sampleSize = 16;
        }

        // Convert the audio to PCM_SIGNED
        if (format.getEncoding() != AudioFormat.Encoding.PCM_SIGNED) {
            AudioFormat newFormat = new AudioFormat(format.getSampleRate(), sampleSize, format.getChannels(), true, false);
            Log.debug("Converting audio format to " + newFormat);
            AudioInputStream newStream;
            try {
                newStream = AudioSystem.getAudioInputStream(newFormat, audioInput);
                format = newFormat;
                audioInput = newStream;
            } catch (IllegalArgumentException e) {
                Log.debug("Conversion failed, reason: " + e.getMessage() + ", let's try with the original format");
            }
        }

        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
        long now = System.currentTimeMillis();

        // Getting line from AudioSystem can be very slow on some platforms
        // In that case we can use cached Mixer instance, see CACHE_MIXER-field
        if (mixer == null) {
            line = (SourceDataLine) AudioSystem.getLine(info);
        } else {
            line = (SourceDataLine) mixer.getLine(info);
        }
        Log.debug("Line took: " + (System.currentTimeMillis() - now));

        line.open(format);
        line.start();
        initDone = true;
        //Log.enable_debug=false;
    }

    /**
     * Shuts down the audio stream and releases all resources associated with
     * it.
     */
    protected synchronized void finalizeAudio() {
    	long now = System.currentTimeMillis();
        try {
            line.stop();
            line.flush();
            line.close();
            input.close();
            audioInput.close();
        } catch (Throwable e) {
        } finally {
            prefetchDone = false;
            playing = false;
            inputReady = false;
            urlStream = null;
            initDone = false;
            Log.debug("FinalizeAudio took "+(System.currentTimeMillis()-now));
        }        
    }

    public void run() {
        synchronized (this) {
            playing = true;
        }

        Log.debug("Starting player");
        try {
            int bytesRead = 0;
            byte[] buffer = new byte[BUFFER_SIZE];
            prefetchDone = false; // prefetched audio is lost,
            // when play starts
            while (playing && (bytesRead = audioInput.read(buffer, 0, buffer.length)) >= 0) {
                if (paused) {
                    line.stop();
                    pauseMutex.checkPaused();
                    if (!playing) {
                        break;
                    }
                    line.start();
                }
                line.write(buffer, 0, bytesRead);
            }
            if (playing) {
                line.drain();
            }
            finalizeAudio();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Enumeration e = mediaListeners.elements(); e.hasMoreElements();) {
            try {
                ((MediaListener) e.nextElement()).mediaEnded();
            } catch (Throwable t) {
            }
        }
        Log.debug("Stopped playing " + url.toString());
    }    
}
