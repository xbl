/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.media.general.jmfxine;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URL;
import java.util.EventListener;

import javax.media.Controller;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.Duration;
import javax.media.EndOfMediaEvent;
import javax.media.GainControl;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.media.Time;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * Class JMFMedia2
 * 
 * @author tjjalava
 * @since Jan 13, 2005
 * @version $Revision: 1.2 $, $Date: 2005/01/26 12:28:54 $
 */
public class JMFXineMedia implements Media, MouseListener {

    private Player player;
    private boolean paused = false;
    private boolean playing = false;
    private Component jmfCanvas;
    private URL url;
    private Container container;
    private MediaListener mediaListener;
    private int x = 0;
    private int y = 0;
    private int width = 720;
    private int height = 576;    
    private boolean canvasAdded = false;

    
    /**
     * A class to listen to JMF events and pass them to either JMFMedia or MediaListener (which should be XElementBasicTimeImpl).
     */
    class JMFListener implements ControllerListener, EventListener {

        Media jmfMedia = null;

        public JMFListener(Media m) {
            jmfMedia = m;
        }

        public void free() {
            jmfMedia = null;
        }
        
        public synchronized void controllerUpdate(ControllerEvent event) {
            Log.debug(event.toString());
            if (event instanceof EndOfMediaEvent) {
                player.close();    
                if (mediaListener != null && playing == true) {
                    mediaListener.mediaEnded();
                }
                Log.debug("Stopped player");
            }        
        }
    }
    
    class JMFCanvas extends Canvas {
        
        public void paint(Graphics g){
            return;
        }
        
        public Dimension getMinimumSize(){
            return new Dimension(160,90);
        }
        
        public Dimension getPreferredSize(){
            return new Dimension(width, height);
        }
    }
        
    public JMFXineMedia() {
        System.out.println("Constructing JMFMedia2");
        jmfCanvas = new JMFCanvas();        
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#setMLFCListener(fi.hut.tml.xsmiles.mlfc.MLFCListener)
     */
    public void setMLFCListener(MLFCListener listener) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#isStatic()
     */
    public boolean isStatic() {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#getOriginalDuration()
     */
    public int getOriginalDuration() {
        if (player == null) {
            return -1;
        }

        Time duration = player.getDuration();
        if (duration.equals(Duration.DURATION_UNKNOWN)) {
            return -1;
        }

        return (int) (duration.getNanoseconds() / 1000);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#getOriginalWidth()
     */
    public int getOriginalWidth() {
        if (jmfCanvas != null) {
            return jmfCanvas.getPreferredSize().width;
        } 
        return -1;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#getOriginalHeight()
     */
    public int getOriginalHeight() {
        if (jmfCanvas != null) {
            return jmfCanvas.getPreferredSize().height;
        } 
        return -1;    
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#setUrl(java.net.URL)
     */
    public void setUrl(URL url) {
        this.url = url;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#setContainer(java.awt.Container)
     */
    public synchronized void setContainer(Container container) {
        if (this.container != null && this.container != container) {
            this.container.remove(jmfCanvas);            
        }
        this.container = container;        
        this.container.add(jmfCanvas);        
        canvasAdded = true;
        notifyAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#setBounds(int, int, int, int)
     */
    public void setBounds(int x, int y, int width, int height) {        
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        Log.debug("Setting canvas size: "+width+":"+height+", location: "+x+":"+y);

        // If the media is shown on the screen, move it immediately
        if (jmfCanvas != null && jmfCanvas.isVisible() == true) {            
            // Use the given coordinates
            jmfCanvas.setLocation(x, y);
            jmfCanvas.setSize(width, height);
        }
        synchronized (jmfCanvas) {
            jmfCanvas.notifyAll();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#setSoundVolume(int)
     */
    public void setSoundVolume(int percentage) {
        GainControl gain = null;

        if (player != null) {
            gain = player.getGainControl();
        }
        
        if (gain == null) {
            return;
        }
        gain.setLevel(((float) percentage) / 100);
    }

    private synchronized void createPlayer() throws NoPlayerException, IOException {
        if (player != null) {
            close();
            player = null;            
        }        
        while (!canvasAdded) {
            Log.debug("Video canvas not ready, waiting..");
            try {
                wait();
            } catch (InterruptedException e) {            
            }
        }
        MediaLocator mrl = new MediaLocator(url);
        player = Manager.createPlayer(mrl);
        if (player instanceof JMFPlayer) {
            ((JMFPlayer)player).setVisualComponent(jmfCanvas);
        }
        player.addControllerListener(new JMFListener(this)); 
        Log.debug("Created player: "+player.getClass().getName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#prefetch()
     */
    public void prefetch() throws Exception {
        if (player==null) {
            createPlayer();
        }
        //player.prefetch();        
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#play()
     */
    public void play() throws Exception {
        if (playing) {
            return;
        }
        if (player==null) {
            try {
                createPlayer();
            } catch (Exception e) { 
                if (e instanceof NoPlayerException) {
                    Log.error("Player not initialized for " + url.toString() + "!");
                } else {
                    e.printStackTrace();
                }
                if (mediaListener != null) {
                    // End this media immediately
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException f) {                        
                    }
                    mediaListener.mediaEnded();
                }
                return;
            }
        }
        if (player.getState()!=Controller.Prefetched) {
            player.prefetch();
        }
        playing = true;
        player.start();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#pause()
     */
    public void pause() {
        Log.debug("JMFMEDIA: pause");
        // Pause it
        if (player == null || !playing) {
            return;
        }

        if (paused) {
            paused = false;
            player.setRate(1.0f);
        } else {
            paused = true;
            player.setRate(0.0f);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#stop()
     */
    public void stop() {
        if (!playing) {
            return;
        }
        playing = false;
        try {
            close();
        } catch (Exception e) {
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#close()
     */
    public void close() {
        if (player!=null) {
            player.stop();
            player.deallocate();
            player.close();
            player = null;
        }
        
        // Clear media listeners
        // This is also needed to prevent JMF from sending MediaEndedEvent
        // to activate the next element.        
        //mediaListener = null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#setMediaTime(int)
     */
    public void setMediaTime(int millisecs) {
        if (player != null) {
            player.setMediaTime(new Time((float) millisecs / 1000));
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#addMediaListener(fi.hut.tml.xsmiles.gui.media.general.MediaListener)
     */
    public void addMediaListener(MediaListener listener) {
        mediaListener = listener;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.Media#showControls(boolean)
     */
    public void showControls(boolean visible) {     
    }
    
    /**
     * Mouse listener...
     */
    public void mouseClicked(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseClicked(e);
    }

    public void mouseEntered(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseEntered();
    }

    public void mouseExited(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseExited();
    }

    public void mousePressed(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mousePressed();
    }

    public void mouseReleased(MouseEvent e) {
        if (mediaListener != null)
            mediaListener.mouseReleased();
    }
}
