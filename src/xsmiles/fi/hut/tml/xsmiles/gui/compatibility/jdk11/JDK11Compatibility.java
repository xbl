/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.compatibility.jdk11;

import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility;
import fi.hut.tml.xsmiles.mlfc.MLFC;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

/**
 * Provides cross-jdk compatibility Issues: such as Font.deriveFont()
 * 
 * @author Mikko Honkala
 * @version 0
 */
public class JDK11Compatibility implements JDKCompatibility
{
    protected static Color jdk11TransparentColor = new Color(0, 0, 0);

    public Font deriveFont(Font orig, int style, float size)
    {
        return new Font(orig.getName(), style, (int) size);
    }

    public Color createTransparentColor()
    {
        return jdk11TransparentColor;
    }

    public boolean isTransparentColor(Color col)
    {
        return (col == jdk11TransparentColor);
    }



    public void drawString(String text, Font font, Graphics g, int x, int y, Object attributedString)
    {
        g.drawString(text, x, y);
    }

    public Object setTextAttribute(Object attributedString, short decoration)
    {
        return null;
    }

    public Object createAttributedString(String text, Font font)
    {
        return font;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility#setFullScreen(java.awt.Container, boolean)
     */
    public void setFullScreen(Container window, boolean fullscreen)
    {
        if (!(window instanceof Window)) return;
        Window myWindow = (Window)window; 
    	myWindow.setLocation(0,0);
    	myWindow.setSize(myWindow.getToolkit().getScreenSize());
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility#invokeAndWait(java.lang.Runnable)
     */
    public void invokeAndWait(Runnable runnable) throws InterruptedException, InvocationTargetException {
        runnable.run();
    }

    public Image getSVGImage(String url)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Component getSVGComponent(MLFC mlfc, URL url, View view)
    {
        // TODO Auto-generated method stub
        return null;
    }
}