/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.gui.compatibility;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.gui.compatibility.jdk11.JDK11Graphics;
/**
 * 
 * @author Mikko Honkala
 * @version 0
 */
public class CompatibilityFactory 
{
    protected final static String JDK11CLASSNAME = 
        "fi.hut.tml.xsmiles.gui.compatibility.jdk11.JDK11Compatibility";
    protected final static String JDK12CLASSNAME = 
        "fi.hut.tml.xsmiles.gui.compatibility.jdk12.JDK12Compatibility";
    protected final static String JDK14CLASSNAME = 
        "fi.hut.tml.xsmiles.gui.compatibility.jdk14.JDK14Compatibility";
    protected final static String JAVA2DCLASSNAME = 
        "fi.hut.tml.xsmiles.gui.compatibility.java2d.Java2DGraphics";
    protected static JDKCompatibility comp;
    protected static GraphicsCompatibility graphicsComp;
    public static JDKCompatibility getCompatibility()
    {
        if (comp==null)
        {
            double version = Utilities.getJavaVersion();
            try
            {
                if (version<1.2f)
                {
                    Class JDK=Class.forName(JDK11CLASSNAME);
                    comp = (JDKCompatibility)JDK.newInstance();
                }
                else if (version <1.4f)
                {
                    Class JDK=Class.forName(JDK12CLASSNAME);
                    comp = (JDKCompatibility)JDK.newInstance();
                }
                else
                {
                    Class JDK=Class.forName(JDK14CLASSNAME);
                    comp = (JDKCompatibility)JDK.newInstance();
                }
            } catch (Throwable t)
            {
                Log.error(t);
            }
        }
        return comp;
    }

    public static GraphicsCompatibility getGraphicsCompatibility()
    {
        if (graphicsComp==null)
        {
            try
            {
                    Class graph=Class.forName(JAVA2DCLASSNAME);
                    graphicsComp = (GraphicsCompatibility)graph.newInstance();
            }
            catch (Throwable t)
            {
                   Log.debug("GraphicsComp: "+t.toString());
            }
            if (graphicsComp==null) graphicsComp=new JDK11Graphics();
        }       
            
        return graphicsComp;
    }

}
