/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.compatibility.jdk12;

import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.font.TextAttribute;
import java.lang.reflect.InvocationTargetException;
import java.text.AttributedString;

import fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility;

/**
 * Provides cross-jdk compatibility Issues: such as Font.deriveFont()
 * 
 * @author Mikko
 * @version 0
 */
public class JDK12Compatibility implements JDKCompatibility
{
    public Font deriveFont(Font orig, int style, float size)
    {
        return orig.deriveFont(style, size);
    }

    public Color createTransparentColor()
    {
        return new Color(0x00, 0x00, 0x00, 0x00);
    }

    public boolean isTransparentColor(Color col)
    {
        return (col.getAlpha() == 0);
    }



    public void drawString(String text, Font font, Graphics g, int x, int y, Object attributedString)
    {
        g.drawString(((AttributedString) attributedString).getIterator(), x, y);
    }

    public Object createAttributedString(String text, Font font)
    {
        AttributedString as = new AttributedString(text);
        as.addAttribute(TextAttribute.FONT, font);
        return as;
    }

    public Object setTextAttribute(Object attributedString, short decoration)
    {
        if (attributedString == null)
            return null;
        AttributedString as = (AttributedString) attributedString;

        switch (decoration)
        {
            case 10 :// TextView.UNDERLINE :
                as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_LOW_ONE_PIXEL);
                break;
            case 15 :// TextView.LINETHROUGH :
                as.addAttribute(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
                break;
            case 20 :// TextView.ALIGNSUB :
                as.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUB);
                break;
            case 25 :// TextView.ALIGNSUPER :
                as.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER);
                break;
        }
        return as;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility#setFullScreen(java.awt.Container,
     *      boolean)
     */
    public void setFullScreen(Container window, boolean fullscreen)
    {
        if (!(window instanceof Window))
            return;
        Window myWindow = (Window) window;
        myWindow.setLocation(0, 0);
        myWindow.setSize(myWindow.getToolkit().getScreenSize());
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility#invokeAndWait(java.lang.Runnable)
     */
    public void invokeAndWait(Runnable runnable) throws InterruptedException, InvocationTargetException
    {
        EventQueue.invokeAndWait(runnable);
    }


}