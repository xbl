/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jan 30, 2006
 *
 */
package fi.hut.tml.xsmiles.gui.compatibility.jdk11;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.gui.compatibility.GraphicsCompatibility;
import fi.hut.tml.xsmiles.mlfc.MLFC;


public class JDK11Graphics implements GraphicsCompatibility
{

    public Image getSVGImage(String url)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Component getSVGComponent(MLFC mlfc, URL url, View view)
    {
        // TODO Auto-generated method stub
        return null;
    }
    public void setAntialias(boolean antialias, Graphics g)
    {
        // anti-alias not supported in jdk 1.1
        return;
    }
}
