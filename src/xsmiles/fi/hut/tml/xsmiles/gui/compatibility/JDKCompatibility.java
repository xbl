/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.compatibility;

import java.awt.Component;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Container;
import java.awt.Image;
import java.util.Hashtable;
import java.util.Vector;
import java.lang.Object;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * Provides cross-jdk compatibility 
 * Issues: such as Font.deriveFont()
 * @author Mikko
 * @version 0
 */
public interface JDKCompatibility
{
    Font deriveFont(Font orig,int style, float size);
    Color createTransparentColor();
    public boolean isTransparentColor(Color col);
    public void drawString(String text, Font font, Graphics g, int x, int y, Object attributedString);
    public Object setTextAttribute(Object attributedString, short decoration);
    public Object createAttributedString(String text, Font font);
    public void setFullScreen(Container window, boolean fullscreen);
    public void invokeAndWait(Runnable runnable) throws InterruptedException, InvocationTargetException;
}
