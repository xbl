/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.compatibility.jdk14;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility;
import fi.hut.tml.xsmiles.gui.compatibility.jdk12.JDK12Compatibility;
import fi.hut.tml.xsmiles.csslayout.view.TextView;

import java.awt.Container;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import java.util.Hashtable;

/**
 * Provides cross-jdk compatibility Issues: such as Font.deriveFont()
 * 
 * @author Mikko
 * @version 0
 */
public class JDK14Compatibility  extends JDK12Compatibility 
	implements JDKCompatibility
{
	    /* (non-Javadoc)
	     * @see fi.hut.tml.xsmiles.gui.compatibility.JDKCompatibility#setFullScreen(java.awt.Container, boolean)
	     */
	    public void setFullScreen(Container window, boolean fullscreen)
	    {
	        GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

	        try {
		        Window myWindow=(Window)window;
	            if (fullscreen)
	                device.setFullScreenWindow(myWindow);
	            else
	                device.setFullScreenWindow(null);
	        } catch (Exception e) {
	            Log.error(e);
	        }
	    }
}
