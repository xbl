package fi.hut.tml.xsmiles.gui;

import java.awt.*;

/**
 * The BrowserConstraints contains all the relevant information that is needed 
 * when choosing the right stylesheet.
 *
 * The existing stylesheet selection logic is the following:
 * - initially use the stylesheet associated with the device (stylesheet title=device title)
 * - if the user chooses to select a different stylesheet, it is possible
 *
 * @author Juha
 * @version 0
 */
public class BrowserConstraints {

    private Dimension    displaySize=new Dimension(640,480);
    private int		 displayDepth = 24;
    private boolean      xslfoSupported=true;
    private boolean      svgSupported=true;
    private boolean      smilSupport=true;
    private boolean      voiceRecognitionSupported=false;
    private boolean      soundSupported=true;

    private boolean      keyboardPresent=true;
    private boolean      discPresent=true;
    private int          connectionSpeed=100; // Mbits per second
    
    private String       title="desktop";
    private String       preferredStylesheetTitle;


    public BrowserConstraints(String t) {
	title=t;
	preferredStylesheetTitle=title;
    }
    
    /**
     * @param d The size of the display.
     */
    public void setDisplaySize(Dimension d) {
	displaySize=d;		              
    }
    /**
     * @param d The depth of the display.
     */
    public void setDisplayDepth(int d) {
	displayDepth=d;		              
    }

    /**
     * @param the name of device
     */
    public void setTitle(String s) {
	title=s;
	preferredStylesheetTitle=s;
    }

    /**
     * @param the name of device
     */
    public void setPreferredStylesheetTitle(String s) {
	preferredStylesheetTitle=s;
    }
    
    /**
     * @return The title of the device.
     */
    public String getTitle() {
	return title;
    }
    
    /**
     * @return The title of the preferred stylesheet.
     */
    public String getPreferredStylesheetTitle() {
	return preferredStylesheetTitle;
    }
    
    /**
     * @return The size of display
     */
    public Dimension getDisplaySize() {
	return displaySize;
    }

    /**
     * @return The depth of display
     */
    public int getDisplayDepth() {
    return displayDepth;
    }

    /**
     * Get requirements in CC/PP (RDF) format. 
     *
     * @retuns nothing yet
     */
    public String getRequirementsInRDF() {
	return "Not implemented yet";
    }

    
}
