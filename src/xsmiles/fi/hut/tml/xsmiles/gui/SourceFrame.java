/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui;


import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SourceFrame extends Frame
{
  public SourceFrame(String heading) {
    this( heading, 800, 600 );
	//setLocation(0,620);
  }

  public SourceFrame( String heading,int width, int height ) {
    // Lay the GUI out
	//this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    this.setLayout( new BorderLayout() );
    setSize( width, height );
    if (heading==null) this.setTitle("X-Smiles Source");
    else this.setTitle(heading);
	
    this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                SourceFrame.this.dispose();
            }
        });

  }
}
