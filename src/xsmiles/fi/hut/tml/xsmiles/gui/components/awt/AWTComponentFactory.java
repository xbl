package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.event.ActionListener;
import java.net.URL;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.components.CSSFormatter;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCalendar;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XConfirmDialog;
import fi.hut.tml.xsmiles.gui.components.XContainer;
import fi.hut.tml.xsmiles.gui.components.XDocument;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;
import fi.hut.tml.xsmiles.gui.components.XLinkComponent;
import fi.hut.tml.xsmiles.gui.components.XMedia;
import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XRange;
import fi.hut.tml.xsmiles.gui.components.XSecret;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.gui.components.XSelectMany;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;
import fi.hut.tml.xsmiles.gui.components.XTabbedPane;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.XUpload;
import fi.hut.tml.xsmiles.gui.components.XFocusManager;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

//import javax.swing.*;

/**
 * The default ComponentFactory which produces components, based on the GUI which is active. This enables the MLFC's to use generic components, whenever it is
 * possible. Each GUI's ComponentFactory should override these methods, to produce wanted components. Interfaces for all components should exist in
 * fi.hut.tml.xsmiles.gui.components
 * 
 * Some more component access methods will be added in the future
 * 
 * @author Juha
 * @author Mikko Honkala
 * @version 0
 */
public class AWTComponentFactory implements ComponentFactory {

    // needed to create an invisible window to add components in order to get their preferred size
    public static Frame topLevelFrame;
    CSSFormatter formatter;

    private BrowserWindow b;

    public AWTComponentFactory(BrowserWindow b) {
        this.b = b;
        // needed to create an invisible window to add components in order to get their preferred size
        this.findTopLevelFrame(null);
        //    setOpaque(false);
    }

    public AWTComponentFactory() {
        this.b = null;
    }

    /**
     * @return a container for components
     */
    public XContainer getXContainer() {
        return new AWTContainer();
    }

    public XPanel getXPanel() {
        return new AWTPanel();
    }

    public XLinkComponent getXLinkComponent(String dest) {
        XLinkComponent lc = new AWTLink();
        lc.setDestination(dest);
        return lc;
    }

    /**
     * HMM.. Is this used somewhere??? Give a XMLDocument, and get a rendered document If you are in for events, just add an actionListener and receive the
     * XMLEvents from the component
     * 
     * @param doc The document
     * @return the visual component that contains rendered xmldocument. Null is returned if rendering is not possible.
     * 
     * XForms notice: (you might want to use this component to render XOutput)
     */
    public XDocument getXDocument(XLink link) {
        if (b != null) {
            XDocument xd = new XADocument(b, link);
            return xd;
        } else {
            Log.error("Cannot return a document, because no link to browser");
            return null;
        }
    }

    /**
     * @param s The intial text
     * @return a textarea
     */
    public XTextArea getXTextArea(String s) {
        XTextArea ta = new AWTTextArea(s, this.b.getCurrentGUI());
        return ta;
    }

    /**
     * @return A secret input control
     */
    public XSecret getXSecret(char c) {
        return new AWTInput(c, null);
    }

    /**
     * @return one line input
     */
    public XInput getXInput() {
        return new AWTInput(null);
    }

    /**
     * @param from from what
     * @param to to where
     * @param step
     * @param orientation
     * @see
     * @return a range control
     */
    public XRange getXRange(int from, int to, int step, int orientation) {
        return new AWTRange(from, to, step, orientation);
    }

    /**
     * Give an URL and receive the mediaelement.
     */
    public XMedia getXMedia(URL u) {
        Log.debug("No media handler yet");
        return null;
    }

    /**
     * @param v A vector containing the items to put in selector
     * @return a XSelectOne, which is used to select one item at a time
     */
    public XSelectOne getXSelectOne(String appearance, boolean open) {
        //if (appearance.equalsIgnoreCase("listbox")||appearance.equalsIgnoreCase("compact"))
        return new AWTSelectOneCompact();
        //if (appearance.equalsIgnoreCase("menu")||appearance.equalsIgnoreCase("minimal"))
        //else
        // return new AWTSelectOneMinimal();

    }

    /**
     * @return a XSelectBoolean, which is used to select true / false
     */
    public XSelectBoolean getXSelectBoolean() {
        return new AWTSelectBoolean();
    }

    /**
     * @param v The vector that contains all possible selections
     * @return A selectMany control.
     */
    public XSelectMany getXSelectMany(String appearance, boolean open) {
        return new AWTSelectManyCompact();
    }

    /**
     * @return an upload control.
     */
    public XUpload getXUpload(String caption) {
        return new AWTUpload(caption, this);
    }

    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save) {
        // TODO: save dialog
        return new AWTFileDialog(this.b, save, AWTComponentFactory.topLevelFrame);
    }

    public XAuthDialog getXAuthDialog() {
        Frame parentframe = (Frame) b.getCurrentGUI().getWindow();
        return new AuthDialog(parentframe);
    }

    /**
     * @return an confirmation control.
     */
    public XConfirmDialog getXConfirmDialog() {
        Frame parentframe = (Frame) b.getCurrentGUI().getWindow();
        return new AWTConfirmDialog(parentframe);
    }

    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save, String filename) {
        // TODO: filename
        return new AWTFileDialog(this.b, save, topLevelFrame);
    }

    /**
     * @return a button w a label
     */
    public XButton getXButton(String label, String iconUrl) {
        return new AWTButton(label, iconUrl);
    }

    /**
     * @return a iconned button
     */
    public XButton getXButton(String iconUrl) {
        return new AWTButton(iconUrl);
    }

    /**
     * @return a button with images
     */
    public XButton getXButton(String imageUrl, String focusedImage, String disabledImage) {
        // TODO
        //return new SwingButton(imageUrl,focusedImage,disabledImage);
        return null;
    }

    /**
     * @return a new calender control
     */
    public XCalendar getXCalendar() {
        return null;
    }

    /**
     * @return a caption (label) component
     */
    public XCaption getXCaption(String captText) {
        return new AWTCaption(captText);
    }

    /**
     * @ return a compound designed for holding a label and a component
     */
    public XLabelCompound getXLabelCompound(XComponent comp, XCaption capt, String captSide) {
        return null;
    }

    /** listen for all help keypresses etc, for this component and its ancestors */
    public void addHelpListener(Component component, ActionListener listener) {
        Log.debug("addHelpListener not implemented");
    }

    /** remove a help listener */
    public void removeHelpListener(Component component, ActionListener listener) {
        Log.debug("removeHelpListener not implemented");
    }

    /** show an error dialog */
    public void showError(String title, String explanation) {
        //javax.swing.JOptionPane.showMessageDialog(null,
        //                    explanation,title,javax.swing.JOptionPane.ERROR_MESSAGE,null);
        Log.error(title + " " + explanation);
    }

    public void showLinkPopup(URL url, XMLDocument doc, java.awt.event.MouseEvent e, MLFCListener listener) {
        Log.debug("link popup not supported.");
    }

    /** create a content panel for the browser, add also maybe a layout manager */
    public Container createContentPanel() {
        Log.debug("AWTComponentFactory.createContentPanel()");
        Container p = new Panel();
        //new Container();
        //new DoubleBufferedContainer();
        p.setLayout(new BorderLayout());
        return p;
    }

    /** create a scroll pane for this components. e.g. in swing, create a JScrollPane and put the component inside */
    public Container createScrollPane(Component comp, int policy) {
        Log.debug("AWTComponentFactory.createScrollPane()");
        ScrollPane p = new ScrollPane(ScrollPane.SCROLLBARS_ALWAYS);
        //Container p = new Panel(new BorderLayout());
        //p.setSize(800,600);
        p.add(comp);
        return p;
    }
    // needed to create an invisible window to add components in order to get their preferred size
    public void findTopLevelFrame(Container p) {
        if (p==null) p = this.b.getContentArea();
        while (p != null && (!(p instanceof Frame))) {
            p = p.getParent();
        }
        AWTComponentFactory.topLevelFrame=(Frame)p; 
    }

    public XMenuBar getXMenuBar() {
        return new AWTMenuBar();
    }

    public XMenu getXMenu(String name) {
        return new AWTMenu(name);
    }

    public XMenuItem getXMenuItem(String name) {
        return new AWTMenuItem(name);
    }

    public void showMessageDialog(boolean isModal, String title, String message, long timeToLiveMillis) {
        this.b.getCurrentGUI().setStatusText(title + " : " + message);
        /*
         * int messageType = JOptionPane.INFORMATION_MESSAGE; int optionType = JOptionPane.DEFAULT_OPTION; JOptionPane pane = new JOptionPane(message,
         * messageType, optionType, null,null, null);
         * 
         * //pane.setInitialValue(initialValue);
         * 
         * final JDialog dialog = pane.createDialog(null, title); dialog.setModal(isModal); pane.selectInitialValue(); dialog.show(); if (timeToLiveMillis>0) {
         * final long time = timeToLiveMillis; Thread t= new Thread(title) { public void run() { try { Thread.sleep(time); dialog.dispose();
         *  } catch (Exception e) { } } }; t.start();
         *  }
         */
    }

    /** query for extension availability. For instance, calendar control is an extension */
    public boolean hasExtension(Class c) {
        return false;
    }

    /** return an extension control. hasExtension must be called first */
    public Object getExtension(Class c) {
        Log.error("There is no extension for : " + c);
        return null;
    }

    public void setScrollBar(Container con, int x, int y) {
        if (con instanceof ScrollPane)
            ((ScrollPane) con).setScrollPosition(x, y);
        else
            Log.error("DefaultComponentFactory: scroll bar not set to: " + con);
    }
    

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getXTabbedPane()
     */
    public XTabbedPane getXTabbedPane() {
        return new AWTTabbedPane();
    }


    public XFocusManager getXFocusManager() {
        // MH: mem leak return new AWTFocusManager();
        return null;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getCSSFormatter()
     */
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getCSSFormatter()
     */
    public CSSFormatter getCSSFormatter()
    {
        // TODO Auto-generated method stub
        if (formatter==null)
            formatter=AWTCSSFormatter.getInstance();
        return formatter;
    }}
