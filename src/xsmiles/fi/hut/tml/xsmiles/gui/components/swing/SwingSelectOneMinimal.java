/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;

import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;

import java.awt.Container;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.event.*;

import javax.swing.event.*;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JComponent;


import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.w3c.dom.css.*;


/**
 * select one minimal mode
 * @author Mikko Honkala
 */
public class SwingSelectOneMinimal extends SwingSelectOne
implements XSelectOne, ItemListener
{
    
    /** the combobox */
    JComboBox combo;
    
    /** selection mode, open/closed (editable/non-editable */
    boolean openSelection=false;
    /**
     * @param v The vector containing all items, the first item is default
     * @param s The style of the selectOne
     */
    public SwingSelectOneMinimal(boolean open)
    {
        super();
        this.openSelection=open;
        this.init();
    }
    /**
     * Creates the visual component of this control.
     */
    //private static Insets insets = new Insets(1,3,1,3);
    public Component createComponent()
    {
        combo=new JComboBox();
        if (openSelection) 
            combo.setEditable(true);
        combo.addItemListener(this);
        selectComponent=combo;
        return selectComponent;
    }
    
    
    /**
     * the default background color for this type of component
     * null = default.
     */
    public Color getDefaultBackgroundColor()
    {
        return null;
        //return Color.lightGray;
    }
    
    
    
    
    public void addSelection(Object o)
    {
        combo.addItem(o);
    }
    
    public void removeSelection(Object o)
    {
        combo.removeItem(o);
    }
    
    public void removeAll()
    {
        combo.removeAllItems();
    }
    
    public void setSelected(Object o)
    {
        combo.setSelectedItem(o);
    }
    public void setSelectedIndex(int index)
    {
        combo.setSelectedIndex(index);
    }
    public int getSelectedIndex()
    {
        return combo.getSelectedIndex();
    }
    /** the selection events from a menu arrive here */
    public void itemStateChanged(ItemEvent e)
    {
        Object item=e.getItem();
        this.notifyChange(item, e.getStateChange());
        //broadcastEvents(ie);
    }
    
    
}
