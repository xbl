/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.apps.svgbrowser;
package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.gui.components.swing.XAImageIcon;
import fi.hut.tml.xsmiles.gui.components.awt.*;
// for version
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Color;

import java.net.URL;

import java.util.Hashtable;

/*
 import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JWindow;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;


import javax.swing.border.BevelBorder;
*/
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import java.awt.*;
import java.awt.event.*;

/**
 * A dialog showing the revision of the Batik viewer as well
 * as the list of contributors.
 * The dialog can be dismissed by click or by escaping.
 *
 * @author <a href="mailto:vincent.hardy@eng.sun.com">Vincent Hardy</a>
 * @version $Id: InitDialog.java,v 1.2 2004/03/26 10:50:06 honkkis Exp $
 */
public class InitDialog extends Frame {
  AboutDialog2 mySplash;
  public InitDialog()
  {
      this("empty");
  }
  public InitDialog(String title)
  {
    super(title);
    addWindowListener
      (new WindowAdapter() {
	  public void windowClosing(WindowEvent e) {
	    System.exit(0);
	  }
	}
       );
    mySplash = new AboutDialog2(this);
  }      
  
  public void increment() {
    mySplash.increment();
  }
  
}
    


