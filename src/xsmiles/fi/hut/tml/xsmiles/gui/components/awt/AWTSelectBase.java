/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;
import java.awt.AWTEventMulticaster;

import fi.hut.tml.xsmiles.gui.components.XSelect;



/**
 * Common base class for text components
 * @author Mikko Honkala
 */

public abstract class AWTSelectBase extends AWTStylableComponent
 implements XSelect
{
 boolean isSelecting=false;
 ItemListener itemlistener = null;
  public AWTSelectBase()
  {
	super();
  }
  
      /** add a listener for changes in the range control */
  public void addItemListener(ItemListener l)
  {
  	  itemlistener = AWTEventMulticaster.add(itemlistener, l);
  }
  /** remove a listener for changes in the range control */
  public void removeItemListener(ItemListener l)
  {
  	  itemlistener = AWTEventMulticaster.remove(itemlistener, l);
  }
  
  
}
