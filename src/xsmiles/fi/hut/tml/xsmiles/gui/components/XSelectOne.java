package fi.hut.tml.xsmiles.gui.components;

import java.util.Vector;

/**
 * Used to select one object out of all. The first object is default
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public interface XSelectOne extends XSelect 
{

}
