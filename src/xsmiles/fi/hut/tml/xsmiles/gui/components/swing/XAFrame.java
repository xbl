package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;



/**
 * Normal JFrame with the addition, that the xsmiles icon is visible
 *
 * @author Juha
 * @version 0
 */
public class XAFrame extends JFrame {
  private static String smilesicon="img/browserlogo.gif";
    
    public XAFrame() {
      super();
      initIcon();
    }
  
  public XAFrame(String title) {
    super(title);
    initIcon();
  }
  
  private void initIcon() {
    XAImageIcon logo=new XAImageIcon(smilesicon);
    setIconImage(logo.getImage());
  }
}

