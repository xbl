/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;

import java.awt.*;
import java.awt.event.*;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    


/**
 * select one compact mode
 * @author Mikko Honkala
 */
public class AWTSelectOneCompact extends AWTSelectOne 
    implements XSelectOne, ItemListener
{
    
    final static Dimension minSize = new Dimension(50,10);
    
    /** the listbox */
    protected List list; 
    protected boolean multiple=false;
    /** the list model, when using the JList */
    //private DefaultListModel listmodel;

		
  /**
   * @param v The vector containing all items, the first item is default
   * @param s The style of the selectOne
   */
    public AWTSelectOneCompact() 
    {
	super();
	this.init();
    }
  
    public Component createComponent()
    {
	//listmodel = new DefaultListModel();
	list=new List();
	
	/* 
	   JScrollPane scrollPane = new JScrollPane(list,
	   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	*/
	
	// TODO: move to add / removeselection 
	//list.setVisibleRowCount(java.lang.Math.min(8,its.size()));
	//list.setVisibleRowCount(8);
	//list.setSelectionMode(this.getListSelectionModel());

	list.addItemListener (this);
	list.setMultipleMode(multiple);
	selectComponent=list;
	return list;
    }

    /*    protected int getListSelectionModel()
	  {
	  return ListSelectionModel.SINGLE_SELECTION;
	  }
    */

    /** 
     * the default background color for this type of component 
     * null = default.
     */
    public Color getDefaultBackgroundColor()
    {
        return Color.white;
    } 
    
    /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
        return minSize;
    }

    public void addSelection(Object o)
    {
        this.isSelecting=true;
        list.add((String)o);
	//if (!this.layoutDone)((JList)list).setVisibleRowCount(java.lang.Math.min(8,listmodel.getSize()));
        this.isSelecting=false;
    }

    public void removeSelection(Object o)
    {
        this.isSelecting=true;
	//listmodel.removeElement(o);
	list.remove((String)o);
	this.isSelecting=false;
	
    }

    public void removeAll()
    {
        this.isSelecting=true;
	//listmodel.removeAllElements();
	list.removeAll();
	this.isSelecting=false;
    }

    public void setSelected(Object o) 
    {
        this.isSelecting=true;
	//list.setSelectedValue(o,true);
	String [] itemz=list.getItems();

	int i=0;

	for(i=0;i<list.getItemCount();i++) {
	    if(o.equals(list.getItem(i)))
		list.select(i);
	}
	this.isSelecting=false;
    }
    
    public void setSelectedIndex(int index) 
    {
        this.isSelecting=true;
	//list.setSelectedIndex(index);
	list.select(index);
	this.isSelecting=false;
    }

    public int getSelectedIndex() {
	//return list.getSelectedIndex();
	return list.getSelectedIndex();
    }	
    
    
    /** the selection events from a menu arrive here */
    public void itemStateChanged(ItemEvent e)
    {
        Log.debug("Item State Changed: "+e);
        Object item=e.getItem();
        if (!isSelecting) this.notifyChange(item, e.getStateChange());
        //broadcastEvents(ie);
    }
    /** the selection events from a list arrive here */
    /* 
       public void valueChanged(ListSelectionEvent e) {
       if (e.getValueIsAdjusting())
       return;
       
       Object item = ((JList)list).getSelectedValue();
       this.notifyChange(item, ItemEvent.ITEM_STATE_CHANGED);
       }	
    */
/*
    public void addItemListener(ItemListener l) 
    {
	//list.addItemListener(l);
    }
    
    public void removeItemListener(ItemListener l) 
    {
	//list.removeItemListener(l);
    }
    */
}
