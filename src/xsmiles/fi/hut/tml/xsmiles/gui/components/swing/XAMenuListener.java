package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;

import com.sun.media.util.Resource;

import java.io.*;
import java.net.URL;
import java.util.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.EventBroker;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.gui.Language;

public class XAMenuListener implements ActionListener {
  JFileChooser fc;
  BrowserWindow browser;
    
  /**
   * A generic listener for gui dropdown menus
   *
   * @param b the browser
   */
  public XAMenuListener(BrowserWindow b) {
    super();
	
    browser=b;
    fc = new JFileChooser("../demo");
  }
    
    
  public void actionPerformed(ActionEvent e) {
    String com = e.getActionCommand();
    if(com.equals(Language.OPEN)) {
      openFileDialog();
    } else if(com.equals(Language.SAVE)) {
      saveFileDialog();
    } else if(com.equals(Language.NEW)) {
      browser.newBrowserWindow();
    } else if(com.equals(Language.CLOSE)) {
      browser.closeBrowserWindow();
    } else if(com.equals(Language.EXIT)) {
      exitDialog();
    } else if(com.equals("Instructions")) {
      instructions();
    } else if(com.equals(Language.ABOUT)) {
      about();
    } 
    else if(com.equals(Language.TREE)) 
	    browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.TREEMLFC,com+" : "+browser.getXMLDocument().getXMLURL());
    else if (com.equals(Language.SOURCEXML))
	  browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.SOURCEMLFC,com+" : "+browser.getXMLDocument().getXMLURL());
	else if(com.equals(Language.SOURCEXMLXSL))
      browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.SOURCE_AND_XSL_MLFC,com+" : "+browser.getXMLDocument().getXMLURL());
    else if(com.equals(Language.SOURCEXSL))
    	browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.XSL_MLFC,com+" : "+browser.getXMLDocument().getXSLURL());
    else if(com.equals(Language.CONFIGURATION)) {
      optionsDialog();
    }
    else if(com.equals(Language.LOG))
      browser.openLog();
  }
  
  /**
   * Show about dialog
   */
  private void about() {
    browser.openLocation("cfg/about.smil");
  }
    
  private void exitDialog() {
    browser.closeBrowser();
  }
    
  /** 
   * Surf to the config file
   */
  private void optionsDialog() {
    try {
      URL requiredDoc = Resources.getResourceURL("xsmiles.config");//new URL("file:"+browser.getOptionsDocument());
      browser.openLocation(requiredDoc, true);
    } catch (Exception e)
      {
	Log.error(e);
      }
  }
    
  private void saveFileDialog() {
    File file=null;
    URL requiredDoc=null;
    int returnVal=0;
    int viewType = browser.getViewType();
    PrintWriter out = null;
    XMLDocument doc = browser.getXMLDocument();
    returnVal = fc.showSaveDialog(browser.getCurrentGUI().getWindow());
    if (returnVal == JFileChooser.APPROVE_OPTION) 
      file = fc.getSelectedFile();
    try {
      requiredDoc = toURL(file);
    } catch(Exception e) {
    }
    if (file != null) {
      try {
	out = new PrintWriter(new FileWriter(file));
      } catch(Exception e) {
      }
      if(viewType == XsmilesView.XSL_MLFC) {
	Vector sourceVector = new Vector();
	sourceVector        = doc.getXSLVector() ;
	for (int i=0; i < sourceVector.size(); i++) {
	  String vectorElement = sourceVector.elementAt(i).toString();
	  out.print(vectorElement);
	  out.print("\n");
	}
      } else if(viewType == XsmilesView.SOURCEMLFC) {
	Vector sourceVector = new Vector();
	sourceVector        = doc.getXMLVector();
		
	for (int i=0; i < sourceVector.size(); i++) {
	  String vectorElement = sourceVector.elementAt(i).toString();
	  out.print(vectorElement);
	  out.print("\n");
	}
      } else if(viewType == XsmilesView.PRIMARYMLFC || viewType == XsmilesView.SOURCE_AND_XSL_MLFC) {
	Vector sourceVector = new Vector();
	sourceVector        = doc.getSourceVector();
	for (int i=0; i < sourceVector.size(); i++) {
	  String vectorElement = sourceVector.elementAt(i).toString();
	  out.print(vectorElement);
	  out.print("\n");
	} 
      }
      out.close();
    }
  }

  public static URL toURL(File file) throws java.net.MalformedURLException {
    String path = file.getAbsolutePath();
    if (File.separatorChar != '/') {
      path = path.replace(File.separatorChar, '/');
    }
    if (!path.startsWith("/")) {
      path = "/" + path;
    }
    if (!path.endsWith("/") && file.isDirectory()) {
      path = path + "/";
    }
    return new URL("file", "", path);
  }
	
  private void openFileDialog() {
    File file=null;
    URL requiredDoc=null;
    int returnVal=0;
    try {
      returnVal = fc.showOpenDialog(browser.getCurrentGUI().getWindow());
      if (returnVal == JFileChooser.APPROVE_OPTION) 
	file = fc.getSelectedFile();
      requiredDoc = toURL(file);
    } catch(Exception e) {
    }
    if(requiredDoc!=null)
      browser.openLocation(requiredDoc, true);
  }
    
  private void instructions() {
  }
}
