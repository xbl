/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Component;
import java.awt.Scrollbar;
import java.awt.event.AdjustmentListener;
import java.util.Dictionary;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XRange;

/**
 * textarea 
 * @author Mikko Honkala
 */
public class AWTRange extends AWTStylableComponent 
    implements XRange
	       //, ChangeListener, Adjustable
{
    private Scrollbar slider;
    AdjustmentListener adjustmentlistener = null;
    
    

    public AWTRange(int min, int max, int stepsize,int orientation)
    {
        super();
        this.init(min,max,stepsize,orientation);
    }
    public AWTRange(int min, int max, int stepsize)
    {
        this(min,max,stepsize,XRange.VERTICAL);
    }

    /**
     * @param min
     * @param max
     * @param orientation VERTICAL HORIZONTAL
     */
    public void init(int min, int max, int stepsize,int orientation) 
    {
        this.content=this.createContent(min,max,stepsize,orientation);
        super.init();
    }
    /** creates the content component */
    public Component createContent(int min, int max, int stepsize,int orientation)
    {
        Log.debug("Creating new slider min:"+min+" max:"+max);
        //slider=new JSlider(orientation,1,5,2);
        slider=new Scrollbar(orientation,min,stepsize,max,min);
        slider.setUnitIncrement(stepsize);
        //slider=new JSlider(orientation,1,5,2);
        //slider=new JSlider(orientation,min,max,min);
        //Log.debug("slider2");
        //slider.setOrientation(orientation);
        //Log.debug("slider3");
        //slider.addChangeListener(this);
        //Turn on labels at major tick marks.
        //slider.setMajorTickSpacing(stepsize);
        //slider.setMinorTickSpacing(stepsize);
        //Log.debug("slider4");

        //slider.setPaintTicks(true);

        /*Hashtable labelTable = new Hashtable();
        labelTable.put( new Integer( min ), new JLabel(""+min) );
        labelTable.put( new Integer( max ), new JLabel(""+max) );
        slider.setLabelTable( labelTable );

        slider.setPaintLabels(true);		

        slider.setBorder(
                BorderFactory.createEmptyBorder(0,0,0,2));*/
        return slider;//new SwingStylableComponent(slider,this);
    }


    /**
     * @param n The new value 
     */
    public void setValue(int n) 
    {
        slider.setValue(n);
    }
    
    /**
     * @return The current value set
     */
    public int getValue()
    {
        return slider.getValue();
    }
    
    /** add a listener for changes in the range control */
    public void addAdjustmentListener(AdjustmentListener l)
    {
        this.slider.addAdjustmentListener(l);
        //adjustmentlistener = AWTEventMulticaster.add(adjustmentlistener, l);
    }
    /** remove a listener for changes in the range control */
    public void removeAdjustmentListener(AdjustmentListener l)
    {
        this.slider.addAdjustmentListener(l);
        //adjustmentlistener = AWTEventMulticaster.remove(adjustmentlistener, l);
    }
    
    /*
    public void stateChanged(ChangeEvent e) 
    {
        if (adjustmentlistener!=null)
        {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                int value = this.getValue();
                AdjustmentEvent ev = new AdjustmentEvent(
                        this, -1, AdjustmentEvent.ADJUSTMENT_VALUE_CHANGED, value);
                adjustmentlistener.adjustmentValueChanged(ev);
            }
        }
    }*/
    // METHODS FROM ADJUSTABLE INTERFACE
    public int getOrientation() {
        // TODO;
        return 0;
    }	

    public int getBlockIncrement() {
        // TODO;
        return 0;
    }	

    public int getMinimum() {
        // TODO;
        return 0;
    }	

    public int getUnitIncrement() {
        // TODO;
        return 0;
    }
    
    public int getVisibleAmount() {
        // TODO;
        return 0;
    }
    
    public void setMaximum(int param) {
        // TODO;
    }
    
    public void setBlockIncrement(int param) {
        // TODO;
    }
    
    public int getMaximum() {
        // TODO;
        return 0;
    }
    
    public void setVisibleAmount(int param) {
        // TODO;
    }
    
    public void setUnitIncrement(int param) {
        // TODO;
    }
    
    public void setMinimum(int param) {
        // TODO;
    }
    /** give the dictionary for the labels */
    public void setLabelTable(Dictionary dictionary)
    {
        /*
        Hashtable labeltable = new Hashtable();
        Enumeration e = dictionary.keys();
        while(e.hasMoreElements())
        {
            Object key = e.nextElement();
            String value = (String)dictionary.get(key);
            if (value !=null)
            {
                JLabel label = new JLabel( value, JLabel.CENTER );
                labeltable.put(key,label);
            }
        }
        this.slider.setLabelTable(labeltable);
        */
    }
    
    /*

    public AWTRange(int min, int max, int stepsize,int orientation)
    {
	super();
	this.init(min,max,stepsize,orientation);
    }

    public AWTRange(int min, int max, int stepsize)
    {
	this(min,max,stepsize,XRange.VERTICAL);
    }

    public void init(int min, int max, int stepsize,int orientation) 
    {
	this.content=this.createContent(min,max,stepsize,orientation);
	super.init();
    }
    public AWTStylableComponent createContent(int min, int max, int stepsize,int orientation)
    {
	Log.debug("Creating new slider min:"+min+" max:"+max);
	//slider=new JSlider(orientation,1,5,2);
	slider=new Scrollbar(orientation,min,stepsize,max,min);
	slider.setUnitIncrement(stepsize);
	//slider.setMajorTickSpacing(stepsize);
	//slider.setMinorTickSpacing(stepsize);
	//Log.debug("slider4");

	//slider.setPaintTicks(true);

	//Hashtable labelTable = new Hashtable();
	//labelTable.put( new Integer( min ), new JLabel(""+min) );
	//labelTable.put( new Integer( max ), new JLabel(""+max) );
	//slider.setLabelTable( labelTable );

	//slider.setPaintLabels(true);		

	//slider.setBorder(
	//		 BorderFactory.createEmptyBorder(0,0,0,2));
	return new AWTStylableComponent(slider,this);
    }

    public void setValue(int n) 
    {
	slider.setValue(n);
    }
  
    public int getValue()
    {
	return slider.getValue();
    }
  
    public void addAdjustmentListener(AdjustmentListener l)
    {
	//adjustmentlistener = AWTEventMulticaster.add(adjustmentlistener, l);
	slider.addAdjustmentListener(l);
    }
    public void removeAdjustmentListener(AdjustmentListener l)
    {
	//adjustmentlistener = AWTEventMulticaster.remove(adjustmentlistener, l);
	slider.removeAdjustmentListener(l);
    }
  
    // METHODS FROM ADJUSTABLE INTERFACE
    public int getOrientation() {
	// TODO;
	return 0;
    }	

    public int getBlockIncrement() {
	// TODO;
	return 0;
    }	

    public int getMinimum() {
	// TODO;
	return 0;
    }	

    public int getUnitIncrement() {
	// TODO;
	return 0;
    }
	
    public int getVisibleAmount() {
	// TODO;
	return 0;
    }
	
    public void setMaximum(int param) {
	// TODO;
    }
	
    public void setBlockIncrement(int param) {
	// TODO;
    }
	
    public int getMaximum() {
	// TODO;
	return 0;
    }
	
    public void setVisibleAmount(int param) {
	// TODO;
    }
	
    public void setUnitIncrement(int param) {
	// TODO;
    }
	
    public void setMinimum(int param) {
	// TODO;
    }
    public void setLabelTable(Dictionary dictionary)
    {
        //TODO
    }*/
}
