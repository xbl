/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.XMedia;
import fi.hut.tml.xsmiles.Log;
import java.net.URL;


/**
 * Interface to a media component
 * Mostly unimplemented, waiting for binary functional component framework.
 * A lot of similarities with SMIL MediaComponent.
 *
 * @author Kari
 * @author Juha
 */
public class XAMedia extends AWTComponent implements XMedia
 {
  private String description;
  private String url;
  private String mime;

   public XAMedia(URL u) 
   {
     Log.debug("Media not functional yet");
   }


  /**
   * Checks if this media is static or continuous.
   * @return true if media is static.
   */
  public boolean isStatic() 
  {
    return true;
  }
  
  /**
   * @param s Set the description of the media (This can be e.g., the ALT tag)
   */ 
  public void setDescription(String s)
  {
    description=s;
  }

  /**
   * Sets the URL of the media.
   */
  public void setURL(String url)
  {
    this.url=url;
  }

  /**
   * Force this media to use this media type
   */     
  public void setMIMEType(String mimeType)
  {
    mime=mimeType;
  }
  public void prefetch()
  {
  }
  public void play()
  {
  }
  public void pause()
  {
  }
  public void stop()
  {
  }
  public void freeze()
  {
  }
  public void close()
  {
  }


/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getEnabled()
 */
public boolean getEnabled()
{
    // TODO Auto-generated method stub
    return this.container.isEnabled();
}
}

