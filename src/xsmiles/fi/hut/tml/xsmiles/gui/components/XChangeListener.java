/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.gui.components;


/**
 * A very general event listener. Listens for a general change. 
 * Note that you must read the value from the component
 * itself.
 */

public interface XChangeListener {
    /**
     * ChangeListener method that notifies that the user has changed the value of the control.
     * @param newValue A schema compatible string containing the new value
     * @param newObjValue The new value as a Java object
     */
    public void valueChanged(boolean valueChanging, Object source);
	
} // AdaptiveControl
