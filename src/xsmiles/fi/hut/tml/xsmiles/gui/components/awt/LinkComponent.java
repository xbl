/* X-Smiles
 * Strip tease license:
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * Full license found in  licenses/XSMILES_LICENSE.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XHoverListener;

/**
 * Default desktop lightweight linkcomponent. 
 * 
 * @author Juha
 */
public class LinkComponent extends InvisibleComponent 
{
  private MouseHandler         mouseHandler;
  private String               destination;
  private String                 label;
  private boolean              active=false;
  private boolean              focused=false;
  private Vector               urlListeners;
  private Vector               focusListeners;
  
  public LinkComponent() {
    init();
  }
  
  private void init() {
    mouseHandler = new MouseHandler();
    addMouseListener(mouseHandler);
    setBackground(Color.white);
    label=null;
    urlListeners=new Vector();
    focusListeners=new Vector();
    addFocusListener(new LinkFocusListener());
  }
  
  public boolean isFocusTraversable() {
    return true;
  }

  public void setDestination(String dest) {
    destination = dest;
  }

  public String getDestination() {
    return destination;
  }

  public void setLabel(String l) {
    label=l;
  }
  
  public String getLabel() {
    return label;
  }
  
  /**
   * Draw a rectangle around the active link, if in Digitv GUI
   *
   * @param g ...
   */ 
  public void paint(Graphics g) {
    super.paint(g);
    /*if(hasFocus()) {
      g.setColor(Color.black);
      g.drawRect(0,0,10,10);
      }*/
  }
    

  /**
   * Sets active flag
   * 
   * @param val true or false
   */
  public void setFocused(boolean val) {
    this.active=val;
    repaint();
  }
    
  /**
   * Is link active
   *
   * @return true if active
   */
  public boolean isActive() {
    return active;
  }

  public void addClickedActionListener(ActionListener al) {
    urlListeners.addElement(al);
  }
  
  public void addHoverListener(XHoverListener fl) {
    focusListeners.addElement(fl);
  }

  public void openAction() {
    Log.debug("Open action.. URL= " + destination);
    ActionListener reactee;
    Enumeration e = urlListeners.elements();
    while(e.hasMoreElements()) {
      reactee = (ActionListener)e.nextElement();
      reactee.actionPerformed(new ActionEvent(this,0,destination));
    }
  }

  public void focusLost() {
    XHoverListener reactee;
    Enumeration e = focusListeners.elements();
    while(e.hasMoreElements()) {
      reactee = (XHoverListener)e.nextElement();
      reactee.focusLost();
    }
  }

  public void focusGained() {
    XHoverListener reactee;
    Enumeration e = focusListeners.elements();
    while(e.hasMoreElements()) {
      reactee = (XHoverListener)e.nextElement();
      reactee.focusGained(destination);
    }
  }



  public class MouseHandler extends MouseAdapter {
    private boolean inside=false;         // is the cursor inside the component
    private boolean startedInside =false; // whether the click started from the inside
    
    public void mouseEntered(MouseEvent e)
    {
      inside=true;
      setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));    
      focusGained();
    }
    
    public void mouseExited(MouseEvent e)
    {
      inside=false;
      setCursor(Cursor.getDefaultCursor());
      focusLost();
    }
    
    public void mousePressed(MouseEvent e)
    {
      startedInside=true;
      repaint();
    }
      
    public void mouseReleased(MouseEvent e)
    {
      if (inside)
	if (startedInside=true) 
	  openAction();
      startedInside=false;
    }
  }

  private class LinkFocusListener implements FocusListener {
    public void focusGained(FocusEvent e) {
      focused=true;
      repaint();
    }

    public void focusLost(FocusEvent e) {
      focused=false;
      repaint();
    }
  }
}
  
