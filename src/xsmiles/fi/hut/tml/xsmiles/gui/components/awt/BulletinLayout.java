/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.*;
import java.util.Hashtable;

/**
 * Lays out components as though they were pinned to 
 * a bulletin board, meaning:<p>
 *
 * Components are moved to the location that was set by calling
 * or setBounds().
 *
 * Components are sized according to the size that was set
 * by calls to either setSize() or setBounds().  If either the
 * width or height of the component is 0, the component is
 * sized to its preferred size.
 *
 * AWT Caveats:
 *
 * Buttons, Labels, Checkboxes and Choices do not take kindly
 * to being resized - it is better to let them take on their
 * preferred sizes instead.
 *
 * Lists must be moved and shaped only after their peers are
 * created.
 *
 * Choices can only be moved after their peers are created.
 *
 * @version 1.0, Apr 1,  1996
 * @version 1.1, Oct 24, 1996 
 * @version 1.15, Oct 23, 2001
 *
 *    Fixed bug that reported wrong preferred and minimum sizes.
 *
 *    layoutContainer() now reshapes components according to 
 *    their actual sizes.  If a component has a zero width or 
 *    height, then its preferred size is used to reshape.
 *
 *    The purpose of the hashtable is not clear. All it accomplishes
 *    is to extend the lifetime of the layouted components until 
 *    the BulletinLayout is finalized, and would make the layouts 
 *    "slide" every time layoutContainer() is called...
 *
 * @author  David Geary, modified by Juha Vierinen & Jukka Santala
 */
public class BulletinLayout implements LayoutManager {
//    private Hashtable hash = new Hashtable();

    public void addLayoutComponent(String s, Component comp) {
    }
    public void removeLayoutComponent(Component comp) {
    }
    public Dimension preferredLayoutSize(Container target) {
        Insets    insets      = target.getInsets();
        Dimension dim         = new Dimension(0,0);
        int       ncomponents = target.getComponentCount();
        Component comp;
        Dimension d;
        Rectangle size = new Rectangle(0,0);
        Rectangle compSize;

        for (int i = 0 ; i < ncomponents ; i++) {
            comp = target.getComponent(i);

            if(comp.isVisible()) {
		d = comp.getSize();
                compSize = new Rectangle(comp.getLocation());
                compSize.width  = d.width;
                compSize.height = d.height;

                size = size.union(compSize);
            }
        }
        dim.width  += size.width + insets.right;
        dim.height += size.height + insets.bottom;

        return dim;
    }
    public Dimension minimumLayoutSize(Container target) {
        Insets    insets      = target.getInsets();
        Dimension dim         = new Dimension(0,0);
        int       ncomponents = target.getComponentCount();
        Component comp;
        Dimension d;
        Rectangle minBounds = new Rectangle(0,0);
        Rectangle compMinBounds;

        for (int i = 0 ; i < ncomponents ; i++) {
            comp = target.getComponent(i);

            if(comp.isVisible()) {
                d = comp.getMinimumSize();
                compMinBounds = 
		    new Rectangle(comp.getLocation());
		compMinBounds.setSize(d.width, d.height);

                minBounds = minBounds.union(compMinBounds);
            }
        }
        dim.width  += minBounds.width  + insets.right;
        dim.height += minBounds.height + insets.bottom;

        return dim;
    }
    public void layoutContainer(Container target) {
        Insets    insets      = target.getInsets();
        int       ncomponents = target.getComponentCount();
        Component comp;
        Dimension sz, ps;
        Point loc;

        for (int i = 0 ; i < ncomponents ; i++) {
            comp = target.getComponent(i);

            if(comp.isVisible()) {
                sz   = comp.getSize();    
		ps   = comp.getPreferredSize();
                loc  = comp.getLocation(); // getComponentLocation(comp)

		if(sz.width < ps.width || sz.height < ps.height)
		    sz = ps;

                comp.setBounds(loc.x, loc.y, sz.width, sz.height);
            }
        }
    }
/*
    private Point getComponentLocation(Component comp) {
	Insets insets = comp.getParent().getInsets();
	Point  loc    = comp.getLocation();

	if( ! hash.containsKey(comp)) {
	    addComponentToHashtable(comp);
	}
	else {
	    Point oldLocation = (Point)hash.get(comp);

	    if(oldLocation.x != loc.x ||
	       oldLocation.y != loc.y) {
		addComponentToHashtable(comp);
	    }
	}
	return comp.getLocation();
    }
    private void addComponentToHashtable(Component comp) {
	Insets insets = comp.getParent().getInsets();
	Point  loc    = comp.getLocation();

	comp.setLocation(loc.x + insets.left, 
			 loc.y + insets.top);
	hash.put(comp, comp.getLocation());
    }
*/
}
