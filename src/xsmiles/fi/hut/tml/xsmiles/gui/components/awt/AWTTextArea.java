/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.*;
import java.awt.*;

import javax.swing.JTextArea;

/**
 *  The only addition is add and remove
 * this is extended from SwingComponent, which will add captions and styling etc
 * @author Mikko Honkala
 */
public class AWTTextArea extends AWTInput implements XTextArea 
{
    //protected JScrollPane scrollPane;
  String initialText;

  public AWTTextArea(String s, GUI g) 
  {
	super(g);
	initialText=s;
	init();
  }
  
  /** creates the content component */
  public Component createContent()
  {
      textcomponent=new TextArea();
      textcomponent.setText(initialText);
      return textcomponent;
  }
  
  
    public void setWordWrapping(boolean wrap) {
		// in AWT, the scrollbars cannot be changed run-time (duh...)
  }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTextArea#append(java.lang.String)
     */
    public void append(String text)
    {
        ((TextArea)this.textcomponent).append(text);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTextArea#setCaretPosition(int)
     */
    public void setCaretPosition(int pos)
    {
            ((TextArea)this.textcomponent).setCaretPosition(pos);
    }
    

}
