/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;
import java.awt.*;
import java.net.URL;
import javax.swing.GrayFilter;
import javax.swing.ImageIcon;
import fi.hut.tml.xsmiles.gui.components.awt.*;
/**
 *
 *
 *
 */
public class XAImageIcon extends XAComponent {
    
    private MediaTracker mt;
    private Image        image;
    private Image        disabledImage;
    private int          imageWidth, imageHeight;
    private int          paintX, paintY;
    private boolean      gray=true;
    
    public XAImageIcon(String fileName, boolean gray) {
	this.gray=gray;
	loadImage(fileName);        
	initSize();
    }
    
    public XAImageIcon(Image image, boolean gray) {
	this.image = image;
	this.gray=gray;
	initSize();
    }
    
  public XAImageIcon(String fileName) {
	this.gray=false;
	loadImage(fileName);        
	initSize();
    }
    
    public XAImageIcon(Image image) {
	this.image = image;
	gray=false;
	initSize();
    }
    
    private void initSize() {
	if(gray) {
	    GrayFilter gF=new GrayFilter(false, 70);
	    image=gF.createDisabledImage(image);
	    
	    mt = new MediaTracker(this);
	    mt.addImage(image, 1);
	    try { 
		mt.waitForID(1);
	    } catch (InterruptedException e) {
		Log.error(e);
	    }
	}
	
	imageWidth  = image.getWidth(this);
	imageHeight = image.getHeight(this);
    super.setSize(imageWidth, imageHeight);
	
	//paintX = (getSize().width  / 2) - (imageWidth  / 2); // middle X
	//paintY = (getSize().height / 2) - (imageHeight / 2); // middle Y
    }
    
    public Image getImage() {
	return image;
    }

    public ImageIcon getImageIcon() {
	return new ImageIcon(image);
    }
    
    public int getIconWidth() {
	return super.getSize().width;
    }
    
    public int getIconHeight() {
	return super.getSize().height;
    }
    
    public void setSize(Dimension d) {
	super.setSize(d);
	initSize();
    }
    
    public void setSize(int x, int y) {
	super.setSize(x, y);
	initSize();
    }
    static int no =0;
    public void loadImage(String s) 
	{
        if (s.startsWith("./")) s=s.substring(2);
	  URL resURL=this.getClass().getResource(s);
	  //Log.debug("*** resource:"+s+" resourceURL: "+resURL);
		Toolkit tk = Toolkit.getDefaultToolkit();
		mt = new MediaTracker(this);
		// 1st try to find the image in the resources, 2nd from a file
		if (resURL!=null) 
			image=tk.getImage(resURL);
		else 
			image = tk.getImage(s);
		mt.addImage(image, ++no);
		try { 
		    mt.waitForID(no);
		} catch (InterruptedException e) {
		    Log.error(e);
		}
    }
    
    public void paint(Graphics g) {
	super.paint(g);
	g.drawImage(image,0,0, this);
    }
}
