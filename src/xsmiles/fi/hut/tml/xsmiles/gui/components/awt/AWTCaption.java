/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.XCaption;
import java.awt.*;
import java.awt.event.*;

/**
 * A caption object
 * @author Mikko Honkala
 * @author juha (mod from SwingCaption)
 */
public class AWTCaption extends AWTStylableComponent
implements XCaption
{
    /** the caption component */
    protected NonFocusableLabel caption;
    
    public AWTCaption(String text)
    {
        super();
        this.caption=new NonFocusableLabel(text);
        this.content=this.caption;
        this.init();
    }
    
    /**
     * super is called always after init, so it can finish up
     */
    public void init()
    {
        caption.setEditable(false);
        //caption.setBackground(NORMALCOLOR);
        //caption.setBorder(new javax.swing.border.EmptyBorder(0,0,0,0));
        super.init();
    }
    
    public Color getDefaultBackgroundColor()
    {
        return AWTCSSFormatter.getTransparentColor();
    }
    
    public boolean isCaption()
    {
        return true;
    }
    
    /** set the minimum size of this component */
    public void setMinimumSize(Dimension min)
    {
        // TODO
    }
    
    public String getText()
    {
        return this.caption.getText();
    }
    
    public void setText(String text)
    {
        this.caption.setText(text);
    }
    public void addTextListener(TextListener tl)
    {
        // TODO
    }
    public void removeTextListener(TextListener tl)
    {
        // TODO
    }
    /**
     * non - focusable TextField to be used as a caption
     */
    public class NonFocusableLabel extends TextField
    {
        public NonFocusableLabel(String text)
        {
            super(text);
        }
        public boolean isFocusTraversable()
        {
            return false;
        }
        
    }
}

