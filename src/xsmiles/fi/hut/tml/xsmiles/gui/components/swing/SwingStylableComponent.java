/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import fi.hut.tml.xsmiles.gui.components.CSSFormatter;
import fi.hut.tml.xsmiles.gui.components.general.ComponentBase;
/**
 * This ontains common functionality
 * to style each of the components (caption, content)
 *
 * @author Mikko Honkala
 */
public  class SwingStylableComponent extends ComponentBase
{
    
    CSSFormatter formatter;

    public SwingStylableComponent()
    {
        super();
    }
    public SwingStylableComponent(JComponent comp)
    {
        this.content=comp;
        this.init();
    }
    
    public void init()
    {
        float alignment= Component.LEFT_ALIGNMENT;
        ((JComponent)this.getSizableComponent()).setAlignmentX(alignment);
        //content.setOpaque(false);
    }

    
    public boolean isCaption()
    {
        return false;
    }
    
    
    public void sizeComponent(double zoom, Dimension size)
    {
        //((JComponent)this.getSizableComponent()).setPreferredSize(size);
        this.getSizableComponent().setSize(size);
        ((JComponent)this.getSizableComponent()).setMinimumSize(size);
        ((JComponent)this.getSizableComponent()).setMaximumSize(size);
    }
    
    public CSSFormatter getFormatter()
    {
        return SwingCSSFormatter.getInstance();
    }
    

    
    public void setBackground(Color bg)
    {
        super.setBackground(bg);
        if(bg!=null)
            if (bg!=SwingCSSFormatter.getTransparentColor())
                ((JComponent)this.getStylableComponent()).setOpaque(true);
    }
    
    /**
     * Sets the components Tooltip (Hint) text
     */
    public void setHintText(String hint)
    {
        this.hintText=hint;
        this.createHint();
    }
    protected void createHint()
    {
        Component main = this.getAddableComponent();
        if (hintText!=null && hintText.length()>0)
        {
            ((JComponent)main).setToolTipText(hintText);
            // If the component is a container, set the tooltip for all of its children
            if (main instanceof JPanel)
            {
                Container c = (Container)main;
                Component[] comps = c.getComponents();
                //			Log.debug("Setting multi hint: "+h.getText());
                this.setHintRecursively(comps, hintText);
            }
        }
    }
    protected void setHintRecursively(Component[] comps, String hinttext)
    {
        for (int i=0; i<comps.length; i++)
        {
            JComponent comp = (JComponent)comps[i];
            if (comp instanceof JPanel)
            {
                Component[] comps2 = comp.getComponents();
                //			Log.debug("Setting multi hint: "+h.getText());
                this.setHintRecursively(comps2, hintText);
            }
            comp.setToolTipText(hintText);
        }
    }
    protected static KeyStroke HELPKEY = KeyStroke.getKeyStroke("F1");
    
    /** add a listener for help events */
    public void addHelpListener(ActionListener fl)
    {
        JComponent comp = (JComponent)this.getComponent();
        comp.registerKeyboardAction(fl,"help",
        HELPKEY,JComponent.WHEN_FOCUSED);
    }
    
    /** remove a listener for help events */
    public void removeHelpListener(ActionListener fl)
    {
        JComponent comp = (JComponent)this.getComponent();
        comp.unregisterKeyboardAction(HELPKEY);
    }
    
    
}

