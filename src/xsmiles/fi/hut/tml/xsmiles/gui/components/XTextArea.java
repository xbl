/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

/**
 *  The only addition is add and remove
 *  No extra functionality just the basics.
 */
public interface XTextArea extends XText 
{
	public void setWordWrapping(boolean wrap);
	public void append(String text);
	public void setCaretPosition(int pos);
	
}
