/* X-Smiles (See XSMILES_LICENCE.txt)
 */



package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
    

/**
 * Auth dialog 
 */

  public class AuthDialog extends AWTDialog implements XAuthDialog  
  {
  protected String usernameStr,passwordStr;
  public AuthDialog(Frame parentFrame)
  {
      super(parentFrame);
  }      
  
     public void showDialog() {
       final Dialog jd = new Dialog (this.parent,this.title==null?"":this.title, true);
       
       
       
       jd.setResizable(false);
       jd.setLayout (new GridLayout (0, 1));
       //jd.setLayout (new FlowLayout());
       Label jl = new Label (this.prompt==null?"":this.prompt);
       jd.add (jl);
       
       Panel userp = new Panel(new GridLayout(0,2));
       Label userlabel = new Label("Username:");
       TextField username = new TextField();
       username.setBackground (Color.white);
       userp.add(userlabel);
       userp.add(username);

       Label passlabel = new Label("Password:");
       TextField password = new TextField();
       password.setEchoChar ('*');
       password.setSize(180,password.getSize().height);
       password.setBackground (Color.white);
       userp.add(passlabel);
       userp.add(password);
       jd.add (userp);
       
       Panel buttonp = new Panel();
       Button jb = new Button ("OK");
       buttonp.add (jb);
       jb.addActionListener (new ActionListener() {
         public void actionPerformed (ActionEvent e) {
             setCancelled(false);
           jd.dispose();
         }
       });
       Button cancelb = new Button ("Cancel");
       buttonp.add (cancelb);
       cancelb.addActionListener (new ActionListener() {
         public void actionPerformed (ActionEvent e) {
           setCancelled(true);  
           jd.dispose();
         }
       });
       jd.add(buttonp);
       password.addActionListener (new ActionListener() {
         public void actionPerformed (ActionEvent e) {
           jd.dispose();
         }
       });
        Dimension ss = jd.getToolkit().getScreenSize();
        Dimension ds = jd.getPreferredSize();
	
        jd.setLocation((ss.width  - ds.width) / 2,
                               (ss.height - ds.height) / 2);

        jd.setSize(ds);

       
       jd.pack();
       jd.setVisible(true);
       this.passwordStr = password.getText();
       this.usernameStr = username.getText();
         super.showDialog();
     }
    public String getUser()
    {
        return this.usernameStr;
    }
    public String getPasswd()
    {
        return this.passwordStr;
    }
  }



