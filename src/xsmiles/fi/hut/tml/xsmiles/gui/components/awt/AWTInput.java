/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.*;
import java.awt.*;
import java.awt.event.*;

/**
 * textinput line
 * this is extended from AWTComponent, which will add captions and styling etc
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public class AWTInput extends AWTTextComponent implements XInput, XSecret
{
    
    
    //private JTextField visualComponent;
    /** the action for enter keypress */
    protected char echoChar = '*';
    protected boolean isSecret = false;
    final static Dimension minSize = new Dimension(40,5);
    
    
    /**
     * @param c The number of columns initially
     * gui is given for the inputmode functionality
     */
    public AWTInput(GUI gui)
    {
        super(gui);
        init();
    }
    public AWTInput(char passwdchar, GUI gui)
    {
        super(gui);
        this.echoChar = passwdchar;
        this.isSecret = true;
        init();
    }
    
    
    /**
     * in init the subclass always creates its component first and then calls
     * super.init
     */
    public void init()
    {
        this.content = this.createContent();
        // Super will then add the content component to its container
        super.init();
    }
    
    /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
        return minSize;
    }
    
    /** creates the content component */
    public Component createContent()
    {
        if (!isSecret)
        {
            textcomponent=new TextField();
        }
        else
        {
            textcomponent = new SecretField();
            ((SecretField)this.textcomponent).setEchoChar(echoChar);
            //((PasswordField)textcomponent).setEchoChar(echoChar);
        }
        return textcomponent;
    }
    public String getPassword()
    {
        return this.getText();
    }
    
    /**
     * the default background color for this component
     * null = transparent.
     */
    
    
    
    public void addActionListener(ActionListener al)
    {
        ((TextField)this.textcomponent).addActionListener(al);
        /*
        this.textcomponent.registerKeyboardAction(
                al,
                ENTER_STROKED,
                KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER,0),
                JComponent.WHEN_FOCUSED
        );
        */
    }
    /**
     * @param b Active or not active
     */
    public void setEnabled(boolean b)
    {
        // we don't want to change the font color
        //this.content.getStylableComponent().setEnabled(b);
        this.textcomponent.setEditable(b);
    }
    
    public void removeActionListener(ActionListener al)
    {
       // this.textcomponent.resetKeyboardActions();
    }
    private class SecretField extends TextField
    {
        /*public char getEchoChar() 
        {
            return '*';
        }*/
    }
    public void removeBorders()
    {
        // TODO Auto-generated method stub
        
    }
    

}
