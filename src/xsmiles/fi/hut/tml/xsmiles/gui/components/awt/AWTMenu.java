/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Menu;
import java.awt.MenuItem;

import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;

/**
 * @author Mikko Honkala
 *
 */
public class AWTMenu implements XMenu
{
    Menu m;
    String name;
    public AWTMenu(String menu)
    {
        name=menu;
        init();
    }
    
    public void init()
    {
        m=new Menu(name);
    }
    
    public void add(XMenu menu)
    {
        m.add((Menu)menu.getObject());
    }
    public void add(XMenuItem menuitem)
    {
        m.add((MenuItem)menuitem.getObject());
    }
    
    public Object getObject()
    {
        return m;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#addSeparator()
     */
    public void addSeparator()
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#removeAll()
     */
    public void removeAll()
    {
        m.removeAll();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#remove(fi.hut.tml.xsmiles.gui.components.XMenu)
     */
    public void remove(XMenu item)
    {
        m.remove((Menu)item.getObject());
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#remove(fi.hut.tml.xsmiles.gui.components.XMenuItem)
     */
    public void remove(XMenuItem item)
    {
        m.remove((MenuItem)item.getObject());
        
    }

}
