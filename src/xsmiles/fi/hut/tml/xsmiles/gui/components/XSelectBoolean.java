package fi.hut.tml.xsmiles.gui.components;

import java.util.Vector;

import java.awt.event.ItemListener;

/**
 * The selectBoolean element (usually a single checkbox)
 *
 * @author Mikko Honkala
 */
public interface XSelectBoolean extends XComponent 
{
  public void setSelected(boolean selected);
  public boolean getSelected();
  public void addItemListener(ItemListener il);
  public void removeItemListener(ItemListener il);
}
