/* 
 * X-Smiles
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.Language;

/**
 * A swing based dynamic menu for switching GUIs
 *
 * @author Juha Vierinen
 * @version 0.2
 */
public class LWGuiMenu extends JMenu {

    BrowserWindow browser;
    ChangeGuiListener changeGuiListener;
    
    public LWGuiMenu(BrowserWindow b) {
        super(Language.CHANGEGUI);
        browser=b;
	
        changeGuiListener=new ChangeGuiListener();

        for(int i=0;i<browser.getGUIManager().getNumGUIs();i++) {
            JMenuItem it = new JMenuItem((String) 
                                         browser.getGUIManager().getGUINames().elementAt(i));
            it.addActionListener(changeGuiListener);
            add(it);
        }
    }
    
    private class ChangeGuiListener implements ActionListener {
	
        public void actionPerformed(ActionEvent e) {
            String com = e.getActionCommand();
            Vector guiNames=browser.getGUIManager().getGUINames();
            if(com!=null && !com.equals(""))
                browser.getGUIManager().showGUI(com,true);
        }
    }
}
