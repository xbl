/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;


import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XUpload;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import java.util.Hashtable;
import java.io.*;
import java.net.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComponent;

import java.awt.AWTEventMulticaster;


/**
 * The XForms/Button element
 * @author Mikko Honkala 
 */
public class SwingUpload extends SwingButton 
	implements XUpload, ActionListener
{
  protected ComponentFactory factory;
  ActionListener actionlistener = null;

	
  /**
   * Constructs a new XButton (XForms/button).
   *
   */
  public SwingUpload(String captionText, ComponentFactory cf)
  {
	  super(captionText,null);
	  factory = cf;
  }
	
  public void init()
  {
	  super.init();
	  this.button.addActionListener(this);
  }
  
	public void actionPerformed(java.awt.event.ActionEvent actionEvent) 
	{
		if (actionEvent.getSource()==this) return;
		else
		{
			// open new dialog
			XFileDialog dia = factory.getXFileDialog(false);
			int ret = dia.select();
			if (ret == XFileDialog.SELECTION_OK)
			{
				File selected = dia.getSelectedFile();
				ActionEvent ev = new ActionEvent(this,-1,selected.toString(),-1);
	            actionlistener.actionPerformed(ev);
			}
	  }
  }
    public void addActionListener(ActionListener tl)
  {
	  actionlistener = AWTEventMulticaster.add(actionlistener, tl);
  }
  public void removeActionListener(ActionListener tl)
  {
	  actionlistener = AWTEventMulticaster.remove(actionlistener, tl);
  }

  
}
