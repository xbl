/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.util.Vector;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JScrollPane;

import fi.hut.tml.xsmiles.gui.components.awt.AWTFocusManager;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;

import fi.hut.tml.xsmiles.gui.components.XFocusManager;

/**
 * 
 * This is the base class that is extended by FocusPointProviders
 * @author Pablo Cesar, Mikko Honkala
 * 
 *  
 */

public class SwingFocusManager extends AWTFocusManager{

    public SwingFocusManager(){
	super();
    }


    public boolean isScrollPane(Container cont){
	return (cont instanceof JScrollPane);
    }

    public Point getScrollPosition(Component component){
	return ((JScrollPane)component).getViewport().getViewPosition();
    }
    
}
