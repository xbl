/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.apps.svgbrowser;
package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.gui.components.swing.XAImageIcon;
import fi.hut.tml.xsmiles.gui.components.awt.*;
// for version
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Color;

import java.net.URL;

import java.util.Hashtable;



import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import java.awt.*;
//import javax.swing.*;
import java.awt.event.*;

/**
 * A dialog showing the revision of the Batik viewer as well
 * as the list of contributors.
 * The dialog can be dismissed by click or by escaping.
 *
 * @author <a href="mailto:vincent.hardy@eng.sun.com">Vincent Hardy</a>
 * @version $Id: AboutDialog2.java,v 1.2 2003/10/10 16:08:44 honkkis Exp $
 */
public class AboutDialog2 extends Window {

    private final int FONTSIZE=12;
  private final String FONT="sans-serif";
    public static final String ICON_XSMILES_SPLASH 
        = "img/splash.jpg";


    public static final String LABEL_XSMILES_PROJECT
        = "Version "+Browser.version;

    public static final String LABEL_CONTRIBUTORS
        = "Contributor list:";
    
  public static final int TEXT_X = 180;
    public static final int TEXT_Y = 280;

  // for progress.
  static XAImageIcon icon,icon2;
  //static DoubleBufferedContainer panel;
  static Panel panel;
  //static XAProgressBar pb=new XAProgressBar();

  

    /**
     * Default constructor
     */
    /*
    public AboutDialog(String name){
        //super();
        buildGUI();
        xsmilesInit();
    }*/

    public AboutDialog2(Frame owner){
        super(owner);
        buildGUI();

        addKeyListener(new KeyAdapter(){
                public void keyPressed(KeyEvent e){
                    if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
                        setVisible(false);
                        dispose();
                    }
                }
            });

        addMouseListener(new MouseAdapter(){
                public void mousePressed(MouseEvent e){
                    setVisible(false);
                    dispose();
                }
            });
        xsmilesInit();    

    }
  
  public static void increment() 
  {
    //pb.increment();
  }
    
    protected void xsmilesInit()
    {
        Window initDialog=this;

        /*final JProgressBar pb = new JProgressBar(0, 3);
        initDialog.getContentPane().add("South", pb);
         */

        // Work around pack() bug on some platforms
        Dimension ss = initDialog.getToolkit().getScreenSize();
	Dimension ds = initDialog.getPreferredSize();
	
        initDialog.setLocation((ss.width  - ds.width) / 2,
                               (ss.height - ds.height) / 2);

	initDialog.setSize(ds);
	initDialog.setVisible(true);
    }

    public void setLocationRelativeTo(Frame f) {
        Dimension invokerSize = f.getSize();
        Point loc = f.getLocation();
        Point invokerScreenLocation = new Point(loc.x, loc.y);

        Rectangle bounds = getBounds();
        int  dx = invokerScreenLocation.x+((invokerSize.width-bounds.width)/2);
        int  dy = invokerScreenLocation.y+((invokerSize.height - bounds.height)/2);
        Dimension screenSize = getToolkit().getScreenSize();

        if (dy+bounds.height>screenSize.height) {
            dy = screenSize.height-bounds.height;
            dx = invokerScreenLocation.x<(screenSize.width>>1) ? invokerScreenLocation.x+invokerSize.width :
                invokerScreenLocation.x-bounds.width;
        }
        if (dx+bounds.width>screenSize.width) {
            dx = screenSize.width-bounds.width;
        }

        if (dx<0) dx = 0;
        if (dy<0) dy = 0;
        setLocation(dx, dy);
    }

    /**
     * Populates this window
     */
    protected void buildGUI(){
      //Panel panel = new Panel(new BorderLayout(5, 5));
      panel = new Panel(); // DoubleBufferedContainer();
      panel.setLayout(new BulletinLayout());

      panel.setBackground(Color.white);
      //pb.setBounds(0,0,400,3);

      // Add splash image
      //
      try
        {
	  icon=new Painter(ICON_XSMILES_SPLASH,LABEL_XSMILES_PROJECT,new Dimension(TEXT_X,TEXT_Y));

	  //panel.add(pb);
	  panel.add(icon);

	  Window initDialog=this;
	  Dimension ds = initDialog.getPreferredSize();
	  //panel.paintImmediately(0,0,ds.width,ds.height);
	  //pb.increment();
	  panel.repaint();

        } catch (Exception e)
	  {
            Log.error(e);
	  }


        //Painter painter = new Painter();
        //painter.setSize(300,400);
        //panel.add(BorderLayout.SOUTH, new Label(tagName));
        //panel.add(painter);
        //painter.setVisible(true);
        setBackground(Color.white);
        this.setBackground(Color.white);

        this.add(panel);
    }
    
  private class Painter extends XAImageIcon {
      private boolean focus;
      private String contents;
      private Dimension fTextPosition;
      
      public Painter(String imgurl,String text,Dimension textposition)
      {
        super(imgurl);
        this.contents=text;
        this.fTextPosition=textposition;
      }
	
	private int h;	
	private int w;

	
	public void paint(Graphics g0) {
	  super.paint(g0);
	  //Graphics2D g=(Graphics2D) g0;
	  Graphics g = g0;
	  h = (int)this.getSize().height;
	  //height = (int)this.getSize().height;
	  //width = (int)this.getSize().width;
	  w = (int)this.getSize().width;
	  Font f = new Font(FONT, Font.BOLD, FONTSIZE);
	  g.setFont(f);
	  //g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	  g.setColor(Color.black);
	  g.drawString(contents, (int)fTextPosition.width, (int)fTextPosition.height);

	  
	  // The caret.. fuck the blinking caret for now..

	}
	/**
	 * This component is focusable
	 */
	public boolean isFocusTraversable() {
	    return true;
	}
	
	/**
	 * This component is Lightweight
	 */
	public boolean isLightWeight() {
	    return true;
	}
    }

    

}
