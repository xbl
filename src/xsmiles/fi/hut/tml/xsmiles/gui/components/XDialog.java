/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;
import fi.hut.tml.xsmiles.dom.VisualComponentService;

import java.io.File;
import java.awt.Component;
import java.awt.Dimension;

import java.awt.event.FocusListener;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;

import org.w3c.dom.css.CSSStyleDeclaration;
/**
 *  Interface to plain GUI component
 *  The GUI component package contains an abstract set of components 
 *  that can generally be used.
 */
public interface XDialog {
	public static final int SELECTION_OK = 0;
	public static final int SELECTION_FAILED = -1;
	
	/** open the selection box and return a failure/ok code */
    public void setTitle(String title);
    public void setPrompt(String prompt);
	/** open the selection box and return a failure/ok code */
	public int select();

}

