/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.XTextArea;

/**
 *  The only addition is add and remove
 * this is extended from SwingComponent, which will add captions and styling etc
 * @author Mikko Honkala
 */
public class SwingTextArea extends SwingTextComponent implements XTextArea 
{
	final static Dimension minSize = new Dimension(50,10);

	/** the Scrollpane */
	public JScrollPane scrollPane;

    String initialText;

  public SwingTextArea(String s, GUI gui) 
  {
	super(gui);
	initialText=s;
	init();
  }
  
  
  public void init()
  {
	  this.content = this.createContent();
	  // Super will then add the content component to its container
	  super.init();
  }
  
  	/** return the minimum size for this component at zoom level 1.0 */
	public Dimension getMinimumSize()
	{
		return minSize;
	}
  
  /** creates the content component */
  public Component createContent()
  {
    textcomponent=new SmoothTextArea();
	textcomponent.setText(initialText);
	scrollPane = new JScrollPane(textcomponent,
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	//			textcomponent.addFocusListener(this);
	this.scrollPane.setOpaque(false);

	initialText=null;
	return scrollPane;
  }
  
  public void setWordWrapping(boolean wrap) {
		JTextArea area = (JTextArea)textcomponent;
		area.setLineWrap(wrap);
		area.setWrapStyleWord(wrap);
  }
  
	public Component getContentComponent()
	{
		return this.scrollPane;
	}
	
	public Component getAddableComponent()
	{
		return this.scrollPane;
	}
	public Component getSizableComponent()
	{
		return this.scrollPane;
	}
	public Component getStylableComponent()
	{
		return this.textcomponent;
	}
            
    /**
     * the default background color for this component
     * null = transparent.
     */
    public Color getDefaultBackgroundColor()
    {
        //return CompatibilityFactory.getCompatibility().createTransparentColor();
        return Color.white;
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTextArea#append(java.lang.String)
     */
    public void append(String text)
    {
        // TODO Auto-generated method stub
        ((JTextArea)this.textcomponent).append(text);
        
    }
    
    /** a textarea with antialiasing */
    private class SmoothTextArea extends JTextArea
    {
    
        SmoothTextArea ()
        {
            super();
        }
        SmoothTextArea (String text)
        {
            super(text);
        }
        public void paint(Graphics g)
        {
            CompatibilityFactory.getGraphicsCompatibility().setAntialias(true,g);
            super.paint(g);
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTextArea#setCaretPosition(int)
     */
    public void setCaretPosition(int pos)
    {
        ((JTextArea)this.textcomponent).setCaretPosition(pos);
    }

}
