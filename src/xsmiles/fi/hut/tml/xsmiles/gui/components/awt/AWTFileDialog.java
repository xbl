/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;

import java.io.File;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.*;

/**
 * The Swing file chooser component
 * @author Mikko Honkala
 * @author awt mod juha
 */
public class AWTFileDialog implements XFileDialog
{
    protected BrowserWindow browser;
    protected File selection;
    protected boolean fSave;
    protected String fFilename;
    protected Frame topLevelFrame;
    protected File dir;
    
    protected FileDialog fc;

    public AWTFileDialog(BrowserWindow w,boolean save, Frame topLevel)
    {
        browser = w;
        fSave=save;
        topLevelFrame=topLevel;
    }
    
    public AWTFileDialog(BrowserWindow w,boolean save,String filename, Frame topLevel)
    {
        this(w,save,topLevel);
        fFilename=filename;
    }
    
    /** open the selection box and return a failure/ok code */
    public int select()
    {
        selection = this.showFileDialog();
        if (selection==null) return XFileDialog.SELECTION_FAILED;
        else return XFileDialog.SELECTION_OK;
    }
    
    /** return the file user selected */
    public File getSelectedFile()
    {
        return selection;
    }
    
    public synchronized File showFileDialog()
    {
        // this would make the current dir the base dir for selection
        //fc=new JFileChooser(".");
        File file=null;
        try {
	            fc=new FileDialog(topLevelFrame);
	            if (dir==null)
	            {
	                File currDir= new File(".").getAbsoluteFile();
	                fc.setDirectory(currDir.toString());
	            } else
	            {
	                if (dir.isFile()) dir = dir.getParentFile();
	                fc.setDirectory(dir.getAbsolutePath());
	            }
	        if (fFilename!=null)
	        {
	            //File currDir = fc.getCurrentDirectory();
	            File preselect = new File(fFilename); 
	            fc.setFile(preselect.getCanonicalPath().toString());
	        }
	        int returnVal=0;
            if(fSave)
            {
                fc.setMode(FileDialog.SAVE);
            }
            else
            {    
                fc.setMode(FileDialog.LOAD);
            }
            fc.setVisible(true);
            String selectedItem=fc.getFile();
            
            if (selectedItem!=null)
            {
                file = new File(fc.getDirectory(),selectedItem);
                dir=file;
            }
        } catch(Exception e) {
            Log.error(e);
        }
        Log.debug("Selected: "+file);
        return file;
    }
    public void setTitle(String title)
    {
    }
    public void setPrompt(String prompt)
    {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XFileDialog#setFile(java.io.File)
     */
    public void setFile(File f)
    {
        this.dir=f;
        
    }
	
}
