/* X-Smiles
 *
 * F THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.Language;	      // All GUI language strings found here
import fi.hut.tml.xsmiles.gui.met.GUIImpl;


/**
 * A Menu for switching themes.
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class LWThemeMenu extends JMenu 
{    
    ChangeListener changeListener;
    GUI gui;
    Vector styles, styleClasses;
    BrowserWindow browser;
    
    public LWThemeMenu(BrowserWindow b, GUI g) 
    {
        super(Language.CHANGESKIN);
        gui=g;
        browser = b;
        styles=new Vector();
        styleClasses=new Vector();
        styles.addElement("System");
        styles.addElement("Crossplatform");
        styles.addElement("Metouia");
        styleClasses.addElement(UIManager.getSystemLookAndFeelClassName());
        styleClasses.addElement(UIManager.getCrossPlatformLookAndFeelClassName());
        styleClasses.addElement("net.sourceforge.mlf.metouia.MetouiaLookAndFeel");
        
        //styleClasses.addElement("");
        changeListener=new ChangeListener();
        
        for(int i=0;i<styles.size();i++) 
            { 
                JMenuItem it = new JMenuItem((String)styles.elementAt(i));
                it.addActionListener(changeListener);
                add(it);
            }
    }
    
    private class ChangeListener implements ActionListener 
    {
        public void actionPerformed(ActionEvent e) 
        {
            try {
                String com = e.getActionCommand();
                int in = styles.indexOf(com);
                String cl = (String)styleClasses.elementAt(in);
                UIManager.setLookAndFeel((LookAndFeel)Class.forName(cl).newInstance());
                SwingUtilities.updateComponentTreeUI((JFrame)gui.getWindow());
                browser.getBrowserConfigurer().setProperty("gui/theme",com);
                browser.getGUIManager().showGUI(browser.getGUIManager().getCurrentGUIName(),false);
            } catch(Exception ee) {
                Log.debug("Sorry, the L&F is not supported.");
            }
        }
    }
}
