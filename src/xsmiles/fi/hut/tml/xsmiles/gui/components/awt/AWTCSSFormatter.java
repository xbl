/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.gui.components.CSSFormatter;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
/**
 * CSS specific operations on components
 * @author Mikko Honkala
 */
public class AWTCSSFormatter extends CSSFormatter
{
    protected static CSSFormatter formatter;
    public static CSSFormatter getInstance()
    {
        if (formatter==null) formatter=new AWTCSSFormatter();
        return formatter;
    }
    public AWTCSSFormatter()
    {
    }
	public void formatComponent(Component comp, CSSStyleDeclaration style, Color defaultComponentColor)
	{
		formatComponent(comp,style,defaultComponentColor,1.0);
	}
    public void formatComponent(Component comp, CSSStyleDeclaration style, Color defaultComponentColor,double zoom)
    {
        if (style==null) return;
        if (comp==null)
        {
            Log.error("CSSFormatter.formatComponent : Component was null");
            return;
        }
        
        // MH: the new renderer
        //Font font = StyleAWTConvertor.getFontObj(style);
        Font font = StyleGenerator.getCSSFontObj(style,zoom);
        
        // Set the style
        if (comp != null)
        {
            comp.setFont(font);
            Color color, bgcolor;
            
            //bgcolor = (Color)style.getAttribute(XMLCSS.ATTRIBUTE_BACKGROUND_COLOR);
            
            bgcolor = StyleGenerator.parseColor(style.getPropertyCSSValue(CSSConstants.CSS_BACKGROUND_COLOR_PROPERTY));
            if (bgcolor==null) bgcolor=defaultComponentColor;
              if (bgcolor == null) {
                //Log.debug("bgcolor: null");
                // TODO: should default values be resolved somewhere else
                bgcolor = StyleGenerator.getTransparentColor();
              }
            if (bgcolor!=null)
            {
                boolean transparent = StyleGenerator.isTransparent(bgcolor);
                setTransparency(comp,bgcolor,transparent);
                if (!transparent) comp.setBackground(bgcolor);
            }
            
            // get color property
            color = StyleGenerator.parseColor(style.getPropertyCSSValue(CSSConstants.CSS_COLOR_PROPERTY));
            if (color == null)
            {
                color = Color.black;
            }
            comp.setForeground(color);
        }
    }
    
    public void setTransparency(Component comp, Color bgcolor, boolean transparent)
    {
        // in awt there is no setOpaque
        return;
    }
    
    public static Color getTransparentColor()
    {
        return StyleGenerator.getTransparentColor();
    }
}
