/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Container;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.ItemSelectable;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.ButtonGroup;
import javax.swing.text.JTextComponent;
import java.util.Enumeration;
import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    

/**
 * textinput line
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public abstract class SwingSelectOne extends SwingSelectBase 
	implements XSelectOne,  ItemSelectable
{
  protected JComponent selectComponent;
  
  /**
   * @param v The vector containing all items, the first item is default
   * @param s The style of the selectOne
   */
  public SwingSelectOne() 
  {
	  super();
  }
  public void init() 
  {
	content=this.createComponent();
    selectComponent.setSize(selectComponent.getPreferredSize());
    selectComponent.setVisible(true);
	super.init();
  }
  

  public abstract Component createComponent();
	/** notify a change to listeners, called internally */

  protected void notifyChange(Object item, int status)
	{
      ItemEvent ie=new ItemEvent(this, -1, item, status);
	  if (this.itemlistener!=null) this.itemlistener.itemStateChanged(ie);
	}

	/** method from itemselectable interface */
	public Object[] getSelectedObjects()
	{
		return null;
          //Returns the selected items or null if no items are selected.
	}
}
