/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;

import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;

/**
 * @author Mikko Honkala
 */
public class AWTMenuBar implements XMenuBar
{
    MenuBar bar;
    public AWTMenuBar()
    {
        this.init();
    }
    
    public void init()
    {
        this.bar=new MenuBar();
    }
    
    public void addMenu(XMenu menu)
    {
        this.bar.add((Menu)menu.getObject());
    }
    
    public void addToFrame(Frame f)
    {
        f.setMenuBar(bar);
    }

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.gui.components.XMenuBar#getObject()
	 */
	public Object getObject() {
		// TODO Auto-generated method stub
		return bar;
	}
}
