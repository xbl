/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import java.awt.Component;

/**
 *  The only addition is add and remove
 */
public interface XContainer extends XComponent
{
  /**
   * @param c The component to be added
   */
  public void add(XComponent c); 
  /**
   * @param c The component to be added
   */
  public void add(Component c); 
  
  /**
   * @param c Remove component c
   */
  public void remove(XComponent c);

  /**
   * @param c The component to be added
   */
  public void remove(Component c); 
  
  /**
   * Remove all components from conatiner.
   */
  public void removeAll();
}

