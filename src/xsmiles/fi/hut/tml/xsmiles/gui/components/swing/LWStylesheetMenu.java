package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.GUIManager;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.BrowserWindow;
import javax.swing.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;

/** 
 * A Menu for switching Stylesheets
 *
 * @author Juha
 */
public class LWStylesheetMenu extends JMenu 
{    
    ListenerWithStyle styleListener;
    BrowserWindow browser;
    JMenuItem dissed;
    
    /**
     * @param The gui, so that menu can mess around with gui's
     * BrowserConstraints
     */
    public LWStylesheetMenu(BrowserWindow b) 
    {
        super(Language.STYLESHEET);
        browser=b;
        styleListener=new ListenerWithStyle();
    }
    
    /**
     * Set the stylesheet titles to put in stylesheet menu
     */
    public void setStylesheets() 
    {
        if(browser.getXMLDocument()!=null) {
            int i;
            removeAll();
            Enumeration e=browser.getXMLDocument().getStylesheetTitles().elements();
            String title=browser.getXMLDocument().getCurrentStylesheetTitle();
	
            for(i=0;e.hasMoreElements();i++) {
                String s=(String)e.nextElement();
                JMenuItem it=new JMenuItem(s);
                if(s.equals(title)) {
                    it.setEnabled(false);
                    dissed=it;
                }
                it.addActionListener(styleListener);
                add(it);
            }
            if(i<2)
                setEnabled(false);
            else
                setEnabled(true);
        }
    }
    
    private class ListenerWithStyle implements ActionListener 
    {
        /**
         * @param ActionEvent encapsulating the title of stylesheet
         * change to disabled one to enabled
         * reload page to make changes effect
         */
        public void actionPerformed(ActionEvent e) {
            dissed.setEnabled(true);
            dissed=(JMenuItem)e.getSource();
            dissed.setEnabled(false);

            browser.setPreferredStylesheetTitle(e.getActionCommand());	  

            // Tell browser window to reload
            browser.navigate(NavigationState.RELOAD);
	  
        }
    }
    
    
    
}
