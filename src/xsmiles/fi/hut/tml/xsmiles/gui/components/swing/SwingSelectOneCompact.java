/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Component;
import java.awt.ItemSelectable;
import java.awt.event.*;

import javax.swing.event.*;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    


/**
 * select one compact mode
 * @author Mikko Honkala
 */
public class SwingSelectOneCompact extends SwingSelectOne 
	implements XSelectOne, ListSelectionListener
{
  /** the listbox */
  protected JList list; 
  /** the list model, when using the JList */
  private DefaultListModel listmodel;

  final static Dimension minSize = new Dimension(50,10);
  
  	/** the Scrollpane */
	public JScrollPane scrollPane;

	
  /**
   * @param v The vector containing all items, the first item is default
   * @param s The style of the selectOne
   */
  public SwingSelectOneCompact() 
  {
	super();
	this.init();
  }
  

	public Component createComponent()
	{
		listmodel = new DefaultListModel();
		list=new JList(listmodel);
		scrollPane = new JScrollPane(list,
								   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
								   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// TODO: move to add / removeselection 
		//list.setVisibleRowCount(java.lang.Math.min(8,its.size()));
        scrollPane.setOpaque(false);
		list.setVisibleRowCount(8);
		list.setSelectionMode(this.getListSelectionModel());

		list.addListSelectionListener (this);
		selectComponent=list;
		return scrollPane;
	}
	protected int getListSelectionModel()
	{
		return ListSelectionModel.SINGLE_SELECTION;
	}


 	/** 
	 * the default background color for this type of component 
	 * null = default.
	 */
	public Color getDefaultBackgroundColor()
	{
		return Color.white;
	} 
	
	/** return the minimum size for this component at zoom level 1.0 */
	public Dimension getMinimumSize()
	{
		return minSize;
	}


  public void addSelection(Object o)
  {
		  listmodel.addElement(o);
		  //if (!this.layoutDone)
              ((JList)selectComponent).setVisibleRowCount(java.lang.Math.min(8,listmodel.getSize()));
  }

	public void removeSelection(Object o)
	{
		  listmodel.removeElement(o);
	}

	public void removeAll()
	{
		  listmodel.removeAllElements();
	}

	public void setSelected(Object o) 
	{
		  list.setSelectedValue(o,true);
	}
	public void setSelectedIndex(int index) 
	{
		  list.setSelectedIndex(index);
	}
	public int getSelectedIndex() {
		  return list.getSelectedIndex();
	}	
	/** the selection events from a list arrive here */
	public void valueChanged(ListSelectionEvent e) {
	    if (e.getValueIsAdjusting())
	        return;

		Object item = ((JList)selectComponent).getSelectedValue();
		this.notifyChange(item, ItemEvent.ITEM_STATE_CHANGED);
	}	
	public Component getContentComponent()
	{
		return this.scrollPane;
	}
	
	public Component getAddableComponent()
	{
		return this.scrollPane;
	}
	public Component getSizableComponent()
	{
		return this.scrollPane;
	}
	public Component getStylableComponent()
	{
		return this.list;
		//return this.content;
	}	
}
