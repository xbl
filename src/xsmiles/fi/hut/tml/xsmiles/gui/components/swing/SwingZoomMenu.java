/* 
 * X-Smiles
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.Language;

/**
 * A swing based dynamic menu for switching GUIs
 *
 * @author Juha Vierinen
 * @version 0.2
 */
public class SwingZoomMenu extends JMenu {

    BrowserWindow browser;
    ChangeZoomListener changeGuiListener;
    
    public SwingZoomMenu(BrowserWindow b) {
        super("Zoom % (reload)");
        browser=b;
	
        changeGuiListener=new ChangeZoomListener();

		for (int i = 25; i <= 300; i = i+25)
		{
			JMenuItem it = new JMenuItem(""+i) ;
			it.addActionListener(changeGuiListener);
			add(it);
		}
    }
    

	private class ChangeZoomListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			String com = e.getActionCommand();
				browser.setZoom(Double.parseDouble(com)/100.0);
		}
	}
    

}
