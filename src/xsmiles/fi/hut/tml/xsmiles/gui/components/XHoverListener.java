package fi.hut.tml.xsmiles.gui.components;

/**
 * When some component is excited (mouseover) the a hover event is sent
 */
public interface XHoverListener {
  public void focusGained(String s);
  public void focusLost();
}
