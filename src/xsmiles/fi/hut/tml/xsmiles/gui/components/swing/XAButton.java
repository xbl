/* X-Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.awt.XAComponent;

/**
 * Similar functionality as normal AWT button.
 * Features:
 * <ul>
 * <li>lightweight</li>
 * <li>three different states (focus, non-focus, pressed), each with graphical representations.</li>
 * <li>not all button functionality is included.</li>
 * </ul>
 */
public class XAButton extends XAComponent
{
  public  final static int NORMAL  = 0;
  public  final static int FOCUSED = 1;      
  public  final static int PRESSED = 2;
  public  final static int DISABLED= 3;
    
  private Vector    actionListeners;
  private String    actionCommand;
  private String    label;
  private String    paramString;

  private int       state;
  private boolean   isActive;
  private boolean   d3=true;

  private XAImageIcon image;
  private XAImageIcon imagePressed;
  private XAImageIcon imageRollOver;
  private XAImageIcon imageDisabled;

  private MouseHandler      mouseHandler;
  private XABorderDecorator borderDecorator;
  private boolean           enabled=false;
  private boolean           gray=true;

  public XAButton(String iName) {
    super();
    init();
    setImage        (iName);
    setImageDisabled(iName);
    setImageRollOver(iName);    
    setimagePressed (iName);    
  }

  public XAButton(String i1Name, String i2Name) {
    super();
    init();
    setImage        (i1Name);
    setImageDisabled(i1Name);    
    setImageRollOver(i1Name);    
    setimagePressed (i2Name);    
  }

  /**
   * @param i1Name normal image
   * @param i2Name RollOver
   * @param i3Name Pressed
   * @param val    Display greyfiltered image for disabled
   */
  public XAButton(String i1Name, String i2Name, String i3Name, boolean val) {
    super();
    init();
    setImage        (i1Name);    
    setImageDisabled(i1Name);
    setImageRollOver(i2Name);
    setimagePressed (i3Name);
    setGray(val);
  }

  public XAButton(String i1Name, String i2Name, String i3Name) {
    super();
    init();
    setImage        (i1Name);    
    setImageDisabled(i1Name);
    setImageRollOver(i2Name);
    setimagePressed (i3Name);
  }

  public XAButton() {
    super();
    init();
  }

  private void init() {
    state           = NORMAL;
    actionListeners = new Vector();
    borderDecorator = new XABorderDecorator(this);
    mouseHandler    = new MouseHandler(this);
    addMouseListener(mouseHandler);
    setEnabled(true);
    repaint();
  }

  public void addActionListener(ActionListener l){ 
    actionListeners.addElement(l);
  }

  /**
   * Does button 3d pop-up.
   */
  public void set3D(boolean val) {
    d3=val;
  }
    
  public String getActionCommand(){ 
    return actionCommand;
  }

  public String getLabel(){ 
    return label;
  }

  protected String paramString(){
    return paramString;
  }

  protected void processActionEvent(ActionEvent e){ 
    Enumeration  i = actionListeners.elements();
    while(i.hasMoreElements()) {
      ((ActionListener) i.nextElement()).actionPerformed(e);
    }
  }

  public void removeActionListener(ActionListener l){
    actionListeners.removeElement(l);
  }

  public void setActionCommand(String command){
    actionCommand = command;
  }

  public void setLabel(String l){
    label = l;
  }   
    
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    this.enabled=enabled;
    repaint();
  }
    
  public boolean isEnabled() {
    return enabled;
  }

  public void paint(Graphics g) {
    super.paint(g);
    if(enabled)
      borderDecorator.decorate(g);
	
    if (label != null) {
      //    g.setColor(Color.black);
      //    g.drawString(label, 10, getHeight() / 2);
    }
    if(enabled) 
      switch(state) {
      case NORMAL:
	image.paint(g);
	break;
      case FOCUSED:
	imageRollOver.paint(g);
	break;
      case PRESSED:
	imagePressed.paint(g);
	break;
      }
    else 
      if(imageDisabled==null||!gray)
	image.paint(g);
      else
	imageDisabled.paint(g);
	
  }

  public void setActive(boolean b) {
    isActive = b;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setImage(XAImageIcon  i) {
    image = i;
  }

  public void setImagePressed(XAImageIcon  i) {
    imagePressed = i;
  }
    
  public void setImageRollOver(XAImageIcon  i) {
    imageRollOver = i;
  }
    
  public void setImage(String fn) {
    image = new XAImageIcon(fn);
  }
    
  public void setimagePressed(String fn) {
    imagePressed = new XAImageIcon(fn);
  }

  public void setImageRollOver(String fn) {
    imageRollOver = new XAImageIcon(fn);
  }
    
  public void setImageDisabled(String fn) {
    imageDisabled=new XAImageIcon(fn, true);
  }

  public void setGray(boolean val) {
    gray=val;
  }

  public int getState() {
    return state;
  }

  public void setState(int s) {
    state = s;
  }

  public Dimension getPreferredSize() {
    //	Log.debug("IMAGE SIZE: ");
    return new Dimension(image.getImage().getWidth(this), image.getImage().getHeight(this));
  }

  public void setSize(Dimension d) {
    setSize(d.width, d.height);
  }

  public void setSize(int w, int h) {
    super.setSize(w, h);
    image.setSize(w, h);
    imagePressed.setSize(w, h);
    imageRollOver.setSize(w, h);
  }

  public void setBounds(int x, int y, int w, int h) {
    super.setBounds(x, y, w, h);
    image.setSize(w, h);
    imagePressed.setSize(w, h);
    imageRollOver.setSize(w, h);
  }

  public class MouseHandler extends MouseAdapter {
	
    private boolean inside=false;         // is the cursor inside the component
    private boolean startedInside =false; // whether the click started from the inside

    private XAButton button;

    public MouseHandler(XAButton b) {
      super();
      button = b;
    }

    public void mouseClicked(MouseEvent e){
    }

    public void mouseEntered(MouseEvent e){
      inside=true;
      setState(FOCUSED);
      repaint();
    }

    public void mouseExited(MouseEvent e){
      inside=false;
      setState(NORMAL);
      repaint();
    }

    public void mousePressed(MouseEvent e){
      setState(PRESSED);
      startedInside=true;
      repaint();
    }

    public void mouseReleased(MouseEvent e){
      //Log.debug(e);
      if (inside) 
	{
	  setState(FOCUSED);
	  if (startedInside=true) 
	    {
	      if(isEnabled())
		processActionEvent(new ActionEvent(button, e.getID(), label)); 
	    }
	}
      else
	{
	  setState(NORMAL);
	}
      startedInside=false;
      repaint();
    }
  }
}
