/* X-Smiles:
 * SUCH DAMAGE.
 * ====================================================================
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;

/**
 * A somewhat lightweight-ish swing combo.
 * 
 * @author juha 
 */
public class XAAddressCombo extends JComboBox 
{
    private Vector history;
    private int items=0;
  
    public XAAddressCombo() 
    {
        super();
        history = new Vector(10);
        this.setEditable(true);
        this.setLightWeightPopupEnabled(false); 
    }
  
    /**
     * sets the combo text and adds an item if this one doesn't exist
     *
     * @param s an item to display in the combobox.
     */
    public void setText(String s) 
    {
        if (history.contains(s)) {
            this.setSelectedItem(s);
        } else {
            this.insertItemAt(s,items);
            this.setSelectedIndex(items);
            items++;
            history.addElement(s);
        }
    }
  
    /**
     * @return get selected text
     */
    public String getText() 
    {
        return (String) getSelectedItem();
    }
    /*
    public void paint(Graphics g)
    {
        CompatibilityFactory.getCompatibility().setAntialias(true,g);
        super.paint(g);
    }*/

}
