/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

/**
 *  This compound lays out a XCaption and XComponent
 *  The caption and component are given in the constructor
 */
public interface XLabelCompound extends XCompound 
{

   
}

