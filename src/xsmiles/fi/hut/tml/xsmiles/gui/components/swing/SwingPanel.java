/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Component;

import javax.swing.JPanel;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XPanel;


/**
 * Class SwingPanel
 * 
 * @author tjjalava
 * @since Oct 22, 2004
 * @version $Revision: 1.2 $, $Date: 2004/11/12 11:38:03 $
 */
public class SwingPanel extends JPanel implements XPanel {
        
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#add(fi.hut.tml.xsmiles.gui.components.XComponent)
     */
    public void add(XComponent c) {
        add(c.getComponent());
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#remove(fi.hut.tml.xsmiles.gui.components.XComponent)
     */
    public void remove(XComponent c) {
        add(c.getComponent());
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#removeComp(java.awt.Component)
     */
    public void removeComp(Component component)
    {
        // TODO Auto-generated method stub
        this.remove(component);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#addComp(java.awt.Component)
     */
    public void addComp(Component component)
    {
        // TODO Auto-generated method stub
        this.add(component);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#getComponent()
     */
    public Component getComponent() {       
        return this;
    }
}
