/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.components;

import javax.swing.*;
import java.awt.*;
import fi.hut.tml.xsmiles.gui.components.awt.ImageCanvas;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.border.EmptyBorder;

/**
 * A dialog that displays the version number and licensing agreement.
 * Should the license be fetched from a file or should it be hardcoded?
 *
 *
 */
public class AboutDialog extends JDialog {
    private String agreement;
    private JTextArea ta;
    private BrowserWindow browser;
    private JPanel kaikki;
    private JDialog aboutDialog;

    /** 
     * Contructs a new AboutDialog with hardcoded images and stuph
     * 
     * @param s the licensing agreement
     */
    public AboutDialog(String s, BrowserWindow b) {
	this.agreement=s;
	browser=b;
	about();
	addWindowListener(new DialogListener());
    }
    
    public void setText(String s) {
	ta.setText(s);
    }
    
    /**
     * Show the dialog 
     */
    public void show() {
	aboutDialog.show();
    }
    
    /**
     * Hide dialog
     */
    public void hide() {
	aboutDialog.hide();
    }
    
    private void about() {

	aboutDialog=new JDialog();
	aboutDialog.setTitle("About X-Smiles " + browser.getVersion());
	aboutDialog.setBackground(Color.white);
	AboutButton butt = new AboutButton("img/logo.gif");

	Container cont=aboutDialog.getContentPane();
	cont.setLayout(new BorderLayout());
	cont.add(butt, BorderLayout.NORTH);
	cont.setBackground(Color.white);

	ta=new JTextArea();
	ta.setEditable(false);
	ta.setBorder(new EmptyBorder(5,5,5,5));
	ta.setLineWrap(true);
	ta.setText(agreement);

	JScrollPane pane=new JScrollPane(ta);
	pane.setBackground(Color.white);
	pane.setPreferredSize(new Dimension(400,155));
	cont.add(pane, BorderLayout.SOUTH);
	aboutDialog.pack();
	aboutDialog.setResizable(false);

    }

    private class DialogListener extends WindowAdapter {
	public void WindowClosing(WindowEvent e) {
	    hide();
	}
	
	public void WindowClosed(WindowEvent e) {
	    Log.debug("FUCK!!! THE DIALOG ACTUALLY GOT CLOSED");
	}

    }

    private class AboutButton extends ImageCanvas {
	public AboutButton(String file) {
	    super(file);
	}
	
	public void paint(Graphics g) {
	    super.paint(g);
	    g.setColor(Color.darkGray);
	    g.setFont(new Font("Arial", Font.PLAIN, 10));
	    g.drawString("VERSION " + browser.getVersion(), 270, 275);
	}
    }


}
