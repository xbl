package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.Log;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;

/** 
 * A Menu for switching Stylesheets
 *
 * @author Juha
 * @version 0
 */
public class StylesheetMenu extends Menu {

    ListenerWithStyle styleListener;
    BrowserWindow browser;
    MenuItem dissed;
    
    /**
     * @param The gui, so that menu can mess around with gui's
     * BrowserConstraints
     */
    public StylesheetMenu(BrowserWindow b) {
	super(Language.STYLESHEET);
	browser=b;
	styleListener=new ListenerWithStyle();
    }
    
    /**
     * Set the stylesheet titles to put in stylesheet menu
     */
    public void setStylesheets() {
	int i;
	removeAll();

    // Check that the document exists before proceeding
    fi.hut.tml.xsmiles.XMLDocument doc = browser.getXMLDocument();
    if (doc == null)
    {
        return;
    }

	Enumeration e=doc.getStylesheetTitles().elements();
	String title=browser.getXMLDocument().getCurrentStylesheetTitle();
	Log.debug("titles.count="+browser.getXMLDocument().getStylesheetTitles().size());
	MenuItem it=null;
	boolean preferredStylesheetFound=false;
	for(i=0;e.hasMoreElements();i++) {
	  String s=(String)e.nextElement();
	  if (s!=null)
	    {
	      it=new MenuItem(s);
	      if(s.equals(title)) {
		it.setEnabled(false);
		dissed=it;
		preferredStylesheetFound=true;
	      }
	      it.addActionListener(styleListener);
	      add(it);
	    }
	}
	if(i<2)
	  setEnabled(false);
	else {
	  setEnabled(true);
	  if(!preferredStylesheetFound) {
	    dissed=it;
	    dissed.setEnabled(false);
	  }
	  
	}
    }
    
    private class ListenerWithStyle implements ActionListener {
	/**
	 * @param ActionEvent encapsulating the title of stylesheet
	 * change to disabled one to enabled
	 * reload page to make changes effect
	 */
      public void actionPerformed(ActionEvent e) {
	if(dissed!=null) {
	  dissed.setEnabled(true);
	}
	dissed=(MenuItem)e.getSource();
	dissed.setEnabled(false);
	
	browser.setPreferredStylesheetTitle(e.getActionCommand());
	//browser.getBrowserConfigurer().setGUIProperty(browser,"preferredStylesheetTitle",e.getActionCommand());
	// Tell browser window to reload
	browser.navigate(NavigationState.RELOAD);	  
      }
    }
    
    
    
}
