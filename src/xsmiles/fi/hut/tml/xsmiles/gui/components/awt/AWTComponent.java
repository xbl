/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;

import java.awt.*;
import java.awt.event.FocusListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.*;

import org.w3c.dom.css.CSSStyleDeclaration;
/**
 *  Interface to plain GUI component
 *  The GUI component package contains an abstract set of components 
 *  that can generally be used.
 *
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public abstract class AWTComponent extends ComponentWithCaption 
    implements XComponent
{
    //protected String hintText;
    protected String fInputMode;

    public AWTComponent()
    {
	super();
    }
  
    /** 
   * in init the subclass always creates its component first and then calls
   * super.init
   */
    public void init()
    {
	// the subclass has already created the content
	if (this.container==null) this.container = new Panel();
	super.init();
    }
    
    protected void addCaption(String text)
    {
	caption = new AWTCaption(text);
    }

    protected void sizeComponent(double zoom)
    {
	Dimension ps=null;
	super.sizeComponent(zoom);
	if (this.container!=null)
	    {
		ps = this.container.getSize();
		this.container.setSize(ps);
	    }

    }
  
    /**
   * Sets the components Tooltip (Hint) text
   */
    public void setHintText(String hint)
    {
    }

    protected void createHint()
    {
    }
        /** set the input mode attribute (from XForms) */
    public void setInputMode(String inputmode)
    {
        this.fInputMode = inputmode;
    }
}

