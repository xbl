/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Component;
import java.awt.Panel;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XPanel;


/**
 * Class AWTPanel
 * 
 * @author tjjalava
 * @since Oct 22, 2004
 * @version $Revision: 1.2 $, $Date: 2004/11/12 11:38:03 $
 */
public class AWTPanel extends Panel implements XPanel {
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#add(fi.hut.tml.xsmiles.gui.components.XComponent)
     */
    public void add(XComponent c) {
        add(c.getComponent());
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#remove(fi.hut.tml.xsmiles.gui.components.XComponent)
     */
    public void remove(XComponent c) {
        remove(c.getComponent());
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#getComponent()
     */
    public Component getComponent() {
        return this;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#removeComp(java.awt.Component)
     */
    public void removeComp(Component component)
    {
        // TODO Auto-generated method stub
        this.remove(component);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XPanel#addComp(java.awt.Component)
     */
    public void addComp(Component component)
    {
        // TODO Auto-generated method stub
        this.add(component);
    }
}
