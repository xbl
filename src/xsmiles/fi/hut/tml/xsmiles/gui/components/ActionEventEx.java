package fi.hut.tml.xsmiles.gui.components;

import java.awt.event.*;

  public class ActionEventEx extends ActionEvent
  {
      public MouseEvent mouseEvent;
      public ActionEventEx(Object source,int num,String text,MouseEvent e)
      {
          super(source,num,text);
          mouseEvent = e;
      }
      public MouseEvent getMouseEvent()
      {
          return mouseEvent;
      }
  }
