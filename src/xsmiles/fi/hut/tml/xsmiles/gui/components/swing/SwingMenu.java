/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;

/**
 * @author Mikko Honkala
 *
 */
public class SwingMenu implements XMenu
{
    JMenu m;
    String name;
    public SwingMenu(String menu)
    {
        name=menu;
        init();
    }
    
    public void init()
    {
        m=new JMenu(name);
    }
    
    public void add(XMenu menu)
    {
        m.add((JMenu)menu.getObject());
    }
    public void add(XMenuItem menuitem)
    {
        m.add((JMenuItem)menuitem.getObject());
    }
    
    public Object getObject()
    {
        return m;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#addSeparator()
     */
    public void addSeparator()
    {
        m.addSeparator();
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#removeAll()
     */
    public void removeAll()
    {
        m.removeAll();
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#remove(fi.hut.tml.xsmiles.gui.components.XMenu)
     */
    public void remove(XMenu item)
    {
        m.remove((JMenu)item.getObject());
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XMenu#remove(fi.hut.tml.xsmiles.gui.components.XMenuItem)
     */
    public void remove(XMenuItem item)
    {
        m.remove((JMenuItem)item.getObject());
    }

}
