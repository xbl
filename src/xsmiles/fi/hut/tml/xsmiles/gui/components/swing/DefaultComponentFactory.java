package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.ScrollPane;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.components.CSSFormatter;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCalendar;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XConfirmDialog;
import fi.hut.tml.xsmiles.gui.components.XContainer;
import fi.hut.tml.xsmiles.gui.components.XDocument;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;
import fi.hut.tml.xsmiles.gui.components.XLinkComponent;
import fi.hut.tml.xsmiles.gui.components.XMedia;
import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XRange;
import fi.hut.tml.xsmiles.gui.components.XSecret;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.gui.components.XSelectMany;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;
import fi.hut.tml.xsmiles.gui.components.XTabbedPane;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.XUpload;
import fi.hut.tml.xsmiles.gui.components.XFocusManager;
import fi.hut.tml.xsmiles.gui.components.awt.AWTConfirmDialog;
import fi.hut.tml.xsmiles.gui.components.awt.AuthDialog;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * The default ComponentFactory which produces components, based on the GUI
 * which is active. This enables the MLFC's to use generic components, whenever
 * it is possible. Each GUI's ComponentFactory should override these methods, to
 * produce wanted components. Interfaces for all components should exist in
 * fi.hut.tml.xsmiles.gui.components
 * 
 * Some more component access methods will be added in the future
 * 
 * @author Juha
 * @author Mikko Honkala
 * @version 0
 */
public class DefaultComponentFactory implements ComponentFactory
{
    BrowserWindow b;
    CSSFormatter formatter;

    public DefaultComponentFactory(BrowserWindow b)
    {
        this.b = b;
        //setOpaque(false);
    }

    public DefaultComponentFactory()
    {
        this.b = null;
    }

    /**
     * @return a container for components
     */
    public XContainer getXContainer()
    {
        //return null;
        return new SwingContainer();
    }
    
    public XPanel getXPanel() {
        return new SwingPanel();
    }

    /**
     * create a scroll pane for this components. e.g. in swing, create a
     * JScrollPane and put the component inside
     */
    public Container createScrollPane(Component comp, int policy)
    {
         JScrollPane scrollPane = new JScrollPane(comp); // Create a scrollpane
                                                        // for the text pane.
        if (policy==VERTICAL_SCROLLBAR_ALWAYS) {
            scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        }
        scrollPane.getVerticalScrollBar().setUnitIncrement(20);
        return scrollPane;
    }

    public XLinkComponent getXLinkComponent(String dest)
    {
        XLinkComponent lc = new SwingLink();
        lc.setDestination(dest);
        return lc;
    }

    /**
     * Give a XMLDocument, and get a rendered document If you are in for events,
     * just add an actionListener and receive the XMLEvents from the component
     * 
     * @param doc
     *            The document
     * @return the visual component that contains rendered xmldocument. Null is
     *         returned if rendering is not possible.
     * 
     * XForms notice: (you might want to use this component to render XOutput)
     */
    public XDocument getXDocument(XLink link)
    {
        return null;
        /*
         * if(b!=null) { XDocument xd=new XADocument(b,link); return xd; } else {
         * Log.error("Cannot return a document, because no link to browser");
         * return null; }
         */
    }

    /**
     * @param s
     *            The intial text
     * @return a textarea
     */
    public XTextArea getXTextArea(String s)
    {
        XTextArea ta = new SwingTextArea(s, b!=null?b.getCurrentGUI():null);
        return ta;
    }

    /**
     * @return A secret input control
     */
    public XSecret getXSecret(char c)
    {
        return new SwingInput(c, b.getCurrentGUI());
    }

    /**
     * @return one line input
     */
    public XInput getXInput()
    {
        return new SwingInput(b!=null?b.getCurrentGUI():null);
    }

    /**
     * @param from
     *            from what
     * @param to
     *            to where
     * @return a range control
     */
    public XRange getXRange(int from, int to, int step, int orientation)
    {
        return new SwingRange(from, to, step, orientation);
    }

    /**
     * Give an URL and receive the mediaelement.
     */
    public XMedia getXMedia(URL u)
    {
        Log.debug("No media handler yet");
        return null;
    }

    /**
     * @param v
     *            A vector containing the items to put in selector
     * @return a XSelectOne, which is used to select one item at a time
     */
    public XSelectOne getXSelectOne(String appearance, boolean open)
    {
        if (open)
            return new SwingSelectOneMinimal(true);

        if (appearance.equalsIgnoreCase("listbox") || appearance.equalsIgnoreCase("compact")
                || appearance.equalsIgnoreCase("full"))
            return new SwingSelectOneCompact();
        //if
        // (appearance.equalsIgnoreCase("menu")||appearance.equalsIgnoreCase("minimal"))
        else
            return new SwingSelectOneMinimal(false);

    }

    /**
     * @return a XSelectBoolean, which is used to select true / false
     */
    public XSelectBoolean getXSelectBoolean()
    {
        return new SwingSelectBoolean();
    }

    /**
     * @param v
     *            The vector that contains all possible selections
     * @return A selectMany control.
     */
    public XSelectMany getXSelectMany(String appearance, boolean open)
    {
        return new SwingSelectManyCompact();
    }

    /**
     * @return an upload control.
     */
    public XUpload getXUpload(String caption)
    {
        return new SwingUpload(caption, this);
    }

    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save)
    {
        return new SwingFileDialog(this.b, save);
    }

    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save, String filename)
    {
        return new SwingFileDialog(this.b, save, filename);
    }

    /**
     * @return an authenticator control.
     */
    public XAuthDialog getXAuthDialog()
    {
        Frame parentframe = (Frame) b.getCurrentGUI().getWindow();
        return new AuthDialog(parentframe);
    }

    /**
     * @return an confirmation control.
     */
    public XConfirmDialog getXConfirmDialog()
    {
        Frame parentframe = (Frame) b.getCurrentGUI().getWindow();
        return new AWTConfirmDialog(parentframe);
    }

    /**
     * @return a button w a label
     */
    public XButton getXButton(String label, String iconUrl)
    {
        return new SwingButton(label, iconUrl);
    }

    /**
     * @return a iconned button
     */
    public XButton getXButton(String iconUrl)
    {
        return new SwingButton(iconUrl);
    }

    /**
     * @return a button with images
     */
    public XButton getXButton(String imageUrl, String focusedImage, String disabledImage)
    {
        // TODO
        //return new SwingButton(imageUrl,focusedImage,disabledImage);
        return null;
    }

    /**
     * @return a new calender control
     */
    protected XCalendar getXCalendar()
    {
        return new SwingCalendar();
    }

    /**
     * query for extension availability. For instance, calendar control is an
     * extension
     */
    public boolean hasExtension(Class c)
    {
        if (c.equals(XCalendar.class))
            return true;
        return false;
    }

    /** return an extension control. hasExtension must be called first */
    public Object getExtension(Class c)
    {
        if (c.equals(XCalendar.class))
            return getXCalendar();
        else
        {
            Log.error("There is no extension for : " + c);
            return null;
        }

    }

    /** show an error dialog */
    public void showError(String title, String explanation)
    {
        javax.swing.JOptionPane.showMessageDialog(null, explanation, title,
                javax.swing.JOptionPane.ERROR_MESSAGE, null);
    }

    public void showLinkPopup(URL url, XMLDocument doc, java.awt.event.MouseEvent e,
            MLFCListener listener)
    {
        SwingLinkPopup popup = new SwingLinkPopup();
        popup.show(e.getComponent(), e.getX(), e.getY(), doc, url, listener);
    }

    /**
     * @return a caption (label) component
     */
    public XCaption getXCaption(String captText)
    {
        return new SwingCaption(captText);
    }

    /**
     * @ return a compound designed for holding a label and a component
     */
    public XLabelCompound getXLabelCompound(XComponent comp, XCaption capt, String captSide)
    {
        return new SwingLabelCompound(comp, capt, captSide);
    }
    protected static KeyStroke HELPKEY = KeyStroke.getKeyStroke("F1");

    /** listen for all help keypresses etc, for this component and its ancestors */
    public void addHelpListener(Component component, ActionListener listener)
    {
        if (component instanceof JComponent)
        {
            JComponent comp = (JComponent) component;
            comp.registerKeyboardAction(listener, "help", HELPKEY, JComponent.WHEN_IN_FOCUSED_WINDOW);
        }
    }

    /** remove a help listener */
    public void removeHelpListener(Component component, ActionListener listener)
    {
        if (component instanceof JComponent)
        {
            JComponent comp = (JComponent) component;
            comp.unregisterKeyboardAction(HELPKEY);
        }
    }

    /** create a content panel for the browser, add also maybe a layout manager */
    public Container createContentPanel()
    {
        //JPanel p = new JPanel();
        Container p = new Container();
        p.setLayout(new BorderLayout());
        return p;
    }

    public void showMessageDialog(boolean isModal, String title, String message, long timeToLiveMillis)
    {
        int messageType = JOptionPane.INFORMATION_MESSAGE;
        int optionType = JOptionPane.DEFAULT_OPTION;
        JOptionPane pane = new JOptionPane(message, messageType, optionType, null, null, null);

        //pane.setInitialValue(initialValue);

        final JDialog dialog = pane.createDialog(null, title);
        dialog.setModal(isModal);
        pane.selectInitialValue();
        dialog.show();
        if (timeToLiveMillis > 0)
        {
            final long time = timeToLiveMillis;
            Thread t = new Thread(title)
            {
                public void run()
                {
                    try
                    {
                        Thread.sleep(time);
                        dialog.dispose();

                    }
                    catch (Exception e)
                    {
                    }
                }
            };
            t.start();

        }
    }

    public XMenuBar getXMenuBar()
    {
        return new SwingMenuBar();
    }

    public XMenu getXMenu(String name)
    {
        return new SwingMenu(name);
    }

    public XMenuItem getXMenuItem(String name)
    {
        return new SwingMenuItem(name);
    }

    public void setScrollBar(Container con, int x, int y)
    {
        if (con instanceof ScrollPane)
            ((ScrollPane) con).setScrollPosition(x, y);
        else if (con instanceof JScrollPane)
        {
            final JScrollPane fcon = (JScrollPane)con;
            final int fx = x;
            final int fy = y;
            try
            {
                Runnable set = new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            ((JScrollPane) fcon).getVerticalScrollBar().setValue(fy);
                            ((JScrollPane) fcon).getHorizontalScrollBar().setValue(fx);
                        }
                        catch (Exception e)
                        {
                            Log.error(e);
                        }
                    }
                };
                javax.swing.SwingUtilities.invokeLater(set);

            }
            catch (Exception e)
            {
                Log.error("Setting scrollbar position: " + e);
            }
        }
        else
            Log.error("DefaultComponentFactory: scroll bar not set to: " + con);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getXTabbedPane()
     */
    public XTabbedPane getXTabbedPane()
    {
        // TODO Auto-generated method stub
        return new SwingTabbedPane();
    }

    public XFocusManager getXFocusManager(){
	// TODO
	return new SwingFocusManager();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getCSSFormatter()
     */
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.ComponentFactory#getCSSFormatter()
     */
    public CSSFormatter getCSSFormatter()
    {
        // TODO Auto-generated method stub
        if (formatter==null)
            formatter=SwingCSSFormatter.getInstance();
        return formatter;
    }

}
