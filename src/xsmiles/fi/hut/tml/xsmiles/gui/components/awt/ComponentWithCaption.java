/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;

import java.awt.*;
import java.awt.event.FocusListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.*;
import org.w3c.dom.css.CSSStyleDeclaration;

/**
 * The base base class for all AWT & Swing components that have a caption
 *
 * @author Mikko Honkala
 */
public abstract class ComponentWithCaption 
{
	
  final static Dimension minSize = new Dimension(1,1);

  /**
   * Specifies that components should be laid out left to right.
   */
  public static final int X_AXIS = 0;

  /**
   * Specifies that components should be laid out top to bottom.
   */
  public static final int Y_AXIS = 1;

  /** the content component (e.g. the slider of range). The subclass will generate this */
  protected AWTStylableComponent content;

  /** the top-level container, containing the caption and the content component */
  protected Container container;

  /** the caption component */
  protected AWTCaption caption;
	
	
  /** the axis depends on the caption side CSS attribute */
  protected int axis;

  protected boolean actionable=false;

  protected boolean layoutDone = false;
	
  protected double currentZoom = 1.0;

  public ComponentWithCaption()
  {
    super();
  }
  /** 
   * in init the subclass always creates its component first and then calls
   * super.init (SwingComponent or AWTComponent)
   */
  public void init()
  {
    // the subclass has already created the content
    if (this.container==null) this.container = new Panel();
  }

  /**
   * whether this is layed out north-south or east-west
   */
  int getAxis()
  {
    return this.axis;
  }

  
  /**
   * layouts the component using the caption, content and the main container parts 
   * of the control
   */
  protected void layoutComponent()
  {
    Log.debug("AWTComponentWithCaption trying to layout");

    // JV 15.8 ei toimi viel� kauhean hyvin. Joutuu tod. n�k laskee k�sin.
    // tilannetta ei helpota yht��n se, ett� jotkut AWT heavyweightit 
    // antaa 0x0 kokea kunnes ne on pistetty ruudulle. v�h�n vaikea laskea siin�
    // kokoja etuk�teen..
	
    /**
     * Using borderlayout
     */ 
	
    if (this.layoutDone) {
      this.container.removeAll();
    }
	
    //float alignment= Component.LEFT_ALIGNMENT;
    //boolean captionFirst=true;
    //axis = BoxLayout.Y_AXIS; 
	
    String captionPlacement=BorderLayout.NORTH;
    if (caption!=null)
      {
	String captionSide=null;
	// Determine which side the caption should be put, default is top
	if (this.content.getStyle()!=null) captionSide=AWTCSSFormatter.getCaptionSide(caption.getStyle());
	    
	if (captionSide!=null)
	  {
	    Log.debug("caption-side: "+captionSide);
	    String cs=captionSide;
	    if (cs.equals("left"))
	      {
		captionPlacement=BorderLayout.WEST;
	      }
	    else if (cs.equals("right"))
	      {
		captionPlacement=BorderLayout.EAST;
	      }
	    else if (cs.equals("bottom"))
	      {
		captionPlacement=BorderLayout.SOUTH;
	      }
	    else if (cs.equals("top"))
	      {
		captionPlacement=BorderLayout.NORTH;
	      }
	  }
      }
	
    //BoxLayout box = new BoxLayout(container, axis);
    container.setLayout(new BorderLayout());
    //content.getContentComponent().setAlignmentX(alignment);
    //caption.getContentComponent().setAlignmentX(alignment);
    //container.setAlignmentX(alignment);
		

	
    // caption-side was left or top
    if (caption!=null)
      //container.add(caption.getAddableComponent(), captionPlacement);
    container.add(content.getAddableComponent(), BorderLayout.CENTER);


    // TODO : format the content if CSS is found
    //this.formatContent();
    //this.defaultColor=content.getContentComponent().getBackground();
    //this.defaultFont=content.getContentComponent().getFont();

    //container.doLayout();

    container.doLayout();
    container.invalidate();
    container.validate();

    this.sizeComponent(this.currentZoom);

    container.doLayout();
    container.invalidate();
    container.validate();

    this.layoutDone=true;
  }
  
  public void setCaptionStyle(CSSStyleDeclaration style)
  {
    if (caption!=null) //this.addCaption("NO CAPTION");
    this.caption.setStyle(style);
		
  }
  public void setStyle(CSSStyleDeclaration style)
  {
    this.content.setStyle(style);
  }
  public CSSStyleDeclaration getStyle()
  {
    return this.content.getStyle();
  }

  /**
   * By default components are inactive
   * @return Is can compnent be active?
   */
  public boolean getActionable()
  {
    return actionable;
  }

  /**
   * @param a The value of actionable
   */
  public void setActionable(boolean a)
  {
    actionable=a;
  }
  public Component getComponent()
  {
    if (!layoutDone) this.layoutComponent();
    return container;
  }
  /*
    public JComponent getContentComponent()
    {
    return content.getContentComponent();
    }*/


  protected abstract void addCaption(String text);

  /** set the text shown in the caption */
  public void setCaptionText(String text)
  {
    if (caption!=null) {
      caption.setText(text);
    }
    else if (text!=null)
      {
	this.addCaption(text);
      }
  }

	
  /**
   * Set's this components zoom (1.0 is the default).
   */
  public void setZoom(double zoom)
  {
    this.content.setZoom(zoom);
    if (caption!=null) this.caption.setZoom(zoom);
    this.sizeComponent(zoom);
  }
	
  //<<<<<<< ComponentWithCaption.java
  
  protected void sizeComponent(double zoom)
  {
    // TODO: do this with absolute positioning and bulletinlayout?
    // Calculate how much space is left for the content component
    // because the caption takes some of the space
    Dimension captionsize=null;
    if (this.caption==null) captionsize=new Dimension(0,0);
    else {
      captionsize = this.caption.getDefaultSize();
    }
    
    Dimension contentsize=this.content.getDefaultSize();
    
    if (contentsize==null) return;
    //Log.debug("AWT Contentsize: " +contentsize.toString());
    
    Dimension zoomedtextsize=new Dimension((int)(zoom*contentsize.width),
					   (int)(zoom*contentsize.height));
    Dimension zoomedcaptionsize=new Dimension((int)(zoom*captionsize.width),
					      (int)(zoom*captionsize.height));
    Dimension d;
    if (this.getAxis()==this.Y_AXIS)
      {
	d = new Dimension(
			  (int)(contentsize.width*zoom),
			  (int)((contentsize.height+captionsize.height)*zoom));
      }
    else
      {
	d = new Dimension(
			  (int)((contentsize.width+captionsize.width)*zoom),
			  (int)(contentsize.height*zoom));
      }
    if (caption!=null)this.caption.sizeComponent(zoom,zoomedcaptionsize);		
    this.content.sizeComponent(zoom,zoomedtextsize);
    
    this.content.getSizableComponent().invalidate();
    if (caption!=null) this.caption.getSizableComponent().invalidate();
    if (this.container!=null)
      {
	this.container.validate();
	
	Dimension ps = this.container.getPreferredSize();
	//((JComponent)this.container).setMinimumSize(ps);
	//((JComponent)this.container).setMaximumSize(ps);
	this.container.setSize(ps);
      }
  }

  /* THIS OLD APPROACH MAY WORK BETTER WHILE ZOOMING, SO HAVE NOT
   * DESTROYED IT YET
   *
   * this is because it measures the whole container as
   * a start point, so insets are taken into account
   */
  /*
    
  */	
  /**
   * Sets the components font
   */
  public void setFont(Font f)
  {
    this.content.setFont(f);
    if (this.caption!=null) this.caption.setFont(f);
  }
  /**
   * Sets the components Tooltip (Hint) text
   */
  public abstract void setHintText(String hint);

  /**
   * Set this drawing area visible.
   * @param v    true=visible, false=invisible
   */
  public void setVisible(boolean v)
  {
    this.container.setVisible(v);
  }

  /**
   * @param b Active or not active
   */
  public void setEnabled(boolean b)
  {
    this.content.getStylableComponent().setEnabled(b);
  }

  /** 
   * the default background color for this type of component 
   * null = transparent.
   */
  public Color getDefaultBackgroundColor()
  {
    return null;
  }

  /** return the minimum size for this component at zoom level 1.0 */
  public Dimension getMinimumSize()
  {
    return minSize;
  }
	
  /** set the background, if null, then reset to default */
  public void setBackground(Color bg)
  {
    this.content.setBackground(bg);
  }
  /** set the foreground */
  public void setForeground(Color bg)
  {
    this.content.setForeground(bg);
  }

  /**
   * Set the bounds of Container
   *
   * @param x X location
   * @param y X location
   * @param width X location
   * @param height X location
   */
  public void setBounds(int x, int y, int width, int height)
  {
    this.container.setBounds(x,y,width,height);
  }

  /**
   * @return The original coordinated, and the default size of component
   */

  public int getX()
  {
    return this.container.getLocation().x;
  }

  public int getY()
  {
    return this.container.getLocation().y;
  }

  public int getWidth()
  {
    return (int)this.getSize().width;
  }

  public int getHeight()
  {
    return (int)this.getSize().height;
  }

  public Dimension getSize()
  {
    if (container.getSize().height>0)
      return container.getSize();
    if (container.getPreferredSize().height>0)
      return container.getPreferredSize();
    return new Dimension(0,0);
  }

  /* The listener stuff */
  public void addActionListener(ActionListener al)
  {
    Log.error("Subclass should override actionlistener methods");
  }

  public void removeActionListener(ActionListener al)
  {
    Log.error("Subclass should override actionlistener methods");
  }

  public void addFocusListener(FocusListener fl)
  {
    this.content.getStylableComponent().addFocusListener(fl);
  }

  public void removeFocusListener(FocusListener fl)
  {
    this.content.getStylableComponent().removeFocusListener(fl);
  }
  public void addMouseListener(MouseListener ml)
  {
    this.content.getStylableComponent().addMouseListener(ml);
  }
  public void removeMouseListener(MouseListener ml)
  {
    this.content.getStylableComponent().removeMouseListener(ml);
  }
  
  /** set the focus to this control */
  public void setFocus()
  {
      this.content.setFocus();
  }
  
      /** add a listener for help events */
    public void addHelpListener(ActionListener fl)
    {
        Log.debug("addHelpListener not implemented!");
    }
    
    /** remove a listener for help events */
    public void removeHelpListener(ActionListener fl)
    {
          Log.debug("removeHelpListener not implemented!");
    }
}

