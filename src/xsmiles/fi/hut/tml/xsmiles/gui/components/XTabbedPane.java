/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Oct 19, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.components;

import java.awt.Component;


/**
 * @author honkkis
 *
 */
public interface XTabbedPane
{
    public int getTabCount();
    public int getMaxTabCount();
    public int getSelectedIndex();
    public Component getSelectedComponent();
    
    public void addTab(String title, Component component);
    
    public void remove(Component c);
    public void remove(int i);
    
    public Component getComponent();
    
    public void setChangeListener(XChangeListener list);
    /**
     * @param component
     * @return
     */
    public int indexOfComponent(Component component);
    /**
     * @param object
     * @param tit
     */
    public void setTitleAt(int index, String tit);
    
    
    

}
