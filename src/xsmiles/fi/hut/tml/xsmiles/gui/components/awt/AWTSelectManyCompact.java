/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    


/**
 * Select Many compact mode
 * @author Mikko Honkala
 * @author juha awt mod
 */
public class AWTSelectManyCompact extends AWTSelectOneCompact 
    implements XSelectMany
{
    public AWTSelectManyCompact() 
    {
	multiple=true;
	this.init();
    }
    
    /*    
	  protected int getListSelectionModel()
	  {
	  return ListSelectionModel.MULTIPLE_INTERVAL_SELECTION ;
	  }
    */

    /**
     * @param o The object to set
     * @param b The value to set it as
     */
    public void setSelected(Object o, boolean b) 
    {
	//list.setSelectedValue(o,true);
	String [] itemz=list.getItems();
	
	for(int i=0;i<list.getItemCount();) {
	    if(o.equals(list.getItem(i))) {
		if(b)
		    list.select(i);
		else
		    list.deselect(i);
	    }
	}
	//list.setSelectedValue(o,true);
    }
    
    public void clearSelection() 
    {
	for(int i=0;i<list.getItemCount();i++) {
	    list.deselect(i);
	    //list.clearSelection();
	}
    }
    
    public int[] getSelectedIndices() 
    {
	return list.getSelectedIndexes();
    }
    
    public void setSelectedIndices(int[] indices) 
    {
	for(int i=0;i<indices.length;i++) {
	    list.select(indices[i]);
	}
    }
}
