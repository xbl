/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import java.awt.Component;

/**
 * Class XPanel
 * 
 * @author tjjalava
 * @since Oct 22, 2004
 * @version $Revision: 1.2 $, $Date: 2004/11/12 11:38:03 $
 */
public interface XPanel {

    /**
     * @param c The component to be added
     */
    public void add(XComponent c);

    /**
     * @param c Remove component c
     */
    public void remove(XComponent c);

    /**
     * Remove all components from conatiner.
     */
    public void removeAll();

    /**
     * Validates the contents of the panel
     */
    public void validate();
    
    /**
     * Get the real implementing component
     * @return
     */
    public Component getComponent();

    /**
     * @param component
     */
    /**
     * @param component
     */
    public void removeComp(Component component);
    public void addComp(Component component);
}