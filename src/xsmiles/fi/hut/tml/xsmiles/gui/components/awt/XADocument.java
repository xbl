/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.components.XDocument;
import java.awt.*;

/**
 *  Interface to a xml media component.
 */
public class XADocument extends XAMedia implements XDocument 
{
  private BrowserWindow b;
  private XLink d;
  
  public  XADocument(BrowserWindow b, XLink l)
  {
    super(l.getURL());
    this.b=b;
    d=l;
    b.displayDocumentInContainer(l,this.container);
  }

  /**
   * @param doc Set the document to be doc
   */
  public void setXMLDocument(XLink l) {
    d=l;
    b.displayDocumentInContainer(l,this.container);
  }
}

