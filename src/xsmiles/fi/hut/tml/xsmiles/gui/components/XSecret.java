/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

/**
 *  The only addition is add and remove
 *  No extra functionality just the basics.
 */
public interface XSecret extends XText {
  
  /**
   * @return Return the password currently typed in.
   */
  public String getPassword();
  
}
