/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import java.util.Hashtable;
import java.io.*;
import java.net.*;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.AWTEventMulticaster;


/**
 * The XForms/Button element
 * @author Mikko Honkala 
 * @author awt mod juha
 */
public class AWTUpload extends AWTButton 
    implements XUpload, ActionListener
{
    protected ComponentFactory factory;
    ActionListener actionlistener = null;
	
    /**
     * Constructs a new XButton (XForms/button).
     *
     */
    public AWTUpload(String captionText, ComponentFactory cf)
    {
	super(captionText,null);
	factory = cf;
    }
	
    public void init()
    {
	super.init();
	//if(button instanceof XAButton)
	//    ((XAButton)this.button).addActionListener(this);
	//else if(button instanceof Button)
	button.addActionListener(this);
    }
  
    public void actionPerformed(java.awt.event.ActionEvent actionEvent)
    {
	if (actionEvent.getSource()==this) return;
	else
	    {
		// open new dialog
		XFileDialog dia = factory.getXFileDialog(false);
		int ret = dia.select();
		if (ret == XFileDialog.SELECTION_OK)
		    {
			File selected = dia.getSelectedFile();
			ActionEvent ev = new ActionEvent(this,-1,selected.toString(),-1);
			actionlistener.actionPerformed(ev);
		    }
	    }
    }
    public void addActionListener(ActionListener tl)
    {
	actionlistener = AWTEventMulticaster.add(actionlistener, tl);
    }
    public void removeActionListener(ActionListener tl)
    {
	actionlistener = AWTEventMulticaster.remove(actionlistener, tl);
    }

  
}
