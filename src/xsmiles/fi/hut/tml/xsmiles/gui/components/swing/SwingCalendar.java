/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.*;
import java.awt.event.*;

import java.text.DateFormat;

import java.util.Vector;
import java.util.Date;
import java.util.Enumeration;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;

//import com.toedter.calendar.*;
import fi.hut.tml.xsmiles.gui.components.swing.calendar.JCalendarEx;

import java.util.*;

import javax.swing.border.BevelBorder;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.*;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

/**
 * A calender control for swing
 * @author Mikko Honkala
 *
 */
public class SwingCalendar extends SwingStylableComponent 
    implements XCalendar, ActionListener,  PropertyChangeListener,
        MouseListener
{
    protected JTextField textcomponent;
    protected JPanel panel;
    protected JButton button;
    protected Popup popup;
    protected JCalendarEx calendarComponent;
    protected Calendar calendar = Calendar.getInstance();
    /** is this componen currently enabled (editable) */
    protected boolean enabled = true;
    
    /** this listens for date changes in the control */
    protected XChangeListener fChangeListener;
/**
   * A iconed swing button
   */
  public SwingCalendar()
  {
      super();
      this.init();
  }
  public void setEnabled(boolean b)
  {
    this.enabled=b;
    button.setEnabled(b);
  }
    /**
   * Set this drawing area visible.
   * @param v    true=visible, false=invisible
   */
  public void setVisible(boolean v)
  {
      super.setVisible(v);
      this.closeCalendar();
  }
  
  public void init()
  {
	  content=this.createContent();
      super.init();
  }
  public Color getDefaultBackgroundColor()
  {
    return Color.white;
  }
  /** creates the content component */
	    /** creates the content component */
  public Component createContent()
  {
	Log.debug("Creating new calendar ");
    textcomponent=new JTextField("initial content");
    textcomponent.setEditable(false);
    //panel= new JPanel(new FlowLayout(FlowLayout.LEFT));
    panel= new JPanel(new BorderLayout());
    button = new BasicArrowButton(BasicArrowButton.SOUTH);

    // to open calender when the user clicks the arrow button
    button.addMouseListener(this);
    button.setFocusable(true);
    
    // TODO: remove this when the textbox is editable
    textcomponent.addMouseListener(this);
    
    panel.add(textcomponent, BorderLayout.CENTER);
    panel.add(button,BorderLayout.EAST);
    panel.setOpaque(false);
    this.dateSelected(calendar,false);
	return panel;
  }
  /**
   * 
   */
  public Calendar getDate()
  {
      return calendar;
  }
  
  /** 
   *?@param s The text is set to s
   */ 
  public void setDate(Calendar d)
  {
      if (d==null) return;
      this.dateSelected(d,false);
  }
  
  public void addActionListener(ActionListener al)
  {
  }

  public void removeActionListener(ActionListener al) 
  {
  }

  public void actionPerformed(java.awt.event.ActionEvent actionEvent) {
      /*
      if (popup!=null&&popup.isVisible())
          closeCalendar();
      else
          openCalendar();
       */
  }
  protected void openCalendar()
  {
      //popup.show();
      if (this.enabled == false) return;
      
      Log.debug("Opening calendar frame");
      if (popup==null)
      {
        Frame parent = this.getParentWindow(button);
        popup = new Popup(parent);
        this.setBorder(
        ((JComponent)popup.getContentPane()));
        popup.getContentPane().add(this.getCalendarComponent());
        popup.pack();
      }
      popup.show(button,1,button.getHeight());
      popup.setVisible(true);  
      
  }
  protected Frame getParentWindow(Component comp)
  {
            Frame parentWindow = null;
            Component p;
            for(p = comp; p != null; p = p.getParent()) {
                if(p instanceof Frame) {
                    parentWindow = (Frame)p;
                    break;
                }
            }  
            return parentWindow;
  }
  protected void setBorder(JComponent component)
  {
          component.setBorder
            (BorderFactory.createCompoundBorder
             (BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.gray, Color.black),
              BorderFactory.createCompoundBorder
             (BorderFactory.createCompoundBorder
              (BorderFactory.createEmptyBorder(3, 3, 3, 3),
               BorderFactory.createLineBorder(Color.black)),
              BorderFactory.createEmptyBorder(3, 3, 3, 3))));
  }

  public void closeCalendar()
  {
      if (popup!=null)
      popup.setVisible(false);
  }
  protected Component getCalendarComponent()
  {
      if (calendarComponent==null)
      {
        calendarComponent = new JCalendarEx();
        calendarComponent.setCalendar(this.calendar,false);      
        calendarComponent.addPropertyChangeListener(this);
      }
      return calendarComponent;
          
  }
  
  
  /** the user has selected a date from the calendar */
  protected void dateSelected (Calendar aCalendar, boolean notifyListener)
  {
        this.closeCalendar();
        calendar = aCalendar;
        textcomponent.setText(this.toDisplayValue(calendar));
        if (notifyListener && this.fChangeListener!=null)
            this.fChangeListener.valueChanged(false,null);
  }
  protected String toDisplayValue(Calendar c)
  {
        Locale locale = null;
        if (calendarComponent==null)
        {
            locale = Locale.getDefault();
        } else 
        {
            locale= calendarComponent.getLocale();
        }

        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG,locale);
        return (df.format(calendar.getTime()));
  }
    public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals("calendar")) 
            {
                Calendar aCalendar = (Calendar) evt.getNewValue();
                this.dateSelected(aCalendar,true);
            }
      
    }  
      
    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
      if (popup!=null&&popup.isVisible())
          closeCalendar();
      else
          openCalendar();
    }
    
    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }
    
    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
    }
    
    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }
    
    /**
     * Invoked when the mouse has been clicked on a component.
     */
    public void mouseClicked(MouseEvent e) {
    }
    
    /**
     * add a change listener
     */
    public void addChangeListener(XChangeListener l) {
        this.fChangeListener = l;
    }
    
    public class Popup extends JWindow
    {
        public Popup(Frame window)
        {
            super(window);
        }
        public void show(Component comp, int x, int y)
        {
            Point point = convertParentLocationToScreen(comp.getParent(),x,y);
/*
            this.setLocation((int)(point.x+comp.getBounds().x+x),
                            (int)(point.y+comp.getBounds().y+y));
 */
            this.setLocation((int)(point.x),
                            (int)(point.y));
            this.setVisible(true);
        }
        Point convertParentLocationToScreen(Container parent,int x,int y) {
            Window parentWindow = null;
            Rectangle r;
            Container p;
            Point pt;
            for(p = parent; p != null; p = p.getParent()) {
                if(p instanceof Window) {
                    parentWindow = (Window)p;
                    break;
                }
            }
            if(parentWindow != null) {
                r = parentWindow.getBounds();
                pt = new Point(x,y);
                pt = SwingUtilities.convertPoint(parent,pt,null);
                pt.x += r.x;
                pt.y += r.y;
                return pt;
            } else
                throw new Error("convertParentLocationToScreen: no window ancestor found");                        
        }


        Point convertScreenLocationToParent(Container parent,int x,int y) {
            Window parentWindow = null;
            Rectangle r;
            for(Container p = parent; p != null; p = p.getParent()) {
                if(p instanceof Window) {
                    parentWindow = (Window)p;
                    break;
                }
            }
            if(parentWindow != null) {
                Point p = new Point(x,y);
                SwingUtilities.convertPointFromScreen(p,parent);
                return p;
            } else
                throw new Error("convertScreenLocationToParent: no window ancestor found");
            }        
        }

      public Component getAddableComponent()
  {
    return this.panel;
  }

  public Component getSizableComponent()
  {
    return this.panel;
  }

  public Component getStylableComponent()
  {
    return this.textcomponent;
  }
}
