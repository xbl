/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import java.util.Calendar;


/**
 * textarea 
 */

public interface XCalendar extends XComponent {
  
  /**
   * 
   */
  public Calendar getDate();
  
  /** 
   *?@param s The text is set to s
   */ 
  public void setDate(Calendar d);
  
  /** closes a popup component of the calender.
   * This is sort of a hack, because I could not use
   * swing JPopup, since month popup closed it
   */
  public void closeCalendar();
  
  /**
   * add a change listener
   */
  public void addChangeListener(XChangeListener l);
}
