/*
 * Created on Mar 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.components;

import java.awt.event.ActionListener;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface XMenuItem
{
    public Object getObject();
    public void addActionListener(ActionListener al);
    public void selectItem(boolean select);
}
