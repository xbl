/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.general;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.CSSFormatter;
/**
 *  Interface to plain GUI component
 *  The GUI component package contains an abstract set of components
 *  that can generally be used.
 *
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public abstract class ComponentBase
{
    
    /** The CSS for the main component */
    protected CSSStyleDeclaration style;
    protected static final Dimension minSize = new Dimension(20,10);
    
    
    /** The current zoom level of this component (default = 1.00) */
    protected double currentZoom=1;
    protected Font currentFont,defaultFont;
    protected Dimension defaultSize;
    /** the default background color */
    protected Color defaultBGColor;
    protected String hintText;
    
    
    
    
    
    /** the content component */
    public Component content;
    
    //protected String hintText;
    protected String fInputMode;
    
    public ComponentBase()
    {
        super();
    }
    
    
    /**
     * Set this drawing area visible.
     * @param v    true=visible, false=invisible
     */
    public void setVisible(boolean v)
    {
        this.content.setVisible(v);
    }
    
    /**
     * Sets the content components font
     */
    public void setFont(Font f)
    {
        this.getStylableComponent().setFont(f);
        // If the component is a container, set the font for all of its children
        if (this.getStylableComponent() instanceof Container) // should also be valid for JComponent
        {
            Container c = (Container)this.getStylableComponent();
            Component[] comps = c.getComponents();
            for (int i=0; i<comps.length; i++)
            {
                Component comp = comps[i];
                comp.setFont(f);
            }
        }
    }
    /** set the input mode attribute (from XForms) */
    public void setInputMode(String inputmode)
    {
        this.fInputMode = inputmode;
    }
    
    public boolean getEnabled()
    {
        return this.content.isEnabled();
    }
    
    /**
     * @param b Active or not active
     */
    public void setEnabled(boolean b)
    {
        this.content.setEnabled(b);
    }
    
    /**
     * @return the CSSStyleDeclaration for the main component
     */
    public CSSStyleDeclaration getStyle()
    {
        return this.style;
    }
    

    
    
    public Component getAddableComponent()
    {
        return this.content;
    }
    
    public Component getSizableComponent()
    {
        return this.content;
    }
    
    public Component getStylableComponent()
    {
        return this.content;
    }
    
    public Component getComponent()
    {
        return this.getAddableComponent();
    }
    
    
    /**
     * the default background color for this type of component
     * null = transparent.
     */
    public Color getDefaultBackgroundColor()
    {
        return null;
    }
    
    /** return the minimum size for this component at zoom level 1.0 */
/*  public Dimension getMinimumSize()
  {
    return minSize;
  }
 */
    /** set the background, if null, then reset to default */
    public void setBackground(Color bg)
    {
        if (bg==null) // reset default background color
            this.content.setBackground(defaultBGColor);
        else
            this.content.setBackground(bg);
    }
    /** set the foreground */
    public void setForeground(Color bg)
    {
        this.content.setForeground(bg);
    }
    
    /**
     * Set the bounds of Container
     *
     * @param x X location
     * @param y X location
     * @param width X location
     * @param height X location
     */
    public void setBounds(int x, int y, int width, int height)
    {
        this.content.setBounds(x,y,width,height);
    }
    /** returns the default size, calculated in the beginning */
    public Dimension getDefaultSize()
    {
        if (this.defaultSize==null) this.setDefaultSize();
        return this.defaultSize;
    }
    
    /**
     * @return The original coordinated, and the default size of component
     */
    
    public int getX()
    {
        return this.content.getLocation().x;
    }
    
    public int getY()
    {
        return this.content.getLocation().y;
    }
    
    public int getWidth()
    {
        return (int)this.getSize().width;
    }
    
    public int getHeight()
    {
        return (int)this.getSize().height;
    }
    
    public Dimension getSize()
    {
        if (content.getSize().height>0)
            return content.getSize();
    /*
    if (content.getPreferredSize().height>0)
      return content.getPreferredSize();*/
        return new Dimension(0,0);
    }
    
    /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
        return minSize;
    }
    
    
    /* The listener stuff */
    public void addActionListener(ActionListener al)
    {
        Log.error("Subclass should override actionlistener methods");
    }
    
    public void removeActionListener(ActionListener al)
    {
        Log.error("Subclass should override actionlistener methods");
    }
    
    public void addFocusListener(FocusListener fl)
    {
        this.content.addFocusListener(fl);
    }
    
    public void removeFocusListener(FocusListener fl)
    {
        this.content.removeFocusListener(fl);
    }
    public void addMouseListener(MouseListener ml)
    {
        this.content.addMouseListener(ml);
    }
    public void removeMouseListener(MouseListener ml)
    {
        this.content.removeMouseListener(ml);
    }
    
    /** set the focus to this control */
    public void setFocus()
    {
        this.content.requestFocus();
    }
    
    
    /** set the hint text */
    public void setHintText(String text)
    {
    }
    
    /**
     * Set's this components zoom (1.0 is the default).
     */
    public void setZoom(double zoom)
    {
        // TODO: zoom
        if (currentZoom==zoom) return; // no change
        if (this.content!=null)
        {
            if (this.getStyle()!=null && !(Utilities.getJavaVersion()<1.2))
            {
                Font newFont = CompatibilityFactory.getCompatibility().deriveFont(
                        defaultFont,
                        defaultFont.getStyle(),
                        (float)(defaultFont.getSize()*zoom));
                this.setFont(newFont);
                //Dimension d = this.cssFormatter.getSize();
                //this.sizeComponent(zoom);
            }
            this.sizeComponent(zoom);
            currentZoom=zoom;
        }
    }
    
    protected void sizeComponent(double zoom)
    {
        Dimension orig = this.getDefaultSize();
        Dimension zoomed = new Dimension((int)(orig.width*zoom),(int)(orig.height*zoom));
        this.sizeComponent(zoom,zoomed);
    }
    
    
    public void sizeComponent(double zoom, Dimension size)
    {
        this.getSizableComponent().setSize(size);
    }
    
    /**
     * Apply a style to the component.
     * It is not specified which style can be associated
     * with the component. It can be CSS, but it can also
     * be, non-visual description languages if need be.
     *
     * Example: Apply CSS style to component
     *
     * ALL RELATIVE/ABSOLUTE POSITIONING, BACKGROUND ALIGNMENT ETC
     * is specified with CSS!
     *
     * @param style The style to apply
     */
    public void setStyle(CSSStyleDeclaration a_style)
    {
        // TODO: do we need to store the CSS style?
        //Log.debug("Styling with "+a_style+" this:"+this);
        style=a_style;
        if (this.getStylableComponent()==null) return;
        // TODO: zoom!
        this.getFormatter().formatComponent(this.getStylableComponent(),style,
                this.getDefaultBackgroundColor(),this.currentZoom);
        this.setDefaultSize();
        this.sizeComponent(1.0,this.getDefaultSize());
        this.defaultFont = this.getStylableComponent().getFont();
        this.defaultBGColor = this.getStylableComponent().getBackground();
    }
    
    /**
     * Called in the beginning to find out the default size (CSS+component)
     */
    protected void setDefaultSize()
    {
        // set the default size, if something is not found in CSS, use the
        // current dimensions
        // TODO: for AWT, this returns (0,0), when called before the component is realized... Maybe we 
        // need to add the component to the container first and then style them... How to do this so that Swing would still work as previously?
        Dimension orig=this.getPreferredSize(this.getSizableComponent());
        //Dimension orig = this.getSizableComponent().getPreferredSize();
        //Log.debug(this.toString()+"setContentDefaultSize: orig: "+orig);
        if (this.style==null) return;
        Dimension css = CSSFormatter.getSize(this.style);
        // Check that this component is bigger than the minimun size
        Dimension minimumSize = this.getMinimumSize();
        int w=(int)css.width, h=(int)css.height;
        if (w<0)
        {
            // CSS had no value
            w=(int)orig.width;
            if (w<minimumSize.width) w=minimumSize.width;
        }
        if (h<0)
        {
            // CSS had no value
            h=(int)orig.height;
            if (h<minimumSize.height) h=minimumSize.height;
        }
        this.defaultSize=new Dimension(w,h);
        //Log.debug("Set content default size: "+this.defaultSize);
    }

    /**
     *  in AWT, we must do extra tricks to get the preferred size before the component is on screen
     * 
     */
    public Dimension getPreferredSize(Component comp)
    {
       return comp.getPreferredSize();
    }    
    
    public CSSFormatter getFormatter()
    {
        return fi.hut.tml.xsmiles.gui.components.awt.AWTCSSFormatter.getInstance();
    }
    
    /** add a listener for help events */
    public void addHelpListener(ActionListener fl)
    {
        Log.debug("addHelpListener not implemented!");
    }
    
    /** remove a listener for help events */
    public void removeHelpListener(ActionListener fl)
    {
          Log.debug("removeHelpListener not implemented!");
    }
}

