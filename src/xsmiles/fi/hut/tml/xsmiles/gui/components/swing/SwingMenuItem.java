/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.JMenuItem;

import fi.hut.tml.xsmiles.gui.components.XMenuItem;

/**
 * @author Mikko Honkala
 */
public class SwingMenuItem extends JMenuItem implements XMenuItem
{    
    String name;
    
    public SwingMenuItem(String n)
    {
    	super(n);
        name=n;     
    }
    
    public Object getObject()
    {
            return this;
    }
    
    public void selectItem(boolean select) {
    	if (select) {
    		setText("<html><b>"+name+"</b></html>");
    	} else {
    		setText(name);
    	}
   	}
}
