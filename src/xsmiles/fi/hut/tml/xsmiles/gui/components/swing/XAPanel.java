package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;

/**
 * A JPanel where you can set an ImageIcon to the background
 * 
 * @author Juha
 * @version 0
 */
public class XAPanel extends JPanel {
    private ImageIcon theImage=null;
    
    /**
     * @param i The ImageIcon to be set to the background
     */
    public void setBackgroundImage(ImageIcon i)
    {
	theImage=i;
    }

    public void paintComponent(java.awt.Graphics g)
    {
	super.paintComponent(g);
	int width = getWidth();
	int height = getHeight();
	java.awt.Color oldColor = g.getColor();
	if (isOpaque())
	    {
		g.setColor(getBackground());
		g.fillRect(0, 0, width, height);
	    }
	if (theImage != null)
	    {
		g.drawImage(theImage.getImage(), 0, 0, this);
	    }
	g.setColor(oldColor);
    }
}
