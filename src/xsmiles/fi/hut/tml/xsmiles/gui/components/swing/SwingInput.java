/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;
import fi.hut.tml.xsmiles.gui.GUI;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XSecret;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.*;

import org.w3c.dom.css.CSSStyleDeclaration;



/**
 * textinput line
 * this is extended from SwingComponent, which will add captions and styling etc
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public class SwingInput extends SwingTextComponent implements XInput, XSecret
{
    //private JTextField visualComponent;
    /** the action for enter keypress */
    protected char echoChar = '*';
    protected boolean isSecret = false;
    final static Dimension minSize = new Dimension(40,5);
    
    
    /**
     * @param c The number of columns initially
     * gui is given for the inputmode functionality
     */
    public SwingInput(GUI gui)
    {
        super(gui);
        init();
    }
    public SwingInput(char passwdchar, GUI gui)
    {
        super(gui);
        this.echoChar = passwdchar;
        this.isSecret = true;
        init();
    }
    
   
    /**
     * in init the subclass always creates its component first and then calls
     * super.init
     */
    public void init()
    {
        this.content = this.createContent();
        // Super will then add the content component to its container
        super.init();
    }
    
    /** return the minimum size for this component at zoom level 1.0 */
    public Dimension getMinimumSize()
    {
        return minSize;
    }
    
    /** creates the content component */
    public Component createContent()
    {
        if (!isSecret)
        {
            textcomponent=new SmoothTextField();
        }
        else
        {
            textcomponent = new JPasswordField();
            ((JPasswordField)textcomponent).setEchoChar(echoChar);
        }
        return textcomponent;
    }
    public String getPassword()
    {
        return this.getText();
    }
    
    /**
     * the default background color for this component
     * null = transparent.
     */
    
    
    
    public void addActionListener(ActionListener al)
    {
        this.textcomponent.registerKeyboardAction(
        al,
        ENTER_STROKED,
        KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER,0),
        JComponent.WHEN_FOCUSED
        );
        
    }

    
    public void removeActionListener(ActionListener al)
    {
        this.textcomponent.resetKeyboardActions();
    }
    /** a textarea with antialiasing */
    private class SmoothTextField extends JTextField
    {
    
        SmoothTextField ()
        {
            super();
        }
        SmoothTextField (String text)
        {
            super(text);
        }
        public void paint(Graphics g)
        {
            CompatibilityFactory.getGraphicsCompatibility().setAntialias(true,g);
            super.paint(g);
        }
    }
    public void removeBorders()
    {
        // Borders are set in the xforms.css
        this.textcomponent.setBorder(new EmptyBorder(0,0,0,0));
    }

    
    
}
