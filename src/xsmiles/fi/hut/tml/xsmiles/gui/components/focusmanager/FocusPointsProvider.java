/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package  fi.hut.tml.xsmiles.gui.components.focusmanager;

/**
 * 
 * This is a simple interface to provide the Focus Points of a visible region
 * (when navigation is done by LEFT, RIGHT, UP; DOWN) or next and previous 
 * Focus Points (when navigation is done by tabbing or voice)
 *
 * This interface should be implemented by GUI (focus through the GUI region) 
 * and ContentHandler (focus through the actual web content)
 *
 * @author Pablo Cesar, Mikko Honkala
 * 
 *  
 */
public interface FocusPointsProvider{


    public FocusPoint[] getFocusPoints();
    
    public FocusPoint getNextFocusPoint();

    public FocusPoint getPreviousFocusPoint();

}
