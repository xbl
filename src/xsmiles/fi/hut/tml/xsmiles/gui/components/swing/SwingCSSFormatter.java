/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComponent;

import fi.hut.tml.xsmiles.gui.components.CSSFormatter;
import fi.hut.tml.xsmiles.gui.components.awt.AWTCSSFormatter;

/**
 * CSS specific operations on components
 * @author Mikko Honkala
 */
public class SwingCSSFormatter extends AWTCSSFormatter
{
  public SwingCSSFormatter()
  {
  }
  public static CSSFormatter getInstance()
  {
	  if (formatter==null) formatter=new SwingCSSFormatter();
	  return formatter;
  }
  
  
	
  /**
   * Hiding/Overriding the setTransparency method of AWTCSSFormatter
   * The only difference here is that the Swing version can be transparent.
   */
  public void setTransparency(Component comp, Color bgcolor, boolean transparent) 
  {
    if (comp instanceof JComponent) {
      JComponent jcomp = (JComponent)comp;
      //comp.setBackground(bgcolor);
      
      if (!transparent) {
	jcomp.setOpaque(true);	
      } else {
	jcomp.setOpaque(false);	
      }
    }
  }
}
