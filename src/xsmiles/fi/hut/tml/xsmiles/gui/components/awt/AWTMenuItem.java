/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import fi.hut.tml.xsmiles.gui.components.XMenuItem;

/**
 * @author Mikko Honkala
 */
public class AWTMenuItem implements XMenuItem
{
    MenuItem item;
    String name;
    public AWTMenuItem(String n)
    {
        name=n;
        this.init();
    }
    public void init()
    {
        this.item=new MenuItem(name);
    }
    public void addActionListener(ActionListener al)
    {
        item.addActionListener(al);
    }
    public Object getObject()
    {
            return item;
    }
	
	public void selectItem(boolean select) {
		if (select) {
			item.setLabel("* "+name);
		} else {
			item.setLabel(name);
		}
	}
    

}
