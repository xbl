/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;
import java.util.Vector;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XContainer;


/**
 *  The only addition is add and remove
 *  Utilize the componentWithcontainer
 */
public class SwingContainer  implements XContainer  
{
    // MH: THIS DOES NOT CURRENTLY WORK!
    // Should XCompound extend XComponent and this then should probably implement that?
  private boolean actionable=false;
  private Vector listeners,flisteners;
  
  protected Container container;

  public SwingContainer() {
    listeners=new Vector();
    flisteners=new Vector();
    container=new Container();
  }

  /**
   * @param c The component to be added
   */
  /*
  public void add(XComponent c) {
    add(c.getComponent());
  }

  /**
   * Apply a style to the component. 
   * It is not specified which style can be associated
   * with the component. It can be CSS, but it can also 
   * be, non-visual description languages if need be.
   * 
   * Example: Apply CSS style to component
   * 
   * ALL RELATIVE/ABSOLUTE POSITIONING, BACKGROUND ALIGNMENT ETC
   * is specified with CSS!
   *
   * @param style The style to apply
   */

  /**
   * By default components are inactive
   * @return Is can compnent be active?
   */
  public boolean getActionable() 
  {
    return actionable;
  }
  
  /**
   * @param a The value of actionable
   */
  public void setActionable(boolean a) 
  {
    actionable=a;
  }

  /**
   * @param c Remove component c
   */
  public void remove(XComponent c) {
    this.container.remove(c.getComponent());
  }

  /**
   * Remove all components from conatiner.
   */
  public void removeAll() {
    this.container.removeAll();
  }
  
  public void add(XComponent comp)
  {
	  this.container.add(comp.getComponent());
  }
/*
  public Component getComponent() 
  {
    return this;
  }
*/
  /**
   * Returns the approximate size of this extension element
   */
  public void setZoom(double zoom)
  {
    Log.debug("Zoom not set yet");
  }

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XContainer#add(java.awt.Component)
 */
public void add(Component c)
{
   this.container.add(c);
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XContainer#remove(java.awt.Component)
 */
public void remove(Component c)
{
    this.container.remove(c);
   
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setStyle(org.w3c.dom.css.CSSStyleDeclaration)
 */
public void setStyle(CSSStyleDeclaration style)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getStyle()
 */
public CSSStyleDeclaration getStyle()
{
    // TODO Auto-generated method stub
    return null;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setVisible(boolean)
 */
public void setVisible(boolean v)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setEnabled(boolean)
 */
public void setEnabled(boolean b)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setBounds(int, int, int, int)
 */
public void setBounds(int x, int y, int width, int height)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getX()
 */
public int getX()
{
    // TODO Auto-generated method stub
    return 0;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getY()
 */
public int getY()
{
    // TODO Auto-generated method stub
    return 0;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getWidth()
 */
public int getWidth()
{
    // TODO Auto-generated method stub
    return 0;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getHeight()
 */
public int getHeight()
{
    // TODO Auto-generated method stub
    return 0;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setHintText(java.lang.String)
 */
public void setHintText(String text)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#addActionListener(java.awt.event.ActionListener)
 */
public void addActionListener(ActionListener al)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeActionListener(java.awt.event.ActionListener)
 */
public void removeActionListener(ActionListener al)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#addFocusListener(java.awt.event.FocusListener)
 */
public void addFocusListener(FocusListener fl)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeFocusListener(java.awt.event.FocusListener)
 */
public void removeFocusListener(FocusListener fl)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#addMouseListener(java.awt.event.MouseListener)
 */
public void addMouseListener(MouseListener fl)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeMouseListener(java.awt.event.MouseListener)
 */
public void removeMouseListener(MouseListener fl)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#addHelpListener(java.awt.event.ActionListener)
 */
public void addHelpListener(ActionListener fl)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeHelpListener(java.awt.event.ActionListener)
 */
public void removeHelpListener(ActionListener fl)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getComponent()
 */
public Component getComponent()
{
    return this.container;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getSize()
 */
public Dimension getSize()
{
    // TODO Auto-generated method stub
    return null;
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setBackground(java.awt.Color)
 */
public void setBackground(Color bg)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setForeground(java.awt.Color)
 */
public void setForeground(Color fg)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setInputMode(java.lang.String)
 */
public void setInputMode(String inputmode)
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#setFocus()
 */
public void setFocus()
{
    // TODO Auto-generated method stub
    
}

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.gui.components.XComponent#getEnabled()
 */
public boolean getEnabled()
{
    // TODO Auto-generated method stub
    return this.container.isEnabled();
}


}

