/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 8, 2005
 *
 */
package fi.hut.tml.xsmiles.gui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;

import org.w3c.dom.css.CSSStyleDeclaration;


/**
 * @author honkkis
 *
 */
public class XComponentWrapper implements XComponent
{
    public Component comp;
    public CSSFormatter formatter;
	public Color defaultColor;
    public XComponentWrapper(Component aComp, CSSFormatter aFormatter, Color aDefaultColor)
    {
        comp=aComp;
        formatter=aFormatter;
		defaultColor=aDefaultColor;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setStyle(org.w3c.dom.css.CSSStyleDeclaration)
     */
    public void setStyle(CSSStyleDeclaration style)
    {
        // TODO Auto-generated method stub
        if (formatter!=null)
            this.formatter.formatComponent(comp,style,defaultColor==null?formatter.getTransparentColor():defaultColor);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getStyle()
     */
    public CSSStyleDeclaration getStyle()
    {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setVisible(boolean)
     */
    public void setVisible(boolean v)
    {
        comp.setVisible(v);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setEnabled(boolean)
     */
    public void setEnabled(boolean b)
    {
        comp.setEnabled(b);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getEnabled()
     */
    public boolean getEnabled()
    {
        return comp.isEnabled();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setBounds(int, int, int, int)
     */
    public void setBounds(int x, int y, int width, int height)
    {
        comp.setBounds(x,y,width,height);
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getX()
     */
    public int getX()
    {
        // TODO Auto-generated method stub
        return comp.getX();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getY()
     */
    public int getY()
    {
        // TODO Auto-generated method stub
        return comp.getY();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getWidth()
     */
    public int getWidth()
    {
        // TODO Auto-generated method stub
        return comp.getWidth();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getHeight()
     */
    public int getHeight()
    {
        // TODO Auto-generated method stub
        return comp.getHeight();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setHintText(java.lang.String)
     */
    public void setHintText(String text)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#addActionListener(java.awt.event.ActionListener)
     */
    public void addActionListener(ActionListener al)
    {
        // TODO Auto-generated method stub
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeActionListener(java.awt.event.ActionListener)
     */
    public void removeActionListener(ActionListener al)
    {
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#addFocusListener(java.awt.event.FocusListener)
     */
    public void addFocusListener(FocusListener fl)
    {
        comp.addFocusListener(fl);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeFocusListener(java.awt.event.FocusListener)
     */
    public void removeFocusListener(FocusListener fl)
    {
        comp.removeFocusListener(fl);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#addMouseListener(java.awt.event.MouseListener)
     */
    public void addMouseListener(MouseListener fl)
    {
        comp.addMouseListener(fl);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeMouseListener(java.awt.event.MouseListener)
     */
    public void removeMouseListener(MouseListener fl)
    {
        comp.removeMouseListener(fl);
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#addHelpListener(java.awt.event.ActionListener)
     */
    public void addHelpListener(ActionListener fl)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#removeHelpListener(java.awt.event.ActionListener)
     */
    public void removeHelpListener(ActionListener fl)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getComponent()
     */
    public Component getComponent()
    {
        // TODO Auto-generated method stub
        return comp;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getSize()
     */
    public Dimension getSize()
    {
        return comp.getSize();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setZoom(double)
     */
    public void setZoom(double zoom)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setBackground(java.awt.Color)
     */
    public void setBackground(Color bg)
    {
        // TODO Auto-generated method stub
        comp.setBackground(bg);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setForeground(java.awt.Color)
     */
    public void setForeground(Color fg)
    {
        comp.setForeground(fg);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setInputMode(java.lang.String)
     */
    public void setInputMode(String inputmode)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#setFocus()
     */
    public void setFocus()
    {
        // TODO Auto-generated method stub
       
    }

}
