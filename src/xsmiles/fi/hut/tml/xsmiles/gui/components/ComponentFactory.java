package fi.hut.tml.xsmiles.gui.components;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.net.URL;

import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * A ComponentFactory is a XContainer, with the functionality to deal out
 * components.
 *
 * The default ComponentFactory which produces components, based on the GUI
 * which is active. This enables the MLFC's to use generic components, whenever
 * it is possible. Each GUI should override these methods, to produce wanted
 * components. Interfaces for all components exist in
 * fi.hut.tml.xsmiles.gui.components
 *
 * The add methods provide a way to move components to the browser GUI.
 *
 * Some more component access methods will be added in the future
 *
 * The generic abstract component library is somewhat based on XForms
 * All copmonents are based on XComponent. Components can also be styled.
 * Although the standard for styling is not specified, in visual platforms
 * it will likely be CSS.
 *
 * For sound based systems, another styling methodology can be used.
 * e.g. Aural Style sheets (ASS).
 *
 * @author  Juha
 * @author  Mikko Honkala
 * @version 0
 */
public interface ComponentFactory 
{
    
    int VERTICAL_SCROLLBAR_AS_NEEDED = 20;
    int VERTICAL_SCROLLBAR_NEVER = 21;
    int VERTICAL_SCROLLBAR_ALWAYS = 22;
    /**
     * Different GUIs have different ways of showing focus etc. This is why
     * Every GUI can have it's own kind of linkComponent.. Even voice browsers
     * have links, and they are quite different from desktop link components
     */
    public XLinkComponent getXLinkComponent(String n);
    
    /**
     * @return a container for components
     */
    public XContainer getXContainer();
    
    public XPanel getXPanel();
    
    /**
     * Give a XMLDocument, and get a rendered document
     * If you are in for events, just add an actionListener
     * and receive the XMLEvents from the component
     * @param doc The document
     * @return the visual component that contains rendered xmldocument. Null
     *         is returned if rendering is not possible.
     *
     */
    public XDocument getXDocument(XLink doc);
    public CSSFormatter getCSSFormatter();
    
    public XTabbedPane getXTabbedPane();
    
    /**
     * @param s The intial text
     * @return a textarea
     */
    public XTextArea getXTextArea(String s);
    
    /**
     * @return A secret input control
     */
    public XSecret getXSecret(char c);
    
    /**
     * @return  one line input
     */
    public XInput getXInput();
    
    /**
     * @param from from what
     * @param to to where
     * @return a range control
     */
    public XRange getXRange(int from, int to, int step, int orientation);
    
    /**
     * Give an URL and receive the mediaelement.
     */
    public XMedia getXMedia(URL u);
    
    /**
     * @param appearance The appearance of selectone: 'minimal','compact','full'
     * @return a XSelectOne, which is used to select one item at a time
     */
    public XSelectOne getXSelectOne(String appearance, boolean open);
    /**
     * @return a XSelectBoolean, which is used to select true / false
     */
    public XSelectBoolean getXSelectBoolean();
    
    /**
     * @param v The vector that contains all possible selections
     * @return A selectMany control.
     */
    public XSelectMany getXSelectMany(String appearance, boolean open);
    
    /**
     * @return an upload control.
     */
    public XUpload getXUpload(String caption);
    
    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save);
    /**
     * @return an file chooser control.
     */
    public XFileDialog getXFileDialog(boolean save,String filename);
    
    /**
     * @return an authenticator control.
     */
    public XAuthDialog getXAuthDialog();
    
    /**
     * @return an confirmation control.
     */
    public XConfirmDialog getXConfirmDialog();
    /**
     * @return a button w a label
     */
    public XButton getXButton(String label, String iconUrl);
    
    /**
     * @return a caption (label) component
     */
    public XCaption getXCaption(String captText);
    
    /**
     * @return a icon button
     */
    public XButton getXButton(String iconUrl);
    /**
     * @return a button with images
     */
    public XButton getXButton(String imageUrl, String focusedImage, String disabledImage);
    
    
    public XMenuBar getXMenuBar();
    public XMenu getXMenu(String name);
    public XMenuItem getXMenuItem( String name);
    /**
     * @ return a compound designed for holding a label and a component
     */
    public XLabelCompound getXLabelCompound(XComponent comp, XCaption capt,
    String captSide);
    
    /* show an error dialog */
    public void showError(String title, String explanation);

    /* show a link popup */
    public void showLinkPopup(URL url, XMLDocument doc,java.awt.event.MouseEvent e, MLFCListener listener);

    
    /** listen for all help keypresses etc, for this component and its ancestors */
    public void addHelpListener(Component component, ActionListener listener);
    
    /** remove a help listener */
    public void removeHelpListener(Component component, ActionListener listener);
    
    /** create a content panel for the browser, add also maybe a layout manager */    
    public Container createContentPanel();
    
    /** create a scroll pane for this components. e.g. in swing, create a JScrollPane and put the component inside */
    public Container createScrollPane(Component comp,int policy);
        
    public void showMessageDialog(boolean isModal, String title, String message, long timeToLiveMillis) ;
    
    /** query for extension availability. For instance, calendar control is an extension */
    public boolean hasExtension(Class c);
    
    /** return an extension control. hasExtension must be called first */
    public Object getExtension(Class c);
        
    /**
     * Set scroll bar to a given position
     * 
     * @param con
     * @param x
     * @param y
     */
    public void setScrollBar(Container con, int x, int y);

    /**
     * Returns a Toolkit dependent FocusProvider
     *
     * @return XFocusManager
     * @param 
     **/
    public XFocusManager getXFocusManager();

}
