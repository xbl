/*
 * Created on Mar 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.gui.components;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface XMenu
{
    /** add menu choice */
    public void add(XMenuItem item);
    /** add submenu */
    public void add(XMenu item); 
    public void remove(XMenu item);
    public void remove(XMenuItem item);
    public void removeAll();
    public void addSeparator(); 
    public Object getObject();
    
}
