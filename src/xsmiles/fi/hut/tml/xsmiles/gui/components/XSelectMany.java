package fi.hut.tml.xsmiles.gui.components;

import java.util.Vector;

/**
 * Used to select one object out of all. The first object is default
 * @author juha
 */
public interface XSelectMany extends XSelect 
{
  /**
   * @param v The vector containing all items, the first item is default
   */

  /**
   * @param o The object to set
   * @param b The value to set it as
   */
  public void setSelected(Object o, boolean b);
  public void clearSelection();
  public int[] getSelectedIndices();
  public void setSelectedIndices(int[] indices);

}
