/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;


import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import org.w3c.dom.DOMException;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;

/**
 * CSS specific operations on components
 * @author Mikko Honkala
 */
public abstract class CSSFormatter 
{
	/** read the size from the CSS declaration. Currently this is
	    very simple, just reads the width and height properties */
	public static Dimension getSize(CSSStyleDeclaration style)
	{
		if (style==null) return new Dimension(-1,-1);
		CSSPrimitiveValue pH=null, pW=null;
		CSSValue h = style.getPropertyCSSValue(CSSConstants.CSS_HEIGHT_PROPERTY);
		CSSValue w = style.getPropertyCSSValue(CSSConstants.CSS_WIDTH_PROPERTY);
		if (h instanceof CSSPrimitiveValue)	pH = (CSSPrimitiveValue) h;
		if (w instanceof CSSPrimitiveValue)	pW = (CSSPrimitiveValue) w;
			
		int width=-1,height=-1;
        try
        {
    		if (pH!=null && pH.getPrimitiveType()!=CSSPrimitiveValue.CSS_IDENT)
    			height=(int)(pH.getFloatValue(CSSPrimitiveValue.CSS_PX) + .5);
        } catch (DOMException e)
        {
            Log.debug("CSSFormatter: height:"+e.getMessage());
        }
        try
        {
    		if (pW!=null && pW.getPrimitiveType()!=CSSPrimitiveValue.CSS_IDENT)
    			width=(int)(pW.getFloatValue(CSSPrimitiveValue.CSS_PX) + .5);
        } catch (DOMException e)
        {
            Log.debug("CSSFormatter: width: "+e.getMessage());
        }
		Dimension d = new Dimension(width<0?-1:StyleGenerator.getScaledSize(width),height<0?-1:StyleGenerator.getScaledSize(height));
        return d;
	}
	/** Returns the string value of the caption-side CSS property */
	public static String getCaptionSide(CSSStyleDeclaration style)
	{
		if (style==null) return null;
		CSSValue val = style.getPropertyCSSValue(CSSConstants.CSS_CAPTION_SIDE_PROPERTY);
		if (val instanceof CSSPrimitiveValue)
		{
			CSSPrimitiveValue pVal = (CSSPrimitiveValue) val;
			return pVal.getStringValue();
		}
		return null;
	}
    public static Color getTransparentColor()
    {
        return Color.white;
    }

	/**
	 * applies CSS formatting to a swing component.
	 * this could be used for AWT components as well without the Opaque - setting
	 */
	public abstract void formatComponent(Component comp, CSSStyleDeclaration style, Color defaultComponentColor);
	/**
	 * applies CSS formatting to a swing component.
	 * this could be used for AWT components as well without the Opaque - setting
	 */
	public abstract void formatComponent(Component comp, CSSStyleDeclaration style, Color defaultComponentColor,double zoom);
	/*{
		Log.error("formatComponent not implemented yet");
	}*/
}
