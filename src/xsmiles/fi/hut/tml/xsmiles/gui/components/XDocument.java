/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;
import fi.hut.tml.xsmiles.XLink;

/**
 *  Interface to a xml media component
 */
public interface XDocument extends XMedia 
{
  /**
   * @param A link to the file which is to be rendered here
   */
  public void setXMLDocument(XLink l);
}

