/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.content.UnknownContentHandler;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.event.*;
import java.net.URL;
import java.io.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import javax.swing.*;

import java.awt.datatransfer.*;

/**
 * A Menu, that can be opened when the user clicks right mouse button on a link
 * @author Mikko Honkala
 */
public class SwingPopup
{
    protected JPopupMenu popup;
    
    protected URL url;
    protected MLFCListener bw;
    protected XMLDocument document;
    
    /**
     * Swing link popup menu
     */
    public SwingPopup()
    {
    }
    
    protected boolean isTabbed()
    {
        if (this.bw==null) return false;
        return this.bw.getIsTabbedGUI();
    }
    
    protected JMenuItem createItem(String title,ActionListener list)
    {
        JMenuItem newItem = new JMenuItem(title);
        newItem.addActionListener(list);
        popup.add(newItem);
        return newItem;
    }
    protected void addSeparator()
    {
        popup.addSeparator();
    }
}
