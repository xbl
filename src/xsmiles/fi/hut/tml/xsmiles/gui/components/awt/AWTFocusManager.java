/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Rectangle;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Component;
import java.awt.Point;
import java.awt.ScrollPane;

import java.util.Vector;
import java.util.Enumeration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.ContentFocusPointsProvider;
import fi.hut.tml.xsmiles.csslayout.FocusComponent;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.components.XFocusManager;

/**
 * 
 * This is the base class that is extended by FocusPointProviders
 * 
 * @author Pablo Cesar, Mikko Honkala
 * 
 *  
 */

public class AWTFocusManager implements XFocusManager
{

    /* Variables */
    public FocusPoint currentFocus;
    public FocusPoint guiFocus;
    public FocusPoint contentFocus;
    public FocusPoint tmpFocus;
    public FocusPoint[] fpList;
    public FocusPointsProvider guiprovider = null;
    public FocusPointsProvider contentprovider = null;
    public FocusComponent focusComponent;
    public Container contentArea;
    public Container rootContainer;
    public Container scrollContainer;

    public AWTFocusManager()
    {
	focusComponent = new FocusComponent();
	currentFocus = new FocusPoint(0,0,0,0);
    }

    public void setFirstFocusPoint(){
	currentFocus = new FocusPoint(0,0,0,0);
    
	// MH: personal java fix changeFocus(XFocusManager.RIGHT_FOCUS_POINT);
    }

    public void addProvider(FocusPointsProvider fp)
    {
        if (fp instanceof ContentFocusPointsProvider)
            this.contentprovider = fp;
        else
            this.guiprovider = fp;
    }
    
    public FocusComponent getFocusComponent()
    {
        return focusComponent;
    }

    
    /* Change Focus has been requested * */
    public void changeFocus(int direction)
    {
        guiFocus = null;
        contentFocus = null;       
	
        if (guiprovider != null){
	    guiFocus = calculateNewFocusPoint(direction, guiprovider, XFocusManager.WIDGETS_FOCUS_PROVIDER);
	}
	if (contentprovider != null){
	    // MH: personal profile bug contentFocus = calculateNewFocusPoint(direction, contentprovider, XFocusManager.DOCUMENT_FOCUS_PROVIDER);
	}
        if ((guiFocus != null) || (contentFocus != null)){
            tmpFocus = calculateMin(currentFocus, direction, guiFocus, contentFocus);
	   
	    if (currentFocus != null){
		cleanFocusPoint(currentFocus);
	    }
	    currentFocus = tmpFocus;
        if (currentFocus!=null)
            drawFocusPoint(currentFocus);
	}
    }


    public FocusPoint calculateMin(FocusPoint origin, int direction, FocusPoint fp1, FocusPoint fp2)
    {
        if (fp1 == null)
            return fp2;
        if (fp2 == null)
            return fp1;
        fpList = null;
        fpList = new FocusPoint[2];
        fpList[0] = fp1;
        fpList[1] = fp2;
        return applyAlgorithm(direction);
    }

    private boolean isValidDirection(int direction)
    {
        return ((direction == NEXT_FOCUS_POINT) || (direction == PREVIOUS_FOCUS_POINT)
                || (direction == LEFT_FOCUS_POINT) || (direction == RIGHT_FOCUS_POINT)
                || (direction == UP_FOCUS_POINT) || (direction == DOWN_FOCUS_POINT));
    }

    
    public boolean isScrollPane(Container cont){
	return (cont instanceof ScrollPane);
    }
    
    public Container getScrollContainer(Container cont){
	Component component;
	Container container = null;
	int numComponents = cont.countComponents();
	int i = 0;
	while (i<numComponents && container==null){
	    component = cont.getComponent(i);
	    if (component instanceof Container){
		if (isScrollPane((Container)component))
		    container = (Container)component;
		else
		    container = getScrollContainer((Container)component);
	    }
	    i++;
	}
	return container;
    }

    private void translateToViewLocation(FocusPoint[] fpListgui){
	/** gui provider gives the location of focus points in relation to their parent container. **/
	for (int i=0;i<fpListgui.length;i++){
	    fpListgui[i].move(getBrowserViewLocation(fpListgui[i].getComponent()));
	}
    }

    public Point getScrollPosition(Component component){
	return ((ScrollPane)component).getScrollPosition();
    }



    public Point getCurrentViewLocation(){
	Point value = new Point(0,0);
	Container scrollPane = getScrollContainer(contentArea);
	if (isScrollPane((Container)scrollPane)){
	    value = getScrollPosition(scrollPane);
	}
	return value;
    }
    
    public Point getBrowserViewLocation(Component comp){
        if (comp==null) return null;
	int X = comp.getLocation().x;
	int Y = comp.getLocation().y;
	if (comp.getParent() != null){
	    Component component = comp.getParent();	    
	    X = X+getBrowserViewLocation(component).x;
	    Y = Y+getBrowserViewLocation(component).y;
	}
	return new Point(X,Y);
    }
    
    private void translateToBrowserLocation(FocusPoint[] fpListcontent){
	Container scrollPane = getScrollContainer(contentArea);

	Point absolute_position = getBrowserViewLocation((Component)scrollPane);
	Point scroll_position = getCurrentViewLocation();

	/** We have to take into account the browserView and the Current View coordinates **/	
	for (int i=0;i<fpListcontent.length;i++){
	    /** We have to translate it into ViewCoordinates [taking into account the current view coordinates]**/
	    fpListcontent[i].translate(new Point(0-scroll_position.x,0-scroll_position.y));
	    if (!controlHaviScrollPage(fpListcontent[i]))
		// The focus point is not in the current page
		fpListcontent[i]=null;
	    else
		// The focus point is in the current page, so we can translate to the browser view coordinates
		fpListcontent[i].translate(absolute_position);	         
	}
    }
    
    private FocusPoint calculateNewFocusPoint(int direction, FocusPointsProvider provider, int providerType)
    {
        /*
         * concept of direction defines the actual place to go next left, right,
         * up, down, previous, next options: NEXT_FOCUS_POINT
         * PREVIOUS_FOCUS_POINT LEFT_FOCUS_POINT RIGHT_FOCUS_POINT
         * UP_FOCUS_POINT DOWN_FOCUS_POINT
         */
        FocusPoint tmpFocus;

        if (!isValidDirection(direction))
            return null;
        switch (direction)
	    {
            case NEXT_FOCUS_POINT :
                tmpFocus = provider.getNextFocusPoint();
                break;
            case PREVIOUS_FOCUS_POINT :
                tmpFocus = provider.getPreviousFocusPoint();
                break;
            default :
                fpList = provider.getFocusPoints();
		/**
		   if (fpList!= null){
		   System.out.println("FPLIST");
		   for (int i=0;i<fpList.length;i++){
		   System.out.println("Elemento:"+i);
		   System.out.println(fpList[i].toString());
		   }
		   }
		**/
		if (providerType== XFocusManager.WIDGETS_FOCUS_PROVIDER){
		    /** gui provider gives the location of focus points in relation to their parent container. We have to translate it into ViewCoordinates [taking into account the BrowserCoordinates and the current view coordinates]**/
		    translateToViewLocation(fpList);
		}else{
		    /** content provider gives the location of focus points in relation to the Document View. We have to translate it into ViewCoordinates [taking into account the BrowserCoordinates] **/
		    translateToBrowserLocation(fpList);
		}		    
		tmpFocus = applyAlgorithm(direction);
	    }
        return tmpFocus;
    }

    /*
     * 
     * @return focusPoint at the direction. (or null if not found)
     */
    protected FocusPoint applyAlgorithm(int direction)
    {
        FocusPoint tmp = null;
        FocusPoint closest = null;
        FocusPoint origo = currentFocus;
        int origox = calculateMidX(origo);
        int origoy = calculateMidY(origo);
        int tmpx, tmpy;
        double distance = 0, closestDistance = 99999999;
        // Go through all links, find the closest
        for (int i = 0; i < fpList.length; i++)
        {

	    if (fpList[i]==null)
		continue;
            tmp = fpList[i];

	    if (tmp.getNumRectangles()<=0)
		continue;
	    
            // Skip itself
            if (tmp.equals(origo))
            {
                continue;
            }
            // If not at upper quadrant -> skip
            tmpx = calculateMidX(tmp);
            tmpy = calculateMidY(tmp);
            if (direction == UP_FOCUS_POINT)
            {
                if (!((tmpx - origox) < -(tmpy - origoy) && (tmpx - origox) > (tmpy - origoy)))
                    continue;
            }
            if (direction == DOWN_FOCUS_POINT)
            {
                if (!((tmpx - origox) > -(tmpy - origoy) && (tmpx - origox) < (tmpy - origoy)))
                    continue;
            }

            if (direction == LEFT_FOCUS_POINT)
            {
                if (!((tmpx - origox) < -(tmpy - origoy) && (tmpx - origox) < (tmpy - origoy)))
                    continue;
            }
            if (direction == RIGHT_FOCUS_POINT)
            {
                if (!((tmpx - origox) > -(tmpy - origoy) && (tmpx - origox) > (tmpy - origoy)))
                    continue;
            }

            // Find the closest link, d = sqrt( (x2-x1)*(x2-x1) +
            // (y2-y1)*(y2-y1) )
            distance = Math.sqrt((tmpx - origox) * (tmpx - origox) + (tmpy - origoy) * (tmpy - origoy));
            if (distance < closestDistance)
            {
                closest = tmp;
                closestDistance = distance;
            }
        }
        return closest;
    }

    protected int calculateMidX(FocusPoint fp)
    {
        /** The focusPoint should be given in absolute position * */
        int minx = 1000;
        int maxx = 0;
        int x1 = 0;
        int x2 = 0;
        Rectangle tmp;
        for (Enumeration e = fp.elements(); e.hasMoreElements();)
        {
            tmp = (Rectangle) e.nextElement();
            x1 = tmp.getLocation().x;
            if (x1 < minx)
                minx = x1;
            x2 = x1 + tmp.getSize().width;
            if (x2 > maxx)
                maxx = x2;
        }
        return ((maxx + minx) / 2);
    }

    protected int calculateMidY(FocusPoint fp)
    {
        int miny = 1000;
        int maxy = 0;
        int y1 = 0;
        int y2 = 0;
        Rectangle tmp;
        for (Enumeration e = fp.elements(); e.hasMoreElements();)
        {
            tmp = (Rectangle) e.nextElement();
            y1 = tmp.getLocation().y;
            if (y1 < miny)
                miny = y1;
            y2 = y1 + tmp.getSize().height;
            if (y2 > maxy)
                maxy = y2;
        }
        return ((maxy + miny) / 2);
    }

    public void addContentArea(Container cont){
	this.contentArea = cont;
    }
    
    public void addRootContainer(Container cont){
	this.rootContainer = cont;
    }


    public void cleanFocusPoint(FocusPoint fp)
    {
	Component component = fp.getComponent();
	if (component == null){
	    focusComponent.setVisible(false);
	    contentArea.remove(focusComponent);
	    Log.debug("Clean focus from " + fp.getRectangleAt(0));
	}else{
	    rootContainer.requestFocus();
	}
    }
    

    private Point distanceToDocumentLocation(){
	Container scrollPane = getScrollContainer(contentArea);
	Point addition = getBrowserViewLocation((Component)scrollPane);
	addition.x = 0-addition.x;
	addition.y = 0-addition.y;
	return addition;
    }

    public boolean controlHaviScrollPage(FocusPoint fp){
	return true;
    }

    public void drawFocusPoint(FocusPoint fp)
    {
	// This is the distance in relation to the browser coordinates
	Point distance_to_document = distanceToDocumentLocation();
	
	/** We have to paint it as document position **/
	Component component = fp.getComponent();
	
	if (component == null){
	    fp.translate(distance_to_document);
	    focusComponent.setCurrentFocus(fp);
	    contentArea.add(focusComponent, BorderLayout.CENTER);
	    popToFront(contentArea, focusComponent);
	    
	    /** But current focus point is in relation to document coordinates **/
	    fp.translate(new Point(0-distance_to_document.x,0-distance_to_document.y));
	    Log.debug("Draw focus to " + fp.getRectangleAt(0));
	}else{
	    component.requestFocus();
	}
    }


    private boolean popToFront(Container cont,Component component){
	
	int index;
	if ((index = getIndexOfComponent(cont,component)) != -1)
	    if (index == 0)
		return true;
	    else
		return moveTo(cont, component, index, 0);
	else
	    return false;
    }

    private synchronized boolean moveTo(Container cont, Component comp, int from, int to){
	cont.remove(from);
	return (cont.add(comp, to) != null);
    }

    public int getIndexOfComponent(Container cont, Component comp){
	int index = 0;
	int maxim = cont.getComponentCount();

	while ( (index <= maxim) && (cont.getComponent(index) != comp) ){ 
	    index++;
	}

	if (index >= cont.getComponentCount())
	    return -1;
	else	
	    return index;
    }

    public FocusPoint getCurrentFocusPoint(){
	return currentFocus;
    }
    
    public Rectangle getCurrentFocusRectangle(){
	Point distance_to_document = distanceToDocumentLocation();
	Point scroll_position = getCurrentViewLocation();
	Rectangle rec = new Rectangle(currentFocus.getRectangleAt(0));
	rec.translate(distance_to_document.x,distance_to_document.y);
	rec.translate(scroll_position.x,scroll_position.y);
	return rec;
    }
    
}
