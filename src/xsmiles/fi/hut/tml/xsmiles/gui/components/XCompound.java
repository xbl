/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import java.awt.Component;
import java.awt.Dimension;
/**
 *  The only addition is add and remove
 */
public interface XCompound  
{
  // VisualComponentService implementation
  /**
   * Return the visual component for this extension element
   */
  public Component getComponent();
  
  /**
   * Returns the size of this compound
   */
  public Dimension getSize();
  /**
   * Set this drawing area visible.
   * @param v    true=visible, false=invisible
   */
  public void setVisible(boolean v);
  
  
  /**
   * @param c The component to be added
   */
  public void add(XComponent c); 
  
  /**
   * @param c Remove component c
   */
  public void remove(XComponent c);
  
  /**
   * Remove all components from conatiner.
   */
  public void removeAll();
  
  /** set the zoom level (1.0 is the normal) */
  public void setZoom(double zoom);
}

