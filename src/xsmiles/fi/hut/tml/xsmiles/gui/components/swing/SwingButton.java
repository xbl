/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import javax.swing.*;

/**
 * Similar functionality as normal AWT button.
 * Features:
 * <ul>
 * <li>lightweight</li>
 * <li>three different states (focus, non-focus, pressed), each with graphical representations.</li>
 * <li>not all button functionality is included.</li>
 * </ul>
 * @author Mikko Honkala
 * @author Juha Vierine
 */
public class SwingButton extends SwingStylableComponent implements XButton
{
    protected JButton button;
    protected String name;
    protected ImageIcon icon;
    /**
     * A plain swing button
     */
    public SwingButton(String iconUrl)
    {
        this(null,iconUrl);
    }
    
    /**
     * A iconed swing button
     */
    public SwingButton(String a_name, String iconUrl)
    {
        name=a_name;
        if (iconUrl!=null) icon = (new XAImageIcon(iconUrl)).getImageIcon();
        init();
    }
    
    
    public void init()
    {
        this.content = this.createContent();
        //this.container = (Container)this.content.getAddableComponent();
    }
    /** creates the content component */
    private static Insets insets = new Insets(2,4,2,4);
    public Component createContent()
    {
        this.button = new SmoothButton(name,icon);
        this.button.setMargin(insets);
        return this.button;
        //return new SwingStylableComponent(this.button,this);
    }
    
    
    public Component getComponent()
    {
        return this.getAddableComponent();
    }
    
    /**
     * the default background color for this component
     * null = default.
     */
    public Color getDefaultBackgroundColor()
    {
        return null;
        //return Color.lightGray;
    }
    
    
    public void setCaptionText(String text)
    {
        this.setLabel(text);
    }
    
    public void setLabel(String t)
    {
        button.setLabel(t);
    }
    
    public void addActionListener(ActionListener al)
    {
        button.addActionListener(al);
    }
    
    public void removeActionListener(ActionListener al)
    {
        button.removeActionListener(al);
    }
    
    public void setImage(String fn)
    {
        button.setIcon(new ImageIcon(fn));
    }
    
    public void setImagePressed(String fn)
    {
        Log.debug("Not implemented");
    }
    
    public void setImageRollOver(String fn)
    {
        Log.debug("Not implemented");
    }
    
    
    public void setImageDisabled(String fn)
    {
        Log.debug("Not implemented");
    }
    
    public void setActionCommand(String ac)
    {
        button.setActionCommand(ac);
    }
    
    /** a textarea with antialiasing */
    private class SmoothButton extends JButton
    {
    
        SmoothButton ()
        {
            super();
        }
        SmoothButton (String text, ImageIcon icon)
        {
            super(text,icon);
        }
        public void paint(Graphics g)
        {
            CompatibilityFactory.getGraphicsCompatibility().setAntialias(true,g);
            super.paint(g);
        }
    }


    
}
