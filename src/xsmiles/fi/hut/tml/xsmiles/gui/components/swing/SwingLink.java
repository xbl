/* X-Smiles
 * Strip tease license:
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * Full license found in  licenses/XSMILES_LICENSE.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JComponent;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ActionEventEx;
import fi.hut.tml.xsmiles.gui.components.XHoverListener;
import fi.hut.tml.xsmiles.gui.components.XLinkComponent;
import fi.hut.tml.xsmiles.gui.components.awt.AWTStylableComponent;

/**
 * Default desktop lightweight linkcomponent. 
 * 
 * @author Juha
 * @author Mikko Honkala
 */
public class SwingLink extends AWTStylableComponent implements XLinkComponent 
{
  private MouseHandler         mouseHandler;
  private String               destination;
  private String               label;
  private boolean              active=false;
  private boolean              focused=false;
  private Vector               urlListeners;
  private Vector               focusListeners;
  
  
  //protected J
  
  public SwingLink() {
    init();
  }
  
  public void init() {
    mouseHandler = new MouseHandler();
	this.content = new InvisibleComponent();
    this.content.addMouseListener(mouseHandler);
    this.content.setBackground(Color.white);
    label=null;
    urlListeners=new Vector();
    focusListeners=new Vector();
    //this.container.addFocusListener(new LinkFocusListener());
  }
  public class InvisibleComponent extends JComponent
  {
        public void paint(Graphics g) {
        super.paint(g);
    /*if(hasFocus()) {
      g.setColor(Color.black);
      g.drawRect(0,0,10,10);
      }*/
/*     g.setColor(Color.blue);
     g.drawRect(0, 0, getWidth()-1, getHeight()-1);
*/        }

  }
  

  
  public boolean isFocusTraversable() {
    return true;
  }

  public void setDestination(String dest) {
    destination = dest;
  }

  public String getDestination() {
    return destination;
  }

  public void setLabel(String l) {
    label=l;
  }
  
  public String getLabel() {
    return label;
  }
  
  /**
   * Draw a rectangle around the active link, if in Digitv GUI
   *
   * @param g ...
   */ 
  public void paint(Graphics g) {
    content.paint(g);
    /*if(hasFocus()) {
      g.setColor(Color.black);
      g.drawRect(0,0,10,10);
      }*/
  }
  
  public void layoutComponent()
  {
  }
    

  /**
   * Sets active flag
   * 
   * @param val true or false
   */
  public void setActive(boolean val) {
    this.active=val;
    content.repaint();
  }
    
  /**
   * Is link active
   *
   * @return true if active
   */
  public boolean isActive() {
    return active;
  }

  public void addClickedActionListener(ActionListener al) {
    urlListeners.addElement(al);
  }
  
  public void addHoverListener(XHoverListener fl) {
    focusListeners.addElement(fl);
  }

  public void openAction(MouseEvent e) {
    Log.debug("Open action.. URL= " + destination);
    ActionListener reactee;
    Enumeration enumeration = urlListeners.elements();
  ActionEvent ev = new ActionEventEx(this,0,destination,e);
    while(enumeration.hasMoreElements()) {
      reactee = (ActionListener)enumeration.nextElement();
	  // TODO: create action object -MH
      reactee.actionPerformed(ev);
    }
  }
  
  

  public void focusLost() {
    XHoverListener reactee;
    Enumeration e = focusListeners.elements();
    while(e.hasMoreElements()) {
      reactee = (XHoverListener)e.nextElement();
      reactee.focusLost();
    }
  }

  public void focusGained() {
    XHoverListener reactee;
    Enumeration e = focusListeners.elements();
    while(e.hasMoreElements()) {
      reactee = (XHoverListener)e.nextElement();
      reactee.focusGained(destination);
    }
  }



  public class MouseHandler extends MouseAdapter {
    private boolean inside=false;         // is the cursor inside the component
    private boolean startedInside =false; // whether the click started from the inside
    
    public void mouseEntered(MouseEvent e)
    {
      inside=true;
      content.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));    
      focusGained();
    }
    
    public void mouseExited(MouseEvent e)
    {
      inside=false;
      content.setCursor(Cursor.getDefaultCursor());
      focusLost();
    }
    
    public void mousePressed(MouseEvent e)
    {
      startedInside=true;
      content.repaint();
    }
      
    public void mouseReleased(MouseEvent e)
    {
      if (inside)
	if (startedInside=true) 
	  openAction(e);
      startedInside=false;
    }
  }

/*
  private class LinkFocusListener implements FocusListener {
    public void focusGained(FocusEvent e) {
      focused=true;
      repaint();
    }

    public void focusLost(FocusEvent e) {
      focused=false;
      repaint();
    }
  }
  
  public Component getVisualComponent() 
  {
    return this.visualComponent;
  }*/
}
  
