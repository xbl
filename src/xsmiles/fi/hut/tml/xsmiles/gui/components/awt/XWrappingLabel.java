/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/*

* X-smiles project 1999

*

* $Id: XWrappingLabel.java,v 1.1 2004/03/26 10:50:06 honkkis Exp $

*

 */




package fi.hut.tml.xsmiles.gui.components.awt;
import java.awt.Canvas;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Dimension;
import java.util.Enumeration;
import java.awt.Image;
import java.util.Vector;


/**

* This class is similar to Symantec WrappingLabel class (a lot of copy-paste here).

* The class displays a long text string in several lines.

*

* @author	Aki Teppo

* @version	$Revision: 1.1 $

*/

public class XWrappingLabel extends Canvas

{
    protected Enumeration textEnumeration;
    protected Vector textVector;
    protected String text;
    transient protected FontMetrics fm;
    protected int align;
    protected int baseline;
    final static int ALIGN_LEFT =   1;
    final static int ALIGN_CENTER = 2;
    final static int ALIGN_RIGHT =  3;
            
 
    private boolean isSetSize = false;
    private int stringHeight = 0;
    private int longestStringWidth = 0;
    private int currenty = 0;
    private int boundx = 0;
    private int boundy = 0;
    private int xsize = 600;
    private int ysize = 400;
    
    /**

     * Constructs a default wrapping label. Default value is an empty string.

     */

    public XWrappingLabel()

    {
        this(new String(""));

    
		//{{REGISTER_LISTENERS
		SymComponent aSymComponent = new SymComponent();
		this.addComponentListener(aSymComponent);
		//}}
	}

    public XWrappingLabel(Enumeration e)
    {
        textEnumeration = e;
    }
    
    public XWrappingLabel(Vector v)
    {
        textVector = v;
    }

    /**

     * Constructs a wrapping label that displays the specified string.

     *

     * @param s string to be displayed in label

     */  
    public XWrappingLabel(String s)

    {
        this(s, XWrappingLabel.ALIGN_LEFT);
    }



    /**

     * Constructs wrapping label with the specified text and alignment.

     *

     * @param s the string to be displayed in label

     * @param a the alignment, one of ALIGN_LEFT, ALIGN_CENTERED, or ALIGN_RIGHT

     */

    public XWrappingLabel(String s, int a)

    {

    	setText(s);

		setAlignStyle(a);

    }



    /**

     * Sets the label text.

     *

     * @param newText the new label text

     */

    public void setText(String newText)

    {

	        text = newText;
	        repaint();

    }



    /**

     * Sets the text alignment.

     *

     * @param newAlignStyle the new alignment style, one of ALIGN_LEFT, ALIGN_CENTERED, or ALIGN_RIGHT

     */

    public void setAlignStyle(int newAlignStyle)

    {

    	if (align != newAlignStyle)

    	{

	        align = newAlignStyle;

	        repaint();

    	}

    }



    /**

     * Moves and/or resizes this component.

     * This is a standard Java AWT method which gets called to move and/or

     * resize this component. Components that are in containers with layout

     * managers should not call this method, but rely on the layout manager

     * instead.

     *

     * It is overridden here to re-wrap the text as necessary.

     *

     * @param x horizontal position in the parent's coordinate space

     * @param y vertical position in the parent's coordinate space

     * @param width the new width

     * @param height the new height

     */

	public synchronized void setBounds(int x, int y, int width, int height) {

	    super.setBounds(x, y, width, height);

	    invalidate();

	    validate();

	    repaint();

	}



    /**

     * Resizes this component.

     * This is a standard Java AWT method which gets called to

     * resize this component. Components that are in containers with layout

     * managers should not call this method, but rely on the layout manager

     * instead.

     *

     * It is overridden here to re-wrap the text as necessary.

     *

     * @param width the new width, in pixels

     * @param height the new height, in pixels

     */

	public synchronized void setSize(int width, int height) {

	    super.setSize(width, height);

	    invalidate();

	    validate();

	    repaint();

	}
    /**
    *   This is a method that calculates and sets the size of the label.
    *   It uses the fontmetrics to find the lenght of the longest string
    *   and the height of the entire text.
    *
    */

    public void createSize()
    {
    
    	if (textVector!= null)
		{
			int x, y;
			
			Dimension d;
			int len;
  
	        // Set up some class variables
		    
			fm = getToolkit().getFontMetrics(getFont());        
            stringHeight = fm.getHeight();
			// Get the maximum height and width of the current control
			d = getSize();
			boundx = d.width;
			boundy = d.height;
			// X and Y represent the coordinates of the upper left portion
			// of the next text line.
			x = 0;
			y = 0;
	
			textEnumeration = textVector.elements();
			
			while (textEnumeration.hasMoreElements()) {
                    y += stringHeight;
    			    len = fm.stringWidth((String) textEnumeration.nextElement());
    			    if (longestStringWidth < len) {
    			        longestStringWidth = len;
    			    }
			}
			
			// We're done with the font metrics...
			fm = null;
			
		    if (!isSetSize) {
		        setSize(longestStringWidth+6, y+20);
		        isSetSize = true;
		    }
		}
    }
    


	/**

	 * Paints this component using the given graphics context.

     * This is a standard Java AWT method which typically gets called

     * by the AWT to handle painting this component. It paints this component

     * using the given graphics context. The graphics context clipping region

     * is set to the bounding rectangle of this component and its [0,0]

     * coordinate is this component's top-left corner.

     *

     * @param g the graphics context used for painting

     * @see java.awt.Component#repaint

     * @see java.awt.Component#update

	 */

    public void paint(Graphics g)

    {   
        textEnumeration = textVector.elements();
        currenty = 20; //This to ensure that all of the lines are visible.
        while (textEnumeration.hasMoreElements()) {        
            g.drawString(textEnumeration.nextElement().toString(), 3, currenty);
            currenty+=stringHeight;            
        }
        currenty = 0;
    }

    
    /**

    * Calculates the needed height for current text.

    *

    * @param width the width against which the heigh must be calculated

    * @return needed height for current text

    */

    public int calculateHeight(int width)

    {

		if (text != null)

		{

			int x, y;

			int boundx;

			// , boundy;

			Dimension d;

			int fromIndex = 0;

			int pos = 0;

			int bestpos;

			String largestString;

			String s;



			// Set up some class variables

			fm = getToolkit().getFontMetrics(getFont());

			baseline = fm.getMaxAscent();



			// Get the maximum height and width of the current control

			//d = getSize();

			//boundx = d.width;

			boundx = width;

			//boundy = d.height;



			// X and Y represent the coordinates of the upper left portion

			// of the next text line.

			x = 0;

			y = 0;



			// While we haven't passed the bottom of the label and we

			// haven't run past the end of the string...

			// (y + fm.getHeight()) <= boundy && 

			while (fromIndex != -1)

			{

				// Automatically skip any spaces at the beginning of the line

				while (fromIndex < text.length() && text.charAt(fromIndex) == ' ')

				{

					++fromIndex;

					// If we hit the end of line while skipping spaces, we're done.

					if (fromIndex >= text.length()) break;

				}



				// fromIndex represents the beginning of the line

				pos = fromIndex;

				bestpos = -1;

				largestString = null;



				while (pos >= fromIndex)

				{

					pos = text.indexOf(' ', pos);



					// Couldn't find another space?

					if (pos == -1)

					{

						s = text.substring(fromIndex);

					}

					else

					{

						s = text.substring(fromIndex, pos);

					}



					// If the string fits, keep track of it.

					if (fm.stringWidth(s) < boundx)

					{

						largestString = s;

						bestpos = pos;



						// If we've hit the end of the string, use it.

						if (pos == -1) break;

					}

					else

					{

					    break;

					}



					++pos;

				}



				if (largestString == null)

				{

					// Couldn't wrap at a space, so find the largest line

					// that fits and print that.  Note that this will be

					// slightly off -- the width of a string will not necessarily

					// be the sum of the width of its characters, due to kerning.

					int totalWidth = 0;

					int oneCharWidth = 0;



					pos = fromIndex;



					while (pos < text.length())

					{

						oneCharWidth = fm.charWidth(text.charAt(pos));

						if ((totalWidth + oneCharWidth) >= boundx) break;

						totalWidth += oneCharWidth;

						++pos;

					}



					// drawAlignedString(g, text.substring(fromIndex, pos), x, y, boundx);

					fromIndex = pos;

				}

				else

				{

					// drawAlignedString(g, largestString, x, y, boundx);



					fromIndex = bestpos;

				}



				y += fm.getHeight();

			}



			// We're done with the font metrics...

			fm = null;

			return y;

		}

		else  {

		    return 0;

		}

    }        

    /**

     * This helper method draws a string aligned the requested way.

     * @param g the graphics context used for painting

     * @param s the string to draw

     * @param x the point to start drawing from, x coordinate

     * @param y the point to start drawing from, y coordinate

     * @param width the width of the area to draw in, in pixels

     */

    protected void drawAlignedString(Graphics g, String s, int x, int y, int width)

    {
        int drawx;
        int drawy;
        drawx = x;
        drawy = y + baseline;
        if (align != XWrappingLabel.ALIGN_LEFT)
        {
            int sw;
            sw = fm.stringWidth(s);
            if (align == XWrappingLabel.ALIGN_CENTER)
            {
                drawx += (width - sw) / 2;
            }
            else if (align == XWrappingLabel.ALIGN_RIGHT)
            {
                drawx = drawx + width - sw;
            }
        }
        g.drawString(s, drawx, drawy);
    }

	class SymComponent extends java.awt.event.ComponentAdapter
	{
		public void componentResized(java.awt.event.ComponentEvent event)
		{
			Object object = event.getSource();
			if (object == XWrappingLabel.this)
				XWrappingLabel_ComponentResized(event);
		}
	}

	void XWrappingLabel_ComponentResized(java.awt.event.ComponentEvent event)
	{
		// to do: code goes here.
	}
}

