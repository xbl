/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;

/**
 * @author Mikko Honkala
 */
public class SwingMenuBar implements XMenuBar
{
    JMenuBar bar;
    public SwingMenuBar()
    {
        this.init();
    }
    
    public void init()
    {
        this.bar=new JMenuBar();
    }
    
    public void addMenu(XMenu menu)
    {
        this.bar.add((JMenu)menu.getObject());
    }
    
    public void addToFrame(Frame f)
    {
    	if (f instanceof JFrame)
    		((JFrame)f).setJMenuBar(bar);
    }

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.gui.components.XMenuBar#getObject()
	 */
	public Object getObject() {
		// TODO Auto-generated method stub
		return bar;
	}
}
