/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.XCaption;


import javax.swing.JTextField;
import javax.swing.*;
import javax.swing.border.Border;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.*;

import java.util.Vector;
import java.util.Enumeration;

/**
 * A caption object
 * @author Mikko Honkala
 */
public class SwingCaption extends SwingStylableComponent
implements XCaption
{
    /** the caption component */
    protected NonFocusableLabel caption;
    protected static final Dimension labelMinSize = new Dimension(0,0);
    protected Dimension myMinSize = labelMinSize; // for captions, minumun size is 0,0
    // for outputs, the minimum size may be set to something else
    
    /** the text listeners of this caption */
    protected Vector textListeners;
    
    final static Border emptyBorder = new javax.swing.border.EmptyBorder(0,0,0,0);
    public SwingCaption(String text)
    {
        super();
        this.caption=new NonFocusableLabel(text);
        this.content=this.caption;
        this.init();
    }
    public void init()
    {
        caption.setEditable(false);
        //caption.setBackground(NORMALCOLOR);
        caption.setBorder(emptyBorder);
        super.init();
    }
    public Dimension getMinimumSize()
    {
        return myMinSize;
    }
    public void setMinimumSize(Dimension min)
    {
        this.myMinSize=min;
    }
    
    public Color getDefaultBackgroundColor()
    {
        return SwingCSSFormatter.getTransparentColor();
    }
    /*
    public void sizeComponent(double zoom, Dimension size)
    {
        if (myMinSize!=labelMinSize)
        {
            ((JComponent)this.getSizableComponent()).setMinimumSize(new Dimension(100,10));
            Log.debug("Set only minimum size: "+this.getText());
        }
        else
        {
            ((JComponent)this.getSizableComponent()).setPreferredSize(size);
            this.getSizableComponent().setSize(size);
            ((JComponent)this.getSizableComponent()).setMinimumSize(size);
            ((JComponent)this.getSizableComponent()).setMaximumSize(size);
        }
    }*/
    
    public boolean isCaption()
    {
        return true;
    }
    
    public String getText()
    {
        return this.caption.getText();
    }
    
    public void setText(String text)
    {
        this.caption.setText(text);
        if (this.textListeners!=null)
        {
            Enumeration e = textListeners.elements();
            while(e.hasMoreElements())
            {
                TextListener l = (TextListener)e.nextElement();
                l.textValueChanged(new TextEvent(text,0));
            }
        }
    }
    
  /*public Component getContentComponent()
  {
          return caption;
  }*/
    /** non - focusable JTextField to be used as a caption */
    public class NonFocusableLabel extends JTextField
    {
        public NonFocusableLabel(String text)
        {
            super(text);
        }
        public boolean isFocusTraversable()
        {
            return false;
        }
        public void paint(Graphics g)
        {
            CompatibilityFactory.getGraphicsCompatibility().setAntialias(true,g);
            super.paint(g);
        }
        
    }
    public void addTextListener(TextListener tl)
    {
        if (tl==null) return;
        if (this.textListeners==null) this.textListeners=new Vector();
        else if (this.textListeners.contains(tl)) return;
        this.textListeners.addElement(tl);
    }
    public void removeTextListener(TextListener tl)
    {
        if (tl==null||textListeners==null) return;
        this.textListeners.removeElement(tl);
    }
    
}
