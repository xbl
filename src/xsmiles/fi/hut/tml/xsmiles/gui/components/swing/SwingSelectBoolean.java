/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import javax.swing.*;

import org.w3c.dom.css.CSSStyleDeclaration;


/**
 * An implementation of XSelectBoolean = a single checkbox
 * @author Mikko Honkala
 */
public class SwingSelectBoolean extends SwingStylableComponent implements XSelectBoolean
{
    protected JCheckBox button;
    protected String name;
    protected ImageIcon icon;
    
    protected boolean layoutDone = false;
    protected boolean stylindDone = false;
    /**
     * A plain swing button
     */
    public SwingSelectBoolean()
    {
        super();
        init();
    }
    
    
    public void init()
    {
        this.content = this.createContent();
        //this.container = (Container)this.content.getAddableComponent();
    }
    /** creates the content component */
    public Component createContent()
    {
        this.button = new JCheckBox("");
        this.button.setSize(this.button.getPreferredSize());
        return button;
    }
    
    public Component getComponent()
    {
        //if (!this.layoutDone) this.layoutComponent();
        return this.getAddableComponent();
    }
    
    /**
     * the default background color for this component
     * null = transparent.
     */
    public Color getDefaultBackgroundColor()
    {
        return CompatibilityFactory.getCompatibility().createTransparentColor();
        //return Color.lightGray;
    }
    
    protected CSSStyleDeclaration cachedStyle;
    public void setStyle(CSSStyleDeclaration a_style)
    {
        // because this component swallows the caption, we dont want to style at this stage
        cachedStyle=a_style;
        this.setStyleLater(a_style);
    }
    
    public void setStyleLater(CSSStyleDeclaration a_style)
    {
    	if (a_style!=null)
		cachedStyle=a_style;
	super.setStyle(cachedStyle);    
        // because this component swallows the caption, we dont want to style at this stage
    }

    public void setCaptionText(String text)
    {
        this.setLabel(text);
    }
    public void setLabel(String t)
    {
        Log.debug("boolean: setting the label: "+t);
        button.setLabel(t);
	//this.setStyleLater(null);
    }
    
    public void addItemListener(ItemListener al)
    {
        button.addItemListener(al);
    }
    
    public void removeItemListener(ItemListener al)
    {
        button.removeItemListener(al);
    }
    
    public void setSelected(boolean selected)
    {
        button.setSelected(selected);
    }
    
    public boolean getSelected()
    {
        return button.isSelected();
    }
    
}
