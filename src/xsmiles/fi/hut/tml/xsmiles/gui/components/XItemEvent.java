package fi.hut.tml.xsmiles.gui.components;

public class XItemEvent 
{
  private Object str;
  public static final int DESELECTED=0;
  public static final int SELECTED=1;
  private int state=DESELECTED;

  public void setItem(Object s)
  {
    str=s;
  }

  public Object getItem() 
  {
    return str;
  }
  public void setStateChange(int s) {
    state=s;
  }

  public int getStateChange() {
    return state;
  }
}
