/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import java.awt.event.AdjustmentListener;
import java.util.Dictionary;

/**
 * @author Mikko Honkala
 */
public interface XRange extends XComponent {
	
	/** Horizontal orientation. */
	public static final int HORIZONTAL = 0;
	/** Vertical orientation.  */
	public static final int VERTICAL   = 1; 

	public void setValue(int n);
	public int getValue();
    
    /** give the dictionary for the labels */
    public void setLabelTable(Dictionary dictinary);
  
	/** add a listener for changes in the range control */
	public void addAdjustmentListener(AdjustmentListener l);
	/** remove a listener for changes in the range control */
	public void removeAdjustmentListener(AdjustmentListener l);
}
