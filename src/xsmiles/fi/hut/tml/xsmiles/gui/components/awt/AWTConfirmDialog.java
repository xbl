/* X-Smiles (See XSMILES_LICENCE.txt)
 */



package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.XConfirmDialog;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;


/**
 * Auth dialog
 */

public class AWTConfirmDialog extends AWTDialog implements XConfirmDialog
{
    public AWTConfirmDialog(Frame parentFrame)
    {
        super(parentFrame);
    }
    
    public void showDialog()
    {
        final Dialog jd = new Dialog(this.parent,this.title==null?"":this.title, true);
        
        
        
        jd.setResizable(false);
        jd.setLayout(new GridLayout(0, 1));
        //jd.setLayout (new FlowLayout());
        Label jl = new Label(this.prompt==null?"":this.prompt);
        jd.add(jl);
        
        
        Panel buttonp = new Panel();
        Button jb = new Button("OK");
        buttonp.add(jb);
        jb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                setCancelled(false);
                jd.dispose();
            }
        });
        Button cancelb = new Button("Cancel");
        buttonp.add(cancelb);
        cancelb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                setCancelled(true);
                jd.dispose();
            }
        });
        jd.add(buttonp);
        Dimension ss = jd.getToolkit().getScreenSize();
        Dimension ds = jd.getPreferredSize();
        
        jd.setLocation((ss.width  - ds.width) / 2,
        (ss.height - ds.height) / 2);
        
        jd.setSize(ds);
        
        
        jd.pack();
        jd.setVisible(true);
        super.showDialog();
    }
}



