/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import java.io.File;
import javax.swing.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;

/**
 * The Swing file chooser component
 * @author Mikko Honkala
 */
public class SwingFileDialog implements XFileDialog
{
    protected BrowserWindow browser;
    protected File selection;
    protected boolean fSave;
    protected String fFilename;
    protected File dir;
    
    protected JFileChooser fc;

    public SwingFileDialog(BrowserWindow w,boolean save)
    {
	browser = w;
    fSave=save;
    }
    
    public SwingFileDialog(BrowserWindow w,boolean save,String filename)
    {
        browser = w;
        fSave=save;
        fFilename=filename;
    }
	
    /** open the selection box and return a failure/ok code */
    public int select()
    {
        selection = this.showFileDialog();
        if (selection==null) return XFileDialog.SELECTION_FAILED;
        else
        {
            this.dir=selection;
            return XFileDialog.SELECTION_OK;
        }
    }
	
    /** return the file user selected */
    public File getSelectedFile()
    {
    	return selection;
    }
    
    public synchronized File showFileDialog()
    {
        // this would make the current dir the base dir for selection
        //fc=new JFileChooser(".");
        if (dir==null) fc=new JFileChooser();
        else fc=new JFileChooser(dir);
        if (fFilename!=null)
        {
            //File currDir = fc.getCurrentDirectory();
            File preselect = new File(fFilename); 
            fc.setSelectedFile(preselect);
        }
    	File file=null;
    	int returnVal=0;
    	try {
            if(fSave)
            	returnVal = fc.showSaveDialog(browser.getCurrentGUI().getWindow());
            else
            	returnVal = fc.showOpenDialog(browser.getCurrentGUI().getWindow());
    	if (returnVal == JFileChooser.APPROVE_OPTION) 
    	    file = fc.getSelectedFile();
    	} catch(Exception e) {
    		Log.error(e);
    	}
    	return file;
    }
        public void setTitle(String title)
        {
        }
    public void setPrompt(String prompt)
    {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XFileDialog#setFile(java.io.File)
     */
    public void  setFile(File f)
    {
        this.dir=f;
    }


}
