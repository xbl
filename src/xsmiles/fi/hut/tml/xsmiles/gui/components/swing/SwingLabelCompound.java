/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;


import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.BoxLayout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.*;

/**
 * A caption object
 * @author Mikko Honkala
 */
public class SwingLabelCompound implements XLabelCompound, TextListener
{
    /** the caption component */
    protected SwingCaption caption;
    
    /** the component itself */
    protected XComponent component;
    
    /** the caption-side string */
    protected String captionSide;
    
    /** the top-level container, containing the caption and the content component */
    protected Container container;
    
    /** the axis depends on the caption side CSS attribute */
    protected int axis;
    
    protected boolean layoutDone = false;
    
    protected double currentZoom = 1.0;
    
    /** Specifies that components should be laid out left to right.    */
    public static final int X_AXIS = 0;
    
    /** Specifies that components should be laid out top to bottom.   */
    public static final int Y_AXIS = 1;
    
    
    
    public SwingLabelCompound(XComponent comp,XCaption capt,  String captSide)
    {
        super();
        this.caption=(SwingCaption)capt;
        this.component=comp;
        this.captionSide=captSide;
        this.init();
    }
    public void init()
    {
        // we have few exceptions, where the caption is part of the component
        // button and selectBoolean (checkbox)
        if (this.component instanceof SwingButton)
        {
            this.container=(JComponent)this.component.getComponent();
            String labelText = this.caption.getText();
            ((SwingButton)this.component).setLabel(labelText);
            this.caption.addTextListener(this);
        }
        else if (this.component instanceof SwingSelectBoolean)
        {
            this.container=(JComponent)this.component.getComponent();
            if (this.caption!=null)
            {
                String labelText = this.caption.getText();
                ((SwingSelectBoolean)this.component).setLabel(labelText);
                this.caption.addTextListener(this);
            }
        }
        else
        {
            // layout container
            this.createContainer();
            this.layoutComponent();
        }
    }
    
    /** creates the main container */
    protected void createContainer()
    {
        this.container=new JPanel();
        ((JComponent)this.container).setOpaque(false);
    }
    
    
    /**
     * layouts the component using the caption, content and the main container parts of the control
     */
    protected void layoutComponent()
    {
        // JV: moved this to ComponentWCaption
        if (this.layoutDone)
        {
            this.container.removeAll();
        }
        float alignment= Component.LEFT_ALIGNMENT;
        boolean captionFirst=true;
        axis = BoxLayout.Y_AXIS;
        if (this.caption!=null)
        {
            // Determine which side the caption should be put, default is top
            if (captionSide!=null)
            {
                String cs=captionSide;
                if (cs.equals("left"))
                {
                    captionFirst=true;
                    axis=BoxLayout.X_AXIS;
                }
                else if (cs.equals("right"))
                {
                    captionFirst=false;
                    axis=BoxLayout.X_AXIS;
                }
                else if (cs.equals("bottom"))
                {
                    captionFirst=false;
                    axis=BoxLayout.Y_AXIS;
                }
                
            }
        }
        
        BoxLayout box = new BoxLayout(container, axis);
        this.container.setLayout(box);
        //content.getAddableComponent().setAlignmentX(alignment);
        //caption.getContentComponent().setAlignmentX(alignment);
        ((JComponent)this.container).setAlignmentX(alignment);
        
        
        if (captionFirst)
        {
            // caption-side was left or top
            if (caption!=null)container.add(caption.getAddableComponent());
            container.add(component.getComponent());
        }
        else
        {
            // caption-side was right or bottom
            container.add(component.getComponent());
            if (caption!=null)container.add(caption.getAddableComponent());
        }
        this.container.setSize(this.container.getPreferredSize());
        //Log.debug("Added "+content.getAddableComponent());
        // TODO : format the content if CSS is found
        //this.formatContent();
        //this.defaultColor=content.getAddableComponent().getBackground();
        //this.defaultFont=content.getAddableComponent().getFont();
        //this.sizeComponent(this.currentZoom);
        //this.createHint();
        this.layoutDone=true;
    }
    
    protected String calculateAxis(String cs)
    {
        //default
        String captionPlacement=BorderLayout.NORTH;
        if (cs!=null)
        {
            if (cs.equals("left"))
            {
                captionPlacement=BorderLayout.WEST;
                this.axis=X_AXIS;
            }
            else if (cs.equals("right"))
            {
                captionPlacement=BorderLayout.EAST;
                this.axis=X_AXIS;
            }
            else if (cs.equals("bottom"))
            {
                captionPlacement=BorderLayout.SOUTH;
                this.axis=Y_AXIS;
            }
            else if (cs.equals("top"))
            {
                captionPlacement=BorderLayout.NORTH;
                this.axis=Y_AXIS;
            }
        }
        return captionPlacement;
    }
    /**
     * Set's this components zoom (1.0 is the default).
     */
    public void setZoom(double zoom)
    {
        if (component!=null) this.component.setZoom(zoom);
        if (caption!=null) ((XComponent)this.caption).setZoom(zoom);
        //this.sizeComponent(zoom);
    }
    
    
    public Component getComponent()
    {
        return this.container;
    }
    public Dimension getSize()
    {
        if (this.container.getSize().height>0)
            return this.container.getSize();
        if (this.container.getPreferredSize().height>0)
            return this.container.getPreferredSize();
        return new Dimension(0,0);
    }
    
    /**
     * Set this drawing area visible.
     * @param v    true=visible, false=invisible
     */
    public void setVisible(boolean v)
    {
        this.container.setVisible(v);
    }
    
    /**
     * @param c The component to be added
     */
    public void add(XComponent c)
    {
        this.container.add(c.getComponent());
    }
    
    /**
     * @param c Remove component c
     */
    public void remove(XComponent c)
    {
        this.container.remove(c.getComponent());
    }
    
    /**
     * Remove all components from conatiner.
     */
    public void removeAll()
    {
        this.container.removeAll();
    }
    
    /**
     * whether this is layed out north-south or east-west
     */
    int getAxis()
    {
        return this.axis;
    }
    
    /**
     * Invoked when the value of the text has changed.
     * The code written for this method performs the operations
     * that need to occur when text changes.
     */
    public void textValueChanged(TextEvent e)
    {
        String newCaption = this.caption.getText();
        // we have few exceptions, where the caption is part of the component
        // button and selectBoolean (checkbox)
        if (this.component instanceof SwingButton)
        {
            ((SwingButton)this.component).setLabel(newCaption);
        }
        else if (this.component instanceof SwingSelectBoolean)
        {
            ((SwingSelectBoolean)this.component).setLabel(newCaption);
        }
        // otherwise the caption has already changed
    }
    
    
    
    
}
