/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.AWTEventMulticaster;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.XText;


/**
 * Common base class for text components
 * @author Mikko Honkala
 */

public abstract class SwingTextComponent extends SwingStylableComponent
implements XText, DocumentListener, FocusListener
{
    protected JTextComponent textcomponent;
    TextListener textlistener = null;
    protected GUI fGUI;
    
    protected boolean insideChangeEvent = false;
    
    public SwingTextComponent(GUI gui)
    {
        super();
        fGUI=gui;
    }
    
    public void init()
    {
        this.textcomponent.getDocument().addDocumentListener(this);
        this.textcomponent.addFocusListener(this);        
        super.init();
    }
    
    public void setText(String text)
    {
        if (!insideChangeEvent)
            this.textcomponent.setText(text);
    }
    public String getText()
    {
        return this.textcomponent.getText();
    }
    public void addTextListener(TextListener tl)
    {
        textlistener = AWTEventMulticaster.add(textlistener, tl);
    }
    public void removeTextListener(TextListener tl)
    {
        textlistener = AWTEventMulticaster.remove(textlistener, tl);
    }
    
    public Color getDefaultBackgroundColor()
    {
        return Color.white;
    }
    
    // DOCUMENT LISTENER METHODS
    public void changedUpdate(DocumentEvent e)
    {
        notifyIncrementalChange(e);
    }
    public void insertUpdate(DocumentEvent e)
    {
        notifyIncrementalChange(e);
    }
    public void removeUpdate(DocumentEvent e)
    {
        notifyIncrementalChange(e);
    }
    
    public void notifyIncrementalChange(DocumentEvent e)
    {
        insideChangeEvent=true;
        try
        {
            // when event occurs which causes "action" semantic
            if (textlistener != null)
            {
                TextEvent te = new TextEvent(this,TextEvent.TEXT_VALUE_CHANGED);
                textlistener.textValueChanged(te);
            }
        } catch (Exception ex)
        {
        }
        finally
        {
            insideChangeEvent=false;
        }
        
    }
    public void setEditable(boolean editable)
    {
        this.textcomponent.setEditable(editable);
    }
    public boolean getEditable()
    {
        return this.textcomponent.isEditable();
    }
    
    /**
     * @param b Active or not active
     */
    public void setEnabled(boolean b)
    {
        // we don't want to change the font color
        //this.content.getStylableComponent().setEnabled(b);
        this.setEditable(b);
    }
    
    // FOCUS LISTENER METHODS
    public void focusGained(FocusEvent ae)
    {
        if( getEditable() )
        {
                        /* This is fairly experimental; inputMode spec in XForms WD
                         * doesn't say anything about languages, just unicode scripts
                         * and character cases. As the input language is most useful
                         * in our case, however, find any xml:lang in effect for this
                         * block and assume that's also the input language. As XML
                         * spec doesn't specify or anticipate this kind of usage,
                         * prepend it to our input-mode string, giving uniform use for
                         * these modifiers.
                         * Note that this is potentially wrong: Nothing says that an
                         * english form for example can't take names etc. in other
                         * languages, and if language is later added as inputMode
                         * modifier, this code won't be entirely compatible.
                         */
            if (fGUI!=null)
            fGUI.displayKeypad(this.textcomponent, this.fInputMode);
        }
    }
    public void focusLost(FocusEvent ae)
    {
        if (fGUI!=null)
        fGUI.hideKeypad();
    }
    
    public void addFocusListener(FocusListener fl)
    {
        this.textcomponent.addFocusListener(fl);
    }
    
    public void removeFocusListener(FocusListener fl)
    {
        this.textcomponent.removeFocusListener(fl);
    }
    
    
    
}
