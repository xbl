/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;

import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;

import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    


/**
 * select one minimal mode
 * @author Mikko Honkala
 */
public class AWTSelectOneMinimal //extends AWTSelectOne 
    //implements XSelectOne
{
	
    /** the combobox */
    Choice combo;
    /**
     * @param v The vector containing all items, the first item is default
     * @param s The style of the selectOne
     */
    /*
    public AWTSelectOneMinimal() 
    {
	super();
	this.init();
    }
    public AWTStylableComponent createComponent()
    {
	combo=new Choice();
	
	//combo.addItemListener(this);
	selectComponent=combo;
	return new AWTStylableComponent(combo,this);
    }
	
	
    public Color getDefaultBackgroundColor()
    {
	//return Color.lightGray;
        return null;
    } 

    public void addSelection(Object o)
    {
	combo.add((String)o);
    }

    public void removeSelection(Object o)
    {
	combo.remove((String)o);
    }

    public void removeAll()
    {
	combo.removeAll();
    }

    public void setSelected(Object o) 
    {
	combo.select((String)o);
    }

    public void setSelectedIndex(int index) 
    {
	combo.select(index);
    }

    public int getSelectedIndex() {
	return combo.getSelectedIndex();
    }

    
    public void addItemListener(ItemListener l)
    {
	combo.addItemListener(l);
    }


    public void removeItemListener(ItemListener l)
    {
	combo.removeItemListener(l);
    }*/
}
