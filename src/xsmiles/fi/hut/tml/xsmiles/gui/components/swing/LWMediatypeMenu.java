/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;

/**
 * Class LWMediatypeMenu
 * 
 * @author tjjalava
 * @since Oct 20, 2004
 * @version $Revision: 1.1 $, $Date: 2004/10/20 09:30:20 $
 */
public class LWMediatypeMenu extends JMenu {

    private BrowserWindow browser;    

    public LWMediatypeMenu(BrowserWindow b) {
        super("Media");
        browser = b;
        
        ChangeMediaListener listener = new ChangeMediaListener();

        for (int i=0;i<MediaQueryEvaluator.MEDIA_TYPES.length;i++) {
            JMenuItem it = new JMenuItem(MediaQueryEvaluator.MEDIA_TYPES[i]);
            it.addActionListener(listener);
            add(it);
        }
    }

    private class ChangeMediaListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String media = e.getActionCommand();
            XMLConfigurer config = browser.getBrowserConfigurer();
            config.setGUIProperty(browser, "mediaType", media);
            browser.navigate(NavigationState.RELOAD);
        }
    }
}