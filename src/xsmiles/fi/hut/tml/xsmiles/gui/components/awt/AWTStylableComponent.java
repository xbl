package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.general.ComponentBase;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Font;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.FocusListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.*;
import org.w3c.dom.css.CSSStyleDeclaration;

/**
 * The baseclass for the AWT implementation of the cross library GUI controls
 * 
 * @author honkkis
 * @author juha
 */
public class AWTStylableComponent extends ComponentBase implements XComponent
{
    /** the hidden frame is used to add components briefly there in order to get correct preferred size.
     * 
     */
    protected static Window hiddenFrame;
    
    public AWTStylableComponent()
        {
            super();
        }
        public AWTStylableComponent(Component comp)
        {
            this.content=comp;
            this.init();
        }
        
        public void init()
        {
            //float alignment= Component.LEFT_ALIGNMENT;
            //((JComponent)this.getSizableComponent()).setAlignmentX(alignment);
            //content.setOpaque(false);
        }

        
        public boolean isCaption()
        {
            return false;
        }
        
        
        public void sizeComponent(double zoom, Dimension size)
        {
            //((JComponent)this.getSizableComponent()).setPreferredSize(size);
            this.getSizableComponent().setSize(size);
            //((JComponent)this.getSizableComponent()).setMinimumSize(size);
            //((JComponent)this.getSizableComponent()).setMaximumSize(size);
        }
        

        // needed to create an invisible window to add components in order to get their preferred size
        protected Container getHiddenFrame()
        {
            if (hiddenFrame==null&&AWTComponentFactory.topLevelFrame!=null)
            {    
                hiddenFrame=new Window(AWTComponentFactory.topLevelFrame);
                hiddenFrame.setSize(1,1);
                hiddenFrame.show();
                hiddenFrame.setVisible(true);
            }
            return hiddenFrame;
        }
        
        
        /**
         *  in AWT, we must do extra tricks to get the preferred size before the component is on screen
         * 
         */
        public Dimension getPreferredSize(Component comp)
        {
            Dimension dim=comp.getPreferredSize();
            if (dim.width>0||dim.height>0) return dim;
            else
            {    
	            Container hiddenFrame=this.getHiddenFrame();
	            if (hiddenFrame==null) return new Dimension(1,1);
	            boolean isVisible=comp.isVisible();
	            Container possibleParent=comp.getParent();
	            hiddenFrame.add(comp);
	            comp.setVisible(true);
	            dim = comp.getPreferredSize();
	            Dimension size = comp.getSize();
	            hiddenFrame.remove(comp);
	            if (possibleParent!=null) possibleParent.add(comp);
	            comp.setVisible(isVisible);
	            //Log.debug("Added component to hidden frame. Pref size: "+dim+" size:"+size);
	            return dim;
            }
            //return new Dimension(100,50);
        }    
        

        
        public void setBackground(Color bg)
        {
            super.setBackground(bg);
            /*
            if(bg!=null)
                if (bg!=SwingCSSFormatter.getTransparentColor())
                    ((JComponent)this.getStylableComponent()).setOpaque(true);
                    */
        }
        
        /**
         * Sets the components Tooltip (Hint) text
         */
        public void setHintText(String hint)
        {
            this.hintText=hint;
            this.createHint();
        }
        protected void createHint()
        {
            Component main = this.getAddableComponent();
            if (hintText!=null && hintText.length()>0)
            {
                //((JComponent)main).setToolTipText(hintText);
                // If the component is a container, set the tooltip for all of its children
                if (main instanceof Container)
                {
                    Container c = (Container)main;
                    //Component[] comps = c.getComponents();
                    //			Log.debug("Setting multi hint: "+h.getText());
                    //this.setHintRecursively(comps, hintText);
                }
            }
        }
        protected void setHintRecursively(Component[] comps, String hinttext)
        {
            for (int i=0; i<comps.length; i++)
            {
                Component comp = (Component)comps[i];
                /*
                if (comp instanceof Container)
                {
                    Component[] comps2 = comp.getComponents();
                    //			Log.debug("Setting multi hint: "+h.getText());
                    this.setHintRecursively(comps2, hintText);
                }*/
                //comp.setToolTipText(hintText);
            }
        }
        //protected static KeyStroke HELPKEY = KeyStroke.getKeyStroke("F1");
        
        /** add a listener for help events */
        public void addHelpListener(ActionListener fl)
        {
            //JComponent comp = (JComponent)this.getComponent();
            //comp.registerKeyboardAction(fl,"help",
             //       HELPKEY,JComponent.WHEN_FOCUSED);
        }
        
        /** remove a listener for help events */
        public void removeHelpListener(ActionListener fl)
        {
            //JComponent comp = (JComponent)this.getComponent();
            //comp.unregisterKeyboardAction(HELPKEY);
        }
        
        

}
