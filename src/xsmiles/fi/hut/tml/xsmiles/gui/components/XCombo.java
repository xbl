package fi.hut.tml.xsmiles.gui.components;

import java.awt.event.*;

/**
 * An interface that describes item selection components.
 *
 * @author Juha
 */
public interface XCombo {
  
  /**
   * @param s Add an element s to the combo selection
   */
  public void add(String s);
  
  /**
   * @param s Remove an element from the combo
   */
  public void remove(String s);

  /**
   * @param al The actionlistener to add to the combo
   */
  public void addActionListener(ActionListener al);
  
  /**
   * @param al The actionListener to remove
   */
  public void removeActionListener(ActionListener al);
  
}
