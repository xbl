package fi.hut.tml.xsmiles.gui.components;

import java.awt.*;
import java.awt.event.*;

/**
 * A generic page selector.
 *
 * @author Juha
 */
public abstract class XImageIcon extends Component 
{
  /**
   * @param s Load the image to the icon
   */
  public abstract void setIcon(String s);
}
