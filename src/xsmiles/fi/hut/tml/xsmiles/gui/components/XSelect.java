package fi.hut.tml.xsmiles.gui.components;

import java.util.Vector;

import java.awt.event.ItemListener;

/**
 * Base interface for selectOne & selectMany
 *
 * @author Mikko Honkala
 */
public interface XSelect extends XComponent 
{

  public void setSelected(Object o);
  public int getSelectedIndex();
  public void setSelectedIndex(int index);
  public void addSelection(Object o);
  public void removeSelection(Object o);
  public void removeAll();
  public void addItemListener(ItemListener il);

  public void removeItemListener(ItemListener il);


}
