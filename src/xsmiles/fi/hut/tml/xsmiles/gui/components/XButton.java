package fi.hut.tml.xsmiles.gui.components;


/**
 * A generic button
 *
 * @author Juha
 */
public interface XButton extends XComponent
{
  /**
   *?@param t Set the label of this component
   */
    public void setLabel(String t);
    
    public void setImage(String fn);
    
    public void setImagePressed(String fn);
    
    public void setImageRollOver(String fn);
    
    public void setImageDisabled(String fn);
    
    public void setActionCommand(String ac);


}
