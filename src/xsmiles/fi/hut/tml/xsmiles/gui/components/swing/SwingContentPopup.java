/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.content.UnknownContentHandler;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.event.*;
import java.net.URL;
import java.io.*;

import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import javax.swing.*;

import java.awt.datatransfer.*;

/**
 * A Menu, that can be opened when the user clicks right mouse button on a link
 * @author Mikko Honkala
 */
public class SwingContentPopup extends SwingPopup implements ActionListener
{
    
    protected JMenuItem goBackItem, goForwardItem, goReloadItem;
    protected JMenuItem viewCompleteSourceItem;
    protected JMenuItem viewXMLSourceItem;
    protected JMenuItem viewXSLSourceItem;
    protected JMenuItem reopenItem;
    
    
    /**
     * Swing link popup menu
     */
    public SwingContentPopup()
    {
        super();
    }
    
    
    
    public void show(Component comp, int x, int y, XMLDocument doc, URL link, MLFCListener window)
    {
        this.popup = new JPopupMenu();
        this.url=link;
        this.bw = window;
        this.document = doc;
        
        this.init();
        if (this.popup!=null)
            this.popup.show(comp,x,y);
    }
    
    
    public void init()
    {
        goForwardItem = createItem(Language.FORWARD,this);
        goBackItem = createItem(Language.BACK,this);
        goReloadItem = createItem(Language.RELOAD,this);
        this.addSeparator();
        viewCompleteSourceItem = createItem(Language.VIEWSOURCECOMPLETE,this);
        viewXMLSourceItem = createItem(Language.VIEWSOURCEXML,this);
        viewXSLSourceItem = createItem(Language.VIEWSOURCEXSL,this);
        this.addSeparator();
        reopenItem = createItem(Language.REOPEN,this);
        if (this.isTabbed())
        {
            this.createItem(Language.REOPEN_TAB,
            new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    bw.openInNewTab(new XLink(document.getXMLURL()),null);
                }
            }
            );
        }
    }
    public void actionPerformed(ActionEvent e)
    {
        JMenuItem source = (JMenuItem)(e.getSource());
        /*
        String s = "Action event detected."
                   + "\n"
                   + "    Event source: " + source.getText()
                   + " (an instance of " + source.getClass() + ")";
        Log.debug(s + "\n");
         */
        if (source == viewCompleteSourceItem)
        {
            bw.showSource(document,XsmilesView.SOURCE_AND_XSL_MLFC,"Current DOM : "+url);
        }
        else if (source == viewXMLSourceItem)
        {
            bw.showSource(document,XsmilesView.SOURCEMLFC,"Current DOM : "+url);
        }
        else if (source == viewXSLSourceItem)
        {
            bw.showSource(document,XsmilesView.XSL_MLFC ,"Current DOM : "+url);
        }
        else if (source == reopenItem)
        {
            bw.openLocationTop(document.getXMLURL().toString());
        }
        else if (source == goBackItem)
        {
            bw.navigate(NavigationState.BACK);
        }
        else if (source == goForwardItem)
        {
            bw.navigate(NavigationState.FORWARD);
        }
        else if (source == goReloadItem)
        {
            bw.navigate(NavigationState.RELOAD);
        }
    }
}
