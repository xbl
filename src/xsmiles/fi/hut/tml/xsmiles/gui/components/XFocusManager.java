/** X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPoint;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;

import java.awt.Container;
import java.awt.Rectangle;

/**
 * Class XFocusManager
 * 
 * @author pcesar
 * @since Oct 29, 2004
 * @version $Revision: 1.5 $, $Date: 2005/07/21 11:41:55 $
 */
public interface XFocusManager {

    /* Constants */
    public static final int NEXT_FOCUS_POINT=0;
    public static final int PREVIOUS_FOCUS_POINT=1;
    public static final int LEFT_FOCUS_POINT=2;
    public static final int RIGHT_FOCUS_POINT=3;
    public static final int UP_FOCUS_POINT=4;
    public static final int DOWN_FOCUS_POINT=5;

    public static final int WIDGETS_FOCUS_PROVIDER=6;
    public static final int DOCUMENT_FOCUS_PROVIDER=7;

    public void changeFocus(int direction);
    public void drawFocusPoint(FocusPoint fp);
    public void cleanFocusPoint(FocusPoint fp);
    public void addProvider(FocusPointsProvider fp);
    
    public void setFirstFocusPoint();

    public void addRootContainer(Container rootContainer);
    public void addContentArea(Container rootContainer);
    
    public Rectangle getCurrentFocusRectangle();
    public FocusPoint getCurrentFocusPoint();
}
