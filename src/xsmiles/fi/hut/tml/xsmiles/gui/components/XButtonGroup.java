/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 29, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.components;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;


/**
 * @author honkkis
 *
 */
public class XButtonGroup implements ItemListener
{
    Vector buttons;
    XSelectBoolean currentlySelected;
    public XButtonGroup()
    {
        
    }
    public void add(XSelectBoolean bool)
    {
        if (buttons==null) buttons=new Vector();
        if (!buttons.contains(bool))
            buttons.addElement(bool);
        this.addListener(bool);
        // TODO: not correct
        if (bool.getSelected()) 
            {
            if (currentlySelected!=null) this.currentlySelected.setSelected(false);
            this.currentlySelected=bool;
            }
    }
    
    protected void addListener(XSelectBoolean bool)
    {
        bool.addItemListener(this);
    }
    /* (non-Javadoc)
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    boolean insideChange=false;
    public void itemStateChanged(ItemEvent arg0)
    {
        if (insideChange) return;
        insideChange=true;
        if (currentlySelected==null)
        {
            // select first
            this.currentlySelected=(XSelectBoolean)buttons.elementAt(0);
            this.currentlySelected.setSelected(true);
        }
        // the source can either be the XSelectBoolean or its real AWT/Swing component
        Object source = arg0.getSource();
        // do not allow unselecting all
        if (currentlySelected==source||currentlySelected.getComponent()==source)
        {
            currentlySelected.setSelected(true);
            return;
        }
        try
        {
	        // change all other to not selected
	        if (buttons==null) return;
	        Enumeration e = buttons.elements();
	        while (e.hasMoreElements())
	        {
	            
	            XComponent comp =  (XComponent)e.nextElement();
	            if (comp!=source&&comp.getComponent()!=source)
	            {
	                if (comp instanceof XSelectBoolean) ((XSelectBoolean)comp).setSelected(false);
	            } else
	            {
	                currentlySelected=(XSelectBoolean)comp;
	            }
	        }
        } catch (Exception e)
        {
            Log.error(e);
        }
        insideChange=false;
    }
}
