/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Oct 20, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import java.awt.Component;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import fi.hut.tml.xsmiles.gui.components.XTabbedPane;


/**
 * @author honkkis
 *
 */
public class SwingTabbedPane implements XTabbedPane, ChangeListener
{

    JTabbedPane tabbedPane;
    
    protected XChangeListener changeListener;
    public SwingTabbedPane()
    {
        tabbedPane=new JTabbedPane();
        tabbedPane.addChangeListener(this);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getTabCount()
     */
    public int getTabCount()
    {
        return tabbedPane.getTabCount();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getMaxTabCount()
     */
    public int getMaxTabCount()
    {
        return 1000;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getSelectedIndex()
     */
    public int getSelectedIndex()
    {
        return tabbedPane.getSelectedIndex();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#addTab(java.lang.String, java.awt.Component)
     */
    public void addTab(String title, Component component)
    {
        this.tabbedPane.addTab(title,component);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#remove(java.awt.Component)
     */
    public void remove(Component c)
    {
        this.tabbedPane.remove(c);

    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#remove(int)
     */
    public void remove(int i)
    {
        this.tabbedPane.removeTabAt(i);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getComponent()
     */
    public Component getComponent()
    {
        // TODO Auto-generated method stub
        return this.tabbedPane;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#setChangeListener(fi.hut.tml.xsmiles.gui.components.ChangeListener)
     */
    public void setChangeListener(XChangeListener list)
    {
        this.changeListener=list;
    }
    /* (non-Javadoc)
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     */
    public void stateChanged(ChangeEvent arg0)
    {
        // comes from swing, convert to cf event
        if (this.changeListener!=null)
        {
            this.changeListener.valueChanged(false,arg0.getSource());
        }
        
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getSelectedComponent()
     */
    public Component getSelectedComponent()
    {
        // TODO Auto-generated method stub
        return this.tabbedPane.getSelectedComponent();
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#indexOfComponent(java.awt.Component)
     */
    public int indexOfComponent(Component component)
    {
        // TODO Auto-generated method stub
        return this.tabbedPane.indexOfComponent(component);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#setTitleAt(java.lang.Object, java.lang.String)
     */
    public void setTitleAt(int index, String tit)
    {
        // TODO Auto-generated method stub
        this.tabbedPane.setTitleAt(index,tit);
        
    }

}
