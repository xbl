/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;

import org.w3c.dom.css.CSSStyleDeclaration;
/**
 *  Interface to plain GUI component
 *  The GUI component package contains an abstract set of components 
 *  that can generally be used.
 * @author Mikko Honkala
 */
public interface XComponent {

  /** sets the CSS style for this component.
   * for components, which support this, all styling should be dont thru this
   */
  public void setStyle(CSSStyleDeclaration style);
  
  /** gets the css style */
  public CSSStyleDeclaration getStyle();
  /**
   * Set this drawing area visible.
   * @param v    true=visible, false=invisible
   */
  public void setVisible(boolean v);
  
  /**
   * @param b Active or not active
   */
  public void setEnabled(boolean b);
  /**
   * @return
   */
  public boolean getEnabled();


  /**
   * Set the bounds of Container
   *
   * @param x X location
   * @param y X location
   * @param width X location
   * @param height X location
   */ 
  public void setBounds(int x, int y, int width, int height);

  /**
   * @return The original coordinated, and the default size of component
   */
  public int getX();
  public int getY();
  public int getWidth();
  public int getHeight();
  

  
  /** set the hint text */
  public void setHintText(String text);
  
  
  /**
   * At the moment ALL events are delegated through a simple actionListener
   *
   * @param al Add the actionlistener
   */
  public void addActionListener(ActionListener al);

  public void removeActionListener(ActionListener al);

  public void addFocusListener(FocusListener fl);
  public void removeFocusListener(FocusListener fl);

  public void addMouseListener(MouseListener fl);
  public void removeMouseListener(MouseListener fl);
  
  /** add a listener for help events */
  public void addHelpListener(ActionListener fl);
  
  /** remove a listener for help events */
  public void removeHelpListener(ActionListener fl);

  // VisualComponentService implementation
  /**
   * Return the visual component for this extension element
   */
  public Component getComponent();
  
  /**
   * Returns the approximate size of this extension element
   */
  public Dimension getSize();
  
  /** set the zoom level (1.0 is the normal) */
  public void setZoom(double zoom);
  
  /** set the background, if null, then reset to default */
  public void setBackground(Color bg);
  public void setForeground(Color fg);  
   // In the future we could also have auralComponentService...
  
    /** set the input mode attribute (from XForms) */
  public void setInputMode(String inputmode);
  
  /** set the focus to this control */
  public void setFocus();
  
}

