/**
 * -Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * AND BLAA BLAA BLAAH. TO BE CONTINUED IN XSMILES_LICENSE LOCATED
 * IN licenses directory
 */
package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.content.UnknownContentHandler;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;
import java.awt.event.*;
import java.net.URL;
import java.io.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import javax.swing.*;

import java.awt.datatransfer.*;

/**
 * A Menu, that can be opened when the user clicks right mouse button on a link
 * @author Mikko Honkala
 */
public class SwingLinkPopup extends SwingPopup implements ActionListener
{
    
    protected JMenuItem newWindowItem,saveLinkTargetItem,copyLinkLocationItem,openInTabItem;

    /**
     * Swing link popup menu
     */
    public SwingLinkPopup()
    {
        super();
    }
    
    
    
    public void show(Component comp, int x, int y, XMLDocument doc, URL link, MLFCListener window)
    {
        this.url=link;
        this.bw = window;
        this.document = doc;
        popup = new JPopupMenu();
        
        this.init();
        if (this.popup!=null)
            this.popup.show(comp,x,y);
    }
    
    public void init()
    {
        newWindowItem = createItem(Language.LINK_OPEN_WINDOW,this);
        if (this.isTabbed())
        {
            openInTabItem = createItem(Language.LINK_OPEN_TAB,this);
            this.addSeparator();
        }
        saveLinkTargetItem = createItem(Language.LINK_SAVE_TARGET,this);
        copyLinkLocationItem = createItem(Language.LINK_COPY_LOCATION,this);
    }
    
        public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        /*
        String s = "Action event detected."
                   + "\n"
                   + "    Event source: " + source.getText()
                   + " (an instance of " + source.getClass() + ")";
        Log.debug(s + "\n");
         */
        if (source==null)
        {
            return;
        }
        else if (source == newWindowItem)
        {
            bw.openLocationTop(url.toString());
        }
        else if (source == saveLinkTargetItem)
        {
            try
            {
                //public static void saveContentFromURL(InputStream stream, XLink url, ComponentFactory factory)
                InputStream stream = HTTP.get(url, null).getInputStream();
                UnknownContentHandler.saveContentFromURL(stream, url, this.bw.getComponentFactory());
            } catch (Exception ex)
            {
                Log.error(ex);
            }
            
        }
        else if (source == copyLinkLocationItem)
        {
            this.copyToClipboard(url.toString());
        }
        else if (source == openInTabItem)
        {
            bw.openInNewTab(new XLink(url),null);
        }
    }
    public void copyToClipboard(String st) {
    //Gets tools from system.
    Toolkit tools = Toolkit.getDefaultToolkit();
    Clipboard clip = tools.getSystemClipboard();
    //This line depends on which classes implemented what
    StringSelection selection = new StringSelection(st);
    clip.setContents(selection,selection);
    }        
        
}
