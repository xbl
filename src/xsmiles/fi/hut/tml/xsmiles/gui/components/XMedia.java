/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

/**
 *  Interface to a media component
 */
public interface XMedia extends XComponent 
{
  /**
   * Checks if this media is static or continuous.
   * @return true if media is static.
   */
  public boolean isStatic();
  
  /**
   * @param s Set the description of the media (This can be e.g., the ALT tag)
   */ 
  public void setDescription(String s);

  /**
   * Sets the URL of the media.
   */
  public void setURL(String url);

  /**
   * Force this media to use this media type
   */     
  public void setMIMEType(String mimeType);
  public void prefetch();
  public void play();
  public void pause();
  public void stop();
  public void freeze();
  public void close();
}

