/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.Log;
//import fi.hut.tml.xsmiles.gui.components.swing.*;

import java.awt.*;
import java.lang.Math;

/**
 * A component for displaying a the nice animation that indicates document
 * being loaded.
 * 
 * @author Juha
 */
public class Animation extends DoubleBufferedContainer implements Runnable {
    private int delay, width, height;
    private boolean play;
    private int frame;
    private Thread animator;
  private boolean resizable=false;
    
    /**
     * The old constructor. Animate until stopped
     *
     * @param width in pixels
     *?@param height in pixels
     * @param delay betwean randomization
     */
    public Animation(int width, int height, int delay) {
	this.delay=delay;
	init(width,height);
    }

  public Animation(int delay) 
  {
    	this.delay=delay;
	play=false;
	resizable=true;
  }

   /**
    * Animate with default delay.
    *
    * @param width in pixels
    *?@param height in pixels
    * @param delay betwean randomization
    */
    public Animation(int width, int height) {
	this.delay=700;
	init(width,height);
    }
    
    /**
     * Init
     *
     */
    private void init(int width, int height) {
	this.width=width;
	this.height=height;
	setSize(width, height);
	play=false;
    }
    
    /**
     * Override a method needed to be overrided by LW components 
     * @return Dimension the dimension of compoentn
     */
  public Dimension getPreferredSize() {
    if(!resizable) 
      return new Dimension(width, height);
    else
      return super.getPreferredSize();
  }
  
    /**
     * Don't now if this is needed
     */
    public boolean isLightWeight() {
	return true;
    }
    
    /**
     * No focus possible
     * @return false 
     */
    public boolean isFocusTraversable() {
	return false;
    }
    
    /**
     * Repaint component on a regular basis, until stopped
     */
    public void run() {
	while(play==true) {
	  if(resizable) {
	    //width=(int)getSize().getWidth();
	    //height=(int)getSize().getHeight();
	    width=(int)getSize().width;
	    height=(int)getSize().height;
	  }
	    repaint();
	    try{Thread.sleep(delay);}catch(Exception e) {	
	    }
	}
        repaint();
    }
    
    /**
     * Stop Animation
     * 
     */
    public synchronized void pause() {
	  if(resizable) {
	    width=(int)getSize().width;
	    height=(int)getSize().height;
	  }
	play=false;
    }
    
    /**
     * Stop Animation
     * 
     */
    public synchronized void play() {
	if (play==false) {
	    play=true;
	    animator=new Thread(this);    
	    animator.start();
	}
    }
    
    /**
     *  Do some fancy animation thingies..
     *
     */
    public void paint(Graphics g) {
	int rowspan=(int)height/3;
	int colspan=(int)width/3;
	if (play) {
	    //super.paint(g);
	    g.setColor(Color.white);
	    g.fillRect(0,0,width, height);
	    g.setColor(Color.black);
	    
	    for(int i=0;i<3;i++) 
		for(int j=0;j<3;j++) {
		    //int y1=gen.nextInt(3);
		    int y1 = this.getRandomInteger(3);
		    if(y1==j)
			g.fillRect(i*colspan,j*rowspan,colspan, rowspan); 
		}
	} else {
	    g.setColor(Color.white);
	    g.fillRect(0,0,width,height);
	    g.setColor(Color.black);
	    g.fillRect(colspan,rowspan,colspan,rowspan);
	}
	g.setColor(Color.black);
	g.drawRect(0,0,width-1,height-1);
    }
    
	private int getRandomInteger(int range) 
    {
	int i = (int)(range * Math.random());
	return i;
    }
    
}
