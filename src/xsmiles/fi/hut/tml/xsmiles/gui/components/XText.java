/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

import java.awt.event.*;

/**
 * textarea 
 */
public interface XText extends XComponent {
  
  /**
   * @return Return the password currently typed in.
   */
  public String getText();
  
  /** 
   * @param s The text is set to s
   */ 
  public void setText(String s);
  /**
   *  Adds the specified text event listener to receive text events from 
   * this text component. If l is null, no exception is thrown and no action 
   * is performed.
   */
  public void addTextListener(TextListener tl);
  public void removeTextListener(TextListener tl);
  
  public void setEditable(boolean editable);
  public boolean getEditable();
  

}
