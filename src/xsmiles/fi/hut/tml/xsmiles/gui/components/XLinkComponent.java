package fi.hut.tml.xsmiles.gui.components;

import java.awt.Component;
import java.awt.event.*;

public interface XLinkComponent extends XComponent
{
  /**
   * @return The destination where the link points to
   */
  public String getDestination();

  /**
   * @param d Set the destination
   */
  public void setDestination(String d);

  /**
   * @param a Is the link active or not
   */
  public void setActive(boolean a);

  public void addClickedActionListener(ActionListener al);

  /**
   * @param l Receive event when link has pointer focus or when it is lost
   */
  public void addHoverListener(XHoverListener l);

  /**
   * A kludge for now. Once we fully apply ComponentFactory, there will be no
   * getVisualComponent. 
   */
  //public Component getVisualComponent();
}
