/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components;

/**
 * textarea 
 */
public interface XInput extends XText {
      public final static String ENTER_STROKED = "enter_stroked";
      public void removeBorders();

}
