/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.focusmanager;

import java.awt.Rectangle;
import java.util.Vector;
import java.awt.Point;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;

import fi.hut.tml.xsmiles.dom.VisualElementImpl;

/**
 * 
 * This is a simple class to define Focus Points
 * @author Pablo Cesar, Mikko Honkala
 * 
 *  
 */
public class FocusPoint extends Vector{
    
    public Component component=null;
    public Rectangle rect;
    public VisualElementImpl element=null;
  
    public FocusPoint(){
	super(5);
    }
    
    public FocusPoint(Point p, Dimension dim){
	this();
	rect = new Rectangle(p, dim);
	this.add(rect);
    }

    public FocusPoint(int x, int y, int width, int height){
	this();
	rect = new Rectangle(x,y,width, height);
	this.add(rect);
    }
    
    public FocusPoint(Rectangle[] listRects){
	this();
	for (int i=0; i<listRects.length; i++){
	    this.add(listRects[i]);
	}
    }

    public FocusPoint(Point p, Dimension dim, Component comp){
	this(p,dim);
	this.component=comp;
    }

    public FocusPoint(int x, int y, int width, int height, Component comp){
	this(x,y,width,height);
	this.component = comp;
    }
    
    public FocusPoint(Rectangle[] listRects, Component comp){
	this(listRects);
	this.component = comp;
    }
    
    public FocusPoint(Rectangle[] listRects,  VisualElementImpl e){
	this(listRects);
	this.element = e;
    }


    public VisualElementImpl getVisualElement(){
	return element;
    }

    public void addVisualElement(VisualElementImpl e){
	this.element = e;
    }

    public Component getComponent(){
	return component;
    }

    public void addComponent(Component component){
	this.component = component;
    }
    
    public void addRectangle(Rectangle rectangle){
	this.add(rectangle);
    }

    public void addRectangles(Rectangle[] rectangles){
	for (int i=0;i<rectangles.length;i++){	    
	    this.addRectangle(rectangles[i]);
	}
    }
    
    public void removeRectangle(Rectangle rectangle){
	this.removeElementAt(this.indexOf(rectangle));
    }

    public void removeRectangleAt(int index){
	this.removeElementAt(index);
    }
    
    public Rectangle[] getRectangles(){
	int i,numRectangles;	
	numRectangles = getNumRectangles();
	Rectangle[] recs = new Rectangle[numRectangles];
	for (i=0;i<numRectangles;i++){	    
	    recs[i] = this.getRectangleAt(i);
	}
	return recs;
    }

    public Rectangle getRectangleAt(int i){
	return ((Rectangle)this.elementAt(i));
    }
	
    public int getNumRectangles(){
	return this.size();
    }

    public void translate(Point value){
	for (int i=0;i<getNumRectangles();i++){
	    ((Rectangle)this.getRectangleAt(i)).translate(value.x, value.y);
	}
    }

    public void move(Point value){
	for (int i=0;i<getNumRectangles();i++){
	    ((Rectangle)this.getRectangleAt(i)).move(value.x, value.y);
	}
    }

    public String toString(){
	String output= "[ ";
	if (component != null)
	    output = component.toString();
	for (int i=0;i<getNumRectangles();i++){
	    output = output + ((Rectangle)this.getRectangleAt(i)).toString();
	}
	output = output + " ]";
	return output;
    }  
}
