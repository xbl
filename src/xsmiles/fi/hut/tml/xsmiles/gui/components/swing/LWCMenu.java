package fi.hut.tml.xsmiles.gui.components.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;
import java.io.*;
import java.net.URL;
import java.util.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;

/**
 * A Lightweight menubar. Specific contruction for this GUI
 * NOTE: Needs a lot more switching
 * Includes menuitems and listening.
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class LWCMenu extends JMenuBar {

    JMenu fileMenu, editMenu, viewMenu, helpMenu;
    BrowserWindow browser;
    LWGuiMenu guiMenu;
    GUI gui;
    JMenu mlfcMenu;
    LWThemeMenu themeMenu;
    LWStylesheetMenu styleMenu;

    public LWCMenu(BrowserWindow b, GUI g) {
        super();
        init(b,g,null);
    }

    public LWCMenu(BrowserWindow b, GUI g, JMenu m) {
        super();
        init(b,g,m);

    }

    private void init(BrowserWindow b, GUI g, JMenu m) 
    {
        browser=b;
        gui=g;
        mlfcMenu=m;
	
        fileMenu = new JMenu(Language.FILE);
        editMenu = new JMenu(Language.EDIT);
        styleMenu = new LWStylesheetMenu(browser);
	
        JMenuItem neww = new JMenuItem(Language.NEW, KeyEvent.VK_N);
        JMenuItem open = new JMenuItem(Language.OPEN, KeyEvent.VK_O);
        JMenuItem save = new JMenuItem(Language.SAVE, KeyEvent.VK_S);
        JMenuItem close = new JMenuItem(Language.CLOSE, KeyEvent.VK_W);
        JMenuItem exit = new JMenuItem(Language.EXIT, KeyEvent.VK_Q);
        JMenuItem instr = new JMenuItem("Instructions");
        JMenuItem about = new JMenuItem(Language.ABOUT);

        JMenuItem options = new JMenuItem(Language.CONFIGURATION, KeyEvent.VK_A);
        JMenuItem log = new JMenuItem(Language.LOG, KeyEvent.VK_L);

        themeMenu=new LWThemeMenu(browser, gui);
	
        guiMenu=new LWGuiMenu(browser);
	
        fileMenu.add(neww);
        fileMenu.add(open);
        fileMenu.add(save);
        fileMenu.addSeparator();
        fileMenu.add(close);
        fileMenu.add(exit);

	
        if(browser.getGUIManager().getThemeNames()!=null)
            editMenu.add(themeMenu);
        editMenu.add(guiMenu);
        editMenu.add(styleMenu);
        editMenu.addSeparator();
        editMenu.add(about);
        editMenu.add(log);
        editMenu.add(options);
	
        ActionListener al = new XAMenuListener(browser);
        open.addActionListener(al);
        neww.addActionListener(al);
        close.addActionListener(al);
        save.addActionListener(al);
        exit.addActionListener(al);
        instr.addActionListener(al);
        about.addActionListener(al);
        options.addActionListener(al);
        log.addActionListener(al);

        add(fileMenu);
        add(editMenu);
        if(mlfcMenu!=null)
            add(mlfcMenu);
    }
    
    public void setStylesheetMenu() 
    {
        styleMenu.setStylesheets();
    }
}
