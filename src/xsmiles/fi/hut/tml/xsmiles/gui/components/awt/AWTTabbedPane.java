/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Oct 19, 2004
 *
 */
package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Component;
import java.awt.Container;

import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import fi.hut.tml.xsmiles.gui.components.XTabbedPane;


/**
 * @author honkkis
 *
 */
public class AWTTabbedPane implements XTabbedPane
{
    protected Container component;
    protected int tabCount=0, maxTabCount=1;
    
    // should be a vector
    
    protected Component tab;
    
    public AWTTabbedPane()
    {
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getTabCount()
     */
    public int getTabCount()
    {
        // TODO Auto-generated method stub
        return tabCount;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getMaxTabCount()
     */
    public int getMaxTabCount()
    {
        // TODO Auto-generated method stub
        return maxTabCount;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getSelectedIndex()
     */
    public int getSelectedIndex()
    {
        // TODO Auto-generated method stub
        return Math.min(1,maxTabCount);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#addTab(java.lang.String, java.awt.Component)
     */
    public void addTab(String title, Component component)
    {
        if (this.tabCount<this.maxTabCount)
        {
	        // TODO Auto-generated method stub
	        this.tab=component;
	        // TODO: store also the title
	        this.tabCount++;
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#remove(java.awt.Component)
     */
    public void remove(Component c)
    {
        // TODO Auto-generated method stub
        if (this.tabCount>0)
        {
            this.component.remove(c);
            this.tab=null;
            this.tabCount--;
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#remove(int)
     */
    public void remove(int i)
    {
        // TODO Auto-generated method stub
        this.remove(tab);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getComponent()
     */
    public Component getComponent()
    {
        // TODO Auto-generated method stub
        return this.tab;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#setChangeListener(fi.hut.tml.xsmiles.gui.components.ChangeListener)
     */
    public void setChangeListener(XChangeListener list)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#getSelectedComponent()
     */
    public Component getSelectedComponent()
    {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#indexOfComponent(java.awt.Component)
     */
    public int indexOfComponent(Component component)
    {
        // TODO Auto-generated method stub
        return 0;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XTabbedPane#setTitleAt(int, java.lang.String)
     */
    public void setTitleAt(int index, String tit)
    {
        // TODO Auto-generated method stub
        
    }

}
