/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.Component;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XContainer;

/**
 * The only addition is add and remove Utilize the ComponentWithContainer, which already is a container
 */
public class AWTContainer extends AWTComponent implements XContainer {

    private boolean actionable = false;
    private Vector listeners, flisteners;

    public AWTContainer() {
        listeners = new Vector();
        flisteners = new Vector();
    }

    /**
     * By default components are inactive
     * 
     * @return Is can compnent be active?
     */
    public boolean getActionable() {
        return actionable;
    }

    /**
     * @param a The value of actionable
     */
    public void setActionable(boolean a) {
        actionable = a;
    }

    /**
     * @param c Remove component c
     */
    public void remove(XComponent c) {
        this.container.remove(c.getComponent());
    }

    /**
     * Remove all components from conatiner.
     */
    public void removeAll() {
        this.container.removeAll();
    }

    public void add(XComponent comp) {
        this.container.add(comp.getComponent());
    }

    /**
     * Returns the approximate size of this extension element
     */
    public void setZoom(double zoom) {
        Log.debug("Zoom not set yet");
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XContainer#add(java.awt.Component)
     */
    public void add(Component c)
    {
       this.container.add(c);
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XContainer#remove(java.awt.Component)
     */
    public void remove(Component c)
    {
        this.container.remove(c);
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.components.XComponent#getEnabled()
     */
    public boolean getEnabled()
    {
        // TODO Auto-generated method stub
        return this.container.isEnabled();
    }     
}

