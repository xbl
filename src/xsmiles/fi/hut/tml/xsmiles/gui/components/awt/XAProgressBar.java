package fi.hut.tml.xsmiles.gui.components.awt;

import java.awt.*;
import java.awt.event.*;

/**
 * A simple black progress bar, with invisible background
 * 
 * @author juha
 */
public class XAProgressBar extends Component 
{
  private double max=1.0;
  private double currentVal=0.0;

  /**
   * @param maxValue is the maximum value, which indicates that progress is complete
   */
  public XAProgressBar() 
  {

  }

  /**
   * @param i Increment progress by i
   */
  public void increment() 
  {
    currentVal=(max-currentVal)/4 + currentVal;
    repaint();
  }

  /**
   * Paint overridden, so that we can do our thing
   */
  public void paint(Graphics g) 
  {
    //super.paint(g);
    int width=this.getSize().width;
    int height=this.getSize().height;
    double ratio=currentVal/max;
    Float t=new Float(Math.floor(width*ratio));
    int blackWidth=t.intValue();
    
    if(blackWidth>width) 
      blackWidth=width;
    float r=0f;
    float gg=r;
    float b=r;
    float a=0.7f;
    //g.setColor(new Color(r,gg,b,a));
    g.setColor(new Color(r,gg,b));
    g.fillRect(0,0,blackWidth, height);
  }
}
