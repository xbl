/* X-Smiles (See XSMILES_LICENCE.txt)
 */



package fi.hut.tml.xsmiles.gui.components.awt;

import fi.hut.tml.xsmiles.gui.components.XDialog;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
    

/**
 * Auth dialog 
 */

  public class AWTDialog implements XDialog 
  {
  protected Frame parent;
  protected String prompt,title;
  public AWTDialog(Frame parentFrame)
  {
      this.parent = parentFrame;
    //super(title);
    /*
    addWindowListener
      (new WindowAdapter() {
	  public void windowClosing(WindowEvent e) {
	  }
	}  );*/
     
  }      
  
     protected void showDialog() {
     }
     protected boolean cancelled=false;
     protected void setCancelled(boolean value)
     {
         this.cancelled=value;
     }
    public void setTitle(String titleStr)
    {
        this.title=titleStr;
    }
    public void setPrompt(String promptStr)
    {
        this.prompt=promptStr;
    }
	public int select()
    {
        this.showDialog();
        return (this.cancelled==true?XDialog.SELECTION_FAILED:XDialog.SELECTION_OK);
    }
  }



