package fi.hut.tml.xsmiles.gui.components;

public interface XItemListener 
{
  public void itemStateChanged(XItemEvent e);
}
