/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.components.swing;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.*;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Container;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.ItemSelectable;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.ButtonGroup;
import javax.swing.text.JTextComponent;
import java.util.Enumeration;
import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import org.w3c.dom.css.*;    


/**
 * Select Many compact mode
 * @author Mikko Honkala
 */
public class SwingSelectManyCompact extends SwingSelectOneCompact 
	implements XSelectMany
{
  public SwingSelectManyCompact() 
  {
	this.init();
  }
  
	protected int getListSelectionModel()
	{
		return ListSelectionModel.MULTIPLE_INTERVAL_SELECTION ;
	}


	/**
	 * @param o The object to set
	 * @param b The value to set it as
	 */
	public void setSelected(Object o, boolean b) {
		  list.setSelectedValue(o,true);
	}
	
	public void clearSelection() {
		list.clearSelection();
	}
	
	public int[] getSelectedIndices() {
		return list.getSelectedIndices();
	}
	
	public void setSelectedIndices(int[] indices) {
		list.setSelectedIndices(indices);
	}
	
}
