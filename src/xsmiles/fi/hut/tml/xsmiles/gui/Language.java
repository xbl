package fi.hut.tml.xsmiles.gui;

/**
 * A Place for consistent terminology: Language.java
 * 
 * - Possibility to change terminology in one place 
 * - Multilingua
 * - Multi-everything
 * 
 * What more could you ask for?
 * 
 * In the future translate this somehow into a XML Configuration file
 * 
 * @author Juha
 * @version 0
 */
public class Language 
{

  // Menu labels
  public static String FILE="File";
  public static String EDIT="Edit";
  public static String TOOLS="Tools";
  public static String VIEW="View";
  public static String SYSTEM="System";
  public static String HELP="Help";
  public static String STYLESHEET="Change Stylesheet";
  public static String BOOKMARKS="Bookmarks";
		
	
  // Menu items:

  // File menu
  public static String NEW="New";
  public static String OPEN="Open";
  public static String SAVE="Save";
  public static String CLOSE="Close";
  public static String EXIT="Exit";
    
  // View menu
  public static String DOCUMENT="Document";
  public static String TREE="Tree : Current DOM";
  public static String SOURCEXMLXSL="Complete source : Current DOM";
  public static String SOURCEXML="XML Source";
  public static String SOURCEXSL="XSL Source";
  public static String CHANGEGUI="Change GUI";
  public static String CHANGESKIN="Change skin";
  public static String LOG="Log";
  public static String CONFIGURATION="Configuration";

  // Help Menu
  public static String ABOUT="About";


  // XSLFOMLFC terminology
  public static String FO_PAGE="Page";
  public static String FO_OF="of";

  // Browser States  
  public static String INITIALIZINGBROWSER = "Initializing browser...";
  public static String RETRIEVINGDOCUMENT="Retrieving document:";
  public static String RETRIEVINGCOMPONENT="Retrieving component...";
  public static String INITIALIZINGCOMPONENT="Initializing component...";
  public static String RETRIEVINGCONTENTDATA="Retrieving content data...";
  public static String DISPLAYINGCONTENT="Displaying document...";
  public static String STOPPED="Stopped.";
  public static String READY="Ready.";
  public static String ERROROCCURED="Error occured: ";
  public static String RETRIEVINGERRORDOCUMENT="Error occured: ";
  public static String RETRIEVINGSECONDARYDOCUMENT="Retrieving secondary document: ";
  public static String SHUTTINGDOWN="Browser shutting down...";
  public static String INITIALIZING_MLFC="Initializing document";
  public static String ACTIVATING_MLFC="Activating document";
  
  // Context menus
  
  // Link menu right click popup
  
  public static String LINK_OPEN_TAB="Open in new tab";
  public static String LINK_OPEN_WINDOW="Open in new window";
  public static String LINK_SAVE_TARGET="Save link target as...";
  public static String LINK_COPY_LOCATION="Copy link location";
  
  // Content right click popup
  
    public static String VIEWSOURCECOMPLETE="View complete source";
  public static String VIEWSOURCEXML="View XML Source";
  public static String VIEWSOURCEXSL="View XSL Source";
  public static String RELOAD="Reload document";
  public static String BACK="Back";
  public static String FORWARD="Forward";
  public static String REOPEN="Reopen in new window";
  public static String REOPEN_TAB="Reopen in new tab";

}
