/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.gui.console;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.swing.XAFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Console extends XAFrame implements ActionListener
{
  static private Console dw;

  private JTextArea ta;
  private JTextField text;
  private JButton clear, freeze, find ,reset;
  private int pos=0;
  private int rowcount=0;
  private JScrollPane scrollPane;
  private int maxchars=200000;
  private final int testratio=100;
  private int test=maxchars/testratio;
  
  static private PrintStream out, err;
  static private Object hookLock = new Object();
  

  public Console() {
    this( 800, 200 );
    Dimension ss = getToolkit().getScreenSize();
    
    setLocation(0,(ss.height - 200));
    this.setTitle("X-Smiles Log");
  }

  public Console( int width, int height ) 
  {
    // Lay the GUI out
	dw=this;
	this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	ta = new JTextArea();
	ta.setFont(new Font("Monospaced",0,12));
	ta.setEditable(false);
 	scrollPane=new JScrollPane(ta);
	this.getContentPane().setLayout( new BorderLayout() );
	this.getContentPane().add(scrollPane, BorderLayout.CENTER);

	clear = new JButton("Clear");
	freeze = new JButton("Freeze");
	find = new JButton("Find");
	find.setActionCommand("Find");
	reset = new JButton("Reset Find");
	reset.setActionCommand("Reset Find");
	text=new JTextField(30);

	clear.setActionCommand("Clear");
	freeze.setActionCommand("Freeze");
	
	
	//ButtonListener bl=new ButtonListener();

	clear.addActionListener(this);
	freeze.addActionListener(this);
	find.addActionListener(this);
	reset.addActionListener(this);

	JPanel p = new JPanel();
	p.add(clear);
	// p.add(freeze);
	p.add(text);
	p.add(find);
	p.add(reset);

	this.getContentPane().add(p, BorderLayout.SOUTH);
	setSize( width, height );
	setVisible( true );
	
	this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                close();
            }
        });

    // Install a listener for implementing utility keys
//    ta.addKeyListener( new KeyListener() {
//      public void keyPressed( KeyEvent ke ) {
//        //Console.println( "Got key "+ke.getKeyChar() );
//        switch( ke.getKeyChar() ) {
//          case 'd':
//            StateDump.dump();
//            break;
//          case 'c':
//            ta.setText( "" );
//            break;
//        }
//        ke.consume();
//      }
//      public void keyReleased( KeyEvent ke ) {
//      }
//      public void keyTyped( KeyEvent ke ) {
//      }
//    } );
  }
  static public boolean isOpen()
  {
  	return (dw!=null);
  }
  static public void print( String s ) {
    dw.showString(s);
  }
  static public void println( String s ) {
    if (s.length()==0) dw.showString("XXX\n");
    else dw.showString(s+'\n');
  }
  public void setMaximumChars(int chars)
  {
  	this.maxchars=chars;
	this.test=maxchars/testratio;
  }
  
  private int count=0;
  private void checksize()
  {
	int curlen=ta.getDocument().getLength();
	  if (curlen>maxchars) 
	  {
	  	ta.replaceRange("",0,curlen-maxchars);
	  }
  }
  
  private void showString( String s ) 
  {
    try {
      count+=s.length();
    // with jdk 1.4 there is problems with running UI stuff in other threads than the swing ui thread
    // so now the scrollpane is added in the swing thread with invokeLater
      // read more from: http://java.sun.com/products/jfc/tsc/articles/threads/threads1.html
      final String copy=s;
      Runnable showPane = new Runnable() {
        public void run() {
	  ta.append(copy);
        }
	};
      javax.swing.SwingUtilities.invokeLater(showPane);
      
      // Let's check the size of the console after 300 chars.
      if (count>test)
	{
	  
	  checksize();
	  //JScrollBar bar=scrollPane.getVerticalScrollBar();
	  //bar.setValue(bar.getMaximum());
	  count=0;
	}
      if(freeze.getText().equals("Freeze"))
	ta.setCaretPosition(ta.getDocument().getLength());
    }
    catch(Exception e) {
    }
  }

  private void closeit() {
    unhookStandards();
	  Log.reHookStandards();
	dw=null;
//    setVisible( false );
  }

  static private void close() {
    if (dw!=null) {
      dw.closeit();
      dw = null;
    }
  }

  static public void closeWindow() {
	  unhookStandards();
	  Log.reHookStandards();
	  dw.dispose();
	  dw=null;
  }

  
  static public void hookStandards() {
    synchronized( hookLock ) {
      if (out!=null)
        return;

      out = System.out;
      err = System.err;
      PrintStream dwout = new PrintStream( new ConsoleOutputStream() );
      System.setOut( dwout );
      System.setErr( dwout );
    }
  }

  static public void unhookStandards() {
    synchronized( hookLock ) {
      if (out == null)
        return;

      System.setOut( out );
      System.setErr( err );
      out = null;
      err = null;
    }
  }
  
  public void actionPerformed(ActionEvent e) 
  {
    String cmd=e.getActionCommand();
    if(cmd.equals("Clear")) {
      ta.setText("");
    } else if(cmd.equals("Freeze")) {
      //ta.setAutoscrolls(false);
      freeze.setActionCommand("Continue");
      freeze.setText("Contunue");
    } else if(cmd.equals("Continue")) {
      //ta.setAutoscrolls(true);
      freeze.setActionCommand("Freeze");
      freeze.setText("Freeze");
    } else if(cmd.equals("Find"))  {
      //Log.debug("FIND");
      String finda=text.getText();
      int p=ta.getText().indexOf(finda, pos);
      pos=p+1;
      if(p==-1) {
	ta.setCaretPosition(ta.getDocument().getLength());
	return;
      } else {
	//System.out.println("selecting: " + finda  + " "  + pos + " " + (pos + finda.length()));
	ta.requestFocus();
	ta.setCaretPosition(pos);
	ta.select(pos-1,pos + finda.length()-1);
      }
    } else if(cmd.equals("Reset Find")) {
      ta.requestFocus();
      pos=0;
      ta.setCaretPosition(pos);
    }
  }
}
