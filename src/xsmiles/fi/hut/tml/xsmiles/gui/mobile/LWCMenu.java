/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.mobile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;
import java.io.*;
import java.net.URL;
import java.util.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.gui.components.swing.LWGuiMenu;
import fi.hut.tml.xsmiles.gui.components.swing.LWStylesheetMenu;
import fi.hut.tml.xsmiles.gui.components.swing.LWThemeMenu;
import fi.hut.tml.xsmiles.gui.components.swing.XAMenuListener;

/**
 * A Lightweight menubar. Specific contruction for this GUI
 * NOTE: Needs a lot more switching
 * Includes menuitems and listening.
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class LWCMenu extends JMenuBar {

    JMenu fileMenu, editMenu, viewMenu, helpMenu;
    BrowserWindow browser;
    LWGuiMenu guiMenu;
    GUI gui;
    JMenu mlfcMenu;
    LWThemeMenu themeMenu;
    LWStylesheetMenu styleMenu;
    

    public LWCMenu(BrowserWindow b, GUI g, JMenu m) {
	super();
	browser=b;
	gui=g;
	mlfcMenu=m;
	
	fileMenu = new JMenu(Language.FILE);
	editMenu = new JMenu(Language.EDIT);
	styleMenu = new LWStylesheetMenu(browser);
	
	JMenuItem neww = new JMenuItem(Language.NEW, KeyEvent.VK_N);
	JMenuItem open = new JMenuItem(Language.OPEN, KeyEvent.VK_O);
	JMenuItem save = new JMenuItem(Language.SAVE, KeyEvent.VK_S);
	JMenuItem close = new JMenuItem(Language.CLOSE, KeyEvent.VK_W);
	JMenuItem exit = new JMenuItem(Language.EXIT, KeyEvent.VK_Q);
	JMenuItem instr = new JMenuItem("Instructions");
	JMenuItem about = new JMenuItem(Language.ABOUT);

	JMenuItem options = new JMenuItem(Language.CONFIGURATION, KeyEvent.VK_A);
	JMenuItem log = new JMenuItem(Language.LOG, KeyEvent.VK_L);

	themeMenu=new LWThemeMenu(browser, gui);
	
	guiMenu=new LWGuiMenu(browser);
	
	fileMenu.add(neww);
	fileMenu.add(open);
	fileMenu.add(save);
	fileMenu.addSeparator();
	fileMenu.add(close);
	fileMenu.add(exit);

	
	if(browser.getGUIManager().getThemeNames()!=null)
	  editMenu.add(themeMenu);
	editMenu.add(guiMenu);
	editMenu.add(styleMenu);
	editMenu.addSeparator();
	editMenu.add(about);
	editMenu.add(log);
	editMenu.add(options);
	
	ActionListener al = new XAMenuListener(browser);
	open.addActionListener(al);
	neww.addActionListener(al);
	close.addActionListener(al);
	save.addActionListener(al);
	exit.addActionListener(al);
	instr.addActionListener(al);
	about.addActionListener(al);
	options.addActionListener(al);
	log.addActionListener(al);

	add(fileMenu);
	add(editMenu);
	add(mlfcMenu);
    }
    
    public void setStylesheetMenu() {
	styleMenu.setStylesheets();
    }
}
