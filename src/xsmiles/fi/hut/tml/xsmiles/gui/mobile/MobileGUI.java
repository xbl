package fi.hut.tml.xsmiles.gui.mobile;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.LWCMenu;
import fi.hut.tml.xsmiles.gui.components.swing.XAButton;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.gui.swing.XSmilesUI;

/**
 * A GUI For mobile devices 
 * 
 * @author Juha
 * @version 0.1
 */
public class MobileGUI extends XSmilesUI {

    private BrowserConstraints constraints;
    private BrowserWindow browser;
    private JPanel presentationPanel; 
    private JFrame mainFrame;	
    private Container contentPane; 
    private JPanel upperBar;	
    private JPanel lowerBar, buttons;
    private JButton hide;
    private JTextField statusBar;
    private LWCMenu menuBar;
    private JMenu mlfcMenu;	
    private Container mlfcToolbar; 
    private Toolbar toolBar;
    private JFrame deviceFrame;
    private boolean covers=false;             // Virtual Prototype mode 
    private boolean fullScreenMode=false;             
    private JRootPane rp;
    private Container devicePane;
    private XAButton devicePicture, onoff,up,down,left,right;
    private GuiListener guiListener;
    private BorderLayout b2;
    public VirtualKeyboard keypad;
    private ContentHandlerFactory contentHandlerFactory;
    private boolean showKeypad=true;
    private MLFCControls mlfcControls;
    private ComponentFactory componentFactory;
    
  
    /**
     * Constructor for switching to this GUI
     *
     * @param b the BrowserWindow
     * @param c the presentationPanel
     */
    public MobileGUI(BrowserWindow b, Container c) {
        super(b,c);
        browser=b;
        if(c==null) {		
            presentationPanel=(JPanel)b.getContentArea();
            Log.error("MobileGUI: null container passed, expecting something more!!!");
        }
        else 
            presentationPanel=(JPanel)c;
        init();
    }
    
    /**
     * Contructor for creating a new GUI
     *
     * @param b the BrowserWindow
     */
    public MobileGUI(BrowserWindow b) {
        super(b,null);
        browser=b;
        presentationPanel=(JPanel)b.getContentArea();
        init();
    }
    
    /**
     * plain vanilla init()
     */
    private void init() {
        setSkinsIfNeeded();
        createGraphicalElements();
        attachListeners();
        constraints=new BrowserConstraints("pda");
        constraints.setTitle("pda");
        String fs=browser.getBrowserConfigurer().getProperty("gui/devices/pda/fullscreen");
        String showKeys=browser.getBrowserConfigurer().getProperty("gui/devices/pda/showkeys");
        if(showKeys != null && showKeys.equals("false") ) {
            showKeypad=false;
        }
        if(fs!=null)
            fullScreenMode=fs.equals("true");
        else 
            fullScreenMode=false;
    
        // Set the depth here and size every time requested in getBrowserContraints
        constraints.setDisplayDepth(8);

    }  
    
    /**
     * Set the skin in browser configuration file
     *
     * @param the skin
     */
    //public void setSkinsIfNeeded(String skin) {
    //    browser.getBrowserConfigurer().setProperty("gui/theme",skin);
    //    setSkinsIfNeeded();
    //}
    
    /**
     * All this stuph is needed for modular skins plugin. 
     * The skin library doesn't need to be resident, if
     * skins aren't used
     *
     * @param t The themePack location
     */
//     private void setSkinsIfNeeded() {
//         String themePack=null;
//         String themePath=null;
//         String theme=null;
//         if(browser.getBrowserConfigurer().getProperty("gui/skinsenabled").equalsIgnoreCase("true")) {
//             try{
//                 themePath=browser.getBrowserConfigurer().getProperty("gui/themepacklocation");
//                 theme=browser.getBrowserConfigurer().getProperty("gui/theme");
//                 themePack=themePath+theme;
		
//                 Object[] initArgs, initArgs2;
		
//                 Class skinClass=Class.forName("com.l2fprod.gui.plaf.skin.Skin");
//                 Class skinLookAndFeel=Class.forName("com.l2fprod.gui.plaf.skin.SkinLookAndFeel");
		
//                 Class[] args;
//                 args=new Class[1];
//                 args[0]=Class.forName("java.lang.String");
//                 Method loadThemePack=skinLookAndFeel.getMethod("loadThemePack", args);
		
//                 args[0]=skinClass;
//                 Method setSkin=skinLookAndFeel.getMethod("setSkin", args);
		
//                 initArgs=new Object[1];
//                 initArgs2=new Object[1];
//                 initArgs2[0]=themePack;
//                 initArgs[0]=loadThemePack.invoke(skinLookAndFeel, initArgs2);
		
//                 setSkin.invoke(skinLookAndFeel, initArgs);
		
//                 // the SkinLookAndFeel.loadSkin tries to determine
//                 // if the theme is GTK (gtkrc) or KDE (ends with .themerc)
//                 // by looking at the filename.
//                 // You can also use GtkSkin or KdeSkin classes.
//             } catch(ClassNotFoundException e) {
//                 Log.debug("Class not found " + e.toString());
//                 Log.error(e);
//             } catch(NoSuchMethodException e) {
//                 Log.debug("No such method " + e.toString());
//                 Log.error(e);
//             } catch(Exception e) {
//                 Log.debug("Some other exception " + e.toString());
//                 Log.error(e);
//             }
//             try {
//                 // ask Swing to use Skin Look And Feel
//                 UIManager.setLookAndFeel("com.l2fprod.gui.plaf.skin.SkinLookAndFeel");
//             } catch(Exception e)
//                 {
//                     Log.debug("MobileGUI: problems with setting up skinLF");
//                     Log.error(e);
//                 }
//         }
//     }
    
    /**
     * create Graphical elements
     */
    private void createGraphicalElements() { 
        keypad=new VirtualKeyboard(this); // Needed for focuslisteners in text-fields...
        mainFrame=new JFrame("mobilegui");
        mainFrame.addKeyListener(new MKeyListener());
        if(browser.getBrowserConfigurer().getProperty("gui/virtualproto").equalsIgnoreCase("true"))
            covers=true;
        else 
            covers=false;
	
        if(covers) {
            deviceFrame=new JFrame("mobilegui - device"); 
            devicePicture=new XAButton("./img/ipaq/device/device.jpg");
            up=new XAButton("./img/ipaq/device/up.jpg","./img/ipaq/device/up_hl.jpg",
                            "./img/ipaq/device/up.jpg");
            down=new XAButton("./img/ipaq/device/down.jpg","./img/ipaq/device/down_hl.jpg",
                              "./img/ipaq/device/down.jpg");
            left=new XAButton("./img/ipaq/device/left.jpg","./img/ipaq/device/left_hl.jpg",
                              "./img/ipaq/device/left.jpg");
            right=new XAButton("./img/ipaq/device/right.jpg","./img/ipaq/device/right_hl.jpg",
                               "./img/ipaq/device/right.jpg");
            onoff=new XAButton("./img/ipaq/device/onoff.jpg","./img/ipaq/device/onoff_hl.jpg",
                               "./img/ipaq/device/onoff.jpg");
            onoff.setBounds(258,33,59,49);
            up.setBounds(139,425,88,49);
            down.setBounds(148,512,72,48);
            left.setBounds(102,449,51,92);
            right.setBounds(211,456,50,80);
            DeviceListener dl=new DeviceListener();
            onoff.addActionListener(dl);
            up.addActionListener(dl);
            down.addActionListener(dl);
            left.addActionListener(dl);
            right.addActionListener(dl);
            onoff.setLabel("onoff");
            up.setActionCommand("up");
            down.setActionCommand("down");
            left.setActionCommand("left");
            right.setActionCommand("right");
            onoff.set3D(false);
            up.set3D(false);
            down.set3D(false);
            left.set3D(false);
            right.set3D(false);
	    
            devicePane=deviceFrame.getContentPane();
        }
	
        contentPane=mainFrame.getContentPane();
	
        BorderLayout b=new BorderLayout();
        b.setVgap(0);		
	
        b2=new BorderLayout();
        b2.setVgap(0);
        b2.setHgap(0);
	
        toolBar=new Toolbar(browser, this);
	
        lowerBar=new JPanel();
        statusBar=new JTextField();
        statusBar.setEditable(false);
        statusBar.setBackground(Color.white);
        statusBar.setBorder(new EmptyBorder(0,0,0,0));
	
        mlfcMenu=new JMenu("Document");
        menuBar=new LWCMenu(browser, this, mlfcMenu);
	
        mlfcToolbar=(Container) getMLFCControls().getMLFCToolBar();
        mlfcToolbar.setLayout(new FlowLayout());
	
        JButton exit=new JButton("Quit");
        exit.setRequestFocusEnabled(false);
        exit.setMargin(new Insets(0,0,0,0));
        hide=new JButton("Show menu");
        hide.setRequestFocusEnabled(false);
        hide.setMargin(new Insets(0,0,0,0));
	
        lowerBar.setLayout(new BorderLayout());
        buttons=new JPanel();
        buttons.setLayout(new FlowLayout());
        buttons.add(exit);
        buttons.add(hide);
        guiListener=new GuiListener();
        exit.addActionListener(guiListener);
        hide.addActionListener(guiListener);
        keypad.setVisible(true);
	
        lowerBar.add(buttons, BorderLayout.CENTER);
        lowerBar.add(statusBar, BorderLayout.NORTH);

        presentationPanel.setLayout(b2);
        presentationPanel.setBorder(new EmptyBorder(0,0,0,0));
        contentPane.setLayout(b);

        contentPane.add(presentationPanel, BorderLayout.CENTER);
        contentPane.add(lowerBar, BorderLayout.SOUTH);

        mainFrame.setJMenuBar(menuBar);
        contentPane.add(toolBar, BorderLayout.NORTH);
        hide.setText("Hide menu");

        mainFrame.pack();
        presentationPanel.setPreferredSize(new Dimension(240,320-lowerBar.getSize().height-toolBar.getSize().height-menuBar.getSize().height));
        Log.debug("presentationPanel.preferredSize: " + (320-lowerBar.getSize().height-toolBar.getSize().height-menuBar.getSize().height));
        mainFrame.pack();
        mainFrame.setResizable(false);
	
        if(!covers) {
	    
            mainFrame.show();
            mainFrame.setVisible(true);
            mainFrame.setResizable(true);
            // The following should only have effect on virtual coordinate systems,
            // such as Kaffe running on iPAQ (Why?) and not desktops.
            Insets browserInsets = mainFrame.getInsets();
            mainFrame.setLocation(-browserInsets.left, -browserInsets.top);
        } else {
            devicePane.setLayout(new BulletinLayout());
            rp=mainFrame.getRootPane(); // get rootpane from real Frame
            rp.setLayout(new BorderLayout());
	    
            rp.setBounds(63,102,240,320);
            devicePane.add(rp);
	    
            devicePane.add(onoff);
            devicePane.add(up);
            devicePane.add(down);
            devicePane.add(left);
            devicePane.add(right);
            devicePicture.setBounds(0,0,384,600);
            devicePane.add(devicePicture);
            deviceFrame.pack();
            deviceFrame.setResizable(false);
            deviceFrame.show();
        }
    }

    public void reValidateDocumentArea() {
        //mainFrame.doLayout();
        //mainFrame.invalidate();
        mainFrame.validate();
    }

    public void validate() {
        //mainFrame.doLayout();
        //mainFrame.invalidate();
        mainFrame.validate();
    }


    public ContentHandlerFactory getContentHandlerFactory()
    {
        if (contentHandlerFactory==null) contentHandlerFactory=new SwingContentHandlerFactory(browser);
        return contentHandlerFactory;
    }

    
    public Dimension getPreferredSize() {
        return new Dimension(240,320);
    }
    
    /**
     * Add a window listener for window closing events
     * 
     */
    private void attachListeners() {
        if(covers) {
            deviceFrame.addWindowListener(new java.awt.event.WindowAdapter() 
                {
                    public void windowClosing(java.awt.event.WindowEvent e)
                    {
                        browser.closeBrowserWindow();
                        System.exit(0);
                    }
                });
        } else {
            mainFrame.addWindowListener(new java.awt.event.WindowAdapter() 
                {
                    public void windowClosing(java.awt.event.WindowEvent e)
                    {
                        browser.closeBrowserWindow();
                        System.exit(0);
                    }
                });
        }
    }
    
    public JMenu getMLFCMenu() {
        return mlfcMenu;
    }

    public void navigate(int navigationCommand){
    }
    
    public void openFile(String fileName){
    }

    public void openLocation(URL url){
    }

    public void openLocation(String urlName){
    }
    
    public void exit(){
    }

    public void changeViewEvent(int viewType){
    }

    public void resizeEvent(Dimension size){
    }
    
    /**
     * Set statusbar text
     * 
     * @param the text
     */
    public void setStatusText(String statusText){
        System.out.println("statusBar.setText("+statusText+");");
        statusBar.setText(statusText);
    }
    
    public void setEnabledBack(boolean value) {
        toolBar.setEnabledBack(value);
    }

    public void setEnabledForward(boolean value) {
        toolBar.setEnabledForward(value);
    }

    public void setEnabledHome(boolean value){
        toolBar.setEnabledHome(value);
    }

    public void setEnabledStop(boolean value){
        toolBar.setEnabledStop(value);
    }
    
    public void setEnabledReload(boolean value){
        toolBar.setEnabledReload(value);
    }
    
    public void setEnabledAllMenus(boolean value){
    }

    public void setEnabledLocationCombo(boolean value){
    }

    public void setTitle(String title){
        mainFrame.setTitle(title);
    }

    public void setEnabledOpenFile(boolean value){
    }


    public void setLocation(String s) {
        toolBar.setText(s);
    }
  
    /**
     * @return The browserConstraints of this gui
     */
    public BrowserConstraints getBrowserConstraints() {
        //constraints.setDisplaySize(presentationPanel.getPreferredSize());
        constraints.setDisplaySize(new Dimension(240,320));
        return constraints;
    }

    /**
     * Returns a container where MLFC's can insert their controls
     *
     * @return A container where controls can be added
     */
    public Container getMLFCToolbar() {
        return mlfcToolbar;
    }
    
    /** 
     * Browser is working, animate the animator
     */
    public void browserWorking() {
        toolBar.play();
    }

    /** 
     * Browser is finished, stop the animator
     */
    public void browserReady() {
        toolBar.pause();
        presentationPanel.validate();
        if (presentationPanel!=null)
            presentationPanel.setVisible(true);
        Log.debug(presentationPanel.getSize().toString());
    
        Log.debug(presentationPanel.getLayout().toString());
        Log.debug(presentationPanel.toString());
        presentationPanel.doLayout();
        menuBar.setStylesheetMenu();
        presentationPanel.repaint();
        toolBar.doLayout();
        mainFrame.doLayout();
    }

    public Container getContentPanel() {
        if(presentationPanel==null)
            Log.error("presentaionPanel given null in MobileGUI");
        return presentationPanel;
    }
  
    public Window getWindow() {
        return mainFrame;
    }
  
    public void destroy() {
        presentationPanel.removeAll();
        mainFrame.dispose();
        if(covers)
            deviceFrame.dispose();
    
    }
  
    public void displayKeypad(Component target, String mode) {
        if(showKeypad) {
            keypad.setTarget(target);
            System.out.println("Should be using inputMode "+mode);
            contentPane.remove(lowerBar);
            contentPane.add(keypad, BorderLayout.SOUTH);
            presentationPanel.setPreferredSize(new Dimension(240,300-keypad.getSize().height-toolBar.getSize().height-menuBar.getSize().height));
            mainFrame.validate();
            keypad.repaint();
            if(covers) {
                rp.validate();
            }
        }
    }
  
    public void hideKeypad() {
        if(showKeypad) {
            contentPane.remove(keypad);
            contentPane.add(lowerBar, BorderLayout.SOUTH);
            presentationPanel.setPreferredSize(new Dimension(240,300-lowerBar.getSize().height-toolBar.getSize().height-menuBar.getSize().height));
            mainFrame.validate();
            lowerBar.repaint();
            if(covers) {
                rp.validate();
            }
        }
    }
  
    /**
     * Hide all widgets
     */
    private void hideEveryThing() {
        if(!covers) 
            mainFrame.setJMenuBar(null);
        contentPane.remove(toolBar);
        lowerBar.remove(statusBar);
        if(fullScreenMode)
            lowerBar.remove(buttons);
        hide.setText("Show menu");
        //    mainFrame.requestFocus();
    }

    /**
     * Show all widgets
     */
    private void showEveryThing() {
        if(!covers) 
            if(mainFrame.getJMenuBar()==null) 
                mainFrame.setJMenuBar(menuBar);
        contentPane.add(toolBar, BorderLayout.NORTH);
        lowerBar.add(statusBar, BorderLayout.NORTH);
        if(fullScreenMode)
            lowerBar.add(buttons, BorderLayout.SOUTH);
        hide.setText("Hide menu");
        //    mainFrame.requestFocus();
    }

    private class GuiListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String cmd=e.getActionCommand();
            if(cmd.equals("Quit"))
                browser.closeBrowser();	
            if(cmd.equals("Hide menu"))
                hideEveryThing();
            if(cmd.equals("Show menu"))
                showEveryThing();
      
            mainFrame.doLayout();
            mainFrame.invalidate();
            mainFrame.validate();	    
      
            if(covers) {
                rp.invalidate();
                rp.doLayout();
            }
        }
	
    }
    
    private class DeviceListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String com=e.getActionCommand();
            if(com.equals("up")) {
            } else if(com.equals("down")) {
            } else if(com.equals("left")) {
            } else if(com.equals("right")) {
            } else if(com.equals("onoff")) {
                browser.getGUIManager().createMainGUI(true);
                Log.debug("onoff");
            }
        }
    }

    public ComponentFactory getComponentFactory() {
        if (componentFactory==null) {
            componentFactory = new DefaultComponentFactory();
        }
        return componentFactory;
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.GUI#getMLFCControls()
     */
    public MLFCControls getMLFCControls() {
        if (mlfcControls == null) {
            mlfcControls = new MobileMLFCControls(getComponentFactory());
        }
        return mlfcControls;
    }
    
    private class MobileMLFCControls extends DefaultMLFCControls {
        /**
         * @param cf
         */
        public MobileMLFCControls(ComponentFactory cf) {
            super(cf);
        }

        public void add(XComponent c) {
            getMLFCMenu().add(c.getComponent());
            mlfcMenu.setEnabled(true);
        }
    
        public void remove(XComponent c) {
            getMLFCMenu().remove(c.getComponent());
            if(getMLFCMenu().getItemCount()==0)
                mlfcMenu.setEnabled(false);
        }
    
        public void removeAll() {
            getMLFCMenu().removeAll();
            mlfcMenu.setEnabled(false);
        }
    }
  
    /**
     * KeyListener for GUI Window key events
     */
    private class MKeyListener implements KeyListener 
    {
        public void keyPressed(KeyEvent e) {
            int key=e.getKeyCode();
            switch(key) {
            case KeyEvent.VK_ESCAPE:
                browser.closeBrowserWindow();
                break;
            case KeyEvent.VK_BACK_SPACE:
                browser.navigate(NavigationState.BACK);
                break;
            case KeyEvent.VK_ENTER:
                browser.navigate(NavigationState.FORWARD);
                break;
            case KeyEvent.VK_INSERT:
                guiListener.actionPerformed(new ActionEvent(this, 0, hide.getText()));
                break;
            case KeyEvent.VK_PAGE_UP:
                browser.getMLFCController().pageForward();
                break;
            case KeyEvent.VK_PAGE_DOWN:
                browser.getMLFCController().pageBack();
                break;
            case KeyEvent.VK_UP:
                browser.getMLFCController().moveActiveSpotUp();
                break;
            case KeyEvent.VK_DOWN:
                browser.getMLFCController().moveActiveSpotDown();
                break;
            case KeyEvent.VK_RIGHT:
                browser.getMLFCController().zoomIn();
                break;
            case KeyEvent.VK_LEFT:
                browser.getMLFCController().zoomOut();
                break;
            case KeyEvent.VK_SPACE:
                browser.getMLFCController().followActiveLink();
                break;
            }
            e.consume();
        }
    
        public void keyReleased(KeyEvent e) {
      
        }
    
        public void keyTyped(KeyEvent e) {
      
        }
    }
}
