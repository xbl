package fi.hut.tml.xsmiles.gui.mobile;


import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.awt.Animation;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.gui.components.swing.XAAddressCombo;
import fi.hut.tml.xsmiles.gui.components.swing.XAButton;

import java.awt.*;
import java.net.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * The toolbar contains the combobox, go-button, back-forward etc.etc. 
 * It also contains the mlfctoolbar. in a drop down menu maybe?. 
 *
 *
 */
public class Toolbar extends Container {

    private BrowserWindow browser;
    private MobileGUI gui;
    private XAButton back, forward, stop, reload, home;
    private XAButton plus, minus;
    private JPanel mlfcToolbar;
    private GridBagLayout g;
    private GridBagConstraints c;
    private XAAddressCombo combo;
    private Animation anim;
    private boolean guiFired;
    
    /**
     * Pass the BrowserWindow and gui to get refrences to the browser-core
     *
     * @param b The BrowserWindow
     * @param g the GUI refering to this
     */
    public Toolbar(BrowserWindow b, MobileGUI g) {
	browser=b;
	gui=g;
	createComponents();
    }
    
    public void play() {
	if(anim!=null)
	    anim.play();
    }
    
    public void pause() {
	if(anim!=null)
	    anim.pause();
    }
    
    public void setEnabledBack(boolean state) {
	back.setEnabled(state);
    }
    public void setEnabledForward(boolean state) {
	forward.setEnabled(state);
    }
    public void setEnabledReload(boolean state) {
	reload.setEnabled(state);
    }
    public void setEnabledHome(boolean state) {
	home.setEnabled(state);
    }
    public void setEnabledStop(boolean state) {
	stop.setEnabled(state);
    }
    
    /** 
     * Set combobox text
     */
    public void setText(String s) {
	guiFired=true;
	combo.setText(s);
    }

    /**
     * Create the graphical components
     */
    private void createComponents() {
	back=new XAButton("img/ipaq/back.gif");
	forward=new XAButton("img/ipaq/forward.gif");
	reload=new XAButton("img/ipaq/reload.gif");
	stop=new XAButton("img/ipaq/stop.gif");
	home=new XAButton("img/ipaq/home.gif");

	back.setLabel("BACK");
	forward.setLabel("FORWARD");
	reload.setLabel("RELOAD");
	stop.setLabel("STOP");
	home.setLabel("HOME");
	
	back.setEnabled(true);
	forward.setEnabled(true);
	reload.setEnabled(true);
	stop.setEnabled(true);
	home.setEnabled(true);
	
	anim=new Animation(19,19);

	JPanel uPanel=new JPanel();               // Panel for buttons
	FlowLayout f=new FlowLayout(FlowLayout.LEFT);
	f.setHgap(0);
	f.setVgap(2);

	uPanel.setLayout(f);
	uPanel.add(back);
	uPanel.add(forward);
	//uPanel.add(stop);
	uPanel.add(reload);
	uPanel.add(home);

	JPanel bPanel=new JPanel(); // A panel holding buttons
	g=new GridBagLayout();
	c=new GridBagConstraints();
	bPanel.setLayout(g);
	
	c.anchor = GridBagConstraints.WEST; //bottom of space
	c.gridx = 0;       //aligned with button 2
	c.gridy = 0;       //first row
	c.weightx=0.2;
	g.setConstraints(bPanel, c);
	bPanel.add(uPanel);
	
	c.weightx=0.8;
	c.anchor = GridBagConstraints.EAST; 
	c.gridx=2;
	c.gridy=0;
	c.gridwidth=GridBagConstraints.REMAINDER;
	g.setConstraints(anim, c);
	bPanel.add(anim);


	BorderLayout b=new BorderLayout();
	b.setHgap(0);
	setLayout(b);

	combo = new XAAddressCombo();
	ToolbarListener cl=new ToolbarListener();
	combo.addActionListener(cl);
	combo.getEditor().getEditorComponent().addFocusListener(gui.keypad);
	back.addActionListener(cl);
	forward.addActionListener(cl);
	reload.addActionListener(cl);
	stop.addActionListener(cl);
	home.addActionListener(cl);

	mlfcToolbar=new JPanel();
	FlowLayout fl=new FlowLayout(FlowLayout.LEFT);
	fl.setVgap(0);
	fl.setHgap(0);
	mlfcToolbar.setLayout(fl);
	
	add(bPanel, BorderLayout.NORTH);
	add(combo, BorderLayout.CENTER);
    }
    
    private class ToolbarListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    boolean ok=true;
	    URL url=null;
	    String com=e.getActionCommand();
	    if(com.equals("BACK"))
		browser.navigate(NavigationState.BACK);
	    else if(com.equals("FORWARD"))
		browser.navigate(NavigationState.FORWARD);
	    else if(com.equals("RELOAD")) {
		browser.navigate(NavigationState.RELOAD);
	    }
	    else if(com.equals("STOP")) {
		browser.navigate(NavigationState.STOP);
	    } else if(com.equals("HOME")) 
		browser.navigate(NavigationState.HOME);
	    else if(com.startsWith("comboBoxChanged")) {
	      if(!guiFired) 
		browser.openLocation((String)combo.getSelectedItem());
	      else 
		guiFired=false;
	    }
	}
      private URL createWellformedURL(String s) throws java.net.MalformedURLException{    
	URL url;    
	int delim = s.indexOf("/");
	if (delim != -1) {
	  url = new URL("http", s.substring(0,delim), s.substring(delim));
	} else {
	  url = new URL("http", s, "");
	  }
	return url;
	}
    }
}
