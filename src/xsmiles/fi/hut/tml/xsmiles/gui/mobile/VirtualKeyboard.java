/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 2002 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.gui.mobile;

import fi.hut.tml.xsmiles.gui.GUI;

import java.awt.event.*;
import java.awt.*;

import javax.swing.text.*;
import javax.accessibility.*;

class VirtualKeyboard extends Component implements MouseListener, FocusListener {
	static final double log2 = Math.log(2);
	static final int SENSITIVITY = 35;
	static final int pxPoints[] = { 12, 24, 24, 12, 0, 0, };
	static final int pyPoints[] = { 0, 7, 21, 28, 21, 7, };
	static final Polygon shape = new Polygon(pxPoints, pyPoints, 6);

	private MobileGUI gui;

	private JTextComponent output; // Area for output

	private int debug=1;
	private int last=-1; // Last key pressed before this...
	private int count; // Shortcut to full key count
	private double bestEv=0, lastEv=0; // Fitness function values
	private double temp=0.01; // Effective "temperature" for simulation
	
	// Mouse tracking
	private int start, startX, startY;

	// Location specific data
	private int xLoc[] = new int[29];
	private int yLoc[] = new int[29];
	private double multi[][] = new double[29][29]; // Distance based multipliers
	
	// Should we store the location this key is at,
	// or the key that is at this location? <-
	//@ non_null
	private int keyAt[] = new int[29]; // Index to character data for fast swap
	
	// Key specific data
	//@ non_null
	private String text[] = new String[29]; // Key character for the freq.
	private int next[][] = new int[29][29]; // Model for frequency calc.

	VirtualKeyboard(MobileGUI gui) {
		super();
		this.gui = gui;

		setSize(240,70);

		// This data should eventually be loaded from a kb profile
		int x = 0; int x0 = 12;
		int y = 0;
		char cStr[] = new char[1];
	        for ( int i=0; i<29; i++) {
			// Coordinates for this location
			xLoc[i] = x;
			yLoc[i] = y;
			// Set up standard key labels
			cStr[0]=(char)(i+'A');
			text[i]=new String(cStr);
			// Initialize default location
			keyAt[i]=i;
			// Layout next component location
	                if ( (x+48) > getBounds().width ){
	                        x = x0;
				if (x0==12) x0=0; else x0=12;
	                        y += 21;
	                }
	                else {
	                        x += 24;
	                }
	        }
		text[26] = new String("\u00e5");
		text[27] = new String("\u00e4");
		text[28] = new String("\u00f6");

		// Pre-calculate position based data for key locations
		for (int i=0; i<29; i++)
			for (int j=0; j<29; j++) {
				int xDist = xLoc[i]-xLoc[j];
				int yDist = yLoc[i]-yLoc[j];
				double distance = Math.sqrt(((xDist*xDist)+(yDist*yDist))/(double)576);
				if (i==j) multi[i][j] = 0.127;
				else multi[i][j] = (Math.log(distance+1)/log2)/(double)4.9;
			}

		addMouseListener(this);
		show();
	}
	//@ requires 0 <= first & first >= 28;
	//@ requires 0 <= second & second >= 28;
	void swap(int first, int second) {
		int saved = keyAt[first];
		keyAt[first] = keyAt[second];
		keyAt[second] = saved;
	}
	void press(String input) {
		for(int i=0; i<29; i++) if(text[i].equalsIgnoreCase(input)) {
			press(i, true); return; }
		if(input.equals("\b")) {
			if(output.getSelectionStart()==output.getSelectionEnd()) {
				int caret = output.getCaretPosition();
				if(caret!=0) output.moveCaretPosition(caret-1);
			}
			output.replaceSelection("");
			return;
		}
		if(input.equals(" ")) last = -1; // For auto-train with words...
		output.replaceSelection(input);
	}
	void press(int key, boolean upper) {
		if(upper==true) output.replaceSelection(text[key].toUpperCase());
		else output.replaceSelection(text[key].toLowerCase());
		if(last!=-1) {
			next[last][key]++;
			count++;

			// Swap keys with each other; if new layout is worse
			// than last, swap back. Otherwise save new evaluation.
			int rnd;
			while(true) {
				rnd = (int)(Math.random()*29);
				if(rnd!=key) break;
			}
			swap(last, rnd);
			if(debug==1) System.out.print(lastEv+": ");
			double ev=evaluate();
			if((lastEv!=0)&&(ev>lastEv)) {
				double T = temp*((double)1024/(double)count);
				if(Math.random()>Math.exp((lastEv-ev)/T)) {
					swap(last, rnd);
				} else lastEv = ev;
			} else lastEv=ev;
			repaint();
		}
		last = key;
	}
	double evaluate() {
	  double value = 0;
	  if (count==0) return 0;
	  for (int i=0; i<29; i++)
	    for (int j=0; j<29; j++) {
	      int hits = next[keyAt[i]][keyAt[j]];
      	      if(hits!=0) value+=(double)hits/(double)count*multi[i][j];
	    }
	    
	  if(debug==1)System.out.println("Evaluation of the keyboard: "+value+" s/key, or "+12/value+" wpm.");
	  return value;
	}

	// Component layout
	public void paint(Graphics g) {
		Font fnot = new Font ("Dialog", Font.BOLD, 14);
		g.setFont(fnot);
		FontMetrics fMet = g.getFontMetrics();

		for(int i=0; i<29; i++) {
			g.setColor(new Color(200, 200, keyAt[i]*8));
			g.translate(xLoc[i], yLoc[i]);
			g.fillPolygon(shape);
			g.setColor(new Color(32, 32, 255-(keyAt[i]*8)));
			g.drawString(text[keyAt[i]], 14-(fMet.stringWidth(text[keyAt[i]])/2),14+(fMet.getHeight()/2));
			g.translate(-xLoc[i], -yLoc[i]);
		}
	}

	public Dimension getPreferredSize() {
		return getMinimumSize();
	}

	public Dimension getMinimumSize() {
		return new Dimension(240,70);
	}

	// MouseListener
	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		System.out.println(e);
		startX = e.getX();
		startY = e.getY();
		for(int i=0; i<29; i++) {
			Polygon newShape = new Polygon(pxPoints, pyPoints, 6);

			newShape.translate(xLoc[i], yLoc[i]);
			if(newShape.contains(startX, startY)) {
				System.out.println("Press: "+i+"->"+text[keyAt[i]]);
				start = i;
				return;
			}
		}
		start = -1;
	}

	public void mouseReleased(MouseEvent e) {
		System.out.println(e);
		if(start==-1) {
			System.out.println("Should bring up extra-char menu...");
			return;
		}
		int dragX = e.getX() - startX;
		int dragY = e.getY() - startY;
		if(dragX*dragX+dragY*dragY >= SENSITIVITY) {
			if (-dragY > Math.abs(dragX)) press(keyAt[start], true);
			if (dragY > Math.abs(dragX)) press(".");
			if (-dragX > Math.abs(dragY)) press("\b");
			if (dragX > Math.abs(dragY)) press(" ");
		} else press(keyAt[start], false);
	}

	public void setTarget(Component target) {
		output=(JTextComponent)target;
	}

	// FocusListener	
	public void focusGained(FocusEvent e) {
		gui.displayKeypad(e.getComponent(), "user");
	}
	
	public void focusLost(FocusEvent e) {
		gui.hideKeypad();
	}
}
