/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.newgui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.net.*;
import java.awt.event.*;
import java.util.Vector;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import fi.hut.tml.xsmiles.gui.components.swing.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;

/**
 * A Lightweight menubar. Specific contruction for this GUI
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class UBar extends JPanel {
    protected XAButton back, forw, rel, stop, home;
    Animation animation;
    private static int BUTTON_WIDTH  = 28; // original=80
    private static int BUTTON_HEIGHT = 25; // original=50
    private static int BUTTON_V_GAP  = 2;
    private        int BUTTON_Y_POS  = 45; 
    private int curr_x;
    private int BWIDTH=30;
    private int BHEIGHT=30;
    private JPanel paneli;
    private XAAddressCombo combo;
    private GridBagLayout g;
    private GridBagConstraints c;
    private BrowserWindow browser;
    private boolean guiFired=true;
    private JPanel mlfcToolbar;

    /**
     * Creates the combobox, buttons and the load-animator
     */
    public UBar(BrowserWindow b) {
	browser=b;
	createComponents();
    }
    
    /**
     * A method that creates gui components
     *
     */
    private void createComponents() {
	setBackground(Color.black);
	JPanel uPanel=new JPanel();
	g=new GridBagLayout();
	c=new GridBagConstraints();
	uPanel.setLayout(g);
	
	BorderLayout b=new BorderLayout();
	b.setHgap(0);
	setLayout(b);
	
	animation=new Animation(32,32,200);

	Log.debug("Creating JCombo");
	combo = new XAAddressCombo();
	ButtonListener cl=new ButtonListener();
	combo.addActionListener(cl);
	
	ButtonPanel bP = new ButtonPanel();
	bP.setSize(175,32);
	
	// JDK 11
	//mlfcToolbar=new Container();
	mlfcToolbar=new JPanel();
	mlfcToolbar.setSize(BWIDTH*6, BHEIGHT);
	FlowLayout fl=new FlowLayout(FlowLayout.LEFT);
	fl.setVgap(0);
	fl.setHgap(0);
	mlfcToolbar.setLayout(fl);

	mlfcToolbar.setForeground(Color.black);
	mlfcToolbar.setBackground(Color.lightGray);
	
	c.anchor = GridBagConstraints.WEST; //bottom of space
	c.gridx = 0;       //aligned with button 2
	c.gridy = 0;       //first row
	c.weightx=0.2;
	g.setConstraints(bP, c);
	uPanel.add(bP);
	uPanel.setBackground(Color.black);
	
	c.weightx=0.8;
	c.anchor = GridBagConstraints.EAST; 
	c.gridx=2;
	c.gridy=0;
	c.gridwidth=GridBagConstraints.REMAINDER;
	g.setConstraints(animation, c);
	uPanel.add(animation);
	
	add(uPanel, BorderLayout.NORTH);
	add(combo, BorderLayout.CENTER);

	JPanel p = new JPanel();
	p.setBackground(Color.lightGray);
	BorderLayout b2=new BorderLayout();
	b2.setHgap(0);
	p.setLayout(b2);
	p.add(mlfcToolbar, BorderLayout.SOUTH);
	add(p, BorderLayout.SOUTH);
    }

    /**
     * Animate animator
     */
    public void play() {
	animation.play();
    }

    /** 
     * Animate animator
     */
    public void pause() {
	animation.pause();
    }

    /**
     * sets the combo text
     *
     */
    public void setText(String s) {
	guiFired=true;
	combo.setText(s);
    }

    /**
     * Returns the mlfcContainer
     *
     */
    public Container getMLFCToolbar() {
	return mlfcToolbar;
    }
    
    /**
     * A component that consists of back/forward/etc buttons
     */
    private class ButtonPanel extends Container {
	ButtonListener bl;

	public ButtonPanel() {
	  this.setLayout(new BulletinLayout());
	  back= new XAButton("img/newgui/newback.gif", "img/newgui/newbackh.gif", "img/newgui/newbackd.gif");
	  forw= new XAButton("img/newgui/newforward.gif", "img/newgui/newforwardd.gif", "img/newgui/newforwardh.gif");
	  rel=new XAButton("img/newgui/newreload.gif", "img/newgui/newreloadh.gif", "img/newgui/newreloadd.gif");
	  stop=new XAButton("img/newgui/newstop.gif", "img/newgui/newstoph.gif", "img/newgui/newstopd.gif");
	  home=new XAButton("img/newgui/newhome.gif", "img/newgui/newhomeh.gif", "img/newgui/newhomed.gif");
	  back.setLabel("BACK");
	  forw.setLabel("FORWARD");
	  rel.setLabel("RELOAD");
	  stop.setLabel("STOP");
	  home.setLabel("HOME");
	  
	  bl=new ButtonListener();
	    back.addActionListener(bl);
	    forw.addActionListener(bl);
	    rel.addActionListener(bl);
	    stop.addActionListener(bl);
	    home.addActionListener(bl);

	    back.setSize(BUTTON_WIDTH,BUTTON_HEIGHT);
	    forw.setSize(BUTTON_WIDTH,BUTTON_HEIGHT);
	    stop.setSize(BUTTON_WIDTH,BUTTON_HEIGHT);
	    rel.setSize(BUTTON_WIDTH,BUTTON_HEIGHT);
	    home.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);        	

	    int ypos=4;
	    int sp=3;
	    int cu=0;
	    back.setLocation(cu,ypos);
	    cu+=(sp+BUTTON_WIDTH);
	    forw.setLocation(cu, ypos);
	    cu+=(sp+BUTTON_WIDTH);
	    stop.setLocation(cu, ypos);
	    cu+=(sp+BUTTON_WIDTH);
	    rel.setLocation(cu, ypos);
	    cu+=(sp+BUTTON_WIDTH);
	    home.setLocation(cu,ypos);
	    cu+=(sp+BUTTON_WIDTH);

	    this.add(back);
	    this.add(forw);
	    this.add(stop);
	    this.add(rel);
	    this.add(home);
	}
	
	public Dimension getPreferredSize() {
	    return new Dimension(175,32);
	}
	
	public boolean isLightweight() {
	    return true;
	}
    }
    
    
    
    /**
     * Custom listener for handling button events
     */
    private class ButtonListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    boolean ok=true;
	    URL url=null;
	    String com=e.getActionCommand();
	    if(com.equals("BACK"))
		browser.navigate(NavigationState.BACK);
	    else if(com.equals("FORWARD"))
		browser.navigate(NavigationState.FORWARD);
	    else if(com.equals("RELOAD")) 
	      browser.navigate(NavigationState.RELOAD);
	    else if(com.equals("STOP")) 
	      browser.navigate(NavigationState.STOP);
	    else if(com.equals("HOME")) 
	      browser.navigate(NavigationState.HOME);
	    else if(com.startsWith("comboBoxChanged")) {
	      if(!guiFired) {
		  
			// AUTOMATIC T?YDENNYS
		  String s = (String)combo.getSelectedItem();
	      // Append with http: prefix if missing protocol prefix or not starting with '.'
	      if (s.indexOf(':') < 0 && s.startsWith(".") == false) {
	      	s = "http://"+s;
	      }
		  /*
		  if (!s.startsWith("dir:")) {
		      // Append with /index.html if no dot after the last slash (ok, ei kovin hyv?)
		      int last = s.lastIndexOf('/');
		      if (last > 6) {
			  	// If no dot and doesn't start with 'dir:'
		        if (s.indexOf('.', last) < 0) {
		        	// Strip off trailing slash
		        	if (s.endsWith("/"))
		      		s = s.substring(0, s.length()-1);
		        	s = s + "/index.html";
		        }
		      } else
		      	// No last slash, append 
		      	s = s + "/index.html";
		  }	
           */	  
		  browser.openLocation(s);
	      } else 
		guiFired=false;
	    }
	}
      private URL createWellformedURL(String s) throws java.net.MalformedURLException{    
	    URL url;    
	    int delim = s.indexOf("/");
	    if (delim != -1) {
		url = new URL("http", s.substring(0,delim), s.substring(delim));
	    } else {
		url = new URL("http", s, "");
	    }
	    return url;
	}
    }
}
