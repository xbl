/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.gui.newgui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.XAFrame;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.gui.swing.XSmilesUI;
import fi.hut.tml.xsmiles.mlfc.MLFCController;

/**
 * A New GUI! With Swing
 *
 * @author Juha
 */
public class NewGUI extends XSmilesUI 
{
//  private MLFCController mlfcController;
  private BrowserWindow browser;
  private Container presentationPanel;  // Rendering area for xsmiles
  //private JScrollPane scrollPane;    //This line awaits for the 100% LW rendering
  private Container contentPane; // Swing.. the frame container
  private XAFrame mainFrame;
  private CMenu menuBar;
  private UBar uBar;
  private JTextField statusBar;
  private JPanel uPanel;
  private MLFCControls mlfcControls;
  private Container mlfcToolbar;
  private String title="The X-Smiles browser";
  private DefaultComponentFactory cf;
  private int workingSemaphore=0;

  /**
   * GUI constructor needs the BrowserWindow and the Container
   *
   * The component hierarchy is as follows:
   *
   * mainFrame<br/>
   * - contentPane<br/>
   * -- StatusBar<br/>
   * -- UBar<br/>
   * -- presentationPanel <br/>
   *
   * @param b the BrowserWindow
   * @param c componentContainer (optional)
   */
  public NewGUI(BrowserWindow b, Container c) {
    super(b,c);
    presentationPanel=c;
    browser=b;
    init();
    
  }
    
  /**
     * When creating the first GUI, then the componentContainer is created
     * 
     * @param b the BrowserWindow
     */
  public NewGUI(BrowserWindow b) {
    super(b,null);
    browser=b;
    presentationPanel=browser.getContentArea();
    init();
  }

  /**
     * init-ruotines:
     * - set cross-platform look and feel
     *
     */
    private void init() {
        // crossplatform L&F
      
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            //.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) { }
      
        cf=new DefaultComponentFactory(browser);
        createComponents();
        createFrame();
        addComponents();
        createListeners();

        // Use real screen size for the New GUI
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int depth = tk.getColorModel().getPixelSize();
        Log.debug(d.toString());

    }
  
  public ContentHandlerFactory getContentHandlerFactory()
  {
      if (contentHandlerFactory==null) contentHandlerFactory=new SwingContentHandlerFactory(browser);
      return contentHandlerFactory;
  }

  public void validate() {
    mainFrame.doLayout();
    //mainFrame.invalidate();
    mainFrame.validate();
  }

    
  
  public File showFileDialog(boolean save)
  {
      	return menuBar.showFileDialog(save);
  }

  /**
   * Create UI components
   *
   */
  private void createComponents() {
    //presentationPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    presentationPanel.setLayout(new BorderLayout());
    presentationPanel.setBackground(Color.lightGray);
    
    //scrollPane=new JScrollPane(presentationPanel);

    //    mlfcControls=new JPanel();
    mlfcToolbar = (Container) getMLFCControls().getMLFCToolBar();
    FlowLayout fl=new FlowLayout(FlowLayout.LEFT);
    fl.setVgap(3);
    mlfcToolbar.setLayout(fl);
    menuBar=new CMenu(this, browser);
    statusBar=new JTextField();
    statusBar.setEditable(false);
    //statusBar.setBorder(new EmptyBorder(0,0,0,0));
    statusBar.setForeground(Color.black);
    statusBar.setBackground(Color.white);
    uBar=new UBar(browser);
    //presentationPanel.requestFocus();
  }
    
  /**
     * Create Listeners
     *
     */
  private void createListeners() {
    mainFrame.addWindowListener(new java.awt.event.WindowAdapter() 
      {
	public void windowClosing(java.awt.event.WindowEvent e)
	{
	  browser.closeBrowserWindow();
	}
      });
  }

  /**
   * Create Frame
   *
   */
  private void createFrame() {
    mainFrame=new XAFrame(title);
    contentPane=mainFrame.getContentPane();

  }
    
  /**
     * Add Components
     *
     */
  private void addComponents() {
    mainFrame.setSize(800, 600);
    mainFrame.setMenuBar(menuBar);
    //contentPane.add(MLFCPanel, BorderLayout.CENTER, -1);
    uPanel=new JPanel();

    uPanel.setLayout(new BorderLayout());
    uPanel.add(statusBar, BorderLayout.SOUTH);
    uPanel.add(mlfcToolbar, BorderLayout.CENTER);

    contentPane.add(uBar, BorderLayout.NORTH);
    contentPane.add(presentationPanel, BorderLayout.CENTER);
    contentPane.add(uPanel, BorderLayout.SOUTH);

    int h=contentPane.getSize().height;
    int w=contentPane.getSize().width;
    //    Log.debug("Setting the preferred size of scrollPane to:" + (new Dimension(799, 600 - uBar.getSize().height - statusBar.getSize().height - mlfcControls.getSize().height)).toString());
    //scrollPane.setPreferredSize(new Dimension(300,600 - uBar.getSize().height - statusBar.getSize().height - mlfcControls.getSize().height));
   // For Swing JScrollPane the commands differ
    //presentationPanel.setSize(300,300);
    //presentationPanel.setSize(scrollPane.getViewport().getViewSize());
    //presentationPanel.setSize(scrollPane.getViewportSize());
   //    mainFrame.doLayout();

    mainFrame.doLayout();
    mainFrame.setVisible(true);
  }
    
  public void navigate(int navigationCommand){
  }

  public void openFile(File file){
  }

  public void openFile(String fileName){
  }

  public void openLocation(URL url){
  }

  public void openLocation(String urlName){
  }

  public void exit(){
  }

  public void changeViewEvent(int viewType){
  }

  public void resizeEvent(Dimension size){
  }

  public void setStatusText(String statusText){
    statusBar.setText(" " + statusText);
    statusBar.repaint();
  }
    
  public void setEnabledBack(boolean value) {
    uBar.back.setEnabled(value);
  }
  public void setEnabledForward(boolean value) {
    uBar.forw.setEnabled(value);
  }
  public void setEnabledHome(boolean value){
    uBar.home.setEnabled(value);
  }
  public void setEnabledStop(boolean value){
    uBar.stop.setEnabled(value);
  }
  public void setEnabledReload(boolean value){
    uBar.rel.setEnabled(value);
  }
  public void setEnabledAllMenus(boolean value){
  }
  public void setEnabledLocationCombo(boolean value){
  }
  public void setTitle(String title){
    mainFrame.setTitle(title);
  }
  public void setEnabledOpenFile(boolean value){
  }
  public void setLocation(String s) {
    uBar.setText(s);
  }
  

  
  
  /**
   * Access to the contentPanel for the MLFC's 
   * @return the rendering area
   */
  public Container getContentPanel() {
    return presentationPanel;
  }
  
  /**
   * Return mainFrame
   * @return mainFrame
   */
  public Window getWindow() {
    return mainFrame;
  }
  
  public void destroy() {
    mainFrame.dispose();
    browser=null;
  }

  /** 
   * Browser is working, animate the animator
   */
  public void browserWorking() {
    uBar.play();

    //	scrollPane.setBackground(Color.lightGray);
    if (presentationPanel!=null) presentationPanel.setBackground(Color.lightGray);
  }

  /** 
   * Browser is finished, stop the animator
   */
  public void browserReady() {
    uBar.pause();
    //    scrollPane.setVisible(true);
    if (presentationPanel!=null)
        presentationPanel.setVisible(true);
    menuBar.setStyleSheets();
    uBar.doLayout();
    //scrollPane.doLayout();
    mainFrame.doLayout();
    presentationPanel.doLayout();    
  }
    
  /**
     * Returns the MLFCToolbar where the MLFC's can then 
     * add their controls
     * 
     * @return The container
     */
  public Container getMLFCToolbar() {
    return uBar.getMLFCToolbar();
  }
    
	
  /**
     * Set the scroll policy. See java.awt.ScrollPane for values for scroll_policy.
     * SCROLLBARS_AS_NEEDED -?Specifies that horizontal/vertical scrollbar should be shown only when the size of the child exceeds the size of the scrollpane in the horizontal/vertical dimension. 
     * SCROLLBARS_NEVER?- Specifies that horizontal/vertical scrollbars should never be shown regardless of the respective sizes of the scrollpane and child. 
     */
  public void setScrollable(int a_scroll_policy)
  {
  }

  private class PageListener extends KeyAdapter {
    public void keyTyped(KeyEvent e) {
      int code=e.getKeyCode();
      Log.debug("Key typed " +code);
      MLFCController mlfcController=browser.getMLFCController();
      
      if(mlfcController!=null) {
	switch(code) {
	case KeyEvent.VK_PAGE_UP:
	  for(int i=0;i<5;i++) {
	    if(!mlfcController.scrollUp()) {
	      mlfcController.pageBack();
	      break;
	    }
	  }
	  break;
	case KeyEvent.VK_PAGE_DOWN:
	  for(int i=0;i<5;i++) 
	    if(!mlfcController.scrollDown()) {
	      mlfcController.pageForward();
	      break;
	    }
	  break;
	}
      }
      
    }
  }
  
  public ComponentFactory getComponentFactory() {
    return cf;
  }  
  
  public MLFCControls getMLFCControls() {
      if (mlfcControls == null) {
          mlfcControls = new DefaultMLFCControls(getComponentFactory());
      }
      return mlfcControls;
  }
}
