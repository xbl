/* X-Smiles
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */



package fi.hut.tml.xsmiles.gui.newgui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;
import java.io.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.EventBroker;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.gui.components.awt.GuiMenu;
import fi.hut.tml.xsmiles.gui.components.awt.ZoomMenu;
import fi.hut.tml.xsmiles.gui.components.awt.StylesheetMenu;
import fi.hut.tml.xsmiles.gui.Language;
import fi.hut.tml.xsmiles.gui.GUI;


/**
 * A Lightweight menubar. Specific contruction for this GUI
 * NOTE: Needs a lot more switching
 * Includes menuitems and listening.
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class CMenu extends MenuBar {
    Menu fileMenu, editMenu, viewMenu, helpMenu, bookmarksMenu;
    JFileChooser fc;
    GuiMenu guiMenu;
    ZoomMenu zoomMenu;
	//BookmarksMenu bookmarksMenu2;
    StylesheetMenu styleMenu;
    BrowserWindow browser;
    
    public static final String BOOKMARKS_VIEW="View";
    public static final String BOOKMARKS_VIEW_FRAME="View Frames";

    public CMenu(GUI g, BrowserWindow b) {
	super();
	browser=b;
	
	fileMenu = new Menu(Language.FILE);
	viewMenu = new Menu(Language.VIEW);
	helpMenu = new Menu(Language.HELP);
	editMenu = new Menu(Language.EDIT);
	bookmarksMenu = new Menu(Language.BOOKMARKS);
	
	Log.debug("bookmarksmenu created");
	
	//bookmarksMenu2 = new BookmarksMenu(g, browser);	
	Log.debug("items to be added created");
	
	guiMenu=new GuiMenu(browser);
	zoomMenu = new ZoomMenu(browser);
	styleMenu=new StylesheetMenu(browser);
	
	fc = new JFileChooser("../demo");
	ActionListener al = new MyListener();	

	createMenuItem(fileMenu,Language.NEW,KeyEvent.VK_N,al);	
	createMenuItem(fileMenu,Language.OPEN,KeyEvent.VK_O,al);
	createMenuItem(fileMenu,Language.SAVE,KeyEvent.VK_S,al);
	fileMenu.addSeparator();
	createMenuItem(fileMenu,Language.CLOSE,KeyEvent.VK_W,al);
	createMenuItem(fileMenu,Language.EXIT,-1,al);


	createMenuItem(helpMenu,Language.ABOUT,-1,al);
	

//	createMenuItem(viewMenu,Language.DOCUMENT,KeyEvent.VK_D,al);
	createMenuItem(viewMenu,Language.TREE,KeyEvent.VK_T,al);
	viewMenu.addSeparator();
	createMenuItem(viewMenu,Language.SOURCEXMLXSL,-1,al);
	createMenuItem(viewMenu,Language.SOURCEXML,KeyEvent.VK_M,al);
	createMenuItem(viewMenu,Language.SOURCEXSL,KeyEvent.VK_L,al);

	viewMenu.addSeparator();
	viewMenu.add(zoomMenu);
	createMenuItem(viewMenu,Language.LOG,KeyEvent.VK_O,al);

	createMenuItem(editMenu,"Set as homepage",-1,al);

	editMenu.add(guiMenu);	
	editMenu.add(styleMenu);
	editMenu.addSeparator();

	createMenuItem(editMenu,Language.CONFIGURATION,KeyEvent.VK_D,al);	
	
	ActionListener editBookmarksListener = new EditBookmarksListener();
	createMenuItem(bookmarksMenu,"Edit",KeyEvent.VK_E,editBookmarksListener);	
	ActionListener viewBookmarksListener = new ViewBookmarksListener();
	createMenuItem(bookmarksMenu,BOOKMARKS_VIEW,KeyEvent.VK_V,viewBookmarksListener);	
	createMenuItem(bookmarksMenu,BOOKMARKS_VIEW_FRAME,KeyEvent.VK_V,viewBookmarksListener);	


	
//	bookmarksMenu.add(bookmarksMenu2);

	add(fileMenu);
	add(editMenu);
	add(viewMenu);
	//add(bookmarksMenu2);
	add(bookmarksMenu);

	add(helpMenu);
	setHelpMenu(helpMenu);
	
    }
    
    private void createMenuItem(Menu parent,String text, int shortcut, ActionListener list)
    {
	MenuItem item;
	if (shortcut!=-1) 
	    item = new MenuItem(text, new MenuShortcut(shortcut));
	else 	
	    item = new MenuItem(text);
	parent.add(item);
	item.addActionListener(list);
    }
    public File showFileDialog(boolean save)
    {
    	File file=null;
    	URL requiredDoc=null;
    	int returnVal=0;
    	try {
            if(save)
            	returnVal = fc.showSaveDialog(browser.getCurrentGUI().getWindow());
            else
            	returnVal = fc.showOpenDialog(browser.getCurrentGUI().getWindow());
    	if (returnVal == JFileChooser.APPROVE_OPTION) 
    	    file = fc.getSelectedFile();
    	} catch(Exception e) {
    		Log.error(e);
    	}
    	return file;
    }

    /** 
     * Update stylesheet references
     */ 
    public void setStyleSheets() {
	styleMenu.setStylesheets();
    }
    
    private class MyListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    String com = e.getActionCommand();
	    if(com.equals(Language.OPEN)) {
		openFileDialog();
	    } else if(com.equals(Language.NEW)) {
		browser.newBrowserWindow();
	    } else if(com.equals(Language.CLOSE)) {
		browser.closeBrowserWindow();
	    } else if(com.equals(Language.SAVE)) {
		saveFileDialog();
	    } else if(com.equals(Language.EXIT)) {
		exitDialog();
	    } else if(com.equals("Instructions")) {
		instructions();
	    } else if(com.equals(Language.ABOUT)) {
		about();
	    }
//		 else if(com.equals(Language.DOCUMENT)) 
//	      browser.changeViewType(XsmilesView.PRIMARYMLFC);
	    else if(com.equals(Language.TREE)) 
		    browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.TREEMLFC,com+" : "+browser.getXMLDocument().getXMLURL());
	    else if (com.equals(Language.SOURCEXML))
		  browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.SOURCEMLFC,com+" : "+browser.getXMLDocument().getXMLURL());
		else if(com.equals(Language.SOURCEXMLXSL))
	      browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.SOURCE_AND_XSL_MLFC,com+" : "+browser.getXMLDocument().getXMLURL());
	    else if(com.equals(Language.SOURCEXSL))
	    	browser.getCurrentGUI().showSource(browser.getXMLDocument(),XsmilesView.XSL_MLFC,com+" : "+browser.getXMLDocument().getXSLURL());
	    else if(com.equals(Language.CONFIGURATION)) {
	      optionsDialog();
	    }
		else if(com.equals(Language.LOG)) {
		openLog();
		} else if(com.equals("Set as homepage")) {
		  //String urli=browser.getXMLDocument().getLink().getURLString();
                  String urli=browser.getDocumentHistory().getLastDocument().getURLString();
		  if(urli!=null || ! urli.equals("")) {
		    browser.getBrowserConfigurer().setProperty("main/homepage",urli);
		    browser.getBrowserConfigurer().writeConfig();
		  }
		}
	}
	
	private void openLog()
	{
	    browser.openLog();
	}
	/**
	 * Show about dialog
	 */
	private void about() {
	    try {
		browser.newBrowserWindow(Browser.getXResource("xsmiles.about").toString());
	    } catch(MalformedURLException e) {
		Log.error("Error about dialog cannot be shown.");
	    }
	}
	
	private void exitDialog() {
	    browser.closeBrowser();
	}

	private void optionsDialog() {
		try
		{
            browser.newBrowserWindow(Browser.getXResource("xsmiles.config.editor").toString());
		} catch (Exception e)
            {
                Log.error(e);
            }
	}
	
	private void saveFileDialog() {
	    File file=null;
	    URL requiredDoc=null;
	    int returnVal=0;
	    int viewType = browser.getViewType();
	    PrintWriter out = null;
	    XMLDocument doc = browser.getXMLDocument();
	    returnVal = fc.showSaveDialog(browser.getCurrentGUI().getWindow());
	    if (returnVal == JFileChooser.APPROVE_OPTION) 
		file = fc.getSelectedFile();
	    try {
		requiredDoc = CMenu.toURL(file);
	    } catch(Exception e) {
	    }
	    if (file != null) {
		try {
		    out = new PrintWriter(new FileWriter(file));
		} catch(Exception e) {
		}
		if(viewType == XsmilesView.XSL_MLFC) {
		    Vector sourceVector = new Vector();
		    sourceVector        = doc.getXSLVector() ;
		    for (int i=0; i < sourceVector.size(); i++) {
			String vectorElement = sourceVector.elementAt(i).toString();
			out.print(vectorElement);
			out.print("\n");
		    }
		} else if(viewType == XsmilesView.SOURCEMLFC) {
		    Vector sourceVector = new Vector();
		    sourceVector        = doc.getXMLVector();
		    
		    for (int i=0; i < sourceVector.size(); i++) {
			String vectorElement = sourceVector.elementAt(i).toString();
			out.print(vectorElement);
			out.print("\n");
		    }
		} else if(viewType == XsmilesView.PRIMARYMLFC || viewType == XsmilesView.SOURCE_AND_XSL_MLFC) {
		    Vector sourceVector = new Vector();
		    sourceVector        = doc.getSourceVector();
		    for (int i=0; i < sourceVector.size(); i++) {
			String vectorElement = sourceVector.elementAt(i).toString();
			out.print(vectorElement);
			//out.print("\n");
		    } 
		}
		out.close();
	    }
	}
	
	private void openFileDialog() {
	    URL requiredDoc=null;
	    File file=showFileDialog(false);
	    try
		{
			requiredDoc = CMenu.toURL(file);
	    } catch(Exception e) {
	      //Log.error(e);
	    }
	    if(requiredDoc!=null)
		browser.openLocation(requiredDoc, true);
	}
	
	
	
	private void instructions() {
	}
    }
    
    private class ChangeGuiListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
	    String com = e.getActionCommand();
	    Vector guiNames=browser.getGUIManager().getGUINames();
	    browser.getGUIManager().showGUI(com);
	}
    }
    public static URL toURL(File file) throws java.net.MalformedURLException {
    String path = file.getAbsolutePath();
    if (File.separatorChar != '/') {
        path = path.replace(File.separatorChar, '/');
    }
    if (!path.startsWith("/")) {
        path = "/" + path;
    }
    if (!path.endsWith("/") && file.isDirectory()) {
        path = path + "/";
    }
    return new URL("file", "", path);
    }
	
		private class EditBookmarksListener implements ActionListener
	{
		public EditBookmarksListener()
		{	
			super();
		}

		public void actionPerformed(ActionEvent e)
		{
			Log.debug("Edit bookmark");
			try
			    {
				browser.newBrowserWindow(Browser.getXResource("xsmiles.bookmarks.document").toString(), true);
			    } catch(Exception e1)
				{
				    Log.error(e1);
				}
			Log.debug("done");
		}

	}
	private class ViewBookmarksListener implements ActionListener
	{
		public ViewBookmarksListener()
		{	
			super();
		}

		public void actionPerformed(ActionEvent e)
		{
			Log.debug("View bookmark: "+e.getActionCommand());
			try
			{
                            if (e.getActionCommand().equals(BOOKMARKS_VIEW_FRAME))
                                browser.newBrowserWindow(Browser.getXResource("xsmiles.bookmarks.view.frame").toString(), true);
                            else
                                browser.newBrowserWindow(Browser.getXResource("xsmiles.bookmarks.view").toString(), true);
			} catch(Exception e1)
			{ Log.error(e1);
			}		
			Log.debug("done");
		}

	}
	
}
