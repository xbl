/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.gui.newgui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import fi.hut.tml.xsmiles.Log;

/**
 * A Lightweight menubar. Specific contruction for this GUI
 * NOTE: Needs a lot more switching
 * Includes menuitems and listening.
 *
 * @author Juha Vierinen
 * @version 0.1
 */
public class LWCMenu extends JMenuBar {
    JMenu fileMenu, editMenu, viewMenu, helpMenu, guiMenu;

    public LWCMenu() {
	super();
	fileMenu = new JMenu("File");
	editMenu = new JMenu("Edit");
	//viewMenu = new JMenu("View");
	helpMenu = new JMenu("Help");
	guiMenu = new JMenu("Change gui");
	fileMenu.setMnemonic(KeyEvent.VK_F);
	editMenu.setMnemonic(KeyEvent.VK_E);

	helpMenu.setMnemonic(KeyEvent.VK_H);
	
	JMenuItem open = new JMenuItem("Open");
	JMenuItem save = new JMenuItem("Save");
	JMenuItem options = new JMenuItem("Options");
	JMenuItem exit = new JMenuItem("Exit");
	JMenuItem views = new JMenuItem("View Source");
	JMenuItem instr = new JMenuItem("Instructions");
	JMenuItem about = new JMenuItem("About");
	JMenuItem classic = new JMenuItem("Classic");
	JMenuItem ftv = new JMenuItem("DigiTV");
	JMenuItem newg = new JMenuItem("Default");
	JMenuItem libr = new JMenuItem("Libretto");

	fileMenu.add(open);
	fileMenu.add(save);
	fileMenu.add(options);
	fileMenu.add(exit);
	
	editMenu.add(views);
	guiMenu.add(newg);
	guiMenu.add(classic);
	guiMenu.add(ftv);
	guiMenu.add(libr);
	editMenu.add(guiMenu);
	
	helpMenu.add(instr);
	helpMenu.add(about);
	
	ActionListener al = new MyListener();
	open.addActionListener(al);
	save.addActionListener(al);
	options.addActionListener(al);
	exit.addActionListener(al);
	views.addActionListener(al);
	newg.addActionListener(al);
	classic.addActionListener(al);
	ftv.addActionListener(al);
	libr.addActionListener(al);
	instr.addActionListener(al);
	about.addActionListener(al);



	add(fileMenu);
	add(editMenu);
	add(helpMenu);
    }
    
    private class MyListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    Log.debug(e.getActionCommand());
	}
    }
}
