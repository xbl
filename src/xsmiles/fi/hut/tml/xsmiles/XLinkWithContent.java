/*
 * Created on 16.3.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package fi.hut.tml.xsmiles;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * @author Mikko Honkala
 *
 * This class is a "fake" link, which contains the content in itself. This way it is possible to construct content programmatically and 
 * display that using X-Smiles.
 */
public class XLinkWithContent extends XLink {

	String iContent;
	/**
	 * @param url
	 */
	public XLinkWithContent(String aUrl,String aContent) {
		super(aUrl,SIMPLE);
		iContent=aContent;
		this.m_method=FAKE_WITH_CONTENT;
	}
	
	  /** this may be implemented by subclasses, if they want to hold the content in themselves */
	  public InputStream getContent()
	  {
	  	if (iContent==null) return null;
	  	return new ByteArrayInputStream(iContent.getBytes());
	  }


}
