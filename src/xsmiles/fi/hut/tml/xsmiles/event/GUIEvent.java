package fi.hut.tml.xsmiles.event;

import java.util.EventObject;

/**
 * This class encapsulates all GUI related events.
 * GUI events are NOT reload, navigate back etc, these are sent straight to the browser.
 * The events are seldom used ones, that clearly need to be implemented as events. 
 *
 * The events are specified here, as static variables. They are move link up, down etc.
 * 
 * @author Juha
 * @version 0.1
 */
public class GUIEvent extends EventObject {
    
    public final static int moveActiveLinkUp=19;
    public final static int moveActiveLinkDown=20;
    public final static int followActiveLink=21;
    private int type;

    /**
     * @param source of event
     * @param t type of event specified in NavigationState
     * @see NavigationState
     */
    public GUIEvent(Object source, int t) {
	super(source);
	type=t;
    }
    
    public int getCommand() {
	return type;
    }
}
