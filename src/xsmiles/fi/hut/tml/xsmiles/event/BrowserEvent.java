package fi.hut.tml.xsmiles.event;
import java.util.EventObject;

/**
 * BrowserEvent encapsulates the event types related with browser events.
 * 
 * @author Juha
 * @deprecated Is not used for anything anymore
 * @version 0.1
 */
public class BrowserEvent extends EventObject {

    public static int GUI_CHANGED;
    private int command;

    /**
     * @see static variables for action commands 
     */
    public BrowserEvent(Object source, int command) {
	super(source);
	this.command=command;
    }
    
    public int getCommand() {
	return command;
    }
}
