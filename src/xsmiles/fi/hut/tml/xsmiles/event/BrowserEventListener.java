package fi.hut.tml.xsmiles.event;


/**
 * BrowserEventListener has to implement these methods:
 */
public interface BrowserEventListener {

  /**
   * Use this method, if you need to relay the summoner
   * of the event to the listener.
   * @param e The browser Event, with referer
   */
    //public void browserActionPerformed(BrowserEvent e);
    
  /**
   * Browser state has been changed. 
   * 
   * @param s The new state
   * @see BrowserState
   */
  public void browserStateChanged(int s, String text);

  /**
   * @param e Inform that gui has changed to new GUI
   * @see GUIManager for GUI enumeration
   */
    //  public void guiChanged(BrowserEvent e); 
}
