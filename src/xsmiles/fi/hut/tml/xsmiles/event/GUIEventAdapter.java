package fi.hut.tml.xsmiles.event;

import fi.hut.tml.xsmiles.*;
/**
 * @author Juha
 */
public class GUIEventAdapter implements GUIEventListener 
{
    public void start() {}

    public void destroy() {}

    public void openInNewTab(XLink l, String id) {}

    public void openInNewWindow(XLink l, String id) {}

    public void setStatusText(String statusText) {}

    public void setEnabledBack(boolean value) {}

    public void setEnabledForward(boolean value) {}

    public void setEnabledHome(boolean value) {}

    public void setEnabledStop(boolean value) {}

    public void setEnabledReload(boolean value) {}

    public void setTitle(String title) {}

    public  void setLocation(String s) {}

    public void browserWorking() {}

    public void browserReady() {}
    
    public void GUIEvent(GUIEvent ev) {}
    

}
