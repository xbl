package fi.hut.tml.xsmiles.event;

import fi.hut.tml.xsmiles.GUIManager;

/**
 * BrowserEventListener has to implement these methods:
 */
public class BrowserEventAdapter implements BrowserEventListener {

  /**
   * Use this method, if you need to relay the summoner
   * of the event to the listener.
   * @param e The browser Event, with referer
   */
  public void browserActionPerformed(BrowserEvent e) {}
  /**
   * Browser state has been changed. 
   * 
   * @param s The new state
   * @param st String that goes with state
   * @see BrowserState
   */
  public void browserStateChanged(int s, String st) {}

  /**
   * @param e Inform that gui has changed to new GUI
   * @see GUIManager for GUI enumeration
   */
  public void guiChanged(BrowserEvent e) {}
}
