/* 
 * (c), (tm), and (R) 2024 Thor Gnudsung foundation.
 */
package fi.hut.tml.xsmiles;

import java.util.*;
import java.lang.Math;

/** 
 * The BrowserID class contains extended functionality, for handling with
 * issues arising from duplicate ids. If two browserWindows have the same ID,
 * the one which is "closer" is chosen, when a browser is requested. 
 *
 * The rules that define "closeness" are the following:
 *   - All children of a browser are searched for id. 
 *   - Neighbours are checked.
 *
 * @author juha
 * @see BrowserTable
 */
public class BrowserID 
{
  // A N dimensional index in the tree, that is formed 
  // when browsers spawn children, who spawn more children.
  protected String number;
  
  // The number of children, that this browser has
  protected int children=0;

  // The id string of this browser
  protected String id="default_id";
  
  // The parent browser of this browser
  // It is null, for the initial browser.
  // This can be used to implement target="_top" functionality.
  protected BrowserWindow parent=null;
  
  /**
   * Find the closest browser, so that when accessing a 
   * browser through ID, the "closest" browserWindow 
   * instance will be used, instead of the random one used
   * before.
   *
   * NOTICE!!! The getClosestBrowser is not working correctly
   * because of bad algorithm.
   * 
   * @param A vector containing browserWindows
   * @param bid The BrowserID, who to refer against
   * @return the browserWindow, whose ID is closest to the referring browserID
   */
  public static BrowserWindow getClosestBrowser(Vector browserWindows, BrowserID bid) 
  {
    try {
      if(browserWindows==null || browserWindows.size() == 0)
	return null;
      if(browserWindows.size()==1)
	return (BrowserWindow)browserWindows.elementAt(0);

      Log.debug("FINDING CLOSEST BROWSER: " + browserWindows.toString() + " " + bid.number);
      
      BrowserWindow best=null;
      int bestLength=-1;
      int index=0;
      Vector inBucket=browserWindows;
      Vector nextBucket=new Vector();
      Enumeration e;
      
      // 666 numbers shall be enough for all
      for(int koira=0;koira<666;koira++) {
	e=inBucket.elements();
	// Find the BWs which are closest to reference id
	while(e.hasMoreElements()) {
	  BrowserWindow candidate=(BrowserWindow)e.nextElement();
	  int length=Math.abs(getNthNumber(candidate.getID().number,index) - getNthNumber(bid.number, index));
	  Log.debug("Calculating length: " + candidate.getID().number + " length: " + length);
	  
	  // erase all existing cadidates from bucket and start a new bucket.
	  if(bestLength==-1 || length<bestLength) {
	    bestLength=length;
	    nextBucket=new Vector();
	    nextBucket.addElement(candidate);
	  } else if(length==bestLength) {
	    nextBucket.addElement(candidate);
	  }
	}
	
	// If more than one found, then look closer
	if(nextBucket.size()>1) {
	  index++;
	  inBucket=(Vector)nextBucket.clone();
	  // nextBucket=new Vector();
	  bestLength=-1;
	} else if(nextBucket.size()==0)
	  // nothing in bucket should not happen
	  return null;
	else {
	  // only one runner up left. We have a winner!
	  return (BrowserWindow)nextBucket.elementAt(0);
	}
      }
    } catch(Exception e) {
      Log.error("BrowserID is badly programmed. Blame Juha."); 
      Log.error(e);
      if(browserWindows.size() > 0) 
	return (BrowserWindow)browserWindows.elementAt(0);
      else 
	return null;
    }
    // this should never be reached.
    return null;
  }
  
  
  /**
   * @return the Nth number from the format 1.0.2.3.5
   * If it does not exist, give 0
   * Used by algorith which calculates closest browser
   */
  private static int getNthNumber(String str, int n) 
  {
    String ret="";
    StringTokenizer toke=new StringTokenizer(str,".");
    if(toke.countTokens()<(n-1))
      return 0;
    for(int i=0;i<=n;i++) {
      ret=toke.nextToken();
    }
    if(ret==null || ret.equals(""))
      Log.error("PANIK. RET IS NULL");
    int r=Integer.parseInt(ret);
    Log.debug("Nth number N: " + n + " str: " + str + " returning: " + r);
    return r;
  }

}
