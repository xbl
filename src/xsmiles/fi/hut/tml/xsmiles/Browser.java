/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles;

import java.awt.Container;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.content.ContentLoaderListener;
import fi.hut.tml.xsmiles.content.ContentManager;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.content.xml.MLFCManager;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XMLBroker;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.messaging.Messaging;
import fi.hut.tml.xsmiles.mlfc.MLFCController;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.util.XSmilesConnectionWithContent;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.xml.XMLParserFactory;
import fi.hut.tml.xsmiles.xslt.XSLTEngine;
import fi.hut.tml.xsmiles.xslt.XSLTFactory;

/**
 * <code>Browser</code> ties together all components, provides access to them
 * and offers some general functionality such as error dialog and document
 * retrieval.
 * 
 * The Browser implements the BrowserWindow interface, which it is referred as
 * from the rest of the browser.
 * 
 * To integrate the Browser in your own project, just use the constructors.
 * e.g. <code>Browser b=new Browser</code>
 * 
 * @author Jukka Heinonen
 * @author Mikko Honkala
 * @author Juha Vierinen
 */
public class Browser implements BrowserWindow {

    // This hashtable container vectors of browserWindows, that are accessed
    // with
    // IDs. Each vector contains a list of browsers with that ID
    public static BrowserTable browserTable;
    public BrowserID browserID;

    // copy version string here
    public static final String version = Version.version;

    // BrowserIDs are used for indexing BW instances.
    // In many cases we will like to know, which
    // BW is closest.
    //
    // Each browser has an ID, and it knows it's
    // parents ID. The format is simple:
    // The rootbrowserID of the initial browser is
    // 1. The First browser will be 1.1, and if a document
    // in this browser loads a new browserwindow instance,
    // it will be 1.1.1. The second one instanciated by 1.1
    // will be 1.1.2.
    //
    //
    // rootBrowser is the mother of all evil.
    public static BrowserID rootBrowserID = new BrowserID();
    static {
        rootBrowserID.number = "1";
    }
    protected boolean hasGUI = true;
    
    public static final short STATE_INITIALIZED=10;
    public static final short STATE_UNINITIALIZED=20;
    
    protected short initstate=STATE_UNINITIALIZED;

    // Managers, handlers, parsers and such.
    protected GUIManager guiManager;
    protected EventBroker eventBroker;

    /** the MLFCListener listens to events from the MLFC */
    protected MLFCListener fMLFCListener;

    public static XMLParser xmlParser;
    protected static XSLTEngine xslEngine;
    protected DocumentHistory documentHistory;
    protected static XMLConfigurer browserConfigurer;
    /**
     * the content manager is used to register content handlers and to destroy
     * them
     */
    protected ContentManager fContentManager;
    protected BrowserLogic state;

    // User Interface Stuff
    // Where is the class of the ComponentFactory defined?
    //  - config.xml
    //  - GUI
    // How is the componentFactory instantiated?
    //  - GUIManager, GUI,
    // Where is it located?
    //  - GUI, Browser, GUIManager
    // Do we use XComponents? or could we
    // possibly also offer AWT base classes?
    protected ComponentFactory componentFactory;
    protected Container contentArea;
    protected Container controlsArea;

    // Current document & view
    protected XLink currentPage;
    protected String lastTried;
    protected XMLDocument currentDocument;
    protected String currentView = "document";
    protected int viewType = XsmilesView.PRIMARYMLFC;

    /* the stylesheet title the user has selected */
    protected String preferredStylesheetTitle;

    public Messaging MessageHandler;

    // Are the MLFC mapping set
    public static boolean mappingsSet = false;

    public final static String resourceLocation = "http://www.xsmiles.org/resources/";

    //used for preventing infloop of errors, when error page throws an error
    // ;)
    protected int errorCount = 0;

    protected static boolean firstLoad = true;

    /**
     * @return url to a browser resource
     */
    public static URL getXResource(String id) throws MalformedURLException {
        return Resources.getResourceURL(id);
    }


    /** the duration for last page load */
    private double lastDur = 0f;

    // to create the initial content container
    public static String componentFactoryClassName = "fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory";

    /**
     * Create the browser object, but do not initialize it
     */
    public Browser() {
        //initBrowser(null, null, null, null, true, true);
    }
    
    public static void setXMLConfigurerStatic(XMLConfigurer config)
    {
        browserConfigurer=config;
    }

    /**
     * Create a browser instance, optionally with a GUI. If you do not use a
     * GUI, you still have to supply the title of the GUI, so that content
     * adaptation, such as CC/PP and Media Query will work. If name is left
     * null, the default desktop constraints will apply.
     * 
     * @param showGUI
     *                   If no main GUI is to be shown, then showGUI=false
     * @param uri
     *                   the initial uri to load
     * @param id
     *                   the id of the new browser
     * @param name
     *                   The title of the GUI constraints to use (see config.xml)
     */
    public Browser(String uri, String id, boolean showGUI, String name) {
        initBrowser(new XLink(uri), name, id, null, showGUI, true);
    }

    public Browser(XLink uri, String id, boolean showGUI, String name) {
        initBrowser(uri, name, id, null, showGUI, true);
    }

    public Browser(XLink uri, String id, boolean showGUI, String name, boolean loadInitialDoc) {
        initBrowser(uri, name, id, null, showGUI, loadInitialDoc);
    }

    /**
     * Create a new browser with GUI, id and initial URI
     * 
     * @param uri
     *                   the initial uri to load
     * @param id
     *                   the id of the new browser
     */
    public Browser(String uri, String id) {
        initBrowser(new XLink(uri), null, id, null, true, true);
    }

    /**
     * Create a new browser with GUI, id and initial URI
     * 
     * @param uri
     *                   the initial uri to load
     * @param id
     *                   the id of the new browser
     */
    public Browser(XLink uri, String id) {
        initBrowser(uri, null, id, null, true, true);
    }

    /**
     * Create a new browser with, id and initial URI, and default GUI
     * 
     * @param uri
     *                   the initial uri to load
     * @param id
     *                   the id of the new browser
     * @param parent
     *                   The parent BW
     */
    public Browser(String uri, String id, BrowserWindow parent) {
        initBrowser(new XLink(uri), null, id, parent, true, true);
    }

    /**
     * Expert constructor. All possible parameters.
     * 
     * @param uri
     *                   Initial URI to load
     * @param title
     *                   The title of the GUI constraints (@see config.xml)
     * @param id
     *                   The id for the browser
     * @param parent
     *                   The parent of this browser
     * @param show
     *                   Show a gui or not (if no GUI is shown, functions more as a
     *                   gui Component
     * @see this.getConentArea())
     */
    public Browser(String uri, String title, String id, BrowserWindow parent, boolean show) {
        initBrowser(new XLink(uri), title, id, parent, show, true);
    }

    /**
     * For debugging purposes. Print out the list of browser instances
     */
    private String listBrowsers() {
        // list browsers in browserTable
        return "jeeje2";
    }

    /**
     * @return resourceLocation for external MLFC loading...
     */
    public String getResourceLocation() {
        return resourceLocation;
    }

    /**
     * The main init()^(TM) method of the browser. Should eat almost any
     * parameters, and still not spit anything out.
     * 
     * creates: configurer, ccpp extensions, parser and prosessor,
     * ContentManager, eventBroker, documentHistory, initial GUI, messaging,
     * and stuff.
     * 
     * @param initialURL
     *                   The initial URI to load, if null, then homepage loaded.
     * @param guiTitle
     *                   The title of the gui configuration (see config.xml
     *                   config/gui/devices/*), if null, then desktop is used.
     * @param id
     *                   The id for this browser, is null, then "default-id" is used.
     * @param showGUI
     *                   Show the GUI or not, if null, then true.
     * @param parent
     *                   The parent BW, if null, then treated as a top level browser.
     */
    public void initBrowser(XLink initialURL, String guiTitle, String id, BrowserWindow parent, boolean showGUI, boolean loadInitialDoc) {
        String parentNumba = "";
        int parentsChildCount = 0;

        printParserVersions();

        if (browserTable == null) {
            browserTable = new BrowserTable();
        }

        if (BrowserWindowList.browserWindows == null) {
            BrowserWindowList.browserWindows = new Vector();
        }

        if (id == null)
            id = "default_id";

        if (parent != null) {
            parentNumba = parent.getID().number;
            parentsChildCount = ++parent.getID().children;
        } else // parent is null, so this is the first browser
        {
            parentNumba = rootBrowserID.number;
            parentsChildCount = ++rootBrowserID.children;
        }

        hasGUI = showGUI;

        browserID = new BrowserID();
        browserID.number = parentNumba + "." + parentsChildCount;
        browserID.id = id;
        browserID.parent = parent;

        // Add the BWs datastruct
        browserTable.add(this);
        // DEBUG
        browserTable.listBrowsers();
        new NavigationState(); // initialises Navigation menu
        // enumeration

        // Browser core related events are handled through EventBroker
        eventBroker = new EventBroker(this);

        /*
         * swing dependency if(firstLoad) AboutDialog.increment();
         */

        // BrowserMLFCListener listens to events from the MLFCs
        fMLFCListener = new BrowserMLFCListener(this);

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        try {
            Log.info("Current dir: " + new File(".").getCanonicalPath());
        } catch (Exception e) {
            Log.error(e);
        }

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // Should be initiated before browserconfigurer
        if (browserConfigurer == null)
            browserConfigurer = new XMLConfigurer();

        if (guiTitle == null)
            guiTitle = browserConfigurer.getProperty("gui/initialgui");

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // Create parser and prosessor
        createParserAndProsessor();

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // All MLFC related methods
        fContentManager = new ContentManager(this, this.fMLFCListener);

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // Set MLFC mappings
        if (!mappingsSet)
            setMLFCMappings();

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // All GUI related methods
        guiManager = new GUIManager(this);

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // History
        documentHistory = new DocumentHistory();

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // Browser should now be ready. Declare ready state.
        //state.setState(BrowserLogic.READY);
        guiManager.setCurrentGUIName(guiTitle);

        /*
         * if(firstLoad) AboutDialog.increment();
         */

        // Let there be GUI...
        // If there is a GUI, it is selected based on the guiTitle
        // Else, if no title specified, use default GUI
        // If no GUI, then just set the title, so that the
        // GUI constraints follow them
        /*
         * if(firstLoad) AboutDialog.increment();
         */
        firstLoad = false;

        // Create class that is in charge of the browser "state machine"
        // It encaplsulates as much as possible of the browser logic, and
        // state transition related issues.
        state = new BrowserLogic(this);
        state.setState(BrowserLogic.INITIALIZINGBROWSER);

        contentArea = this.createContentArea();
        if (showGUI && guiTitle != null) {
            Log.debug("Creating GUI " + guiTitle);
            guiManager.showGUI(guiTitle, false);
        } else if (showGUI) {
            guiManager.createMainGUI();
        } else if (guiTitle != null) {
            guiManager.setCurrentGUIName(guiTitle);
        }

        // Initialize JMS
        initializeMessaging();

        // This causes the home page to be retrieved and parsed.
        if ((initialURL == null || initialURL.getURL() == null) && loadInitialDoc)
            openLocation(this.getBrowserConfigurer().getProperty("main/homepage"));
        else if (loadInitialDoc)
            openLocation(initialURL, true);

        // A vector that contains browserWindows
        if (BrowserWindowList.browserWindows != null)
            BrowserWindowList.browserWindows.addElement(this);
    }

    /**
     * we do not want swing dependency, so this method creates the content area
     * by reflection
     */
    protected Container createContentArea() {
        try {
            Class c = Class.forName(componentFactoryClassName);
            ComponentFactory fact = (ComponentFactory) c.newInstance();
            return fact.createContentPanel();
        } catch (Throwable t) {
            Log.error(t, "Could not instantiate " + componentFactoryClassName + ". SMIL will not work");
        }
        return null;
    }

    public static void printParserVersions() {
        try {
            // use reflection to make sure that it can be run without
            // xerces/xalan
            String xalanVersion = (String) Class.forName("org.apache.xalan.Version").getDeclaredMethod("getVersion", null).invoke(null, null);
            String xercesVersion = (String) Class.forName("org.apache.xerces.impl.Version").getDeclaredMethod("getVersion", null).invoke(null, null);
            Log.info("Xalan version: " + xalanVersion);
            Log.info("Xerces version: " + xercesVersion);
        } catch (Throwable e) {
            Log.error(e);
        }
    }

    /**
     * Create an XML parser, and an XSL prosessor
     */
    private void createParserAndProsessor() {
        try {
            if (xmlParser == null)
                xmlParser = XMLParserFactory.createXMLParser(browserConfigurer.getProperty("main/parser"));
        } catch (Exception e) {
            Log.error("Cannot create XML processor named: " + browserConfigurer.getProperty("main/parser") + ". Using default.");
            xmlParser = XMLParserFactory.createDefaultXMLParser();
        }

        try {
            if (xslEngine == null)
                xslEngine = XSLTFactory.createXSLTEngine(browserConfigurer.getProperty("main/xslprocessor"));
        } catch (Exception e) {
            Log.error("Cannot create XSLT processor named: " + browserConfigurer.getProperty("main/xslprocessor") + ". Using default.");
            xslEngine = XSLTFactory.createDefaultXSLTEngine();
        }
    }

    /**
     * Search for JMS libraries. If not found, them messaging is disabled
     */
    private void initializeMessaging() {
        try {
            // Just to check whether messaging exists in the classpath
            Class jms = this.getClass().getClassLoader().loadClass("fi.hut.tml.xsmiles.messaging.JMSMessaging");
            MessageHandler = (Messaging) jms.newInstance();
            MessageHandler.initialize(this);
        } catch (java.lang.NoClassDefFoundError e) {
            Log.error("Java Messaging disabled, JMS jars (webclient.jar, jndi.jar) not found in CLASSPATH.");
        } catch (Exception e) {
            Log.error("Java Messaging disabled, JMS jars (webclient.jar, jndi.jar) not found in CLASSPATH.");
        }
    }

    /**
     * Gets the version of the X-smiles browser.
     * 
     * @return core's version
     */
    public String getVersion() {
        return Version.version;
    }

    /**
     * Gets the primary document that is currently shown in the browser's main
     * window.
     * 
     * @return The document that is currentyl shown. null, if problems have
     *               occured.
     */
    public XMLDocument getXMLDocument() {
        return currentDocument;
    }

    /**
     * @return Java Messaging Handler
     */
    public Messaging getMessageHandler() {
        return MessageHandler;
    }

    /**
     * Open the Log window, to output debug data
     */
    public void openLog() {
        openStaticLog();
    }

    /**
     * Open the Log window, to output debug data, using static method.
     */
    public static void openStaticLog() {
        Utilities.openStaticLog();
    }

    protected long threadID = 0;
    //protected long last=0;
    private Integer fetcherMutex = new Integer(0), numberMutex = new Integer(1);
    protected Thread currentThread;

    /**
     * this method is called in the start of the fetcher thread to get a ticket
     * number. The ticket number is only valid if it is the same as last,
     * otherwise the thread must terminate
     */
    protected long enterFetcher() {
        synchronized (numberMutex) {
            //last=++threadID;
            return ++threadID;
        }
    }

    /**
     * this method is called in the end of the fetcher. it currently does
     * nothing
     */
    protected void exitFetcher(long threadnumber) {
        synchronized (fetcherMutex) {
            Log.debug("***** EXIT_FETCHER: NUMBER OF THREADS:" + threadID);
            fetcherMutex.notifyAll();
        }

    }

    /**
     * This function is used both in primary and secondary MLFC loading SMIL
     * uses this to have better control over media This method takes care of
     * opening the HTTP connection and reading the media type
     */
    public XSmilesContentHandler createContentHandler(XLink link, Container cArea, boolean primary) throws Exception {
        return createContentHandler(link, cArea, primary, null);
    }

    /**
     * This function is used both in primary and secondary MLFC loading SMIL
     * uses this to have better control over media This method takes care of
     * opening the HTTP connection and reading the media type
     */
    public XSmilesContentHandler createContentHandler(XLink link, Container cArea, boolean primary, ContentLoaderListener list) throws Exception {
        InputStream stream = null;
        XSmilesConnection conn = null;
        conn = this.openConnection(link);
        String contentType = conn.getContentType();
        if (list != null && !list.doHandle(contentType)) {
            Log.debug("doHandle returned false, does not create a content handler for: " + link.getURL() + " : " + list);
            return null;
        } else {
            stream = conn.getInputStream();
            XSmilesContentHandler handler = this.getCurrentGUI().getContentHandlerFactory().createContentHandler(contentType, link);
            if (handler != null) {
                handler.setBrowserWindow(this);
                handler.setMLFCListener(this.fMLFCListener);
                if (cArea != null)
                    handler.setContainer(cArea);
                handler.setPrimary(primary);
                handler.setInputStream(stream);
                handler.setURL(link);
                handler.setConnection(conn);
            }
            return handler;
        }
    }

    /**
     * This method creates a contenthandler based on the contentType parameter,
     * without reading the media type from the connection
     * 
     * @param contentType
     * @param cArea
     * @return
     */
    public XSmilesContentHandler createContentHandler(String contentType, XLink link, Container cArea, boolean primary) throws IOException, Exception {
        XSmilesContentHandler handler = this.getCurrentGUI().getContentHandlerFactory().createContentHandler(contentType, link);
        if (handler != null) {
            handler.setBrowserWindow(this);
            handler.setMLFCListener(this.fMLFCListener);
            if (cArea != null)
                handler.setContainer(cArea);
            handler.setPrimary(primary);
            handler.setURL(link);
            this.fContentManager.addContentHandler(handler);
        }
        return handler;
    }

    /*
     * protected void handleFetchError(Exception e, XLink link) { if (e
     * instanceof TransformerException) { TransformerException te =
     * (TransformerException)e; Log.error(e); // ---- XSL EXCEPTION
     * if(state!=null) { state.setState(BrowserLogic.ERROROCCURED, "XSL
     * Transformation error."); }
     * 
     * String mes=te.getMessageAndLocation(); // Show error document, or dialog
     * of errordocument fails showErrorDialog( "Error retrieving document"
     * ,"XSL Error:\n"+"\n"+link.getURL().toString()+"\n"+mes,false); } else if (e
     * instanceof SAXException) { // ---- XML EXCEPTION Log.error(e);
     * state.setState(BrowserLogic.ERROROCCURED, "XML Parser error.");
     * 
     * SAXParseException pe=getXMLParser().getLastParseException(); String
     * mes=e.getMessage(); if (pe!=null) mes+="\n"+"At
     * line:"+pe.getLineNumber()+" column:"+ pe.getColumnNumber()+" of
     * "+pe.getSystemId(); pe=null; // Show error document, or dialog of
     * errordocument fails showErrorDialog( "Error retrieving document" ,"XML
     * Parse Error:\n"+"\n"+link.getURL().toString()+"\n"+mes); } else { //
     * ---- OTHER EXCEPTION String error = "Failed to read document:
     * "+link.getURL(); Log.error(e,error);
     * state.setState(BrowserLogic.ERROROCCURED, error+"\n"+e.getMessage()); //
     * Show error document, or dialog of errordocument fails
     * showErrorDialog("Error retrieving document"
     * ,link.getURL().toString()+"\n"+e.getMessage()); Log.error(e); } }
     */

    public void waitState(int state) {
        long now = System.currentTimeMillis();
        Log.debug("Waiting for the state " + state);
        synchronized (fetcherMutex) {
            // Wait for the state no longer than 5 seconds
            for (int i = 5; getState() != state && i > 0; i--) {
                try {
                    fetcherMutex.wait(1000);
                } catch (InterruptedException e) {
                }
            }
        }
        Log.debug("Waited for the state " + (System.currentTimeMillis() - now) + " milliseconds");
    }

    private class FetchStopped extends Exception {
    }

    private class HandlerThread implements Runnable {

        private static final int PREFETCH = 0;
        private static final int PLAY = 1;
        private static final int CREATE = 2;

        private XSmilesContentHandler handler;
        private int job;
        private Exception error = null;
        
        private XLink link;
        private Container contentArea;
        
        /**
         * @param link
         * @param contentArea
         */
        public HandlerThread(XLink link, Container contentArea) {
            this.link = link;
            this.contentArea = contentArea;            
        }

        private void throwException() throws Exception {
            if (error!=null) {              
                throw error;
            }
        }
        
        public synchronized XSmilesContentHandler createHandler() throws Exception {
            job = CREATE;
            Thread t = new Thread(this);
            t.start();
            t.join();
            throwException();
            return handler;
        }
        
        public synchronized void prefetch() throws Exception {
            job = PREFETCH;
            Thread t = new Thread(this);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                Log.debug("*** Prefetch interrupted");                
            }
            throwException();
        }
        
        public synchronized void play() throws Exception {
            job = PLAY;
            Thread t = new Thread(this);
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                Log.debug("*** Play interrupted");                
            }
            throwException();
        }

        public void run() {
            error = null;
            try {
                switch (job) {
                case PREFETCH:
                    handler.prefetch();
                    break;
                case PLAY:
                    handler.play();
                    break;
                case CREATE:
                    handler = Browser.this.createContentHandler(link, contentArea, true, null);
                }            
            } catch (Exception t) {
                error = t;
            }
        }
    }

    /**
     * FETCH AND CREATE PRIMARY MLFCs
     * 
     * @param d
     *                   Display the URI in the GUI, i.e., show the URI in GUI
     *                   combo-box. E.g., the error page wouldn't want to display the
     *                   error page URI
     *  
     */
    protected void fetchProcessAndForkDocument(XLink l, boolean s, boolean d) {
        Log.debug("GOING INTO DEADLOCK, FETCHING DOCUMENT");
        final XLink link = l;
        final boolean saveInDocumentHistory = s;
        final boolean displayURL = d;
        final BrowserLogic state = this.state;
        final BrowserWindow window = this;
        final Container contentArea = this.getContentArea();
        final GUI gui = this.getCurrentGUI();

        Thread t = new Thread(l.getURL().toString()) {

            private long myNumber;

            private void testThread() throws FetchStopped {
                if (myNumber != threadID) {
                    throw new FetchStopped();
                }
            }

            public void run() {
                XSmilesContentHandler handler = null;
                myNumber = enterFetcher();
                if (currentThread != null) {
                    currentThread.interrupt();
                }
                long now = System.currentTimeMillis();
                int stage = 1;
                Log.debug("Entering into synchronized");

                synchronized (fetcherMutex) {
                    currentThread = this;
                    try {
                        // test if I am not the latest thread
                        testThread();

                        // we are still the main thread
                        currentPage = link;
                        if (displayURL) {
                            Log.debug("Should display: " + link.getURL().toString());
                            lastTried = link.getURL().toString();
                            state.setState(BrowserLogic.RETRIEVINGDOCUMENT, link.getURL().toString());
                            if (saveInDocumentHistory) {
                                documentHistory.addURL(link);
                            }
                            checkButtons();
                        } else {
                            state.setState(BrowserLogic.RETRIEVINGERRORDOCUMENT);
                        }
                        
                        HandlerThread ht = null;
                        // TODO: open connection and read the content type
                        
                        try {                                                        
                            ht = new HandlerThread(link, contentArea);
                            handler = ht.createHandler();
                            //ht.prefetch();  
                        } catch (InterruptedException e) {
                        } catch (Exception e) {
                            Log.error(e, "Error fetching document at: " + link.getURL().toString());
                            state.setState(BrowserLogic.ERROROCCURED, e + "\n" + e.getMessage());
                            // Show error document, or dialog of errordocument
                            // fails
                            showErrorDialog("Error retrieving document", link.getURL().toString(), false, e);
                            return;
                        }

                        stage++;
                        testThread();

                        Thread.yield();
                        
                        // we are still the main thread
                        state.setState(BrowserLogic.INITIALIZING_MLFC);
                  
                        stage++;
                        testThread();

                        state.setState(BrowserLogic.ACTIVATING_MLFC);
                        getContentManager().stopCurrentActiveContent();
                        contentArea.removeAll();
                        if (getCurrentGUI() != null) {
                            // TODO: the componentFactory should be bound to
                            // the Browser
                            // Which componentFactory.. that should be defined
                            // in configuration file
                            getCurrentGUI().getMLFCControls().getMLFCToolBar().removeAll();
                        }

                        try {
                            // we add this handler to the ContentManager
                            // in order for it to destroy it
                            getContentManager().addPrimaryContentHandler(handler);                            
                            ht.play();
                        } catch (Exception e) {
                            Log.error(e);
                            showErrorDialog("MLFC Error", e.toString(), false, e);
                            return;
                        }

                        // stop called during play
                        stage++;
                        testThread();

                        currentDocument = handler.getXMLDocument();
                        // document can be parsed, tranformed, and received
                        
                        state.setState(BrowserLogic.READY);
                        // reset errorCount, because everything ok.
                        errorCount = 0;
                    } catch (FetchStopped e) {
                        // Thread was stopped at some point
                        Log.debug("*** Thread stop at stage " + stage);
                        try {
                            // if handler has been added to content, this
                            // should close it
                            getContentManager().stopCurrentActiveContent();
                            contentArea.removeAll();
                            if (getCurrentGUI() != null) {                              
                                getCurrentGUI().getMLFCControls().getMLFCToolBar().removeAll();
                                getCurrentGUI().getWindow().repaint();
                            }                            
                            // if handler is around, but not in contentmanager
                            if (handler != null) {
                                handler.close();
                                handler = null;
                            } 
                        } catch (Throwable t) {
                        }
                    } finally {
                        lastDur = (System.currentTimeMillis() - now) / (double) 1000;
                        Log.debug("*** Fetcher thread exiting, time: " + lastDur + " secs.");
                        exitFetcher(myNumber);
                        currentThread = null;
                    }
                } // sychronized
            } // run()
        }; // thread
        t.start();
        return;
    }

    /**
     * Normally load a document
     * 
     * @param aLink
     *                   Where to
     * @param saveHistory
     *                   Save in History
     */
    /*
     * private void fetchProcessAndForkDocument(XLink aLink,boolean
     * saveHistory) { fetchProcessAndForkDocument(aLink,saveHistory,true); }
     */

    /**
     * SECONDARY MLFC Causes an secondary XML document to be retrieved and
     * parsed, and displayed in a container. A thread is created for this
     * action. Creates the secondary MLFC thru MLFCManager
     * 
     * @param l
     *                   XLink object containing the URL of the required document
     * @param c
     *                   the Container in which to display the secondary MLFC
     * 
     * @see XMLDocument
     */
    public XSmilesContentHandler displayDocumentInContainer(XLink l, Container c) {
        return displayDocumentInContainer(l, c, null);
    }

    /**
     * SECONDARY MLFC Causes an secondary XML document to be retrieved and
     * parsed, and displayed in a container. A thread is created for this
     * action. Creates the secondary MLFC thru MLFCManager
     * 
     * @param l
     *                   XLink object containing the URL of the required document
     * @param c
     *                   the Container in which to display the secondary MLFC
     * @param list
     *                   this listener can stop the loading of the content
     * 
     * @see XMLDocument
     */
    public XSmilesContentHandler displayDocumentInContainer(XLink l, Container c, ContentLoaderListener list) {
        final XLink link = l;
        final Container container = c;

        if (state != null) {
            state.setState(BrowserLogic.RETRIEVINGSECONDARYDOCUMENT, link.getURL().toString());
        }
        final XSmilesContentHandler handler;
        try {
            handler = createContentHandler(link, container, false, list);
        } catch (Exception e) {
            Log.error(e, "displayDocumentInContainer: for: " + link.getURL().toString());
            return null;
        } // we add this handler to the ContentManager
        // in order for it to destroy it
        if (handler != null) {
            getContentManager().addContentHandler(handler);
            Thread t = new Thread(link.getURL().toString()) {

                public void run() {
                    Log.debug("*** New thread started, and retrieving a new secondary document");
                    XMLDocument doc = null;
                    try {
                        doc = handler.getXMLDocument();
                        handler.prefetch();
                        //handler.initDocument();
                        handler.play();

                    } catch (Exception e) {
                        // XML exceptions handled by XMLContentHandler
                        //handleFetchError(e,link);
                        Log.error(e, "Error fetching document at: " + link.getURL().toString());
                        return;
                    }
                    //XMLDocument doc = fetchParseAndTransformDocument(link);
                    // if doc null, then error has already been reported by
                    // fetchParse and so on.
                    Log.debug("*** Secondary fetcher thread exiting");
                }
            };
            t.start();
        }
        return handler;

    }

    /**
     * Returns the state of the browser.
     * 
     * @return the current <code>state</code> of the browser
     * @see BrowserLogic
     */
    public int getState() {
        return state.getState();
    }

    /**
     * Show error page as an XSL-FO document.
     * 
     * @param heading
     *                   The heading
     * @param description
     *                   Description of the error.
     */
    public void showErrorDialog(String heading, String description) {
        showErrorDialog(heading, description, false);
    }

    /**
     * The general error dialog service. This is forwarded to the GUI, so GUI
     * should not call this.
     * 
     * Causes an error dialog to be shown.
     * 
     * The dialog has only one button, OK. When pressed, closes the dialog.
     * 
     * @param heading
     *                   a short cause for the error situation
     * @param description
     *                   a more detailed description of the error
     * @param dialog
     *                   force error as an annoying popup. This is automatically, if
     *                   can not display error document.
     */
    public void showErrorDialog(String heading, String description, boolean popup) {
        this.showErrorDialog(heading, description, popup, null);
    }

    /**
     * The general error dialog service. This is forwarded to the GUI, so GUI
     * should not call this.
     * 
     * Causes an error dialog to be shown.
     * 
     * The dialog has only one button, OK. When pressed, closes the dialog.
     * 
     * @param heading
     *                   a short cause for the error situation
     * @param description
     *                   a more detailed description of the error
     * @param dialog
     *                   force error as an annoying popup. This is automatically, if
     *                   can not display error document.
     */
    public void showErrorDialog(String heading, String description, boolean popup, Throwable exception) {
        // An error state might already be declared, but it doesn't hurt to
        // make sure.
        String stacktrace = "";
        if (exception != null) {
            StringWriter w = new StringWriter();
            exception.printStackTrace(new PrintWriter(w));
            stacktrace = w.toString();
        }
        state.setState(BrowserLogic.ERROROCCURED, heading);

        errorCount++;
        Log.debug("Error dialog called, errorcount: " + errorCount + " " + heading + " " + description + " popup:" + popup);
        if (errorCount == 1 && !popup) {
            try {
                URL ss1 = getXResource("xsmiles.error.stylesheet");

                String s1 = ss1.toExternalForm();

                Document errorDoc = this.getXMLParser().createEmptyDocument();
                errorDoc.appendChild(errorDoc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" title=\"desktop\" href=\"" + s1 + "\""));
                Element errorElem = errorDoc.createElement("error");
                errorDoc.appendChild(errorElem);

                Element titleElem = errorDoc.createElement("title");
                titleElem.appendChild(errorDoc.createTextNode(heading));
                errorElem.appendChild(titleElem);

                Element docUrlElem = errorDoc.createElement("documentURL");
                docUrlElem.appendChild(errorDoc.createTextNode(this.getDocumentHistory().getLastDocument().getURL().toString()));
                errorElem.appendChild(docUrlElem);

                Element versionElem = errorDoc.createElement("version");
                versionElem.appendChild(errorDoc.createTextNode(this.getVersion()));
                errorElem.appendChild(versionElem);

                Element descElem = errorDoc.createElement("description");
                descElem.appendChild(errorDoc.createTextNode(description));
                errorElem.appendChild(descElem);

                Element stackElem = errorDoc.createElement("stacktrace");
                stackElem.appendChild(errorDoc.createTextNode(stacktrace));
                errorElem.appendChild(stackElem);

                String errorPage = "data:application/xml," + this.getXMLParser().write(errorDoc, true);
                openLocation(new URL(errorPage), false, false);
                state.setState(BrowserLogic.STOPPED);
            } catch (Exception e) {
            	// for instance, in Applet the data: URL creation fails
                e.printStackTrace();
                Log.error("Exception while showing error dialog" + heading + ": " + description);
                if (guiManager != null && guiManager.getCurrentGUI() != null)
                    guiManager.getCurrentGUI().showErrorDialog(true, heading, description);
                errorCount = 0;
            }
        } else if (guiManager != null && guiManager.getCurrentGUI() != null)
            guiManager.getCurrentGUI().showErrorDialog(true, heading, description);
    }

    /**
     * The accessor method for the <code>MLFCManager</code>.
     * 
     * @return a reference to the system's <code>MLFCManager</code>
     * 
     * @see MLFCManager
     */
    /*
     * public MLFCManager getMLFCManager() { return mlfcManager;
     */

    public MLFCController getMLFCController() {
        if (this.currentDocument != null) {
            ExtendedDocument extdoc = (ExtendedDocument) this.currentDocument.getDocument();
            if (extdoc != null) {
                return extdoc.getHostMLFC().getMLFCController();
            }
        }
        return null;
    }

    /**
     * The accessor method for the <code>ContentManager</code>.
     * 
     * @return a reference to the system's <code>ContentManager</code>
     * 
     * @see MLFCManager
     */
    public ContentManager getContentManager() {
        return fContentManager;
    }

    /**
     * The accessor method for the <code>EventBroker</code>.
     * 
     * @return a reference to the system's <code>EventBroker</code>
     * 
     * @see EventBroker
     */
    public EventBroker getEventBroker() {
        return eventBroker;
    }

    /**
     * The accessor method for the <code>GUI</code>.
     * 
     * @return a reference to the core's GUI component
     * @see GUI
     * @deprecated
     */
    public GUIManager getGUIManager() {
        return guiManager;
    }

    /**
     * The accessor method for the <code>GUI</code>.
     * 
     * @return a reference to the core's GUI component
     * @see GUI
     */
    public GUI getCurrentGUI() {
        if (guiManager == null)
            Log.error("GUIMANAGER NULL. WHAT THE F?");
        return guiManager.getCurrentGUI();
    }

    /**
     * The contentArea of the browser
     */
    public Container getContentArea() {
        return (Container) contentArea;
    }

    /**
     * The MLFC Controls for the current document
     */
    public Container getControlsArea() {
        return (Container) controlsArea;
    }

    public ComponentFactory getComponentFactory() {
        return guiManager.getCurrentGUI().getComponentFactory();
    }
    
    public MLFCControls getMLFCControls() {
        return guiManager.getCurrentGUI().getMLFCControls();
    }

    /**
     * The accessor method for the <code>DocumentHistory</code>.
     * 
     * @return a reference to the system's <code>DocumentHistory</code>
     * @see DocumentHistory
     */
    public DocumentHistory getDocumentHistory() {
        return documentHistory;
    }

    /**
     * The accessor method for the <code>XMLConfigurer</code>.
     * 
     * @return a reference to the system's configurer
     * @see XMLConfigurer
     */
    public XMLConfigurer getBrowserConfigurer() {
        return browserConfigurer;
    }
    
    /**
     * The accessor method for the <code>XMLConfigurer</code>.
     * 
     * @return a reference to the system's configurer
     * @see XMLConfigurer
     */
    void setBrowserConfigurer(XMLConfigurer conf) {
        browserConfigurer=conf;
    }

    /**
     * The accessor method for the <code>XMLParser</code>.
     * 
     * @return a reference to the XMLParser Class
     * 
     * @see XMLParser
     */
    public XMLParser getXMLParser() {
        if (xmlParser == null) {
            Log.debug("Creating a new Parser");
            try {
                xmlParser = XMLParserFactory.createXMLParser(browserConfigurer.getProperty("main/parser"));
            } catch (Exception e) {
                Log.debug("Could not create Parser");
            }
        }
        return xmlParser;
    }

    /**
     * The accesor method for the <code>XSLEngine </code>.
     * 
     * @return a reference to the XSLEngine class
     * @see XSLEngine
     */
    public XSLTEngine getXSLEngine() {
        if (xslEngine == null) {
            try {
                Log.debug("Creating a new XSL engine");
                xslEngine = XSLTFactory.createXSLTEngine(browserConfigurer.getProperty("main/xslprocessor"));
            } catch (Exception e) {
                Log.debug("Could not create XSLT");
            }
        }
        return xslEngine;
    }

    /**
     * The browser is shut down.
     */
    public void closeBrowser() {
        state.setState(BrowserLogic.SHUTTINGDOWN);
        getContentManager().stopCurrentActiveContent();

        if (guiManager != null)
            guiManager.destroyOldGui();
        System.exit(0);
    }

    /**
     * @return path to config.xml file with configuration
     */
    /*
    public String getOptionsDocument() {
        return optionsDocument;
    }*/

    /**
     * <code>Browser</code>'s reaction to the Browser Commands events.
     * 
     * This method is called when the toolbar buttons are pressed or equivalent
     * menu items are selected.
     * 
     * @param browserCommandEventNumber
     *                   Browser Command object associated with the GUIActionEvent
     */
    public void navigate(int browserCommandEventNumber) {
        switch (browserCommandEventNumber) {
        case (NavigationState.BACK):
            /*
             * XLink l=documentHistory.backwardsInHistory(); if(l==null) {
             * Log.error("Cannot go back!"); } else { }
             */
            XLink link = documentHistory.backwardsInHistory();
            if (link != null)
                this.openLocation(link, false);
            break;
        case (NavigationState.FORWARD):
            XLink nlink = documentHistory.forwardInHistory();
            if (nlink != null)
                this.openLocation(nlink, false);
            break;
        case (NavigationState.STOP):
            // this will instruct the current thread to stop loading
            long num = this.enterFetcher();
        	if (currentThread != null) {
        	    currentThread.interrupt();
        	}
            this.exitFetcher(num);
            state.setState(BrowserLogic.STOPPED);
            break;
        case (NavigationState.RELOAD):
            Log.debug("Opening location " + lastTried);
            openLocation(currentPage, false);
            break;
        case (NavigationState.RELOAD_LAST_IN_HISTORY):
            Log.debug("Opening last in history" + getDocumentHistory().getLastDocument().getURLString());
            openLocation(getDocumentHistory().getLastDocument().getURL());
            break;
        case (NavigationState.HOME):
            openLocation(this.getBrowserConfigurer().getProperty("main/homepage"));
            break;
        }
        return;
    }

    /**
     * Reset current URI. This is a way for external modules to control the
     * appeareance of the URI after it has been set in document history, after
     * loading the document.
     * 
     * If a page has been loaded as a result of back or forwards command, then
     * <code>resetCurrentURI</code> has no effect, because documentHistory is
     * sacred, and it can not be modified, otherwise you will destroy the
     * future!
     *  
     */
    public void resetCurrenURI(XLink l) {

    }

    /**
     * @param s
     *                   Surf to the URI
     */
    public void openLocation(String s) {
        try {
            // mit? *?? relatiivinen _URI toimii n?in automaattisesti?
            if (getXMLDocument() != null)
                fetchProcessAndForkDocument(new XLink(new URL(getXMLDocument().getXMLURL(), s)), true, true);
            else
                fetchProcessAndForkDocument(new XLink(new URL(s)), true, true);
        } catch (MalformedURLException e) {
            //Log.error(e); // too ugly if file not found
            showErrorDialog("Slightly Mutilated URI Problem (SMUP).", "Try something else, " + s
                    + " didn't work so well. Check that the protocol declaration is correct.\n" + e.getMessage());
        }
    }

    /**
     * @param u
     *                   Surf to the URI
     */
    public void openLocation(URL u) {
        this.openLocation(u, true);
    }

    /**
     * @param u
     *                   Surf to the URI
     * @param save
     *                   Save in document history
     */
    public void openLocation(URL u, boolean save) {
        this.openLocation(new XLink(u), save);
    }

    /**
     * @param u
     *                   Surf to the Xlink
     * @param save
     *                   Save in document history
     */
    public void openLocation(XLink u, boolean save) {
        fetchProcessAndForkDocument(u, save, true);
    }

    /**
     * Open URL
     * 
     * @param u
     *                   The URL
     * @param c
     *                   Save in history
     * @param b
     *                   Display in GUI
     */
    public void openLocation(URL u, boolean c, boolean b) {
        fetchProcessAndForkDocument(new XLink(u), c, b);
    }

    /**
     * @return String containing description of the current View.
     */
    public String getCurrentView() {
        return currentView;
    }

    /**
     * @return The current viewtype
     */
    public int getViewType() {
        return viewType;
    }

    public double getJavaVersion() {
        return Utilities.getJavaVersion();
    }

    /**
     * @return current page as a XLink
     */
    public XLink getCurrentPage() {
        return currentPage;
    }

    /**
     * @return BrowserLogic, which controls the states of the Browser
     */
    public BrowserLogic getBrowserLogic() {
        return state;
    }

    /**
     * a new browser window created with default GUI
     */
    public void newBrowserWindow() {
        newBrowserWindow(this.getBrowserConfigurer().getProperty("main/homepage"));
    }

    /**
     * ?@param id the ID of the browser to get
     * 
     * @return the browser windows instance with the id, which resides closest
     *               in the browserWindow datastructure. If nothing found, then
     *               return null.
     */
    public BrowserWindow getBrowserWindow(String id) {
        return BrowserID.getClosestBrowser(browserTable.getBrowsersByID(id), this.browserID);
    }

    /**
     * @param url
     *                   The URL to open
     * @param id
     *                   The ID of the browser to open the url in. If BW with that id
     *                   is not found, a new one is spawned
     */
    public void openLocation(XLink url, String id) {
        BrowserWindow bw = BrowserID.getClosestBrowser(browserTable.getBrowsersByID(id), this.browserID);
        if (bw == null)
            //newBrowserWindow(url, id);
        	this.getCurrentGUI().openInNewWindow(url,id);
        else
            bw.openLocation(url,true);
    }

    /** which type of gui we should open when a new window is requested */
    public String getDefaultGUIName() {
        return getBrowserConfigurer().getProperty("gui/initialgui");
    }

    /**
     * Create a new top level browser, without parent.
     * 
     * @param url
     *                   The initial uri to open in the brand new BW
     * @param id
     *                   The id of the new BW
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
     */
    public void newBrowserWindow(String url, String id) {
        new Browser(url, getDefaultGUIName(), id, null, true);
    }

   /** 
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
    */
        public void newBrowserWindow(String url) {
        newBrowserWindow(url, null);
    }

    /**
     * Creates a new window. The id can be anything but the reserved words, for
     * targets:
     * 
     * <p>
     * The following <span class="index-inst" title="target frame::reserved
     * names|frame::list of reserved target names"> <a
     * name="idx-target_frame">target names </a> </span> are reserved and have
     * special meanings.
     * </p>
     * 
     * <dl>
     * <dt><strong>_blank </strong></dt>
     * 
     * <dd>The user agent should load the designated document in a new,
     * unnamed window.</dd>
     * 
     * <dt><strong>_self </strong></dt>
     * 
     * <dd>The user agent should load the document in the same frame as the
     * element that refers to this target.</dd>
     * 
     * <dt><strong>_parent </strong></dt>
     * 
     * <dd>The user agent should load the document into the immediate <a
     * href=" present/frames.html#edef-FRAMESET" class="noxref"> <samp
     * class="einst">FRAMESET </samp> </a> parent of the current frame. This
     * value is equivalent to <tt>_self</tt> if the current frame has no
     * parent.</dd>
     * 
     * <dt><strong>_top </strong></dt>
     * 
     * <dd>The user agent should load the document into the full, original
     * window (thus canceling all other frames). This value is equivalent to
     * <tt>_self</tt> if the current frame has no parent.</dd>
     * </dl>
     * 
     * @param displayGUI
     *                   Does the browser have a GUI
     * @param uri
     *                   Initial URI
     * @param id
     *                   of the gui
     * @param parent
     *                   The parent for the new browserWindow
     * @return a reference to the browser
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
     */
    public BrowserWindow newBrowserWindow(String uri, String id, boolean displayGUI, BrowserWindow parent) {
        return new Browser(uri, getDefaultGUIName(), id, parent, displayGUI);
    }

    /**
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
    */
    public BrowserWindow newBrowserWindow(String uri, String id, boolean displayGUI) {
        return newBrowserWindow(uri, id, displayGUI, this);
    }

    /**
     * @param displayGUI
     *                   Does the browser have a GUI
     * @param uri
     *                   Initial URI
     * @return a reference to the browser
     * NOTE! this will launch the old gui system
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
     */
    public BrowserWindow newBrowserWindow(String uri, boolean displayGUI) {
        return newBrowserWindow(uri, null, displayGUI, this);
    }

    /**
     * @param guiName
     *                   The name of the GUI to be used
     * @param uri
     *                   Initial URI
     * @param loadInitialDoc
     *                   Load the initial document
     * @return a reference to the browser
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
     */
    public BrowserWindow newBrowserWindowWithGUI(String uri, String guiName, boolean loadInitialDoc) {
        guiName=(guiName==null?"about":guiName);
        return new Browser(new XLink(uri), guiName, true,guiName, loadInitialDoc);
    }

    /**
     * @param displayGUI
     *                   Does the browser have a GUI
     * @param uri
     *                   Initial URI
     * @return a reference to the browser
    * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
     */
    public void newBrowserWindow(XLink uri) {
    	this.openLocation(uri,"_blank");
    }

    /**
     * @param title
     *                   Sets the preferred stylesheet title.
     */
    public void setPreferredStylesheetTitle(String title) {
        this.preferredStylesheetTitle = title;
    }

    /**
     * @return the preferred stylesheet title, chosen by the user
     */
    public String getPreferredStylesheetTitle() {
        return this.preferredStylesheetTitle;
    }

    /**
     * @param displayGUI
     *                   Does the browser have a GUI
     * @return a reference to the browser with initial page as the homepage
     * @deprecated  In the new gui system, use MLFCListeners events to open new windows and tabs
     */
    public BrowserWindow newBrowserWindow(boolean displayGUI) {
        return newBrowserWindow(null, null, displayGUI, this);
    }

    public void closeBrowserWindow() {
        browserTable.remove(this);
        if (state != null)
            state.setState(BrowserLogic.SHUTTINGDOWN);
        if (fContentManager != null) {
            getContentManager().stopCurrentActiveContent();
        }

        if (guiManager != null)
            guiManager.destroyOldGui();
        if (BrowserWindowList.browserWindows != null)
            BrowserWindowList.browserWindows.removeElement(this);
        guiManager = null;
        eventBroker = null;
        documentHistory = null;
        fContentManager = null;
        state = null;
        currentPage = null;
        currentDocument = null;
        MessageHandler = null;

        if (BrowserWindowList.browserWindows == null || BrowserWindowList.browserWindows.size() == 0)
            System.exit(0);
    }

    public BrowserID getID() {
        return browserID;
    }

    /**
     * Sets the MLFC mappings
     *  
     */
    public static void setMLFCMappings() {
        Log.debug("Registering namespace <-> MLFC mappings.");

        mappingsSet = true;
        String url, mlfc, root, name, enabled;
        NodeList mappings = null;
        Node node;
        Element m = null;
        Element e = browserConfigurer.findElement("mlfcmappings");

        // Check that mappings are there
        if (e == null) {
            Log.error("MLFC mappings missing in cfg/config.xml - NO RENDERERS!");
            return;
        }
        mappings = e.getChildNodes();

        for (int i = 0; i < mappings.getLength(); i++) {
            node = mappings.item(i);
            name = node.getLocalName();

            if (name != null && name.equals("namespace")) {
                m = (Element) node;
                enabled = m.getFirstChild().getNodeValue();
                if (enabled != null && enabled.equals("true")) {
                    url = m.getAttribute("url");
                    mlfc = m.getAttribute("mlfc");
                    XMLBroker.registerMLFCNS(url, mlfc);
                }
            } else if (name != null && name.equals("rootelement")) {
                m = (Element) node;
                enabled = m.getFirstChild().getNodeValue();
                if (enabled != null && enabled.equals("true")) {
                    m = (Element) node;
                    root = m.getAttribute("element");
                    mlfc = m.getAttribute("mlfc");
                    XMLBroker.registerMLFC(root, mlfc);
                }
            }
        }
    }

    /**
     * @return the duration of last page load in secs
     */
    public double getDocumentLoadDuration() {
        return lastDur;
    }

    /**
     * @param u
     *                   open a new URLCOnnection to this XLink url
     */
    public XSmilesConnection openConnection(XLink link) throws Exception {
        XSmilesConnection conn = null;
        switch (link.getMethod()) {
        case XLink.POST_HTTP:
            {
                conn = HTTP.post(link.getURL(), link.getPostData(), null, link.getContentType());
                // i18n: Parsing a document from a Reader assumes that
                //        the document is already in Unicode. This works
                //        fine with ISO-8859-1, but not with other
                //        encodings.
                //document = m_parser.openDocument(new
                // InputStreamReader(in),true);
                //document =
                // m_parser.openDocument(in,true,link.getURL().toString());
                break;
            }
        case XLink.PUT_HTTP:
            {
                conn = HTTP.put(link.getURL(), link.getPostData(), null, link.getContentType());
                break;
            }
        case XLink.FAKE_WITH_CONTENT:
        {
            conn = new XSmilesConnectionWithContent(link);
            break;
        }

        case XLink.POST_JMS:
            break;

        case XLink.POST_URLENCODED_HTTP:
            break;

        case XLink.POST_FORMDATA_HTTP:
            break;

        case XLink.POST_MULTIPART_HTTP:
            break;

        default:
            {
                conn = HTTP.get(link.getURL(), this);
            }
        }
        // check if we've been redirected, and update the link
        if (conn.getURL() != link.getURL()) {
            link.setURL(conn.getURL());
            getCurrentGUI().setLocation(conn.getURL().toExternalForm());
        }
        return conn;
    }

    /**
     * Enables and disables main browser window's Back and Forward buttons as
     * necessary. This method can be called to prevent incorrect access of the
     * document history.
     */
    public void checkButtons() {
        if (getDocumentHistory().canGoBack()) {
            this.getCurrentGUI().setEnabledBack(true);
        } else {
            this.getCurrentGUI().setEnabledBack(false);
        }
        if (getDocumentHistory().canGoForward()) {
            this.getCurrentGUI().setEnabledForward(true);
        } else {
            this.getCurrentGUI().setEnabledForward(false);
        }

    }

    /**
     * @return the MLFC listener attached with this browserwindow instance
     */
    public MLFCListener getMLFCListener() {
        return fMLFCListener;
    }

    /**
     * @param l
     *                   Set the mlfclistener associated with this browserwindow
     */
    public void setMLFCListener(MLFCListener l) {
        fMLFCListener = l;
    }

    // the zoom level is static now... may have to be changed.
    private static double zoomLevel = 1.0;

    /**
     * return the current zoom level
     */
    public double getZoom() {
        return zoomLevel;
    }

    /**
     * set the current zoom level
     */
    public void setZoom(double zoom) {
        zoomLevel = zoom;
        Log.debug("Current Zoom: " + zoomLevel);
        Enumeration contentEnum = this.fContentManager.getContentHandlers().elements();
        while (contentEnum.hasMoreElements())
        {
            XSmilesContentHandler ch = (XSmilesContentHandler)contentEnum.nextElement();
            ch.setZoom(zoom);
        }
        //this.fContentManager.getPrimaryContentHandler().setZoom(zoom);
    }


    public void registerFocusPointProvider(FocusPointsProvider fpp)
    {
        this.getCurrentGUI().registerFocusPointProvider(fpp, this);
    }

    public void unRegisterFocusPointProvider(FocusPointsProvider fpp)
    {
        this.getCurrentGUI().unRegisterFocusPointProvider(fpp, this);
    }

}
