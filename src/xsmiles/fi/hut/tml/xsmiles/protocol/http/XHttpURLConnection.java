/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
package fi.hut.tml.xsmiles.protocol.http;

import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.protocol.XURLStreamHandlerFactory;

import java.net.*;
//import java.security.Permission;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This is a wrapper for HttpURLConnection
 * We want to send CC/PP with the header
 */
public class XHttpURLConnection extends java.net.HttpURLConnection {

  // There is only one configurer for the whole browser.
  private static XMLConfigurer config;
  private static String profileURI="desktop"; // initially set to desktop, but reset before
                                              // every url connection, by corresponding GUI window
                                              // See rereadDeviceURL(String gui) 
	
  // This is fine for now, but in the future, there may be several Browsers. Then this is not ok.
  //  private Browser browser;

  public static void setConfigurer(XMLConfigurer conf) {
    config = conf;
  }

  private HttpURLConnection hc;

  public XHttpURLConnection(URL u) {
    super(u) ;
    try {
		
      URL newUrl = new URL("http"+u.toString().substring(4));

      //			URL newUrl = new URL(null, u.toString(), (URLStreamHandler)null);
      hc = (HttpURLConnection)newUrl.openConnection();
      hc.setFollowRedirects(true);

      if(config == null) {
	Log.error("CC/PP setup problem. Browser or config not set.");
	return;
      }

      if (!config.getProperty("ccpp/enabled").equals("true")) {
	Log.debug("CC/PP disabled.");
	return;
      }
				
      // Get the GUI title to select the correct CC/PP profile from the config.
      // Set CC/PP Header
      // Send Browser constrains
      hc.setRequestProperty("Opt", '"'+"http://www.w3.org/1999/06/24-CCPPexchange"+'"'+" ; ns=19");
      hc.setRequestProperty("19-Profile", '"'+profileURI+'"');
      // This profile-diff should be in config!!?
      String diffi=config.getProperty("ccpp/profilediff");
      if(diffi!=null&&!diffi.equals(""))
	hc.setRequestProperty("19-Profile-Diff-1", "<?xml version="+'"'+"1.0"+'"'+"?>"+diffi);
      /*else
	hc.setRequestProperty("19-Profile-Diff-1", "<?xml version="+'"'+"1.0"+'"'+"?><RDF><Description></Description></RDF>");*/
      // THIS SHOULD INCLUDE application/smil, scheme="modules.dtd"
      hc.setRequestProperty("Accept", "application/xml");
   
			
    } catch(IOException e) {
    }
    Log.debug("CC/PP sent.");
    return;
  }

  /**
   * Kludge to enable different CCPP URI in different 
   * BrowserWindows.
   */
  public static void rereadDeviceURI(String guiName) {
    try {
      if(config!=null)
	profileURI = config.getGUIProperty(guiName, "ccppInRDF");
      if (profileURI == null || profileURI.length() == 0) {
	profileURI = "";
	Log.error("CC/PP profile not set up in config.xml");
      }
    } catch(Exception e) {
      Log.error("Problems getting the CC/PP profile.");
      Log.error(e);
    }
  }

  public boolean usingProxy() {
    return hc.usingProxy();
  }

  public synchronized void disconnect() {
    hc.disconnect();
  }

  public void connect() throws IOException
  {
    hc.connect();
  }

//  public InputStream getErrorStream() {
//    return hc.getErrorStream();
//  }

  public static boolean getFollowRedirects() {
    return HttpURLConnection.getFollowRedirects();
  }
  public long getHeaderFieldDate(String name, long Default) {
    return hc.getHeaderFieldDate(name, Default);
  }
//  public boolean getInstanceFollowRedirects() {
//    return hc.getInstanceFollowRedirects();
//  }
//  public Permission getPermission() throws IOException {
//    return hc.getPermission();
//  }
  public String getRequestMethod() {
    return hc.getRequestMethod();
  }
  public int getResponseCode() throws IOException {
    return hc.getResponseCode();
  }
  public String getResponseMessage() throws IOException {
    return hc.getResponseMessage();
  }
  //	public static void setFollowRedirects(boolean set) {
  //		HttpURLConnection.setFollowRedirects(set);
  //		return;
  //    }
//  public void setInstanceFollowRedirects(boolean followRedirects) {
//    hc.setInstanceFollowRedirects(followRedirects);
//    return;
//  }
  public void setRequestMethod(String method) throws ProtocolException  {
    hc.setRequestMethod(method);
    return;
  }
  // URLConnection methods
  public boolean getAllowUserInteraction() {
    return hc.getAllowUserInteraction();
  }
  public Object getContent() throws IOException {
    return hc.getContent();
  }
	
//  public Object getContent(Class[] classes) throws IOException {
//    return hc.getContent(classes);
//  }
  public String getContentEncoding() {
    return hc.getContentEncoding();
  }
  public int getContentLength() {
    return hc.getContentLength();
  }
  public String getContentType() {
    return hc.getContentType();
  }
  public long getDate() {
    return hc.getDate();
  }
  public static boolean getDefaultAllowUserInteraction()  {
    return URLConnection.getDefaultAllowUserInteraction();
  }
  // This has been deprecated in URLConnection, but we still support it, 
  // in case someone happens to use it.
  public static String getDefaultRequestProperty(String key) {
    return URLConnection.getDefaultRequestProperty(key);
  }
  public boolean getDefaultUseCaches() {
    return hc.getDefaultUseCaches();
  }
  public boolean getDoInput() {
    return hc.getDoInput();
  }
  public boolean getDoOutput() {
    return hc.getDoOutput();
  }
  public long getExpiration() {
    return hc.getExpiration();
  }
  public static FileNameMap getFileNameMap() {
    return HttpURLConnection.getFileNameMap();
  }
  public String getHeaderField(int n) {
    return hc.getHeaderField(n);
  }
  public String getHeaderField(String name) {
    return hc.getHeaderField(name);
  }
  //	long getHeaderFieldDate(String name, long Default) {
  //		return hc.getHeaderFieldDate(name, Default);
  //  }
  public int getHeaderFieldInt(String name, int Default) {
    return hc.getHeaderFieldInt(name, Default);
  }
  public String getHeaderFieldKey(int n) {
    return hc.getHeaderFieldKey(n);
  }
  public long getIfModifiedSince() {
    return hc.getIfModifiedSince();
  }
  public InputStream getInputStream() throws IOException  {
    return hc.getInputStream();
  }
  public long getLastModified() {
    return hc.getLastModified();
  }
  public OutputStream getOutputStream() throws IOException {
    return hc.getOutputStream();
  }
  //	Permission getPermission() {
  //		return hc.getPermission();
  //  }
  public String getRequestProperty(String key) {
    return hc.getRequestProperty(key);
  }
  public URL getURL() {
    return hc.getURL();
  }
	
  public boolean getUseCaches() {
    return hc.getUseCaches();
  }
	
  //	protected static String guessContentTypeFromName(String fname) {
  //		return URLConnection.guessContentTypeFromName(fname);
  //    }
  //	public static String guessContentTypeFromStream(InputStream is) throws IOException {
  //		return URLConnection.guessContentTypeFromStream(is);
  //    }
  public void setAllowUserInteraction(boolean allowuserinteraction) {
    hc.setAllowUserInteraction(allowuserinteraction);
    return;
  }
  //	public static void setContentHandlerFactory(ContentHandlerFactory fac) {
  //		URLConnection.setContentHandlerFactory(fac);
  //		return;
  //    }
  //	public static void setDefaultAllowUserInteraction(boolean defaultallowuserinteraction) {
  //		URLConnection.setDefaultAllowUserInteraction(defaultallowuserinteraction);
  //		return;
  //    }
  //	public static void setDefaultRequestProperty(String key, String value) {
  //		URLConnection.setDefaultRequestProperty(key, value);
  //		return;
  //    }
  public void setDefaultUseCaches(boolean defaultusecaches) {
    hc.setDefaultUseCaches(defaultusecaches);
    return;
  }
  public void setDoInput(boolean doinput) {
    hc.setDoInput(doinput);
    return;
  }
  public void setDoOutput(boolean dooutput) {
    hc.setDoOutput(dooutput);
    return;
  }
  public static void setFileNameMap(FileNameMap map) {
    URLConnection.setFileNameMap(map);
    return;
  }
  public void setIfModifiedSince(long ifmodifiedsince) {
    hc.setIfModifiedSince(ifmodifiedsince);
    return;
  }
  public void setRequestProperty(String key, String value) {
    hc.setRequestProperty(key, value);
    return;
  }
  public void setUseCaches(boolean usecaches) {
    hc.setUseCaches(usecaches);
    return;
  }
  public String toString() {
    return hc.toString();
  }
}

