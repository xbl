/*
 * Created on 15.1.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.protocol.sip;

import fi.hut.tml.xsmiles.Log;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.ByteArrayInputStream;

import org.apache.xerces.parsers.*;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xml.serialize.OutputFormat;
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import java.io.StringWriter;

/**
 * Creates a new URL connection to handle the sip urls. In practice it feeds the browser
 * a new xml document that opens the SIP MLFC with the necessary parameters.
 * 
 * @author ssundell
 *
 */
public class SipUrlConnection extends URLConnection
{
	private Document commDoc;
	 
    private String sipuri;
    private String contentType = "text/xml";    
    
	private StringWriter stringie = new StringWriter();
	private XMLSerializer serializer = new XMLSerializer(stringie, new OutputFormat("XML", "UTF-8", false));
    
    /**
     * @param url The sip URL that invokes the creation of the new UrlConnection class. No checks as of yet.
     */
    public SipUrlConnection(URL url)
    {
        // TODO: Url sensibility checking?
        super(url);
        sipuri = url.toString();
        
    	Log.debug("Creating a new COMM document");
		DOMParser parser = new DOMParser();
        
		try {
			parser.setFeature("http://xml.org/sax/features/validation", false);
			parser.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			parser.setFeature("http://xml.org/sax/features/external-general-entities", false);
			parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			parser.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			
			//TODO: Change the filename to be something configurable.
			
			if (!url.getPath().trim().equals(""))
				parser.parse(new InputSource(new FileInputStream("cfg/comm.xml")));
			else
				parser.parse(new InputSource(new FileInputStream("cfg/commABook.xml")));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		commDoc = parser.getDocument();
		Node commRoot = commDoc.getElementsByTagNameNS("http://www.x-smiles.org/2004/comm", "comm").item(0);
		String prefix = commRoot.getPrefix();
		if (prefix == null || prefix == "")
			prefix = "";
		else
			prefix = prefix + ":";
		
		
		Element commSession = commDoc.createElementNS("http://www.x-smiles.org/2004/comm", prefix + "commsession");
		commSession.setAttribute("sessiontype", "sip");

		// This actually an ugly way to do this, but so far, there's nothing
		// else to make this work...
		if (url.getPath().trim().equals(""))
		{
			commSession.setAttribute("main", "true");
			commSession.appendChild(commDoc.createElementNS("http://www.x-smiles.org/2004/comm", prefix + "addressbook"));
		}
		else
		{
			Element session = commDoc.createElementNS("http://www.x-smiles.org/2004/comm", prefix + "session");
			Element user = commDoc.createElementNS("http://www.x-smiles.org/2004/comm", prefix + "user");
			user.setAttribute("address", sipuri);
			session.appendChild(user);
			commSession.appendChild(session);
		}
		
		commRoot.appendChild(commSession);
    }
    
    /* (non-Javadoc)
     * @see java.net.URLConnection#connect()
     */
    public void connect() throws IOException
    {
        connected = true;
    }
     
    public String getContentType()
    {
        return contentType;
    }
     
    synchronized public InputStream getInputStream() throws IOException
    {
        if (!connected) connect();
        
		stringie.flush();
        
		try {
			serializer.serialize(commDoc);
		}
		catch (Exception e)
		{
			Log.debug("Error while creating a new COMM document");
			e.printStackTrace();
			return null;
		}
        
        return new ByteArrayInputStream(stringie.toString().getBytes());
    }
}
