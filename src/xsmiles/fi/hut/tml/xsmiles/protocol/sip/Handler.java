/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
 
package fi.hut.tml.xsmiles.protocol.sip;

import java.net.*;

/**
 * Dummy class to make URL class to carry SIP addresses
 * @author ppeisa
 */

public class Handler extends URLStreamHandler
{
	public Handler()
	{
	}
	
	protected URLConnection openConnection(URL u)
	{
		return new SipUrlConnection(u);
	}
}