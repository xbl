/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
package fi.hut.tml.xsmiles.protocol.data;

import java.net.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import fi.hut.tml.xsmiles.Log;

class DataURLConnection extends URLConnection {

	String contentType;
	String content;
    byte[] contentBytes;
    ByteArrayInputStream is;

    DataURLConnection(URL url) throws IOException {
		super(url);
//		String str = url.getPath();
		// JDK 1.1
		String str = url.getFile();

		contentType = null;
		content = null;

		// Get contentType and content
		try {		
			contentType = str.substring(0,str.indexOf(','));
			content = str.substring(str.indexOf(',')+1);
			
			// Remove parameters from content type
			if (contentType.indexOf(';') >= 0) {
				contentType = str.substring(0,str.indexOf(';'));
			}
		} catch (IndexOutOfBoundsException e) {
		}

		if (contentType == null || contentType.equals(""))
			contentType = "text/plain";
		if (content == null)
			content = "";

        // Replace "%20" with ' ' - already processed at gui.media.TextMedia
        //int i = -1;
        //while ((i = content.indexOf("%20")) != -1)
        //    content = content.substring(0, i) + " " + content.substring(i+3);

        try { 
			contentBytes = content.getBytes();
			is = new ByteArrayInputStream(contentBytes);
        } catch (Exception e) {
			Log.error("ByteArrayInputStream creation failed for data: URL");
		}
    }
    
    synchronized public void connect() throws IOException {
        
        connected=true;
    }
    
    synchronized public InputStream getInputStream() throws IOException {
        if (!connected) connect();
        return is;
    }

    public String getContentType() {
        return contentType;
    }
	
	public void setRequestHeader(String name, String value) { 
	}
}



