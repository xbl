/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
 
package fi.hut.tml.xsmiles.protocol.data;

import java.net.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.protocol.data.DataURLConnection;
import java.io.IOException;

/**
 * Class to make URL class to implement data: protocol
 * @author kari
 */

public class Handler extends URLStreamHandler
{

	public Handler()
	{
	}

	protected void parseURL(URL u, String spec, int start, int limit) {
		String data;
//		super.parseURL(u,spec,start,limit);

		// Set parsed data
		try {
			data = spec.substring(start);
		} catch (IndexOutOfBoundsException e) {
			data = "";
		}
		
		//setURL(u, "data", null, 0, null, null, data, null, null); 
		//JDK 11
		setURL(u, "data", null, 0, data, null); 
		return;
	}

	protected URLConnection openConnection(URL u)
	{
		  try {
		     return new DataURLConnection(u);   
		  } catch (IOException e) {
		  	return null;
		  }
	}
}

