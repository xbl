/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
package fi.hut.tml.xsmiles.protocol.dir;

import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.protocol.XURLStreamHandlerFactory;

import java.net.*;
//import java.security.Permission;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import java.io.IOException;
import java.io.StringBufferInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;

// To remove Java 1.2 dependency (class Arrays)
import java.text.Collator;
import java.text.CollationKey;
import java.util.Vector;
import java.util.Locale;

/**
 * This is a implementation for dir: protocol
 * We want to receive a HTML page for directories.
 * This is a very very ugly implementation, but
 * works in Windows and Linux.
 */
public class DirURLConnection extends java.net.HttpURLConnection {

	private String fileDoc = null;
	private URL url;

	public DirURLConnection(URL u)
	{
		super(u) ;

		url = u;
	    String[] fileList;
		
		File f;
	    try {
		    // Create doc
		URL ss1=Browser.getXResource("xsmiles.dir.stylesheet");
		String s1=ss1.toExternalForm();

	    	fileDoc = new String("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><?xml-stylesheet title=\"Dir\" type=\"text/xsl\" href=\"" + s1 + "\"?><folder>");

			String dirPath = "???";
			dirPath = u.toString().substring(4);

			// Get those ugly windows slashes correct
			dirPath = dirPath.replace('\\','/');

			// Remove head & tail slashes - only for Windows
			while (dirPath.charAt(0) == '/' && File.separatorChar == '\\')
				dirPath = dirPath.substring(1);
			while (dirPath.endsWith("/"))
				dirPath = dirPath.substring(0, dirPath.length()-1);

			// Add a '/' after 'dir:d:' or 'dir:c:' or 'dir:'
			if (dirPath.endsWith(":"))
				dirPath = dirPath + "/";

			fileDoc = fileDoc + "<name>"+dirPath+"</name>";

			// Fake url
			URL newUrl = new URL("http://www.xsmiles.org/index.html");
			//			URL newUrl = new URL(null, u.toString(), (URLStreamHandler)null);

			// Get list of file names
			File dir = new File(dirPath);
			if (dir.isDirectory() == false) {
				Log.error("dir protocol: NOT A DIRECTORY: "+dir.toString());
				fileDoc = fileDoc + "<error>Not a directory: "+u.toString().substring(4)+"</error></folder>";
				return;
			}

			fileList = dir.list();
			// Sort them (not 1.2, but 1.1 way)
//			Arrays.sort(fileList);
			fileList = sort(fileList);
			// Add parent
			String parent = dir.getParent();
			if (parent != null) {
				// Get those ugly windows slashes correct
				parent = parent.replace('\\','/');
				fileDoc = fileDoc + "<parent href=\"dir:/" +parent + "\">UP</parent>";
			}

			for (int i=0 ; i < fileList.length ; i++) {
				f = new File(dirPath,fileList[i]);
				if (f.isDirectory())
					fileDoc = fileDoc + "<dir href=\"dir:/" +dirPath+"/"+fileList[i] + "\">" + fileList[i] +"</dir>";
				else
					fileDoc = fileDoc + "<file href=\"file:" +dirPath+"/"+fileList[i] + "\">" + fileList[i] +"</file>";
			}
			fileDoc = fileDoc + "</folder>";

			Log.debug("DIR opened:"+dirPath);

		} catch(Exception e) {
			Log.debug("Dir exception:"+e.toString());
		    fileDoc = fileDoc + "<name>"+u.toString().substring(4)+"</name>";
			fileDoc = fileDoc + "<error>Invalid directory: "+u.toString().substring(4)+"</error></folder>";
		}
	    return;
	}

//--------------------------------------------------------------------------
// Method to sort a vector of strings. This sort uses CollationKeys, where 
// are about 10 times faster than using Collator.compare. This is true even 
// including the overhead of setting up the extra arrays, etc.
// It is a simple bubble sort, which is faster than other algorithms
// for very small vectors and for vectors that are almost in order already
// This sample illustrates use of CollationKey class

    public static String[] sort (String[] unsorted) {
      CollationKey temp;
        int i,j;
        int size = unsorted.length;
		String[] sorted = new String[unsorted.length];

        Collator collator = Collator.getInstance(Locale.getDefault());
        CollationKey[] keys = new CollationKey[size];
        for (i=0; i<size; i++)
          keys[i] = collator.getCollationKey((String)(unsorted[i]));
        for (i=0; i<size; i++)
            for (j=i; j<size; j++)
               if( keys[i].compareTo( keys[j] ) > 0 ) {
                    temp = keys[j];
                    keys[j] = keys[i];
                    keys[i] = temp;
                }    
        for (i=0; i<size; i++)
            sorted[i] = keys[i].getSourceString();

        return sorted;
    }

	public boolean usingProxy()
	{
		return false;
	}

	public synchronized void disconnect()
	{
		fileDoc = null;
		return;
	}

	public void connect() throws IOException
	{
		return;
	}

	//  public InputStream getErrorStream() {
	//    return hc.getErrorStream();
	//  }

	public static boolean getFollowRedirects()
	{
		return false;
	}
	public long getHeaderFieldDate(String name, long Default)
	{
		return 0;
	}
	//  public boolean getInstanceFollowRedirects() {
	//    return hc.getInstanceFollowRedirects();
	//  }
	//  public Permission getPermission() throws IOException {
	//    return hc.getPermission();
	//  }
	public String getRequestMethod()
	{
		return "GET";
	}
	public int getResponseCode() throws IOException
	{
		return 200;
	}
	public String getResponseMessage() throws IOException
	{
		return "OK";
	}
	//	public static void setFollowRedirects(boolean set) {
	//		HttpURLConnection.setFollowRedirects(set);
	//		return;
	//    }
	//  public void setInstanceFollowRedirects(boolean followRedirects) {
	//    hc.setInstanceFollowRedirects(followRedirects);
	//    return;
	//  }
	public void setRequestMethod(String method) throws ProtocolException
	{
		return;
	}
	// URLConnection methods
	public boolean getAllowUserInteraction()
	{
		return getDefaultAllowUserInteraction();
	}
	public Object getContent() throws IOException
	{
		return fileDoc;
	}

	//  public Object getContent(Class[] classes) throws IOException {
	//    return hc.getContent(classes);
	//  }
	public String getContentEncoding()
	{
		return null;
	}
	public int getContentLength()
	{
		return fileDoc.length();
	}
	public String getContentType()
	{
		return "application/xml";
	}
	public long getDate()
	{
		return 0;
	}
	public static boolean getDefaultAllowUserInteraction()
	{
		return URLConnection.getDefaultAllowUserInteraction();
	}
	// This has been deprecated in URLConnection, but we still support it, 
	// in case someone happens to use it.
	public static String getDefaultRequestProperty(String key)
	{
		return URLConnection.getDefaultRequestProperty(key);
	}
	public boolean getDefaultUseCaches()
	{
		return false;
	}
	public boolean getDoInput()
	{
		return false;
	}
	public boolean getDoOutput()
	{
		return false;
	}
	public long getExpiration()
	{
		return 0;
	}
	public static FileNameMap getFileNameMap()
	{
		return HttpURLConnection.getFileNameMap();
	}
	public String getHeaderField(int n)
	{
		return null;
	}
	public String getHeaderField(String name)
	{
		return null;
	}
	//	long getHeaderFieldDate(String name, long Default) {
	//		return hc.getHeaderFieldDate(name, Default);
	//  }
	public int getHeaderFieldInt(String name, int Default)
	{
		return 0;
	}
	public String getHeaderFieldKey(int n)
	{
		return null;
	}
	public long getIfModifiedSince()
	{
		return 0;
	}
	public InputStream getInputStream() throws IOException
	{
		return new StringBufferInputStream(fileDoc);
	}
	public long getLastModified()
	{
		return 0;
	}
	public OutputStream getOutputStream() throws IOException
	{
		return null;
	}
	//	Permission getPermission() {
	//		return hc.getPermission();
	//  }
	public String getRequestProperty(String key)
	{
		return null;
	}
	public URL getURL()
	{
		return url;
	}

	public boolean getUseCaches()
	{
		return false;
	}

	//	protected static String guessContentTypeFromName(String fname) {
	//		return URLConnection.guessContentTypeFromName(fname);
	//    }
	//	public static String guessContentTypeFromStream(InputStream is) throws IOException {
	//		return URLConnection.guessContentTypeFromStream(is);
	//    }
	public void setAllowUserInteraction(boolean allowuserinteraction)
	{
		return;
	}
	//	public static void setContentHandlerFactory(ContentHandlerFactory fac) {
	//		URLConnection.setContentHandlerFactory(fac);
	//		return;
	//    }
	//	public static void setDefaultAllowUserInteraction(boolean defaultallowuserinteraction) {
	//		URLConnection.setDefaultAllowUserInteraction(defaultallowuserinteraction);
	//		return;
	//    }
	//	public static void setDefaultRequestProperty(String key, String value) {
	//		URLConnection.setDefaultRequestProperty(key, value);
	//		return;
	//    }
	public void setDefaultUseCaches(boolean defaultusecaches)
	{
		return;
	}
	public void setDoInput(boolean doinput)
	{
		return;
	}
	public void setDoOutput(boolean dooutput)
	{
		return;
	}
	public static void setFileNameMap(FileNameMap map)
	{
		return;
	}
	public void setIfModifiedSince(long ifmodifiedsince)
	{
		return;
	}
	public void setRequestProperty(String key, String value)
	{
		return;
	}
	public void setUseCaches(boolean usecaches)
	{
		return;
	}
	//  public String toString() {
	//    return fileDoc;
	//  }
}


