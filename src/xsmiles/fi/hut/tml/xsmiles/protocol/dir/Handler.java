/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */



package fi.hut.tml.xsmiles.protocol.dir;
import java.net.*;
import java.util.*;
import java.io.*;
import java.security.*;

import fi.hut.tml.xsmiles.Log;

public class Handler extends URLStreamHandler {
    
    protected URLConnection openConnection(URL url) throws IOException {
      return new DirURLConnection(url);
    }
}

