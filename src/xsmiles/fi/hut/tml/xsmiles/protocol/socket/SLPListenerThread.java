package fi.hut.tml.xsmiles.protocol.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;

import fi.hut.tml.xsmiles.Log;

public class SLPListenerThread extends Thread
{
    Socket socket = null;

    SLPSocketListener socketListener;

    public SLPListenerThread(Socket socket, SLPSocketListener sl)
    {
        super("SLPListenerThread");
        this.socket = socket;
        this.socketListener = sl;
    }

    public void run()
    {
        BufferedReader input;
        String msg;
        try
        {
            input = new BufferedReader(new InputStreamReader(socket
                    .getInputStream()));
            while (true)
            {
                msg = input.readLine();
                if (msg != null)
                {
                    Log.debug("SLP message: " + msg);
                    StringTokenizer tokens = new StringTokenizer(msg, ";");
                    socketListener.createSubscriber(tokens.nextToken(), tokens
                            .nextToken(), tokens.nextToken());
                }
                else
                    break;
            }
            socket.close();
            Log.debug("Socket in a thread closed.");
        }
        catch (IOException e)
        {
            Log.error(e);
        }

    }

}
