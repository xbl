/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Feb 14, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import fi.hut.tml.xsmiles.Log;

public class ConnectionThread extends Thread
{
    private ServerSocket serverSocket = null;
    SocketListener socketListener;
    int port;

    public ConnectionThread(int port, SocketListener sl)
    {
        super("ConnectionThread");
        this.port = port;
        this.socketListener = sl;

        try
        {
            serverSocket = new ServerSocket(port);
        }
        catch (IOException e)
        {
            Log.error("Could not listen on port: " + port);
        }

    }

    public void run()
    {
        while (true)
        {
            try
            {
                Socket clientSocket = serverSocket.accept();
                Thread listener = socketListener.createListener(clientSocket);
                listener.start();
            }
            catch (IOException e)
            {
                Log.error("Accept failed on port: " + port);
            }

            
        }
    }
}
