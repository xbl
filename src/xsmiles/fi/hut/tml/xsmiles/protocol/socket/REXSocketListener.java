/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 16, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.io.InputStream;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;

import org.jaxen.BaseXPath;
import org.jaxen.SimpleNamespaceContext;
import org.jaxen.XPath;
import org.jaxen.dom.DocumentNavigator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.BrowserTable;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen.BaseXPathEx;

public class REXSocketListener extends SocketListener
{
    public REXSocketListener(int port, BrowserTable bt)
    {
        super(port, bt);
    }

    public Thread createListener(Socket clientSocket)
    {
        // TODO Auto-generated method stub
        return new REXListenerThread(clientSocket, this);
    }

    /**
     * @param doc
     * @param serviceId
     */
    public void createEvent(InputStream is)
    {
        BrowserWindow bw = browserTable.getLatestBrowserWindow();
        // Log.debug(browserTable.getBrowsersByID("check-in").toString());
        // BrowserWindow bw =
        // (BrowserWindow)browserTable.getBrowsersByID("check-in").get(0);

        if (bw != null)
        {
            XSmilesXMLDocument xDoc = new XSmilesXMLDocument(bw, is);
            try
            {
                xDoc.retrieveDocument();
            }
            catch (Exception e)
            {
                Log.error(e);
            }

            Document eventDoc = xDoc.getDocument();
            Log.debug("New doc: " + eventDoc);

            Node serviceNode = eventDoc.getElementsByTagName("service").item(0)
                    .getFirstChild();
            String serviceId = serviceNode.getNodeValue();
            Log.debug("Service id from SOAP: " + serviceId);

            Vector browsers = browserTable.getBrowsersByID(serviceId);
            Document doc = ((BrowserWindow) (browsers.get(0))).getXMLDocument()
                    .getDocument();
            NodeList events = eventDoc.getElementsByTagName("event");
            Element event;

            for (int i = 0; i < events.getLength(); i++)
            {

                event = (Element) events.item(i);
                String id = event.getAttribute("target");
                String type = event.getAttribute("name");

                Log.debug("REX event " + type + " to element with id: " + id);

                BaseXPath xpath = null;
                Element target = null;
                try
                {
                    xpath = new BaseXPath(id, new DocumentNavigator());
                    xpath.addNamespace("x", "http://www.w3.org/1999/xhtml");
                    Log.debug("XPATH: " + xpath.debug()); // TODO: error in
                    // namespace description
                    target = (Element) xpath.selectSingleNode(doc);
                }
                catch (Exception e)
                {
                    Log.error(e);
                }

                // Element target = doc.getElementById(id);
                Element eventClone = (Element) event.cloneNode(true);
                Node adopted = doc.adoptNode(eventClone);

                if (adopted != null && target != null)
                {

                    if (type.equals("DOMNodeRemoved"))
                    {
                        NodeList children = eventClone.getChildNodes();
                        Node parent = target.getParentNode();
                        int j = 0;
                        Log.debug("Removing " + target);
                        //parent.removeChild(target);
                        while (children.getLength() > j)
                        {
                            if (children.item(j) instanceof Element)
                            {
                                Log.debug("Inserting " + children.item(j)
                                        + " before " + target);
                                //parent.insertBefore(children.item(j), target);
                                parent.replaceChild(children.item(j), target);
                            }
                            else
                                j++;
                        }
                    }
                    else if (type.equals("DOMNodeInserted"))
                    {
                        NodeList children = eventClone.getChildNodes();
                        while (children.getLength() > 0)
                        {
                            target.appendChild(children.item(0));
                        }
                    }
                    else if (type.equals("DOMAttrModified"))
                    {
                        String attr = eventClone.getAttribute("attrName");
                        String value = eventClone.getAttribute("newValue");
                        target.setAttribute(attr, value);
                    }
                    else if (type.equals("DOMCharacterDataModified"))
                    {
                        Log
                                .error("Event not delivered, DOMCharacterDataModified for REX not implemented yet");
                    }
                }
                else
                {
                    Log.error("Cannot adopt or cannot find target" + eventClone);
                }
            }
        }
    }
}
