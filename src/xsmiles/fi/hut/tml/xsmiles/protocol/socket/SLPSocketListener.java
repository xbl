package fi.hut.tml.xsmiles.protocol.socket;

import java.net.Socket;

import fi.hut.tml.xsmiles.BrowserTable;
import fi.hut.tml.xsmiles.protocol.wesahmi.BrowserSubscriber;

public class SLPSocketListener extends SocketListener
{

    public SLPSocketListener(int port, BrowserTable bt)
    {
        super(port, bt);
        // TODO Auto-generated constructor stub
    }

    public Thread createListener(Socket clientSocket)
    {
        return new SLPListenerThread(clientSocket, this);
    }

    public void createSubscriber(String sipAddress, String serviceId, String url)
    {
        BrowserSubscriber browserSubscriber = new BrowserSubscriber(browserTable,
                sipAddress, serviceId, url, url, new String[1], new String[1]);
        browserSubscriber.registerToService();
    }
}
