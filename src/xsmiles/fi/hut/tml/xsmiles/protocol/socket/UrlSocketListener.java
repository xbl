/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Feb 14, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.net.Socket;

import fi.hut.tml.xsmiles.BrowserTable;
import fi.hut.tml.xsmiles.Log;

public class UrlSocketListener extends SocketListener
{

    public UrlSocketListener(int port, BrowserTable bt)
    {
        super(port, bt);
    }

    public void openLocation(String location)
    {
        try
        {
            browserTable.getLatestBrowserWindow().getMLFCListener()
                    .openURLFromExternalProgram(location);
        }
        catch (Exception e)
        {
            Log.error("UrlSocketListener: " + e);
        }
    }

    public Thread createListener(Socket clientSocket)
    {
        return new UrlListenerThread(clientSocket, this);
    }
}
