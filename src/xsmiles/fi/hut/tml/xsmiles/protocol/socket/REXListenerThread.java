/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 16, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import fi.hut.tml.xsmiles.Log;

public class REXListenerThread extends Thread
{
    Socket socket = null;
    REXSocketListener socketListener;

    public REXListenerThread(Socket socket, REXSocketListener sl)
    {
        super("REXListenerThread");
        this.socket = socket;
        this.socketListener = sl;
    }

    public void run()
    {
        BufferedReader input;
        InputStream stream, docStream;
        try
        {
            Log.debug("REXListenerThread reads the message...");
            stream = socket.getInputStream();
            //docStream = socket.getInputStream();
            
/*            byte[] buf = new byte[4096];
            //InputStream is; // your input stream
            //URI uri = new URI("file", "util/REXEvent.xml", null);
            //Log.debug("File: "+uri);
            OutputStream os = new FileOutputStream(new File(".", "util/REXEvent.xml")); // your output stream
            for(int len=-1;(len=stream.read(buf))!=-1;)
            {
                Log.debug(buf.toString());
                os.write(buf,0,len);
            }
                    
            os.close();
            
            input = new BufferedReader(new InputStreamReader(stream));
            DOMParser parser = new DOMParser();
            parser.parse(new InputSource(input));
            DocumentImpl soapDoc = (DocumentImpl) parser.getDocument();

            // Log.debug("SOAP message: "+soapDoc.toString());
//            Node eventElement = soapDoc.getElementsByTagName("event").item(0).cloneNode(true);
            
            Node serviceElement = soapDoc.getElementsByTagName("service").item(0).getFirstChild();
            String id = serviceElement.getNodeValue();
            Log.debug("Service id from SOAP: " + id);
            */
            
            //socketListener.createEvent(soapDoc, id);
            socketListener.createEvent(stream);

            socket.close();
            Log.debug("Socket in a REX listener thread closed.");
        }
        catch (Exception e)
        {
            Log.error(e);
        }

    }
}