/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 16, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import fi.hut.tml.xsmiles.Log;

public class SoapListenerThread extends Thread
{
    Socket socket = null;
    SoapSocketListener socketListener;

    public SoapListenerThread(Socket socket, SoapSocketListener sl)
    {
        super("SoapListenerThread");
        this.socket = socket;
        this.socketListener = sl;
    }

    public void run()
    {
        BufferedReader input;
        try
        {
            Log.debug("SoapListenerThread reads the message...");
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            DOMParser parser = new DOMParser();
            parser.parse(new InputSource(input));
            DocumentImpl soapDoc = (DocumentImpl) parser.getDocument();

            // Log.debug("SOAP message: "+soapDoc.toString());
            Node urlElement = soapDoc.getElementsByTagName("url").item(0).getFirstChild();
            String url = urlElement.getNodeValue();
            Log.debug("URL from SOAP: " + url);

            Node serviceElement = soapDoc.getElementsByTagName("service").item(0).getFirstChild();
            String id = serviceElement.getNodeValue();
            Log.debug("Service id from SOAP: " + id);
            
            socketListener.openLocation(id, url);

            socket.close();
            Log.debug("Socket in a SOAP listener thread closed.");
        }
        catch (Exception e)
        {
            Log.error(e);
        }

    }
}
