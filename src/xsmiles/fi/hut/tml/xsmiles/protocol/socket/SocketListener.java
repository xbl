/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 16, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.net.Socket;

import fi.hut.tml.xsmiles.BrowserTable;

public abstract class SocketListener
{
    int port;
    BrowserTable browserTable;
    Thread connThread;
    Socket clientSocket;

    public SocketListener(int port, BrowserTable bt)
    {
        this.port = port;
        browserTable = bt;
        connThread = new ConnectionThread(port, this);
        connThread.start();
    }

    public abstract Thread createListener(Socket clientSocket);
}
