/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Feb 14, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import fi.hut.tml.xsmiles.Log;

public class UrlListenerThread extends Thread
{
    Socket socket = null;
    UrlSocketListener socketListener;

    public UrlListenerThread(Socket socket, UrlSocketListener sl)
    {
        super("UrlListenerThread");
        this.socket = socket;
        this.socketListener = sl;
    }

    public void run()
    {
        BufferedReader input;
        String url;
        try
        {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true)
            {
                url = input.readLine();
                if (url != null)
                {
                    Log.debug("URL from socket: " + url);
                    socketListener.openLocation(url);
                }
                else
                    break;
            }
            socket.close();
            Log.debug("Socket in a thread closed.");
        }
        catch (IOException e)
        {
            Log.error(e);
        }

    }
}
