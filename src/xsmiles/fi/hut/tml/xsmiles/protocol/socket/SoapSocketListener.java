/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 16, 2006
 */
package fi.hut.tml.xsmiles.protocol.socket;

import java.net.Socket;
import java.util.Vector;

import fi.hut.tml.xsmiles.BrowserTable;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;

public class SoapSocketListener extends SocketListener
{
    public SoapSocketListener(int port, BrowserTable bt)
    {
        super(port, bt);
    }

    public Thread createListener(Socket clientSocket)
    {
        // TODO Auto-generated method stub
        return new SoapListenerThread(clientSocket, this);
    }

    public void openLocation(String serviceId, String url)
    {
        Vector browsers = browserTable.getBrowsersByID(serviceId);

        if (browsers == null)
        {
            try
            {
                // NOTE: asks for latest browser window
                browserTable.getLatestBrowserWindow().getMLFCListener().openURLFromExternalProgram(url,
                        serviceId);
//                browserTable.getLatestBrowserWindow().getMLFCListener().reloadCurrentPage(serviceId);
            }
            catch (Exception e)
            {
                Log.error("SoapSocketListener: " + e);
            }
        }
        else
        {
            try
            {
                //((BrowserWindow) (browsers.get(0))).getMLFCListener().openLocation(url);
                ((BrowserWindow) (browsers.get(0))).getMLFCListener().reloadCurrentPage();
            }
            catch (Exception e)
            {
                Log.error("SoapSocketListener: " + e);
            }
        }

    }
    
    public void createSubscriber(BrowserTable bt, String address, String service, String url)
    {
        
    }

}
