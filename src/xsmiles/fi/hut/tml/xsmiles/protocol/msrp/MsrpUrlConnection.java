/*
 * Created on 11.5.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.protocol.msrp;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author ssundell
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MsrpUrlConnection extends URLConnection
{

    public MsrpUrlConnection (URL u)
    {
        super(u);
    }

    /* (non-Javadoc)
     * @see java.net.URLConnection#connect()
     */
    public void connect() throws IOException
    {
        // TODO Auto-generated method stub

    }

}
