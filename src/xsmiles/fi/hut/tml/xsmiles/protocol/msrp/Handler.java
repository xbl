/*
 * Created on 11.5.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.protocol.msrp;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * @author ssundell
 *
 */
public class Handler extends URLStreamHandler
{

    public Handler()
    {
    }
    
    protected URLConnection openConnection(URL u)
    {
        return new MsrpUrlConnection(u);
    }

}
