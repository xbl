/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
package fi.hut.tml.xsmiles.protocol.rtsp;

import java.net.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import fi.hut.tml.xsmiles.Log;

/**
 * Dummy class to get rtsp URLs through browser to the JMF player.
 */
class RtspURLConnection extends URLConnection {

	String contentType;
	String content;
    byte[] contentBytes;
    ByteArrayInputStream is;

    RtspURLConnection(URL url) throws IOException {
		super(url);

		// Get contentType and content
		contentType = "video/quicktime";
		content = "dummy text";

        try { 
			contentBytes = content.getBytes();
			is = new ByteArrayInputStream(contentBytes);
        } catch (Exception e) {
			Log.error("ByteArrayInputStream creation failed for rtsp: URL");
		}
    }
    
    synchronized public void connect() throws IOException {
        connected=true;
    }
    
    synchronized public InputStream getInputStream() throws IOException {
        if (!connected) connect();
        return is;
    }

    public String getContentType() {
        return contentType;
    }
	
//	public void setRequestHeader(String name, String value) { 
//	}
}



