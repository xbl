/*
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources."
 */
 
 
package fi.hut.tml.xsmiles.protocol.rtsp;

import java.net.*;
import java.io.IOException;

/**
 * Dummy class to make URL class to carry rtsp addresses.
 * @author karip
 */

public class Handler extends URLStreamHandler
{
	public Handler()
	{
	}
	protected URLConnection openConnection(URL u)
	{
		try {
		   return new RtspURLConnection(u);   
		} catch (IOException e) {
			return null;
		}
	}
}