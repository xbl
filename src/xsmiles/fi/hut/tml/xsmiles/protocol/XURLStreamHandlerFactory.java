/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.protocol;
import java.net.*;

import fi.hut.tml.xsmiles.Log;


public class XURLStreamHandlerFactory implements URLStreamHandlerFactory {
  public URLStreamHandler createURLStreamHandler(String protocol) {
    if (protocol.equalsIgnoreCase("data"))
      return new fi.hut.tml.xsmiles.protocol.data.Handler();
    if (protocol.equalsIgnoreCase("sip"))
    {
        // use reflection so that this is not dependent of SIP implementation
      //return new fi.hut.tml.xsmiles.protocol.sip.Handler();
		try
		{
	        return (URLStreamHandler)Class.forName("fi.hut.tml.xsmiles.protocol.sip.Handler").newInstance();
		} 
		catch (Exception t)
		{
			Log.error(t);
			return null;
		}
            
    }
    if (protocol.equalsIgnoreCase("msrp"))
        {
            // use reflection so that this is not dependent of SIP implementation
          //return new fi.hut.tml.xsmiles.protocol.sip.Handler();
            try
            {
                return (URLStreamHandler)Class.forName("fi.hut.tml.xsmiles.protocol.msrp.Handler").newInstance();
            } 
            catch (Exception t)
            {
                Log.error(t);
                return null;
            }
            
    }
    if (protocol.equalsIgnoreCase("ccpp"))
      return new fi.hut.tml.xsmiles.protocol.http.Handler();
    if (protocol.equalsIgnoreCase("dir"))
      return new fi.hut.tml.xsmiles.protocol.dir.Handler();
    if (protocol.equalsIgnoreCase("rtsp"))
      return new fi.hut.tml.xsmiles.protocol.rtsp.Handler();
    if (protocol.equalsIgnoreCase("urn"))
	{
		Log.debug("******* XURLSTreamHandler: urn");
		try
		{
	        return (URLStreamHandler)fi.hut.tml.xsmiles.MLFCLoader.loadClass("sun.net.www.protocol.urn.Handler").newInstance();
		} 
		catch (Exception t)
		{
			Log.error(t);
			return null;
		}
	}
    else 
      return null;
  }
}
