package fi.hut.tml.xsmiles.protocol.wesahmi;

import java.net.*;
import java.io.*;
import java.util.*;

public class Subscriber {
    
    Socket socket, dataSocket;
    PrintWriter out, dataOut;
    BufferedReader in, dataIn;
    String sipAddress;

    MessageListener listener;
    Thread listenerThread;
    boolean running;
    
    final int nsDaemonPort = 22580;
    final int nsDataPort = 22581;

    public interface MessageListener {
        public void notificationReceived(Notification notification);
    }

    /**
     * @param  sipAddress  SIP-address used for subscriptions
     */
    public Subscriber(String sipAddress) {
        this.socket = null;
        this.dataSocket = null;
        this.out = null;
        this.in = null;
        this.sipAddress = sipAddress;

        this.listener = null;
        this.running = false;
    }    
    
    /**
     * @param  sipAddress  SIP-address used for subscriptions
     */
    public Subscriber(String sipAddress, MessageListener listener) {
        this.socket = null;
        this.dataSocket = null;
        this.out = null;
        this.in = null;
        this.sipAddress = sipAddress;

        this.listener = listener;
        this.running = false;
    }

    /**
     * Starts the subscriber service. If listener object was given to class
     * constructor a thread is started for handling incoming messages.
     * Otherwise checkForNotifications should be used for polling incoming
     * messages.
     * 
     * @return               <code>true</code> if service was started.
     */
    public boolean startService() {

        if (listener != null) {

            /* Create thread for listening incoming messages */
            listenerThread = new Thread() {
                
                public void run () {

                    running = true;
                    openDataConnection();
                    
                    while (running) {
                        try{Thread.sleep(500);}catch(InterruptedException e){}
                        Notification notify = waitForNotification();

                        if (notify != null) {
                            listener.notificationReceived(notify);
                            notify = null;
                        }
                    }

                }

            };

            listenerThread.start();
        }

        return true;
    }

    /**
     * Stops the subscriber service
     */
    public void stopService() {

        running = false;
    }
    
    /**
     * Registers to the local notification client daemon. After the 
     * registration subscriber will receive notification from the
     * service it has subscribe.
     * 
     * @param  service  service name that subscriber is interested
     * @param  url      url of the service that subscriber is interested
     *                  (optional)
     * @return          registration identifier. 0 if registration fails.
     */

    public int registerToService(String service, String url)
        throws IOException {
        
        if (socket == null || socket.isConnected() == false)
            ConnectToDaemon();

        /* Send REGISTER to daemon */
        out.println("REGISTER " +
                    sipAddress + " " +
                    service + " " +
                    url);

        /* Parse received message */
        StringTokenizer st = new StringTokenizer(in.readLine());

        if (st.nextToken().equals("REGID") == true)
            return Integer.parseInt(st.nextToken());
        else
            return 0;
    }

    /**
     * Unregisters from the local notification client daemon.
     * 
     * @param  id       id of the service
     * @return          <code>true</code> if unregistration succeeded.
     */

    public boolean unregisterFromService(int id) throws IOException {

        if (socket == null || socket.isConnected() == false)
            ConnectToDaemon();
 
        /* Send CHECK to daemon */
        out.println("UNREGISTER " + id);
        
        String str = in.readLine();

        return str.equals("OK");
    }

    /**
     * Checks if there is a notification waiting.
     * 
     * @param  notification  Object containing the notification data.     
     * @return               <code>true</code> if there was a notification.
     */

    public Notification checkForNotifications() throws IOException {

        if (socket == null || socket.isConnected() == false)
            ConnectToDaemon();
 
        /* Send CHECK to daemon */
        out.println("CHECK " + sipAddress);

        StringTokenizer st = new StringTokenizer(in.readLine());
        String token = st.nextToken();
        
        /* Check if received DATA or NODATA */
        if (token.equals("DATA") == true)
        {
            /* Parse DATA message (DATA <registrationId> <data_length>\n<data>)*/
            int subscriptionId = Integer.parseInt(st.nextToken());
            int length = Integer.parseInt(st.nextToken());
            char cbuf[] = new char[length];

            System.out.println("DATA-received. Length = " + length);

            int nread = in.read(cbuf, 0, length);

            System.out.println("Read " + nread + " characters");

            return new Notification(new String(cbuf), subscriptionId, Notification.DATA);
        }
        else if (token.equals("SUBSCRIBE_OK") == true) {
            int subscriptionId = Integer.parseInt(st.nextToken());
            return new Notification("", subscriptionId, Notification.SUBSCRIBE_OK);
        }
        else /* NODATA received */
            return null;
    }

    /**
     * Sends data given by caller to notification server. Any data can be
     * sent and interpretting the data is responsibility of the
     * receiver. This method can be used for example to fetch data from
     * server if server has sent a notification telling that it has data
     * waiting to be fetched. This method also refreshes the subscription and
     * without the body can be used to only refreshing the subscription.
     * 
     * @param  subscriptionsId   Identifier of the subscription.
     * @param  body              Data which caller wants to send to notification server
     * @return                   <code>true</code> if the query was sent to server.
     */

    public boolean sendSubscriptionRefresh(int subscriptionId, String body)
        throws IOException {

        if (socket == null || socket.isConnected() == false)
            ConnectToDaemon();

        /* Send REFRESH to daemon */
        out.println("REFRESH " +
                    subscriptionId + " " +
                    body.length() + "\n" +
                    body);

        String str = in.readLine();

        return str.equals("OK");
    }


    private boolean ConnectToDaemon() {
        try {
            socket = new Socket("127.0.0.1", nsDaemonPort);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                                        socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Unknown host.");
            return false;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                               + "the connection to: 127.0.0.1.");
            return false;
        }

        return true;
    }


    private boolean openDataConnection() {
        try {
            dataSocket = new Socket("127.0.0.1", nsDataPort);
            dataOut = new PrintWriter(dataSocket.getOutputStream(), true);
            dataIn = new BufferedReader(new InputStreamReader(
                    dataSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Unknown host.");
            return false;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                    + "the connection to: 127.0.0.1.");
            return false;
        }
        return true;
    }
    
    private Notification waitForNotification() {

        if (dataSocket == null || dataSocket.isConnected() == false)
            openDataConnection();

        try {
            String recvLine = dataIn.readLine();
            
            StringTokenizer st = new StringTokenizer(recvLine);
            String token = st.nextToken();
            
            /* Check if received DATA or NODATA */
            if (token.equals("DATA") == true)
            {
                /* Parse DATA message (DATA <registrationId> <data_length>\n<data>)*/
                int subscriptionId = Integer.parseInt(st.nextToken());
                int length = Integer.parseInt(st.nextToken());
                char cbuf[] = new char[length];
    
                System.out.println("DATA-received. Length = " + length);
    
                int nread = dataIn.read(cbuf, 0, length);
    
                System.out.println("Read " + nread + " characters");
    
                return new Notification(new String(cbuf), subscriptionId, Notification.DATA);
            }
            else if (token.equals("SUBSCRIBE_OK") == true) {
                int subscriptionId = Integer.parseInt(st.nextToken());
                return new Notification("", subscriptionId, Notification.SUBSCRIBE_OK);
            }
            else /* NODATA received */
            {
                System.out.println("DEBUG: waitForNotification error >>>");
                
                while (st.hasMoreTokens())
                    System.out.println(st.nextToken());

                System.out.println("<<< DEBUG");

                return null;
            }
        
        } catch (IOException e) {
            return null;
        }

    }
    
}