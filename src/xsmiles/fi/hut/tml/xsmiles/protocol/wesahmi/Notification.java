package fi.hut.tml.xsmiles.protocol.wesahmi;

public class Notification {

    private String body;
    private int subscriptionId;
    private int type;
    
    public static final int SUBSCRIBE_OK = 1;
    public static final int SUBSCRIBE_FAILED = 2;
    public static final int DATA = 3;
    
    public Notification(String body, int subscriptionId, int type) {
        this.body = body;
        this.subscriptionId = subscriptionId;
        this.type = type;
    }

    public String getBody() {

        return body;
    }

    public int getSubscriptionId() {

        return subscriptionId;
    }

    public int getType() {

        return type;
    }
}