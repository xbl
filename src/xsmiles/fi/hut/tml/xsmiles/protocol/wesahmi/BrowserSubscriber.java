package fi.hut.tml.xsmiles.protocol.wesahmi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Vector;

import org.apache.fop.fo.pagination.SubSequenceSpecifier;
import org.jaxen.BaseXPath;
import org.jaxen.dom.DocumentNavigator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.BrowserTable;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;

public class BrowserSubscriber extends Thread implements Subscriber.MessageListener
{
    String sipAddress, serviceID, url, clientID,
            currentViewID;

    String [] flightNumbers, sdts;
    
    Subscriber subscriber;

    int registrationId, subscriptionId;

    BrowserTable browserTable;

    boolean checkNotifications = true;

    public BrowserSubscriber(BrowserTable bt, String address, String service,
            String url, String clientID, String[] flightNumbers, String[] sdts)
    {
        super("BrowserSubscriber");
        browserTable = bt;
        sipAddress = address;
        serviceID = service;
        this.url = url;
        this.flightNumbers = flightNumbers;
        this.sdts = sdts;
        this.clientID = clientID;
    }

    public void registerToService()
    {
        Log.debug("Register for service: " + sipAddress + ", " + serviceID
                + ", " + url);
        subscriber = new Subscriber(sipAddress); // Polling impl
        //subscriber = new Subscriber(sipAddress, this);
        try
        {
            registrationId = subscriber.registerToService(serviceID, url);
            Log.debug("Registration ID: " + registrationId);
        }
        catch (IOException ioe)
        {
            Log.error("Couldn't register to service " + serviceID + ". " + ioe);
        }
        Log.debug("Starting notification service.");
        this.start(); // Polling impl
        //subscriber.startService();
    }

    public void run()
    {
        Notification notification = null;
        while (checkNotifications)
        {
            try
            {
                notification = subscriber.checkForNotifications();
                Log.debug("Checking for notifications");
            }
            catch (IOException ioe)
            {
                Log.error("Couldn't check for notifications.\n" + ioe);
            }

            if (notification != null)
            {
                Log.debug("Received a notification!");
                subscriptionId = notification.getSubscriptionId();
                if (notification.getType() == Notification.SUBSCRIBE_OK)
                    sendClientInfo();
                else if (notification.getType() == Notification.DATA)
                    parseMessage(notification.getBody());
            }

            try
            {
                Thread.sleep(800);
            }
            catch (Exception e)
            {
                Log.error(e);
            }
        }
    }

    void parseMessage(String msg)
    {
        Log.debug("Notification body:\n" + msg);
        InputStream is = new ByteArrayInputStream(msg.getBytes());
        BrowserWindow bw = browserTable.getLatestBrowserWindow();
        // Log.debug(browserTable.getBrowsersByID("check-in").toString());
        // BrowserWindow bw =
        // (BrowserWindow)browserTable.getBrowsersByID("check-in").get(0);

        if (bw != null)
        {
            XSmilesXMLDocument xDoc = new XSmilesXMLDocument(bw, is);
            try
            {
                xDoc.retrieveDocument();
            }
            catch (Exception e)
            {
                Log.error(e);
            }

            Document eventDoc = xDoc.getDocument();
            Log.debug("New doc: " + eventDoc);
            Node newLoc = eventDoc.getElementsByTagName("wes:OpenLocation")
                    .item(0);
            Node newCon = eventDoc.getElementsByTagName("wes:ContentChanged")
                    .item(0);
            if (newLoc != null)
            {
                Log.debug("Open location");
                String serviceId = eventDoc.getElementsByTagName("wes:service")
                        .item(0).getFirstChild().getNodeValue();
                String url = eventDoc.getElementsByTagName("url").item(0)
                        .getFirstChild().getNodeValue();
                openLocation(url, serviceId);
            }
            else if (newCon != null)
            {
                createEvent(eventDoc);
            }
        }
    }

    void openLocation(String url, String id)
    {
        Vector browsers = browserTable.getBrowsersByID(id);

        if (browsers == null)
        {
            try
            {
                // NOTE: asks for latest browser window
                browserTable.getLatestBrowserWindow().getMLFCListener()
                        .openURLFromExternalProgram(url, id);
                browsers = browserTable.getBrowsersByID(id);
                ((BrowserWindow) (browsers.get(0))).getMLFCListener()
                        .setSubscriber(this);
            }
            catch (Exception e)
            {
                Log.error("BrowserSubscriber: " + e);
            }
        }
        else
        {
            try
            {
                Log.debug("Opening url in existing window. ");
                ((BrowserWindow)(browsers.get(0))).getMLFCListener().openLocation(url);
                //((BrowserWindow) (browsers.get(0))).getMLFCListener().reloadCurrentPage();
            }
            catch (Exception e)
            {
                Log.error("BrowserSubscriber: " + e);
            }
        }
    }

    void createEvent(Document eventDoc)
    {

        Node serviceNode = eventDoc.getElementsByTagName("wes:service").item(0)
                .getFirstChild();
        String serviceId = serviceNode.getNodeValue();
        Log.debug("Service id from SOAP: " + serviceId);

        Node viewIDNode = eventDoc.getElementsByTagName("wes:viewID").item(0);
        if (viewIDNode != null)
        {
            currentViewID = viewIDNode.getFirstChild().getNodeValue();
            Log.debug("View id from SOAP: " + currentViewID);
        }

        Vector browsers = browserTable.getBrowsersByID(serviceId);
        Document doc = ((BrowserWindow) (browsers.get(0))).getXMLDocument()
                .getDocument();
        NodeList events = eventDoc.getElementsByTagName("event");
        Element event;

        for (int i = 0; i < events.getLength(); i++)
        {

            event = (Element) events.item(i);
            String id = event.getAttribute("target");
            String type = event.getAttribute("name");

            Log.debug("REX event " + type + " to element with id: " + id);

            BaseXPath xpath = null;
            Element target = null;
            try
            {
                xpath = new BaseXPath(id, new DocumentNavigator());
                xpath.addNamespace("x", "http://www.w3.org/1999/xhtml");
                Log.debug("XPATH: " + xpath.debug()); // TODO: error in
                // namespace description
                target = (Element) xpath.selectSingleNode(doc);
            }
            catch (Exception e)
            {
                Log.error(e);
            }

            // Element target = doc.getElementById(id);
            Element eventClone = (Element) event.cloneNode(true);
            Node adopted = doc.adoptNode(eventClone);

            if (adopted != null && target != null)
            {

                if (type.equals("DOMNodeRemoved"))
                {
                    NodeList children = eventClone.getChildNodes();
                    Node parent = target.getParentNode();
                    int j = 0;
                    Log.debug("Removing " + target);
                    //parent.removeChild(target);
                    while (children.getLength() > j)
                    {
                        if (children.item(j) instanceof Element)
                        {
                            Log.debug("Inserting " + children.item(j)
                                    + " before " + target);
                            //parent.insertBefore(children.item(j), target);
                            parent.replaceChild(children.item(j), target);
                        }
                        else
                            j++;
                    }
                }
                else if (type.equals("DOMNodeInserted"))
                {
                    NodeList children = eventClone.getChildNodes();
                    while (children.getLength() > 0)
                    {
                        target.appendChild(children.item(0));
                    }
                }
                else if (type.equals("DOMAttrModified"))
                {
                    String attr = eventClone.getAttribute("attrName");
                    String value = eventClone.getAttribute("newValue");
                    target.setAttribute(attr, value);
                }
                else if (type.equals("DOMCharacterDataModified"))
                {
                    Log
                            .error("Event not delivered, DOMCharacterDataModified for REX not implemented yet");
                }
            }
            else
            {
                Log.error("Cannot adopt or cannot find target" + eventClone);
            }
        }
    }

    public void closeView()
    {
        if (currentViewID != null)
        {
            subscriber.stopService();
            checkNotifications = false;
            String data = " <wes:closeView>"+currentViewID+"</wes:closeView>";
            sendSubscriptionRefresh(soapWrap(data));
        }
/*        try
        {
            subscriber.sendSubscriptionRefresh(Integer.parseInt(currentViewID),
                    "CloseView");
        }
        catch (IOException ioe)
        {
            Log.error("Couldn't order data updates.\n" + ioe);
        }
        catch (NumberFormatException nfe)
        {
            Log.error("Error parsing string: " + currentViewID + "\n" + nfe);
        }
*/    }

    public void sendSubscriptionRefresh(String body)
    {
        try
        {
            boolean success = subscriber.sendSubscriptionRefresh(subscriptionId, body);
            
            checkNotifications = true;
        }
        catch (IOException ioe)
        {
            Log.error("Couldn't order data updates.\n" + ioe);
        }
    }
    
    void sendClientInfo()
    {
        String data = "    <wes:newSubscription>client info</wes:newSubscription>\n"
        + "    <wes:clientID>" + this.getClientID() + "</wes:clientID>\n";
        
        int count = Array.getLength(flightNumbers);
        for (int i = 0; i < count; i++)
        {
            if (flightNumbers[i] != null && flightNumbers[i] != "")
                data += "<wes:flight number='" + i+1 + "' flightNumber='"
                + flightNumbers[i] + "' sdt='" + sdts[i] + "'/>\n";
        }        
        
        sendSubscriptionRefresh(soapWrap(data));
    }

    String soapWrap(String content)
    {
        String soapMessage = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wes=\"http://www.tml.hut.fi/Research/wesahmi\">\n"
                + "  <env:Body>\n"
                + content
                + "  </env:Body>\n"
                + "</env:Envelope>";

        return soapMessage;
    }

    public void unregisterFromService()
    {
        try
        {
            subscriber.unregisterFromService(registrationId);
        }
        catch (Exception e)
        {
            Log.error("BrowserSubscriber: " + e);
        }
    }

    public String getClientID()
    {
        return clientID;
    }

    public String[] getFlightNumbers()
    {
        return flightNumbers;
    }

    public String[] getFlightTimes()
    {
        return sdts;
    }

    public void notificationReceived(Notification notification)
    {
        Log.debug("Received a notification!");
        subscriptionId = notification.getSubscriptionId();
        if (notification.getType() == Notification.SUBSCRIBE_OK)
            sendClientInfo();
        else if (notification.getType() == Notification.DATA)
            parseMessage(notification.getBody());
    }
}
