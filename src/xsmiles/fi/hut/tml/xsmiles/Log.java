/*
 * X-Smiles
 * 
 * 
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0 (based on the Apache
 * Software License Version 1.1)
 * 
 * 
 * Copyright (c) 1999-2001 Helsinki University of Technology. All rights
 * reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *  3. The end-user documentation included with the redistribution, if any,
 * must include the following acknowledgment: "This product includes software
 * developed by the Telecommunications Software and Multimedia Laboratory,
 * Helsinki University of Technology (http://www.tml.hut.fi/)." Alternately,
 * this acknowledgment may appear in the software itself, if and wherever such
 * third-party acknowledgments normally appear.
 *  4. The names "HUT" and "Telecommunications Software and Multimedia
 * Laboratory, Helsinki University of Technology" must not be used to endorse
 * or promote products derived from this software without prior written
 * permission. For written permission, please contact info@xsmiles.org.
 *  5. Products derived from this software may not be called "X-Smiles", nor
 * may "X-Smiles" appear in their name, without prior written permission of The
 * Telecommunications Software and Multimedia Laboratory, Helsinki University
 * of Technology
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * TELECOMMUNICATIONS SOFTWARE AND MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY
 * OF TECHNOLOGY OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *  
 */

/*
 * X-smiles project 2000
 * 
 * $Id: Log.java,v 1.19 2005/09/13 10:02:51 honkkis Exp $
 *  
 */

package fi.hut.tml.xsmiles;
/**
 * Contains methods for printing debug, info and error messages. NOTE: do not
 * use other X-Smiles classes from this class, it should be as independent as
 * possible for embedding into other projects.
 * 
 * @author Mikko Honkala
 * @version $Revision: 1.19 $
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Log {
	public static boolean enable_debug = true;
	public static boolean enable_info = true;
	private static boolean enable_log = false;
	public static boolean enable_error_source = false;
	public static boolean enable_debug_source = false;
	
	private static String log_filename = "";

	static private PrintStream out = System.out;
	private static PrintStream err = System.err;
	static private Object hookLock = new Object();

	static private FileOutputStream fos;
	
	/**
	 * If enabled in the XSmiles config, parses the source file and line from where
	 * the log message originated.
	 * 
	 * @param enabled enable the parsing
	 * @return a String containing the source file and linenumber, or an empty String if this disabled
	 */	
	private static String getMessageSource(boolean enabled) {
	    if (!enabled) {
	        return "";
	    }
	    try {
		    throw new Throwable();
		} catch (Throwable t) {
		    StackTraceElement[] sTrace = t.getStackTrace();
		    if (sTrace[2]!=null) {
		        return " (at "+sTrace[2].getFileName()+":"+sTrace[2].getLineNumber()+")";
		    }
		}
	    return "";
	}
    /**
	 * Output a debug message. Debug messages are shown if enable_debug is
	 * true, otherwise they are discarded
	 * 
	 * @param message
	 *                  a String that contains the message to be outputted.
	 *  
	 */
	public static void debug(String message) {
		if (enable_debug) {
			output("[Debug] ");
			outputln(message + getMessageSource(enable_debug_source));
		}
	}
	/**
	 * Output an info message. Info messages are always shown.
	 * 
	 * @param message
	 *                  a String that contains the message to be outputted.
	 *  
	 */
	public static void info(String message) {
		if (enable_info)
			outputln(message);
	}
	/**
	 * Output an error message.
	 * 
	 * @param message
	 *                  a String that contains the message to be outputted.
	 *  
	 */
	public static void error(String message) {
		outputErrln("ERROR: " + message + getMessageSource(enable_error_source));
		
	}
	/**
	 * Output an error message.
	 * 
	 * @param ex
	 *                  a Throwable object, that contains the message to be
	 *                  outputted.
	 *  
	 */
	public static void error(java.lang.Throwable t) {
		outputErrln("ERROR: " + t.toString());
		if (t != null)
			t.printStackTrace(System.err);
	}

	/**
	 * Output an error message.
	 * 
	 * @param ex
	 *                  a Throwable object, that contains the message to be
	 *                  outputted.
	 *  
	 */
	public static void error(java.lang.Throwable t, String message) {
		outputErrln("ERROR: " + message + "\n" + t.toString());
		if (t != null)
			t.printStackTrace(System.err);
	}

	/**
	 * Output an error message.
	 * 
	 * @param ex
	 *                  an Exception object, that contains the message to be
	 *                  outputted.
	 *  
	 */
	//  public static void error(Exception e)
	//  {
	//  	output("ERROR: "+e.toString());
	//  	e.printStackTrace(System.err);
	//  }
	/**
	 * Enable log writing to a file.
	 * 
	 * @param message
	 *                  a String that contains the message to be outputted.
	 * @exception IOException
	 *                       for JDK 1.1 compatibility.
	 *  
	 */
	public static void enableLogFile(String filename) throws IOException {
		if (Log.enable_log)
			return;
		File f = new File(filename);
		fos = new FileOutputStream(f);
		hookStandards(fos);
		Log.enable_log = true;
	}
	/**
	 * Disable log writing to a file.
	 * 
	 * @param message
	 *                  a String that contains the message to be outputted.
	 *  
	 */
	public static void disableLogFile() {
		if (Log.enable_log == false)
			return;
		unhookStandards();
		try {
			fos.close();
		} catch (Exception e) {
			Log.error(e);
		}
		fos = null;
		Log.enable_log = false;
	}

	protected static void outputln(String text) {
		out.println(text);
	}
	protected static void output(String text) {
		out.print(text);
	}
	
	protected static void outputErrln(String text) {
		err.println(text);
	}
	
	protected static void outputErr(String text) {
		err.print(text);
	}

	static public void hookStandards(FileOutputStream fos) {
		synchronized (hookLock) {
			if (out != null)
				return;

			out = System.out;
			err = System.err;
			PrintStream dwout = new PrintStream(fos);
			System.setOut(dwout);
			System.setErr(dwout);
		}
	}

	static public void reHookStandards() {
		synchronized (hookLock) {
			out = System.out;
			err = System.err;
		}
	}

	static public void unhookStandards() {
		synchronized (hookLock) {
			if (out == null)
				return;

			System.setOut(out);
			System.setErr(err);
			out = null;
			err = null;
		}
	}

}
