/*
* X-smiles project 1999
*
* $Id: XercesXMLParser.java,v 1.25 2006/12/15 11:03:55 mpohja Exp $
*/


package fi.hut.tml.xsmiles.xml;

import org.w3c.dom.*;

import java.net.URL ;
import java.io.Reader;
import java.io.Writer;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.IOException;
import org.apache.xerces.parsers.DOMParser;
import  org.apache.xml.serialize.OutputFormat;
import  org.apache.xml.serialize.Serializer;
import  org.apache.xml.serialize.LineSeparator;
import  org.apache.xml.serialize.SerializerFactory;
import  org.apache.xml.serialize.XMLSerializer;
import  org.apache.xml.serialize.DOMSerializer;
import org.xml.sax.EntityResolver;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ErrorHandler;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;

/**
 * Contains the DOM-implementation used by the current configuration.
 * 
 * @author Mikko Honkala
 * @version $Revision: 1.25 $
 */
public class XercesXMLParser extends DOMParser implements ErrorHandler, XMLParser, EntityResolver
{
   
  
  private Browser m_browser;
  private URL XSLURL;
  private URL XMLURL;
 
  
  
  /*
   *  This is the DOM-implementation that the parser uses.
   */
//    private DOMParser parser; 

    /**
    *   Constructor.
    *   Creates a new instance of the DOM-parser.
    */
    public XercesXMLParser(Browser browser)
    {
		super();
        m_browser = browser;
		
        //parser = new DOMParser();
        return;
    }


    /**
     * Asks XSLEngine-class to evaluate xml and xsl documents.
     * 
     * @param xml the URL of the xml document
     * @param xsl the URL of the xsl document
     * @return the evaluated documents as string
     */

  
// public String useXSLEngine(URL xml,URL xsl)
//   {
//  XSLURL = xsl;
//  XMLURL = xml;
//  String source = m_browser.getXSLEngine().transform(xml,xsl);
//  return source;  
//  
//}  
 

    /**
    * Accessor method.      
    * @return the current DOM-implementation.
    */   
    /*public com.docuverse.dom.DOM getDOMImplementation()
    {
      return dom;
    }*/
	/**
    * Reads an document to DOM tree      
    * @return read DOM document.
    */   
    public synchronized Document openDocument(Reader sourceReader,boolean presentationDOM) throws IOException,SAXException
    {
	  Log.debug("Xerces.openDocument()");
	  InputSource input = new InputSource(sourceReader);
	  this.doSettings(null);
	  super.parse(input);
      return super.getDocument();
    }
	
	/**
    * Reads a document from and InputStream to a DOM tree
    * This method was added for i18n.  A parser assumes that a Reader source
    * is already in Unicode, whereas it will check the xml header for the
    * encoding of an InputStream.
    *
    * @return read DOM document.
    */   
    public synchronized Document openDocument(InputStream sourceStream, boolean presentationDOM,String documentURL) throws IOException,SAXException
    {
	  Log.debug("Xerces.openDocument()");
	  InputSource input = new InputSource(sourceStream);
      input.setSystemId(documentURL);
	  this.doSettings(null);
	  super.parse(input);
      return super.getDocument();
    }
	
    public synchronized Document openDocument(URL url, String documentClassname) throws IOException,SAXException
    {
      return openDocument(url);
    }
	
    /**
    * Reads an document to DOM tree      
    * @return read DOM document.
    */   
    public synchronized Document openDocument(URL url) throws IOException,SAXException
    {
      Log.debug("Xerces.openDocument()");
      InputSource input = new InputSource(url.toString());
      this.doSettings(url); 
      super.parse(input);
      return super.getDocument();
    }
 
 	private void doSettings(URL url) 
 	{
 
	 	try 
	 	{
			  if (m_browser.getJavaVersion()<1.2) 
			  {
				  //super.setDeferNodeExpansion(false);
				  super.setFeature("http://apache.org/xml/features/dom/defer-node-expansion",false);
			  }
//			  if (url!=null&&url.toString().endsWith(".svg"))
//			  {
//				  setDocumentClassName("org.csiro.svg.dom.SVGDocumentImpl2");
//			  }


//			  setDocumentClassName("fi.hut.tml.xsmiles.xml.dom.DocumentImpl");
//		      setValidation(true);
//		      setCreateEntityReferenceNodes(false);
		      setErrorHandler(this);
		      setEntityResolver(this);
			  super.setFeature("http://xml.org/sax/features/namespaces",true);
//		      setNamespaces(true);
	 	}
		catch (Exception e) 
		{
			Log.error("Error at XercesXMLparser.doSettings() : " + e);
			
		}
		
		
 	}
 
//    public String write(Node doc) 
//    {
//		return write(doc,false); // do not pretty print
//    }
//    public String write(Node doc,boolean prettyPrinting) 
//    {
//    	StringWriter  stringOut = new StringWriter();        //Writer will be a String
//    	if (prettyPrinting) write(stringOut,doc,false,true);
//        else write(stringOut,doc,true,false);
//        Log.debug("Prettyprinting : "+prettyPrinting);
//    	return stringOut.toString();
//    }
	/**
	 * The main write method, writes the DOM document to a Writer
	 *
	 * @param preserveSpace preserves the white space in the document
	 * @param intenting sets pretty printing on
	 *
	 *
	 */
//    public void write(Writer writer,Node doc, boolean preserveSpace, boolean indenting)
//	{
//		try
//		{
//			OutputFormat    format  = new OutputFormat();   //Serialize DOM
//			format.setEncoding("ISO-8859-1");
//			format.setPreserveSpace(preserveSpace);
//			//format.setLineSeparator(LineSeparator.Windows);
//			//format.setIndent(5);
//			format.setIndenting(indenting);
//			//Log.debug("Outputformat: "+format.getPreserveSpace()+" : "+format.getLineSeparator()+" ind: "+format.getIndent()+" : "+format.getIndenting()+" : ");
//			XMLSerializer    serial = new XMLSerializer( writer, format );
//			DOMSerializer dSer = serial.asDOMSerializer();                            // As a DOM Serializer
//
//			if (doc instanceof Element) 
//			{
//				dSer.serialize( (Element)doc);
//			} else if (doc instanceof Document) 
//			{
//				dSer.serialize( (Document)doc);
//			}
//			//System.out.println( "STRXML = " + stringOut.toString() ); //Spit out DOM as a String
//		} catch (IOException e) 
//		{
//			Log.error(e);
//		}
//	}

    public String write(Node doc,boolean prettyPrinting) 
    {
    	StringWriter  stringOut = new StringWriter();        //Writer will be a String
    	if (prettyPrinting) write(stringOut,doc,false,true);
        else write(stringOut,doc,true,false);
        Log.debug("Prettyprinting : "+prettyPrinting);
    	return stringOut.toString();
    }
	/**
	 * Writes the dom node to a string. Note! Uses Jaxp transformations.
	 */ 
    public String write(Node doc) 
    {
		return write(doc,false); // do not pretty print
    }

    public void write(Writer writer,Node node,boolean prettyPrinting)
    {
		this.write(writer,node,prettyPrinting ? false:true,prettyPrinting ? true:false);
    }

	public void write(Writer writer,Node doc, boolean preserveSpace, boolean indenting)
	{
		try
		{
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(writer,doc,preserveSpace,indenting,true);
		} catch (Exception e) 
		{
			Log.error(e);
		}
	}
	public void writeWithoutXMLDecl(Writer writer, Node node) throws IOException
	{
		try
		{
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(writer,node,true,true,false);
		} catch (Exception e) 
		{
			Log.error(e);
		}
	}
	
	public SAXParseException lastParseException;
	public SAXParseException getLastParseException()
	{
		return lastParseException;
	}
    
  	
   public InputSource resolveEntity (String publicId, String systemId)
   {
	   Log.debug("resolveEntity: publicId="+publicId+", systemId="+systemId);
//	   org.csiro.svg.parser.SVGParser parser = new org.csiro.svg.parser.SVGParser();
//	   return super.resolveEntity(publicId, systemId); 	

//     if (systemId.equals("http://www.myhost.com/today")) {
//              // return a special input source
//       MyReader reader = new MyReader();
//       return new InputSource(reader);
//     } else {
//              // use the default behaviour

       return null;
//     }
   }
      public org.w3c.dom.Document createEmptyDocument()
     {
             return null;
     }
   /** Warning. */
   public void warning(SAXParseException ex) {
     System.err.println("[Warning] "+
   	       getLocationString(ex)+": "+
   	       ex.getMessage());
   }
 
   /** Error. */
   public void error(SAXParseException ex) {
   	lastParseException=ex;
     System.err.println("[Error] "+
   	       getLocationString(ex)+": "+
   	       ex.getMessage());
   }
 
   /** Fatal error. */
   public void fatalError(SAXParseException ex) throws SAXException {
     System.err.println("[Fatal Error] "+
   	       getLocationString(ex)+": "+
   	       ex.getMessage());
     throw ex;
   }
 
   /** Returns a string of the location. */
   private String getLocationString(SAXParseException ex) {
     StringBuffer str = new StringBuffer();
     String systemId = ex.getSystemId();
     if (systemId != null) {
       int index = systemId.lastIndexOf('/');
       if (index != -1)
         systemId = systemId.substring(index + 1);
       str.append(systemId);
     }
     str.append(':');
     str.append(ex.getLineNumber());
     str.append(':');
     str.append(ex.getColumnNumber());
     return str.toString();
   }


public Document openDocument(InputStream sourceStream, boolean presentationDOM) throws IOException, SAXException
{
      Log.debug("Xerces.openDocument()");
      InputSource input = new InputSource(sourceStream);
      input.setSystemId("fromSocketXerces");
      this.doSettings(null);
      super.parse(input);
      return super.getDocument();
}
 
 
    
} 
