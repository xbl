/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Resources;

/**
 * Contains the Jaxp XML DOM-implementation
 * 
 * @author Mikko Honkala
 * @version $Revision: 1.33 $
 */
public class JaxpXMLParser implements XMLParser
{
   
  private URL XSLURL;
  private URL XMLURL;
  
  private JAXPErrorHandler errorHandler;
  private JAXPEntityResolver entityResolver;
  
  /*
   *  This is the DOM-implementation that the parser uses.
   */
    private DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance(); 
    //private DocumentBuilder parser; 

    /**
    *   Constructor.
    *   Creates a new instance of the DOM-parser.
    */
    public JaxpXMLParser()
    {
        factory.setNamespaceAware( true );
        return;
    }
	private void setParserSettings(DocumentBuilder builder)
	{
		if (errorHandler==null)
			errorHandler = new JAXPErrorHandler();
        this.setErrorHandler(errorHandler,builder);
        if (entityResolver==null)
            entityResolver = new JAXPEntityResolver();
		this.setEntityResolver(entityResolver,builder);
	}
	
	public void setErrorHandler(ErrorHandler handler, DocumentBuilder builder)
	{
		if (builder!=null)
        {
            builder.setErrorHandler(handler);
        }
	}
    public void setEntityResolver(EntityResolver handler, DocumentBuilder builder)
    {
        if (builder!=null)
        {
            builder.setEntityResolver(handler);
        }
    }

  /**
    * Reads an document to DOM tree      
    * @return read DOM document.
    */   
    public synchronized Document openDocument(Reader sourceReader, boolean presentationDOM) throws IOException,SAXException
    {
	  InputSource input = new InputSource(sourceReader);
	  if (presentationDOM==false)
	    return this.openDocument(input);
	  else
	    return this.openDocument(input,	"fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl");
    }

  /**
    * Reads a document from an InputStream into a DOM tree.
    * This method was added for i18n.  A parser assumes that a Reader source
    * is already in Unicode, whereas it will check the xml header for the
    * encoding of an InputStream.
    *
    * @return read DOM document.
    */   
    public synchronized Document openDocument(InputStream sourceStream, boolean presentationDOM,String documentURL) throws IOException,SAXException
    {
	  InputSource input = new InputSource(sourceStream);
      input.setSystemId(documentURL);
	  if (presentationDOM==false)
	    return this.openDocument(input);
	  else
	    return this.openDocument(input, "fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl");
    }

    public synchronized Document openDocument(InputStream sourceStream, boolean presentationDOM) throws IOException,SAXException
    {
      InputSource input = new InputSource(sourceStream);
      input.setSystemId("fromSocket");
      if (presentationDOM==false)
        return this.openDocument(input);
      else
        return this.openDocument(input, "fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl");
    }

    /**
    * Reads an document to DOM tree      
    * @return read DOM document.
    */   
    public synchronized Document openDocument(URL url) throws IOException,SAXException
    {
	    	InputSource input = new InputSource(url.toString());
	    	return openDocument(input,
	    		"fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl");
    }
    public synchronized Document openDocument(URL url, String documentClassname) throws IOException,SAXException
    {
        	InputSource input = new InputSource(url.toString());
        	return openDocument(input,
        		documentClassname);
    }
	
    public synchronized Document openDocument(InputSource input, String documentClassname) throws IOException,SAXException
    {
		if (documentClassname==null||documentClassname.length()<1)
		{
		factory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",
			"org.apache.xerces.dom.DocumentImpl"  );
		
		}
        else
		{
	    	factory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",documentClassname);
		}
    	org.w3c.dom.Document document =  this.openDocument(input);

		// recreate factory
//		factory=DocumentBuilderFactory.newInstance(); 
//		factory.setNamespaceAware( true );
		factory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",
			"org.apache.xerces.dom.DocumentImpl"  );

		return document;		
    }
    private synchronized Document openDocument(InputSource input) throws IOException,SAXException
    {
		try
		{
	   		DocumentBuilder parser = factory.newDocumentBuilder();
	    	this.setParserSettings(parser);
			
			// Set the document class, because smil openDocument() may have failed
	    	factory.setAttribute("http://apache.org/xml/properties/dom/document-class-name",
	    		"org.apache.xerces.dom.DocumentImpl"  );
	    	Log.debug("JAXP.openDocument() parser:"+parser.getClass()+". NSpaces:"+parser.isNamespaceAware());
	    	return parser.parse(input);
		} catch (ParserConfigurationException e)
		{
		  Log.error(e);
		}
		return null;
    }
     public org.w3c.dom.Document createEmptyDocument()
     {
         try
         {
	   		DocumentBuilder parser = factory.newDocumentBuilder();
	    	this.setParserSettings(parser);
            return parser.newDocument();
         } catch (Exception e)
         {
             Log.error(e);
             return null;
         }
     }
	/**
	 * Writes the dom node to a string. Note! Uses Jaxp transformations.
	 */ 
    public String write(Node doc) 
    {
		return write(doc,false); // do not pretty print
    }

    public void write(Writer writer,Node node,boolean prettyPrinting)
    {
		this.write(writer,node,prettyPrinting ? false:true,prettyPrinting ? true:false);
    }
    /**
     * Writes the dom node to a string. Note! Uses Jaxp transformations.
     */ 
    public String write(Node doc,boolean prettyPrinting) 
    {
    	StringWriter  stringOut = new StringWriter();        //Writer will be a String
    	if (prettyPrinting) write(stringOut,doc,false,true);
        else write(stringOut,doc,true,false);
        Log.debug("Prettyprinting : "+prettyPrinting);
    	return stringOut.toString();
    }
	/**
	 * The main write method, writes the DOM document to a Writer. Note! Uses Jaxp transformations
	 *
	 * @param preserveSpace preserves the white space in the document
	 * @param intenting sets pretty printing on
	 *
	 *
	 */
    public void write(Writer writer,Node doc, boolean preserveSpace, boolean indenting)
	{
		try
		{
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(writer,doc,preserveSpace,indenting,true);
		} catch (Exception e) 
		{
			Log.error(e);
		}
	}
	public void writeWithoutXMLDecl(Writer writer, Node node) throws IOException
	{
		try
		{
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(writer,node,true,true,false);
		} catch (Exception e) 
		{
			Log.error(e);
		}
	}
	
    public SAXParseException lastParseException;
    public SAXParseException getLastParseException()
    {
		return lastParseException;
    }

    public class JAXPEntityResolver implements EntityResolver
    {
        public final String w3cPrefix = "-//W3C//";
        public final String w3cXhtmlEntPrefix = "ENTITIES XHTML";
        public final String w3cXhtmlEntPrefix2 = "DTD XHTML";
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
        {
            boolean ignoreXHTMLDTD=true;
            // TODO Auto-generated method stub
            Log.debug("JAXP parser.resolveEntity:'"+publicId+"':'"+systemId+"'");
            if (ignoreXHTMLDTD)
            {
                if (publicId.startsWith(w3cPrefix))
                {
                    if (
                            publicId.substring(w3cPrefix.length()).startsWith(w3cXhtmlEntPrefix) ||
                            publicId.substring(w3cPrefix.length()).startsWith(w3cXhtmlEntPrefix2) 
                            )
                    {
                        String entitiesURL = Resources.getResourceURL("xsmiles.dtd.xhtml").toString();
                        Log.debug("Reading local XHTML entities: "+entitiesURL);
                        return new InputSource(entitiesURL);
                    }
                    /*
                    else
                    {
                        Log.debug("Ignoring XHTML entity");
                        StringReader reader = new StringReader("");
                        return new InputSource(reader);
                    }*/
                }
            }
            return null;
        }
    }

    
	public class JAXPErrorHandler implements ErrorHandler
	{
		public JAXPErrorHandler()
		{
		}
		public void error(SAXParseException exception)  
		{
			handleError(exception);
		}
		public void fatalError(SAXParseException exception)  
		{
			handleError(exception);
		}
		public void warning(SAXParseException exception)  
		{
			handleError(exception);
		}
		public int errCount=0;
		private void handleError(SAXParseException ex)  
		{
			try
			{
				//Node current = ( Node ) parser.getProperty( "http://apache.org/xml/properties/dom/current-element-node" );
				errCount++;
		    	StringBuffer errorString = new StringBuffer();
				errorString.append("at line number, ");
				errorString.append(ex.getLineNumber());
				errorString.append(": ");
				errorString.append(ex.getMessage());
				Log.error(errorString.toString());
				//print(current);
				//this.xform.getModel().validationFailed(current,ex.getMessage());		  
			} catch (Exception e)
			{
				System.err.println(e);
			}
			lastParseException=ex;
			
		}
	}
  
} 
