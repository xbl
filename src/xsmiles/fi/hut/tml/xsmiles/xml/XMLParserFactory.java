/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



/*
* X-smiles project 1999
* @author Mikko Honkala
*
* $Id: XMLParserFactory.java,v 1.10 2002/03/27 14:24:30 jvierine Exp $
*
 */


package fi.hut.tml.xsmiles.xml;
import java.lang.ClassNotFoundException;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;

/**
 * Contains the XSL-Engine factory of the product.
 * 
 * @author Mikko Honkala
 * @version $Revision: 1.10 $
 */
public class XMLParserFactory{

 /**
  * Factory method.
  *
  * @return the XML wrapper
  */
  
  public static XMLParser createDefaultXMLParser() 
  {
  	try 
	  {
	    return createXMLParser("Xerces");
	  } catch (ClassNotFoundException e) 
	    {
	      Log.error(e);
	      return null;
	    }
	
  }
  public static XMLParser createXMLParser(String id) throws ClassNotFoundException{
    
    if (id.equalsIgnoreCase("Xerces")||id.equalsIgnoreCase("JAXP"))  // Jaxp Engine
      {
	return new JaxpXMLParser();
	
      }
	
    //	if (id.equalsIgnoreCase("Docuverse"))  // XT Engine
    //	{
    //		return new DocuverseXMLParser(browser);		 
    //	}
   	
    else  // Other Engines , To be included Later
      {
	throw new ClassNotFoundException("XML parser '"+id+"' is not supported. Specify it and load again");
     }
  }
  
  
}
