/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/*
* X-smiles project 1999
*
* $Id: XMLParser.java,v 1.16 2006/12/15 11:03:55 mpohja Exp $
*
 */


package fi.hut.tml.xsmiles.xml;

import org.w3c.dom.*;
import org.xml.sax.SAXException;


import java.net.URL ;
import java.io.Reader;
import java.io.Writer;
import java.io.InputStream;
import java.io.IOException;

import org.xml.sax.SAXParseException;

/**
 * Contains the DOM-implementation used by the current configuration.
 * 
 */
public interface XMLParser
{
   
 //public String useXSLEngine(URL xml,URL xsl);
 
 /**
 * Reads an document to DOM tree      
 * @return read DOM document.
 */   
 public Document openDocument(Reader sourceReader, boolean presentationDOM) throws IOException,SAXException;
 /**
  * Reads a document from an InputStream into a DOM tree.
  * This method was added for i18n.  A parser assumes that a Reader source
  * is already in Unicode, whereas it will check the xml header for the
  * encoding of an InputStream.
  *
  * @return the read in DOM document
  */
 public Document openDocument(InputStream sourceStream, boolean presentationDOM, String documentURL) throws IOException,SAXException;
 public Document openDocument(URL url) throws IOException,SAXException;
 public Document openDocument(URL url, String documentClassname) throws IOException,SAXException;
 public org.w3c.dom.Document createEmptyDocument();
 public String write(Node node);  
 public String write(Node node,boolean prettyPrinting);  
 public void write(Writer writer,Node node, boolean prettyPrinting) throws IOException;
 public void writeWithoutXMLDecl(Writer writer, Node node) throws IOException;
 public SAXParseException getLastParseException();
 public Document openDocument(InputStream sourceStream, boolean presentationDOM) throws IOException,SAXException;
  
    
} 
