/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.xml.serializer;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.xml.XMLParser;

import org.w3c.dom.*;
import org.w3c.dom.traversal.*;

import java.io.*;


/**
 * the class for serializing the XForms instance as URL encoded for get
 */
public class URLSerializer implements Serializer{
	
  /** the reference to the xforms handler */
  //XFormsElementHandler handler;
  /** The separator string; default is ";" */
  String separator = ";";
	
  /*
  public URLSerializer(XFormsElementHandler a_handler) {
      Log.error("Never call this method");
  }*/

  public URLSerializer(String a_separator)
  {
    this.separator = a_separator;
  }
	
  /** serialize this node and its ancestors */
  public void serialize(Node root, Writer wr)
  {
    try
      {
	String url = "";
	DocumentTraversal trav=null;
	// traverse the tree
	if (root instanceof DocumentTraversal) trav=(DocumentTraversal)root;
	else trav = (DocumentTraversal)root.getOwnerDocument();
	NodeIterator iter = trav.createNodeIterator(root,NodeFilter.SHOW_ALL,null,true);
	// for each element, create a new URL parameter
	Node next = iter.nextNode();
	boolean written=false;
	while (next!=null)
	  {
	    if (next.getNodeType()==Node.ELEMENT_NODE)
	      {
		Element e = (Element)next;
		String part = this.createURLFromElement(e);
		// todo: URL encoding
		if (part!=null) 
		  {
		    if (written)
		      wr.write(separator);
		    else written=true;
		    wr.write(part);
		  }
	      }
	    next=iter.nextNode();
	  }
      } catch (Exception e)
	{
	  Log.error(e);
	}
  }
  
  public static String getTextIfOneChild(Element e)
  {
    // get first text node, and check that there are no child elements
    boolean childElements=false;
    String text=null;
    Node n = e.getFirstChild();
    while (n!=null)
      {
	if (n.getNodeType()==Node.ELEMENT_NODE) 
	  {
	    childElements=true;
	    break;
	  }
	else if (text==null && n.getNodeType()==Node.TEXT_NODE)
	  {
	    text=n.getNodeValue();
	  }
	n=n.getNextSibling();
      }
    if (childElements==false)
      {
          return text;
      } else return null;
  }
	
  protected static String createURLFromElement(Element e)
  {
    // get first text node, and check that there are no child elements
    boolean childElements=false;
    String text=null;
    Node n = e.getFirstChild();
    while (n!=null)
      {
	if (n.getNodeType()==Node.ELEMENT_NODE) 
	  {
	    childElements=true;
	    break;
	  }
	else if (text==null && n.getNodeType()==Node.TEXT_NODE)
	  {
	    text=n.getNodeValue();
	  }
	n=n.getNextSibling();
      }
    if (childElements==false)
      {
	String localn = java.net.URLEncoder.encode(e.getLocalName());
	if (text==null) return localn+"=";
	else return localn+"="+java.net.URLEncoder.encode(text);
	
      } else return null;
  }
	
  // TODO: do we need multiple roots, then we need to add serialize(Vector nodes, String rootnode, Writer wr)
}

