/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.xml.serializer;

import java.io.*;

import org.w3c.dom.Node;


/**
 * the interface for serializing the XForms instance
 */
public interface Serializer {
	
	/** serialize this node and its ancestors */
	//public void serialize(Node root, Writer wr);
}

