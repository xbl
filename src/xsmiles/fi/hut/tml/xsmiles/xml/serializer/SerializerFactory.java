/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.xml.serializer;

import java.io.*;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Node;


/**
 * the interface for serializing the XForms instance
 */
public class SerializerFactory {
	
    protected SerializerFactory m_instance;
    
    public static final short SERIALIZER_XML = 3;
    public static final short SERIALIZER_URL = 5;
    
    public SerializerFactory()
    {
    }
    
	/** get an instance of a serializerfactory */
    public SerializerFactory getInstance()
    {
        if (m_instance==null) m_instance=new SerializerFactory();
        return m_instance;
    }
    
	/** get an instance of a serializer */
    public Serializer getSerializer(short type)
    {
        if (type==SERIALIZER_XML) return this.getXMLSerializer();
        Log.error(this+ " unknown serializer type: "+type);
        return null;
    }
    
    public XMLSerializerInterface getXMLSerializer()
    {
        return new XercesXMLSerializer();
        //return new XMLSerializer();
    }

	
}

