/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Dec 23, 2005
 *
 */
package fi.hut.tml.xsmiles.xml.serializer;

import java.io.OutputStream;

import org.apache.xerces.dom.DOMImplementationImpl;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.serialize.DOMWriterImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.DOMWriter;
import org.w3c.dom.ls.DOMWriterFilter;

import fi.hut.tml.xsmiles.Log;


public interface XMLSerializerInterface extends Serializer
{
    public void setFilter(DOMWriterFilter filter);
    public void setOmitXMLDeclaration(boolean omit);    /** set the indent on/off */
    public void setIndent(boolean ind);
    /** set standalone on/off */
    public void setStandalone(boolean sa);
    
    public String writeToString(Node elem) throws Exception;
    public String writeToString(Node elem, boolean fixupNamespaces) throws Exception;   
    public void writeNode(OutputStream out,Node elem);

}
