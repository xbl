/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.xml.serializer;


import org.apache.xml.serialize.*;
import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.ls.DOMWriter;
import org.w3c.dom.ls.DOMWriterFilter;
import org.w3c.dom.ls.DOMImplementationLS;
import org.apache.xerces.dom.DOMImplementationImpl;
import org.apache.xerces.impl.Constants;



import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.xslt.JaxpXSLT;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
//import fi.hut.tml.xsmiles.xml.XMLSerializer;


import java.io.*;


/**
 * the class for serializing the XForms instance as XML (simple, uses just a serializer to do the job)
 */
public class XercesXMLSerializer implements XMLSerializerInterface
{
    protected DOMWriterFilter fDOMFilter;
    
    protected boolean omitXMLDeclaration = false, indent=false,standalone=false;
    /** the XML version, null=default (1.0) */
    protected String version=null;
    
    public XercesXMLSerializer()
    {
    }
    
    /** sets a DOMWriterFilter */
    public void setFilter(DOMWriterFilter filter)
    {
        fDOMFilter=filter;
    }
    
    /** omit XML declaration */
    
    public void setOmitXMLDeclaration(boolean omit)
    {
        this.omitXMLDeclaration = omit;
    }
    
    /** set the indent on/off */
    public void setIndent(boolean ind)
    {
        this.indent=ind;
    }
    
    /** set standalone on/off */
    public void setStandalone(boolean sa)
    {
        this.standalone=sa;
    }
    
    
    public String writeToString(Node elem) throws Exception
    {
        // by default we fixup namespaces
        return this.writeToString(elem,true);
    }
    public String writeToString(Node elem, boolean fixupNamespaces) throws Exception
    {
        Document adoc;
        if (elem instanceof Document) adoc=(Document)elem;
        else adoc = elem.getOwnerDocument();
        //boolean origStandalone = adoc.getStandalone();
        DOMWriterImpl domWriter = this.getWriter(adoc);
        if (domWriter instanceof DOMWriterImpl)
        {
            DOMWriterImpl impl = (DOMWriterImpl)domWriter;
            //Xerces 2.4.0 does not reckognize these
            if (fixupNamespaces == false)
            {
                //impl.setParameter(org.apache.xerces.impl.Constants.NAMESPACES_FEATURE,new Boolean(fixupNamespaces));
            }
            //impl.setParameter(Constants.DOM_XMLDECL,new Boolean(!omitXMLDeclaration));
            //impl.setParameter(Constants.DOM_FORMAT_PRETTY_PRINT, new Boolean(this.indent));
            this.setStandalone(adoc,standalone);
        }
        String ret =  domWriter.writeToString(elem);
        //adoc.setStandalone(origStandalone);
        return ret;
    }
    
    /** X-Smiles 0.8 packs with  a version of Xerces, that actually does not support this
     * but a later version will support it
     */
    protected void setStandalone(Document doc, boolean standalone)
    {
        java.lang.reflect.Method setStandalone = null;
        // this should run under JDK 1.1.8...
        try {
            setStandalone = doc.getClass().getMethod("setStandalone", new Class[]{Boolean.class});
            if(setStandalone != null ) {
                setStandalone.invoke(doc, new Object[]{new Boolean(standalone)});
            }
        } catch (Exception e) {
            // no way to test the version...
            // ignore the exception
            //Log.error(e);
        }        
    }

    /*
            ser.fFeatures.put(Constants.DOM_ENTITIES, Boolean.FALSE);
        ser.fFeatures.put(Constants.DOM_WHITESPACE_IN_ELEMENT_CONTENT, Boolean.TRUE);
        ser.fFeatures.put(Constants.DOM_DISCARD_DEFAULT_CONTENT, Boolean.TRUE);
        ser.fFeatures.put(Constants.DOM_CANONICAL_FORM, Boolean.FALSE);
        ser.fFeatures.put(Constants.DOM_FORMAT_PRETTY_PRINT, Boolean.FALSE);
        ser.fFeatures.put(Constants.DOM_XMLDECL, Boolean.TRUE);
        ser.fFeatures.put(Constants.DOM_UNKNOWNCHARS, Boolean.TRUE);
     */
    
    protected DOMWriterImpl getWriter(Document adoc)
    {
        DOMWriterImpl domWriter = new DOMWriterImpl();
        String encoding = null;
        if (adoc!=null && adoc instanceof DocumentImpl)
        {
            encoding = ((DocumentImpl)adoc).getEncoding();
        }
        if (encoding == null) encoding = "ISO-8859-1";
        domWriter.setEncoding(encoding);
        if (this.fDOMFilter!=null) domWriter.setFilter(this.fDOMFilter);
        return domWriter;
    }
    public void writeNode(OutputStream out,Node elem)
    {
        try
        {
            // create DOM Serializer
            Document adoc;
            if (elem instanceof Document) adoc=(Document)elem;
            else adoc = elem.getOwnerDocument();
            DOMWriter domWriter = this.getWriter(adoc);
            domWriter.writeNode(out,elem);
        } catch (Exception e)
        {
            Log.error(e,"While trying to write a node: "+elem);
        }
    }

    
    // TODO: do we need multiple roots, then we need to add serialize(Vector nodes, String rootnode, Writer wr)
}

