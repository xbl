package fi.hut.tml.xsmiles;

import java.awt.Container;
import java.net.URL;

import fi.hut.tml.xsmiles.content.ContentLoaderListener;
import fi.hut.tml.xsmiles.content.ContentManager;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.content.xml.MLFCManager;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.focusmanager.FocusPointsProvider;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.messaging.Messaging;
import fi.hut.tml.xsmiles.mlfc.MLFCController;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.xslt.XSLTEngine;

/**
 * BrowserWindowInterface is the interface for the browser core. It contains all 
 * relevant methods for controlling the instance of the browser.
 * (note: now, there is only one browser window, so the browser class 
 * extends this interface, but in the future when there will be more
 * browser windows, the BrowserWindow class will implement this interface)
 *
 * @author Juha
 */
public interface BrowserWindow {
   
  /**
   * Open a new BrowserWindow with a GUI, loading the browser homepage initially
   */
  public void newBrowserWindow();

  /**
   * @return the BrowserID object of the browser.
   * @see BrowserID
   */
  public BrowserID getID();

  /**
   * @param displayGUI Does the browser have a GUI
   * @param uri Initial URI
   * @param id The id of the new window
   * @return a reference to the browser
   */
  public BrowserWindow newBrowserWindow(String uri, String id, boolean displayGUI);

  
  /**
   * @param displayGUI Does the browser have a GUI
   * @param uri Initial URI
   * @return a reference to the browser
   */
  public BrowserWindow newBrowserWindow(String uri, boolean displayGUI);
  
    /**
   * @param displayGUI Does the browser have a GUI
   * @param uri Initial URI
   * @return a reference to the browser
   */
  public void newBrowserWindow(XLink uri);
  //public BrowserWindow newBrowserWindow(XLink uri, boolean displayGUI);
  
  //public URL getXResource(String id) throws MalformedURLException;

    /**
     * @param guiName The name of the GUI to be used
     * @param uri Initial URI
     * @param loadInitialDoc Display initial page
     * @return a reference to the browser
     */
    public BrowserWindow newBrowserWindowWithGUI(String uri, String guiName, boolean loadInitialDoc);

    /**
     * Wait for the browser to achieve a certain state, ie. wait for the browser to finish fetching the document by waiting
     * BrowserLogic.READY. It helps the xframes to build the actual frames-layout before showing anything. There should 
     * be some other way to do it, though.. 
     * 
     * @param state the state to wait for
     */
    public void waitState(int state);

  /**
   * @param displayGUI Does the browser have a GUI
   * @return a reference to the browser with initial page as the homepage
   */
  public BrowserWindow newBrowserWindow(boolean displayGUI);


  /**
   * With GUI and the initial URL url
   * @param url intial url to load
   */
  public void newBrowserWindow(String url);

  /**
   * Close this BrowserWindow
   */
  public void closeBrowserWindow();


  /**
   * Shutdown all browser instances. (Exit browser)
   */
  public void closeBrowser();

  /**
   *?@param id the ID of the browser to get
   * @return the browser windows instance with the id
   */
  public BrowserWindow getBrowserWindow(String id);

    /**
     * @return the MLFC listener attached with this browserwindow instance
     */
    public MLFCListener getMLFCListener();

    /**
     * @param l Set the mlfclistener associated with this browserwindow
     */
    public void setMLFCListener(MLFCListener l);

  /**
   * @return The EventBroker of the Browser
   */
  public EventBroker getEventBroker();

  /** 
   * Navigate back, forward, reload, change view, stop etc.
   *
   * @param command See NavigationState for static variables associated with commands
   * @see NavigationState
   */
  public void navigate(int command);

  /**
   * Gets the document that has recently been valid (shown)
   *
   * @return The recent valid document (previous document shown)
   */
  public XMLDocument getXMLDocument();

  /**
   * @param headline Display error page in browser with this headline, if no page can be rendered,
   *                 a dialog is shown
   * @param message And with this message.
   */
  public void showErrorDialog(String headline, String message);

  /**
   * Open the Log window, to output debug data
   */
  public void openLog();
    
  /**
   * Gets the version of the X-smiles browser.
   * @return core's version
   */ 
  public String getVersion();

  /**
   * @return The current viewtype. 
   * @see XsmilesView for valid viewtypes
   * @deprecated There is no need for viewtype anymore
   */
  public int getViewType();

  /**
   * Returns the state of the browser.
   *
   * @return the current <code>state</code> of the browser
   * @see    BrowserLogic
   */
  public int getState();

  /**
   * The accessor method for the <code>GUI</code>.
   *
   * @return a reference to the core's GUI component, null returned, if no GUI exists.
   * @see GUI
   */
  public GUI getCurrentGUI();

  /**
   * MLFCManager is a toolkit, for rendering XML documents, using the MLFCs that 
   * are registered in the configuration file. 
   * 
   * @return The MLFC Manager of browser
   * @see MLFCManager
   */
  //public MLFCManager getMLFCManager();
  
  public MLFCController getMLFCController();
    
  /**
   * @return The Java Messaging Handler
   * @deprecated I guess jms has been broken for quite a while?
   */
  public Messaging getMessageHandler();

  /**
   * The accessor method for the <code>DocumentHistory</code>.
   *
   * @return a reference to the system's <code>DocumentHistory</code>
   * @see DocumentHistory
   */
  public DocumentHistory getDocumentHistory();

  /**
   * The accessor method for the <code>XMLConfigurer</code>.
   *
   * @return a reference to the system's configurer
   * @see XMLConfigurer
   */
  public XMLConfigurer getBrowserConfigurer();

  /**
   * The accessor method for the <code>XMLParser</code>.
   *
   * @return a reference to the XMLParser Class
   * @see XMLParser
   */
  public XMLParser getXMLParser();

  /** 
   * The accesor method for the <code>XSLEngine </code>.
   *
   * @return a reference to the XSLEngine class
   * @see XSLEngine
   */ 
  public XSLTEngine getXSLEngine();

  /**
   * Shows the current document in the selected view. Source view, 
   * document view, etc are supported. 
   *
   * @param viewType defines the required view
   * @see XsmilesView
   */
  //public void changeViewType(int viewType);

  /**
   * Causes an secondary XML document to be retrieved and parsed. Creates 
   * a thread for this action.
   * Creates the secondary MLFC thru MLFCManager
   *
   * @param link XLink object containing the URL of the required document
   * @aContainer the Container in which to display the secondary MLFC
   *
   * @see XMLDocument
   */
  public XSmilesContentHandler displayDocumentInContainer(XLink aLink, Container aContainer);
  /**
   * Causes an secondary XML document to be retrieved and parsed. Creates 
   * a thread for this action.
   * Creates the secondary MLFC thru MLFCManager
   *
   * @param link XLink object containing the URL of the required document
   * @aContainer the Container in which to display the secondary MLFC
   *
   * @see XMLDocument
   */
  public XSmilesContentHandler displayDocumentInContainer(XLink aLink, Container aContainer,ContentLoaderListener listener);

    /**
     * same as displayDocumentInContainer, but
     * will not call prefetch and play, so it is up to the user to call these functions
     */
    public XSmilesContentHandler createContentHandler(XLink link, Container cArea, boolean primary) throws Exception;
    
    /**
     * same as displayDocumentInContainer, but
     * will not call prefetch and play, so it is up to the user to call these functions
     */
    public XSmilesContentHandler createContentHandler(String contentType, XLink link, Container cArea, boolean primary) throws Exception;
    /**
     * same as displayDocumentInContainer, but
     * will not call prefetch and play, so it is up to the user to call these functions
     */
    public XSmilesContentHandler createContentHandler(XLink link, Container cArea, boolean primary, ContentLoaderListener listener) throws Exception;

  /**
   * @return Java version.
   */
  public double getJavaVersion();

  /**
   * @return the GUIManager, null if no GUI for this browser instance
   * @see GUIManager
   */
  public GUIManager getGUIManager();
    
    /**
   * @return the default GUI name
   */
  public String getDefaultGUIName();
    
  /**
   * @return The resourceLocation of Browser
   */
  public String getResourceLocation();
    
  /**
   * @return path to config.xml file with configuration
   */
  //public String getOptionsDocument();

  /**
   * @return BrowserLogic, which controls the states of the Browser
   */
  public BrowserLogic getBrowserLogic();

  /**
   * @return the content area of the browser
   */
  public Container getContentArea();

  /**
   * @return a componentFactory for cross-device gui components 
   */
  public ComponentFactory getComponentFactory();
  
  public MLFCControls getMLFCControls();
  
  public ContentManager getContentManager();

  /**
   * Open location in the window with the correct id. Otherwise open new window with default GUI
   */
  public void openLocation(XLink link, String windowID);

  /**
   * @param s Surf to the URI
   */
  public void openLocation(String s);

  /**
   * @param u Surf to the URI
   */
  public void openLocation(URL u);

  /**
   * @param u Surf to the URI
   * @param save Save in document history
   */
  public void openLocation(URL u, boolean save);

  /**
   * @param u Surf to the XLink
   * @param save Save in document history
   */
  public void openLocation(XLink u, boolean save);
  
    /**
   * @param u open a new URLCOnnection to this XLink url
   */
  public XSmilesConnection openConnection(XLink u) throws Exception;

  /**
   * @return current page as a XLink
   **/
  public XLink getCurrentPage();
  
    public void showErrorDialog(String heading, 
			      String description, boolean popup, Throwable t);
    public void showErrorDialog(String heading, 
			      String description, boolean popup);

   /**
    * @return the duration of last page load in secs
    */
    public double getDocumentLoadDuration();
    
    
/**
 * @param title Sets the preferred stylesheet title.
 */
  public void setPreferredStylesheetTitle(String title);
/**
 * @param title Sets the preferred stylesheet title.
 */
  public String getPreferredStylesheetTitle();
  
  /**
   * return the current zoom level
   */
 public double getZoom();  
 /**
   * set the current zoom level
   */
 public void setZoom(double zoom);  

 public void registerFocusPointProvider(FocusPointsProvider fpp);

 public void unRegisterFocusPointProvider(FocusPointsProvider fpp);
}
