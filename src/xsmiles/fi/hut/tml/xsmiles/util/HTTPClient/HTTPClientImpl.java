/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util.HTTPClient;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.auth.CredentialsProvider;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.protocol.Protocol;


import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.NavigatorObject;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.HTTPInterface;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.util.XSmilesConnectionJava;
import fi.hut.tml.xsmiles.util.HTTPClient.ssl.EasySSLProtocolSocketFactory;

/**
 * @author honkkis
 *
 */
public class HTTPClientImpl implements HTTPInterface{
	static
	{
		try
		{
			System.setProperty("apache.commons.httpclient.cookiespec", "COMPATIBILITY");
			if (HTTP.debug)
			{
			 System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
			 System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
			 //System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
			 System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error");
			}
		} catch (Exception e)
		{
			// silently fail
		}
	}
	HttpClient client;
	PasswordAuthenticatorHTTP auth;
	BrowserWindow browser;
	public HTTPClientImpl()
	{
		this.init();
	}
	public boolean allowUntrustedCertificates=true;
	protected void init()
	{
		if (allowUntrustedCertificates)
		{
			Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 443);
			Protocol.registerProtocol("https", easyhttps);
		}
		MultiThreadedHttpConnectionManager connectionManager = 
      		new MultiThreadedHttpConnectionManager();
		connectionManager.setMaxConnectionsPerHost(10);
      	client = new HttpClient(connectionManager);
		auth=new PasswordAuthenticatorHTTP();
		if (this.browser!=null)
			auth.setComponentFactory(this.browser.getComponentFactory());
		
        client.getParams().setParameter(
                CredentialsProvider.PROVIDER,auth);
        
        try
        {
            // read systemproperties set by XMLConfigurer based on config.xml
			Properties props = System.getProperties();
			String proxyHost = (String)props.get("http.proxyHost");
			if (proxyHost!=null&&proxyHost.length()>0)
			{
			    int proxyPort = Integer.parseInt((String)props.get("http.proxyPort"));
			    this.setProxy(client,proxyHost,proxyPort);
			}
        }
        catch (Throwable t)
        {
            Log.error("HTTPClient: NONFATAL: failed to set the proxy: "+t.toString()+" : "+t.getMessage());
        }

	}
	
	protected void setProxy(HttpClient client, String proxyhost, int proxyport)
	{
        HostConfiguration hc = client.getHostConfiguration();//new HostConfiguration();
        //hc.setProxy("wwwproxy.hut.fi", 8080); // Why hard coded?
        hc.setProxy(proxyhost, proxyport);
        client.setHostConfiguration(hc);
		Log.debug("HTTPClient : Using HTTP proxy: "+proxyhost+":"+proxyport);
	}
	/* TODO: set proxy if necessary (read the system properties for proxy */
	/*
    HttpClient client = new HttpClient();
    HostConfiguration hc = new HostConfiguration();
    hc.setHost(TARGET_HOST, TARGET_PORT, Protocol.getProtocol("http"));
    hc.setProxy(proxy.getLocalAddress(), proxy.getLocalPort());
    client.setHostConfiguration(hc);
    */

	
	/* 
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#get(java.net.URL, fi.hut.tml.xsmiles.BrowserWindow)
	 */
	public XSmilesConnection get(URL dest, BrowserWindow b) throws Exception {
		GetMethod method = new GetMethod(dest.toString());
		this.setRequestProperties(method);
		client.executeMethod(method);
		return new XSmilesConnectionHTTP(method,client);
	}

	/* 
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#post(java.net.URL, byte[], java.util.Hashtable, java.lang.String)
	 */
	public XSmilesConnection post(URL dest, byte[] data, Hashtable properties, String content_type) throws Exception {
        Log.debug("HTTPClient: Posting to URL: "+dest.toString());
        // Open connection to the URL
        PostMethod method=new PostMethod(dest.toString());
        addDataAndProperties(dest,data,properties,content_type,method);
		this.setRequestProperties(method);
		client.executeMethod(method);
        return new XSmilesConnectionHTTP(method,client);
	}
    private void addDataAndProperties(URL dest, byte[] data, Hashtable properties, String content_type,EntityEnclosingMethod method) throws IOException,ProtocolException
    {
        // Specify the content type.
        if (content_type!=null&&content_type.length()>0)
        	method.setRequestHeader("Content-Type",content_type);
        
        // Write data to the connection
        writedata(data, method);
        
	}
	
    private static void writedata(byte[] data, EntityEnclosingMethod method) throws ProtocolException, IOException
    {
            method.setRequestEntity(new ByteArrayRequestEntity(data));
    }
    
    public void setRequestProperties(HttpMethod connection)
    {
        // TODO: get this from the contenthandler!
        connection.setRequestHeader("Accept",this.getAcceptHeader());
        connection.setRequestHeader("User-Agent", NavigatorObject.userAgent);
        connection.setRequestHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        connection.setRequestHeader("Accept-Language", "en-us,en;q=0.5");
    }
	/* 
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#put(java.net.URL, byte[], java.util.Hashtable, java.lang.String)
	 */
	public XSmilesConnection put(URL dest, byte[] data, Hashtable properties, String content_type) throws Exception {
		// TODO Auto-generated method stub
        Log.debug("HTTPClient: Putting to URL: "+dest.toString());
        // Open connection to the URL
        PutMethod method=new PutMethod(dest.toString());
        addDataAndProperties(dest,data,properties,content_type,method);
		this.setRequestProperties(method);
		client.executeMethod(method);
        return new XSmilesConnectionHTTP(method,client);
	}
    
    public String getAcceptHeader()
    {
        return "text/xml,application/xml,application/xhtml+xml,image/svg+xml,text/html;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1";
    }


	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#setBrowser(fi.hut.tml.xsmiles.BrowserWindow)
	 */
	public void setBrowser(BrowserWindow b) {
		// TODO Auto-generated method stub
		this.browser=b;
		if (this.browser!=null)
			auth.setComponentFactory(this.browser.getComponentFactory());
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#enableDebug(boolean)
	 */
	public void enableDebug(boolean debug) {
		// TODO Auto-generated method stub
		 if (debug) System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
		 else
			 System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error");
		 this.init();
	}
}
