/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util.HTTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * @author honkkis
 *
 */
public class XSmilesConnectionHTTP implements XSmilesConnection{
	HttpMethod method;
	HttpClient client;
	URL url;
	public XSmilesConnectionHTTP(HttpMethod met, HttpClient cl)//, URL u)
	{
		this.method=met;
		this.client=cl;
	}
	private void connect() throws HttpException, IOException
	{
	}
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.XSmilesConnection#getURL()
	 */
	public URL getURL() {
		// TODO Auto-generated method stub
		try
		{
			this.connect();
			return new URL(method.getURI().toString());
		} catch (Exception e)
		{
			Log.error(e);
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.XSmilesConnection#getInputStream()
	 */
	public InputStream getInputStream() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			this.connect();
			return method.getResponseBodyAsStream();
		}
		catch (HttpException e)
		{
			Log.error(e);
			throw new IOException (e.getMessage());
		}
	}
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.XSmilesConnection#getContentType()
	 */
	public String getContentType() {
		// TODO Auto-generated method stub
		try
		{
			final String CT="Content-Type";
			this.connect();
			Header h = method.getResponseHeader(CT); 
			if (h==null) return null;
			else return h.getValue();
		}
		catch (Exception e)
		{
			Log.error(e);
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.XSmilesConnection#releaseConnection()
	 */
	public void releaseConnection() {
		this.method.releaseConnection();
		
	}
}
