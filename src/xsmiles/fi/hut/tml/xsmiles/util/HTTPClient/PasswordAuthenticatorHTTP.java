/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util.HTTPClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.PasswordAuthentication;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScheme;
import org.apache.commons.httpclient.auth.CredentialsNotAvailableException;
import org.apache.commons.httpclient.auth.CredentialsProvider;
import org.apache.commons.httpclient.auth.NTLMScheme;
import org.apache.commons.httpclient.auth.RFC2617Scheme;

import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XAuthDialog;

/**
 * @author honkkis
 *
 */
public class PasswordAuthenticatorHTTP implements CredentialsProvider {
    protected static ComponentFactory factory;
    public PasswordAuthenticatorHTTP() {
        super();
    }
    
    
    public Credentials getCredentials(
        final AuthScheme authscheme, 
        final String host, 
        int port, 
        boolean proxy)
        throws CredentialsNotAvailableException 
    {
        if (authscheme == null) {
            return null;
        }
        try{
            if (authscheme instanceof NTLMScheme) {
                System.out.println(host + ":" + port + " requires Windows authentication");
                /*
                System.out.print("Enter domain: ");
                String domain = readConsole();   
                System.out.print("Enter username: ");
                String user = readConsole();   
                System.out.print("Enter password: ");
                String password = readConsole();
                return new NTCredentials(user, password, host, domain);
                */ 
                return null;    
            } else
            if (authscheme instanceof RFC2617Scheme) {
                System.out.println(host + ":" + port + " requires authentication with the realm '" 
                    + authscheme.getRealm() + "'");
                String user,password;
                /*
                System.out.print("Enter username: ");
                String user = readConsole();   
                System.out.print("Enter password: ");
                String password = readConsole();
                */
                XAuthDialog ma = factory.getXAuthDialog();
                ma.setTitle("Enter password");
                ma.setPrompt("Enter username and password for \""+authscheme.getRealm() +"\" at "+host);
                int ret = ma.select();
                if (ret==XAuthDialog.SELECTION_FAILED)
                    throw new CredentialsNotAvailableException("User cancelled:");
                //return new PasswordAuthentication(ma.getUser(),ma.getPasswd().toCharArray());

                return new UsernamePasswordCredentials(ma.getUser(), ma.getPasswd());    
            } else {
                throw new CredentialsNotAvailableException("Unsupported authentication scheme: " +
                    authscheme.getSchemeName());
            }
        } catch (IOException e) {
            throw new CredentialsNotAvailableException(e.getMessage(), e);
        }
    }


    public void setComponentFactory(fi.hut.tml.xsmiles.gui.components.ComponentFactory f)
    {
        factory=f;
    }

}

