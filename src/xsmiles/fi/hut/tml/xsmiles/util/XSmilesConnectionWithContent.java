/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import fi.hut.tml.xsmiles.XLink;


/**
 * @author honkkis
 *
 */
public class XSmilesConnectionWithContent implements XSmilesConnection
{
	protected XLink iLink;
    public XSmilesConnectionWithContent(XLink aLink)
    {
    	iLink=aLink;
    }
    public URL getURL()
    {
        return this.iLink.getURL();
    }
    public InputStream getInputStream() throws IOException
    {
        return this.iLink.getContent();
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.util.XSmilesConnection#getContentType()
     */
    public String getContentType()
    {
    	//TODO: parameter
        return "application/xml";
        //return "text/plain";
    }
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.XSmilesConnection#releaseConnection()
	 */
	public void releaseConnection() {
		// TODO Auto-generated method stub
		this.iLink=null;
	}
}
