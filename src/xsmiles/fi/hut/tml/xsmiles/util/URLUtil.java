/* X-Smiles (See XSMILES_LICENCE.txt)
 */



package fi.hut.tml.xsmiles.util;

import fi.hut.tml.xsmiles.Log;
import java.io.*;

import java.net.*;


/*
 * This is the URL util
 *
 */

public class URLUtil
{
    
    public static File urlToFile(URL res)
    {
        String externalForm = res.toExternalForm(  );
        if (externalForm.startsWith("file:"))
        {
            return new File(decode(externalForm.substring(5)));
        }
        return null;
    }
    
    public static URL fileToURL(File f) throws IOException, MalformedURLException
    {
        // getAbsolutePath() does not seem to work always
        String absolute = f.getAbsolutePath();
         
        if (File.separatorChar != '/')
            absolute = absolute.replace(File.separatorChar, '/');
        if (!absolute.startsWith("/"))
            absolute = "/" + absolute;
        if (!absolute.endsWith("/") && f.isDirectory())
            absolute = absolute + "/";
        return new URL("file:" + absolute);
    }
    
    public static String decode(String urls)
    {
        // try to use reflection to do decoding, works only in java 1.2
        try
        {
            return (String)Class.forName("java.net.URLDecoder").getDeclaredMethod("decode", new Class[]
            {String.class}).invoke(null,new Object[]
            {urls});
        } catch (Throwable t)
        {
            Log.debug("URL decoding works only in jdk 1.2");
            return urls;
        }
    }
}
