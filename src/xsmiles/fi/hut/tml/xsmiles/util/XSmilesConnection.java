/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


/**
 * @author honkkis
 *
 */
public interface XSmilesConnection
{
    public URL getURL();
    public InputStream getInputStream() throws IOException;
    public String getContentType();
    /** YOU MUST CALL THIS METHOD AFTER READING THE CONTENTS */
    public void releaseConnection();
}
