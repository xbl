/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


/**
 * @author honkkis
 *
 */
public class XSmilesConnectionJava implements XSmilesConnection
{
    protected URLConnection urlConnection;
    public XSmilesConnectionJava(URLConnection conn)
    {
        this.urlConnection=conn;
    }
    public URL getURL()
    {
        return this.urlConnection.getURL();
    }
    public InputStream getInputStream() throws IOException
    {
        return this.urlConnection.getInputStream();
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.util.XSmilesConnection#getContentType()
     */
    public String getContentType()
    {
        return this.urlConnection.getContentType();
    }
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.XSmilesConnection#releaseConnection()
	 */
	public void releaseConnection() {
		// TODO Auto-generated method stub
		this.urlConnection=null;
	}
}
