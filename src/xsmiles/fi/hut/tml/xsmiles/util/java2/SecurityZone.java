/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 14, 2004
 *
 */
package fi.hut.tml.xsmiles.util.java2;

import java.security.AccessControlContext;


/**
 * @author honkkis
 *
 */
public abstract class SecurityZone
{
	public abstract AccessControlContext getContext();

}
