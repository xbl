/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 13, 2004
 *
 */
package fi.hut.tml.xsmiles.util.java2;

import java.security.AccessControlContext;

/**
 * @author honkkis
 * @author Igor Bukanov igor at fastmail.fm
 *
 */
public class SecurityZones {
	protected AccessControlContext untrusted_context,fileread_context;
	protected static SecurityZones instance;
	/** cached objects */
	protected static SecurityZone scriptSecurityZone;
	public static SecurityZones getInstance()
	{
		if (instance==null) instance=new SecurityZones();
		return instance;
	}
	public SecurityZone getScriptContext()
	{
	    if (scriptSecurityZone==null)
	        scriptSecurityZone= new ScriptSecurityZone();
	    return scriptSecurityZone;
	}

}
