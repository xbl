/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 14, 2004
 *
 */
package fi.hut.tml.xsmiles.util.java2;

import java.io.FilePermission;
import java.net.SocketPermission;
import java.security.AccessControlContext;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.ProtectionDomain;


/**
 * @author honkkis
 *
 */
public class ScriptSecurityZone extends SecurityZone
{
	protected AccessControlContext context;
	protected static ScriptSecurityZone instance;
	
	/** in future give also the URL */
	public AccessControlContext getContext()
	{
		if (this.context==null)
		{
		    PermissionCollection pc = get_fileread_permissions();
		    ProtectionDomain[] untrusted_domains
		        = { new ProtectionDomain(null, pc) };
		    context= new AccessControlContext(untrusted_domains);
		    // The result can be cached ...
		}
		return context;
	}

	/**
	 * permissions for scripts
	 * @return
	 */
	PermissionCollection get_fileread_permissions()
	{
		PermissionCollection pc = new Permissions();
		//pc.add(new File(".").getAbsolutePath().toString(),"read")
		pc.add(new FilePermission("<<ALL FILES>>","read"));
		pc.add(new SocketPermission("*", "connect"));
	    pc.add(new SocketPermission("*", "resolve"));
		
	    // Construct PermissionCollection corresponding to permission
	    // untrusted code can have. Typically it should allow only
	    // access to some system properties
		return pc;
	} 
}
