/* X-Smiles (See XSMILES_LICENCE.txt)
 */



package fi.hut.tml.xsmiles.util.java2;


import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.lang.reflect.*;

import fi.hut.tml.xsmiles.util.PassAuthenticator;

import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;



/*
 * Java2 password authenticator
 */

public class XPasswordAutheticator extends Authenticator implements PassAuthenticator
{
    
    protected ComponentFactory factory;
    
    
    protected PasswordAuthentication getPasswordAuthentication()
    {
        
        if (factory!=null)
        {
            XAuthDialog ma = factory.getXAuthDialog();
            if (ma!=null)
            {
                String realm=getRequestingPrompt();
                String host=this.getRequestingHostReflection();
                //XAuthDialog ma = b.getCurrentGUI().getComponentFactory().getXAuthDialog();
                ma.setTitle("Enter password");
                ma.setPrompt("Enter username and password for \""+realm+"\" at "+host);
                int ret = ma.select();
                return new PasswordAuthentication(ma.getUser(),ma.getPasswd().toCharArray());
            }
        }
        return null;
    }
    
    public void setComponentFactory(fi.hut.tml.xsmiles.gui.components.ComponentFactory f)
    {
        factory=f;
    }
    
    public String getRequestingHostReflection()
    {
        try
        {
                Class au = this.getClass();
                Method method = au.getDeclaredMethod("getRequestingHost",
                    new Class[]{}
                    ); // need array of parameter types?
                if (method != null)
                {
                    Object ret = method.invoke(null, new Object[] {});
                    if (ret!=null&&ret instanceof String) return (String)ret;
                }
        } catch (java.lang.Exception e)
        {
            return null;
        }
        return null;
    
    }
    
}
