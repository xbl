/* X-Smiles (See XSMILES_LICENCE.txt)
 */
package fi.hut.tml.xsmiles.util;

import fi.hut.tml.xsmiles.Log;
import java.util.Hashtable;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

import java.lang.reflect.Method;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.ecma.NavigatorObject;

import fi.hut.tml.xsmiles.mlfc.general.Base64EncoderStream;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import fi.hut.tml.xsmiles.Utilities;
/*
 * A convinience class which can be statically used to do HTTP connections
 * Actually, it also allows other types of connections.
 * It also checks, if HTTPClient by apache is available in Classpath and uses that for
 * HTTP connections is possible.
 */

public class HTTP
{
	public static boolean debug=true;

	private final static String HTTP_CLIENT_CLASSNAME="fi.hut.tml.xsmiles.util.HTTPClient.HTTPClientImpl"; 
	private static HTTPInterface httpClient;
    private static HTTPJavaImpl httpJava=new HTTPJavaImpl();
	private static HTTPInterface defaultClient=httpJava;
    
    static
	{
    	// checks whether the HTTPClient is in the classpath and instantiates it
    	try
		{
    		if (true)
    		{
	    		httpClient = (HTTPInterface)Class.forName(HTTP_CLIENT_CLASSNAME).newInstance();
	    		defaultClient=httpClient;
	    		Log.info("Using HTTPClient for HTTP connections");
    		}
		} catch (Throwable t)
		{
			Log.info("NON-FATAL: HTTPClient was not in classpath, using Java's HTTP client");
		}
	}

    private static boolean isHTTPProtocol(URL dest)
	{
    	String protocol = dest.getProtocol();
    	if (protocol.equals("http")||protocol.equals("https")) return true;
    	return false;
	}
    protected static HTTPInterface getHTTPInterface(URL dest, BrowserWindow b)
    {
    	HTTPInterface http = httpJava;
    	if (httpClient!=null&&httpClientEnabled)
    	{
    		if (isHTTPProtocol(dest)) http=httpClient;
    		if (b!=null) httpClient.setBrowser(b);
    	}
    	return http;
    }
    public static XSmilesConnection get(URL dest, BrowserWindow b) throws Exception
    {
    	return getHTTPInterface(dest,b).get(dest,b);
    }
    protected static boolean httpClientEnabled=true;
    public static void enableHTTPClient(boolean en)
    {
    	httpClientEnabled=en;
    }
    public static void enableDebug(boolean en)
    {
    	debug=en;
    	if (httpClient!=null)
    	{
    		httpClient.enableDebug(en);
    	}
    }
    
    /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public static XSmilesConnection post(URL dest,String data,Hashtable properties, String content_type) throws Exception
    {
        return post(dest,data.getBytes(),properties,content_type);
    }
    /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public static XSmilesConnection post(URL dest,byte[] data,Hashtable properties, String content_type) throws Exception
    {
    	return getHTTPInterface(dest,null).post(dest,data,properties,content_type);
    }
    
        /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public static XSmilesConnection put(URL dest,String data,Hashtable properties, String content_type) throws Exception
    {
        return put(dest,data.getBytes(),properties,content_type);
    }
        /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public static XSmilesConnection put(URL dest,byte[] data,Hashtable properties, String content_type) throws Exception
    {
    	return getHTTPInterface(dest,null).put(dest,data,properties,content_type);
    }
    
    

}
