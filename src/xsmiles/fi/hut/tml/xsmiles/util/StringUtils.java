/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



// StringUtils.java
//    Utility methods for strings.
//
//    muilu@ebi.ac.uk
//    April, 2000
//

//package embl.ebi.utils;
package fi.hut.tml.xsmiles.util;

import java.util.*;
//import embl.ebi.tools.*;
import java.io.*;

/**
 * StringUtils.java
 *
 * Several utilities dealing with strings.
 *
 * Taken from freely available java tools at:
 * http://industry.ebi.ac.uk/~senger/tools/
 * 
 * <P>
 * @author <A HREF="mailto:muilu@ebi.ac.uk">Juha Muilu</A>
 * @version $Id: StringUtils.java,v 1.1 2006/02/06 13:22:42 honkkis Exp $
 **/

public abstract class StringUtils { 

    /** Version and date of the last update.
     */
    public static final String VERSION = "$Id: StringUtils.java,v 1.1 2006/02/06 13:22:42 honkkis Exp $";

    /**
     * remove occurrences of character CHAR from  string STR.
     * <p>
     * @param CHAR The character to be removed. 
     * @param str  The string from which to remove them
     * @return     string with CHARs removed
     **/
    public static String  stripChar(char CHAR,String  str) { 
	char s[] = str.toCharArray();
	int padds = 0;
	for ( int i = 0;  i < s.length; i++) {
	    if ( s[ i] == CHAR ) padds++;
	}
	char n[] = new char[ s.length - padds];
	int j = 0;
	for ( int i = 0;  i < s.length; i++) {
	    if ( s[ i] !=  CHAR ) 
		n[ j++] = s[ i] ;
	}
	return new String( n);
    } // stripChar()

    /**
     * number of occurrences of character CHAR before location loc in string
     * str. 
     * <p>
     * @param CHAR 	character whose occurrences should be counted
     * @param str 	the string in which to do this
     * @param loc 	the offset in str at which to start.
     * @return 		number of occurrences.
     *
     **/
    public static int numBeforeLocation( char CHAR, String str, int loc) { 
	char c[] = str.toCharArray();
	int ix = 0;
	for ( int i = 0 ; i < Math.min (loc, c.length) ; i++ ) {  // M.S. corrected
	    if ( c[i] == CHAR ) ix ++ ;
	}
	return ix;
    }

    /**
     * return list of positions of character CHAR inside string str.
     * <p>
     * @param CHAR 	the character whose positions are sought
     * @param str 	the string in which to find them
     * @return 		a (possibly empty) list of integers that lists the
                        positions at which CHAR was found.
     * 
     **/
    public static int[] positions( char CHAR, String str) { 
	char c[] = str.toCharArray();
	int ix = 0;
	for ( int i = 0 ; i < c.length ; i++ ) { 
	    if ( c[i] == CHAR ) ix ++ ;
	}
	int res[] = new int[ ix];
	ix = 0;
	for ( int i = 0 ; i < c.length ; i++ ) { 
	    if ( c[i] == CHAR ) { 
		res[ ix++] =  i; 
	    }
	}
	return res;
    }
    
    /**
     * removes all whitespace and 'unprintables characters' from String str,
     * and returns new one. 
     * <p>
     * @param  str The string on which  to operate
     * @return a string that doesn't have the whitespace nor unprintables
     */
    public static String removeWhites(String str) {
	StringBuffer buf = new StringBuffer();   // M.S. changed to StringBuffer
	char c[] = str.toCharArray();
	for ( int i = 0 ; i < c.length ; i++ ) { 
	    if ( c[i] > ' ' )  
                buf.append (c[ i]);
	}
	return new String (buf);
    }



    /** Opposite of split; concatenates STRINGLIST using DELIMITER as the
     *  separator. The separator is only added between strings, so there will
     *  be no separator at the beginning or end. 
     * <p>
     * @param  stringList The list of strings that will to be put together
     * @param  delimiter  The string to put between the strings of stringList
     * @return            string that has DELIMITER put between each of the
                          elements of stringList
     */
    public static String join (String[] stringList, String delimiter) {
        StringBuffer buf = new StringBuffer (stringList.length*20);
	synchronized (stringList) {
	    int len = stringList.length;
	    for (int i = 0; i < len - 1; i++) {
		buf.append (stringList [i]);
		buf.append (delimiter);
	    }
            if (len > 0)
		buf.append (stringList [len - 1]);
	}
        return buf.toString();
    }

    /** Make one string out of stringList by putting spaces between each
        element. Same as join(stringList, " "); 
     * <p>
     * @param  stringList the list of strings that will be put together
     * @return            a string that has ' ' put between each of the
                          elements of stringList. 
     * @see               #join(String[],String) join(String[],String)

     */
    public static String join(String [] stringList) {
        return join(stringList, " ");
    }


    /** In string S, replace all occurrence of FROM by TO and return this
     * string. If string FROM is empty, this will be considered to match no
     * single occurrence of it in the target string, hence no replacements
     * will be made.

     * <p>
     * @param string The string in which the replacements will be made
     * @param from String that, when it occurs in string, will be replaced
     * @param to The replacement of from in string
     * @return A new string
     */
    public static String replace(String string, String from, String to) {
        if (from.equals("")) 
            return string;
        StringBuffer buf = new StringBuffer(2*string.length());

        int previndex=0;
        int index=0;
        int flen = from.length();
        while (true) { 
            index = string.indexOf(from, previndex);
            if (index == -1) {
                buf.append(string.substring(previndex));
                break;
            }
            buf.append( string.substring(previndex, index) + to );
            previndex = index + flen;
        }
        return buf.toString();
    } // replace

    /** Delete all occurrences of substrings from a string. Same as
        replace(string, from, "") 
     * <p>
     * @param  string The string from which substrings will be deleted.
     * @param  delete The substrings that are to be deleted.
     * @return        new string.
     * @see    #replace(String, String, String) replace(String, String, String)
     */ 
    public static String delete(String string, String delete) {
        return replace(string, delete, "");
    }

    /** Opposit string. 
     * @param str string
     * @param pos positions of chars to be removed (must be ordered from lowest to highest and position can occur only once)
     * @return new string from which the chars are removed
     **/
    public static String removePositions( String str , int pos[] ) {
	char[] n = new char [ str.length() - pos.length ];
	char[] s = str.toCharArray();
	int shift = 0;
	for ( int i = 0; i < s.length ; i++ ) {
	    if ( shift < pos.length && i == pos[ shift]) {
		shift++;
	    } else {
		n[ i - shift] = s[ i];
	    }
	    
	}
	return new String ( n);
    }

} // StringUtils
