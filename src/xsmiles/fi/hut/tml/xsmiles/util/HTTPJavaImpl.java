/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.NavigatorObject;
import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import fi.hut.tml.xsmiles.mlfc.general.Base64EncoderStream;

/**
 * @author honkkis
 *
 */
public class HTTPJavaImpl implements HTTPInterface{
    
    private static String encoding;
    /** used for opening the dialog, if not supplied in the method */
    private static BrowserWindow lastBrowserWindow;
    
    /** the protection space is host:realm (www.w3c.org:my_realm)*/
    private  Hashtable protectionSpaces = new Hashtable();
    
    private  PassAuthenticator auth;
    
    {
        // if in j2se, install my own password authenticator
        // for jdk1.1 support, use reflection here!
        try
        {
            Class ge = Class.forName("fi.hut.tml.xsmiles.util.java2.XPasswordAutheticator");
            if (ge != null)
            {
                Object authenticator=ge.newInstance();
                Class au = Class.forName("java.net.Authenticator");
                Method method = au.getDeclaredMethod("setDefault",
                    new Class[]{au}
                    ); // need array of parameter types?
                if (method != null)
                {
                    method.invoke(null, new Object[] {authenticator});
                    auth = (PassAuthenticator)authenticator;
                }
            }
        }
        catch (Throwable th)
        {
            Log.error("Non-fatal error while instantiating an HTTP authenticator, must be in jdk 1.1:");
            Log.error(th);
        }
        
        //java.net.Authenticator authenticator = new fi.hut.tml.xsmiles.util.java2.XPasswordAutheticator();
        //java.net.Authenticator.setDefault(authenticator);
        //auth = (PassAuthenticator)authenticator;
    }
    /**
     * Post data to an URL and return the InputStream to the reply
     *
     */
    public  XSmilesConnection get(URL dest, BrowserWindow b) throws Exception
    {
        URLConnection conn =  getConn(dest,b);
        return new XSmilesConnectionJava(conn);
    }
    private  URLConnection getConn(URL dest, BrowserWindow b) throws Exception
    {
        if (b!=null&&b.getCurrentGUI()!=null)
            if (b!=null&&auth!=null) auth.setComponentFactory(b.getCurrentGUI().getComponentFactory());
        
        Log.debug("HTTP: Getting from URL: "+dest.toString());
        if (b==null)
            b = lastBrowserWindow;
        else
            lastBrowserWindow=b;
        // HTTP post is always syncronous
        // Open connection to the URL
        URLConnection conn = openConnection(dest);
        conn.connect();
        if (conn instanceof HttpURLConnection)
        {
            HttpURLConnection httpconn = (HttpURLConnection)conn;
            int code = httpconn.getResponseCode();
            String realm = getRealm(httpconn.getHeaderField("WWW-Authenticate"));
            String host = dest.getHost();
            
            if (code==401&&auth==null)
            {
                // ----------------- JDK 1.1 SUPPORT -------------------
                // This is actually never called in Java2, since the XPasswordAuthenticator is used there
                // try once with the previous passwd
                String enc = (String)protectionSpaces.get(host+":"+realm);
                
                if (enc!=null)
                {
                    httpconn = (HttpURLConnection)openConnection(dest);
                    httpconn.setRequestProperty  ("Authorization", "Basic " + enc);
                    httpconn.connect();
                    code = httpconn.getResponseCode();
                }
                if (code==401)
                {
                    // Popup Window to request username/password password
                    XAuthDialog ma = b.getCurrentGUI().getComponentFactory().getXAuthDialog();
                    ma.setTitle("Enter password");
                    ma.setPrompt("Enter username and password for \""+realm+"\" at "+host);
                    int ret = ma.select();
                    // Need to work with URLConnection to set request property
                    httpconn = (HttpURLConnection)openConnection(dest);
                    if (ret==XAuthDialog.SELECTION_OK)
                    {
                        String userPassword = ma.getUser()+":"+ma.getPasswd();
                        
                        // Encode String
                        //encoding = new sun.misc.BASE64Encoder().encode (userPassword.getBytes());
                        
                        // or
                        encoding = base64Encode(userPassword.getBytes());
                        protectionSpaces.put(host+":"+realm,encoding);
                        
                        httpconn.setRequestProperty  ("Authorization", "Basic " + encoding);
                    }
                }
                
            }
            return httpconn;
        }
        return conn;
    }
    
    /** encodes an string for jdk 1.1 authentication support */
    public  String base64Encode(byte[] bytes)
    {
        try
        {
            ByteArrayOutputStream ba = new ByteArrayOutputStream();
            Base64EncoderStream b64 = new Base64EncoderStream(ba,true);
            b64.write(bytes);
            ba.flush();b64.flush();b64.close();
            return ba.toString();
        } catch (IOException e)
        {
            Log.error(e);
        }
        return "";
    }
    
    protected  URLConnection openConnection(URL dest) throws IOException
    {
        URLConnection connection = dest.openConnection();
        setRequestProperties(connection);
        return connection;
    }
    
    /** parses the WWW-Authenticate property and returns the realm
     * for jdk1.1 support
     */
    protected  String getRealm(String property)
    {
        if (property==null||property.length()<1) return "";
        int start = property.indexOf('\"');
        int end = property.lastIndexOf('\"');
        return property.substring(start+1,end);
    }
    
    public  void setRequestProperties(URLConnection connection)
    {
        // TODO: get this from the contenthandler!
        connection.setRequestProperty("Accept", this.getAcceptHeader());
        connection.setRequestProperty("User-Agent", NavigatorObject.userAgent);
        connection.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        connection.setRequestProperty("Accept-Language", "en-us,en;q=0.5");
    }
    public String getAcceptHeader()
    {
        return "text/xml,application/xml,application/xhtml+xml,image/svg+xml,text/html;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1";
    }
    /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public  XSmilesConnection post(URL dest,String data,Hashtable properties, String content_type) throws Exception
    {
        return post(dest,data.getBytes(),properties,content_type);
    }
    /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public  XSmilesConnection post(URL dest,byte[] data,Hashtable properties, String content_type) throws Exception
    {
        Log.debug("Posting to URL: "+dest.toString());
        // HTTP post is always syncronous
        // Open connection to the URL
        URLConnection conn = openConnection(dest);
        // Specify the content type.
        if (content_type!=null&&content_type.length()>0)
            conn.setRequestProperty("Content-Type", content_type);
        
        // Write data to the connection
        writedata(data, conn, "POST");
        // Write data to the connection
        //Log.debug("Writing data:"+conn.getRequestProperty("content-type"));
        //Log.debug(new String(data));
        
        return new XSmilesConnectionJava(conn);
    }
    
        /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public  XSmilesConnection put(URL dest,String data,Hashtable properties, String content_type) throws Exception
    {
        return put(dest,data.getBytes(),properties,content_type);
    }
        /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public  XSmilesConnection put(URL dest,byte[] data,Hashtable properties, String content_type) throws Exception
    {
        Log.debug("Posting to URL: "+dest.toString());
        // HTTP post is always syncronous
        // Open connection to the URL
        URLConnection conn = openConnection(dest);
        if (conn instanceof HttpURLConnection)
            ((HttpURLConnection)conn).setRequestMethod("PUT");
        // Specify the content type.
        if (content_type!=null&&content_type.length()>0)
            conn.setRequestProperty("Content-Type", content_type);
        
        writedata(data, conn, "PUT");
        
        return new XSmilesConnectionJava(conn);
    }
    
    private  void debug(InputStream in)
    {
        try
        {
            int c;
            while ( (c = in.read())!=-1)
            {
                System.out.print((char)c);
            }
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    /*
    private static void writedata(String data, URLConnection conn, String method) throws ProtocolException, IOException
    {
        writeData(data.getBytes(),conn,method);
    }*/
    
    private static void writedata(byte[] data, URLConnection conn, String method) throws ProtocolException, IOException
    {
        if (conn instanceof HttpURLConnection)
        {
            ((HttpURLConnection)conn).setRequestMethod(method);
            ((HttpURLConnection)conn).setDoOutput(true);
            OutputStream out = conn.getOutputStream();
            // Send data to stream
            out.write(data);
            //out.write("\n\n".getBytes());
            out.flush();out.close();
        } else
        {
            Log.debug("Connection was not HTTP!");
        }
    }
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#setBrowser(fi.hut.tml.xsmiles.BrowserWindow)
	 */
	public void setBrowser(BrowserWindow b) {
		// TODO Auto-generated method stub
		
	}
	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.util.HTTPInterface#enableDebug(boolean)
	 */
	public void enableDebug(boolean debug) {
		// TODO Auto-generated method stub
		
	}
    

}
