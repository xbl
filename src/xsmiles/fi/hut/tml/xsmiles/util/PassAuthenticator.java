/* X-Smiles (See XSMILES_LICENCE.txt)
 */
package fi.hut.tml.xsmiles.util;

import fi.hut.tml.xsmiles.Log;
import java.util.Hashtable;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

import fi.hut.tml.xsmiles.gui.components.XAuthDialog;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;

/*
 * Java2 password authenticator interface, so that this compiles in JDK11
 */

public interface PassAuthenticator
{
    public void setComponentFactory(ComponentFactory f);
}
