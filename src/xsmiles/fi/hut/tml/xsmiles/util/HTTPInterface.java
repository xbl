/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 22, 2004
 *
 */
package fi.hut.tml.xsmiles.util;

import java.net.URL;
import java.util.Hashtable;

import fi.hut.tml.xsmiles.BrowserWindow;

/**
 * @author honkkis
 *
 */
public interface HTTPInterface {
	public void setBrowser(BrowserWindow b);
	public void enableDebug(boolean debug);
    public  XSmilesConnection get(URL dest, BrowserWindow b) throws Exception;
    /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public XSmilesConnection post(URL dest,byte[] data,Hashtable properties, String content_type) throws Exception;
    
        /**
     * Post data to an URL and return the InputStream to the reply
     * TODO: support for authentication!
     */
    public XSmilesConnection put(URL dest,byte[] data,Hashtable properties, String content_type) throws Exception;

}
