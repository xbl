/* X-Smiles (See XSMILES_LICENCE.txt)
 */



package fi.hut.tml.xsmiles.util;

import fi.hut.tml.xsmiles.Log;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
    

/*
 * This is the event util
 * 
 */

public class EventUtil {
    
    /** a property request is e.g. right mouse click */
    public static boolean isPropertyRequest(AWTEvent e)
    {
        if (e instanceof MouseEvent)
        {
            return ((MouseEvent)e).isMetaDown();
        }
        return false;
    }
    /** for instance, a middle click is follow alternative 
      It is not recommended to compare the return value of this method via == because new modifiers can be added in the future. For example, the appropriate way to check that SHIFT and BUTTON1 are down, but CTRL is up is demonstrated by the following code:

    int onmask = SHIFT_DOWN_MASK | BUTTON1_DOWN_MASK;
    int offmask = CTRL_DOWN_MASK;
    if (event.getModifiersEx() & (onmask | offmask) == onmask) {
        ...
    }
 */
    public static boolean isFollowAlternativeRequest(AWTEvent e)
    {
        if (e instanceof InputEvent)
        {
            InputEvent me = (InputEvent)e;
            return (isControlPressed(me)||isMiddleClick(me));
        }
        return false;
    }
    
    public static boolean isControlPressed(InputEvent me)
    {
            int mod = me.getModifiers();
            int onmask = InputEvent.CTRL_MASK;
            if ((mod & onmask) == onmask) return true;
            return false;
    }

    public static boolean isMiddleClick(InputEvent me)
    {
            int mod = me.getModifiers();
            int onmask = InputEvent.BUTTON2_MASK;
            if ((mod & onmask) == onmask) return true;
            return false;
    }

}
