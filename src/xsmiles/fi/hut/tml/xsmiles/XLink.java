/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/*
* X-smiles project 1999
 */


package fi.hut.tml.xsmiles;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
/**
* Contains the data in an XLink and the functions required for the XPointer 
* fragments.
*
* @author	Jukka Heinonen
* @version	$Revision: 1.21 $
*/
public class XLink 
{ 
  // Link types
  public static final int SIMPLE = 1;
  public static final int POST = 2;
  public static final int FAKE_WITH_CONTENT = 644;  

  public static final String FILEURLPREFIX= new String("file:///");

  // Post method's
  public static final int POST_HTTP = 1;
  public static final int POST_JMS = 2;
  public static final int POST_URLENCODED_HTTP = 3;
  public static final int POST_FORMDATA_HTTP = 4;
  public static final int POST_MULTIPART_HTTP = 5;
  public static final int PUT_HTTP = 6;  
  
  private int    m_linkType;
  private URL    m_url;
  //private String m_postData;	  // the data to be posted
  private byte[] m_postData;	  // the data to be posted
  protected int 	 m_method;
  
  /** this field specifies a mime type that should be used instead of the mime
   type from the connection */
  private String m_mimeOverride;
  private Hashtable m_properties; // the post properties, e.g. mimetype
  /**
  * The constructor of the XLink object.
  *
  * @param url a String that contains the URL the link points to.
  * @param linkType     The type of the link, an enumerated type (e.g. 1 means "SIMPLE") 
  *  
  */
  public XLink(String url, int linkType)
  {
  	  if (url!=null) 
	  {
	      try {
	          m_url = new URL(url);
	      }
	      catch (MalformedURLException e) {
	          Log.error(e);
	          m_url = null;
	      }
	  }
      m_linkType = linkType;
      return;
  }
  
    public XLink(String url)
  {
      this(url,SIMPLE);
  }
  
  /**
  * Initializes XLink with URL parameter. Link type is set SIMPLE.
  *
  * @param url URL to be assigned to XLink
  * @author Aki
  */
  public XLink(URL url)
  {
    m_url = url;
    m_linkType = SIMPLE;
  }
    
  /**
  * Initializes XLink with POST data. Link type is set POST.
  *
  * @param method Can be either XLink.POST_HTTP or POST_JMS
  * @author honkkis
  */
  public XLink(URL url,String data,int method,Hashtable properties)
  {
      this(url,data.getBytes(),method,properties);
  }
  /**
  * Initializes XLink with POST data. Link type is set POST.
  *
  * @param method Can be either XLink.POST_HTTP or POST_JMS
  * @author honkkis
  */
  public XLink(URL url,byte[] data,int method,Hashtable properties)
  {
    m_url = url;
    m_linkType = POST;
	m_postData=data;
	m_properties=properties;
	m_method=method;
  }

  /**
   * some XLinks may want to override the MIME type 
   * gotten from the URL connection.
   * if this is null, then no overriding is used
   */
  public String getMIMEOverride()
  {
  	return m_mimeOverride;
  }
  
  /**
   * some XLinks may want to override the MIME type 
   * gotten from the URL connection.
   * if this is null, then no overriding is used
   */
  public void setMIMEOverride(String override)
  {
  	m_mimeOverride = override;
  }
  /**
  * Accessor method.
  * @return url the URL of the link.
  */
  public URL getURL()
  {
    return m_url;
  }

    /**
  * Set the URL, for example after a redirect has happened
  */
  public void setURL(URL url)
  {
    m_url=url;
  }

  public String getURLString() 
  {
  	if (m_url==null) return "";
      else return m_url.toString();    
  }
  
  public int getLinkType()
  {
  	return m_linkType;
  }
  public int getMethod()
  {
  	return m_method;
  }
  public byte[] getPostData()
  {
  	return m_postData;
  }
  
  public void removePostData()
  {
  	m_postData=null;
  }
  
  public Hashtable getProperties()
  {
  	return m_properties;
  }
  
  public String getContentType()
  {
      try
      {
          if (this.getProperties()!=null)
          {
              String contentType = (String)this.getProperties().get("Content-Type");
              if (contentType!=null) return contentType;
          }
          // fallback
          if (this.m_linkType == POST)
          {
              return "application/xml";
          }
      } catch (Exception e)
      {
          Log.error(e);
          return null;
      }
      return null;
  }
  
  /** this may be implemented by subclasses, if they want to hold the content in themselves */
  public InputStream getContent()
  {
  	return null;
  }
}
