/* X-Smiles
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * look in licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles;

import java.awt.Container;
import java.net.URL;

import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.dom.XMLBroker;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.protocol.wesahmi.BrowserSubscriber;

/**
 * A Realization of the MLFCListener interface which ties MLFCs to the Browser.
 */
public class BrowserMLFCListener implements MLFCListener {
    
    protected BrowserWindow fBrowser;
    protected BrowserSubscriber browserSubscriber;
    
    public BrowserMLFCListener(BrowserWindow browser)
    {
        fBrowser=browser;
    }
    /**
     * @param title Set the title of the current GUI to title.
     */
    public void setTitle(String title)
    {
        fBrowser.getCurrentGUI().setTitle(title);
    }
    
    /**
     * open in new browserwindow
     */
    public void openLocationTop(String url) {
        fBrowser.newBrowserWindow(url);
    }

    /**
     * open in new browserwindow
     */
    public void openLocationTop(XLink link, String id) {
        fBrowser.openLocation(link,id);
    }
       
    
	/** get the current zoom level */
	public double getZoom()
	{
		return fBrowser.getZoom();
	}

    /**
     * opens link in new tab
     */
    public void openInNewTab(XLink l, String id)
    {
        fBrowser.getGUIManager().getCurrentGUI().openInNewTab(l,id);
    }

    /**
     * @return true if GUI can open new tabs
     */
    public boolean getIsTabbedGUI()
    {
        return fBrowser.getGUIManager().getCurrentGUI().isTabbed();
    }

    /**
     * Open URL in browser
     */
    public void openLocation(URL l)
    {
        fBrowser.openLocation(l);
    }

    public void openLocation(String s)
    {
        fBrowser.openLocation(s);
    }

    /**
     * @param displayGUI Does the browser have a GUI
     * @param uri Initial URI
     * @param id The id of the new window
     * @return a reference to the browser
     */
    // public BrowserWindow newBrowserWindow(String uri, String id, boolean displayGUI) {
    //  return fBrowser.newBrowserWindow(uri,id,displayGUI);
    // }
    
    /**
     * @param displayGUI Does the browser have a GUI
     * @param uri Initial URI
     * @return a reference to the browser
     */
    // public BrowserWindow newBrowserWindow(String uri, boolean displayGUI)
    // {
    //  return fBrowser.newBrowserWindow(uri,displayGUI);
    // }

    /**
     * @param displayGUI Does the browser have a GUI
     * @return a reference to the browser with initial page as the homepage
     */
    // public BrowserWindow newBrowserWindow(boolean displayGUI) {
    //  return fBrowser.newBrowserWindow(displayGUI);
    // }

    /**
     * @param prop Parameter in XPath e.g. (main/parser)
     * @return configuration property from browser's configurator.
     */
    public String getProperty(String prop)
    {
        return fBrowser.getBrowserConfigurer().getProperty(prop);
    }

    /**
     * @param prop A GUI property requested.
     * @return the value
     * @see XMLConfigurator
     */
    public String getGUIProperty(String prop)
    {
        return fBrowser.getBrowserConfigurer().getGUIProperty(fBrowser, prop);
    }

    /**
     * @param l Display the document pointed to with link in
     * @param c Container supplied.
     */
    public XSmilesContentHandler displayDocumentInContainer(XLink l, Container c)
    {
        return fBrowser.displayDocumentInContainer(l,c);
    }
    
    /**
     * same as displayDocumentInContainer, but
     * will not call prefetch and play, so it is up to the user to call these functions
     */
    public XSmilesContentHandler createContentHandler(XLink link, Container cArea, boolean primary) throws Exception
    {
        return fBrowser.createContentHandler(link,cArea,primary);
    }
    
    public XSmilesContentHandler createContentHandler(String contentType, XLink link, Container cArea, boolean primary) throws Exception
    {
        return fBrowser.createContentHandler(contentType, link,cArea,primary);
    }


    public void showErrorDialog(String title, String message) 
    {
        fBrowser.showErrorDialog(title,message);
    }

    /**
     * @param status The status of the MLFC in string form
     */
    public void setStatusText(String status)
    {
        fBrowser.getCurrentGUI().setStatusText(status);
    }

    /**
     * If stand-alone. Then this is ir-relevant.
     *
     * @param s The new state in which to put the browser in
     * @see BrowserLogic enumeration for valid states. 
     */
    public void setBrowserState(int i)
    {
        fBrowser.getBrowserLogic().setState(i);
    }

    /**
     * @return the componentFactory
     */
    public ComponentFactory getComponentFactory()
    {
        return fBrowser.getGUIManager().getCurrentGUI().getComponentFactory();
    }

	    
    /**
     * @see interface for details
     */
    public double getJavaVersion()
    {
        return fBrowser.getJavaVersion();
    }

    public Container getContentArea() 
    {
        return fBrowser.getContentArea();
    }
    
    /**
     * Navigate back, forward, reload, change view, stop etc.
     *
     * @param command See NavigationState for static variables associated with commands
     * @see NavigationState
     */
    public void navigate(int command)
    {
        this.fBrowser.navigate(command);
    }


    /**
     * Checks if there is a MLFC available for the given namespace URI.
     * @param namespace	URI for the namespace.
     * @return 			true if namespace has an MLFC.
     */
    public boolean isNamespaceSupported(String namespace)
    {
        String mlfc = XMLBroker.getMLFCClassNS(namespace);
        if (mlfc == null)
            return false;
        else
            return true;	
    }
    
    /** The modes are from XSmilesView class  */
    public void showSource(XMLDocument doc, int mode, String heading)
    {
        fBrowser.getCurrentGUI().showSource(doc,mode,heading);
    }
    
    /**
     * open a link popup
     */
    public void showLinkPopup(URL url, XMLDocument doc,java.awt.event.MouseEvent e)
    {
        /*
        SwingLinkPopup popup = new SwingLinkPopup();
        popup.show(e.getComponent(),e.getX(),e.getY(),doc,url,this);
         */
        fBrowser.getComponentFactory().showLinkPopup(url,doc,e,this);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.MLFCListener#getMLFCControls()
     */
    public MLFCControls getMLFCControls() {
        return fBrowser.getGUIManager().getCurrentGUI().getMLFCControls();
    }
    
    public void openURLFromExternalProgram(String url)
    {
        XMLConfigurer conf = fBrowser.getBrowserConfigurer();
        String target = conf.getProperty("gui/openextlinks");
        if (target.equals("tab"))
            openInNewTab(new XLink(url), "external");
        else if (target.equals("window"))
            openLocationTop(url);
        else
            openLocation(url);
    }

    public void openURLFromExternalProgram(String url, String id)
    {
        XMLConfigurer conf = fBrowser.getBrowserConfigurer();
        String target = conf.getProperty("gui/openextlinks");
        if (target.equals("tab"))
            openInNewTab(new XLink(url), id);
        else if (target.equals("window"))
            openLocationTop(new XLink(url), id);
        else
            openLocation(url);
    }
	public void reloadCurrentPage() {
		
		Log.debug("Reload URL: "+ fBrowser.getCurrentPage().getURLString());
            openLocation(fBrowser.getCurrentPage().getURLString());
	}
    
    public void openURLFromExternalProgram(BrowserSubscriber browserSubscriber, String url, String id)
    {
        this.browserSubscriber = browserSubscriber;
        openURLFromExternalProgram(url, id);
    }
    
    public void closeView()
    {
        if (browserSubscriber != null)
            browserSubscriber.closeView();
    }
    
    public void setSubscriber(BrowserSubscriber s)
    {
        this.browserSubscriber = s;
        
    }
    public BrowserSubscriber getSubscriber()
    {
        return browserSubscriber;
    }
}
