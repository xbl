/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.signature;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JFrame;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.content.ResourceReferencer;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.XFormsModelElement;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


/**
 *
 *
 * @author hguo
 */
public class SignatureElementImpl extends XSmilesElementImpl 
implements EventHandlerService, ActionListener{


	SignatureMLFC signatureMLFC = null;
	XFormsElementImpl XFormsMLFC = null;
	public Document instancedoc = null;
	public Document xmldoc = null;
	DocumentImpl ownerDoc = null;
    VerifyStream vStream;
	
	SignDialog dialog;
	
	public static final short SIGNATURE_ENVELOPED = 1;
	public static final short SIGNATURE_ENVELOPING = 10;
	public static final short SIGNATURE_DETACHED = 100;

    static {
       org.apache.xml.security.Init.init();
    }

    public SignatureElementImpl(DocumentImpl owner, SignatureMLFC signature, String namespace, String tag) {
		    super(owner, namespace, tag);
			signatureMLFC = signature;
			ownerDoc = owner;
			Log.debug("Signature created!");	
	}

	public void activate(Event evt) 
	{
		Log.debug("Signature.activate");
		showDialog();
	}
	
	void showDialog()
	{
	    if (signingKey==null) signingKey=SignatureCreator.getKey(signingKey);
	    if (dialog!=null)
	    {
	        dialog.disposeDialog();
	    }
		dialog = new SignDialog();
		dialog.setActionListener(this);
		dialog.setComponentFactory(this.signatureMLFC.getMLFCListener().getComponentFactory());
		short sType = this.getSignatureType();
	    dialog.showDialog(sType);
	    //dialog.showDialog(instancedoc, xmldoc,this.signatureMLFC.getXMLDocument().getXMLURL(),sType);
	    dialog.writeToInstance(this.getRefNode());
	    dialog.writeToUI(this.getUINode());
	}
	
	protected short getSignatureType()
	{
	    String typeS = this.getAttribute("type");
	    if (typeS.equals("enveloped")) return SIGNATURE_ENVELOPED;
	    return SIGNATURE_ENVELOPING;
	}
   
   public void destroy()
   {
       if (vStream!=null) vStream.close();
   	    if (dialog!=null) dialog.disposeDialog();
   		Log.debug("Signature element destroy() called!");
   		super.destroy();
   }   
	
   public static void debugNode(Node n)
   {
		try
		{
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(
				new java.io.OutputStreamWriter(System.out),
				n,true,true,false);
		} catch (Exception e)
		{
			Log.error(e);
		}
   }

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e)
	{
	    try
	    {
		    Log.debug(e.toString());
		    if (e.getActionCommand().equalsIgnoreCase("sign"))
		    {
		        this.sign(this.getSignatureType());
		    }
		    else if (e.getActionCommand().equalsIgnoreCase("verify"))
		    {
		        this.verify();
		    }
		    else if (e.getActionCommand().equalsIgnoreCase("Select Key"))
		    {
		        this.selectKey();
		    }
	        else if (e.getActionCommand().equals("Show Tree")) {
	        }

	    } catch (Exception ex)
	    {
	        Log.error(ex);
	    }
	}
	protected void showTree(Element sig)
	{
	    this.dialog.showTree(sig);
	}
	
	protected SigningKey signingKey = null;
    protected KeyDialog keyDialog;
	protected void selectKey()
	{
	    if (keyDialog!=null) keyDialog.disposeDialog();
	    keyDialog=new KeyDialog();
	    final KeyDialog dialog;
	    keyDialog.setKey(this.signingKey);
	    keyDialog.setActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                SigningKey  key = 
                    keyDialog.getSigningKey();
                if (key!=null) signingKey=key;
            }
	    });
	    keyDialog.setComponentFactory(this.getComponentFactory());
	    signingKey = keyDialog.showDialog();
	    Log.debug("Signing key:"+signingKey);
	}
	
	Node getUINode()
	{
	    URL url = this.signatureMLFC.getXMLDocument().getXMLURL();
	    Document xmldoc=null;
		try
		{
		    xmldoc = signatureMLFC.getXMLDocument().getBrowser().getXMLParser().openDocument(url);
		}catch(Exception e)
		{
			Log.error(e);
		}
		return xmldoc;
	}
	
	Node getToNode()
	{
	    	try
	    	{
	    	    XFormsModelElement model = this.getModel();
	    	    String to = this.getAttribute("to");
	    	    XPathExpr expr = model.getXPathEngine().createXPathExpression(to);
	    	    NodeList nl = model.getXPathEngine().evalToNodelist(model.getInstanceDocument(null),expr,this,null);
	    	    Node n = nl.item(0);
	    	    return n;
	    	}
	    	catch (Exception e)
	    	{
	    	    Log.error(e);
	    	    return null;
	    	}
	}
	Node getRefNode()
	{
	    	try
	    	{
	    	    XFormsModelElement model = this.getModel();
	    	    if (this.getAttributeNode("ref")==null)
	    	    {
	    	        return model.getInstanceDocument(null);	    	        
	    	    }
	    	    else
	    	    {
		    	    String to;
	    	        to = this.getAttribute("ref");
		    	    XPathExpr expr = model.getXPathEngine().createXPathExpression(to);
		    	    NodeList nl = model.getXPathEngine().evalToNodelist(model.getInstanceDocument(null),expr,this,null);
		    	    Node n = nl.item(0);
		    	    return n;
	    	    }
	    	}
	    	catch (Exception e)
	    	{
	    	    Log.error(e);
	    	    return null;
	    	}
	}
	
	public XFormsModelElement getModel()
	{
	    try
	    {
		    // now just return the first model...
		    Element el = (Element)this.getOwnerDocument().getElementsByTagNameNS(XFormsConstants.XFORMS_NS,"model").item(0);
		    return (XFormsModelElement)el;
	    } catch (Exception e)
	    {
	        Log.error(e);
	        return null;
	    }
	    
	}

	
	public Vector getModels()
	{
	    Vector models=new Vector();
	        NodeList nl = this.getOwnerDocument().getElementsByTagNameNS(XFormsConstants.XFORMS_NS,"model");
		    // now just return the first model...
	        for (int i=0;i<nl.getLength();i++)
	        {
	    	    try
	    	    {
		            XFormsModelElement el; 
	    	        el = (XFormsModelElement)nl.item(i);
		    	    models.addElement(el);
	    	    } catch (Exception e)
	    	    {
	    	        Log.error(e);
	    	    }
	        }
	    return models;
	}

	void replaceInstanceNode(Node from, Node to) throws Exception
	{
	    if (from==null||to==null) return;
	    /*
	    if (to.getNodeType()==Node.DOCUMENT_NODE)
	    {
	        to=((Document)to).getDocumentElement();
	    }
	    */
	    if (from.getNodeType()!=ELEMENT_NODE||(to.getNodeType()!=Node.ELEMENT_NODE&&to.getNodeType()!=Node.DOCUMENT_NODE))
	    {
	        throw new Exception("Cannot replace other that DOCUMENT or ELEMENT!");
	        //return;
	    }
	    if (from.getNodeType()==Node.DOCUMENT_NODE)
	    {
	        from=((Document)from).getDocumentElement();
	    }
	    Document doc = (Document)(to instanceof Document?to:to.getOwnerDocument());
	    Element fromCopy = (Element)doc.importNode(from,true);
	    removeAllChildren(to);
	    to.appendChild(fromCopy);
	    //Element toE = (Element)to;
	}
	static void removeAllChildren(Node e)
	{
	    while (e.getFirstChild()!=null)
	    {
	        e.removeChild(e.getFirstChild());
	    }
	}

	protected boolean hasBinding()
	{
	    return (this.getAttributeNode("ref")!=null);
	}
	/**
	 * returns a list of all nodes that should be created as objects and signed
	 * @return
	 */
	protected Vector getSignedNodes()
	{
        Vector signedNodes=new Vector();
	    
	    if (this.hasBinding())
	    {
	        signedNodes.addElement(this.getRefNode());
	    }
	    else
	    {
	        // get all instances in all models and add the document node
	        Vector models=this.getModels();
	        Enumeration modelEnum = models.elements();
	        while(modelEnum.hasMoreElements())
	        {
		        XFormsModelElement m = (XFormsModelElement)modelEnum.nextElement();
		        Enumeration e = m.getInstanceDocuments();
		        while (e.hasMoreElements())
		        {
		        	Document doc = (Document)e.nextElement();
		        	signedNodes.addElement(doc);
		        	Log.debug("Added document: "+doc+" to signed documents.");
		        }
	        }
	    }
	    return signedNodes;
	}

    synchronized void sign(short type) throws Exception
    {
        XMLDocument xmldoc = this.signatureMLFC.getXMLDocument();
        URL baseURL = xmldoc.getXMLURL();
        Vector URLs=new Vector();
        URLs.addElement(baseURL);
        Vector signedNodes=this.getSignedNodes();
        // add all references to stylesheets, scripts etc.
        ResourceReferencer resList =this.signatureMLFC.getResourceReferencer(); 
        Enumeration e = resList.getResources();
        while (e.hasMoreElements())
        {
            URLs.addElement((((Resource)e.nextElement())).getURL());
        }
        SigningKey key=this.signingKey;
        Document targetDoc=null;
	    Element signature = SignatureCreator.sign(type,signedNodes,URLs,baseURL,key,targetDoc);
	    this.replaceInstanceNode(signature,this.getToNode());
	    this.dialog.writeToInstance(signature);
	    this.dispatchEvent(EventFactory.createEvent("signature-created",true,false));
	    this.showTree(signature);
    }
    void verify() throws Exception
    {
        XMLDocument xmldoc = this.signatureMLFC.getXMLDocument();
        URL baseURL = xmldoc.getXMLURL();
        Node node=this.getToNode();
        InputStream is = Resolver.createStream(node);
        if (vStream!=null) vStream.close();
        vStream = new VerifyStream(this.signatureMLFC.getMLFCListener().getComponentFactory());
        PrintStream p = new PrintStream(vStream);
        VerifySignature.verify(is,baseURL,p);
    }
    
    public ComponentFactory getComponentFactory()
    {
        return this.signatureMLFC.getMLFCListener().getComponentFactory();
    }
    
    /* a print stream that opens a new Frame */
    public class VerifyStream extends OutputStream
    {
        JFrame vFrame=new JFrame();
        XTextArea text;
        
        	public VerifyStream(ComponentFactory f)
        	{
        	    text=f.getXTextArea("Verifying Signature\n");
        	    vFrame.getContentPane().add(text.getComponent());
        	    vFrame.setSize(700,200);
        	    vFrame.show();
        	}
        	public void 	write(byte[] b)
        	{
            //Writes b.length bytes from the specified byte array to this output stream.
        	    String t = b.toString();
        	    text.setText(text.getText()+t);
        	}
        	public  void 	write(int b)
        	{
        	    char ch=(char)b; // VERY UNEFFICIENT!
        	    text.setText(text.getText()+ch);
            //Writes the specified byte to this output stream.
        	}
        	public void close()
        	{
        	    vFrame.dispose();
        	    vFrame=null;
        	}
    }

}




