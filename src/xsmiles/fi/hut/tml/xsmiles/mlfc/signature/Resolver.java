
/*
 * X-Smiles license
*/
package fi.hut.tml.xsmiles.mlfc.signature;

//import org.apache.xml.security.samples.signature.*;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ModelElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.smil20.*;

import java.util.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import org.apache.xml.utils.URI;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.apache.xml.security.utils.Base64;

/**
 *
 * @author Mikko Honkala
 */
public class Resolver extends ResourceResolverSpi {

    Document instanceDoc;
    URL baseURL;
    PrintStream debug;
    static {
    org.apache.xml.security.Init.init();
    }
    public Resolver(Document instance, URL base, PrintStream out)
    {
        super();
        debug=out;
        if (debug==null) debug=System.out;
        instanceDoc=instance;
        baseURL=base;
    }
   /** {@link org.apache.log4j} logging facility */
   //static org.apache.log4j.Category cat =
   //   org.apache.log4j.Category.getInstance(resolver.class.getName());

   /**
    * Method engineResolve
    *
    * @param uri
    * @param BaseURI
    * @return
    * @throws ResourceResolverException
    */
   public XMLSignatureInput engineResolve(Attr uri, String BaseURI)
           throws ResourceResolverException {

       String URI = uri.getNodeValue();
      try {
		 

         if (URI.equals("")) {
            XMLSignatureInput result = new XMLSignatureInput(this.instanceDoc);
            // InputStream streamToDoc = this.createStream(this.instanceDoc);
           // XMLSignatureInput result = new XMLSignatureInput(streamToDoc);

            // XMLSignatureInput result = new XMLSignatureInput(inputStream);
            result.setSourceURI(BaseURI);
            result.setMIMEType("application/xml");

            return result;
         } else {
             URL u = new URL(URI);
             XMLSignatureInput result = new XMLSignatureInput(u.openConnection().getInputStream());

             // XMLSignatureInput result = new XMLSignatureInput(inputStream);
             result.setSourceURI(BaseURI);
             result.setMIMEType("application/xml");
             return result;
             /*
            Object exArgs[] = {
               "The URI " + URI + " is not configured for signing" };

            throw new ResourceResolverException("generic.EmptyMessage", exArgs,
                                                uri, BaseURI);*/
         }
      } catch (Exception ex) {
         throw new ResourceResolverException("generic.EmptyMessage", ex, uri,
                                             BaseURI);
      }
   }
   
   public static InputStream createStream(Node n) throws Exception
   {
       StringWriter sw = new StringWriter();
		fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(
				sw,
				n,true,false,true);
		String res=sw.toString();
		//res=res.substring(0,res.length()-2);
		Log.debug(res);
		sw.flush();
		sw.close();
		ByteArrayInputStream is=new ByteArrayInputStream(res.getBytes());
		return is;
   }

   /**
    * We resolve http URIs <I>without</I> fragment...
    *
    * @param uri
    * @param BaseURI
    * @return
    */
   public boolean engineCanResolve(Attr uri, String BaseURI) {

      String uriNodeValue = uri.getNodeValue();

      if (uriNodeValue.equals("") || uriNodeValue.startsWith("#")) {
         return false;
      }

      try {
         URI uriNew = new URI(new URI(BaseURI), uri.getNodeValue());

         if (uriNew.getScheme().equals("http")||uriNew.getScheme().equals("file")) {
            debug.println("Resolving resource for signing/verifying " + uriNew.toString());

            return true;
         }
         debug.println("Could not resolve resource for signing/verifying " + uriNew.toString());

      } catch (URI.MalformedURIException ex) {}

      return false;
   }

   /** Field _uriMap */
   Map _uriMap = null;

   /** Field _mimeMap */
   Map _mimeMap = null;

   /**
    * Method register
    *
    * @param URI
    * @param filename
    * @param MIME
    */
   void register(String URI, String filename, String MIME) {
      _uriMap.put(URI, filename);
      _mimeMap.put(URI, MIME);
   }

}
