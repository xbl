/*
 * Copyright 1999-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  
 */
package fi.hut.tml.xsmiles.mlfc.signature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.security.PublicKey;
import java.security.cert.X509Certificate;

import org.apache.xml.security.keys.KeyInfo;
//import org.apache.xml.security.samples.utils.resolver.OfflineResolver;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author $Author: honkkis $
 *  
 */
public class VerifySignature
{

    /**
     * Method main
     * 
     * @param unused
     */
    public static void main(String unused[])
    {

        boolean schemaValidate = false;
        final String signatureSchemaFile = "data/xmldsig-core-schema.xsd";
        // String signatureFileName =
        // "data/ie/baltimore/merlin-examples/merlin-xmldsig-fifteen/signature-enveloping-rsa.xml";
        String signatureFileName = "signature.xml";
        if (unused.length == 1)
        {
            signatureFileName = unused[0];
        }
        try
        {

            // File f = new File("signature.xml");
            File f = new File(signatureFileName);

            System.out.println("Try to verify " + f.toURL().toString());

            InputStream is = new java.io.FileInputStream(f);
            verify(is, f.toURL(), null);

        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static Element findSigElement(Node doc)
    {
        if (doc.getNodeType() == Node.DOCUMENT_NODE)
        {
            //return findSigElement(((Document)doc).getDocumentElement());
            Document el = (Document) doc;
            return (Element) el.getElementsByTagNameNS(
                    Constants.SignatureSpecNS, "Signature").item(0);
        } else if (doc.getNodeType() == Node.ELEMENT_NODE)
        {
            Element el = (Element) doc;
            return (Element) el.getElementsByTagNameNS(
                    Constants.SignatureSpecNS, "Signature").item(0);
        } else
            return null;
    }
    public  static boolean verify(Element sigElement, URL baseURL, PrintStream debug) throws Exception
    {
        // serialize to memory and call verify (inputStream)
        InputStream is = Resolver.createStream(sigElement);
        return verify(is,baseURL,debug);
    }

    public static boolean verify(InputStream is, URL baseURL, PrintStream debug)
    {
        try
        {
            if (debug == null) debug = System.out;
            javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory
                    .newInstance();

            dbf.setNamespaceAware(true);
            dbf.setAttribute("http://xml.org/sax/features/namespaces",
                    Boolean.TRUE);

            javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
            db
                    .setErrorHandler(new org.apache.xml.security.utils.IgnoreAllErrorHandler());

            org.w3c.dom.Document doc = db.parse(is);
            Element nscontext = XMLUtils.createDSctx(doc, "ds",
                    Constants.SignatureSpecNS);
            //         Element sigElement = (Element) XPathAPI.selectSingleNode(doc,
            //                                 "//ds:Signature[1]", nscontext);
            Element sigElement = findSigElement(doc);
            if (sigElement == null)
            {
                debug.println("No signature found, exiting!");
                return false;
            }
            XMLSignature signature = new XMLSignature(sigElement, baseURL
                    .toString());

            signature.addResourceResolver(new Resolver(null, baseURL, debug));

            // XMLUtils.outputDOMc14nWithComments(signature.getElement(),
            // System.out);
            KeyInfo ki = signature.getKeyInfo();

            if (ki != null)
            {
                if (ki.containsX509Data())
                {
                    debug
                            .println("Could find a X509Data element in the KeyInfo");
                }

                X509Certificate cert = signature.getKeyInfo()
                        .getX509Certificate();

                if (cert != null)
                {
                    /*
                     * System.out.println( "I try to verify the signature using
                     * the X509 Certificate: " + cert);
                     */
                    boolean valid = signature.checkSignatureValue(cert) ;
                    debug
                            .println("The XML signature in file "
                                    + baseURL.toString()
                                    + " is "
                                    + (valid ? "valid (good)"
                                            : "invalid !!!!! (bad)"));
                    return valid;
                } else
                {
                    debug.println("Did not find a Certificate");

                    PublicKey pk = signature.getKeyInfo().getPublicKey();

                    if (pk != null)
                    {
                        /*
                         * System.out.println( "I try to verify the signature
                         * using the public key: " + pk);
                         */
                        boolean valid = signature.checkSignatureValue(pk);
                        debug
                                .println("The XML signature in file "
                                        + baseURL.toString()
                                        + " is "
                                        + (valid ? "valid (good)"
                                                : "invalid !!!!! (bad)"));
                        return valid;
                    } else
                    {
                        debug
                                .println("Did not find a public key, so I can't check the signature");
                    }
                }
            } else
            {
                debug.println("Did not find a KeyInfo");
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    static
    {
        org.apache.xml.security.Init.init();
    }
}