/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 11, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.signature.swing;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/** 
 * the Swing dependent tree implementation
 * 
 * @author honkkis
 *
 */
public class SignatureTree extends JTree
{
    protected TreeNode rootNode;
    public SignatureTree(Object root)
    {
        super((TreeNode)root);
        rootNode=(TreeNode)root;
    }
    
    public void expand()
    {
        this.scrollPathToVisible(new TreePath(
                ((DefaultMutableTreeNode)rootNode.getChildAt(0)).getPath()));
    }
    
    public static Object createRootNode()
    {
        TreeNode root = new DefaultMutableTreeNode();
        return root;
        
    }
    public String  convertValueToText (Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        if (value instanceof DefaultMutableTreeNode)
	    {
	        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
	        Object userObject = node.getUserObject();
	        if (userObject!=null && userObject instanceof String)
	        {
	            return (String)userObject;
	        }
	        else if (userObject != null && userObject instanceof Node)
		    {
		        Node xnode = (Node)userObject;
		        String text;
		        switch (xnode.getNodeType())
			    {
			    case Node.DOCUMENT_NODE:
			        text = xnode.getNodeValue();
			        break;
    			    
			    case Node.ELEMENT_NODE:
			        text = xnode.getNodeName();
//			        if (xnode.hasChildNodes())
//				    {
//				        Node firstNode = xnode.getFirstChild();
//				        if (firstNode.getNodeType() == Node.TEXT_NODE &&
//					    firstNode.getNextSibling() == null &&
//					    firstNode.getNodeValue().length() > 0)
//					    text += " -- " + firstNode.getNodeValue();
//				    }
			        break;
    			    
			    case Node.ATTRIBUTE_NODE:
			        text= "Attr: " + xnode.getNodeName()+" = \""+xnode.getNodeValue()+"\"";
			        break;
    			    
			    case Node.PROCESSING_INSTRUCTION_NODE:
					text="<?"+xnode.getNodeName()+" "+xnode.getNodeValue()+"?>";
					break;
			    case Node.COMMENT_NODE:
			    case Node.TEXT_NODE: 
			    default:
			        text = xnode.getNodeValue();
			        break;
			    }
		        return text;
		    }
	    }
        //return this.convertValueToText(value, selected, expanded, leaf, row, hasFocus);
          return new String("Signature");   
    }

    public Object createNode(Node n, boolean addAttributes,boolean recursive,boolean addNSAttributes)
    {
        MutableTreeNode tnode =
		    new DefaultMutableTreeNode(n, true);
        if (addAttributes&&n.getNodeType()==Node.ELEMENT_NODE)
        {
            Element el = (Element)n;
            NamedNodeMap map = el.getAttributes();
            for (int i =0;i<map.getLength();i++)
            {
                Attr attr = (Attr)map.item(i);
                if ((addNSAttributes)||(!attr.getName().startsWith("xmlns")))
                {
	                MutableTreeNode anode =
	        		    new DefaultMutableTreeNode(attr, true);
	                this.addNode(tnode,anode);
                }
            }
        }
        if (recursive)
        {
            Node child = n.getFirstChild();
            while (child!=null)
            {
                if (child.getNodeType()==Node.ELEMENT_NODE )
                {
                    Object treeChildNode = createNode(child,addAttributes,recursive,addNSAttributes);
                    this.addNode(tnode,treeChildNode);
                }
                else if (child.getNodeType()==Node.TEXT_NODE)
                {
                    if (child.getNodeValue().trim().length()>0)
                    {
	                    Object treeChildNode = createNode(child,addAttributes,recursive,addNSAttributes);
	                    this.addNode(tnode,treeChildNode);
                    }
                }
                child=child.getNextSibling();
            }
        }
        return tnode;
    }

    public Object createNode(String s)
    {
        MutableTreeNode tnode =
		    new DefaultMutableTreeNode(s, true);
        return tnode;
    }

    
    public void addNode(Object parent, Object newNode)
    {
        if (parent==null) parent=this.rootNode;
        MutableTreeNode parentTreeNode = (MutableTreeNode)parent;
        MutableTreeNode newTreeNode = (MutableTreeNode)newNode;
        int location=parentTreeNode!=null?parentTreeNode.getChildCount():0;
        parentTreeNode.insert(newTreeNode, location);
    }
    
}
