/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.signature.swing;

import java.awt.Component;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.signature.SignatureMLFC;



/**
 * @author honkkis
 *
 */
public class SignatureVisualizer
{
    protected SignatureTree sigTree;
    protected JScrollPane container;
    protected Element sigElement;

    public SignatureVisualizer()
    {
    }
    public void visualize()
    {
        this.createTree();
        this.container=new JScrollPane(this.sigTree);
    }
    
    protected void createTree()
    {
        if (this.sigElement==null)
        {
            Log.error("SignatureElement not specified");
            return;
        }
        // go thru the tree
        // create root tree node 
        this.sigTree = new SignatureTree(SignatureTree.createRootNode());
        // while more reference elements
        int whatToShow=NodeFilter.SHOW_ALL;
        NodeFilter filter=null;
        boolean entities=true;
        NodeIterator iterator = ((DocumentTraversal)this.sigElement.getOwnerDocument()).createNodeIterator(this.sigElement,whatToShow,filter,entities);
        //while (iterator.)
        Node child = iterator.nextNode();
        while(child!=null)
        {
            if (child instanceof Element)
            {
                Element childElem = (Element)child;
                if (childElem.getNamespaceURI()==SignatureMLFC.XMLSIG_ns)
                {
                    if (childElem.getLocalName().equals("Reference"))
                    {
                        String nodeText=childElem.getNodeName();
                        Attr uriAttr = childElem.getAttributeNode("URI");
                        if (uriAttr!=null) nodeText+=" "+uriAttr.getNodeName()+"=\""+uriAttr.getNodeValue()+"\"";
                        Object treeNode=this.sigTree.createNode(nodeText);
                        this.sigTree.addNode(null,treeNode);
                        // add objects referenced in reference URI="#" directly as children for readability
                        if (uriAttr!=null)
                        {
                            String ref=uriAttr.getNodeValue();
                            if (ref.startsWith("#"))
                            {
                                Object objectTreeNode=null;
                                ref=ref.substring(1);
                                // find object
                                Element object=this.findObject(ref);
                                if (object==null)
                                    objectTreeNode=this.sigTree.createNode("ERROR: Object #"+ref+" not found! Signature creation has failed!");
                                else
                                    objectTreeNode=this.sigTree.createNode(object,true,true,false);
                                this.sigTree.addNode(treeNode,objectTreeNode);
                            }
                        }
                    }
                }
            }
            child=iterator.nextNode();
        }
        this.sigTree.expand();
        // create a tree node for reference
        // if URL, set the text to tree node
        // if #id, find the object node
        // create tree node for the object
        // create tree nodes for the objects contents, along with all text
        
    }

    Element findObject(String id)
    {
        NodeList list = this.sigElement.getElementsByTagNameNS(SignatureMLFC.XMLSIG_ns,"Object");
        for (int i=0;i<list.getLength();i++)
        {
            Element e = (Element)list.item(i);
            Attr attr=e.getAttributeNode("Id");
            if (attr!=null)
            {
                if (attr.getNodeValue().equals(id)) return e;
            }
        }
        return null;
    }
    
    
    
    protected MutableTreeNode createNode(Node n,boolean recursive)
    {
        // TODO:
        return null;
    }
    
    public void setSignature(Element e)
    {
        sigElement=e;
    }
    
    public Component getComponent()
    {
        return this.container;
    }
}
