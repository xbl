/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 2, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.signature;

import java.net.URL;


/**
 * @author honkkis
 *
 */
public class SigningKey
{
    public String keyAlias;
    public String keyPass;
    public String certAlias;
    public String storePass;
    public URL storeURL;

}
