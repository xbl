/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.signature;

import java.net.URL;
import java.awt.Dimension;
import java.awt.Container;

import fi.hut.tml.xsmiles.Browser; // for version number
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.XMLDocument;
import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.Log;

/**
 * SignatureMLFC is the XML Signature main function. It creates the elements
 * and controls them. Namespace: http://www.xsmiles.org/2002/signature
 *
 * @author hguo
 */
public class SignatureMLFC extends MLFC {
 
    DocumentImpl xsmilesDoc = null;
    public static final String namespace = "http://www.xsmiles.org/2002/signature";     
    public static final String XMLSIG_ns="http://www.w3.org/2000/09/xmldsig#";

	public SignatureMLFC(){
	}

	/**
	 * Get the version of the MLFC. This version number is updated 
	 * with the browser version number at compilation time. This version number
	 * indicates the browser version this MLFC was compiled with and should be run with.
	 * @return 	MLFC version number.
	 */
	public final String getVersion() {
		return Browser.version;
	}

	/**
	 * Create a DOM element.
	 */
	public Element createElementNS(DocumentImpl doc, String ns, String tag)
	{
		xsmilesDoc = doc;
		Element element = null;

		if (tag == null)
		{
			Log.error("Invalid null tag name!");
			return null;
		}
		String localname = getLocalname(tag);
		if (localname.equals("sign"))
		{
			element = new SignatureElementImpl(doc, this, ns, tag);
		}

		// Other tags go back to the XSmilesDocument as null...
		if (element == null)
			Log.debug("XML Signature didn't understand element: "+tag);

		return element;
	}

    public void start(){
		Log.debug("SignatureMLFC start.");
	}

	public void stop(){
		Log.debug("SignatureMLFC end.");
	}

}



