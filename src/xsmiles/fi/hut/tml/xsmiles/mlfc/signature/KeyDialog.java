/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 2, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.signature;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XCompound;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
import fi.hut.tml.xsmiles.util.URLUtil;


/**
 * @author honkkis
 *
 */
public class KeyDialog implements ActionListener
{
    protected JFrame frame;
    protected Container mainPanel;
    protected XButton fileButton;
	protected XInput keyPassInput, keyAliasInput, storePassInput,certAliasInput, URLInput;
	//protected Document instancedata, xmldata;
	protected URL keystoreURL = null;
	protected ComponentFactory cfactory;
	
	protected Thread waitThread = null;
	protected SigningKey signingKey;
	
	protected ActionListener actionListener;
	
	protected static final String FILEBUTTON_COMMAND="KEY_FILEBUTTON";
	protected static final String OK_COMMAND="KEY_OK";
	protected static final String CANCEL_COMMAND="KEY_CANCEL";
	
	public SigningKey showDialog()
	{
	    if (frame==null)
	    {
	        this.createFrame();
	        this.frame.setSize(400,300);
	        this.frame.show();
	    }
	    return null;
	}
	
	public void setKey(SigningKey key)
	{
	    this.signingKey=key;
	}
	
	public void setActionListener(ActionListener l)
	{
	    this.actionListener=l;
	}
	
	public void disposeDialog()
	{
	    if (this.frame!=null)
	    {
		    this.frame.dispose();
		    this.frame=null;
	    }
	}

	
	
	
	protected ComponentFactory getComponentFactory()
	{
	    if (cfactory==null)
	    {
	        Log.error("ComponentFactory was null for SignDialog... create a default componentfactory");
	        cfactory=new AWTComponentFactory();
	    }
	    return cfactory;
	}
	
	protected void setComponentFactory(ComponentFactory f)
	{
	    this.cfactory=f;
	}
	
	Container createContainer(LayoutManager layout)
	{
	    return new JPanel(layout);
	}
	
	public XCaption getCaption(String s)
	{
	    XCaption c = this.getComponentFactory().getXCaption(s);
	    // TODO: set size
	    return c;
	}
	
	void createFrame()
	{
	    String cs = "left";
	    frame=new JFrame("Sign a form");
	    mainPanel=createContainer(new GridLayout(0,1));
	    XCompound compound;
	    
	    this.storePassInput=this.getComponentFactory().getXInput();
	    compound=this.getComponentFactory().getXLabelCompound(
	            storePassInput,this.getCaption("KeyStore password:"),cs);
	    mainPanel.add(compound.getComponent());


	    this.certAliasInput=this.getComponentFactory().getXInput();
	    compound=this.getComponentFactory().getXLabelCompound(
	            certAliasInput,this.getCaption("Certificate alias:"),cs);
	    mainPanel.add(compound.getComponent());

	    keyPassInput=this.getComponentFactory().getXInput();
	    compound=this.getComponentFactory().getXLabelCompound(
	            keyPassInput,this.getCaption("Key password:"),cs);
	    mainPanel.add(compound.getComponent());

	    keyAliasInput=this.getComponentFactory().getXInput();
	    compound=this.getComponentFactory().getXLabelCompound(
	            keyAliasInput,this.getCaption("Key alias:"),cs);
	    mainPanel.add(compound.getComponent());


	    this.URLInput=this.getComponentFactory().getXInput();
	    compound=this.getComponentFactory().getXLabelCompound(
	            URLInput,this.getCaption("KeyStore URL"),cs);
	    mainPanel.add(compound.getComponent());
	    
	    this.setDefaults();
	    
	    this.fileButton=this.getComponentFactory().getXButton("Select file",null);
	    this.fileButton.setActionCommand(FILEBUTTON_COMMAND);
	    this.fileButton.addActionListener(this);
	    mainPanel.add(this.fileButton.getComponent());
	    
	    Container buttons = this.createContainer(new FlowLayout());
	    XButton ok, cancel;
	    ok=this.getComponentFactory().getXButton("OK",null);
	    ok.setActionCommand(OK_COMMAND);
	    ok.addActionListener(this);
	    buttons.add(ok.getComponent());

	    cancel=this.getComponentFactory().getXButton("Cancel",null);
	    cancel.setActionCommand(CANCEL_COMMAND);
	    cancel.addActionListener(this);
	    buttons.add(cancel.getComponent());

	    mainPanel.add(buttons);
	    

        addToFrame(mainPanel);
        
	}
	
	void setDefaults()
	{
	    if (this.signingKey!=null)
	    {
	        this.certAliasInput.setText(this.signingKey.certAlias);
	        this.keyAliasInput.setText(this.signingKey.keyAlias);
	        this.keyPassInput.setText(this.signingKey.keyPass);
	        this.storePassInput.setText(this.signingKey.storePass);
	        if (this.signingKey.storeURL!=null)
	            this.URLInput.setText(this.signingKey.storeURL.toString());
	    }
	}
	
	public SigningKey getSigningKey()
	{
	    return this.signingKey;
	}
	void addToFrame(Component c)
	{
	    frame.getContentPane().add(c);
	}
	protected static File lastFile=new File(".");
    public void actionPerformed(ActionEvent e) {
		try
		{
	        if (e.getActionCommand().equals(CANCEL_COMMAND)) {					
	            if (this.actionListener!=null )
	                this.actionListener.actionPerformed(e);
	            this.disposeDialog();
	        }
	        if (e.getActionCommand().equals(OK_COMMAND)) {
	            if (this.keystoreURL!=null)
	            {
					this.signingKey=new SigningKey();
					this.signingKey.certAlias=this.certAliasInput.getText();
					this.signingKey.keyAlias=this.keyAliasInput.getText();
					this.signingKey.keyPass=this.keyPassInput.getText();
					this.signingKey.storePass=this.storePassInput.getText();
					this.signingKey.storeURL=this.keystoreURL;
	            }
	            if (this.actionListener!=null )
	                this.actionListener.actionPerformed(e);
	            this.disposeDialog();
	        }
	        else if (e.getActionCommand().equals(FILEBUTTON_COMMAND)) {
	            // open up a file dialog and change the URL field
	            XFileDialog dialog = this.getComponentFactory().getXFileDialog(false,lastFile.getAbsolutePath());
	            int res = dialog.select();
	            if (res==dialog.SELECTION_OK)
	            {
	                File f = dialog.getSelectedFile();
	                lastFile =f;
	                URL u = URLUtil.fileToURL(f);
	                this.keystoreURL=u;
	                this.URLInput.setText(this.keystoreURL.toString());
	            }
	        }
		} catch (Exception ex)
		{
		    Log.error(ex);
		}
    }

}
