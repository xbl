/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 26, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.signature;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
import fi.hut.tml.xsmiles.mlfc.signature.swing.SignatureVisualizer;


/**
 * @author honkkis
 *
 */
public class SignDialog implements ActionListener
{
    protected JFrame frame;
    protected Container mainPanel, centerPanel, buttonPanel;
    protected XButton signButton, cancelButton, verifyButton, selectKeyButton,showTreeButton;
	protected XTextArea instLog,uiLog;
	protected File keystoreFile = null;
	protected ComponentFactory cfactory;
	protected short signatureType;
	
	protected ActionListener actionListener;
	
	public void showDialog(short sigType)
	{
	    if (frame==null)
	    {
	        this.signatureType=sigType;
	        this.createFrame();
	        this.frame.setSize(800,600);
	        this.frame.show();
	    }
	}
	
	public void setActionListener(ActionListener l)
	{
	    this.actionListener=l;
	}
	
	public void disposeDialog()
	{

	    if (frame!=null)
	    {
	        frame.dispose();
	        frame=null;
	    }
	}
	
	protected ComponentFactory getComponentFactory()
	{
	    if (cfactory==null)
	    {
	        Log.error("ComponentFactory was null for SignDialog... create a default componentfactory");
	        cfactory=new AWTComponentFactory();
	    }
	    return cfactory;
	}
	
	protected void setComponentFactory(ComponentFactory f)
	{
	    this.cfactory=f;
	}
	
	Container createContainer(LayoutManager layout)
	{
	    return new JPanel(layout);
	}
	void createFrame()
	{
	    frame=new JFrame("Sign a form");
	    mainPanel=createContainer(new BorderLayout());
		centerPanel=createContainer(new GridLayout(1,2));
	    buttonPanel = createContainer(new FlowLayout());

	    // the textarea for the instance data
	    instLog = this.getComponentFactory().getXTextArea("the instance data");//30,40);
        instLog.setEditable(false);
        
        // the textarea for UI data
	    uiLog = this.getComponentFactory().getXTextArea("the ui source");//30,40);
        uiLog.setEditable(false);
        
        // the label for the frame
        XCaption fcaption = this.getComponentFactory().getXCaption("The instance data is on the left, and the UI source on the right.");
        
        signButton = this.getComponentFactory().getXButton("Sign",null);
        signButton.addActionListener(this);
        signButton.setActionCommand("Sign");
        cancelButton = this.getComponentFactory().getXButton("Close",null);
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("Cancel");

        verifyButton = this.getComponentFactory().getXButton("Verify",null);
        verifyButton.addActionListener(this);
        verifyButton.setActionCommand("Verify");

        selectKeyButton = this.getComponentFactory().getXButton("Select Key",null);
        selectKeyButton.addActionListener(this);
        selectKeyButton.setActionCommand("Select Key");

        /*showTreeButton = this.getComponentFactory().getXButton("Show signature Tree",null);
        showTreeButton.addActionListener(this);
        showTreeButton.setActionCommand("Show Tree");*/

        //buttonPanel.add(showTreeButton.getComponent());
        buttonPanel.add(signButton.getComponent());
        buttonPanel.add(cancelButton.getComponent());
        buttonPanel.add(verifyButton.getComponent());
        buttonPanel.add(selectKeyButton.getComponent());
        
        mainPanel.add(fcaption.getComponent(),BorderLayout.NORTH);
        mainPanel.add(buttonPanel,BorderLayout.SOUTH);
        
        mainPanel.add(centerPanel,BorderLayout.CENTER);
        centerPanel.add(instLog.getComponent());
        centerPanel.add(uiLog.getComponent());
        
        addToFrame(mainPanel);
        
	}
	
	void addToFrame(Component c)
	{
	    frame.getContentPane().add(c);
	}
	
    public void actionPerformed(ActionEvent e) {
		try
		{
        if (e.getActionCommand().equals("Sign")) {					
            this.actionListener.actionPerformed(e);
        }
        else if (e.getActionCommand().equals("Verify")) {
            this.actionListener.actionPerformed(e);
        }
        else if (e.getActionCommand().equals("Select Key")) {
            this.actionListener.actionPerformed(e);
        }
        else if (e.getActionCommand().equals("Show Tree")) {
            this.actionListener.actionPerformed(e);
        }
        else this.disposeDialog();
		} catch (Exception ex)
		{
		    Log.error(ex);
		}
    }
    
    //protected Si
    public void showTree(Element signatureElem)
    {
        if (signatureElem!=null)
        {
            SignatureVisualizer vis = new SignatureVisualizer();
            vis.setSignature(signatureElem);
            vis.visualize();
	        Container parent=this.uiLog.getComponent().getParent();
	        try
	        {
	            parent.remove(this.uiLog.getComponent());
	        } catch (Exception e)
	        {
	            
	        }
	        parent.add(vis.getComponent());
	        parent.validate();
	        
        }        
    }
    
    public void writeToInstance(Node n)
    {
        this.writeToTextArea(n,instLog);
    }
    public void writeToUI(Node n)
    {
        this.writeToTextArea(n,uiLog);
    }
    protected void writeToTextArea(Node n, XTextArea log) 
    {
        try
        {
        
	        StringWriter w = new StringWriter();
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(
					w,
					n,true,true,false);
			log.setText(w.toString());
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    
	public static void debugNode(Node n)
    {
		try
		{
			fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(
				new java.io.OutputStreamWriter(System.out),
				n,true,true,false);
		} catch (Exception e)
		{
			Log.error(e);
		}
    }
}
