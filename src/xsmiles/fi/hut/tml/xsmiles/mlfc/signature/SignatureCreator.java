/*
 * /* X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources. Created on May 26, 2004
 *  
 */
package fi.hut.tml.xsmiles.mlfc.signature;

import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Resources;

/**
 * @author honkkis
 *  
 */
public class SignatureCreator
{

    
    /*      sign the UI doc
     * Note that this way the UI doc is refetched from the URL
     * This might be wrong, since the document might be generated just for this user
     * The right way would be to read it from the cache.
     * But usually the applications wants the signed content to be identified and accessible
     * via an URL, because otherwise it would be hard to validate the document again
     * Note that any transformations, such as XSLT should be run before signing
     */
    
    static void addURLs(XMLSignature sig, Vector URLs ) throws XMLSignatureException
    {
        // ADD EXTERNAL REFERENCES
        URL url = null;
        for (Enumeration e=URLs.elements();e.hasMoreElements();)
        {
            url=(URL)e.nextElement();
            if (url!=null) sig.addDocument(url.toString()); 
        }
        
    }
    static void addNodes(XMLSignature sig, Vector signedNodes, Document targetDoc)
    	throws XMLSignatureException, TransformationException
    {
	    int i=0;
	    for (Enumeration e=signedNodes.elements();e.hasMoreElements();)
	    {
	        Node doc=(Node)e.nextElement();
	        if (doc.getNodeType()==Node.DOCUMENT_NODE)
	        {
	            doc=((Document)doc).getDocumentElement();
	        }
	        String Id="referenced_object"+(i++);
	        ObjectContainer obj = new ObjectContainer(targetDoc);	
	        Node imported = targetDoc.importNode(doc,true);
	        obj.appendChild(imported);
	        obj.setId(Id);
	        sig.appendObject(obj);
	
	        Transforms transforms = new Transforms(targetDoc);
	        transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);
	        sig.addDocument("#" + Id, transforms, Constants.ALGO_ID_DIGEST_SHA1);        
	     }
    }
    
    public static SigningKey getKey(SigningKey key)
    {
        try
        {
	        if (key==null)
	        {
	            key = new SigningKey();
	            key.storeURL = Resources.getResourceURL("xsmiles.default.key");
	            key.certAlias=Resources.getResourceString("xsmiles.default.certificate.alias");
	            key.keyAlias=Resources.getResourceString("xsmiles.default.key.alias");
	            key.keyPass=Resources.getResourceString("xsmiles.default.key.pass");
	            key.storePass=Resources.getResourceString("xsmiles.default.keystore.pass");
	        }
        } catch (Exception e)
        {
            Log.error(e);
        }
        return key;
    }
    /**
     * Create an enveloping signature with all of the documents as an object and all the URLs as 
     * external references. 
     */
    public static Element signEnveloping(Vector signedNodes, Vector URLs, URL baseURL, SigningKey key, Document targetDoc) throws Exception
    {
        if (targetDoc==null)
        {
            //targetDoc=(Document)signedDocuments.elementAt(0);
            targetDoc=createEmptyDoc(true);
        }
        KeyStore ks;
        try
        {
            key=getKey(key);
            ks = getKeyStore(key);
        } catch (Exception e)
        {
            Log.error(e, "Could not open keystore " + key);
            return null;
        }
        PrivateKey privateKey = (PrivateKey) ks.getKey(key.keyAlias,
                key.keyPass.toCharArray());

        
        XMLSignature sig = new XMLSignature(targetDoc, baseURL!=null?baseURL.toString():"",
                XMLSignature.ALGO_ID_SIGNATURE_DSA);
        targetDoc.appendChild(sig.getElement());
        Resolver res = new Resolver(null,  baseURL,null);
        // create the object elements and copy the data inside
        addNodes(sig,signedNodes,targetDoc);
        addURLs(sig,URLs);
        { // the key info
            X509Certificate cert =
               (X509Certificate) ks.getCertificate(key.certAlias);

            sig.addKeyInfo(cert);
            sig.addKeyInfo(cert.getPublicKey());
            Log.debug("Start signing");
            sig.sign(privateKey);
            Log.debug("Finished signing");
         }
        return sig.getElement();
    }
    public static Document createEmptyDoc(boolean ns) throws Exception
    {
        javax.xml.parsers.DocumentBuilderFactory dbf =
            javax.xml.parsers.DocumentBuilderFactory.newInstance();

         dbf.setNamespaceAware(ns);

         javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
         return db.newDocument();
    }

    public static Element sign(short type,Vector signedNodes, Vector URLs, 
            URL baseURL, SigningKey key,Document targetDoc) throws Exception
    {
        if (type==SignatureElementImpl.SIGNATURE_ENVELOPED)
            return signEnveloped(signedNodes,URLs,baseURL,key,targetDoc);
        else 
            return signEnveloping(signedNodes,URLs,baseURL,key,targetDoc);
    }
    public static Element signEnveloped(Vector signedNodes, Vector URLs, 
            URL baseURL, SigningKey key,Document targetDoc) throws Exception
    {
        if (targetDoc==null)
        {
            //targetDoc=(Document)signedDocuments.elementAt(0);
            targetDoc=createEmptyDoc(true);
        }
        // currently add only the first node
        Node firstNode=(Node)signedNodes.elementAt(0);
        Element r = (Element)(firstNode instanceof Document?((Document)firstNode).getDocumentElement():firstNode);
        Node imported = targetDoc.importNode(r,true);
        targetDoc.appendChild(imported);
        //Document instancedata=instancenode.getOwnerDocument();
        KeyStore ks;
        try
        {
            key=getKey(key);
            ks = getKeyStore(key);
        } catch (Exception e)
        {
            Log.error(e, "Could not open keystore " + key);
            return null;
        }
        PrivateKey privateKey = (PrivateKey) ks.getKey(key.keyAlias,
                key.keyPass.toCharArray());

        Element root = targetDoc.getDocumentElement();
        
        XMLSignature sig = new XMLSignature(targetDoc, baseURL!=null?baseURL.toString():"",
                XMLSignature.ALGO_ID_SIGNATURE_DSA);
        // SIGNATURE ELEMENT MUST BE ADDED BEFORE SIGNING
        Element sigElement = sig.getElement();
        root.appendChild(sigElement);

        Resolver res = new Resolver(null,  baseURL,null);
        sig.getSignedInfo().addResourceResolver(res);
        Transforms transforms = new Transforms(targetDoc);
        transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
        transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);
        
        // the first element is cloned into the target, but the other ones have to be added via addNodes
        sig.addDocument("", transforms, Constants.ALGO_ID_DIGEST_SHA1);
        signedNodes.remove(0);
        addNodes(sig,signedNodes,targetDoc);

        addURLs(sig,URLs);
    
        X509Certificate cert = (X509Certificate) ks
                .getCertificate(key.certAlias);

        sig.addKeyInfo(cert);
        sig.addKeyInfo(cert.getPublicKey());
        System.out.println("Start signing");
        sig.sign(privateKey);
        System.out.println("Finished signing");
        return root;
    }

    public static KeyStore getKeyStore(SigningKey key) throws Exception
    {
        String keystorePass = Resources.getResourceString("xsmiles.default.key.pass");

        Constants.setSignatureSpecNSprefix("dsig");

        String keystoreType = "JKS";

        InputStream keystoreIs = null;
        URL keyURL;
        if (key == null)
        {
            key=getKey(key);
        } 
        {
            keyURL=key.storeURL;
        }
        keystoreIs = keyURL.openConnection().getInputStream();
        KeyStore ks = KeyStore.getInstance(keystoreType);
        ks.load(keystoreIs, keystorePass.toCharArray());
        return ks;
    }

}