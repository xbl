/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.awt.Component;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.EventImpl;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.gui.components.XSelect;
import fi.hut.tml.xsmiles.gui.components.XSelectMany;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public class SelectElementImpl extends VisualFormElement 
	implements VisualComponentService, MultiValue
{
    protected XSelect select;
    protected Vector options;
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public SelectElementImpl(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc)
    {
        super(ownerDocument, namespaceURI, qualifiedName,mlfc);
        // TODO Auto-generated constructor stub
    }
    public void init()
    {
        this.createVisual();
        this.component=select;
        super.init();
    }
    protected void createVisual()
    {
        // select1
        if (this.isMultiple())
        {
            select=mlfc.getMLFCListener().getComponentFactory().getXSelectMany("compact",false);
        } 
        else
        {
            select=mlfc.getMLFCListener().getComponentFactory().getXSelectOne("minimal",false);
        }
        this.addOptions();
    }
    protected void addOptions()
    {
        Enumeration options=this.getOptions().elements();
        while(options.hasMoreElements())
        {
            OptionElementImpl option = (OptionElementImpl)options.nextElement();
            String label = option.getLabel();
            this.select.addSelection(label);
        }
    }
    
    public boolean isMultiple()
    {
        if (this.getAttributeNode("multiple")==null) return false;
        return true;
    }
    public boolean isSuccessful()
    {
        return true;
    }
    
    public Vector getOptions()
    {
        if (options==null)
        {
            NodeList nl = this.getElementsByTagNameNS(this.getNamespaceURI(),"option");
            options = new Vector();
            this.copyNodeListToVector(nl,options);
        } 
        return options;
    }
    
    protected void copyNodeListToVector(NodeList nl, Vector v)
    {
        for (int i=0;i<nl.getLength();i++)
        {
            v.addElement(nl.item(i));
        }
    }

    protected String getValueForElementAt(int i)
    {
        OptionElementImpl selection =  (OptionElementImpl)this.getOptions().elementAt(i);
        return selection.getValue();
    }
    protected String getCurrentValue()
    {
        try
        {
                return getValueForElementAt(this.select.getSelectedIndex());
        } catch (ArrayIndexOutOfBoundsException e)
        {
            Log.debug(e.getMessage());
            return null;
        }
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.MultiValue#hasMultiValue()
     */
    public boolean hasMultiValue()
    {
        // TODO Auto-generated method stub
        return this.isMultiple();
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.MultiValue#getNumValues()
     */
    public int getNumValues()
    {
        // TODO Auto-generated method stub
        if (! (select instanceof XSelectMany)) return 0;
        return ((XSelectMany)select).getSelectedIndices().length;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.MultiValue#getMultiValue(int)
     */
    public String getMultiValue(int i)
    {
        // TODO Auto-generated method stub
        if (!(select instanceof XSelectMany)) return null;
        int ind =  ((XSelectMany)select).getSelectedIndices()[i];
        return this.getValueForElementAt(ind);
    }


}
