/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.gui.components.XButtonGroup;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public class FormElementImpl extends FormElement
{
    protected Hashtable buttonGroups;
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public FormElementImpl(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc )
    {
        super(ownerDocument, namespaceURI, qualifiedName,mlfc);
        // TODO Auto-generated constructor stub
    }
    
    protected synchronized XButtonGroup getButtonGroup(String name)
    {
        if (buttonGroups==null) buttonGroups=new Hashtable();
        XButtonGroup group = (XButtonGroup)buttonGroups.get(name);
        if (group==null)
        {
            group=new XButtonGroup();
            buttonGroups.put(name,group);
        }
        return group;
    }

    /**
     * 
     */
    public void doSubmission(InputElementImpl initiator)
    {
        Submission submission = new Submission(this);
        // TODO Auto-generated method stub
        submission.doSubmission(initiator);
    }
    
    public String getAction()
    {
        return this.getAttribute("action");
    }
    public URL getActionURL() throws MalformedURLException
    {
          return this.resolveURI(this.getAction());
    }
    public String getMethod()
    {
        return this.getAttribute("method");
    }
    

}
