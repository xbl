/*
 * Created on Feb 6, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.VisualElementImpl;

/**
 * @author honkkis
 *
 * Simple class to enable inline style attribute for HTML
 */
public class XHTMLElement extends VisualElementImpl
{
    
    public XHTMLElement(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName) 
    {
        super(ownerDocument,namespaceURI, qualifiedName);
    }
    
    /** By default return null, so style attribute is not supported,
     * this can be overridden by subclasses */
    public String getStyleAttrValue()
    {
        return this.getAttribute("style");
    }

}
