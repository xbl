/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XButtonGroup;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.gui.components.XText;


/**
 * @author honkkis
 *
 */
public class InputRadioImpl extends InputBase
{
    XSelectBoolean input;
    /**
     * @param domElement
     */
    public InputRadioImpl(InputElementImpl domElement)
    {
        super(domElement);
        // TODO Auto-generated constructor stub
    }
    public void init()
    {
        super.init();
        try
        {
	        XButtonGroup group = this.element.getFormElement().getButtonGroup(this.element.getName());
	        if (group!=null)
	        {
	            group.add(input);
	        }
        } catch (Exception e)
        {
            Log.debug("InputRadioImpl.init() "+e.getMessage());
        }
    }
    
    protected void createComponent()
    {
        boolean initialValue=this.element.getAttributeNode("checked") != null;
        this.input=this.getComponentFactory().getXSelectBoolean();
        this.input.setSelected(initialValue);
        this.component=this.input;
        // debug
        //this.input.getComponent().setSize(100,100);
    }
    public boolean getChecked()
    {
        // TODO Auto-generated method stub
        return input.getSelected();
    }
    

    
    public boolean isSuccessful()
    {
        return getChecked();
    }


}
