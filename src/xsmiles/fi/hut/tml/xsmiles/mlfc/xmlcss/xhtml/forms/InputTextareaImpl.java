/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import fi.hut.tml.xsmiles.gui.components.XInput;


/**
 * @author honkkis
 *
 */
public class InputTextareaImpl extends InputTextImpl
{
    /**
     * @param domElement
     */
    public InputTextareaImpl(InputElementImpl domElement)
    {
        super(domElement);
        // TODO Auto-generated constructor stub
    }
    
    protected void createComponent()
    {
        String initialText=this.element.getInitialValue();
        this.input=this.getComponentFactory().getXTextArea("");
        this.input.setText(initialText);
        this.component=this.input;
    }


}
