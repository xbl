/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.awt.Component;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.EventImpl;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public class InputElementImpl extends VisualFormElement implements VisualComponentService
{
    protected InputBase visual;
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public InputElementImpl(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc)
    {
        super(ownerDocument, namespaceURI, qualifiedName,mlfc);
        // TODO Auto-generated constructor stub
    }
    public void init()
    {
        this.createVisual();
        if (visual!=null)
        {
            this.visual.init();
            this.component=visual.getXComponent();
        }
        super.init();
    }
    protected void createVisual()
    {
        String type=this.getType();
        /*
        if (type.equalsIgnoreCase("text"))
        {
        }
        else 
        */
        if (type.equalsIgnoreCase("submit"))
        {
            visual=new InputSubmitImpl(this);
        }
        else if (type.equalsIgnoreCase("reset"))
        {
            visual=new InputResetImpl(this);
        }
        else if (type.equalsIgnoreCase("password"))
        {
            visual=new InputPasswordImpl(this);
        }
        else if (type.equalsIgnoreCase("checkbox"))
        {
            visual=new InputCheckboxImpl(this);
        }
        else if (type.equalsIgnoreCase("radio"))
        {
            visual=new InputRadioImpl(this);
        }
        else 
        {
            visual=new InputTextImpl(this);
        }
    }
    
    public boolean isSuccessful()
    {
        return visual.isSuccessful();
    }

    protected String getType()
    {
        final String typeN="type";
        final String typeDef="text";
        if (this.getAttributeNode(typeN)==null) return typeDef;
        return this.getAttribute(typeN);
    }
    public Component getComponent()
    {
        // TODO Auto-generated method stub
        if (this.visual==null)return null;
        return this.visual.getComponent();
    }
    
    /** By default return null, so style attribute is not supported,
     * this can be overridden by subclasses */
    public String getStyleAttrValue()
    {
        if (this.isHidden()) return "display:none;";
        return super.getStyleAttrValue();
    }
    
    public boolean isHidden()
    {
        return this.getType().equalsIgnoreCase("hidden");
    }
    
    /**
     * this method is overridden so that default actions for certain events can be processed
     */
    public boolean dispatchEvent(org.w3c.dom.events.Event event)
    {
        boolean ret = super.dispatchEvent(event);
        this.visual.dispatchEvent(event);
        return ret;
    }
    
    protected String getCurrentValue()
    {
        if (this.visual instanceof InputTextImpl)
        {
            return this.visual.getCurrentValue();
        }
        return this.getValueAttr();
    }


}
