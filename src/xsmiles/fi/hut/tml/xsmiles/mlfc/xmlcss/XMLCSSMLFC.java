/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss;
import java.awt.Container;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;
import fi.hut.tml.xsmiles.csslayout.ExternalWindow;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.PopupHandler;
import fi.hut.tml.xsmiles.dom.RefreshableService;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.ObjectElementImpl;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLAppletElement;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.FormElementImpl;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.InputElementImpl;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.OptionElementImpl;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.SelectElementImpl;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms.TextAreaElementImpl;


/**
 * @author Mikko Pohja / Mikko Honkala
 */
public class XMLCSSMLFC extends MLFC implements RefreshableService, PopupHandler // refreshable
// service
// for
// "dynamic"
// xforms
// effects
{
    protected CSSRenderer renderer;
    protected Container scrollPane;
    public static final String XHTML_NS = "http://www.w3.org/1999/xhtml";
    public static final String XHTML2_NS = "http://www.w3.org/2002/06/xhtml2/";
    public static final short DOC_HTML_OR_XHTML = 123;
    public static final short DOC_XML = 124;
    public static final short DOC_UNKNOWN = 0;
    protected short documentType = DOC_UNKNOWN;
    public Vector windows;

    public XMLCSSMLFC()
    {
        super();
    }
    /**
     * Initialize the MLFC. This function is called just before the elements are
     * initialized. There is no pair to this method.
     */
    protected boolean defaultStylesheetAdded = false;

    public void init()
    {
        if (defaultStylesheetAdded == false)
        {
            try
            {
                ((ExtendedDocument) getXMLDocument().getDocument())
                        .getStyleSheet()
                        .addXMLDefaultStyleSheet(Browser.getXResource("xsmiles.xhtml.stylesheet"));
                defaultStylesheetAdded = true;
            }
            catch (Exception e)
            {
                Log.error(e);
            }
        }
        if (documentType == DOC_HTML_OR_XHTML)
        {
            try
            {
	            String ns = this.getXMLDocument().getDocument().getDocumentElement().getNamespaceURI();
	            Element head =  (Element)this.getXMLDocument().getDocument().getDocumentElement().getElementsByTagNameNS(ns,"head").item(0);
	            XSmilesElementImpl titleElem =  (XSmilesElementImpl)head.getElementsByTagNameNS(ns,"title").item(0);
	            
	            this.setTitle(titleElem.getText());
            } catch (Exception e)
            {
                Log.debug(this+"init(): "+e.getMessage());
            }
        }
            
        super.init();
        this.addXHTMLStyleSheets();

        this.addRenderer();
    }


    /** create or re-create renderer. NOTE: creates also the scollpane */
    public void addRenderer()
    {
        renderer = createRenderer();
        renderer.setZoom(this.getMLFCListener().getZoom());
        scrollPane = this.getMLFCListener().getComponentFactory().createScrollPane(renderer.getComponent(),0);
	//renderer.addScrollPane();
	//scrollPane.setViewportBorder(new EmptyBorder(0, 0, 0, 0));
        scrollPane.setVisible(true);
        Log.debug("XMLCSS.init()");
    }

    protected CSSRenderer createRenderer()
    {
        return new CSSRenderer(this, false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#commitUpdateTransaction()
     */
    public void commitUpdateTransaction()
    {
        if (renderer != null)
        {
            renderer.commit();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#rollbackUpdateTransaction()
     */
    public void rollbackUpdateTransaction()
    {
        commitUpdateTransaction();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#startUpdateTransaction()
     */
    public void startUpdateTransaction()
    {
        if (renderer != null)
        {
            renderer.startTransaction();
        }
    }

    /**
     * notify that the zoom level has changed. Note that some content may choose
     * to not implement zooming.
     */
    public void setZoom(double zoom)
    {
        this.renderer.setZoom(zoom);
        try
        {
            VisualElementImpl elem = (VisualElementImpl) this.getXMLDocument().getDocument()
                    .getDocumentElement();
            elem.styleChanged();
            /*
            this.renderer.createViews();
            //this.renderer.refresh();
            this.renderer.layoutRenderer();
            */ 
            //this.renderer.setDocument(this.getXMLDocument().getDocument());
            this.renderer.recreateViews();
            renderer.getComponent().repaint();
            this.renderer.getComponent().invalidate();
            this.renderer.getComponent().getParent().validate();
            
        }
        catch (Exception e)
        {
            Log.error(e);
        }
    }
    protected boolean started = false;

    /**
     * Start the MLFC. This function is the pair to stop().
     */
    public void start()
    {

        //long start = System.currentTimeMillis();
        Log.debug("XMLCSS.start() at " + System.currentTimeMillis());

        renderer.setDocument(this.getXMLDocument().getDocument());
        //renderer.setSize(1000,3000);
        this.getContainer().add(scrollPane);
        //this.createDummyComponent(this.getContainer());
        setScrollBar(getXMLDocument().getXMLURL().getRef());
        started = true;
        repaintExternalWindows();
        //long time = System.currentTimeMillis() - start;
        //System.out.println("***** XMLCSS.start() took " + Long.toString(time) + " ms");
    }

    /*
     * public void createDummyComponent(Container container) { // Just for debug
     * purposes, create a source MLFC and display the document in it
     * //SourceMLFC source = new SourceMLFC();
     * //source.display(this.getXMLDocument(), XsmilesView.SOURCEMLFC,
     * container);
     *  
     */
    // public abstract void start(Hashtable params);
    /**
     * Stop the MLFC. This function is the pair to start().
     */
    public void stop()
    {
        if (started)
        {
            Log.debug("XMLCSS.stop()");
            getContainer().removeAll();
            started = false;

            // Remove external windows.
            if (windows != null)
            {
                Enumeration e = windows.elements();
                while (e.hasMoreElements())
                {
                    ((ExternalWindow) e.nextElement()).dispose();
                }
            }
            
            // Close a View from Wesahmi system
            getMLFCListener().closeView();
        }
    }

    public void repaintExternalWindows()
    {
        // Paint external windows.
        if (windows != null)
        {
            Enumeration e = windows.elements();
            while (e.hasMoreElements())
            {
                ((ExternalWindow) e.nextElement()).repaint();
            }
        }
    }

    /** Creates xhtml elements to XSimles document. */
    public org.w3c.dom.Element createElementNS(DocumentImpl doc, String URI, String tag)
            throws DOMException
    {
        if (documentType == DOC_UNKNOWN)
        {
            if (XHTML_NS.equals(URI) || XHTML2_NS.equals(URI)
                    || ((ExtendedDocument) doc).isHTMLDocument())
            {
                documentType = DOC_HTML_OR_XHTML;
            }
            else
                documentType = DOC_XML;
        }

        // TODO: check namespace or if NekoHTML was used
        if (this.documentType == DOC_HTML_OR_XHTML)
        {
            // To support inline style attribute
            if (tag.equals("object"))
            {
                return new ObjectElementImpl(doc, URI, tag, this);
            }
            else if (tag.equals("applet"))
            {
                return new XHTMLAppletElement(doc, URI, tag, this);
            }
            // ------------- FORM ELEMENTS -------
            else if (tag.equals("input"))
            {
                return new InputElementImpl(doc, URI, tag, this);
            }
            else if (tag.equals("textarea"))
            {
                return new TextAreaElementImpl(doc, URI, tag, this);
            }
            else if (tag.equals("select"))
            {
                return new SelectElementImpl(doc, URI, tag, this);
            }
            else if (tag.equals("option"))
            {
                return new OptionElementImpl(doc, URI, tag, this);
            }
            else if (tag.equals("form"))
            {
                return new FormElementImpl(doc, URI, tag, this);
            }
            else
            {
                return new XHTMLElement(doc, URI, tag);
            }
        }
        else
        {
            // To support DOM's dynamic modifications.
            return new VisualElementImpl(doc, URI, tag);
        }
        //        return null; // generate default element
    }

    /**
     * finds html/head/style elements and adds them to the CSS engine does not
     * care about namespaces, so should work with XHTML1 & 2
     */
    public void addXHTMLStyleSheets()
    {
        Document doc = this.getXMLDocument().getDocument();
        Element docElem = doc.getDocumentElement();
        if (docElem == null || (!docElem.getLocalName().equals("html")))
            return; // TODO: ns
        // Try to find /html/head/style elements with DOM operations (do not
        // care about namespaces now)
        Element headElem = findChildElement("head", docElem); // TODO: ns
        if (headElem == null)
            return;
        Node child = headElem.getFirstChild();
        while (child != null)
        {
            boolean linkElement = false;
            if (child instanceof XSmilesElementImpl)
            {
                XSmilesElementImpl childElem = (XSmilesElementImpl) child;
                if (childElem.getLocalName().equals("link"))
                    linkElement = true;
                if (linkElement || childElem.getLocalName().equals("style")) // TODO:
                // ns
                {
                    // Check Media Query attribute
                    String media = childElem.getAttribute("media");
                    if (media != null && media.length() > 0)
                        // Evaluate media string
                        if (this.getXMLDocument().evalMediaQuery(media) == false)
                        {
                            Log.debug("Skipping <style> element, media query evaluated to false: "
                                    + childElem.getAttribute("href"));
                            break;
                        }
                    // Media Query passed
                    try
                    {
                        XSmilesStyleSheet sheet = ((ExtendedDocument) getXMLDocument().getDocument())
                                .getStyleSheet();
                        if (linkElement)
                        {
                            if (
                                    childElem.getAttribute("rel").equalsIgnoreCase("stylesheet")
                                    || childElem.getAttribute("type").equalsIgnoreCase("text/css"))
                            {
                                if (!childElem.getAttribute("rel").toLowerCase().startsWith("alternate"))
                                {
                                sheet.addXMLStyleSheet(childElem.resolveURI(childElem
                                        .getAttribute("href")));
                                }
                            }
                        }
                        else
                        {
                            if (
                                    childElem.getAttributeNode("type")==null // this allows style without type attribute
                                    || childElem.getAttribute("type").equalsIgnoreCase("text/css")
                                    )
                                sheet.addXMLStyleSheet(childElem.getText(), new URL(childElem
                                        .getBaseURI()));
                        }
                    }
                    catch (MalformedURLException mue)
                    {
                        Log.error(mue);
                    }
                }
            }
            child = child.getNextSibling();
        }
    }
    
    /**
     * Set the title for the presentation.
     * @param title		Title for the presentation
     */
    /* the super handles this from now on
    public void setTitle(String title)
    {
        if (isPrimary() == true)
        {
            if (title!=null && title.length()>0)
            {
                getMLFCListener().setTitle(title);
            }
        }
    }
    */

    public static Element findChildElement(String name, Element parent)
    {
        Node child = parent.getFirstChild();
        while (child != null)
        {
            if (child instanceof Element)
            {
                if (((Element) child).getLocalName().equals(name))
                    return (Element) child;
            }
            child = child.getNextSibling();
        }
        return null;
    }

    public void refresh()
    {
        /*
         * if (started&&!refreshBlocking) { this.getContainer().removeAll();
         * this.createRenderer(); this.start(); this.getContainer().validate(); }
         */
    }
    protected boolean refreshBlocking = false;

    public void setBlocking(boolean state)
    {
        this.refreshBlocking = state;
    }

    public void setScrollBar(String id)
    {
        if (id != null && !id.equals(""))
        {
            Element elem = getXMLDocument().getDocument().getElementById(id);
            if (elem == null)
                return;
            Vector views = ((VisualElementImpl) elem).getViews();
            View view = null;
            if (views != null)
                view = (View) views.firstElement();
            if (view != null)
            {
                int y = view.getAbsolutePositionY();
                this.getMLFCListener().getComponentFactory().setScrollBar(scrollPane, 0, y);
            }
        }
        else
        {
            this.getMLFCListener().getComponentFactory().setScrollBar(scrollPane, 0, 0);
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.PopupHandler#showAsPopup(fi.hut.tml.xsmiles.dom.StylableElement)
     */
    public void showAsPopup(StylableElement elem, String containerType)
    {
        if (this.renderer!=null) this.renderer.showAsPopup(elem,containerType);
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.PopupHandler#closePopup(fi.hut.tml.xsmiles.dom.StylableElement)
     */
    public void closePopup(StylableElement elem)
    {
        // TODO Auto-generated method stub
        if (this.renderer!=null) this.renderer.closePopup(elem);
        
    }
}
