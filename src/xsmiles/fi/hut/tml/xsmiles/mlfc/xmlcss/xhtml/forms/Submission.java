/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 28, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.net.URL;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;


/**
 * @author honkkis
 *
 */
public class Submission
{
    FormElementImpl form;
    InputElementImpl initiator;
    public Submission(FormElementImpl element)
    {
        form=element;
    }
    
    public synchronized void doSubmission(InputElementImpl init)
    {
        this.initiator=init;
        if (form.getMethod().equalsIgnoreCase("post")) this.submitPost();
        else submitGet();
    }
    
    public void submitGet()
    {
        try
        {
	        URL actionURL=form.getActionURL();
	        // TODO: add URL parameters
	        actionURL=new URL(actionURL.toString()+this.toGetParams());
	        form.mlfc.getMLFCListener().openLocation(actionURL);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    protected String getEncoded(String localn, String text)
    {
    	if (localn!=null&&localn.length()>0)
    	{
        	if (text==null) return ( localn+"=&");
        	return ( localn+"="+java.net.URLEncoder.encode(text)+"&");
    	}
    	return "";
    }
    
    
    protected String toGetParams()
    {
        StringBuffer res=new StringBuffer();
        res.append("?");
        NodeList nl = form.getElementsByTagNameNS("*","*");
        for (int i=0;i<nl.getLength();i++)
        {
            Element e = (Element)nl.item(i);
            if (e instanceof VisualFormElement)
            {
                VisualFormElement control = (VisualFormElement)e;
                if (isSuccesful(control))
                {
                    if (control instanceof MultiValue && ((MultiValue)control).hasMultiValue())
                    {
                        MultiValue multi = (MultiValue )control;
                        for (int j=0;j<multi.getNumValues();j++)
                        {
                            res.append(this.getEncoded(control.getName(),multi.getMultiValue(j)));
                        }
                    }
                    else
                    {
                        res.append(this.getEncoded(control.getName(),control.getCurrentValue()));
                    }
                }
                // http://www./../../..?f.t.name1=value1&f.t.name2=value2
            }
        }
        return res.toString();
    }
    
    public boolean isSuccesful(VisualFormElement vis)
    {
        if (!vis.isSuccessful()) return false;
        if (vis instanceof InputElementImpl)
        {
            if  ( ((InputElementImpl)vis).visual instanceof InputSubmitImpl)
            {
            	if (vis != this.initiator) return false;
            }
        }
        return true;
    }

    public void submitPost()
    {
        Log.error("POST NOT IMPLEMENTED YET");
        submitGet();
        
    }
}

