/*
 * Created on Jun 24, 2004
 * 
 * TODO To change the template for this generated file go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml2;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;

import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.Log;

/**
 * @author mpohja
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class XHTML2NavigationListElementImpl extends VisualElementImpl implements EventListener
{

    boolean selected = false, focusLost = true;

    /**
     * @param ownerDocument
     * @param value
     */
    public XHTML2NavigationListElementImpl(DocumentImpl ownerDocument, String value)
    {
        super(ownerDocument, value);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public XHTML2NavigationListElementImpl(DocumentImpl ownerDocument, String namespaceURI,
            String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
        addEventListener("mouseover", this, true);
        addEventListener("mouseout", this, true);
    }

    public void handleEvent(Event evt)
    {
        String type = evt.getType();
        if (type.equals("mouseover"))
        {
            focusLost = false;
            setStatus(true);
        }
        else if (type.equals("mouseout"))
        {
            focusLost = true;
            MouseOuter mo = new MouseOuter(50);
            mo.start();
        }
        //Log.debug("Got event " + evt.getType() + ", which phase is " +
        // evt.getEventPhase());
    }

    public boolean isSelected()
    {
        return selected;
    }

    private void loseFocus()
    {
        if (focusLost)
            setStatus(false);
    }

    public void setStatus(boolean status)
    {
        if (status != selected)
        {
            selected = status;
            styleChanged();
        }
        //Log.debug("set status to "+status);
    }

    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        if (pseudoclass.equals("active"))
            return selected;
        else
            return super.isPseudoClass(pseudoclass);
    }

    /**
     * Handles consecutive mouseout-mouseover events.
     * 
     * @author mpohja
     */
    private class MouseOuter extends Thread
    {
        long wait;

        MouseOuter(long msec)
        {
            wait = msec;
        }

        public void run()
        {
            try
            {
                Thread.sleep(wait);
                loseFocus();
            }
            catch (InterruptedException e)
            {
                Log.error(e);
            }
        }
    }
}