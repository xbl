/*
 * Created on Jun 23, 2004
 * 
 * TODO To change the template for this generated file go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;

import fi.hut.tml.xsmiles.dom.*;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml2.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.XHTML2Renderer;
import fi.hut.tml.xsmiles.csslayout.CSSRenderer;

/**
 * @author mpohja
 * 
 * 
 * Preferences - Java - Code Generation - Code and Comments
 */
public class XHTML2MLFC extends XMLCSSMLFC
{

    public void start()
    {
        if (this.isHost())
        {
            Log.debug("XHTML2MLFC.start()");
            super.start();
        }
    }

    /** creates XHTML 2 renderer */
    public CSSRenderer createRenderer()
    {
        return new XHTML2Renderer(this, false);
    }

    /** Creates xhtml2 elements to XSimles document. */
    public org.w3c.dom.Element createElementNS(DocumentImpl doc, String URI, String tag)
            throws DOMException
    {
        if (documentType == DOC_UNKNOWN)
        {
            if (XHTML_NS.equals(URI) || XHTML2_NS.equals(URI)
                    || ((ExtendedDocument) doc).isHTMLDocument())
            {
                documentType = DOC_HTML_OR_XHTML;
            }
            else
                documentType = DOC_XML;
        }

        // TODO: check namespace or if NekoHTML was used
        if (this.documentType == DOC_HTML_OR_XHTML)
        {
            // To support inline style attribute
            if (tag.equals("nl"))
                return new XHTML2NavigationListElementImpl(doc, URI, tag);
        }
        // To support DOM's dynamic modifications.
        return super.createElementNS(doc, URI, tag);
    }
}