/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 17, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Panel;
import java.net.MalformedURLException;

import javax.swing.JPanel;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.jax.AppletStarter;


/**
 * @author honkkis
 *
 */
public class XHTMLAppletElement extends XHTMLElement implements VisualComponentService
{
    AppletStarter appletStarter;
    Container appletPanel;
    MLFC mlfc;
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public XHTMLAppletElement(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC amlfc)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
        this.mlfc=amlfc;
        // TODO Auto-generated constructor stub
    }
    
    public void init()
    {
        super.init();
    }
    public void destroy()
    {
        appletStarter.stop();
        super.destroy();
    }
    
    protected void initApplet()
    {
        //      using AWT panel resulted in null font... maybe because the panel is not added anywhere...
        // could be fixed by doing initApplet after adding the panel 
        appletPanel=new JPanel(); 
        appletPanel.setLayout(new BorderLayout());
        int width=this.getWidth();
        int height=this.getHeight();
        appletPanel.setSize(width,height); // TODO: get from applet
        appletStarter = new AppletStarter();
        try
        {
            appletStarter.displayApplet(this.resolveURI(""),appletPanel,this,mlfc.getMLFCListener(),true);
            appletPanel.setVisible(true);
        } catch (MalformedURLException e)
        {
            Log.error(e);
            appletPanel=null;
            appletStarter=null;
        }
    }
    protected int getWidth()
    {
        int width=600;
        try
        {
            String widthA=this.getAttribute("width");
            if (widthA!=null) width=Integer.parseInt(widthA);
        }
        catch (Exception e)
        {
            Log.error(e);
        }
        return width;
    }
    protected int getHeight()
    {
        int width=400;
        try
        {
            String widthA=this.getAttribute("height");
            if (widthA!=null) width=Integer.parseInt(widthA);
        }
        catch (Exception e)
        {
            Log.error(e);
        }
        return width;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getComponent()
     */
    public Component getComponent()
    {
        if (appletPanel==null)
        {
            initApplet();
        }
        return appletPanel;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getSize()
     */
    public Dimension getSize()
    {
        return appletPanel.getSize();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#setZoom(double)
     */
    public void setZoom(double zoom)
    {
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#setVisible(boolean)
     */
    public void setVisible(boolean visible)
    {
        appletPanel.setVisible(visible);
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getVisible()
     */
    public boolean getVisible()
    {
        return appletPanel.isVisible();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }

}
