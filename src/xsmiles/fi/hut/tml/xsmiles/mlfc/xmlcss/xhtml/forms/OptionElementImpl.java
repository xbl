/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public class OptionElementImpl extends FormElement
{

    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public OptionElementImpl(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc )
    {
        super(ownerDocument, namespaceURI, qualifiedName,mlfc);
        // TODO Auto-generated constructor stub
    }
    
    public String getValue()
    {
        Attr labelAttr = this.getAttributeNode("value");
        if (labelAttr==null) return this.getText().trim();
        return labelAttr.getNodeValue();
    }
    public String getLabel()
    {
        Attr labelAttr = this.getAttributeNode("label");
        if (labelAttr==null) return this.getText().trim();
        return labelAttr.getNodeValue();
    }
    

}
