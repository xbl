/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.awt.Component;
import java.awt.Dimension;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public class VisualFormElement extends FormElement implements VisualComponentService
{
    XComponent component;
    
    // TODO: use em to get real size

    int preferredWidth = -1; // the width
    int preferredHeight = -1; // the height
    
    protected String initialValue;
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     * @param mlfc
     */
    public VisualFormElement(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc)
    {
        super(ownerDocument,namespaceURI, qualifiedName,mlfc);
        // TODO Auto-generated constructor stub
    }
    
    public void init()
    {
        super.init();
        this.formatComponent();
    }
    
    protected void setInitialValue()
    {
        this.initialValue=this.getValueAttr();
    }
    
    public boolean isSuccessful()
    {
        return true;
    }
    
    /**
     * Formats the content according to the CSS style attribute,
     * this can be overridden by the extending classes
     */
    protected void formatComponent()
    {
        // format the content component
        CSSStyleDeclaration style=this.getStyle();
        if (style==null||this.component==null) 
        {
            Log.error("formatComponent: style or component was null: style:"+style+" component:"+component);
            return;
        }
        this.component.setStyle(style);
    }
    
    protected String getValueAttr()
    {
        return this.getAttribute("value");
    }

    protected String getCurrentValue()
    {
        return this.getValueAttr();
    }



    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getComponent()
     */
    public Component getComponent()
    {
        // TODO Auto-generated method stub
        return component.getComponent();
    }
    
    /** By default return null, so style attribute is not supported,
     * this can be overridden by subclasses */
    public String getStyleAttrValue()
    {
        // TODO: use em to get real size

        String addition="";
        if (this.preferredWidth>-1)
        {
            addition+="width:"+preferredWidth+"px;";
        }
        if (this.preferredHeight>-1)
        {
            addition+="height:"+preferredHeight+"px;";
        }
        return super.getStyleAttrValue()+addition;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getSize()
     */
    public Dimension getSize()
    {
        // TODO Auto-generated method stub
        try
        {
            return new Dimension(100,100);
            //return this.getComponent().getSize();
        } catch (Exception e)
        {
            return new Dimension(0,0);
        }
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#setZoom(double)
     */
    public void setZoom(double zoom)
    {
        // TODO Auto-generated method stub
        
    }
    

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#setVisible(boolean)
     */
    public void setVisible(boolean visible)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getVisible()
     */
    public boolean getVisible()
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    /**
     * @return
     */
    public String getInitialValue()
    {
        // TODO Auto-generated method stub
        if (initialValue==null) this.setInitialValue();
        return initialValue;
    }

    /**
     * @return
     */
    public String getName()
    {
        // TODO Auto-generated method stub
        return this.getAttribute("name");
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        if (event==VisualComponentService.EVENT_STYLECHANGED)
        {
            this.formatComponent();
        }
        
    }

}
