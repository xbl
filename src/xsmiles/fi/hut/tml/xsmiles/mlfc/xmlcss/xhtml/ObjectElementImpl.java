/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.MediaElement;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * Implements the html object element
 * 
 * @author tjjalava
 * @since Mar 23, 2004
 * @version $Revision: 1.8 $, $Date: 2006/02/24 13:51:01 $
 */
public class ObjectElementImpl extends XHTMLElement implements VisualComponentService, MediaElement, MediaListener {

    private static final int DEFAULT_WIDTH = 150;
    private static final int DEFAULT_HEIGHT = 150;
    
    private String source;
    private String mimeType;
    private XSmilesContentHandler handler;
    private MLFC mlfc;
    private Container container = null;
    private boolean visible;
    private boolean active = true;
    private boolean prefetch = false;
    private Vector mediaListeners = new Vector();
    private int status = STOPPED;
    private boolean prefetched = false;
    
    /**
     * New object element
     * 
     * @param ownerDocument
     *                     the document this element belongs to
     * @param namespaceURI
     * @param qualifiedName
     * @param mlfc
     *                     MLFC that is controlling the current document
     */
    public ObjectElementImpl(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc) {
        super(ownerDocument, namespaceURI, qualifiedName);
        this.mlfc = mlfc;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.XSmilesElementImpl#init()
     */
    public void init() {
        super.init();
        source = getAttribute("data");
        mimeType = getAttribute("type");
        try {
            XLink link = new XLink(resolveURI(source));
            container = new Container();
            container.setLayout(new BorderLayout());
            handler = mlfc.getMLFCListener().createContentHandler(mimeType, link, container, false);
            // add to the list of referenced URLs for XML signature purposes
            try
            {
                String contentType=null; //TODO:
                Resource r = new Resource(link.getURL(),Resource.RESOURCE_OBJECT,contentType);
                this.getResourceReferencer().addResource(r);
            } catch (Exception e)
            {
                Log.error(e);
            }
            if (!handler.isStatic()) {
                handler.addMediaListener(this);
            }

            if (prefetch) {
                prefetch();
            }
            if (active) {
                handler.play();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.XSmilesElementImpl#destroy()
     */
    public void destroy() {
        super.destroy();
        if (handler != null) {
            handler.close();
        }
    }

    /**
     * Tries to parse a integer from a String.
     * 
     * @param value
     * 
     *                     the String to parse
     * @param defValue
     *                     if can't parse, return this
     * @return parsed value, or defValue if can't parse the String
     */
    private int parseInt(String value, int defValue) {
        int r = defValue;
        int i = value.length();
        while (i > 0) {
            try {
                r = Integer.parseInt(value);
                break;
            } catch (NumberFormatException e) {
                value = value.substring(0, (--i));
            }
        }
        return r;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getComponent()
     */
    public Component getComponent() {
        if (container != null) {
            int width = 0;
            int height = 0;
            if (!mimeType.startsWith("audio")) {
                width = handler.getOriginalWidth();
                height = handler.getOriginalHeight();
                if (width<0) {
                    width = DEFAULT_WIDTH;
                }
                if (height < 0) {
                    height = DEFAULT_HEIGHT;
                }
                width = parseInt(getStyle().getPropertyValue("width"), width);
                height = parseInt(getStyle().getPropertyValue("height"), height);
            }
            width = ((int) StyleGenerator.getScaledSize((int)width));
            height = ((int)StyleGenerator.getScaledSize((int)height));
                       
            container.setSize(width, height);
            handler.setBounds(0, 0, width, height);
            return container;
        } else {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getSize()
     */
    public Dimension getSize() {
        return new Dimension(0, 0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#setZoom(double)
     */
    public void setZoom(double zoom) {
       Log.debug("ObjectElementImp.setZoom"+zoom);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#setVisible(boolean)
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#getVisible()
     */
    public boolean getVisible() {
        return visible;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#setActive(boolean)
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#play()
     */
    public void play() throws Exception {
        if (handler != null) {
            handler.play();
            status = PLAYING;
            if (!handler.isStatic()) {
                prefetched = false;
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#pause()
     */
    public void pause() {
        if (handler != null) {
            handler.pause();
            status = (status == PAUSED ? PLAYING : PAUSED);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#stop()
     */
    public void stop() {
        if (handler != null && active) {
            handler.stop();
            status = STOPPED;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#addMediaListener(fi.hut.tml.xsmiles.gui.media.general.MediaListener)
     */
    public void addMediaListener(MediaListener listener) {
        if (!mediaListeners.contains(listener)) {
            mediaListeners.add(listener);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#removeMediaListener(fi.hut.tml.xsmiles.gui.media.general.MediaListener)
     */
    public void removeMediaListener(MediaListener listener) {
        mediaListeners.remove(listener);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mediaPrefetched()
     */
    public void mediaPrefetched() {
        prefetched = true;
        for (Enumeration e = mediaListeners.elements(); e.hasMoreElements();) {
            ((MediaListener) e.nextElement()).mediaPrefetched();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mediaEnded()
     */
    public void mediaEnded() {
        for (Enumeration e = mediaListeners.elements(); e.hasMoreElements();) {
            ((MediaListener) e.nextElement()).mediaEnded();
        }
        dispatchEvent(EventFactory.createEvent("end", true, true));
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseEntered()
     */
    public void mouseEntered() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseExited()
     */
    public void mouseExited() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mousePressed()
     */
    public void mousePressed() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseReleased()
     */
    public void mouseReleased() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#getStatus()
     */
    public int getStatus() {
        return status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return getLocalName() + " - " + getAttribute("id");
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#prefetch()
     */
    public void prefetch() throws Exception {
        if (handler != null) {
            if (!prefetched) {
                handler.prefetch();
                prefetched = true;
            }
        } else {
            prefetch = true;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.MediaElement#isStatic()
     */
    public boolean isStatic() {
        if (handler != null) {
            return handler.isStatic();
        }
        return true;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}