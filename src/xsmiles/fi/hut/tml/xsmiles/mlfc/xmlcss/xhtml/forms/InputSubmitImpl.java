/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import fi.hut.tml.xsmiles.dom.EventImpl;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;


/**
 * @author honkkis
 *
 */
public class InputSubmitImpl extends InputButtonImpl
{
    /**
     * @param domElement
     */
    public InputSubmitImpl(InputElementImpl domElement)
    {
        super(domElement);
        // TODO Auto-generated constructor stub
    }
    protected String getButtonLabel()
    {
        String value =  this.element.getValueAttr();
        if (value.length()==0) value="Submit";
        return value;
    }
    
    /**
     * this method is overridden so that default actions for certain events can be processed
     */
    public boolean dispatchEvent(org.w3c.dom.events.Event event)
    {
        // simply forward to super class
        super.dispatchEvent(event);
        if (event instanceof EventImpl)
        {
            EventImpl xe = (EventImpl)event;
            if (!xe.preventDefault&&!xe.stopPropagation)
            {
                if (xe.getType().equals(XFormsEventFactory.DOMACTIVATE_EVENT))
                {
                    this.doSubmission();
                }
            }
        }
        return true;
    }
    
    protected void doSubmission()
    {
        FormElementImpl form = this.element.getFormElement();
        form.doSubmission(this.element);
    }


}
