/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import fi.hut.tml.xsmiles.gui.components.XButton;


/**
 * @author honkkis
 *
 */
public class InputResetImpl extends InputButtonImpl
{
    /**
     * @param domElement
     */
    public InputResetImpl(InputElementImpl domElement)
    {
        super(domElement);
        // TODO Auto-generated constructor stub
    }
    protected String getButtonLabel()
    {
        String value =  this.element.getValueAttr();
        if (value.length()==0) value="Reset";
        return value;
    }
}