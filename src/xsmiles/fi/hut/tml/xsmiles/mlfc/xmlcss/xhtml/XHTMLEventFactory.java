/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Mar 31, 2006
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.UIEventImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Node;
import org.w3c.dom.events.Event;
import org.w3c.dom.DOMException;

import java.util.Hashtable;


public class XHTMLEventFactory extends EventFactory
{
    /* X-Smiles
     * Copyright (C) Helsinki University of Technology. All rights reserved.
     * For details on use and redistribution please refer to the
     * LICENSE_XSMILES file included with these sources.
     */


    /**
     * The factory for creating DOM event instances
     */
   // public class XFormsEventFactory extends EventFactory implements XFormsConstants{

      
      public XHTMLEventFactory() {
      }
      public final static String DOMACTIVATE_EVENT="DOMActivate";
      public final static String FOCUSOUT_NOTIFICATION_EVENT="blur";
      public final static String FOCUSIN_NOTIFICATION_EVENT="focus";
      /** XForms event prototypes */
      static Hashtable events;
      static
      {
        // create event prototypes
        // TODO: check that bubbling / cancelling is ok since the new events roundup finishes
        events=new Hashtable(5);
        
        // DOM events prototypes
        events.put(FOCUSIN_NOTIFICATION_EVENT, EventFactory.createEvent(FOCUSIN_NOTIFICATION_EVENT));
        events.put(FOCUSOUT_NOTIFICATION_EVENT, EventFactory.createEvent(FOCUSOUT_NOTIFICATION_EVENT));
        events.put(DOMACTIVATE_EVENT, EventFactory.createEvent(DOMACTIVATE_EVENT));
      }

      public static Event createEvent(String eventType) throws DOMException {
        Event theEvent = null;
        
            // Use the main event factory
            theEvent = EventFactory.createEvent(eventType);
        return theEvent;
      }

      public static Event createXHTMLEvent(String typeArg)
      {
       Event ev=null;
        try
        {
          // first check, whether we have a prototype ready
          ev = (Event)events.get(typeArg);
          if (ev==null) 
          {
            Log.debug("Could not find event prototype for: "+typeArg);
            return null;
          }
          if (ev instanceof UIEventImpl)
              return (Event)((UIEventImpl)ev).clone();
        } catch (CloneNotSupportedException e)
        {
            Log.error(e);
            return null;
        }
       return null;
      }


    } // EventFactory

