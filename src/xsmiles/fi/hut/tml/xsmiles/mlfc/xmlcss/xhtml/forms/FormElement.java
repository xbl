/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLEventFactory;


/**
 * @author honkkis
 *
 */
public class FormElement extends XHTMLElement
{
    protected MLFC mlfc;
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public FormElement(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName,MLFC a_mlfc)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
        this.mlfc=a_mlfc;
        // TODO Auto-generated constructor stub
    }
    
    public void dispatchActivateEvent()
    {
        dispatchEvent(XHTMLEventFactory.createXHTMLEvent(XHTMLEventFactory.DOMACTIVATE_EVENT));
    }
    
    public FormElementImpl getFormElement()
    {
        Element e = this;
        while (e!=null&&e instanceof Element)
        {
            if (e instanceof FormElementImpl) return (FormElementImpl)e;
            Node n=e.getParentNode();
            if (n==e.getOwnerDocument().getDocumentElement()) return null;
            e=(Element)n;
        }
        return null;
    }


}
