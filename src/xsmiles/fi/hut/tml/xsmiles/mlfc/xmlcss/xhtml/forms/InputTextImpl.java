/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XText;


/**
 * @author honkkis
 *
 */
public class InputTextImpl extends InputBase
{
    XText input;
    /**
     * @param domElement
     */
    public InputTextImpl(InputElementImpl domElement)
    {
        super(domElement);
        // TODO Auto-generated constructor stub
    }
    
    protected void setPreferredSize()
    {
        try
        {
            int charwidth=8;
            int size = Integer.parseInt(this.element.getAttribute("size"));
            this.element.preferredWidth=size*charwidth;
        } catch (Exception e)
        {
            //Log.error("InputBase.prefSize: "+e.getMessage());
        }
    }
    
    protected void createComponent()
    {
        String initialText=this.element.getInitialValue();
        this.input=this.getComponentFactory().getXInput();
        this.input.setText(initialText);
        this.component=this.input;
        // debug
        //this.input.getComponent().setSize(100,100);
    }
    public String getCurrentValue()
    {
        // TODO Auto-generated method stub
        return input.getText();
    }

}
