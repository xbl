/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.awt.Component;
import java.awt.Dimension;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public abstract class InputBase 
{
    protected InputElementImpl element;
    public InputBase(InputElementImpl domElement)
    {
        this.element = domElement;
        this.setPreferredSize();
    }
    
    protected void setPreferredSize()
    {
        try
        {
            int size = Integer.parseInt(this.element.getAttribute("size"));
            this.element.preferredWidth=size;
        } catch (Exception e)
        {
            //Log.error("InputBase.prefSize: "+e.getMessage());
        }
    }
    
    protected XComponent component;
    public XComponent getXComponent()
    {
        return this.component;
    }
    public Component getComponent()
    {
        return this.getXComponent().getComponent();
    }
    public boolean isSuccessful()
    {
        return true;
    }

    public void init()
    {
        this.createComponent();
    }
    protected abstract void createComponent();
    
    protected ComponentFactory getComponentFactory()
    {
        return this.element.mlfc.getMLFCListener().getComponentFactory();
    }
    
    /**
     * this method is overridden so that default actions for certain events can be processed
     */
    public boolean dispatchEvent(org.w3c.dom.events.Event event)
    {
        return true;
    }
    /**
     * @return
     */
    public String getCurrentValue()
    {
        // TODO Auto-generated method stub
        return this.element.getValueAttr();
    }

}

