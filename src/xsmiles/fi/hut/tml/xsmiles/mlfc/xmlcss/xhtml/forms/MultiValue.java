/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 29, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;


/**
 * @author honkkis
 *
 */
public interface MultiValue
{
    public boolean hasMultiValue();
    public int getNumValues();
    public String getMultiValue(int i);
}
