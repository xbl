/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fi.hut.tml.xsmiles.gui.components.XButton;


/**
 * @author honkkis
 *
 */
public class InputButtonImpl extends InputBase implements ActionListener
{
    /**
     * @param domElement
     */
    public InputButtonImpl(InputElementImpl domElement)
    {
        super(domElement);
        // TODO Auto-generated constructor stub
    }
    XButton button;
    protected void createComponent()
    {
        this.button=this.getComponentFactory().getXButton(this.getButtonLabel(),null);
        this.addButtonListener();
        this.component=this.button;
    }
    
    protected void addButtonListener()
    {
        this.button.addActionListener(this);
    }
    protected void removeButtonListener()
    {
        this.button.removeActionListener(this);
    }
    protected String getButtonLabel()
    {
        return this.element.getValueAttr();
    }
    
    public void destroy()
    {
        this.removeButtonListener();
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent arg0)
    {
        this.element.dispatchActivateEvent();
    }
}
