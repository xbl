/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 10, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.forms;

import java.awt.Component;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.EventImpl;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;


/**
 * @author honkkis
 *
 */
public class TextAreaElementImpl extends InputElementImpl 
{
    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public TextAreaElementImpl(DocumentImpl ownerDocument, String namespaceURI, String qualifiedName, MLFC mlfc)
    {
        super(ownerDocument, namespaceURI, qualifiedName,mlfc);
        // TODO Auto-generated constructor stub
    }
    protected void createVisual()
    {
        	setPreferredSize();
            visual=new InputTextareaImpl(this);
    }
    protected String getType()
    {
        return "xsmiles-textarea";
    }
    
    protected void setInitialValue()
    {
        this.initialValue=this.getText();
    }
    
    protected void setPreferredSize()
    {
        // TODO: use em to get real size
        int charwidth=8;
        int charheight=18;
        try
        {
            int size = Integer.parseInt(this.getAttribute("cols"));
            this.preferredWidth=size*charwidth;
        } catch (Exception e)
        {
            //Log.error("InputBase.prefSize: "+e.getMessage());
        }
        try
        {
            int size = Integer.parseInt(this.getAttribute("rows"));
            this.preferredHeight=size*charheight;
        } catch (Exception e)
        {
            //Log.error("InputBase.prefSize: "+e.getMessage());
        }
    }



}
