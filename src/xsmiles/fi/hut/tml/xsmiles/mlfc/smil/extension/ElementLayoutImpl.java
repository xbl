/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;

/**
 *  This interface is used by SMIL elements root-layout, top-layout and region.
 *
 * Includes attributes:
 *  o title, backgroundColor, height, width
 *  + 
 * 
 */
public class ElementLayoutImpl implements ElementLayout {
    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getTitle() {
		return null;
	}
    public void setTitle(String title) throws DOMException {
		return;
	}

    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getBackgroundColor() {
		return null;
	}
    public void setBackgroundColor(String backgroundColor) throws DOMException {
		return;
	}

    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getHeight() {
		return null;
	}
    public void setHeight(int height) throws DOMException {
		return;
	}

    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getWidth() {
		return null;
	}
    public void setWidth(int width)	throws DOMException {
		return;
	}

}

