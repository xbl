/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

/**
 * Interface for all elements with top, bottom, left and right. These methods will
 * calculate the percentage and absolute values correctly.
 */
public interface LayoutCalc {
	int calcTop();
	int calcBottom();
	int calcRight();
	int calcLeft();
	
	/**
	 * Add region to this region. This will cause the drawingarea to become visible.
	 */
	public void addRegion(SMILRegionElementImpl region);

	/**
	 * Remove region from this region. This may cause the drawingarea to become invisible,
	 * if this was the last region, and no medias either in the region.
	 */
	public void removeRegion(SMILRegionElementImpl region);
	
	/**
	 * Get the drawing area for this region.
	 */
	public DrawingArea getDrawingArea();
	
}