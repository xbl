/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

import javax.media.Manager;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.smil20.SMILDocument;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;

/**
 * This is a standalone application, SMIL Viewer, showing how to display SMIL
 * presenations. This class shows a deeper approach compared to the SMILApp
 * class, thus giving also more flexibility.
 * <p>
 * To run this viewer as a standalone application, classpath should include
 * xerces.jar and xml-apis.jar
 */

public class SMILViewer extends WindowAdapter implements Viewer, ActionListener {

	// XML file document path, without the filename
	private static String docPath = null;

	// Currently open filename
	private String filename = null;

	// true if JMF is available
	private boolean jmfAvailable = false;

	// Main frame (root-layout) for this Viewer
	private JFrame frame;

	// SMIL presenation document
	private SMILDocument smilDoc = null;

	// Status text textfield
	JTextField statusText = null;

	// Combo box for time points
	JComboBox timePoints = null;

	// Combo box selected element for start
	private String selectedElement = "body";

	// Root-layout container - this will hold the SMIL presentation
	private Container rootlayoutContainer = null;

	// FileChooser
	JFileChooser filechooser = null;
	SmilFileFilter filter = null;

	// URL field
	JTextField urlField = null;

	// Performance tracking/debugging...
	public static long initTime, initMem;

	/**
	 * Main method
	 */
	public static void main(String[] args) {

		//		// !!!!PERFORMANCE TRACKING!!!!
		System.gc();
		initTime = System.currentTimeMillis();
		initMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println(
			"SWING INIT TIME: " + SMILViewer.initTime + "ms MEMORY: " + (double) SMILViewer.initMem / 1000 + "k");
		//		// !!!!PERFORMANCE TRACKING!!!!

		// Craete the player and view the GUI
		SMILViewer v = new SMILViewer();
		v.createGUI();

		// Doc given as arg?
		if (args.length > 0)
			v.open(args[0]);

		//		// !!!!PERFORMANCE TRACKING!!!!
		System.out.println(
			"PREFETCH INIT TIME: +"
				+ ((System.currentTimeMillis() - SMILViewer.initTime))
				+ "ms MEMORY: +"
				+ ((double) (Runtime.getRuntime().totalMemory()
					- Runtime.getRuntime().freeMemory()
					- SMILViewer.initMem))
					/ 1000
				+ "k");
		System.gc();
		System.out.println(
			"PREFETCH GC TIME: +"
				+ ((System.currentTimeMillis() - SMILViewer.initTime))
				+ "ms MEMORY: +"
				+ ((double) (Runtime.getRuntime().totalMemory()
					- Runtime.getRuntime().freeMemory()
					- SMILViewer.initMem))
					/ 1000
				+ "k");
		//		// !!!!PERFORMANCE TRACKING!!!!

	}

	/**
	 * Open args document.
	 */
	private void open(String s) {
		Log.debug("Opening " + s);

		// Initialize, and if failed, just exit
		Reader reader = null;
		URL u = null;
		InputStreamReader in = null;
		try {
			u = new URL(s);
			in = new InputStreamReader(u.openStream());
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL: " + filename);
			displayStatusText("Malformed URL: " + filename);
			return;
		} catch (IOException e) {
			System.out.println("Error reading URL: " + filename);
			displayStatusText("Error reading URL: " + filename);
			return;
		}

		if (init(in, s) == false)
			return;

		((SMILDocumentImpl) smilDoc).start();
		displayStatusText(filename + " started.");
	}

	/**
	 * Create the GUI for this standalone application.
	 */
	private void createGUI() {
		frame = new JFrame("SMIL Viewer");
		// This will get the window closed event.
		frame.addWindowListener(this);
		frame.getContentPane().setLayout(new BorderLayout());

		// Play controls
		Container pc = new JPanel();
		pc.setLayout(new FlowLayout());
		urlField = new JTextField(20);
		urlField.addActionListener(this);
		pc.add(urlField);
		JButton open = new JButton("Browse");
		open.addActionListener(this);
		pc.add(open);
		JButton start = new JButton("Start");
		start.addActionListener(this);
		pc.add(start);
		//		JButton pause = new JButton("Pause");
		//		pause.addActionListener(this);
		//		pc.add(pause);
		JButton stop = new JButton("Stop");
		stop.addActionListener(this);
		pc.add(stop);
		JButton about = new JButton("About");
		about.addActionListener(this);
		pc.add(about);
		JButton exit = new JButton("Exit");
		exit.addActionListener(this);
		pc.add(exit);
		//		timePoints.addActionListener(this);
		//		pc.add(timePoints);
		frame.getContentPane().add(pc, BorderLayout.NORTH);

		// Play area
		JLayeredPane c = new JLayeredPane();
		frame.getContentPane().add(c, BorderLayout.CENTER);
		frame.setSize(600, 400);
		statusText = new JTextField("No SMIL file selected.");
		frame.getContentPane().add(statusText, BorderLayout.SOUTH);
		frame.show();

		// Default transparent background color
		c.setOpaque(false);
		//scrollPanel.setOpaque(false);
		//scrollPanel.getViewport().setOpaque(false);

		setRootlayoutContainer(c);
	}

	// Dummy constructor
	public SMILViewer() {
		timePoints = new JComboBox();
		timePoints.addItem("body");
		jmfAvailable = isJMFAvailable();
	}

	// Dummy constructor
	public SMILViewer(String fileName) {
		timePoints = new JComboBox();
		timePoints.addItem("body");
		jmfAvailable = isJMFAvailable();
		try {
			init(new FileReader(fileName), fileName);
		} catch (java.io.FileNotFoundException e) {
			System.out.println("NOT FOUND!!!" + fileName);
		}
	}

	// Dummy constructor
	public SMILViewer(URL url) {
		timePoints = new JComboBox();
		timePoints.addItem("body");
		jmfAvailable = isJMFAvailable();
		try {
			init(new InputStreamReader(url.openStream()), url.toString());
		} catch (java.io.IOException e) {
			System.out.println("NOT FOUND!!!" + url);
		}
	}

	/**
	 * Initializes the SMIL presentation for playing.
	 * 
	 * @param reader
	 *                  Reader, where the presentation is loaded from
	 * @param path
	 *                  Path to the file (can be a URL)
	 */
	public boolean init(Reader reader, String path) {

		// If no reader, then exit
		if (reader == null)
			return false;

		// Document URL
		try {
			docPath = path.substring(0, path.lastIndexOf('/'));
		} catch (StringIndexOutOfBoundsException e) {
			try {
				docPath = path.substring(0, path.lastIndexOf('\\'));
			} catch (StringIndexOutOfBoundsException e2) {
			}
		}

		Log.debug("Document Path is " + docPath);

		// Xerces Parser - requires xerces.jar
		// The SMILDocumentImpl and SMILElementImpl and smil20 package must
		// be compiled with this classpath - no Palm parser should be in the
		// classpath
		SMILDocumentImpl smil = null;
		try {
			javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();

			factory.setNamespaceAware(true);

			factory.setAttribute(
				"http://apache.org/xml/properties/dom/document-class-name",
				"fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl");
			javax.xml.parsers.DocumentBuilder parser = factory.newDocumentBuilder();
			Log.debug("Xerces parser:" + parser.getClass() + ". NSpaces:" + parser.isNamespaceAware());
			org.xml.sax.InputSource input = new org.xml.sax.InputSource(reader);
			Document doc = parser.parse(input);
			smil = (SMILDocumentImpl) doc;
		} catch (javax.xml.parsers.ParserConfigurationException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText(filename + ": Error parsing.");
			return false;
		} catch (org.xml.sax.SAXException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText(filename + ": Error parsing.");
			return false;
		} catch (java.io.IOException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText(filename + ": Error parsing.");
			return false;
		}
		// End Xerces Parser

		System.out.println("SMIL PARSED");

		smilDoc = smil;
		smil.setViewer(this);

		// Initialise the document - this is size and color for root-layout
		// etc.
		smil.initialize(false);

		// Validate the browser's container to get the root-layout visible.
		rootlayoutContainer.validate();

		// Optional prefetch.
		smil.prefetch();
		return true;
	}

	/**
	 * Start the presentation.
	 */
	public void start() {
		if (smilDoc != null)
			 ((SMILDocumentImpl) smilDoc).start();
	}

	/**
	 * Stop the presentation.
	 */
	public void stop() {
		if (smilDoc != null)
			 ((SMILDocumentImpl) smilDoc).stop();
	}

	/**
	 * Get the SMILDocument. Called from other Swing handlers.
	 * 
	 * @return Currently opened SMIL document.
	 */
	public SMILDocument getSMILDoc() {
		return smilDoc;
	}

	public void setDocumentBaseURI(String base) {
	}

	/**
	 * Return the base URL of the document.
	 */
	public URL getBaseURL() {
		// Overridden base url
		//if (baseURI != null) {
		//	return baseURI;
		//}

		try {
			return new URL(docPath);
		} catch (MalformedURLException e) {
			return null;
		}
	}

	/**
	 * Called when an external link is activated.
	 * 
	 * @param url
	 *                  Link destination
	 */
	public void gotoExternalLink(String url) {
		Log.debug("Now we should go to " + url + " but it hasn't been implemented.");
		//    URL u = URLFactory.getInstance().createURL(url);
		//  EventBroker.getInstance().issueXMLDocRequiredEvent(u, true);
	}
	/**
	 * Open external link replacing/opening new target
	 * 
	 * @param url
	 *                  URL to open
	 * @param target
	 *                  target frame/window
	 */
	public void gotoExternalLinkTarget(String url, String target) {
		Log.debug("Now we should go to " + url + " but it hasn't been implemented.");
	}
	/**
	 * Open external link in a new window
	 * 
	 * @param url
	 *                  URL to open
	 */
	public void gotoExternalLinkNewWindow(String url) {
		Log.debug("Now we should go to " + url + " but it hasn't been implemented.");
	}

	/**
	 * Displays a status text
	 */
	public void displayStatusText(String txt) {
		Log.debug("STATUS: " + txt);
		statusText.setText(txt);
	}

	/**
	 * Creates a new MediaHandler. Works as a factory for SMILDocument.
	 */
	public MediaHandler getNewMediaHandler() {
		SwingMediaHandler mh = new SwingMediaHandler(docPath);
		mh.setViewer(this);
		return mh;
	}

	/**
	 * Returns a new BrushHandler for SMIL core logic.
	 */
	public BrushHandler getNewBrushHandler() {
		SwingBrushHandler bh = new SwingBrushHandler();
		bh.setViewer(this);
		return bh;
	}

	/**
	 * Creates a new LinkHandler. Works as a factory for SMILDocument.
	 */
	public LinkHandler getNewLinkHandler() {
		LinkHandler lh = new SwingLinkHandler();
		lh.setViewer(this);
		return lh;
	}

	/**
	 * Returns a new ForeignHandler for SMIL core logic. NOT IMPLEMENTED IN
	 * STANDALONE PLAYER.
	 */
	public MediaHandler getNewForeignHandler(Element e) {
		return null;
	}

	/**
	 * Set the root-layout container. The container will hold the SMIL
	 * presentation.
	 * 
	 * @param rootlayout
	 *                  Container for SMIL presentation
	 */
	public void setRootlayoutContainer(Container rootlayout) {
		rootlayoutContainer = rootlayout;
	}

	/**
	 * Creates a new DrawingArea. Works as a factory for SMILDocument.
	 */
	public DrawingArea getNewDrawingArea(int type, boolean block) {
		if (type == DrawingArea.ROOTLAYOUT) {
			return new SwingDrawingArea(rootlayoutContainer);
		} else
			return new SwingDrawingArea(type);
	}

	/**
	 * Called from the SMILDocument, when a new time point is found during
	 * prefetching.
	 */
	public void addTimePoint(String elementId) {
		if (elementId != null)
			timePoints.addItem(elementId);
	}

	/**
	 * Returns window width for the SMILDocument.
	 */
	public int getWindowWidth() {
		return 700;
	}
	/**
	 * Returns window height for the SMILDocument.
	 */
	public int getWindowHeight() {
		return 500;
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemBitrate() {
		return "14400";
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemCaptions() {
		return "false";
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemLanguage() {
		return "fi";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemOverdubOrCaption() {
		return "caption";
	}

	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemRequired(String prefix) {
		return false;
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public int getSystemScreenWidth() {
		return 700;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public int getSystemScreenHeight() {
		return 500;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public int getSystemScreenDepth() {
		return 16;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemOverdubOrSubtitle() {
		return "overdub";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemAudioDesc() {
		return "on";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemOperatingSystem() {
		return "Linux";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemCPU() {
		return "x386";
	}
	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemComponent(String component) {
		return false;
	}

	/**
	 * Returns true if images should be played.
	 */
	public boolean getPlayImage() {
		return true;
	}

	/**
	 * Returns true if audio should be played.
	 */
	public boolean getPlayAudio() {
		return jmfAvailable;
	}

	/**
	 * Returns true if video should be played.
	 */
	public boolean getPlayVideo() {
		return jmfAvailable;
	}

	/**
	 * Get the title of the presentation. null if no title present.
	 */
	public String getTitle() {
		return null;
	}

	/**
	 * Set the title for the presentation.
	 * 
	 * @param title
	 *                  Title for the presentation
	 */
	public void setTitle(String title) {
		frame.setTitle(title);
	}

	public boolean isHost() {
		return true;
	}

	/**
	 * Check if JMF class is available. At the same time, set the hint...
	 * 
	 * @return true if JMF is available, false otherwise
	 */
	private boolean isJMFAvailable() {
		try {
			Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true));
		} catch (Throwable e) {
			System.out.println("JMF not available! It is recommended to have it installed.");
			System.out.println("Video will not be played.");
			return false;
		}
		return true;
	}

	// Convert file to url
	public URL toURL(File file) throws java.net.MalformedURLException {
		String path = file.getAbsolutePath();
		if (File.separatorChar != '/') {
			path = path.replace(File.separatorChar, '/');
		}
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		if (!path.endsWith("/") && file.isDirectory()) {
			path = path + "/";
		}
		return new URL("file", "", path);
	}

	/**
	 * Control buttons.
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() instanceof JTextField) {
			System.out.println("TEXTFIELD:" + event.getActionCommand());
			filename = event.getActionCommand();

			// Stop the previous presentation
			if (smilDoc != null) {
				((SMILDocumentImpl) smilDoc).freeResources(false);
				smilDoc = null;

				if (rootlayoutContainer != null)
					rootlayoutContainer.removeAll();

			}
			// Open the SMIL url
			displayStatusText(filename + " prefetching.");
			// Initialize, and if failed, just exit
			Reader reader = null;
			URL u = null;
			InputStreamReader in = null;
			try {
				u = new URL(filename);
				in = new InputStreamReader(u.openStream());
			} catch (MalformedURLException e) {
				System.out.println("Malformed URL: " + filename);
				displayStatusText("Malformed URL: " + filename);
				return;
			} catch (IOException e) {
				System.out.println("Error reading URL: " + filename);
				displayStatusText("Error reading URL: " + filename);
				return;
			}

			if (init(in, filename) == false)
				return;
			displayStatusText(filename + " stopped.");

			return;
		}

		if (event.getSource() instanceof JComboBox) {
			selectedElement = (String) ((JComboBox) (event.getSource())).getSelectedItem();
			System.out.println("Button: " + selectedElement);
			return;
		}
		String text = ((JButton) event.getSource()).getText();

		if (text.equals("Browse")) {

			if (filechooser == null) {
				filechooser = new JFileChooser();
				filter = new SmilFileFilter();
				filechooser.setFileFilter(filter);
			}
			int returnVal = filechooser.showOpenDialog(frame);
			File f = filechooser.getSelectedFile();

			// No file selected?
			if (f == null || returnVal != JFileChooser.APPROVE_OPTION)
				return;

			// Stop the previous presentation
			if (smilDoc != null) {
				((SMILDocumentImpl) smilDoc).freeResources(false);
				smilDoc = null;

				if (rootlayoutContainer != null)
					rootlayoutContainer.removeAll();

			}

			// Update URL field
			try {
				String url = toURL(f).toString();
				urlField.setText(url);
			} catch (MalformedURLException e) {
			}

			// Open the SMIL file
			try {
				filename = f.toString();
				displayStatusText(filename + " prefetching.");
				// Initialize, and if failed, just exit
				if (init(new FileReader(f), f.toString()) == false)
					return;
				displayStatusText(f.toString() + " stopped.");
			} catch (java.io.FileNotFoundException e) {
				System.out.println("File not found: " + filename);
				displayStatusText("File not found: " + filename);
			}

		}

		if (text.equals("Stop") && smilDoc != null) {
			((SMILDocumentImpl) smilDoc).stop();
			displayStatusText(filename + " stopped.");
		}
		if (text.equals("Pause") && smilDoc != null) {
			((SMILDocumentImpl) smilDoc).pause();
		}

		if (text.equals("Start") && smilDoc != null) {
			if (selectedElement.equals("body"))
				 ((SMILDocumentImpl) smilDoc).start();
			else
				 ((SMILDocumentImpl) smilDoc).startFromElementName(selectedElement);

			displayStatusText(filename + " playing.");
		}

		if (text.equals("About")) {

			JOptionPane.showMessageDialog(
				frame,
				"X-Smiles SMIL Viewer\nCopyright (C) Helsinki University of Technology. All rights reserved.\nFor details on use and redistribution please refer to the\nLICENSE_XSMILES file included with this player.");
		}

		if (text.equals("Exit")) {
			if (smilDoc != null)
				 ((SMILDocumentImpl) smilDoc).freeResources(false);
			System.exit(0);
		}

	}

	/**
	 * Window closed. Stops the presentation and exits.
	 */
	public void windowClosing(WindowEvent e) {
		if (smilDoc != null)
			 ((SMILDocumentImpl) smilDoc).stop();
		System.exit(0);
	}

	/**
	 * Simple FileFilter for smil files and directories.
	 */
	private class SmilFileFilter extends FileFilter {
		public boolean accept(File f) {
			if (f.isDirectory())
				return true;
			if (f.toString().endsWith("smil") || f.toString().endsWith("smi"))
				return true;
			return false;
		}
		public String getDescription() {
			return "SMIL files";
		}
	}
	// TODO: should this return the decorator from the SMIL MLFC?
	public Decorator getDecorator()
	{
	    return null;
	}
	
}
