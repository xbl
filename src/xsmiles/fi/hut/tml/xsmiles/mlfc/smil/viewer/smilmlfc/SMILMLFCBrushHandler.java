/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingBrushHandler;

/**
 *  Implements brush for SMILMLFC. Inherited from Swing version.
 */
public class SMILMLFCBrushHandler extends SwingBrushHandler implements BrushHandler {
	

}

