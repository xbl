/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.smil20.*;


// Element implementation - either real one or palm hack
//import org.apache.xerces.dom.ElementNSImpl;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
/**
 *  The <code>SMILElement</code> is the base for all SMIL element 
 * types. It follows the model of the <code>HTMLElement</code> in the HTML 
 * DOM, extending the base <code>Element</code> class to denote SMIL-specific 
 * elements. 
 * <p> Note that the <code>SMILElement</code> interface overlaps with the 
 * <code>HTMLElement</code> interface. In practice, an integrated document 
 * profile that include HTML and SMIL modules will effectively implement both 
 * interfaces (see also the DOM documentation discussion of  Inheritance vs 
 * Flattened Views of the API ).  // etc. This needs attention
 *
 */
public class SMILElementImpl extends VisualElementImpl implements SMILElement {

	// SMILDocumentImpl - to get Viewer and DocLayout etc.
	protected SMILDocumentImpl smilDoc = null;
	
	// XSmilesDocumentImpl - to create new elements
	DocumentImpl ownerDoc = null;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public SMILElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String nameSpace, String name) {
	    super(owner, nameSpace, name);
		smilDoc = smil;
		ownerDoc = owner;
	}
	
	/**
	 * Get the value of smilDoc.
	 * @return Value of smilDoc.
	 */	
	protected SMILDocumentImpl getSMILDoc() {
	  return smilDoc;
	}

	/**
	 * Get the value of ownerDoc.
	 * @return Value of ownerDoc.
	 */	
	protected DocumentImpl getOwnerDoc() {
	  return ownerDoc;
	}
	
	/**
	 * Dispatch the event to DOM for this element.
	 * @param type		event type
	 * @param bubble	true if the event should bubble
	 */
	protected void dispatch(String type, boolean bubble) {	 
		// Dispatch the event
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, bubble, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	 * Get the unique id.
	 * @return id
	 * @exception DOMException NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getId() {
	  return getAttribute("id");
	}
	
	/**
	 * Set the value of id.
	 * @param id  Value to assign to id.
	 */
	public void setId(String id) throws DOMException {
	  throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL id attribute cannot be modified.");
//	  if (id != null && id.length() > 0) {
//	    setAttribute("id", id);
//	  } else {
//	    removeAttribute("id");
//	  }
//	  getOwnerDoc().setChanged();
	}


	/**
	 *  The class attribute. This method should be called getClass, but it is 
	 * already reserved by java.lang.Object.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getClassName() {
		return getAttribute("class");
	}
	
	public void setClassName(String cl) throws DOMException {
		if (cl != null && cl.length() > 0) {
		  setAttribute("class", cl);
		} else {
		  removeAttribute("class");
		}
	}

	/**
	 *  The title attribute.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getTitle() {
		return getAttribute("title");
	}
	
	public void setTitle(String title) throws DOMException {
		if (title != null && title.length() > 0) {
		  setAttribute("title", title);
		} else {
		  removeAttribute("title");
		}
	}

	/**
	 *  The alt text attribute.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getAlt() {
		return getAttribute("alt"); 
	}
	public void setAlt(String alt) throws DOMException {
		if (alt != null && alt.length() > 0) {
		  setAttribute("alt", alt);
		} else {
		  removeAttribute("alt");
		}
	}
	

	/**
	 *  The longdesc attribute.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getLongdesc() {
		return getAttribute("longdesc"); 
	}
	public void setLongdesc(String longdesc) throws DOMException {
		if (longdesc != null && longdesc.length() > 0) {
		  setAttribute("longdesc", longdesc);
		} else {
		  removeAttribute("longdesc");
		}
	}

}

