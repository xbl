/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

import java.awt.Container;
import org.w3c.dom.Element;

/**
 *  Interface to drawing area. 
 */
public interface DrawingArea {

	// These are types for DrawingAreas
	public static final int ROOTLAYOUT	= 0;
	public static final int TOPLAYOUT 	= 1;
	public static final int REGION 		= 2;
	public static final int REGIONSCROLL = 3;

//	public void addListener(MediaListener mediaListener);

	public int getLeft();
	public int getTop();
	public int getWidth();
	public int getHeight();
	public void setBounds(int left, int top, int width, int height);

	/**
	 * Set this drawing area visible.
	 * @param v    true=visible, false=invisible
	 */
	public void setVisible(boolean v);

	/**
	 * Set the title of this area (used for top-layout)
	 * @param name		Title name of the area
	 */
	public void setTitle(String name);
	 
	/**
	 * Add a region to this drawing area. A region can be added to root-layout,
	 * topLayout, or another region.
	 * @param region		Region drawing area to be added.
	 */
//	 public void addRegion(DrawingArea region);

	/**
	 * Add a region to this drawing area. A region can be added to root-layout,
	 * topLayout, or another region.
	 * @param region		Region drawing area to be added.
	 */
	public void addRegion(DrawingArea region, int z);

	/**
	 * Bring this region into front of other regions
	 * @param drawingArea		Region to be the top most
	 */
	public void bringToFront(DrawingArea drawingArea);

	/**
	 * Set the background color for this drawing area.
	 */
	public void setBackgroundColor(String color);
	
	/**
	 * For toplayouts (frames), add a close listener.
	 * @param FrameListener		listener for closing events.
	 */
	public void addFrameListener(FrameListener frameListener);
        
    	public	Container getContentContainer();
        
        public boolean setCSSStretch(String awidth,String aheight, Element origElem);

	
}

