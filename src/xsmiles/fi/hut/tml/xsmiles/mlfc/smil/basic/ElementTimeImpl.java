/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  This interface defines the set of timing attributes that are common to all 
 * timed elements. 
 */
public class ElementTimeImpl extends ElementBasicTimeImpl implements ElementTime {

	public ElementTimeImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}

//	/**
//	 * Just pass through to ElementBasicTimeImpl
//	 */
//	public void startup() {
//		super.startup();
//	}
//	/**
//	 * Just pass through to ElementBasicTimeImpl
//	 */
//	public void activate() {
//		super.activate();
//	}
//	/**
//	 * Just pass through to ElementBasicTimeImpl
//	 */
//	public void deactivate() {
//		super.deactivate();
//	}
//
//	/**
//	 * Just pass through to ElementBasicTimeImpl
//	 */
//	public void closedown() {
//		super.closedown();
//	}

    /**
     *  SyncBehavior 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public short getSyncBehavior() {
		return 0;
	}
    public void setSyncBehavior(short syncBehaviour) throws DOMException {
		return;
    }

    /**
     *  SyncTolerance - TODO: Correct return type?
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getSyncTolerance() {
		return 0;
    }
    public void setSyncTolerance(short syncTolerance) throws DOMException {
		return;
    }

    /**
     *  SyncBehaviorDefault
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getSyncBehaviorDefault() {
		return 0;
    }
    public void setSyncBehaviorDefault(short syncToleranceDefault) throws DOMException {
		return;
    }

	/**
	 *  SyncToleranceDefault - TODO: Return value?
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getSyncToleranceDefault() {
		return 0;
	}
	public void setSyncToleranceDefault(short syncToleranceDefault) throws DOMException {
		return;
	}

    /**
     *  A code representing the value of the  restart attribute, as defined 
     * above. Default value is <code>RESTART_ALWAYS</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getRestartDefault() {
		return 0;
    }
    public void setRestartDefault(short restartDefault) throws DOMException {
		return;
    }
    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getFillDefault() {
		return FILLDEFAULT_INHERIT;
    }
    public void setFillDefault(short fillDefault) throws DOMException {
		return;
    }


}

