/*
 * TextFormattingMediaHandler.java
 *
 * Created on 31. maaliskuuta 2003, 14:32
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

import java.awt.Component;

/**
 *
 * @author  honkkis
 */ 
public interface TextFormattingMediaHandler extends MediaHandler
{
    	/*****************
	 * @return the component for text formatting purposes (SMIL extension).
	 */
	public Object getComponent();

    
}
