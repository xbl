/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  This interface defines the set of basic timing attributes that are common to all 
 * timed elements. 
 */
public abstract class XElementCustomTestImpl implements XElementCustomTest {

	/**
	 * Constructor - set the owner
	 */
	public XElementCustomTestImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super();
	}

    /**
     *  The value of the customTest attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCustomTest() {
		return null;
    }
    public void setCustomTest(String customTest) throws DOMException {
		return;
    }
}

