/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;

/**
 *  This interface defines the set of basic timing attributes that are common to all 
 * timed elements. 
 */
public abstract class XElementSubRegionAttributesImpl implements XElementSubRegionAttributes {
    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFit() {
		return null;
    }
    public void setFit(String fit) throws DOMException {
		return;
    }

    /**
	 * top
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTop() {
		return null;
    }
    public void setTop(String top) throws DOMException {
		return;
    }
	/**
	 * bottom
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBottom() {
		return null;
	}
	public void setBottom(String bottom) throws DOMException {
		return;
	}

	/**
	 * left
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getLeft() {
		return null;
	}
	public void setLeft(String left) throws DOMException {
		return;
	}

	/**
	 * right
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRight() {
		return null;
	}
	public void setRight(String right) throws DOMException {
		return;
	}

	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public int getZIndex() {
		return 0;
	}
	public void setZIndex(int zIndex) throws DOMException {
		return ;
	}
	
	/**
	 * backgroundColor
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBackgroundColor() {
		return null;
	}
	public void setBackgroundColor(String backgroundColor) throws DOMException {
		return;
	}

	/**
	 * regPoint
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegPoint() {
		return null;
	}
	public void setRegPoint(String regPoint) throws DOMException {
		return;
	}

	/**
	 * regAlign
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegAlign() {
		return null;
	}
	public void setRegAlign(String regAlign) throws DOMException {
		return;
	}

}

