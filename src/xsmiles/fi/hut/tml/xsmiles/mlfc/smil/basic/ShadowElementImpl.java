/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.*;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.Log;
import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;

import java.util.Vector;
import java.util.Enumeration;

// These break the portability principle
import java.awt.Component;
import java.awt.Dimension;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCMediaHandler;
//import javax.swing.JButton;
import java.awt.*;

/**
 * ShadowElementImpl is a "shadow" element, which does not live in the DOM.
 * Its task is to allow the use of SMIL media attributes on a foreign element, e.g. XForms:
 * <xfm:button begin="0s" dur="3s" region="test"/>.<br/>
 * A shadow element is created for each foreign element implementing VisualComponentService
 * under any timed element (ElementBasicTimeImpl).
 * It grabs the Component from VisualComponentService and displays it in the correct
 * region at the correct time.
 * ShadowElementImpls are automatically shown to other SMIL elements in their TimedChildLists.
 * Thus, the other elements treat this element as usual SMILMediaElementImpl.
 * <br/>Note that foreign elements, which are added during run-time, will not
 * currently get a shadow created. The top most element inserted to the live DOM tree
 * must be a SMIL element. Foreign elements under it will get a shadow during run-time.
 */
public class ShadowElementImpl extends SMILMediaElementImpl {

	// The real DOM element
	Element realElement = null;

	/**
	 * Overrides get parent, so that shadow element's actual parent is realElement's parent.
	 * Thus, we fake that this Shadow is part of the DOM tree, while it isn't.
	 */
	public Node getParentNode()  {
		return realElement.getParentNode();
	}

//	MediaHandler media = null;
//	LinkHandler link = null;
//	String src, alt;
	
	// true if media initialized. Means that original attributes have been set to DOM.
//	boolean elementInitialized = false;

//	private String namespace = null;
//	private SMILRegionElementImpl region = null;
	
	/**
	 * If this media element is under a 'a' element, then this variable
	 * will contain a reference to the 'a' element.
	 */
//	private SMILAElementImpl linkElement = null;
	
	/**
	 * Mouse events.
	 */
//	public void mouseClicked() {
//		dispatch("click", true);	
//		dispatch("activateEvent", false);	
//	}
//	 
//	public void mouseEntered() {
//		dispatch("mouseover", true);	
//		dispatch("inBoundsEvent", false);	
//	}
//
//	public void mouseExited() {
//		dispatch("mouseout", true);	
//		dispatch("outOfBoundsEvent", false);	
//	}
//	
//	public void mousePressed() {
//		dispatch("mousedown", true);	
//	}
//		
//	public void mouseReleased() {
//		dispatch("mouseup", true);	
//	}
//	
	/**********************************************************************
	 * VisualComponentService Interface - to use SMIL as parasite language.
	 */

	private boolean visualComponentVisible = false;
	private boolean parasiteElement = false;
	private Component paraComponent = null;
	/**
	 * Return the visual component for this extension element
	 */
	public Component getComponent() {
		Log.debug("SMIL: getComponent()");
		// If already fetched and started, just return the component
		if (paraComponent != null)
			return paraComponent;
			
		parasiteElement = true;
		if (media == null) {
			initMedia();
//			media.setViewer(getSMILDoc().getViewer());
			media.prefetch();
			// Start scheduler. It can be started several times, which is ok.
			getSMILDoc().getScheduler().start();
		}
		// This method is called only in X-Smiles!
                /*
		if (media != null && media instanceof SMILMLFCMediaHandler) {
			Component comp = (Component)((SMILMLFCMediaHandler)media).getComponent();
                 */
            	if (media!=null)
                {
                    Component comp = (Component)(media).getComponent();
                    if (comp!=null) {
    //			media.play();
                            Log.debug("Got component!");
    //			media.setTop(0);
    //			media.setLeft(0);
    //			media.setRight((int)(media.getOriginalWidth()*zoom));
    //			media.setBottom((int)(media.getOriginalHeight()*zoom));
                            media.setBounds(0, 0, (int)(media.getOriginalWidth()*zoom), (int)(media.getOriginalHeight()*zoom));
                            comp.setSize(new Dimension((int)((double)media.getOriginalWidth()*zoom),
                                                                    (int)((double)media.getOriginalHeight()*zoom)));

                            // Do the timing!
                            startup();
                            paraComponent = comp;
                            return comp;
                    }
                }
		return null;
	}
	/**
	 * Returns the approximate size of this extension element
	 */
	public Dimension getSize() {
		Log.debug("GET SIZE!"+media.getOriginalWidth()+"-"+media.getOriginalHeight());
		if (media != null)
			return new Dimension(media.getOriginalWidth(), media.getOriginalHeight());
		return new Dimension(10,10);
	}
	double zoom = 1.0;
	public void setZoom(double zoom) {
		if (media != null) {
			paraComponent.setSize(new Dimension((int)((double)media.getOriginalWidth()*zoom),
								(int)((double)media.getOriginalHeight()*zoom)));
//			media.setRight((int)(media.getOriginalWidth()*zoom));
//			media.setBottom((int)(media.getOriginalHeight()*zoom));
			media.setBounds(0, 0, (int)(media.getOriginalWidth()*zoom), (int)(media.getOriginalHeight()*zoom));
		}
		return;
	}
	public void setVisible(boolean visible) {
		Log.debug("SET VISIBLE!"+visible);
		visualComponentVisible = visible;
		if (paraComponent != null)
			paraComponent.setVisible(visible);
		return;
	}
	public boolean getVisible() {
		return visualComponentVisible; 
	}

	/**
	 * Constructor - set the owner
	 * @param real		The real foreign element in the DOM
	 */
	public ShadowElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag,
								Element real) {
	    super(owner, smil, ns, tag);

		elementInitialized = false;
		// Save the real element
		realElement = real;
		
		// Save namespace for setAttribute()
		namespace = ns;
	}
	
	public void setAttribute(String name, String value) {
		// Use the element's namespace (correct way to do this?)
		setAttributeNS(namespace, name, value);
	}
		
	public void setAttributeNS(String ns, String name, String value) {
		this.setAttributeValue(name,value);
		// Set the attribute
		super.setAttributeNS(ns, name, value);
	}
	public Attr setAttributeNode(Attr newAttr) throws DOMException {
	  setAttributeValue(newAttr.getName(), newAttr.getValue());
	  return super.setAttributeNode(newAttr);
	}
	
	protected void setAttributeValue(String name, String value) {
		// If region is changed (or set for the first time), then register
		// this media to the new one. This media will get refreshed if region changes.
		// Only do this if the elements have been initialized (i.e. the DOM exists)
		if (name.equals("region") && elementInitialized == true) {
			// Remove from the old region
			SMILRegionElementImpl region = (SMILRegionElementImpl)getRegionElement();
			if (region != null)
				region.removeMedia(media);

			
			// Add to the new region
			region = (SMILRegionElementImpl)getRegionElement();
			if (region != null)
				region.addMedia(media);
		} else if (name.equals("src")) {
			src = value;
			if (media != null) {
				media.setURL(src);
				// Refit the media
				SMILRegionElementImpl region = (SMILRegionElementImpl)getRegionElement();
				region.removeMedia(media);				
				region.addMedia(media);				
			}
		}
		if (name.equals("alt")) {
			alt = value;
			if (media != null)
				media.setAlt(value);
		}
	}
	
	
	public void removeAttribute(String name) {
	  super.removeAttribute(name);
	}
	
	/***
	 * Creates a foreign handler instead of media handler.
	 */
	public void initMedia() {
		media = getSMILDoc().getViewer().getNewForeignHandler(realElement);
		media.setURL(src);
		media.setAlt(alt);
		media.setMIMEType(getType());
		media.setViewer(getSMILDoc().getViewer());
	}	
//	
//	public MediaHandler getMedia() {
//		return media;
//	}
//	
//	
//	/**
//	 * Initialize this shadow element.
//	 */
//	public void init() {
//		elementInitialized = true;
//		super.init();
//	}
//	
//	
//	/** 
//	 * PREFETCH - prefetching the media.
//	 * This prefetches the element for playing. It set's the element to
//	 * 'prefetched' state.
//	 * This method calls (XElementBasicTime)super.prefetch().
//	 */
//	public void prefetch() {
//		if (media == null)
//			initMedia();
//
//		media.prefetch();
//
//		// This breaks Device Independency!
//		// CSS FIXTHIS: ADD container - element pair to lookup list
//		// Needed?????????????? CSS??????
//	//	if (media instanceof SMILMLFCMediaHandler)
//	//		fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILCSSFlowLayout.componentFlowMap.put(
//	//					(((SMILMLFCMediaHandler)media).media.getComponent(), this);
//
//		// TODO: This breaks the recursive prefetching, but we assume foreign doesn't
//		// have smil elements under it.
//		//super.prefetch();
//	}
//	
//	/**
//	 * Overridden startup() - this will search for the first parent a element and creates
//	 * corresponding link.
//	 */
//	public void startup() {
//	
//		Node node = getParentNode();
//		while (node != null) {
//			// Have we found the a element?
//			if (node instanceof XSMILAElement) {
//				linkElement = (SMILAElementImpl)node;
//				Log.debug(getId()+" -> link to " + linkElement.getHref());
//			}
//			// Get the next parent
//			node = node.getParentNode();
//		}
//		// Not found - no links needed.
//			
//		super.startup();
//	}
//	
//	/**
//	 * Overridden display() - this will also show the media.
//	 * This will also startup the children (areas or anchors).
//	 */
//	public void display() {
//		if (media == null)
//			initMedia();
//
//		// This is a parasite - only call play!
//		if (parasiteElement == true) {
//			// Listen to the end of media
//			media.addListener(this);
//			media.play();
//			return;
//		}
//		
//		SMILLayoutElement layout = getSMILDoc().getDocLayout();
//		region = (SMILRegionElementImpl)getRegionElement();
//		SMILRootLayoutElementImpl rootLayout = null;
//		if (layout == null) 
//			Log.error("<layout> not found!");
//		else	
//			rootLayout = (SMILRootLayoutElementImpl)layout.getRootLayoutElement();
//		
//		if (region != null && rootLayout != null) {
//			// Media needs to know the rootlayout size to zoom the presentation
//			media.setRootLayoutSize(layout.getRootLayoutWidth(), layout.getRootLayoutHeight());
//
//			// Add media to region
//			region.addMedia(media);
//			Log.debug(getId()+" - MEDIA SHOWN "+getSrc()+" ");
//
//		} else
//			Log.error("No region for "+getId()+" - media not displayed.");
//	
//		// Clip the beginning of the media, if the begin time was negative	
//		if (clipBeginTime != null) {
//			media.setMediaTime(clipBeginTime.getResolvedOffset().intValue());
//			if (clipBeginTime.getResolvedOffset().intValue() != 0)
//				Log.debug(" * skipped to "+clipBeginTime.getResolvedOffset().intValue()/1000+"s");
//		}
//		// Listen to the end of media
//		media.addListener(this);
//		media.play();
//		
//		// Handle links - if this media was under an 'a' element.
//
//		if (linkElement != null) {
//			if (link == null) {
//				link = getSMILDoc().getViewer().getNewLinkHandler();
//				// Set url to point to href
//				link.setURL(linkElement.getHref());
//				link.setAlt(linkElement.getAlt());
//				link.setTitle(linkElement.getTitle());
//				link.setViewer(getSMILDoc().getViewer());
//				link.addListener(linkElement);
//			}
//					
//			if (region != null && rootLayout != null) {
//				// Link needs to know the rootlayout size to zoom the presentation
//				DrawingArea drawingArea = rootLayout.getDrawingArea();
//				link.setRootLayoutSize(layout.getRootLayoutWidth(), layout.getRootLayoutHeight());
//				link.setBounds(0, 0, media.getWidth(), media.getHeight());
//				region.addLink(link);
//
//				Log.debug(getId()+" - LINK SHOWN "+getSrc()+" "+region.calcLeft()+
//						" "+region.calcTop());
//
//			} else
//				Log.error("No region for "+getId()+" - link not displayed.");
//			
//			link.play();
//
//		}
//		
//		super.display();
//	}
//
//	/**
//	 * Overridden. This is called when simple duration for this element is ended.
//	 * This restarts the media.
//	 */
//	public void repeat(long time) {
//		Log.debug(getId()+" media repeat("+time+")");
//		// Restart the media.
//		if (media != null) {
//			// Stop playing media & rewind
//			media.stop();
//			// Stop removes listeners
//			media.addListener(this);
//			// Play media
//			media.play();
//		}
//		if (link != null) {
//			// Media stopped/played - it will be the top most component
//			// This will bring the link to top most.
//			link.stop();
//			link.play();
//		}
//				
//		super.repeat(time);
//	}
//	
//	public void freeze() {
//		Log.debug(getId()+" - MEDIA FROZEN "+getSrc());
//		if (media != null)
//			media.freeze();
//		// Handle links, if this media was under an 'a' element.
//		if (link != null) {
//			link.freeze();
//			Log.debug(getId()+" - LINK FROZEN "+getSrc());
//		}
//		// Freeze all children as well
//		super.freeze();
//	}
//
//	/**
//	 * Overridden remove() - this will also remove the media. End of Active Duration.
//	 */
//	public void remove() {
//	//	Log.debug(getId()+" - MEDIA REMOVE "+getSrc());
//
//		if (media != null)  {
//			media.stop();
//			if (region != null)
//				region.removeMedia(media);
//		}	
//		// Handle links, if this media was under an 'a' element.
//		if (link != null) {
//			link.stop();
//			Log.debug(getId()+" - LINK REMOVE "+getSrc());
//		}
//
//		super.remove();
//	}
//
//	/**
//	 * This method will destroy the element, freeing all its memory.
//	 */
//	public void destroy() {
//		if (media != null) {
//			media.close();
//			media = null;
//		}
//
//		if (link != null) {
//			link.close();
//			link = null;
//		}
//
//		super.destroy();
//	}
//
//
//	/**
//	 * Callback method - called when the media has ended.
//	 * This may cause the media to end its simple duration, if dur = "media" or dur = unspecified.
//	 * The reason is that Simple Duration is first unresolved, and now it is resolved.
//	 */
//	public void mediaEnded() {
//	//	Log.debug(getId()+" mediaEnded()");
//		
//		// Get values
//		Time duri = getDur();
//		String end = getEnd();
//		Time repeatDur = getRepeatDur();
//		String repeatCount = getRepeatCount();
//		String dur = getAttribute("dur");
//		
//		// repeatCount exception for continuous media - see calculation of IntermediateActiveDuration()
//		if (isStatic() == false && manualRepeat > 0) {
//			// Loop forever
//			if (repeatCount.equals("indefinite")) {
//				repeat(System.currentTimeMillis() - activateTime);
//				return;
//			}
//			// Decrement repeat counts, and restart Simple Duration (repeat media).
//			// TODO: This can currently only count integer values, not fractions of counts.
//			manualRepeat--;
//			if (manualRepeat > 0) {
//				repeat(System.currentTimeMillis() - activateTime);
//				return;
//			}
//
//			manualRepeat = 0;
//		}
//		
//		// Simple Duration is "resolved" - see the Simple Duration Table
//		// If dur, repeatDur, repeatCount are unspecified and end is specified, 
//		// then Simple Duration = "indefinite"
//		if ((dur == null || dur.length() == 0) && repeatDur == null && repeatCount == null && 
//				end != null)
//			return;
//
//		// repeatDur exception for media - see calculation of IntermediateActiveDuration()
//		// This is the case for Simple Duration = unresolved and repeatDur specified
//		// Active Duration will end this media, therefore repeat forever here.
//		if ((dur == null || dur.length() == 0) && repeatDur != null) {
//			// Continuous media will repeat
//			if (isStatic() == false) {
//				repeat(System.currentTimeMillis() - activateTime);
//			}
//			// Static media will only sit there until Active Duration ends
//			return;
//		}
//
//		// If dur is "media" or not specified -> end of Simple Duration
//		if (dur == null || dur.length() == 0 || dur.equals("media")) {
//
//			immediateIntervalEnd(null);
//			Log.debug(getId()+" - MEDIA ENDED-deactivate() "+getSrc()+" activeDuration:"+accActiveDuration+" waittime:"+beginTime);
//			return;
//
//		}
//		
//		// Other dur specification don't do anything
//	}
//	
//	public void mediaPrefetched() {
//	}
//	
//	/**
//	 * Checks if media is static or continuous (video/audio).
//	 * @return		true if media is static.
//	 */
//	public boolean isStatic() {
//		return media.isStatic();
//	}
//	
//	/**
//	 * This is called from the child to tell that it has ended.
//	 * This media element doesn't care about it.
//	 * @param childDuration		Duration of the child element
//	 */
//	public void childEnded(long childDuration) {
//		return;
//	}
//
    public SMILRegionElement getRegionElement() {
		SMILLayoutElement layout = getSMILDoc().getDocLayout();
		String regStr = realElement.getAttribute("region");
		if (layout != null) {
			return layout.getRegionElement(regStr, realElement);
		}

		return null;	
    }
//    public void setRegion(SMILRegionElement region) {
//		if (region == null)
//			removeAttribute("region");
//		else
//			setAttribute("region", region.getId());
//    }
//    public void setRegion(String region) {
//    	setAttribute("region", region);
//    }

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFill() {
 	   return realElement.getAttribute("fill");
    }
//    public void setFill(String fill) throws DOMException {
//		if (fill == null)
//			removeAttribute("fill");
//		else
//			setAttribute("fill", fill);
//		return;
//    }

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr() {
		return realElement.getAttribute("abstract");
    }
//    public void setAbstractAttr(String abstractAttr) throws DOMException {
//	    if (abstractAttr != null && abstractAttr.length() > 0) {
//	      setAttribute("abstract", abstractAttr);
//	    } else {
//	      removeAttribute("abstract");
//	    }
//    }

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor() {
		return realElement.getAttribute("author");
    }
//    public void setAuthor(String author) throws DOMException {
//	    if (author != null && author.length() > 0) {
//	      setAttribute("author", author);
//	    } else {
//	      removeAttribute("author");
//	    }
//    }

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright() {
		return realElement.getAttribute("copyright");
    }
//    public void setCopyright(String copyright) throws DOMException {
//	    if (copyright != null && copyright.length() > 0) {
//	      setAttribute("copyright", copyright);
//	    } else {
//	      removeAttribute("copyright");
//	    }
//    }

    /**
     *  See the  clipBegin attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getClipBegin() {
		return realElement.getAttribute("clipBegin");
    }
//    public void setClipBegin(String clipBegin) throws DOMException {
//	    if (clipBegin != null && clipBegin.length() > 0) {
//	      setAttribute("clipBegin", clipBegin);
//	    } else {
//	      removeAttribute("clipBegin");
//	    }
//    }

    /**
     *  See the  clipEnd attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getClipEnd() {
		return realElement.getAttribute("clipEnd");
    }
//    public void setClipEnd(String clipEnd) throws DOMException {
//	    if (clipEnd != null && clipEnd.length() > 0) {
//	      setAttribute("clipEnd", clipEnd);
//	    } else {
//	      removeAttribute("clipEnd");
//	    }
//    }

    /**
     *  See the  readIndex attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getReadIndex() {
		return realElement.getAttribute("readIndex");
    }
//    public void setReadIndex(String readIndex) throws DOMException {
//	    if (readIndex != null && readIndex.length() > 0) {
//	      setAttribute("readIndex", readIndex);
//	    } else {
//	      removeAttribute("readIndex");
//	    }
//    }

    /**
     *  See the  src attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSrc() {
		return realElement.getAttribute("src");
    }
//    public void setSrc(String src) throws DOMException {
//	    if (src != null && src.length() > 0) {
//	      setAttribute("src", src);
//	    } else {
//	      removeAttribute("src");
//	    }
//    }

    /**
     *  See the  type attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getType() {
		String type = realElement.getAttribute("type");
		if (type.length() == 0)
			type = null;
			
		return type;	
    }
//    public void setType(String type) throws DOMException {
//	    if (type != null && type.length() > 0) {
//	      setAttribute("type", type);
//	    } else {
//	      removeAttribute("type");
//	    }
//    }

    /**
     *  See the erase attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getErase() {
		return 0;
    }
//    public void setErase(short erase) throws DOMException {
//	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
//    }

    /**
     *  See the mediaRepeat attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getMediaRepeat() {
		return 0;
    }
//    public void setMediaRepeat(short mediaRepeat) throws DOMException {
//	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
//    }

    /**
     *  See the tabindex attribute in LinkingAttributes module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getTabindex() {
		return 0;
    }
//    public void setTabindex(int tabindex) throws DOMException {
//	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
//    }

    /**
     *  See the transIn attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransIn() {
		return realElement.getAttribute("transIn");
    }
//    public void setTransIn(String transIn) throws DOMException {
//	    if (transIn != null && transIn.length() > 0) {
//	      setAttribute("transIn", transIn);
//	    } else {
//	      removeAttribute("transIn");
//	    }
//    }

    /**
     *  See the transOut attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransOut() {
		return realElement.getAttribute("transOut");
    }
//    public void setTransOut(String transOut) throws DOMException {
//	    if (transOut != null && transOut.length() > 0) {
//	      setAttribute("transOut", transOut);
//	    } else {
//	      removeAttribute("transOut");
//	    }
//    }

	// Methods overridden from ElementBasicTimeImpl
	
    /**
     *  The desired value (as a list of times) of the  begin instant of this 
     * node. 
	 * If not found, default time 0s is returned.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBegin() {
		String begin = realElement.getAttribute("begin");
		if (begin.length() == 0)
			begin = null;
			
		return begin;
    }
//    public void setBegin(TimeList begin) throws DOMException {
//		return;
//    }

    /**
     *  The list of active  ends for this node. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getEnd() {
		String end = realElement.getAttribute("end");
		if (end.length() == 0)
			end = null;
			
		return end;
    }
//    public void setEnd(TimeList end) throws DOMException {
//		return;
//    }

	
    /**
     * The desired simple  duration value of this node in seconds.
	 * Note that this returns a Time, not a String. Therefore, dur="media" is returned as null.
	 *
     * @return 	Time object, which can have a value, be indefinite or null. Null means unspecified.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */

    public Time getDur() {
//		if (duration != null)
//			return duration;
	
		String dur = realElement.getAttribute("dur");
		if (dur == null || dur.length() == 0)
			return null;
			
		// If this is a time container and dur = "media", unspecify this attr. pg.14
		if (dur.equals("media"))
			return null;

		TimeImpl duration = new TimeImpl(dur);
		return duration;
    }
//    public void setDur(String dur) throws DOMException {
//		setAttribute("dur", dur);
//		return;
//    }

    /**
     *  A code representing the value of the  restart attribute, as defined 
     * above. Default value is <code>RESTART_ALWAYS</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getRestart() {
		String restart = realElement.getAttribute("restart");
		if (restart.equals("never"))
			return RESTART_NEVER;
		else if (restart.equals("whenNotActive"))
			return RESTART_WHEN_NOT_ACTIVE;

		// Default value
	    return RESTART_ALWAYS;
    }
	
//    public void setRestart(short restart) throws DOMException {
//		if (restart == RESTART_ALWAYS)
//			setAttribute("restart","always");
//		else if (restart == RESTART_NEVER)
//			setAttribute("restart","never");
//		else if (restart == RESTART_WHEN_NOT_ACTIVE)
//			setAttribute("restart","whenNotActive");
//		return;
//    }


    /**
     *  The  repeatCount attribute causes the element to play repeatedly 
     * (loop) for the specified number of times. A negative value repeat the 
     * element indefinitely. Default value is 0 (unspecified). 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getRepeatCount() {
		String repeatCount = realElement.getAttribute("repeatCount");
		
		if (repeatCount == null || repeatCount.length() == 0) {
			// A KLUDGE to get some repeat behaviour
			String repeat = getAttribute("repeat");
			if (repeat == null || repeat.length() == 0)
				return null;
			return repeat;
		}
		
		return repeatCount;
    }

//    public void setRepeatCount(String repeatCount) throws DOMException {
//		setAttribute("repeatCount", repeatCount);
//		return;
//    }

    /**
     *  The  repeatDur causes the element to play repeatedly (loop) for the 
     * specified duration in seconds. 
     * @return 	Time object, which can have a value, be indefinite or null. Null means unspecified.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public Time getRepeatDur() {
	    String repeatDur = realElement.getAttribute("repeatDur");
	    if (repeatDur == null || repeatDur.length() == 0)
	    	return null;
	    	
	    return new TimeImpl(repeatDur);
    }
//    public void setRepeatDur(MyFloat repeatDur) throws DOMException {
//		return;
//    }


	/**
	 *  
	 *  The  repeat causes the element to play repeatedly (loop) for the 
	 * specified duration in milliseconds. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public MyFloat getRepeat() {
		return zero;
	}
//	public void setRepeat(MyFloat repeatDur) throws DOMException {
//		return;
//	}

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMin() {
		Time minTime = null;
		String min = realElement.getAttribute("min");
		if (min == null || min.length() == 0)
			min = null;
		else
		// indefinite is not an allowed value
			if (min.equals("indefinite") == true) {
				min = null;
			}
		// "media" must be handled somehow... it is invalid for time container
			
		// Default value is zero
		if (min == null)
			min = "0s";

		// If min was not understood or <= 0, then use default
		minTime = new TimeImpl(min);
		if (minTime.getResolved() == false) // FLOAT BUG || (minTime.getResolved() == true && minTime.getResolvedOffset() <= 0))
			minTime = new TimeImpl("0s");

		// Min on joku outo s��nt�...

		return minTime;
	}
	
//	public void setMin(Time min) throws DOMException {
//		setAttribute("min", String.valueOf((float)min.getResolvedOffset().intValue()/1000));
//		return;
//	}

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMax() {
		Time maxTime = null;
		String max  = realElement.getAttribute("max");
		if (max == null || max.length() == 0)
			max = null;

		// "media" must be handled somehow... it is invalid for time container

		// Default value is indefinite
		if (max == null)
			max = "indefinite";

		// If max was not understood or <= 0, then use default
		maxTime = new TimeImpl(max);
		if (maxTime.getResolved() == false || (maxTime.getResolved() == true && maxTime.getResolvedOffset().intValue() <= 0))
			maxTime = new TimeImpl("indefinite");

		return maxTime;
	}
//	public void setMax(Time max) throws DOMException {
//		setAttribute("max", String.valueOf((float)max.getResolvedOffset().intValue()/1000));
//		return;
//	}

}

