/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/** 
 * // audio, video, ... 
 */
public class SMILRefElementImpl extends SMILMediaElementImpl implements SMILRefElement {

	/**
	 * Constructor - set the owner
	 */
	public SMILRefElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

}

