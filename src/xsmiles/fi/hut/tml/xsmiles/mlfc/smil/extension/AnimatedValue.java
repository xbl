/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import java.util.Vector;

/**
 * Abstract Animated Value. Types to be animated must implement this.
 */
public interface AnimatedValue {

	/**
	 * Parse this string into value. Throws NumberFormatException, if cannot parse.
	 * @param str	String to be parsed
	 * @return 		Parsed AnimatedValue
	 */
	//static public AnimatedValue parse(String str);

	public String toString();
	
	public void clampValue();
	
	/**
	 * Add val to this.
	 * @param val	Val to be added to this.
	 * @return		Returns val+this.
	 */
	public AnimatedValue add(AnimatedValue val);

	/**
	 * Multiply this by integer value.
	 * @param integer	Integer value
	 * @return			Returns this * integer.
	 */
	public AnimatedValue mult(int integer);
	/**
	 * Interpolate between this and val by t, where t is [0,1]. For integers: (val-this)*t.
	 * @param val	AnimatedValue
	 * @return		Returns this * integer.
	 */
	public AnimatedValue interpolate(AnimatedValue val, float t);

	/**
	 * Calculates the distance between this and val. (for one dimension: abs(this-val) ) 
	 * @param val	AnimatedValue
	 * @return		Returns distance between this nad val..
	 */
	public float distance(AnimatedValue val);
	
}
