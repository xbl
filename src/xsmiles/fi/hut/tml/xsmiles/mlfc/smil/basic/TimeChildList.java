/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import java.util.Enumeration;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;


/**
 * This class is enumeration to get all timed children for an element.
 */
public class TimeChildList implements Enumeration {
	
	Element element = null;
	NodeList children = null;
	
	// index will always point to the next timed element, not the current element
	int index = 0;

	// Required to evaluate system values
	Viewer viewer = null;

	/**
	 * Initialize the class and make index to point to the first timed child
	 * @param	e		Element's children to iterate
	 * @param	v		Viewer for evaluating system values
	 */
	public TimeChildList(Element e, Viewer v) {
		element = e;
		children = e.getChildNodes();
		viewer = v;
		nextTimedChild();
	}

	/**
	 * Searches the next timed child.
	 * Skip all non-nodes, non-ElementBasicTime nodes and nodes, which have false system attributes.
	 * And except for VisualComponentServices with true system attributes.
	 * All timed children must be tested (Timing+Test)
	 */
	private void nextTimedChild() {
		while (index < children.getLength()) {
			// Check that the element is ElementBasicTimeImpl and has true systemAttrs
			if (children.item(index) instanceof ElementBasicTimeImpl)
				if (AttributeHandler.evaluateSystemValues((Element)(children.item(index)), viewer, true) == true)
					break;
			// Check that the element is VisualComponentService and has true systemAttrs
			if (children.item(index) instanceof VisualComponentService)
				if (AttributeHandler.evaluateSystemValues((Element)(children.item(index)), viewer, true) == true)
					break;
			if (children.item(index) instanceof CompoundService)
				if (AttributeHandler.evaluateSystemValues((Element)(children.item(index)), viewer, true) == true)
					break;
			index++;
		}
	}

	/**
	 * Returns true if there are still timed elements.
	 */
	public boolean hasMoreElements() {
		if (index < children.getLength())
			return true;
		
		return false;	
	}

	/**
	 * Returns the next timed child, or null if no more timed children.
	 */
	public Object nextElement() {
		// If no more elements, then return null
		if (!hasMoreElements())
			return null;
	
		// Get the next element - BasicTimeImpl or VisualComponent
		Object o = children.item(index);
		// If VisualComponent, then get the shadow for it
		if ((o instanceof VisualComponentService || o instanceof CompoundService) && !(o instanceof SMILElementImpl))
			o = ((ElementTimeControlImpl)element).getShadowElement(o);
		
		index++;
		nextTimedChild();		
		return o;
	}
}