/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import java.util.Vector;

/**
 * Animated Pair Value, animates values in format "(x, y)"
 */
public class AnimatedPairValue implements AnimatedValue {

	float x = 0;
	float y = 0;

	public AnimatedPairValue(float xx, float yy) {
		x = xx;
		y = yy;
	}

	public AnimatedPairValue(String str) {
		if (str == null)
			throw new NumberFormatException("Parse error: null");

		String xStr, yStr;
		str = str.trim();
		
		// Remove possible "(" and ")"
		if (str.charAt(0) == '(' &&
			str.charAt(str.length()-1) == ')')
				str = str.substring(1, str.length()-1);

		int comma = str.indexOf(',');

		xStr = str.substring(0, comma);
		yStr = str.substring(comma+1, str.length());
		xStr = xStr.trim();
		yStr = yStr.trim();
		
		// Parse and indicate that it was a number
		x = Integer.parseInt(xStr);
		y = Integer.parseInt(yStr);
	}

	/**
	 * Parse this string into value. Throws NumberFormatException, if cannot parse.
	 * @param str	String to be parsed
	 * @return 		Parsed AnimatedPairValue
	 */
	static public AnimatedPairValue parse(String str) {
		// Parse and indicate that it was a number
		return new AnimatedPairValue(str);
		
	}

	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}

	public String toString()  {
		return "("+((int)x)+","+((int)y)+")";
	}


	public void clampValue()  {
		// Nothing to do.
	}

	/**
	 * Add val to this.
	 * @param val	Val to be added to this.
	 * @return		Returns val+this.
	 */
	public AnimatedValue add(AnimatedValue val) {
		float xx = x + ((AnimatedPairValue)val).getX();
		float yy = y + ((AnimatedPairValue)val).getY();
		return new AnimatedPairValue(xx, yy);
	}

	/**
	 * Multiply this by integer value.
	 * @param integer	Pair value
	 * @return			Returns this * integer.
	 */
	public AnimatedValue mult(int integer) {
		float xx = x * integer;
		float yy = y * integer;
		return new AnimatedPairValue(xx, yy);
	}
	
	/**
	 * Interpolate between this and val by t, where t is [0,1]. For integers: (val-this)*t.
	 * @param val	AnimatedPairValue
	 * @return		Returns this * integer.
	 */
	public AnimatedValue interpolate(AnimatedValue val, float t) {
		float xx = (x - ((AnimatedPairValue)val).getX()) * t;
		float yy = (y - ((AnimatedPairValue)val).getY()) * t;
		return new AnimatedPairValue(xx, yy);
	}
	

	/**
	 * Calculates the distance between this and val. (for one dimension: abs(this-val) ) 
	 * @param val	AnimatedPairValue
	 * @return		Returns distance between this nad val..
	 */
	public float distance(AnimatedValue val) {
		float xx = Math.abs(x - ((AnimatedPairValue)val).getX());
		float yy = Math.abs(y - ((AnimatedPairValue)val).getY());
		return (float)Math.sqrt(xx*xx + yy*yy);
	}
	
}
