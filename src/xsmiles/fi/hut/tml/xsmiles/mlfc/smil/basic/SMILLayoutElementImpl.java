/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import java.net.URL;
import java.net.MalformedURLException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.XSmilesXMLDocument; // for one static function
import fi.hut.tml.xsmiles.dom.StylableElement;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.mlfc.smil.extension.SMILTopLayoutElementImpl;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.Log;

/**
 *  Declares layout type for the document. See the  LAYOUT element definition .
 *  
 */
public class SMILLayoutElementImpl extends SMILElementImpl implements SMILLayoutElement, LayoutCalc {

	/**
	* Constructor with owner doc
	*/
	public SMILLayoutElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "layout");
		cssBlocks = new Hashtable();
	}
	
	private SMILRootLayoutElementImpl rootLayoutElement = null;
	private SMILRegionElementImpl defaultRegion = null;
	
	// CSS: True is CSS Layout Model is in use
	boolean cssLayout = false;
	
	// CSS: Hashtable of block containers, key: element, value: SMILRegionElementImpl
	Hashtable cssBlocks = null;
	
	/**
	 * Calls init() for the first root-layout, sets the size and sets the view to be visible.
 	 * Also, calls init() for other children.
	 *
	 * One layout can only have one root-layout element
	 * (this uses the first instance). If root-layout element not found, then uses
	 * default root-layout, which is the maximum size of the regions. If no regions,
	 * or all have percentage values, a default root-layout is defined with the window size.
	 * <br/>
	 * If type="text/css", then add the text content to the stylesheet.
	 */
	public void init() {
		NodeList children = getChildNodes();
		Node e;

		// CSS: Check if the type of the layout is "text/css"
		if (this.getAttribute("type").equalsIgnoreCase("text/css")) {
			// No further processing required - no root-layout or regions.
			Log.debug("LAYOUT - CSS LAYOUT TURNED ON with"+this.getText()+"!");
			cssLayout = true;
			smilDoc.setCSSLayoutModel(true);
			
			// This is a kludge to get PIs to work - this should be in the browser core.
			// Search for processing instructions
			// TODO: multiple stylesheets
			Node child = this.getOwnerDocument().getFirstChild();
			while(child!=null)	
			{
				if (child.getNodeType()==Node.PROCESSING_INSTRUCTION_NODE)
				{
					ProcessingInstruction pi = (ProcessingInstruction)child;
					String target = pi.getTarget();
					String data = pi.getData();
					String type = XSmilesXMLDocument.getAttribute(data,"type");
					if (type.equals("text/css"))
					{
						// Check Media Query attribute - only for X-Smiles Viewer
						boolean skip = false;
						String media = XSmilesXMLDocument.getAttribute(data,"media");
						if (media != null && media.length() > 0 && 
									smilDoc.getViewer() instanceof SMILMLFC) {
							// Evaluate media string
							SMILMLFC mlfc = (SMILMLFC)(smilDoc.getViewer());
							if (mlfc.getXMLDocument().evalMediaQuery(media) == false)  {
								Log.debug("Skipping external stylesheet, media query evaluated to false: "+XSmilesXMLDocument.getAttribute(data,"href"));
								skip = true;
							}
						}

						if (skip == false) {
							String href = XSmilesXMLDocument.getAttribute(data,"href");
							Log.debug("SMIL including external stylesheet "+href);
							if (href!=null&&href.length()>0)
							{
								try {
									smilDoc.getStyleSheet().addXMLStyleSheet(
													new URL(smilDoc.getViewer().getBaseURL(), href));
								} catch (MalformedURLException me) {
									Log.error("Stylesheet URL is malformed:"+href);
								}
								break;
							}
						}
					}
				}
				child=child.getNextSibling();
			}

			// After processing instructions, add the style under the layout element.
			smilDoc.getStyleSheet().addXMLStyleSheet(this.getText(), smilDoc.getViewer().getBaseURL());

			// No return - let this fall through and create default rootlayouts etc.
			//return;
		}

		
		// Init rootLayoutElement
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);
			if (e instanceof SMILRootLayoutElement) {
				// Test the Inline systemAttributes
				if (AttributeHandler.evaluateSystemValues((SMILElement)e, getSMILDoc().getViewer(), true) == true) {
					rootLayoutElement = (SMILRootLayoutElementImpl)e;
					rootLayoutElement.initHead();
					break;
				}
			}
		}
		
		// Did root-layout element exist?
		if (rootLayoutElement == null) {
			// Not found
			Log.info("<root-layout> not found - using the largest region size, or the window size.");
			// Create the default root-layout, with dummy namespace
			rootLayoutElement = new SMILRootLayoutElementImpl(getOwnerDoc(), getSMILDoc(), "ns");
			// Get the maximum region sizes (or current window size)
			computeMaxRegionSize();
			rootLayoutElement.setWidth(maxRegionWidth);
			rootLayoutElement.setHeight(maxRegionHeight);
			
			// This init requires root-layout width and height attributes specified
			rootLayoutElement.initHead();
			Log.debug("They are "+maxRegionWidth+"-"+maxRegionHeight);
		}

		// Init other elements (regions) - these require rootLayoutElement != null.
		// Initializes only region with correct systemAttributes
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);
			if (e instanceof SMILElement) {
				// Test the Inline systemAttributes
				if (AttributeHandler.evaluateSystemValues((SMILElement)e, getSMILDoc().getViewer(), true) == true) {
					if (e instanceof SMILRegionElement) {
						((SMILRegionElementImpl)e).initHead();
					} else if (e instanceof SMILTopLayoutElementImpl) {
						((SMILTopLayoutElementImpl)e).initHead();
					} else {
						// Init all SMIL elements with ok systemAttributes	
						((XSmilesElementImpl)e).init();
					}
				}
			} else if (e instanceof XSmilesElementImpl)
				((XSmilesElementImpl)e).init();
		}

		// This element will not be part of the DOM tree, only a default element.
		defaultRegion = new SMILRegionElementImpl(getOwnerDoc(), getSMILDoc(), "ns");
		defaultRegion.setTop("0");
		defaultRegion.setRight("0");
		defaultRegion.setWidth(getRootLayoutWidth());
		defaultRegion.setHeight(getRootLayoutHeight());
		defaultRegion.initHead();

		super.init();

		return;
	}
	
	/**
	 * Return the root-layout element. 
	 *
	 * @return	root-layout element from the DOM or default root-layout element (not in DOM)
	 */
	public SMILRootLayoutElement getRootLayoutElement() {
		return rootLayoutElement;
	}
	
	/**
	 * Return the width of the root-layout or if null, then largest region.
	 */
	public int getRootLayoutWidth() {
		SMILRootLayoutElement rle = getRootLayoutElement();
		if (rle == null) {
			computeMaxRegionSize();
			return maxRegionWidth;
		}
		// Try to parse width. If it doesn't exists / is null, use the largest region.
		try {
			return Integer.parseInt(rle.getWidth());
		} catch (NumberFormatException e) {
			computeMaxRegionSize();
			return maxRegionWidth;
		}
	}
	
	/**
	 * Return the height of the root-layout or if null, then largest region.
	 */
	public int getRootLayoutHeight() {
		SMILRootLayoutElement rle = getRootLayoutElement();
		if (rle == null) {
			computeMaxRegionSize();
			return maxRegionHeight;
		}

		// Try to parse height. If it doesn't exists / is null, use the largest region.
		try {		
			return Integer.parseInt(rle.getHeight());
		} catch (NumberFormatException e) {
			computeMaxRegionSize();
			return maxRegionHeight;
		}
			
	}
	
	//// LayoutCalc interface - uses the values from RootLayout ////
	
	/**
	 * Returns zero as the root-layout top coord.
	 */
	public int calcTop() {
	 	return 0;
	}

	/**
	 * Returns zero as the root-layout left coord.
	 */
	public int calcLeft() {
		return 0;
	}

	/**
	 * Returns the root-layout width.
	 */
	public int calcRight() {
	 	return getRootLayoutWidth();
	}

	/**
	 * Returns the root-layout height.
	 */
	public int calcBottom() {
		return getRootLayoutHeight();
	}

	/**
	 * Add region to this region. This will cause the drawingarea to become visible.
	 */
	public void addRegion(SMILRegionElementImpl region)  {
	}

	/**
	 * Remove region from this region. This may cause the drawingarea to become invisible,
	 * if this was the last region, and no medias either in the region.
	 */
	public void removeRegion(SMILRegionElementImpl region)  {
	}
	
	/**
	 * Get the drawing area for this region.
	 */
	public DrawingArea getDrawingArea()  {
		return rootLayoutElement.getDrawingArea();
	}
	
	private int maxRegionWidth = -1, maxRegionHeight = -1;

	/**
	 * Computes the largest width and height in the regions.
	 */
	private void computeMaxRegionSize() {
		int left, width, top, height;
		NodeList children;
		Node e;
		
		if (maxRegionWidth > 0)
			return;

		maxRegionWidth = 0;
		maxRegionHeight = 0;
		
		// Get the children of the layout
		
		/**
		 * Modified so that root layout size is defined by screen size, if 
		 * nothing else defined.
		 *
		   children = getChildNodes();

		   // Find the largest region!
		   for (int i=0 ; i < children.getLength() ; i++) {
		   e = children.item(i);
		   if (e instanceof SMILRegionElement) {
		   // Does this region have the largest width/height?
		   try {
		   left = Integer.parseInt(((SMILRegionElement)e).getLeft());
		   width = left + Integer.parseInt(((SMILRegionElement)e).getWidth());
		   } catch (NumberFormatException ne) {
		   width = 0;
		   }
		   
		   try {
		   top = Integer.parseInt(((SMILRegionElement)e).getTop());
		   height = top + Integer.parseInt(((SMILRegionElement)e).getHeight());
		   } catch (NumberFormatException ne) {
		   height = 0;
		   }
		   
		   if (maxRegionWidth < width)
		   maxRegionWidth = width;
		   if (maxRegionHeight < height)
		   maxRegionHeight = height;
		   }
		   }
		*/
		
		// Regions not found, or only have percentage values -> get system window size
		if (maxRegionWidth == 0 || maxRegionHeight == 0) {
			maxRegionWidth = getSMILDoc().getViewer().getWindowWidth()-5;
			maxRegionHeight = getSMILDoc().getViewer().getWindowHeight()-5;
		}
		
		Log.debug("Computed max region size: "+maxRegionWidth+" x "+maxRegionHeight);
		// Finished.
		return;
	}
	
	/**
	 * Get a region from the elements under this layout element. If no matching region is
	 * found, returns the default region, which has the size of the roo-layout.
	 * The default region is not part of the DOM tree.
	 * @param name 	The name for the region (regionName or id attr). Can be null, which returns default region.
	 * @param elem	Element, who asks for the region (use to get the CSS values).
	 * @return	Region element matching the name (either regionName or id attr). Or the default region.
	 */
	public SMILRegionElement getRegionElement(String name, Element elem) {
		SMILRegionElement reg = null;

		// CSS: Dynamically create elements	
		if (cssLayout == true) {
			return searchForBlock(elem);
		}
			
		// Go through all regions until the matching one is found.
		// Searches through all children in a depth-first order.
		// Should return a list of regions!! (may have multiple regionNames matching)
		if (name != null && name.length() != 0)  {
			reg = searchBasicRegion(name, this);
			if (reg != null)
				return reg;
		}
		
		// Not found, use a default region with root-layout size, and dummy namespace.
		return defaultRegion;
	}

	private SMILRegionElement searchBasicRegion(String name, Node se)  {
		SMILRegionElement reg = null;
		NodeList children = se.getChildNodes();
		Node e;

		// Go through all regions until the matching one is found.
		// Searches through all children in a depth-first order.
		// Should return a list of regions!! (may have multiple regionNames matching)
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);
			reg = searchBasicRegion(name, e);
			if (reg != null)
				return reg;

			if (e instanceof SMILRegionElement) {
				// If region has a matching regionName && systemAttrs are true
				if (((SMILRegionElement)e).getRegionName().equals(name) &&
					AttributeHandler.evaluateSystemValues((SMILRegionElement)e, getSMILDoc().getViewer(), true) == true)
					return (SMILRegionElement)e;
				// If region has a matching id && systemAttrs are true
				if (((SMILRegionElement)e).getId().equals(name) &&
					AttributeHandler.evaluateSystemValues((SMILRegionElement)e, getSMILDoc().getViewer(), true) == true)
					return (SMILRegionElement)e;
			}
		}
		return null;
	}

	/**
	 * CSS: Takes care of returning the correct container for CSS regions. Searches for
	 * the closest ancestor with a block container. An element has a block container,
	 * if it has position="absolute". When asked for the first time, this method will
	 * create the block container, and save it for further requests. This way, even
	 * foreign elements can act as block containers. <br/> If ancestor with a block container
	 * does not exist, then root-layout container will be returned.
	 *
	 * @param e		The start for the closest ancestor with a block flow will be started at element e.
	 * @return 		The closest block container, where inline elements are added. 
	 *				Can be element e itself.
	 */
	private SMILRegionElementImpl searchForBlock(Element e) {
		SMILRegionElementImpl region = null, parentRegion = null;

		// If the body of the DOM tree was finished 
		if ((e instanceof SMILSMILElementImpl) == true) {
			// Retrieve / create the region for root-layout, Add the new region to the rootlayout
			region = retrieveRegion(rootLayoutElement, null, rootLayoutElement.getDrawingArea(), true);
			return region;
		}

		// If this is an absolute element
		if (getStyle(e, "position").equals("absolute") == true) {
			// Retrieve / create a region for this element, Add the new region to the rootlayout
			region = retrieveRegion(e, null, rootLayoutElement.getDrawingArea(), true);
			return region;
		}

		// If this is a block element
		if (getStyle(e, "display").equals("block") == true) {
			// Use (eli addaa) to ancestor block/absolute/rootlayout region
			parentRegion = searchForBlock((Element)(e.getParentNode()));
			// Retrieve / create a region for this element
			// Add the new region to the parentRegion, display: block is handled (linefeeds)!
			region = retrieveRegion(e, parentRegion, parentRegion.getDrawingArea(), true);
			return region;
		}

		// default: If this is an inline element
//		if (getStyle(e, "display").equals("inline") == true) {
			// Use (eli addaa) to ancestor block/absolute/rootlayout region
			parentRegion = searchForBlock((Element)(e.getParentNode()));
			// In case of par element - this is probably a foreign element container
			// Just add the foreign to the ancestor block container. If we created
			// a new region with retrieve region, we would not know the size of foreigns
			// under par, zero size would be used and nothing would show up.
			if (e instanceof ElementParallelTimeContainerImpl) 
				return parentRegion;
			// Retrieve / create a region for this element
			// Add the new region to the parentRegion, display: inline (no linefeeds)!
			region = retrieveRegion(e, parentRegion, parentRegion.getDrawingArea(), false);
			return region;
//		}
	}

	/**
	 * CSS: Retrieves or creates a region for element e.
	 * @param e			element, that requires a dynamically created region
	 * @param parentReg Parent Region, to set parent regions visible/invisible (null for root-layout)
	 * @param da		DrawingArea, which should contain the created region
	 * @param linefeeds	true if this is a block, false for inline
	 */
	public SMILRegionElementImpl retrieveRegion(Element e, SMILRegionElementImpl parentReg, DrawingArea da, boolean linefeeds) {
		// Retrieve / Create the container for it.
		SMILRegionElementImpl block = (SMILRegionElementImpl)cssBlocks.get(e);
		if (block == null) {
			// This element will not be part of the DOM tree, only a dynamically created region.
			block = new SMILRegionElementImpl(getOwnerDoc(), getSMILDoc(), "ns");
			block.setTop(getStyle(e, "top"));
			block.setLeft(getStyle(e, "left"));
			block.setRight(getStyle(e, "right"));
			block.setBottom(getStyle(e, "bottom"));
			block.setWidth(getStyle(e, "width"));
		 	// No width - use media size
			if (getStyle(e, "width").length() == 0) {
				if (e instanceof SMILMediaElementImpl)
					block.setWidth(((SMILMediaElementImpl)e).getMedia().getOriginalWidth());
			}
			block.setHeight(getStyle(e, "height"));
			// No height - use media size
			if (getStyle(e, "height").length() == 0) {
				if (e instanceof SMILMediaElementImpl)
					block.setHeight(((SMILMediaElementImpl)e).getMedia().getOriginalHeight());				
			}
			block.setBackgroundColor(getStyle(e, "background-color"));
			block.setAttribute("z-index", getStyle(e, "z-index"));
			// set region to show only if it has media
			block.setAttribute("showBackground","whenActive");

			// Map CSS overflow property to SMIL fit attribute			
			String overflow = getStyle(e, "overflow");
			String fit = "fill";
			if (overflow.equals("scroll"))
				fit ="scroll";
			block.setFit(fit);
			
			// Add the new region to the drawing area
			block.initHeadAndAddTo(parentReg, da, linefeeds, e);
//			block.initHead(); Not used - this adds only to rootlayout

			cssBlocks.put(e, block);
		}

		return block;
	}

	/**
	 * CSS: Returns the CSS property for an element. If the element doesn't support
	 * CSS, then "" will be returned.
	 *
	 * @param	The style of element e is asked for.
	 * @param	prop is the CSS property asked for.
	 * @return 	the value of CSS property.
	 */	
	private String getStyle(Element e, String prop) {
		if (e instanceof StylableElement) {
			CSSStyleDeclaration style = ((StylableElement)e).getStyle();
                        if (style!=null)
                        {
                            CSSValue val = style.getPropertyCSSValue(prop);
                            if (val != null)
                                    return val.getCssText();
                        }
		}
		
		return "";
	}

	/**
	 *  The mime type of the layout langage used in this layout element.The 
	 * default value of the type attribute is "text/smil-basic-layout". 
	 */
	public String getType() {
		return getAttribute("type");
	}

	/**
	 *  <code>true</code> if the player can understand the mime type, 
	 * <code>false</code> otherwise. 
	 */
	public boolean getResolved() {
		String type = getType();
		// Not found - use the default value "text/smil-basic-layout"
		if (type == null || type.length() == 0)
			type = "text/smil-basic-layout";
		if (type.equals("text/smil-basic-layout") == true)
			return true;

		return false;
	}

}
