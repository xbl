/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.media.ImageMedia;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.media.JMFMedia;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.media.TextMedia;

/**
 * Interface to media.
 */
public class SwingMediaHandler implements MediaHandler, ActionListener {
	// Public because JDK 1.1.8 cannot handle protected from
	// SMILMLFCLinkHandler.ClickListener
	public String alt, url;
	protected MediaListener mediaListener;
	protected SwingDrawingArea drawingArea;
	protected Container container;
	protected String documentURL;

	protected SMILDocumentImpl smilDoc = null;
	protected Viewer viewer = null;

	// CSS FIXTHIS : public access to get media component!
	public Media media = null;
	protected boolean prefetched = false;
	protected boolean playing = false;

	// Overridden MIME Type for this media. Can be set from MediaElement, if
	// type attr is set.
	protected String mimeType = null;

	// Coordinates of the media
	protected int top = 0, left = 0;
	protected int width = 0, height = 0;

	/**
	 * Create a new MediaHandler. Required for links.
	 * 
	 * @param documentURL
	 *                  The URL for smil document path.
	 */
	public SwingMediaHandler() {
		this.documentURL = null;
	}

	/**
	 * Create a new MediaHandler for Swing.
	 * 
	 * @param documentURL
	 *                  The URL for smil document path.
	 */
	public SwingMediaHandler(String documentURL) {
		this.documentURL = documentURL;
	}

	public void setViewer(Viewer v) {
		viewer = v;
		smilDoc = (SMILDocumentImpl) (viewer.getSMILDoc());
	}

	/**
	 * Set the drawing area. The given drawing area MUST be a
	 * SMILMLFCDrawingArea. This method must be dynamic - if called with a new
	 * value, should change the container for the media.
	 */
	public void setDrawingArea(DrawingArea d) {
		if (drawingArea != d) {
			// The given drawing area MUST be SMILMLFCDrawingArea
			drawingArea = (SwingDrawingArea) d;
			container = drawingArea.getContentContainer();
			if (media != null)
				media.setContainer(container);
		}
	}

	/**
	 * Add a media listener (currently supports only one listener)
	 */
	public void addListener(MediaListener mediaListener) {
		this.mediaListener = mediaListener;

		// Try to add listeners to this media, if it has already been
		// prefetched.
		if (media != null)
			media.addMediaListener(mediaListener);
	}

	/**
	 * Checks if this media is static or continuous.
	 * 
	 * @return true if media is static.
	 */
	public boolean isStatic() {
		if (media != null)
			return media.isStatic();
		else
			return false;
	}

	/**
	 * This media handler will know the rootlayout size after this is called.
	 * 
	 * @param width
	 *                  RootLayout width
	 * @param height
	 *                  RootLayout height
	 */
	public void setRootLayoutSize(int width, int height) {
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}
	public void setURL(String url) {
		this.url = url;

		// If media has already been prefetched, then prefetch it again
		if (prefetched == true) {
			Log.debug("+++ MediaHandler-Re-Prefetching " + url);
			// Close earlier media
			if (media != null) {
				media.close();
				media = null;
			}
			// Prefetch new media
			prefetch();
			// Play it, if the media was playing
			if (playing == true) {
				Log.debug("+++ Was playing -> playing");
				play();
			}
		}
	}

	public void setMIMEType(String type) {
		mimeType = type;
	}

	public void prefetch() {
		Log.debug("Prefetching " + url);
		int lastDot = 0;
		URL u = null;
		String type = null;

		/// ADDED
		// Normal media
		if (url == null) {
			Log.error("Media not found - src attribute missing?!");
			return;
		}

		// Editor may produce urls like
		// file:///D|/xsmiles40/demo/smil/ahma/ahma.jpg
		// Modify the pipe sign to be colon:
		int i = url.indexOf("|/");
		if (i > 3) {
			// Found, replace '|' with ':'
			url = url.substring(0, i) + ":" + url.substring(i + 1, url.length());
		}

		// This is RealPlayer's cache http protocol, use it as normal http
		// protocol
		if (url.startsWith("chttp") == true) {
			url = url.substring(1);
			Log.debug("Protocol chttp treated as normal http:");
		}

		if (!url.startsWith("http") && !url.startsWith("data") && !url.startsWith("file")) {
			if (documentURL.startsWith("file") || documentURL.startsWith("http"))
				url = documentURL + "/" + url;
			else
				url = "file:" + documentURL + "\\" + url;
		}

		try {
			u = new URL(url);
		} catch (MalformedURLException e) {
		}

		if (url.startsWith("data")) {
			Log.debug("Creating text media for text/plain");
			media = new TextMedia(url);
		} else
			// Check the type of the url
			try {

				/// ADDED END
				// Check the type of the url
				/// try {
				/// u = ((SMILMLFC)viewer).createURL(url);
				// This URLConnection should be passed to Media instead of URL
				// Passing URL will cause the URL to be opened twice.
				URLConnection urlConn = u.openConnection();
				String urltype = urlConn.getContentType();
				// Override MIME type?
				if (mimeType != null)
					urltype = mimeType;

				// Check the ending / mime type
				try {
					lastDot = url.lastIndexOf('.') + 1;
				} catch (Exception allExp) {
					// This is meant to catch 'out of index' exceptions
					lastDot = 0;
				}

				/***************************************************************
				 * if (url.substring(lastDot).equals("svg")) {
				 * Log.debug("Creating svg proxy for type:"+urltype); media =
				 * new XMLMedia(); } // url ends with ".fo" else if
				 * (url.substring(lastDot).equals("fo")) { Log.debug("Creating
				 * svg proxy for type:"+urltype); media = new XMLMedia(); } //
				 * url ends with ".smil" or ".smi" else if
				 * (url.substring(lastDot).equals("smi") ||
				 * url.substring(lastDot).equals("smil")) { Log.debug("Creating
				 * smil proxy for type:"+urltype); media = new XMLMedia(); }
				 **************************************************************/
				// "image/gif" "image/jpeg" ELSE
				if (urltype.startsWith("image")) {
					if (viewer.getPlayImage() == true) {
						Log.debug("Creating imageproxy for type:" + urltype);
						media = new ImageMedia();
					} else {
						Log.debug("Creating alt-proxy for type:" + urltype);
						media = new TextMedia();
						((TextMedia) media).setText(alt);
					}
				}

				// "text/plain"
				else if (urltype.startsWith("text")) {
					// Always create textproxy - no alt texts
					Log.debug("Creating textproxy for type:" + urltype);
					media = new TextMedia();
				}

				// "audio/*"
				else if (urltype.startsWith("audio")) {
					if (viewer.getPlayAudio() == true) {
						Log.debug("Creating (audio)mediaproxy for type:" + urltype);
						media = new JMFMedia();
					} else {
						Log.debug("Creating alt-proxy " + alt + " for type:" + urltype);
						media = new TextMedia();
						((TextMedia) media).setText(alt);
					}
				}

				// others... (video)
				else {
					if (viewer.getPlayVideo() == true) {
						Log.debug("Creating mediaproxy for type:" + urltype);
						media = new JMFMedia();
					} else {
						Log.debug("Creating alt-proxy for type:" + urltype);
						media = new TextMedia();
						((TextMedia) media).setText(alt);
					}
				}
			} catch (MalformedURLException ex) {
				Log.debug("URL type error: " + ex.toString());
			} catch (IOException io) {
				Log.debug("URL type error: " + io.toString());
			}

		if (media != null) {
			media.setUrl(u);
			try {
				media.prefetch();
			} catch (Exception e) {
				Log.error(e);
			}

			// Add all listeners to this media, now as it has been created.
			media.addMediaListener(mediaListener);
		}
		prefetched = true;
	}

	public void play() {
		if (container == null) {
			Log.error("No drawing area.container for " + url);
			return;
		}

		// If this media hasn't yet been prefetched, then prefetch it.
		if (prefetched == false)
			prefetch();

		// Media may be null, if the url was illegal
		if (media != null) {
			media.setContainer(container);
			media.setBounds(left, top, width, height);
			try {
				media.play();
			} catch (Exception e) {
				Log.error(e);
			}
		}
		playing = true;
	}

	public void pause() {
	}

	public void stop() {
		playing = false;
		if (container != null && media != null)
			media.stop();

	}
	public void freeze() {
		// TODO: This stop should not always be called..
		//		stop();
		if (container != null && media != null)
			media.pause();
		// The media will not be restarted - only paused to get it frozen
	}

	public void close() {
		playing = false;
		if (media != null)
			media.close();

		viewer = null;
		smilDoc = null;
		container = null;
		mediaListener = null;

		media = null;
	}

	/**
	 * Set the media time position. Usually used to skip the beginning of
	 * media.
	 * 
	 * @param millisecs
	 *                  Time in milliseconds
	 */
	public void setMediaTime(int millisecs) {
		if (media != null)
			media.setMediaTime(millisecs);
	}

	public int getTop() {
		return top;
	}
	public int getLeft() {
		return left;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setBounds(int x, int y, int w, int h) {
		left = x;
		top = y;
		width = w;
		height = h;
		if (media != null) {
			media.setContainer(container);
			media.setBounds(left, top, width, height);
		}
	}

	/**
	 * Get the real width of the media.
	 */
	public int getOriginalWidth() {
		if (media != null)
			return media.getOriginalWidth();
		else
			return -1;
	}

	/**
	 * Get the real height of the media.
	 */
	public int getOriginalHeight() {
		if (media != null)
			return media.getOriginalHeight();
		else
			return -1;
	}

	/**
	 * Set the volume of audio (if available).
	 * 
	 * @param percentage
	 *                  0-100-oo , 100 giving normal sound level.
	 */
	public void setAudioVolume(int percentage) {
		if (media != null)
			media.setSoundVolume(percentage);
	}

	public void actionPerformed(ActionEvent e) {
		if (mediaListener != null)
			mediaListener.mediaEnded();
	}

	public Object getComponent() {
		return null;
	}

}
