/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.basic.TimeChildList;
import fi.hut.tml.xsmiles.mlfc.smil.basic.ElementBasicTimeImpl;


import org.w3c.dom.smil20.SMILDocument;
import org.w3c.dom.smil20.*;
import org.w3c.dom.Node;

import java.awt.*;
import java.awt.event.*;

import fi.hut.tml.xsmiles.Log;

/**
 *  Implements links in Swing. 
 */
public class AwtLinkHandler extends AwtMediaHandler implements LinkHandler, MouseListener {

	protected Component linkComp;
	protected String linkTitle = null;
	protected SMILDocumentImpl smilDoc = null;
	protected Viewer viewer = null;

	public AwtLinkHandler() {
	}
	
	public void setTitle(String title) {
		if (title == null || title.length() == 0)
			linkTitle = null;
		else
			linkTitle = title;
	}	

	public void play() {
		Log.debug("Link at "+left+" "+top+" - "+width+" "+height);
		if (container == null) {
			Log.debug("No drawing area.container for link "+url);
			return;
		}
		linkComp = new InvisibleComponent();
		linkComp.addMouseListener(this);
		// Add this link on top of everything
		container.add(linkComp,0);
		linkComp.setBounds(left, top, width, height);
		linkComp.setEnabled(true);
		linkComp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		linkComp.setVisible(true);
	}
	public void pause() {
	}
	public void stop() {
		if (container != null && linkComp != null)
			container.remove(linkComp);

	}

	public void mouseEntered(MouseEvent e) {
		viewer.displayStatusText(url);
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mousePressed(MouseEvent e) {
	}
	public void mouseReleased(MouseEvent e) {
	}

	public void actionPerformed(ActionEvent e) {
		linkClicked(null);
	}

    public void mouseClicked(MouseEvent e) {
		linkClicked(e); 
    }

	public void linkClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}

	public void setViewer(Viewer v) {
		viewer = v;
		smilDoc = (SMILDocumentImpl)(viewer.getSMILDoc());
	}

}

