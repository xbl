/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import org.w3c.dom.*;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AnimationService;

import fi.hut.tml.xsmiles.dom.EventFactory;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

/**
 * Implements Brush media element. Inherited from MediaElement, to get area element to work.
 */
//public class SMILBrushElementImpl extends ElementTimeImpl 
public class SMILBrushElementImpl extends ElementTimeContainerImpl 
						implements XSMILBrushElement, MediaListener, AnimationService {

	BrushHandler brush = null;
	LinkHandler link = null;
	String src, alt;
	boolean elementInitialized = false;
	private String namespace = null;

	// Animation values
	private Hashtable animAttributes = null;

	// Region for the brush
	SMILRegionElementImpl region;

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 */
	public void setAnimAttribute(String attr, String value) {
		if (attr != null && value != null) {
			if (attr.equals("color"))
				animAttributes.put(attr, value);
			else
				Log.debug("Brush Animation of "+attr+" not supported.");
		}
		// Refresh animated attributes
		if (attr.equals("color") && brush != null) {
			brush.setColor(getAColor());
		}
	}

	/**
	 * This method returns the animation value of the attribute, and
	 * if not available, returns the DOM value.
	 */
	public String getAnimAttribute(String attr) {
		String val = (String)animAttributes.get(attr);
		if (val == null)
			val = getAttribute(attr);
		return val;	
	}
	
	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 */
	public void removeAnimAttribute(String attr) {
		animAttributes.remove(attr);
		// Refresh animated attributes
		if (attr.equals("color") && brush != null) {
			brush.setColor(getAColor());
		}
	}

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 */
	public void refreshAnimation()  {
//		refreshRegion();
	}

	// NOT USED
	public float convertStringToUnitless(String attr, String value) {
		return 0;
	}
	// NOT USED
	public String convertUnitlessToString(String attr, float value) {
		return null;
	}

	/**
	 * Mouse events.
	 */
	public void mouseClicked(java.awt.event.MouseEvent e) {
		dispatch("click", true);	
		dispatch("activateEvent", false);	
	}
	 
	public void mouseEntered() {
		dispatch("mouseover", true);	
		dispatch("inBoundsEvent", false);	
	}

	public void mouseExited() {
		dispatch("mouseout", true);	
		dispatch("outOfBoundsEvent", false);	
	}
	
	public void mousePressed() {
		dispatch("mousedown", true);	
	}
		
	public void mouseReleased() {
		dispatch("mouseup", true);	
	}

	/**
	 * Constructor - set the owner
	 */
	public SMILBrushElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
	    super(owner, smil, ns, "brush");

		elementInitialized = false;
		
		// Save namespace for setAttribute()
		namespace = ns;
	
		// Initialize hash for animation attributes
		animAttributes = new Hashtable();	
	}
	
	public void setAttribute(String name, String value) {
		// Use the element's namespace (correct way to do this?)
		setAttributeNS(namespace, name, value);
	}
		
	public void setAttributeNS(String ns, String name, String value) {
		this.setAttributeValue(name,value);
		super.setAttributeNS(ns,name,value);
	}
	public Attr setAttributeNode(Attr newAttr) throws DOMException {
	  setAttributeValue(newAttr.getName(), newAttr.getValue());
	  return super.setAttributeNode(newAttr);
	}
	
	
	protected void setAttributeValue(String name, String value) {
		// If region is changed (or set for the first time), then register
		// this media to the new one. This media will get refreshed if region changes.
		// Only do this if the elements have been initialized (i.e. the DOM exists)
		if (name.equals("region") && elementInitialized == true) {
			// Remove from the old region
			SMILRegionElementImpl region = (SMILRegionElementImpl)getRegionElement();
			if (region != null)
				region.removeMedia(brush);

			// Add to the new region
			region = (SMILRegionElementImpl)getRegionElement();
			if (region != null)
				region.addMedia(brush);
		} else if (name.equals("color") && brush != null) {
			brush.setColor(getAColor());
		}
	}
	
	public void removeAttribute(String name) {
	  super.removeAttribute(name);
	}
	
	public void initBrush() {
		brush = getSMILDoc().getViewer().getNewBrushHandler();
		brush.setURL(src);
		brush.setAlt(alt);
		brush.setColor(getAColor());
	}	
	public BrushHandler getBrush() {
		return brush;
	}
	
	/**
	 * If this brush element is under a 'a' element, then this variable
	 * will contain a reference to the 'a' element.
	 */
	private SMILAElementImpl linkElement = null;
	
	/**
	 * Initialize this brush element.
	 */
	public void init() {
		elementInitialized = true;
		super.init();
	}
	
	
	/** 
	 * PREFETCH - prefetching the brush.
	 * This method calls (XElementBasicTime)super.prefetch().
	 */
	public void prefetch() {
		if (brush == null)
			initBrush();

		brush.setViewer(getSMILDoc().getViewer());

		brush.prefetch();

		super.prefetch();
	}
	
	/**
	 * Overridden startup() - this will search for the first parent a element and creates
	 * corresponding link.
	 */
	public synchronized void startup() {
		Node node = getParentNode();
		while (node != null) {
			// Have we found the a element?
			if (node instanceof XSMILAElement) {
				linkElement = (SMILAElementImpl)node;
				Log.debug(getId()+" THIS BRUSH IS A LINK!!!" + linkElement.getHref());
			}
			// Get the next parent
			node = node.getParentNode();
		}
		// Not found - no links needed.
			
		super.startup();
	}
	
	/**
	 * Overridden display() - this will also show the media.
	 * This will also startup the children (areas or anchors).
	 */
	public void display() {
		if (brush == null)
			initBrush();
		
		SMILLayoutElement layout = getSMILDoc().getDocLayout();
		region = (SMILRegionElementImpl)getRegionElement();
		SMILRootLayoutElementImpl rootLayout = null;
		if (layout == null) 
			Log.error("<layout> not found!");
		else	
			rootLayout = (SMILRootLayoutElementImpl)layout.getRootLayoutElement();
		
		if (region != null && rootLayout != null) {
			// Brush needs to know the rootlayout size to zoom the presentation
			brush.setRootLayoutSize(layout.getRootLayoutWidth(), layout.getRootLayoutHeight());
			// Add media to region
			region.addMedia(brush);
			Log.debug(getId()+" - BRUSH SHOWN ");
		} else
			Log.error("No region for "+getId()+"        BRUSH not SHOWN ");

		// Clip the beginning of the brush - not done!

		// Listen to the end of brush
		brush.addListener(this);
		brush.play();
		
		// Handle links - if this media was under an 'a' element.

		if (linkElement != null) {
			if (link == null) {
				link = getSMILDoc().getViewer().getNewLinkHandler();
				// Set url to point to href
				link.setURL(linkElement.getHref());
				link.setAlt(linkElement.getAlt());
				link.setTitle(linkElement.getTitle());
				link.setViewer(getSMILDoc().getViewer());
				link.addListener(linkElement);
			}
					
			if (region != null && rootLayout != null) {
				// Link needs to know the rootlayout size to zoom the presentation
				DrawingArea drawingArea = rootLayout.getDrawingArea();
				link.setRootLayoutSize(layout.getRootLayoutWidth(), layout.getRootLayoutHeight());
	//				link.setTop(0);
	//				link.setLeft(0);
	//				link.setBottom(brush.getBottom());
	//				link.setRight(brush.getRight());
				link.setBounds(0, 0, brush.getWidth(), brush.getHeight());
				region.addLink(link);

				Log.debug(getId()+" - LINK SHOWN "+region.calcLeft()+
						" "+region.calcTop()+"-"+linkElement.getHref());
			} else
				Log.error("No region for "+getId()+"        LINK not SHOWN ");
			
			link.play();
		}
	
		super.display();
	}

	/**
	 * Overridden. This is called when simple duration for this element is ended.
	 * This restarts the media. Not needed for brush.
	 */
//	public void repeat(long time) {
//		Log.debug(getId()+" media repeat("+time+")");
//		// Restart the media.
//		if (media != null) {
//			// Stop playing media & rewind
//			media.stop();
//			// Stop removes listeners
//			media.addListener(this);
//			// Play media
//			media.play();
//		}
//		if (link != null) {
//			// Media stopped/played - it will be the top most component
//			// This will bring the link to top most.
//			link.stop();
//			link.play();
//		}
//				
//		super.repeat(time);
//	}
	
	public void freeze() {
		Log.debug("                   BRUSH FROZEN ");
		if (brush != null)
			brush.freeze();
		// Handle links, if this media was under an 'a' element.
		if (link != null) {
			link.freeze();
			Log.debug("                   LINK FROZEN ");
		}
		// Freeze all children as well
		super.freeze();
	}

	/**
	 * Overridden remove() - this will also remove the media. End of Active Duration.
	 */
	public void remove() {
//		Log.debug(getId()+" - MEDIA REMOVE "+getSrc());

		if (brush != null)  {
			brush.stop();
			if (region != null)
				region.removeMedia(brush);
		}	
		// Handle links, if this media was under an 'a' element.
		if (link != null) {
			link.stop();
			Log.debug(getId()+" - BLINK REMOVED ");
		}

		super.remove();
	}

	/**
	 * This method will destroy the element, freeing all its memory.
	 */
	public void destroy() {
		if (brush != null) {
			brush.close();
			brush = null;
		}

		if (link != null) {
			link.close();
			link = null;
		}

		super.destroy();
	}


	/**
	 * Callback method - called when the media has ended.
	 * This may cause the media to end its simple duration, if dur = "media" or dur = unspecified.
	 * The reason is that Simple Duration is first unresolved, and now it is resolved.
	 * THIS SHOULD NEVER BE CALLED FOR A BRUSH.
	 */
	public void mediaEnded() {
		Log.debug(getId()+" mediaEnded() - for a brush! ");
		
		// Get values
		Time duri = getDur();
		String end = getEnd();
		Time repeatDur = getRepeatDur();
		String repeatCount = getRepeatCount();
		String dur = getAttribute("dur");
		
		// repeatCount exception for continuous media - see calculation of IntermediateActiveDuration()
		if (isStatic() == false && manualRepeat > 0) {
			// Loop forever
			if (repeatCount.equals("indefinite")) {
				simpleDurEnded();
				return;
			}
			// Decrement repeat counts, and restart Simple Duration (repeat media).
			// TODO: This can currently only count integer values, not fractions of counts.
			manualRepeat--;
			if (manualRepeat > 0) {
				simpleDurEnded();
				return;
			}

			manualRepeat = 0;
		}
		
		// Simple Duration is "resolved" - see the Simple Duration Table
		// If dur, repeatDur, repeatCount are unspecified and end is specified, 
		// then Simple Duration = "indefinite"
		if ((dur == null || dur.length() == 0) && repeatDur == null && repeatCount == null && 
				end != null)
			return;

		// repeatDur exception for media - see calculation of IntermediateActiveDuration()
		// This is the case for Simple Duration = unresolved and repeatDur specified
		// Active Duration will end this media, therefore repeat forever here.
		if ((dur == null || dur.length() == 0) && repeatDur != null) {
			// Continuous media will repeat
			if (isStatic() == false)
				simpleDurEnded();
			// Static media will only sit there until Active Duration ends
			return;
		}

		// If dur is "media" or not specified -> end of Simple Duration
		if (dur == null || dur.length() == 0 || dur.equals("media")) {
			Log.debug("                   BRUSH ENDED-deactivate() ");
			deactivate();
			// SYNCBASE: Notify about the change of the end of interval
			notifyEndListeners(new TimeImpl(getCurrentParentTime()/1000));
		}
		
		// Other dur specification don't do anything
	}
	
	public void mediaPrefetched() {
	}
	
	/**
	 * Checks if media is static or continuous (video/audio).
	 * @return		true if media is static.
	 */
	public boolean isStatic() {
		return brush.isStatic();
	}
	
	/**
	 * This starts the children - overridden from ElementBasicTime.
	 * The children are area/anchor/switch...
	 */
	 public void startChildren() {
		XElementBasicTime activeChild;
		
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		// If no children, then just exit
		if (children.hasMoreElements() == false) {
			return;
		}

		// Get timed children
		while (children.hasMoreElements()) {
			activeChild = (XElementBasicTime)children.nextElement();
			activeChild.startup();
		}
	}

	/**
	 * This is called from the child to tell that it has ended.
	 * This brush element doesn't care about it.
	 * @param childDuration		Duration of the child element
	 */
	public void childEnded(long childDuration) {
		return;
	}

    public SMILRegionElement getRegionElement() {
		SMILLayoutElement layout = getSMILDoc().getDocLayout();
		String regStr = getAttribute("region");
		if (layout != null) {
			return layout.getRegionElement(regStr, this);
		}

		return null;	
    }
    public void setRegion(SMILRegionElement region) {
		if (region == null)
			removeAttribute("region");
		else
			setAttribute("region", region.getId());
    }
    public void setRegion(String region) {
    	setAttribute("region", region);
    }

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFill() {
 	   return getAttribute("fill");
    }
    public void setFill(String fill) throws DOMException {
		if (fill == null)
			removeAttribute("fill");
		else
			setAttribute("fill", fill);
		return;
    }

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr() {
		return getAttribute("abstract");
    }
    public void setAbstractAttr(String abstractAttr) throws DOMException {
	    if (abstractAttr != null && abstractAttr.length() > 0) {
	      setAttribute("abstract", abstractAttr);
	    } else {
	      removeAttribute("abstract");
	    }
    }

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor() {
		return getAttribute("author");
    }
    public void setAuthor(String author) throws DOMException {
	    if (author != null && author.length() > 0) {
	      setAttribute("author", author);
	    } else {
	      removeAttribute("author");
	    }
    }

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright() {
		return getAttribute("copyright");
    }
    public void setCopyright(String copyright) throws DOMException {
	    if (copyright != null && copyright.length() > 0) {
	      setAttribute("copyright", copyright);
	    } else {
	      removeAttribute("copyright");
	    }
    }

    /**
     *  See the  readIndex attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getReadIndex() {
		return getAttribute("readIndex");
    }
    public void setReadIndex(String readIndex) throws DOMException {
	    if (readIndex != null && readIndex.length() > 0) {
	      setAttribute("readIndex", readIndex);
	    } else {
	      removeAttribute("readIndex");
	    }
    }

    /**
     * Get the color attribute, giving precendence to the animated value.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAColor() {
    	String color = getAnimAttribute("color");
    	if (color.length() == 0)
    		color = null;
    		
    	return color;	
    }
    /**
     * Get the color attribute.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getColor() {
		String color = getAttribute("color");
		if (color.length() == 0)
			color = null;
			
		return color;	
    }
    public void setColor(String color) throws DOMException {
	    if (color != null && color.length() > 0) {
	      setAttribute("color", color);
	    } else {
	      removeAttribute("color");
	    }
    }

    /**
     *  See the erase attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getErase() {
		return 0;
    }
    public void setErase(short erase) throws DOMException {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
    }

    /**
     *  See the tabindex attribute in LinkingAttributes module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getTabindex() {
		return 0;
    }
    public void setTabindex(int tabindex) throws DOMException {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
    }

    /**
     *  See the transIn attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransIn() {
		return getAttribute("transIn");
    }
    public void setTransIn(String transIn) throws DOMException {
	    if (transIn != null && transIn.length() > 0) {
	      setAttribute("transIn", transIn);
	    } else {
	      removeAttribute("transIn");
	    }
    }

    /**
     *  See the transOut attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransOut() {
		return getAttribute("transOut");
    }
    public void setTransOut(String transOut) throws DOMException {
	    if (transOut != null && transOut.length() > 0) {
	      setAttribute("transOut", transOut);
	    } else {
	      removeAttribute("transOut");
	    }
    }


}
