/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.Log;

/**
 *  Declares custom attributes element.
 */
public class SMILCustomAttributesElementImpl extends SMILElementImpl implements XSMILCustomAttributesElement {

	/**
	* Constructor with owner doc
	*/
	public SMILCustomAttributesElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "customAttributes");
	}

	public boolean evaluateCustomTest(String tests)  {
		int start = 0, end = tests.indexOf("+");
		boolean result = false;
		
		if (end < 0)
			end = tests.length();

		do {
			result = getCustomTestValue(tests.substring(start, end).trim());
			// First test evaluating to false will finish
			if (result == false)
				return false;

			start = end+1;
			end = tests.indexOf("+", start);
			if (end < 0)
				end = tests.length();
		} while (start < tests.length());
		

		return true;
	}

	/**
	 * Get the boolean value of customTest element 'id'.
	 * @param id 		Id of customTestElement
	 * @return			true if customTest evaluates to true.
	 */
	public boolean getCustomTestValue(String id) {
		NodeList children = getChildNodes();
		Node e;
		XSMILCustomTestElement element = null;
		
		// Search for customTest element with id==id
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);
			if (e instanceof XSMILCustomTestElement) {
				element = (XSMILCustomTestElement)e;
				if (element.getId().equals(id) == true)  {
					// Get the UID - check value
					String uid = element.getUid();
										
					if (uid.startsWith("http://www.xsmiles.org/test/location") == true)  {
						return checkLocation(uid);
					}
					
					// UID not understood - use defaultState
					if (element.getDefaultState() == true)
						return true;
					
					// No defaultState or if was set to false.
					return false;	
				}
			}
		}
		
		// Not found
		Log.error("Custom Test '"+id+"' not found. Evaluated to default value false.");
		
		return false;
	}
		
	/**
	 * Special test - location. Format "http://www.xsmiles.org/test/location?latitude,longitude,radius",
	 * where latitute and longitude are coords and radius is distance in coords.
	 * @param uid - location URL + coords
	 */
	private boolean checkLocation(String uid)  {
		String location = uid.substring("http://www.xsmiles.org/test/location".length());
		int latitude = 0, longitude = 0, radius = 0;
		int realLat = 0, realLong = 0, distance = 0;
		int start = 1, end = 1;
		
		if (location.charAt(0) != '?')
			return false;

		try  {
			end = location.indexOf(",");
			latitude = Integer.parseInt(location.substring(start, end));
			start = end+1;
			end = location.indexOf(",", start);
			longitude = Integer.parseInt(location.substring(start, end));
			start = end+1;
			radius = Integer.parseInt(location.substring(start));
		} catch (NumberFormatException e)  {
			Log.error("CustomTest attribute has illegal value: "+uid);
			return false;
		} catch (IndexOutOfBoundsException e)  {
			Log.error("CustomTest attribute has illegal value: "+uid);
			return false;
		}
		
		// Get the coordinates from the system...
		realLat = 100;
		realLong = 100;

		distance = (realLat-latitude)*(realLat-latitude) + (realLat-latitude)*(realLat-latitude);
		distance = (int)Math.sqrt((double)distance);

		// If within the radius, then ok
		if (distance < radius)
			return true;

		return false;
	}

}
