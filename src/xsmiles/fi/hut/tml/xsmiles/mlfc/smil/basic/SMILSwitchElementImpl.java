/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;

/**
 *  Defines a block of content control. See the  switch element definition.
 * This class is extended from ElementTimeContainer, and therefore can be run inside the body
 * element or any other time container. This will search for starting child element
 * in startup(), a bit like ParTimeContainer in activate(), but starts only one child. Also,
 * this finishes immediately the child has finished.
 */
public class SMILSwitchElementImpl extends ElementTimeContainerImpl implements SMILSwitchElement {

	XElementBasicTime activeChild = null;

	/**
	 * Constructor with owner doc
	 */
	public SMILSwitchElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "switch");
	}

	/**
	 * This element can be in <head> or <body>. This method handles the initialization
	 * in case of <head>. <body> will be handled by the usual timing: startup(), activate()...
	 * Initialize the head element and its children. Calls init() only for the first layout element.
	 */
	 public SMILLayoutElement initHead(SMILHeadElementImpl head) {
	 	Node child = null;
	 	SMILLayoutElement layoutElement = null;
	 	boolean result;
	 	String type;
	
	 	// Get children of <head><switch>
	 	NodeList childList = getChildNodes();
	 	// If no children, then just exit
	 	if (childList.getLength() == 0) {
	 		return null;
	 	}
	
	 	// Go through layouts and init the _first_ with acceptable attributes.
	 	for (int i=0 ; i < childList.getLength() ; i++) {
	 		if (childList.item(i) instanceof SMILLayoutElement) {
	 			layoutElement = (SMILLayoutElement)childList.item(i);
	 			result = AttributeHandler.evaluateSystemValues(layoutElement, getSMILDoc().getViewer(), true);
	 			// This also checks for mime type="text/smil-basic-layout"
	 			type = layoutElement.getType();
	 			// Not found - use the default value "text/smil-basic-layout"
	 			if (type == null || type.length() == 0)
	 				type = "text/smil-basic-layout";
	 			if (type.equals("text/smil-basic-layout") == false)
	 				result = false;
	
	 			if (result == true) {
	 				Log.debug("Layout '"+layoutElement.getId()+"' init!");
					head.setLayout(layoutElement);
	 				layoutElement.init();
					return layoutElement;
	 			}
	 		} else {
		   		child = childList.item(i);
		   		if (child instanceof XSmilesElementImpl)
		   			((XSmilesElementImpl)child).init();
	 		}	
	 	}
		return null;
	 }


	/**
	 * This element SHOULD NOT ALLOW begin, dur, end etc. timing!!!
	 */		 

	//// public methods ////

	/** 
	 * PREFETCH - prefetching the media.
	 * This will prefetch ONLY selected elements. (otherwise switch wouldn't make sense)
	 */
	 public void prefetch() {
		 Log.debug(getId()+" switch-prefetch()");
	 	// Select the child
		
	 	TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
	 	// If no children, then just exit
	 	if (children.hasMoreElements() == false) {
	 		return;
	 	}

	 	// Get a timed child
	 	while (children.hasMoreElements()) {
	 		activeChild = (XElementBasicTime)children.nextElement();
			// The first child is the one to prefetch, TimeChildList has already evaluated the systemAttrs
			if (activeChild != null) {
		 		Log.debug("SWITCH "+((SMILElement)activeChild).getId()+" prefetching...");
		 		activeChild.prefetch();
		 		return;
			}
	 	}

	 	Log.debug("SWITCH nothing selected!");
	 }
	 
	 public void startChildren() {
		Log.debug(getId()+" switch-startChildren()");
		// Select the child
		
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		// Child found, play it
		if (children.hasMoreElements() == true) {

		 	// Get a timed child
		 	while (children.hasMoreElements()) {
		 		activeChild = (XElementBasicTime)children.nextElement();
		 	// The first child is the one to run, TimeChildList has already evaluated the systemAttrs
		 	if (activeChild != null) {
		  		Log.debug("SWITCH "+((SMILElement)activeChild).getId()+" selected!");
		  		activeChild.startup();
		  		return;
		 	}
		 	}
		}

		Log.debug("SWITCH nothing selected!");
		activeChild = null;
		deactivate();
	 }
	 
	 public void closeChildren()  {
		 Log.debug(getId()+" switch-closeChildren()");
		 // close the selected child
		 if (activeChild != null)
		 	activeChild.closedown();
		 
		 activeChild = null;	
	 }

	/**
	 *  Returns the selected element at runtime. <code>null</code> if the 
	 * selected element is not yet available. 
	 * @return  The selected <code>Element</code> for this <code>switch</code>
	 *    element. 
	 */
	public Element getSelectedElement() {
		return (Element)activeChild;
	}
	
	/**
	 *  The desired value (as a list of times) of the  begin instant of this 
	 * node. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBegin() {
		return null;
	}
	public void setBegin(TimeList begin) throws DOMException {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
    }

	/**
	 *  The list of active  ends for this node. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getEnd() {
		return null;
	}
	public void setEnd(TimeList end) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  The desired simple  duration value of this node in seconds.
	 * @return 	String (eg. 2s, media or indefinite)
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getDur() {
		return null;
	}
	public void setDur(MyFloat dur) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// restartTypes - TODO: DEFAULT?
	public static final short RESTART_ALWAYS            = 0;
	public static final short RESTART_NEVER             = 1;
	public static final short RESTART_WHEN_NOT_ACTIVE   = 2;

	/**
	 *  A code representing the value of the  restart attribute, as defined 
	 * above. Default value is <code>RESTART_ALWAYS</code> . 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getRestart() {
		return 0;
	}
	public void setRestart(short restart) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  The  repeatDur causes the element to play repeatedly (loop) for the 
	 * specified duration in milliseconds. Negative means "indefinite". 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getRepeatDur() {
		return null;
	}
	public void setRepeatDur(MyFloat repeatDur) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}


	/**
	 * 
	 *  The  repeat causes the element to play repeatedly (loop) for the 
	 * specified duration in milliseconds. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public MyFloat getRepeat() {
		return null;
	}
	public void setRepeat(MyFloat repeatDur) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Not available in switch.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRepeatCount() {
		return null;
	}

	public void setRepeatCount(String repeatCount) throws DOMException {
		setAttribute("repeatCount", repeatCount);
		return;
	}

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMin() {
		return null;
	}
	public void setMin(Time min) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMax() {
		return null;
	}
	public void setMax(Time max) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

}

