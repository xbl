/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AnimationService;

import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Enumeration;

import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

/**
 *  This is the animation scheduler. Every active animation should
 * register itself to this scheduler. During run time, the steps are the following
 * to comply with the "sandwich model":
 * <br>
 * Every timer tick (50 ms): <br>
 *  1. Scheduler clears all domBuffer values<br>
 *  2. Scheduler asks animation elements sequenced by priority to update domBuffer <br>
 *  3. Scheduler updates relevant target elements based on updated domBuffer <br>
 *  4. Scheduler refreshes the target elements - all animations are visible at the
 *     same time. Also no longer used domBuffer attributes are cleared.
 * <br>
 * When the animation element is removed, it should remove itself from the scheduler.<p>
 * Only one instance of the scheduler is created by SMILDocumentImpl. 
 *
 * The animated target Element must implement AnimationService to be animated.
 */
public class AnimationScheduler implements Runnable {

	// Number of animations - if zero, then updateAnimations() is not called
	private int animations = 0;

	// Vector of PrioritizedAnimations containing animation elements and their priorities.
	// This list is maintained to keep track of addAnimation() and removeAnimation() calls.
	// This will also dictate the sequence defined by "sandwich model".
	private Vector prianims = null;

	// A mapping from animation elements (AnimationElementImpl) to 
	// prioritized animations (PrioritizedAnimation).
	// This is used to find the PrioritizedAnimation object for AnimationElementImpl,
	// so that PrioritizedAnimation can be removed from prianims vector.
	private Hashtable anims = null;

	// Hash of "Target"s (target+attributes) and their animated values.
	// First, this is empty, but is then updated by the AnimationElementImpls.
	// Finally, this hash has animated DOM values (or empty values, if target
	// was no longer animated).
	private Hashtable domBuffer = null;
	
	// Reference to SMILDocumentImpl - to get the timer and search for ids
	private SMILDocumentImpl smilDoc = null;

	// Dummy object to be saved to domBuffer to indicate that the value is not set
	// during a animation round (updateAnimations() ).
	private String blank = "*-*-*-*-*";

	/** 
	 * Creates necessary private variables.
	 */
	public AnimationScheduler(SMILDocumentImpl smil) {
		smilDoc = smil;
		
		prianims = new Vector();
		anims = new Hashtable();
		domBuffer = new Hashtable();
	}

	/**
	 * Stop the scheduler - frees timers etc. Called from the SMILDocumentImpl.
	 */
	public void stop() {
		Target target = null;
		
		// End the thread
		animations = 0;

		// Clear animation values
		for (Enumeration e = domBuffer.keys() ; e.hasMoreElements() ;) {
		 	target = (Target)e.nextElement();

			// Remove Anim Attribute from target element
			if (target.target instanceof AnimationService) {
				((AnimationService)target.target).removeAnimAttribute(target.attr);
				((AnimationService)target.target).refreshAnimation();
			}
			
		}
		// Clear vectors and hashtables
		prianims.removeAllElements();
		anims.clear();
		domBuffer.clear();

	}

	/**
	 * Add an animation element. Keeps them ordered by priority.
	 * Priority should be the animation element's begin time 
	 * (relative to document or world time), with exceptions for timebase elements.
	 * The update() method of this animation element will be called to 
	 * get the animated value for the target attribute.
	 */
	public void addAnimation(long priority, SMILAnimationImpl anim) {
		// This method will keep the anims vector ordered by priority
		int i;
		PrioritizedAnimation p = null;
				
		// Search the correct position
		for (i = 0 ; i < prianims.size() ; i++) {
			p = (PrioritizedAnimation) prianims.elementAt(i);
			if (p.priority > priority)
				break;
		}
		
		// Add the element
		PrioritizedAnimation pa = new PrioritizedAnimation(priority, anim);
		prianims.insertElementAt(pa, i);
		anims.put(anim, pa);

		animations++;
		if (animations == 1) {
			Thread t = new Thread(this);
			t.start();	
		}
	}
	
	/**
	 * Remove an animation element from the AnimationScheduler. update() method
	 * of this animation element will no longer be called.
	 */
	public boolean removeAnimation(SMILAnimationImpl anim) {
		// Remove and if not found, just exit
		PrioritizedAnimation pa = (PrioritizedAnimation)anims.get(anim);
		anims.remove(anim);
		if (pa == null || prianims.removeElement(pa) == false)
			return false;
		
		animations--;
		// Make sure old DOM values are updated to screen
		if (animations == 0)
			updateAnimations();	
		
		return true;	
	}

	/**
	 * Update all animations in the presentation. Goes through all animation elements
	 * registered to this scheduler and updates the target attributes in the DOM.
	 */
	public void updateAnimations() {
		Target target;
		String value;
		PrioritizedAnimation p;
	
		// 1. Clear all domBuffer
		for (Enumeration e = domBuffer.keys() ; e.hasMoreElements() ;)
			domBuffer.put(e.nextElement(), blank);

		// 2. Ask animations to update the domBuffer
		for (Enumeration e = prianims.elements() ; e.hasMoreElements() ;) {
			p = (PrioritizedAnimation)e.nextElement();
			// update() will call getAnimAttribute() and setAnimAttribute() 
			// to access DOM values
			(p.anim).update(this);
		}
		
		// 3. Set non-null domBuffer values to targets. 
		for (Enumeration e = domBuffer.keys() ; e.hasMoreElements() ;) {
		 	target = (Target)e.nextElement();
			value = (String)domBuffer.get(target);

			// Null value - call remove
			if (value == null || value.equals(blank)) {
				// Remove Anim Attribute from target element
				if (target.target instanceof AnimationService) {
					((AnimationService)target.target).removeAnimAttribute(target.attr);
//					Log.debug("!!Anim remove: "+target.target.getAttribute("id")+"."+target.attr+" : "+value);
				}
			} else {
				// Set Anim Attribute for target element
				if (target.target instanceof AnimationService)
					((AnimationService)target.target).setAnimAttribute(target.attr, value);
//				if (target.target instanceof SMILMediaElementImpl)
//					((SMILMediaElementImpl)target.target).setAttribute(target.attr, value);
									
//				Log.debug("!!Anim set: "+target.target.getAttribute("id")+"."+target.attr+" : "+value);
			}
		}

		// 4. Refresh non-null targets. If domBuffer value is still blank, 
		//    remove the animation value from target, letting the true DOM value 
		//    be again effective. 
		for (Enumeration e = domBuffer.keys() ; e.hasMoreElements() ;) {
		 	target = (Target)e.nextElement();
		 	value = (String)domBuffer.get(target);
		 	// Null value - remove from list
		 	if (value == null || value.equals(blank))
		 		domBuffer.remove(target);

			if (target.target instanceof AnimationService) {
//				Log.debug("!!Anim refresh: "+target.target.getAttribute("id")+"."+target.attr);
				((AnimationService)target.target).refreshAnimation();
			}
		}
	}

	/**
	 * Callback method. updateAnimations() will call update() for every active
	 * SMILAnimationElementImpl. update() will call back this method
	 * to get the animated value, i.e. update() will not get
	 * the attribute value straight from the DOM, but from this AnimationScheduler.
	 * This method will return the "DOM buffer" value or if it doesn't exist,
	 * the real DOM value.
	 *
	 * @param element		Target element
	 * @param attribute		Target attribute
	 * @return				Value of target attribute from the "DOM buffer"
	 */
	public String getAnimAttribute(Element element, String attribute) {
		// Get the value - read already updated value from the "DOM buffer" or real DOM value
		Target target = new Target(element, attribute);
		// Get it from DOM buffer
		String value = (String)domBuffer.get(target);
		// Not found -> get it from the real DOM
		if (value == null || value.equals(blank))
			value = element.getAttribute(attribute);

		return value;	
	}

	/**
	 * Callback method. updateAnimations() will call update() for every active
	 * SMILAnimationElementImpl. update() will call back this method
	 * to set the calculated animated value, i.e. update() will not set
	 * the animated value straight to the DOM, but to this AnimationScheduler.
	 * updateAnimations() will finally flush these values to the DOM.
	 * @param element		Target element
	 * @param attribute		Target attribute
	 * @param value			Value of target attribute, to be set to the "DOM buffer"
	 */
	public void setAnimAttribute(Element element, String attribute, String value) {
		// Set the value - write it to the "DOM buffer"
		Target target = new Target(element, attribute);
		domBuffer.put(target, value);
	}

	/**
	 * Animate everything. This thread runs if there are more than zero animations.
	 * This thread will call updateAnimations() every 50 ms.
	 */
	public void run() {
		long delay;
		while (animations > 0) {
			delay = System.currentTimeMillis();
//			((SMILMLFC)smilDoc.getViewer()).refresh();
			updateAnimations();
			// Wait for a while...
			delay = System.currentTimeMillis() - delay;
			try {
//				Log.debug("wait: "+delay);
			//	if (delay < 30)
				Thread.sleep(50);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Class to store target and attribute combinations.
	 */
	private class Target {
		public Element target = null;
		public String attr = null;
		
		public Target(Element t, String a) {
			target = t;
			attr = a;
		}

		// Custom hashcode to get two Targets to return the same hash if the same targetElement.
		// Fast way to get different hashcodes, although different attributes return the
		// same hash value.
		// This is required because two instances of Target with the same values
		// are considered to be different items by the Hashtable
		public int hashCode() {
			return target.toString().hashCode();
		}
		
		// Custom equals to get two instances of Target to return true if the same values
		public boolean equals(Object o) {
			Target t = (Target)o;
			if (target.equals(t.target) && attr.equals(t.attr))
				return true;
			
			return false;
		}
	}

	/**
	 * Class to store animation elements, their priorities and targetElements.
	 * The scheduler has a vector of these sorted by their priorities.
	 * Each animation element will have one of these in the vector.
	 */
	private class PrioritizedAnimation {
		public long priority = 0;
		public SMILAnimationImpl anim = null;

		public PrioritizedAnimation(long p, SMILAnimationImpl anim) {
			priority = p;
			this.anim = anim;
		}
		
	}


}

