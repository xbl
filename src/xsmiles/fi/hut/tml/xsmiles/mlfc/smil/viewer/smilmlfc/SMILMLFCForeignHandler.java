/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.AWTForeignMediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;

import org.w3c.dom.Element;
import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.dom.VisualComponentService;

import java.awt.*;

//import javax.swing.JComponent;

import fi.hut.tml.xsmiles.Log;

/**
 * This class takes care of ForeignElements resizing in the SMIL documents.
 * This SMILMLFC class is a full implementation. This class implements MediaHandler
 * interface, so that SMILREgionElementImpl can control this component.
 */
public class SMILMLFCForeignHandler extends AWTForeignMediaHandler implements MediaHandler {
	public SMILMLFCForeignHandler() {
	    super();
	}

	/**
	 * Create a new ForeignHandler for SMIL MLFC.
	 * @param e		The foreign element implementing VisualComponentService.
	 */
	public SMILMLFCForeignHandler(Element e) {
	    super(e);
	}

}

