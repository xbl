/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.smil20.*;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;

import fi.hut.tml.xsmiles.Log;

/**
 *  The <code>Time</code> interface is a datatype that represents times within 
 * the timegraph. A <code>Time</code> has a type, key values to describe the 
 * time, and a boolean to indicate whether the values are currently 
 * unresolved.  Still need to address the wallclock values. 
 */
public class TimeImpl implements Time, EventListener {
	// time offset value in millisecs - time of event, syncbase time etc. doesn't include offset
    private long		timeValue = 0;

	// offset for syncbase time values
	private long		offset = 0;

	// true if this time is part of begin list, otherwise false
	private boolean		beginList = false;

	// true if the time is indefinite
    private boolean		indefinite = false;
	
	// true if the time is resolved
	private boolean 	resolved = true;

	// true if this clock value is based on syncbase element's begin
	private boolean		baseBegin = false;

	// true if this time cannot be changed anymore
	private boolean		locked = false;

	// SMILDocumentImpl to handle clock values, to search for base elements.
	private SMILDocumentImpl sDoc = null;
	
	// if this is a syncbase clock value, this is the base element
	private ElementBasicTimeImpl	syncbaseElement = null;

	// Element, which will receive time changes from the base element.
	// This element is the one having this Time value.
	private ElementBasicTimeImpl dependentElement = null;

	// The type of this time
	private short		timeType;

	// TimeTypes
	public static final short SMIL_TIME_INDEFINITE      = 0;
	public static final short SMIL_TIME_OFFSET          = 1;
	public static final short SMIL_TIME_SYNC_BASED      = 2;
	public static final short SMIL_TIME_EVENT_BASED     = 3;
	public static final short SMIL_TIME_WALLCLOCK       = 4;
	public static final short SMIL_TIME_MEDIA_MARKER    = 5;
	public static final short SMIL_TIME_REPEAT		    = 6;
	public static final short SMIL_TIME_ACCESSKEY		= 7;

	/**
	 * Contructor to define blank time, defaults to indefinite.
	 */
	 public TimeImpl() {
	 	init("indefinite");
	 }

	/**
	 * Contructor to define the time.
	 * @param timeFloat	Time float, which will be parsed.
	 */
	public TimeImpl(MyFloat timeFloat) {
		init(timeFloat.toString()+"ms");
	}

	/**
	 * Contructor to define the time and type.
	 * @param time		Time long, which will be parsed.
	 * @param tt		TimeType
	 * @param base		Base element, will have intervalchangedListener
	 * @param baseBegin	true if based on begin attribute
	 */
	public TimeImpl(long time, long offset, short tt, ElementBasicTimeImpl base,
					ElementBasicTimeImpl dependent, boolean baseBegin, boolean beginList,
					boolean resolved, boolean indefinite) {
		timeValue = time;
		timeType = tt;
		this.offset = offset;
		this.baseBegin = baseBegin;
		this.beginList = beginList;
		syncbaseElement = base;
		dependentElement = dependent;
		this.resolved = resolved;
		this.indefinite = indefinite;
		
		// If dependent element, add listener
		if (base != null)  {
			if (baseBegin == true)
				base.addIntervalBeginListener(this, dependent);
			else	
				base.addIntervalEndListener(this, dependent);
		}
	}

	/**
	 * Contructor to define the time.
	 * @param timeFloat	Time float, which will be parsed.
	 */
	 public TimeImpl(int timeInt) {
	 	init(String.valueOf(timeInt)+"ms");
	 }

	/**
	 * Contructor to define the time.
	 * @param timeString	Time string, which will be parsed.
	 */
	 public TimeImpl(String timeString) {
	 	init(timeString);
	 }

	 /**
	  * Contructor to define the time. This can also handle syncbase clock values,
	  * using the SMILDocumentImpl passed in.
	  * @param timeString	Time string, which will be parsed.
	  * @param doc			SMILDocumentImpl, to handle syncbase clock values
	  * @param e			Element, which will receive time changes from syncbase element
	  * @param beginList	true if this Time is part of beginList, otherwise false
	  */
	  public TimeImpl(String timeString, SMILDocumentImpl doc, ElementBasicTimeImpl e,
	  					boolean beginList) {
	  	dependentElement = e;
	  	sDoc = doc;
		this.beginList = beginList;
	  	init(timeString);
	  }

	/**
	 * Initializes this time instance.
	 */
	private void init(String timeString) {
		parseTimeAttribute(timeString);
	}
	
	/**
	 * Lock this time - changes cannot be made after this call.
	 */
	public void lock()  {
		locked = true;
	}

	/**
	 * Unlock this time - changes can be made after this call.
	 */
	public void unlock()  {
		locked = false;
	}
	
	/**
	 * Forces this Time object to have the same time value offset as t. Only the time value
	 * will change - not the Time type or syncbase element. This method is used to
	 * change the time value for a resolved syncbase clock value.
	 * If t is null, this Time object will be unresolved.
	 * @param	t		Time Object 
	 */
	public void setTimeValue(Time t) {
		if (t == null || t.getResolved()==false) {
			resolved = false;
			indefinite = false;
			return;
		}
		if (t.isIndefinite())  {
			resolved = true;
			indefinite = true;
			return;
		}
			
		timeValue = ((TimeImpl)t).getTimeValue();
		resolved = true;
		indefinite = false;
	}
	
	/**
	 * Returns the time value. This is only for internal processing. Use getResolvedOffset().
	 * @return		timeValue
	 */
	public long getTimeValue() {
		return timeValue;
	}
	
	/**
	 * Returns true if this Time is part of begin time list.
	 * @return		true if this time is part of begin time list.
	 */
	public boolean isInBeginList() {
		return beginList;
	}
	
    /**
     * A boolean indicating whether the current <code>Time</code> has been 
     * fully resolved to the document schedule.  Note that for this to be 
     * true, the current <code>Time</code> must be defined (not indefinite), 
     * the syncbase and all <code>Time</code> 's that the syncbase depends on 
     * must be defined (not indefinite), and the begin <code>Time</code> of 
     * all ascendent time containers of this element and all <code>Time</code>
     *  elements that this depends upon must be defined (not indefinite). 
     * <br> If this <code>Time</code> is based upon an event, this 
     * <code>Time</code> will only be resolved once the specified event has 
     * happened, subject to the constraints of the time container. 
     * <br> Note that this may change from true to false when the parent time 
     * container ends its simple duration (including when it repeats or 
     * restarts). 
     */
	public boolean getResolved() {
		return resolved;
	}

    /**
     *  The clock value in seconds relative to the parent time container begin.
     *  This indicates the resolved time relationship to the parent time 
     * container.  This is only valid if resolved is true. 
     */
	public MyFloat getResolvedOffset() {
		if (resolved == true)
			return new MyFloat((int)(timeValue+offset));

		return new MyFloat(0);	
	}

    /**
     *  A code representing the type of the underlying object, as defined 
     * above. 
     */
	public short getTimeType() {
		if (indefinite)
			return SMIL_TIME_INDEFINITE;

		return timeType;
	}

    /**
     *  The clock value in seconds relative to the syncbase or eventbase. 
     * Default value is <code>0</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised on attempts to modify this 
     *   readonly attribute. 
     */
	public MyFloat getOffset() {
		return new MyFloat(0);
	}
	public void setOffset(MyFloat offset) throws DOMException {
		return;
	}

    /**
     *  The base element for a sync-based or event-based time. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised on attempts to modify this 
     *   readonly attribute. 
     */
	public Element getBaseElement() {
		return syncbaseElement;
	}
	public void setBaseElement(Element baseElement) throws DOMException {
		return;
	}

    /**
     *  If <code>true</code> , indicates that a sync-based time is relative to 
     * the begin of the baseElement.  If <code>false</code> , indicates that a
     *  sync-based time is relative to the active end of the baseElement. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised on attempts to modify this 
     *   readonly attribute. 
     */
	public boolean getBaseBegin() {
		return baseBegin;
	}
	public void setBaseBegin(boolean baseBegin) throws DOMException {
		return;
	}

    /**
     *  The name of the event for an event-based time. Default value is 
     * <code>null</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised on attempts to modify this 
     *   readonly attribute. 
     */
	public String getEvent() {
		return null;
	}
	public void setEvent(String event) throws DOMException {
		return;
	}

    /**
     *  The name of the marker from the media element, for media marker times. 
     * Default value is <code>null</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised on attempts to modify this 
     *   readonly attribute. 
     */
	public String getMarker() {
		return null;
	}
	public void setMarker(String marker) throws DOMException {
		return;
	}

	//// Added public methods ////
	
	/**
	 * For debugging purposes
	 * @return time in seconds
	 */
	 public String getString() {
	 	if (isIndefinite() == true) 
	 		return "indefinite";
	 	if (getResolved() == false)
			return "unresolved";
		
		return String.valueOf(((float)(getResolvedOffset().intValue()))/1000);
	 }
	
	/**
	 * Is time greater than this.
	 * @param time	Time value
	 * @return 		true if this is greater than time.
	 */
	public boolean isGreaterThan(Time time) {
		// unresolved > indefinite|resolved |unresolved
		if (this.getResolved() == false)
			return true;
		
		// indefinite > resolved
		if (this.isIndefinite() == true && (time.getResolved() == true && time.isIndefinite() == false))
			return true;

		// indefinite < unresolved / indef.
		if (this.isIndefinite() == true)
			return false;

		// time val < indefinite
		if (this.isIndefinite() == false && time.isIndefinite() == true)
			return false;

		// time val > time val
		if (this.getResolved() == true && time.getResolved() == true) {
			if (this.getResolvedOffset().intValue() > time.getResolvedOffset().intValue())
				return true;
		}

		return false;			
	}

	/**
	 * Is time equal to this.
	 * @param time	Time value
	 * @return 		true if this is greater than time.
	 */
	public boolean isEqualTo(Time time) {
		if (time == null)
			return false;
			
		// indefinites
		if (this.isIndefinite() == true && time.isIndefinite() == true)
			return true;

		// unresolved never equals
		if (this.getResolved() == true || time.getResolved() == true)
			return false;
		
		// time val == time val
		if (this.getResolvedOffset().intValue() == time.getResolvedOffset().intValue())
			return true;

		return false;
	}

	/**
	 * Checks if the time is negative.
	 * @return 		true if time is negative.
	 */
	 public boolean isNegative() {
	 	if (this.getResolved() == true && this.isIndefinite() == false)
			if (this.getResolvedOffset().intValue() < 0)
				return true;
				
	 	return false;
	 }

	/**
	 * @return 	true if time is indefinite
	 */
	public boolean isIndefinite() {
		return indefinite;
	}

	//// Private methods ////
	
	private void parseTimeAttribute(String time) {
		int signIndicator, dot;
		String token;
		indefinite = false;
		resolved = false;
		timeValue = 0;
		offset = 0;

		// If time is unspecified or "unresolved" (a new value).
		if (time == null || time.length() == 0 || time.equals("unresolved")) {
			return;
		}
	
		// Strip any leading, trailing, or intervening white space characters
		time = time.trim();

		try {	
			// Parse as Offset Value
			if (time.charAt(0) == '-' || time.charAt(0) == '+' || Character.isDigit(time.charAt(0))) {
				resolved = true;
				timeType = SMIL_TIME_OFFSET;
				timeValue = parseOffsetValue(time);
				return;
			}

			// Parse as Wallclock Value
			if (time.startsWith("wallclock") == true) {
				resolved = true;
				timeType = SMIL_TIME_WALLCLOCK;
				timeValue = parseWallclockValue(time);
				return;
			}

			// Parse as "indefinite"
			if (time.equals("indefinite") == true) {
				resolved = true;
				timeType = SMIL_TIME_INDEFINITE;
				timeValue = 0;
				indefinite = true;
				return;
			}
		
			// Build token substring and parse offset string
			// TODO: This cannot handle situations, where we have
			// XForms events with offsets: xforms-select-1s
			token = time;
			signIndicator = time.lastIndexOf('+');
			if (signIndicator != -1)  {
				token = time.substring(0, signIndicator);
				try {
					offset = parseOffsetValue(time.substring(signIndicator));
				} catch (NumberFormatException e) {
					// offset wasn't offset?? just use it as event name.
					Log.debug("WARNING: SMIL EventTime Offset may be incorrect: "+time);
					token = time;
					offset = 0;
				}
			}
			signIndicator = time.lastIndexOf('-');
			if (signIndicator != -1)  {
				token = time.substring(0, signIndicator);
				try {
					offset = parseOffsetValue(time.substring(signIndicator));
				} catch (NumberFormatException e) {
					// offset wasn't offset?? just use it as event name.
					Log.debug("WARNING: SMIL EventTime Offset may be incorrect: "+time);
					token = time;
					offset = 0;
				}
			}

			// Parse Event-Value, if no dot found
			dot = token.lastIndexOf('.');
			if (dot == -1) {
				resolved = true;
				timeType = SMIL_TIME_EVENT_BASED;
				timeValue = 0;
				parseEventValue(token, dot);
				return;
			}

			// Parse Syncbase Value, if ends with non-escaped '.begin' or '.end'
			if ((token.length() > 6 && token.endsWith(".begin") && 
								token.indexOf(token.length()-7) != '\\') ||
				(token.length() > 4 && token.endsWith(".end") && 
								token.indexOf(token.length()-5) != '\\'))  {
				resolved = true;
				timeType = SMIL_TIME_SYNC_BASED;
				timeValue = 0;
				parseSyncbaseValue(token, dot);
				return;
			}
			
			// Parse Media Marker Value
			if ((token.length() > 8 && token.endsWith(".marker(") && 
								token.indexOf(token.length()-9) != '\\'))  {
				resolved = true;
				timeType = SMIL_TIME_MEDIA_MARKER;
				timeValue = 0;
				parseMediaMarkerValue(token);
				return;
			}
			
			// Parse as Event-Value
			resolved = true;
			timeType = SMIL_TIME_EVENT_BASED;
			timeValue = 0;
			parseEventValue(token, dot);
			return;

		} catch (NumberFormatException e) {
			Log.error("Illegal time value: "+time+" "+e.getMessage());
			// Leave the time unresolved
			resolved = false;
//			timeType = SMIL_TIME_EVENT_BASED;
			timeValue = 0;
			offset = 0;
			indefinite = false;
			return;
		}
		// NOTREACHED
	}
	
	/**
	 * Find unescaped char, escaped char has a preceding '\'
	 * @param str		String, where the char should be
	 * @param c 		Char to be found
	 * @return			Position of char, or -1 if not found.
	 */
	private int findUnescaped(String str, char c)  {
		if (str.charAt(0) == c)
			return 0;

		for (int i = 1 ; i < str.length() ; i++)  {
			if (str.charAt(i) == c && str.charAt(i-1) != '\\')
				return i;
		}
		return -1;
	}
	
	/**
	 * Parse Offset Value
	 * @param time		String to parse
	 */
	private long parseOffsetValue(String time)  {
		// Offset can be null/empty if after eventbase or syncbase
		if (time == null || time.length() == 0)
			return 0;
	
		// Get the sign and parse clock value
		if (time.charAt(0) == '+')  {
			return parseClockValue(time.substring(1));
		} else if (time.charAt(0) == '-')  {
			return -parseClockValue(time.substring(1));
		}
		
		return parseClockValue(time);
	}

	/**
	 * Parse Wallclock Value
	 * @param time		String to parse
	 */
	private long parseWallclockValue(String time)  {
		// Not implemented
		return 0;
	}

	/**
	 * Parse Event Value
	 * @param time		String to parse
	 */
	private void parseEventValue(String time, int dot)  {
		handleEventbase(time, dot);
		return;
	}

	/**
	 * Parse Syncbase Value
	 * @param time		String to parse
	 */
	private void parseSyncbaseValue(String time, int dot)  {
		handleSyncbase(time, dot);
		return;
	}
	/**
	 * Parse Media Marker Value
	 * @param time		String to parse
	 */
	private long parseMediaMarkerValue(String time)  {
		// Not implemented
		return 0;
	}

	/**
	 * Parse Clock value
	 * @param time		Clock value
	 * @return		Time specified by the clock value, in millisecs.
	 */
	private long parseClockValue(String time) throws NumberFormatException {
		// Try to parse clock value
	    if (isFullClockValue(time)) {
	        return handleFullClockValue(time);
	    } else if (isPartialClockValue(time)) {
	        return handlePartialClockValue(time);
	    } else
			return handleTimeCountValue(time);
	}

	/**
	 * Handles time count values. Note: negative values not well supported.
	 */
	private long handleTimeCountValue(String time) {
	    if (time.endsWith("ms") || time.endsWith("MS")) {
	        return handleMilliseconds(time);
	    } else if (time.endsWith("s") || time.endsWith("S")) {
	        return handleSeconds(time);            
	    } else if (time.endsWith("min") || time.endsWith("MIN")) {
	        return handleMinutes(time);            
	    } else if (time.endsWith("h") || time.endsWith("H")) {
	        return handleHours(time);
	    } else {
			// Default is seconds
		    return handleSeconds(time+"s");            
	    }
	}

	// two colons found -> fullClockValue
	private boolean isFullClockValue(String time) {

	    int colon = getNextColon(time, 0);
	    if (colon != -1) {
	        colon = getNextColon(time, colon + 1);
	        if (colon != -1) {
	            colon = getNextColon(time, colon + 1);
	            if (colon == -1) {  // not expecting more colons!
	                return true;
	            }
	        }
	    }
	    
	    return false;
	}
		
	// hh:mm:ss.fraction
	private long handleFullClockValue(String time) {
		int colon = getNextColon(time, 0);
		int colon2 = getNextColon(time, colon + 1);
		int h=0, min=0, s=0;
		long t=0;

		if (hasFraction(time)) {
		    h   = Integer.parseInt(time.substring(0, colon));
			min = Integer.parseInt(time.substring(colon+1, colon2));
			s   = Integer.parseInt(time.substring(colon2+1, time.indexOf(".")));
			t   = handleFraction(time, 0);
		} else {
			h   = Integer.parseInt(time.substring(0, colon));
			min = Integer.parseInt(time.substring(colon+1, colon2));
			s   = Integer.parseInt(time.substring(colon2+1));
		}
		// Check minute/second limits
		if (min < 0 || min > 59)
			throw new NumberFormatException();
		if (s < 0 || s > 59)
			throw new NumberFormatException();

		// Millisecs
		t = t + h*3600000 + min*60000 + s*1000;
		return t;
	}

	// only one colon found -> partialClockValue
	private boolean isPartialClockValue(String time) {
	    
	    int colon = getNextColon(time, 0);
	    if (colon != -1) {
	        colon = getNextColon(time, colon + 1);
	        if (colon == -1) {  // not expecting more colons!
	            return true;
	        }
	    }
	    return false;
	}

	// mm:ss.fraction
	private long handlePartialClockValue(String time) {
		int colon = getNextColon(time, 0);
		int min=0, s=0;
		long t=0;

		if (hasFraction(time)) {
		    min = Integer.parseInt(time.substring(0, colon));
			s = Integer.parseInt(time.substring(colon+1, time.indexOf(".")));
			t = handleFraction(time, 0);
		} else {
		    min = Integer.parseInt(time.substring(0, colon));
			s   = Integer.parseInt(time.substring(colon+1));
		}

		// Check minute/second limits
		if (min > 59)
			throw new NumberFormatException();
		if (s < 0 || s > 59)
			throw new NumberFormatException();

		// Millisecs
		t = t + min*60000 + s*1000;
		return t;
	}
	
	private int getNextColon(String s, int i) {
	    if (s == null) {
	        return -1;
	    }

	    int colon = s.indexOf(":", i);
	    return colon;
	    
	}

	private boolean hasFraction(String s) {
	    if (s.indexOf(".") != -1) {
	        return true;
	    }
	    return false;
	}
	
	/**
	 * Returns millisec time.
	 * @param t   Time string
	 * @param unit   Number of chars in unit (s,h,min)
	 */
	private long handleFraction(String t, int nu) {
		// Add zeros and cut of at 3.
		String time = t.substring(0, t.length()-nu)  + "000";
	    int dot       = time.indexOf(".");
		int lastDigit = dot+4;

		return Integer.parseInt(time.substring(dot + 1, lastDigit));
		
	}
			
	private long handleMilliseconds(String time) {
	    long t=0;
	    if (hasFraction(time)) {
			// Values after dot are discarded, nanosecs not supported.
	        t = Integer.parseInt(time.substring(0, time.indexOf(".")));
	    } else {
	        t = Integer.parseInt(time.substring(0, time.length()-2));
	    }
		return t;
	}
	
	private long handleSeconds(String time) {
		long t=0;
	    if (hasFraction(time)) {
	        t = 1000 * Integer.parseInt(time.substring(0, time.indexOf(".")));
			t = t + handleFraction(time, 1);

	    } else {
	        t = 1000 * Integer.parseInt(time.substring(0, time.length()-1));
	    }
		return t;

	}

	private long handleMinutes(String time) {
		long t=0;
		if (hasFraction(time)) {
		    t = 1000 * Integer.parseInt(time.substring(0, time.indexOf(".")));
			t = t + handleFraction(time, 3);
		} else {
		    t = 1000 * Integer.parseInt(time.substring(0, time.length()-3));
		}
		// Convert mins to secs
		t = t * 60;
		return t;
	}
	
	private long handleHours(String time) {
		long t=0;
		if (hasFraction(time)) {
		    t = 1000 * Integer.parseInt(time.substring(0, time.indexOf(".")));
			t = t + handleFraction(time, 1);
		} else {
		    t = 1000 * Integer.parseInt(time.substring(0, time.length()-1));
		}
		// Convert hours to secs
		t = t * 3600;
		return t;
	}

	private int findLastDigit(String s, int start) {
	    int i;
	    for (i = start; i < s.length(); i++) {
	        if (Character.isLetter(s.charAt(i))) {
	            return i;
	        }
	    }
	    return i;
	}
		

	/**
	 * Parse the syncbase clock value string.
	 */
	private void handleSyncbase(String time, int dot) {
		// If SMILDocumentImpl is not passed in, cannot handle syncbase clock values
		if (sDoc == null) {
			return;
		}
			
		// Get the syncbase element
		SMILElement element = sDoc.searchElementWithIdInBody(time.substring(0, dot));
		if (element == null) {
			Log.error("Syncbase element '"+time.substring(0, dot)+"' not found!");
			return;
		}

		// If not a timed element
		if (!(element instanceof ElementBasicTimeImpl)) {
			Log.error("Syncbase element '"+time.substring(0, dot)+"' not correct type!");
			return;
		}
		syncbaseElement = (ElementBasicTimeImpl)element;

		// Check '.begin' or '.end' - spellchecking already made in parseTimeAttribute()
		baseBegin = true;
		if (time.substring(dot, dot+4).equals(".end"))
			baseBegin = false;
		
		// Try getting syncbase element's begin/end time
//		Time syncTime = null;
//		if (baseBegin == true)
//			syncTime = syncbaseElement.getCurrentIntervalBegin();
//		else
//			syncTime = syncbaseElement.getCurrentIntervalEnd();
//		
//		if (syncTime == null || syncTime.getResolved() == false) {
//			// The time is syncbased, but unresolved
//			resolved = false;
//			indefinite = false;
//			Log.debug("Syncbase unresolved '"+syncbaseElement+"' beg:"+baseBegin+" offset:"+timeValue);
//		} else {
//			// It was possible to solve the time
//			resolved = true;
//			indefinite = false;
//			timeValue = syncTime.getResolvedOffset().intValue() + offset;
//			Log.debug("Syncbase resolved '"+syncbaseElement+"' beg:"+baseBegin+" offset:"+timeValue);
//		}
		
		// Add a listener to the syncbase element for time changes.
		//if (baseBegin == true)
		//	syncbaseElement.addIntervalBeginListener(this, dependentElement);
		//else
		//	syncbaseElement.addIntervalEndListener(this, dependentElement);
		
		syncbaseElement.addNewIntervalListener(this, dependentElement);
		Log.debug("Sync Base newIntervallistener for "+dependentElement.getId()+" added for "+baseBegin);
	}

	public void intervalCreated(TimeImpl begin, TimeImpl end)  {
		TimeImpl time;
		if (baseBegin == true)
			time = new TimeImpl(begin.getResolvedOffset().intValue(), offset, SMIL_TIME_SYNC_BASED, syncbaseElement, dependentElement, true, beginList, begin.resolved, begin.indefinite);
		else
			time = new TimeImpl(end.getResolvedOffset().intValue(), offset, SMIL_TIME_SYNC_BASED, syncbaseElement, dependentElement, false, beginList, end.resolved, end.indefinite);
			
		Log.debug("Dependent "+dependentElement.getId()+" creates an time instance "+time.getString());
		if (beginList)
			dependentElement.addTime(true, time);
		else	
			dependentElement.addTime(false, time);
	}

	/**
	 * Parse the event value string.
	 */
	private void handleEventbase(String time, int dot) {
		Element element;
		String event;

		// If SMILDocumentImpl is not passed in, cannot handle event based values
		if (sDoc == null)
			return;
		
		// Get the eventbase element - accept any type of element
		if (dot != -1)  {
			element = sDoc.searchElementWithId(time.substring(0, dot));
			if (element == null) {
				Log.error("EventBase element '"+time.substring(0, dot)+"' not found!");
				throw new NumberFormatException("EventBase element '"+
									time.substring(0, dot)+"' not found!");
			}
			// Get event name after dot
			event = time.substring(dot+1);
		} else  {
			// Use default element : TODO: This may be overridden (e.g. animate target)
			element = dependentElement;
			event = time;
		}

		// Add this TimeImpl as a listener for events in evantbase element - use bubble phase to
		// catch the event on the target element itself.
		((EventTarget)element).addEventListener(event, this, false);
	}

	/**
	 * EventBase event received - restart / end element
	 * This method handles the event, when it is received
	 * THIS SHOULD BE IN ELEMENT- TO SECURE CORRECT click-click BEHAVIOUR!
	 * (in case of exception & wrong event order)
	 */
	public void handleEvent(Event evt)
	{
		//
		Log.debug("SMIL "+dependentElement.getId()+" begin: "+beginList+" got event "+evt.getType()+
					" t: "+(dependentElement.getCurrentParentTime()+offset));
		TimeImpl time = new TimeImpl(dependentElement.getCurrentParentTime(), offset, 
										SMIL_TIME_EVENT_BASED, null, null, false, beginList, resolved, indefinite);
		// Event Sensitivity
		Node node = dependentElement.getParentNode();
		ElementBasicTimeImpl parent;
		if (node instanceof ElementBasicTimeImpl)  {
			parent = (ElementBasicTimeImpl)node;
			if (parent.isActive() == false)
				return;
		}

		// Event sensitivity point 3. not completely implemented.
		short restart = dependentElement.getRestart();
		boolean active = dependentElement.isActive();
		if (beginList)  {
			if (active == false || (active == true && restart==XElementBasicTime.RESTART_ALWAYS))  {
				dependentElement.addTime(true, time);
			}
		} else  {
			if (active == true)  {
				dependentElement.addTime(false, time);
			}
		}
	}

	// Time Instance Stuff
	
	public void intervalBeginChanged(TimeImpl begin)  {
		Log.debug("Interval Begin Changed: "+dependentElement.getId()+" "+beginList);
		// Locked?
		if (locked == true)  {
			Log.debug("LOCKED!");
			return;
		}

		setTimeValue(begin);
		Log.debug(" Got nonnull notification - begin/end changed to "+this.getString());

		// Reevaluate begin or end
		if (beginList == true)
			dependentElement.reevaluateIntervalBegin(this);
		else	
			dependentElement.reevaluateIntervalEnd(this);
	}

	public void intervalEndChanged(TimeImpl end)  {
		Log.debug("Interval End Changed: "+dependentElement.getId()+" "+beginList);
		// Locked?
		if (locked == true)  {
			Log.debug("LOCKED!");
			return;
		}

		setTimeValue(end);
		Log.debug(" Got nonnull notification - begin/end changed to "+this.getString());

		// Reevaluate begin or end
		if (beginList == true)
			dependentElement.reevaluateIntervalBegin(this);
		else	
			dependentElement.reevaluateIntervalEnd(this);
	}
	
}

