/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;


/**
 *  This interface define the set of animation target extensions. 
 */
public abstract class ElementTargetAttributesImpl implements ElementTargetAttributes {
    /**
     *  The name of the target attribute. 
     */
	public String getAttributeName() {
			return null;
	}
	
	public void setAttributeName(String attributeName) {
		return;
	}

    // attributeTypes
    public static final short ATTRIBUTE_TYPE_AUTO       = 0;
    public static final short ATTRIBUTE_TYPE_CSS        = 1;
    public static final short ATTRIBUTE_TYPE_XML        = 2;

    /**
     *  A code representing the value of the  attributeType attribute, as 
     * defined above. Default value is <code>ATTRIBUTE_TYPE_CODE</code> . 
     */
	public short getAttributeType() {
		return ATTRIBUTE_TYPE_AUTO;
	}
	public void setAttributeType(short attributeType) {
		return;
	}

}

