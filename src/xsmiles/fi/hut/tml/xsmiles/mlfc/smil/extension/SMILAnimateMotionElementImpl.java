/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  AnimateMotion Element. This element animates a pair of attributes.
 * These attributes have been hard-coded to be "left" and "top".
 * The attributes are read in as string "(left, top)" and animated
 * with AnimatedPairValue. Finally, they are written back to the
 * DOM by splitting the string into "left" and "top" attributes.
 */
public class SMILAnimateMotionElementImpl extends SMILAnimateElementImpl 
	implements SMILAnimateMotionElement {

	/**
	 * Constructor - set the owner
	 */
	public SMILAnimateMotionElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

	/**
	 * Parses the attribute value. 
	 * Assumes attribute format "(123, 321)",
	 * and returns AnimatedPairValue.
	 * @param value		Attribute value to be parsed
	 * @return 			AnimatedPairValue of the attribute
	 */
	public AnimatedValue parse(String value)  {
		return new AnimatedPairValue(value);
	}

	/**
	 * Parses the "left" and "top" attributes of the target element,
	 * and returns AnimatedPairValue(left, top).
	 *
	 * @param scheduler	AnimationScheduler, this object has the DOM and "DOM buffer" values.
	 * @return 		AnimatedPairValue of the target attributes
	 */
	public AnimatedValue parse(AnimationScheduler scheduler)  {
		String left = scheduler.getAnimAttribute(target, "left");
		String top = scheduler.getAnimAttribute(target, "top");
		return new AnimatedPairValue("("+left+","+top+")");
	}
	
	/**
	 * Parses the integer val. Returns always AnimatedPairValue with x=0 and y=0.
	 * @param val	Attribute value as integer
	 * @return 		AnimatedPairValue(0, 0)
	 */
	public AnimatedValue parse(int val)  {
		return new AnimatedPairValue(0, 0);
	}

	/**
	 * Returns an empty array of AnimatedPairValues.
	 * @param size	Size of the array
	 * @return 		Array AnimatedPairValue[size]
	 */
	public AnimatedValue[] initArray(int size)  {
		return new AnimatedPairValue[size];
	}

	/**
	 * This is used to write the animated value back the DOM buffer.
	 * This splits the string into left and top coords and
	 * sets it to the target element in the "DOM buffer".
	 *
	 * @param scheduler		AnimationScheduler, holding "DOM buffer"
	 * @param value			String value to be set to the DOM buffer
	 */
	public void write(AnimationScheduler scheduler, String value) {
		AnimatedPairValue pair;
		try {
			pair = new AnimatedPairValue(value);
		} catch (NumberFormatException e) {
			// Survive if something has gone wrong
			return;
		}
		scheduler.setAnimAttribute(target, "left", String.valueOf((int)pair.getX()));
		scheduler.setAnimAttribute(target, "top", String.valueOf((int)pair.getY()));
	}

    /**
     *  Specifies the curve that describes the attribute value as a function 
     * of time.  Check with the SVG spec for better support 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getPath() {
		return null;
	}
	public void setPath(String path) throws DOMException {
		return;
	}

    /**
     *  Specifies the origin of motion for the animation. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getOrigin() {
		return null;
	}
	public void setOrigin(String origin) throws DOMException {
		return;
	}

}

