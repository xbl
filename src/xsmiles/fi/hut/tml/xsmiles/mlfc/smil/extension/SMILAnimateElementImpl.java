/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Vector;
import java.util.Enumeration;

/**
 *  AnimateElement class.
 */
public class SMILAnimateElementImpl extends SMILAnimationImpl implements SMILAnimateElement {
	/**
	 * Constructor - set the owner
	 */
	public SMILAnimateElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

	/**
	 * Parses the attribute value. 
	 * Assumes attribute format "123",
	 * and returns AnimatedIntegerValue.
	 * @param value		Attribute value to be parsed
	 * @return 			AnimatedIntegerValue of the attribute
	 */
	public AnimatedValue parse(String value)  {
		return new AnimatedIntegerValue(value);
	}

	/**
	 * Parses the attribute value of the target attribute. 
	 * Assumes attribute format "x",
	 * and returns AnimatedIntegerValue.
	 * @param scheduler	AnimationScheduler, this object has the DOM and "DOM buffer" values.
	 * @return 		AnimatedIntegerValue of the target attribute
	 */
	public AnimatedValue parse(AnimationScheduler scheduler)  {
		String value = scheduler.getAnimAttribute(target, attributeName);
		return new AnimatedIntegerValue(value);
	}
	
	/**
	 * Parses the integer val. Returns always AnimatedIntegerValue.
	 * @param val	Attribute value as integer
	 * @return 		AnimatedIntegerValue(val)
	 */
	public AnimatedValue parse(int val)  {
		return new AnimatedIntegerValue(val);
	}
	
	/**
	 * Abstract method to be overridden in the implementing class.
	 * This is used to write the animated value back the DOM buffer.
	 * This converts AnimatedValue to string and sets it to the
	 * target element in the "DOM buffer".
	 * @param scheduler		AnimationScheduler, holding "DOM buffer"
	 * @param value			Value to be set to the DOM buffer
	 */
	public void write(AnimationScheduler scheduler, AnimatedValue value) {
	}
	
	/**
	 * Returns an empty array of AnimatedIntegerValues.
	 * @param size	Size of the array
	 * @return 		Array AnimatedIntegerValue[size]
	 */
	public AnimatedValue[] initArray(int size)  {
		return new AnimatedIntegerValue[size];
	}

}

