/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  This interface defines a time container with semantics based upon par, but 
 * with the additional constraint that only one child element may play at a 
 * time. 
 */
public class  ElementExclusiveTimeContainerImpl extends ElementTimeContainerImpl
		implements ElementExclusiveTimeContainer {

	/**
	 * Constructor - Set the owner and name.
	 */
	public ElementExclusiveTimeContainerImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}

    /**
     *  Controls the end of the container.  Need to address thr id-ref value. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
	public String getEndSync() {
		return null;
	}
	public void setEndSync(String endSync) throws DOMException {
		return;
	}

    /**
     *  This should support another method to get the ordered collection of 
     * paused elements (the paused stack) at a given point in time. 
     * @return  All paused elements at the current time. 
     */
	public NodeList getPausedElements() {
		return null;
	}

	/**
	 * Default begin value 
	 * For media, par and seq this is 0s, for excl this is indefinite.
	 * @return	Time string
	 */
	protected String defaultBegin() {
		return "indefinite";
	}

	public void childEnded(long dur) {
	}
}

