/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;
import org.w3c.dom.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.util.Hashtable;
import java.util.Enumeration;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.FrameListener;

/**
 *  Declares layout properties for the top-layout element. See the  top-layout 
 * element definition . 
 *
 * Includes attributes:
 *  o 
 *  + open, close
 */
public class SMILTopLayoutElementImpl extends SMILElementImpl 
								implements SMILTopLayoutElement, LayoutCalc, FrameListener {

	// Regions added to this region (key: RegionElementImpl, value: nonsense)
	private Hashtable childRegions = null;

	// Parent region. Used to set parent region visible/invisible when media is added/removed.
	// null if no parent or the parent is root-layout (which is always visible)
	private SMILRegionElementImpl parentRegion = null;	

	// This region's own drawingarea
	private DrawingArea drawingArea = null;
	
	// Number of child Regions in this region
	private int regionCount = 0;
	
	// State of the frame - opened or closed. Used to track event dispatching.
	private boolean opened = false;

	/**
	 * Constructor - set the owner
	 */
	public SMILTopLayoutElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
	    super(owner, smil, ns, "topLayout");
	    childRegions = new Hashtable();
	}

	/**
	 * Initializes this region and its associated DrawingArea. This is only called 
	 * for regions under a valid layout element.
	 */
	public void initHead() {
		NodeList children = getChildNodes();
		Node e;

		// Create a drawing area for this region.
		if (drawingArea == null) {

			// Get a toplayout
			drawingArea = getSMILDoc().getViewer().getNewDrawingArea(DrawingArea.TOPLAYOUT, false);
			// Get title attribute as the frame title, or the SMIL presentation title
			String title = getTitle();
			if (title == null || title.length() == 0)
				drawingArea.setTitle(getSMILDoc().getViewer().getTitle());
			else	
				drawingArea.setTitle(title);

			// Set the framelistener, to dispatch events if the user closes the frame.
			drawingArea.addFrameListener(this);

			SMILLayoutElement layout = getSMILDoc().getDocLayout();
			drawingArea.setBounds(calcLeft(), calcTop(), calcRight() - calcLeft(), calcBottom() - calcTop());
			drawingArea.setBackgroundColor(getBackgroundColor());

//			Log.debug(getId()+" left:"+calcLeft()+" width:"+(calcRight() - calcLeft())+" right:"+calcRight());

			// Set visible if always visible...
			if (getOpen() == OPEN_ONSTART)  {
				getDrawingArea().setVisible(true);
				frameOpened();
	//			if (parentRegion != null)
	//				parentRegion.addRegion(this);
			}

		}

		// Init other elements (regions) - these require rootLayoutElement != null.
		// Initializes only region with correct systemAttributes
		for (int i=0 ; i < children.getLength() ; i++) {
			e = children.item(i);
			if (e instanceof SMILElement) {
				// Test the Inline systemAttributes
				if (AttributeHandler.evaluateSystemValues((SMILElement)e, getSMILDoc().getViewer(), true) == true) {
					if (e instanceof SMILRegionElement) {
						((SMILRegionElementImpl)e).initHead();
					} else {
						// Init all SMIL elements with ok systemAttributes	
						((XSmilesElementImpl)e).init();
					}
				}
			} else if (e instanceof XSmilesElementImpl)
				((XSmilesElementImpl)e).init();
		}
	}

	/**
	 * Destroy the frame of this top-layout.
	 */
	public void destroy()  {
		// remove the frame
		getDrawingArea().setVisible(false);
		drawingArea = null;
		
		super.destroy();
	}

	/** 
	 * Return drawing area.
	 */
	 public DrawingArea getDrawingArea() {
	 	return drawingArea;
	 }

	// These are ElementLayout interface

	public String getBackgroundColor() {
		String color = getAttribute("backgroundColor");
		// if not found, then try to find the deprecated attribute
		if (color == null || color.length() == 0) {
			color = getAttribute("background-color");
			if (color == null || color.length() == 0)
				return null;
		}
			
		return color;
	}
	public void setBackgroundColor(String backgroundColor) throws DOMException {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Height of the root-layout - can only have absolute values.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getHeight() {
		String height = getAttribute("height");
		if (height == null || height.length() == 0)
			return null;
			
		return height;
	}
	public void setHeight(int height) throws DOMException {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Width of the root-layout - can only have absolute values.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getWidth() {
		String width = getAttribute("width");
		if (width == null || width.length() == 0)
			return null;
			
		return width;
	}
	public void setWidth(int width)  throws DOMException {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * open attribute.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getOpen()  {
		String open = getAttribute("open");
		if (open.equals("whenActive")) 
			return OPEN_WHENACTIVE;
		else
			return OPEN_ONSTART; // default value
	}
	public void setOpen(short open) throws DOMException  {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * open attribute.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getClose()  {
		String close = getAttribute("close");
		if (close.equals("whenNotActive")) 
			return CLOSE_WHENNOTACTIVE;
		else
			return CLOSE_ONREQUEST; // default value
	}
	public void setClose(short close) throws DOMException  {
	    throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	////// LayoutCalc interface, to get the size of this region //////

	/**
	 * Set DrawingArea visible/invisible, and add/remove this region to the parent region.
	 * Effective only, if showBackground = SHOWBACKGROUND_WHENACTIVE.
	 * @param flag		true=visible, false=invisible
	 */
	private void setDrawingAreaVisible(boolean flag)  {

		if (flag == true)  {
			if (regionCount  == 1 && getOpen() == OPEN_WHENACTIVE)  {
						getDrawingArea().setVisible(true);
						// Dispatch event
						frameOpened();
//							if (parentRegion != null)
//								parentRegion.addRegion(this);
			}	
		} else  {
			// If no more regions visible, and presentation is allowed to close this top layout.
			if (regionCount  == 0 && getClose() == CLOSE_WHENNOTACTIVE)  {
						getDrawingArea().setVisible(false);
						// Dispatch event
						frameClosed();
	//							if (parentRegion != null)
	//								parentRegion.removeRegion(this);
			}	
		}
	}

	/**
	 * Add region to this region. This will cause the drawingarea to become visible.
	 */
	public void addRegion(SMILRegionElementImpl region)  {
		if (region == null)
			return;
		
		// If not yet added.. add it!
		if (childRegions.get(region) == null)  {
			childRegions.put(region, "krsmilp");
			regionCount++;
			setDrawingAreaVisible(true);
		}
	}

	/**
	 * Remove region from this region. This may cause the drawingarea to become invisible,
	 * if this was the last region, and no medias either in the region.
	 */
	public void removeRegion(SMILRegionElementImpl region)  {
		if (region == null)
			return;
		
		// If not yet removed... check if this region should become invisible.
		if (childRegions.remove(region) != null)  {
			regionCount--;
			setDrawingAreaVisible(false);
		}
	}

	/**
	 * Returns zero as the root-layout top coord.
	 */
	public int calcTop() {
	 	return 0;
	}

	/**
	 * Returns zero as the root-layout left coord.
	 */
	public int calcLeft() {
		return 0;
	}

	/**
	 * Returns the top layout width.
	 */
	public int calcRight() {
	 	return convert(getWidth());
	}

	/**
	 * Returns the top layout height.
	 */
	public int calcBottom() {
		return convert(getHeight());
	}

	/**
	 * Converts a string to int.
	 */
	private int convert(String str)  {
		str = trimAttr(str);
		if (str == null || str.length() == 0)
			return 0;

		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * Removes the "px" ending in length attributes.
	 */
	private String trimAttr(String value) {
		if (value == null || value.length() == 0)
			return null;
			
		// Strip off the pixel unit
		if (value.endsWith("px")) {
			value = value.substring(0, value.length()-2);
		}
		return value;
	}

	/**
	 * Frame opened by presentation.
	 */
	public void frameOpened()  {
		opened = true;
		Log.debug("DISPATCHING TOPLAYOUT OPENED EVENT.");
		dispatch("topLayoutOpenEvent", false);	
	}

	/**
	 * Frame closed by user or presentation.
	 */
	public void frameClosed()  {
		if (opened == true)  {
			Log.debug("DISPATCHING TOPLAYOUT CLOSED EVENT.");
			dispatch("topLayoutCloseEvent", false);	
			opened = false;
		}
	}
}

