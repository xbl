/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;
import java.awt.Container;

import javax.media.Manager;
import javax.swing.*;
import javax.swing.border.Border;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.*;

/**
 *  
 */
public class SwingDecorator implements Decorator {

	public Container createRootLayout() {
		// Set the root layout drawing area
		// This MLFC has scrollbars.

		Container rootLayout = new JLayeredPane();
		((JLayeredPane) rootLayout).setOpaque(false);
		return rootLayout;
	}
	// The MLFC has to take care of the scroll bars...
	public Container createScrollPanel_noscrollBars(Container rootLayout) {
		return rootLayout;
	}
	// The MLFC has to take care of the scroll bars...
	public Container createScrollPanel(Container rootLayout) {
		JScrollPane scrollPanel = new JScrollPane(rootLayout);

		// Default transparent background color
		scrollPanel.setOpaque(false);
		scrollPanel.getViewport().setOpaque(false);
		return scrollPanel;
	}
	public void createSecondaryBorders(Container rootLayout, Container scrollPanel) {
		Border empty = BorderFactory.createEmptyBorder();
		((JLayeredPane) rootLayout).setBorder(empty);
		((JComponent) scrollPanel).setBorder(empty);
	}

	public void addToContainer(Container scrollPanel, Container container) {
		if (container instanceof JFrame) {
			Container bp = ((JFrame) container).getContentPane();
			bp.add(scrollPanel, 0);
			bp.validate();
			bp.setVisible(true);
		} else {
			container.add(scrollPanel, 0);
		}
	}

	/**
	 * Returns a new MediaHandler for SMIL core logic.
	 */
	public MediaHandler getNewMediaHandler() {
		// Pass XMLDocument for ExtensionMedia
		SMILMLFCMediaHandler mh = new SMILMLFCMediaHandler();
		return mh;
	}

	/**
	 * Returns a new BrushHandler for SMIL core logic.
	 */
	public BrushHandler getNewBrushHandler(Viewer v) {
		SMILMLFCBrushHandler bh = new SMILMLFCBrushHandler();
		bh.setViewer(v);
		return bh;
	}

	/**
	 * Returns a new LinkHandler for SMIL core logic.
	 */
	public LinkHandler getNewLinkHandler() {
		LinkHandler lh = new SMILMLFCLinkHandler();
		return lh;
	}

	/**
	 * Returns a new DrawingArea for SMIL core logic.
	 * 
	 * @param type
	 *                  ROOTLAYOUT for the broswer container, TOPLAYOUT for a new
	 *                  frame.
	 * @param block
	 *                  CSS: used to create a JBlockPanel (true) instead of JPanel
	 *                  (false)
	 */
	public DrawingArea getNewDrawingArea(int type, boolean block, Container rootLayout, boolean layoutModel) {
		// CSS: Create a DrawingArea with the CSS layout information
		// It will cause the area to have a flow layout...
		if (type == DrawingArea.ROOTLAYOUT)
			return new SMILMLFCDrawingArea(rootLayout, layoutModel);
		else
			return new SMILMLFCDrawingArea(type, layoutModel, block);
	}

	/**
	 * Returns a new ForeignHandler for SMIL core logic.
	 */
	public MediaHandler getNewForeignHandler(Element e) {
		MediaHandler mh = new SMILMLFCForeignHandler(e);
		return mh;
	}

	/**
	 * Check if JMF class is available. At the same time, set the hint...
	 * 
	 * @return true if JMF is available, false otherwise
	 */
	public boolean isJMFAvailable(MLFCListener listener) {
		try {
			Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true));
			// If we are in lightweight mode, then enable modular
			// plugin-players
			if (listener.getProperty("media/lightweight").equals("true")) {
				Manager.setHint(Manager.PLUGIN_PLAYER, new Boolean(true));
			} else {
				Manager.setHint(Manager.PLUGIN_PLAYER, new Boolean(false));
			}
		} catch (Throwable e) {
			Log.info("JMF not available! It is recommended to have it installed.");
			Log.info("Video will not be played.");
			return false;
		}
		return true;
	}
}
