/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import java.awt.*;

/**
 * A Component that by default displays nothing. The Paint method
 * may be overridden for debugging purposes.
 * This class is used to display links.
 */
public class InvisibleComponent extends Component {

    public InvisibleComponent() {
    }
    
    public void paint(Graphics g) {
    // do nothing
    
    // for debugging - draws the borders of the component.
//     g.setColor(Color.blue);
//     g.drawRect(0, 0, getWidth()-1, getHeight()-1);
    }
}