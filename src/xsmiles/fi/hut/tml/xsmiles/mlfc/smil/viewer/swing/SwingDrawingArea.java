/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.FrameListener;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;
import fi.hut.tml.xsmiles.Log;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.w3c.dom.Element;

/**
 *  Class for Swing drawing area. 
 */
public class SwingDrawingArea implements DrawingArea {
	// Container having the drawing area (a Frame or Container)
	protected Container container;

	// Content Container contains the Components. This is the same as container, 
	// except for JScrollPane
	protected Container contentContainer;

	// The left and top coords of the drawing area. 
	// Default values zero for rootlayout and toplayout.
	private int left = 0, top = 0;

	// The width and height of the drawing area	
	private int width = 0, height = 0;

	// Close mode, true=close only if user requests
	private boolean closeMode = false;
	
	// Frame Listener - to know when frame has been closed
	private FrameListener frameListener = null;

	/**
	 * Creates a new drawing area of type ROOTLAYOUT, TOPLAYOUT or REGION.
	 */
	public SwingDrawingArea(int type) {
		if (type == DrawingArea.ROOTLAYOUT) {
			Log.error("ROOT LAYOUT INCORRECT CREATION");
		} else if (type == DrawingArea.TOPLAYOUT) {
			JFrame frame = new JFrame("The X-Smiles Browser"); // default top-layout title

			// Default transparent background color	- also z-indexing enabled panel
			JLayeredPane lp = new JLayeredPane();
			lp.setOpaque(false);
			lp.setVisible(true);

			frame.getContentPane().add(lp);	

			container = frame;	
			contentContainer = lp; //frame.getContentPane();	

			// frame.getContentPane().setLayout(null);
		//	frame.getContentPane().setLayout(null);
			// Default transparent background color		
//			frame.setOpaque(false);
			// Default not visible
			frame.setVisible(false);
			
			// Add close listener, to send events
			frame.addWindowListener(new JFListener());
		} else if (type == DrawingArea.REGION) {
			JPanel panel = new JPanel();
//			Container panel = new Container();
			// Use absolute coords
			panel.setLayout(null);
			container = panel;
			contentContainer = panel;	
			// Default transparent background color		
			panel.setOpaque(false);
			// Default not visible
			panel.setVisible(false);
			
			// This is to get a region with scroll bars
		} else if (type == DrawingArea.REGIONSCROLL) {
			// Add the components into this one
			JPanel panel = new JPanel();
			// This is added to the rootLayout and has a size
			JScrollPane scroll = new JScrollPane(panel);
			// Use layout that works with ScrollPane
			panel.setLayout(new FlowLayout(0,0,0));
			// Get rid of borders coming thanks to FlowLayout
			Border empty = BorderFactory.createEmptyBorder();
			panel.setBorder(empty);
			// Get rid of borders coming thanks to ScrollLayout
			scroll.setBorder(empty);
//			empty = BorderFactory.createRaisedBevelBorder();
//			scroll.setViewportBorder(empty);
//			scroll.getViewport().setBorder(empty);
//			scroll.getViewport().setLayout(null);
			
			container = scroll;
			contentContainer = panel;	
			// Default transparent background color		
			panel.setOpaque(false);
			scroll.setOpaque(false);
			scroll.getViewport().setOpaque(false);

			panel.setVisible(true);
			// Default not visible
			scroll.setVisible(false);
		}
	}
	
	/**
	 * Creates a new drawing area using container c. Used for root-layout.
	 * @param c		Container
	 */
	public SwingDrawingArea(Container c) {
		container = c;
		container.setLayout(null);

		contentContainer = c;
		if (contentContainer instanceof JFrame)
			contentContainer = ((JFrame)container).getContentPane();
	}

	public int getLeft() {
		return left;
	}

	public int getTop() {
		return top;
	}
		
	public int getWidth() {
		return width;
	}
		
	public int getHeight() {
		return height;
	}

	public void setBounds(int x, int y, int w, int h) {
		//
		left = x;
		top = y;
		width = w;
		height = h;

		container.setLocation(left, top);
		container.setSize(width, height);

		// Also call setPreferredSize, because of the JScrollPanel in SMILMLFC
		if (container instanceof JComponent) 
			((JComponent)container).setPreferredSize(new Dimension(width, height));

		// Validate to update the scroll bars
		container.validate();	
	}

	/**
	 * Add a region to this drawing area. A region can be added to root-layout,
	 * topLayout, or another region.
	 * @param region		Region's drawing area to be added.
	 */
	public void addRegion(DrawingArea region, int zindex) {
		// Assumes everything is SwingDrawingArea... add to top of other regions with same z-index
		contentContainer.add(((SwingDrawingArea)region).getContainer(), new Integer(zindex), -1); 
					// css this was zero before css set it to -1
	} 

	/**
	 * Bring this region into front of other regions
	 * @param region		Region to be the top most
	 */
	public void bringToFront(DrawingArea region) {
		if (contentContainer instanceof JLayeredPane)
			((JLayeredPane)contentContainer).moveToFront(((SwingDrawingArea)region).getContainer());
	}

	public void setBackgroundColor(String color) {

		// Special case transparent. Java fails to use alpha color there above.
		if (color == null || color.equals("transparent")) {
			if (container instanceof JComponent)
				((JComponent)container).setOpaque(false);
			if (container instanceof JFrame)
				((JFrame)container).getContentPane().setBackground(Color.lightGray);
			return;
		}
	
		Color c = ColorConverter.hexaToRgb(color);
		if (container instanceof JFrame)
			((JFrame)container).getContentPane().setBackground(c);
		else {
			container.setBackground(c);
			if (container instanceof JComponent) {
				// Default opaque background color off
				((JComponent)container).setOpaque(true);
			}
		}
	}
	
	/**
	 * Set this drawing area visible. Won't set the area invisible, if close mode is true.
	 * @param v    true=visible, false=invisible
	 */
	public void setVisible(boolean v) {
		// Don't close the frame automatically, if the onRequest flag is on.
		if (v == false && closeMode == true)
			return;
			
		container.setVisible(v);
		container.doLayout();
	}

	/**
	 * Set the title of this area (used for top-layout)
	 * @param name		Title name of the area
	 */
	public void setTitle(String name)  {
		if (container instanceof JFrame && name != null)  {
			((JFrame)container).setTitle(name);
		}
	}

	/**
	 * For toplayouts (frames), add a close listener. Currently supports only one listener.
	 * @param FrameListener		listener for closing events.
	 */
	public void addFrameListener(FrameListener frameListener)  {
		this.frameListener = frameListener;
	}

	/**
	 * This is a Swing specific method to return the container to add this to other regions.
	 * Returns either the container or container.getContentPane().
	 * MediaHandler calls this to obtain the container of this DrawingArea.
	 *
	 * @return		Container to draw media.
	 */
	public Container getContainer() {
		if (container instanceof JFrame)
			return ((JFrame)container).getContentPane();
			
		return container;	
	}

	/**
	 * This is a Swing specific method to return the container to draw media.
	 * Returns either the container or container.getContentPane().
	 * MediaHandler calls this to obtain the container of this DrawingArea.
	 *
	 * @return		Container to draw media.
	 */
	public Container getContentContainer() {
		//if (contentContainer instanceof JFrame)
		//	return ((JFrame)contentContainer).getContentPane();
			
		return contentContainer;	
	}

	/**
	 * Handle toplayout window closing.
	 */
	private class JFListener extends WindowAdapter  {
		public void windowClosing(WindowEvent e)  {
			Log.debug("CLOLSOE SCLOSE");
			if (frameListener != null)
				frameListener.frameClosed();
		}
	}
        
        public boolean setCSSStretch(String awidth, String aheight, Element origElem)
        {
            return false;
        }

}

