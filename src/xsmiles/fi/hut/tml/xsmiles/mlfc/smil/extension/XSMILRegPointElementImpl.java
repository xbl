/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  Declares layout type for the document. See the  LAYOUT element definition .
 *  
 */
public abstract class XSMILRegPointElementImpl extends SMILElementImpl implements XSMILRegPointElement {

	public XSMILRegPointElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

}