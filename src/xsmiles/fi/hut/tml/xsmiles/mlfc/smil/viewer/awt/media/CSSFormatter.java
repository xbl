/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media;

import java.awt.Component;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import java.net.URL;

/**
 * This is the interface for media.
 */
public interface CSSFormatter {
	public void setStyle(String s);
	public void formatComponent(Component comp);
}