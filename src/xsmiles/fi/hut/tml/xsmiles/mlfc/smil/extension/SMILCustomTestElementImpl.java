/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.Log;

/**
 *  Declares custom attributes element.
 */
public class SMILCustomTestElementImpl extends SMILElementImpl implements XSMILCustomTestElement {

	/**
	* Constructor with owner doc
	*/
	public SMILCustomTestElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "customTest");
	}

	/**
	 * defaultState
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public boolean getDefaultState()  {
		String val = getAttribute("defaultState");
		if (val != null && val.equals("true"))
			return true;
		
		return false;	
	}
	
	public void setDefaultState(boolean defaultState)  {
		if (defaultState == true)
			setAttribute("defaultState", "true");
		else
			setAttribute("defaultState", "false");
	}

	/**
	 * override
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getOverride()  {
		return 0;
	}
	public void setOverride(short override) throws DOMException  {
		return;
	}

	/**
	 * uid
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getUid()  {
		return getAttribute("uid");
	}
	public void setUid(String uid) throws DOMException  {
		if (uid != null && uid.length() > 0) {
		  setAttribute("uid", uid);
		} else {
		  removeAttribute("uid");
		}
	}
		

}
