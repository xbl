/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

import org.w3c.dom.smil20.SMILDocument;

/**
 *  Interface to link. 
 */
public interface LinkHandler extends MediaHandler {

	public void setViewer(Viewer viewer);
	
	/**
	 * Sets the link title
	 */
	public void setTitle(String title);
	
}

