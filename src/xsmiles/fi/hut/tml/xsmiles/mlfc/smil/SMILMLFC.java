/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Element;
import org.w3c.dom.smil20.SMILDocument;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.smil.basic.ElementBasicTimeImpl;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Decorator;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.util.EventUtil;

// This will import JMF Manager for JMF availability test
//import javax.media.Manager;

/**
 * SMILMLFC is the SMIL Viewer for X-Smiles browser. It controls the SMIL core
 * functionality and handles the data exchange between the SMIL player and the broswer.
 * This is also a factory for MLFC specific MediaHandlers,
 * LinkHandlers and DrawingAreas.
 */
public class SMILMLFC extends MLFC implements Viewer
{
    // GUI buttons
    XButton play, stop;
    ActionListener playStopListener;
    boolean playing=false;
    
    // Container for main drawing area in the browser
    private Container rootLayout = null;
    
    // SMIL DOM Document
    private SMILDocumentImpl smilDoc = null;
    
    // JMF availability
    private boolean jmfAvailable;
    
    // Browser Container
    private Container scrollPanel;
    
    // Performance tracking/debugging...
    public static long initTime, initMem;
    public static long startTime, startMem;
    
    // Vector of parasite smil elements
    private Vector parasiteElements;
    
    protected Decorator decorator;

    /** the decorator class name, this is changed e.g. by the HAVI SMIL viewer*/
    public static String decorClassName = "fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingDecorator";
    
    // Title of the presentation, viewed in the window title
    String frameTitle = null;
    
    /**
     * Constructor.
     */
    public SMILMLFC()
    {
	        Log.debug("SMILMLFC()");
	        this.createDecorator();
	        smilDoc = new SMILDocumentImpl();
	        smilDoc.setViewer(this);
	        
	        //		// !!!!PERFORMANCE TRACKING!!!!
	        //		System.gc();
	        //		initTime = System.currentTimeMillis();
	        //		initMem =Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
	        //		System.out.println("SMILMLFC INIT TIME: "+SMILMLFC.initTime+"ms MEMORY: "+(double)SMILMLFC.initMem/1000+"k");
	        //		// !!!!PERFORMANCE TRACKING!!!!
	        
	        
	        // Set the root layout drawing area
	        // This MLFC has scrollbars.
	        //rootLayout = new JLayeredPane();
	        
	        rootLayout = decorator.createRootLayout();
	        
	        // The MLFC has to take care of the scroll bars...
	        scrollPanel = decorator.createScrollPanel(rootLayout);
	        
	        /*
	        scrollPanel = new JScrollPane(s);
	        
	        // Default transparent background color
	        ((JLayeredPane)rootLayout).setOpaque(false);
	        scrollPanel.setOpaque(false);
	        scrollPanel.getViewport().setOpaque(false);
	         **/
	        
	        // List of parasite elements
	        parasiteElements = new Vector();
    }
    
    protected void createDecorator()
    {
        //String decorClassName = "fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingDecorator";
        try
        {
            Class c = Class.forName(decorClassName);
            this.decorator=(Decorator)c.newInstance();
        } catch (Throwable t)
        {
            Log.error(t,"Could not instantiate "+decorClassName+". SMIL will not work");
        }
    }
    
    /**
     * Get the version of the MLFC. This version number is updated
     * with the browser version number at compilation time. This version number
     * indicates the browser version this MLFC was compiled with and should be run with.
     * @return 	MLFC version number.
     */
    public final String getVersion()
    {
        return Browser.version;
    }
    
    /**
     * Create a DOM element.
     */
    public Element createElementNS(DocumentImpl doc, String URI,String tagname)
    {
        if (isHost() == true)
            return smilDoc.createElementNS(doc, URI,tagname);
        
        Element e = smilDoc.createElementNS(doc, URI,tagname);
        parasiteElements.addElement(e);
        return e;
    }
    
    /**
     * Displays the presentation. Prefetches and plays the presentation.
     */
    public void start()
    {
        playing=true;
        jmfAvailable = isJMFAvailable();
        
        // Is this host or parasite SMIL document?
        if (isHost() == false)
        {
            Log.debug("Parasite SMIL.start() document");
            return;
        }
        
        Log.debug("SMILMLFC.start()");
        
        // Create XButtons
        if (isPrimary())
        {
        	try
			{
	            play = getMLFCListener().getComponentFactory().getXButton("Play","splay.gif");
	            stop = getMLFCListener().getComponentFactory().getXButton("Stop","sstop.gif");
	            XPanel cb = getMLFCListener().getMLFCControls().getMLFCToolBar();
	            cb.add(play);
	            cb.add(stop);
	            playStopListener=new ScaleListener();
	            play.addActionListener(playStopListener);
	            stop.addActionListener(playStopListener);
			} catch (Exception e)
			{
				Log.error(e);
			}
        }
        
        // Secondary documents won't have annoying borders
        if (isPrimary() == false)
        {
            this.decorator.createSecondaryBorders(rootLayout,scrollPanel);
            /*
            Border empty = BorderFactory.createEmptyBorder();
            ((JLayeredPane)rootLayout).setBorder(empty);
            scrollPanel.setBorder(empty);
             */
        }
        
        
        //		// !!!!PERFORMANCE TRACKING!!!!
        //		System.gc();
        //		startTime = System.currentTimeMillis();
        //		startMem =Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
        //		System.out.println("START TIME: "+SMILMLFC.startTime+"ms MEMORY: "+(double)SMILMLFC.startMem/1000+"k");
        //		// !!!!PERFORMANCE TRACKING!!!!
        
        // Add the scrollPanel to the browser's container
        Container container=this.getContainer();
        this.decorator.addToContainer(scrollPanel,container);
        /*
        if (container instanceof JFrame)
        {
            Container bp = ((JFrame)container).getContentPane();
            bp.add(scrollPanel, 0);
            bp.validate();
            bp.setVisible(true);
        }
        else
        {
            container.add(scrollPanel, 0);
        }
         */
        scrollPanel.setVisible(true);
        
        if (!this.isPrimary())
        {
            // Initialize, prefetch and start the presentation.
            startup(this.getXMLDocument());
            return;
        }
        
        // Initialize, prefetch and start the presentation.
        startup(this.getXMLDocument());
    }
    
    private class ScaleListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            String s = ev.getActionCommand();
            
            if (s.equals("Play") && !playing)
            {
                playing=true;
                smilDoc.start();
            }
            if (s.equals("Stop") && playing)
            {
                playing=false;
                smilDoc.stop();
            }
        }
    }
    
    public Decorator getDecorator()
    {
        return this.decorator;
    }
    
    /**
     * Common starter for primary and secondary MLFC.
     */
    public void startup(XMLDocument doc)
    {
        // Initialize the SmilDocumentImpl for X-Smiles
        smilDoc.initialize(true);
        
        // Validate the browser's container to get the root-layout visible.
        this.getContainer().validate();
        
        // Prefetch media
        smilDoc.prefetch();
        
        // Check if smilDoc is null - may happen if someone presses Back-button while
        // prefetching...
        if (smilDoc == null || smilDoc.getDocumentState() == SMILDocumentImpl.ABORTED)
        {
            Log.error("SMIL could not be started - document lost.");
            return;
        }
        
        // Check if there is a internal link: URL#link
        String realURL = getXMLDocument().getXMLURL().toString();
        int delimiter = realURL.lastIndexOf('#');
        if (delimiter != -1)
        {
            // Start the presentation from element
            String startTag = realURL.substring(delimiter+1);
            boolean res = smilDoc.startFromElementName(startTag);
            if (res == true)
                return;
            Log.info("Starting presentation from start.");
        }
        
        // Start the presentation from start
        smilDoc.start();
        
        
        //		// !!!!PERFORMANCE TRACKING!!!!
        //		System.out.println("PREFETCH INIT TIME: +"+((System.currentTimeMillis()-SMILMLFC.initTime))+
        //			"ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILMLFC.initMem))/1000 +"k");
        //		System.out.println("PREFETCH TIME: +"+((System.currentTimeMillis()-SMILMLFC.startTime))+
        //			"ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILMLFC.startMem))/1000 +"k");
        //		System.gc();
        //		System.out.println("PREFETCH GC TIME: +"+((System.currentTimeMillis()-SMILMLFC.startTime))+
        //			"ms MEMORY: +"+((double)(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-SMILMLFC.startMem))/1000 +"k");
        //		// !!!!PERFORMANCE TRACKING!!!!
        
        
    }
    
    /**
     * Deactivate the MLFC. The presentation needs to be stopped.
     */
    public void stop()
    {
        if (play!=null)  play.removeActionListener(playStopListener);
        if (stop!=null)  stop.removeActionListener(playStopListener);
        playStopListener=null;
        playing=false;
        // Destroy parasite timers
        if (isHost() == false)
        {
            Log.debug("SMILMLFC.para-stop()");
            ElementBasicTimeImpl para;
            // Stop animations
            smilDoc.parasiteStop();
            
            for (Enumeration e = parasiteElements.elements() ; e.hasMoreElements() ;)
            {
                try
                {
                    para = (ElementBasicTimeImpl)(e.nextElement());
                    if (para != null)
                    {
                        para.closedown();
                        //para.destroy();
                    }
                } catch (ClassCastException ex)
                {
                }
            }
            return;
        }
        
        // This is host stop:
        Log.debug("SMILMLFC.stop()");
        
        // Remove the play/stop buttons
        MLFCListener list = getMLFCListener();
        if (list!=null)
        {
	        list.getMLFCControls().getMLFCToolBar().removeAll();
        }
        
        if (smilDoc != null)
        {
            // Abort prefetching
            smilDoc.setDocumentState(SMILDocumentImpl.ABORTED);
            // Stop playing
            smilDoc.stop();
            smilDoc.setViewer(null);
            // Free resources
            //      smilDoc.destroy();
        }
        
        Log.debug("SMILMLFC.destroy()");
        // Free resources
        smilDoc.freeResources(true);
        //		smilDoc = null;
        Container container=this.getContainer();
        if (container != null)
            container.removeAll();
        
        if (rootLayout != null)
            rootLayout.removeAll();
        rootLayout = null;
        
        // Secondary MLFC doesn't have scroll bars...
        if (scrollPanel != null)
            scrollPanel.removeAll();
        scrollPanel = null;
        
        
        //    smilDoc = null;
        //
        //    if (container != null)
        //      container.removeAll();
        //
        //    if (rootLayout != null)
        //      rootLayout.removeAll();
        //    rootLayout = null;
        //
        //    document = null;
        //
        //    // Secondary MLFC doesn't have scroll bars...
        //    if (scrollPanel != null)
        //      scrollPanel.removeAll();
        //    scrollPanel = null;
    }
    
    // Base URI for this document - document specific. Should be disabled when
    // xml:base is supported ?
    private URL baseURI = null;
    
    public void setDocumentBaseURI(String base)
    {
        try
        {
            baseURI = new URL(base);
        } catch (java.net.MalformedURLException e)
        {
            Log.error(e);
            return;
        }
    }
    
    /**
     * Append the given URL to be a full URL.
     * @param partURL		Partial URL, e.g. fanfaari.wav
     * @return 				Full URL, e.g. http://www.x-smiles.org/demo/fanfaari.wav
     */
    public URL createURL(String partURL)
    {
        try
        {
            if (baseURI != null)
            {
                return new URL(baseURI,partURL);
                
            } else
                return new URL(this.getXMLDocument().getXMLURL(),partURL);
        } catch (java.net.MalformedURLException e)
        {
            Log.error(e);
            return null;
        }
    }
    
    /**
     * Return the base URL of the document.
     */
    public URL getBaseURL()
    {
        // Overridden base url
        if (baseURI != null)
        {
            return baseURI;
        }
        
        return getXMLDocument().getXMLURL();
    }
    
        /**
     * open a link popup
     */
    public void showLinkPopup(String urlStr,java.awt.event.MouseEvent e)
    {
        URL url = this.createURL(urlStr);
        if (url!=null)
            if (getMLFCListener().getIsTabbedGUI()&&EventUtil.isFollowAlternativeRequest(e))
            {
                getMLFCListener().openInNewTab(new XLink(createURL(urlStr)),null);
            }
            else
            getMLFCListener().showLinkPopup(url,getXMLDocument(),e);
    }

    
    /**
     * Called from the LinkHandler - this method asks the browser to go to this external link.
     * @param url		URL to jump to.
     */
    public void gotoExternalLink(String url)
    {
        Log.debug("...going to "+url);
        URL u = createURL(url);
        getMLFCListener().openLocation(u);
    }
    /**
     * Open external link replacing/opening new target
     * - this method asks the browser to go to this external link.
     * @param url		URL to open
     * @param target	target frame/window
     */
    public void gotoExternalLinkTarget(String url, String target)
    {
        Log.debug("...going to "+url);
        URL u = createURL(url);
        getMLFCListener().openLocationTop(new XLink(u.toString()), target);
    }
    /**
     * Open external link in a new window
     * - this method asks the browser to go to this external link.
     * @param url		URL to open
     */
    public void gotoExternalLinkNewWindow(String url)
    {
        Log.debug("...going to "+url);
        URL u = createURL(url);
        getMLFCListener().openLocationTop(u.toString());
    }
    
    /**
     * Display status text in the broswer. Usually shows the link destination.
     */
    public void displayStatusText(String url)
    {
        getMLFCListener().setStatusText(url);
    }
    
    /**
     * Returns the visible container width.
     */
    public int getWindowWidth()
    {
        if (this.getContainer() != null)
            return this.getContainer().getSize().width;
        else
            return 50;
    }
    
    /**
     * Returns the visible container height.
     */
    public int getWindowHeight()
    {
        if (this.getContainer() != null)
            return this.getContainer().getSize().height;
        else
            return 50;
    }
    
    /**
     * Returns a new MediaHandler for SMIL core logic.
     */
    public MediaHandler getNewMediaHandler()
    {
        // Pass XMLDocument for ExtensionMedia
        /*SMILMLFCMediaHandler mh = new SMILMLFCMediaHandler();
        return mh;*/
        return decorator.getNewMediaHandler();
    }
    
    /**
     * Returns a new BrushHandler for SMIL core logic.
     */
    public BrushHandler getNewBrushHandler()
    {
        return decorator.getNewBrushHandler(this);
        /*
        SMILMLFCBrushHandler bh = new SMILMLFCBrushHandler();
        bh.setViewer(this);
        return bh;*/
    }
    
    /**
     * Returns a new LinkHandler for SMIL core logic.
     */
    public LinkHandler getNewLinkHandler()
    {
        return decorator.getNewLinkHandler();
        /*
        LinkHandler lh = new SMILMLFCLinkHandler();
        return lh;*/
    }
    
    /**
     * Returns a new DrawingArea for SMIL core logic.
     * @param type		ROOTLAYOUT for the broswer container, TOPLAYOUT for a new frame.
     * @param block		CSS: used to create a JBlockPanel (true) instead of JPanel (false)
     */
    public DrawingArea getNewDrawingArea(int type, boolean block)
    {
        return decorator.getNewDrawingArea(type,block, rootLayout,smilDoc.getCSSLayoutModel());
        /*
        // CSS: Create a DrawingArea with the CSS layout information
        // It will cause the area to have a flow layout...
        if (type == DrawingArea.ROOTLAYOUT)
            return new SMILMLFCDrawingArea(rootLayout, smilDoc.getCSSLayoutModel());
        else
            return new SMILMLFCDrawingArea(type, smilDoc.getCSSLayoutModel(), block);
         **/
    }
    
    /**
     * Returns a new ForeignHandler for SMIL core logic.
     */
    public MediaHandler getNewForeignHandler(Element e)
    {
        return decorator.getNewForeignHandler(e);
        /*
        MediaHandler mh = new SMILMLFCForeignHandler(e);
        return mh;
         */
    }
    
    /**
     * Returns the SMILDocument (SMIL-DOM).
     */
    public SMILDocument getSMILDoc()
    {
        return smilDoc;
    }
    
    /**
     * Adds a timepoint, which can be used to jump to some specific location in the
     * presentation. The timepoints are actually the element ids in the XML file.
     * This method is called for every element in the body section.
     *
     * @param elementId		TImepoint, actually the id of the element
     */
    public void addTimePoint(String elementId)
    {
        return;
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemBitrate()
    {
        return getMLFCListener().getProperty("smil/systemBitrate");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemCaptions()
    {
        return getMLFCListener().getProperty("smil/systemCaptions");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemLanguage()
    {
        return getMLFCListener().getProperty("smil/systemLanguage");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemOverdubOrCaption()
    {
        return getMLFCListener().getProperty("smil/system-overdub-or-caption");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public boolean getSystemRequired(String url)
    {
        return getMLFCListener().isNamespaceSupported(url);
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public int getSystemScreenWidth()
    {
        return Integer.parseInt(getMLFCListener().getGUIProperty("displayWidth"));
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public int getSystemScreenHeight()
    {
        return Integer.parseInt(getMLFCListener().getGUIProperty("displayHeight"));
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public int getSystemScreenDepth()
    {
        return Integer.parseInt(getMLFCListener().getGUIProperty("screenDepth"));
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemOverdubOrSubtitle()
    {
        return getMLFCListener().getProperty("smil/systemOverdubOrSubtitle");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemAudioDesc()
    {
        return getMLFCListener().getProperty("smil/systemAudioDesc");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemOperatingSystem()
    {
        return getMLFCListener().getProperty("smil/systemOperatingSystem");
    }
    
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public String getSystemCPU()
    {
        return getMLFCListener().getProperty("smil/systemCPU");
    }
    /**
     * Returns the value of systemAttribute for the SMIL core logic.
     */
    public boolean getSystemComponent(String component)
    {
        if (component.equals("http://www.x-smiles.org") == true)
            return true;
        if (component.equals("http://www.xsmiles.org") == true)
            return true;
        
        return false;
    }
    
    /**
     * Tests if images should be shown.
     * @return		true if images should be played.
     */
    public boolean getPlayImage()
    {
        fi.hut.tml.xsmiles.mlfc.MLFCListener list = getMLFCListener();
        Log.debug("MLFCListener : "+list);
        String prop=list.getProperty("smil/play-image");
        Log.debug("property: "+prop);
        if (prop==null) return true;
        return prop.equals("true");
    }
    
    /**
     * Tests if audio should be played.
     * @return		true if audio should be played.
     */
    public boolean getPlayAudio()
    {
        // If JMF is not available, then never play audio
        if (jmfAvailable == false)
            return false;
        fi.hut.tml.xsmiles.mlfc.MLFCListener list = getMLFCListener();
        String prop=list.getProperty("smil/play-audio");
        Log.debug("property: "+prop);
        if (prop==null) return true;
        
        return prop.equals("true");
    }
    
    /**
     * Tests if video should be played.
     * @return		true if video should be played.
     */
    public boolean getPlayVideo()
    {
        // If JMF is not available, then never play video
        if (jmfAvailable == false)
            return false;
        fi.hut.tml.xsmiles.mlfc.MLFCListener list = getMLFCListener();
        String prop=list.getProperty("smil/play-video");
        Log.debug("property: "+prop);
        if (prop==null) return true;
        
        return prop.equals("true");
        
    }
    
    
    
    /**
     * Set the title for the presentation.
     * @param title		Title for the presentation
     */
    public void setTitle(String title)
    {
        super.setTitle(title);
        
        frameTitle = title;
    }
    
    /**
     * Get the title of this presentation.
     * @return		The title of this presentation
     */
    public String getTitle()
    {
        return frameTitle;
    }
    
    /**
     * Check if JMF class is available. At the same time, set the hint...
     * @return		true if JMF is available, false otherwise
     */
    private boolean isJMFAvailable()
    {
        return this.decorator.isJMFAvailable(this.getMLFCListener());
    }
    
    int aa = 0;
    public void refresh()
    {
        aa=0;
        rootLayout.repaint();
        //		ll.repaint();
    }
    
    
    //	JLayeredPanel ll = new JLayeredPanel();
    /*
    public class JLayeredPanel extends JLayeredPane
    {
        public void paint(Graphics g)
        {
            if (aa ==0)
            {
                smilDoc.getAnimationScheduler().updateAnimations();
                aa=1;
                System.out.println("-hit-");
            } else
            {
                System.out.println("miss!");
                aa=0;
            }
            //			repaint();
            super.paint(g);
        }
        public void update(Graphics g)
        {
        }
    }*/
}



