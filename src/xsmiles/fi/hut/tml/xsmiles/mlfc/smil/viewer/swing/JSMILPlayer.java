/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import java.io.Reader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.net.URL;

import javax.swing.JLayeredPane;
import java.awt.Container;

/**
 * This hides the SMIL viewer implementation, implementing it as a simple
 * Component, which can be given the source (URL or filename) for the
 * SMIL presentation.
 * <p>The presentation is initialized by calling init().
 * <p>The presentation can be controlled by calling methods start() and stop().
 * <p>The presenation is closed by calling destroy(), proper memory freeing is not guaranteed.
 * <p>
 * To use this component, classpath should include xerces.jar and xml-apis.jar
 * To play audio and video, JMF should be installed.
 */
public class JSMILPlayer extends JLayeredPane
{
    
    // State of this component
    private boolean initialized = false;
    
    // Viewer object
    private JSMILPlayerCore smil = null;
    
    /**
     * Init to create a new SMIL presentation. Can only be called once.
     * @param fileName		Filename to be played
     * @return				true if successful
     */
    public boolean init(String fileName)
    {
        try
        {
            return init(this, new FileReader(fileName), fileName);
        } catch (java.io.FileNotFoundException e)
        {
            smil = null;
            return false;
        }
    }
    
    public boolean init(InputStream is, String filename)
    {
        return init(this, new InputStreamReader(is), filename);
    }
    
    /**
     * Init to create a new SMIL presentation. Can only be called once.
     * @param url		URL to be played
     * @return				true if successful
     */
    public boolean init(URL url)
    {
        try
        {
            return init(this, new InputStreamReader(url.openStream()), url.toString());
        } catch (java.io.IOException e)
        {
            smil = null;
            return false;
        }
    }
    
    /**
     * Private method to serve public init() methods.
     */
    private boolean init(Container c, Reader r, String path)
    {
        // Check that this is called only once.
        // Otherwise, the viewer may get confused (maybe merge the SMIL docs).
        if (initialized == true)
            return false;
        initialized = true;
        
        // Create the viewer
        smil = new JSMILPlayerCore();
        
        // Try to init it.
        smil.init(c, r, path);
        return true;
    }
    
    
    
    /**
     * Start the presentation.
     */
    public void start()
    {
        if (smil != null)
            smil.start();
    }
    
    /**
     * Stop the presentation.
     */
    public void stop()
    {
        if (smil != null)
            smil.stop();
    }
    
    /**
     * Clears all resources and frees memory.
     * The presentation cannot be played anymore after this call.
     */
    public void destroy()
    {
        if (smil != null)
        {
            smil.destroy();
            smil = null;
        }
    }
    
}

