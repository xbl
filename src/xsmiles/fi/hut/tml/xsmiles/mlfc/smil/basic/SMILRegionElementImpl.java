/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AnimationService;

import java.util.Hashtable;
import java.util.Enumeration;

// Device Independeny broken...
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.JBlockPanel;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCDrawingArea;

/**
 *  Controls the position, size and scaling of media object elements. See the  
 * region element definition . 
 *
 * MediaElements should register themselves to their regions. Otherwise, media elements
 * will not get refreshed if the region attributes change.
 *
 * This implements AnimationService, thus allows animation of some attributes.
 */
public class SMILRegionElementImpl extends SMILElementImpl 
		       implements SMILRegionElement, LayoutCalc, AnimationService {

	// Media handlers added to this region (key: MediaHandler, value: nonsense)
	private Hashtable mediaHandlers = null;

	// Regions added to this region (key: RegionElementImpl, value: nonsense)
	private Hashtable childRegions = null;

	// Parent region. Used to set parent region visible/invisible when media is added/removed.
	// null if no parent or the parent is root-layout (which is always visible)
	private LayoutCalc parentRegion = null;	

	// This region's own drawingarea
	private DrawingArea drawingArea = null;
	
	// Number of MediaElements/Child Regions in this region
	private int mediaCount = 0, regionCount = 0;

	// Animation values
	private Hashtable animAttributes = null;

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 */
	public void setAnimAttribute(String attr, String value) {
		if (attr != null && value != null) {
			animAttributes.put(attr, value);
			if (attr.equals("z-index") || attr.equals("regionName"))
				Log.debug("Region Animation of z-index or regionName not yet supported.");
		}
	}

	/**
	 * This method returns the animation value of the attribute, and
	 * if not available, returns the DOM value.
	 */
	public String getAnimAttribute(String attr) {
		String val = (String)animAttributes.get(attr);
		if (val == null)
			val = getAttribute(attr);
		return val;	
	}
	
	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 */
	public void removeAnimAttribute(String attr) {
		animAttributes.remove(attr);
	}

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 */
	public void refreshAnimation()  {
		refreshRegion();
	}

	// NOT USED
	public float convertStringToUnitless(String attr, String value) {
		return 0;
	}
	// NOT USED
	public String convertUnitlessToString(String attr, float value) {
		return null;
	}
	
	/**
	 * Constructor with owner doc
	 */
	public SMILRegionElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "region");
		mediaHandlers = new Hashtable();
		childRegions = new Hashtable();

		animAttributes = new Hashtable();
	}

	/**
	 * Initializes this region and its associated DrawingArea. This is only called 
	 * for regions under a valid layout element.
	 */
	public void initHead() {

		// Create a drawing area for this region.
		if (drawingArea == null) {

			// Get a normal or scroll region		
			if (getFit() == null || getFit().equals("scroll") == false)
				drawingArea = getSMILDoc().getViewer().getNewDrawingArea(DrawingArea.REGION, false);
			else
				drawingArea = getSMILDoc().getViewer().getNewDrawingArea(DrawingArea.REGIONSCROLL, false);

			SMILLayoutElement layout = getSMILDoc().getDocLayout();
			drawingArea.setBounds(calcLeft(), calcTop(), calcRight() - calcLeft(), calcBottom() - calcTop());
			drawingArea.setBackgroundColor(getABackgroundColor());

//			Log.debug(getId()+" left:"+calcLeft()+" width:"+(calcRight() - calcLeft())+" right:"+calcRight());

			// Get the parent region
			Node n = this.getParentNode();
			if (n == null)	// If not in DOM, use rootlayout (layout) as parent
				n = layout;
			if (n instanceof LayoutCalc)  {
				parentRegion = (LayoutCalc)n;
			} else  {
				Log.error("Region should be under a layout, region or topLayout element, found: "+n);
				return;
			}

			// Set visible if always visible...
			if (getShowBackground() == SHOWBACKGROUND_ALWAYS)  {
				getDrawingArea().setVisible(true);
				if (parentRegion != null)
					parentRegion.addRegion(this);
			}

			// Add this region to the parent element's region
//			SMILRootLayoutElementImpl rl = (SMILRootLayoutElementImpl)layout.getRootLayoutElement();
			parentRegion.getDrawingArea().addRegion(drawingArea, getZIndex());
		}
	}

	/**
	 * CSS: Initializes this region and its associated DrawingArea. This is only called 
	 * for regions created for CSS layout. This is the same as initHead(), but
	 * instead of adding the region to the rootlayout, this will add it to param parent.
	 * @param parentReg		Parent Region (or null for root-layout)
	 * @param parent		Parent DrawingArea
	 * @param block			Init the container to be block (=true) or inline (=false) container
	 */
	public void initHeadAndAddTo(SMILRegionElementImpl parentReg, DrawingArea parentDrawingArea, boolean block, Element origElem) {
		// Create a drawing area for this region.
		if (drawingArea == null) {

			// Get a normal or scroll region		
			if (getFit() == null || getFit().equals("scroll") == false)
				drawingArea = getSMILDoc().getViewer().getNewDrawingArea(DrawingArea.REGION, block);
			else
				drawingArea = getSMILDoc().getViewer().getNewDrawingArea(DrawingArea.REGIONSCROLL, block);

			SMILLayoutElement layout = getSMILDoc().getDocLayout();
			// TODO: Breaks device independency!
			// Set the block container without width/height to be stretched during doLayout().
                        drawingArea.setCSSStretch(getWidth(),getHeight(),origElem);
                        /*
			if (drawingArea instanceof SMILMLFCDrawingArea) {
				if (((SMILMLFCDrawingArea)drawingArea).getContainer() instanceof JBlockPanel) {
					Log.debug("JBLOCKPANEL CRAETED!!! w:"+getWidth()+" h:"+getHeight());
					JBlockPanel jbp = (JBlockPanel)((SMILMLFCDrawingArea)drawingArea).getContainer();
					if (getWidth() == null) {
						jbp.setStretchWidth(true);
						Log.debug("JBLOCKPANEL WIDTH STRECT!!!");
					}
					if (getHeight() == null) {
						jbp.setStretchHeight(true);
						Log.debug("JBLOCKPANEL HEIGHT STRECT!!!");
					}
				}	
				// CSS FIXTHIS: ADD container - element pair to lookup list
				fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILCSSFlowLayout.componentFlowMap.put(
								((SMILMLFCDrawingArea)drawingArea).getContainer(), origElem);
			}
                         */
			drawingArea.setBounds(calcLeft(), calcTop(), calcRight() - calcLeft(), calcBottom() - calcTop());
			drawingArea.setBackgroundColor(getABackgroundColor());

//			Log.debug(getId()+" left:"+calcLeft()+" width:"+(calcRight() - calcLeft())+" right:"+calcRight());

			// Set parent region - to add this region to it, 
			// when drawingarea of this becomes visible.
			parentRegion = parentReg;

			// Set visible if always visible...
			if (getShowBackground() == SHOWBACKGROUND_ALWAYS)  {
				getDrawingArea().setVisible(true);
				if (parentRegion != null)
					parentRegion.addRegion(this);
			}

			// Add this region to the parent element
			parentDrawingArea.addRegion(drawingArea, getZIndex());
		}
	}

	/**
	 * Release resources.
	 */
	public void destroy() {
		if (drawingArea != null)  {
			getDrawingArea().setVisible(false);
			if (parentRegion != null)
				parentRegion.removeRegion(this);
		}
		drawingArea = null;
		super.destroy();
	}

	/** 
	 * Return drawing area.
	 */
	 public DrawingArea getDrawingArea() {
	 	return drawingArea;
	 }

	/**
	 * Set DrawingArea visible/invisible, and add/remove this region to the parent region.
	 * Effective only, if showBackground = SHOWBACKGROUND_WHENACTIVE.
	 * @param flag		true=visible, false=invisible
	 */
	private void setDrawingAreaVisible(boolean flag)  {

		if (getShowBackground() == SHOWBACKGROUND_WHENACTIVE)
			if (flag == false)  {
				if (regionCount + mediaCount == 0)  {
							getDrawingArea().setVisible(false);
							if (parentRegion != null)
								parentRegion.removeRegion(this);
				}	
			} else  {
				if (regionCount + mediaCount == 1)  {
							getDrawingArea().setVisible(true);
							if (parentRegion != null)
								parentRegion.addRegion(this);
				}	
			}
	}

	/**
	 * Add region to this region. This will cause the drawingarea to become visible.
	 */
	public void addRegion(SMILRegionElementImpl region)  {
		if (region == null)
			return;
		
		// If not yet added.. add it!
		if (childRegions.get(region) == null)  {
			childRegions.put(region, "krsmilp");
			regionCount++;
			setDrawingAreaVisible(true);
		}
	}

	/**
	 * Remove region from this region. This may cause the drawingarea to become invisible,
	 * if this was the last region, and no medias either in the region.
	 */
	public void removeRegion(SMILRegionElementImpl region)  {
		if (region == null)
			return;
		
		// If not yet removed... check if this region should become invisible.
		if (childRegions.remove(region) != null)  {
			regionCount--;
			setDrawingAreaVisible(false);
		}
	}

	/**
	 * Add media to this region.
	 */
	public void addMedia(MediaHandler media) {
		if (media == null)
			return;
	
		media.setDrawingArea(drawingArea);
		fitMedia(media);

		mediaHandlers.put(media, "kmsmilp");
		mediaCount++;
		Log.debug("mediacount++:"+mediaCount+" "+media);
		// If not visible, then make this region visible
		setDrawingAreaVisible(true);	
		
		// Place this region on top of other same z-index regions
		SMILRootLayoutElementImpl rl = (SMILRootLayoutElementImpl)getSMILDoc().getDocLayout().getRootLayoutElement();
		rl.getDrawingArea().bringToFront(getDrawingArea());	
	}

	/**
	 * Add media to this region.
	 */
	public void addLink(LinkHandler link) {
		if (link == null)
			return;
	
		link.setDrawingArea(drawingArea);
		// No need for fitting - this is handled in the link element itself

		mediaHandlers.put(link, "klsmilp");
		mediaCount++;
		// This region must already be visible - it must have the link's media element
	}

	/**
	 * Remove media or link from this region.
	 */
	public void removeMedia(MediaHandler media) {
		if (media == null)
			return;

		// Remove and if not found, just exit
		if (mediaHandlers.remove(media) == null)  {
//			Log.debug("       ---- ooops. region.removeMedia() didn't find "+media+" in "+mediaHandlers);
			return;
		}

		mediaCount--;
		Log.debug("mediacount--: "+mediaCount+" "+media);
		// If no more media, make this region invisible
		setDrawingAreaVisible(false);
	}

	public void setAttribute(String name, String value) {
		super.setAttribute(name, value);
		refreshRegion();
	}

	// Notify media, if attributes change
	public void setAttributeNS(String ns, String name, String value) {
		super.setAttributeNS(ns, name, value);
		refreshRegion();
	}
	public Attr setAttributeNode(Attr newAttr) throws DOMException {
	  Attr ret =  super.setAttributeNode(newAttr);
	  refreshRegion();
	  return ret;
	}
	

	/**
	 * Fit all medias added to this region.
	 */
	 private void fitMediaHandlers() {
	 	for (Enumeration e = mediaHandlers.keys() ; e.hasMoreElements() ;)
			fitMedia((MediaHandler) e.nextElement());
	 }

	/**
	 * Fit the given media to the region. Also, sets the soundLevel.
	 */
	private void fitMedia(MediaHandler media) {
		// Handle fit attribute and resize media according to it.
		String fit = getFit();
		if (fit == null)
			fit = "";

		int regionWidth = calcRight() - calcLeft();
		int regionHeight = calcBottom() - calcTop();
		int imageWidth = media.getOriginalWidth();
		int imageHeight = media.getOriginalHeight();
		// image width or height not known - use the region size
		if (imageWidth == -1 || imageHeight == -1)
			fit = "fill";
		
		int realWidth = 0, realHeight = 0;
		if (fit.equals("fill")) { 
                    // MH Test for havi
		//if (fit.equals("fill")||fit.equals("hidden")) {
			realWidth = regionWidth;	// Match the region
			realHeight = regionHeight;
		} else if (fit.equals("meet")) {
			// Scale to smaller axis
			float scaleX = (float)regionWidth / (float)imageWidth;
			float scaleY = (float)regionHeight / (float)imageHeight;
			if (scaleX < scaleY) {
				realWidth = (int)((float)imageWidth * scaleX);
				realHeight = (int)((float)imageHeight * scaleX);
			} else {
				realWidth = (int)((float)imageWidth * scaleY);
				realHeight = (int)((float)imageHeight * scaleY);
			}
		} else if (fit.equals("scroll")) {
			realWidth = imageWidth;		// It is ok to go over the region
			realHeight = imageHeight;	// Scroll is handled in DrawingArea
		} else if (fit.equals("slice")) {
			// Scale to larger axis - ok to slice
			float scaleX = (float)regionWidth / (float)imageWidth;
			float scaleY = (float)regionHeight / (float)imageHeight;
			if (scaleX > scaleY) {
				realWidth = (int)((float)imageWidth * scaleX);
				realHeight = (int)((float)imageHeight * scaleX);
			} else {
				realWidth = (int)((float)imageWidth * scaleY);
				realHeight = (int)((float)imageHeight * scaleY);
			}
		} else {
			realWidth = imageWidth;		// default "hidden": It is ok to go over the region
			realHeight = imageHeight;
		}
		
		media.setBounds(0, 0, realWidth, realHeight);
		
		String level = getASoundLevel();
		int vol = 100;
		if (level != null && level.length() > 0)
			// Sound must be a percentage value? This accepts 100% and 100 (for animation)
			if (level.endsWith("%") == true)
				level = level.substring(0, level.length()-1);

			try {
				vol = Integer.parseInt(level);
			} catch (NumberFormatException e) {
				return;
			}
			media.setAudioVolume(vol);
	}

	/**
	 * Refresh position and media in this region.
	 */
	private void refreshRegion() {
	  if (drawingArea != null) {
	    drawingArea.setBounds(calcLeft(), calcTop(), calcRight() - calcLeft(), calcBottom() - calcTop());
	    drawingArea.setBackgroundColor(getABackgroundColor());
	  }
	  fitMediaHandlers();
	}

	/**
	 * Calculate the left border, relative to the given root-layout/region (in case of %).
	 * @param c		Root-layout or region
	 * @return 		The coordinate for the left border
	 */
	public int calcLeft() {
		// Get the container's width
		int bbw = getSMILDoc().getDocLayout().getRootLayoutWidth();

		// Get parent height - the parent must implement LayoutCalc
		LayoutCalc parent = (LayoutCalc)getParentNode();

		// Get the left attribute
		String left = getALeft();
		
		// Left defined, not 'auto' -> left = left
		if (left != null && left.equals("auto") == false) 
			return AttributeHandler.convertHorizStringToInt(left, parent);

		// If width not defined -> left = 0
		String width = getAWidth();
		if (width == null || width.equals("auto") == true) 
			return 0;

		// If right not defined -> left = 0
		String right = getARight();
		if (right == null || right.equals("auto") == true) 
			return 0;

		// left not defined, width and right defined -> left = bbw - right - width
		return bbw - AttributeHandler.convertHorizStringToInt(right, parent) - 
				AttributeHandler.convertHorizStringToInt(width, parent);
	}

	/**
	 * Calculate the right border, relative to the given root-layout/region (in case of %).
	 * @param c		Root-layout or region
	 * @return 		The coordinate for the right border
	 */
	public int calcRight() {
		// Get the container's width
		int bbw = getSMILDoc().getDocLayout().getRootLayoutWidth();

		// Get parent height - the parent must implement LayoutCalc
		LayoutCalc parent = (LayoutCalc)getParentNode();

		String left = getALeft();
		if (left != null && left.equals("auto") == true)
			left = null;

		String right = getARight();
		if (right != null && right.equals("auto") == true)
			right = null;
		
		String width = getAWidth();
		if (width != null && width.equals("auto") == true)
			width = null;
		
		return calcEndCoord(bbw, left, width, right, false, parent);
	}		

	/**
	 * Calculate the top border, relative to the given root-layout/region (in case of %).
	 * @param c		Root-layout or region
	 * @return 		The coordinate for the top border
	 */
	public int calcTop() {
		// Get the container's height
		int bbw = getSMILDoc().getDocLayout().getRootLayoutHeight();
		// Get the top attribute
		String top = getATop();
		
		// Get parent height - the parent must implement LayoutCalc
		LayoutCalc parent = (LayoutCalc)getParentNode();
		
		// Top defined, not 'auto' -> top = top
		if (top != null && top.equals("auto") == false) 
			return AttributeHandler.convertVertStringToInt(top, parent);

		// If height not defined -> top = 0
		String height = getAHeight();
		if (height == null || height.equals("auto") == true) 
			return 0;

		// If bottom not defined -> top = 0
		String bottom = getABottom();
		if (bottom == null || bottom.equals("auto") == true) 
			return 0;

		// top not defined, height and bottom defined -> top = bbw - bottom - height
		return bbw - AttributeHandler.convertVertStringToInt(bottom, parent) - 
				AttributeHandler.convertVertStringToInt(height, parent);

	}

	/**
	 * Calculate the bottom border, relative to the given root-layout/region (in case of %).
	 * @param c		Root-layout or region
	 * @return 		The coordinate for the bottom border
	 */
	public int calcBottom() {
		// Get the container's height
		int bbw = getSMILDoc().getDocLayout().getRootLayoutHeight();

		// Get parent height - the parent must implement LayoutCalc
		LayoutCalc parent = (LayoutCalc)getParentNode();

		String top = getATop();
		if (top != null && top.equals("auto") == true)
			top = null;

		String bottom = getABottom();
		if (bottom != null && bottom.equals("auto") == true)
			bottom = null;
		
		String height = getAHeight();
		if (height != null && height.equals("auto") == true)
			height = null;

		return calcEndCoord(bbw, top, height, bottom, true, parent);
			
	}

	private int convertValue(String value, boolean vert, LayoutCalc parent) {
		if (vert == true)
			return AttributeHandler.convertVertStringToInt(value, parent);
		else	
			return AttributeHandler.convertHorizStringToInt(value, parent);
	}

	/**
	 * Calculates right or bottom coordinate for this region.
	 */	
	private int calcEndCoord(int bbw, String start, String dist, String end, boolean vert, LayoutCalc lc) {

		// 8 different cases - [left/top] value + [width/height] value in the spec.
		
		// auto, auto, auto -> res = 0 + bbw
		if (start == null && dist == null && end == null)
			return bbw;

		// auto, auto, defined -> res = 0 + bbw - end
		if (start == null && dist == null && end != null)
			return bbw - convertValue(end, vert, lc);

		// auto, defined, auto -> res = 0 + dist
		if (start == null && dist != null && end == null)
			return convertValue(dist, vert, lc);

		// auto, defined, defined -> res = bbw - end - dist + dist
		if (start == null && dist != null && end != null)
			return bbw - convertValue(end, vert, lc);

		// defined, auto, auto -> res = start + bbw - start
		if (start != null && dist == null && end == null)
			return bbw;

		// defined, auto, defined -> res = start + bbw - end - start
		if (start != null && dist == null && end != null)
			return bbw - convertValue(end, vert, lc);

		// defined, defined, auto -> res = start + dist
		if (start != null && dist != null && end == null)
			return convertValue(start, vert, lc) +
				convertValue(dist, vert, lc);			

		// defined, defined, defined -> res = start + dist
		if (start != null && dist != null && end != null)
			return convertValue(start, vert, lc) +
				convertValue(dist, vert, lc);			

		// Should never come to here
		return 0;
	}

		
    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFit() {
	    String fit = getAttribute("fit");
	    if (fit == null || fit.length() == 0)
	    	return null;

		return fit;	
    }
    public void setFit(String fit) throws DOMException {
	    setAttribute("fit", fit);
    }

    /**
     * animtop
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    private String getATop() {
    	String top = getAnimAttribute("top");
	    return trimAttr(top);
    }
    /**
	 * top
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTop() {
		String top = getAttribute("top");
		return trimAttr(top);
	}
    public void setTop(String top) throws DOMException {
	    setAttribute("top", top);
    }

	/**
	 * abottom
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private String getABottom() {
		String bottom = getAnimAttribute("bottom");
		return trimAttr(bottom);
	}
	/**
	 * bottom
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBottom() {
		String bottom = getAttribute("bottom");
		return trimAttr(bottom);
	}
	
	public void setBottom(String bottom) throws DOMException {
		setAttribute("bottom", bottom);
	}

	/**
	 * aleft
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private String getALeft() {
		String left = getAnimAttribute("left");
		return trimAttr(left);
	}
	/**
	 * left
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getLeft() {
		String left = getAttribute("left");
		return trimAttr(left);
	}
	
	public void setLeft(String left) throws DOMException {
		setAttribute("left", left);
	}

	/**
	 * aright
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private String getARight() {
		String right = getAnimAttribute("right");
		return trimAttr(right);
	}
	/**
	 * right
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRight() {
		String right = getAttribute("right");
		return trimAttr(right);
	}
	public void setRight(String right) throws DOMException {
		setAttribute("right", right);
	}

	/**
	 * anim z-index
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private int getAZIndex() {
		String z = getAnimAttribute("z-index");
		if (z == null || z.length() == 0)
			return 0;

		try {
			return Integer.parseInt(z);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public int getZIndex() {
		String z = getAttribute("z-index");
		if (z == null || z.length() == 0)
			return 0;

		try {
			return Integer.parseInt(z);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	public void setZIndex(int zIndex) throws DOMException {
		setAttribute("z-index", Integer.toString(zIndex));
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}
	// showBackground types
	public static final short SHOWBACKGROUND_ALWAYS            = 0;
	public static final short SHOWBACKGROUND_WHENACTIVE        = 1;

	/**
	 * showBackground
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getShowBackground() {
		String show = getAttribute("showBackground");
		if (show != null && show.equals("whenActive"))
			return SHOWBACKGROUND_WHENACTIVE;
			
		// Default value
		return SHOWBACKGROUND_ALWAYS;
	}

	public void setShowBackground(short showBackground) throws DOMException {
//		setAttribute("showBackground", showBackground);
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * anim SoundLevel
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private String getASoundLevel() {
		return getAnimAttribute("soundLevel");
	}

	/**
	 * soundLevel
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getSoundLevel() {
		return getAttribute("soundLevel");		
	}
	public void setSoundLevel(String soundLevel) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * regionName
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegionName() {
		return getAttribute("regionName");
	}
	
	public void setRegionName(String regionName) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// These are ElementLayout interface

	private String getABackgroundColor() {
		String color = getAnimAttribute("backgroundColor");
		// if not found, then try to find the deprecated attribute
		if (color == null || color.length() == 0) {
			color = getAnimAttribute("background-color");
			if (color == null || color.length() == 0)
				return null;
		}
			
		return color;
	}
	public String getBackgroundColor() {
		String color = getAttribute("backgroundColor");
		// if not found, then try to find the deprecated attribute
		if (color == null || color.length() == 0) {
			color = getAttribute("background-color");
			if (color == null || color.length() == 0)
				return null;
		}
			
		return color;
	}
	public void setBackgroundColor(String backgroundColor) throws DOMException {
		setAttribute("backgroundColor", backgroundColor);
	}

	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private String getAHeight() {
		String height = getAnimAttribute("height");
		return trimAttr(height);
	}
	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getHeight() {
		String height = getAttribute("height");
		return trimAttr(height);
	}
	public void setHeight(int height) throws DOMException {
		setAttribute("height", String.valueOf(height));
	}

  /**
   * Prosentual height
   */
  public void setHeight(String h) throws DOMException 
  {
    setAttribute("height", h);
  }

  /**
   * Prosentual width
   */
  public void setWidth(String w) throws DOMException 
  {
    setAttribute("width", w);
  }



	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	private String getAWidth() {
		String width = getAnimAttribute("width");
		return trimAttr(width);
	}
	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getWidth() {
		String width = getAttribute("width");
		return trimAttr(width);
	}
	public void setWidth(int width)  throws DOMException {
		setAttribute("width", String.valueOf(width));
	}

	/**
	 * Removes the "px" ending in length attributes.
	 */
	private String trimAttr(String value) {
		if (value == null || value.length() == 0)
			return null;
			
		// Strip off the pixel unit
		if (value.endsWith("px")) {
			value = value.substring(0, value.length()-2);
		}
		return value;
	}
}

