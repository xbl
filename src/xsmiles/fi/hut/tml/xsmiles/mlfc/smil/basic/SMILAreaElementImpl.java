/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

// DOM interface
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.mlfc.smil.extension.SMILBrushElementImpl;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.Log;

/**
 *  XSMILAElement is handled as an time container - containing media (or link or switch...).
 */
public class SMILAreaElementImpl extends ElementTimeContainerImpl 
							implements XSMILAreaElement, MediaListener {

	LinkHandler link;
	String tag = null;
	SMILRegionElementImpl region;

	/**
	 * Constructor - Set the owner and name.
	 */
	public SMILAreaElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
		this.tag = tag;
	}

	/**
	 * Overridden display() - this will also show the link.
	 */
	public void display() {
		// If actuate == "onLoad", follow the link as the active duration has started
		if (getActuate() == ACTUATE_ONLOAD) {
			activateLink();
		}

		// show link
		if (link == null)
			link = getSMILDoc().getViewer().getNewLinkHandler();

		// Get mouse clicks - to activate the link
		link.addListener(this);
		link.setAlt("");
		// Set URL point to href value
		link.setURL(getHref());
		link.setTitle(getTitle());
		link.setViewer(getSMILDoc().getViewer());

		SMILLayoutElement layout = getSMILDoc().getDocLayout();
		// This assumes that area is ALWAYS under media or brush element.
		Node parent = getParentNode();
		int width = 0, height = 0;
		if (parent instanceof SMILMediaElementImpl) {
			MediaHandler media = ((SMILMediaElementImpl)parent).getMedia();
			width = media.getWidth();
			height = media.getHeight();
			
		} else if (parent instanceof SMILBrushElementImpl) {
			BrushHandler brush = ((SMILBrushElementImpl)parent).getBrush();
			width = brush.getWidth();
			height = brush.getHeight();
		} else {
			Log.error(tag+" element under a wrong element.");
			return;
		}
		
		SMILRegionInterface visibleElement = ((SMILRegionInterface)getParentNode());
		// We should actually use the image size - not the region size.
		region = (SMILRegionElementImpl)visibleElement.getRegionElement();
		if (layout == null) 
			Log.error("<layout> not found!");
		
		if (region != null) {
			// Media needs to know the rootlayout size to zoom the presentation
			DrawingArea drawingArea = ((SMILRootLayoutElementImpl)(layout.getRootLayoutElement())).getDrawingArea();
			link.setRootLayoutSize(layout.getRootLayoutWidth(), layout.getRootLayoutHeight());

			// Get the coords values
			String coords = getCoords();
			String leftString, topString, rightString, bottomString;
			int leftCoord = 0, rightCoord = 0, topCoord = 0, bottomCoord = 0;
			if (coords != null && coords.length() != 0) {
				// use "," as a separator between coords.
				int pos = 0;
				leftString = getNextToken(coords, pos, ',');
				pos += leftString.length()+1;
				leftCoord = AttributeHandler.convertSizeStringToInt(leftString, width);

			    topString = getNextToken(coords, pos, ',');
				pos += topString.length()+1;
				topCoord = AttributeHandler.convertSizeStringToInt(topString, height);

				rightString = getNextToken(coords, pos, ',');
				pos += rightString.length()+1;
				rightCoord = AttributeHandler.convertSizeStringToInt(rightString, width);

				bottomString = getNextToken(coords, pos, ',');
				pos += bottomString.length()+1;
				bottomCoord = AttributeHandler.convertSizeStringToInt(bottomString, height);
			} else {
				// Coords were not specified, so use the media size
				leftCoord = AttributeHandler.convertSizeStringToInt("0%", width);
				topCoord = AttributeHandler.convertSizeStringToInt("0%", height);
				rightCoord = AttributeHandler.convertSizeStringToInt("100%", width);	
				bottomCoord = AttributeHandler.convertSizeStringToInt("100%", height);
			}

			link.setAlt("AREA "+getId());

//			link.setTop(topCoord);
//			link.setLeft(leftCoord);
//			link.setBottom(bottomCoord);
//			link.setRight(rightCoord);
			link.setBounds(leftCoord, topCoord, rightCoord-leftCoord, bottomCoord-topCoord);
			region.addLink(link);

			Log.debug("AREA SHOWN  "+(leftCoord)+
					","+(topCoord)+"-"+(rightCoord)+","+(bottomCoord));

		} else
			Log.error("No region for "+getId()+"- AREA not SHOWN ");
		
		link.play();

		super.display();
	}

	/**
	 * Get the next token starting from pos until delimiter or end of string.
	 * @param s			String to be tokenized
	 * @param pos		Position to start
	 * @param delimiter	Character to delimit
	 */
	String getNextToken(String s, int pos, char delimiter) {
		try {
			int next = s.indexOf(delimiter, pos);
			if (next != -1) {
				return s.substring(pos, s.indexOf(delimiter, pos));
			} else {
				return s.substring(pos);
			}
		} catch (NullPointerException e) {
			return "";
		} catch (IndexOutOfBoundsException e) {
			return "";
		}
	}
	
	/**
	 * Overridden remove() - this will also remove the link.
	 */
	public void remove() {
		Log.debug("LINK REMOVE"+getHref());
		// remove media
		if (link != null)  {
			link.stop();
			if (region != null) {
				Log.debug("Link removed from region");
				region.removeMedia(link);
			}
		}
		super.remove();
	}

	/**
	 * Overridden freeze() - this will also freeze the link. Freezing a link will
	 * not remove it - in fact, it doesn't have any effect.
	 */
	public void freeze() {
		Log.debug("LINK FREEZE"+getHref());
		// freeze media
		if (link != null)  {
			link.freeze();
		}
		super.freeze();
	}

	/**
	 * This method will destroy the element, freeing all its memory.
	 */
	public void destroy() {
		if (link != null) {
			link.close();
			link = null;
		}

		super.destroy();
	}

	/**
	 *  See the shape attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getShape() {
		return null;
	}
	public void setShape(String shape) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the coords attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getCoords() {
		return getAttribute("coords");
	}
	public void setCoords(String coords) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the nohref attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public boolean getNoHref() {
		return false;
	}
	public void setNoHref(boolean nohref) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the href attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getHref() {
		return getAttribute("href");
	}
	public void setHref(String href) throws DOMException {
		setAttribute("href", href);
	}

	/**
	 *  See the sourceLevel attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getSourceLevel() {
		return null;
	}
	public void setSourceLevel(String sourceLevel) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the destinationLevel attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getDestinationLevel() {
		return null;
	}
	public void setDestinationLevel(String destinationLevel) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// sourcePlaystate Types
	public static final short SOURCEPLAYSTATE_PLAY                 = 0;
	public static final short SOURCEPLAYSTATE_PAUSE                = 1;
	public static final short SOURCEPLAYSTATE_STOP                 = 2;

	/**
	 *  See the sourcePlaystate attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getSourcePlaystate() {
		return 0;
	}
	public void setSourcePlaystate(short sourcePlaystate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// destinationPlaystate Types
	public static final short DESTINATIONPLAYSTATE_PLAY                 = 0;
	public static final short DESTINATIONPLAYSTATE_PAUSE                = 1;
	public static final short DESTINATIONPLAYSTATE_STOP                 = 2;

	/**
	 *  See the destinationPlaystate attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getDestinationPlaystate() {
		return 0;
	}
	public void setDestinationPlaystate(short destinationPlaystate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// show Types
	public static final short SHOW_REPLACE              = 0;
	public static final short SHOW_NEW                  = 1;
	public static final short SHOW_PAUSE                = 2;

	/**
	 *  See the show attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getShow() {
		String show = getAttribute("show");
		if (show.equals("pause") == true)
			return SHOW_PAUSE;

		if (show.equals("new") == true)
			return SHOW_NEW;

		return SHOW_REPLACE;
	}
	public void setShow(short show) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the accesskey attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getAccessKey() {
		return null;
	}
	public void setAccessKey(String accesskey) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the tabindex attribute in LinkingAttributes module. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public int getTabindex() {
		return 0;
	}
	public void setTabindex(int tabindex) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the target attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getTarget() {
		return getAttribute("target");
	}
	public void setTarget(String target) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the external attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public boolean getExternal() {
		return false;
	}
	public void setExternal(String external) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// actuate Types
	public static final short ACTUATE_ONREQUEST              = 0;
	public static final short ACTUATE_ONLOAD                 = 1;

	/**
	 *  See the actuate attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getActuate() {
		if (getAttribute("actuate").equals("onLoad") == true)
			return ACTUATE_ONLOAD;
		else	
			return ACTUATE_ONREQUEST;
	}
	public void setActuate(short actuate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the fragment attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getFragment() {
		return null;
	}
	public void setFragment(String fragment) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Traverse link in this element. Opens the new document either in this
	 * window or a new window. Or an internal or external link.
	 */
	public void activateLink() {
		String url = getHref();
		int show = getShow();			

		// Get target - if it is defined (non-empty), it'll override show attribute
		String target = getAttribute("target");
		smilDoc.traverseLink(url, show, target,null);
	}

	/**
	 * Mouse events - this will be called if a link is clicked.
	 */
	public void mouseClicked(java.awt.event.MouseEvent e) {
		activateLink();
	}
	 
	public void mouseEntered() {
	}

	public void mouseExited() {
	}
	
	public void mousePressed() {
	}
		
	public void mouseReleased() {
	}
	
	public void mediaPrefetched() {
	}
	
	public void mediaEnded() {
	}
}

