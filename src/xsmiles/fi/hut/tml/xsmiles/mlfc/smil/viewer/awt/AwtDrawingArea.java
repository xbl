/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.FrameListener;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;

import java.awt.Container;
import java.awt.*;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Element;

/**
 *  Interface to drawing area. 
 */
public class AwtDrawingArea implements DrawingArea {

	// Container having the drawing area (a Frame or Container)
	private Container container;

	// Content Container contains the Components. This is the same as container, 
	// except for JScrollPane
	private Container contentContainer;

	// The left and top coords of the drawing area. 
	// Default values zero for rootlayout and toplayout.
	private int left = 0, top = 0;

	// The width and height of the drawing area	
	private int width = 0, height = 0;
	
	// Type of the area
	private int type = DrawingArea.REGION;

	/**
	 * Creates a new drawing area of type (ROOTLAYOUT or TOPLAYOUT).
	 */
	public AwtDrawingArea(int type) {
		if (type == DrawingArea.ROOTLAYOUT) {
			Log.error("ROOT LAYOUT INCORRECT CREATION");
			this.type = type;
		} else if (type == DrawingArea.TOPLAYOUT) {
			Frame frame = new Frame("The AWT SMIL Viewer"); // default top-layout title
			frame.setLayout(null);
			container = frame;	
			this.type = type;
		} else if (type == DrawingArea.REGION) {
//			Panel panel = new Panel();
//			panel.setBackground(null);
			Container panel = new RealContainer();
			// Use absolute coords
			panel.setLayout(null);
			container = panel;
			contentContainer = panel;	
			// Default transparent background color		
			//panel.setOpaque(false);
			// Default not visible
			panel.setVisible(false);
			this.type = type;
			
			// This is to get a region with scroll bars
		} else if (type == DrawingArea.REGIONSCROLL) {
//			Panel panel = new Panel();
//			panel.setBackground(null);
			Container panel = new RealContainer();
			// Use absolute coords
			panel.setLayout(null);
			container = panel;
			contentContainer = panel;	
			// Default transparent background color		
			//panel.setOpaque(false);
			// Default not visible
			panel.setVisible(false);
			this.type = type;
		}
		
	}
	
	/**
	 * Creates a new drawing area using container c.
	 * @param c		Container
	 */
	public AwtDrawingArea(Container c) {
		container = c;
		container.setLayout(null);

		contentContainer = c;
		this.type = DrawingArea.ROOTLAYOUT;
	}

	public int getLeft() {
		return left;
	}

	public int getTop() {
		return top;
	}
		
	public int getWidth() {
		return width;
	}
		
	public int getHeight() {
		return height;
	}

	public void setBounds(int x, int y, int w, int h) {
		//
		left = x;
		top = y;
		width = w;
		height = h;
		// Don't move root layout!
		if (type != DrawingArea.ROOTLAYOUT)
			container.setLocation(left, top);
		container.setSize(width, height);
		
		// Validate to update the scroll bars
		container.validate();	
	}

	/**
	 * Set this drawing area visible.
	 * @param v    true=visible, false=invisible
	 */
	public void setVisible(boolean v) {
		container.setVisible(v);
	}

	/**
	 * Add a region to this drawing area. A region can be added to root-layout,
	 * topLayout, or another region.
	 * @param region		Region drawing area to be added.
	 */
	public void addRegion(DrawingArea region, int zindex) {
		// Assumes everything is AwtDrawingArea... add to top of other regions with same z-index
		contentContainer.add(((AwtDrawingArea)region).getContainer(), 0);
	}

	/**
	 * Bring this region into front of other regions
	 * @param drawingArea		Region to be the top most
	 */
	public void bringToFront(DrawingArea drawingArea) {
		contentContainer.remove(((AwtDrawingArea)drawingArea).getContainer());
		contentContainer.add(((AwtDrawingArea)drawingArea).getContainer(), 0);
	}

	public void setBackgroundColor(String color) {
		if (color == null)
			return;

		// Special case transparent. Java fails to use alpha color there above.
//		if (color.equals("transparent")) {
//			((JComponent)container).setOpaque(false);
//			return;
//		}

		Color c = ColorConverter.hexaToRgb(color);
		container.setBackground(c);
	}

	/**
	 * Set the title of this area (used for top-layout). not implemented.
	 * @param name		Title name of the area
	 */
	public void setTitle(String name)  {
//		if (container instanceof JFrame)  {
//			((JFrame)container).setTitle(name);
//		}
	}

	/**
	 * For toplayouts (frames), add a close listener.
	 * @param FrameListener		listener for closing events.
	 */
	public void addFrameListener(FrameListener frameListener)  {
	}

	/**
	 * This is a Swing specific method to return the container to draw media.
	 * @return		Container to draw media.
	 */
	public Container getContainer() {
		if (container instanceof Frame)
			return ((Frame)container);
			
		return container;	
	}

	/**
	 * Dummy class to get a non-abstract transparent Container.
	 */	
	private class RealContainer extends Container {
	}
        
        	/**
	 * This is a Swing specific method to return the container to draw media.
	 * Returns either the container or container.getContentPane().
	 * MediaHandler calls this to obtain the container of this DrawingArea.
	 *
	 * @return		Container to draw media.
	 */
	public Container getContentContainer() {
		//if (contentContainer instanceof JFrame)
		//	return ((JFrame)contentContainer).getContentPane();
			
		return contentContainer;	
	}

        public boolean setCSSStretch(String awidth, String aheight, org.w3c.dom.Element origElem)
        {
            return false;
        }        

}

