/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;

import org.w3c.dom.Element;
import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.dom.VisualComponentService;

import java.awt.*;

//import javax.swing.JComponent;

import fi.hut.tml.xsmiles.Log;

/**
 * This class takes care of ForeignElements resizing in the SMIL documents.
 * This SMILMLFC class is a full implementation. This class implements MediaHandler
 * interface, so that SMILREgionElementImpl can control this component.
 */
public class AWTForeignMediaHandler implements MediaHandler {
	// Public because JDK 1.1.8 cannot handle protected from SMILMLFCLinkHandler.ClickListener
	public String alt, url;
	protected MediaListener mediaListener;
	protected DrawingArea drawingArea;
	protected Container container;

	protected SMILDocumentImpl smilDoc = null;
	protected Viewer viewer = null;

	protected boolean prefetched = false;
	protected boolean playing = false;
	
	// Overridden MIME Type for this media. Can be set from MediaElement, if type attr is set.
	protected String mimeType = null;

	// Coordinates of the media
	protected int top = 0, left = 0;
	protected int width = 0, height = 0;

	// Foreign element
	Element foreignElement = null;
        
    VisualComponentService visualComponentService = null;
	
	// Foreign original width/height
	int foreignWidth = 0, foreignHeight = 0;
	
	// VisualComponent
	protected Component visualComponent = null;

	/**
	 * Create a new ForeignHandler. 
	 */
	public AWTForeignMediaHandler() {
	}

	/**
	 * Create a new ForeignHandler for SMIL MLFC.
	 * @param e		The foreign element implementing VisualComponentService.
	 */
	public AWTForeignMediaHandler(Element e) {
                foreignElement = e;
		if (foreignElement!=null && foreignElement instanceof VisualComponentService)  {
                        visualComponentService = ((VisualComponentService)foreignElement);
			visualComponent = visualComponentService.getComponent();
			Dimension d = visualComponent.getSize();
			// Get original size
			foreignWidth = (int)d.width;
			foreignHeight = (int)d.height;
                }
		
                else if (foreignElement!=null && foreignElement instanceof CompoundService)  {
                        visualComponentService = ((CompoundService)foreignElement).getVisualComponent();
			visualComponent = visualComponentService.getComponent();
			Dimension d = visualComponent.getSize();
			// Get original size
			foreignWidth = (int)d.width;
			foreignHeight = (int)d.height;
		
                } else
			foreignElement = null;
	}

	public void setViewer(Viewer v) {
		viewer = v;
		smilDoc = (SMILDocumentImpl)(viewer.getSMILDoc());
	}

	/**
	 * Set the drawing area. The given drawing area MUST be a SMILMLFCDrawingArea.
	 */
	public void setDrawingArea(DrawingArea d) {
		// The given drawing area MUST be SMILMLFCDrawingArea
		drawingArea = d;
		Container container = drawingArea.getContentContainer();

		if (this.container != container) {

			// Change container
			if (visualComponent.isVisible() == true) {
				visualComponentService.setVisible(false);
				if (this.container != null)
					this.container.remove(visualComponent);
				container.add(visualComponent, 0);
				visualComponentService.setVisible(true);
			}

			this.container = container;
		}

	}
	
	/**
	 * Add a media listener (currently supports only one listener)
	 * ForeignElement does not support links.
	 */
	public void addListener(MediaListener mediaListener) {
//		this.mediaListener = mediaListener;
//		
//		// Try to add listeners to this media, if it has already been prefetched.
//		if (media != null)
//			media.addMediaListener(mediaListener);
	}
	
	/**
	 * Checks if this media is static or continuous. Static for Foreign Elements.
	 * @return true	if media is static.
	 */
	public boolean isStatic() {
		return true;
	}
	
	/**
	 * This media handler will know the rootlayout size after this is called.
	 * @param width		RootLayout width
	 * @param height	RootLayout height
	 */
	public void setRootLayoutSize(int width, int height) {
	}
	
	public void setAlt(String alt) {
		this.alt=alt;
	}
	public void setURL(String url) {
		this.url = url;
	}

	public void setMIMEType(String type) {
	}
	
	public void prefetch() {
		Log.debug("Prefetching foreign element x ");//+(visualComponent==null?"null":visualComponent.toString()));
		prefetched = true;		
	}


	public void play() {
		if (container == null) {
			Log.error("No drawing area.container for foreign element");
			return;
		}

		// If this media hasn't yet been prefetched, then prefetch it.
		if (prefetched == false)
			prefetch();

		if (container != null) {

			// Set up foreign component
			visualComponent.setLocation(left, top);
//				((JComponent)visualComponent).setPreferredSize(new Dimension(width, height));
			visualComponent.setSize(width, height);
			container.add(visualComponent, 0);
			visualComponent.setVisible(true);
                        //visualComponent.repaint();
			visualComponent.validate();
			// Use the proper Service call to set this component visible
			if (visualComponentService != null)
				visualComponentService.setVisible(true);
		} else
			Log.error("Region container not set for foreign element");

		// Media ends immediately for static media (almost..).
		// Media ended - inform the SMIL player.
//		if (mediaListener != null) {
//			Thread t = new Thread(this);
//			t.start();
//		}
	}

	public void pause() {
	}
	public void stop() {
		playing = false;
		visualComponent.setVisible(false);
		if (container != null)
			container.remove(visualComponent);

		// Use the proper Service call to set this component visible
		if (visualComponentService != null)
			visualComponentService.setVisible(false);
	}
	
	public void freeze() {
	}
	
	public void close() {
		playing = false;
		stop();
	
		viewer = null;
		smilDoc = null;
		container = null;
		mediaListener = null;
		
	}

	/**
	 * Set the media time position. Usually used to skip the beginning of media.
	 * @param millisecs			Time in milliseconds
	 */
	public void setMediaTime(int millisecs) {
	}
	
	public int getTop() {
		return top;
	}
	public int getLeft() {
		return left;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setBounds(int x, int y, int w, int h) {
		left = x;
		top = y;
		width = w;
		height = h;
		updateSize();
	}

	private void updateSize()  {
		if (visualComponent.isVisible() == true)  {
			visualComponent.setLocation(left, top);
//	 			((JComponent)visualComponent).setPreferredSize(new Dimension(width, height));
			visualComponent.setSize(width, height);
			visualComponent.validate();
		}
	}
	
	
	/** 
	 * Get the real width of the media.
	 */
	public int getOriginalWidth() {
		return foreignWidth;	
	}

	/** 
	 * Get the real height of the media.
	 */
	public int getOriginalHeight() {
		return foreignHeight;	
	}

	/**
	 * Set the volume of audio (if available). 
	 * @param percentage      0-100-oo , 100 giving normal sound level.
	 */
	public void setAudioVolume(int percentage) {
	}
        
        public Object getComponent()
        {
            return this.visualComponent;
        }        
        
	
}

