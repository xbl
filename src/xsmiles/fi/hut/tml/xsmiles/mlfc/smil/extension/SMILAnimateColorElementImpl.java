/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import java.util.Vector;

/**
 *  AnimateColor Element. 
 */
public class SMILAnimateColorElementImpl extends SMILAnimationImpl implements SMILAnimateColorElement {
	
	/**
	 * Constructor - set the owner
	 */
	public SMILAnimateColorElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

	/**
	 * Parses the attribute value. 
	 * Assumes attribute format "rgb(16,16,16)" or "#101010",
	 * and returns AnimatedColorValue.
	 * @param value		Attribute value to be parsed
	 * @return 			AnimatedColorValue of the attribute
	 */
	public AnimatedValue parse(String value)  {
		return new AnimatedColorValue(value);
	}

	/**
	 * Parses the attribute value of the target attribute. 
	 * Assumes attribute format "rgb(16,16,16)" or "#101010",
	 * and returns AnimatedColorValue.
	 * @param scheduler	AnimationScheduler, this object has the DOM and "DOM buffer" values.
	 * @return 			AnimatedColorValue of the attribute
	 */
	public AnimatedValue parse(AnimationScheduler scheduler)  {
		String value = scheduler.getAnimAttribute(target, attributeName);
		return new AnimatedColorValue(value);
	}
	
	/**
	 * Parses the integer val. Returns always AnimatedColorValue.
	 * @param val	Attribute value as integer
	 * @return 		AnimatedColorValue(val)
	 */
	public AnimatedValue parse(int val)  {
		return new AnimatedColorValue(val);
	}

	/**
	 * Returns an empty array of AnimatedColorValues.
	 * @param size	Size of the array
	 * @return 		Array AnimatedColorValue[size]
	 */
	public AnimatedValue[] initArray(int size)  {
		return new AnimatedColorValue[size];
	}


}

