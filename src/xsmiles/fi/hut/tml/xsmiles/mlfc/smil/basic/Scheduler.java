/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;

/**
 * Scheduler schedules the whole SMIL presentation.
 * It is used as a global timer. All timed elements will add themselves as 
 * listeners for time evets, which are notified according to the element request.
 * <br/>
 * A centralized scheduler is used to synchronize timed elements. It also
 * helps starting, stopping and pausing the presentation.
 */
public class Scheduler {

	/**
	 * Reference to SMILDocument instance.
	 */
	private SMILDocumentImpl smilDoc = null;
	
	/**
	 * Reference to Viewer instance.
	 */
	private Viewer viewer = null;

	/**
	 * The time now. 
	 */
	private long now = 0;

	/**
	 * The time the presentation was started. 
	 */
	private long startTime = 0;

	/**
	 * The time the presentation was paused. 
	 */
	private long pauseTime = 0;

	/**
	 * List of Timed Elements waiting for time notify.
	 */
	private Vector listeners = null;

	/**
	 * Callback methods, timed element may request one of these
	 * methods to be called when the timer triggers.
	 */	
	public final static int TIMER_ACTIVATE = 0;
	public final static int TIMER_DEACTIVATE = 1;
	public final static int TIMER_SIMPLEDUR = 2;

	private Timer timer = null;

	/**
	 * Constructor.
	 */
    public Scheduler(SMILDocumentImpl doc) {
		// Init and save values
		smilDoc = doc;

		listeners = new Vector();
		timer = new Timer(this);
    }
	
	/**
	 * Return the start time of the presentation
	 * @return	system clock time the presentation was started
	 */
	 public long getStartTime() {
	 	return startTime;
	 }

	/**
	 * Return the current time in document time.
	 */
	 public long getDocTimeNow() {
	 	return System.currentTimeMillis() - startTime;
	 }

	/**
	 * Add a time listener. This will cause timer to be checked and restarted.
	 * @param elem				Element to be notified after time has elapsed.
	 * @param time				Time value in ms, related to body start time
	 * @param callbackMethod	Method to call back when notification happens
	 */
	public synchronized void addTimeListener(ElementBasicTimeImpl elem, long time, int callbackMethod) {
		viewer = smilDoc.getViewer();

//		Log.debug("(time) ++ Adding "+elem.getId()+" time: "+time);
		
		ListenerData data = new ListenerData();
		data.element = elem;
		data.time = time;
		data.method = callbackMethod;
	
		// Add to vector
		listeners.addElement(data);
		// Check if time was less than current wait time
		restartTimer();
	}
	
	/**
	 * Remove all time listeners for element elem. This will cause timer to be checked and restarted.
	 * @param elem			Element to be removed
	 */
	public void removeTimeListeners(ElementBasicTimeImpl elem) {
//		Log.debug("(time) - Trying Removing "+elem.getId());
		// remove all listeners for elem (hashtable would be faster)
		for (int i = listeners.size()-1 ; i >= 0 ; i-- ) {
			ListenerData tl = (ListenerData)listeners.elementAt(i);
			if (tl.element.equals(elem) == true)  {
//				Log.debug("(time) -- Removing "+tl.element.getId());
				listeners.removeElement(tl);
			}
		}
		// Check the next time
		restartTimer();
	}

	/**
	 * Remove time listener for element elem. This will cause timer to be checked and restarted.
	 * @param elem			Element to be removed
	 * @param method		Method to be removed
	 */
	public void removeTimeListener(ElementBasicTimeImpl elem, int callbackMethod) {
//		Log.debug("(time) - Trying Removing "+elem.getId());
		// remove all listeners for elem (hashtable would be faster)
		for (int i = listeners.size()-1 ; i >= 0 ; i-- ) {
			ListenerData tl = (ListenerData)listeners.elementAt(i);
			if (tl.element.equals(elem) == true && tl.method == callbackMethod)  {
//				Log.debug("(time) -- Removing "+tl.element.getId());
				listeners.removeElement(tl);
			}
		}
		// Check the next time
		restartTimer();
	}

//	public void addTimeListener(Element e, String event, TimeImpl time, int callbackMethod) {
//	}
//
//	public void addTimeListener(TimeImpl time, int callbackMethod) {
//	}
//
	/**
	 * Start all elements and the timer.
	 */
	public void start() {
		startTime = System.currentTimeMillis();
		
	
		// Start elements 
			// - elements add timeListeners
		// while (zero times)? -> Notify them!
		// Start timer
	}
	
	/**
	 * Stop all elements and the timer.
	 */ 
	public void stop() {
		// Stop Timers
		if (timer!=null) {
			timer.stop();
		}
	}

	/**
	 * Pause all elements and the timer.
	 */ 
	public void pause() {
	}
	/**
	 * Destroy scheduler and the timer.
	 */ 
	public void destroy() {
		// SERGE -- add  if
		if (timer != null) {
		// Stop Timers
		timer.destroy();
		}
        timer=null;
        smilDoc=null;
        viewer=null;
	}	
	/**
	 * Continue all elements and the timer.
	 */ 
	public void cont() {
	}

	/**
	 * Checks if the currently waited time is still applicable.
	 * The time may have been removed or a new earlier time may have been added.
	 */
	private void restartTimer() {
		ListenerData tl;
		long earliestTime = -1;  // Marked as not found
		String id = "none";
		
		// Brute way to do it: stop, seek earliest time, start
		timer.stop();
		
		for (Enumeration e = listeners.elements() ; e.hasMoreElements() ;) {
			tl = (ListenerData)e.nextElement();
//			Log.debug("(time)        LIST Time: "+tl.time+" tag "+tl.element.getNodeName()+" id "+tl.element.getId());

			// If the time has already gone, notify immediately
//			if (tl.time <= System.currentTimeMillis()) {
//				timeNotify(tl);
//				listeners.removeElement(tl);
//				continue;
//			}

			if (earliestTime == -1)  {
				earliestTime = tl.time;
				id = tl.element.getId();
			}
			
			if (earliestTime > tl.time) {
				earliestTime = tl.time;
				id = tl.element.getId();
			}	
		}
		// No times found? Don't start the timer.
		if (earliestTime == -1)
			return;

		// Start the timer  (e.g., 11000 - now, now = system-start)
//		Log.debug("(time) clock:"+(System.currentTimeMillis()-startTime)+" Earliest Time: "+earliestTime+" "+id);
		timer.start(earliestTime - (System.currentTimeMillis()-startTime));
	}

	/**
	 * The time has been reached.
	 * @param callbackId		TIMER_ACTIVATE   - calls activate(), 
	 *							TIMER_DEACTIVATE - calls deactivate()
	 *							TIMER_SIMPLEDUR  - calls simpleDurEnded()
	 */
	public synchronized void timeReached() {
		ListenerData tl;
		
		timer.stop();

		// Notify the elements
//		Log.debug("(time) = NOTIFY!");
		// while (zero times)? -> Notify & remove them!
		
		for (Enumeration e = listeners.elements() ; e.hasMoreElements() ;) {
			tl = (ListenerData)e.nextElement();
			if (tl.time <= System.currentTimeMillis()-startTime) {
			//	Log.debug("(time)  == notify : "+(tl.time)+" tag "+tl.element.getNodeName()+" id "+tl.element.getId()+
			//								" method "+tl.method);
				listeners.removeElement(tl);
				timeNotify(tl);
			}
		}

		// Restart Timer
		restartTimer();
	}

	/**
	 * Notify the listener that the time has gone.
	 */
	private void timeNotify(ListenerData ld) {

		if (ld.method == TIMER_ACTIVATE) {
			ld.element.activate();
		}
		if (ld.method == TIMER_DEACTIVATE) {
			ld.element.deactivate();
		}
		if (ld.method == TIMER_SIMPLEDUR) {
			ld.element.simpleDurEnded();
		}
	}

	/**
	 * Inner class to save listener related data.
	 */
	private class ListenerData {
		public ElementBasicTimeImpl element;
		public int method;
		public long time;
	}

	/**
	 * Inner Timer class. This timer can be started with a wait time and stopped as many times as wanted.
	 */
	private class Timer implements Runnable {
		private long last_wait_time = -1;
		private long wait_time = 0;
		private Thread thread = null;
		private Scheduler scheduler = null;
		private boolean stopped = true;

		public Timer(Scheduler s) {
			//thread = new Thread(this);
			scheduler = s;
		}
		
		/**
		 * Start the timer - if the timer was already started, it will be restarted.
		 * When the time has elapsed, this will call timeReached() in Scheduler.
		 * @param time		Time to wait, in millisecs
		 */
		public void start(long time) {
		//	if (last_wait_time == time && stopped == false)
		//		return;
		
			last_wait_time = time;
		
			stop();
			// Start the thread
			wait_time = time;
			stopped = false;
			thread = new Thread(this); // Could we reuse the old thread?
			thread.start();
		}
		
		/**
		 * Stops the timer. If already stopped, nothing happens.
		 */
		public void stop() {
			stopped = true;
			// Get out of the sleep()
			if (thread!=null && thread.isAlive())
				thread.interrupt();
		}
		/**
		 * Destroys the timer. 
		 */
		public void destroy() {
            this.stop();
            this.scheduler=null;
            this.thread=null;
		}
		
		public void run() {
			// sleep for time ms
			try {
//				Log.debug("(time) sleeping for "+wait_time);
				if (wait_time > 0 && stopped == false)
					Thread.sleep(wait_time);
			} catch (InterruptedException ie) {
				// No notification
				return;
			}
			// Notify scheduler
			if (stopped == false)
				scheduler.timeReached();
		}
	}

}
