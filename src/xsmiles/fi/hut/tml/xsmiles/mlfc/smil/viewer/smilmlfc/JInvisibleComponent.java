/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import java.awt.*;
import javax.swing.*;

/**
 * A Component that by default displays nothing. The Paint method
 * may be overridden for debugging purposes.
 * This class is used to display links.
 */
public class JInvisibleComponent extends JComponent {

    public JInvisibleComponent() {
    }
    
    public void paint(Graphics g) {
    // do nothing
    
    // for debugging - draws the borders of the component.
//     g.setColor(Color.blue);
//     g.drawRect(0, 0, getWidth()-1, getHeight()-1);
    }
}