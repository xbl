/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.media.Manager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.smil20.SMILDocument;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;

/**
 * This is the hidden functionality of the SMILComponent class, which is a
 * wrapper to hide all public methods in this class.
 */
public class JSMILPlayerCore implements Viewer {

	// XML file document path, without the filename
	private static String docPath = null;

	// Currently open filename
	private String filename = null;

	// true if JMF is available
	private boolean jmfAvailable = false;

	// SMIL presention document
	private SMILDocument smilDoc = null;

	// Screen size
	private int screenWidth = 640;
	private int screenHeight = 480;
	private int screenDepth = 16;

	// Container for the presentation
	private Container rootlayoutContainer = null;

	protected JSMILPlayerCore() {
		jmfAvailable = isJMFAvailable();
		// Don't show debugs!
		Log.enable_debug = false;
	}

	/**
	 * Initializes the SMIL presentation for playing.
	 * 
	 * @param c
	 *                  Root-layout container
	 * @param reader
	 *                  Reader, where the presentation is loaded from
	 * @param path
	 *                  Path to the file (can be a URL)
	 */
	protected boolean init(Container c, Reader reader, String path) {

		// Save presentation container
		rootlayoutContainer = c;

		// Get screen size
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension screenSize = tk.getScreenSize();
		screenWidth = screenSize.width;
		screenHeight = screenSize.height;
		screenDepth = tk.getColorModel().getPixelSize();

		// If no reader, then exit
		if (reader == null)
			return false;

		// Document URL
		try {
			docPath = path.substring(0, path.lastIndexOf('/'));
		} catch (StringIndexOutOfBoundsException e) {
			try {
				docPath = path.substring(0, path.lastIndexOf('\\'));
			} catch (StringIndexOutOfBoundsException e2) {
			}
		}

		Log.debug("Document Path is " + docPath);

		// Xerces Parser - requires xerces.jar and xml-apis.jar
		// The SMILDocumentImpl and SMILElementImpl and smil20 package must
		// be compiled with this classpath - no Palm parser should be in the
		// classpath
		SMILDocumentImpl smil = null;
		try {
			javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();

			// This fails for some reason
			//			factory.setNamespaceAware( true );

			factory.setAttribute(
				"http://apache.org/xml/properties/dom/document-class-name",
				"fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl");
			javax.xml.parsers.DocumentBuilder parser = factory.newDocumentBuilder();
			Log.debug("Xerces parser:" + parser.getClass() + ". NSpaces:" + parser.isNamespaceAware());
			org.xml.sax.InputSource input = new org.xml.sax.InputSource(reader);
			Document doc = parser.parse(input);
			smil = (SMILDocumentImpl) doc;
		} catch (javax.xml.parsers.ParserConfigurationException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText(filename + ": Error parsing.");
			return false;
		} catch (org.xml.sax.SAXException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText(filename + ": Error parsing.");
			return false;
		} catch (java.io.IOException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText(filename + ": Error parsing.");
			return false;
		}
		// End Xerces Parser

		Log.debug("SMIL PARSED");

		smilDoc = smil;
		smil.setViewer(this);

		// Initialise the document - this is size and color for root-layout
		// etc.
		smil.initialize(false);

		// Validate the browser's container to get the root-layout visible.
		rootlayoutContainer.validate();

		// Optional prefetch.
		smil.prefetch();
		return true;
	}

	/**
	 * Start the presentation.
	 */
	public void start() {
		if (smilDoc != null)
			 ((SMILDocumentImpl) smilDoc).start();
	}

	/**
	 * Stop the presentation.
	 */
	public void stop() {
		if (smilDoc != null)
			 ((SMILDocumentImpl) smilDoc).stop();
	}

	/**
	 * Clears all resources and frees memory. The presentation cannot be played
	 * after this call.
	 */
	public void destroy() {
		if (smilDoc != null) {
			((SMILDocumentImpl) smilDoc).freeResources(false);
			smilDoc = null;
		}
	}

	/**
	 * Return the base URL of the document.
	 */
	public URL getBaseURL() {
		// Overridden base url
		//if (baseURI != null) {
		//	return baseURI;
		//}

		try {
			return new URL(docPath);
		} catch (MalformedURLException e) {
			return null;
		}
	}

	/**
	 * Called from SMILDocument when an external link is activated.
	 * 
	 * @param url
	 *                  Link destination
	 */
	public void gotoExternalLink(String url) {
		Log.debug("...going to " + url);
		//    URL u = URLFactory.getInstance().createURL(url);
		//  EventBroker.getInstance().issueXMLDocRequiredEvent(u, true);
	}
	/**
	 * Open external link replacing/opening new target
	 * 
	 * @param url
	 *                  URL to open
	 * @param target
	 *                  target frame/window
	 */
	public void gotoExternalLinkTarget(String url, String target) {
		Log.debug("...going to" + url);
	}
	/**
	 * Open external link in a new window
	 * 
	 * @param url
	 *                  URL to open
	 */
	public void gotoExternalLinkNewWindow(String url) {
		Log.debug("...going to" + url);
	}

	/**
	 * Displays a status text, SMILDocument calls this.
	 */
	public void displayStatusText(String txt) {
		Log.debug("STATUS: " + txt);
		//statusText.setText(txt);
	}

	/**
	 * Called from the SMILDocument, when a new time point is found during
	 * prefetching.
	 */
	public void addTimePoint(String elementId) {
		//if (elementId != null)
		//timePoints.addItem(elementId);
	}

	/**
	 * Get the SMILDocument. Called from other Swing handlers.
	 * 
	 * @return Currently opened SMIL document.
	 */
	public SMILDocument getSMILDoc() {
		return smilDoc;
	}

	public void setDocumentBaseURI(String base) {
	}

	/**
	 * Creates a new MediaHandler. Works as a factory for SMILDocument.
	 */
	public MediaHandler getNewMediaHandler() {
		SwingMediaHandler mh = new SwingMediaHandler(docPath);
		mh.setViewer(this);
		return mh;
	}

	/**
	 * Returns a new BrushHandler for SMIL core logic.
	 */
	public BrushHandler getNewBrushHandler() {
		SwingBrushHandler bh = new SwingBrushHandler();
		bh.setViewer(this);
		return bh;
	}

	/**
	 * Creates a new LinkHandler. Works as a factory for SMILDocument.
	 */
	public LinkHandler getNewLinkHandler() {
		LinkHandler lh = new SwingLinkHandler();
		lh.setViewer(this);
		return lh;
	}

	/**
	 * Creates a new DrawingArea. Works as a factory for SMILDocument.
	 */
	public DrawingArea getNewDrawingArea(int type, boolean block) {
		if (type == DrawingArea.ROOTLAYOUT) {
			return new SwingDrawingArea(rootlayoutContainer);
		} else
			return new SwingDrawingArea(type);
	}

	/**
	 * Returns a new ForeignHandler for SMIL core logic. NOT IMPLEMENTED IN
	 * STANDALONE PLAYER.
	 */
	public MediaHandler getNewForeignHandler(Element e) {
		return null;
	}

	/**
	 * Returns window width for the SMILDocument.
	 */
	public int getWindowWidth() {
		return 700;
	}
	/**
	 * Returns window height for the SMILDocument.
	 */
	public int getWindowHeight() {
		return 500;
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemBitrate() {
		return "14400";
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemCaptions() {
		return "false";
	}

	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemLanguage() {
		return "fi";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemOverdubOrCaption() {
		return "caption";
	}

	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemRequired(String prefix) {
		return false;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public int getSystemScreenWidth() {
		return screenWidth;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public int getSystemScreenHeight() {
		return screenHeight;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public int getSystemScreenDepth() {
		return screenDepth;
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemOverdubOrSubtitle() {
		return "overdub";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemAudioDesc() {
		return "on";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemOperatingSystem() {
		return "Linux";
	}
	/**
	 * Returns a system attribute for the SMILDocument.
	 */
	public String getSystemCPU() {
		return "x386";
	}
	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemComponent(String component) {
		return false;
	}

	/**
	 * Returns true if images should be played. Called from handlers.
	 */
	public boolean getPlayImage() {
		return true;
	}

	/**
	 * Returns true if audio should be played. Called from handlers.
	 */
	public boolean getPlayAudio() {
		return jmfAvailable;
	}

	/**
	 * Returns true if video should be played. Called from handlers.
	 */
	public boolean getPlayVideo() {
		return jmfAvailable;
	}

	/**
	 * Get the title of the presentation. null if no title present.
	 */
	public String getTitle() {
		return null;
	}

	/**
	 * Set the title for the presentation.
	 * 
	 * @param title
	 *                  Title for the presentation
	 */
	public void setTitle(String title) {
		return;
	}

	public boolean isHost() {
		return true;
	}

	/**
	 * Check if JMF class is available. At the same time, set the hint...
	 * 
	 * @return true if JMF is available, false otherwise
	 */
	private boolean isJMFAvailable() {
		try {
			Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true));
		} catch (Throwable e) {
			System.out.println("JMF not available! It is recommended to have it installed.");
			System.out.println("Video will not be played.");
			return false;
		}
		return true;
	}
	// TODO: should this return the decorator from the SMIL MLFC?
	public Decorator getDecorator()
	{
	    return null;
	}
	
}
