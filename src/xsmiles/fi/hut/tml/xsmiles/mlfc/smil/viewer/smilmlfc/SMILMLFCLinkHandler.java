/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;

// TODO: move to components
import fi.hut.tml.xsmiles.gui.components.ActionEventEx;

import org.w3c.dom.smil20.SMILDocument;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JComponent;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingLinkHandler;
import fi.hut.tml.xsmiles.gui.components.XLinkComponent;
import fi.hut.tml.xsmiles.gui.components.XHoverListener;
import fi.hut.tml.xsmiles.gui.components.XItemListener;
import fi.hut.tml.xsmiles.gui.components.XItemEvent;

/**
 *  Implements links for SMILMLFC. Amost exact copy of Swing version.
 */
public class SMILMLFCLinkHandler extends SwingLinkHandler implements LinkHandler {

	/**
	 * SMILMLFC will use XLinkComponent instead of JInvisibleComponent.
	 */
	public void play() {

		if (container == null) {
			Log.debug("No drawing area.container for link "+url);
			return;
		}

		XLinkComponent c = ((SMILMLFC)viewer).getMLFCListener().getComponentFactory().getXLinkComponent("");
		c.addClickedActionListener(new ClickListener());
		c.addHoverListener(new URLFListener(this));
		linkComp = c.getComponent();

//		linkComp.addMouseListener(this);
		// Add this link on top of everything
		// Add to the top most in the region. (Assumes no images added to the same region after this.)
		container.add(linkComp, 0);
		linkComp.setBounds(left, top, width, height);
		linkComp.setEnabled(true);
		linkComp.setVisible(true);
//		linkComp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}


	private class ClickListener implements ActionListener {
	
	  public void actionPerformed(ActionEvent e) {
		  viewer.displayStatusText("");

		  if (mediaListener != null)
                  {
                      MouseEvent me = null;
                      if (e instanceof ActionEventEx)
                          me=((ActionEventEx)e).getMouseEvent();
		  	mediaListener.mouseClicked(me);
                  }

//		  if (isLinkToSameDocument(url)) {
//			  gotoInternalLink(url);	  
//		  }
//		  else {
//		      Log.debug("External link clicked, going to url: " + url);
//		  	viewer.gotoExternalLink(url);
//		  }
	  }
	}
	
	private class URLFListener implements XHoverListener {
	  SMILMLFCLinkHandler handler;
	  public URLFListener(SMILMLFCLinkHandler h) {
	  	handler = h;
	  }

	  public void focusGained(String s) {
		  if (linkTitle == null)
		  	handler.viewer.displayStatusText(url);
		  else
		  	handler.viewer.displayStatusText(linkTitle);	
	  }
	  public void focusLost() {
		  handler.viewer.displayStatusText("");
	  }
	
	}

}

