/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

//import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

/**
 *  Interface to media. 
 */
public interface MediaHandler {

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static.
	 */
	public boolean isStatic();

	/** 
	 * Adds a listener for this media. The listener is notified when the media has been 
	 * prefetched or ends.
	 */
	public void addListener(MediaListener mediaListener);

	/**
	 * Sets the alt text for the media.
	 */	
	public void setAlt(String alt);
	
	/**
	 * Sets the URL of the media.
	 */
	public void setURL(String url);

	/** 
	 * Set the viewer - required for prefetch, URL creation etc.
	 */
	public void setViewer(Viewer v);
	/**
	 * Force this media to use this media type
	 */	
	public void setMIMEType(String mimeType);
	
	public void prefetch();
	public void play();
	public void pause();
	public void stop();
	public void freeze();
	public void close();
	
	/**
	 * Set the media start time.
	 * @param	millisecs		Time in milliseconds
	 */
	public void setMediaTime(int millisecs);
	
	/**
	 * Set the volume of audio (if available). 
	 * @param percentage      0-100-oo , 100 giving normal sound level.
	 */
	public void setAudioVolume(int percentage);

	/**
	 * Set the drawing area - media will be rendered in this drawing area.
	 */
	public void setDrawingArea(DrawingArea d);
        
        /**
         * get the possible component (only used when run in x-smiles
         */
        public Object getComponent();
	/**
	 * Set the drawing area size.
	 */
	public void setRootLayoutSize(int width, int height);

	public int getTop();	
//	public void setTop(int top);
	public int getLeft();
//	public void setLeft(int left);
//	public int getBottom();
//	public void setBottom(int bottom);
//	public int getRight();
//	public void setRight(int right);
	public int getWidth();
	public int getHeight();
	
	public void setBounds(int left, int top, int width, int height);
	
	/** 
	 * Get the real width of the media.
	 */
	public int getOriginalWidth();

	/** 
	 * Get the real height of the media.
	 */
	public int getOriginalHeight();

}

