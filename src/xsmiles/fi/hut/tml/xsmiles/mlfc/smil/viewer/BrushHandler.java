/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

/**
 *  Interface to brush. 
 */
public interface BrushHandler extends MediaHandler {
	
	/**
	 * Sets the color of the brush.
	 */
	public void setColor(String color);

}

