/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.BrushHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.net.URLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JPanel;
import javax.swing.JButton;

import fi.hut.tml.xsmiles.Log;

/**
 *  Swing Brush Handler. Displays a colored panel.
 */
public class SwingBrushHandler implements BrushHandler, MouseListener, Runnable {

	protected String alt, url;
	protected MediaListener mediaListener;
	protected SwingDrawingArea drawingArea;
	protected Container container;

	private SMILDocumentImpl smilDoc = null;
	private Viewer viewer = null;

	protected boolean prefetched = false;
	
	protected int top = 0, left = 0;
	protected int width = 0, height = 0;

	private JPanel brushPanel = null;

	/**
	 * Create a new BrushHandler.
	 */
	public SwingBrushHandler() {
		brushPanel = new JPanel();

		// Default transparent background color		
//		brushPanel.setOpaque(false);
		brushPanel.setVisible(false);
		
		// This will capture all mouse events
		brushPanel.addMouseListener(this);
	}

	public void setViewer(Viewer v) {
		viewer = v;
		smilDoc = (SMILDocumentImpl)(viewer.getSMILDoc());
	}

	/**
	 * Set the drawing area. The given drawing area MUST be a SwingDrawingArea.
	 */
	public void setDrawingArea(DrawingArea d) {
		// The given drawing area MUST be SwingDrawingArea
		drawingArea = (SwingDrawingArea)d;
		Container container = drawingArea.getContentContainer();

		if (this.container != container) {

			// Change container
			if (brushPanel.isVisible() == true) {
				brushPanel.setVisible(false);
				this.container.remove(brushPanel);
				container.add(brushPanel, 0);
				brushPanel.setVisible(true);
			}

			this.container = container;
		}

	}
	
	/**
	 * Add a media listener (currently supports only one listener)
	 */
	public void addListener(MediaListener mediaListener) {
		this.mediaListener = mediaListener;
	}
	
	/**
	 * Checks if this media is static or continuous.
	 * @return always true, because this is static brush.
	 */
	public boolean isStatic() {
		return true;
	}
	
	/**
	 * This media handler will know the rootlayout size after this is called.
	 * @param width		RootLayout width
	 * @param height	RootLayout height
	 */
	public void setRootLayoutSize(int width, int height) {
	}
	
	public void setAlt(String alt) {
		this.alt=alt;
	}
	public void setURL(String url) {
		this.url = url;
	}

	public void setColor(String color)  {
		Color c = ColorConverter.hexaToRgb(color);
		brushPanel.setBackground(c);
//		brushPanel.setOpaque(true);
	}

	public void setMIMEType(String type) {
	}

	public void prefetch() {
		Log.debug("BRUSH.prefetch() ");
		prefetched = true;		
	}

	public void play() {
		Log.debug("BRUSH.play() ");

		// If this media hasn't yet been prefetched, then prefetch it.
		if (prefetched == false)
			prefetch();

		if (container != null) {
			container.add(brushPanel, 0);
			// Set up text component
			brushPanel.setLocation(left, top);
			brushPanel.setPreferredSize(new Dimension(width, height));
			brushPanel.setSize(width, height);
			brushPanel.setVisible(true);
		} else
			Log.error("Region container not set for brush");

		// Media ends immediately for static media (almost..).
		// Media ended - inform the SMIL player.
		if (mediaListener != null) {
			Thread t = new Thread(this);
			t.start();
		}
	}

	public void run()
	{
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		// Media ended immediately (almost...)
		mediaListener.mediaEnded();
	}

	public void pause() {
	}

	public void stop() {
		brushPanel.setVisible(false);
		if (container != null)
			container.remove(brushPanel);

		// Clear media listeners
//		if (mediaListener != null)
//			mediaListener = null;
	}
	
	/**
	 * Brush freeze - won't do anything.
	 */
	public void freeze() {
	}
	
	public void close() {
		stop();
	
		viewer = null;
		smilDoc = null;
		container = null;
		
		brushPanel = null;
	}

	/**
	 * Set the media time position. Not implemented for brushes.
	 * @param millisecs			Time in milliseconds
	 */
	public void setMediaTime(int millisecs) {
	}
	
	/** 
	 * Get the real width of the media.
	 */
	public int getOriginalWidth() {
		return -1;	
	}

	/** 
	 * Get the real height of the media.
	 */
	public int getOriginalHeight() {
		return -1;	
	}

	/**
	 * Set the sound volume for media. Does nothing for brushes.
	 */
	 public void setAudioVolume(int percentage) {
	 }

	
	public int getTop() {
		return top;
	}
	public int getLeft() {
		return left;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setBounds(int x, int y, int w, int h) {
		left = x;
		top = y;
		width = w;
		height = h;
		updateSize();
	}
	
	private void updateSize()  {
		if (brushPanel.isVisible() == true)  {
			brushPanel.setLocation(left, top);
			brushPanel.setPreferredSize(new Dimension(width, height));
			brushPanel.setSize(width, height);
		}
	}
	
	/**
	 * Mouse listener...
	 */
	public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}
	
	public void mouseEntered(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseEntered();
	}

	public void mouseExited(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseExited();
	}
	
	public void mousePressed(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mousePressed();
	}
	
	public void mouseReleased(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseReleased();
	}	
        public Object getComponent()
        {
            Log.info(this+".getComponent() called, returning null!");
            return null;
        }    
}

