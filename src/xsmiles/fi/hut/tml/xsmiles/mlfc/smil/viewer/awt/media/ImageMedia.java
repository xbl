/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.FlowLayout;
import java.awt.Graphics;

import java.net.URL;
import java.net.MalformedURLException;

import fi.hut.tml.xsmiles.Log;

/**
 * This is the implementation of image media.
 */
public class ImageMedia implements Media, Runnable, MouseListener {

  	// Hashtable holding loaded images - this is a proxy
	private Hashtable loadedImages;
  
	// ImageCanvas for current image
	ImageCanvas jlabel;

	// Container for media (DrawingArea)
	Container container = null;

	// MediaListener - called after prefetch and endOfMedia.
	MediaListener mediaListener = null;

	// Location and coords for the media
	int x=0, y=0, width=0, height=0;

	Image pic = null;

	public ImageMedia() {
		// Create the components		
		jlabel=new ImageCanvas();

		// Create the proxy hashtable
		loadedImages = new Hashtable();		
		
		// This will get all mouse events, and pass them to the MediaListener
		jlabel.addMouseListener(this);			
	}

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static.
	 */
	public boolean isStatic() {
		return true;
	}

	URL url = null;

	public void setUrl(URL url) {
		this.url = url;
	}

	public void prefetch() {
//		url = "D:\\XSmiles\\work\\demo\\smil\\GUIdemo\\BigFO.gif";
		Log.debug("IMAGEMEDIA: "+this+".prefetch("+url+") ");
		
		Toolkit tk=Toolkit.getDefaultToolkit();
		pic=tk.getImage(url);
		MediaTracker mt=new MediaTracker(jlabel);
		mt.addImage(pic, 1);
		try { 
		    mt.waitForID(1);
		} catch (InterruptedException e) {
		    Log.error(e);
		}
		// Add image to the proxy		
		loadedImages.put(url.toString(),pic);

		jlabel.setImage(pic);
//		jlabel.setSize(size);
//		jlabel.setLocation(0, 0);
		jlabel.setVisible(false);
	}

	/**
	 * Set the container. Must be dynamic, the media must move if container changes.
	 */
	public void setContainer(Container container) {
		if (this.container != container) {

			// Change container
			if (jlabel.isVisible() == true) {
				jlabel.setVisible(false);
				this.container.remove(jlabel);
				container.add(jlabel, 0);
				jlabel.setVisible(true);
			}

			this.container = container;
		}
	}

	/** 
	 * Displays the image media. Also calls mediaEnded() immediately - this is static media,
	 * and therefore will "freeze" immediately.
	 */
	public void play() {
		if (container != null) {
			container.add(jlabel, 0);
			jlabel.setLocation(x, y);
			jlabel.setSize(width, height);
			jlabel.setVisible(true);
		} else
			Log.error("Region container not set for media "+url.toString());
		
		// Media ends immediately for static media (almost..).
		// Media ended - inform the SMIL player.
		if (mediaListener != null) {
			Thread t = new Thread(this);
			t.start();
		}
		
	}

	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		// Media ended immediately (almost...)
		if (mediaListener != null)
			mediaListener.mediaEnded();
	}

	public void pause() {
	}

	public void stop() {
		if (jlabel != null) {
			jlabel.setVisible(false);
			if (container != null)
				container.remove(jlabel);
		}
		
		// Clear media listeners
		if (mediaListener != null)
			mediaListener = null;

	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;

		this.width = width;
		this.height = height;	
		
		// If the image is shown on the screen, move it immediately
		if (jlabel.isVisible() == true) {
			// Use the given coordinates
			jlabel.setLocation(x, y);
			jlabel.setSize(width, height);     
		}			
	}


	/** 
	 * Get the real width of the media.
	 */
	public int getOriginalWidth() {
		if (pic != null)
			return pic.getWidth(jlabel);
		else
			return -1;	
	}

	/** 
	 * Get the real height of the media.
	 */
	public int getOriginalHeight() {
		if (pic != null)
			return pic.getHeight(jlabel);
		else
			return -1;	
	}

	public void close() {
		stop();

		if (container != null)
			container.remove(jlabel);

		// Flush all images
		if (loadedImages != null) {
			Enumeration i = loadedImages.elements();
			while(i.hasMoreElements()) {
				((Image)i.nextElement()).flush();
			}
		 
			loadedImages.clear();
		}
		loadedImages = null;
		jlabel = null;
		mediaListener = null;
	}
	
	/**
	 * This moves the time position in media. Not effective for this media.
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs) {
	 }

	 /**
	  * Set the sound volume for media. Only applicable for sound media formats.
	  * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
	  * dB change in signal level = 20 log10(percentage / 100) )
	  */
	  public void setSoundVolume(int percentage) {
	  }
	

	public void addMediaListener(MediaListener listener) {
		mediaListener = listener;
	}

	/**
	 * Mouse listener...
	 */
	public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}
	
	public void mouseEntered(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseEntered();
	}

	public void mouseExited(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseExited();
	}
	
	public void mousePressed(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mousePressed();
	}
	
	public void mouseReleased(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseReleased();
	}	

	/**
	 * Simple class to draw images. This is required to scale the images.
	 */
	public class ImageCanvas extends Component {
		int width = 0;
		int height = 0;
		Image image = null;

		public ImageCanvas() {
		}

		public void setImage(Image i) {
			image = i;
		}

		public void setSize(int w, int h) {
			width = w;
			height = h;
			super.setSize(w, h);
//			super.setPreferredSize(new Dimension(w, h));
		}

		public void paint(Graphics g) {
			// Draw image
			if (image != null)
				g.drawImage(image, 0, 0, width, height, null, this);

			return;
		}  
	} 
	
	
}