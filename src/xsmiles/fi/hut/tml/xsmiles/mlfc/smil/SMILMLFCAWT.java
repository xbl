/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.smil;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Decorator;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;

// This will import JMF Manager for JMF availability test
//import javax.media.Manager;
/**
 * SMILMLFC is the SMIL Viewer for X-Smiles browser. It controls the SMIL core
 * functionality and handles the data exchange between the SMIL player and the
 * broswer. This is also a factory for MLFC specific MediaHandlers,
 * LinkHandlers and DrawingAreas.
 */
public class SMILMLFCAWT extends SMILMLFC implements Viewer {

    protected void createDecorator() {
        String decorClassName = "fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.AWTDecorator";
        try {
            Class c = Class.forName(decorClassName);
            this.decorator = (Decorator) c.newInstance();
        } catch (Throwable t) {
            Log.error(t, "Could not instantiate " + decorClassName + ". SMIL will not work");
        }
    }
}
