/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  This is a placeholder - subject to change. This represents generic 
 * timelines. 
 * This doesn't implement childEnded() in ElementTimeContainer interface.
 */
public abstract class ElementTimeContainerImpl extends ElementTimeImpl implements ElementTimeContainer {

	/**
	 * Constructor - Set the owner and name.
	 */
	public ElementTimeContainerImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
     public String getFill() {
        return getAttribute("fill");
     }
     public void setFill(String fill) throws DOMException {
     	if (fill == null)
     		removeAttribute("fill");
     	else
     		setAttribute("fill", fill);
     	return;
     }

    /**
     *  Returns a list of child elements active at the specified invocation. 
     * @param instant  The desired position on the local timeline in 
     *   milliseconds. 
     * @return  List of timed child-elements active at instant. 
     */
    public NodeList getActiveChildrenAt(int instant) {
		return null;
    }

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr() {
    	return getAttribute("abstract");
    }
    public void setAbstractAttr(String abstractAttr) throws DOMException {
        if (abstractAttr != null && abstractAttr.length() > 0) {
          setAttribute("abstract", abstractAttr);
        } else {
          removeAttribute("abstract");
        }
    }

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor() {
    	return getAttribute("author");
    }
    public void setAuthor(String author) throws DOMException {
        if (author != null && author.length() > 0) {
          setAttribute("author", author);
        } else {
          removeAttribute("author");
        }
    }

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright() {
    	return getAttribute("copyright");
    }
    public void setCopyright(String copyright) throws DOMException {
        if (copyright != null && copyright.length() > 0) {
          setAttribute("copyright", copyright);
        } else {
          removeAttribute("copyright");
        }
    }

    /**
     *  See the region attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getRegion() {
	    return null;
    }
    public void setRegion(String region) throws DOMException {
	    if (region != null && region.length() > 0) {
	      setAttribute("region", region);
	    } else {
	      removeAttribute("region");
	    }
    }



}

