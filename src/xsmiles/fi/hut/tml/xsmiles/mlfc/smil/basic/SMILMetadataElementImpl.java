/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.*;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  Declares smil metadata element.
 *  
 */
public class SMILMetadataElementImpl extends SMILElementImpl implements XSMILMetadataElement {

  /**
   * Constructor with owner doc
   */
  public SMILMetadataElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
    super(owner, smil, ns, "metadata");
  }

}