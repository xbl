/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;

/**
 *  The synchronization behavior extension. 
 */
public class ElementSyncBehaviorImpl {
    /**
     *  The runtime synchronization behavior for an element. 
     */
    public String getSyncBehavior() {
		return null;
    }

    /**
     *  The sync tolerance for the associated element. It has an effect only if
     *  the element has <code>syncBehavior="locked"</code> . 
     */
    public float getSyncTolerance() {
		return 0;
    }

    /**
     *  Defines the default value for the runtime synchronization behavior for 
     * an element, and all descendents. 
     */
    public String getDefaultSyncBehavior() {
		return null;
    }

    /**
     *  Defines the default value for the sync tolerance for an element, and 
     * all descendents. 
     */
    public float getDefaultSyncTolerance() {
		return 0;
    }

    /**
     *  If set to true, forces the time container playback to sync to this 
     * element.  
     */
    public boolean getSyncMaster() {
		return false;
    }

}

