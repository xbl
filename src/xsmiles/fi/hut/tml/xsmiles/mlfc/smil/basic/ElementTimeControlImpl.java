/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import java.util.Hashtable;
import java.util.Enumeration;

import org.w3c.dom.DOMException;

/**
 * Blank Time Controls.
 * Also, contains the code to initialize CSS styling. This init must only be done for
 * elements under the body element, i.e. timed elements.
 */
public class ElementTimeControlImpl extends SMILElementImpl 
						implements ElementTimeControl, StylableElement {

	// Table of shadow elements (key: visualcomponentservice, value: shadowelement)
	Hashtable shadowElements = null;

	/**
	 * Constructor - Set the owner and name.
	 */
	ElementTimeControlImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}
	
    /**
     *  Causes this element to begin the local timeline (subject to sync 
     * constraints). Will start the media immediately, resolving the time.
	 * This method implements the basic timing for media. This should be
	 * overridden in time containers to provide correct handling of the children.
	 *
     * @return  <code>true</code> if the method call was successful and the 
     *   element was begun. <code>false</code> if the method call failed. 
     *   Possible reasons for failure include:  The element doesn't support 
     *   the <code>beginElement</code> method (KP: ODD!). (the <code>begin</code> 
     *   attribute is not set to <code>"indefinite"</code> )  The element is 
     *   already active and can't be restart when it is active. (the 
     *   <code>restart</code> attribute is set to <code>"whenNotActive"</code>
     *    )  The element is active or has been active and can't be restart. 
     *   (the <code>restart</code> attribute is set to <code>"never"</code> ).
     *    
     * @exception DOMException
     *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
     *   to allow <code>beginElement</code> calls. 
     */
	public boolean beginElement() throws DOMException {
		return true;
	}

    /**
     *  Causes this element to begin the local timeline (subject to sync 
     * constraints), at the passed offset from the current time when the 
     * method is called. If the offset is &gt;= 0, the semantics are 
     * equivalent to an event-base begin with the specified offset. If the 
     * offset is &lt; 0, the semantics are equivalent to beginElement(), but 
     * the element active duration is evaluated as though the element had 
     * begun at the passed (negative) offset from the current time when the 
     * method is called. 
     * @param offset  The offset in seconds at which to begin the element. 
     * @return  <code>true</code> if the method call was successful and the 
     *   element was begun. <code>false</code> if the method call failed. 
     *   Possible reasons for failure include:  The element doesn't support 
     *   the <code>beginElementAt</code> method. (the <code>begin</code> 
     *   attribute is not set to <code>"indefinite"</code> )  The element is 
     *   already active and can't be restart when it is active. (the 
     *   <code>restart</code> attribute is set to <code>"whenNotActive"</code>
     *    )  The element is active or has been active and can't be restart. 
     *   (the <code>restart</code> attribute is set to <code>"never"</code> ).
     *    
     * @exception DOMException
     *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
     *   to allow <code>beginElementAt</code> calls. 
     */
	public boolean beginElementAt(int offset) throws DOMException {
		return true;
	}

    /**
     *  Causes this element to end the local timeline (subject to sync 
     * constraints). 
     * @return  <code>true</code> if the method call was successful and the 
     *   element was ended. <code>false</code> if method call failed. 
     *   Possible reasons for failure include:  The element doesn't support 
     *   the <code>endElement</code> method. (the <code>end</code> attribute 
     *   is not set to <code>"indefinite"</code> )  The element is not active.
     *    
     * @exception DOMException
     *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
     *   to allow <code>endElement</code> calls. 
     */
	public boolean endElement() throws DOMException {
		return true;
	}

    /**
     *  Causes this element to end the local timeline (subject to sync 
     * constraints) at the specified offset from the current time when the 
     * method is called. 
     * @param offset  The offset in seconds at which to end the element. Must 
     *   be &gt;= 0. 
     * @return  <code>true</code> if the method call was successful and the 
     *   element was ended. <code>false</code> if method call failed. 
     *   Possible reasons for failure include:  The element doesn't support 
     *   the <code>endElementAt</code> method. (the <code>end</code> 
     *   attribute is not set to <code>"indefinite"</code> )  The element is 
     *   not active. 
     * @exception DOMException
     *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
     *   to allow <code>endElementAt</code> calls. 
     */
	public boolean endElementAt(int offset) throws DOMException {
		return true;
	}

	/**
	 *  Causes this element to pause the local timeline (subject to sync 
	 * constraints). 
	 */
	public void pauseElement() {
		return;
	}
	
	/**
	 *  Causes this element to resume a paused local timeline. 
	 */
	public void resumeElement() {
		return;
	}
	
	/**
	 *  Seeks this element to the specified point on the local timeline 
	 * (subject to sync constraints).  If this is a timeline, this must seek 
	 * the entire timeline (i.e. propagate to all timeChildren). 
	 * @param seekTo  The desired position on the local timeline in 
	 *   milliseconds. 
	 */
	public void seekElement(int seekTo) {
		return;
	}



	/** CSS support **/

	/**
	 * CSS: Initialize this smil element. This does the CSS initialization.
	 */
	public void init() {
		// If CSS is turned on, init the style in the element
            /*
		if (smilDoc.getCSSLayoutModel() == true)
			initStyle(smilDoc.getStyleSheet());*/

		// Initialize children before creating a shadow for them.
		super.init();	
		
		// Create shadow elements for elements implementing VisualComponentService.
		NodeList childList = getChildNodes();
		Node child = null;
		ShadowElementImpl shadow = null;
		shadowElements = new Hashtable();
		
		for (int i=0 ; i < childList.getLength() ; i++) {
			child = childList.item(i);
			if (child instanceof VisualComponentService && !(child instanceof SMILElement)) {
				shadow = new ShadowElementImpl(ownerDoc, smilDoc, "ns", "shadow", (Element)child);
				shadow.init();
				shadowElements.put(child, shadow);
			}
                        else if (child instanceof CompoundService)
                        {
				shadow = new ShadowElementImpl(ownerDoc, smilDoc, "ns", "shadow", (Element)child);
				shadow.init();
				shadowElements.put(child, shadow);
                            
                        }
		}

	}
	
	/**
	 * Destroy shadow elements.
	 */
	public void destroy()  {
		ShadowElementImpl shadow = null;
		
		// Go through all shadows.
		if (shadowElements!=null)
		{    
			for (Enumeration e = shadowElements.elements() ; e.hasMoreElements() ;)  {
				shadow = (ShadowElementImpl)e.nextElement();
				shadow.destroy();  // This also calls closedown: region.removeMedia() & media.close()
			}
			shadowElements.clear();
		}
		super.destroy();
	}

	/**
	 * Get the shadow element for real VisualComponentService element.
	 */
	public ShadowElementImpl getShadowElement(Object v)  {
		return (ShadowElementImpl)shadowElements.get(v);
	}

	/** CSS: Support the style attribute for CSS */
	public String getStyleAttrValue()
	{
		return getAttribute("style");
	}

	/**
	 * CSS: Initializes the style for this element.
	 */
	private void initStyle_REMOVED(XSmilesStyleSheet styleSheet)
	{
		if (styleSheet==null) 
			return;

		// TODO: remove this test, after moving to the new CSS implementation
		this.setStyle(styleSheet.getParsedStyle(this));
		
//		Log.debug("InitStyle: "+this+": "+style);
//		CSSStyleDeclaration style = this.getStyle();
//		CSSValue bgValue = style.getPropertyCSSValue("color");
//		if (bgValue != null)
//			Log.debug("CSS VALUE DUMP - color:"+bgValue.getCssText());
//		else
//			Log.debug("CSS VALUE DUMP - color:"+null);
			
//		Log.debug("left - "+style.getPropertyCSSValue("left"));
//		Log.debug("r - "+style.getPropertyCSSValue("right"));
//		Log.debug("t - "+style.getPropertyCSSValue("top"));
//		Log.debug("bot - "+style.getPropertyCSSValue("bottom"));
//		Log.debug("w - "+style.getPropertyCSSValue("width"));
//		Log.debug("h - "+style.getPropertyCSSValue("height"));
	}

	/**
	 * CSS: Return the CSS style value.
	 * @param prop		Property
	 * @return 			Property value
	 */
	public String getStyleString(String prop) {
		CSSStyleDeclaration style = this.getStyle();
		CSSValue val = style.getPropertyCSSValue(prop);
		if (val != null)
			return val.getCssText();
		return "";
	}

}

