/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  A SMIL document is the root of the SMIL Hierarchy and holds the entire 
 * content. Beside providing access to the hierarchy, it also provides some 
 * convenience methods for accessing certain sets of information from the 
 * document.  Cover document timing, document locking?, linking modality and 
 * any other document level issues. Are there issues with nested SMIL files?  
 * Is it worth talking about different document scenarios, corresponding to 
 * differing profiles? E.g. Standalone SMIL, HTML integration, etc. 
 */
//#extends ElementTestImpl.java, XElementCustomTestImpl.java;
public class SMILParElementImpl extends ElementParallelTimeContainerImpl
				implements XSMILParElement  {

	/**
	 * Constructor with owner doc
	 */
	public SMILParElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
	  super(owner, smil, ns, "par");
	}

//// GENERATED CODE - DO NOT CHANGE ANYTHING BELOW ////
// File ElementTestImpl.java inherited.
    /**
     *  The  systemBitrate value. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getSystemBitrate() {
		return 0;
    }
    public void setSystemBitrate(int systemBitrate)  throws DOMException {
		return;
    }

    /**
     *  The  systemCaptions value. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getSystemCaptions() {
		return true;
    }
    public void setSystemCaptions(boolean systemCaptions) throws DOMException {
		return;
    }

    /**
     *  The  systemLanguage value. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSystemLanguage() {
		return null;
    }
    public void setSystemLanguage(String systemLanguage) throws DOMException {
		return;
    }

    /**
     *  The result of the evaluation of the  systemRequired attribute. 
     */
    public boolean getSystemRequired() {
		return true;
    }
    public void setSystemRequired(String systemRequired) throws DOMException {
		return;
    }

    /**
     *  The result of the evaluation of the  systemScreenSize attribute. 
     */
    public boolean getSystemScreenSize() {
		return true;
    }
    public void setSystemScreenSize(String systemScreenSize) throws DOMException {
		return;
    }

    /**
     *  The result of the evaluation of the  systemScreenDepth attribute. 
     */
    public boolean getSystemScreenDepth() {
		return true;
    }
    public void setSystemScreenDepth(String systemScreenDepth) throws DOMException {
		return;
    }

    /**
     *  The value of the  systemOverdubOrSubtitle attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSystemOverdubOrSubtitle() {
		return null;
    }
    public void setSystemOverdubOrSubtitle(String systemOverdubOrSubtitle) throws DOMException {
		return;
    }

    /**
     *  The value of the  systemAudioDesc attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getSystemAudioDesc() {
		return true;
    }
    public void setSystemAudioDesc(boolean systemAudioDesc) throws DOMException {
		return;
    }

    /**
     *  The value of the  systemOperatingSystem attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getSystemOperatingSystem() {
		return true;
    }
    public void setSystemOperatingSystem(boolean systemOperatingSystem) throws DOMException {
		return;
    }
									
    /**
     *  The value of the  systemCPU attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getSystemCPU() {
		return true;
    }
    public void setSystemCPU(boolean systemCPU) throws DOMException {
		return;
    }

    /**
     *  The value of the  systemComponent attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getSystemComponent() {
		return true;
    }
    public void setSystemComponent(boolean systemComponent) throws DOMException {
		return;
    }

    /**
     *  The value of the  system-overdub-or-caption attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getSystemOverdubOrCaption() {
		return true;
    }
    public void setSystemOverdubOrCaption(boolean systemOverdubOrCaption) throws DOMException {
		return;
    }
									  

// File XElementCustomTestImpl.java inherited.
    /**
     *  The value of the customTest attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCustomTest() {
		return null;
    }
    public void setCustomTest(String customTest) throws DOMException {
		return;
    }
}