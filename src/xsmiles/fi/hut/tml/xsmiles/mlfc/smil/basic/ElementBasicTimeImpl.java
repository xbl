/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.smil20.*;
import org.w3c.dom.events.DocumentEvent;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;
import fi.hut.tml.xsmiles.Log;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Hashtable;
import java.util.Enumeration;

/**
 *  This interface defines the set of basic timing attributes that are common to all 
 * timed elements. 
 */
public class ElementBasicTimeImpl extends ElementTimeControlImpl implements XElementBasicTime {

	// Zero value
	protected static final MyFloat zero = new MyFloat(0);

	// Time Lists for this element
	private TimeListImpl beginList, endList;

	// Instance Time Lists for this element
	private TimeListImpl beginInstanceList, endInstanceList;

	// Duration, which will be calculated only once.
	private TimeImpl duration = null;
	
	// Current interval begin and end times, null = no begin or end
	protected Time currentIntervalBegin, currentIntervalEnd;

	// Previous interval begin and end times, null = no begin or end. Used to get next interval.
	protected Time previousIntervalBegin, previousIntervalEnd;

	// Flag to tell the state of this element
	protected int state = 0;

	// Clip Begin Time - this is non-zero if the begin time is negative
	// MediaElement should use this to fastforward media to this time point.
	protected Time clipBeginTime;
	
	// This is set, if Simple Duration is unresolved and repeatCount is defined.
	protected float manualRepeat = 0;

	// This is the iteration number of the repeat
	protected int repeatIteration = 0;
	
	// Start time of the simple duration, relational to the startUp() time of this element.
	// This variable tries to keep track of the accurate time,
	// i.e. not by taking real time, but by accumulating single duarations
	protected long beginTime = 0;
	protected long repeatTime = 0;
	
	// Time used between activate() and deactivate()
	// This is ActiveDuration (preferred), or if accurate value is not available
	// (i.e. in case of media), this will be approximate media length time
	// This is used to tell parent (seq) container the exact duration of the child
	protected long accActiveDuration = 0;

	// Simple Duration and active duration
	int simpleTime = 0;
	protected Time activeDuration = null;
	
	// The activate() time of this element. Used to calculate the current time in this element.
	// Maybe this should be replace with something that is relative to the body time space?
	protected long startupTime = 0;

	// Used to calculate active duration for media end event times
	protected long activateTime = 0;

	// Element activation time thanks to linking or scripting
	protected long elementForceStartTime = 0;

	protected static final int STATE_UNINIT		= 0;
	protected static final int STATE_IDLE		= 1;
	protected static final int STATE_WAIT		= 2;
	protected static final int STATE_PLAY		= 3;
	protected static final int STATE_AFTERPLAY	= 4;
	protected static final int STATE_PREFETCHED	= 5;
	protected static final int STATE_DESTROYED	= 6;
	
	// Flag to tell if this element has ever been started
	private boolean started;
	
	protected static final int TIMER_ACTIVATE	= 0;
	protected static final int TIMER_DEACTIVATE	= 1;
	protected static final int TIMER_SIMPLEDUR	= 2;
	
	// These are hashtables, to save two values, the Time and ElementBasicTimeImpl
	// Interval Begin Changed listeners
	private Hashtable beginListeners = null;
	// Interval End Changed listeners
	private Hashtable endListeners = null;
	// New Interval Created listeners
	private Hashtable newIntervalListeners = null;

	// Flag to break cyclic dependencies
	private boolean visited = false;

	protected ElementBasicTimeImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}

	/**
	 * Initialize this element.
	 */
	public void init()  {
		// Check that init() is not called twice!
		if (state != STATE_UNINIT)  {
			Log.error("Double init() call for "+this+":"+getId()+"! Dumping stack with intentional NullPtrException...");
			try  {
				Object t = null;
				t.hashCode();
			} catch (NullPointerException e)  {
				e.printStackTrace();
			}
			//super.init(); We don't want to call TimeControl's Shadow creation init()
			// and children can remain uninitialized - there has been an error!
			return;
		}

		// Init state
		state = STATE_IDLE;
		started = false;
		activateTime = 0;
		
		beginListeners = new Hashtable();
		endListeners = new Hashtable();
		newIntervalListeners = new Hashtable();

		// clear force start time
		elementForceStartTime = 0;

		// Initialize time lists and time instance lists

		// First, just get the begin and end lists parsed (should be in init?)
		beginList = getBeginList();
		endList = getEndList();
		
		// Then, get the associated instance lists, copying offsets to instance lists
		beginInstanceList = beginList.getTimeInstanceList();
		endInstanceList = endList.getTimeInstanceList();

		// Initialize children before possible immediate starting up.
		super.init();

		// Start up this element immediately, if the presentation is playing.
		// This is required to get dynamic SMIL presentations.
		// Only start up the element, if the parent is playing.
		if (getParentNode() instanceof ElementBasicTimeImpl)  {
			int s = ((ElementBasicTimeImpl)getParentNode()).getState();
			if (smilDoc.getDocumentState() != SMILDocumentImpl.STOPPED &&
				(s == STATE_WAIT || s == STATE_PLAY || s == STATE_AFTERPLAY)) {
				Log.debug("init(): SMIL element '"+getId()+"' instant-start-up!"+this);
				startup();
			}
		}

//		Log.debug(getId()+" Time Lists Created");
	}

	/**
	 * Close down this element immediately, if the presentation is playing.
	 * This is required to get dynamic SMIL presentations.
	 */
	public void destroy() {
		if (state == STATE_WAIT || state == STATE_PLAY || state == STATE_AFTERPLAY)  {
			// If the presentation is playing, close down this element immediately
			if (smilDoc.getDocumentState() != SMILDocumentImpl.STOPPED) {
				Log.debug("destroy(): SMIL element '"+getId()+"' instant-delete!"+this);
				//closeChildren();
				closedown();
			}
			// Stop all timers
			smilDoc.getScheduler().removeTimeListeners(this);
		}

		state = STATE_DESTROYED;	

		super.destroy();
	}
	
	/** 
	 * PREFETCH - prefetching the media
	 * This prefetches the element for playing. It set's the element to
	 * 'prefetched' state.
	 */
	public void prefetch() {
		XElementBasicTime child;
	
//		Log.debug(getId()+" prefetch()");
		state = STATE_PREFETCHED;

		// Add a time point to the viewer.
		if (getId() != null && getId().length() > 0)
			getSMILDoc().getViewer().addTimePoint(getId());

		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());

		// Prefetch all timed children, unless the document is aborted.
		while (children.hasMoreElements() && 
				getSMILDoc().getDocumentState() != SMILDocumentImpl.ABORTED) {
			child = (XElementBasicTime)children.nextElement();
			child.prefetch();
		}
		
	}

	// Parent Restarted, initializing (remove event condition times, reconvert syncbases)
	// When parent time container repeats/restarts
	public void restartelementparent()  {
		// TTIME
		// remove event condition times (except the one defining the begin of the current interval)
		// reconvert syncbase times
		// Clear restart="never" flag - it can restart again
		// Get first interval
	}
	
	// Restart element, initializing (remove event condition times, reconvert syncbases)
	// When parent time container repeats/restarts
	public void restartelement()  {
		// TTIME
		// remove event condition times (except the one defining the begin of the current interval)
		// reconvert syncbase times
		// Get first interval
	}

	/**
	 * Removes all event conditions from the begin and end instance lists.
	 */
	private void removeEventConditions() {
		// Begin Instance List
		beginInstanceList.reset();
		// End Instance List
		endInstanceList.reset();

		return;
	}

	/**
	 * HACK to resolve seek times for links.
	 */
	public void resolveSeekTime(ElementBasicTimeImpl child)  {
		// Already active.
		if (isActive())
			return;

		startupTime = System.currentTimeMillis();
		state = STATE_WAIT;
		
		// Force dummy setup
		currentIntervalBegin = new TimeImpl(0);
		currentIntervalEnd = null;
		activeDuration = null;
		// this calculates the active dur with an end constraint

		// We have a begin value - get an end
		Time tempEnd = endInstanceList.getTimeGreaterThan(currentIntervalBegin);
		if (tempEnd == null)  {
			// Events leave the end open-ended. If there are other conditions
			// that have not yet generated instances, they must be unresolved.
			if (endHasEventConditions() == true) {
//				            OR if the instance list is empty
				tempEnd = new TimeImpl("unresolved");
			// if all ends are before the begin, bad interval
			} else {
//				Log.debug("NI() - no event conditions - using pure begin");
				tempEnd = calcActiveEnd( currentIntervalBegin );
			}
		} else  {
			// this calculates the active dur with an end constraint
			tempEnd = calcActiveEnd( currentIntervalBegin, tempEnd );
		}
		currentIntervalEnd = tempEnd;

		// set timer(Active Duration) for deactivate() - currentIntervalEnd is Active Duration
		if (currentIntervalEnd.getResolved() == true && currentIntervalEnd.isIndefinite() == false) {
			// Save Elapsed Time for Parent (Seq) Time Container
			activeDuration = ACSub(currentIntervalEnd, currentIntervalBegin);
			smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime((long)activeDuration.getResolvedOffset().intValue()), Scheduler.TIMER_DEACTIVATE);
		}

		// Activate
		activate();
		if (getParentNode() instanceof ElementBasicTimeImpl)
			((ElementBasicTimeImpl)getParentNode()).resolveSeekTime(this);
		return;
	}

	/** 
	 * STARTUP - getting the first interval.
	 * This initializes the element for playing. It set's the element to  
	 * 'wait' state.
	 * This method is called from the parent container, when its simple dur begins.
	 */
	public void startup() {		
//		Log.debug(getId()+" startup()");
		// Resetting element state
		state = STATE_WAIT;
		started = false;

		// Save the start up time.
		startupTime = System.currentTimeMillis();
		activateTime = 0;
		beginTime = 0;

		// This should clean the lists - and leave syncbase times (after re-evaluating)
		// Don't leave syncbase time, if its parent was restarted as well
		removeEventConditions();

		setupFirstInterval(true);		
	}

	private void setupFirstInterval(boolean newInterval)  {
		// Do if parent time container allows (is active and seq says this child is active)
		if (isStartable() == false)
			return;

		// Remove possible timers
		smilDoc.getScheduler().removeTimeListeners(this);

		// Set manual repeat to zero - this may be set when Active Duration is calculated.
		manualRepeat = 0;

		// No active duration, yet.
		accActiveDuration = 0;
		// Calculate Active Duration and the first Interval
		getFirstInterval();
		// If parent == seq, the begin time cannot be negative
		// If begin time is negative, make it zero and clip the media and simple dur.
		handleNegativeBegin();
		
		// Print out the interval for debugging
		if (currentIntervalBegin == null || currentIntervalEnd == null) 
			Log.debug("F "+getId()+" sd:"+computeSimpleDuration().getString()+" ad:"+activeDuration+" s, null interval is "+currentIntervalBegin+"-"+currentIntervalEnd);
		else
			Log.debug("F "+getId()+" sd:"+computeSimpleDuration().getString()+" ad:"+activeDuration.getString()+" s, interval is "+currentIntervalBegin.getString()+"-"+currentIntervalEnd.getString());

		// No begin time, indefinite or resolved - just exit. The element will never start.
		if (currentIntervalBegin == null)  {
			return;
		}
		if (currentIntervalBegin.getResolved() == false || currentIntervalBegin.isIndefinite() == true)  {
			return;	
		}
		
		// Notify dependent elements.
		if (newInterval == true)
			notifyNewIntervalListeners();
		else  {
			// Notify dependent elements
			notifyBeginListeners(currentIntervalBegin);
			notifyEndListeners(currentIntervalEnd);
		}

		// activate() should be called at currentIntervalBegin
		// Set the timer(begin) for activate()
		int waitTime = (int)(currentIntervalBegin.getResolvedOffset().intValue());

		// Save the start time so that time can be converted to document time
		beginTime = waitTime;
		repeatTime = 0;

		// Decrement the elapsed time from the begin time
//		Log.debug("Wait time: "+waitTime+"- pcurrenttime : "+getCurrentParentTime()+" Accurate cur.time"+getTimeInBodyTime(0));
//		waitTime = waitTime - getCurrentTime();

		// No sleep time at all?
		if (waitTime <= 0)
			activate();
		else  {	// getTimeInBodyTime already includes beginTime
				// ; -currentTime because time has already gone.
			smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime(0), Scheduler.TIMER_ACTIVATE);
		}
	}

	private void setupNextInterval(boolean newInterval)  {
		// Do if parent time container allows (is active and seq says this child is active)
		if (isStartable() == false)
			return;

		// Do if restart semantics allow this
		if (getRestart() == RESTART_NEVER)
			return;

		// Remove possible timers
		smilDoc.getScheduler().removeTimeListeners(this);

		// get next interval
		getNextInterval();
		// call activate() when begin is reached - Set the timer(begin) for activate()
		// If parent == seq, the begin time cannot be negative
		// If begin time is negative, make it zero and clip the media and simple dur.
		handleNegativeBegin();

		// Print out the interval for debugging
		if (currentIntervalBegin == null || currentIntervalEnd == null)
			Log.debug("N "+getId()+" sdur:"+computeSimpleDuration().getString()+" ms, null interval is "+currentIntervalBegin+"-"+currentIntervalEnd);
		else
			Log.debug("N "+getId()+" sdur:"+computeSimpleDuration().getString()+" ms, interval is "+currentIntervalBegin.getString()+"-"+currentIntervalEnd.getString());
		
		// No begin time, indefinite or resolved - just exit. The element will never start.
		if (currentIntervalBegin == null)  {
			return;
		}
		if (currentIntervalBegin.getResolved() == false || currentIntervalBegin.isIndefinite() == true)  {
			return;	
		}

		// Notify dependent elements.
		if (newInterval == true)
			notifyNewIntervalListeners();
		else  {
			// Notify dependent elements
			notifyBeginListeners(currentIntervalBegin);
			notifyEndListeners(currentIntervalEnd);
		}

		// activate() should be called at currentIntervalBegin
		// Set the timer(begin) for activate()
		int waitTime = (int)(currentIntervalBegin.getResolvedOffset().intValue());

		// Save the start time so that time can be converted to document time
		beginTime = waitTime;
		repeatTime = 0;

		// No sleep time at all?
		if (waitTime <= 0)
			activate();
		else	// getTimeInBodyTime already includes beginTime
			smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime((long)0), Scheduler.TIMER_ACTIVATE);
	}

	/**
	 * If currentIntervalBegin time is negative, the begin time will be zero,
	 * and the Simple Duration and Active Duration are clipped.
	 */
	private void handleNegativeBegin() {
		clipBeginTime = new TimeImpl(0);

		if (currentIntervalBegin == null || currentIntervalBegin.isNegative() == false)
			return;
		
		// MediaElement will use this to skip the media to this time point.
		clipBeginTime = new TimeImpl(-(currentIntervalBegin.getResolvedOffset().intValue()));
		setCurrentIntervalBegin(new TimeImpl(0));
		// Also, the Simple and Active Duration are recalculated. For now, only active dur is done.
	}

	/**
	 * Checks if this is startable - i.e. if the element's parent is active
	 * and if the parent is seq, that this element is the active child. This
	 * is used to prevent syncbased elements responding if not active.
	 * This will be overridden in body time container.
	 */	
	public boolean isStartable()  {

		// If forced to start - then always startable..
		if (elementForceStartTime != 0)
			return true;

		Node n = getParentNode();
		if (n instanceof ElementBasicTimeImpl)
			return ((ElementBasicTimeImpl)n).isChildStartable(this);
		else
			return true; // Always if under non-SMIL element.
	}
	
	/**
	 * Checks if this child is startable - i.e. if the child's parent is active
	 * and if the parent is seq, that this child is the active child. This
	 * is used to prevent syncbased elements responding if not active.
	 * This will be overridden in seq time container.
	 */	
	public boolean isChildStartable(ElementBasicTimeImpl child)  {
		return isActive();
	}

	// To be overridden in time containers
	public void startChildren() {
		// Get Timed Elements
		XElementBasicTime activeChild;
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());

		// Start timed children
		while (children.hasMoreElements()) {
			activeChild = (XElementBasicTime)children.nextElement();
			activeChild.startup();
		}
		return;
	}
	
	public void closeChildren()  {
		// close children
		XElementBasicTime activeChild;
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		// If no children, then skip
		if (children.hasMoreElements() == true) {
			// Get timed children
			while (children.hasMoreElements()) {
				activeChild = (XElementBasicTime)children.nextElement();
				activeChild.closedown();
			}
		}
	}

	/**
	 * ACTIVE DURATION START - playing an interval.
	 * This is called to activate the element - after begin time has been reached.
	 * This sets the element to 'play' state.
	 * TODO: If restart==RESTART_ALWAYS - create a next BeginTimer to restart the element.
	 * Question: Can it be taken into account in the ActiveDuration calculation?
	 */
	public void activate() {
//		Log.debug(getId()+" activate("+getTimeInBodyTime(0)+")");//+getTimeInBodyTime(new TimeImpl(0)).getString());
		state = STATE_PLAY;
		started = true;

		// This begin time cannot be changed anymore. This locking business won't
		// work, because new TiemImpl might have been created since the original
		// TimeImpl added the change listener.
		((TimeImpl)currentIntervalBegin).lock();

		// Save the activation time.
		activateTime = System.currentTimeMillis();

		// display media in MediaElement
		display();
		
		// Start Time Container children
		startChildren();
		
		// Reset repeat iteration #
		repeatIteration = 0;
		
		// dispatch DOM event
		dispatch("beginEvent", false);
	
		// set timer(Active Duration) for deactivate() if defined
		if (activeDuration != null &&
			activeDuration.getResolved() == true && activeDuration.isIndefinite() == false) {

			// Straight deactivate() if zero time - no deactivate or simpleDur timers
			if (activeDuration.getResolvedOffset().intValue() <= 0) {
				Log.debug("  "+getId()+" active time zero, deactivate() "+activeDuration.getResolvedOffset());
				deactivate();
				return;
			}

			smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime((long)activeDuration.getResolvedOffset().intValue()), Scheduler.TIMER_DEACTIVATE);
		}

		// set timer(SimpleDuration) for simpleDurEnded()
		Time sdur = computeSimpleDuration();
		simpleTime = 0;
		if (sdur.getResolved() == true && sdur.isIndefinite() == false) {
			simpleTime = (int)(sdur.getResolvedOffset().intValue());
			// Create the timer, only if Simple Duration < Active Duration
			if (activeDuration.isIndefinite() == true || activeDuration.getResolved() == false ||
					simpleTime < activeDuration.getResolvedOffset().intValue()) {
				smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime((long)simpleTime), Scheduler.TIMER_SIMPLEDUR);
			}
		}
	}
	
	/**
	 * Simple display for inherited elements.
	 */
	public void display()  {
		return;
	}
	
	/**
	 * This is called when simple duration for this element is ended.
	 * This repeats the media.
	 */
	public void simpleDurEnded() {
//		Log.debug(getId()+" simpleDurEnded("+simpleTime+")");

		// Scheduler removed simpleDurTimer

		// Repeat Simple Duration Timer (repeatTime hasn't been updated, yet!)
		smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime((long)simpleTime+simpleTime), Scheduler.TIMER_SIMPLEDUR);

		repeat(simpleTime);
	}

	/**
	 * This is called from the child to tell that it has ended.
	 * childEnded and activate may have to be synchronized - to allow
	 * starting all children before they are closed down. (in case of ad = 0)
	 * @param childDuration		Duration of the child element
	 */
	public void childEnded(long childDuration) {
		Log.debug(getId()+" childEnded(), dur: "+childDuration);

		// Check if this element is affected by its children (AD == 0)
		// If Active Duration was resolved, then the timer will end this element
		// or if the Active Duration is "indefinite", the container will remain for ever
		if (activeDuration != null && (activeDuration.getResolved() == true || activeDuration.isIndefinite() == true))
			// Wait until AD timer
			return;

		// check if time container has ended
		if (timeContainerHasEnded() == true) {
			repeatRemoveFreeze(childDuration);
		}

		return;
	}

	// This will always end as endsync="last"
	public boolean timeContainerHasEnded()  {
		ElementBasicTimeImpl c;
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		// If no children, then just return true
		if (children.hasMoreElements() == false)
			return true;

		boolean assumedResult = true;

		// loop on each child in collection of timed children,
		//  and consider it in terms of the endsyncRule

		// foreach ( child c in timed-children-collection )
		while (children.hasMoreElements()) {
		   c = (ElementBasicTimeImpl)children.nextElement();
	         // we just test for disqualifying children
	         // If the child is active, we're definitely not done.
	         // If the child has not yet begun but has a resolved begin,
	         // then we're not done.
	         if( c.isActive() || c.isResolved() == true )
	             //|| c.begin.isResolved(now) )
	             return false;
	         // else, keep checking (the assumed result is true)
		} // close foreach loop

		return assumedResult;
	} // close timeContainerHasEnded()

	/**
	 * Callback method - called when the media has ended.
	 * This may cause the media to end its simple duration, if dur = "media" or dur = unspecified.
	 * The reason is that Simple Duration is first unresolved, and now it is resolved.
	 */
	public void mediaEnded() {
//		Log.debug(getId()+" mediaEnded()");
		accActiveDuration = System.currentTimeMillis() - activateTime;

		repeatRemoveFreeze(accActiveDuration);
		return;
	}

	/**
	 * @param timeDur	childDur / mediaDur
	 * @return	REPEAT/REMOVE/FREEZE
	 */
	public void repeatRemoveFreeze(long timeDur)  {

//		Log.debug(getId()+" RepeatRemoveFreeze("+timeDur+").");
		// ended -> repeat or close children, if still active

		// This is required, if deactivate(): child.deactivate()->this.childEnded() is called.
		if (isActive() == false)  {
//			Log.debug("   RRF Cancelled");
			return;
		}

		// Manual repeatCount - see calculation of IntermediateActiveDuration()
		if (manualRepeat > 0) {
			// Loop forever
			if (getRepeatCount().equals("indefinite")) {
				repeat(timeDur);
				return;
			}
			// Decrement repeat counts, and restart Simple Duration (repeat media).
			// TODO: This can currently only count integer values, not fractions of counts.
			manualRepeat--;
			if (manualRepeat > 0) {
				repeat(timeDur);
				return;
			}

			manualRepeat = 0;
		}
		
		// repeatDur repeating - Active Duration (repeatDur) will end this element
		if (getRepeatDur() != null) {
			repeat(timeDur);
			return;
		}
		
		// If Active Duration was resolved, then the timer will end the Active Duration
		// or if the Active Duration is "indefinite", the container will remain for ever
		if (activeDuration != null && (activeDuration.getResolved() == true || activeDuration.isIndefinite() == true)) {
			// Freeze or remove the children to the end of Active Duration
			removeFreeze();
			return;
		} 
		// No repeat, no Active Duration
		Log.debug("TODO KOHTA!");
		// Active Duration is the duration of this last child
		accActiveDuration = timeDur;
		deactivate();
		//TODO
		// Active Duration not resolved - the end of the last child will end it
		// Close all children & deactivate
//			closeChildren();
		// Elapsed time is the duration of this last child
//			accActiveDuration = timeDur;
//			deactivate();
//		}

	 	return;
	 }

	/**
	 * @return	REMOVE/FREEZE
	 */	
	public boolean removeFreeze()  {
		// Freeze or remove element
		// This is semantics of the fill attribute
		Time dur = getDur();
		String end = getEnd();
		Time repeatDur = getRepeatDur();
		String repeatCount = getRepeatCount();
		String fill = getFill();
		boolean freeze = true;
		
		// Set the default value for fill - 'auto' (because fillDefault is not part of Basic profile)	
		if (fill == null || fill.length() == 0 || fill.equals("default"))
			fill = "auto";
		
		// If dur, repeatDur, repeatCount and end are unspecified, then freeze
		if (fill.equals("auto"))
			if (dur == null && repeatDur == null && repeatCount == null && end == null)
				freeze = true;
			else
				freeze = false;
		else if (fill.equals("freeze"))
			// Freeze (seq should show this media until next media is visible)
			freeze = true;
		else if (fill.equals("hold"))
			// Freeze until the end of parent Simple Duration (par effect in seq)
			freeze = true;
		else if (fill.equals("transition"))
			// Transition will only hold - not until end of transition in Basic profile
			freeze = true;
		else if (fill.equals("remove"))
			freeze = false;

		if (freeze == true) {
			Log.debug(getId()+" - ELEMENT FROZEN");
			freeze();
		} else {
			Log.debug(getId()+" - ELEMENT REMOVED");
			remove();
		}
		return freeze;
	}

	/**
	 * Repeat the element - if simpleDur ended or childEnded or mediaEnded.
	 */
	public void repeat(long time)  {
//		Log.debug(getId()+" repeat("+time+")");

		// Repeat the media in MediaElement

		// Add repeat iteration # - used in animation
		repeatIteration++;

		// Add time to accumulated durations
		repeatTime += time;
		accActiveDuration += time;

		// restart children
		closeChildren();
		startChildren();

		// dispatch DOM event
		dispatch("repeatEvent", false);
	}

	/**
	 * Remove the element - can be called if the parent ends its Simple Duration
	 * Pass through the call to all children
	 */
	public void remove() {
//		Log.debug(getId()+" remove()");

		closeChildren();
	}

	/**
	 * Freeze the element - can be called if the parent ends its Simple Duration
	 * Pass through the call to all children
	 */
	public void freeze() {
//		Log.debug(getId()+" freeze()");

		// Freeze all children
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		while (children.hasMoreElements()) {
			((XElementBasicTime)children.nextElement()).deactivate();
		}			
	}

	/** 
	 * ACTIVE DURATION END / END OF INTERVAL / POST ACTIVE
	 * This is called to deactivate the element - after the active duration end has been reached.
	 * This will search for the next interval and call activate() and set the element to 'fill' state.
	 * @return		true if element is frozen, false if removed
	 */
	public void deactivate() {
		Log.debug(getId()+" deactivate("+beginTime+"+"+accActiveDuration+")");

		// clear timer for simpleDurEnded() (all timers)
		smilDoc.getScheduler().removeTimeListeners(this);

		// This end time cannot be changed anymore. This locking business won't
		// work, because new TiemImpl might have been created since the original
		// TimeImpl added the change listener.
		//((TimeImpl)currentIntervalEnd).lock();

		state = STATE_AFTERPLAY;
		// This will either remove or freeze the element.
		removeFreeze();

		// TTIME : restart = "never" - just remain here (can be restarted if parent restarts)
//		if (restart = never)
//			setCurrentIntervalBegin(null);
//			setCurrentIntervalEnd(null);
//			return;

		// dispatch DOM event
		dispatch("endEvent", false);

		// Save last played interval for getNextInterval
		if (currentIntervalEnd != null)  {
			previousIntervalBegin = currentIntervalBegin;
			previousIntervalEnd = currentIntervalEnd;
			currentIntervalBegin = null;
			currentIntervalEnd = null;
			//Log.debug("LATEST INTERVAL: "+previousIntervalBegin.getString()+"-"+previousIntervalEnd.getString());
		}

		// notify parent that this media has stopped, but might have a new interval.
		if (getParentNode() instanceof ElementBasicTimeImpl)  {
			// Send either exact AD or accumulated approximated AD to parent
			if (activeDuration != null && activeDuration.getResolved() == true && activeDuration.isIndefinite() == false) 
				((ElementBasicTimeImpl)getParentNode()).childEnded(beginTime + activeDuration.getResolvedOffset().intValue());
			else
				((ElementBasicTimeImpl)getParentNode()).childEnded(beginTime + accActiveDuration);
		}

		setupNextInterval(true);

		// t�� vanha interval ei en�� muutu -> can break dependency relationships.
		return;
	}

	/**
	 * This closes this element. The element will not be active or show fill behaviour.
	 * It will be in 'idle' state.
	 * This method is called from the parent container, when its simple dur ends.
	 */
	public void closedown() {
//		Log.debug(getId()+" closedown()");
		// clear timer for activate(), simpleDurEnded() and deactivate()
		smilDoc.getScheduler().removeTimeListeners(this);

		// remove the media/link - and closes all children
		remove();
		
		state = STATE_IDLE;
	}

	/**
	 * Set the element activation time.
	 * @param t		System time
	 */
	public void setForceStartTime(long t) {
		// Convert to body time
		t = t - smilDoc.getScheduler().getStartTime();
		// In case t and scheduler start time are the same, mark that this
		// element uses forced start time.
		if (t==0)
			t=1;

		elementForceStartTime = t;
	}

	/**
	 * Returns time in body time space,
	 * @param t		Time to convert, in millisecs
	 * @return 		time in body time, in millisecs
	 */
	public long getTimeInBodyTime(long t) {
		if (elementForceStartTime != 0)
			return elementForceStartTime + t;
		else  {
			if (getParentNode() instanceof XElementBasicTime)
				return ((XElementBasicTime)getParentNode()).getTimeInBodyTime(beginTime+repeatTime) + t;
			else
				return beginTime+repeatTime+t;
		}
	}

	/**
	 * @return time in parent time
	 */
	public Time getTimeInParentTime(Time t) {
		if (t.getResolved() == false) {
			return t;
	 	}
		if (t.isIndefinite() == true) {
			return t;
		}
		return ACAdd(  ((XElementBasicTime)getParentNode()).getCurrentIntervalBegin(), t );
		
	}

	/**
	 *  A NodeList that contains all timed childrens of this node. If there are
	 *  no timed children, the <code>Nodelist</code> is empty.  An iterator 
	 * is more appropriate here than a node list but it requires Traversal 
	 * module support. 
	 * Required method by SMIL-DOM. However, returns false answer.
	 */
	public NodeList getTimeChildren() {
		// These should be timed nodes only - not switch etc. 
		return getChildNodes();
	}

	/**
	 * Get the current interval begin time.
	 * @return	Current interval begin time, in future or past, or unresolved or null
	 */
	public Time getCurrentIntervalBegin() {
	 	return currentIntervalBegin;
	 }

	/**
	 * Set the current interval begin time. If the begin time changes, the dependent
	 * elements will be notified of IntervalBeginChange.
	 *
	 * @param b	New current interval begin time, in future or past, or unresolved or null
	 */
	public void setCurrentIntervalBegin(Time b) {
		if (currentIntervalBegin == null && b == null)
			return;
	
		if (currentIntervalBegin != null && currentIntervalBegin.isEqualTo(b))
			return;
			
		currentIntervalBegin = b;
	}

	/**
	 * Get the current interval end time.
	 * @return	Current interval end time, in future or past, or unresolved or null
	 */
	public Time getCurrentIntervalEnd() {
		return currentIntervalEnd;
	}

	/**
	 * Set the current interval end time. If the end time changes, the dependent
	 * elements will be notified of IntervalEndChange.
	 *
	 * @param b	New current interval end time, in future or past, or unresolved or null
	 */
	public void setCurrentIntervalEnd(Time e) {
		if (currentIntervalEnd == null && e == null)
			return;

		if (currentIntervalEnd != null && currentIntervalEnd.isEqualTo(e))
			return;
			
		currentIntervalEnd = e;
		// Notify dependent elements
	//	notifyEndListeners(e);
	}

	public int getState()  {
		return state;
	}

	/**
	 * Checks if this element is playing.
	 * @return	true if this element is active.
	 */
	public boolean isActive() {
		return state == STATE_PLAY;
	}
	
	/**
	 * Checks if this element has ever started. Note, restarting will cause the element to be
	 * not started. (?)
	 * @return	true if this element has ever started.
	 */
	public boolean hasStarted() {
		return started;
	}

	/**
	 * Checks whether this element still has begin values in the future.
	 * @return	true if more begin values in the future.
	 */
	public boolean isResolved()  {
		// Hasn't finished the first interval, yet.
		if (previousIntervalEnd == null)  {
			if (beginInstanceList.getTimeGreaterThan(new TimeImpl(0)) != null)
				return true;
			else
				return false;
		}
		if (beginInstanceList.getTimeGreaterThan(previousIntervalEnd) != null)
			return true;

		return false;
	}

	/**
	 * Tests if the end attribute has event conditions.
	 * @return 		true if end has EVENT, ACCESSKEY or TIME_REPEAT
	 */
	private boolean endHasEventConditions() {
		TimeList tl = endList;
		short t;

		for (int i = 0 ; i < tl.getLength() ; i++) {
			t = tl.item(i).getTimeType();
			if (t == Time.SMIL_TIME_EVENT_BASED || t == Time.SMIL_TIME_REPEAT || 
				t == Time.SMIL_TIME_ACCESSKEY)
				return true;	
		}
		return false;	
	}

	/**
	 * Get a begin time list
	 */
	public TimeListImpl getBeginList() {
		String begin = getBegin();
		if (begin == null || begin.length() == 0)
			return new TimeListImpl(defaultBegin(), getSMILDoc(), this, true);
			
		return new TimeListImpl(begin, getSMILDoc(), this, true);
	}

	/**
	 * Get an end time list
	 */
	public TimeListImpl getEndList() {
		return new TimeListImpl(getEnd(), getSMILDoc(), this, false);
	}
	
	/** 
	 * Add a time to begin/end instance list
	 */
	public void addTime(boolean begin, TimeImpl time)  {
		if (begin == true)  {
			beginInstanceList.add(time);
			reevaluateIntervalBegin(time);
		} else  {
			endInstanceList.add(time);
			reevaluateIntervalEnd(time);
		}
	}
	
	/**
	 * Get first or next interval.
	 */
	private void getInterval()  {
	}
	
	/**
	 * Get the first interval for this element. Sets the currentIntervalBegin and currentIntervalEnd.
	 * Notifies the dependent elements of NewInterval.
	 */
	private void getFirstInterval() {
		Time tempBegin = null, tempEnd;
		Time tempPreventLoop = null;

		// THIS SHOULD BE -INFINITY
		Time beginAfter = new TimeImpl(-900000);

		while (true) { // loop till return
			if (tempBegin == null)
				tempPreventLoop = null;
			else		
				tempPreventLoop = new TimeImpl(tempBegin.getString()); 
				
			tempBegin = beginInstanceList.getTimeGreaterThan(beginAfter);
			// If while loop is going for ever... exit.
			if (tempPreventLoop == null && tempBegin == null) {
				setCurrentIntervalBegin(null);
				setCurrentIntervalEnd(null);
				return; // FAILURE
			}

			if (tempBegin == null) { // no interval
				setCurrentIntervalBegin(null);
				setCurrentIntervalEnd(null);
				return; // FAILURE
			}
			// If looping forever... exit.
			if (tempPreventLoop != null)
				if (tempPreventLoop.getString().equals(tempBegin.getString())) {
					setCurrentIntervalBegin(null);
					setCurrentIntervalEnd(null);
					return; // FAILURE
				}
			
			
			if (endInstanceList.isDefined() == false) {
				// this calculates the active end with no end constraint
      			tempEnd = calcActiveEnd( tempBegin );
			} else
			{
				// We have a begin value - get an end
				tempEnd = endInstanceList.getTimeGreaterThan(tempBegin);
				// Allow for non-0-duration interval that begins immediately
				// after a 0-duration interval.
//				if (tempEnd.equals(tempBegin) && tempEnd has already been used in 
//					        an interval calculated in this method call
//				{
//					set tempEnd to the next value in the end list that is > tempEnd
//				}         
				if (tempEnd == null)
				{
					// Events leave the end open-ended. If there are other conditions
					// that have not yet generated instances, they must be unresolved.
					if (endHasEventConditions() == true) {
//				            OR if the instance list is empty
						tempEnd = new TimeImpl("unresolved");
					// if all ends are before the begin, bad interval
					} else {
						setCurrentIntervalBegin(null);
						setCurrentIntervalEnd(null);
						return; // FAILURE
					}
				}
				// this calculates the active dur with an end constraint
				tempEnd = calcActiveEnd( tempBegin, tempEnd );
			}

			// We have an end - is it after the parent simple begin?
			if( tempEnd.isGreaterThan(new TimeImpl(0)) == true) {
				// Create the interval
				setCurrentIntervalBegin(tempBegin);
				setCurrentIntervalEnd(tempEnd);
				return; // SUCCESS ( Interval( tempBegin, tempEnd ) );
			}

			// interval is too early 
			else if( getRestart() == RESTART_NEVER ) {
				// if can't restart, no good interval
				setCurrentIntervalBegin(null);
				setCurrentIntervalEnd(null);
				return; // FAILURE
			} else {
				// Change beginAfter to find next interval, and loop
				beginAfter = tempEnd;
			}
		} // close while loop

	} // close getFirstInterval

	/**
	 * Get the next interval for this element. Sets the currentIntervalBegin and currentIntervalEnd.
	 * Notifies the dependent elements of NewInterval.
	 */
	private void getNextInterval() {
		Time tempBegin = null, tempEnd;
		Time tempPreventLoop = null;

		Time beginAfter = previousIntervalEnd;
		if (beginAfter == null)  {
//			Log.debug("getNextInteval() PREVIOUS END NULL");
			return;
		}

		tempBegin = beginInstanceList.getTimeGreaterThan(beginAfter);

		if (tempBegin == null) { // no interval
//			Log.debug("getNextInteval() TEMP BEGIN NULL "+beginAfter.getString());
			setCurrentIntervalBegin(null);
			setCurrentIntervalEnd(null);
			return; // FAILURE
		}
		// If TempBegin > parentSimpleDur (not implemented- affects dependencies)
		
		if (endInstanceList.isDefined() == false) {
			// this calculates the active end with no end constraint
  			tempEnd = calcActiveEnd( tempBegin );
		} else
		{
			// We have a begin value - get an end
			tempEnd = endInstanceList.getTimeGreaterThan(tempBegin);
			// Allow for non-0-duration interval that begins immediately
			// after a 0-duration interval.
//				if (tempEnd.equals(tempBegin) && tempEnd has already been used in 
//					        an interval calculated in this method call
//				{
//					set tempEnd to the next value in the end list that is > tempEnd
//				}         
			if (tempEnd != null && tempEnd.isEqualTo(currentIntervalEnd))  {
				tempEnd = endInstanceList.getTimeGreaterThan(tempEnd);
			}
			
			if (tempEnd == null)  {
				// Events leave the end open-ended. If there are other conditions
				// that have not yet generated instances, they must be unresolved.
				if (endHasEventConditions() == true) {
//				            OR if the instance list is empty
					tempEnd = new TimeImpl("unresolved");
				// if all ends are before the begin, bad interval
				} else {
//					Log.debug("NI() - no event conditions - using pure begin");
					tempEnd = calcActiveEnd( tempBegin );
//					setCurrentIntervalBegin(null);
//					setCurrentIntervalEnd(null);
//					return; // FAILURE ?? 
					// This will go to calcActiveEnd again?? It seems to work...
				}
			}
			// this calculates the active dur with an end constraint
			tempEnd = calcActiveEnd( tempBegin, tempEnd );
		}
		// Create the interval
		setCurrentIntervalBegin(tempBegin);
		setCurrentIntervalEnd(tempEnd);
		return; // SUCCESS ( Interval( tempBegin, tempEnd ) );
	} // close getNextInterval

	/**
	 * Re-evaluates the current interval begin time. The restart attribute will control this.
	 * In Basic profile, the element will restart ="always". Otherwise, the default
	 * value is restartDefault (which default value is "always").
	 * <p>
	 * If current interval is waiting to play, the element recalculates the begin and end times.
	 * If current interval is playing and a new begin instance time was added, then
	 *   1. if restart="never" then nothing will be done. 
	 *   2. if restart="whenNotActive" then nothing will be done.
	 *   3. if restart="always" and the new instance time is between start and end times,
	 *       then the element will restart and set a new interval
	 */
	public void reevaluateIntervalBegin(TimeImpl newBegin) {
	
		// The element hasn't started yet.
		if (state == STATE_WAIT) {
			// Re-evaluate the first interval - create a new or change an interval.
			if (currentIntervalBegin == null)
				setupFirstInterval(true);
			else	
				setupFirstInterval(false);

		} else if (state == STATE_AFTERPLAY) {
			// Re-evaluate the next interval - create a new or change an interval.
			if (currentIntervalBegin == null)
				setupNextInterval(true);
			else	
				setupNextInterval(false);
		} else if (state == STATE_PLAY)  {
			// If the element is playing (active), begin cannot be changed,
			// unless restart==always.
			if (getRestart() != RESTART_ALWAYS)
				return;
			
			// End current interval and get the next interval
			if (newBegin.getResolvedOffset().intValue() <= getCurrentParentTime()) {
				immediateIntervalEnd(newBegin);
			} else {
				// ReCheck BeginTimer
			}
		}
	}

	/** 
	 * Ends current interval immediately notifying dependent elements.
	 * @param endtime		This is the end time - sent by event or media end. 
	 *						Current Interval End will be endtime-10ms, to let
	 *						possibly added event time to start.
	 */
	protected void immediateIntervalEnd(TimeImpl endtime)  {
		if (endtime == null)
			currentIntervalEnd = new TimeImpl(getCurrentParentTime());
		else
			currentIntervalEnd = new TimeImpl(endtime.getResolvedOffset().intValue()-10);

		// SYNCBASE: Notify about the change of the end of interval
		notifyEndListeners(currentIntervalEnd);
		// Save Elapsed Time for Parent (Seq) Time Container
		accActiveDuration = (int)(System.currentTimeMillis()-activateTime);
//		Log.debug("accActiveDuration: "+accActiveDuration+" ce"+currentIntervalEnd.getString());
		// End Active Duration - get next interval
		deactivate();
	}

	/**
	 * Re-evaluates the current interval end time. 
	 * <br/>
	 * In Event Based timing, this method will be called only if the element is active.
	 * @param newEnd		Proposed new currentIntervalEnd
	 */
	public void reevaluateIntervalEnd(TimeImpl newEnd) {

		// If the element hasn't started yet, then re-evaluate the whole interval.
		// EventBased Timing will never execute this, but syncbased might.
		if (state == STATE_WAIT) {
			// Re-evaluate the (existing) interval - will be the first interval, 
			// because the element hasn't started yet.
			// Re-evaluate the first interval - create a new or change an interval.
			if (currentIntervalBegin == null)
				setupFirstInterval(true);
			else	
				setupFirstInterval(false);
			}
		// If the element is playing, then only recalculate the end time.
		else if (state == STATE_PLAY) {
			// If the new time is between interval begin and end -> its effective
//			Log.debug("PLAY curr:"+currentIntervalBegin.getString()+" t"+newEnd.getString()+" cpt"+getCurrentParentTime());
			if (newEnd.isGreaterThan(currentIntervalBegin) && 
				currentIntervalEnd.isGreaterThan(newEnd)) {
				
				// this calculates the active dur with an end constraint
				Time tempEnd = calcActiveEnd( currentIntervalBegin, newEnd );
				
				currentIntervalEnd = tempEnd;

				// End has changed.				
				notifyEndListeners(currentIntervalEnd);
				// Less than now -> end immediately!
				if (currentIntervalEnd.getResolvedOffset().intValue() <= getCurrentParentTime()) {
					// Save Elapsed Time for Parent (Seq) Time Container
					accActiveDuration = (int)(System.currentTimeMillis()-activateTime);
//					Log.debug("accActiveDuration: "+accActiveDuration);
					deactivate();
					return;
				} 
				// Set the new end time
				smilDoc.getScheduler().removeTimeListener(this, Scheduler.TIMER_DEACTIVATE);
				
				// set timer(Active Duration) for deactivate() - currentIntervalEnd is Active Duration
				if (currentIntervalEnd.getResolved() == true && currentIntervalEnd.isIndefinite() == false) {
					//int activeTime = (int)(currentIntervalEnd.getResolvedOffset().intValue() - 
					//					getCurrentTime());	

					// Save Elapsed Time for Parent (Seq) Time Container
					activeDuration = ACSub(currentIntervalEnd, currentIntervalBegin);

					smilDoc.getScheduler().addTimeListener(this, getTimeInBodyTime((long)activeDuration.getResolvedOffset().intValue()), Scheduler.TIMER_DEACTIVATE);
				}
			}
		}
		// If the element has started, but rewaiting, then re-evaluate the whole interval.
		// EventBased Timing will never execute this, but syncbased might.
		else if (state == STATE_AFTERPLAY) {
			// Re-evaluate the first interval - create a new or change an interval.
			if (currentIntervalBegin == null)
				setupNextInterval(true);
			else	
				setupNextInterval(false);
			}
		return;
	}

	/**
	 * Add a new begin interval changed listener.
	 * @param time		Time object, which has the syncbase clock value
	 * @param dependent	The dependent element, which will be notified of changes
	 */
	public void addIntervalBeginListener(Time time, ElementBasicTimeImpl dependent) {
		beginListeners.put(time, dependent);
	}

	/**
	 * Notify all begin interval changed listeners.
	 * @param newBeginTime		The new current interval begin time in this base element.
	 */
	public void notifyBeginListeners(Time newBeginTime) {
		TimeImpl t;
		ElementBasicTimeImpl elem;
	
		for (Enumeration e = beginListeners.keys() ; e.hasMoreElements() ;) {
			t = (TimeImpl)(e.nextElement());
			elem = (ElementBasicTimeImpl)(beginListeners.get(t));

			Log.debug("sync "+getId()+" begin notifies element "+elem.getId());
			t.intervalBeginChanged((TimeImpl)newBeginTime);
		}
	}	

	/**
	 * Add a new end interval changed listener.
	 * @param time		Time object, which has the syncbase clock value
	 * @param dependent	The dependent element, which will be notified of changes
	 */
	public void addIntervalEndListener(Time time, ElementBasicTimeImpl dependent) {
		endListeners.put(time, dependent);
	}

	/**
	 * Notify all end interval changed listeners.
	 * @param newEndTime		The new current interval end time in this base element.
	 */
	public void notifyEndListeners(Time newEndTime) {
		TimeImpl t;
		ElementBasicTimeImpl elem;

		for (Enumeration e = endListeners.keys() ; e.hasMoreElements() ;) {
			t = (TimeImpl)(e.nextElement());
			elem = (ElementBasicTimeImpl)(endListeners.get(t));

			Log.debug("sync "+getId()+" end notifies about time "+newEndTime.getString());
			t.intervalEndChanged((TimeImpl)newEndTime);
		}
	}	

	/**
	 * Add a new interval created listener.
	 * @param time		Time object, which has the syncbase clock value
	 * @param dependent	The dependent element, which will be notified of changes
	 */
	public void addNewIntervalListener(Time time, ElementBasicTimeImpl dependent) {
		newIntervalListeners.put(time, dependent);
	}

	/**
	 * Notify all new interval listeners about the new interval.
	 */
	public void notifyNewIntervalListeners() {
		TimeImpl t;
		ElementBasicTimeImpl elem;
	
		for (Enumeration e = newIntervalListeners.keys() ; e.hasMoreElements() ;) {
			t = (TimeImpl)(e.nextElement());
			elem = (ElementBasicTimeImpl)(newIntervalListeners.get(t));
			Log.debug("sync "+getId()+" notifies -new interval- element "+elem.getId());
//			elem.syncbaseNewIntervalCreated(t, currentIntervalBegin, currentIntervalEnd);
			t.intervalCreated((TimeImpl)currentIntervalBegin, (TimeImpl)currentIntervalEnd);
		}
	}	

	/**
	 * Returns the current time in the parent time space. Valid only if the parent
	 * is active. (activate() has been called). In Seq Time Container, this
	 * will return the time since the startup() of this element.
	 * @return		time from parent activate() in millisecs, except under seq time container.
	 */
	public int getCurrentParentTime() {
		Node n = getParentNode();
		if ((n instanceof ElementBasicTimeImpl) == false)
			return 0;

		ElementBasicTimeImpl parent = (ElementBasicTimeImpl)n;
		if (parent.isActive() == false)
			return 0;
		
		return (int)(System.currentTimeMillis()-startupTime);
	}

	/**
	 * Calculates the active end time, relative to the parent simple time.
	 * @param begin		Begin time
	 * @param end		End time
	 * @return			Active end time (not duration)
	 */
	private Time calcActiveEnd(Time begin, Time end) {
		return ACAdd(begin, computeActiveDuration(begin, end));
	}

	/**
	 * Calculates the active end time, relative to the parent simple time.
	 * This doesn't require the end parameter - it uses the dur parameter.
	 * @param begin		Begin time
	 * @return			Active end time (not duration)
	 */
	private Time calcActiveEnd(Time begin) {
		return calcActiveEnd(begin, null);
	}

	/**
	 * Multiplication for active duration computation.
	 *	zero value * value 				= zero value 
	 *  zero value * indefinite 		= zero value 
	 *  non-zero value * non-zero value = non-zero value 
	 *  non-zero value * indefinite 	= indefinite 
	 *  indefinite * indefinite 		= indefinite 
	 *  unresolved * anything 			= unresolved 
	 */
	private Time ACMult(Time m1, Time m2) {
		// Safety check
		if (m1 == null || m2 == null)
			return new TimeImpl("unresolved");
		
		if (m1.getResolved() == true) {
			//	zero value * value 				= zero value 
			//  zero value * indefinite 		= zero value 
			if (m1.getResolvedOffset().equals(zero)) {
				return m1;
			} else {
				//  non-zero value * non-zero value = non-zero value 
				if (m2.getResolved() == true && m2.getResolvedOffset().equals(zero) != true)
					return new TimeImpl(m1.getResolvedOffset().intValue() * m2.getResolvedOffset().intValue());
				//  non-zero value * indefinite 	= indefinite 
				if (m2.isIndefinite() == true)
					return m2;
			}
			//  indefinite * indefinite 		= indefinite 
			if (m1.isIndefinite() == true && m2.isIndefinite() == true)
				return m1;
		}
		
		if (m1.getResolved() == false)
			return new TimeImpl("unresolved");
		
		// If none matched, then switch the variables
		// This is dangerous - what if we loop forever?
		return ACMult(m2, m1);
	}

	/**
	 * Substraction for active duration computation.
	 *  value +/- value = value 
	 *  indefinite +/- value = indefinite 
	 *  indefinite +/- indefinite = indefinite 
	 *  unresolved +/- anything = unresolved
	 */
	private Time ACSub(Time m1, Time m2) {
		// Safety check
		if (m1 == null || m2 == null)
			return new TimeImpl("unresolved");
		
		return ACAdd(m1, new TimeImpl(-m2.getResolvedOffset().intValue()));
	}

	/**
	 * Addition/Substraction for active duration computation.
	 *  value +/- value = value 
	 *  indefinite +/- value = indefinite 
	 *  indefinite +/- indefinite = indefinite 
	 *  unresolved +/- anything = unresolved
	 */
	public Time ACAdd(Time m1, Time m2) {
		// Safety check
		if (m1 == null || m2 == null || m1.getResolved() == false || m2.getResolved() == false)
			return new TimeImpl("unresolved");

		//  value +/- value = value 
		if (m1.isIndefinite() == false && m2.isIndefinite() == false) 
			return new TimeImpl(m1.getResolvedOffset().intValue() + m2.getResolvedOffset().intValue());
			
		//  indefinite +/- value = indefinite 
		//  indefinite +/- indefinite = indefinite 
		if (m1.isIndefinite() == true || m2.isIndefinite() == true) {
			return new TimeImpl("indefinite");
		}	
		
		//  unresolved +/- anything = unresolved
		return new TimeImpl("unresolved");		
	}

	/**
	 * Minimization for active duration computation.
	 *  MIN( zero value, anything) = zero value 
	 *  MIN( non-zero value, non-zero value) = non-zero value 
	 *  MIN( non-zero value, indefinite) = non-zero value 
	 *  MIN( non-zero value, unresolved) = non-zero value 
	 *  MIN( indefinite, unresolved) = indefinite 
	 */
	private Time ACMin(Time m1, Time m2) {
		// Safety check
		if (m1 == null || m2 == null)
			return new TimeImpl("unresolved");
		if (m1.getResolved() == false && m2.getResolved() == false)
			return new TimeImpl("unresolved");
		
		//  MIN( indefinite, unresolved) = indefinite 
		//  MIN( non-zero value, unresolved) = non-zero value 
		if (m1.getResolved() == false)
			return m2;
		if (m2.getResolved() == false)
			return m1;

		//  MIN( non-zero value, indefinite) = non-zero value 
		// [ MIN( zero value, indefinite) = zero value ]
		if (m1.isIndefinite() == true)
			return m2;
		if (m2.isIndefinite() == true)
			return m1;

		//  MIN( zero value, anything) = zero value 
		if (m1.getResolvedOffset().equals(zero))
			return m1;
		if (m2.getResolvedOffset().equals(zero))
			return m2;
		
		//  MIN( non-zero value, non-zero value) = non-zero value 
		if (m1.getResolvedOffset().intValue() < m2.getResolvedOffset().intValue())
			return m1;

		return m2;
	
	}

	/**
	 * Maximization for active duration computation.
	 *  MAX( numeric value A, numeric value B) = B if B > A, otherwise A 
	 *  MAX( numeric value, indefinite) = indefinite 
	 *  MAX( numeric value, unresolved) = unresolved 
	 *  MAX( indefinite, unresolved) = unresolved 
	 */
	private Time ACMax(Time m1, Time m2) {
		// Safety check
		if (m1 == null || m2 == null)
			return new TimeImpl("unresolved");

		//  MAX( numeric value, unresolved) = unresolved 
		//  MAX( indefinite, unresolved) = unresolved 
		if (m1.getResolved() == false || m2.getResolved() == false)
			return new TimeImpl("unresolved");

		//  MAX( numeric value, indefinite) = indefinite 
		if (m1.isIndefinite() == true || m2.isIndefinite() == true)
			return new TimeImpl("indefinite");

		//  MAX( numeric value A, numeric value B) = B if B > A, otherwise A 	
		if (m1.isGreaterThan(m2))
			return m1;
		else 
			return m2;
	}	

	/**
	 * Define the simple duration.
	 * This is based on the dur and other attributes. Based on the Simple Duration Table in the spec.
	 * MediaElement also checks the Simple Duration Table, when the media ends.
	 *
	 * @return		Simple duration
	 */
	protected Time computeSimpleDuration() {
		Time dur = getDur();
		TimeList end = endInstanceList;
		Time repeatDur = getRepeatDur();
		String repeatCount = getRepeatCount();

		// dur can't be less than zero
		if (dur != null && dur.isNegative() == true)
			dur = null;
		
		// dur == unspecified && repeatDur == unspecified && repeatCount == unspecified &&
		// end == specified
		if (dur == null && repeatDur == null && repeatCount == null && end.isDefined() == true)
			return new TimeImpl("indefinite");
		
		// dur == unspecified && implicit element dur == resolved
		// Not implemented - we don't have media duration.

		// dur == unspecified && implicit element dur == unresolved (handles "media" as well)
		if (dur == null)
			return new TimeImpl("unresolved");		
				
		// dur == indefinite
		if (dur.isIndefinite() == true) 
			return new TimeImpl("indefinite");
		
		// dur == Clock-value
		if (dur.getResolved() == true) {
			return dur;
		}	

		// all other == unspecified
		// return unresolved.
		return new TimeImpl("unresolved");
	}

	/**
	 * Compute the active duration.
	 * @param begin		Assumed begin parameter time
	 * @param end		Assumed end parameter time
	 * @return	Active duration of the element.
	 */
	private Time computeActiveDuration(Time begin, Time end) {
		Time dur = getDur();
		Time repeatDur = getRepeatDur();
		String repeatCount = getRepeatCount();
		Time min, max;
	
		// PAD
		Time pad = new TimeImpl("unresolved");
		// dur == unspecified && repeatDur == unspecified && repeatCount == unspecified &&
		// end == specified
		if (dur == null && repeatDur == null && repeatCount == null && end != null) {
			// End resolved to a value, pad = end - begin
			if (end.getResolved() == true) // && end.getResolvedOffset() >= 0)
				pad = ACSub(end, begin);
			// end == indefinite
			if (end.isIndefinite() == true)
				pad = new TimeImpl("indefinite");
			// end == unresolved
			if (end.getResolved() == false)
				pad = new TimeImpl("unresolved");
		}
		
		// end unspecified or end == indefinite
		if (end == null || end.isIndefinite() == true) {
			// Intermediate Active Duration computation
			pad = computeIntermediateActiveDuration();
		} else {
			// end specified and not indefinite, and dur, repeatDur, repeatCount specified
			if (dur != null || repeatDur != null || repeatCount != null) {
				pad = ACMin(computeIntermediateActiveDuration(), ACSub(end, begin));
			}
			
		}
		
		// Check for unresolved PAD (KP) - and return unresolved
		if (pad.getResolved() == false)  {
			activeDuration = pad;
			return pad;
		}
		
		min = getMin();
		max = getMax();

		// Min must be less than max, otherwise ignore
		if (min.isGreaterThan(max)) {
			min = new TimeImpl("0s");
			max = new TimeImpl("indefinite");
		}
		
		Time time = ACMin(max, ACMax(min, pad));
		activeDuration = time;
		return time;
	}

	/**
	 * Compute Intermedia Action Duration
	 * IAD = MIN( simpleDur(), repeatCount*media, repeatDur)
	 * @return	Intermediate active duration
	 */
	private Time computeIntermediateActiveDuration() {
		Time p0, p1, p2;
		
		p0 = computeSimpleDuration();

		// Time calculated from repeatCount
		// If not specified, or Simple Duration is indefinite (it * count = indefinite)
		if (getRepeatCount() == null || p0.isIndefinite() == true)
			p1 = new TimeImpl("indefinite");
		else if (getRepeatCount().equals("indefinite")) {
			p1 = new TimeImpl("indefinite");
			// If Simple Duration is unresolved, then do manual repeatCount forever
			if (p0.getResolved() == false)
				manualRepeat = 1000;
		} else {
			// Calculate p1
			if (p0.isIndefinite() == true)
				p1 = p0;
			else {
				// Parse repeatCount
				float count = 0;
				try {
					// count = Float.parseFloat(getRepeatCount());
					count = new Float(getRepeatCount()).floatValue();
				} catch (NumberFormatException e) {
				}
				
				// Negative values should be zero
				if (count < 0)
					count = 0;

				// KP: repeatCount can't be calculated here, if Simple Duration is unresolved.
				// Added rule to return zero, if Simple Duration is unresolved (element won't start).
				// Exception are MediaElements with continuous media - for them
				// the repeatCount is calculated manually in mediaEnded() in MediaElement.
				// This implements the long description for repeatCount and P1 in the spec.
				if (p0.getResolved() == false) {
					// Return zero, except for media with Simple Dur and time containers
					if (!(this instanceof SMILMediaElementImpl || this instanceof ElementTimeContainerImpl))
						return new TimeImpl(0);
					// If static media, and simple dur is not defined
					if (this instanceof SMILMediaElementImpl && ((SMILMediaElementImpl)this).isStatic() == true)
						return new TimeImpl(0);

					// Set the value						
					manualRepeat = count;
					// The spec says that we don't have to care about values less 
					// than one in this case.
					if (manualRepeat < 1)
						manualRepeat = 1;
					// p1 will be unresolved.	
					p1 = p0;
					// If repeatDur is unspecified, then Active Duration will be unresolved
					// It will be resolved when the media ends or time container ends.
					// This check is required - otherwise ACMIN() will result "indefinite", if p2==null.
					if (getRepeatDur() == null)
						return p1;
				}
				else
					// The standard case - just multiply them.
					p1 = new TimeImpl((int)(p0.getResolvedOffset().intValue() * count));
			}
		}	
		
		p2 = getRepeatDur();
		if (p2 == null) {
			p2 = new TimeImpl("indefinite");
		}
		
		// If p0 equals 0, IAD = 0
		if (p0.getResolved() == true && p0.getResolvedOffset().equals(zero) && p0.isIndefinite() == false)
			return p0;

		// If repeatDur and repeatCount unspecified
		if (getRepeatDur() == null && getRepeatCount() == null)
			return p0;
		
		
		return ACMin(p1, ACMin(p2, new TimeImpl("indefinite")));
	}

	//// Standard methods ////

	/**
	 * Default begin value 
	 * For media, par and seq this is 0s, for excl this is indefinite.
	 * @return	Time string
	 */
	protected String defaultBegin() {
		return "0s";
	}
	
	/**
	 *  Causes this element to begin the local timeline.
	 *
	 * @return  <code>true</code> if the method call was successful and the 
	 *   element was begun. <code>false</code> if the method call failed, the element
	 *   cannot be restarted. 
	 *    
	 * @exception DOMException
	 *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
	 *   to allow <code>beginElement</code> calls. 
	 */
	public boolean beginElement() throws DOMException {
		// Add time to begin list
		TimeImpl t = new TimeImpl(getCurrentParentTime(), 0, Time.SMIL_TIME_EVENT_BASED, null,
					null, true, true, true, false);
		this.addTime(true, t); //new TimeImpl(getCurrentParentTime()));
		return true;
	}

	/**
	 *  Causes this element to begin the local timeline.
	 * @param offset		Time in seconds
	 *
	 * @return  <code>true</code> if the method call was successful and the 
	 *   element was begun. <code>false</code> if the method call failed, the element
	 *   cannot be restarted. 
	 *    
	 * @exception DOMException
	 *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
	 *   to allow <code>beginElement</code> calls. 
	 */
	public boolean beginElementAt(int offset) throws DOMException {
		// Add time to begin list
		TimeImpl t = new TimeImpl(getCurrentParentTime()+offset*1000, 0, Time.SMIL_TIME_EVENT_BASED, null,
					null, true, true, true, false);
		this.addTime(true, t); //new TimeImpl(getCurrentParentTime()+offset*1000));
		return true;
	}

	/**
	 *  Causes this element to end the local timeline (subject to sync 
	 * constraints). 
	 * @return  <code>true</code> if the method call was successful and the 
	 *   element was ended. <code>false</code> if method call failed (the
	 *   element is not active). 
	 *    
	 * @exception DOMException
	 *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
	 *   to allow <code>endElement</code> calls. 
	 */
	public boolean endElement() throws DOMException {
		// Add time to end list
		TimeImpl t = new TimeImpl(getCurrentParentTime(), 0, Time.SMIL_TIME_EVENT_BASED, null,
					null, true, true, true, false);
		this.addTime(false, t); //new TimeImpl(getCurrentParentTime()));
		return true;
	}

	/**
	 *  Causes this element to end the local timeline (subject to sync 
	 * constraints). 
	 * @param offset		Time in seconds
	 * @return  <code>true</code> if the method call was successful and the 
	 *   element was ended. <code>false</code> if method call failed (the
	 *   element is not active). 
	 *    
	 * @exception DOMException
	 *    SYNTAX_ERR: The element was not defined with the appropriate syntax 
	 *   to allow <code>endElement</code> calls. 
	 */
	public boolean endElementAt(int offset) throws DOMException {
		// Add time to end list
		TimeImpl t = new TimeImpl(getCurrentParentTime()+offset*1000, 0, Time.SMIL_TIME_EVENT_BASED, null,
					null, true, true, true, false);
		this.addTime(false, t); //new TimeImpl(getCurrentParentTime()+offset*1000));
		return true;
	}
	
	///////////// METHODS TO ACCESS ATTRIBUTES /////////////

    /**
     *  The desired value (as a list of times) of the  begin instant of this 
     * node. 
	 * If not found, default time 0s is returned.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBegin() {
		String begin = getAttribute("begin");
		if (begin.length() == 0)
			begin = null;
			
		return begin;
    }
    public void setBegin(TimeList begin) throws DOMException {
		return;
    }

    /**
     *  The list of active  ends for this node. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getEnd() {
		String end = getAttribute("end");
		if (end.length() == 0)
			end = null;
			
		return end;
    }
    public void setEnd(TimeList end) throws DOMException {
		return;
    }

	
    /**
     * The desired simple  duration value of this node in seconds.
	 * Note that this returns a Time, not a String. Therefore, dur="media" is returned as null.
	 *
     * @return 	Time object, which can have a value, be indefinite or null. Null means unspecified.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */

    public Time getDur() {
		if (duration != null)
			return duration;
	
		String dur = getAttribute("dur");
		if (dur == null || dur.length() == 0)
			return null;
			
		// If this is a time container and dur = "media", unspecify this attr. pg.14
		if (dur.equals("media"))
			return null;

		duration = new TimeImpl(dur);
		return duration;
    }
    public void setDur(String dur) throws DOMException {
		setAttribute("dur", dur);
		return;
    }

    /**
     *  A code representing the value of the  restart attribute, as defined 
     * above. Default value is <code>RESTART_ALWAYS</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getRestart() {
		String restart = getAttribute("restart");
		if (restart.equals("never"))
			return RESTART_NEVER;
		else if (restart.equals("whenNotActive"))
			return RESTART_WHEN_NOT_ACTIVE;

		// Default value
	    return RESTART_ALWAYS;
    }
	
    public void setRestart(short restart) throws DOMException {
		if (restart == RESTART_ALWAYS)
			setAttribute("restart","always");
		else if (restart == RESTART_NEVER)
			setAttribute("restart","never");
		else if (restart == RESTART_WHEN_NOT_ACTIVE)
			setAttribute("restart","whenNotActive");
		return;
    }


    /**
     *  The  repeatCount attribute causes the element to play repeatedly 
     * (loop) for the specified number of times. A negative value repeat the 
     * element indefinitely. Default value is 0 (unspecified). 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getRepeatCount() {
		String repeatCount = getAttribute("repeatCount");
		
		if (repeatCount == null || repeatCount.length() == 0) {
			// A KLUDGE to get some repeat behaviour
			String repeat = getAttribute("repeat");
			if (repeat == null || repeat.length() == 0)
				return null;
			return repeat;
		}
		
		return repeatCount;
    }

    public void setRepeatCount(String repeatCount) throws DOMException {
		setAttribute("repeatCount", repeatCount);
		return;
    }

    /**
     *  The  repeatDur causes the element to play repeatedly (loop) for the 
     * specified duration in seconds. 
     * @return 	Time object, which can have a value, be indefinite or null. Null means unspecified.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public Time getRepeatDur() {
	    String repeatDur = getAttribute("repeatDur");
	    if (repeatDur == null || repeatDur.length() == 0)
	    	return null;
	    	
	    return new TimeImpl(repeatDur);
    }
    public void setRepeatDur(MyFloat repeatDur) throws DOMException {
		return;
    }


	/**
	 *  
	 *  The  repeat causes the element to play repeatedly (loop) for the 
	 * specified duration in milliseconds. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public MyFloat getRepeat() {
		return zero;
	}
	public void setRepeat(MyFloat repeatDur) throws DOMException {
		return;
	}

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMin() {
		Time minTime = null;
		String min = getAttribute("min");
		if (min == null || min.length() == 0)
			min = null;
		else
		// indefinite is not an allowed value
			if (min.equals("indefinite") == true) {
				min = null;
			}
		// "media" must be handled somehow... it is invalid for time container
			
		// Default value is zero
		if (min == null)
			min = "0s";

		// If min was not understood or <= 0, then use default
		minTime = new TimeImpl(min);
		if (minTime.getResolved() == false) // FLOAT BUG || (minTime.getResolved() == true && minTime.getResolvedOffset() <= 0))
			minTime = new TimeImpl("0s");

		// Min on joku outo s��nt�...

		return minTime;
	}
	
	public void setMin(Time min) throws DOMException {
		setAttribute("min", String.valueOf((float)min.getResolvedOffset().intValue()/1000));
		return;
	}

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMax() {
		Time maxTime = null;
		String max  = getAttribute("max");
		if (max == null || max.length() == 0)
			max = null;

		// "media" must be handled somehow... it is invalid for time container

		// Default value is indefinite
		if (max == null)
			max = "indefinite";

		// If max was not understood or <= 0, then use default
		maxTime = new TimeImpl(max);
		if (maxTime.getResolved() == false || (maxTime.getResolved() == true && maxTime.getResolvedOffset().intValue() <= 0))
			maxTime = new TimeImpl("indefinite");

		return maxTime;
	}
	public void setMax(Time max) throws DOMException {
		setAttribute("max", String.valueOf((float)max.getResolvedOffset().intValue()/1000));
		return;
	}

	/**
	 * Dummy getFill() method - this returns always null.
	 * To be overridden in inherited elements with a real implementation.
	 */
	public String getFill()  {
		return null;
	}
}
