/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;

import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.Log;

/**
 * Set Element. This animation element overrides the usual animation methods (parse()/initArray()),
 * but also init() and update() in order to make the animation quicker. update() only
 * has to copy the "to" value to the AnimationScheduler. No interpolation or
 * additions.
 */
public class SMILSetElementImpl extends SMILAnimationImpl implements SMILSetElement {

	/**
	 * Constructor - set the owner
	 */
	public SMILSetElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

	/**
	 * Overridden init, because the set element only requires to-value.
	 */
	public void init() {	
		// Only for X-Smiles - standalones won't run this.
		//  - Start timing... ok, this is a bit early, but so what?
		if (getSMILDoc().getViewer() instanceof SMILMLFC) {
			// If this is parasite, then startup
			if ( ((SMILMLFC)(getSMILDoc().getViewer())).isHost() == false) {
				Log.debug("PARASITE ANIMATION SET INIT!!");
				startup();
			}
		}	

		// Get target
		attributeName = getAttributeName();
		target = getTargetElement();

		super.init();
	}

	/**
	 * Not used in set element. Set overrides update completely.
	 * @return null
	 */
	public AnimatedValue parse(AnimationScheduler sch)  {
		return null;
	}
	/**
	 * Not used in set element. Set overrides update completely.
	 * @return null
	 */
	public AnimatedValue parse(String str)  {
		return null;
	}
	/**
	 * Not used in set element. Set overrides update completely.
	 * @return null
	 */
	public AnimatedValue parse(int val)  {
		return null;
	}
	/**
	 * Not used in set element. Set overrides update completely.
	 * @return null
	 */
	public AnimatedValue[] initArray(int size)  {
		return null;
	}


	/**
	 * This method implements update for set element, i.e. return value of the to attribute.
	 * @param scheduler		AnimationSchduler, to get and set attribute values into "DOM buffer"
	 * @param oldvalue		DOM / earlier animation value
	 * @return				new calculated value
	 */
	public void update(AnimationScheduler scheduler) {
		String to;
		
		// If not active, just return
		if (state != ElementBasicTimeImpl.STATE_PLAY)
			return;

		to = getTo();
		if (to == null)
			to = "";

		// Set "to" value to the "DOM buffer"
		scheduler.setAnimAttribute(target, attributeName, to);
		return;
	}

	/**
	 *  A <code>DOMString</code> representing the value of the  to attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getTo() {
		return getAttribute("to");
	}
	public void setTo(String to) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

}

