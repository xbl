/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import fi.hut.tml.xsmiles.Log;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.VisualComponentService;

// DOM interface
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.events.MutationEvent;
import org.w3c.dom.events.Event;
import org.w3c.dom.Element;

import java.util.Vector;
import java.util.Enumeration;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;

/**
 *  A <code>parallel</code> container defines a simple parallel time grouping 
 * in which multiple elements can play back at the same time.  It may have to 
 * specify a repeat iteration. (?) 
 */
public class ElementParallelTimeContainerImpl extends ElementTimeContainerImpl 
							implements ElementParallelTimeContainer {

	// Region for this parallel time container
	SMILRegionElementImpl region = null;

	/**
	 * Constructor - Set the owner and name.
	 */
	public ElementParallelTimeContainerImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}
	
	/**
	 * This starts the children - overridden from ElementBasicTime.
	 */
	 public void startChildren() {
		XElementBasicTime activeChild;

		if (region == null)
			region = (SMILRegionElementImpl)getRegionElement();

		Log.debug("Parallel "+getId()+" element startChildren()");
		
		// Display SMIL elements and shadows
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		// If no children, then just wait until ??
		if (children.hasMoreElements() == false) {
			return;
		}

		// Get timed children
		while (children.hasMoreElements()) {
			activeChild = (XElementBasicTime)children.nextElement();
			activeChild.startup();
		}
		
	}

	public SMILRegionElement getRegionElement() {
		SMILLayoutElement layout = getSMILDoc().getDocLayout();
		String regStr = getAttribute("region");
		if (layout != null) {
			return layout.getRegionElement(regStr, this);
		}

		return null;	
	}

	/** 
	 * boolean timeContainerHasEnded()
	 *
	 * method on time containers called to evaluate whether
	 * time container has ended, according to the rules of endsync.
	 * Note: Only supported on par and excl
	 *
	 * A variant on this could be called when a child end is updated to
	 * create a scheduled (predicted) end time for the container.
	 *
	 * Note that we never check the end time of children - it doesn't matter.
	 *
	 * Assumes: 
	 *     child list is stable during evaluation
	 *     isActive state of children is up to date for current time.
	 *      [In practice, this means that the children must all be
	 *        pre-visited at the current time to see if they are done.
	 *        If the time container is done, and repeats, the children
	 *        may be resampled at the modified time.]
	 *
	 *   Uses interfaces: 
	 *   on TimedNode:
	 *     isActive()             tests if node is currently active
	 *     hasStarted()           tests if node has (ever) begun
	 *     begin and end          begin and end TimeValues of node
	 *
	 *   on TimeValue         (a list of times for begin or end)
	 *   isResolved(t)          true if there is a resolved time
	 *                                     at or after time t
	 */
	public boolean timeContainerHasEnded()
	{
		ElementBasicTimeImpl c;
		TimeChildList children = new TimeChildList(this, getSMILDoc().getViewer());
		// If no children, then just return true
		if (children.hasMoreElements() == false) {
			return true;
		}

//		Time now = new TimeImpl(0); //getCurrentTime(); // normalized for time container

		boolean assumedResult;

		// For first or ID, we assume a false result unless we find a child that has ended
		// For last and all, we assume a true result unless we find a dis-qualifying child

		String endsync = getEndSync();
		short endsyncRule = getEndSyncRule();
		
		// No endsync specified - or endsync == "media" -> use default ENDSYNC_LAST
		if (endsync == null || endsyncRule == ENDSYNC_MEDIA) {
			endsync = "last";
			endsyncRule = ENDSYNC_LAST;
		}
	
		if (endsync != null) {
			// if both, dur and endsync specified -> ignore endsync
			if (getDur() != null) {
				return false;
			}
			// if end and endsync, but none of dur, repeatDur, repeatCount specified.
			if (getEnd() != null && getDur() == null && getRepeatDur() == null && getRepeatCount() == null) {
				return false;
			}
		}
		
		if( ( endsyncRule == ENDSYNC_FIRST ) || ( endsyncRule == ENDSYNC_ID ) )
		   assumedResult = false;
		else
		   assumedResult = true;

		// Our interpretation of endsync == all:
		//          we're done when all children have begun, and none is active
		//

		// loop on each child in collection of timed children,
		//  and consider it in terms of the endsyncRule

		// foreach ( child c in timed-children-collection )
		while (children.hasMoreElements()) {
		   c = (ElementBasicTimeImpl)children.nextElement();
		   switch( endsyncRule ) {
		      case ENDSYNC_FIRST:
		         // as soon as we find an ended child, return true.
		         if( c.hasStarted() & !c.isActive() )
		            return true;
		         // else, keep looking (assumedResult is false)
		         break;

		      case ENDSYNC_ID:
		         // if we find the matching child, just return result
		         if( endsync.equals(c.getId()) )
		                 return( c.hasStarted() & !c.isActive() );
		         // else, keep looking (we'll assume the ID is valid)
		         break;

		      case ENDSYNC_LAST:
		         // we just test for disqualifying children
		         // If the child is active, we're definitely not done.
		         // If the child has not yet begun but has a resolved begin,
		         // then we're not done.
		         if( c.isActive() || c.isResolved() == true )
		             //|| c.begin.isResolved(now) )
		             return false;
		         // else, keep checking (the assumed result is true)
		         break;

		      case ENDSYNC_ALL:
		         // we just test for disqualifying children
		        // all_means_last_done_after_all_begin

		         // If the child is active, we're definitely not done.
		         // If the child has not yet begun then we're not done. 
		         // Note that if it has already begun,
		         // then we still have to wait for any more resolved begins
		         if( c.isActive() || !c.hasStarted() || c.isResolved() == true) //.getResolved() )
		             //|| c.begin.isResolved(now) )
		             return false;
		         // else, keep checking (the assumed result is true)
		         break;

		   } // close switch

		} // close foreach loop

		return assumedResult;

	} // close timeContainerHasEnded()

	/**
	 *  Controls the end of the container.  Need to address thr id-ref value. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getEndSync() {
		String endsync = getAttribute("endsync");
		
		if (endsync == null || endsync.length() == 0)
			return null;
			
		return endsync;
	}
	public void setEndSync(String endSync) throws DOMException {
		return;
	}
	
	private final short ENDSYNC_FIRST	= 0;
	private final short ENDSYNC_LAST	= 1;
	private final short ENDSYNC_ID		= 2;
	private final short ENDSYNC_ALL		= 3;
	private final short ENDSYNC_MEDIA	= 4;
	
	/**
	 * Returns the endsync rule, which can be ENDSYNC_FIRST, ENDSYNC_LAST, ENDSYNC_ID,
	 * ENDSYNC_ALL, ENDSYNC_MEDIA.
	 * @return endsync rule
	 */
	private short getEndSyncRule() {
		String endsync = getEndSync();
		if (endsync == null)
			return ENDSYNC_LAST;
		if (endsync.equals("first"))
			return ENDSYNC_FIRST;
		if (endsync.equals("last"))
			return ENDSYNC_LAST;
		if (endsync.equals("all"))
			return ENDSYNC_ALL;
		if (endsync.equals("media"))
			return ENDSYNC_MEDIA;

		// other strings are ids...
		return ENDSYNC_ID;
	}

	/**
	 *  This method returns the implicit duration in seconds. 
	 * @return  The implicit duration in seconds or -1 if the implicit is 
	 *   unknown (indefinite?). 
	 */
	public int getImplicitDuration() {
		return -1;
	}
	
	
}

