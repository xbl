/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.Component;
import java.awt.Dimension;

import java.net.URL;
import java.net.MalformedURLException;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

// These will import JMF Player
import javax.media.*;
import javax.media.protocol.*;

import fi.hut.tml.xsmiles.Log;

/**
 * This is the implementation of JMF media (audio/video...).
 */
public class JMFMedia implements Media, MouseListener, Runnable {

  	// Hashtable holding loaded images - this is a proxy
	private Hashtable loadedImages;
  
	// Label and ImageIcon for current image
	private Component comp = null;

	// Container for media (DrawingArea)
	private Container container = null;

	private javax.media.Player	player = null;

	// MediaListener - called after prefetch and endOfMedia.
	MediaListener mediaListener = null;

	// Location and coords for the media
	private int x=0, y=0, width=0, height=0;

	// Media URL
	private URL url = null;

	// ControllerListener to listen for JMF
	PlayerListener playerListener = null;

	// Player state set by this
	boolean playing = false;


	public JMFMedia() {
		// Create the proxy hashtable
		loadedImages = new Hashtable();			
	}

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static.
	 */
	public boolean isStatic() {
		return false;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public synchronized void prefetch() {
				
		javax.media.Player newPlayer = null;
		MediaLocator ml;
		
		try {
		    if ((ml = new MediaLocator(url)) == null) {
		        throw new Exception("Can't build URL for " + url.toString());
		    }
		    // This has been commented out, because it was used only for debugging.
		    // Also, the DataSource was creating extra threads, which were not killed.
		    //DataSource src = null;
		    //try  {
		    //    src = Manager.createDataSource(ml);
		    //    Log.debug("Player needed for mime-type: " + src.getContentType());
		    //} catch (NoDataSourceException e)  {
		    //    Log.error("Cannot find url: " + url.toString());
		    //    throw e;
		    //}
		    try  {
//				Manager.setHint(Manager.LIGHTWEIGHT_RENDERER, new Boolean(true)); 
		        newPlayer = Manager.createPlayer(ml);
		    } catch (NoPlayerException e) {
		        Log.debug("ElementPlayer:" + e);
		        throw e;
		    }
		} catch (Exception e) {
		    // Loading of media failed -> display broken image.
		    Log.debug("File not found, fetching broken image.");
		 //   SMILConfig sc = SMILConfig.getInstance();
		 // TODO: Fix this to be correct image
//		    String s  = "broken.gif"; //sc.getBrokenImage();
//		    URL    brokenImageUrl = URLFactory.getInstance().createURL(s);
//		    return fetchBrokenImage(brokenImageUrl);
		    //throw(e);
		}

		if (newPlayer != null) {
			player = newPlayer;
			playerListener = new PlayerListener(this);
			player.addControllerListener(playerListener);
			player.realize();

			// Wait until notified, after realization.
			try {
				this.wait();
			} catch (InterruptedException e) {
			}
		}
		
	}
	
	public void setContainer(Container container) {
		if (this.container != container) {

			// Change container
			if (comp != null && comp.isVisible() == true) {
				comp.setVisible(false);
				this.container.remove(comp);
				container.add(comp, 0);
				comp.setVisible(true);
			}

			this.container = container;
		}
	}

	public void play() {
		// Set to true, mediaEnded() will be called
		playing = true;

		if (container != null && comp != null) {
			container.add(comp);
			comp.setLocation(x, y);
			comp.setSize(width, height);
			comp.setVisible(true);
		} else if (container == null) 
			Log.error("Region container not set for media "+url.toString());
		else	
			Log.debug("Component missing for media "+url.toString());

		// Play it!
		if (player != null)
			player.start();
		else {
			Log.error("Player not initialized!");
			// End this media immediately
			Thread t = new Thread(this);
			t.start();
		}
	}

	/**
	 * This media ends immediately, if the player was not found. Actually,
	 * this should be replaced by an alt-text component!!!! (ending immediately)
	 */
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
		// Media ended immediately (almost...)
		if (mediaListener != null)
			mediaListener.mediaEnded();
	}


	public void pause() {
		// Pause it
		if (player != null)
			player.stop();
	}

	public void stop() {

		// Set to false, mediaEnded() will not be called
		playing = false;

		if (container != null && comp != null)
			container.remove(comp);
	
		// Stop it!
		if (player != null) {
			try {
				player.stop();
				player.setMediaTime(new Time(0));
			} catch (javax.media.NotRealizedError e) {
				// Catch if time is set for unrealized player
			}
		}

		// Clear media listeners
		// This is also needed to prevent JMF from sending MediaEndedEvent 
		// to activate the next element.
		if (mediaListener != null)
			mediaListener = null;	
			
	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		// If the media is shown on the screen, move it immediately
		if (comp != null && comp.isVisible() == true) {
			// Use the given coordinates
			comp.setLocation(x, y);
			comp.setSize(width, height);
		}			
	}

	public void close() {
		// close the player and set it to null to free memory
		if (player != null) {
			stop();
			player.removeControllerListener(playerListener);
			player.close();
		}
		player = null;

		// Free the listener		
		if (playerListener != null) {
			playerListener.free();
			playerListener = null;
		}
		
		// Flush all images
		if (loadedImages != null) {
			Enumeration i = loadedImages.elements();
			while(i.hasMoreElements()) {
	//				((Image)i.nextElement()).flush();
			}
		 
			loadedImages.clear();
		}
		loadedImages = null;
		
		if (container != null && comp != null)
			container.remove(comp);
		container = null;
		comp = null;
		mediaListener = null;
	}
	
	public synchronized void prefetched() {
		notify();
	}

	/**
	 * This moves the time position in media. JMF supports this for audio/video.
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs) {
	 	if (player != null)
		 	player.setMediaTime(new Time((float)millisecs/1000));
	 }

	/** 
	 * Get the real width of the media. It should be known after prefetch.
	 */
	public int getOriginalWidth() {
	 	if (comp != null) {
	 		Dimension d = comp.getPreferredSize();
	 		return d.width;
	 	} else
	 		return -1;	
	 }
	
	/** 
	 * Get the real height of the media. It should be known after prefetch.
	 */
	public int getOriginalHeight() {
		if (comp != null) {
			Dimension d = comp.getPreferredSize();
			return d.height;
		} else
				return -1;
	}

	/**
	 * Set the sound volume for media. Only applicable for sound media formats.
	 * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
	 * dB change in signal level = 20 log10(percentage / 100) )
	 */
	 public void setSoundVolume(int percentage) {
	 }

	/**
	 * Add a listener for this media.
	 */	
	public void addMediaListener(MediaListener listener) {
		mediaListener = listener;
	}
	

	/**
	 * A class to listen to JMF events and pass them to either JMFMedia or
	 * MediaListener (which should be XElementBasicTimeImpl).
	 */
	class PlayerListener implements ControllerListener {
	    JMFMedia jmfMedia = null;
		
	    public PlayerListener(JMFMedia m) {
			jmfMedia = m;
	    }
	    
		public void free() {
			jmfMedia = null;
		}
		
	    public synchronized void controllerUpdate(ControllerEvent event) {
	
		    if (event instanceof RealizeCompleteEvent) {
		    	// automatically prefetch after realized
		        player.prefetch();
		    } else if (event instanceof PrefetchCompleteEvent) {
				// Save the visual component so that it can be shown
				// Audio will return value null
			    comp = player.getVisualComponent();
				Log.debug("Realized + prefetched "+url.toString()+" comp: "+comp);

				// This will get all mouse events, and pass them to the MediaListener
				if (comp != null) {
//					comp.addMouseListener(jmfMedia);
					comp.setVisible(false);
				}
								
				// Inform the JMFMedia
				jmfMedia.prefetched();
		    } else if (event instanceof ResourceUnavailableEvent) {
			    Log.error("Error prefetching "+url.toString()+": "+((ResourceUnavailableEvent)event).getMessage());
				// Close the invalid player
				player.close();
				player = null;
				// Inform the JMFMedia
			    jmfMedia.prefetched();
		    } else if (event instanceof EndOfMediaEvent) {
				// Media ended - inform the SMIL player.
				// Calling stop() will not cause mediaEnded() to be called.
				if (mediaListener != null && playing == true)
					mediaListener.mediaEnded();
		    }
			return;
	    }
	}

	/**
	 * Mouse listener...
	 */
	public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}
	
	public void mouseEntered(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseEntered();
	}

	public void mouseExited(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseExited();
	}
	
	public void mousePressed(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mousePressed();
	}
	
	public void mouseReleased(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseReleased();
	}	

}