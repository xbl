/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

// My floating point hack for Palm...

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

import java.lang.Integer;
import java.lang.String;

/**
 *  Implementation of floating point numbers - ONLY FOR Palm. 
 * This floating point class has been used in the SMIL 2.0 interface, 
 * see package org.w3c.dom.smil20.
 */
public class MyFloat {
	int value;

	public MyFloat(String str) {
		value = new Integer(str).intValue();
	}

	public MyFloat(int i) {
		value = i;
	}

	public int intValue() {
		return value;
	}

	public String toString() {
		return Integer.toString(value);
	}
	
	public boolean equals(MyFloat f) {
		if (f.intValue() == intValue())
			return true;
		
		return false;	
	}
}

