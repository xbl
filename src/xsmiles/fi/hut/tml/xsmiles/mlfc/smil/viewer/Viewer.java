/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

import java.net.URL;
import org.w3c.dom.smil20.SMILDocument;
import org.w3c.dom.Element;

/**
 * This is the viewer and port to the world. It implements everything the SMIL DOM requires.
 */

public interface Viewer {

	/**
	 * Return the base URL of the document.
	 */	
	public URL getBaseURL();
	
	public int getWindowWidth();
	public int getWindowHeight();

	public String getSystemBitrate();
	public String getSystemCaptions();
	public String getSystemLanguage();
	public String getSystemOverdubOrCaption();
	public boolean getSystemRequired(String prefix);
	public int getSystemScreenWidth();
	public int getSystemScreenHeight();
	public int getSystemScreenDepth();
	public String getSystemOverdubOrSubtitle();
	public String getSystemAudioDesc();
	public String getSystemOperatingSystem();
	public String getSystemCPU();
	// Returns true if component is available
	public boolean getSystemComponent(String component);
	
	public MediaHandler getNewMediaHandler();

	public BrushHandler getNewBrushHandler();
	
	/**
	 * Get a new instance of foreign media handler - to render parasite elements.
	 */
	public MediaHandler getNewForeignHandler(Element e);
	

	/**
	 * Creates a new link handler.
	 */
	public LinkHandler getNewLinkHandler();
	
	/**
	 * Returns a new drawing area, of type DrawingArea.ROOTLAYOUT or DrawingArea.TOPLAYOUT
	 */
	public DrawingArea	getNewDrawingArea(int type, boolean block);
	
	public SMILDocument getSMILDoc();
	
	// Called from meta name="base"
	public void setDocumentBaseURI(String base);
	
	/**
	 * Open external link replacing the existing presentation
	 * @param url		URL to open
	 */
	public void gotoExternalLink(String url);
	/**
	 * Open external link replacing/opening new target
	 * @param url		URL to open
	 * @param target	target frame/window
	 */
	public void gotoExternalLinkTarget(String url, String target);
	/**
	 * Open external link in a new window
	 * @param url		URL to open
	 */
	public void gotoExternalLinkNewWindow(String url);
	
	public void displayStatusText(String url);
	
	public void addTimePoint(String elementId);
	
	public boolean getPlayImage();
	public boolean getPlayAudio();
	public boolean getPlayVideo();
	
	/**
	 * SMILDoc requires this from the SMILMLFC, which may return true or false. Other viewers
	 * always return true.
	 */
	public boolean isHost();

	/**
	 * Get the title of the presentation. null if no title present.
	 */
	public String getTitle();
	/**
	 * Set the title for the presentation.
	 * @param title		Title for the presentation
	 */
	public void setTitle(String title);
	
	public Decorator getDecorator();
}