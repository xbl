/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.StylesheetService;


import fi.hut.tml.xsmiles.mlfc.css.CSSImpl2;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;

import fi.hut.tml.xsmiles.util.EventUtil;


import org.w3c.dom.*;
import org.w3c.dom.smil20.*;
//import fi.hut.tml.xsmiles.xml.dom.DocumentImpl;
import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.mlfc.smil.extension.*;

import java.util.Hashtable;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.File;

import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import java.lang.reflect.*;

// Document implementation - either real one or palm hack
//import org.apache.xerces.dom.DocumentImpl;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.DocumentImpl;
//import fi.hut.tml.xsmiles.xml.dom.DocumentImpl;

/**
 *  A SMIL document is the root of the SMIL Hierarchy and holds the entire
 * content. Beside providing access to the hierarchy, it also provides some
 * convenience methods for accessing certain sets of information from the
 * document.  Cover document timing, document locking?, linking modality and
 * any other document level issues. Are there issues with nested SMIL files?
 * Is it worth talking about different document scenarios, corresponding to
 * differing profiles? E.g. Standalone SMIL, HTML integration, etc.
 */

public class SMILDocumentImpl extends DocumentImpl implements SMILDocument
{
    // Name of the default CSS stylesheet, to be used with CSS layout model.
    public static URL defaultSMILCSSDocument=null; 
    static
	{
    	try
		{
    		defaultSMILCSSDocument = Browser.getXResource("xsmiles.smil.stylesheet");
		} catch (MalformedURLException e)
		{
			Log.error(e);
		}
	}
    //new File("cfg/smil.css").getAbsolutePath();
    
    // Viewer containing the GUI
    Viewer viewer;
    
    // One document can have only one active layout
    SMILLayoutElement layout = null;
    
    // One document has only one smil
    XSMILSMILElement smil = null;
    
    // One document has only one body
    XSMILBodyElement body = null;
    
    // One document has only one head
    XSMILHeadElement head = null;
    
    // XSmilesDocument
    DocumentImpl xsmilesDoc = null;
    
    // Scheduler for animations
    AnimationScheduler animScheduler = null;
    
    // Scheduler for SMIL presentation
    Scheduler scheduler = null;
    
    // CSS stylesheet
    XSmilesStyleSheet styleSheet = null;
    
    // CSS: Layout mode: basic / CSS
    boolean cssLayout = false;
    
    // Document state
    public static final int STOPPED 	= 0;
    public static final int PREFETCHED	= 1;
    public static final int STARTED 	= 2;
    public static final int PAUSED 		= 3;
    public static final int ABORTED		= 4;
    public static final int DESTROYED	= 5;
    
    private int documentState = STOPPED;
    
    protected static Constructor paramElementConst;
    protected static boolean noParamElement=false;
    
    
    /**
     * Create a SMIL Document.
     */
    public SMILDocumentImpl()
    {
        animScheduler = new AnimationScheduler(this);
        scheduler = new Scheduler(this);
    }
    
    /**
     * Get AnimationScheduler.
     */
    public AnimationScheduler getAnimationScheduler()
    {
        return animScheduler;
    }
    
    /**
     * Get Scheduler.
     */
    public Scheduler getScheduler()
    {
        return scheduler;
    }
    
    /**
     * Set the viewer, which has MediaHandlers and TimerHandlers.
     */
    public void setViewer(Viewer v)
    {
        viewer = v;
    }
    
    public Viewer getViewer()
    {
        return viewer;
    }
    
    public Element createElement(String tag) throws DOMException
    {
        Log.debug("createElement without NS!");
        
        Element e = createElementNS((String)null, tag);
        return e;
    }
    
    /**
     * NON-DOM: Xerces-specific constructor. "localName" is passed in, so we don't need
     * to create a new String for it.
     */
    public Element createElementNS(String namespaceURI, String qualifiedName,
    String localpart)
    throws DOMException
    {
        return this.createElementNS(namespaceURI,qualifiedName);
        // From Xerces.CoreDocumentImpl:
        //return new ElementNSImpl(this, namespaceURI, qualifiedName, localpart);
    }
    
    /**
     * Create a new element - this method should be the only way to create new elements.
     * To copy an element, call this and then copy the attributes.
     * This method is for standalone players.
     */
    public Element createElementNS(String ns, String tag) throws DOMException
    {
        Element e = createElementNS(this, ns, tag);
        if (e == null)
            return super.createElementNS(ns, tag);
        
        return e;
    }
    
    protected String getLocalname(String tagname)
    {
        int index = tagname.indexOf(':');
        if (index<0) return tagname;
        String localname = tagname.substring(index+1);
        return localname;
    }
    
    /**
     * Create a new element - this method should be the only way to create new elements.
     * To copy an element, call this and then copy the attributes.
     */
    public Element createElementNS(DocumentImpl XSmilesDoc, String ns, String tagi) throws DOMException
    {
        xsmilesDoc = XSmilesDoc;
        Element element = null;
        
        String tag = getLocalname(tagi);
        
        if (tag == null)
        {
            Log.error("Invalid null tag name!");
            return null;
        }
        if (tag.equals("smil"))
        {
            element = new SMILSMILElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("head"))
        {
            element = new SMILHeadElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("layout"))
        {
            element = new SMILLayoutElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("topLayout"))
        {
            element = new SMILTopLayoutElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("root-layout"))
        {
            element = new SMILRootLayoutElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("region"))
        {
            element = new SMILRegionElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("meta"))
        {
            element = new SMILMetaElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("metadata"))
        {
            element = new SMILMetadataElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("body"))
        {
            element = new SMILBodyElementImpl(XSmilesDoc, this, ns);
            //			element = body;
            
        } else if (tag.equals("text") || tag.equals("img") || tag.equals("audio")
        || tag.equals("video") || tag.equals("ref") || tag.equals("animation")
        || tag.equals("textstream"))
        {
            element = new SMILMediaElementImpl(XSmilesDoc, this, ns, tagi);
            
        } else if (tag.equals("a"))
        {
            element = new SMILAElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("area") || tag.equals("anchor"))
        {
            element = new SMILAreaElementImpl(XSmilesDoc, this, ns, tagi);
            
        } else if (tag.equals("par"))
        {
            element = new SMILParElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("seq"))
        {
            element = new SMILSeqElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("switch"))
        {
            element = new SMILSwitchElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("customTest"))
        {
            element = new SMILCustomTestElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("customAttributes"))
        {
            element = new SMILCustomAttributesElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("brush"))
        {
            element = new SMILBrushElementImpl(XSmilesDoc, this, ns);
            
        } else if (tag.equals("animate"))
        {
            element = new SMILAnimateElementImpl(XSmilesDoc, this, ns, tagi);
            
        } else if (tag.equals("animateColor"))
        {
            element = new SMILAnimateColorElementImpl(XSmilesDoc, this, ns, tagi);
            
        } else if (tag.equals("animateMotion"))
        {
            element = new SMILAnimateMotionElementImpl(XSmilesDoc, this, ns, tagi);
            
        } else if (tag.equals("set"))
        {
            element = new SMILSetElementImpl(XSmilesDoc, this, ns, tagi);
            
        } else if (tag.equals("param"))
        {
            try
            {
                if (!noParamElement)
                {
                    // TODO: remove swing dependency
                    if (paramElementConst == null)
                    {
                        // first run, let's search for the constructor
                        Class[] types= new Class[] {XSmilesDoc.getClass(),this.getClass(),ns.getClass()};
                        paramElementConst = Class.forName("fi.hut.tml.xsmiles.mlfc.smil.extension.SMILParamElementImpl").getDeclaredConstructor(types);
                    }
                    Object[] argum = new Object[] {XSmilesDoc,this,ns};
                    element = (Element) paramElementConst.newInstance(argum);
                }
            }
            catch (Throwable t)
            {
                Log.error(t,"Probably no swing");
                noParamElement=true;
                element=null;
            }
        }
        
        // Other tags go back to the XSmilesDocument as null...
        if (element == null)
            Log.debug("SMIL didn't understand element: "+tagi);
        
        return element;
    }
    
    /**
     * Searches through the document to find the first smil that is active.
     * Sets the result to smil variable.
     */
    private Element searchFirstSMIL()
    {
        XSMILSMILElement smilElement = null;
        Element root = null;
        boolean result = true;
        
        if (smil != null)
            return smil;
        
        // Get root
        root = xsmilesDoc.getDocumentElement();
        
        // If parasite MLFC, any element is ok
        if (viewer.isHost() == false)
            return root;
        
        // If host MLFC, must be be <smil>
        if (root == null || !(root instanceof XSMILSMILElement))
        {
            Log.error("<smil> element not found.");
            throw new RuntimeException("<smil> element not found.");
        }
        
        // Check the systemAttributes
        smilElement = (XSMILSMILElement)root;
        result = AttributeHandler.evaluateSystemValues(smilElement, getViewer(), false);
        if (result == true)
        {
            Log.debug("SMIL root element found.");
            smil = smilElement;
            return smil;
        }
        
        Log.error("<smil> element tests failed. SMIL document not viewable in this player.");
        throw new RuntimeException("SMIL document not viewable in this player.");
    }
    
    /**
     * Searches through the document to find the first head and body.
     * Sets the result to body variable.
     */
    private void searchHeadAndBody()
    {
        
        if (smil == null)
            return;
        
        if (head != null && body != null)
            return;
        
        // Get children of <smil>
        NodeList childList = smil.getChildNodes();
        
        for (int i=0 ; i < childList.getLength() ; i++)
        {
            if (childList.item(i) instanceof XSMILBodyElement)
                body = (XSMILBodyElement)childList.item(i);
            if (childList.item(i) instanceof XSMILHeadElement)
                head = (XSMILHeadElement)childList.item(i);
        }
        
        if (head == null)
            Log.debug("<head> element not found.");
        if (body == null)
            Log.error("<body> element not found.");
        
        return;
    }
    
    /**
     * Searches through the document head to find the first layout that is active.
     * Sets the result to layout variable, which will be read by getDocLayout().
     */
    private void searchFirstLayout()
    {
        SMILLayoutElement layoutElement = null;
        boolean result = true;
        layout = null;
        String type = null;
        
        searchFirstSMIL();
        searchHeadAndBody();
        if (head != null)
            layout = ((SMILHeadElementImpl)head).getLayout();
        else
        {
            Log.info("<head>/<layout> elements not found - using the default layout.");
            // Create a layout element using dummy namespace
            // This layout will not have any root-layout or regions - defaults are used.
            layout = new SMILLayoutElementImpl(xsmilesDoc, this, "ns");
            layout.init();
        }
        
        return;
    }
    
    public SMILLayoutElement getDocLayout()
    {
        // If layout not yet known, then find it.
        if (layout == null)
            searchFirstLayout();
        return layout;
    }
    
    /**
     * Searches through the document head to find the first customAttributes element that is active.
     * Returns the element.
     */
    public SMILCustomAttributesElementImpl searchFirstCustomAttributesElement()
    {
        SMILCustomAttributesElementImpl customElement = null;
        boolean result = true;
        String type = null;
        
        // Get children of <head>
        NodeList childList = head.getChildNodes();
        
        // Go through elements until one with acceptable attributes is found.
        for (int i=0 ; i < childList.getLength() ; i++)
        {
            if (childList.item(i) instanceof SMILCustomAttributesElementImpl)
            {
                customElement = (SMILCustomAttributesElementImpl)childList.item(i);
                result = AttributeHandler.evaluateSystemValues(customElement, getViewer(), false);
                
                if (result == true)
                {
                    Log.debug("customAttributes "+customElement.getId()+" selected!");
                    return customElement;
                }
            }
        }
        
        // Not found
        return null;
    }
    
    /**
     * Get this SMIL document state. Used in prefetch() to ABORT the prefetching.
     */
    public int getDocumentState()
    {
        return documentState;
    }
    
    /**
     * Set this SMIL document state. Used in SMILMLFC to ABORT the prefetch.
     */
    public void setDocumentState(int state)
    {
        documentState = state;
    }
    
    /**
     * Initialize this smil document / presentation.
     * @param browser	Set to true, if run in X-Smiles
     */
    public void initialize(boolean browser)
    {
        
        // Get the first smil element with ok system attributes
        searchFirstSMIL();
        if (smil == null)
        {
            Log.error("<smil> element not defined. The presentation will not be played.");
            return;
        }
        
        // Initialize the smil element and its children (i.e. body)
        // (all except layout and its children)
        if (browser == false)
            smil.init();
        
        // Get the body element for startup() and closedown()
        searchHeadAndBody();
    }
    
    /**
     * Prefetch media (not non-playable media, under switch elements);
     * This prefetch goes the same route through elements as start().
     */
    public void prefetch()
    {
        // Prefetch the body
        if (body != null)
        {
            body.prefetch();
            documentState = PREFETCHED;
        } else
            Log.error("Can't prefetch - <body> not found!");
    }
    
    /**
     * Starts the whole presentation from the start time = 0.
     * Cannot currently restart a paused presentation.
     */
    public void start()
    {
        if (documentState == STARTED)
        {
            Log.debug("SMIL already started.");
            return;
        }
        if (documentState == PAUSED)
        {
            documentState = STARTED;
            return;
        }
        
        // Start the body
        if (body != null)
        {
            scheduler.start();
            body.startup();
            documentState = STARTED;
        } else
            Log.error("Can't start - <body> not found!");
    }
    
    /**
     * Searches for element with id from the tree under element e.
     * @param	e	start element
     * @param	id	id to search for
     * @return	the element with id, or null if not found.
     */
    public Element searchElementWithId(Element e, String id)
    {
        Element result = null;
        
        if (id == null || id.length() == 0)
            return null;
        
        if (e.getAttribute("id").equals(id))
            return e;
        
        NodeList children = e.getChildNodes();
        Node node = null;
        
        // If no children, then return null
        if (children.getLength() == 0)
        {
            return null;
        }
        
        for (int i=0 ; i < children.getLength() ; i++)
        {
            node = children.item(i);
            if (node instanceof Element)
            {
                result = searchElementWithId((Element)node, id);
                // If found, then return it up.
                if (result != null)
                {
                    return result;
                }
            }
        }
        
        // Not this one or any of the children, return null
        return null;
    }
    
    /**
     * Searches for element with id.
     * @param	id	id to search for
     * @return	the element with id, or null if not found.
     */
    public Element searchElementWithId(String id)
    {
        return searchElementWithId(searchFirstSMIL(), id);
    }
    
    /**
     * Searches for element with id under body.
     * @param	id	id to search for
     * @return	the element with id, or null if not found.
     */
    public SMILElement searchElementWithIdInBody(String id)
    {
        Element e = searchElementWithId(body, id);
        if (e instanceof SMILElement)
            return (SMILElement)e;
        return null;
    }
    
    /**
     * Starts the presentation from the element id = s.
     * @return	true if presentation playing after this method (was playing or was started)
     *			false if psesentation wasn't playing and the element wasn't found.
     */
    public boolean startFromElementName(String s)
    {
        if (documentState == STARTED)
        {
            Log.debug("SMIL already started.");
            return true;
        }
        
        Element e = searchElementWithId(body, s);
        // id not found -> not started
        if (e == null)
        {
            Log.error("SMIL Presentation cannot be started, id '"+s+"' not found in body.");
            return false;
        }
        
        if ((e instanceof SMILElement) == false)
        {
            Log.error("SMIL can start only smil elements.");
            return false;
        }
        
        // Start if the element has timing capabilities
        if (e instanceof ElementBasicTimeImpl)
        {
            scheduler.start();
            // Set link activation time TODO NICE SEQ TIME SETUP
            ((ElementBasicTimeImpl)e).setForceStartTime(System.currentTimeMillis());
            //			((ElementBasicTimeImpl)target).resolveSeekTime(null);
            ((ElementBasicTimeImpl)e).startup();
            documentState = STARTED;
        } else
        {
            Log.error("SMIL Presentation cannot be started, element '"+s+"' has no timing.");
            return false;
        }
        return true;
    }
    
    /**
     * Pauses the whole presentation immediately.
     */
    public void pause()
    {
        if (documentState != STARTED)
        {
            Log.debug("SMIL cannot be paused.");
            return;
        }
        
        documentState = PAUSED;
    }
    
    /**
     * Stops parasite smil document.
     */
    public void parasiteStop()
    {
        // Close the main scheduler
        if (scheduler != null)
            scheduler.stop();
        
        // Close the animation scheduler
        if (animScheduler != null)
            animScheduler.stop();
    }
    
    /**
     * Stops the whole presentation immediately.
     */
    public void stop()
    {
        Log.debug("SMILDoc.Stop");
        
        if (documentState == STOPPED)
        {
            Log.debug("SMIL already stopped.");
            return;
        }
        documentState = STOPPED;
        
        // Close the main scheduler
        if (scheduler != null)
            scheduler.stop();
        
        // Close the animation scheduler
        if (animScheduler != null)
            animScheduler.stop();
        
        // Stop the body, if X-Smiles hasn't destroyed it.
        if (body != null)
        {
            if (((ElementBasicTimeImpl)body).getState() != ElementBasicTimeImpl.STATE_DESTROYED)
                body.closedown();
        } else
            Log.error("Can't stop - <body> not found!");
        
    }
    
    /**
     * Uninitializes all elements in this document, freeing all memory.
     * Also, stops all timers and so on.
     * @param browser		true, if called from X-Smiles browser
     */
    public void freeResources(boolean browser)
    {
        Log.debug("SMILDoc.freeResources()");
        
        // Stop the body, if still playing and X-Smiles hasn't destroyed it yet.
        // (X-Smiles calls destroy() before MLFC.stop())
        if (documentState != STOPPED &&
        ((ElementBasicTimeImpl)body).getState() != ElementBasicTimeImpl.STATE_DESTROYED)
            stop();
        documentState = STOPPED;
        
        // Close the main scheduler
        if (scheduler != null)
            scheduler.destroy();
        
        // Close the animation scheduler
        if (animScheduler != null)
            animScheduler.stop();
        
        // Destroy the smil
        if (browser == false)
            if (smil != null)
            {
                smil.destroy();
            } else
                Log.error("Can't destroy / free memory - <smil> not found!");
        
        // Set references to null
        layout = null;
        body = null;
        smil = null;
    }
    
    /**
     * open a link popup
     */
    public void showLinkPopup(String url,java.awt.event.MouseEvent e)
    {
            ((SMILMLFC)getViewer()).showLinkPopup(url,e);
    }
    
    
    /**
     * Move to destination node or document.
     * @param	url		URL, can be internal "#node" or external "http://..."
     * @param	show	show attribute in area and a elements
     * @param	target	target attribute in area and a elements
     */
    public void traverseLink(String url, int show, String target, java.awt.event.MouseEvent e)
    {
        if (url == null || url.length() == 0)
        {
            Log.error("Link href empty.");
        }
        
        if (target == null)
            target = "";
        
        if (target.length() != 0)
        {
            Log.debug("External link actuated with target "+target+", going to url: " + url);
            if (e!=null&&(EventUtil.isPropertyRequest(e)||EventUtil.isFollowAlternativeRequest(e)))
            {
                this.showLinkPopup(url,e);
            }
            else
            {
                getViewer().gotoExternalLinkTarget(url, target);
            }
        } else if (isLinkToSameDocument(url) && show != XSMILAElement.SHOW_NEW)
        {
            Log.debug("Internal link actuated, going to url: " + url);
            gotoInternalLink(url);
        } else
        {
            if (e!=null&&(EventUtil.isPropertyRequest(e)||EventUtil.isFollowAlternativeRequest(e)))
            {
                this.showLinkPopup(url,e);
            }
            else
            {
                if (show == XSMILAElement.SHOW_NEW)
                { // New window
                    Log.debug("External link actuated, going to new window and url: " + url);
                    getViewer().gotoExternalLinkNewWindow(url);
                } else
                { // Replace
                    Log.debug("External link actuated, replacing with url: " + url);
                    getViewer().gotoExternalLink(url);
                }
            }
        }
    }
    
    /**
     * Seeks the SMIL presentation to the correct time - to activate internal doc link.
     * @param url		Internal url, '#node'
     * @return 	true if successfully started.
     */
    public boolean gotoInternalLink(String url)
    {
        String s = url.substring(1, url.length());
        
        // Get the target element
        SMILElement target = searchElementWithIdInBody(s);
        if (target == null)
        {
            Log.error("Internal link error: id '"+s+"' not found!");
            return false;
        }
        
        // Go upwards until body is found.
        // Stop elements, which are not in the same seq path.
        Node current = null;
        Node previous = current;
        ElementBasicTimeImpl child = null;
        current = target.getParentNode();
        while (current != null && !(current instanceof SMILSMILElementImpl))
        {
            if (current instanceof ElementSequentialTimeContainerImpl)
            {
                // Seq time container - stop all children, except target
                TimeChildList children = new TimeChildList((Element)current, getViewer());
                while (children.hasMoreElements())
                {
                    child = (ElementBasicTimeImpl)children.nextElement();
                    if (child.equals(previous) == false)
                    {
                        // Stupid kaffe requires this - otherwise the if clause above won't work. -KP
                        previous=previous;
                        child.closedown();
                    }
                }
            }
            previous = current;
            current = current.getParentNode();
        }
        
        // Start the target element if the element has timing capabilities
        if (target instanceof ElementBasicTimeImpl)
        {
            // Set link activation time TODO NICE SEQ TIME SETUP
            ((ElementBasicTimeImpl)target).setForceStartTime(System.currentTimeMillis());
            ((ElementBasicTimeImpl)target).resolveSeekTime(null);
        } else
        {
            Log.error("Internal link error: target element '"+s+"' cannot be started.");
            return false;
        }
        Log.debug("INTERNAL LINK STARTED!");
        return true;
    }
    
    /**
     * Check for '#' in link url.
     * @param s 		url string
     * @return			true if starts with '#'
     */
    private boolean isLinkToSameDocument(String s)
    {
        if (s.startsWith("#"))
        {
            return true;
        }
        return false;
    }
    
    public String    getTitle( )
    {
        return null;
    }
    public String     getReferrer( )
    {
        return null;
    }
    public String      getDomain( )
    {
        return null;
    }
    public String      getURL( )
    {
        return null;
    }
    public SMILElement getRootElement( )
    {
        return null;
    }
    
    /** CSS support **/
    
    /**
     * Get the body element.
     */
    public XSMILBodyElement getBodyElement()
    {
        return body;
    }
    
    /**
     * Returns a stylesheet instance created from default and associated stylesheet file.
     * @return instance of XMLStyleSheet2
     */
    public XSmilesStyleSheet getStyleSheet()
    {
        if (xsmilesDoc!=null&&xsmilesDoc instanceof StylesheetService)
        {
            return ((StylesheetService)xsmilesDoc).getStyleSheet();
        }
        else
        {
            if (styleSheet == null)
                styleSheet = createDefaultStyleSheet();
            return styleSheet;
        }
    }
    
    /**
     * Creates default stylesheet defined in ../xsmiles/bin/cfg/smil.css
     * @return instance of XMLStyleSheet2
     */
    public static XSmilesStyleSheet createDefaultStyleSheet()
    {
        try
        {
            //URL cssLocation = new URL("file:" + defaultSMILCSSDocument);
            URL cssLocation = defaultSMILCSSDocument;
            XSmilesStyleSheet ss = new CSSImpl2();
            ss.addXMLDefaultStyleSheet(cssLocation);
            return ss;
        }
        catch (Exception e)
        {
            Log.error(e);
        }
        return null;
    }
    
    /**
     * Set the CSS Layout Model on/off.
     * @param 	true=CSS Layout Model, false=SMIL Basic Layout
     */
    public void setCSSLayoutModel(boolean flag)
    {
        cssLayout = flag;
    }
    
    /**
     * Get the CSS Layout Model on/off.
     * @return 	true=CSS Layout Model, false=SMIL Basic Layout
     */
    public boolean getCSSLayoutModel()
    {
        return cssLayout;
    }
    
}


