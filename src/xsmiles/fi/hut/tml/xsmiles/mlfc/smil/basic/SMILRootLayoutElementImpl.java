/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.Log;
import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

/**
 *  Declares layout properties for the root-layout element. See the  
 * root-layout element definition . 
 *
 * Includes attributes:
 *  o 
 *  + 
 */
public class SMILRootLayoutElementImpl extends SMILElementImpl implements SMILRootLayoutElement {
	
	DrawingArea drawingArea = null;
	int width = 0, height = 0;

	/**
 	 * Constructor with owner doc
 	 */
	public SMILRootLayoutElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "root-layout");
	}
	
	/**
	 * Initializes this root-layout and its associated DrawingArea. This is only called 
	 * for root-layout under a valid layout element.
	 */
	public void initHead() {
		Log.debug("Root-layout initHead()");
		// Create a drawing area for this root-layout.
		if (drawingArea == null) {
			drawingArea = getSMILDoc().getViewer().getNewDrawingArea(DrawingArea.ROOTLAYOUT, false);

			SMILLayoutElement layout = getSMILDoc().getDocLayout();
			drawingArea.setBounds(0, 0, layout.getRootLayoutWidth(), layout.getRootLayoutHeight());
			drawingArea.setBackgroundColor(getBackgroundColor());
		}
	}

	/**
	 * Release resources.
	 */
	public void destroy() {
		drawingArea = null;
		super.destroy();
	}

	/** 
	 * Return drawing area.
	 */
	 public DrawingArea getDrawingArea() {
	   return drawingArea;
	 }

	// These are ElementLayout interface

	public String getBackgroundColor() {
		String color = getAttribute("backgroundColor");
		// if not found, then try to find the deprecated attribute
		if (color == null || color.length() == 0) {
			color = getAttribute("background-color");
			if (color == null || color.length() == 0)
				return null;
		}
			
		return color;
	}
	public void setBackgroundColor(String backgroundColor) throws DOMException {
		setAttribute("backgroundColor", backgroundColor);
		drawingArea.setBackgroundColor(getBackgroundColor());
	}

	/**
	 * Height of the root-layout - can only have absolute pixel unit values.
	 * Strips off 'px' if defined. Returns null, if no height attribute specified.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getHeight() {
		String height = getAttribute("height");
		if (height == null || height.length() == 0)
			return null;
		
		// Strip off the pixel unit
		if (height.endsWith("px")) {
			height = height.substring(0, height.length()-2);
		}
		
		return height;
	}
	public void setHeight(int height) throws DOMException {
		setAttribute("height", String.valueOf(height));
//		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}
	/**
	 * Width of the root-layout - can only have absolute pixel unit values.
	 * Strips off 'px' if defined. Returns null, if no width attribute specified.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getWidth() {
		String width = getAttribute("width");
		if (width == null || width.length() == 0)
			return null;

		// Strip off the pixel unit
		if (width.endsWith("px")) {
			width = width.substring(0, width.length()-2);
		}
			
		return width;
	}
	public void setWidth(int width)  throws DOMException {
		setAttribute("width", String.valueOf(width));
//		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

}

