/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import java.awt.Container;
import java.awt.Panel;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;

/**
 *
 */
public class AWTDecorator implements Decorator
{
    public AWTDecorator()
    {
        Log.debug("AWTDecorator created!");
    }
    public Container createRootLayout()
    {
        // Set the root layout drawing area
        // This MLFC has scrollbars.
        
        return new Panel();
    }
    // The MLFC has to take care of the scroll bars...
    public Container createScrollPanel(Container rootLayout)
    {
        return rootLayout;
        /*
        JScrollPane scrollPanel = new JScrollPane(rootLayout);
        
        // Default transparent background color
        ((JLayeredPane)rootLayout).setOpaque(false);
        scrollPanel.setOpaque(false);
        scrollPanel.getViewport().setOpaque(false);
        return scrollPanel;*/
    }
    public void createSecondaryBorders(Container rootLayout, Container scrollPanel)
    {
        /*
            Border empty = BorderFactory.createEmptyBorder();
            ((JLayeredPane)rootLayout).setBorder(empty);
            ((JComponent)scrollPanel).setBorder(empty);
         */
    }
    
    public void addToContainer(Container scrollPanel, Container container)
    {
        /*
        if (container instanceof JFrame)
        {
            Container bp = ((JFrame)container).getContentPane();
            bp.add(scrollPanel, 0);
            bp.validate();
            bp.setVisible(true);
        }
        else
        {
            container.add(scrollPanel, 0);
        }*/
    }
    
        /**
     * Returns a new MediaHandler for SMIL core logic.
     */
    public MediaHandler getNewMediaHandler()
    {
        // Pass XMLDocument for ExtensionMedia
        return new AwtMediaHandler();
    }
    
    /**
     * Returns a new BrushHandler for SMIL core logic.
     */
    public BrushHandler getNewBrushHandler(Viewer v)
    {
        AwtBrushHandler bh = new AwtBrushHandler();
        bh.setViewer(v);
        return bh;
    }
    
    /**
     * Returns a new LinkHandler for SMIL core logic.
     */
    public LinkHandler getNewLinkHandler()
    {
        return new AwtLinkHandler();
    }
    
    /**
     * Returns a new DrawingArea for SMIL core logic.
     * @param type		ROOTLAYOUT for the broswer container, TOPLAYOUT for a new frame.
     * @param block		CSS: used to create a JBlockPanel (true) instead of JPanel (false)
     */
    public DrawingArea getNewDrawingArea(int type, boolean block, Container rootLayout,boolean layoutModel)
    {
        // CSS: Create a DrawingArea with the CSS layout information
        // It will cause the area to have a flow layout...
        if (type == DrawingArea.ROOTLAYOUT)
            return new AwtDrawingArea(rootLayout);//, layoutModel);
        else
            return new AwtDrawingArea(type);
    }
    
    /**
     * Returns a new ForeignHandler for SMIL core logic.
     */
    public MediaHandler getNewForeignHandler(Element e)
    {
        // TODO: create AWT version
        Log.debug("Creating SMILMLFCForeignHandler instead of AWT!");
        MediaHandler mh = new AWTForeignMediaHandler(e);
        return mh;
    }
        /**
     * Check if JMF class is available. At the same time, set the hint...
     * @return		true if JMF is available, false otherwise
     */
  
    public boolean isJMFAvailable(MLFCListener listener)
    {
        return false;
    }
    
}

