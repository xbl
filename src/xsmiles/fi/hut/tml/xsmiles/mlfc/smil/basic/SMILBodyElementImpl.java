/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import fi.hut.tml.xsmiles.Log;
import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;

import org.w3c.dom.NodeList;


/**
 *  Declares body element.
 *  
 */
//public class XSMILBodyElementImpl extends ElementSequentialTimeContainerImpl 
//									implements  {
public class SMILBodyElementImpl extends ElementSequentialTimeContainerImpl 
									implements XSMILBodyElement {

  /**
   * Constructor with owner doc
   */
  public SMILBodyElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
    super(owner, smil, ns, "body");
  }
  
  /**
   * This will initialize the CSS background-color of the "virtual" root-layout.
   */
  public void init() {
	// Initialize the CSS values
	super.init();

	// If CSS is turned on, get the css background-color
	if (smilDoc.getCSSLayoutModel() == true) {
		// Set the background-color
		String bgc = getStyleString("background-color");
		SMILLayoutElement layout = getSMILDoc().getDocLayout();
		if (layout != null) {
			SMILRootLayoutElementImpl rootLayout = (SMILRootLayoutElementImpl)layout.getRootLayoutElement();
			rootLayout.setBackgroundColor(bgc);
		}
	}
  }

  /**
   * Returns time in body time space, start of the presentation.
   * @param t		Time to convert, in millisecs
   * @return 		time in body time, in millisecs
   */
  public long getTimeInBodyTime(long t) {
  	
  	return beginTime + repeatTime + t + totalChildDuration;
  }

  /**
   * Returns the current time in the parent time space. Valid only if the parent
   * is active. (activate() has been called).
   * @return		time from parent activate() in millisecs.
   */
  public int getCurrentParentTime() {

  	if (isActive() == false)
  		return 0;
  	
  	return (int)(System.currentTimeMillis()-activateTime);
  }

  /**
   * Checks if this is startable - i.e. if the element's parent is active
   * and if the parent is seq, that this element is the active child. This
   * is used to prevent syncbased elements responding if not active.
   * This will be overridden in body time container.
   */	
  public boolean isStartable()  {
  	return true;
  }

}