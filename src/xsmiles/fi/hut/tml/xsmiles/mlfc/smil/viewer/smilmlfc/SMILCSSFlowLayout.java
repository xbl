/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import java.util.Vector;
import java.util.Enumeration;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.FlowLayout;
import fi.hut.tml.xsmiles.Log;
import java.util.Hashtable;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingDrawingArea;

public class SMILCSSFlowLayout extends FlowLayout {

	// This is an evil static to get started with this experiment...
	// Key: Component, Value: Element
	static public Hashtable componentFlowMap = new Hashtable();

	/**
	 * Simple constructor.
	 */
	public SMILCSSFlowLayout() {
		super( FlowLayout.LEFT, 0, 0);
	}

	/**
	 * Layout the container with CSS flow.
	 */
	public void layoutContainer(Container target) {
		final int gap = 0;       // space between elements
		// FIXTHIS: repeat delete/insert won't work if this is non-zero!!!!!!
		final int minrowh = 5;  // minimum row height, in case of linefeeds
		int rowh = minrowh;      // current row height
		int maxlayoutw = 0, maxlayouth = 0; // Maximum w/h of layouted components

		Insets insets = target.getInsets();
		int maxwidth = target.getSize().width - (insets.left + insets.right + 2*gap);

		// If target is a stretched JBlockPanel, then get the max width of the parent
		if (target instanceof JBlockPanel) {
			JBlockPanel bp = (JBlockPanel)target;
			if (bp.getStretchWidth() == true) {
				// Find a parent container which is no stretched
				Container c = target.getParent();
				while (c != null && c instanceof JBlockPanel) {	
					if (((JBlockPanel)c).getStretchWidth() == false)
						break;
					c = c.getParent();	
				}
				maxwidth = c.getSize().width - (insets.left + insets.right + 2*gap);
			}
		}

		// Get all components and sort them into correct order
		Vector sortedComponents = sort(target);

		int nmembers = sortedComponents.size();
		int x = 0, y = insets.top + gap;
		// No linefeed in the start
		boolean lastComponentWasBlock = true; // No gap in the top row

		//Log.debug(" +++ Target: "+target+" "+nmembers);

		for (int i = 0 ; i < nmembers ; i++) {
			Component m = (Component)(sortedComponents.elementAt(i));
			//Log.debug(" +++++ Component: "+m+" vis:"+m.isVisible());
			// Don't care about invisible components
			if (m.isVisible() == false)
				continue;
			// This is a block panel - add a line before & after 
			// (except if previous component added linefeed or this is the first comp.)
			if( m instanceof JBlockPanel && lastComponentWasBlock == false && i != 0) {
				x = 0;
				y += rowh;
				rowh = minrowh;
			} 
			Dimension d = m.getPreferredSize();
			int width  = d.width; 
			int height = d.height;
			m.setSize( width, height );
			//Log.debug(" +++++            new size:"+width+"x"+ height);
			if((x != 0) && ((x+width+gap) > maxwidth)) {
				x = 0;
				y += rowh;
				rowh = minrowh;
			}
			x += gap;
			m.setLocation(x, y);
			x += width;
			rowh = Math.max(rowh, height);

			maxlayoutw = Math.max(maxlayoutw, x);
			maxlayouth = Math.max(maxlayouth, y+rowh);

			// This is a block panel - add a line before & after
			if( m instanceof JBlockPanel ) {
				x = 0;
				y += rowh;
				rowh = minrowh;
				lastComponentWasBlock = true;
			} else
				lastComponentWasBlock = false;
		}
		
		// Size the container, if it is a block, and doesn't have size set.
		// This is to automatically stretch it according to the children.
		if (target instanceof JBlockPanel) {
			JBlockPanel bp = (JBlockPanel)target;
			int w = bp.getPreferredSize().width;
			int h = bp.getPreferredSize().height;
			//Log.debug("----- target "+target+" "+bp.getStretchWidth()+" "+bp.getStretchHeight());
			if (bp.getStretchWidth() == true) {
				//Log.debug("Stretching width "+w+" -> "+maxlayoutw);
				w = maxlayoutw;
			}
			if (bp.getStretchHeight() == true) {
				//Log.debug("Stretching height "+h+" -> "+maxlayouth);
				h = maxlayouth;
			}
			// If size changed
			if (w != bp.getPreferredSize().width || h != bp.getPreferredSize().height) {
				bp.setSize(w, h);
				bp.setPreferredSize(new Dimension (w, h));
				// The size of this target changed -> force parent to redo its layout
				if (target.getParent() instanceof JBlockPanel)
					((JBlockPanel)target.getParent()).doLayout();
			}	
		}
	}

	/**
	 * Sort the components under target container according to their appearance in the DOM.
	 * If not found in DOM, just place the component to the end.
	 */
	private Vector sort(Container target) {
		int nmembers = target.getComponentCount();
		Vector s = new Vector();
		Component c;
		Element e;
		String path;
		SortedList slist = new SortedList();
//		Log.debug("sorting..."+nmembers);
		
		for (int i = 0 ; i < nmembers ; i++) {
			c = target.getComponent(i);
			e = (Element)(componentFlowMap.get(c));
			// Not found in the map, place it to the last
			if (e != null)
				path = getPath(e);
			else // not found in the map, use the last possible key..
				path = "/9zzz";
			slist.add(path, c);
		}
//		slist.dump();
		return slist.getValues();
	}

	/**
	 * Get the path to node n.
	 * @param n 		node
	 * @return			String path to node n
	 */
	private String getPath(Node n) {
		StringBuffer sb = new StringBuffer();
		while (n != null) {
			sb.insert(0, "/"+childNo(n));
			n = n.getParentNode();
		}
		return sb.toString();
	}
	
	private String childNo(Node n) {
		Node p = n.getParentNode();
		if (p == null)
			return "0";
		NodeList nl = p.getChildNodes();
		for (int i = 0 ; i < nl.getLength() ; i++) {
			if (nl.item(i).equals(n) == true) {
				String ii = "0000000000"+i;
				return ii.substring(ii.length()-10);
			}
		}
		return "-1";
	}

	/**
	 * This will sort the (key, value) pairs inserted into it. Really dummy implementation.
	 * @author Mr. Brickhead
	 */	
	private class SortedList {
		Vector sortedKeys;
		Vector sortedValues;
		
		public SortedList() {
			sortedKeys = new Vector();
			sortedValues = new Vector();
		}
		
		public void add(String key, Component value) {
			int i = 0;
			for (Enumeration e = sortedKeys.elements() ; e.hasMoreElements() ;) {
				if (((String)(e.nextElement())).compareTo(key) > 0)
					break;
				i++;				
			}
			
			sortedKeys.insertElementAt(key, i);
			sortedValues.insertElementAt(value, i);
		}
		
		public Vector getValues() {
			return sortedValues;
		}
		
		public void dump() {
			System.out.print("SL dump keys: ");
			for (Enumeration e = sortedKeys.elements() ; e.hasMoreElements() ;) {
				System.out.print(((String)(e.nextElement())).toString()+" ");
			}
			System.out.println("");
			System.out.print("SL dump vals: ");
			for (Enumeration e = sortedValues.elements() ; e.hasMoreElements() ;) {
				System.out.print(((Component)(e.nextElement())).toString()+" ");
			}
			System.out.println("");
		}
	}
}


