/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.smil20.SMILDocument;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.*;

/**
 * This is a sample class that shows how to view a smil document and play it
 * using awt.
 * <p>
 * NOTE: THIS VIEWER IS NOT COMPLETE AND WON'T WORK CORRECTLY.
 */
public class SMILViewer extends Container implements WindowListener, Runnable, Viewer, ActionListener {

	private static String docPath = null;
	private boolean jmfAvailable = false;
	private Panel rootlayoutContainer = null;
	private TextField urlField = null;
	private Frame frame;

	// Performance tracking/debugging...
	public static long initTime, initMem;

	public static void main(String[] args) {

		//		// !!!!PERFORMANCE TRACKING!!!!
		System.gc();
		initTime = System.currentTimeMillis();
		initMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println(
			"AWT INIT TIME: " + SMILViewer.initTime + "ms MEMORY: " + (double) SMILViewer.initMem / 1000 + "k");
		//		// !!!!PERFORMANCE TRACKING!!!!

		String str =
			"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><smil><head><meta name=\"title\" content=\"About\"/><layout><root-layout background-color=\"#c0c0c0\" width=\"640\" height=\"400\"/><region id=\"title\" left=\"100\" top=\"100\" width=\"500\" height=\"50\" z-index=\"3\"/></layout></head><body><par><text region=\"title\" src=\"data:,AWT SMIL Player, copyright Helsinki University of Technology, 2002.\" dur=\"indefinite\"/></par></body></smil>";
		Reader rd = null;
		String url = "about:";

		if (args.length < 1) {
			rd = new StringReader(str);
		} else {
			try {
				URL u = new URL(args[0]);
				url = args[0];
				rd = new InputStreamReader(u.openStream());
			} catch (MalformedURLException e) {
				url = "error:";
				rd = new StringReader(str);
			} catch (java.io.IOException e) {
				System.out.println("NOT FOUND!!!" + url);
				url = "error:";
				rd = new StringReader(str);
			}
		}

		// Display in urlField
		if (File.separatorChar != '/') {
			url = url.replace(File.separatorChar, '/');
		}

		SMILViewer cs = new SMILViewer(url);
		cs.init(cs, rd, url);
		Log.debug("INIT OK");

		//		// !!!!PERFORMANCE TRACKING!!!!
		System.out.println(
			"PREFETCH INIT TIME: +"
				+ ((System.currentTimeMillis() - SMILViewer.initTime))
				+ "ms MEMORY: +"
				+ ((double) (Runtime.getRuntime().totalMemory()
					- Runtime.getRuntime().freeMemory()
					- SMILViewer.initMem))
					/ 1000
				+ "k");
		System.gc();
		System.out.println(
			"PREFETCH GC TIME: +"
				+ ((System.currentTimeMillis() - SMILViewer.initTime))
				+ "ms MEMORY: +"
				+ ((double) (Runtime.getRuntime().totalMemory()
					- Runtime.getRuntime().freeMemory()
					- SMILViewer.initMem))
					/ 1000
				+ "k");
		//		// !!!!PERFORMANCE TRACKING!!!!

	}

	public SMILViewer() {
		jmfAvailable = isJMFAvailable();
		createGUI(null);
	}

	public SMILViewer(URL url) {
		jmfAvailable = isJMFAvailable();
		Log.debug("JMF OK");
		createGUI(url.toString());
		Log.debug("GUI OK");
		try {
			init(this, new InputStreamReader(url.openStream()), url.toString());
			Log.debug("INIT OK");
		} catch (java.io.IOException e) {
			System.out.println("NOT FOUND!!!" + url);
		}
	}

	public SMILViewer(String url) {
		jmfAvailable = isJMFAvailable();
		createGUI(url);
	}

	public void init(SMILViewer viewer, Reader reader, String path) {

		if (reader == null)
			return;

		// Document URL
		try {
			docPath = path.substring(0, path.lastIndexOf('/'));
		} catch (StringIndexOutOfBoundsException e) {
			try {
				docPath = path.substring(0, path.lastIndexOf('\\'));
			} catch (StringIndexOutOfBoundsException e2) {
			}
		}

		Log.debug("iDocument Path is " + docPath);

		// Xerces Parser - requires xerces.jar and xml-apis.jar
		SMILDocumentImpl smil = null;
		try {
			javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();

			factory.setNamespaceAware(true);

			factory.setAttribute(
				"http://apache.org/xml/properties/dom/document-class-name",
				"fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl");
			javax.xml.parsers.DocumentBuilder parser = factory.newDocumentBuilder();
			Log.debug("Xerces parser:" + parser.getClass() + ". NSpaces:" + parser.isNamespaceAware());
			org.xml.sax.InputSource input = new org.xml.sax.InputSource(reader);
			Document doc = parser.parse(input);
			smil = (SMILDocumentImpl) doc;
		} catch (javax.xml.parsers.ParserConfigurationException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText("Error parsing.");
			return;
		} catch (org.xml.sax.SAXException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText("Error parsing.");
			return;
		} catch (java.io.IOException e) {
			Log.error(e);
			smilDoc = null;
			displayStatusText("Error parsing.");
			return;
		}
		// End Xerces Parser

		System.out.println("SMIL PARSED");

		viewer.initViewer(smil);
		smil.setViewer(viewer);

		// Initialise the document - this is size and color for root-layout
		// etc.
		smil.initialize(false);

		// Optional prefetch.
		smil.prefetch();
		smil.start();
	}

	private void createGUI(String urlfield) {
		frame = new Frame("AWT SMIL Viewer");
		// This will get the window closed event.
		frame.addWindowListener(this);
		frame.setLayout(new BorderLayout());

		// Play controls
		Container pc = new Panel();
		pc.setLayout(new FlowLayout(0, 0, 0));
		pc.setBackground(Color.gray);
		Label label = new Label("URL:");
		//pc.add(label);
		urlField = new TextField(urlfield, 40);
		urlField.addActionListener(this);
		Panel urlp = new Panel();
		urlp.setLayout(new BorderLayout());
		urlp.setBackground(Color.gray);
		urlp.add(urlField, BorderLayout.CENTER);
		//pc.add(urlField);
		Button start = new Button("Start");
		start.addActionListener(this);
		pc.add(start);
		//			Button pause = new Button("Pause");
		//			pause.addActionListener(this);
		//			pc.add(pause);
		Button stop = new Button("Stop");
		stop.addActionListener(this);
		pc.add(stop);
		Button exit = new Button("Exit");
		exit.addActionListener(this);
		pc.add(exit);
		//		timePoints.addActionListener(this);
		//		pc.add(timePoints);
		Panel flexurl = new Panel();
		flexurl.setLayout(new BorderLayout());
		flexurl.setBackground(Color.gray);
		flexurl.add(label, BorderLayout.WEST);
		flexurl.add(urlp, BorderLayout.CENTER);
		flexurl.add(pc, BorderLayout.EAST);

		frame.add(flexurl, BorderLayout.NORTH);
		//		c.setSize(100,20);

		// Play area
		rootlayoutContainer = new Panel();
		frame.add(rootlayoutContainer, BorderLayout.CENTER);
		frame.setSize(640, 400);
		frame.show();
		//		statusText = new JTextField("Playing.");
		//		frame.getContentPane().add(statusText, BorderLayout.SOUTH);

	}

	public void start() {
		((SMILDocumentImpl) smilDoc).start();
	}

	public void stop() {
		((SMILDocumentImpl) smilDoc).stop();
	}

	private SMILDocument smilDoc = null;

	private void initViewer(SMILDocument s) {
		smilDoc = s;
	}

	/**
	 * Return the base URL of the document.
	 */
	public URL getBaseURL() {
		// Overridden base url
		//if (baseURI != null) {
		//	return baseURI;
		//}

		try {
			return new URL(docPath);
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public void gotoExternalLink(String url) {
		Log.debug("...going to " + url);
		//    URL u = URLFactory.getInstance().createURL(url);
		//  EventBroker.getInstance().issueXMLDocRequiredEvent(u, true);
	}

	/**
	 * Open external link replacing/opening new target
	 * 
	 * @param url
	 *                  URL to open
	 * @param target
	 *                  target frame/window
	 */
	public void gotoExternalLinkTarget(String url, String target) {
		Log.debug("... going to " + url);
	}
	/**
	 * Open external link in a new window
	 * 
	 * @param url
	 *                  URL to open
	 */
	public void gotoExternalLinkNewWindow(String url) {
		Log.debug("Links to new windows not supported yet.");
	}

	public void displayStatusText(String url) {
		Log.debug("LINK: " + url);
		//	statusText.setText(url);
	}

	public MediaHandler getNewMediaHandler() {
		AwtMediaHandler mh = new AwtMediaHandler(docPath);
		mh.setViewer(this);
		return mh;
	}

	/**
	 * Returns a new BrushHandler for SMIL core logic.
	 */
	public BrushHandler getNewBrushHandler() {
		AwtBrushHandler bh = new AwtBrushHandler();
		bh.setViewer(this);
		return bh;
	}

	public LinkHandler getNewLinkHandler() {
		LinkHandler lh = new AwtLinkHandler();
		lh.setViewer(this);
		return lh;
	}

	public void setDocumentBaseURI(String base) {
	}

	public SMILDocument getSMILDoc() {
		return smilDoc;
	}

	public DrawingArea getNewDrawingArea(int type, boolean block) {
		if (type == DrawingArea.ROOTLAYOUT) {
			return new AwtDrawingArea(rootlayoutContainer);
		} else
			return new AwtDrawingArea(type);
	}

	/**
	 * Returns a new ForeignHandler for SMIL core logic. NOT IMPLEMENTED IN
	 * STANDALONE PLAYER.
	 */
	public MediaHandler getNewForeignHandler(Element e) {
		return null;
	}

	public void addTimePoint(String elementId) {
		//  	if (elementId != null)
		//	  	timePoints.addItem(elementId);
	}

	/**
	 * Returns window width for the SMILDocument.
	 */
	public int getWindowWidth() {
		return 700;
	}
	/**
	 * Returns window height for the SMILDocument.
	 */
	public int getWindowHeight() {
		return 500;
	}

	public String getSystemBitrate() {
		return "14400";
	}

	public String getSystemCaptions() {
		return "false";
	}

	public String getSystemLanguage() {
		return "fi";
	}
	public String getSystemOverdubOrCaption() {
		return "caption";
	}
	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemRequired(String prefix) {
		return false;
	}
	public int getSystemScreenWidth() {
		return 700;
	}
	public int getSystemScreenHeight() {
		return 500;
	}
	public int getSystemScreenDepth() {
		return 16;
	}
	public String getSystemOverdubOrSubtitle() {
		return "overdub";
	}
	public String getSystemAudioDesc() {
		return "on";
	}
	public String getSystemOperatingSystem() {
		return "Linux";
	}
	public String getSystemCPU() {
		return "x386";
	}
	/**
	 * Returns the value of systemAttribute for the SMIL core logic.
	 */
	public boolean getSystemComponent(String component) {
		return false;
	}

	public boolean getPlayImage() {
		return true;
	}

	public boolean getPlayAudio() {
		return jmfAvailable;
	}

	public boolean getPlayVideo() {
		return jmfAvailable;
	}

	/**
	 * Get the title of the presentation. null if no title present.
	 */
	public String getTitle() {
		return null;
	}

	/**
	 * Set the title for the presentation.
	 * 
	 * @param title
	 *                  Title for the presentation
	 */
	public void setTitle(String t) {
		if (frame != null)
			frame.setTitle(t);
	}

	public boolean isHost() {
		return true;
	}

	/**
	 * Check if JMF class is available. At the same time, set the hint...
	 * 
	 * @return true if JMF is available, false otherwise
	 */
	private boolean isJMFAvailable() {
		try {
			Class m = Class.forName("javax.swing.Manager");
		} catch (Throwable e) {
			System.out.println("JMF not available! It is recommended to have it installed.");
			System.out.println("Video will not be played.");
			return false;
		}
		return true;
	}

	private String selectedElement = "body";

	/**
	 * Playback buttons.
	 */
	public void actionPerformed(ActionEvent e) {
		/*
		 * if (e.getSource() instanceof ComboBox) { selectedElement =
		 * (String)((ComboBox)(e.getSource())).getSelectedItem();
		 * System.out.println("---- "+selectedElement); return;
		 */

		if (e.getSource() instanceof TextField) {
			System.out.println("TEXTFIELD:" + e.getActionCommand());
			String filename = e.getActionCommand();

			// Stop the previous presentation
			if (smilDoc != null) {
				((SMILDocumentImpl) smilDoc).freeResources(false);
				smilDoc = null;

				if (rootlayoutContainer != null)
					rootlayoutContainer.removeAll();

			}
			// Open the SMIL url
			displayStatusText(filename + " prefetching.");
			// Initialize, and if failed, just exit
			Reader reader = null;
			URL u = null;
			InputStreamReader in = null;
			try {
				u = new URL(filename);
				in = new InputStreamReader(u.openStream());
			} catch (MalformedURLException ex) {
				System.out.println("Malformed URL: " + filename);
				displayStatusText("Malformed URL: " + filename);
				return;
			} catch (IOException ex) {
				System.out.println("Error reading URL: " + filename);
				displayStatusText("Error reading URL: " + filename);
				return;
			}

			init(this, in, filename);
			displayStatusText(filename + " stopped.");

			return;
		}

		String text = ((Button) e.getSource()).getLabel();
		if (text.equals("Stop")) {
			((SMILDocumentImpl) smilDoc).stop();
		}
		if (text.equals("Pause")) {
			((SMILDocumentImpl) smilDoc).pause();
		}

		if (text.equals("Start")) {
			if (selectedElement.equals("body"))
				 ((SMILDocumentImpl) smilDoc).start();
			else
				 ((SMILDocumentImpl) smilDoc).startFromElementName(selectedElement);
		}

		if (text.equals("Exit")) {
			((SMILDocumentImpl) smilDoc).stop();
			System.exit(0);
		}

	}

	/**
	 * Window closed.
	 */
	public void windowClosing(WindowEvent e) {
		((SMILDocumentImpl) smilDoc).stop();
		System.exit(0);
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowActivated(WindowEvent e) {
	}
	public void windowDeactivated(WindowEvent e) {
	}
	public void windowDeiconified(WindowEvent e) {
	}
	public void windowIconified(WindowEvent e) {
	}
	public void windowOpened(WindowEvent e) {
	}

	public void run() {
		//	  try {
		//	  	this.sleep(10000);
		//	  } catch ( InterruptedException e) {
		//	  }

	}
	// TODO: should this return the decorator from the SMIL MLFC?
	public Decorator getDecorator()
	{
	    return null;
	}
	
}
