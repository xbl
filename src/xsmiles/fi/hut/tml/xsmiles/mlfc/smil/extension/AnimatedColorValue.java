/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import java.util.Vector;
import java.awt.Color;

/**
 * Animated Color Value, animates values in format "rgb(16,16,16)" or "#101010"
 */
public class AnimatedColorValue implements AnimatedValue {

	float value = 0;
	float red = 0, green = 0, blue = 0;

	public AnimatedColorValue(float dummy) {
		red = 0;
		green = 0;
		blue = 0;
	}

	public AnimatedColorValue(float r, float g, float b) {
		red = r;
		green = g;
		blue = b;
	}

	public AnimatedColorValue(String str) {
		// Parse and indicate that it was a number
//		value = Integer.parseInt(str);
		Color c = ColorConverter.hexaToRgb(str);
		red = c.getRed();
		green = c.getGreen();
		blue = c.getBlue();
	}

	/**
	 * Parse this string into value. Throws NumberFormatException, if cannot parse.
	 * @param str	String to be parsed
	 * @return 		Parsed AnimatedValue
	 */
	static public AnimatedColorValue parse(String str) {
		// Parse and indicate that it was a number
//		int val = Integer.parseInt(str);
		return new AnimatedColorValue(0,0,0);
		
	}

	public float getRed() {
		return red;
	}
	public float getGreen() {
		return green;
	}
	public float getBlue() {
		return blue;
	}

	public String toString()  {
		return "rgb("+(int)red+","+(int)green+","+(int)blue+")";
	}

	public void clampValue()  {
		// Check color values.
		if (red < 0)
			red = 0;
		if (red > 255)
			red = 255;	
		if (green < 0)
			green = 0;
		if (green > 255)
			green = 255;	
		if (blue < 0)
			blue = 0;
		if (blue > 255)
			blue = 255;	
	}

	/**
	 * Add val to this.
	 * @param val	Val to be added to this.
	 * @return		Returns val+this.
	 */
	public AnimatedValue add(AnimatedValue val) {
		float r = red + ((AnimatedColorValue)val).getRed();
		float g = green + ((AnimatedColorValue)val).getGreen();
		float b = blue + ((AnimatedColorValue)val).getBlue();
		return new AnimatedColorValue(r,g,b);
	}

	/**
	 * Multiply this by integer value.
	 * @param integer	Integer value
	 * @return			Returns this * integer.
	 */
	public AnimatedValue mult(int integer) {
		float r = red * integer;
		float g = green * integer;
		float b = blue * integer;
		return new AnimatedColorValue(r,g,b);
	}
	
	/**
	 * Interpolate between this and val by t, where t is [0,1]. For integers: (val-this)*t.
	 * @param val	AnimatedColorValue
	 * @return		Returns this * integer.
	 */
	public AnimatedValue interpolate(AnimatedValue val, float t) {
		float r = (red - ((AnimatedColorValue)val).getRed()) * t;
		float g = (green - ((AnimatedColorValue)val).getGreen()) * t;
		float b = (blue - ((AnimatedColorValue)val).getBlue()) * t;
		return new AnimatedColorValue(r,g,b);
	}
	

	/**
	 * Calculates the distance between this and val. (for one dimension: abs(this-val) ) 
	 * @param val	AnimatedColorValue
	 * @return		Returns distance between this nad val..
	 */
	public float distance(AnimatedValue val) {
		float d = Math.abs(red - ((AnimatedColorValue)val).getRed()) +
				Math.abs(green - ((AnimatedColorValue)val).getGreen()) +
				Math.abs(blue - ((AnimatedColorValue)val).getBlue());
		return d;
	}
	
}
