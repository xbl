/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media;

import java.awt.Container;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import java.net.URL;

/**
 * This is the interface for media.
 */
public interface Media {
	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static.
	 */
	public boolean isStatic();

	/** 
	 * Sets the URL for this media. This method will only set the URL for the media.
	 * To actually download the data, prefetch() or play() should be called.
	 * @param url		URL for media
	 */
	public void setUrl(URL url);

	/**
	 * Sets the container the media will be rendered in. If media is audio, this
	 * can be null.
	 * @param container   This container will contain the media.
	 */
	public void setContainer(Container container);

	/**
	 * Set the coordinates for the media. These are relative to the given container,
	 * set using setContainer(). 
	 */
	public void setBounds(int x, int y, int width, int height);

	/**
	 * Set the sound volume for media. Only applicable for sound media formats.
	 * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
	 * dB change in signal level = 20 log10(percentage / 100) )
	 */
	 public void setSoundVolume(int percentage);

	/** 
	 * Get the real width of the media. If not visible, or size is unknown, then returns -1.
	 * @return Original width of the media.
	 */
	public int getOriginalWidth();

	/** 
	 * Get the real height of the media. If not visible, or size is unknown, then returns -1.
	 * @return Original height of the media.
	 */
	public int getOriginalHeight();

	/**
	 * Prefetches media. The URL must have been set using setUrl(). 
	 * The data will be downloaded from the URL. After calling this method,
	 * the media will be in memory and can be played. This is a blocking method.
	 */
	public void prefetch();

	/**
	 * Plays the media. The media will be added to the container set using setContainer().
	 * It will be visible. It will also play any animation it possibly has. Also,
	 * audio media is started using this method.
	 * <p>If the media is not yet prefetched, it will first be prefetched.
	 */
	public void play();

	/**
	 * Pauses the media. The media will stay visible, but any animations will be paused.
	 * Audio media will be silent. NOT IMPLEMENTED YET.
	 * ?How to restart paused media?
	 */
	public void pause();

	/**
	 * Stops the media. The media will be stopped and it will be invisible. Audio will be
	 * silent.
	 */
	public void stop();

	/**
	 * This will freeze all memory and references to this media. If the media is not
	 * yet stopped, it will first be stopped.
	 */
	public void close();

	/**
	 * This moves the time position in media. Works only for video/audio.
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs);

	/**
	 * Adds a MediaListener for this media. The listener will be called <ol>
	 * <li>When the media has been prefetched. (NOT IMPLEMENTED YET).
	 * <li>When the media ends. (animation or audio media) </ol>
	 * Static media, such as text and images will end immediately, notifying
	 * immediately about the end of the media.
	 */
	public void addMediaListener(MediaListener listener);

}