/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

import java.util.Hashtable;
import java.util.Enumeration;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Label;
import java.awt.Color;
import java.awt.Font;
import java.awt.Component;
import java.awt.Graphics;

import java.net.MalformedURLException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.EOFException;
import java.io.IOException;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import fi.hut.tml.xsmiles.Log;

/**
 * This is the implementation of image media.
 */
public class TextMedia implements Media, Runnable, MouseListener
{
    
    // Hashtable holding loaded images - this is a proxy
    private Hashtable loadedImages;
    
    // Label and ImageIcon for current image
    MyLabel jtext;
    
    // Container for media (DrawingArea)
    Container container = null;
    
    // Url to be shown
    URL url = null;
    
    // Text to be shown
    String text = null;
    
    // MediaListener - called after prefetch and endOfMedia.
    MediaListener mediaListener = null;
    
    // MouseListener - standard java listener
    MouseListener mouseListener = null;
    
    // Location and coords for the media
    int x=0, y=0, width=0, height=0;
    
    public TextMedia()
    {
        // Create the components
        jtext=new MyLabel();
        
        // This will get all mouse events, and pass them to the MediaListener
        jtext.addMouseListener(this);
        
        // Create the proxy hashtable
        loadedImages = new Hashtable();
    }
    
    /**
     * Checks if this media is static or continuous.
     * @return true	if media is static.
     */
    public boolean isStatic()
    {
        return true;
    }
    
    /**
     * Set the url for this text component. Text will have higher priority than URL.
     * @param url		URL string to show
     */
    public void setUrl(URL url)
    {
        this.url = url;
    }
    
    /**
     * Set the text for this text component. Text has higher priority than URL.
     * @param text		String to show
     */
    public void setText(String text)
    {
        this.text = text;
        if (text == null)
            this.text = "";
    }
    
    /**
     * Fetch the text from url and return it.
     * @param 	url		URL to be retrieved
     * @return			string in the URL
     */
    private String fetchText(URL url)
    {
        StringBuffer intext = new StringBuffer();
        String str;
        BufferedReader in = null;
        int i, hexVal;
        
        // If text has been set, then display it instead of the content of the URL
        // This happens, if this component is used as alt-text displayer.
        if (text != null)
            return text;
        
        // Load text from URL
        try
        {
            //  Create an input stream
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            
            // Read a string
            while ((str = in.readLine()) != null)
            {
                // Check for %xx hex encoding in the string, convert them to real chars
                i = 0;
                while (i < str.length())
                {
                    if (str.charAt(i) == '%')
                    {
                        try
                        {
                            hexVal = Integer.parseInt(str.substring(i+1, i+3), 16);
                            intext.append((char)hexVal);
                            i = i + 2;
                        } catch (NumberFormatException e)
                        {
                            // Just leave it as it is
                        } catch (IndexOutOfBoundsException e)
                        {
                            // Just leave it as it is
                        }
                    } else
                        intext.append(str.charAt(i));
                    i++;
                }
                // line end
                intext.append("\n");
            }
        } catch(EOFException e)
        {
            // OK
        } catch(IOException e)
        {
            Log.error("I/O Error reading text from "+url);
            return "";
        }
        finally
        {
            try
            { in.close(); } catch(Exception e)
            {}
        }
        
        // Add text to the proxy
        String contents = new String(intext);
        loadedImages.put(url.toString(), contents);
        return contents;
    }
    
    public void setContainer(Container container)
    {
        if (this.container != container)
        {
            
            // Change container
            if (jtext.isVisible() == true)
            {
                jtext.setVisible(false);
                this.container.remove(jtext);
                container.add(jtext, 0);
                jtext.setVisible(true);
            }
            
            this.container = container;
        }
    }
    
    public void play()
    {
        Log.debug("TEXTMEDIA: "+this+".play("+url+") ");
        
        if (container != null)
        {
            container.add(jtext, 0);
            // Set up text component
            jtext.setLocation(x, y);
            jtext.setSize(width-50, height);
            jtext.setVisible(true);
        } else
            Log.error("Region container not set for media "+url);
        
        // Media ends immediately for static media (almost..).
        // Media ended - inform the SMIL player.
        if (mediaListener != null)
        {
            Thread t = new Thread(this);
            t.start();
        }
        
    }
    
    
    public void run()
    {
        try
        {
            Thread.sleep(100);
        } catch (InterruptedException e)
        {
        }
        // Media ended immediately (almost...)
        if (mediaListener != null)
            mediaListener.mediaEnded();
    }
    
    public void pause()
    {
    }
    
    public void stop()
    {
        //Log.debug("PROXY: stop()");
        //		timer.stop();
        
        // Second event, this is the stop event.
        //	gfxComponent.setVisible(false);
        //		container.showComponents(false);
        //		container.repaint();
        //		container.setVisible(false);
        if (jtext != null)
        {
            jtext.setVisible(false);
            if (container != null)
                container.remove(jtext);
        }
        
        // Clear media listeners
        if (mediaListener != null)
            mediaListener = null;
        
    }
    
    public void setBounds(int x, int y, int width, int height)
    {
        // If the text is shown on the screen, move it immediately
        if (jtext.isVisible() == true)
        {
            // Use the given coordinates
            jtext.setLocation(x, y);
            jtext.setSize(width, height);
        }
        
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void close()
    {
        stop();
        
        // Clear all strings
        if (loadedImages != null)
        {
            Enumeration i = loadedImages.elements();
            //	while(i.hasMoreElements()) {
            //		(String)(i.nextElement()) = null;
            //	}
            
            loadedImages.clear();
        }
        
        jtext = null;
        loadedImages = null;
        mediaListener = null;
    }
    
    /**
     * This moves the time position in media. Not effective for this media.
     * @param millisecs		Time in millisecs
     */
    public void setMediaTime(int millisecs)
    {
    }
    
    /**
     * Get the real width of the media.
     */
    public int getOriginalWidth()
    {
        return -1;
    }
    
    /**
     * Get the real height of the media.
     */
    public int getOriginalHeight()
    {
        return -1;
    }
    
    /**
     * Set the sound volume for media. Only applicable for sound media formats.
     * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
     * dB change in signal level = 20 log10(percentage / 100) )
     */
    public void setSoundVolume(int percentage)
    {
    }
    
    public void addMediaListener(MediaListener listener)
    {
        mediaListener = listener;
    }
    
    /**
     * Mouse listener...
     */
    public void mouseClicked(MouseEvent e)
    {
        if (mediaListener != null)
            mediaListener.mouseClicked(e);
    }
    
    public void mouseEntered(MouseEvent e)
    {
        if (mediaListener != null)
            mediaListener.mouseEntered();
    }
    
    public void mouseExited(MouseEvent e)
    {
        if (mediaListener != null)
            mediaListener.mouseExited();
    }
    
    public void mousePressed(MouseEvent e)
    {
        if (mediaListener != null)
            mediaListener.mousePressed();
    }
    
    public void mouseReleased(MouseEvent e)
    {
        if (mediaListener != null)
            mediaListener.mouseReleased();
    }
    
    
    ////////// THESE DIFFER FROM SMILMLFC VERSION ///////////
    
    // data:, because URL won't understand data:,...
    String data;
    
    /**
     * Extra constructor for data.
     */
    public TextMedia(String d)
    {
        // Create the components
        jtext=new MyLabel();
        
        // This will get all mouse events, and pass them to the MediaListener
        jtext.addMouseListener(this);
        // Default opaque background color
        jtext.setOpaque(false);
        
        // Create the proxy hashtable
        loadedImages = new Hashtable();
        
        // Save data
        data = d;
    }
    
    private String fetchData()
    {
        // Strip off "data:,"
        return data.substring(6);
    }
    
    public void prefetch()
    {
        ///		Log.debug("TEXTMEDIA: "+this+".prefetch("+url.toString()+") ");
        String realBody;
        String type = "text/html";
        
        String content = "! not found !";
        if (url != null)
            content = fetchText(url);
        else
        {
            content = fetchData();
        }
        
        // Set content style - this should be "text/plain" OR "text/html"
        realBody = setContentStyle(content, type);
        
        // Add image to the proxy
        ///		loadedImages.put(url.toString(), realBody);
        
        jtext.setText(realBody);
        //		jtext.setLineWrap(true);
        //		jtext.setWrapStyleWord(true);
        //		jtext.setEditable(false);
        jtext.setVisible(false);
    }
    
    
    /**
     * Set the style, if found in the content. (<html><body style="color:blue">Jee</body></html>)
     * This is a very ugly way to find the style attribute and body content.
     *
     * @param content	Content string
     */
    private String setContentStyle(String content, String contentType)
    {
        int i, end, len = content.length();
        // First quick checks for the style
        String useStyles = "true";
        
        // If stylesheets are enabled, then set the style
        if (useStyles.equals("true"))
        {
            // If this is html and style attribute exists, then get the style
            if (contentType.equals("text/html")== true)
            {
                //if (content.regionMatches(0, "style=", 0, 6)) {
                
                // Go through the content and search for 'style='
                for (i = 0 ; i < len-8 ; i++)
                {
                    
                    if (content.substring(i, i+6).equals("style="))
                    {
                        // Found, use it
                        end = content.indexOf('"', i+7);
                        if (end == -1)
                            end = i+6;
                        //Log.debug("STYLE FOUND: "+content.substring(i+7, end));
                        this.format(content.substring(i+7, end),jtext);
                    }
                }
                //}
            } else
            { // No html - maybe text/plain
                //Log.debug("STYLE is text/plain: "+content);
                return content;
            }
        }
        
        // Search for body text
        // Go through the content and search for '<body'
        String s;
        for (i = 0 ; i < content.length()-8 ; i++)
        {
            s = content.substring(i,i+5);
            if (s.equals("<body"))
            {
                // Found, use it
                try
                {
                    end = content.indexOf('<', i+6);
                    i = content.indexOf('>', i);
                    //Log.debug("BODY FOUND: "+content.substring(i+1, end));
                    return content.substring(i+1, end);
                } catch (StringIndexOutOfBoundsException e)
                {
                    return content;
                }
                
            }
        }
        // Body not found - show it as is
        return content;
        
    }
    protected void format(String content, Component comp)
    {
        CSSFormatter csstf = null;
        try
        {
            
            Class cssClass = Class.forName("fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media.java2.CSSTextFormatter");
            java.lang.reflect.Constructor constructor = cssClass.getDeclaredConstructor(null);
            Object inst = constructor.newInstance(null);
            
            csstf=(CSSFormatter)inst;
            if (csstf!=null)
            {
                csstf.setStyle(content);
                csstf.formatComponent(jtext);
            }
            
        } catch (NoClassDefFoundError e)
        {
            csstf=null;
        } catch (java.lang.ClassNotFoundException e)
        {
            csstf=null;
            Log.debug(e.getMessage());
        } catch (java.lang.NoSuchMethodError e)
        {
            csstf=null;
            Log.debug(e.getMessage());
        }
        catch (Exception e)
        {
            csstf=null;
            Log.debug(e.getMessage());
        }
    }
    
    
    public class MyLabel extends Component
    {
        String text = "";
        Font font = null;
        Color foreground = Color.black, background = null;
        int width = 0, height = 0;
        
        public MyLabel()
        {
            font = new Font(null, Font.PLAIN, 12);
        }
        
        public void setText(String t)
        {
            if (t == null)
                text = "";
            else
                text = t;
        }
        
        public void setFont(Font f)
        {
            font = f;
        }
        
        public void setBackground(Color c)
        {
            background = c;
            super.setBackground(c);
        }
        
        public void setForeground(Color c)
        {
            foreground = c;
            super.setForeground(c);
        }
        
        public void setSize(int w, int h)
        {
            width = w;
            height = h;
            super.setSize(w, h);
        }
        
        public void setOpaque(boolean f)
        {
            // false = transparent
            if (f == false)
                background = null;
        }
        
        /**
         * Draw text
         */
        public void paint(Graphics g)
        {
            int h = 12;
            if (background != null)
            {
                g.setColor(background);
                g.fillRect(0,0, width, height);
            }
            
            g.setColor(foreground);
            
            if (font != null)
            {
                g.setFont(font);
                h = (int)font.getSize();
            }
            g.drawString(text, 0, h);
        }
        
    }
    
}