/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.events.Event;
import org.w3c.dom.smil20.XSMILAElement;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

/**
 * XSMILAElement is handled as an time container - containing media (or link or switch...).
 * Media will search through its parents for an a element. If it is found, the media element
 * will create a corresponding link.
 */
public class SMILAElementImpl extends ElementTimeContainerImpl 
							implements XSMILAElement, MediaListener {

	/**
	 * Constructor - Set the owner and name.
	 */
	public SMILAElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
	    super(owner, smil, ns, "a");
	}

	/**
	 * Overridden activate() - this will also show the media.
	 * If actuate == ACTUATE_ONLOAD, this will immediately follow a link.
	 * Otherwise, nothing is done. Links are actually handled in the media
	 * element - clicking any media element under a link will actuate the link.
	 * So, the link processing is actually done in the media element.
	 */
	public void activate() {
		// If actuate == "onLoad", follow the link as the active duration has started
		if (getActuate() == ACTUATE_ONLOAD) {
			activateLink(null);
		}
		super.activate();
	}

	/**
	 *  See the href attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getHref() {
		return getAttribute("href");
	}
	public void setHref(String href) throws DOMException {
		if (href != null && href.length() > 0) {
		  setAttribute("href", href);
		} else {
		  removeAttribute("href");
		}
	}

	/**
	 *  See the sourceLevel attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getSourceLevel() {
		return null;
	}
	public void setSourceLevel(String sourceLevel) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the destinationLevel attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getDestinationLevel() {
		return null;
	}
	public void setDestinationLevel(String destinationLevel) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// sourcePlaystate Types
	public static final short SOURCEPLAYSTATE_PLAY                 = 0;
	public static final short SOURCEPLAYSTATE_PAUSE                = 1;
	public static final short SOURCEPLAYSTATE_STOP                 = 2;

	/**
	 *  See the sourcePlaystate attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getSourcePlaystate() {
		return 0;
	}
	public void setSourcePlaystate(short sourcePlaystate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// destinationPlaystate Types
	public static final short DESTINATIONPLAYSTATE_PLAY                 = 0;
	public static final short DESTINATIONPLAYSTATE_PAUSE                = 1;
	public static final short DESTINATIONPLAYSTATE_STOP                 = 2;

	/**
	 *  See the destinationPlaystate attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getDestinationPlaystate() {
		return 0;
	}
	public void setDestinationPlaystate(short destinationPlaystate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// show Types
	public static final short SHOW_REPLACE              = 0;
	public static final short SHOW_NEW                  = 1;
	public static final short SHOW_PAUSE                = 2;

	/**
	 *  See the show attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getShow() {
		String show = getAttribute("show");
		if (show.equals("pause") == true)
			return SHOW_PAUSE;

		if (show.equals("new") == true)
			return SHOW_NEW;

		return SHOW_REPLACE;
	}
	public void setShow(short show) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the accesskey attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getAccessKey() {
		return null;
	}
	public void setAccessKey(String accesskey) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the tabindex attribute in LinkingAttributes module. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public int getTabindex() {
		return 0;
	}
	public void setTabindex(int tabindex) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the target attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getTarget() {
		return getAttribute("target");
	}
	public void setTarget(String target) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  See the external attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public boolean getExternal() {
		return false;
	}
	public void setExternal(String external) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	// actuate Types
	public static final short ACTUATE_ONREQUEST              = 0;
	public static final short ACTUATE_ONLOAD                 = 1;

	/**
	 *  See the actuate attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getActuate() {
		if (getAttribute("actuate").equals("onLoad") == true)
			return ACTUATE_ONLOAD;
		else	
			return ACTUATE_ONREQUEST;
	}
	public void setActuate(short actuate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Traverse link in this element. Opens the new document either in this
	 * window or a new window. Or an internal or external link.
	 */
	public void activateLink(java.awt.event.MouseEvent e) {
                String eventname = "DOMActivate";
                Event ev = EventFactory.createEvent(eventname); 
                Log.debug(this.getClass()+" dispatching DOM event: "+ev);
                this.dispatchEvent(ev);
                
		String url = getHref();
		int show = getShow();	
                {

                    // Get target - if it is defined (non-empty), it'll override show attribute
                    String target = getAttribute("target");
                    smilDoc.traverseLink(url, show, target, e);
                }
	}

	/**
	 * Mouse events - this will be called if a link is clicked.
	 */
	public void mouseClicked(java.awt.event.MouseEvent e) {
		activateLink(e);
	}
	 
	public void mouseEntered() {
	}

	public void mouseExited() {
	}
	
	public void mousePressed() {
	}
		
	public void mouseReleased() {
	}
	
	public void mediaPrefetched() {
	}
	
	public void mediaEnded() {
	}
}

