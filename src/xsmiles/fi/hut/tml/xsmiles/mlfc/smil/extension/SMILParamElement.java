/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;

import java.util.Hashtable;

import java.awt.*;


/**
 *  Declares parameters for media elements. This understands currently the following syntax 
 * to format plain text:
 *  <param name="color" value="#ffffff">
 *  <param name="bgcolor" value="#ffffff">
 *  <param name="font" value="courier">
 *  <param name="size" value="20">
 */
public interface SMILParamElement {


	/**
	 * This is a really stupid method formatting the possible JTextArea under the c component.
	 * Formats the text's font, size or color.
	 * @param c 	Component or its child to be formatted
	 */
	public void formatMedia(Component c);

}
