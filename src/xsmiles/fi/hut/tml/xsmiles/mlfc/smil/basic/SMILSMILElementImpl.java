/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.*;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.RefreshableService;
import fi.hut.tml.xsmiles.dom.StylesheetService;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;

/**
 *  Declares smil element.
 *  CSS: Implements the Stylesheet Service, so that XForms can access SMIL's stylesheets.
 */
public class SMILSMILElementImpl extends SMILElementImpl implements XSMILSMILElement, StylesheetService {

  /**
   * Constructor with owner doc
   */
  public SMILSMILElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
    super(owner, smil, ns, "smil");
  }


  /**
   * CSS: get the stylesheet object
   */
  public XSmilesStyleSheet getStyleSheet() {
  	// Get it from the smilDoc, which will always create it.
	return smilDoc.getStyleSheet();
  }

  /**
   * If smil element is destroyed, then the whole presentation will stop!
   */
  public void destroy()  {
  	smilDoc.stop();
  	super.destroy();
  }


}