/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JPanel;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.TextFormattingMediaHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingMediaHandler;

/**
 *  Implements media for SMILMLFC.
 */
public class SMILMLFCMediaHandler extends SwingMediaHandler implements TextFormattingMediaHandler, ActionListener {
	
	/**
	 * Create a new MediaHandler for MLFC.
	 * @param documentURL		The URL for smil document path.
	 */
	public SMILMLFCMediaHandler() {
		super();
		this.documentURL = null;
	}

	/**
	 * Create a new MediaHandler for MLFC.
	 * @param documentURL		The URL for smil document path.
	 */
	public SMILMLFCMediaHandler(String documentURL) {
		super(documentURL);
	}

	/*****************
	 * Visual Component Interface requires the component.
	 */
	public Object getComponent() {
		// Create container, if not yet created.
		if (container == null) {
			JPanel p = new JPanel();
			// No layout manager messing up the left-top coords
			p.setLayout(null);
			// Transparent Panel
			p.setOpaque(false);
			// This is required for XHTML - otherwise no size for component!
			if (media.getOriginalWidth() > 0) {
				p.setMinimumSize(new Dimension(media.getOriginalWidth(), media.getOriginalHeight()));
				p.setPreferredSize(new Dimension(media.getOriginalWidth(), media.getOriginalHeight()));
				p.setSize(media.getOriginalWidth(), media.getOriginalHeight());
			} 
			container = p;
		}
		return container;
	}
	public void prefetch() {
		URL u = null;

		// Normal media
		if (url == null || url.length() == 0) {
			Log.error("Media not found - src attribute missing?!");
			return;
		}

		// This is RealPlayer's cache http protocol, use it as normal http protocol
		if (url.startsWith("chttp") == true) {
			url = url.substring(1);
			Log.debug("Protocol chttp treated as normal http:");
		}

		// This is MMS cid: protocol, just strip it off!
		// This should actually be a real protocol handler, reading multipart content.
		if (url.startsWith("cid:") == true) {
			Log.info("WARNING: cid: protocol not supported.");
			Log.debug("File "+url+" will be read from local directory.");
			url = url.substring(4);
		}

		// Check the type of the url
		try {			
			u = ((SMILMLFC)viewer).createURL(url);
			XLink link = new XLink(u);
			
            // Override MIME type?
            if (mimeType != null)
            	link.setMIMEOverride(mimeType);
				
            media = (Media)((SMILMLFC)viewer).getMLFCListener().createContentHandler(link,null,false);
		} catch (Exception io) {
			Log.debug("URL type error: "+io.toString());
		}
				
		if (media != null) {
			//media.setUrl(u);
            try
            {
    			media.prefetch();
            } catch (Exception e)
            {
                Log.error(e, "Prefetching");
            }
			
			// Add all listeners to this media, now as it has been created.
			media.addMediaListener(mediaListener);
		}
		prefetched = true;		
	}
/*
	public void prefetch_old() {
		int lastDot = 0;
		URL u = null;
		String type = null;
		String urltype = null;
		String urlshort = null;

		// Normal media
		if (url == null || url.length() == 0) {
			Log.error("Media not found - src attribute missing?!");
			return;
		}
		urlshort = url.substring(0,url.length()>30?30:url.length());

		// This is RealPlayer's cache http protocol, use it as normal http protocol
		if (url.startsWith("chttp") == true) {
			url = url.substring(1);
			Log.debug("Protocol chttp treated as normal http:");
		}

		// Check the type of the url
		try {			
			u = ((SMILMLFC)viewer).createURL(url);
			// This URLConnection should be passed to Media instead of URL
			// Passing URL will cause the URL to be opened twice.
		    URLConnection urlConn = u.openConnection();
			if (urlConn != null)
			    urltype = urlConn.getContentType();
			// Override MIME type?
			if (mimeType != null)
				urltype = mimeType;

			// Check the ending / mime type		
		    try {
				lastDot = url.lastIndexOf('.')+1;
		    } catch(Exception allExp) {
				// This is meant to catch 'out of index' exceptions
				lastDot = 0;
		    } 

			// url starts with dir:
			if (url.substring(0,4).equals("dir:")) {
				Log.debug("Prefetch "+urlshort+": Creating DIR media for type:"+urltype);
				media = new XMLMedia(viewer);
			}
		    // url ends with ".doc" and not data:
		    else if (url.substring(0,5).equals("data:") == false) {
				if (url.substring(lastDot).equals("doc") || url.substring(lastDot).equals("ppt") ||
						 url.substring(lastDot).equals("mov") || url.substring(lastDot).equals("pdf") ||
						 url.substring(lastDot).equals("html")) {
			    	Log.debug("Prefetch "+urlshort+": Creating NATIVE media for type:"+urltype);
			    	media = new NativeMedia();
			    }
			    // url ends with ".ogg"
			    else if (url.substring(lastDot).equals("ogg")) {
			    	Log.debug("Prefetch "+urlshort+": Creating OGG media for type:"+urltype);
			    	media = new OggMedia();
			    }
			    // url ends with ".svg" or ".xml"
			    else if (url.substring(lastDot).equals("svg") || url.substring(lastDot).equals("xml")) {
					Log.debug("Prefetch "+urlshort+": Creating svg media for type:"+urltype);
					media = new XMLMedia(viewer);
			    }
			    // url ends with ".fo"
			    else if (url.substring(lastDot).equals("fo")) {
				    Log.debug("Prefetch "+urlshort+": Creating fo media for type:"+urltype);
				    media = new XMLMedia(viewer);
			    }
			    // url ends with ".xhtml"
			    else if (url.substring(lastDot).equals("xhtml")) {
			        Log.debug("Prefetch "+urlshort+": Creating fo media for type:"+urltype);
			        media = new XMLMedia(viewer);
			    }
			    // url ends with ".smil" or ".smi"
			    else if (url.substring(lastDot).equals("smi") || url.substring(lastDot).equals("smil")) {
			        Log.debug("Prefetch "+urlshort+": Creating smil media for type:"+urltype);
			        media = new XMLMedia(viewer);
			    }
		    }

			if (media == null && urltype != null) {
			    // "image/gif" "image/jpeg"
			    if (urltype.startsWith("image")) {
	//			    if (((SMILViewer)viewer).getPlayImage() == true) {
				    if (((SMILMLFC)viewer).getPlayImage() == true) {
					    Log.debug("Prefetch "+urlshort+": Creating imagemedia for type:"+urltype);
					    media = new ImageMedia();
						
	//					((Media)media).setMLFCListener(((SMILMLFC)viewer).getMLFCListener());
	//					((Media)media).showControls(true);
					} else {
					    Log.debug("Prefetch "+urlshort+": Creating alt-media for type:"+urltype);
					    media = new TextMedia();
						((TextMedia)media).setText(alt);
					}
			    }

			    // "text/plain"
			    else if (urltype.startsWith("text")) {
					// Always create textproxy - no alt texts
					Log.debug("Prefetch "+urlshort+": Creating textmedia for type:"+urltype);
					media = new TextMedia();
			    }

			    // "audio/*"
			    else if (urltype.startsWith("audio")) {
	//				if (((SMILViewer)viewer).getPlayAudio() == true) {
					if (((SMILMLFC)viewer).getPlayAudio() == true) {
					    Log.debug("Prefetch "+urlshort+": Creating (audio)mediaproxy for type:"+urltype);
					    media = new JMFMedia();
					} else {
					    Log.debug("Prefetch "+urlshort+": Creating alt-media "+alt+" for type:"+urltype);
					    media = new TextMedia();
						((TextMedia)media).setText(alt);
					}
			    }
			    // others... (video)
			    else {
	//				if (((SMILViewer)viewer).getPlayVideo() == true) {
					if (((SMILMLFC)viewer).getPlayVideo() == true) {
					    Log.debug("Prefetch "+urlshort+": Creating audio/video for type:"+urltype);
					    media = new JMFMedia();
					} else {
					    Log.debug("Prefetch "+urlshort+": Creating alt-media for type:"+urltype);
					    media = new TextMedia();
						((TextMedia)media).setText(alt);
					}
			    }
		    }
		} catch(IndexOutOfBoundsException ex) {
			Log.debug("URL '"+url+"' error: "+ex.toString());	
//		} catch (NullPointerException ex) {
//			Log.debug("URL null ptr error: "+ex.toString());
		} catch (MalformedURLException ex) {
			Log.debug("URL type error: "+ex.toString());
		} catch (IOException io) {
			Log.debug("URL type error: "+io.toString());
		}
				
		if (media != null) {
			media.setUrl(u);
            try
            {
    			media.prefetch();
            } catch (Exception e)
            {
                Log.error(e,"Prefetching");
            }
			
			// Add all listeners to this media, now as it has been created.
			media.addMediaListener(mediaListener);
		}
		prefetched = true;		
	}
*/
}

