/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.Log;
import java.awt.*;
import javax.swing.JTextArea;

/**
 *  Declares parameters for media elements. This understands currently the following syntax 
 * to format plain text:
 *  <param name="color" value="#ffffff">
 *  <param name="bgcolor" value="#ffffff">
 *  <param name="font" value="courier">
 *  <param name="size" value="20">
 */
public class SMILParamElementImpl extends SMILElementImpl implements XSMILParamElement, SMILParamElement {

	/**
	* Constructor with owner doc
	*/
	public SMILParamElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
		super(owner, smil, ns, "param");
	}

	/**
	 * name
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getName()  {
		return getAttribute("name");
	}
	
	public void setName(String name)  {
		setAttribute("name", name);
	}

	/**
	 * value
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getValue()  {
		return getAttribute("value");
	}
	public void setValue(String value) throws DOMException  {
		setAttribute("value", value);
	}

	/**
	 *  See the valuetype attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getValueType() {
		return getAttribute("valuetype");
	}
	public void setValueType(String valuetype) throws DOMException {
		setAttribute("valuetype", valuetype);
	}


	/**
	 *  See the  type attribute. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getType() {
		return getAttribute("type");
	}
	public void setType(String type) throws DOMException {
		setAttribute("type", type);
	}

	/**
	 * This is a really stupid method formatting the possible JTextArea under the c component.
	 * Formats the text's font, size or color.
	 * @param c 	Component or its child to be formatted
	 */
	public void formatMedia(Component c) { 

		// Skip for non-Containers, JTextArea is a Container!
		if ((c instanceof Container) == false)
			return;

		// Go through all children, try to find the JTextArea
		Component[] comps = ((Container)c).getComponents();
		int num_comps = ((Container)c).getComponentCount();
		
		for (int i = 0 ; i< num_comps ; i++) {
			// Format possible JTextArea and return
			if (comps[i] instanceof JTextArea) {
				String name = getAttribute("name");
				String value = getAttribute("value");
				Log.debug("Formatting JTEXTAREA "+name);
				Color color;

				if (name.equals("color")) {
					color = ColorConverter.hexaToRgb(value);
					comps[i].setForeground(color);
				} else if (name.equals("bg-color")) {
					color = ColorConverter.hexaToRgb(value);
					comps[i].setBackground(color);
					((JTextArea)comps[i]).setOpaque(true);
				} else if (name.equals("size")) {
					Font font = comps[i].getFont();
					Log.debug("size: "+toInt(value));
					font = new Font(font.getName(), font.getStyle(), toInt(value));
					comps[i].setFont(font);
				} else if (name.equals("family")) {
					// Map to  Dialog, DialogInput, Monospaced, Serif, SansSerif, or Symbol
					Font font = comps[i].getFont();
					if (value.equals("times"))
						value = "Serif";
					if (value.equals("helvetica") || value.equals("arial"))
						value = "SansSerif";
					if (value.equals("symbol"))
						value = "Symbol";
					if (value.equals("courier"))
						value = "Monospaced";
					Log.debug("family: "+value);
					font = new Font(value, font.getStyle(), font.getSize());
					comps[i].setFont(font);
				}
				return;
			}
			// Recurse down the Container children
			if (comps[i] instanceof Container)
				formatMedia(comps[i]);
		}
		// Not found
		return;
	}		

	// Parse String to int
	private int toInt(String size) {
		try {
			return Integer.parseInt(size);
		} catch (NumberFormatException e) {
			Log.error("param: Couldn't parse integer "+size);
			return 10; // Default size
		}
	}

}
