/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import java.util.Vector;

/**
 * SMILAnimationImpl is an abstract parent class, inherited by the animation elements.
 * This class performs the basic animation for all animation elements.
 * It uses AnimatedValue class to animate any attribute in any format.
 * The real animation elements must override parse() and initArray() methods to return
 * a correct type of AnimatedValue. Also, write() may be overridden to achieve
 * custom write back to DOM.
 */
abstract public class SMILAnimationImpl extends ElementTimeImpl implements SMILAnimation, ElementTargetAttributes {

	// The time the animation started
	protected long startTime = 0;

	// Target Element
	protected Element target = null;

	// Attribute Name
	protected String attributeName = null;

	// The active duration, in millisecs
	private int AD = 0;
	
	// The simple duration, in millisecs, -1 means not defined
	private int d = 0;

	// Cumulative animation
	private boolean isCumulative = false;
	
	// Additive animation
	private boolean isAdditive = false;

	// Saved attributes
	private AnimatedValue from, to, by;
	
	// Parsed values attributes in a vector: "xxx;yyy;zzz" -> "xxx","yyy","zzz"
	private Vector values = null;
	
	// values attribute parsed and converted to an array of integer
	private AnimatedValue[] value;
	
	/**
	 * Constructor - set the owner
	 */
	public SMILAnimationImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String tag) {
	    super(owner, smil, ns, tag);
	}

	/** 
	 * Initialize the animation element. Parses from, to, by and values etc.
	 * Also, check the target element and attribute.
	 */
	public void init() {
	
		// Only for X-Smiles - standalones won't run this.
		//  - Start timing... ok, this is a bit early, but so what?
		if (getSMILDoc().getViewer() instanceof SMILMLFC) {
			// If this is parasite, then startup
			if ( ((SMILMLFC)(getSMILDoc().getViewer())).isHost() == false) {
				Log.debug("PARASITE ANIMATION INIT!!");
				startup();
			}
		}	

		// Get the parameters for animation
		attributeName = getAttributeName();
		target = getTargetElement();
		if (getAccumulate() == ACCUMULATE_SUM)
			isCumulative = true;
		else 	
			isCumulative = false;

		if (getAdditive() == ADDITIVE_SUM)
			isAdditive = true;
		else 	
			isAdditive = false;

		// Store attributes that cannot be modified with a script
		try {
			from = parse(getFrom());
		} catch (NumberFormatException e) {
			from = parse(0);
		}
		try {
			to = parse(getTo());
		} catch (NumberFormatException e) {
			to = parse(0);
		}
		try {
			by = parse(getBy());
		} catch (NumberFormatException e) {
			by = parse(0);
		}
		// by-animation forces additive to be "sum"
		if (getFrom() == null && getBy() != null && getTo() == null) {
			isAdditive = true;
		}
		// to-animation forces cumulative to be "none"
		if (getFrom() == null && getBy() == null && getTo() != null) {
			isCumulative = false;
		}

		// Splines not supported
		if (getCalcMode() == CALCMODE_SPLINE) {
			Log.error("Spline Animation not implemented.");
		}
		
		values = new Vector();
		int startOffset = 0, end;
		String valuesString = getValues();

		// Parse values into a vector. If not found, set it to null.
		if (valuesString != null && valuesString.length() > 0) {
			values = new Vector();
			// Go through all values, separated by ';'
			while (startOffset < valuesString.length()) {
				end = valuesString.indexOf(';', startOffset);
				if (end == -1)
					end = valuesString.length();
	
				values.addElement(valuesString.substring(startOffset, end).trim());
				startOffset = end + 1;
			}

			// Convert string vector into an array of integers
			value = initArray(values.size());
			for (int i = 0 ; i < values.size() ; i++) {
				try {
					value[i] = parse((String)(values.elementAt(i)));
				} catch (NumberFormatException n) {
					// Value not parsed - this is illegal value OR discrete interpolation
					value[i] = parse(0);
				}
			
			}
		} else {
			values = null;
		}

		super.init();
	}

	/**
	 * Called when an animation is started. Adds this animation element to the
	 * Animation Scheduler, which schedules all animation elements (to achieve
	 * the "submarine sandwich" model).
	 */
	public synchronized void activate() {
		Log.debug(getId()+" ANIM activate()");

		// Get the parameters for animation 
		// - update in case a script has changed these since init()
		attributeName = getAttributeName();
		target = getTargetElement();

		// No processing for CSS animations
		if (getAttributeType() == ATTRIBUTE_TYPE_CSS) {
			Log.debug("CSS animation not supported, id: "+getId());
			return;
		}
			
		// Start time of the animation (should be relative to the document time)
		startTime = System.currentTimeMillis();

		// Simple Duration - may change from interval to another
		Time sdur = computeSimpleDuration();
		if (sdur.getResolved() == true && sdur.isIndefinite() == false) {
			d = (int)(sdur.getResolvedOffset().intValue());
		} else
			d = -1; // For "indefinite" and unresolved values

		// Add this animation to the scheduler
		getSMILDoc().getAnimationScheduler().addAnimation(startTime, this);

		// Set-up timers or call deactivate()
		super.activate();		

		// Calculate active duration - at this point, deactivate() may already have been called
		if (currentIntervalEnd != null && currentIntervalBegin != null &&
			currentIntervalEnd.getResolved() == true && currentIntervalEnd.isIndefinite() == false) {
			AD = currentIntervalEnd.getResolvedOffset().intValue() - currentIntervalBegin.getResolvedOffset().intValue();
		} else 
			AD = -1; // For "indefinite" and unresolved values
	}

	/**
	 * Called when active duation for an animation element ends.
	 * This will remove/freeze the animation. Removing the animation
	 * actually removes it from the Animation Scheduler, thus animation
	 * is no longer performed.
	 */
	public synchronized void deactivate() {
		Log.debug(getId()+" ANIM deactivate()");

		// freeze or stop animation
		Time dur = getDur();
		String end = getEnd();
		Time repeatDur = getRepeatDur();
		String repeatCount = getRepeatCount();
		String fill = null;
		boolean freeze = true;
		
		// If dur, repeatDur, repeatCount and end are unspecified, then freeze
		// This is semantics of the fill attribute
		fill = getFill();
		// Set the default value for fill - 'auto' (because fillDefault is not part of Basic profile)	
		if (fill == null || fill.length() == 0 || fill.equals("default"))
			fill = "auto";
		
		if (fill.equals("auto"))
			if (dur == null && repeatDur == null && repeatCount == null && end == null)
				freeze = true;
			else
				freeze = false;
		else if (fill.equals("freeze"))
			// Freeze (seq should show this media until next media is visible)
			freeze = true;
		else if (fill.equals("hold"))
			// Freeze until the end of parent Simple Duration (par effect in seq)
			freeze = true;
		else if (fill.equals("transition"))
			// Transition will only hold - not until end of transition in Basic profile
			freeze = true;
		else if (fill.equals("remove"))
			freeze = false;

		if (freeze == true) {
			Log.debug(getId()+" - ANIM FROZEN");
//			media.freeze();
		} else {
			Log.debug(getId()+" - ANIM REMOVED");
			getSMILDoc().getAnimationScheduler().removeAnimation(this);
		}

		super.deactivate();
	}

	/**
	 * This closes this element. The element will not be active or show fill behaviour.
	 * It will be in 'idle' state.
	 * This method is called from the parent container, when its simple dur ends.
	 * The animation element is removed from the Animation Scheduler.
	 */
	public synchronized void closedown() {
		Log.debug(getId()+" ANIM closedown()");

		// The animation may have been frozen - so remove it permanently
		getSMILDoc().getAnimationScheduler().removeAnimation(this);

		super.closedown();
	}	

	/**
	 * Abstract method to be overridden in the implementing class.
	 * This is used to parse animation element's DOM value into 
	 * AnimatedValue, e.g. "from", "to", "by" values.
	 * This method decides the type, i.e. AnimatedColorValue, AnimatedIntegerValue, etc.
	 * @param str		DOM value to be parsed
	 */
	abstract public AnimatedValue parse(String str);
	/**
	 * Abstract method to be overridden in the implementing class.
	 * This is used to parse the DOM value of the target element's attribute 
	 * into AnimatedValue.
	 * This method decides the type, i.e. AnimatedColorValue, AnimatedIntegerValue, etc.
	 * @param scheduler		AnimationScheduler, holding "DOM buffer" and real DOM values
	 */
	abstract public AnimatedValue parse(AnimationScheduler schudeler);
	/**
	 * Abstract method to be overridden in the implementing class.
	 * This is used to parse integer value, mainly zero, into AnimatedValue.
	 * This method decides the type, i.e. AnimatedColorValue, AnimatedIntegerValue, etc.
	 * @param val		Value to be converted
	 */
	abstract public AnimatedValue parse(int val);
	/**
	 * Inits an empty array of AnimatedValues.
	 */
	abstract public AnimatedValue[] initArray(int size);

	/**
	 * This method may be overridden in the implementing class (see AnimateMotionElement).
	 * This is used to write the animated value back the AnimationScheduler's "DOM buffer".
	 *
	 * target element in the "DOM buffer".
	 * @param scheduler		AnimationScheduler, holding "DOM buffer"
	 * @param value			String value to be set to the "DOM buffer"
	 */
	public void write(AnimationScheduler scheduler, String value) {
		scheduler.setAnimAttribute(target, attributeName, value);
	}

	/**
	 * Updates the value of the animated attribute. This method is called
	 * from the Animation Scheduler, when this element gets its turn in
	 * the "submarine sandwich" model.
	 * <p>This method should be optimized - now it may call f() several times.</p>
	 * @param scheduler		AnimationScheduler, scheduler is used to access (read/write) 
	 *						the target's DOM values
	 */
	public void update(AnimationScheduler scheduler) {
		float t;
		AnimatedValue f, fr, fc, ff, F, u;
		boolean isValid = false;
		
		// Parse and indicate that it was a number
		try {
//			u = parse(oldvalue);
			u = parse(scheduler);
			isValid = true;
		} catch (NumberFormatException e) {
			u = parse(0);
			isValid = false;
		}

		// time, in millisecs
		t = (float)(System.currentTimeMillis() - startTime);

		// If values attr. only has one value, attr. is invalid or
		// calcMode is discrete, then just pick the value from values array.
		// TODO: All values which are not additive (ie. numbers/colors) should go here
		if ((values != null && values.size() == 1) || !isValid) {

			// If discrete and values not defined, use "to" value.
			if (values == null)  {
				if (getTo() != null)
					write(scheduler, to.toString());	
				// If "to" not defined, don't do anything
				return;
			}

			// i = floor((t*n)/d)
			int n = values.size();
			int i = (int)Math.floor(((float)(t*n))/d);
			// Check index won't go over, when t == d.
			if (t == d)
				i = n-1;
			// repeat index
			i = i % n;

			// Discrete values
			write(scheduler, (String)values.elementAt(i));	
			return;
		}	

		// Values that can be interpolated:

		// repeated animation function, fr = f( REMAINDER( t, d ) )
		fr = f(t - d* (int)Math.floor((double)t/(double)d),u);
		
		// cumulative animation function
		if (isCumulative == false) {
			// Non-cumulative, fc(t) = fr(t)
			fc = fr;
		} else {
			// Cumulative
			if (t < d) { //repeatIteration == 0) - Check changed, because of timing problems
				// first iteration, f0(t) = f(t)
				fc = fr;
			} else {
				// next iterations, fi(t) = (f(d) * i) + f(t - (i*d))
				//fc = (f(d,u) * repeatIteration) + f(t - (repeatIteration*d),u);
				fc = (f(d,u).mult(repeatIteration)).add( f(t - (repeatIteration*d),u) );
			}
		}

		// frozen animation function (AD == -1 means indefinite/unresolved)
		if (t < AD || AD == -1) {
			// during Active Duration: ff(t) = fc(t)
			ff = fc;
		} else {
			// After Active Duration
			// If AD is not an even multiple of the simple duration d
			double i = AD / d;
			if (i != Math.floor(i)) {
				ff = fc;  // ff(AD) = fc(AD)
			} else {
				if (isCumulative == false) {
					// non-cumulative, ff(AD) = f(d)
					ff = f(d,u);
				} else {
					// cumulative, ff(AD) = f(d) * i
					ff = f(d,u).mult( (int)i );
				}
			}
		}
		
		// animation effect function, F(t,u)
		if (isAdditive == true) {
			// If the animation is additive, F(t,u) = u + ff(t).
			F = u.add( ff );
		} else {
			// If the animation is non-additive, F(t,u) = ff(t). 
			F = ff;
		}
		
		F.clampValue();
		write(scheduler, F.toString());
		return;
	}

	/** 
	 * Simple Animation Function. See the SMIL spec for details about the
	 * behavior of this function.
	 * @param t		Time value
	 * @param u		Underlying DOM value
	 * @return		New calculated value
	 */
	private AnimatedValue f(float t, AnimatedValue u) {
		AnimatedValue f = parse(0);

		// If simple duration not defined or zero
		if (d <= 0) {
			if (values != null)
				return value[0];
			else	
				return from;
		}
	
		// Interpolate either values or from-to-by (?)
		if (values != null) {
			int n = values.size();

			//!! A fix to bug in the spec, sec. 3.4.2
			if (t >= d)
				return value[n-1];

			// i = floor((t*n)/d)
			int i = (int)Math.floor(((float)(t*n))/d);
	
			// Discrete values
			if (getCalcMode() == CALCMODE_DISCRETE) {
				// f(t) = value[i] 
				f = value[i];
			
			// Linear values
			} else if (getCalcMode() == CALCMODE_LINEAR) {
				// BUG: f(t) = value[i] + (value[i+1]-value[i]) * (t-ti)/d.
				// int ti = d / (n-1) * i;
				// f = value[i] + (value[i+1]-value[i])* (t-ti)/d;

				// Fixed version
				int j = (int)Math.floor(((float)(t*(n-1)))/d);
				int tj = j * d / (n-1);
//				f = value[j] + (value[j+1]-value[j])* (t-tj)/(d/(n-1));
				f = value[j].add( value[j+1].interpolate(value[j], (t-tj)/(d/(n-1))));

			// Paced values
			} else if (getCalcMode() == CALCMODE_PACED) {
				// BUG: T(i) = (D(i)/D(n)) * d, for integers i with 0<=i<=n. 
				// f(t) = value[i] + (value[i+1]-value[i]) * (t-T(i))/d 
				// float T = D(i)/D(n) * d;
				// f = value[i] + (value[i+1]-value[i]) * (t-T)/d;

				// Fixed version - first check exceptional case n=1
				float startDist = 0, distance = 0, totalDistance = 0;
				int start=0, end=0;
				// Calculate the total distance
				for (int x = 0 ; x < value.length-1 ; x++)
					totalDistance += (float)(value[x].distance(value[x+1]));
				
				// See if the time t is within (startDist/totalDist*d) and (endDist/totalDist*d)
				for (int x = 0 ; x < value.length-1 ; x++) {
					distance = (float)(value[x].distance(value[x+1]));
					start = (int)((startDist/totalDistance)*(float)d);
					end = (int)(((startDist+distance)/totalDistance)*(float)d);
					if (t >= start && t < end) {
						// Temporal interval found - interpolate in it
					//	f = (float)value[x] + (float)(value[x+1]-value[x])* 
					//			((float)(t-start))/((float)(end-start));

						f = value[x].add( value[x+1].interpolate(value[x],
								((float)(t-start))/((float)(end-start))) );
						return f;
					}
					startDist += distance;
				}
				Log.error("Internal Paced Animation error: No time found for "+t+" ("+start+"-"+end+")");
				f = parse(0);
			} else if (getCalcMode() == CALCMODE_SPLINE) {
				f = parse(0);
			} 

		} else {
			// simple from-to-by animation
			
			// to-animation overrides by-animation
			if (getTo() != null) {
				// to-animations
				if (getFrom() != null) {
					// from-to-animation
				//	f = (from + (to-from)*t/d);
					f = from.add( to.interpolate(from,(float)t/(float)d));
				} else {
					// to-animation
					// f(t,u) = (u * (d-t)/d) + (vt * t/d)
					// the same as: f = (u + (vt-u)*t/d);
					//f = (u * (d-t)/d) + (to * t/d);
					//f = (u + (to-u)*t/d);
					f = u.add( to.interpolate(u,t/d));
					// Special case: if calcMode == discrete, then always to
					if (getCalcMode() == CALCMODE_DISCRETE)
						f = to;
				}
			} else {
				// by-animations
				if (getFrom() != null) {
					// from-by-animation
				//	f = (from + (by-0)*t/d);
					f = from.add (by.interpolate(parse(0),t/d));
				} else {
					// by-animation - u is always be added to this, because additive is
					// forced to be "sum"
					// f = u + ((by)*t/d);
				//	f = ((by-0)*t/d);
					f = by.interpolate(parse(0), t/d);
				}
			}
		}
		return f;
	}

	/**
	 * Get the attributeType value from the DOM.
	 * @return attributeType, ATTRIBUTE_TYPE_AUTO, ATTRIBUTE_TYPE_CSS or ATTRIBUTE_TYPE_XML.
	 */
	public short getAttributeType() {
		String type = getAttribute("attributeType");
		if (type.equals("auto"))
			return ATTRIBUTE_TYPE_AUTO;
		if (type.equals("CSS"))
			return ATTRIBUTE_TYPE_CSS;
		if (type.equals("XML"))
			return ATTRIBUTE_TYPE_XML;
		
		// Default value
		return ATTRIBUTE_TYPE_AUTO;	
	}
	public void setAttributeType(short s) {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Get the targetElement value from the DOM.
	 * @return targetElement
	 */
	public Element getTargetElement() {
		// Get the target element
		Element target = smilDoc.searchElementWithId(getAttribute("targetElement"));
		if (target == null) {
			target = (Element)this.getParentNode();
		}
		return target;
	}

	public void setTargetElement(Element s) {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 * Get the attributeName value from the DOM.
	 * @return attributeName
	 */
	public String getAttributeName() {
		return getAttribute("attributeName");
	}
	public void setAttributeName(String s) {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A code representing the value of the  additive attribute, as defined 
     * above. Default value is <code>ADDITIVE_REPLACE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getAdditive() {
		if (getAttribute("additive").equals("sum") == true)
			return ADDITIVE_SUM;

		return ADDITIVE_REPLACE;
	}
    public void setAdditive(short additive) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A code representing the value of the  accumulate attribute, as defined 
     * above. Default value is <code>ACCUMULATE_NONE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getAccumulate() {
		if (getAttribute("accumulate").equals("sum") == true)
			return ACCUMULATE_SUM;

		return ACCUMULATE_NONE;
	}
    public void setAccumulate(short accumulate) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    // calcModeTypes
    public static final short CALCMODE_DISCRETE         = 0;
    public static final short CALCMODE_LINEAR           = 1;
    public static final short CALCMODE_PACED            = 2;
    public static final short CALCMODE_SPLINE           = 3;

    /**
     *  A code representing the value of the  calcMode attribute, as defined 
     * above. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getCalcMode() {
		String mode = getAttribute("calcMode");

		if (mode.equals("discrete"))
			return CALCMODE_DISCRETE;
		if (mode.equals("linear"))
			return CALCMODE_LINEAR;
		if (mode.equals("paced"))
			return CALCMODE_PACED;
		if (mode.equals("spline"))
			return CALCMODE_SPLINE;

		// Default value
		return CALCMODE_LINEAR;	
	}
    public void setCalcMode(short calcMode) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A <code>DOMString</code> representing the value of the  keySplines 
     * attribute.  Need an interface a point (x1,y1,x2,y2) 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getKeySplines() {
		return null;
	}
    public void setKeySplines(String keySplines) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A list of the time value of the  keyTimes attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public TimeList getKeyTimes() {
		return null;
	}
    public void setKeyTimes(TimeList keyTimes) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A <code>DOMString</code> representing the value of the  values 
     * attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getValues() {
		return getAttribute("values");
	}
    public void setValues(String values) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A <code>DOMString</code> representing the value of the  from attribute.
     *  
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFrom() {
		String from = getAttribute("from");
		if (from.length() == 0)
			return null;
		return from;
	}
    public void setFrom(String from) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A <code>DOMString</code> representing the value of the  to attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTo() {
		String to = getAttribute("to");
		if (to.length() == 0)
			return null;
		return to;
	}
    public void setTo(String to) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

    /**
     *  A <code>DOMString</code> representing the value of the  by attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBy() {
		String by = getAttribute("by");
		if (by.length() == 0)
			return null;
		return by;
	}
    public void setBy(String by) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

	/**
	 *  A code representing the value of the  fill attribute, as defined 
	 * above. Default value is <code>FILL_REMOVE</code> . 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getFill() {
	   return getAttribute("fill");
	}
	public void setFill(String fill) throws DOMException {
		throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "SMIL attribute cannot be modified.");
	}

}

