/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.LinkHandler;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.MediaHandler;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;
import fi.hut.tml.xsmiles.mlfc.smil.basic.SMILDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.basic.TimeChildList;
import fi.hut.tml.xsmiles.mlfc.smil.basic.ElementBasicTimeImpl;

import org.w3c.dom.smil20.*;
import org.w3c.dom.Node;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JComponent;

import fi.hut.tml.xsmiles.Log;

/**
 *  Implements links in Swing.
 */
public class SwingLinkHandler extends SwingMediaHandler implements LinkHandler, MouseListener {

	public Component linkComp;
	private SMILDocumentImpl smilDoc = null;
	public Viewer viewer = null;
	public String linkTitle = null;
	
	public SwingLinkHandler() {
	}
	
	
	public void setTitle(String title) {
		if (title == null || title.length() == 0)
			linkTitle = null;
		else
			linkTitle = title;
	}
		
	public void play() {

		if (container == null) {
			Log.debug("No drawing area.container for link "+url);
			return;
		}
		linkComp = new JInvisibleComponent();
		linkComp.addMouseListener(this);
		// Add this link on top of everything
		container.add(linkComp, 0);
		linkComp.setBounds(left, top, width, height);
		linkComp.setEnabled(true);
		linkComp.setVisible(true);
		linkComp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

	}
	public void pause() {
	}
	public void stop() {
		if (container != null && linkComp != null) {
			linkComp.setVisible(false);
			container.remove(linkComp);
		}
	}

	public void close() {
		linkComp = null;
		viewer = null;
		smilDoc = null;
	}

	/**
	 * If link entered, display url or title
	 */
	public void mouseEntered(MouseEvent e) {
		if (linkTitle == null)
			viewer.displayStatusText(url);
		else
			viewer.displayStatusText(linkTitle);	
	}
	
	public void mouseExited(MouseEvent e) {
		viewer.displayStatusText("");
	}

	public void mousePressed(MouseEvent e) {
	}
	public void mouseReleased(MouseEvent e) {
	}

    public void mouseClicked(MouseEvent e) {
		if (mediaListener != null)
			mediaListener.mouseClicked(e);
	}

	public void setViewer(Viewer v) {
		viewer = v;
		smilDoc = (SMILDocumentImpl)(viewer.getSMILDoc());
	}

}

