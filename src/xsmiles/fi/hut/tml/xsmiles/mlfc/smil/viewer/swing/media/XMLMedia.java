/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.media;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Image;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JPanel;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;
import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;


/**
 * This is the implementation of image media.
 */
public class XMLMedia implements Media {

  // Hashtable holding loaded images - this is a proxy
  private Hashtable loadedImages;
  
  // Label and ImageIcon for current image
  JPanel xmlComp = null;

  // Container for media (DrawingArea)
  Container container = null;

  // MediaListener - called after prefetch and endOfMedia.
  MediaListener mediaListener = null;

  // Location and coords for the media
  int x=0, y=0, width=0, height=0;

  // SMILMLFC
  SMILMLFC viewer;

  Image pic = null;

  public XMLMedia(Viewer v) {
    viewer=(SMILMLFC)v;
    // Create the components		
    xmlComp = new JPanel();
    // Set it to transparent, to prevent it from drawing
    xmlComp.setOpaque(false);

    // Create the proxy hashtable
    loadedImages = new Hashtable();			
  }

  /**
   * Checks if this media is static or continuous.
   * @return true	if media is static.
   */
  public boolean isStatic() {
    return true;
  }

  URL url = null;

  public void setUrl(URL url) {
    this.url = url;
  }

  public void prefetch() {
    // Create the container and the external MLFC
    // Should this be just container?
    xmlComp.setLayout(new BorderLayout());
    xmlComp.setVisible(false);

    //			container.add(p,0);

    // create an XLink object
    XLink link = new XLink(url);
    // call request browser to display a secondary document in 
    // container xmlComp
    viewer.getMLFCListener().displayDocumentInContainer(link,xmlComp);

    // Convert from heavy to light..
    //		xmlPanel = new JPanel();
    //		svgPanel.setLayout(new BorderLayout());
    //		svgPanel.add(svgComp);
    //		svgPanel.setPreferredSize(size);
    //		svgPanel.setSize(size);
    //		svgPanel.setVisible(false);
  }
  
  public void setContainer(Container container) {
	  if (this.container != container) {

	  	// Change container
	  	if (xmlComp != null && xmlComp.isVisible() == true) {
	  		xmlComp.setVisible(false);
	  		this.container.remove(xmlComp);
	  		container.add(xmlComp, 0);
	  		xmlComp.setVisible(true);
	  	}

	  	this.container = container;
	  }
  }

  public void play() {
    if (container != null) {
      container.add(xmlComp, 0);
      xmlComp.setLocation(x, y);
      xmlComp.setSize(width, height);
      xmlComp.setVisible(true);
    } else
	  Log.error("Container not set for media "+url.toString());
  }

  public void pause() {
  }

  public void stop() {
    //Log.debug("PROXY: stop()");
    //		timer.stop();
		
    // Second event, this is the stop event.
    //	gfxComponent.setVisible(false);
    //		container.showComponents(false);
    //		container.repaint();
    //		container.setVisible(false);
    xmlComp.setVisible(false);
    if (container != null)
      container.remove(xmlComp);

  }

  /**
   * This moves the time position in media. Not effective for this media.
   * @param millisecs		Time in millisecs
   */
  public void setMediaTime(int millisecs) {
  }

  public void setBounds(int x, int y, int width, int height) {
    this.x = x;
    this.y = y;

    this.width = width;
    this.height = height;	

    // If the media is shown on the screen, move it immediately
    if (xmlComp.isVisible() == true) {
      // Use the given coordinates
      xmlComp.setLocation(x, y);
      xmlComp.setSize(width, height);
    }			
  }

  public void close() {
  	stop();
  
    // Flush all images
    Enumeration i = loadedImages.elements();
    while(i.hasMoreElements()) {
      ((Image)i.nextElement()).flush();
    }
		 
    loadedImages.clear();
    loadedImages = null;
	
	mediaListener = null;
  }
  
  /** 
   * Get the real width of the media.
   */
  public int getOriginalWidth() {
	return -1;	
  }

  /** 
   * Get the real height of the media.
   */
  public int getOriginalHeight() {
	return -1;	
  }

  /**
   * Set the sound volume for media. Does nothing for this media player.
   * @param percentage	Not used.
   */
  public void setSoundVolume(int percentage) {
  }

  /**
   * Get the duration of media. Only applicable for continuous media (audio, video).
   * @return The duration of media in millisecs. 
   * 			-1 means indefinite (infinite streamed media or unknown duration).
   */
  public int getOriginalDuration() {
  	return -1;
  }
  
  public void addMediaListener(MediaListener listener) {
  }

  /**
   * Requests the media player to display a control panel for media.
   * For audio and video, these can be a volume/play/stop controls,
   * for images, these can be zoom controls.
   * The controls are GUI dependent, generated through ComponentFactory.
   * @param visible	true=Display controls, false=don't display controls.
   */
  public void showControls(boolean visible) {
  }
 

  /**
   * All traffic to the browser, such as openLocation, etc goes through this listener.
   * If no listener supplied media players should still function with some basic level. 
   * 
   * @param listener The MLFCListener supplied by the browser
   * @see MLFCListener
   */
  public void setMLFCListener(MLFCListener listener) { 
  }

	
}
