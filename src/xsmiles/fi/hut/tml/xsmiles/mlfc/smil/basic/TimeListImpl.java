/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.smil20.*;
import java.util.Vector;
import fi.hut.tml.xsmiles.Log;

/**
 *  The <code>TimeList</code> interface provides the abstraction of an ordered 
 * collection of times, without defining or constraining how this collection 
 * is implemented.
 * <p> The items in the <code>TimeList</code> are accessible via an integral 
 * index, starting from 0. 
 */
public class TimeListImpl implements TimeList {

	// List of times
	private Vector timeList = null;

	// Reference to SMILDocumentImpl, for syncbase clock values
	private SMILDocumentImpl sDoc = null;

	// Reference to element, which will receive time changes from syncbase element
	private ElementBasicTimeImpl element = null;

	// BeginList or EndList - true for beginlist
	boolean beginList = false;

	/**
	 * Initializes this object.
	 */
	public TimeListImpl() {
		timeList = new Vector();
	}

	/**
	 * Parses a time list string into Time objects. This requires the SMILDocumentImpl
	 * for syncbase clock values. Those values link themselves to SMILElements, which
	 * are found with SMILDocumentImpl.searchElementWithIdInBody().
	 *
	 * @param timeListString  List of times '2s;3s'
	 * @param doc			  SMILDocumentImpl, required for syncbase clock values.
	 * @param e				  Element, which will receive time changes from syncbase element
	 * @param beginList		  true for beginlists, false for endlists
	 */
	public TimeListImpl(String timeListString, SMILDocumentImpl doc, ElementBasicTimeImpl e,
						boolean beginList) {
		sDoc = doc;
		element = e;
		timeList = new Vector();
		this.beginList = beginList;
		init(timeListString);
	}

	/**
	 * Parses a time list string into Time objects.
	 * @param listString  List of times '2s;3s;id.event;id.begin'
	 */
	private void init(String listString) {
		int startOffset = 0, end;

		// Don't process blank strings
		if (listString == null || listString.length() == 0)
			return;

		// Go through all times, separated by ';'
		while (startOffset < listString.length()) {
			end = listString.indexOf(';', startOffset);
			if (end == -1)
				end = listString.length();

			// If smilDoc is null, this will result in unresolved time.
			// Parse Time and set up listeners (event and new interval listeners)
			add(new TimeImpl(listString.substring(startOffset, end), sDoc, 
													element, beginList));
			// add event listeners
			// add new instance listeners (time dependencies)
			
			startOffset = end + 1;
		}
	}

	/**
	 * Add a time to TimeList. The times will be kept in order, 
	 * unresolved > indefinite > resolved time.
	 * @param time	Time value to be added
	 */
	public void add(Time time) {
		Time lTime;
		int i = 0;
		
		while (i < getLength()) {
			lTime = item(i);
			if (lTime.isGreaterThan(time)) {
				timeList.insertElementAt(time, i);
				return;
			}
			i++;
		}
		timeList.addElement(time);
		return;
	}

    /**
     *  Returns the <code>index</code> the item in the collection. If 
     * <code>index</code> is greater than or equal to the number of times in 
     * the list, this returns <code>null</code> .
     * @param index  Index into the collection.
     * @return  The time at the <code>index</code> th position in the 
     *   <code>TimeList</code> , or <code>null</code> if that is not a valid 
     *   index.
     */
    public Time item(int index) {
		Time t = null;
		
		try {
			t = (Time)timeList.elementAt(index);
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
		
		return t;
	}

    /**
     *  The number of times in the list. The range of valid child time indices 
     * is 0 to <code>length-1</code> inclusive. 
     */
	public int getLength() {
		return timeList.size();
	}
	
	/**
	 * Get the next time greater than time.
	 * @param time		Time to compare against
	 * @return			Index to time item, -1 if not found
	 */
	public Time getTimeGreaterThan(Time time) {
		Time lTime;
		int i = 0;
		
		while (i < getLength()) {
			lTime = item(i);
			if (lTime.isGreaterThan(time)) {
				return lTime;
			}
			i++;
		}
		return null;
	}

	/**
	 * If this list has time instances - then the parameter was defined.
	 */
	public boolean isDefined() {
		if (getLength() > 0)
			return true;
		
		return false;
	}
	
	/**
	 * Reset this instance time list - all event condition times are cleared from the list.
	 * DOM created times are of type SMIL_TIME_EVENT_BASED.
	 */
	public void reset()  {
		TimeImpl time;
		int i = 0;
		
		while (i < getLength()) {
			time = (TimeImpl)item(i);
			// Allow changes
//			time.unlock();
			// Remove event condition based times
			if (time.getTimeType() == Time.SMIL_TIME_EVENT_BASED ||
				time.getTimeType() == Time.SMIL_TIME_REPEAT ||
				time.getTimeType() == Time.SMIL_TIME_ACCESSKEY) {
				timeList.removeElementAt(i);
				i--;
			}
			i++;
		}
		return;
	}

	// Debugging
	public void print() {
		Time lTime;
		int i = 0;
		
		while (i < getLength()) {
			lTime = item(i);
			System.out.println("Time "+i+" "+lTime.getString());
			i++;
		}
		return;
	}
	
	/**
	 * Create a Time Instance List from this list.
	 * Event and syncbase listeners are still hold in the original TimeList.
	 * Time Instance List will only hold offset values (resolved or unresolved).
	 */
	public TimeListImpl getTimeInstanceList()  {

		TimeListImpl timeList = new TimeListImpl(null, sDoc, element, beginList);

		TimeImpl time, instance;
		int i = 0;
		
		while (i < getLength()) {
			time = (TimeImpl)item(i);
			
			instance = null;
			// Copy time to time instances
			if (time.getTimeType() == Time.SMIL_TIME_INDEFINITE ||
					time.getTimeType() == Time.SMIL_TIME_OFFSET ||
					time.getTimeType() == Time.SMIL_TIME_WALLCLOCK)  {
//					time.getTimeType() == Time.SMIL_TIME_SYNC_BASED)  {

				if (time.isIndefinite() == true)  {
					// indefinite will only be copied for end lists
					if (this.beginList == false)
						instance = new TimeImpl();
				} else  {
					instance = new TimeImpl((int)time.getResolvedOffset().intValue());
				}

				if (instance != null)  {
//					Log.debug("Instance "+instance.getString());
					timeList.add(instance);
				}
			}
			
			i++;
		}
		return timeList;
	
	}
	
	////////////////// Listeners to handle dependencies and events //////////
	
	/**
	 * Time in this instance time list has changed (dependency time's base time was changed).
	 * This will notify the dependency element.
	 */
	public void timeChanged(Time time)  {
		//element.instanceListChanged(int begin/end, time);
	}
	

}

