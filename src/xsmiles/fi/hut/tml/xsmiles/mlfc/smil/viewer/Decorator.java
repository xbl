/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer;

import java.awt.Container;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 *  
 */
public interface Decorator {

	public Container createRootLayout();
	// The MLFC has to take care of the scroll bars...
	public Container createScrollPanel(Container rootLayout);
	public void createSecondaryBorders(Container rootLayout, Container scrollPanel);

	public void addToContainer(Container scrollPanel, Container container);

	/**
	 * Returns a new MediaHandler for SMIL core logic.
	 */
	public MediaHandler getNewMediaHandler();
	/**
	 * Returns a new BrushHandler for SMIL core logic.
	 */
	public BrushHandler getNewBrushHandler(Viewer v);

	/**
	 * Returns a new LinkHandler for SMIL core logic.
	 */
	public LinkHandler getNewLinkHandler();
	/**
	 * Returns a new DrawingArea for SMIL core logic.
	 * 
	 * @param type
	 *                  ROOTLAYOUT for the broswer container, TOPLAYOUT for a new
	 *                  frame.
	 * @param block
	 *                  CSS: used to create a JBlockPanel (true) instead of JPanel
	 *                  (false)
	 */
	public DrawingArea getNewDrawingArea(int type, boolean block, Container rootLayout, boolean layoutModel);
	/**
	 * Returns a new ForeignHandler for SMIL core logic.
	 */
	public MediaHandler getNewForeignHandler(Element e);

	/**
	 * Check if JMF class is available. At the same time, set the hint...
	 * 
	 * @return true if JMF is available, false otherwise
	 */
	public boolean isJMFAvailable(MLFCListener listener);
}
