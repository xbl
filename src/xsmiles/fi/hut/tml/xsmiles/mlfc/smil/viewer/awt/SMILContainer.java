/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt;

import java.awt.Container;
import java.net.URL;

/**
 * This hides the SMIL viewer implementation, implementing it as a simple
 * Component, which can be given the source (URL or filename) for the 
 * SMIL presentation.
 * <p>The presentation can be controlled by calling methods start() and stop().
 * <p>Proper memory freeing is not guaranteed.
 */
public class SMILContainer extends Container {

	private SMILViewer viewer = null;

	/**
	 * Empty container - no URL or filename given.
	 */
	public SMILContainer() {
		viewer = null;
	}

	/**
	 * Retrieves SMIL presentation from the given URL.
	 * @param url		URL for SMIL presentation
	 */
	public SMILContainer(URL url) {
		viewer = new SMILViewer(url);
	}

	/**
	 * Retrieves SMIL presentation from the given file.
	 * @param file		filename for SMIL presentation
	 */
	public SMILContainer(String file) {
		viewer = new SMILViewer(file);
	}

	/**
	 * Starts the retrieved SMIL presentation.
	 */
	public void start() {
		if (viewer != null)
			viewer.start();
	}

	/**
	 * Stops the retrieved SMIL presentation.
	 */
	public void stop() {
		if (viewer != null)
			viewer.stop();
	}

}

