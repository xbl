/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.DrawingArea;

import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;
import fi.hut.tml.xsmiles.Log;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.border.Border;
import javax.swing.BorderFactory;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.JBlockPanel;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILMLFCDrawingArea;


import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.SwingDrawingArea;

/**
 *  Drawing area for SMILMLFC. This is the container, which will hold media.
 * This is a exact copy of the Swing version.
 */
public class SMILMLFCDrawingArea extends SwingDrawingArea implements DrawingArea {

	/**
	 * Creates a new drawing area of type ROOTLAYOUT, TOPLAYOUT or REGION.
	 * @param type		type of the area
	 * @param css		CSS layout=true, basic=false
	 * @param block		CSS block type of container (true=JBlockPanel) or normal (false=JPanel)
	 */
	public SMILMLFCDrawingArea(int type, boolean css, boolean block) {
		super(type);

		// If css layout model is on, convert drawingarea to flow.
		if (css == true) {
			// overwrite the default panels, if this is a block panel
			if (block == true)	
				createCSSPanel(type);
			// set CSS layout
			SMILCSSFlowLayout fl = new SMILCSSFlowLayout();
			contentContainer.setLayout(fl);
		}
	}
	
	/**
	 * Creates a new drawing area using container c. Used for root-layout.
	 * @param c		Container
	 * @param css	CSS layout=true, basic=false
	 */
	public SMILMLFCDrawingArea(Container c, boolean css) {
		super(c);

		// If css layout model is on, convert rootlayout to flow. Not needed because a container
		// is dynamically created for root-layout.
//		if (css == true) {
			//FlowLayout fl = new FlowLayout();
			//fl.setHgap(0);
			//fl.setVgap(0);
			//fl.setAlignment(FlowLayout.LEFT);
			//c.setLayout(fl);
//		}
	}

	/**
	 * Creates a new drawing area of type ROOTLAYOUT, TOPLAYOUT or REGION.
	 * This will create a special JBlockPanel for CSS block container.
	 * The JBlockPanel is recognized by the SMILCSSFlowLayout.
	 */
	public void createCSSPanel(int type) {
		if (type == DrawingArea.ROOTLAYOUT) {
			Log.error("ROOT LAYOUT INCORRECT CREATION");
		} else if (type == DrawingArea.TOPLAYOUT) {
			JFrame frame = new JFrame("TOPLAYOUT?");
			container = frame;	
			contentContainer = frame.getContentPane();	

			// frame.getContentPane().setLayout(null);
			frame.getContentPane().setLayout(null);
			// Default not visible
			frame.setVisible(false);
		} else if (type == DrawingArea.REGION) {
			JPanel panel = new JBlockPanel();	// Create a block panel!
//			Container panel = new Container();
			// Use absolute coords
			panel.setLayout(null);
			container = panel;
			contentContainer = panel;	
			// Default transparent background color		
			panel.setOpaque(false);
			// Default not visible
			panel.setVisible(false);
			
			// This is to get a region with scroll bars
		} else if (type == DrawingArea.REGIONSCROLL) {
			// Add the components into this one
			JPanel panel = new JBlockPanel();	// Create a block panel!
			// This is added to the rootLayout and has a size
			JScrollPane scroll = new JScrollPane(panel);
			// Use layout that works with ScrollPane
			panel.setLayout(new FlowLayout(0,0,0));
			// Get rid of borders coming thanks to FlowLayout
			Border empty = BorderFactory.createEmptyBorder();
			panel.setBorder(empty);
			// Get rid of borders coming thanks to ScrollLayout
			scroll.setBorder(empty);
//			empty = BorderFactory.createRaisedBevelBorder();
//			scroll.setViewportBorder(empty);
//			scroll.getViewport().setBorder(empty);
//			scroll.getViewport().setLayout(null);
			
			container = scroll;
			contentContainer = panel;	
			// Default transparent background color		
			panel.setOpaque(false);
			scroll.setOpaque(false);
			scroll.getViewport().setOpaque(false);

			panel.setVisible(true);
			// Default not visible
			scroll.setVisible(false);
		}
	}
        
        public boolean setCSSStretch(String awidth, String aheight, Element origElem)
        {
                    // Set the block container without width/height to be stretched during doLayout().
                    if (this.getContainer() instanceof JBlockPanel) {
                            Log.debug("JBLOCKPANEL CRAETED!!! w:"+awidth+" h:"+aheight);
                            JBlockPanel jbp = (JBlockPanel)this.getContainer();
                            if (awidth == null) {
                                    jbp.setStretchWidth(true);
                                    Log.debug("JBLOCKPANEL WIDTH STRECT!!!");
                            }
                            if (aheight== null) {
                                    jbp.setStretchHeight(true);
                                    Log.debug("JBLOCKPANEL HEIGHT STRECT!!!");
                            }
                    }	
                    // CSS FIXTHIS: ADD container - element pair to lookup list
                    fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc.SMILCSSFlowLayout.componentFlowMap.put(
                                                    this.getContainer(), origElem);
            return true;
        }

}

