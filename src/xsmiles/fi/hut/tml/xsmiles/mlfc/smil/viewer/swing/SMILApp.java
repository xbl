/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

// Doesn't require to be in this package
package fi.hut.tml.xsmiles.mlfc.smil.viewer.swing;

// Import the SMILComponent
import fi.hut.tml.xsmiles.mlfc.smil.viewer.swing.JSMILPlayer;

import javax.swing.JFrame;
import java.awt.BorderLayout;

/**
 * This is a simple standalone application to show how to use the SMIL component JSMILPlayer. 
 * This app will play the presentation given as a command line parameter.
 * <p>
 * After 10 seconds, the presentation will be closed.
 * <p>
 * To run this viewer as a standalone application, classpath should include
 * xerces.jar and xml-apis.jar 
 */
public class SMILApp {

	/** 
	 * Main to play a SMIL presentation.
	 */
	public static void main(String[] args) {

		String filename = null;

		// Get the filename
		if (args.length > 0) {
			filename = args[0];
		} else
			filename = "d:/source/xsmiles/demo/smil/css/css.smil";

		System.out.println("SMILApp playing '"+filename+"' for 10 seconds");

		// Create the SMIL player
		JSMILPlayer smil = new JSMILPlayer();

		// Initialize with the filename (could also be URL)
		if (smil.init(filename) == false)
			System.out.println("ERROR initializing "+filename);
		
		// Create a frame and add the SMIL component to it
		JFrame frame = new JFrame("Simple SMIL player");
		frame.setSize(600,400);
		frame.getContentPane().add(smil, BorderLayout.CENTER);
		frame.show();
		
		// Start the SMIL presentation
		smil.start();
		
		// Wait for 10 seconds
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
		}

		System.out.println("SMILApp stopping.");
	
		// Stop the SMIL presentation
		smil.stop();

		// Remove the SMIL component from the frame
		frame.removeAll();
		frame.hide();
		
		// Free memory
		smil.destroy();

		// Clear variables
		frame = null;
		smil = null;
		
		// Exit
		System.exit(0);	
	}
}