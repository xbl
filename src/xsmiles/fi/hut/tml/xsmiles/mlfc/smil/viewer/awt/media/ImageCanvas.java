/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.awt.media;

import java.awt.*;
import java.awt.image.*;

/**
 * Simple class to draw images. This is required for AWT (?), in Swing JIcon 
 * can be used...
 * REDUNDANT!
 */
public class ImageCanvas extends Canvas {

	Image image = null;

	public ImageCanvas() {
	}

	public void setImage(Image i) {
		image = i;
	}

	public void paint(Graphics  g) {

		// Draw image
		if (image != null)
			g.drawImage(image, 0, 0, this);

		return;
	}  
} 

