/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.viewer.smilmlfc;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

/**
 * Dummy class to tell the SMILCSSFlowLayout that a line break should be before
 * and after this JPanel, instead of just adding in on the same line (in flow).
 */
public class JBlockPanel extends JPanel {

	public JBlockPanel() {
		super();
	}

	public void paint(Graphics g) {
		// for debugging - draws the borders of the component.
	   // g.setColor(Color.blue);
	   // g.drawRect(0, 0, getWidth()-1, getHeight()-1);
		super.paint(g);
	}

	private boolean stretchWidth = false;
	private boolean stretchHeight = false;

	public boolean getStretchWidth() {
		return stretchWidth;
	}
	public void setStretchWidth(boolean val) {
		stretchWidth = val;
	}

	public boolean getStretchHeight() {
		return stretchHeight;
	}
	public void setStretchHeight(boolean val) {
		stretchHeight = val;
	}

}
