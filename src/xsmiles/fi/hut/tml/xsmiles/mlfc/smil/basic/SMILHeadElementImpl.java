/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;

import org.w3c.dom.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 *  Declares smil head element.
 */
public class SMILHeadElementImpl extends SMILElementImpl
									implements XSMILHeadElement {

  private SMILLayoutElement layout = null;

  /**
   * Constructor with owner doc
   */
  public SMILHeadElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
    super(owner, smil, ns, "head");
  }

  /**
   * Initialize the head element and its children. Calls init() only for the first layout element.
   * TODO: Doesn't work with <switch>!
   * Note: This method won't call super.init() and initialize children, because
   * only one layout should be initialized.
   */
  public void init() {
  	Node child = null;
  	SMILLayoutElement layoutElement = null;
	boolean result;
	String type;

	// Initialize meta elements - to get the title before layout initialization.
	NodeList childList = getChildNodes();
	// Go through meta elements
	for (int i=0 ; i < childList.getLength() ; i++) {
		child = childList.item(i);
		if (child instanceof SMILMetaElementImpl)
			((SMILMetaElementImpl)child).init();
	}

  	// Get children of <head> - should also check under a switch statement?
  	childList = getChildNodes();
  	// Go through layouts and init the _first_ with acceptable attributes.
  	for (int i=0 ; i < childList.getLength() ; i++) {
		child = childList.item(i);
  		if (child instanceof SMILLayoutElement) {
  			layoutElement = (SMILLayoutElement)child;
  			result = AttributeHandler.evaluateSystemValues(layoutElement, getSMILDoc().getViewer(), true);
  			// This also checks for mime type="text/smil-basic-layout"
  			type = layoutElement.getType();
  			// Not found - use the default value "text/smil-basic-layout"
  			if (type == null || type.length() == 0)
  				type = "text/smil-basic-layout";

  			if (type.equalsIgnoreCase("text/smil-basic-layout") == false &&
				type.equalsIgnoreCase("text/css") == false)
  				result = false;

  			if (result == true && layout == null) {
  				Log.debug("Layout '"+layoutElement.getId()+"' init!");
  				layout = layoutElement;
				layout.init();
  			}
		// If switch and layout not yet found
  		} else if (child instanceof SMILSwitchElement && layout == null) {
			((SMILSwitchElementImpl)child).initHead(this);
  		} else {   // Init all except meta elements.
	  		if (child instanceof XSmilesElementImpl && !(child instanceof SMILMetaElementImpl))
	  			((XSmilesElementImpl)child).init();
  		}	
  	}

	// Layout was not found - use default layout
	if (layout == null) {
	  	Log.info("<layout> element not found - using the default layout.");
	  	// Create a layout element using dummy namespace
	  	// This layout will not have any root-layout or regions - defaults are used.
	  	layout = new SMILLayoutElementImpl(getOwnerDoc(), getSMILDoc(), "ns");
		layout.init();
	}
  	return;	
  }

	/**
	 * Get the first layout element under head element.
	 * @return 	The first layout with true systemAttributes. If not found, returns default Layout
	 */
	public SMILLayoutElement getLayout() {
		return layout;
	}

	/**
	 * Set the layout for this presentation. SMILSwitchElementImpl calls this to set the layout.
	 * @param 	The layout for this presentation.
	 */
	public void setLayout(SMILLayoutElement l) {
		layout = l;
	}

}