/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.smil20.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.Viewer;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.smil.extension.SMILCustomAttributesElementImpl;

/**
 * AttributeHandler parses and returns a value of
 * one attribute.
 */
public class AttributeHandler {

    protected AttributeHandler() {
    }
    /*
    public static int getAttributeAsInt(NamedNodeMap attrs, String attrName) {
        Node n = attrs.getNamedItem(attrName);
        int attr;
        
        if (n != null) {
            try {
                return Integer.parseInt(n.getNodeValue());
            } catch (NumberFormatException e) {
                //setDefaultValue(var);
                return 0;
            }
        }
        return 0;
    }
    
    public static String getAttributeAsString(NamedNodeMap attrs, String attrName) {
        
        Node n = attrs.getNamedItem(attrName);
        
        if (n != null) {
            return n.getNodeValue();
        }
        return null;
    }
    
    public static boolean getAttributeAsBoolean(NamedNodeMap attrs, String attrName) {
        
        Node n = attrs.getNamedItem(attrName);
        String s;
        
		if (n != null) {
             s = n.getNodeValue();
             return Boolean.valueOf(s).booleanValue();
        }
        return false;
    }
    
    */
	
	//// Test attribute handling
	
	/**
	 * Evaluates the "system-xxx" attributes for the element and returns
	 * value true/false. This method is static, so it can be called from DocumentHandler
	 * for head switch element.
	 * NOT DONE: This method also tests the skip-content attribute for elements, which implement
	 * ElementSkipContent interface.
	 *
	 * @param e		Element with system-attributes
	 * @return		Evaluated true/false-condition
	 */
	static public boolean evaluateSystemValues(Element element, Viewer viewer, boolean customTest) {
		String testString, wStr, hStr;
		int testInt, wInt, hInt, configBitRate;
		
		// Test system attributes for SMILElement

		// systemBitrate
		testString = element.getAttribute("systemBitrate");
		if (testString == null || testString.length() == 0)
			testString = element.getAttribute("system-bitrate");
		
		// If system-bitrate found, then check it
		if (testString != null && testString.length() > 0) {
			try {
				testInt = Integer.parseInt(testString);
				configBitRate = Integer.parseInt(viewer.getSystemBitrate());
				// If asked rate is too high for us -> false
				if (testInt > configBitRate) 
					return false;
			} catch (NumberFormatException e) {
				// invalid value, therefore false
				return false;
			}
		}

		// systemCaptions
		testString = element.getAttribute("systemCaptions");
		if (testString == null || testString.length() == 0)
			testString = element.getAttribute("system-captions");
		// If found, then must be the same as config to pass
		if (testString != null  && testString.length() > 0 
				&& testString.equals(viewer.getSystemCaptions()) == false)
			return false;

		// systemLanguage
		testString = element.getAttribute("systemLanguage");
		if (testString == null || testString.length() == 0)
			testString = element.getAttribute("system-language");
		// If found, then must be the same as config to pass
		// TODO: This is not really this easy - we should check for multi-languages!
		if (testString != null && testString.length() > 0
				&& testString.equals(viewer.getSystemLanguage()) == false)
			return false;

		// system-overdub-or-caption -> converted to systemOverbubOrSubtitle
		testString = element.getAttribute("system-overdub-or-caption");
		if (testString.equals("caption") == true)
			testString = "subtitle";
		// If found, then must be the same as config to pass
		if (testString != null  && testString.length() > 0
					&& testString.equals(viewer.getSystemOverdubOrSubtitle()) == false)
			return false;

		// systemOverdubOrSubtitle
		testString = element.getAttribute("systemOverdubOrSubtitle");
		// If found, then must be the same as config to pass
		if (testString != null  && testString.length() > 0
					&& testString.equals(viewer.getSystemOverdubOrSubtitle()) == false)
			return false;

		// system-required
		testString = element.getAttribute("systemRequired");
		if (testString == null || testString.length() == 0)
			testString = element.getAttribute("system-required");
			if (testString != null && testString.length() > 0) {
				int start = 0, end = testString.indexOf("+");
				boolean result = false;
				
				if (end < 0)
					end = testString.length();
	
				// All system prefixes MUST BE supported.
				do {
					String prefix = testString.substring(start, end);
					prefix = prefix.trim();
					result = getSystemRequired(prefix, element, viewer);
					// First test evaluating to false will finish - AND operand doesn't require more
					if (result == false) {
						Log.info("SMIL doesn't support XML prefix '"+prefix+"'.");
						return false;
					}
	
					start = end+1;
					end = testString.indexOf("+", start);
					if (end < 0)
						end = testString.length();
				} while (start < testString.length());
			}

		// systemComponent - some components are supported (www.xsmiles.org).
		// This can be used to check media players.
		testString = element.getAttribute("systemComponent");
		if (testString != null && testString.length() > 0) {
			// This should check for white space and not for space.
			int start = 0, end = testString.indexOf(" ");
			boolean result = false;
			
			if (end < 0)
				end = testString.length();

			// All system components MUST BE supported.
			do {
				result = viewer.getSystemComponent(testString.substring(start, end));
				// First test evaluating to false will finish - AND operand doesn't require more
				if (result == false) {
					Log.info("SMIL doesn't support component '"+testString.substring(start, end)+"'.");
					return false;
				}

				start = end+1;
				end = testString.indexOf(" ", start);
				if (end < 0)
					end = testString.length();
			} while (start < testString.length());
		}

		// systemAudioDesc
		testString = element.getAttribute("systemAudioDesc");
		// If found, then must be the same as config to pass
		if (testString != null && testString.length() > 0
				&& testString.equals(viewer.getSystemAudioDesc()) == false)
			return false;

		// systemCPU
		testString = element.getAttribute("systemCPU");
		// If found, then must be the same as config to pass
		if (testString != null && testString.length() > 0
				&& testString.equals(viewer.getSystemCPU()) == false)
			return false;

		// systemOperatingSystem
		testString = element.getAttribute("systemOperatingSystem");
		// If found, then must be the same as config to pass
		if (testString != null && testString.length() > 0
				&& testString.equals(viewer.getSystemOperatingSystem()) == false)
			return false;

		int screenWidth = viewer.getSystemScreenWidth();
		int screenHeight = viewer.getSystemScreenHeight();
		int screenDepth = viewer.getSystemScreenDepth();

		// systemScreenSize
		testString = element.getAttribute("systemScreenSize");
		if (testString == null || testString.length() == 0)
			testString = element.getAttribute("system-screen-size");
		// If found, then must be less than or equal to system screen size to pass
		if (testString != null && testString.length() > 0) {
			try { 
				wStr = testString.substring(0,testString.indexOf('x'));
				hStr = testString.substring(testString.indexOf('x')+1);
				wInt = Integer.parseInt(wStr);
				hInt = Integer.parseInt(hStr);
				// If asked width is too wide for us -> false
				if (wInt > screenWidth) 
					return false;
				// If asked height is too high for us -> false
				if (hInt > screenHeight) 
					return false;
			} catch (NumberFormatException e) {
				// invalid value, therefore false
				return false;
			} catch (IndexOutOfBoundsException e) {
				// invalid value, therefore false
				return false;
			}
		}
		
		// systemScreenDepth
		testString = element.getAttribute("systemScreenDepth");
		if (testString == null || testString.length() == 0)
			testString = element.getAttribute("system-screen-depth");
		// If found, then must be less than or equal to system screen depth to pass
		if (testString != null && testString.length() > 0) {
			try {
				testInt = Integer.parseInt(testString);
				// If asked depth is too high for us -> false
				if (testInt > screenDepth) 
					return false;
			} catch (NumberFormatException e) {
				// invalid value, therefore false
				return false;
			}
		}

		// NOT WORKING...
		// Check the skip-content attribute, default value is true
//		if (element instanceof XElementSkipContent) {
//			testString = element.getAttribute("skip-content");
//			if (testString != null && testString.equals("false"))
//				return false;
//		}

		// TODO: CustomTest won't work for Foreign Elements!
		if (customTest == true) 
			testString = element.getAttribute("customTest");
			if (testString != null && testString.length() > 0)  {
				SMILCustomAttributesElementImpl e = 
					(((SMILElementImpl)element).getSMILDoc()).searchFirstCustomAttributesElement();
				if (e != null)  {
					return e.evaluateCustomTest(testString);
				}
				// customAttributes not found - return false.
				return false;
			}

		// All checks passed, return true			
		return true;
	}

	/**
	 * Go through parent elements and search for the prefix definition to find out
	 * the related URL.
	 * @return true if the prefix is supported by the X-Smiles
	 */
	public static boolean getSystemRequired(String prefix, Element element, Viewer viewer) {
		Node n = element;
		String val = null;
		
		while (n != null && n instanceof Element) {
			val = ((Element)n).getAttribute("xmlns:"+prefix);
			Log.debug("Testing "+n+" got "+val+" searching "+prefix);
			if (val != null && val.length() > 0) {
				return viewer.getSystemRequired(val);
			}
			n = n.getParentNode();
		}
		// Prefix declaration not found! Return true - always passes.
		return true;
	}
	
	
	//// Size handling
	
    public static int intOrRepeatToInt(String s) {
        int n = 1;
        if (s.equals("indefinite")) {
            return 1;
        } else {
            try {
                n = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                return 1;
            }
        }
        return n;
    }
    
    public static final int top    = 0;
    public static final int left   = 1;
    public static final int width  = 2;
    public static final int height = 3;
    public static final int right  = 4;
    public static final int bottom = 5;
    
	/**
	 * Is the value percentage value?
	 * @param s			value to be checked
	 * @return true 	if s is a percentage value
	 */
    public static boolean isPercentage(String s) {
        if (s != null && s.length() > 0 && s.endsWith("%")) {
            return true;
        }
        return false;
    }

    public static int convertVertStringToInt(String value, LayoutCalc parent) {
		int size = 0;
		if (parent != null)
    		size = parent.calcBottom() - parent.calcTop();
    	return convertSizeStringToInt(value, size);
    }

	public static int convertHorizStringToInt(String value, LayoutCalc parent) {
		int size = 0;
		if (parent != null)
			size = parent.calcRight() - parent.calcLeft();
		return convertSizeStringToInt(value, size);
	}
    
    /**
     * Converts CSS2 size values to integer sizes, relative to the given size (width/height). 
     * Accepts absolute coordinates even if size is zero. If the size is zero,
     * and percentages are used, this will return zero values.
     * @param value				Percentage/absolute value
     * @param parentSize		Width or height of the parent layout element
     */
    public static int convertSizeStringToInt(String value, int size) {

		value = value.trim();

    	if (value == null || value.length() == 0)
    		return 0;
    		
        if (isPercentage(value)) {
			// Percentage value
			if (size <= 0)
				return 0;
				
			String s = value.substring(0, value.lastIndexOf('%'));
			try {
			    // float t = Float.parseFloat(s);
				// FLOAT BUG ! These should be 100.2% etc
			//			float t = (new Float(s)).floatValue();
			    int v = (new Integer(s)).intValue();
			    return (int) ((v * size)/100);
			} catch (NumberFormatException e) {
				return 0;
			}
        } else {
			// Absolute value
	        try {
	            return Integer.parseInt(value);
	        } catch (NumberFormatException e) {
	            return 0;
	        }
        }
    }


	/**
	 * Converts CSS2 size values to integer sizes, relative to the given element 
	 * (RootLayout/Region).
	 * Accepts absolute coordinates even if SMILElement is null. If the element is
	 * null and percentages are used, this will return zero values. But will not crash.
	 * @param s				Percentage/absolute value
	 * @param attributeType	top/bottom/left/right/width/height - type of the attribute
	 * @param e				RootLayoutElement or RegionElement - width or height for %
	 */
    public static int convertSizeStringToInt2(String s, int attributeType, SMILElement e) {
	
		if (s == null || s.length() == 0)
			return 0;
			
		s = s.trim();

        if (isPercentage(s)) {
			if (e != null)
	            return convertPercentageToInt(s, attributeType, e);
			else
				return 0;	
        } else {
            return convertAmountToString(s);
        }
    }
    
    public static int convertAmountToString(String s) {
        int n;
        
        try {
            n = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            n = 0;
        }
        return n;
    }
    
    public static int convertPercentageToInt(String p, int attributeType, SMILElement e) {
        if (e instanceof SMILLayoutElement) {
            return handleRootLayout(p, attributeType, e);
        } else if (e instanceof SMILRegionElement) {
            return handleRegion(p, attributeType, e);
        }
        return 0;
    }
    
    private static int handleRegion(String p, int attributeType, SMILElement e) {
        SMILRegionElement r = (SMILRegionElement) e;
        switch(attributeType) {
            case top:
                return handleTop(p, r);
                
            case left:
                return handleLeft(p, r);

            case right:
                return handleRight(p, r);
                
            case bottom:
                return handleBottom(p, r);
                
            default:
                break;
        }
        return 0;
        
    }
    
    private static int handleTop(String s, SMILRegionElement r) {
        s = s.substring(0, s.lastIndexOf('%'));
		SMILLayoutElement l = ((SMILRegionElementImpl)r).getSMILDoc().getDocLayout();
        try {
            // float t = Float.parseFloat(s);
			// FLOAT BUG ! These should be 100.2% etc
//			float t = (new Float(s)).floatValue();
            int t = (new Integer(s)).intValue();
			int rh = convertSizeStringToInt2(r.getHeight(), top, l);
	        return (int) ((t * rh)/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    private static int handleLeft(String s, SMILRegionElement r) {
        s = s.substring(0, s.lastIndexOf('%'));
        SMILLayoutElement l = ((SMILRegionElementImpl)r).getSMILDoc().getDocLayout();
        try {
            //float t = Float.parseFloat(s);
			int t = (new Integer(s)).intValue();
	        int rw = convertSizeStringToInt2(r.getWidth(), left, l);
	        return (int) ((t * rw)/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }
    private static int handleBottom(String s, SMILRegionElement r) {
        s = s.substring(0, s.lastIndexOf('%'));
        SMILLayoutElement l = ((SMILRegionElementImpl)r).getSMILDoc().getDocLayout();
        try {
            //float t = Float.parseFloat(s);
			int t = (new Integer(s)).intValue();
	        int rh = convertSizeStringToInt2(r.getHeight(), top, l);
	        return (int) ((t * rh)/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    private static int handleRight(String s, SMILRegionElement r) {
        s = s.substring(0, s.lastIndexOf('%'));
        SMILLayoutElement l = ((SMILRegionElementImpl)r).getSMILDoc().getDocLayout();
        try {
            //float t = Float.parseFloat(s);
			int t = (new Integer(s)).intValue();
 	       int rw = convertSizeStringToInt2(r.getWidth(), left, l);
 	       return (int) ((t * rw)/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }
    
    private static int handleRootLayout(String p, int attributeType, SMILElement e) {
        SMILLayoutElement rl = (SMILLayoutElement)e;
        switch(attributeType) {
            case top:
                return handleTop(p, rl);
                
            case left:
                return handleLeft(p, rl);

            case width:
                return handleWidth(p, rl);
                
            case height:
                return handleHeight(p, rl);
                
            case right:
                return handleWidth(p, rl);
                
            case bottom:
                return handleHeight(p, rl);
            default:
                break;
        }
        return 0;
    }
    	
    private static int handleTop(String s, SMILLayoutElement rl) {
//        s = s.substring(0, s.lastIndexOf('%'));
		s = s.substring(0, s.length()-1);
        try {
            //float t = Float.parseFloat(s);
  	      int t = Integer.parseInt(s);
//            int t = (new Integer(s)).intValue();
        return (int) ((t * rl.getRootLayoutHeight())/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    private static int handleLeft(String s, SMILLayoutElement rl) {
        s = s.substring(0, s.lastIndexOf('%'));
        try {
            //float t = Float.parseFloat(s);
			int t = Integer.parseInt(s);
//            int t = (new Integer(s)).intValue();
            return (int) ((t * rl.getRootLayoutWidth())/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    private static int handleWidth(String s, SMILLayoutElement rl) {
//        s = s.substring(0, s.lastIndexOf('%'));
		s = s.substring(0, s.length()-1);
        try {
            //float t = Float.parseFloat(s);
 	       int t = Integer.parseInt(s);
//            int t = (new Integer(s)).intValue();
        return (int) ((t * rl.getRootLayoutWidth())/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    private static int handleHeight(String s, SMILLayoutElement rl) {
//        s = s.substring(0, s.lastIndexOf('%'));
		s = s.substring(0, s.length()-1);
        try {
            //float t = Float.parseFloat(s);
 	       int t = Integer.parseInt(s);
//            int t = (new Integer(s)).intValue();
        return (int) ((t * rl.getRootLayoutHeight())/100);
        } catch (NumberFormatException e) {
        }
        return 0;
    }
}
