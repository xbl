/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.*;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

/**
 *  Declares smil meta element.
 */
public class SMILMetaElementImpl extends SMILElementImpl implements XSMILMetaElement {

  /**
   * Constructor with owner doc
   */
  public SMILMetaElementImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns) {
    super(owner, smil, ns, "meta");
  }

  /**
   *  See the content attribute. 
   * @exception DOMException
   *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
   */
  public String getContent() {
	  return getAttribute("content");
  }
  public void setContent(String content) throws DOMException {
	  if (content != null && content.length() > 0) {
	    setAttribute("content", content);
	  } else {
	    removeAttribute("content");
	  }
  }

  /**
   *  See the name attribute. 
   * @exception DOMException
   *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
   */
  public String getName() {
	  return getAttribute("name");
  }
  public void setName(String name) throws DOMException {
	  if (name != null && name.length() > 0) {
	    setAttribute("name", name);
	  } else {
	    removeAttribute("name");
	  }
  }

	public void init() {
		String name = getName();

		if (name != null) {	
		// If this meta element defines the title, then display it.
			if (name.equals("title") == true) {
				String content = getContent();
				if (content != null && content.length() > 0)
					getSMILDoc().getViewer().setTitle(content);
			}

			if (name.equals("base") == true) {
				String content = getContent();
				if (content != null && content.length() > 0)
					getSMILDoc().getViewer().setDocumentBaseURI(content);
			}
		}
		// Initialize all children
		super.init();
	}

}