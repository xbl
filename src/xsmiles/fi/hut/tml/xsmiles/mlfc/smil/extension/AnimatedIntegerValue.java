/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.extension;

import fi.hut.tml.xsmiles.mlfc.smil.basic.*;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.smil20.*;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.mlfc.smil.SMILMLFC;

import java.util.Vector;

/**
 * Animated Integer Value, animates values in format "x"
 */
public class AnimatedIntegerValue implements AnimatedValue {

	float value = 0;
	boolean percentageValue = false;

	public AnimatedIntegerValue(float val) {
		value = val;
	}

	public AnimatedIntegerValue(float val, boolean percentageVal) {
		value = val;
		percentageValue = percentageVal;
	}

	public AnimatedIntegerValue(String str) {
		if (str == null)
			throw new NumberFormatException("Null value");
			
		if (str.endsWith("%") == true)  {
			str = str.substring(0, str.length()-1);
			percentageValue = true;
		} else
			percentageValue = false;
	
		// Parse and indicate that it was a number
		value = Integer.parseInt(str);
	}

	/**
	 * Parse this string into value. Throws NumberFormatException, if cannot parse.
	 * @param str	String to be parsed
	 * @return 		Parsed AnimatedIntegerValue
	 */
	static public AnimatedIntegerValue parse(String str) {
		// Parse and indicate that it was a number
		int val = Integer.parseInt(str);
		return new AnimatedIntegerValue(val);
		
	}

	public boolean isPercentage()  {
		return percentageValue;
	}

	public float getValue() {
		return value;
	}

	public String toString()  {
		return ""+((int)value)+(percentageValue == true ? "%" : "");
	}


	public void clampValue()  {
		// Nothing to do.
	}

	/**
	 * Add val to this.
	 * @param val	Val to be added to this.
	 * @return		Returns val+this.
	 */
	public AnimatedValue add(AnimatedValue val) {
		float v = value + ((AnimatedIntegerValue)val).getValue();
		return new AnimatedIntegerValue(v, percentageValue);
	}

	/**
	 * Multiply this by integer value.
	 * @param integer	Integer value
	 * @return			Returns this * integer.
	 */
	public AnimatedValue mult(int integer) {
		float v = value * integer;
		return new AnimatedIntegerValue(v, percentageValue);
	}
	
	/**
	 * Interpolate between this and val by t, where t is [0,1]. For integers: (val-this)*t.
	 * @param val	AnimatedIntegerValue
	 * @return		Returns this * integer.
	 */
	public AnimatedValue interpolate(AnimatedValue val, float t) {
		float v = (value - ((AnimatedIntegerValue)val).getValue()) * t;
		return new AnimatedIntegerValue(v, percentageValue);
	}
	

	/**
	 * Calculates the distance between this and val. (for one dimension: abs(this-val) ) 
	 * @param val	AnimatedIntegerValue
	 * @return		Returns distance between this nad val..
	 */
	public float distance(AnimatedValue val) {
		return Math.abs(value - ((AnimatedIntegerValue)val).getValue());
	}
	
}
