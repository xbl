/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.smil.basic;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil20.*;
import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.Log;

// DOM interface
import org.w3c.dom.NodeList;

/**
 *  A <code>seq</code> container defines a sequence of elements in which 
 * elements play one after the other. 
 */
public class ElementSequentialTimeContainerImpl extends ElementTimeContainerImpl
									implements ElementSequentialTimeContainer {


	private TimeChildList timeChildren = null;		
	private XElementBasicTime activeChild = null;

	// Accumulated time from the first child to the current child

	// Total duration of the children activated so far
	protected long totalChildDuration = 0;

	/**
	 * Constructor - Set the owner and name.
	 */
	public ElementSequentialTimeContainerImpl(DocumentImpl owner, SMILDocumentImpl smil, String ns, String name) {
	    super(owner, smil, ns, name);
	}

	/**
	 * HACK to resolve seek times for links. In Seq, set the activeChild.
	 */
	public void resolveSeekTime(ElementBasicTimeImpl child)  {
		timeChildren = new TimeChildList(this, getSMILDoc().getViewer());
		activeChild = null;
		// Seek the timed child
		if (child != null)  {
			while (child.equals(activeChild) == false && timeChildren.hasMoreElements() == true)
				activeChild = (XElementBasicTime)timeChildren.nextElement();
		} else  {
			if (timeChildren.hasMoreElements() == false)
				return;
			// Get a timed child
			activeChild = (XElementBasicTime)timeChildren.nextElement();
		}	
		totalChildDuration = smilDoc.getScheduler().getDocTimeNow();
		super.resolveSeekTime(child);
	}

	public void activate() {
		// No children run, yet. 
		totalChildDuration = 0;
		super.activate();
	}

	public void simpleDurEnded()  {
		// No children run, yet. Reset this before setting new simpleDurTimer. 
		totalChildDuration = 0;
		super.simpleDurEnded();
	}

	public void repeat(long time)  {
		// No children run, yet. Reset this for all repeats. 
		long temp = totalChildDuration;
		totalChildDuration = 0;
		// If totalChildDuration was zero, we are coming from simpleDur, use simpleTime
		// Otherwise, use totalChildDur (last childEnded)
		if (temp != 0)
			super.repeat(temp);
		else
			super.repeat(time);		
	}

    /**
	 * Start children one by one.
     */
	public void startChildren() {
		// Initialize the TimeChildren list.
    	timeChildren = new TimeChildList(this, getSMILDoc().getViewer());
    	// If no children, then just wait until ???
    	if (timeChildren.hasMoreElements() == false) {

	    	// If Active Duration was resolved, then the timer will end the Active Duration
	    	// or if the Active Duration is "indefinite", the container will remain for ever
//	    	if (currentIntervalEnd.getResolved() == true || currentIntervalEnd.isIndefinite() == true) {
//	    	} else {
	    		// Active Duration not resolved - the end of the last child will end it
				
				// Save Elapsed Time for Parent (Seq) Time Container
//				activeDuration = 0;

//	    		deactivate();
//	    	}

	    	return;
    	}

    	// Get a timed child
    	activeChild = (XElementBasicTime)timeChildren.nextElement();
    	
    	activeChild.startup();
    }

    /**
     * Checks if this child is startable - i.e. if the child's parent is active
     * and that this child is the active child. This
     * is used to prevent syncbased elements responding if not active.
     */	
	public boolean isChildStartable(ElementBasicTimeImpl child)  {
    	return isActive() && activeChild==child;
    }

	/**
	 * This is called from the child to tell that it has ended.
	 * @param childDuration		Duration of the child element
	 */
	public void childEnded(long childDuration) {
		String fill = null;

		Log.debug(getId()+" Seq child ended, used time "+childDuration);

		// Check the child really ended. (the same as par syncend="last")
		// If the child still has a resolved begin, then we're not done.
		if (activeChild != null)
			if( activeChild.isActive() || ((ElementBasicTimeImpl)activeChild).isResolved() == true )
			    return;

		// Child duration added to our simple duration start time
		// i.e. simple dur is the duration of a child
		totalChildDuration += childDuration;

		// Add the time spend in the child to the accumulated time
//		currentTime = ACADD ( currentTime, elapsedTime (relative) );	

		// KLUDGE: TODO: THIS PREVENTS STUPID NULLPOINTEREXCEPTION
		// IF A LINK HAS BEEN PRESSED AND THE PARENTS HAVEN'T BEEN
		// RESOLVED. (because the link points to the child).
		if (timeChildren == null) {
			Log.debug("Seq time container malfunction due to hyperlinking.. not implemented. sorry.");
			return;
		}

		// Last child played - end of seq Simple Duration
		// repeat or let the child be (freeze and don't close down)
		if (timeChildren.hasMoreElements() == false && activeDuration != null &&
				(activeDuration.getResolved() == false || activeDuration.isIndefinite() == true)) {
			// repeatCount exception for seq - see calculation of IntermediateActiveDuration()
			repeatRemoveFreeze(totalChildDuration);
			return;
		}

		// SEQ/FILL: close/hold the ended child, or freeze the last child
		boolean close = true;
		if (activeChild != null) {
			if (activeChild instanceof ElementBasicTimeImpl)
				fill = ((ElementBasicTimeImpl)activeChild).getFill();
				if (fill != null && fill.equals("hold"))
					close = false;
				if (fill != null && fill.equals("freeze") && timeChildren.hasMoreElements() == false)
					close = false;
		}
		Log.debug("SEQ CHILD:"+close+" ");
		if (close == true)
			activeChild.closedown();

		// Start the next child
		if (timeChildren.hasMoreElements() == true)  {
			activeChild = (XElementBasicTime)timeChildren.nextElement();
			activeChild.startup();
		}
	}	

	// Free resources
	public void destroy() {
		// Free reference to a TimeChildList
		timeChildren = null;
		super.destroy();
	}

	/**
	 * Returns time in body time space,
	 * @param t		Time to convert, in millisecs
	 * @return 		time in body time, in millisecs
	 */
	public long getTimeInBodyTime(long t) {
		if (elementForceStartTime != 0)
			return elementForceStartTime + t;
		else
			return ((XElementBasicTime)getParentNode()).getTimeInBodyTime(beginTime+repeatTime) +
					t + totalChildDuration;
	}

}
