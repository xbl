/*
 * X-Smiles THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED look for full legal shaisse in
 * licenses/LICENSE_XSMILES
 */

package fi.hut.tml.xsmiles.mlfc;

import java.awt.Container;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.mlfc.general.XFMUtils;

/**
 * A CoreMLFC is an extened version of an MLFC. It is core in a sence, that it is intended only for use in X-Smiles, as
 * it extensively uses the BrowserWindow, and other core classes. Because of this, it is difficult to reuse this
 * component seperately in some other project.
 * 
 * @author Juha
 */
public abstract class CoreMLFC extends MLFC {
    private BrowserWindow browserWindow;
    private XFMUtils xfmutils = null;

    public BrowserWindow getBrowserWindow() {
        if (browserWindow == null)
            browserWindow = getXMLDocument().getBrowser();
        return browserWindow;
    }

    public void setBrowserWindow(BrowserWindow bw) {
        browserWindow = bw;
    }

    public final void startMLFC(XMLDocument a_doc, Container cont, boolean a_primary, BrowserWindow bw) {
        browserWindow = bw;
        super.startMLFC(a_doc, cont);
    }

    public XFMUtils getXFMUtils() {
        if (xfmutils == null)
            xfmutils = new XFMUtils(this);
        return xfmutils;
    }

    public XSmilesStyleSheet getStyleSheet() {
        return null;
    }
}
