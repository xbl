package fi.hut.tml.xsmiles.mlfc;

import fi.hut.tml.xsmiles.XSmilesException;

/**
 * General exception thrown by an MLFC
 * @author Mikko Honkala
 */
public class MLFCException extends XSmilesException{
  
        /** constructs an exception without a message */
    public MLFCException()
    {
        super();
    }
    
        /** constructs an exception without a message */
    public MLFCException(String message)
    {
        super(message);
    }
}
