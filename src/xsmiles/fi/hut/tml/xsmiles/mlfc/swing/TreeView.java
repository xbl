/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.swing;

import javax.swing.JOptionPane;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.JScrollPane;

import java.awt.*;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;

import org.w3c.dom.*;

/**
 * Tree view of DOM Tree 
 *
 * @author	Aki Teppo
 * @version	$Revision: 1.1 $
 */
public class TreeView implements javax.swing.event.TreeExpansionListener
{
  private DefaultMutableTreeNode root;
  private XMLTree treeView;
    
    /**
     * Constructs SourceMLFC
     *
     * @param browser reference to main Browser
     */
    public TreeView()
    {
    }

    
    /**
     * Displays tree in the MLFC area of the GUI window.
     *
     * @param doc the XML document to be displayed.
     */
//    public void start()
//    {
//	    Container container=this.getContainer();
//	    XMLDocument document=this.getXMLDocument();
//		this.view(container,document);
//    }
	public void view(Container container, XMLDocument document)
	{
		container.setBackground(java.awt.Color.white);
		
	    root = new DefaultMutableTreeNode();
		treeView = new XMLTree(root);
		treeView.addNode(root, document.getDocument().getDocumentElement());
		treeView.putClientProperty("JTree.lineStyle", "Angled");
		Container scrollPane = new JScrollPane(treeView);
		
		container.add(scrollPane);
		//treeView.setVisible(true);
		treeView.addTreeExpansionListener(this); 
		treeView.invalidate();
		scrollPane.invalidate();
		container.validate();
    }
    
    /**
     * Returns the state of object to similar as just after constructing.
     */
//    public void stop()
//    {
//      this.getContainer().removeAll();
//    }
	
    public void init()
    {
    }
    public void destroy()
    {
    }
    
    public void treeCollapsed(TreeExpansionEvent event)
    {
	Log.debug("Tree collapsed "+" event " +" : size:"+treeView.getSize());
    }

    public void treeExpanded(TreeExpansionEvent event)
    {
	Log.debug("Tree expanded "+ " event " +" : size:"+treeView.getSize());
    }
    
    public void displaySecondaryMLFC(XMLDocument d, Container c) {
	Log.debug("TreeMLFC doesn't support secondary MLFC yet");
    }
    
}







