/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.mlfc.swing;


//import com.sun.java.swing.JOptionPane;
//import com.sun.java.swing.JTree;
//import com.sun.java.swing.JTree;
//import com.sun.java.swing.tree.TreeNode;
//import com.sun.java.swing.tree.MutableTreeNode;
//import com.sun.java.swing.tree.DefaultMutableTreeNode;

import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualElement;


public class XMLTree extends JTree {

    public XMLTree(TreeNode root) {
        super(root);    
    }


    public String  convertValueToText (Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        if (value instanceof DefaultMutableTreeNode)
	    {
	        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
	        Object userObject = node.getUserObject();
	        if (userObject != null && userObject instanceof Node)
		    {
		        Node xnode = (Node)userObject;
		        String text;
		        switch (xnode.getNodeType())
			    {
			    case Node.DOCUMENT_NODE:
			        text = xnode.getNodeValue();
			        break;
    			    
			    case Node.ELEMENT_NODE:
			        if (xnode instanceof PseudoElement)
			        {
			            text="PseudoElem ::"+xnode.getNodeName();
			        }
			        else
			        {
			            text = xnode.getNodeName();
			        }
                    text+=" ["+xnode.hashCode()+"] ";
			    	if (xnode instanceof StylableElement)
			    	{
			    	    text+=" : "+((StylableElement)xnode).getStyle();
			    	}
//			        if (xnode.hasChildNodes())
//				    {
//				        Node firstNode = xnode.getFirstChild();
//				        if (firstNode.getNodeType() == Node.TEXT_NODE &&
//					    firstNode.getNextSibling() == null &&
//					    firstNode.getNodeValue().length() > 0)
//					    text += " -- " + firstNode.getNodeValue();
//				    }
			        break;
    			    
			    case Node.ATTRIBUTE_NODE:
			        text= "Attr: " + xnode.getNodeName()+" = \""+xnode.getNodeValue()+"\"";
			        break;
    			    
			    case Node.PROCESSING_INSTRUCTION_NODE:
					text="<?"+xnode.getNodeName()+" "+xnode.getNodeValue()+"?>";
					break;
			    case Node.COMMENT_NODE:
			    case Node.TEXT_NODE:
			    default:
			        text = xnode.getNodeValue();
			        break;
			    }
		        return text;
		    }
	    }
        //return this.convertValueToText(value, selected, expanded, leaf, row, hasFocus);
          return new String("Document root");   
    }


    protected void addNode (MutableTreeNode parent, Node xnode)
    {
        if (parent==null) parent=(MutableTreeNode)this.treeModel.getRoot();
        boolean include = false;
        switch (xnode.getNodeType())
	    {
            case Node.DOCUMENT_NODE:
                include = true;
                break;
    	    
            case Node.ELEMENT_NODE:
                include = true;
                break;
    	    
            case Node.ATTRIBUTE_NODE:
                include=true;
                break;
    	    
            case Node.PROCESSING_INSTRUCTION_NODE:
                include = true;
                break;
    	    
            case Node.COMMENT_NODE:
                break;
    	    
            case Node.TEXT_NODE:
                String rawdata = xnode.getNodeValue();
                String trimmed = rawdata.replace('\r',' ').replace('\t',' ').replace('\n',' ');
				trimmed=trimmed.trim();
                if (rawdata.length() > trimmed.length())
                    xnode.setNodeValue(trimmed);
//                if (xnode.getNextSibling() != null && trimmed.length() > 0)
                  if (trimmed.length() > 0)
                    include = true;
                break;
            default:
                break;
	    }
        
        if (include)
	    {
	        MutableTreeNode tnode =
		    new DefaultMutableTreeNode(xnode, true);
	        parent.insert(tnode, parent.getChildCount());
    	    
	        addChildren(tnode, xnode);
	    }
    }

    protected void addChildren (MutableTreeNode parent, Node node)
    {
    	NamedNodeMap attributes = node.getAttributes();
		// Add attributes
		if (attributes!=null) 
		for (int i=0;i<attributes.getLength();i++) 
		{
			addNode(parent,attributes.item(i));
		}	
	
        if (node.getNodeType() != Node.ATTRIBUTE_NODE)
        {
            // Add child nodes
    	    for (Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
	    	        addNode(parent, child);
        	}
    	    // add pseudo elements
    	    if (node instanceof PseudoElementContainerService)
    	    {
    	        Vector pseudoElements = ((PseudoElementContainerService)node).getPseudoElements();
    	        for (Enumeration e = pseudoElements.elements();e.hasMoreElements();)
    	        {
    	            Node n = (Node)e.nextElement();
    	            addNode(parent,n);
    	        }
    	    }
        }
    }

}