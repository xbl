/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;

/**
 * Mostly this complies with the requirements for StylableElement
 * 
 * @author Juha
 */
public abstract class StylableImpl extends VisualElementImpl implements StylableElement {

    private CSSStyleDeclaration style;

    public StylableImpl(org.apache.xerces.dom.DocumentImpl ownerDocument, String value) {
        super(ownerDocument, value);
    }

    public StylableImpl(org.apache.xerces.dom.DocumentImpl ownerDocument, String namespaceURI, String qualifiedName) {
        super(ownerDocument, namespaceURI, qualifiedName);
    }

    /** get the resolved CSS style for this element */
    public CSSStyleDeclaration getStyle() {
        return style;
    }

    /** set the resolved CSS style for this element */
    public void setStyle(CSSStyleDeclaration s) {
        Log.debug("setting style");
        style = s;
    }

    /** return the string contents of the style attribute. If no support for style attribute, return null */
    public String getStyleAttrValue() {
        return null;
    }

    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass) {
        return false;
    }

}

