	/**
	 * Licence BS found in licenses directory.
	 */
	package fi.hut.tml.xsmiles.mlfc.xframes;
	
	import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XMenu;
import fi.hut.tml.xsmiles.gui.components.XMenuBar;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;
import fi.hut.tml.xsmiles.mlfc.general.XFMUtils;
	
	/**
	 * Implmentation of the XFrames W3C Working Draft 6 August 2002. ns: http://www.w3.org/2002/06/xframes
	 * 
	 * For now Swing dependent Other problems. Only one level of frames allowed till now. Embedded frames withing frames
	 * require a parser for dealing with the #frames directive
	 * 
	 * @author juha
	 */
	public class XFramesMLFC extends CoreMLFC {
	
	    // Container containing the XFrames
	    private Container container;
	
	    // The <frames> element, the root tag of a xfm file
	    // in this case, also a visual component service
	    private FramesImpl framesRoot;
	
	    // All elements used by XFramesMLFC
	    protected Vector elements = null;
	    // protected XSmilesStyleSheet styleSheet;
	    private XFMUtils xfmutils;
	    
	    private XMenuBar menuBar;
	    private XMenu menu;
	    private ComponentFactory cf;
	    private String docTitle;
	
	    /**
	     * Nothing special. Just initialize vectors
	     */
	    public XFramesMLFC() {
	        initFrames();
	        xfmutils = new XFMUtils(this);
	    }
	
	    public void init() {
	        Document doc = this.getXMLDocument().getDocument();
	        docTitle = getXMLDocument().getXMLURL().toString();
	        if (doc instanceof XSmilesDocumentImpl) {
	            XSmilesStyleSheet stylesheet = ((XSmilesDocumentImpl) doc).getStyleSheet();
	        } else {
	            Log.error(this.toString() + ": docElem was not instanceof StylesheetService");
	        }
	        cf = getMLFCListener().getComponentFactory();
	        	        
	        //styleSheet = new CSSImpl();
	        //try { // add default XHTML CSS values ;)
	        //styleSheet.addXMLDefaultStyleSheet(Browser.getXResource("xsmiles.xhtml.stylesheet"));
	        //}
	        //catch (Exception mue) { Log.error(mue); }
	    }
	
	    /**
	     * This should be called, whenever using XFramesMLFC Also remember to call destroy, if it is not otherwise called
	     */
	    private void initFrames() {
	        elements = new Vector();
	        super.init();	        
	    }
	    
	    public void setTitle(String title) {
	        docTitle = title;
	    }
	    
	    public XMenuItem addFrameToMenu(String name) {
	    	if (menuBar==null) {
	    		menuBar = cf.getXMenuBar();
	    		menu = cf.getXMenu("Frame list");
	    	}
	    	XMenuItem menuItem = cf.getXMenuItem(name);
	        menu.add(menuItem);	        
	        return menuItem;
	    }
	
	    /**
	     * Start the MLFC This function is the pair to stop().
	     */
	    public void start() {
	        container = getContainer();
	        container.setLayout(new BorderLayout());
	        if (menuBar!=null) {
	        	menuBar.addMenu(menu);	        
	        	container.add((Component) menuBar.getObject(), BorderLayout.NORTH);
	        }
	        container.add(framesRoot.getComponent(), BorderLayout.CENTER);        
	        framesRoot.getComponent().invalidate();
	        container.validate();
	        if (docTitle!=null) {
	            this.setTitle(docTitle);
	        }
	    }
	
	    /**
	     * Stop the MLFC. This function is the pair to start().
	     */
	    public void stop() {
	        // Destroy all windows used in frameset
	        /*
	         * BrowserWindow b=null; for(Enumeration
	         * e=browsers.elements();e.hasMoreElements();b=(BrowserWindow)e.nextElement()) { b.closeBrowserWindow(); }
	         */
	    }
	
	    /**
	     * Create a DOM element.
	     */
	    public Element createElementNS(DocumentImpl doc, String ns, String tag) {
	        Element element = null;
	
	        if (tag == null) {
	            Log.error("Invalid null tag name!");
	            return null;
	        }
	        String localname = getLocalname(tag);
	
	        if (localname.equals("frames")) {
	            element = new FramesImpl(doc, this, ns, tag);
	        } else if (localname.equals("group")) {
	            element = new GroupImpl(doc, this, ns, tag);
	        } else if (localname.equals("row")) {
	            element = new RowImpl(doc, this, ns, tag);
	        } else if (localname.equals("column")) {
	            element = new ColumnImpl(doc, this, ns, tag);
	        } else if (localname.equals("frame")) {
	            element = new FrameImpl(doc, this, ns, tag);
	        } else if (localname.equals("head")) {
	            element = new HeadImpl(doc, this, ns, tag);
	        }
	        return element;
	    }
	
	    /**
	     * The FramesImpl propably sets this?
	     */
	    public void setFramesImpl(FramesImpl i) {
	        framesRoot = i;
	    }
	
	    public XFMUtils getXFMUtils() {
	        if (xfmutils == null)
	            Log.debug("XFMUTILS NULL");
	        return xfmutils;
	    }
	
	    //public XSmilesStyleSheet getStyleSheet()
	    //{
	    //return styleSheet;
	    // }
	
	    /**
	     * Returns a stylesheet instance created from default and associated stylesheet file.
	     * 
	     * @return instance of XMLStyleSheet2
	     */
	    public XSmilesStyleSheet getStyleSheet() {
	        return ((ExtendedDocument) getXMLDocument().getDocument()).getStyleSheet();
	    }
	}
