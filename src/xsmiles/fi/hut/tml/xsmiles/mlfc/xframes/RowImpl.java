/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JComponent;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;

/**
 * RowElementImpl Go through all first level children, and add them to own container,
 * 
 * Styles: resizeable: true; JSplitPanes false; with boxlayout and X_AXIS border-width: only for boxlayout, set
 * parameter in box. border-color: color the border somehow.
 * 
 * @author Juha
 * @deprecated &lt;row&gt; is not part of the XFrames specs anymore, it's replaced by the "compose"-attribute of &lt;group&gt; 
 */
public class RowImpl extends StylableImpl implements VisualComponentService {
    // The container for row
    private Container container;
    protected StackableComponent sc;
    protected int stackDynamicity = StackableComponent.STATIC;

    private CoreMLFC mlfc;

    /**
     * Constructor - Set the owner, name and namespace.
     */
    public RowImpl(DocumentImpl owner, CoreMLFC m, String namespace, String tag) {
        super(owner, namespace, tag);
        mlfc = m;
        System.err.println("<row> is deprecated, use <group compose=\"horizontal\">");
    }

    protected void dispatch(String type) {
        org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        ((org.w3c.dom.events.EventTarget) this).dispatchEvent(evt);
        return;
    }

    protected void setSC(int orientation) {
        sc = new StackableComponent(stackDynamicity, orientation);
        sc.getComponent().addComponentListener(new Resizer());
    }

    /**
     * Initialize this element. This requires that the DOM tree is available.
     */
    public void init() {
        super.init();

        // get CSS style for child.
        CSSStyleDeclaration stylee = mlfc.getStyleSheet().getParsedStyle(this);

        NodeList children = getChildNodes();

        String dynamic = null;
        if (stylee!=null) {
            dynamic = stylee.getPropertyValue("border");
        }
        // String dynamic=getAttribute("dynamic");
        Log.debug("Border property: " + dynamic);

        if (dynamic != null && dynamic.equals("dynamic"))
            stackDynamicity = StackableComponent.DYNAMIC;

        String rows = getAttribute("rows");
        String cols = getAttribute("cols");

        if (cols != null && !cols.equals("")) {
            setSC(StackableComponent.HORIZONTAL);
        } else if (rows != null && !rows.equals("")) {
            setSC(StackableComponent.VERTICAL);
        } else if (sc == null) { // ColumnImpl extends this and might have already setSC
            setSC(StackableComponent.HORIZONTAL);
        } // Otherwise we use default row behaviour

        Log.debug("ROW INIT");

        // Add all elements in a row formation
        // In the future row is also to handle width and height specified
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof VisualComponentService) {
                Log.debug("Adding component to ROW: " + ((VisualComponentService) child).getComponent().toString());
                Component c = ((VisualComponentService) child).getComponent();

                // get CSS style for child.
                // stylee = mlfc.getStyleSheet().getParsedStyle((StylableElement)child);
                stylee = ((StylableElement) child).getStyle();

                String w = null;
                String h = null;

                if (stylee !=null) {
                    w = stylee.getPropertyValue("width");
                    h = stylee.getPropertyValue("height");
                    Log.debug("STYLE: " + stylee.toString() + " STYLESHEET: " + mlfc.getStyleSheet().toString());
                    Log.debug("FRAME WIDTH: " + w + " HEIGHT: " + h);
                }
                int width = 10;
                int height = 10;

                if (sc.getOrientation() == StackableComponent.HORIZONTAL) {
                    // Handle procentages and pixels...
                    // w = w.substring(0,w.length()-2);
                    try {
                        if (w != null && !w.equals("")) {
                            width = Integer.parseInt(w);
                        }
                    } catch (Exception e) {
                        Log.error("Error parsing int in Frame element");
                    }
                    height = mlfc.getContainer().getSize().height - 30;
                } else if (sc.getOrientation() == StackableComponent.VERTICAL) {
                    try {
                        // 
                        // h = h.substring(0,h.length()-2);
                        if (h != null && !h.equals("")) {
                            height = Integer.parseInt(h);
                        }
                    } catch (Exception e) {
                        Log.error("Error parsing int in Frame element");
                    }
                    width = mlfc.getContainer().getSize().width - 30;
                }
                ((JComponent) c).setMinimumSize(new Dimension(10, 10));
                int scaledWidth = (int) (mlfc.getContainer().getSize().width * (double) (((double) width) / 100));
                int scaledHeight = (int) (mlfc.getContainer().getSize().height * (double) (((double) height) / 100));
                Log.debug(
                    "Using xframes values: "
                        + scaledWidth
                        + " "
                        + scaledHeight
                        + " "
                        + mlfc.getContainer().getSize().toString());
                c.setSize(new Dimension(scaledWidth - 10, scaledHeight - 10));
                ((JComponent) c).setPreferredSize(new Dimension(scaledWidth - 20, scaledHeight - 20));

                sc.add(c);
                c.invalidate();
                sc.getComponent().validate();
            }
        }
        sc.getComponent().doLayout();
    }

    /**
     * Return the visual component for this extension element
     */
    public Component getComponent() {
        if (sc == null || sc.getComponent() == null) {
            init();
        }
        return sc.getComponent();
    }

    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize() {
        if (sc == null || sc.getComponent() == null) {
            init();
        }

        return sc.getComponent().getSize();
    }

    public void setZoom(double zoom) {
        Log.debug("Zoom does not work");
    }

    public void setVisible(boolean visible) {
        if (sc == null || sc.getComponent() == null) {
            init();
        }
        sc.getComponent().setVisible(visible);
    }

    public boolean getVisible() {
        if (sc == null || sc.getComponent() == null) {
            init();
        }
        return sc.getComponent().isVisible();
    }

    private void resizeChildren() {
        NodeList children = getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof VisualComponentService) {
                Log.debug("Adding component to ROW: " + ((VisualComponentService) child).getComponent().toString());
                Component c = ((VisualComponentService) child).getComponent();

                // get CSS style for child.
                CSSStyleDeclaration stylee = mlfc.getStyleSheet().getParsedStyle((StylableElement) child);

                String w = null;
                String h = null;

                if (stylee !=null) {
                    w = stylee.getPropertyValue("width");
                    h = stylee.getPropertyValue("height");
                    Log.debug("STYLE: " + stylee.toString() + " STYLESHEET: " + mlfc.getStyleSheet().toString());
                    Log.debug("FRAME WIDTH: " + w + " HEIGHT: " + h);
                }

                int width = 10;
                int height = 10;

                if (sc.getOrientation() == StackableComponent.HORIZONTAL) {
                    // Handle procentages and pixels...
                    // w = w.substring(0,w.length()-2);
                    try {
                        if (w != null && !w.equals("")) {
                            width = Integer.parseInt(w);
                        }
                    } catch (Exception e) {
                        Log.error("Error parsing int in Frame element");
                    }
                    height = mlfc.getContainer().getSize().height - 10;
                } else if (sc.getOrientation() == StackableComponent.VERTICAL) {
                    try {
                        // 
                        // h = h.substring(0,h.length()-2);
                        if (h != null && !h.equals("")) {
                            height = Integer.parseInt(h);
                        }
                    } catch (Exception e) {
                        Log.error("Error parsing int in Frame element");
                    }
                    width = mlfc.getContainer().getSize().width - 10;
                }
                int scaledWidth = (int) (mlfc.getContainer().getSize().width * (double) (((double) width) / 100));
                int scaledHeight = (int) (mlfc.getContainer().getSize().height * (double) (((double) height) / 100));
                Log.debug(
                    "Using xframes values: "
                        + scaledWidth
                        + " "
                        + scaledHeight
                        + " "
                        + mlfc.getContainer().getSize().toString());
                ((JComponent) c).setMinimumSize(new Dimension(10, 10));
                c.setSize(new Dimension(scaledWidth, scaledHeight));
                ((JComponent) c).setPreferredSize(new Dimension(scaledWidth, scaledHeight));
                c.invalidate();
                sc.getComponent().validate();
            }
        }
        sc.getComponent().doLayout();
    }

    /**
     * Rezie children whenever parent is resized.
     *  
     */
    private class Resizer extends ComponentAdapter {
        public void componentResized(ComponentEvent e) {
            resizeChildren();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}
