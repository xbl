/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * Attaches the CSS stylesheet to the parser.
 * 
 * @author Juha
 */
public class StyleImpl extends XSmilesElementImpl {

    private XFramesMLFC mlfc;

    public StyleImpl(DocumentImpl owner, XFramesMLFC m, String namespace, String tag) {
        super(owner, namespace, tag);
        mlfc = m;
    }

    public void init() {
        // Set the CSS
        String source = getAttribute("source");
        if (source != null && !source.equals(""))
            try {
                mlfc.getStyleSheet().addXMLStyleSheet(new URL(mlfc.getXMLDocument().getLink().getURL(), source));
            } catch (Exception mue) {
                Log.error(mue);
            }

        // add inline CSS
        if (this.getAttribute("type").equalsIgnoreCase("text/css"))
            mlfc.getStyleSheet().addXMLStyleSheet(this.getText(), mlfc.getXMLDocument().getLink().getURL());
    }
}

