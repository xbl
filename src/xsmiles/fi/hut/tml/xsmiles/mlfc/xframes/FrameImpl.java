/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.net.URL;

import javax.swing.JComponent;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.EventBroker;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.event.GUIEventAdapter;
import fi.hut.tml.xsmiles.event.GUIEventListener;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;

/**
 * FrameImpl Create a new browserWindow with correct id. Find the source document, if one exists. Otherwise load
 * "empty" document.
 * 
 * with boxlayout and Y_AXIS
 * 
 * @author Juha
 */
public class FrameImpl extends StylableImpl implements VisualComponentService {
    // The browserWindow associated with THIS frame
    private BrowserWindow browserWindow;
    private CoreMLFC mlfc;
    private Container container;
    private GUIEventListener gl;
    private String id;
    private String uri;
    private int width = 0;
    private int height = 0;
    private FramesImpl fi = null;
    
    /**
     * Constructor - Set the owner, name and namespace.
     */
    public FrameImpl(DocumentImpl owner, CoreMLFC mlfc, String namespace, String tag) {
        super(owner, namespace, tag);
        this.mlfc = mlfc;
    }
    
    private void dispatch(String type) {
        org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        ((org.w3c.dom.events.EventTarget) this).dispatchEvent(evt);
        return;
    }
    
    /**
     * Initialize this element. This requires that the DOM tree is available.
     *  
     */
    public void init() {
        try {
            XSmilesDocumentImpl d = (XSmilesDocumentImpl) mlfc.getXMLDocument().getDocument();
            fi = (FramesImpl) d.getElementsByTagName("frames").item(0);
        } catch (Exception e) {
            mlfc.getMLFCListener().showErrorDialog("XFrames Error", "Missing <frames> tag");
        }
        
        id = getAttribute("id");
        uri = getAttribute("source");
        String htmlSrc = getAttribute("src");
        
        // HTML frames compatibility
        if (uri == null || uri.equals("")) {
            if (htmlSrc != null && !htmlSrc.equals(""))
                uri = htmlSrc;
        }
        
        // Translate a possibly relative uri into a global URI
        try {
            if ((uri != null) && !uri.equals("")) {
                URL utmp = resolveURI(uri);
                uri = utmp.toString();
            }
        } catch (Exception e) {
            // woop woop.
            Log.error(e, "Error resolving uri '" + uri + "'");
        }
        
        if (id == null || id.equals("")) {
            if (uri == null || uri.equals(""))
                browserWindow = mlfc.getBrowserWindow().newBrowserWindow("cfg/empty.xml", false);
            else
                browserWindow = mlfc.getBrowserWindow().newBrowserWindow(uri, false);
        } else {
            if (mlfc.getXFMUtils().getURIPopulators().contains(id)) {
                uri = (String) mlfc.getXFMUtils().getURIPopulators().get(id);
                try {
                    uri = resolveURI(uri).toExternalForm();
                } catch (Exception e) {
                    Log.error(e, "Error resolving uri '" + uri + "'");
                }
            }
            if (uri != null && !uri.equals(""))
                browserWindow = mlfc.getBrowserWindow().newBrowserWindow(uri, id, false);
            else
                browserWindow = mlfc.getBrowserWindow().newBrowserWindow("cfg/empty.xml", id, false);
        }
        
        
        if (browserWindow == null)
            mlfc.getMLFCListener().showErrorDialog("XFrames Error", "BrowserWindow null for FrameImpl.");
        gl = new GUIEventRouter();
        container = browserWindow.getContentArea();
        browserWindow.getEventBroker().addGUIEventListener(gl);
        browserWindow.waitState(BrowserLogic.READY);
          
        
        
        super.init();
        container.doLayout();
        container.invalidate();
        //container.getParent().validate();
        String framesUri = mlfc.getXFMUtils().createFramesURI();
        mlfc.getBrowserWindow().getGUIManager().getCurrentGUI().setLocation(framesUri);
    }
    
    /**
     * Destroy this element.
     */
    public void destroy() {
        if (browserWindow != null) {
            browserWindow.getEventBroker().removeGUIEventListener(gl);
            browserWindow.closeBrowserWindow();
        }
        fi = null;
        super.destroy();
    }
    
    /**
     * Return the visual component for this extension element
     */
    public Component getComponent() {
        if (container == null)
            init();
        ((JComponent) container).setPreferredSize(new Dimension(10, 10));
        if (width != 0 && height == 0)
            ((JComponent) container).setPreferredSize(new Dimension(width, 1000));
        else if (width == 0 && height != 0)
            ((JComponent) container).setPreferredSize(new Dimension(1000, height));
        else if (width != 0 && height != 0)
            ((JComponent) container).setPreferredSize(new Dimension(width, height));
        
        return container;
    }
    
    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize() {
        if (container == null)
            init();
        return container.getSize();
    }
    
    public void setZoom(double zoom) {
        // Log.debug("Zoom does not work");
    }
    
    public void setVisible(boolean visible) {
        if (container == null)
            init();
        
        container.setVisible(visible);
    }
    
    public boolean getVisible() {
        if (container == null)
            init();
        
        return container.isVisible();
    }
    
    private class GUIEventRouter extends GUIEventAdapter {
        EventBroker hostBrowserBroker;
        
        /**
         * After browser is ready, then start is called. Not neccesary to implement
         */
        public void start() {
            
        }
        
        /**
         * Destroy The GUI (delete frame, etc)
         */
        public void destroy() {
            
        }
        
        /**
         * Informs the gui that browser is working
         */
        public void browserWorking() {
            mlfc.getBrowserWindow().getGUIManager().getCurrentGUI().browserWorking();
        }
        
        /**
         * Informs the gui that browser is resting
         */
        public void browserReady() {
            mlfc.getBrowserWindow().getGUIManager().getCurrentGUI().browserReady();
        }
        
        /**
         * @param s
         *                  The location that is beeing loaded
         */
        public void setLocation(String s) {
            
            if (id != null) {
                mlfc.getXFMUtils().getURIPopulators().remove(id);
                mlfc.getXFMUtils().getURIPopulators().put(id, s);
            }
            // Log.debug(mlfc.createFramesURI());
            
            mlfc.getBrowserWindow().getGUIManager().getCurrentGUI().setLocation(mlfc.getXFMUtils().createFramesURI());
            
            // WE_HAVE_NOT_GONE_BACK_OR_FORWARD
            // THEN save the documenthistory uri to documenthistory.
            
            //if(browser.getDocumentHistory().getLatestCommand())
            mlfc.getBrowserWindow().getDocumentHistory().addURL(
                    new XLink(mlfc.getXFMUtils().createFramesURI(), XLink.SIMPLE));
            container.validate();
        }
        
        /**
         * @param statusText
         *                  The text to put in status bar
         */
        public void setStatusText(String statusText) {
            mlfc.getBrowserWindow().getGUIManager().getCurrentGUI().setStatusText(statusText);
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}
