/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * Implements the base Frames element of XFrames language Go through all children. If more than one visual child, then
 * create a tabbed pane for each.
 * 
 * The visual container if the one of the MLFC.
 * 
 * @author Juha
 */
public class FramesImpl extends XSmilesElementImpl implements VisualComponentService {
    private Container container;
    private XFramesMLFC mlfc;
    private boolean superReady = false;

    public FramesImpl(DocumentImpl owner, XFramesMLFC mlfc, String namespace, String tag) {
        super(owner, namespace, tag);
        this.mlfc = mlfc;
        mlfc.setFramesImpl(this);
    }

    /**
     * Initialize this element. This requires that the DOM tree is available.
     */
    public void init() {        
        Log.debug("FramesImpl: super.init()");
        super.init();        
        Log.debug("FramesImpl: super.init() exit");
        
        container = new JPanel();
        container.setVisible(false);
        container.setLayout(new BorderLayout());
        if (container == null)
            Log.debug("XFrames: Container null.");

        NodeList children = getChildNodes();
        Vector visuals = new Vector();
       

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof VisualComponentService) {
                visuals.addElement(child);
            }
        }

        try {
            if (visuals.size() > 1) {
                // TabbedPain 667
                JTabbedPane tb = new JTabbedPane();
                for (Enumeration e = visuals.elements(); e.hasMoreElements();) {
                    Element fi = (Element) e.nextElement();
                    String txt = fi.getAttribute("id");
                    if (txt == null || txt.equals("")) {
                        txt = "Untitled Document";
                    }
                    tb.add(txt, ((VisualComponentService) fi).getComponent());
                }
                container.add(tb);
            } else if (visuals.size() == 1) {
                Log.debug("Adding only one visual to frames; " + visuals.elementAt(0).toString());
                Component comp = ((VisualComponentService) visuals.elementAt(0)).getComponent();
                container.add(comp);
                comp.invalidate();
                container.validate();
            } else {
                mlfc.getMLFCListener().showErrorDialog(
                    "Error with XFrames",
                    "Viddy, s?no Andy, kun ei keksinyt muuta sanottavaa. The element under <frames> should be something visual, but it is in fact not.");
            }
        } catch (Exception e) {
            Log.error(e);
            mlfc.getMLFCListener().showErrorDialog(
                "Error with XFrames",
                "Maybe a classcast exception. FramesImpl had problems.");
        }
        if (container != null) {
            container.setVisible(true);
            container.invalidate();
            if (container.getParent() != null) {
                container.getParent().validate();
            } else {
                container.validate();
            }
        }
    }

    /**
     * Return the visual component for this extension element
     */
    public Component getComponent() {
        if (container == null)
            init();
        return container;
    }

    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize() {
        if (container == null)
            init();
        return container.getSize();
    }

    public void setZoom(double zoom) {
        Log.debug("Zoom does not work");
    }

    public void setVisible(boolean visible) {
        if (container == null)
            init();
        container.setVisible(visible);
    }

    public boolean getVisible() {
        if (container == null)
            init();
        return container.isVisible();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}
