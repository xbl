/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Vector;

import javax.swing.JComponent;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;

/**
 * Class GroupImpl
 * 
 * @author tjjalava
 * @since Jan 26, 2004
 * @version $Revision: 1.7 $, $Date: 2005/01/31 17:51:49 $
 */
public class GroupImpl extends StylableImpl implements VisualComponentService {

    private Container container;
    protected StackableComponent sc;
    protected int stackDynamicity = StackableComponent.STATIC;
    private CoreMLFC mlfc;

    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public GroupImpl(DocumentImpl ownerDocument, CoreMLFC mlfc, String namespaceURI, String tag) {
        super(ownerDocument, namespaceURI, tag);
        this.mlfc = mlfc;
    }

    /**
     * Initialize this element. This requires that the DOM tree is available.
     */
    public void init() {
        super.init();

        // get CSS style for child.
        CSSStyleDeclaration stylee = mlfc.getStyleSheet().getParsedStyle(this);

        NodeList children = getChildNodes();

        String dynamic = null;
        if (stylee != null) {
            dynamic = stylee.getPropertyValue("border");
        }
        Log.debug("Border property: " + dynamic);

        if (dynamic != null && dynamic.equals("dynamic"))
            stackDynamicity = StackableComponent.DYNAMIC;

        String compose = getAttribute("compose");

        if (compose.equals("vertical")) {
            setSC(StackableComponent.VERTICAL);
        } else {
            if (compose.equals("tabbed")) {
                setSC(StackableComponent.TABBED);
            } else {
                setSC(StackableComponent.HORIZONTAL);
            }
        }

        Vector ids = new Vector();

        // Add all elements in a row formation
        // In the future row is also to handle width and height specified
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof VisualComponentService) {
                Component c = ((VisualComponentService) child).getComponent();

                String id = ((Element) child).getAttribute("id");
                if (id == null || id.equals("")) {
                    id = "Untitled document";
                }

                int idNum = 1;
                String tmpId = id;
                while (ids.contains(tmpId)) {
                    tmpId = id + " (" + (idNum++) + ")";
                }
                id = tmpId;
                ids.addElement(id);

                Log.debug("Adding component : " + id);

                setSize((JComponent)c, mlfc.getStyleSheet().getParsedStyle((StylableElement)child));
                c.invalidate();
                sc.add(c, id);
            }
        }
        sc.getComponent().validate();
    }

    private void setSize(JComponent c, CSSStyleDeclaration stylee) {
        int width = 10;
        int height = 10;
        if (stylee != null) {
            Log.debug("STYLE: " + stylee.toString() + " STYLESHEET: " + mlfc.getStyleSheet().toString());
            String value = stylee.getPropertyValue("width");
            if (value!=null && value.length()>0) {
                width = StyleGenerator.getMeasure(stylee, "width");
            } else {
                width = mlfc.getContainer().getSize().width;
            }
            
            value = stylee.getPropertyValue("height");
            if (value!=null && value.length()>0) {
                height = StyleGenerator.getMeasure(stylee, "height");    
            } else {
                height = mlfc.getContainer().getSize().height;
            }
        }
        c.setMinimumSize(new Dimension(10, 10));
        int scaledWidth = (int) (mlfc.getContainer().getSize().width * (double) (((double) width) / 100));
        int scaledHeight = (int) (mlfc.getContainer().getSize().height * (double) (((double) height) / 100));
        Log.debug("Using xframes values: " + scaledWidth + " " + scaledHeight + " " + mlfc.getContainer().getSize().toString());
        c.setSize(new Dimension(scaledWidth - 10, scaledHeight - 10));
        c.setPreferredSize(new Dimension(scaledWidth - 20, scaledHeight - 20));
    }

    protected void setSC(int orientation) {
        sc = new StackableComponent(stackDynamicity, orientation, (XFramesMLFC) mlfc);
        sc.getComponent().addComponentListener(new ComponentAdapter() {

            public void componentShown(ComponentEvent e) {
                resizeChildren();
            }

            public void componentResized(ComponentEvent e) {
                resizeChildren();
            }
        });
    }

    private void resizeChildren() {
        NodeList children = getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof VisualComponentService) {
                JComponent c = (JComponent) ((VisualComponentService) child).getComponent();                
                setSize(c, mlfc.getStyleSheet().getParsedStyle((StylableElement) child));
                c.invalidate();
            }
        }
        sc.getComponent().validate();
        sc.getComponent().doLayout();        
    }

    public Component getComponent() {
        if (sc == null || sc.getComponent() == null) {
            init();
        }
        return sc.getComponent();
    }

    public Dimension getSize() {
        if (sc == null || sc.getComponent() == null) {
            init();
        }

        return sc.getComponent().getSize();
    }

    public void setZoom(double zoom) {
        Log.debug("Zoom doesn't work");
    }

    public void setVisible(boolean visible) {
        if (sc == null || sc.getComponent() == null) {
            init();
        }
        sc.getComponent().setVisible(visible);
    }

    public boolean getVisible() {
        if (sc == null || sc.getComponent() == null) {
            init();
        }
        return sc.getComponent().isVisible();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}