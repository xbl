/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.mlfc.CoreMLFC;

/**
 * ColumnElementImpl Go through all first level children, and add them to own container, with boxlayout and Y_AXIS
 * 
 * @author Juha
 * @deprecated &lt;column&gt; is not part of the XFrames specs anymore, it's replaced by the "compose"-attribute of &lt;group&gt;
 */
public class ColumnImpl extends RowImpl {

    public ColumnImpl(DocumentImpl owner, CoreMLFC mlfc, String namespace, String tag) {
        super(owner, mlfc, namespace, tag);
        System.err.println("<column> is deprecated, use <group compose=\"vertical\">");
    }

    /*
     * protected void setSC() { sc=new StackableComponent(stackDynamicity, StackableComponent.HORIZONTAL); }
     */
    public void init() {
        setSC(StackableComponent.VERTICAL);
        super.init();
    }

}
