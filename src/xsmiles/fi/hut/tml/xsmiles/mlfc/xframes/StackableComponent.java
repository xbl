package fi.hut.tml.xsmiles.mlfc.xframes;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XMenuItem;

/**
 * A Stackable component object to ease creation of xframes The main function is to utilize static borders or resizable
 * borders. (JSplitPane, or BoxLayout)
 * 
 * @author juha
 */
public class StackableComponent {

	private boolean useMenus;
    public static int STATIC = 0;
    public static int DYNAMIC = 1;
    public final static int HORIZONTAL = 0;
    public final static int VERTICAL = 1;
    public final static int TABBED = 2;
    private int style = STATIC;
    private int orientation = HORIZONTAL;
    private Container container;
    private JTabbedPane tp;
    private Vector children = new Vector();
    private JSplitPane last = null;
    private XFramesMLFC mlfc;
    private MenuListener menuListener;
    private XMenuItem selected;
    
    private class MenuListener implements ActionListener {
    	
    	private Container cont;
    	private CardLayout cards;    	
    	
    	public MenuListener(Container cont) {
    		this.cont = cont;
    		this.cards = (CardLayout) cont.getLayout();
    	}
		
		public void actionPerformed(ActionEvent e) {
			cards.show(container, e.getActionCommand());
			if (selected!=null) {
				selected.selectItem(false);
			}
			selected = (XMenuItem) e.getSource();
			selected.selectItem(true);			
		}    	
    }

    /**
     * @param s
     *                  style (STATIC, DYNAMIC)
     * @param o
     *                  orientation (HORIZONTAL, VERTICAL)
     */
    public StackableComponent(int s, int o) {
    	this(s,o,null);        
    }
    
    public StackableComponent(int s, int o, XFramesMLFC mlfc) {
    	this.mlfc = mlfc;
    	if (mlfc!=null) {
    		useMenus = new Boolean(mlfc.getMLFCListener().getProperty("xframes/menuswhentabbed")).booleanValue();
    	} else {
    		useMenus = false;
    	}
    	style = s;
        orientation = o;
        if (orientation == TABBED) {        	        	
        	if (useMenus) {
        		container = new JPanel(new CardLayout());
        		menuListener = new MenuListener(container);
        	} else {       		
        		container = new JPanel(new BorderLayout());
        		container.add(tp = new JTabbedPane(),BorderLayout.CENTER);
        	}
        } else {
            if (style == STATIC) {
                if (orientation == HORIZONTAL) {
                    container = Box.createHorizontalBox();
                } else if (orientation == VERTICAL) {                
                    container = Box.createVerticalBox();
                }
            } else {
                container = new JPanel();
            }
        }
    }
    
    public void add(Component c, String id) {        
        children.addElement(c);
        if (orientation == TABBED) {
        	if (useMenus) {
        		container.add(c,id);        		
        		XMenuItem item = mlfc.addFrameToMenu(id);        		
        		item.addActionListener(menuListener);    
        		if (container.getComponentCount()==1) {
        			item.selectItem(true);
        			selected = item;
        		}        		
        	} else {        		
        		tp.addTab(id, c);
        		tp.setSelectedIndex(0);
        	}
        	Log.debug("SC: added "+id);        		
        } else {
            if (style == STATIC) {                
                container.add((Component) c);
            } else {
                if (children.size() == 1) {
                    container.add((Component) c);
                    return;
                }

                if (orientation == HORIZONTAL) {
                    splice(JSplitPane.HORIZONTAL_SPLIT, c);
                } else if (orientation == VERTICAL) {
                    splice(JSplitPane.VERTICAL_SPLIT, c);
                }
            }
        }        
    }

    /**
     * Add a component to the end of the stack
     *  
     */
    public void add(Component c) {
        add(c,"");
    }

    private void splice(int or, Component c) {
        if (children.size() == 2) {
            container.removeAll();
            JSplitPane s = new JSplitPane(or, (Component) children.elementAt(0), (Component) children.elementAt(1));
            last = s;
            container.add(last);
        } else if (children.size() > 1) {
            Component tmp = last.getRightComponent();
            JSplitPane s = new JSplitPane(or, tmp, c);
            last.setRightComponent(s);
            last = s;
        }
        container.doLayout();
    }

    /**
     * Set the width of the border, doesn't do anything yet
     */
    public void setBorderWidth(int width) {
        Log.debug("set border width not implemented");
    }

    public Component getComponent() {
        return container;
    }

    public int getOrientation() {
        return orientation;
    }
}
