/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xframes;

import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * Class HeadImpl
 * 
 * @author tjjalava
 * @since Sep 13, 2004
 * @version $Revision: 1.1 $, $Date: 2004/09/14 12:12:10 $
 */
public class HeadImpl extends XSmilesElementImpl {

    private XFramesMLFC mlfc;
    private XSmilesElementImpl style;
    //TODO: multiple styles ?

    String title;

    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public HeadImpl(DocumentImpl ownerDocument, XFramesMLFC mlfc, String namespaceURI, String qualifiedName) {
        super(ownerDocument, namespaceURI, qualifiedName);
        this.mlfc = mlfc;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.XSmilesElementImpl#init()
     */
    public void init() {
        String ns = getNamespaceURI();
        NodeList list = getElementsByTagNameNS(ns, "title");
        if (list.getLength() > 0) {
            XSmilesElementImpl titleElem = (XSmilesElementImpl) list.item(0);
            title = titleElem.getFirstChild().getNodeValue();
            setTitle(title);
        }

        list = getElementsByTagNameNS(ns, "style");
        if (list.getLength() > 0) {
            style = (XSmilesElementImpl) list.item(0);
            String source = style.getAttribute("source");
            if (source != null && !source.equals(""))
                try {
                    mlfc.getStyleSheet().addXMLStyleSheet(new URL(mlfc.getXMLDocument().getLink().getURL(), source));
                } catch (Exception mue) {
                    Log.error(mue);
                }

            // add inline CSS
            if (style.getAttribute("type").equalsIgnoreCase("text/css")) {
                mlfc.getStyleSheet().addXMLStyleSheet(style.getText(), mlfc.getXMLDocument().getLink().getURL());
            }
        }
    }

    private void setTitle(String title) {
        if (mlfc.isPrimary() && title != null && title.length() > 0) {
            mlfc.setTitle(title);
        }
    }
}