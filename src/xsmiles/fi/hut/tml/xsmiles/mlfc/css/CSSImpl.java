/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css;

import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.Selectors;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;


import com.steadystate.css.*;
import org.w3c.dom.css.*;
import org.w3c.css.sac.CSSException;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;

import java.util.Vector;
import java.net.URL;

/**
 * This class is the interface to MLFCs for 1. reading the style of a single
 * element. 2. Adding new stylesheet references (both author + user agent)
 *
 * @author Mikko Honkala
 * @author Alessandro Cogliati
 */
public class CSSImpl implements XSmilesStyleSheet {
    private URL baseURL;
    private URL defaultCssLoc;
    private Vector styleSheetListUser;
    private Vector styleSheetListDefault;
    private CSSParser parser;
    private MediaQueryEvaluator mqe;
    
    protected Selectors cssSelectors;
    
    
    public CSSImpl(MediaQueryEvaluator mqe, Document doc) {
        super();
        this.mqe = mqe;
        styleSheetListDefault = new Vector();
        styleSheetListUser = new Vector();
        parser=new CSSParser(this, mqe,this.getMLFC(doc));
        this.createCSSSelectors(doc);
    }
    MLFC getMLFC(Document doc)
    {
       return ((ExtendedDocument)doc).getHostMLFC();
    }

    public CSSImpl() {
        super();
        Log.error("CSSImpl NEVER CALL THIS CONSTRUCTOR!"); // why is this here still?
        /*
        this.mqe = null;
        styleSheetListDefault = new Vector();
        styleSheetListUser = new Vector();
        parser=new CSSParser(this,mqe);
        */
    }
    
    public CSSStyleDeclaration getParsedStyle(StylableElement elem){
       
        CSSStyleDeclaration dec = this.getParsedStyle(elem, null);
        return dec;
    }
    
    protected void createCSSSelectors(Document doc)
    {
        boolean HTMLMode;
        if (doc instanceof ExtendedDocument)
            HTMLMode = ((ExtendedDocument)doc).isHTMLDocument();
        else HTMLMode = false;
        
        if (HTMLMode) this.cssSelectors=new CSSSelectorsHTML();
        else this.cssSelectors=new CSSSelectors();
    }
    
    public Selectors getCSSSelectors()
    {
        return this.cssSelectors;
    }
    
    /**
     * This method is the main entry point for the MLFCs. It gets the style for a single element.
     */
    public CSSStyleDeclaration getParsedStyle(StylableElement elem, String pseudoelement) {
        //Log.debug("getParsedStyle("+elem+")");
        try {
            XSmilesCSSStyleDeclarationImpl style = new XSmilesCSSStyleDeclarationImpl(null);
            org.w3c.dom.Element el = (org.w3c.dom.Element) elem;
            String elementName = el.getLocalName();
            XSmilesCSSStyleDeclarationImpl parentStyle=null;
            Node parentNode;
            // for the pseudoelement, the style parent is the given element
            // otherwise it's the parent node
            if (pseudoelement!=null) parentNode=elem;
            else
                parentNode = el.getParentNode();
            // first do inheritence from the parent's style
            if (parentNode instanceof StylableElement) {
                parentStyle = (XSmilesCSSStyleDeclarationImpl)((StylableElement)parentNode).getStyle();
            }
            /*
            if (parentStyle==null) {
                Log.error("Parent style for "+elementName+" was null. Parent Element: "+parentNode);
            }*/
            //			if (parentStyle!=null)
            //				style.inherit(parentStyle);
            // for all rules, check which ones apply to this element, and combine them into 'style'
            for (int z=0; z<styleSheetListDefault.size(); z++) {
                CSSStyleSheet ss = (XSmilesCSSStyleSheetImpl)styleSheetListDefault.elementAt(z);
                if (ss!=null)
                    matchRules(ss,elem,pseudoelement,style);
            }
            for (int z=0; z<styleSheetListUser.size(); z++) {
                CSSStyleSheet ss = (XSmilesCSSStyleSheetImpl)styleSheetListUser.elementAt(z);
                if (ss!=null)
                    matchRules(ss,elem,pseudoelement,style);
            }
            
            // if the element supports the style attribute, it will return the contents
            //String localstyle = elem.getStyleAttrValue();
            String localstyle = this.cssSelectors.getStyleAttrValue(elem);
            if (localstyle!=null&&localstyle.length()>0) {
                // convert to CSSStyleDeclaration, and combine
                XSmilesCSSStyleDeclarationImpl localCSS = parser.parseStyleAttrValue(localstyle);
                style.combineFrom(localCSS);
                //Log.debug(elem.getLocalName()+"* localCSS : "+localCSS+" combined:"+style);
            }
            // compute relative values
            if (parentStyle != null)
                style.computeRelativeValues(elem,null,style,parentStyle);
            /*
            else
                Log.debug("Relative values not computed. "+parentNode+"'s style is null.");
             */
            
//            System.out.println(XSmilesCSSStyleDeclarationImpl.count+ "---------------------------");
            //Log.debug(elem.getLocalName()+"* combined:"+style);
            return style;
        }
        catch (Exception e) {
            Log.error(e);
        }
        return null;
    }
    
    protected void matchRules(CSSStyleSheet ss, StylableElement elem, String pseudoelement, XSmilesCSSStyleDeclarationImpl style) {
        // TODO: check class type && null
        XSmilesCSSRuleListImpl allRules=(XSmilesCSSRuleListImpl)ss.getCssRules();
        for (int i = 0; i<allRules.getLength(); i++) {
            Object r = allRules.item(i);
            if (! (r instanceof XSmilesCSSStyleRule))
            {
                Log.error(r+" was not instanceof XSmilesCSSStyleRule");
                
            }
            else
            {
                XSmilesCSSStyleRule rule = (XSmilesCSSStyleRule)allRules.item(i);
                if (rule.match(elem,this.getCSSSelectors())) {
                    //Log.debug("*** Rule "+rule+" matches "+elem);
                    XSmilesCSSStyleDeclarationImpl fromStyle = (XSmilesCSSStyleDeclarationImpl)rule.getStyle();
                    style.combineFrom(fromStyle);
                }
            }
        }
    }
    
    public void prepareStyleSheet(CSSStyleSheet ss)
    {
            XSmilesCSSRuleListImpl rules = (XSmilesCSSRuleListImpl)ss.getCssRules();
            rules.setMediaQueryEvaluator(mqe);
            rules.sort();
    }
    
    /** Add a new default (User Agent) stylesheet */
    public void addXMLDefaultStyleSheet(URL defaultUrl) {
        try {
            CSSStyleSheet ss = parser.parse(defaultUrl, CSSParser.STYLESHEET_USERAGENT, 0,styleSheetListDefault,styleSheetListUser);
            defaultCssLoc = defaultUrl;
            this.prepareStyleSheet(ss);
        }
        catch (Exception e) {
            Log.error(e);
        }
        
    }
    
    /** Add a new author stylesheet */
    public void addXMLStyleSheet(URL stylesheetUrl) {
        try {
            CSSStyleSheet ss = parser.parse(stylesheetUrl, CSSParser.STYLESHEET_AUTHOR, 0,styleSheetListDefault,styleSheetListUser);
            this.prepareStyleSheet(ss);
        }
        catch (Exception e) {
            Log.error("While adding CSS stylesheet: "+e.getMessage());
        }
        
    }
    /** Add a new author stylesheet */
    public void addXMLStyleSheet(String stylesheetText,URL baseURL) {
        int p = 2;
        try {
            // pass the string to the parser
            CSSStyleSheet ss = parser.parse(baseURL,stylesheetText, p, 0,styleSheetListDefault,styleSheetListUser);
            this.prepareStyleSheet(ss);
        }
        catch (Exception e) {
            Log.error(e);
        }
        
    }
    

    
    
}

