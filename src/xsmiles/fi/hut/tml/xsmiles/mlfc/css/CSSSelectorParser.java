/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css;

import java.io.IOException;
import java.io.StringReader;

import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.SelectorList;

import fi.hut.tml.xsmiles.Log;

/**
 * Class CSSSelectorParser
 * 
 * @author tjjalava
 * @since Apr 14, 2004
 * @version $Revision: 1.3 $, $Date: 2006/01/27 09:32:45 $
 */
public class CSSSelectorParser {

    // MH: mem test
    //private static XSmilesCSSOMParser parser = new XSmilesCSSOMParser();

    /**
     * Parses CSS selector instances from a comma separated list of selectors
     * @param selectors list of selectors
     * @return SelectorList-instance containing Selector objects.
     */
    public static SelectorList parseSelectors(String selectors) {
        // MH: mem test
        XSmilesCSSOMParser parser = new XSmilesCSSOMParser();
        try {
            synchronized (parser) {
                return parser.parseSelectors(new InputSource(new StringReader(selectors)));
            }
        } catch (Throwable e) {
            // shouldn't be possible with StringReader
            Log.error("parseSelectors: "+e.getMessage());
            return null;
        }
    }
}
