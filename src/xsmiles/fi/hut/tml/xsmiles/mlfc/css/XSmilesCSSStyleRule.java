/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.css;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.Selectors;

import com.steadystate.css.dom.*;
import com.steadystate.css.parser.SelectorListImpl;
import java.util.Vector;
import org.w3c.dom.css.*;
import org.w3c.css.sac.*;
import org.w3c.dom.*;


/**
 * Implements a single style rule with specificity calculation
 * This class extends SS CSS style rule, with the following addition:
 * - Ability to parse the selector and count the specificity value
 *
 * @author Mikko Honkala
 */
 public class XSmilesCSSStyleRule extends com.steadystate.css.dom.CSSStyleRuleImpl implements CSSStyleRule {
    
	
	/** no of id conditionals within selector */
	int idcount;
	
	/** no of classes in the selector */
	int classcount;
	
	/** no of elements in the selector */
	int elementcount;
	
	/** the initial order within a stylesheet category */
	int stylesheetorder;

	/** the origin of the stylesheet, the lower, the less powerful */
	int origin;
	
	private static final int n = 100;
	private static final int n1 = 1000;
	private static final int n2 = n*n;
	private static final int n3 = n*n*n;
	private static final int n4 = n*n*n*n;

	
    public XSmilesCSSStyleRule(CSSStyleSheetImpl parentStyleSheet, CSSRule parentRule, SelectorList selectors) {
		super(parentStyleSheet,parentRule, selectors);
    }
	
	
	/** check whether this style matches a certain element */
	public boolean match(Element elem, Selectors sel)
	{
                boolean HTMLMode;
                Document doc = elem.getOwnerDocument();
                if (doc instanceof ExtendedDocument)
                    HTMLMode = ((ExtendedDocument)doc).isHTMLDocument();
                else HTMLMode=false;
		SelectorList list = this.getSelectorList();		
//		for (int i=0;i<list.getLength();i++)
//		{
			Selector s = list.item(0);
			boolean matches = sel.selectorMatchesElem(s,elem);        
			if (matches) return true;
//		}
		return false;
	}
	
	/** Return the selectors for this rule */
	public SelectorList getSelectorList()
	{
		return this._selectors;
	}

	/** Return the specificity for this rule */
	public long getSpecificity()
	{
		return origin*n4 + idcount*n3 + 
			classcount*n2 +
			elementcount*n1 +
			stylesheetorder;
	}	
	
	public String toString()
	{
		return "["+origin+","+idcount+","+classcount+","+elementcount+","+stylesheetorder+"="+getSpecificity()+"]"+super.toString();
	}
	
	void setOrigin(int a_origin)
	{
		this.origin=a_origin;
	}
	
	void setStyleSheetOrder(int a_order)
	{
		this.stylesheetorder=a_order;
	}
	
	/** Count the CSS specificity of this rule */	
	void countSpecificity(){
	
		int elementcount=0, idcount=0, classcount=0;
		SelectorList list = this.getSelectorList();
		Selector s = list.item(0);
		countCondition(s);
		
	}
	
	void countCondition(Selector sel)
	{
		if (sel.getSelectorType() == Selector.SAC_ELEMENT_NODE_SELECTOR ){
			elementcount++;	
		}
		if (sel.getSelectorType() == Selector.SAC_CONDITIONAL_SELECTOR ){
			ConditionalSelector condSel = (ConditionalSelector) sel;
			countCondition(condSel.getSimpleSelector());
			Condition cond = condSel.getCondition();
			countRecoursiveCondition(cond);
		}	
		if (sel.getSelectorType() == Selector.SAC_DESCENDANT_SELECTOR ){
			countDescendantSelector(sel);	
		}
		if (sel.getSelectorType() == Selector.SAC_CHILD_SELECTOR ){
			countDescendantSelector(sel);	
		}
		if (sel.getSelectorType() == Selector.SAC_ELEMENT_WITH_PSEUDO_ELEMENT_SELECTOR  ){
			countDescendantSelector(sel);	
		}
	
	}
	
	void countRecoursiveCondition(Condition condition){
					
		if (condition.getConditionType() == Condition.SAC_AND_CONDITION){
			CombinatorCondition comCon = (CombinatorCondition)condition;
			countRecoursiveCondition(comCon.getFirstCondition());
			countRecoursiveCondition(comCon.getSecondCondition());
			
		}else{
			countCondition(condition);
		}
	}
	
	void countCondition(Condition condition){
		
		if (condition.getConditionType() == Condition.SAC_CLASS_CONDITION){
			classcount++;
		}
		if (condition.getConditionType() == Condition.SAC_ID_CONDITION){
			idcount++;
		}
		if (condition.getConditionType() == Condition.SAC_PSEUDO_CLASS_CONDITION){
			classcount++;
		}
		if (condition.getConditionType() == Condition.SAC_POSITIONAL_CONDITION){
			classcount++;
		}
		if (condition.getConditionType() == Condition.SAC_ATTRIBUTE_CONDITION){
			classcount++;
		}
		
	}

	void countDescendantSelector(Selector sel)
	{
		DescendantSelector descSel = (DescendantSelector) sel;
		countCondition(descSel.getSimpleSelector());
		countCondition(descSel.getAncestorSelector());	
	}

}
