/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.css.value;
package fi.hut.tml.xsmiles.mlfc.css.value;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

//import org.apache.batik.css.CSSOMReadOnlyStyleDeclaration;
//import org.apache.batik.css.CSSOMReadOnlyValue;
//import org.apache.batik.css.HiddenChildElementSupport;
//import org.apache.batik.css.value.ImmutableValue;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.ViewCSS;
import org.w3c.css.sac.InputSource;
import java.io.StringReader;

/**
 * This class provides a relative value resolver for the 'font-weight' CSS
 * property.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: FontWeightResolver.java,v 1.9 2003/06/29 06:00:47 ronald Exp $
 */
public class FontWeightResolver extends AbstractResolver implements RelativeValueResolver {

	public static final float WeightDefault= 400f;
    float pFloatVal = WeightDefault;

    /**
     * Whether the handled property is inherited or not.
     */
    public boolean isInheritedProperty() {
	return true;
    }

    /**
     * Returns the name of the handled property.
     */
    public String getPropertyName() {
	return CSSConstants.CSS_FONT_WEIGHT_PROPERTY;
    }

    /**
     * Returns the default value for the handled property.
     */
//    public CSSOMReadOnlyValue getDefaultValue() {
//	return new CSSOMReadOnlyValue(FontSizeFactory.MEDIUM_VALUE);
//    }
    
    /**
     * Resolves the given value if relative, and puts it in the given table.
     * @param element The element to which this value applies.
     * @param pseudoElement The pseudo element if one.
     * @param view The view CSS of the current document.
     * @param styleDeclaration The computed style declaration.
     * @param value The cascaded value.
     * @param priority The priority of the cascaded value.
     * @param origin The origin of the cascaded value.
     */
    public void resolveValue(Element element,
			     String pseudoElement,
			     //ViewCSS view,
			     XSmilesCSSStyleDeclarationImpl styleDeclaration,
			     //CSSValue value,
			     XSmilesCSSStyleDeclarationImpl parentStyle
				 //,boolean isImportant
			     //,String priority,
			     //int origin
				 ) 
	{
		CSSValue value = styleDeclaration.getPropertyCSSValue(getPropertyName());
		if (value instanceof CSSPrimitiveValue){
			CSSPrimitiveValue pValue = (CSSPrimitiveValue)value;
			if (pValue.getPrimitiveType()==CSSPrimitiveValue.CSS_IDENT){
//				System.out.println("------"+pValue.getPrimitiveType()+"----ident---------------------------"+pValue);
				if ((pValue.getStringValue()).equals("normal")){
					this.setNewNumberProperty(400,styleDeclaration);			
				}else if ((pValue.getStringValue()).equals("bold")){
					this.setNewNumberProperty(700,styleDeclaration);
				}else if ((pValue.getStringValue()).equals("bolder")){
					setBolderOrLighterProperty(styleDeclaration, parentStyle,pValue, (100));
				}else if ((pValue.getStringValue()).equals("lighter")){
					setBolderOrLighterProperty(styleDeclaration, parentStyle,pValue, (-100));
				}
				this.resolveValue(element,pseudoElement,styleDeclaration,parentStyle);
					
			}else if (pValue.getPrimitiveType()==CSSPrimitiveValue.CSS_NUMBER){
		
				float fValue = pValue.getFloatValue(CSSPrimitiveValue.CSS_NUMBER);	
				if (!(fValue>=100 && fValue<=900)){
					if (parentStyle == null) pValue.setFloatValue(CSSPrimitiveValue.CSS_NUMBER,WeightDefault);
					else{
						CSSValue parentValue = parentStyle.getPropertyCSSValue(getPropertyName());
						if (parentValue instanceof CSSPrimitiveValue){
							if (parentValue==null) pValue.setFloatValue(CSSPrimitiveValue.CSS_NUMBER,WeightDefault);
							else{
								XSmilesCSSValueImpl pParentValue = (XSmilesCSSValueImpl) parentValue;
								float pFloatVal = pParentValue.getFloatValue(CSSPrimitiveValue.CSS_NUMBER);
								this.setNewNumberProperty((int)(pFloatVal + .5),styleDeclaration);		
							}
						}
					}
				}
			}	
		}	
    }
	
	void setNewNumberProperty(int numberValue,XSmilesCSSStyleDeclarationImpl styleDec){	
		try{
			CSSPrimitiveValue newValue = (CSSPrimitiveValue)parser.parsePropertyValue(new InputSource(new StringReader(""+numberValue)));	
			styleDec.removeProperty(this.getPropertyName());
			styleDec.setProperty(this.getPropertyName(),newValue,AbstractResolver.getIsImportant(styleDec,getPropertyName()));
//		System.out.println("------"+styleDec.getPropertyValue(getPropertyName()) +"------------------------------");	
		} catch (Exception e){
			Log.error(e);
		}		
	}
	
	void setBolderOrLighterProperty(XSmilesCSSStyleDeclarationImpl styleDec,
										   XSmilesCSSStyleDeclarationImpl parentStyleDec,
										   CSSPrimitiveValue pVal,
										   int factor)
	{ 
//        System.out.println("------"+pVal.getPrimitiveType()+"-//////////////////////////////-"+pVal);
        if (parentStyleDec != null){
            CSSValue parentValue = parentStyleDec.getPropertyCSSValue(getPropertyName());
            if (parentValue != null){
                if (parentValue instanceof CSSPrimitiveValue){
                XSmilesCSSValueImpl pParentValue = (XSmilesCSSValueImpl) parentValue;
                pFloatVal = pParentValue.getFloatValue(CSSPrimitiveValue.CSS_NUMBER);
                }
            }
        } 
        float newVal = pFloatVal+factor;
        this.setNewNumberProperty((int)(newVal + .5), styleDec);	  	
	}
}
