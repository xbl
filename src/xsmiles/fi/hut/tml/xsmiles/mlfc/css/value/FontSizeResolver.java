/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.css.value;
package fi.hut.tml.xsmiles.mlfc.css.value;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

//import org.apache.batik.css.CSSOMReadOnlyStyleDeclaration;
//import org.apache.batik.css.CSSOMReadOnlyValue;
//import org.apache.batik.css.HiddenChildElementSupport;
//import org.apache.batik.css.value.ImmutableValue;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.ViewCSS;
import org.w3c.css.sac.InputSource;
import java.io.StringReader;
/**
 * This class provides a relative value resolver for the 'font-size' CSS
 * property.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: FontSizeResolver.java,v 1.4 2002/04/11 12:24:11 ale Exp $
 */
public class FontSizeResolver extends AbstractResolver
    implements RelativeValueResolver {

    /**
     * Whether the handled property is inherited or not.
     */
    public boolean isInheritedProperty() {
	return true;
    }

    /**
     * Returns the name of the handled property.
     */
    public String getPropertyName() {
	return CSSConstants.CSS_FONT_SIZE_PROPERTY;
    }

    /**
     * Returns the default value for the handled property.
     */
//    public CSSOMReadOnlyValue getDefaultValue() {
//	return new CSSOMReadOnlyValue(FontSizeFactory.MEDIUM_VALUE);
//    }
    
    /**
     * Resolves the given value if relative, and puts it in the given table.
     * @param element The element to which this value applies.
     * @param pseudoElement The pseudo element if one.
     * @param view The view CSS of the current document.
     * @param styleDeclaration The computed style declaration.
     * @param value The cascaded value.
     * @param priority The priority of the cascaded value.
     * @param origin The origin of the cascaded value.
     */
    public void resolveValue(Element element,
			     String pseudoElement,
			     //ViewCSS view,
			     XSmilesCSSStyleDeclarationImpl styleDeclaration,
			     //CSSValue value,
			     XSmilesCSSStyleDeclarationImpl parentStyle
				 //,boolean isImportant
			     //,String priority,
			     //int origin
				 ) 
	{
	
		float size = 0f;
	
		CSSValue value = styleDeclaration.getPropertyCSSValue(getPropertyName());
		if (value instanceof CSSPrimitiveValue)
		{
			CSSPrimitiveValue pValue = (CSSPrimitiveValue)value;
			// percentage values
			if (pValue.getPrimitiveType()==CSSPrimitiveValue.CSS_PERCENTAGE)
			{
				CSSValue parentValue = parentStyle.getPropertyCSSValue(getPropertyName());
				// if the parent does not have font value, set it as 0
				if (parentValue==null) pValue.setFloatValue(CSSPrimitiveValue.CSS_NUMBER,0f);
				else
				{
					CSSValue newValue = this.resolvePercentage(pValue,parentValue,styleDeclaration);
				}		
			}else if (pValue.getPrimitiveType()==CSSPrimitiveValue.CSS_IDENT){
				if ((pValue.getStringValue()).equals("xx-small")){
					size = 4f;
				}else if ((pValue.getStringValue()).equals("x-small")){
					size = 5f;
				}else if ((pValue.getStringValue()).equals("small")){	
					size = 8f;
				}else if ((pValue.getStringValue()).equals("medium")){
					size = 12f;
				}else if ((pValue.getStringValue()).equals("large")){	
					size = 18f;	
				}else if ((pValue.getStringValue()).equals("x-large")){
					size = 27f;
				}else if ((pValue.getStringValue()).equals("xx-large")){	
					size = 40f;	
				}	
				try{
					CSSPrimitiveValue newValue = (CSSPrimitiveValue)parser.parsePropertyValue(new InputSource(new StringReader(""+size)));	
					styleDeclaration.removeProperty(this.getPropertyName());
					styleDeclaration.setProperty(this.getPropertyName(),newValue,AbstractResolver.getIsImportant(styleDeclaration,getPropertyName()));
				} catch (Exception e){
					Log.error(e);
				}
				if ((pValue.getStringValue()).equals("smaller")){
					CSSValue parentValue = parentStyle.getPropertyCSSValue(getPropertyName());
					// if the parent does not have font value, set it as 0
					if (parentValue==null) pValue.setFloatValue(CSSPrimitiveValue.CSS_NUMBER,0f);
					else
					{
						createNewValue(67,parentValue,styleDeclaration);
					}
				}else if ((pValue.getStringValue()).equals("larger")){
					CSSValue parentValue = parentStyle.getPropertyCSSValue(getPropertyName());
					// if the parent does not have font value, set it as 0
					if (parentValue==null) pValue.setFloatValue(CSSPrimitiveValue.CSS_NUMBER,0f);
					else
					{
						createNewValue(150,parentValue,styleDeclaration);	
					}	
				}
			} 
		}				 				 
	}
	
	public void createNewValue(int percentage,CSSValue parentV,XSmilesCSSStyleDeclarationImpl styleDec ){
		
		try{
			CSSPrimitiveValue newValue = (CSSPrimitiveValue)parser.parsePropertyValue(new InputSource(new StringReader(""+ percentage +"%")));
			CSSValue nValue = this.resolvePercentage(newValue,parentV,styleDec);
		} catch (Exception e){
			Log.error(e);
		}	
	
	}
	   
}
