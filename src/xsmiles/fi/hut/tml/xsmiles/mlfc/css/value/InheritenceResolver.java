/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.css.value;
package fi.hut.tml.xsmiles.mlfc.css.value;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import com.steadystate.css.dom.Property;

//import org.apache.batik.css.CSSOMReadOnlyStyleDeclaration;
//import org.apache.batik.css.CSSOMReadOnlyValue;
//import org.apache.batik.css.HiddenChildElementSupport;
//import org.apache.batik.css.value.ImmutableValue;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.ViewCSS;

/**
 * This class provides a relative value resolver for the 'font-size' CSS
 * property.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: InheritenceResolver.java,v 1.5 2002/04/25 12:37:05 ale Exp $
 */
public class InheritenceResolver extends AbstractResolver
    implements RelativeValueResolver {


	protected String propertyName;
	public InheritenceResolver(String property)
	{
		this.propertyName = property;
	}
    /**
     * Whether the handled property is inherited or not.
     */
    public boolean isInheritedProperty() {
	return true;
    }

    /**
     * Returns the name of the handled property.
     */
    public String getPropertyName() {
	return propertyName;
    }

    
    /**
     * Resolves the given value if relative, and puts it in the given table.
     * @param element The element to which this value applies.
     * @param pseudoElement The pseudo element if one.
     * @param view The view CSS of the current document.
     * @param styleDeclaration The computed style declaration.
     * @param value The cascaded value.
     * @param priority The priority of the cascaded value.
     * @param origin The origin of the cascaded value.
     */
    public void resolveValue(Element element,
			     String pseudoElement,
			     //ViewCSS view,
			     XSmilesCSSStyleDeclarationImpl styleDeclaration,
			     //CSSValue value,
			     XSmilesCSSStyleDeclarationImpl parentStyle
//				 ,boolean isImportant
			     //,String priority,
			     //int origin
				 ) 
	{
		inherit(parentStyle,styleDeclaration, this.getPropertyName());
    }
    /**
     * Inherit values from parent style. NOTE! use this only for empty style
     * @author MH
     */
     protected static void inherit(XSmilesCSSStyleDeclarationImpl parentStyle, 
	 							XSmilesCSSStyleDeclarationImpl localStyle, 
								String propertyName)
     {
     	if (parentStyle==null) return;
		// do not inherit, if local value is present
		if (localStyle.getProperty(propertyName)!=null) return;
   		Property parentProperty = parentStyle.getProperty(propertyName);
   		if (parentProperty==null) return;
   		// inheritence should not keep the value of the important attribute
   		localStyle.setProperty(propertyName,parentProperty.getValue(),false);
		//Log.debug("Inherited: " +parentProperty.getValue());
     }
	

}
