/*
 * CSSStyleDeclarationImpl.java
 *
 * Steady State CSS2 Parser
 *
 * Copyright (C) 1999, 2002 Steady State Software Ltd.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * To contact the authors of the library, write to Steady State Software Ltd.,
 * 49 Littleworth, Wing, Buckinghamshire, LU7 0JX, England
 *
 * http://www.steadystate.com/css/
 * mailto:css@steadystate.co.uk
 *
 * $Id: XSmilesCSSStyleDeclarationImpl.java,v 1.29 2006/05/19 11:36:00 ale Exp $
 */


package fi.hut.tml.xsmiles.mlfc.css;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSStyleDeclaration;

import com.steadystate.css.dom.CSSStyleDeclarationImpl;
import com.steadystate.css.dom.DOMExceptionImpl;
import com.steadystate.css.dom.Property;

import fi.hut.tml.xsmiles.csslayout.CSS_Property;
import fi.hut.tml.xsmiles.mlfc.css.value.FontSizeResolver;
import fi.hut.tml.xsmiles.mlfc.css.value.FontWeightResolver;
import fi.hut.tml.xsmiles.mlfc.css.value.InheritenceResolver;
import fi.hut.tml.xsmiles.mlfc.css.value.RelativeValueResolver;

/**
 * @author David Schweinsberg
 * @version $Release$
 */
/**
 * MH: This class has been modified for use with X-Smiles browser.
 * Reason: this class can efficiently combine two or more StyleDeclarations
 */
public class XSmilesCSSStyleDeclarationImpl extends CSSStyleDeclarationImpl
{
    
    public XSmilesCSSStyleDeclarationImpl(CSSRule parentRule)
    {
        super(parentRule);
        _parentRule = parentRule;
        //    	Log.debug( "Created declaration. Parent:" +(parentRule==null?"null":"non-null"));
    }
    
    /** the vector containing names of all inherited properties */
    static Vector valueResolvers;
    
    static
    {
        valueResolvers = new Vector(30);
        // These properties are inherited by default
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_COLOR_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_CAPTION_SIDE_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_FAMILY_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_SIZE_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_STRETCH_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_STYLE_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_VARIANT_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSSConstants.CSS_FONT_WEIGHT_PROPERTY));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.WHITESPACE));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.TEXT_ALIGN));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.TEXT_DECORATION));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.TEXT_TRANSFORM));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.TEXT_INDENT));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.LIST_STYLE_IMAGE));
        valueResolvers.addElement(new InheritenceResolver(CSS_Property.LIST_STYLE_POSITION));
        
        // The font-size resolver
        valueResolvers.addElement(new FontSizeResolver());
        valueResolvers.addElement(new FontWeightResolver());
        // TODO: add rest
    }
    
    /**
     * Returns the property at certain index
     * @author MH
     */
    Property getProperty(int index)
    {
        return (Property) _properties.elementAt(index);
    }
    
    /**
     * Take in a style declaration, and combine all attributes to this style declaration
     * @author MH
     */
    public void combineFrom(XSmilesCSSStyleDeclarationImpl fromStyle)
    {
        for (int i = 0; i<fromStyle.getLength(); i++)
        {
            Property prop = fromStyle.getProperty(i);
            //			boolean expanded = this.expandShorthands(prop);
            boolean expanded = CSSPropertyExpander.expandShorthands(this, prop,fromStyle.getParentRule());
            if (!expanded) this.setProperty(prop.getName(),prop.getValue(),prop.isImportant());
        }
    }
/*    
    public void combineFrom(CSSStyleDeclaration fromStyle)
    {
        for (int i = 0; i<fromStyle.getLength(); i++)
        {
            Property prop = fromStyle.;
            //			boolean expanded = this.expandShorthands(prop);
            //boolean expanded = CSSPropertyExpander.expandShorthands(this, prop,fromStyle.getParentRule());
            this.setProperty(fromStyle.item(i),fromStyle.getPropertyValue(fromStyle.item(i))
            				,fromStyle.getPropertyPriority(fromStyle.item(i))m(i)).isImportant());
        }
    }
*/    
    /** this method checks the property, and if it has the value of 'inherit'
     * it inherits it from the parent style */
    protected boolean inherit(
    Property prop,
    XSmilesCSSStyleDeclarationImpl parentStyle)
    {
        // if 'inherit' take parents value
        // check for resolvers for this type of property
        // resolve the property
        
        CSSValue value = prop.getValue();
        String name = prop.getName();
        
        if (value.getCssText().equals("inherit"))
            //		if (value.getCssValueType()==CSSValue.CSS_INHERIT)
        {
            // inherit 'inherit' values
            Property parentProp = parentStyle.getProperty(name);
            if (parentProp != null)
            {
                prop.setValue(parentProp.getValue());
                prop.setImportant(parentProp.isImportant());
            }else
            {
                removeProperty(name);
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Inherit values from parent style. NOTE! use this only for empty style
     * @author MH
     */
     /*
     public void inherit(XSmilesCSSStyleDeclarationImpl parentStyle)
     {
        if (parentStyle==null) return;
        //this.combineFrom((XSmilesCSSStyleDeclarationImpl)parentStyle);
        // TODO: check 'inherit' values, and inherit things that are inherit by default
        for (int i=0;i<valueResolvers.size();i++)
        {
            String name = (String)valueResolvers.elementAt(i);
            Property parentProperty = parentStyle.getProperty(name);
            if (parentProperty==null) break;
            // inheritence should not keep the value of the important attribute
            this.setProperty(name,parentProperty.getValue(),false);
        }
        Log.debug("Inherited style from parent, now: "+this);
     }
      
      */
    
    /**
     * Computes the relative values in the given style declaration for the
     * given element and pseudo-element.
     */
    protected void computeRelativeValues(
    Element elem, String pseudoclass,
    XSmilesCSSStyleDeclarationImpl localStyle,
    XSmilesCSSStyleDeclarationImpl parentStyle
    )
    {
        
        // go thru values
        for (int i = 0; i<localStyle.getLength(); i++)
        {
            Property prop = localStyle.getProperty(i);
            boolean removed = this.inherit(prop,parentStyle);
            if (removed) i--;
        }
        
        // go thru resolvers
        for (int i = 0;i < valueResolvers.size(); i++)
        {
            RelativeValueResolver resolver = (RelativeValueResolver)valueResolvers.elementAt(i);
            resolver.resolveValue(elem,pseudoclass,localStyle,parentStyle);
        }
    }
    public void copyPropertySet(XSmilesCSSStyleDeclarationImpl fromStyle)
    {
//    	 DEBUG, remove
        //count++;
        for (int i = 0; i<fromStyle.getLength(); i++)
        {
            Property prop = fromStyle.getProperty(i);
            this.setProperty(prop.getName(),prop.getValue(),prop.isImportant());
        }
    }
    
    
    /**
     * Add property (new / old) without using CSS parser
     * @author MH
     */
    public void setProperty(
    String propertyName,
    CSSValue expr,
    boolean important ) throws DOMException
    {
        try
        {
            Property p = getPropertyDeclaration(propertyName);
            if (p == null)
            {
                p = new Property(propertyName, expr, important);
                addProperty(p);
            } else
            {
                // if there is already an important value, then add only important
                if (p.isImportant()==false || important==true)
                {
                    p.setValue(expr);
                    p.setImportant(important);
                }
            }
        } catch (Exception e)
        {
            throw new DOMExceptionImpl(
            DOMException.SYNTAX_ERR,
            DOMExceptionImpl.SYNTAX_ERROR,
            e.getMessage());
        } finally
        {
            fireChangeListeners();
        }
    }
    protected void fireChangeListeners()
    {
        if (stylelisteners!=null)
        {
            Enumeration e = stylelisteners.elements();
            while(e.hasMoreElements())
            {
                CSSStyleChangeListener listener = (CSSStyleChangeListener)e.nextElement();
                listener.styleChanged();
            }
        }
    }
    
    public void setProperty(
    String propertyName,
    String value,
    String priority ) throws DOMException
    {
        super.setProperty(propertyName,value,priority);
        fireChangeListeners();
        
    }
    public Property getProperty(String name)
    {
        return this.getPropertyDeclaration(name);
    }
    
    protected Vector stylelisteners;
    
    public void addCSSStyleChangeListener(CSSStyleChangeListener listener)
    {
        if (this.stylelisteners==null) this.stylelisteners=new Vector();
        if (!this.stylelisteners.contains(listener))
            this.stylelisteners.addElement(listener);
    }
    public void removeCSSStyleChangeListener(CSSStyleChangeListener listener)
    {
        if (this.stylelisteners==null) return;
        this.stylelisteners.removeElement(listener);
    }
    
}
