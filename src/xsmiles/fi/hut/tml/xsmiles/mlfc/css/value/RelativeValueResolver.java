/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.css.value;
package fi.hut.tml.xsmiles.mlfc.css.value;

import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
//import org.apache.batik.css.CSSOMReadOnlyStyleDeclaration;
//import org.apache.batik.css.CSSOMReadOnlyValue;
import org.w3c.dom.Element;
import org.w3c.dom.css.ViewCSS;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;


/**
 * This interface is used to provide a resolver for relative CSS values.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: RelativeValueResolver.java,v 1.1 2002/03/15 09:17:22 honkkis Exp $
 */
public interface RelativeValueResolver {
    /**
     * Whether the handled property is inherited or not.
     */
    boolean isInheritedProperty();

    /**
     * Returns the name of the handled property.
     */
    String getPropertyName();

    /**
     * Returns the default value for the handled property.
     */
    //CSSOMReadOnlyValue getDefaultValue();


    public void resolveValue(Element element,
    		     String pseudoElement,
    		     //ViewCSS view,
    		     XSmilesCSSStyleDeclarationImpl styleDeclaration,
    		     //CSSValue value,
    		     XSmilesCSSStyleDeclarationImpl parentStyle
				 //,boolean isImportant
    		     //,String priority,
    		     //int origin
    			 ) ;

    /**
     * Resolves the given value if relative, and puts it in the given table.
     * @param element The element to which this value applies.
     * @param pseudoElement The pseudo element if one.
     * @param view The view CSS of the current document.
     * @param styleDeclaration The computed style declaration.
     * @param value The cascaded value.
     * @param priority The priority of the cascaded value.
     * @param origin The origin of the cascaded value.
     */
/*    void resolveValue(Element element,
		      String pseudoElement,
		      ViewCSS view,
		      CSSStyleDeclaration styleDeclaration,
		      CSSValue value,
		      String priority,
		      int origin);*/
}
