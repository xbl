
package fi.hut.tml.xsmiles.mlfc.css;

import java.util.StringTokenizer;

import org.w3c.dom.css.*;
import org.w3c.css.sac.*;
import org.w3c.dom.*;

import com.steadystate.css.parser.selectors.PseudoElementConditionImpl;
import com.steadystate.css.parser.selectors.ElementSelectorImpl;

import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.Log;

public class CSSSelectorsHTML extends CSSSelectors
{
    public CSSSelectorsHTML()
    {
        super();
    }
    
    /**
     * match element name to selector name
     * in HTMLSelectors, this is overridden to match case insensitively
     */
    protected boolean matchElementName(String elementName, String selName)
    {
        return selName.equalsIgnoreCase(elementName);
        
    }
    
    /**
     * in HTMLSelectors, this is overridden to match case insensitively
     */
    protected String normalizeAttributeName(String attrName)
    {
        return attrName.toLowerCase();
    }
    /**
     * in HTMLSelectors, this is overridden to match case insensitively
     */
    protected boolean matchAttributeName(String attrName, String selName)
    {
        return attrName.equalsIgnoreCase(selName);
    }
    
    /** in HTML converts border=0 to border-width:0px, etc.  */
    /*
    public String getStyleAttrValue(StylableElement element)
    {
        String base = element.getStyleAttrValue();
        if (base==null) base="";
        if (element instanceof XHTMLElement)
        {
            Attr attr;
            attr = element.getAttributeNode("border");
            if (attr!=null)
                base+="border-width: 0px;";
                //base+="border-width: "+attr.getNodeValue()+";";
        }
        Log.debug("XHTML style: "+base);
        return base;
    }*/
    
    
}


