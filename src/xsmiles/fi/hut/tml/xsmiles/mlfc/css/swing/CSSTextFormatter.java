/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.mlfc.css.swing;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.general.ColorConverter;
import fi.hut.tml.xsmiles.gui.media.general.CSSTextFormatterInterface;
import fi.hut.tml.xsmiles.mlfc.css.swing.CSSFormatter;

import java.awt.*;
import java.util.Vector;
import java.util.Enumeration;
//import java.util.Iterator;
import java.util.Hashtable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.text.Style;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleContext;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;


// For CSS2
import com.steadystate.css.parser.CSSOMParser;
import java.io.StringReader;
import org.w3c.css.sac.InputSource;
import java.awt.font.TextAttribute;
import org.w3c.dom.css.*;
import java.util.Map;
import java.lang.Float;

/**
 * Reads in a style string and formats a text component (font, color).
 *
 * @author Kari Pihkala
 * @author Mikko Honkala
 */
public class CSSTextFormatter implements CSSTextFormatterInterface//implements CSSFormatter
{
    // Style for this component
    protected CSSStyleDeclaration cssStyle;
    
    public CSSTextFormatter()
    {
    }
    
    public void setStyle(String s)
    {
        int size = 16;
        
        // Check for no style
        if (s == null || s == "")
            return;
        
        // Get CSS Style Decleration
        CSSOMParser parser = new CSSOMParser();
        try
        {
            cssStyle = parser.parseStyleDeclaration(new InputSource(new StringReader( "{" + s + "}" )));
        } catch (IOException e)
        {
            cssStyle = null;
            Log.debug(e.getMessage());
            return;
        }
        
        
        return;
    }
    public void formatComponent(Component comp)
    {
        if (comp instanceof JComponent)
            this.formatComponent((JComponent)comp);
    }
    public void formatComponent(JComponent comp)
    {
        if (cssStyle==null) return;
        Font font = getFont();
        //		Log.debug("[Proxy]"+" FONTTI: "+cssStyle.toString()+" fontti:"+font);
        //
        // Set the style
        if (comp != null)
        {
            Color color, bgcolor;
            
            comp.setFont(font);
            
            // get color property
            CSSValue colorVal = getProperty("color");
            if (colorVal != null)
            {
                color = ColorConverter.hexaToRgb(colorVal.getCssText());
                comp.setForeground(color);
            }
            
            // get background-color property
            CSSValue bgcolorVal = getProperty("background-color");
            if (bgcolorVal != null)
            {
                Log.debug("bgcolor:"+bgcolorVal.getCssText());
                bgcolor = ColorConverter.hexaToRgb(bgcolorVal.getCssText());
                comp.setBackground(bgcolor);
                comp.setOpaque(true);
            }
            
        }
    }
    //	public void formatComponent(JComponent comp, AttributeSet style)
    //	{
    //		//Log.debug("Formatting "+comp+" \n style:"+style);
    //		if (style==null)
    //		{
    //formatComponent(comp);
    //			return;
    //		}
    //
    //		Font font = getFont(style);
    //
    //		// Set the style
    //		if (comp != null) {
    //			comp.setFont(font);
    //			Color color, bgcolor;
    //			bgcolor = (Color)style.getAttribute(XMLCSS.ATTRIBUTE_BACKGROUND_COLOR);
    //			if (bgcolor == null) {
    //				bgcolor = Color.white;
    //			}
    //			comp.setBackground(bgcolor);
    //			comp.setOpaque(true);
    //
    //			// get color property
    //			color = (Color)style.getAttribute(XMLCSS.ATTRIBUTE_FOREGROUND_COLOR);
    //			if (color == null) {
    //				color = Color.black;
    //			}
    //			comp.setForeground(color);
    //
    //
    //		}
    //	}
    //
    //	public static Font getFont(AttributeSet at)
    //	{
    //		Font fo = (Font) at.getAttribute(XMLCSS.ATTRIBUTE_FONT);
    //		if (fo == null){
    //			StyleContext st = new StyleContext();
    //			fo = st.getFont("TimesRoman", Font.PLAIN, 14);
    //		}
    //
    //		return fo;
    //	}
    //
    //	public Dimension getSize(AttributeSet style)
    //	{
    //		if (style==null) return new Dimension(-1,-1);
    //		Float heightF = (Float) style.getAttribute(XMLCSS.ATTRIBUTE_HEIGHT);
    //		Float widthF = (Float) style.getAttribute(XMLCSS.ATTRIBUTE_WIDTH);
    //		Log.debug("getSize: "+heightF+" x "+widthF);
    //		int width=-1,height=-1;
    //		if (heightF!=null) height=heightF.intValue();
    //		if (widthF!=null) width=widthF.intValue();
    //		return new Dimension(width,height);
    //	}
    
    public Dimension getSize()
    {
        //Dimension d = comp.getPreferredSize();
        double heightf=-1,widthf=-1;
        CSSValue cssheight = getProperty("height");
        if (cssheight != null)
        {
            String heightstr = cssheight.getCssText();
            if (!heightstr.equalsIgnoreCase("auto"))
            {
                CSSTextFormatter.LengthUnit l = new LengthUnit();
                double h = l.parse(heightstr,(short)4,(float)300);
                if (h>=(double)1) heightf=h;
            }
        }
        CSSValue csswidth = getProperty("width");
        if (csswidth != null)
        {
            String widthstr = csswidth.getCssText();
            if (!widthstr.equalsIgnoreCase("auto"))
            {
                CSSTextFormatter.LengthUnit l = new LengthUnit();
                double w=l.parse(widthstr,(short)4,(float)300);
                if (w>=(double)1) widthf=w;
            }
        }
        Dimension nd = new Dimension((int)(widthf + .5),(int)(heightf + .5));
        return nd;
    }
    
    
    
    
    /**
     * Returns the specified property's CSSValue.
     * @param property 		The name of the property to return.
     * @return 				The specified property's CSSValue.
     */
    public CSSValue getProperty(String property)
    {
        CSSValue v = null;
        
        if (cssStyle != null)
        {
            v = cssStyle.getPropertyCSSValue(property);
        }
        
        return v;
    }
    
    
    /**
     * Returns the Font accoring to the cssStyle variable.
     * @return The Font that should be used when painting this element.
     */
    public Font getFont()
    {
        
        Float fontSize = new Float(12);     // default size 12px
        String fontFamily = "Default";
        Float fontStyle = TextAttribute.POSTURE_REGULAR;
        Float fontWeight = TextAttribute.WEIGHT_REGULAR;
        Float fontStretch = TextAttribute.WIDTH_REGULAR;
        boolean underline = false;
        boolean strikeThrough = false;
        boolean overline = false;
        
        // get font-size property
        CSSValue sizeVal = getProperty("font-size");
        if (sizeVal != null)
        {
            Log.debug("Font Size: "+sizeVal.getCssText());
            String sizeStr = sizeVal.getCssText();
            // Pixels
            if (sizeStr.endsWith("px"))
            {
                try
                {
                    fontSize = new Float(sizeStr.substring(0, sizeStr.length()-2));
                } catch (NumberFormatException e)
                {
                }
                
            }
        }
        
        // get font-family property
        CSSValue familyVal = getProperty("font-family");
        if (familyVal != null)
        {
            Log.debug("Font Family: "+familyVal.getCssText());
            String[] supportedFamilies = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
            if (familyVal.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE)
            {
                fontFamily = familyVal.getCssText();
                fontFamily = fontFamily.replace('\"', ' ');
                fontFamily = fontFamily.trim();
                if (fontFamily.equalsIgnoreCase("Courier"))
                {
                    fontFamily = "Courier New";
                } else if (fontFamily.indexOf("Times") != -1 && fontFamily.indexOf("Roman") != -1)
                {
                    fontFamily = "Times New Roman";
                }
                boolean supported = false;
                for (int i = 0; i < supportedFamilies.length; i++)
                {
                    if (fontFamily.equalsIgnoreCase(supportedFamilies[i]))
                    {
                        supported = true;
                        break;
                    }
                }
                if (!supported)
                {
                    fontFamily = "Default";
                }
                
            } else if (familyVal.getCssValueType() == CSSValue.CSS_VALUE_LIST)
            {
                CSSValueList familyList = (CSSValueList)familyVal;
                int numItems = familyList.getLength();
                String familyString = "";
                if (numItems > 0)
                {
                    for (int i = 0; i < numItems; i++)
                    {
                        CSSValue family = familyList.item(i);
                        familyString = family.getCssText();
                        familyString = familyString.replace('\"', ' ');
                        familyString = familyString.trim();
                        if (familyString.equalsIgnoreCase("Courier"))
                        {
                            familyString = "Courier New";
                        } else if (familyString.indexOf("Times") != -1 && familyString.indexOf("Roman") != -1)
                        {
                            familyString = "Times New Roman";
                        }
                        boolean supported = false;
                        for (int j = 0; j < supportedFamilies.length; j++)
                        {
                            if (familyString.equalsIgnoreCase(supportedFamilies[j]))
                            {
                                supported = true;
                                break;
                            }
                        }
                        if (supported)
                        {
                            break;
                        } else
                        {
                            familyString = "";
                        }
                    }
                }
                if (familyString.length() > 0)
                {
                    fontFamily = familyString;
                }
            }
        }
        
        // get font-style property
        CSSValue styleVal = getProperty("font-style");
        if (styleVal != null)
        {
            Log.debug("Font Style: "+styleVal.getCssText());
            String styleString = styleVal.getCssText();
            if (styleString.equalsIgnoreCase("italic") || styleString.equalsIgnoreCase("oblique"))
            {
                fontStyle = new Float(((float)Font.ITALIC)); //TextAttribute.POSTURE_OBLIQUE;
            }
            if (styleString.equalsIgnoreCase("normal"))
            {
                fontStyle = new Float(((float)Font.PLAIN)); //TextAttribute.POSTURE_OBLIQUE;
            }
        }
        
        // get font-weight property
        CSSValue weightVal = getProperty("font-weight");
        if (weightVal != null)
        {
            Log.debug("Font Weight: "+weightVal.getCssText());
            String weightString = weightVal.getCssText();
            if (weightString.equalsIgnoreCase("bold"))
            {
                fontWeight = TextAttribute.WEIGHT_BOLD;
            } else if (!weightString.equals("normal") && !weightString.equals("all"))
            {
                int fontWeightInt = Integer.parseInt(weightString);
                switch (fontWeightInt)
                {
                    case 100: fontWeight = TextAttribute.WEIGHT_EXTRA_LIGHT;  break;
                    case 200: fontWeight = TextAttribute.WEIGHT_LIGHT; break;
                    case 300: fontWeight = TextAttribute.WEIGHT_REGULAR; break;
                    case 400: fontWeight = TextAttribute.WEIGHT_SEMIBOLD; break;
                    case 500: fontWeight = TextAttribute.WEIGHT_MEDIUM; break;
                    case 600: fontWeight = TextAttribute.WEIGHT_DEMIBOLD; break;
                    case 700: fontWeight = TextAttribute.WEIGHT_BOLD; break;
                    case 800: fontWeight = TextAttribute.WEIGHT_EXTRABOLD; break;
                    case 900: fontWeight = TextAttribute.WEIGHT_ULTRABOLD; break;
                }
            }
        }
        
        // it seems that any font weight that isn't bold or regular is treated as regular
        // make sure that any font that is supposed to be at least bold is drawn bold
        if (fontWeight.floatValue() > TextAttribute.WEIGHT_BOLD.floatValue())
        {
            fontWeight = TextAttribute.WEIGHT_BOLD;
        }
        
        // get font-stretch property
        CSSValue stretchVal = getProperty("font-stretch");
        if (stretchVal != null)
        {
            Log.debug("Font Stretch: "+stretchVal.getCssText());
            String stretchString = stretchVal.getCssText();
            if (stretchString.equalsIgnoreCase("ultra-condensed"))
            {
                fontStretch =  new Float(0.5);
            } else if (stretchString.equalsIgnoreCase("extra-condensed"))
            {
                fontStretch =  new Float(0.625);
            } else if (stretchString.equalsIgnoreCase("condensed"))
            {
                fontStretch =  new Float(0.75);
            } else if (stretchString.equalsIgnoreCase("semi-condensed"))
            {
                fontStretch =  new Float(0.875);
            } else if (stretchString.equalsIgnoreCase("semi-expanded"))
            {
                fontStretch =  new Float(1.125);
            } else if (stretchString.equalsIgnoreCase("expanded"))
            {
                fontStretch =  new Float(1.25);
            } else if (stretchString.equalsIgnoreCase("extra-expanded"))
            {
                fontStretch =  new Float(1.375);
            } else if (stretchString.equalsIgnoreCase("ultra-expanded"))
            {
                fontStretch =  new Float(1.5);
            }
        }
        
        // get text-decoration property
        CSSValue decorVal = getProperty("text-decoration");
        if (decorVal != null)
        {
            Log.debug("Font Decoration: "+decorVal.getCssText());
            String decorString = decorVal.getCssText();
            if (decorString.equalsIgnoreCase("underline"))
            {
                underline = true;
            } else if (decorString.equalsIgnoreCase("overline"))
            {
                overline = true;
            } else if (decorString.equalsIgnoreCase("line-through"))
            {
                strikeThrough = true;
            }
        }
        
        //	  Font font = new Font("Serif", Font.PLAIN, 1);
        Font font = new Font(fontFamily, (int)fontStyle.floatValue(), (int)fontSize.floatValue());
        Map fontAttributes = font.getAttributes();
        fontAttributes.put(TextAttribute.FAMILY, fontFamily);
        fontAttributes.put(TextAttribute.POSTURE, fontStyle);
        // java seems to render fonts too small need to increase by 1.3?? times - NOT compared to IE6.0/NS6.0
        fontSize = new Float((float)(fontSize.floatValue()));
        fontAttributes.put(TextAttribute.SIZE, fontSize);
        fontAttributes.put(TextAttribute.WEIGHT, fontWeight);
        fontAttributes.put(TextAttribute.WIDTH, fontStretch);
        if (underline)
        {
            fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        }
        if (overline)
        {  // no overline support??
            //  fontAttributes.put(TextAttribute.OVERLINE, TextAttribute.OVERLINE_ON);
        }
        if (strikeThrough)
        {
            fontAttributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
        }
        
        font = new Font(fontAttributes);
        
        //	 font = new Font(fontFamily, (int)fontStyle.floatValue(), (int)fontSize.floatValue());
        return font;
    }
    public Font deriveFont(Font orig,int style,float size)
    {
        return orig.deriveFont(style,size);
    }
    
    
    
    
    // --------------- copied
    /**
     * Parses a length value, this is used internally, and never added
     * to an AttributeSet or returned to the developer.
     */
    static class LengthUnit
    {
        static Hashtable lengthMapping = new Hashtable(6);
        static short type;
        static float value;
        
        static
        {
            float res = Toolkit.getDefaultToolkit().getScreenResolution();
            lengthMapping.put("pt", new Float(1f));
            lengthMapping.put("px", new Float(res / 72f));
            lengthMapping.put("mm", new Float(72f / 25.4f));
            lengthMapping.put("cm", new Float(72f / 2.54f));
            lengthMapping.put("pc", new Float(12f));
            lengthMapping.put("in", new Float(72f));
        }
        
        LengthUnit()
        {
            //parse(value, defaultType, defaultValue);
        }
        
        public float parse(String value, short defaultType, float defaultValue)
        {
            type = defaultType;
            this.value = defaultValue;
            
            int length = value.length();
            if (length > 0 && value.charAt(length - 1) == '%')
            {
                try
                {
                    this.value = Float.valueOf(value.substring(0, length - 1)).
                    floatValue() / 100.0f;
                    type = 1;
                }
                catch (NumberFormatException nfe)
                { }
            }
            if (length >= 2)
            {
                String units = value.substring(length - 2, length);
                Float scale = (Float)lengthMapping.get(units);
                
                if (scale != null)
                {
                    try
                    {
                        this.value = Float.valueOf(value.substring(0,
                        length - 2)).floatValue() * scale.floatValue();
                        type = 0;
                    }
                    catch (NumberFormatException nfe)
                    { }
                }
                else if (units.equals("em") ||
                units.equals("ex"))
                {
                    try
                    {
                        this.value = Float.valueOf(value.substring(0,
                        length - 2)).floatValue();
                        type = 3;
                    }
                    catch (NumberFormatException nfe)
                    { }
                }
                else if (value.equals("larger"))
                {
                    this.value = 2f;
                    type = 2;
                }
                else if (value.equals("smaller"))
                {
                    this.value = -2;
                    type = 2;
                }
                else
                {
                    // treat like points.
                    try
                    {
                        this.value = Float.valueOf(value).floatValue();
                        type = 0;
                    } catch (NumberFormatException nfe)
                    {}
                }
            }
            return this.value;
        }
    }
    
    // --------------- copied
    
    
}
