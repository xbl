/*
 * CSSRuleListImpl.java
 *
 * SteadyState CSS2 Parser
 *
 * Copyright (C) 1999, 2000 Steady State Software Ltd.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * To contact the authors of the library, write to Steady State Software Ltd.,
 * Unit 5, Manor Farm Courtyard, Rowsham, Buckinghamshire, HP22 4QP, England
 *
 * http://www.steadystate.com/css/
 * mailto:css@steadystate.co.uk
 *
 * $Id: XSmilesCSSRuleListImpl.java,v 1.17 2004/08/27 10:18:24 ale Exp $
 */
 
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 

package fi.hut.tml.xsmiles.mlfc.css;
//package com.steadystate.css.dom;


import fi.hut.tml.xsmiles.mlfc.general.QuickSort;
import fi.hut.tml.xsmiles.mlfc.general.Comparer;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;

import fi.hut.tml.xsmiles.Log;

import com.steadystate.css.dom.CSSRuleListImpl;
import com.steadystate.css.dom.CSSStyleDeclarationImpl;
import com.steadystate.css.parser.SelectorListImpl;
import org.w3c.dom.stylesheets.MediaList ;
import java.util.Vector;
import org.w3c.dom.css.*;
import org.w3c.css.sac.*;


 /**
  * MH: This class has been taken from Steady State CSS and modified for use with X-Smiles browser. 
  * XSmilesCSSOMParser generates instances of this type.
  *
  * Reason: allow sorting of rule lists
  */

public class XSmilesCSSRuleListImpl extends CSSRuleListImpl implements CSSRuleList {
    
	public static final short UNINITIALIZED = 0;
    public static final short STATE_RULES_SPLIT = 10;
    public static final short STATE_RULES_SORTED = 20;
    
    /** the current state */
    protected short listState = UNINITIALIZED;
    
	/** the static comparer, that can compare the specificity of two rules */
	static CSSComparer comparer = new CSSComparer();
	

	/** the vector of rules, SS had private access */
    protected Vector _rules = null;

    public XSmilesCSSRuleListImpl() {
    }
	
	/** sorts this rule list according to the getSpecificity method from rule */
	public void sort()
    {
        if (this.listState==STATE_RULES_SORTED) 
        {
            Log.debug("Rules were alreade sorted.");
            return;
        }
		if (this._rules==null || this._rules.size()==0) return;
        if (this.listState==UNINITIALIZED) this.splitAllRules();
    	QuickSort.sort(this._rules,comparer,null);
        this.listState=STATE_RULES_SORTED;
    }
    
    public void appendTo(CSSRuleList list)
    {
    	// TODO	
    }
    
    protected MediaQueryEvaluator mqe = null;
    /** this method splits all rules that are comma separated,
     * and it also counts the specificity of all rules
     */
    
    public void setMediaQueryEvaluator (MediaQueryEvaluator evaluator)
    {
        this.mqe=evaluator;
    }
    protected void splitAllRules()
    {
        //this.appendFrom(0,true,mqe);
        try
        {
            XSmilesCSSRuleListImpl clone = (XSmilesCSSRuleListImpl)this.clone();
            this.clearRules();
            this.appendFrom(clone,1,true,mqe);
        } catch (CloneNotSupportedException e)
        {
            Log.error(e,"While trying to split all CSS rules.");
        }
        finally
        {
            this.listState=STATE_RULES_SPLIT;
        }
    }
    protected void clearRules()
    {
        this._rules=new Vector();
    }

    
        /** appends all rules from ruleList into this rule list, comma separated rules will be split if 'spit' is true */
    public void appendFrom(XSmilesCSSRuleListImpl ruleList, int origin, boolean split,MediaQueryEvaluator mqe)
    {
    	// Appends from list, possibly splitting rules with ','s
		for (int i=0;i<ruleList.getLength();i++)
		{
			if (ruleList.item(i).getType() != CSSRule.IMPORT_RULE)	
			{	
                if (ruleList.item(i) instanceof XSmilesCSSStyleRule)
                {
                    XSmilesCSSStyleRule rule = (XSmilesCSSStyleRule)ruleList.item(i);
                    rule.setStyleSheetOrder(this.getLength());
                    rule.setOrigin(origin);
                    SelectorList selList = rule.getSelectorList();
                    int selCount = selList.getLength();
                    if (split&&selCount>1)
                    {
                        this.splitAndAppend(rule,selList,ruleList,origin);
                    }
                    else
                    {
                        rule.countSpecificity();
                        this.add(rule);
                    }
/*              }else if (ruleList.item(i).getType() == CSSRule.MEDIA_RULE)
				{
					CSSMediaRule mediaRule = (CSSMediaRule)ruleList.item(i);
					MediaList mediaList = mediaRule.getMedia();
					XSmilesCSSRuleListImpl  mediaRuleList= (XSmilesCSSRuleListImpl)mediaRule.getCssRules(); 
                    if (mqe != null){
						if (mqe.evalMediaQuery(mediaList.getMediaText()) == true){
							this.appendFrom( mediaRuleList, origin, true, mqe); 
						}
                    }
*/				}else{
                   	// Log.error(this.getClass()+" rulelist is of wrong type");				
				}
			}	
		}
    }

	
	/** appends a single rule into this rule, but splits it if there are multiple selectors separeted with a comma */
	public void splitAndAppend(XSmilesCSSStyleRule rule, SelectorList selList, XSmilesCSSRuleListImpl ruleList, int origin)
	{
		for (int j=0;j<selList.getLength();j++)
		{
			SelectorListImpl newSelList= new SelectorListImpl();
			newSelList.add(selList.item(j));
			XSmilesCSSStyleRule newRule = new XSmilesCSSStyleRule(
				(XSmilesCSSStyleSheetImpl)rule.getParentStyleSheet(),
				(CSSRule)null,newSelList);
			newRule.setStyle((CSSStyleDeclarationImpl)rule.getStyle());		
			newRule.setStyleSheetOrder(rule.stylesheetorder);
			newRule.setOrigin(rule.origin);
			newRule.countSpecificity();
			this.add(newRule);
		}
	}
    
	
	/** a comparer class for the QuickSort algorithm in package mlfc.general */
    public static class CSSComparer implements Comparer
    {
    	public int compare(Object handle, Object o1, Object o2)
    	{
    		XSmilesCSSStyleRule rule1 = (XSmilesCSSStyleRule)o1;
    		XSmilesCSSStyleRule rule2 = (XSmilesCSSStyleRule)o2;
			long s1 = rule1.getSpecificity();
			long s2 = rule2.getSpecificity();
			// TODO: compare logic
			//Log.debug("compare :"+rule1.toString()+" - "+rule2.toString());
			if  (s1<s2) return -1;
			else if (s1>s2) return 1;
			else return 0;
    	}
    }
	
	

	/** this method is copied from the SS CSS implementation */
    public int getLength() {
        return (_rules != null) ? _rules.size() : 0;
    }

    /** this method is copied from the SS CSS implementation */
    public CSSRule item(int index) {
        return (_rules != null) ? (CSSRule) _rules.elementAt(index) : null;
    }

    /** this method is copied from the SS CSS implementation */
    public void add(CSSRule rule) {
        if (_rules == null) {
            _rules = new Vector();
        }
        _rules.addElement(rule);
        //Log.debug("Adding rule: "+rule);
    }
    
    /** this method is copied from the SS CSS implementation */
    public void insert(CSSRule rule, int index) {
        if (_rules == null) {
            _rules = new Vector();
        }
        _rules.insertElementAt(rule, index);
    }
    
    /** this method is copied from the SS CSS implementation */
    public void delete(int index) {
        if (_rules == null) {
            _rules = new Vector();
        }
        _rules.removeElementAt(index);
    }
    
    /** this method is copied from the SS CSS implementation */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < getLength(); i++ ) {
            sb.append(item(i).toString()).append("\r\n");
        }
        return sb.toString();
    }
    public Object clone() throws java.lang.CloneNotSupportedException
    {
        //Log.debug("** XSmilesCSSRuleListImpl.clone called.");
        
        //XSmilesCSSRuleListImpl clone = (XSmilesCSSRuleListImpl)super.clone();
        XSmilesCSSRuleListImpl clone = new XSmilesCSSRuleListImpl();
        clone._rules=this._rules;
        return clone;
    }
}
