/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css.swing;

import javax.swing.*;
import org.w3c.dom.css.*;
import java.awt.Font;
import java.awt.Dimension;
import javax.swing.text.AttributeSet;



public interface CSSFormatter
{
		public void setStyle(String s);
		public void formatComponent(JComponent comp);
		public Dimension getSize();
		public CSSValue getProperty(String property);
		
		public Font deriveFont(Font orig,int style,float size);
		
		

}
