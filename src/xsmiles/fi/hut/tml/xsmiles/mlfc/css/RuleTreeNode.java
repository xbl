 /*
  * Created on Aug 12, 2004
  *
  * TODO To change the template for this generated file go to
  * Window - Preferences - Java - Code Style - Code Templates
  */
 package fi.hut.tml.xsmiles.mlfc.css;

 import java.util.Enumeration;
 import java.util.Vector;

 import org.w3c.dom.Node;

 import fi.hut.tml.xsmiles.Log;
 import fi.hut.tml.xsmiles.dom.StylableElement;

 /**
  * @author Alessandro
  *
  * TODO To change the template for this generated type comment go to
  * Window - Preferences - Java - Code Style - Code Templates
  */
 public class RuleTreeNode{
     
         private RuleTreeNode parentNode;
         private XSmilesCSSStyleRule rule = null;
         private XSmilesCSSStyleDeclarationImpl style = null;
         private int nodeStatus = 0;
         private Vector children;
         private Vector domElementList = null;

         
         public RuleTreeNode(RuleTreeNode parentNode, XSmilesCSSStyleRule rule){
             this.rule = rule;
             this.parentNode = parentNode;
             children = new Vector(1,1);
             style = new XSmilesCSSStyleDeclarationImpl(null);
         }
                
         public RuleTreeNode(int root){ 
             this.nodeStatus = root;
             children = new Vector(1,1);
             style = new XSmilesCSSStyleDeclarationImpl(null);
         }
         
         public RuleTreeNode(RuleTreeNode parentNode, XSmilesCSSStyleDeclarationImpl dec){           
             this.parentNode = parentNode;
             style = new XSmilesCSSStyleDeclarationImpl(null);
         }
         
         
         public void addElement(org.w3c.dom.Element elem ){
         	if (domElementList == null){
         		domElementList = new Vector(1,1);       	
         	}
         	domElementList.addElement(elem);       	
         }
         
         public void removeElement(org.w3c.dom.Element elem ){
         	if (domElementList != null){
         		boolean r = domElementList.removeElement(elem);      		
         	}      	
         }
             
         public RuleTreeNode getParent(){
             return this.parentNode;
         } 
         
         public boolean isRoot(){
             return (this.nodeStatus == RuleTree.ROOT);
         }
         
         public XSmilesCSSStyleRule getCSSRule(){
             return rule;
         }
         
         public Vector getChildren(){
             return children;
         }
         
         public XSmilesCSSStyleDeclarationImpl getCSSStyle(){
             return style;
         }
         
         public StylableElement checkBrothers(StylableElement elem){
         	StylableElement el = null;
         	if (domElementList != null){
         		for (Enumeration e = domElementList.elements() ; e.hasMoreElements() ;) {
         			el = ((StylableElement)e.nextElement());
 /*        			Log.debug("element style:"+el.hasStyle()+" opt style:"+this.getCSSStyle());
         			if (!el.hasStyle()|| el.getStyle() != this.getCSSStyle()){
         				// the node in the opt tree is invalid
         				// clear the dom element list
         				domElementList = null;
         				return null;
         			}
 */
     				// the node is valid in the tree
         			if (elem.getParentNode() == el.getParentNode()){
//         	        	System.out.println(elem.getParentNode()+"----"+elem+"----"+el.getParentNode()+"-------"+el+"------"+this);
         				return el;        				
         			}else if(((elem.getParentNode()) instanceof StylableElement) && 
         					((el.getParentNode()) instanceof StylableElement))
         			{
                        // THIS WAS ADDED BY MH because XBL initialization went into inf loop... I don't know if
                        // this disables the brother optimization!!!
                        if(((StylableElement)elem.getParentNode()).hasStyle() &&
                            ((StylableElement)el.getParentNode()).hasStyle()){
         					if(((StylableElement)elem.getParentNode()).getStyle() ==
         						((StylableElement)el.getParentNode()).getStyle()){
         						return el;
         					}
                        }
         			}
         		}
         	}   
          return null;
         }
         
         public RuleTreeNode addLeaf(XSmilesCSSStyleDeclarationImpl dec){
         	
         	RuleTreeNode child = new RuleTreeNode(this, dec);
         	XSmilesCSSStyleDeclarationImpl childStyle = child.getCSSStyle();
         	childStyle.copyPropertySet(this.getCSSStyle());
         	childStyle.combineFrom(dec);
         	return child;        
         }
                 
         public RuleTreeNode addChild(XSmilesCSSStyleRule CSSRule){

             if((CSSRule == this.getCSSRule())){
                 return this;
             }
                 for (Enumeration e = children.elements() ; e.hasMoreElements() ;) {
                     RuleTreeNode r = ((RuleTreeNode)e.nextElement());
                     if (r.getCSSRule() != null){
 	                    if (r.getCSSRule() == CSSRule){
 	                        return r;
 	                    }
                 	}
                 }
                
                 RuleTreeNode child = new RuleTreeNode(this, CSSRule);
                 XSmilesCSSStyleDeclarationImpl childStyle = child.getCSSStyle(); //empty style//
                 childStyle.copyPropertySet(this.getCSSStyle());
                 XSmilesCSSStyleDeclarationImpl childRuleStyle = (XSmilesCSSStyleDeclarationImpl)CSSRule.getStyle();
                 childStyle.combineFrom(childRuleStyle);
                 children.addElement(child);
                 return child;
         }
     }   
