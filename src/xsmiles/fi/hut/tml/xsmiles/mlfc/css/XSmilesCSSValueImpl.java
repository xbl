/*
 * CSSValueImpl.java
 *
 * Steady State CSS2 Parser
 *
 * Copyright (C) 1999, 2002 Steady State Software Ltd.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * To contact the authors of the library, write to Steady State Software Ltd.,
 * 49 Littleworth, Wing, Buckinghamshire, LU7 0JX, England
 *
 * http://www.steadystate.com/css/
 * mailto:css@steadystate.co.uk
 *
 * $Id: XSmilesCSSValueImpl.java,v 1.7 2003/06/29 05:40:51 ronald Exp $
 */

package fi.hut.tml.xsmiles.mlfc.css;

import com.steadystate.css.dom.*;

import java.awt.Toolkit;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.*;
import org.w3c.css.sac.*;
import org.w3c.dom.*;
import org.w3c.dom.css.*;
import com.steadystate.css.parser.CSSOMParser;
import com.steadystate.css.parser.LexicalUnitImpl;

/**
 * The <code>CSSValueImpl</code> class can represent either a
 * <code>CSSPrimitiveValue</code> or a <code>CSSValueList</code> so that
 * the type can successfully change when using <code>setCssText</code>.
 *
 * TO DO:
 * Float unit conversions,
 * A means of checking valid primitive types for properties
 *
 * @author  David Schweinsberg
 * @version $Release$
 */
public class XSmilesCSSValueImpl extends CSSValueImpl implements CSSPrimitiveValue, CSSValueList, Serializable {

    /**
     * Constructors
     */

    /**
     * Constructor
     */
    /*
    public XSmilesCSSValueImpl(LexicalUnit value, boolean forcePrimitive) {
        if (value.getParameters() != null) {
            if (value.getLexicalUnitType() == LexicalUnit.SAC_RECT_FUNCTION) {
                // Rect
                _value = new RectImpl(value.getParameters());
            } else if (value.getLexicalUnitType() == LexicalUnit.SAC_RGBCOLOR) {
                // RGBColor
                _value = new RGBColorImpl(value.getParameters());
            } else if (value.getLexicalUnitType() == LexicalUnit.SAC_COUNTER_FUNCTION) {
                // Counter
                _value = new CounterImpl(false, value.getParameters());
            } else if (value.getLexicalUnitType() == LexicalUnit.SAC_COUNTERS_FUNCTION) {
                // Counter
                _value = new CounterImpl(true, value.getParameters());
            } else {
                _value = value;
            }
        } else if (forcePrimitive || (value.getNextLexicalUnit() == null)) {
            
            // We need to be a CSSPrimitiveValue
            _value = value;
        } else {
            
            // We need to be a CSSValueList
            // Values in an "expr" can be seperated by "operator"s, which are
            // either '/' or ',' - ignore these operators
            Vector v = new Vector();
            LexicalUnit lu = value;
            while (lu != null) {
                if ((lu.getLexicalUnitType() != LexicalUnit.SAC_OPERATOR_COMMA)
                    && (lu.getLexicalUnitType() != LexicalUnit.SAC_OPERATOR_SLASH)) {
                        // X-SMiles change
                    //v.addElement(new CSSValueImpl(lu, true));
                    v.addElement(new XSmilesCSSValueImpl(lu, true));
                }
                lu = lu.getNextLexicalUnit();
            }
            _value = v;
        }
    }    */
    
    public XSmilesCSSValueImpl(LexicalUnit value, boolean forcePrimitive) {
		super(value,forcePrimitive);
    }
	public XSmilesCSSValueImpl(LexicalUnit value) {
        this(value, false);
    }
        
            protected CSSValueImpl createValueImpl(LexicalUnit l, boolean forcePrimitive)
    {
        return new XSmilesCSSValueImpl(l, true);
    }
	
	/** returns the lexical unit value object, if accessible */
	public LexicalUnit getLexicalUnit()
	{
		if (this._value instanceof LexicalUnit) return (LexicalUnit)_value;
		else return null;
	}
	
    public float getFloatValue(short unitType) throws DOMException {
    if (_value instanceof LexicalUnit) {
		short originalType = this.getPrimitiveType();
		LexicalUnit lu = (LexicalUnit) _value;
		float origValue = lu.getFloatValue();
		if (unitType==CSSPrimitiveValue.CSS_NUMBER||
		    unitType==CSSPrimitiveValue.CSS_UNKNOWN)
		{
			return origValue;		
		}
                else if (originalType==CSSPrimitiveValue.CSS_EMS&&fontSize!=-1)
                {
                    return origValue*fontSize;
                }
                else
                {
        		return origValue * conversionFactor(originalType) / conversionFactor(unitType);
                }
    }
    throw new DOMExceptionImpl(
        DOMException.INVALID_ACCESS_ERR,
        DOMExceptionImpl.FLOAT_ERROR);

    // We need to attempt a conversion
//        return 0;
    }
    protected int fontSize = -1;
    public void setFontSize(int fs)
    {
        fontSize=fs;
    }
    /*
    public float getFloatValue(short unitType, int fontSize) throws DOMException {
    if (_value instanceof LexicalUnit) {
		short originalType = this.getPrimitiveType();
		LexicalUnit lu = (LexicalUnit) _value;
		float origValue = lu.getFloatValue();
		if (unitType==CSSPrimitiveValue.CSS_NUMBER||
		    unitType==CSSPrimitiveValue.CSS_UNKNOWN)
		{
			return origValue;		
		}
                else if (originalType==CSS_EM)
                {
                    return origValue*fontSize;
                }
                else
                {
                    return origValue * conversionFactor(originalType) / conversionFactor(unitType);
                }
    }*/	
	private static final float CONVERSION_PT;
	private static final float CONVERSION_PX;
	private static final float CONVERSION_IN;
	private static final float CONVERSION_CM;
	private static final float CONVERSION_MM;
	private static final float CONVERSION_PC;
	
	static
	{
		CONVERSION_PX = 1f;
		CONVERSION_IN = Toolkit.getDefaultToolkit().getScreenResolution();
		CONVERSION_CM = CONVERSION_IN/2.54f;
		CONVERSION_MM = CONVERSION_CM/10f;
		CONVERSION_PT = CONVERSION_IN/72f;
		CONVERSION_PC = CONVERSION_PT*12f;
	}
	
	protected float conversionFactor(short unitType)
	{
		switch (unitType) {
			case CSSPrimitiveValue.CSS_PT:
			    return CONVERSION_PT; 
			case CSSPrimitiveValue.CSS_PX:
			    return CONVERSION_PX;
			case CSSPrimitiveValue.CSS_MM:
			    return CONVERSION_MM;
			case CSSPrimitiveValue.CSS_CM:
			    return CONVERSION_CM;
			case CSSPrimitiveValue.CSS_PC:
			    return CONVERSION_PC;
			case CSSPrimitiveValue.CSS_IN:
			    return CONVERSION_IN;
			// TODO: add other value types
			default :
				return 1f;
		}
	}

}
