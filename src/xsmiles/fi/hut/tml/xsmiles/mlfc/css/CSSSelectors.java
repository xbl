
package fi.hut.tml.xsmiles.mlfc.css;

import java.util.StringTokenizer;
import org.w3c.dom.css.*;
import org.w3c.css.sac.*;
import org.w3c.dom.*;
import com.steadystate.css.parser.selectors.PseudoElementConditionImpl;
import com.steadystate.css.parser.selectors.ElementSelectorImpl;

import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.Selectors;
import fi.hut.tml.xsmiles.dom.PseudoElement;

public class CSSSelectors implements Selectors
{
	public CSSSelectors()
	{
	}
	
	public boolean selectorMatchesElem(Selector selector, Element element)
	{  
		
		if (selector.getSelectorType() == Selector.SAC_ELEMENT_NODE_SELECTOR ){
			return matchNodeSelector(selector, element);	
		}
		if (selector.getSelectorType() == Selector.SAC_CONDITIONAL_SELECTOR ){
			return matchConditionalSelector(selector, element);	
		}	
		if (selector.getSelectorType() == Selector.SAC_DESCENDANT_SELECTOR ){
			return matchDescendantSelector(selector, element);	
		}
		if (selector.getSelectorType() == Selector.SAC_CHILD_SELECTOR ){
			return matchChildSelector(selector, element);	
		}
		if (selector.getSelectorType() == Selector.SAC_ELEMENT_WITH_PSEUDO_ELEMENT_SELECTOR ){
			
			return matchPseudoElementSelector(selector, element);	
		}
	
		return false;
	}
        /**
         * match element name to selector name
         * in HTMLSelectors, this is overridden to match case insensitively
         */
	protected boolean matchElementName(String elementName, String selName)
        {
            return selName.equals(elementName);
        }
	boolean matchNodeSelector(Selector sel, Element elem)
	{
        if (elem == null)
            return false;
		String elementName = elem.getLocalName();
		String selName = sel.toString().trim();
		
        //    System.out.println("------------"+elem.getNamespaceURI()+"--------"+sel+"-----"+((ElementSelectorImpl)sel).getNamespaceURI());
        if (checkUri(sel,  elem)){
            if (selName.equals("*")){return true;}
            if (matchElementName(elementName,selName)){return true;}
        }
		return false;
	}
    
	boolean checkUri(Selector sel, Element elem)
    {
        String uriElement = elem.getNamespaceURI();
        String uriSelector = ((ElementSelectorImpl)sel).getNamespaceURI();
 //           System.out.println("------------S--"+uriSelector+"--------------E--"+uriElement);
        if (uriSelector==null) return true;
        if (uriSelector.equals("")&&(uriElement==null)) return true;
        if (uriSelector.equals("*")) return true;
        if (uriSelector.equals(uriElement)) return true;
        return false;
    }
    
	boolean matchConditionalSelector(Selector sel, Element elem)
	{
         
		ConditionalSelector condSel = (ConditionalSelector) sel;
		if (matchNodeSelector(condSel.getSimpleSelector(), elem)){
			Condition cond = condSel.getCondition();
			if (recursiveChecking(cond, elem)){
				return true;
			} 
		}	
		return false;
	}
	
	boolean recursiveChecking(Condition condition, Element elem){
					
		if (condition.getConditionType() == Condition.SAC_AND_CONDITION){
			CombinatorCondition comCon = (CombinatorCondition)condition;
			Condition cond1 = comCon.getFirstCondition();
			Condition cond2 = comCon.getSecondCondition();			
			if ((recursiveChecking(cond1,elem)) && (recursiveChecking(cond2,elem))){
				return true;
			}
		}else{     
			return matchCondition(condition,elem);
		}
		return false;
	}
	
	boolean matchCondition(Condition condition, Element elem){
	
		String elementName = elem.getLocalName();
		String className = elem.getAttribute("class");
		String idName = elem.getAttribute("id");

		if (condition.getConditionType() == Condition.SAC_CLASS_CONDITION){
			String condS = condition.toString().trim().substring(1);
			/* The list of delimiters is the union of the white space
			 * characters defined in html 4.0 and in xhtml/xml.
			 */
			StringTokenizer classes = new StringTokenizer(className, " \t\f\r\n\u200B");
			while (classes.hasMoreTokens()){
				if (condS.equals(classes.nextToken())){
					return true;
				}
			}
		}
		if (condition.getConditionType() == Condition.SAC_ID_CONDITION){
			String condS = condition.toString().trim().substring(1);
			if (condS.equals(idName)){
				return true;
			}
		}
		if (condition.getConditionType() == Condition.SAC_PSEUDO_CLASS_CONDITION){
			String condS = condition.toString().trim().substring(1);
			if (elem instanceof StylableElement){
				if (((StylableElement)elem).isPseudoClass(condS)){
					return true;
				}
			}
		}
		if (condition.getConditionType() == Condition.SAC_POSITIONAL_CONDITION){
			String condS = condition.toString().trim().substring(1);
			if (elem instanceof StylableElement){
				if (((StylableElement)elem).isPseudoClass(condS)){
					return true;
				}
			}
		}
		if (condition.getConditionType() == Condition.SAC_ATTRIBUTE_CONDITION){
			AttributeCondition condA = (AttributeCondition) condition;
			String CSSattrName = condA.getLocalName();
			String CSSattrValue = condA.getValue();
			if (matchAttributes(CSSattrName, CSSattrValue, elem)){
				return true;
			}
		}
/*		
        if (condition.getConditionType() == PseudoElementConditionImpl.CSS3_PSEUDO_ELEMENT_CONDITION){
			String condS = ((PseudoElementConditionImpl)condition).getValue();
			if (pseudoelement != null){
				if (condS.equals(pseudoelement)){
					return true;
				}
			}
		}
*/		
		return false;
	}
	
        /**
         * in HTMLSelectors, this is overridden to match case insensitively
         */
	protected String normalizeAttributeName(String attrName)
        {
            return attrName;
        }
        /**
         * in HTMLSelectors, this is overridden to match case insensitively
         */
	protected boolean matchAttributeName(String attrName, String selName)
        {
            return attrName.equals(selName);
        }
        boolean matchAttributes (String CSSattrN, String CSSattrV, Element elem)
	{
            CSSattrN = this.normalizeAttributeName(CSSattrN);
		if ( elem.hasAttribute(CSSattrN)){
            if (CSSattrV==null) return true;
			Attr attribute = elem.getAttributeNode(CSSattrN);
			String attributeValue = attribute.getValue();
			if (matchAttributeName(CSSattrV,attributeValue)){
				return true;
			}   
		}
		return false;
	}
	
	boolean matchDescendantSelector(Selector sel, Element elem)
	{
		DescendantSelector descSel = (DescendantSelector) sel;	
		if (selectorMatchesElem(descSel.getSimpleSelector(), elem)){
			Selector ancestor = descSel.getAncestorSelector();	
			if (elem.getParentNode() instanceof Element){
				Element parentElement = (Element)elem.getParentNode();
				if (parentElement != null){
					Element checkedElement = recursiveAnchestorChecking(ancestor,parentElement);	
					if (checkedElement != null){
						return true;
					}
				}
			}
		}	
		return false;
	}
	
	Element recursiveAnchestorChecking(Selector anc, Element elem){
		
		if (elem == null){ return null;}			
		if (anc.getSelectorType() == Selector.SAC_DESCENDANT_SELECTOR  ){
			DescendantSelector descSel = (DescendantSelector)anc;
			Selector ancestor = descSel.getAncestorSelector();
			Selector simple = descSel.getSimpleSelector();
			Element found = recursiveAnchestorChecking(ancestor, recursiveAnchestorChecking(simple, elem));
			return found;
		}else{
			return checkElement(anc, elem);
		}
	}
	
	Element checkElement(Selector simp, Element el){
	
		boolean found = false;
		while ((el != null) && !found){
			found = selectorMatchesElem(simp, el);		
			if (found == true){
				if (el.getParentNode() instanceof Element){
					el = (Element)el.getParentNode();
				}
				return el;
			}else{
				if (el.getParentNode() instanceof Element){
					el = (Element)el.getParentNode();
				}else{
					el = null;
				}
			}			
		}
		return el;
	}
	
	boolean matchChildSelector(Selector sel, Element elem)
	{
	if (sel.getSelectorType() == Selector.SAC_CHILD_SELECTOR){
			DescendantSelector descSel = (DescendantSelector) sel;
			Selector simple = descSel.getSimpleSelector();			
			Selector ancestor = descSel.getAncestorSelector();
		
			if (elem.getParentNode() instanceof Element){
				Element parentElem = (Element)elem.getParentNode();
			
				if (parentElem != null){	
					if (selectorMatchesElem(simple, elem) && 
						selectorMatchesElem(ancestor, parentElem)){
					
						return true;
					}
				}
			}	
		}else{
			return selectorMatchesElem(sel,elem);		
		}	
		return false;
	}
	boolean matchPseudoElementSelector(Selector sel, Element elem)
	{
	if (sel.getSelectorType() == Selector.SAC_ELEMENT_WITH_PSEUDO_ELEMENT_SELECTOR){
			DescendantSelector descSel = (DescendantSelector) sel;
			Selector simple = descSel.getSimpleSelector();			
			Selector ancestor = descSel.getAncestorSelector();
			if (elem.getParentNode() instanceof Element){
				Element parentElem = (Element)elem.getParentNode();
				if (parentElem != null){	
					if (selectorMatchesElem(simple, elem) && 
						selectorMatchesElem(ancestor, parentElem)){
						return true;
					}
				}
			}	
		}else{
			return selectorMatchesElem(sel,elem);		
		}	
		return false;
	}
	
        
        /** in HTML converts border=0 to border-width:0px, etc.  */
        public String getStyleAttrValue(StylableElement element)
        {
            return element.getStyleAttrValue();
        }
        
}
	

