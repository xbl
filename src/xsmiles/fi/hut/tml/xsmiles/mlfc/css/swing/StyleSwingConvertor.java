/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css.swing;

import java.awt.Color;
import java.awt.Font;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.text.AttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;

import fi.hut.tml.xsmiles.csslayout.StyleGenerator;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;



public class StyleSwingConvertor{
	
	
	// TODO: remove these, and use CSSConstants
	public static final String DEFAULT_FONT = "serif";
	public static final String ATTRIBUTE_FOREGROUND_COLOR = "color";
	public static final String ATTRIBUTE_BACKGROUND_COLOR = "background-color";
	public static final String ATTRIBUTE_REAL_BACKGROUND_COLOR = "real-background-color";
	public static final String ATTRIBUTE_BORDER_COLOR = "border-color";
	public static final String ATTRIBUTE_BORDER_TOP_COLOR = "border-top-color";
	public static final String ATTRIBUTE_BORDER_STYLE = "border-style";
	public static final String ATTRIBUTE_BORDER_TOP_STYLE = "border-top-style";
	public static final String ATTRIBUTE_BORDER_WIDTH = "border-width";
	public static final String ATTRIBUTE_BORDER_TOP_WIDTH = "border-top-width";
	public static final String ATTRIBUTE_FONT = "font";
	public static final String ATTRIBUTE_FONT_FAMILY = "font-family";
	public static final String ATTRIBUTE_FONT_STYLE = "font-style";
	public static final String ATTRIBUTE_FONT_WEIGHT = "font-weight";
	public static final String ATTRIBUTE_FONT_SIZE = "font-size";
	public static final String ATTRIBUTE_BACKGROUND_IMAGE = "background-image";
	public static final String ATTRIBUTE_BACKGROUND_POSITION = "background-position";
	public static final String ATTRIBUTE_H_BACK_POSITION = "h-back-position";
	public static final String ATTRIBUTE_V_BACK_POSITION = "v-back-position";
	public static final String ATTRIBUTE_BACKGROUND_REPEAT = "background-repeat";
	public static final String ATTRIBUTE_MARGIN = "margin";
	public static final String ATTRIBUTE_MARGIN_TOP = "margin-top";
	public static final String ATTRIBUTE_MARGIN_BOTTOM = "margin-bottom";
	public static final String ATTRIBUTE_MARGIN_RIGHT = "margin-right";
	public static final String ATTRIBUTE_MARGIN_LEFT = "margin-left";
	public static final String ATTRIBUTE_PADDING = "padding";
	public static final String ATTRIBUTE_PADDING_TOP = "padding-top";
	public static final String ATTRIBUTE_PADDING_BOTTOM = "padding-bottom";
	public static final String ATTRIBUTE_PADDING_RIGHT = "padding-right";
	public static final String ATTRIBUTE_PADDING_LEFT = "padding-left";
	public static final String ATTRIBUTE_TEXT_ALIGN = "text-align";
	public static final String ATTRIBUTE_TEXT_DECORATION = "text-decoration";
	public static final String ATTRIBUTE_WIDTH = "width";	
	public static final String ATTRIBUTE_HEIGHT = "height";
	public static final String ATTRIBUTE_CAPTION_SIDE = "caption-side";
    public static final String ATTRIBUTE_LIST_STYLE_TYPE = "list-style-type";
    public static final String ATTRIBUTE_LIST_STYLE_IMAGE = "list-style-image";
    public static final String ATTRIBUTE_LIST_STYLE_POSITION = "list-style-position";
       
//	private CSSValue value;

	public StyleSwingConvertor(){
	
	}

	
	public static Style convert(XSmilesCSSStyleDeclarationImpl declarationObj, String tagName){
	
		Style elStyle = (new StyleContext()).addStyle(tagName, null);
		CSSValue value;
		
		elStyle = addFontObj(elStyle, declarationObj);
		elStyle = addBorderObj(elStyle, declarationObj);
		elStyle = addBackObj(elStyle, declarationObj);
		elStyle = addMarginAndPaddingObj(elStyle, declarationObj);
        elStyle = addListObj(elStyle, declarationObj);
		
		value = declarationObj.getPropertyCSSValue(ATTRIBUTE_FOREGROUND_COLOR);			
		if (value != null){
			addColorAttribute(elStyle,ATTRIBUTE_FOREGROUND_COLOR, value);
		}
		value = declarationObj.getPropertyCSSValue(ATTRIBUTE_WIDTH);			
		if (value != null){
			addFloatAttribute(elStyle,ATTRIBUTE_WIDTH, value);
		}
		value = declarationObj.getPropertyCSSValue(ATTRIBUTE_TEXT_DECORATION);			
		if (value != null){
			addStringAttribute(elStyle,ATTRIBUTE_TEXT_DECORATION, value);
		}
		value = declarationObj.getPropertyCSSValue(ATTRIBUTE_TEXT_ALIGN);			
		if (value != null){
			addStringAttribute(elStyle,ATTRIBUTE_TEXT_ALIGN, value);
		}
		value = declarationObj.getPropertyCSSValue(ATTRIBUTE_HEIGHT);			
		if (value != null){
			addFloatAttribute(elStyle,ATTRIBUTE_HEIGHT, value);
		}
		value = declarationObj.getPropertyCSSValue(ATTRIBUTE_CAPTION_SIDE);			
		if (value != null){
			addStringAttribute(elStyle,ATTRIBUTE_CAPTION_SIDE, value);
		}
		value = declarationObj.getPropertyCSSValue(CSSConstants.CSS_DISPLAY_PROPERTY);			
		if (value != null){
			addStringAttribute(elStyle,CSSConstants.CSS_DISPLAY_PROPERTY, value);
		}		
        else
        {
            // if we don't put this default value, somehow it inherits this from parent...
			addStringAttribute(elStyle,CSSConstants.CSS_DISPLAY_PROPERTY, "inline");
        }
		value = declarationObj.getPropertyCSSValue(CSSConstants.CSS_VISIBILITY_PROPERTY);			
		if (value != null){
			addStringAttribute(elStyle,CSSConstants.CSS_VISIBILITY_PROPERTY, value);
		}		
		value = declarationObj.getPropertyCSSValue(CSSConstants.CSS_VERTICAL_ALIGN_PROPERTY);			
		if (value != null){
			addStringAttribute(elStyle,CSSConstants.CSS_VERTICAL_ALIGN_PROPERTY, value);
		}		
        
//	System.out.println(elStyle.getName()+"///////////////"+elStyle+"---------");
	
		return elStyle;
	}
	
	static Style addFontObj(Style styleP, XSmilesCSSStyleDeclarationImpl dec){
	
		CSSValue value = dec.getPropertyCSSValue(ATTRIBUTE_FONT_FAMILY);
		Font font = StyleGenerator.getCSSFontObj(dec);
		styleP.addAttribute(ATTRIBUTE_FONT, font); 

		return styleP;
	}
	
	static Style addBackObj(Style styleP, XSmilesCSSStyleDeclarationImpl dec){
		
		CSSValue value = dec.getPropertyCSSValue(ATTRIBUTE_BACKGROUND_COLOR);	
		if (value != null){
			addColorAttribute(styleP,ATTRIBUTE_BACKGROUND_COLOR, value);
			addColorAttribute(styleP,ATTRIBUTE_REAL_BACKGROUND_COLOR, value);
		} 
		value = dec.getPropertyCSSValue(ATTRIBUTE_BACKGROUND_IMAGE);			
		if (value != null){
			addURLAttribute(styleP,ATTRIBUTE_BACKGROUND_IMAGE, value);
			styleP.addAttribute(ATTRIBUTE_BACKGROUND_COLOR, StyleGenerator.getTransparentColor());
			addColorAttribute(styleP,ATTRIBUTE_REAL_BACKGROUND_COLOR, value);
			
		}else{
			try {
				// have to put a fake url to not inherit photos
	    		URL url = new URL("file:/C:/miocvs/xsmiles/demo");
				styleP.addAttribute(ATTRIBUTE_BACKGROUND_IMAGE, url);
							
			} catch (MalformedURLException mue) {
				}
		
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_BACKGROUND_REPEAT);			
		if (value != null){
			addStringAttribute(styleP,ATTRIBUTE_BACKGROUND_REPEAT, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_BACKGROUND_POSITION);	
		if (value != null){
		
			float HorizontalBP = 0.0f;
			float VerticalBP = 0.0f;
				
			if (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE){
				HorizontalBP = getFloatBackPosition(value,(short) 1);
				VerticalBP = getFloatBackPosition(value, (short)2);
			
			}else if(value.getCssValueType() == CSSValue.CSS_VALUE_LIST){
				CSSValueList vlValue = (CSSValueList)value;
				if (vlValue.getLength() == 2){
					value = vlValue.item(0);
					if (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE){
						CSSPrimitiveValue pv = (CSSPrimitiveValue)value;
						
						if (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE){
							HorizontalBP = getFloatBackPosition(value, (short)1);
							VerticalBP = getFloatBackPosition(vlValue.item(1), (short)2);
						
						}else if(pv.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT){
							String sValue =pv.getStringValue();						
							if (sValue.indexOf("left") != -1 ||
								(sValue.indexOf("right")) != -1){
								HorizontalBP = getFloatBackPosition(value, (short)1);
								VerticalBP = getFloatBackPosition(vlValue.item(1), (short)2);	
							}else if(sValue.indexOf("top") != -1 ||
									 (sValue.indexOf("bottom")) != -1){
								VerticalBP = getFloatBackPosition(value, (short)2);
								HorizontalBP = getFloatBackPosition(vlValue.item(1), (short)1);
							}else if(sValue.indexOf("center") != -1){
								value = vlValue.item(1);
								if (value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE){
									CSSPrimitiveValue pv2 = (CSSPrimitiveValue)value;
										String s2Value = pv2.getStringValue();
									if (s2Value.indexOf("left") != -1 ||
										(s2Value.indexOf("right")) != -1){
										HorizontalBP = getFloatBackPosition(value, (short)1);
										VerticalBP = getFloatBackPosition(value, (short)2);									
									}else if(s2Value.indexOf("top") != -1 ||
									 		 (s2Value.indexOf("bottom")) != -1){
										VerticalBP = getFloatBackPosition(value, (short)2);
										HorizontalBP = getFloatBackPosition(value, (short)1);
									}else if(s2Value.indexOf("center") != -1){
										VerticalBP = getFloatBackPosition(value, (short)2);
										HorizontalBP = getFloatBackPosition(value, (short)1);	
									}
								}
							}	 
						}
					}	
				}
			}
			
			Float hbp = new Float(HorizontalBP);
			Float vbp = new Float(VerticalBP);
			styleP.addAttribute(ATTRIBUTE_H_BACK_POSITION, hbp);
			styleP.addAttribute(ATTRIBUTE_V_BACK_POSITION, vbp);
		}
		return styleP;
	}
	
	static float getFloatBackPosition(CSSValue v, short flag){
	
		float floatValue = 0.5f;
		
		if (v.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE){
			CSSPrimitiveValue pv = (CSSPrimitiveValue)v;
			if (pv.getPrimitiveType() == CSSPrimitiveValue.CSS_PERCENTAGE){
				floatValue = (pv.getFloatValue(CSSPrimitiveValue.CSS_PERCENTAGE))/100;
			}else if(pv.getPrimitiveType() == CSSPrimitiveValue.CSS_IDENT){
				if (flag == 1){
					if ((pv.getStringValue()).equals("left")){
						floatValue = 0.0f;	
					}else if ((pv.getStringValue()).equals("right")){
						floatValue = 1.0f;	
					}else if ((pv.getStringValue()).equals("center")){
						floatValue = 0.5f;
					}
				}else if (flag == 2){
					if ((pv.getStringValue()).equals("top")){
						floatValue = 0.0f;	
					}else if ((pv.getStringValue()).equals("bottom")){
						floatValue = 1.0f;	
					}else if ((pv.getStringValue()).equals("center")){
						floatValue = 0.5f;	
					}
				}
			}
		}
		return floatValue;
	}
		
	static Style addBorderObj(Style styleP, XSmilesCSSStyleDeclarationImpl dec){
	
		CSSValue value = dec.getPropertyCSSValue(ATTRIBUTE_BORDER_TOP_COLOR);			
		if (value != null){
			addColorAttribute(styleP,ATTRIBUTE_BORDER_COLOR, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_BORDER_TOP_STYLE);			
		if (value != null){
			addStringAttribute(styleP,ATTRIBUTE_BORDER_STYLE, value);
		}else{
			styleP.addAttribute(ATTRIBUTE_BORDER_STYLE, new String("solid"));
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_BORDER_TOP_WIDTH);			
		if (value != null){
			if (value instanceof CSSPrimitiveValue){
				CSSPrimitiveValue pValue = (CSSPrimitiveValue)value;				
				if (pValue.getPrimitiveType()==CSSPrimitiveValue.CSS_IDENT){
			
					if ((pValue.getStringValue()).equals("thin")){
						Float val = new Float(1.0f);
						styleP.addAttribute(ATTRIBUTE_BORDER_WIDTH, val);
					}else if ((pValue.getStringValue()).equals("medium")){
						Float val = new Float(4.0f);
						styleP.addAttribute(ATTRIBUTE_BORDER_WIDTH, val);
					}else if ((pValue.getStringValue()).equals("thick")){
						Float val = new Float(6.0f);
						styleP.addAttribute(ATTRIBUTE_BORDER_WIDTH, val);
					}				
				}else{
					addFloatAttribute(styleP,ATTRIBUTE_BORDER_WIDTH, value);
				}
			}
		}else{
			styleP.addAttribute(ATTRIBUTE_BORDER_WIDTH, new Float(0.0f));
		}
		
		return styleP;
	}
	
	static Style addMarginAndPaddingObj(Style styleP, XSmilesCSSStyleDeclarationImpl dec){
	
		CSSValue value = dec.getPropertyCSSValue(ATTRIBUTE_MARGIN_TOP);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_MARGIN_TOP, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_MARGIN_RIGHT);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_MARGIN_RIGHT, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_MARGIN_BOTTOM);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_MARGIN_BOTTOM, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_MARGIN_LEFT);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_MARGIN_LEFT, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_PADDING_TOP);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_PADDING_TOP, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_PADDING_RIGHT);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_PADDING_RIGHT, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_PADDING_BOTTOM);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_PADDING_BOTTOM, value);
		}
		value = dec.getPropertyCSSValue(ATTRIBUTE_PADDING_LEFT);			
		if (value != null){
			addFloatAttribute(styleP,ATTRIBUTE_PADDING_LEFT, value);
		}
		return styleP;
	}
    
    static Style addListObj(Style styleP, XSmilesCSSStyleDeclarationImpl dec){
	
		CSSValue value = dec.getPropertyCSSValue(ATTRIBUTE_LIST_STYLE_TYPE);			
		if (value != null){
			addStringAttribute(styleP,ATTRIBUTE_LIST_STYLE_TYPE, value);
		}
        value = dec.getPropertyCSSValue(ATTRIBUTE_LIST_STYLE_IMAGE);			
		if (value != null){
			addURLAttribute(styleP,ATTRIBUTE_LIST_STYLE_IMAGE, value);
		}
        value = dec.getPropertyCSSValue(ATTRIBUTE_LIST_STYLE_POSITION);			
		if (value != null){
			addStringAttribute(styleP,ATTRIBUTE_LIST_STYLE_POSITION, value);
		}
        return styleP;
	}
	
	static void addColorAttribute(Style styleP , String attrName, CSSValue value){
	
		Color v = StyleGenerator.parseColor(value);
		if (v != null)	{	
			styleP.addAttribute(attrName, v);
		}			
	}

	static void addStringAttribute(Style styleP , String attrName, CSSValue value){
	
		String xStringValue = new String();
	
		if (value instanceof XSmilesCSSValueImpl){
        	XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl)value;			
            if (xValue.getPrimitiveType()==CSSPrimitiveValue.CSS_IDENT){
            	xStringValue = xValue.getStringValue();					
			}
		}	
		if (!xStringValue.equals("")){
			styleP.addAttribute(attrName, xStringValue);		
		}			
	}
    	static void addStringAttribute(Style styleP , String attrName, String value){
	
		if (value!=null && !value.equals("")){
			styleP.addAttribute(attrName, value);		
		}			
	}
	
	static void addFloatAttribute(Style styleP , String attrName, CSSValue value){
	
		Float xFloatValue = new Float(0.0f);
                
                // font size
                Object font= styleP.getAttribute(ATTRIBUTE_FONT);
                int fontSizeNum = 12;
                if (font!=null)
                {
                    Font f = (Font)font;
                    fontSizeNum = f.getSize();
                    
                }
                
                
		if (value instanceof XSmilesCSSValueImpl){		
                	XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl)value;			
                        xValue.setFontSize(fontSizeNum);
			xFloatValue = new Float(xValue.getFloatValue(CSSPrimitiveValue.CSS_PX));					
		}
		styleP.addAttribute(attrName, xFloatValue);
			
	}
	
	static void addURLAttribute(Style styleP , String attrName, CSSValue value){
	
		if (value instanceof XSmilesCSSValueImpl){
				
        	XSmilesCSSValueImpl xValue = (XSmilesCSSValueImpl)value;
			if (xValue.getPrimitiveType()==CSSPrimitiveValue.CSS_URI){
				try {
	    			URL url = new URL(xValue.getStringValue());
					styleP.addAttribute(attrName, url);
							
				} catch (MalformedURLException mue) {
					}					
			}
		}					
	}

	public static Style getContentStyle(AttributeSet parentStyle)
	{
  		if (parentStyle == null) return null;
			
		Style contentStyle = (new StyleContext()).addStyle("content", null);
    	if (parentStyle.getAttribute(ATTRIBUTE_BACKGROUND_COLOR) != null)
			contentStyle.addAttribute(ATTRIBUTE_BACKGROUND_COLOR, parentStyle.getAttribute(ATTRIBUTE_BACKGROUND_COLOR));
    	if (parentStyle.getAttribute(ATTRIBUTE_FOREGROUND_COLOR) != null)
			contentStyle.addAttribute(ATTRIBUTE_FOREGROUND_COLOR, parentStyle.getAttribute(ATTRIBUTE_FOREGROUND_COLOR));
    	if (parentStyle.getAttribute(ATTRIBUTE_FONT) != null)
	  		contentStyle.addAttribute(ATTRIBUTE_FONT, parentStyle.getAttribute(ATTRIBUTE_FONT));
		String dec = (String)parentStyle.getAttribute(ATTRIBUTE_TEXT_DECORATION);
		if (dec != null){
  			if (dec.equals("underline")){
				StyleConstants.setUnderline(contentStyle, true);
			}else if (dec.equals("none")){
				StyleConstants.setUnderline(contentStyle, false);
			}
		}
    	if (parentStyle.isDefined("href"))
	    	contentStyle.addAttribute("href", parentStyle.getAttribute("href"));
    	if (parentStyle.isDefined("target"))
	    	contentStyle.addAttribute("target", parentStyle.getAttribute("target"));
    	if (parentStyle.isDefined("isLink")){
    		contentStyle.addAttribute("isLink", parentStyle.getAttribute("isLink"));
			//StyleConstants.setUnderline(contentStyle, true);
		}
    	if (parentStyle.isDefined("CSSConstants.CSS_VISIBILITY_PROPERTY"))
    		contentStyle.addAttribute("CSSConstants.CSS_VISIBILITY_PROPERTY", parentStyle.getAttribute("CSSConstants.CSS_VISIBILITY_PROPERTY"));
		return contentStyle;
	}


}
