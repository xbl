/*****************************************************************************
 * Copyright (C) The Apache Software Foundation. All rights reserved.        *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the Apache Software License *
 * version 1.1, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/

//package org.apache.batik.css.value;
package fi.hut.tml.xsmiles.mlfc.css.value;

import com.steadystate.css.dom.Property;
import com.steadystate.css.parser.LexicalUnitImpl;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

//import org.apache.batik.css.CSSOMReadOnlyStyleDeclaration;
//import org.apache.batik.css.CSSOMReadOnlyValue;
//import org.apache.batik.css.HiddenChildElementSupport;
//import org.apache.batik.css.value.ImmutableValue;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSOMParser;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSValueImpl;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.ViewCSS;
import org.w3c.css.sac.InputSource;

import java.io.StringReader;

/**
 * This class provides a relative value resolver for the 'font-size' CSS
 * property.
 *
 * @author <a href="mailto:stephane@hillion.org">Stephane Hillion</a>
 * @version $Id: AbstractResolver.java,v 1.3 2006/01/27 09:32:45 honkkis Exp $
 */
public abstract class AbstractResolver implements RelativeValueResolver
    {
    // static // MH: mem test
	XSmilesCSSOMParser parser = new XSmilesCSSOMParser();
    /**
     * Resolve percentage values
     */
	 public CSSValue resolvePercentage (CSSPrimitiveValue pValue, CSSValue parentValue,XSmilesCSSStyleDeclarationImpl styleDeclaration)
	 {
	 	if (parentValue instanceof CSSPrimitiveValue)
		{
			try
			{
				XSmilesCSSValueImpl pParentValue = (XSmilesCSSValueImpl) parentValue;
				LexicalUnitImpl parentUnit = (LexicalUnitImpl)pParentValue.getLexicalUnit();
				if (parentUnit!=null)
				{  
					float multiplyBy = pValue.getFloatValue(CSSPrimitiveValue.CSS_NUMBER)/100f;
					float pFloatVal = pParentValue.getFloatValue(CSSPrimitiveValue.CSS_NUMBER);
					String stringValue =""+(multiplyBy*pFloatVal)+parentUnit.getDimensionUnitText();
					CSSPrimitiveValue newValue = (CSSPrimitiveValue)parser.parsePropertyValue(new InputSource(new StringReader(stringValue)));
					// replace the current value with the calculated value
					if (newValue !=null) 
					{
						styleDeclaration.removeProperty(this.getPropertyName());
						styleDeclaration.setProperty(this.getPropertyName(),newValue,AbstractResolver.getIsImportant(styleDeclaration,getPropertyName()));
					}
				}
			} catch (Exception e)
			{
				Log.error(e);
			}
		}
		else 
		{
			Log.error("resolvePercentage: parent was not primitive : "+parentValue);
		}
		return null;
	 }
	 
	 public static boolean getIsImportant (XSmilesCSSStyleDeclarationImpl style, String propertyName)
	 {
	 	Property prop = style.getProperty(propertyName);
		if (prop==null) return false;
		return prop.isImportant();
	 }
}
