/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css;

import java.awt.*;
import java.awt.event.*;

import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.content.ResourceFetcher;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;
import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;

// parser libraries
import java.lang.*;
import java.lang.Float;
import java.net.*;
import java.io.*;
import java.awt.Color;
import com.steadystate.css.*;
import com.steadystate.css.dom.*;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.stylesheets.MediaList ;

import org.w3c.dom.css.*;
import org.w3c.css.sac.CSSException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

/**
 * Parses CSS files into CSSStyleSheet objects 
 *
 * @author Mikko Honkala
 * @author Alessandro Cogliati
 */

public class CSSParser
{
    
    public static int STYLESHEET_USERAGENT = 1;
    public static int STYLESHEET_AUTHOR = 2;

    private MediaQueryEvaluator mqe = null;
    private ResourceFetcher fetcher = null;
    
	XSmilesCSSOMParser parser;
        XSmilesStyleSheet ssHandler;
	public CSSParser(XSmilesStyleSheet styleSheetHandler, MediaQueryEvaluator evaluator, ResourceFetcher f)
	{
	    this.fetcher=f;
		parser=new XSmilesCSSOMParser();
        ssHandler=styleSheetHandler;
        if (evaluator != null){
            this.mqe = evaluator;
        }
	}

	public CSSStyleSheet parse(InputSource input, URL baseURL,int flag, Vector styleSheetListDefault, Vector styleSheetListUser) throws Exception
	{
		Log.debug("CSSParser.parse called:"+baseURL);
		InputStream is;
		CSSStyleSheet stylesheet;
		CSSStyleDeclaration attrSet;
		CSSStyleDeclaration passedST;
        try
        {
    		stylesheet = parser.parseStyleSheet(input);
        } catch (java.lang.Error e)
        {
            Log.error(e,"while parsing css stylesheet: "+baseURL);
            return null;
        }catch (Exception e)
        {
            Log.error(e,e.toString()+"while parsing css stylesheet: "+baseURL);
            return null;
        }
		if (stylesheet instanceof CSSStyleSheetImpl)
		{
			((CSSStyleSheetImpl)stylesheet).setHref(baseURL.toString());		
		}
        checkRules(baseURL,stylesheet,styleSheetListDefault, styleSheetListUser,flag);
//        checkMedia(stylesheet,flag);

//		checkImport(baseURL,stylesheet,styleSheetListDefault, styleSheetListUser,flag); 
		if (flag == STYLESHEET_USERAGENT)
		{
			//styleSheetListDefault.insertElementAt(stylesheet,position);
			styleSheetListDefault.addElement(stylesheet);
		}else if(flag == STYLESHEET_AUTHOR)
		{
			//styleSheetListUser.insertElementAt(stylesheet,position);
			styleSheetListUser.addElement(stylesheet);
		}
		return stylesheet;
	}
	/** This method parses the stylesheet, reading also imported stylesheets */
	public CSSStyleSheet parse(URL cssLocationUrl, int flag, int position, Vector styleSheetListDefault, Vector styleSheetListUser) throws Exception
	{
        // This will also do basic authentication
		//Log.debug("CSSParser.parse. Will call HTTP to :"+cssLocationUrl);
	    short type = (flag==STYLESHEET_USERAGENT?Resource.RESOURCE_DO_NOT_STORE:Resource.RESOURCE_STYLESHEET);
		XSmilesConnection connection = this.fetcher.get(cssLocationUrl,type);
		//Log.debug("connection:"+connection);
		InputStream is = connection.getInputStream();
		//Log.debug("inputstream:"+is);
		InputSource input = new InputSource(new InputStreamReader(is));
		//Log.debug("inputsource:"+input);
		return this.parse(input,cssLocationUrl,flag,styleSheetListDefault,styleSheetListUser);
	}
	/** This method parses the stylesheet, reading also imported stylesheets */

	public CSSStyleSheet parse(URL baseURL, String cssString, int flag, int position, Vector styleSheetListDefault, Vector styleSheetListUser) throws Exception
	{
		InputSource input = new InputSource(new StringReader(cssString));
		return this.parse(input,baseURL,flag,styleSheetListDefault,styleSheetListUser);
	}

    public void checkRules(URL cssLocationUrl, CSSStyleSheet st,Vector styleSheetListDefault, Vector styleSheetListUser, int flag) throws Exception
	{
        if (st==null) return;
        CSSRuleList rList = st.getCssRules();
        int i=0;
        while (i!=rList.getLength()){
   			CSSRule cssr = rList.item(i);
			if (cssr.getType() == CSSRule.MEDIA_RULE){
				CSSMediaRule mediaRule = (CSSMediaRule)rList.item(i);
                MediaList mediaList = mediaRule.getMedia();
				XSmilesCSSRuleListImpl  mediaRuleList= (XSmilesCSSRuleListImpl)mediaRule.getCssRules(); 
                if (mqe != null){                
                    if (mqe.evalMediaQuery(mediaList.getMediaText()) == true){
                        int  mediaRuleListLength = mediaRuleList.getLength();
                        if (mediaRuleListLength != 0){
                            for (int j = mediaRuleListLength-1; j> -1; j--){
                                ((XSmilesCSSRuleListImpl)rList).insert( mediaRuleList.item(j), i);
                            }
                            ((XSmilesCSSRuleListImpl)rList).delete(i+(mediaRuleListLength));
                        }
					}
                }
			}
            if (cssr.getType() == CSSRule.IMPORT_RULE )
			{
                Log.debug("**** checkImport + "+cssLocationUrl);
				CSSImportRule importRule = (CSSImportRule)cssr;
				String href = importRule.getHref();
                String mediaText = importRule.getMedia().getMediaText();
                if (mqe != null){
                    if (mqe.evalMediaQuery(mediaText) == true){
						URL importUrl = new URL(cssLocationUrl, href);
                        Log.debug("Found an import rule: "+href);
                        CSSStyleSheet ss = this.parse(importUrl, flag, 1,styleSheetListDefault, styleSheetListUser);
                                ssHandler.prepareStyleSheet(ss);   
					}
                }	
			}
          i++;  
        }//while    
    }
    
    
	/** parses a single declaration string. Used with the style attribute */
	public XSmilesCSSStyleDeclarationImpl parseStyleAttrValue(String styleText)
	{
		try {
		  return (XSmilesCSSStyleDeclarationImpl)parser.parseStyleDeclaration(new InputSource(new StringReader( "{" + styleText + "}" )));
		} catch (Exception e) {
		  return null;
		}
	}

}	
	


