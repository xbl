/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css.swing;

import fi.hut.tml.xsmiles.dom.StylableElement;

import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.text.Style;

public interface SwingStylableElement extends StylableElement
{
	/** get the swing style for this element */
	public Style getElementStyle();
}
