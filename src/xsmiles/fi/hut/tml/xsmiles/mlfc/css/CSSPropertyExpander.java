/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleRule;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleSheetImpl;
import org.w3c.dom.css.*;
import fi.hut.tml.xsmiles.csslayout.CSS_Property;

import com.steadystate.css.dom.Property;
import com.steadystate.css.dom.DOMExceptionImpl;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.StringTokenizer;

public class CSSPropertyExpander
{

    public CSSPropertyExpander()
    {

    }

    public static boolean expandShorthands(XSmilesCSSStyleDeclarationImpl declaration,
            Property property, CSSRule parentRule)
    {

        if ((property.getName()).equals(CSS_Property.MARGIN))
        {

            expandProperty(declaration, property, CSS_Property.MARGIN_TOP, CSS_Property.MARGIN_RIGHT,
                    CSS_Property.MARGIN_LEFT, CSS_Property.MARGIN_BOTTOM);
            return true;

        }
        else if ((property.getName()).equals(CSS_Property.PADDING))
        {

            expandProperty(declaration, property, CSS_Property.PADDING_TOP, CSS_Property.PADDING_RIGHT,
                    CSS_Property.PADDING_LEFT, CSS_Property.PADDING_BOTTOM);
            return true;

        }
        else if ((property.getName()).equals(CSS_Property.BORDER_WIDTH))
        {

            expandProperty(declaration, property, CSS_Property.BORDER_TOP_WIDTH,
                    CSS_Property.BORDER_RIGHT_WIDTH, CSS_Property.BORDER_LEFT_WIDTH,
                    CSS_Property.BORDER_BOTTOM_WIDTH);
            return true;

        }
        else if ((property.getName()).equals(CSS_Property.BORDER_COLOR))
        {

            expandProperty(declaration, property, CSS_Property.BORDER_TOP_COLOR,
                    CSS_Property.BORDER_RIGHT_COLOR, CSS_Property.BORDER_LEFT_COLOR,
                    CSS_Property.BORDER_BOTTOM_COLOR);
            return true;

        }
        else if ((property.getName()).equals(CSS_Property.BORDER_STYLE))
        {

            expandProperty(declaration, property, CSS_Property.BORDER_TOP_STYLE,
                    CSS_Property.BORDER_RIGHT_STYLE, CSS_Property.BORDER_LEFT_STYLE,
                    CSS_Property.BORDER_BOTTOM_STYLE);
            return true;

        }
        else if ((property.getName()).equals(CSS_Property.BACKGROUND_IMAGE))
        {

            expandURL(declaration, property, parentRule);
            return true;

            //		TODO--but we don?t know how to do yet!!!

            //		}else if ((property.getName()).equals("border")){

            //		}else if ((property.getName()).equals("background")){

            //		}else if ((property.getName()).equals("font")){

        }
        else if ((property.getName()).equals(CSS_Property.LIST_STYLE_IMAGE))
        {
            expandURL(declaration, property, parentRule);
            return true;
        }

        return false;
    }

    static void expandProperty(XSmilesCSSStyleDeclarationImpl dec, Property prop, String top,
            String right, String left, String bottom)
    {
        if ((prop.getValue().getCssValueType()) == CSSValue.CSS_PRIMITIVE_VALUE)
        {

            CSSPrimitiveValue pValue = (CSSPrimitiveValue) prop.getValue();
            dec.setProperty(top, pValue, prop.isImportant());
            dec.setProperty(right, pValue, prop.isImportant());
            dec.setProperty(left, pValue, prop.isImportant());
            dec.setProperty(bottom, pValue, prop.isImportant());

        }
        else if ((prop.getValue().getCssValueType()) == CSSValue.CSS_VALUE_LIST)
        {

            CSSValueList cssVL = (CSSValueList) prop.getValue();
            int l = cssVL.getLength();
            if (l == 2)
            {
                dec.setProperty(top, cssVL.item(0), prop.isImportant());
                dec.setProperty(right, cssVL.item(1), prop.isImportant());
                dec.setProperty(left, cssVL.item(1), prop.isImportant());
                dec.setProperty(bottom, cssVL.item(0), prop.isImportant());
            }
            else if (l == 3)
            {
                dec.setProperty(top, cssVL.item(0), prop.isImportant());
                dec.setProperty(right, cssVL.item(1), prop.isImportant());
                dec.setProperty(left, cssVL.item(1), prop.isImportant());
                dec.setProperty(bottom, cssVL.item(2), prop.isImportant());
            }
            else if (l == 4)
            {
                dec.setProperty(top, cssVL.item(0), prop.isImportant());
                dec.setProperty(right, cssVL.item(1), prop.isImportant());
                dec.setProperty(left, cssVL.item(3), prop.isImportant());
                dec.setProperty(bottom, cssVL.item(2), prop.isImportant());
            }
        }
    }

    static void expandURL(XSmilesCSSStyleDeclarationImpl dec, Property prop, CSSRule parentRule)
    {

        if ((prop.getValue().getCssValueType()) == CSSValue.CSS_PRIMITIVE_VALUE)
        {

            CSSPrimitiveValue pValue = (CSSPrimitiveValue) prop.getValue();

            if (pValue.getPrimitiveType() == CSSPrimitiveValue.CSS_URI)
            {
                try
                {
                    if (parentRule == null)
                    {
                        Log.error("PropertyExp. Parent rule was null");
                        return;
                    }

                    CSSStyleSheet ss = parentRule.getParentStyleSheet();
                    if (ss == null)
                    {
                        Log.error("PropertyExp. Parent stylesheet was null");
                        return;
                    }
                    String href = ss.getHref();
                    URL baseUrl = new URL(href);
                    URL expandedUrl = new URL(baseUrl, pValue.getStringValue());
                    pValue.setStringValue(CSSPrimitiveValue.CSS_URI, expandedUrl.toString());

                    dec.setProperty(prop.getName(), pValue, prop.isImportant());
                }
                catch (MalformedURLException mue)
                {
                }
            }
        }
    }

}