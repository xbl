/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css.swing;

import fi.hut.tml.xsmiles.mlfc.css.XMLCSS;
import java.awt.*;
import java.awt.event.*;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.Log;
import java.io.*;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Vector;
import java.util.StringTokenizer;
import java.awt.Insets;
import javax.swing.border.*;
import javax.swing.text.*;

// parser libraries
import java.lang.*;
import java.lang.Float;
import java.net.*;
import java.io.*;
import java.awt.Color;
import com.steadystate.css.*;
//import org.w3c.dom.*;
import org.w3c.dom.css.*;
import org.w3c.css.sac.CSSException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
//  static final Border noMyBorder = new EmptyBorder(0,0,0,0);
  
  public class SwingBoxPainter implements Serializable {

	public SwingBoxPainter(AttributeSet a) {
		border = getBorder(a);
	    binsets = border.getBorderInsets(null);

		if (a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_TOP) != null){
	    	topMargin = (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_TOP))).floatValue());
		}else{topMargin= 0.0f;}
//		System.out.println(topMargin+"---------------------------------------");
		if (a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_BOTTOM) != null){
	    	bottomMargin = (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_BOTTOM))).floatValue());
		}else{bottomMargin= 0.0f;}
//		System.out.println(bottomMargin+"---------------------------------------");
		if (a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_LEFT) != null){
	    	leftMargin = (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_LEFT))).floatValue());
		}else{leftMargin= 0.0f;}
//		System.out.println(leftMargin+"---------------------------------------");
		if (a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_RIGHT) != null){
	    	rightMargin = (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_MARGIN_RIGHT))).floatValue());
		}else{rightMargin= 0.0f;}
//		System.out.println(rightMargin+"---------------------------------------");

		bg = (Color)a.getAttribute(StyleSwingConvertor.ATTRIBUTE_REAL_BACKGROUND_COLOR);
	    if (a.getAttribute(XMLCSS.ATTRIBUTE_BACKGROUND_IMAGE) != null) {
			bgPainter = new SwingBackgroundImagePainter(a);
	    }
	}

	Border getBorder(AttributeSet a2) {
	    //Border b = new LineBorder(Color.yellow,1);
		Border b = new EmptyBorder(0,0,0,0);
		Object o = null;
		int bwInt = 0;
         
	    o = (Object) a2.getAttribute(XMLCSS.ATTRIBUTE_BORDER_STYLE);
		//System.out.println(o+"-----------"+a2+"----------------------------");
	    if (o != null) {
			String bstyle = o.toString();
			Object bw = ((Object)a2.getAttribute(XMLCSS.ATTRIBUTE_BORDER_WIDTH));
			if (bw != null){
				bwInt = ((Float)bw).intValue();
			
				if (bwInt > 0) {
					if (bstyle.equals("inset")) {
						Color c = getBorderColor(a2);
						b = new BevelBorder(BevelBorder.RAISED, c.darker(), c.brighter());
//						b = new LineBorder(c, bw);
		  	  		} else if (bstyle.equals("outset")) {
						Color c = getBorderColor(a2);				
//						Border b1 = new BevelBorder(BevelBorder.RAISED, c.brighter(), c.darker());
						b = new BevelBorder(BevelBorder.LOWERED, c.darker(), c.brighter());
//						b = new CompoundBorder(b, b1);
//						b = new CompoundBorder(b, b);
				 
		    		} else if (bstyle.equals("solid")) {
						Color c = getBorderColor(a2);
						b = new LineBorder(c,bwInt);
					}
				}
			}
	    }
	    return b;
	}

	Color getBorderColor(AttributeSet a) {
	    Color colore = (Color)a.getAttribute(XMLCSS.ATTRIBUTE_BORDER_COLOR);
	    if (colore == null) {
			colore = (Color)a.getAttribute(XMLCSS.ATTRIBUTE_FOREGROUND_COLOR);
	    	if (colore == null) {
		    	colore = Color.black;
			}
	    }
	    return colore;
	}

	/**
	 * Fetches the inset needed on a given side to
	 * account for the margin, border, and padding.
	 *
	 * @param side The size of the box to fetch the
	 *  inset for.  This can be View.TOP,
	 *  View.LEFT, View.BOTTOM, or View.RIGHT.
	 * @param v the view making the request.  This is
	 *  used to get the AttributeSet, and may be used to
	 *  resolve percentage arguments.
	 * @exception IllegalArgumentException for an invalid direction
	 */
        public float getInset(int side, View v) {
	    AttributeSet a = v.getAttributes();
	    float inset = 0;
	    switch(side) {
	    case View.LEFT:
		inset += leftMargin;
		inset += binsets.left;
		if (a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_LEFT) != null){
	    	inset += (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_LEFT))).floatValue());
		}else{inset += (float)0.0;}
//		inset += (float)1.0;
		break;
	    case View.RIGHT:
		inset += rightMargin;
		inset += binsets.right;
		if (a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_RIGHT) != null){
	    	inset += (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_RIGHT))).floatValue());
		}else{inset += (float)0.0;}
//		inset += (float)1.0;
		break;
	    case View.TOP:
		inset += topMargin;
		inset += binsets.top;
		if (a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_TOP) != null){
	    	inset += (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_TOP))).floatValue());
		}else{inset += (float)0.0;}
//		inset += (float)1.0;
		break;
	    case View.BOTTOM:
		inset += bottomMargin;
		inset += binsets.bottom;
		if (a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_BOTTOM) != null){
	    	inset += (((Float)(a.getAttribute(XMLCSS.ATTRIBUTE_PADDING_BOTTOM))).floatValue());
		}else{inset += (float)0.0;}
//		inset += (float)1.0;
		break;
	    }
	    return inset;
	}

        public void paint(Graphics g, float x, float y, float w, float h, View v) {
	    // PENDING(prinz) implement real rendering... which would
	    // do full set of border and background capabilities.
	    // remove margin
	    x += leftMargin;
	    y += topMargin;
	    w -= leftMargin + rightMargin;
	    h -= topMargin + bottomMargin;
		
		int bwInt = 0;
	
		AttributeSet a3 = v.getAttributes();
		
	    if (bg != null) {
		g.setColor(bg);
		g.fillRect((int) x, (int) y, (int) w, (int) h);
	    }
		
		if (bgPainter != null) {
			Object bw = ((Object)a3.getAttribute(XMLCSS.ATTRIBUTE_BORDER_WIDTH));
			if (bw != null){
				bwInt = ((Float)bw).intValue();
			}
			bgPainter.paint(g, x+bwInt, y+bwInt, w-bwInt, h-bwInt, v);
	    }
	    border.paintBorder(null, g, (int) x, (int) y, (int) w, (int) h);
		
/*		
		border.paintBorder(null, g, (int) x, (int) y, (int) w, (int) h);
		Insets borderInsets = border.getBorderInsets(null);
		if (bgPainter != null) {
		bgPainter.paint(g, x+borderInsets.left, y+borderInsets.top, w-borderInsets.right, h-borderInsets.bottom, v);
	    }
*/
		
	}
	
	float topMargin;
	float bottomMargin;
	float leftMargin;
	float rightMargin;
	// Bitmask, used to indicate what margins are relative:
	// bit 0 for top, 1 for bottom, 2 for left and 3 for right.
	short marginFlags;
	Border border;
	Insets binsets;
//	CSS css;
	XSmilesStyleSheet ss;
	Color bg;
	SwingBackgroundImagePainter bgPainter;

}	
	
