/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css.swing;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.StyleSheet;

//---------------------------------------
//from  Stylesheet, keep those just for now! 
import java.util.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.*;
import javax.swing.event.ChangeListener;
import javax.swing.text.*;
//-------------------------------------------------


public class SwingStyleSheetWrapper extends StyleSheet
{
  public AttributeSet getViewAttributes(View v)
  {
    javax.swing.text.Element el = v.getElement();
    AttributeSet att = el.getAttributes();
    return att;
  }
}
