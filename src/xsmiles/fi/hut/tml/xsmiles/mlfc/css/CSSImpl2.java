/*
 * CSSImpl2.java
 *
 * Created on October 22, 2003, 1:10 PM
 */

package fi.hut.tml.xsmiles.mlfc.css;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.Selectors;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLConfigurer;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.general.MediaQueryEvaluator;


import com.steadystate.css.*;
import org.w3c.dom.css.*;
import org.w3c.css.sac.CSSException;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;

import java.util.Vector;
import java.net.URL;
/**
 *
 * @author  alessandro
 */
public class CSSImpl2 implements XSmilesStyleSheet{
    
    private URL baseURL;
    private URL defaultCssLoc;
    private Vector styleSheetListUser;
    private Vector styleSheetListDefault;
    private CSSParser parser;
    private MediaQueryEvaluator mqe;
    private RuleTree RuleTree;
    private RuleTreeNode ruleNode;
    private boolean useRuleTree=true;
    protected Selectors cssSelectors;
   
    
    public CSSImpl2(MediaQueryEvaluator mqe, Document doc) {
        super();
        if (doc instanceof ExtendedDocument)
        {
            try {
                XMLConfigurer config = ((ExtendedDocument)doc).getHostMLFC().getXMLDocument().getBrowser().getBrowserConfigurer();
                this.useRuleTree = config.getBooleanProperty("gui/cssoptimization");
            } catch (NullPointerException e) { }
        }
        this.mqe = mqe;
        styleSheetListDefault = new Vector();
        styleSheetListUser = new Vector();
        parser=new CSSParser(this, mqe,this.getMLFC(doc));
        this.createCSSSelectors(doc);
        RuleTree = new RuleTree();
    }
    
    MLFC getMLFC(Document doc)
    {
       return ((ExtendedDocument)doc).getHostMLFC();
    }
    
    public CSSImpl2() {
        super();
        Log.error("CSSImpl2 NEVER CALL THIS CONSTRUCTOR!"); // why is this here still?
        /*
        this.mqe = null;
        styleSheetListDefault = new Vector();
        styleSheetListUser = new Vector();
        parser=new CSSParser(this,mqe,);
        RuleTree = new RuleTree();
        */
    }
    
    protected void createCSSSelectors(Document doc)
    {
        boolean HTMLMode;
        if (doc instanceof ExtendedDocument)
            HTMLMode = ((ExtendedDocument)doc).isHTMLDocument();
        else HTMLMode = false;
        
        if (HTMLMode) this.cssSelectors=new CSSSelectorsHTML();
        else this.cssSelectors=new CSSSelectors();
    }
    
    public Selectors getCSSSelectors()
    {
        return this.cssSelectors;
    }
    
    
    /**
     * This method is the main entry point for the MLFCs. It gets the style for a single element.
     */
    public CSSStyleDeclaration getParsedStyle(StylableElement elem) {
//        Log.debug("getParsedStyle("+elem+")");
        try {
            XSmilesCSSStyleDeclarationImpl style=null;// = new XSmilesCSSStyleDeclarationImpl(null);
            org.w3c.dom.Element el = (org.w3c.dom.Element) elem;
            String elementName = el.getLocalName();
            XSmilesCSSStyleDeclarationImpl parentStyle=null;
            Node parentNode = el.getParentNode();
//            System.out.println(elem+ "--------------"+parentNode+"------------ hash:"+parentNode.hashCode());
            // first do inheritence from the parent's style
            if (parentNode instanceof StylableElement) {
                parentStyle = (XSmilesCSSStyleDeclarationImpl)((StylableElement)parentNode).getStyle();
//                Log.debug("parentStyle : "+parentStyle);
            }
            
            if(el instanceof XSmilesElementImpl && !((XSmilesElementImpl)el).getAuthorSheets())
            {
            	/*
            	 * TODO: set parentStyle as default style of any unmodified element so
            	 * that the inheriting of attributes from upper elements stops at the
            	 * element if it has its authorSheets set as false
            	 */
            }
            
            ruleNode = RuleTree.getRoot(); // get the opt-tree root
            
            // for all rules, check which ones apply to this element, and combine them into 'style'
            for (int z=0; z<styleSheetListDefault.size(); z++) {
                CSSStyleSheet ss = (XSmilesCSSStyleSheetImpl)styleSheetListDefault.elementAt(z);
                if (ss!=null)
                	ruleNode = matchRules(ss,elem,null);
            }    
            for (int z=0; z<styleSheetListUser.size(); z++) {
                CSSStyleSheet ss = (XSmilesCSSStyleSheetImpl)styleSheetListUser.elementAt(z);
                if (ss!=null)
                	ruleNode = matchRules(ss,elem,null);
            }
            // if the element supports the style attribute, it will return the contents
            String localstyle = this.cssSelectors.getStyleAttrValue(elem); 
            if (ruleNode.getCSSStyle()!=null&&localstyle!=null&&localstyle.length()>0) {
                // convert to CSSStyleDeclaration
                XSmilesCSSStyleDeclarationImpl localCSS = parser.parseStyleAttrValue(localstyle);
                if (localCSS!=null){
                	ruleNode = ruleNode.addLeaf(localCSS);
//                	 compute relative values
                	if (ruleNode.getCSSStyle()!=null && parentStyle!= null){
                		ruleNode.getCSSStyle().computeRelativeValues(elem,null,ruleNode.getCSSStyle(),parentStyle);
                    	// it is CSSImpl's responsibility to setStyle to the element
                		elem.setStyle(ruleNode.getCSSStyle());
                		return ruleNode.getCSSStyle();
                	}
                }                  
            }
            
            StylableElement brother = null;
            if (useRuleTree){
            	brother = ruleNode.checkBrothers(elem);
            }
        	
        	// Why does have to node take his sibling's style? Doesn't make sense.
/*
            if (brother !=  null&&brother.hasStyle()){
            	style = (XSmilesCSSStyleDeclarationImpl)brother.getStyle();
            	// it is CSSImpl's responsibility to setStyle to the element
            	elem.setStyle(style);
            }else*/
            {
            	// why do we create a new style object here?
            	 XSmilesCSSStyleDeclarationImpl s = new XSmilesCSSStyleDeclarationImpl(null);
            	 s.copyPropertySet(ruleNode.getCSSStyle());
//            	 compute relative values
            	 if (s!=null&&parentStyle!= null)
            	 {
                	s.computeRelativeValues(elem,null,s,parentStyle);
            	 }
            	 style = s;
                	// it is CSSImpl's responsibility to setStyle to the element
            	 elem.setStyle(style);
            	 ruleNode.addElement(elem);
            	 elem.setRuleNode(ruleNode);
            	 //System.out.println(elem+"---"+style+ "-----------------"+ruleNode+"------------");
            }
             return style;
        }
        catch (Exception e) {
            Log.error(e);
        }
        return null;
    }

    
    protected RuleTreeNode matchRules(CSSStyleSheet ss, StylableElement elem, XSmilesCSSStyleDeclarationImpl style) {
        // TODO: check class type && null
        XSmilesCSSRuleListImpl allRules=(XSmilesCSSRuleListImpl)ss.getCssRules();
        
        for (int i = 0; i<allRules.getLength(); i++) {
            Object r = allRules.item(i);
            if (! (r instanceof XSmilesCSSStyleRule))
            {
                Log.error(r+" was not instanceof XSmilesCSSStyleRule");
                
            }
            else
            {
                XSmilesCSSStyleRule rule = (XSmilesCSSStyleRule)allRules.item(i);
                if (rule.match(elem,this.getCSSSelectors())) {               
                    ruleNode = ruleNode.addChild(rule);
                }
            }
        }
//       style = ruleNode.getCSSStyle();

       return ruleNode;
    }
    
    public void prepareStyleSheet(CSSStyleSheet ss)
    {
    	    //Log.debug("prepareStyleSheet: "+ss);
            XSmilesCSSRuleListImpl rules = (XSmilesCSSRuleListImpl)ss.getCssRules();
            rules.setMediaQueryEvaluator(mqe);
            rules.sort();
    }
    
    /** Add a new default (User Agent) stylesheet */
    public void addXMLDefaultStyleSheet(URL defaultUrl) {
        try {
            CSSStyleSheet ss = parser.parse(defaultUrl, CSSParser.STYLESHEET_USERAGENT, 0,styleSheetListDefault,styleSheetListUser);
            defaultCssLoc = defaultUrl;
            this.prepareStyleSheet(ss);

        }
        catch (Exception e) {
            Log.error(e);
        }
        
    }
    
    /** Add a new author stylesheet */
    public void addXMLStyleSheet(URL stylesheetUrl) {
        try {
            CSSStyleSheet ss = parser.parse(stylesheetUrl, CSSParser.STYLESHEET_AUTHOR, 0,styleSheetListDefault,styleSheetListUser);
            this.prepareStyleSheet(ss);
        }
        catch (Exception e) {
            Log.error("While adding CSS stylesheet: "+e.getMessage());
        }
        
    }
    /** Add a new author stylesheet */
    public void addXMLStyleSheet(String stylesheetText,URL baseURL) {
        int p = 2;
        try {
            // pass the string to the parser
            CSSStyleSheet ss = parser.parse(baseURL,stylesheetText, p, 0,styleSheetListDefault,styleSheetListUser);
            this.prepareStyleSheet(ss);
        }
        catch (Exception e) {
            Log.error(e);
        }
        
    }
    

    
    
}
