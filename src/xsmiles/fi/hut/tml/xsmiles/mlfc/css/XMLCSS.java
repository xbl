/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css;

import fi.hut.tml.xsmiles.dom.XSmilesStyle;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Toolkit;
import java.io.*;
import java.lang.String;
import java.lang.Float;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;


/**
 This class has just some constants, and it should be removed and all
 references moved to reference CSSConstants interface.
 */
public class XMLCSS //extends CSS
{	
	
	public static final String DEFAULT_FONT = "TimesRoman";
	public static final String ATTRIBUTE_FOREGROUND_COLOR = "color";
	public static final String ATTRIBUTE_BACKGROUND_COLOR = "background-color";
	public static final String ATTRIBUTE_BORDER_COLOR = "border-color";
	public static final String ATTRIBUTE_BORDER_STYLE = "border-style";
	public static final String ATTRIBUTE_BORDER_WIDTH = "border-width";
	public static final String ATTRIBUTE_FONT = "font";
	public static final String ATTRIBUTE_FONT_FAMILY = "font-family";
	public static final String ATTRIBUTE_FONT_STYLE = "font-style";
	public static final String ATTRIBUTE_FONT_WEIGHT = "font-weight";
	public static final String ATTRIBUTE_FONT_SIZE = "font-size";
	public static final String ATTRIBUTE_BACKGROUND_IMAGE = "background-image";
	public static final String ATTRIBUTE_MARGIN = "margin";
	public static final String ATTRIBUTE_MARGIN_TOP = "margin-top";
	public static final String ATTRIBUTE_MARGIN_BOTTOM = "margin-bottom";
	public static final String ATTRIBUTE_MARGIN_RIGHT = "margin-right";
	public static final String ATTRIBUTE_MARGIN_LEFT = "margin-left";
	public static final String ATTRIBUTE_PADDING = "padding";
	public static final String ATTRIBUTE_PADDING_TOP = "padding-top";
	public static final String ATTRIBUTE_PADDING_BOTTOM = "padding-bottom";
	public static final String ATTRIBUTE_PADDING_RIGHT = "padding-right";
	public static final String ATTRIBUTE_PADDING_LEFT = "padding-left";
	public static final String PARENT_ATTRIBUTE_MARGIN = "parent-margin";
	public static final String PARENT_ATTRIBUTE_MARGIN_TOP = "parent-margin-top";
	public static final String PARENT_ATTRIBUTE_MARGIN_BOTTOM = "parent-margin-bottom";
	public static final String PARENT_ATTRIBUTE_MARGIN_RIGHT = "parent-margin-right";
	public static final String PARENT_ATTRIBUTE_MARGIN_LEFT = "parent-margin-left";
	public static final String PARENT_ATTRIBUTE_PADDING = "parent-padding";
	public static final String PARENT_ATTRIBUTE_PADDING_TOP = "parent-padding-top";
	public static final String PARENT_ATTRIBUTE_PADDING_BOTTOM = "parent-padding-bottom";
	public static final String PARENT_ATTRIBUTE_PADDING_RIGHT = "parent-padding-right";
	public static final String PARENT_ATTRIBUTE_PADDING_LEFT = "parent-padding-left";
	public static final String ATTRIBUTE_TEXT_ALIGN = "text-align";
	public static final String ATTRIBUTE_TEXT_DECORATION = "xtext-decoration";
	
	public static final String ATTRIBUTE_HEIGHT = "height";
	public static final String ATTRIBUTE_WIDTH = "width";
	public static final String ATTRIBUTE_CAPTION_SIDE = "caption-side";
		
	public XMLCSS()
	{
	
	}

}










