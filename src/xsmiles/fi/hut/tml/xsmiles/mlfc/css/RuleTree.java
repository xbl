/*
 * RuleTree.java
 *
 * Created on October 13, 2003, 3:16 PM
 */

package fi.hut.tml.xsmiles.mlfc.css;

import org.w3c.dom.*;
import java.util.Vector;
import java.util.Enumeration;
/**
 *
 * @author  alessandro
 */
public class RuleTree {
    
    public static final int ROOT = 1;
    public static final int NORMAL_NODE = 2;
    public static final int LEEF = 3;
    private RuleTreeNode root;
    
    /** Creates a new instance of RuleTree */
    public RuleTree() {
        root = createRoot();
        //System.out.println(root.isRoot()+"-------------------------------");
    }
    
    private RuleTreeNode createRoot(){
        return (new RuleTreeNode(ROOT));
    }
    
    public RuleTreeNode getRoot(){
        return root;
    }
    
    public void printTree(){
    
        RuleTreeNode r = this.getRoot();
        this.printNode(r);
    }
    
    public void printNode(RuleTreeNode rtn){
        //TODO
    }
    
    
}
