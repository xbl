/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.css.swing;

import fi.hut.tml.xsmiles.mlfc.css.XMLCSS;
import java.awt.*;
import java.awt.event.*;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.Log;
import java.io.*;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Vector;
import java.util.StringTokenizer;

// parser libraries
import java.lang.*;
import java.lang.Float;
import java.net.*;
import java.io.*;
import java.awt.Color;
import com.steadystate.css.*;
//import org.w3c.dom.*;
import org.w3c.dom.css.*;
import javax.swing.border.CompoundBorder;
import org.w3c.css.sac.CSSException;

import javax.swing.ImageIcon;
import javax.swing.text.AttributeSet;
import javax.swing.text.View;



import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
 public class SwingBackgroundImagePainter implements Serializable {
	ImageIcon   backgroundImage;
	float       hPosition;
	float       vPosition;
	// bit mask: 0 for repeat x, 1 for repeat y, 2 for horiz relative,
	// 3 for vert relative
	short       flags;
	// These are used when painting, updatePaintCoordinates updates them.
	private int paintX;
	private int paintY;
	private int paintMaxX;
	private int paintMaxY;

	SwingBackgroundImagePainter(AttributeSet a) {
	
	    backgroundImage = new ImageIcon((URL)a.getAttribute(XMLCSS.ATTRIBUTE_BACKGROUND_IMAGE));

		hPosition = 0.0f;
		vPosition = 0.0f;
		
		if (a.getAttribute(StyleSwingConvertor.ATTRIBUTE_H_BACK_POSITION) != null) {
			hPosition = ((Float)a.getAttribute(StyleSwingConvertor.ATTRIBUTE_H_BACK_POSITION)).floatValue();
		    flags |= 4;
		}
//		else if (pos.isHorizontalPositionRelativeToFontSize()) {
//		    hPosition *= css.getFontSize(a, 12);
//		}
		if (a.getAttribute(StyleSwingConvertor.ATTRIBUTE_V_BACK_POSITION) != null) {
			vPosition = ((Float)a.getAttribute(StyleSwingConvertor.ATTRIBUTE_V_BACK_POSITION)).floatValue();
		    flags |= 8;
		}
//		else if (pos.isVerticalPositionRelativeToFontSize()) {
//		    vPosition *= css.getFontSize(a, 12);
//		}
//	    }
	    // Determine any repeating values.
		if (a.getAttribute(StyleSwingConvertor.ATTRIBUTE_BACKGROUND_REPEAT) != null){
			if (((String)a.getAttribute(StyleSwingConvertor.ATTRIBUTE_BACKGROUND_REPEAT)).equals("repeat")){
				flags |= 3;
			}else if(((String)a.getAttribute(StyleSwingConvertor.ATTRIBUTE_BACKGROUND_REPEAT)).equals("repeat-x")){
				flags |= 1;
			}else if(((String)a.getAttribute(StyleSwingConvertor.ATTRIBUTE_BACKGROUND_REPEAT)).equals("repeat-y")){
				flags |= 2;
			}
		}else{
			flags |= 3;
		}
		
//	    CSS.Value repeats = (CSS.Value)a.getAttribute(CSS.Attribute.
//							  BACKGROUND_REPEAT);
//	    if (repeats == null || repeats == CSS.Value.BACKGROUND_REPEAT) {
//		flags |= 3;
//	    }
//	    else if (repeats == CSS.Value.BACKGROUND_REPEAT_X) {
//		flags |= 1;
//	    }
//	    else if (repeats == CSS.Value.BACKGROUND_REPEAT_Y) {
//		flags |= 2;
//	    }
	}

        void paint(Graphics g, float x, float y, float w, float h, View v) {
	    Rectangle clip = g.getClipRect();
	    if (clip != null) {
		// Constrain the clip so that images don't draw outside the
		// legal bounds.
		g.clipRect((int)x, (int)y, (int)w, (int)h);
	    }
	    if ((flags & 3) == 0) {
		// no repeating
		int width = backgroundImage.getIconWidth();
		int height = backgroundImage.getIconWidth();
		if ((flags & 4) == 4) {
		    paintX = (int)(x + w * hPosition -
				  (float)width * hPosition);
		}
		else {
		    paintX = (int)x + (int)hPosition;
		}
		if ((flags & 8) == 8) {
		    paintY = (int)(y + h * vPosition -
				  (float)height * vPosition);
		}
		else {
		    paintY = (int)y + (int)vPosition;
		}
		if (clip == null ||
		    !((paintX + width <= clip.x) ||
		      (paintY + height <= clip.y) ||
		      (paintX >= clip.x + clip.width) ||
		      (paintY >= clip.y + clip.height))) {
		    backgroundImage.paintIcon(null, g, paintX, paintY);
		}
	    }
	    else {
		int width = backgroundImage.getIconWidth();
		int height = backgroundImage.getIconHeight();
		if (width > 0 && height > 0) {
		    paintX = (int)x;
		    paintY = (int)y;
		    paintMaxX = (int)(x + w);
		    paintMaxY = (int)(y + h);
		    if (updatePaintCoordinates(clip, width, height)) {
			while (paintX < paintMaxX) {
			    int ySpot = paintY;
			    while (ySpot < paintMaxY) {
				backgroundImage.paintIcon(null, g, paintX,
							  ySpot);
				ySpot += height;
			    }
			    paintX += width;
			}
		    }
		}
	    }
	    if (clip != null) {
		// Reset clip.
		g.setClip(clip.x, clip.y, clip.width, clip.height);
	    }
	}

	private boolean updatePaintCoordinates
	         (Rectangle clip, int width, int height){
	    if ((flags & 3) == 1) {
		paintMaxY = paintY + 1;
	    }
	    else if ((flags & 3) == 2) {
		paintMaxX = paintX + 1;
	    }
	    if (clip != null) {
		if ((flags & 3) == 1 && ((paintY + height <= clip.y) ||
					 (paintY > clip.y + clip.height))) {
		    // not visible.
		    return false;
		}
		if ((flags & 3) == 2 && ((paintX + width <= clip.x) ||
					 (paintX > clip.x + clip.width))) {
		    // not visible.
		    return false;
		}
		if ((flags & 1) == 1) {
		    if ((clip.x + clip.width) < paintMaxX) {
			if ((clip.x + clip.width - paintX) % width == 0) {
			    paintMaxX = clip.x + clip.width;
			}
			else {
			    paintMaxX = ((clip.x + clip.width - paintX) /
					 width + 1) * width + paintX;
			}
		    }
		    if (clip.x > paintX) {
			paintX = (clip.x - paintX) / width * width + paintX;
		    }
		}
		if ((flags & 2) == 2) {
		    if ((clip.y + clip.height) < paintMaxY) {
			if ((clip.y + clip.height - paintY) % height == 0) {
			    paintMaxY = clip.y + clip.height;
			}
			else {
			    paintMaxY = ((clip.y + clip.height - paintY) /
					 height + 1) * height + paintY;
			}
		    }
		    if (clip.y > paintY) {
			paintY = (clip.y - paintY) / height * height + paintY;
		    }
		}
	    }
	    // Valid
	    return true;
	}
    }

//------------------------------------------------------------------------------------------		
