/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.css; 
 
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

public interface CSSStyleChangeListener {
    // notify that this style has changed
    public void styleChanged ();
}
