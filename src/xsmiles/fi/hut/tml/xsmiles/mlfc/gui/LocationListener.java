package fi.hut.tml.xsmiles.mlfc.gui;

public interface LocationListener {
  public void locationChanged(String s);
}
