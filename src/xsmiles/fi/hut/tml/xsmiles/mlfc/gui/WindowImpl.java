/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import java.awt.*;
import javax.swing.*;

/**
 * This holds the data of the current URI
 *
 * @author Juha
 */
public class WindowImpl extends XSmilesElementImpl  
{
  // GPSMLFC
  private GUIMLFC guiMLFC = null;
	
  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;
  private boolean initialized=false;

  /**
   * Constructor - Set the owner, name and namespace.
   */
  public WindowImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
    super(owner, namespace, tag);
    guiMLFC = gui;
    ownerDoc = owner;
    Log.debug("Status element created!");
  }

  protected void dispatch(String type) {	 
    if(!initialized) 
      return;
    // Dispatch the event
    Log.debug("Dispatching GUI Event");
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }
	
  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() {
    try {
      int w=Integer.parseInt(this.getAttribute("width"));
      int h=Integer.parseInt(this.getAttribute("height"));
      guiMLFC.getBrowser().getCurrentGUI().getWindow().setSize(w,h);
      initialized=true;
    } catch(Exception e) {

    }
  }

  public Window getWindow() 
  {
    return guiMLFC.getBrowser().getCurrentGUI().getWindow();
  }
  
  public void setSize(int w, int h) 
  {
    guiMLFC.getBrowser().getCurrentGUI().getWindow().setSize(w,h);
  }
  
  public void setLocation(int x, int y) 
  {
    guiMLFC.getBrowser().getCurrentGUI().getWindow().setLocation(x,y);
  }
  
  /**
   * get the screensize
   */
  public Dimension getScreenSize() 
  {
    return guiMLFC.getBrowser().getCurrentGUI().getWindow().getToolkit().getScreenSize();
  }

    /**
     * set current window to fullScreen
     */
    public void fullScreen() 
    {
	guiMLFC.getBrowser().getCurrentGUI().getWindow().setLocation(0,0);
	guiMLFC.getBrowser().getCurrentGUI().getWindow().setSize(guiMLFC.getBrowser().getCurrentGUI().getWindow().getToolkit().getScreenSize());
    }
    
  /**
   * Destroy this element.
   */
  public void destroy() {
  }
}

