/**
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;
import java.awt.*;
import java.util.Vector;
import java.util.Enumeration;


/**
 * GUI Element send dom changed events to inform 
 * changes in coordinate data
 *
 * @author Juha
 */
public class LoadAGuiImpl extends XSmilesElementImpl {

  // GPSMLFC
  private GUIMLFC guiMLFC = null;
	
  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;
  private Container contentPanel;
  private BrowserWindow browza;

  /**
   * Constructor - Set the owner, name and namespace.
   */
  public LoadAGuiImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
    super(owner, namespace, tag);
    guiMLFC = gui;
    ownerDoc = owner;
    Log.debug("ContentArea element created!");
  }

  private void dispatch(String type) {	 
    // Dispatch the event
    Log.debug("Dispatching GUI Event");
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }
	
  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() {
    browza=guiMLFC.getXMLDocument().getBrowser();
    String name=this.getAttribute("name");
    String href=this.getAttribute("href");
    String current=browza.getGUIManager().getCurrentGUIName();
    // maybe this still works? who knows? -MH
    //if(!href.equals("")) 
      //browza.getBrowserConfigurer().setGUIProperty(browza, "file", href);

    Log.debug("GUI.init");
    for(Enumeration e=browza.getGUIManager().getGUINames().elements();e.hasMoreElements();) {
      if(name.equals((String)e.nextElement()) && !name.equals(current)) {
	browza.getGUIManager().showGUI(name);
	return;
      }
    }
  }
  
  /**
   * Destroy this element.
   */
  public void destroy() {
    
    Log.debug("destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?");
  }
}

