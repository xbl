/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.net.URL;
import java.util.Enumeration;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XPanel;

/**
 * GUI Element send dom changed events to inform changes in coordinate data
 * 
 * @author Juha
 */
public class ContentAreaImpl extends XSmilesElementImpl implements VisualComponentService {

    // GPSMLFC
    private GUIMLFC guiMLFC = null;

    // XSmilesDocumentImpl - to create new elements
    private DocumentImpl ownerDoc = null;
    private Container contentPanel;

    // Browser of this content area
    private BrowserWindow browza;
    private BrowserWindow guiBrowser;

    /**
     * Constructor - Set the owner, name and namespace.
     */
    public ContentAreaImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
        super(owner, namespace, tag);
        guiMLFC = gui;
        ownerDoc = owner;
        Log.debug("ContentArea element created!");
    }

    private Container getContentPanel() {
        if (contentPanel == null) {
            contentPanel = browza.getContentArea();
        }
        return contentPanel;
    }

    private void dispatch(String type) {
        // Dispatch the event
        Log.debug("Dispatching GUI Event");
        org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        ((org.w3c.dom.events.EventTarget) this).dispatchEvent(evt);
        return;
    }

    /**
     * Initialize this element. This requires that the DOM tree is available.
     */
    public void init() {
        guiBrowser = guiMLFC.getBrowser(); // Get a reference to the original "parent" browser

        String id = this.getAttribute("id");
        String uri = this.getAttribute("uri");
        String type = this.getAttribute("type");
        // if uri == "" and this is the first contentArea, then we
        // use the first contentArea

        if (id != null || !id.equals(""))
            browza = guiBrowser.getBrowserWindow(id);

        if (browza == null) {
            if (id == null || id.equals("")) {
                if (uri == null || uri.equals("")) {
                    Log.debug("Creating browserwindow with url: " + guiBrowser.getDocumentHistory().getLastDocument().getURLString());
                    browza = guiBrowser.newBrowserWindow(guiBrowser.getDocumentHistory().getLastDocument().getURLString(), false);
                }
            } else if (id != null || !id.equals("")) {
                if (uri == null || uri.equals("")) {

                    if (guiBrowser.getDocumentHistory().getLastDocument() == null)
                        browza = guiBrowser.newBrowserWindow(false);
                    else {
                        Log.debug("Creating browserwindow with url and id: " + guiBrowser.getDocumentHistory().getLastDocument().getURLString() + " " + id);
                        browza = guiBrowser.newBrowserWindow(guiBrowser.getDocumentHistory().getLastDocument().getURLString(), id, false);
                    }
                }
            } else if (id == null || id.equals("")) {
                if (uri != null || !uri.equals(""))
                    browza = guiBrowser.newBrowserWindow(uri, false);
            } else if (id != null || !id.equals(""))
                if (uri != null || !uri.equals(""))
                    browza = guiBrowser.newBrowserWindow(uri, id, false);
        } else { // SOME BROWSER FOUND
            if (uri != null && !uri.equals("")) {
                browza.openLocation(uri);
            }
        }

        /**
         * get the constraints straight
         */
        if (type != null && !type.equals("")) {
            browza.getGUIManager().setCurrentGUIName(type);
        }
    }

    /**
     * Destroy this element.
     */
    public void destroy() {
        if (contentPanel != null)
            getContentPanel().removeAll();
        contentPanel = null;
        if (browza != null)
            browza.closeBrowserWindow();

        guiMLFC = null;
        ownerDoc = null;
        contentPanel = null;

        browza = null;
        guiBrowser = null;
    }

    /**
     * Return the visual component for this extension element
     */
    public Component getComponent() {
        return getContentPanel();
    }

    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize() {
        return getContentPanel().getSize();
    }

    public void setZoom(double zoom) {
        Log.debug("Zoom does not work");
    }

    public void setVisible(boolean visible) {
        getContentPanel().setVisible(visible);
    }

    public boolean getVisible() {
        return getContentPanel().isVisible();
    }

    // HERE BEGIN THE METHODS INTENDED FOR JAVASCRIPT
    /**
     * Redraws the GUI if possible
     */
    public void reloadGUI() {
        Log.debug("reloadGUI called");
        Log.debug(guiMLFC.toString());
        Log.debug(guiMLFC.getBrowser().toString());
        if (guiMLFC != null) {
            guiMLFC.getBrowser().getGUIManager().getCurrentGUI().reDrawGUI();

        }
    }

    protected ComponentFactory getComponentFactory() {
        return browza.getComponentFactory();
    }
    
    protected XPanel getControlBar() {
        return browza.getMLFCControls().getMLFCToolBar();
    }

    /**
     * Navigate back
     */
    public void back() {
        browza.navigate(NavigationState.BACK);
    }

    /**
     * Navigate forward
     */
    public void forward() {
        browza.navigate(NavigationState.FORWARD);
    }

    /**
     * Navigate Home
     */
    public void home() {
        browza.navigate(NavigationState.HOME);
    }

    /**
     * RELOAD
     */
    public void reload() {
        browza.navigate(NavigationState.RELOAD);
    }

    public void openLocation(String s) {
        URL u = null;
        try {
            if (guiBrowser.getXMLDocument() != null)
                u = new URL(guiBrowser.getXMLDocument().getXMLURL(), s);
            else
                u = new URL(s);
        } catch (Exception e) {
            Log.error("XMLGUI could not create URI");
        }
        if (u != null)
            browza.openLocation(u);
    }

    /**
     * Navigate to the current directory with the directory protocol
     */
    public void openFile() {
        // get current directory
        // create a dir: url out of it
        // navigate to it.
        //browza.
    }

    /**
     * Save current dom to file. Maybe navigate to some save file document
     */
    public void saveFile() {
        // not implemented yet
    }

    /**
     * Close the current browserwindow
     */
    public void closeWindow() {
        browza.closeBrowserWindow();
    }

    /**
     * Open up a new window
     */
    public void newWindow() {
        browza.newBrowserWindow();
    }

    /**
     * @return Get the stylesheet titles
     */
    public Enumeration getStylesheetTitles() {
        return browza.getXMLDocument().getStylesheetTitles().elements();
    }

    /**
     * Change stylesheet
     */
    public void setStylesheet(String s) {
        browza.getXMLDocument().setPreferredStylesheetTitle(s);
        browza.navigate(NavigationState.RELOAD);
    }

    /**
     * @return The title of the current stylesheet
     */
    public String getCurrentStylesheetTitle() {
        return browza.getXMLDocument().getCurrentStylesheetTitle();
    }

    /**
     * @return For hardcore user, access for the browserwindow.. not very secure though...
     */
    protected BrowserWindow getBrowserWindow() {
        return browza;
    }

    public void setGUI(String title) {
        guiMLFC.getBrowser().getGUIManager().showGUI(title);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event, Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}

