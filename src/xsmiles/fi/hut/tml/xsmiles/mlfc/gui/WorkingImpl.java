/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.gui.components.awt.Animation;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.event.*;
import java.awt.*;
import javax.swing.JPanel;

/**
 * Animator.. the so called "raksutin"
 *
 * @author Juha
 */
public class WorkingImpl extends XSmilesElementImpl implements VisualComponentService 
{
  // GPSMLFC
  private GUIMLFC guiMLFC = null;
  	
  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;
  private Animation raksutin;
  private WorkingSniffer sniffa;
  private BrowserWindow targetBrowser;
  
  /**
   * Constructor - Set the owner, name and namespace.
   */
  public WorkingImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
    super(owner, namespace, tag);
    guiMLFC = gui;
    raksutin=new Animation(200);
    raksutin.play();
    raksutin.pause();
    ownerDoc = owner;
  }
  
  private void dispatch(String type) {	 
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }
  	
  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() 
  {
    ContentAreaImpl ca=null;
    try {
      // Add listener to the correct content area
      String target=this.getAttribute("contentArea");
      XSmilesDocumentImpl d=(XSmilesDocumentImpl)guiMLFC.getXMLDocument().getDocument();

      if(target==null || target.equals("")) {
	// Get the first contentArea
	ca=(ContentAreaImpl)d.getElementsByTagName("contentArea").item(0);
      } else {
	ca=(ContentAreaImpl)d.getElementById(target);
      }
    } catch(Exception e) {
      Log.debug("No target id found for working element");
      e.printStackTrace();
    }
    if(ca!=null) {
      sniffa=new WorkingSniffer(raksutin);
      targetBrowser=ca.getBrowserWindow();
      targetBrowser.getEventBroker().addGUIEventListener(sniffa);
    } else { 
      Log.debug("MLFC ERROR: ContentArea not found. Try inserting the contentArea tag first, before any other XMLGUI elements.");
    }
    
    
  }
  
  /**
   * Destroy this element.
   */
  public void destroy() {
    raksutin.pause();
    if(targetBrowser!=null && sniffa!=null)
      if(targetBrowser.getEventBroker()!=null)
	targetBrowser.getEventBroker().removeGUIEventListener(sniffa);
    targetBrowser=null;
  }
  
  /**
   * Return the visual component for this extension element
   */
  public Component getComponent()
  {
    return raksutin;
  }
  
  /**
   * Returns the approximate size of this extension element
   */
  public Dimension getSize()
  {
    return raksutin.getSize();
  }
  
  public void setZoom(double zoom)
  {
    Log.debug("Zoom does not work");
  }

  public void setVisible(boolean visible)
  {
    raksutin.setVisible(visible);
  }

  public boolean getVisible()
  {
    return raksutin.isVisible();
  }
  
  private class WorkingSniffer extends GUIEventAdapter 
  {
    Animation an;
    public WorkingSniffer(Animation a) 
    {
      an=a;
    }

    public void browserWorking() {
      //an.play();
      raksutin.play();
    }

    public void browserReady() {
      //an.pause();
      raksutin.pause();
    }
  }

/* (non-Javadoc)
 * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
 */
public void visualEvent(int event,Object obj)
{
    // TODO Auto-generated method stub
    
}
}

