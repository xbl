/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import java.awt.*;
import javax.swing.*;

/**
 * This holds the data of the current URI
 *
 * @author Juha
 */
public class CurrentURIImpl extends XSmilesElementImpl  {

  // GPSMLFC
  private GUIMLFC guiMLFC = null;
	
  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;
  private boolean initialized=false;
  private LocList locList;

  /**
   * Constructor - Set the owner, name and namespace.
   */
  public CurrentURIImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
    super(owner, namespace, tag);
    guiMLFC = gui;
    ownerDoc = owner;
    Log.debug("Status element created!");
  }

  protected void dispatch(String type) {	 
    if(!initialized) 
      return;
    // Dispatch the event
    Log.debug("Dispatching GUI Event");
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }
	
  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() {
    Log.debug("GUI.init");
    try {
      locList=new LocList(this);
      // LISTENER guiMLFC.getXMLGUI().addLocationListener(locList);
      initialized=true;
    } catch(Exception e) {
      Log.debug("Shag it. DefaultComponentfactoryshite null or something");
      e.printStackTrace();
    }
  }
  
  /**
   * Destroy this element.
   */
  public void destroy() {
    Log.debug("Viddu destroyataanko t�t� elementtie� koskaan?");
    // LISTENER guiMLFC.getXMLGUI().removeLocationListener(locList);
    // LISTENER guiMLFC.getXMLGUI().removeAllListeners();
  }

  public String getLocation() 
  {
    return getFirstChild().getNodeValue();
  }

  private class LocList implements LocationListener {
    private CurrentURIImpl imp;
    private int count=0;
    
    public LocList(CurrentURIImpl i) {
      imp=i;
    }

    public void locationChanged(String s) {
      Log.debug("SETTING CURRENTURI NODEVALUE TO " + s);
      imp.getFirstChild().setNodeValue(s);
      imp.dispatch("currentURIChanged");
    }
  }
}

