/* 
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import java.net.URL;
import java.awt.Dimension;
import java.awt.Container;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;
import fi.hut.tml.xsmiles.XMLDocument;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import java.util.*;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;

/**
 * GUIMLFC is a utility class for creating GUIs with the XML-Languages 
 * supported by X-Smiles.. 
 * 
 * Namespace: http://www.xsmiles.com/GUI-2002-JEEJEE
 */
public class GUIMLFC extends CoreMLFC {

    protected Container contentPanel;
    private Vector guiElements;
    private BrowserWindow browser;
    protected boolean started=false;

    /**
     * Constructor.
     */
    public GUIMLFC()
    {
      guiElements=new Vector();
    }

    /**
     * Get the browser, which the GUI document was loaded from
     */
    protected BrowserWindow getBrowser()
    {
	if(browser==null) {
	    try {
		browser=getBrowserWindow();
	    } catch(Exception e) {
		e.printStackTrace();
		getMLFCListener().showErrorDialog("XMLGUI Error", 
						  "Could not locate the BrowserWindow instance. Problem with GUIMLFC, not you. Sorry for the inconvenience.");
	    }
	    Log.debug(browser.toString());
	}
	return browser;
    }
    
    /*
    protected String getLocalname(String tagname)
    {
	int index = tagname.indexOf(':');
	if (index<0) return tagname;
	String localname = tagname.substring(index+1);
	return localname;
    }
    */
    
    /**
     * Create a DOM element.
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag)
    {
	
	Element element = null;
	Log.debug("GUIMLFC Element created: "+ns+":"+tag);
	
	if (tag == null)
	    {
		Log.error("Invalid null tag name!");
		return null;
	    }
	String localname = getLocalname(tag);
	
	if (localname.equals("contentArea")) {
	    Log.debug("**** Creating content area data element");
	    element = new ContentAreaImpl(doc, this, ns, tag);
	} else if(localname.equals("mlfcControls")) {
	    element = new ControlsAreaImpl(doc, this, ns,tag);
	} else if(localname.equals("raksutin")) {
	    element = new WorkingImpl(doc, this, ns,tag);
	} else if(localname.equals("animation")) {
	    element = new WorkingImpl(doc, this, ns,tag);
	} else if(localname.equals("statusBar")) {
	    element = new StatusAreaImpl(doc, this, ns,tag);
	} else if(localname.equals("loadAGUI")) {
	    element = new LoadAGuiImpl(doc, this, ns,tag);
	} else if(localname.equals("currentURI")) {
	    element = new CurrentURIImpl(doc, this, ns,tag);
	} else if(localname.equals("window")) {
	    element = new WindowImpl(doc, this, ns,tag);
	} else if(localname.equals("remoteControl")) {
	    element = new RemoteImpl(doc, this, ns,tag);
	}
	
	
	if(element!=null)
	    guiElements.addElement(element);
	// Other tags go back to the XSmilesDocument as null...
	if(element==null) 
	    {
		Log.debug("tag null!!");
	    }
	return element;
    } 
    
    public void start() 
    { 
	XMLDocument doc = this.getXMLDocument(); 
	Log.debug("EVENTS.initialize() called."); 
	ECMAScripter ecma = doc.getECMAScripter(); 
	// SVG may have already initilized ecmascripter 
	if (!ecma.isInitialized()) ecma.initialize(doc.getDocument());
	started=true;
    }
    
    /**
     * Append the given URL to be a full URL.
     * @param partURL		Partial URL, e.g. fanfaari.wav
     * @return  Full URL, e.g. http://www.x-smiles.org/demo/fanfaari.wav
     */
    public URL createURL(String partURL)
    {
	try
	    {
		return new URL(this.getXMLDocument().getXMLURL(),partURL);
	    } catch (java.net.MalformedURLException e)
		{
		    Log.error(e);
		    return null;
		}
    }
    
    /**
     * Not implemented method.
     */
    public void setSize(Dimension d) {
    }
    
    /**
     * Called from the LinkHandler - this method asks the browser to go to this external link.
     * @param url		URL to jump to.
     */
    public void gotoExternalLink(String url)
    {
	Log.debug("...going to "+url);
	URL u = createURL(url);
	getMLFCListener().openLocation(u);
    } 
    
    /**
     * Display status text in the broswer. Usually shows the link destination.
     */
    public void displayStatusText(String url)
    {
	getMLFCListener().setStatusText(url);
    }
    
    
    
    /**
     * The opposite of init()
     * deactivate is only called for displayable MLFCs
     */
    public void stop() {
		Log.debug("Destroying objects related with GUI MLFC");
		if (guiElements!=null)
		{
			Enumeration e = guiElements.elements();
			while(e.hasMoreElements()) 
			    ((XSmilesElementImpl)e.nextElement()).destroy();
			guiElements=null;
		}
    }
}
