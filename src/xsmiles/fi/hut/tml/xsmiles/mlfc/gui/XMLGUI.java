package fi.hut.tml.xsmiles.mlfc.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Window;
import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JTextField;
import javax.swing.JWindow;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.content.ContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.awt.Animation;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.components.swing.XAFrame;
import fi.hut.tml.xsmiles.gui.media.swing.SwingContentHandlerFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.gui.swing.XSmilesUI;


/**
 * A GUI, which is to be configured via XML
 *
 * @author juha
 */
public class XMLGUI extends XSmilesUI
{
    private   Container        presentationPanel;
    private   Container        contentPanel;
    protected ComponentFactory componentFactory;
    protected MLFCControls mlfcControls;
    public    BrowserWindow    browser;
    private   Window          frame;
    private   Animation        raksutin;
    private   JTextField       statusBar;
    private   Vector           statListeners,locationListeners, stateListeners;
    private   XLink            guiFile;
    private   String           guiFileString;
    private   boolean          contentAreaSwitch=true;

    public XMLGUI(BrowserWindow b, Container c) 
    {
	super(b,c);
	browser=b;
	//presentationPanel=c;
	init();
    }

    public XMLGUI(BrowserWindow b)
    {
	super(b);
	browser=b;
	//presentationPanel=browser.getContentArea();
	init();
    }

    private void init() 
    {
	raksutin=new Animation(200);
	statusBar=new JTextField();
	statusBar.setEditable(false);
	statusBar.setForeground(Color.black);
	statusBar.setBackground(Color.white);
	statListeners=new Vector();
	locationListeners=new Vector();
	stateListeners=new Vector();

	String borders=guiFileString=browser.getBrowserConfigurer().getGUIProperty(browser, "borders");
	if(borders!=null && borders.equals("none")) {
	    frame=new JWindow();
	    contentPanel=((JWindow)frame).getContentPane();
	} else {
	    frame=new XAFrame();
	    contentPanel=((XAFrame)frame).getContentPane();
	}
	try {
	  guiFileString=browser.getBrowserConfigurer().getGUIProperty(browser.getGUIManager().getCurrentGUIName(), "file");
	  guiFile=new XLink("file:" + new File(guiFileString).getAbsolutePath(),1);
	  guiFileString=guiFile.getURLString();
	  Log.debug("OPEN GUI FILE " + guiFile.getURLString());
	    componentFactory=new DefaultComponentFactory(browser);
	    mlfcControls = new DefaultMLFCControls(componentFactory);
	} catch(Exception e) {
	    Log.error("Error initializing browser, exiting.." + e.toString());
	    e.printStackTrace();
	    System.err.println("Error " +e.toString());
	    //System.exit(0);
	}
	contentPanel.add(browser.getContentArea());
	frame.setSize(800,600);
	frame.show();
	frame.setVisible(true);
	frame.addWindowListener(new java.awt.event.WindowAdapter() 
	    {
		public void windowClosing(java.awt.event.WindowEvent e)
		{
		    browser.closeBrowserWindow();
		}
	    });
    }

    public void start() 
    {
      browser.openLocation(guiFile, false);
    }

    /**
     * Reload xml file and draw the GUI
     */
    public void reDrawGUI()
    {
	try {
	    removeAllListeners();
	    Log.debug("OPEN GUI FILE " + guiFile.toString());
	    browser.openLocation(guiFile, false);
	} catch(Exception e) {
	    Log.error("Error initializing browser, exiting.." + e.toString());
	    e.printStackTrace();
	    System.err.println("Error " +e.toString());
	    //System.exit(0);
	}
    }
  

    /**
     * @return A ComponentFactory which is capable of returning all components needed
     *         by MLFCs, or any other non-GUI package classes.
     * @see ComponentFactory
     */
    public ComponentFactory getComponentFactory()
    {
	return componentFactory;
    }

    /**
     * Destroy The GUI (delete frame, etc)
     */
    public void destroy() 
    {
	frame.dispose();
	browser=null;
    }

    /**
     * @return the Frame of current GUI
     */
    public Window getWindow()
    {
	return frame;
    }

    public ContentHandlerFactory getContentHandlerFactory()
    {
      if (contentHandlerFactory==null) contentHandlerFactory=new SwingContentHandlerFactory(browser);
      return contentHandlerFactory;
    }

    /**
     * Informs the gui that browser is working
     */
    public void browserWorking()
    {
	raksutin.play();
	for(Enumeration e=stateListeners.elements();e.hasMoreElements();) {
	    StateListener sl=(StateListener)e.nextElement();
	    sl.working();
	}
	Log.debug("Working");
    }

    /**
     * Informs the gui that browser is resting
     */
    public void browserReady()
    {
	raksutin.pause();
	for(Enumeration e=stateListeners.elements();e.hasMoreElements();) {
	    StateListener sl=(StateListener)e.nextElement();
	    sl.resting();
	}
	Log.debug("Resting");
    }

    /**
     * @param s The location that is beeing loaded
     */
    public void setLocation(String s)
    {
	for(Enumeration e=locationListeners.elements();e.hasMoreElements();) {
	    LocationListener sl=(LocationListener)e.nextElement();
	    sl.locationChanged(s);
	}
    }

    public void removeAllListeners() 
    {
	locationListeners.removeAllElements();
	statListeners.removeAllElements();
	stateListeners.removeAllElements();
    }
  
    public void addStateListener(StateListener l) 
    {
	stateListeners.addElement(l);
    }
  
    public void removeStateListener(StateListener l)
    {
	stateListeners.removeElement(l);
    }
  
    public void addLocationListener(LocationListener l) 
    {
	locationListeners.addElement(l);
    }

    public void removeLocationListener(LocationListener l) 
    {
	locationListeners.removeElement(l);
    }

    /**
     * @param statusText The text to put in status bar
     */
    public void setStatusText(String statusText)
    {
	statusBar.setText(statusText);
	for(Enumeration e=statListeners.elements();e.hasMoreElements();) {
	    StatusListener sl=(StatusListener)e.nextElement();
	    sl.setStatusText(statusText);
	}
	Log.debug("Status text set");
    }

    public void addStatusListener(StatusListener l) {
	statListeners.addElement(l);
    }

    public void removeStatusListener(StatusListener l) {
	statListeners.removeElement(l);
    }

    public JTextField getStatusBar() {
	return statusBar;
    }
    
    /**
     * @param value Set state of back widget
     */
    public void setEnabledBack(boolean value)
    {
	Log.debug("Back enabled");
    }
  
    /**
     * @param value Set state of forward widget
     */
    public void setEnabledForward(boolean value)
    {
	Log.debug("Forward enabled");
    }

    /**
     * @param value Set state of home widget
     */
    public void setEnabledHome(boolean value)
    {
	Log.debug("Home neabled");
    }


    /**
     * @param value Set state of top widget
     */
    public void setEnabledStop(boolean value)
    {
	Log.debug("Enabling stop");
    }

    /** 
     * @param value Set state of reload widget
     */
    public void setEnabledReload(boolean value)
    {
	Log.debug("Reload being enabled");
    }

    /**
     * @param title Set title of UI frame
     */
    public void setTitle(String title)
    {
	//frame.setTitle(title);
    }

    public Animation getRaksutin() 
    {
	return raksutin;
    }
    
    public BrowserWindow getBrowserWindow() 
    {
	if(contentAreaSwitch) {
	    contentAreaSwitch=false;
	    return browser;
	} else 
	    return null;
	
    }
  
    public void validate()
    {
	frame.validate();
    }
  
    public void setWindowSize(int w, int h) 
    {
	frame.setSize(w,h);
    }
    
    /** @return whether or not this gui should be instructed to reload.
     * XMLGUI should return false, since it creates an extra content area and
     * browser for the content
     */
    public boolean shouldReloadAtStartup()
    {
        return false;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.XSmilesUIAWT#getMLFCControls()
     */
    public MLFCControls getMLFCControls() {
        return mlfcControls;
    }


}
