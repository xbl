/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.event.*;
import java.awt.*;
import javax.swing.*;

/**
 * GUI Element send dom changed events to inform 
 * changes in coordinate data
 * Also provides a Swing textfield as visual component service
 *
 * @author Juha
 */
public class StatusAreaImpl extends XSmilesElementImpl implements VisualComponentService {

    private GUIMLFC guiMLFC = null;
	
    // XSmilesDocumentImpl - to create new elements
    private DocumentImpl ownerDoc = null;
    private JTextField statusBar;
    private boolean initialized=false;
    private BrowzaListener l;
    private BrowserWindow b;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public StatusAreaImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
	super(owner, namespace, tag);
	guiMLFC = gui;
	ownerDoc = owner;
	statusBar=new JTextField();
	Log.debug("Status element created!");
    }

    protected void dispatch(String type) {	 
	// Dispatch the event
	Log.debug("Dispatching GUI Event " + type);
	org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
	evt.initEvent(type, true, true);
	((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
	return;
    }
	
    /**
     * Initialize this element.
     * This requires that the DOM tree is available.
     */
    public void init() {
	Log.debug("GUI.init");
	try {
	  XSmilesDocumentImpl d=(XSmilesDocumentImpl)guiMLFC.getXMLDocument().getDocument();
	  String id=this.getAttribute("contentArea");
	  ContentAreaImpl ca=null;
	  if(id==null || id.equals("")) {
	    ca=(ContentAreaImpl)d.getElementsByTagName("contentArea").item(0);
	  } else {
	    ca=(ContentAreaImpl)d.getElementById(id);
	  }
	  if(ca != null) {
	    b=ca.getBrowserWindow();
	    l=new BrowzaListener(this);
	    b.getEventBroker().addBrowserEventListener(l);
	    b.getEventBroker().addGUIEventListener(l);
	  } else 
	    {
	      Log.debug("MLFC ERROR: ContentArea not found. Try inserting the contentArea tag first, before any other XMLGUI elements.");

	    }
	} catch(Exception e) {
	  e.printStackTrace();
	}
    }
  
    /**
     * Destroy this element.
     */
    public void destroy() {
      //Log.debug("Viddu destroyataanko t?t? elementtie? koskaan?");
      if(b!=null && l!=null) {
	if(b.getEventBroker()!=null) {
	  b.getEventBroker().removeBrowserEventListener(l);
	  b.getEventBroker().removeGUIEventListener(l);
	}
      }
      b=null;
      
    }

    /**
     * Return the visual component for this extension element
     */
    public Component getComponent()
    {
	return statusBar;
    }
  
    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize()
    {
	return statusBar.getSize();
    }

    public void setZoom(double zoom)
    {
	Log.debug("Zoom does not work");
    }
    public void setVisible(boolean visible)
    {
	statusBar.setVisible(visible);
    }
    public boolean getVisible()
    {
	return statusBar.isVisible();
    }
  
    public String getText() 
    {
	return statusBar.getText();
    }
    
    public void setText(String t) 
    {
	statusBar.setText(t);
    }
    
    private class BrowzaListener extends GUIEventAdapter implements BrowserEventListener 
    {
      StatusAreaImpl imp;
      
      public BrowzaListener(StatusAreaImpl i)
      {
	imp=i;
      }
      
      public void browserStateChanged(int s, String text)
      {
	imp.setText(text);
	imp.dispatch("textChanged");
      }
      
      public void browserWorking() 
      {
	imp.dispatch("browserWorking");
      }
      
      public void browserReady() 
      {
	imp.dispatch("browserReady");
      }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event, Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}


