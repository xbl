/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

/**
 * �Status text changes through this listener
 * @author Juha
 */
public interface StateListener  
{
  public void working();
  public void resting();
}

