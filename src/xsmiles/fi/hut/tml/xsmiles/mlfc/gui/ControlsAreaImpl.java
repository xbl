/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;
import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.gui.mlfc.DefaultMLFCControls;
import fi.hut.tml.xsmiles.mlfc.css.RuleTreeNode;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * GUI Element send dom changed events to inform changes in coordinate data
 * 
 * @author Juha
 */
public class ControlsAreaImpl extends XSmilesElementImpl implements VisualComponentService {

    private GUIMLFC guiMLFC = null;

    // XSmilesDocumentImpl - to create new elements
    private DocumentImpl ownerDoc = null;
    //private DefaultComponentFactory componentFactory;
    private XPanel mlfcControlbar;

    private ComponentFactory componentFactory;

    /**
     * Constructor - Set the owner, name and namespace.
     */
    public ControlsAreaImpl(DocumentImpl owner, GUIMLFC gui, String namespace, String tag) {
        super(owner, namespace, tag);
        guiMLFC = gui;
        ownerDoc = owner;
        Log.debug("Controls element created!");
    }

    private void dispatch(String type) {
        // Dispatch the event
        Log.debug("Dispatching GUI Event");
        org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        ((org.w3c.dom.events.EventTarget) this).dispatchEvent(evt);
        return;
    }

    /**
     * Initialize this element. This requires that the DOM tree is available.
     */
    public void init() {
        try {
            XSmilesDocumentImpl d = (XSmilesDocumentImpl) guiMLFC.getXMLDocument().getDocument();
            String id = this.getAttribute("contentArea");
            ContentAreaImpl ca = null;
            if (id == null || id.equals("")) {
                // Get the first contentArea
                ca = (ContentAreaImpl) d.getElementsByTagName("contentArea").item(0);
            } else {
                ca = (ContentAreaImpl) d.getElementById(id);
            }
            if (ca != null) {    
                componentFactory = ca.getComponentFactory();
                mlfcControlbar = ca.getControlBar();
            } else {            
                componentFactory = new DefaultComponentFactory();
                mlfcControlbar = new DefaultMLFCControls(componentFactory).getMLFCToolBar();
            }
        } catch (Exception e) {
            Log.debug("Shag it. DefaultComponentfactoryshite null or something");
            e.printStackTrace();
            // A plain placeholder
            componentFactory = new DefaultComponentFactory();
            mlfcControlbar = new DefaultMLFCControls(componentFactory).getMLFCToolBar(); 
        }
    }

    /**
     * Destroy this element.
     */
    public void destroy() {
        Log.debug("Viddu destroyataanko t?t? elementtie? koskaan?");
        mlfcControlbar.removeAll();
    }

    /**
     * Return the visual component for this extension element
     */
    public Component getComponent() {
        ((Component) mlfcControlbar).setVisible(true);
        return (Component) mlfcControlbar;
    }

    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize() {
        return ((Component) mlfcControlbar).getSize();
    }

    public void setZoom(double zoom) {
        Log.debug("Zoom does not work");
    }

    public void setVisible(boolean visible) {
        ((Component) mlfcControlbar).setVisible(visible);
    }

    public boolean getVisible() {
        return ((Component) mlfcControlbar).isVisible();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.StylableElement#setStyle(org.w3c.dom.css.CSSStyleDeclaration)
     */
    public void setStyle(CSSStyleDeclaration style)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.StylableElement#setRuleNode(fi.hut.tml.xsmiles.mlfc.css.RuleTreeNode)
     */
    public void setRuleNode(RuleTreeNode rtn)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
     */
    public XSmilesConnection get(URL dest, short type)
    {
        // TODO Auto-generated method stub
        return null;
    }
}

