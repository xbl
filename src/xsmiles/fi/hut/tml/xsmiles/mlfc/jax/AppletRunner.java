/**
 * Copyright(c) 1996 DTAI, Incorporated (http://www.dtai.com)
 *
 *                        All rights reserved
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of DTAI, Incorporated not be used in
 * advertising or publicity pertaining to this material without the
 * specific, prior written permission of an authorized representative of
 * DTAI, Incorporated.
 *
 * DTAI, INCORPORATED MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES,
 * EXPRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST
 * INFRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL DTAI, INCORPORATED OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELATING
 * TO THE SOFTWARE.
 *
 * 02.06.2001 Modifications by Juha 
 * 12.03.2002 Jukka: Added function stubs for JDK1.4 AppletContext...
 */
package fi.hut.tml.xsmiles.mlfc.jax;

import java.awt.*;
import java.applet.*;
import java.net.*;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.*;
import java.io.*;
//import javax.swing.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * AppletRunner - implements AppletContext and AppletStub to allow any applet to
 * easily run as an application. The only thing it can't do is access URL's.
 * Applet parameters are entered on the command line with name as one word and
 * value as the next.
 * 
 * @version 1.1
 * @author DTAI, Incorporated, Juha
 * 
 * $Id: AppletRunner.java,v 1.12 2005/03/16 16:12:15 honkkis Exp $
 * 
 * $Source:
 * /home/cvs/xsmiles/src/xsmiles/fi/hut/tml/xsmiles/mlfc/jax/AppletRunner.java,v $
 */

public class AppletRunner extends Container implements AppletStub,
        AppletContext//, URLStreamHandlerFactory
{

    //private JTextField status;

    private Hashtable params = new Hashtable();

    private Vector applets = new Vector();

    private int initial_width;

    private int initial_height;

    private String argsit[], codeBase;

    private URL documentBase;

    private Applet applet;
    
    private MLFCListener mlfcListener;

    /**
     * Entry point into the standalone program.
     * 
     * @param args
     *                  the command line arguments
     */
    /*
    public static void main(String args[])
    {
        new AppletRunner(args);
    }*/

    /**
     * Constructor for the main class, given an existing applet object.
     * 
     * @param applet
     *                  the applet embedded in this AppletContext
     * @param codeBase
     * @param documentBase
     * @param width
     * @param height
     *                  height and width, and any applet parameters
     */
    public AppletRunner(Applet applet, String cB, URL docBase, int w, int h, MLFCListener listener)
    {
        mlfcListener = listener;
        documentBase = docBase;
        codeBase = cB;
        init(applet, w, h);
    }

    /**
     * Constructor for the main class, given an existing applet object and a
     * default frame (window) width and height.
     * 
     * @param applet
     *                  the applet embedded in this AppletContext
     * @param default_width
     *                  the default width of the window
     * @param default_height
     *                  the default width of the window
     * @param args
     *                  the command line arguments. Contains possibly height and
     *                  width, and any applet parameters
     */
    /*
    public AppletRunner(Applet applet, int default_width, int default_height,
            String args[])
    {

        //super ( applet.getClass().getName() );
        super();
        argsit = args;

        init(applet, default_width, default_height, args, 0);
    }*/

    /**
     * Constructor for the main class, from the command line arguments.
     * 
     * @param args
     *                  the command line arguments. Contains the name of the applet
     *                  class, possibly height and width, and any applet parameters.
     */
    /*
    public AppletRunner(String args[])
    {
        //super ( args[0] );
        super();

        try
        {
            Applet applet = (Applet) Class.forName(args[0]).newInstance();

            init(applet, 640, 480, args, 1);
        } catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }*/

    /*
     * PRIVATE initialization function.
     * 
     * @param applet the applet embedded in this AppletContext @param
     * default_width the default width of the window @param default_height the
     * default width of the window @param args the command line arguments.
     * Contains possibly height and width, and any applet parameters @param
     * startidx index in the args array at which to start parsing
     */
    private void init(Applet applet, int default_width, int default_height,
            String args[], int startidx)
    {

        //URL.setURLStreamHandlerFactory( this );
        this.applet = applet;
        applets.addElement(applet);
        applet.setStub(this);

        initial_width = default_width;
        initial_height = default_height;

        //status = new JTextField();
        //status.setBackground(Color.white);
        //status.setEditable(false);

        add("Center", applet);
        //add("South", status);

        appletResize(initial_width, initial_height);

        show();
    }

    private void init(Applet applet, int default_width, int default_height)
    {

        //URL.setURLStreamHandlerFactory( this );
        this.applet = applet;
        applets.addElement(applet);
        applet.setStub(this);

        initial_width = default_width;
        initial_height = default_height;

        //status = new JTextField();
        //status.setBackground(Color.white);
        //status.setEditable(false);

        add("Center", applet);
        //add("South", status);

        appletResize(initial_width, initial_height);
        setVisible(true);
        show();
    }

    /**
     * Starts the crapplet
     */
    public void runApplet()
    {
        applet.init();
        applet.start();
    }
    public void stop()
    {
        applet.stop();
        applet.destroy();
        if (applet.getParent()!=null)
            applet.getParent().remove(applet);
    }

    /**
     * A hashtable containing all the parameters, with the name as
     * a key
     *
     * @param @p The hashtable
     */
    public void setParams(Hashtable p)
    {
        params = p;
    }

    public void setDimension(Dimension d)
    {
        initial_width = d.width;
        initial_height = d.height;
    }

    /**
     * Event handler to catch the Frame (window) close action, and exit the
     * program.
     * 
     * @param evt
     *                  The event that occurred
     * @return false if the event was not handled by this object.
     */
    public boolean handleEvent(Event evt)
    {

        if (evt.id == Event.WINDOW_DESTROY)
        {
            System.exit(0);
        }

        return super.handleEvent(evt);
    }

    /** ********** AppletStub methods ************ */

    /**
     * Returns true if the applet is active.
     * 
     * @return always true
     */
    public boolean isActive()
    {
        return true;
    }

    /**
     * Gets the document URL.
     * 
     * @return a "file:" URL for the current directory
     */
    public URL getDocumentBase()
    {
        URL url = null;
        try
        {
            if (documentBase != null)
                return documentBase;
            /*
             * if(codeBase.endsWith(".zip")||codeBase.endsWith(".jar")
             * ||codeBase.endsWith(".ZIP")||codeBase.endsWith(".JAR")) { url=new
             * URL(codeBase.substring(0,codeBase.lastIndexOf("/")));
             * Log.debug(url.toString());
             */
            else
            {
                //if(codeBase!=null)
                url = new URL(codeBase);
                Log.debug(url.toString());
            }
            //Log.error("No documentBase or codebase specified");
        } catch (MalformedURLException mue)
        {
            mue.printStackTrace();
        }
        return url;

    }

    /**
     * Gets the codebase URL.
     * 
     * @return in this case, the same value as getDocumentBase()
     */
    public URL getCodeBase()
    {
        URL url = null;
        try
        {
            if (codeBase == null)
                return getDocumentBase();
            else if (documentBase != null)

                url = new URL(getDocumentBase(),codeBase);
            else
                Log.error("No codeBase specified");

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return url;
    }

    /**
     * Gets a parameter of the applet.
     * 
     * @param name
     *                  the name of the parameter
     * @return the value, or null if not defined
     */
    public final String getParameter(String name)
    {
        return (String) params.get(name);
    }

    /**
     * Gets a handler to the applet's context.
     * 
     * @return this object
     */
    public final AppletContext getAppletContext()
    {
        return this;
    }

    /**
     * Called when the applet wants to be resized. This causes the Frame
     * (window) to be resized to accomodate the new Applet size.
     * 
     * @param width
     *                  the new width of the applet
     * @param height
     *                  the new height of the applet
     */
    public void appletResize(int width, int height)
    {

        //Insets insets = getInsets();

        Log.debug("Setting size to :" + width + " " + height);
        applet.setSize(width, height);
        //invalidate();
        //validate();
        //doLayout();
        //applet.setPreferredSize(width,height);
        //setPreferedSize(width,height);
        //resize(width,height);

        //setSize(width,height + status.preferredSize().height);
    }

    /** ***** so that this is a perfect component */
    public Dimension getPreferredSize()
    {
        return new Dimension(initial_width, initial_height);
    }

    public Dimension getSize()
    {
        return new Dimension(initial_width, initial_height);
    }

    /** ********** AppletContext methods ************ */

    /**
     * Gets an audio clip. (There doesn't seem to be a "Toolkit" for audio clips
     * in my JDK, so this always returns null. You could implement this
     * differently, returning a dummy AudioClip object for which the class could
     * be defined at the bottom of this file.)
     * 
     * @param url
     *                  URL of the AudioClip to load
     * @return the AudioClip object if it exists (in our case, this is always
     *              null
     */
    public final AudioClip getAudioClip(URL url)
    {
        return new AudioClipImpl(url);
    }

    /**
     * Gets an image. This usually involves downloading it over the net.
     * However, the environment may decide to cache images. This method takes an
     * array of URLs, each of which will be tried until the image is found.
     * 
     * @param url
     *                  URL of the Image to load
     * @return the Image object
     */
    public final Image getImage(URL url)
    {
        return Toolkit.getDefaultToolkit().getImage(url);
    }

    /**
     * Gets an applet by name.
     * 
     * @param name
     *                  the name of the applet
     * @return null if the applet does not exist, and it never does since we
     *              never name the applet.
     */
    public final Applet getApplet(String name)
    {
        return null;
    }

    /**
     * Enumerates the applets in this context. Only applets that are accessible
     * will be returned. This list always includes the applet itself.
     * 
     * @return the Enumeration -- contains ONLY the applet created with this
     *              AppletRunner
     */
    public final Enumeration getApplets()
    {
        return applets.elements();
    }

    /**
     * Shows a new document. This may be ignored by the applet context (and in
     * our case, it is, but we'll show the user, in the status area, that the
     * document was requested and WOULD be loaded if in a browser).
     * 
     * @param url
     *                  URL to load
     */
    public void showDocument(final URL url)
    {
        Log.debug("AppletContext request to show URL " + url.toString());
        AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                // privileged code goes here, for example:
                mlfcListener.openLocationTop(url.toString());
                return null; // nothing to return
            }
        });
    }

    /**
     * Show a new document in a target window or frame. This may be ignored by
     * the applet context. (Again, it is ignored, but we'll show the request
     * information to the user in the status area.)
     * 
     * This method accepts the target strings: _self show in current frame
     * _parent show in parent frame _top show in top-most frame _blank show in
     * new unnamed top-level window <other>show in new top-level window named
     * <other>
     * 
     * @param url
     *                  URL to load
     * @param target
     *                  the target string
     */
    public void showDocument(final URL url,final  String target)
    {
        
        Log.debug("AppletContext request to show URL " + url.toString()
                + " in target: " + target);
        AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                // privileged code goes here, for example:
                mlfcListener.openLocationTop(new XLink(url.toString()),target);
                return null; // nothing to return
            }
        });
    }

    /**
     * Show a status string in the status area (the Text object at the bottom of
     * the window.
     * 
     * @param text
     *                  the text to display
     */
    public void showStatus(String text)
    {
        Log.debug(text);
        mlfcListener.setStatusText("Applet status: "+text);
    }

    /** ********** URLStreamHandlerFactory methods ************ */

    /**
     * Creates a new URLStreamHandler instance with the specified protocol.
     * 
     * @param protocol
     *                  the protocol to use (ftp, http, nntp, etc.). THIS PROTOCOL IS
     *                  IGNORED BY THIS APPLET CONTEXT
     */
    /*
    public URLStreamHandler createURLStreamHandler(String protocol)
    {
        return new DummyURLStreamHandler();
    }*/

    /*
     * JDK 1.4: Associates the specified stream with the specified key in this
     * applet context.
     */
    public void setStream(String key, InputStream stream) throws IOException
    {
        return;
    }

    /*
     * JDK 1.4: Returns the stream to which specified key is associated within
     * this applet context.
     */
    public InputStream getStream(String key)
    {
        return null;
    }

    /*
     * JDK 1.4: Finds all the keys of the streams in this applet context. XXXX:
     * Can't have Iterator without Java2!! Functions need implementation.
     */
    public Iterator getStreamKeys()
    {
        return null;
    }
}

class AudioClipImpl implements AudioClip
{

    private boolean stop;

    private URL url;

    public AudioClipImpl(URL url)
    {
        this.url = url;
    }

    public void loop()
    {
        stop = false;
        for (; !stop;)
        {
            play();
            try
            {
                Thread.sleep(100);
            } catch (InterruptedException _x)
            {
            }
        }
    }

    public void play()
    {
        Log.debug("Should be playing sound: "+url.getFile());
        //playFile(url.getFile());
    }

    //native static void playFile(String file);

    public void stop()
    {
        stop = true;
    }

}