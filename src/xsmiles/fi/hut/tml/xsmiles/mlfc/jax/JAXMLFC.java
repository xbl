package fi.hut.tml.xsmiles.mlfc.jax;

import java.applet.*;
import java.awt.*;
import javax.swing.*;

import java.io.IOException;
import java.net.*;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.Hashtable;
import java.util.StringTokenizer;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import org.w3c.dom.*;

public class JAXMLFC extends MLFC
{



    private static URL s[];

    //BrowserWindow browser;
    AppletStarter appletStarter;

    private boolean primary = false;

    private ScrollPane sp = null;

    private SecurityManager defaultSM;

    public JAXMLFC()
    {
        s = new URL[1];
    }

    /**
     * Get the version of the MLFC. This version number is updated with the
     * browser version number at compilation time. This version number indicates
     * the browser version this MLFC was compiled with and should be run with.
     * 
     * @return MLFC version number.
     */
    public final String getVersion()
    {
        return Browser.version;
    }

    /**
     * Display primary MLFC
     * 
     * @param doc
     *                  The document to render
     */
    public void start()
    {
        //browser=this.getXMLDocument().getBrowser();
        //if(!applets.eqauls("true"))
        //  return;
        sp = new ScrollPane();
        this.getContainer().setLayout(new BorderLayout());
        this.getContainer().add(sp);
        Panel pa = new Panel();
        sp.add(pa);
        pa.setLayout(new BulletinLayout());
        primary = true;
        displaySecondaryMLFC(this.getXMLDocument(), pa);
    }

    /**
     * Display document in some container
     * 
     * @param doc
     *                  document
     * @param cont
     *                  The container where to render
     */
    public void displaySecondaryMLFC(XMLDocument doc, Container cont)
    {
        String codeBase = null, appletName = null, archive = null;
        int width = 640, height = 480;
            Element docElement = doc.getXMLDocument().getDocumentElement();
            Element appletElement = (Element) docElement.getElementsByTagName(
                    "applet").item(0);
        appletStarter = new AppletStarter();
        appletStarter.displayApplet(doc.getXMLURL(),cont,appletElement,this.getMLFCListener(),this.isPrimary());
    }
    public void stop()
    {
        appletStarter.stop();
        //System.setSecurityManager(defaultSM);
    }
    

}