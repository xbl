/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 17, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.jax;

import java.applet.Applet;
import java.awt.Component;
import java.awt.Container;
import java.net.URL;
import java.util.Hashtable;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;


/**
 * @author honkkis
 *
 */
public class AppletStarter
{
    AppletRunner appletRunner;
    /**
     * Display document in some container
     * 
     * @param doc
     *                  document
     * @param cont
     *                  The container where to render
     */
    public void displayApplet(URL docURL, Container cont, Element appletElement, MLFCListener listener, boolean primary)
    {
        String codeBase = null, appletName = null, archive = null;
        int width = 640, height = 480;
        try
        {
            try
            {
                codeBase = appletElement.getAttribute("codebase");
                appletName = appletElement.getAttribute("code");
                archive = appletElement.getAttribute("archive");
                width = Integer.parseInt(appletElement.getAttribute("width"));
                height = Integer.parseInt(appletElement.getAttribute("height"));
                int pos = appletName.lastIndexOf(".class");
                if (pos > -1)
                {
                    appletName=appletName.substring(0,pos);
                }
            } catch (Exception e)
            {
                Log.error("Problems reading attributes");
            }
            String applets = listener.getProperty("security/applets/enabled");
            if (!applets.equals("true"))
            {
                listener
                        .showErrorDialog(
                                "Applet Security",
                                "Applets disabled, because there is no applet security in X-Smiles. If you still really want to use applets if research or testing purposes, enable them from the configuration file, under Security.");
                return;
            }
            Class ac = null;
            if (codeBase==null||codeBase.equals("")) codeBase=".";
            if (!codeBase.endsWith("/")) codeBase=codeBase+"/";
            URL baseURL = new URL(docURL, codeBase);
            Log.debug("Applet codebase:" + codeBase + "applet class name:"
                    + appletName+" base URL:"+baseURL);
            AppletClassLoader ul = new AppletClassLoader(baseURL,archive);
            ac = ul.loadClass(appletName);
            Applet applet = (Applet) ac.newInstance();
            if (applet != null && codeBase != null)
            {
                URL documentBase = docURL;
                //defaultSM=System.getSecurityManager();
                //System.setSecurityManager(new SimpleSecurityManager());
                appletRunner = new AppletRunner(applet, codeBase, documentBase,
                        width, height,listener);
                Hashtable params = getParams(appletElement);
                appletRunner.setParams(params);
                if (primary)
                {
                    //appletRunner.setSize(browser.getCurrentGUI().getContentPanel().getPreferredSize());
                    //cont.setSize(browser.getCurrentGUI().getContentPanel().getPreferredSize());
                }
                cont.add((Component) appletRunner);
                cont.setVisible(true);
                appletRunner.setVisible(true);
                appletRunner.runApplet();
                Log.debug(appletRunner.getPreferredSize().toString());
                cont.invalidate();
                cont.validate();
                if (primary&&cont.getParent()!=null)
                {
                    cont.getParent().invalidate();
                    cont.getParent().validate();
                    //		  browser.getCurrentGUI().getWindow().doLayout();
                }
            }
        } catch (Exception e)
        {
            Log.error(e);
        }
    }

    private Hashtable getParams(Element e)
    {
        Hashtable params;
        params = new Hashtable();
        NodeList nl = e.getElementsByTagName("param");
        int len = nl.getLength();
        for (int i = 0; i < len; i++)
        {
            params.put((((Element) nl.item(i)).getAttribute("name")),
                    (((Element) nl.item(i)).getAttribute("value")));
        }
        return params;
    }
    public void stop()
    {
        appletRunner.stop();
    }
}
