/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 17, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.jax;

import java.io.FilePermission;
import java.io.IOException;
import java.net.SocketPermission;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.PropertyPermission;
import java.util.StringTokenizer;

import fi.hut.tml.xsmiles.Log;


    /**
     * This classloader, which is based on URLClassLoader is used to load the classes 
     * of the applet in a secure manner
     * 
     * It gives applets minimal permissions in the getPermissions method
     * @author honkkis
     *
     */
class AppletClassLoader extends URLClassLoader
{
    AppletClassLoader(URL codebase, String archive) throws IOException 
    {
        super(new URL[0]);
        if (archive.equals("")) {
            addURL(codebase);
        } else {
            for (StringTokenizer t = new StringTokenizer(archive, ", ");
                    t.hasMoreTokens(); ) {
                addURL(new URL(codebase, t.nextToken()));
            }
        }
    }
    protected  PermissionCollection 	getPermissions(CodeSource codesource)
    //Returns the permissions for the given codesource object.
    {
        PermissionCollection p = new Permissions(); // to make sure applets have no permissions
        URL location = codesource.getLocation();
        Log.debug("Returning null permissions for an applet: "+location);
        // Give applets rights to access the files under it's own directory
        if (location.getProtocol().equals("file"))
        {
            String dir = location.getFile();
            if (!dir.endsWith("/"))
            {
                int pos = dir.lastIndexOf('/');
                dir = dir.substring(0,pos+1);
            }
            p.add(new FilePermission(dir+"-","read"));
            Log.debug("Added permission to read: "+dir);
        }
        // the applets can access sockets to the same host
        else if (location.getProtocol().equals("http") || location.getProtocol().equals("https"))
        {
            String host=location.getHost();
            p.add(new SocketPermission(host,"connect,resolve"));
            Log.debug("Added permission to connect and resolve host: "+host);
        }
        this.addReadPropertyPermission(p,"file.separator");
        this.addReadPropertyPermission(p,"java.class.version");
        this.addReadPropertyPermission(p,"java.vendor");
        this.addReadPropertyPermission(p,"java.vendor.url");
        this.addReadPropertyPermission(p,"java.version");
        this.addReadPropertyPermission(p,"line.separator");
        this.addReadPropertyPermission(p,"os.arch");
        this.addReadPropertyPermission(p,"os.name");
        this.addReadPropertyPermission(p,"path.separator");
        return p;
    }
    private void addReadPropertyPermission(PermissionCollection p,String prop)
    {
        p.add(new PropertyPermission(prop,"read"));
    }
    private void addPropertyPermission(PermissionCollection p,String prop,String action)
    {
        p.add(new PropertyPermission(prop,action));
    }
}
