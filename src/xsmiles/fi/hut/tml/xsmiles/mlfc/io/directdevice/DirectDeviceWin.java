/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io.directdevice;

/**
 * Win32 platform DirectDevice class to read joystick, mouse and keyboard.
 * Uses native methods (DirectInput) to read the values.
 */
public class DirectDeviceWin extends DirectDevice {

	/*
	 * Load win32 dll library.
	 */
    static {
        try {
            System.loadLibrary("DirectDeviceWin");
        }
        catch (UnsatisfiedLinkError e) {
            e.fillInStackTrace();
            throw e;
        }
    }

	// Array of integers to hold joystick values
	private static int joystickValues[] = new int[300];

	// Array of integers to hold key values
	private static int keyboardValues[] = new int[260];

	// Positions in the array
	private static final int JOY_BUTTONS = 0;  // -31
	private static final int JOY_X		= 32;
	private static final int JOY_Y		= 33;
	private static final int JOY_Z		= 34;
	private static final int JOY_RX		= 35;
	private static final int JOY_RY		= 36;
	private static final int JOY_RZ		= 37;

    /**
	 * Get device values in a big array of integers.
	 * @param	array of 300 integers
	 * @return	true if successful
     */
    public native static final boolean getJoystickValues(int joystickValues[]);

    /**
	 * Get device values in a big array of integers.
	 * @param	array of 256 integers, one for each key
	 * @return	true if successful
     */
    public native static final boolean getKeyboardValues(int keyboardValues[]);

    /**
	 * Test
     */
    public native static final int getTest();

    /**
	 * Test
     */
    public native static boolean isTest(int id);

	/**
	 * Sample joystick values in Win32 dependent way - use DirectInput.
	 * This method updates protected x,y,z,rx,ry,rz... values in DirectDevice class.
	 */
	protected void sample() {
		if (getJoystickValues(joystickValues) == true) {
			x = joystickValues[JOY_X];
			y = joystickValues[JOY_Y];
			z = joystickValues[JOY_Z];
			rx = joystickValues[JOY_RX];
			ry = joystickValues[JOY_RY];
			rz = joystickValues[JOY_RZ];

			for (int i = 0 ; i < 32 ; i ++)
				button[i] = (joystickValues[i]==1);
	//		System.out.println("Win32 sample "+x);
		}

		if (getKeyboardValues(keyboardValues) == true) {
			for (int i = 0 ; i < 256 ; i ++)
				key[i] = (keyboardValues[i]==1);
		}

		return;
	}

}
