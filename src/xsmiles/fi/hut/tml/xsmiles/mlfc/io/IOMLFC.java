/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io;

import java.net.URL;
import java.awt.Dimension;
import java.awt.Container;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.XMLDocument;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import java.util.*;

/**
 * IOMLFC provides raw input/output capabilities. 
 * Namespace: http://www.xsmiles.org/2002/inputoutput
 *
 * @author Kari
 */
public class IOMLFC extends MLFC {


  private Vector ioElements;

  /**
   * Constructor.
   */
  public IOMLFC()
  {
    ioElements=new Vector();
  }

    /*
  protected String getLocalname(String tagname)
  {
    int index = tagname.indexOf(':');
    if (index<0) return tagname;
    String localname = tagname.substring(index+1);
    return localname;
  }
    */

  /**
   * Create a DOM element.
   */
  public Element createElementNS(DocumentImpl doc, String ns, String tag)
  {
    Element element = null;
    Log.debug("IO : "+ns+":"+tag);
    
    if (tag == null)
      {
		Log.error("Invalid null tag name!");
		return null;
      }
    String localname = getLocalname(tag);

    if (localname.equals("joystick"))
      {
		Log.debug("**** Creating Joystick element");
		element = new JoystickElementImpl(doc, this, ns, tag);
		ioElements.addElement(element);
      } 
    else if (localname.equals("remoteControl"))
      {
		Log.debug("**** Creating RemoteControl element");
		element = new RemoteElementImpl(doc, this, ns, tag);
		ioElements.addElement(element);
      } 
	  // Printing disable for security reasons
//      else if (localname.equals("print"))
//        {
//      	Log.debug("**** Creating Print element");
//      	element = new PrintElementImpl(doc, this, ns, tag);
//      	ioElements.addElement(element);
//        } 



    // Other tags go back to the XSmilesDocument as null...
    if (element == null)
      Log.debug("IO MLFC didn't understand element, it is propably not implemented yet: "+tag);
    
    return element;
  } 

  public void start() 
  { 
    XMLDocument doc = this.getXMLDocument(); 
  }

  /**
   * Append the given URL to be a full URL.
   * @param partURL		Partial URL, e.g. fanfaari.wav
   * @return  Full URL, e.g. http://www.x-smiles.org/demo/fanfaari.wav
   */
  public URL createURL(String partURL)
  {
    try
      {
		return new URL(this.getXMLDocument().getXMLURL(),partURL);
      } catch (java.net.MalformedURLException e)
	{
	  Log.error(e);
	  return null;
	}
  }

  /**
   * Not implemented method.
   */
  public void setSize(Dimension d) {
  }

  /**
   * Called from the LinkHandler - this method asks the browser to go to this external link.
   * @param url		URL to jump to.
   */
  public void gotoExternalLink(String url)
  {
    Log.debug("...going to "+url);
    URL u = createURL(url);
    getMLFCListener().openLocation(u);
  } 

  /**
   * Display status text in the broswer. Usually shows the link destination.
   */
  public void displayStatusText(String url)
  {
    getMLFCListener().setStatusText(url);
  }

  /**
   * The opposite of init()
   * deactivate is only called for displayable MLFCs
   */
  public void stop() {
    //super.destroy();
    Log.debug("Destroying objects related with IO MLFC");
    Enumeration e = ioElements.elements();
    while(e.hasMoreElements()) 
      ((XSmilesElementImpl)e.nextElement()).destroy();
    ioElements=null;
  }
}

 

