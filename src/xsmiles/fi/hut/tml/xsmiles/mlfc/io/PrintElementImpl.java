/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io;

import java.awt.*;
import java.awt.geom.*;
import java.awt.print.*;
import java.awt.font.*;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;

/**
 * Print Element prints stuff our to a printer. 
 * Has the following attributes: showDialog="true|false" - display the print dialog, if
 * not, then uses system default printer. Dispatches "printed" event if printing was successful.
 *
 * <io:print when="id1.click" showDialog="false" href="test.svg?NOT_WORKING"/>
 *
 * @author Kari
 */
public class PrintElementImpl extends XSmilesElementImpl implements EventListener {

	private final double INCH = 72;
	private final static int POINTS_PER_INCH = 72;

	// IOMLFC
	private IOMLFC ioMLFC = null;
	
	// XSmilesDocumentImpl - to create new elements
	private DocumentImpl ownerDoc = null;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public PrintElementImpl(DocumentImpl owner, IOMLFC io, String namespace, String tag)
	{
		super(owner, namespace, tag);
		ioMLFC = io;
		ownerDoc = owner;
		Log.debug("Print element created!");
	}

	private void dispatch(String type)
	{	 
		// Dispatch the event
		Log.debug("Dispatching InputOutput Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		Log.debug("Print.init");
		
		String when = getAttribute("when");
		handleEventAttr(when);

		super.init();
	}


	/**
	 * Destroy this element.
	 */
	public void destroy()
	{
	}
	
	/**
	 * Parse the event value string.
	 */
	private void handleEventAttr(String attr) {
		Element element;
		String event;

		int dot = attr.indexOf('.');

		// Get the eventbase element - accept any type of element
		if (dot != -1)  {
			element = searchElementWithId(attr.substring(0, dot));
			if (element == null) {
				Log.error("Print: Event element '"+attr.substring(0, dot)+"' not found!");
				throw new NumberFormatException("Print: element '"+
									attr.substring(0, dot)+"' not found!");
			}
			// Get event name after dot
			event = attr.substring(dot+1);
		} else  {
			// Use default element this
			element = this;
			event = attr;
		}

		// Add this as a listener for events in target - use bubble phase to
		// catch the event on the target element itself.
		((EventTarget)element).addEventListener(event, this, false);
	}


	/**
	 * Searches for element with id.
	 * @param	id	id to search for
	 * @return	the element with id, or null if not found.
	 */
	public Element searchElementWithId(String id) {
		return searchElementWithId(getOwnerDocument().getDocumentElement(), id);
	}

	/**
	 * Event received.
	 */
	public void handleEvent(Event evt)
	{
		Log.debug("PRINTPRINTPRINTPRINT!!!!");

       // Create a new PrinterJob object
       PrinterJob printJob = PrinterJob.getPrinterJob ();
 
       // Create a new book to add pages to
       Book book = new Book ();
 
       // Add the cover page using the default page format for this print job
       book.append (new OutputPage (), printJob.defaultPage ());
 
       // Add the document page using a landscape page format
       //   PageFormat documentPageFormat = new PageFormat ();
       //   documentPageFormat.setOrientation (PageFormat.LANDSCAPE);
       //   book.append (new Document (), documentPageFormat);
 
       // Tell the printJob to use the Book as the Pageable object
       printJob.setPageable (book);
 
       // Show the print dialog box. If the user clicks the
       // print button, we then proceed to print, else we cancel
       // the process.
	   
	   if (getAttribute("showDialog").equals("false")) {
		   try {
		      printJob.print();
			   dispatch("printed");
			   Log.debug("printed.");
		   } catch (Exception PrintException) {
		      PrintException.printStackTrace();
		   }	   	   
	   } else {
	       if (printJob.printDialog()) {
	          try {
	             printJob.print();
				 dispatch("printed");
  	             Log.debug("printed.");
	          } catch (Exception PrintException) {
	             PrintException.printStackTrace();
	          }
	       }
	   }

	}
	
    /**
	 * Print the page.
     */
    private class OutputPage implements Printable {
       /**
	    * Print it.
        * @param g Graphics
        * @param pageFormat PageFormat
        * @param page page number
        * @return If ok
        */
       public int print (Graphics g, PageFormat pageFormat, int page) {
 
          // Create the Graphics2D object
          Graphics2D g2d = (Graphics2D) g;
 
          // Translate the origin to 0,0 for the top left corner
          g2d.translate (pageFormat.getImageableX (), pageFormat.getImageableY ());
 
          // Set the default drawing color to black
          g2d.setPaint (Color.black);
 
          // Draw a border around the page
          Rectangle2D.Double border = new Rectangle2D.Double (0,
                                                              0,
                                                              pageFormat.getImageableWidth (),
                                                              pageFormat.getImageableHeight ());
          g2d.draw (border);
 
          // Print the title
          String titleText = "Printing from X-Smiles";
          Font titleFont = new Font ("helvetica", Font.BOLD, 36);
          g2d.setFont (titleFont);
 
          // Compute the horizontal center of the page
          FontMetrics fontMetrics = g2d.getFontMetrics ();
          double titleX = (pageFormat.getImageableWidth () / 2) - (fontMetrics.stringWidth (titleText) / 2);
          double titleY = 3 * POINTS_PER_INCH;
          g2d.drawString (titleText, (int) titleX, (int) titleY);
 
          return (PAGE_EXISTS);
       }
    }
	
	
}

