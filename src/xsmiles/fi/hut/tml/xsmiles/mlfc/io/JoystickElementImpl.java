/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.io.directdevice.DirectDevice;
//import fi.hut.tml.xsmiles.mlfc.io.directdevice.DirectDeviceWin;
import fi.hut.tml.xsmiles.mlfc.io.directdevice.JoystickListener;
import fi.hut.tml.xsmiles.mlfc.io.directdevice.KeyboardListener;

/**
 * Joystick Element sends joystickChanged events to inform 
 * changes in joystick
 *
 * @author Kari
 */
public class JoystickElementImpl extends XSmilesElementImpl implements JoystickListener {

	// IOMLFC
	private IOMLFC ioMLFC = null;
	
	// XSmilesDocumentImpl - to create new elements
	private DocumentImpl ownerDoc = null;

	private DirectDevice joy = null;

	// Previous values
	private boolean left = false, right = false, up = false, down = false;
	private boolean button0 = false, button1 = false, button2 = false, button3 = false;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public JoystickElementImpl(DocumentImpl owner, IOMLFC io, String namespace, String tag)
	{
		super(owner, namespace, tag);
		ioMLFC = io;
		ownerDoc = owner;
		Log.debug("Joy element created!");
	}

	private void dispatch(String type)
	{	 
		// Dispatch the event
		Log.debug("Dispatching InputOutput Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		Log.debug("Joystick.init");

		try  {
			joy = DirectDevice.createInstance();
		} catch (java.lang.UnsatisfiedLinkError e)  {
			Log.error("Native library for Joystick not installed!");
			throw e;
		}
		joy.addJoystickListener(this);

		String rate=getAttribute("sampleRate");
		if(rate != null && rate.equals("")==false) {
			int valrate=Integer.parseInt(rate);
			joy.setSamplingRate(valrate);
		}

		super.init();
	}

	// Should be defined with an attribute
	private static final int limit = 20;

	public void joystickChanged(DirectDevice dev) {
		int x = dev.getX(), y = dev.getY(), z = dev.getZ();
		boolean b0 = dev.getButton(0), b1 = dev.getButton(1);
		boolean b2 = dev.getButton(2), b3 = dev.getButton(3);

		// Check directions
		boolean l = false, r = false, u = false, d = false;
		if (x < -limit)
			l = true;
		if (x > limit)
			r = true;
		if (y < -limit)
			u = true;
		if (y > limit)
			d = true;

		// Send events
		if (l != left)  {
			if (l == true)
				dispatch("joyLeftPressed");
			else	
				dispatch("joyLeftReleased");
			left = l;
		}
		if (u != up)  {
			if (u == true)
				dispatch("joyUpPressed");
			else	
				dispatch("joyUpReleased");
			up = u;
		}
		if (d != down)  {
			if (d == true)
				dispatch("joyDownPressed");
			else	
				dispatch("joyDownReleased");
			down = d;
		}
		if (r != right)  {
			if (r == true)
				dispatch("joyRightPressed");
			else	
				dispatch("joyRightReleased");
			right = r;
		}
		if (button0 != b0)  {
			if (b0 == true)
				dispatch("joyButton0Pressed");
			else	
				dispatch("joyButton0Released");
			button0 = b0;
		}
		if (button1 != b1)  {
			if (b1 == true)
				dispatch("joyButton1Pressed");
			else	
				dispatch("joyButton1Released");
			button1 = b1;
		}
		if (button2 != b2)  {
			if (b2 == true)
				dispatch("joyButton2Pressed");
			else	
				dispatch("joyButton2Released");
			button2 = b2;
		}
		if (button3 != b3)  {
			if (b3 == true)
				dispatch("joyButton3Pressed");
			else	
				dispatch("joyButton3Released");
			button3 = b3;
		}

		//System.out.println("Joy changed "+dev.getX()+" "+dev.getY()+" "+dev.getZ()+" "+dev.getButton(0)+" "+dev.getButton(1));
	}

	/**
	 * Get joystick x-axis value.
	 * @return		Joystick x-axis value [-100, 100]
	 */
	public int getX() {
		return joy.getX();
	}

	/**
	 * Get joystick y-axis value.
	 * @return		Joystick y-axis value [-100, 100]
	 */
	public int getY() {
		return joy.getY();
	}

	/**
	 * Get joystick z-axis value.
	 * @return		Joystick z-axis value [-100, 100]
	 */
	public int getZ() {
		return joy.getZ();
	}

	/**
	 * Get joystick x-axis rotation value.
	 * @return		Joystick x-axis rotation value [-100, 100]
	 */
	public int getRX() {
		return joy.getRX();
	}

	/**
	 * Get joystick y-axis rotation value.
	 * @return		Joystick y-axis rotation value [-100, 100]
	 */
	public int getRY() {
		return joy.getRY();
	}

	/**
	 * Get joystick z-axis rotation value.
	 * @return		Joystick z-axis rotation value [-100, 100]
	 */
	public int getRZ() {
		return joy.getRZ();
	}

	/**
	 * Get joystick button state.
	 * @param num	Button number, 0-31
	 * @return		Joystick button state, true if pressed, false if unpressed
	 */
	public boolean getButton(int num) {
		return joy.getButton(num);
	}


	/**
	 * Destroy this element.
	 */
	public void destroy()
	{
		if (joy != null)  {
			joy.removeJoystickListener(this);
			joy = null;
		}
	}
}


