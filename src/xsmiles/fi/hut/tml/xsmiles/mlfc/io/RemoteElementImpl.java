/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.NavigationState;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.Enumeration;


/**
 * Fake remote control.
 * Maybe, we could add a possibility for a real one as well.
 *
 * @author Juha
 */
public class RemoteElementImpl extends XSmilesElementImpl implements ActionListener {

  // XSmilesDocumentImpl - to create new elements
  private DocumentImpl ownerDoc = null;
  private JFrame emuWindow;
  private JButton red, green, yellow, blue, right, left, up, down;
  
  /**
   * Constructor - Set the owner, name and namespace.
   */
  public RemoteElementImpl(DocumentImpl owner, MLFC mlfc, String namespace, String tag) {
    super(owner, namespace, tag);
    ownerDoc = owner;
    Log.debug("RemoteControl element created!");
  }

  private void dispatch(String type) {	 
    // Dispatch the event
    Log.debug("Dispatching GUI Event");
    org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
    evt.initEvent(type, true, true);
    ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    return;
  }

  /**
   * Initialize this element.
   * This requires that the DOM tree is available.
   */
  public void init() {
    //Log.debug("GUI.init");
    String emulation=this.getAttribute("emulation");
    if(emulation!=null && emulation.equals("true")) 
      createWindow();
  }
  
  /**
   * Destroy this element.
   */
  public void destroy() {
    Log.debug("Destroying window?");
    destroyWindow();
  }

  private void createWindow() 
  {
    emuWindow=new JFrame("FakeRemote");
    emuWindow.setSize(344,218);
    right=new JButton("right");
    left=new JButton("left");
    up=new JButton("up");
    down=new JButton("down");
    red=new JButton("red");
    green=new JButton("green");
    yellow=new JButton("yellow");
    blue=new JButton("blue");
    red.addActionListener(this);
    green.addActionListener(this);
    yellow.addActionListener(this);
    blue.addActionListener(this);
    right.addActionListener(this);
    left.addActionListener(this);
    up.addActionListener(this);
    down.addActionListener(this);
    Container contentPane=emuWindow.getContentPane();
    contentPane.setLayout(new FlowLayout());
    contentPane.add(left);
    contentPane.add(right);
    contentPane.add(up);
    contentPane.add(down);
    contentPane.add(red);
    contentPane.add(green);
    contentPane.add(yellow);
    contentPane.add(blue);
    emuWindow.show();
  }
  
  public void actionPerformed(ActionEvent e) 
  {
    String cmd=e.getActionCommand();
    dispatch(cmd);
  }
  
  private void destroyWindow() 
  {
    red.removeActionListener(this);
    green.removeActionListener(this);
    yellow.removeActionListener(this);
    blue.removeActionListener(this);
    right.removeActionListener(this);
    left.removeActionListener(this);
    up.removeActionListener(this);
    down.removeActionListener(this);

    if(emuWindow!=null)
      emuWindow.dispose();
  }
  
}

