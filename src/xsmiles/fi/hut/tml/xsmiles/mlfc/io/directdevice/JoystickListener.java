/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io.directdevice;

/**
 * Joystick listener to listen to joystick changes.
 */
public interface JoystickListener {
	/**
	 * Called when joystick axises or buttons are changed.
	 * @param joystick		Reference to the joystick object, to retrieve the values
	 */
	public void joystickChanged(DirectDevice joystick);
}