/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io.directdevice;

/**
 * Keyboard listener to listen to keyboard changes.
 */
public interface KeyboardListener {
	/**
	 * Called when keyboard keys change state.
	 * @param keyboard		Reference to the keyboard object, to retrieve the values
	 */
	public void keyboardChanged(DirectDevice keyboard);
}