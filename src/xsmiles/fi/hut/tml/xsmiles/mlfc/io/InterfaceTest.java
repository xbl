/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import fi.hut.tml.xsmiles.mlfc.io.directdevice.DirectDevice;
import fi.hut.tml.xsmiles.mlfc.io.directdevice.DirectDeviceWin;
import fi.hut.tml.xsmiles.mlfc.io.directdevice.JoystickListener;
import fi.hut.tml.xsmiles.mlfc.io.directdevice.KeyboardListener;

// Test the DirectDevice interface

public class InterfaceTest implements JoystickListener, KeyboardListener {

    public InterfaceTest() {
    }

    public static void main(String args[]) {
		System.out.println("INTERFACE TEST");
		InterfaceTest intt = new InterfaceTest();


		DirectDevice joy = DirectDevice.createInstance();
		joy.setSamplingRate(50);
		joy.addJoystickListener(intt);
		joy.addKeyboardListener(intt);

		for (int i = 0; i<10; i++) {
			System.out.println("WIN: "+i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

		}

		joy.removeJoystickListener(intt);
 		System.out.println("DONE.");
    }

	public void joystickChanged(DirectDevice dev) {
		System.out.println("Joy changed "+dev.getX()+" "+dev.getY()+" "+dev.getZ()+" "+dev.getButton(0)+" "+dev.getButton(1));
	}

	public void keyboardChanged(DirectDevice dev) {
		System.out.println("Joy changed "+dev.getKeyboard(1));
	}

}
