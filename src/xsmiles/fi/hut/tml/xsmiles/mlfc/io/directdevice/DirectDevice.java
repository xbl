/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io.directdevice;

import java.util.Vector;
import java.util.Enumeration;

/**
 */
public class DirectDevice implements Runnable {

	// Interval between samples, in millisecs
	private int interval = 200;

	// Sampled values - updated in platform dependent subclasses
	protected int x=0, y=0, z=0, rx=0, ry=0, rz=0;
	protected boolean button[];

	// Old Sampled values - compared to new values to notice device changes
	protected int oldx=0, oldy=0, oldz=0, oldrx=0, oldry=0, oldrz=0;
	protected boolean oldbutton[];

	// Sampled key values - updated in platform dependent subclasses
	protected boolean key[];

	// Old sampled key values
	protected boolean oldkey[];

	// Listeners
	private Vector joystickListeners = null;
	private Vector keyboardListeners = null;

	// Total number of listeners
	private int listeners = 0;

	// A way to stop the thread
	private boolean threadStop = true;

	// Singleton of this object (or device depentent subclass)
	private static DirectDevice device = null;

	// Thread sampling devices
	private static Thread thread = null;

	/**
	 * Private constructor - this class should be instantiated with a call to
	 * createInstance().
	 */
	protected DirectDevice()  {
		// Init button values
		button = new boolean[32];
		for (int i = 0 ; i<32 ; i++)
			button[i] = false;

		// Init button values
		oldbutton = new boolean[32];
		for (int i = 0 ; i<32 ; i++)
			oldbutton[i] = false;

		key = new boolean[256];
		oldkey = new boolean[256];

		// Init listener vectors
		joystickListeners = new Vector();
		keyboardListeners = new Vector();
	}

	/**
	 * Create a instance of the DirectDevice class - 
	 * platform dependent class is returned.
	 * (DirectDeviceWin or DirectDeviceLinux...)
	 */
	public static DirectDevice createInstance() {
		if (device == null) {
			device = new DirectDeviceWin();
			thread = new Thread(device);
		}
		return device;
	}

	/**
	 * Get joystick state sampling rate. The joystick is sampled all the time
	 * by a thread. This method returns the interval between samples.
	 * @return		Sampling rate, in millisecs.
	 */
	public int getSamplingRate() {
		return interval;
	}

	/**
	 * Set joystick state sampling rate. The joystick is sampled all the time
	 * by a thread. This method sets the interval between samples.
	 * @param rate		Sampling rate, in millisecs.
	 */
	public void setSamplingRate(int rate) {
		if (rate < 0)
			return;

		interval = rate;
	}

	/**
	 * Sampling method - this class has a dummy implementation,
	 * but platform dependent subclasses are expected to override
	 * this and implement their own joystick sampling routine.
	 */
	 protected void sample() {
		System.out.println("sampling... ups, not working.");
		return;
	 }

	/**
	 * Checks if any of the joystick values changed.
	 * @return			true if changed
	 */
	 private boolean hasJoystickChanged() {
		if (oldx != x ||
			oldy != y ||
			oldz != z ||
			oldrx != rx ||
			oldry != ry ||
			oldrz != rz)
			return true;

		for (int i = 0 ; i < 32 ; i++)
			if (oldbutton[i] != button[i])
				return true;

		return false;
	 }

	/**
	 * Checks if any of the key values changed.
	 * @return			true if changed
	 */
	 private boolean hasKeyboardChanged() {
		for (int i = 0 ; i < 256 ; i++)
			if (oldkey[i] != key[i])
				return true;

		return false;
	 }

	/**
	 * Saves new joy and key values to old values.
	 */
	 private void saveDeviceValues() {
		// Joystick
		oldx = x;
		oldy = y;
		oldz = z;
		oldrx = rx;
		oldry = ry;
		oldrz = rz;
		for (int i = 0 ; i < 32 ; i++)
			oldbutton[i] = button[i];

		for (int i = 0 ; i < 256 ; i++)
			oldkey[i] = key[i];
	}

	/**
	 * This is a thread sampling joystick every <i>interval</i> ms.
	 * Runs all the time.
	 */
	public void run() {
		while (threadStop == false) {
			try {
				Thread.sleep(interval);

				// Sample new values
				sample();

				// Did joystick change?
				if (hasJoystickChanged() == true)
					notifyJoystickListeners();

				// Did key change?
				if (hasKeyboardChanged() == true)
					notifyKeyboardListeners();

				// Save values for comparison
				saveDeviceValues();
			} catch(InterruptedException e) {
				System.out.println("ERROR: Joystick Thread Interrupted - joystick no longer functional.");
			}
		}
	}

	////// Listener methods //////////
	/**
	 * Add a joystick listener to receive joystick chages,
	 * This will also start the sampling thread, if not running.
	 * @param listener		Joystick Listener
	 */
	public void addJoystickListener(JoystickListener listener) {
		joystickListeners.add(listener);
		listeners++;
		if (listeners == 1) {
			threadStop = false;
			thread.start();
		}
	}

	/**
	 * Remove a joystick listener. Also stops the sampling thread, if no more listeners.
	 * @param listener		Joystick Listener
	 */
	public void removeJoystickListener(JoystickListener listener) {
		joystickListeners.remove(listener);
		listeners--;
		if (listeners == 0)
			threadStop = true;
	}

	/**
	 * Remove a joystick listener,
	 * @param listener		Joystick Listener
	 */
	public void notifyJoystickListeners() {
		for (Enumeration e = joystickListeners.elements() ; e.hasMoreElements() ;) {
			((JoystickListener)e.nextElement()).joystickChanged(this);
		}
	}

	/**
	 * Add a keyboard listener to receive key chages,
	 * This will also start the sampling thread, if not running.
	 * @param listener		Keyboard Listener
	 */
	public void addKeyboardListener(KeyboardListener listener) {
		keyboardListeners.add(listener);
		listeners++;
		if (listeners == 1) {
			threadStop = false;
			thread.start();
		}
	}

	/**
	 * Remove a keyboard listener. Also stops the sampling thread, if no more listeners.
	 * @param listener		Keyboard Listener
	 */
	public void removeKeyboardListener(KeyboardListener listener) {
		keyboardListeners.remove(listener);
		listeners--;
		if (listeners == 0)
			threadStop = true;
	}

	/**
	 * Remove a keyboard listener,
	 * @param listener		Keyboard Listener
	 */
	public void notifyKeyboardListeners() {
		for (Enumeration e = keyboardListeners.elements() ; e.hasMoreElements() ;) {
			((KeyboardListener)e.nextElement()).keyboardChanged(this);
		}
	}

	///////////////////// Boring get methods ///////////////////////////////////

	/**
	 * Get joystick x-axis value.
	 * @return		Joystick x-axis value [-100, 100]
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get joystick y-axis value.
	 * @return		Joystick y-axis value [-100, 100]
	 */
	public int getY() {
		return y;
	}

	/**
	 * Get joystick z-axis value.
	 * @return		Joystick z-axis value [-100, 100]
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Get joystick x-axis rotation value.
	 * @return		Joystick x-axis rotation value [-100, 100]
	 */
	public int getRX() {
		return rx;
	}

	/**
	 * Get joystick y-axis rotation value.
	 * @return		Joystick y-axis rotation value [-100, 100]
	 */
	public int getRY() {
		return ry;
	}

	/**
	 * Get joystick z-axis rotation value.
	 * @return		Joystick z-axis rotation value [-100, 100]
	 */
	public int getRZ() {
		return rz;
	}

	/**
	 * Get joystick button state.
	 * @param num	Button number, 0-31
	 * @return		Joystick button state, true if pressed, false if unpressed
	 */
	public boolean getButton(int num) {
		if (num < 0 || num > 31)
			return false;

		return button[num];
	}

	/**
	 * Get keyboard key state.
	 * @param num	Key number (pplatform dependent!)
	 * @return		Key state, true if pressed, false if unpressed
	 */
	public boolean getKeyboard(int num) {
		if (num < 0 || num > 256)
			return false;

		return key[num];
	}

}
