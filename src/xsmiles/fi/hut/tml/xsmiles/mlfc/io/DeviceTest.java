/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.io;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

//import fi.hut.tml.xsmiles.mlfc.io.directdevice.DirectDevice;
import fi.hut.tml.xsmiles.mlfc.io.directdevice.DirectDeviceWin;

public class DeviceTest implements Runnable {

    public DeviceTest() {
    }

	public void run() {
		DirectDeviceWin win = new DirectDeviceWin();
		for (int i = 0; i<10; i++) {
			System.out.println("***WIN: "+win.getTest()+" b: "+win.isTest(-12));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

		}
 		System.out.println("***WIN: "+win.getTest()+" b: "+win.isTest(12));
 		System.out.println("***DONE.");
	}

    public static void main(String args[]) {
		Thread t = new Thread(new DeviceTest());
		t.start();

		DirectDeviceWin win = new DirectDeviceWin();
		for (int i = 0; i<10; i++) {
			System.out.println("WIN: "+win.getTest()+" b: "+win.isTest(-12));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

		}
 		System.out.println("WIN: "+win.getTest()+" b: "+win.isTest(12));
 		System.out.println("DONE.");
    }
}
