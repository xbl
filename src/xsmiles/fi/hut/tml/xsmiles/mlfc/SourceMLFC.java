/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc;



import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XsmilesView;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.awt.XWrappingLabel;
import fi.hut.tml.xsmiles.util.StringUtils;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.*;

import javax.swing.JComponent;
import javax.swing.KeyStroke;


/**
 * Class displays the XML document source to the MLFC area in the GUI. The
 * format is just plain text.
 *
 * Note: This can also be used in some lightweight popup Frame or smth. 
 *
 * @author	Aki Teppo
 * @author	Mikko Honkala
 * @version	$Revision: 1.36 $
 */

public class SourceMLFC extends MLFC
{    
  private Container contentPanel; // Browser contentPanel
  private Vector mlfcListeners;

  protected XWrappingLabel wrappingLabel = null;



  public void start()
  {
      this.display(this.getXMLDocument(),XsmilesView.SOURCEMLFC,this.getContainer());
  }
  public void stop()
  {
    this.getContainer().removeAll();
  }

  /**
     * Displays document in the MLFC area of the GUI window.
     *
     * @param doc the XML document to be displayed.
     *
     */
  public void display(XMLDocument doc,int mode, Container c )

  {
    contentPanel=c;
    String text=null;
    c.setBackground(java.awt.Color.white);
    org.w3c.dom.Document xsldoc = doc.getXSLDocument();				  
    if (mode==XsmilesView.SOURCEMLFC) {  
      text = doc.getSourceText(doc.getXMLDocument(),true);
    } else if (mode==XsmilesView.XSL_MLFC) {
      try { 
	if (xsldoc==null) 
	  {
	    text="XSL source not available or nonexistent";
	    Log.error(text);
	  }
	else text = doc.getSourceText(doc.getXSLDocument(),false);
      }
      catch (Exception e) {
	text="XSL source not available or nonexistent";
				
      }
            
    } else {
      // THe default : show the transformed XML source
      boolean prettyPrint = false;
      if (xsldoc!=null) prettyPrint = true;
      text = doc.getSourceText(doc.getDocument(),prettyPrint);
    }
    // Replace tabs with few spaces 
    text = StringUtils.replace(text,"\t","   ");
		
    // Create a new text area
    XTextArea t = this.getMLFCListener().getComponentFactory().getXTextArea(text);
    //JTextArea t = new SmoothTextArea(text);
    t.setEditable(false);
    //t.setBorder(new EmptyBorder(0,0,0,0));
    t.setCaretPosition(0);

    // Create a scrollpane for the text area
    //JScrollPane p = new JScrollPane(t);
    Component p = t.getComponent();
    p.setVisible(true);
    c.add(p);
    p.invalidate();
    c.validate();
  }
  
}
