/* X-Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * look for full legal shaisse in licenses/LICENSE_XSMILES
 */
package fi.hut.tml.xsmiles.mlfc;

import java.awt.Container;
import java.net.URL;
import java.util.Hashtable;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.content.ResourceFetcher;
import fi.hut.tml.xsmiles.content.ResourceReferencer;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
//import fi.hut.tml.xsmiles.xml.dom.DocumentImpl;


/**
 * ALL MLFCs derive this abstract base class. Class provides default functionality 
 * for generic MLFC functionalities.
 *
 * The MLFCs can easily be used as beans. They are controlled with MLFCController.
 * The MLFCListener interface defines services, that are needed by MLFCs.
 *
 * Use the MLFC interface, if your MLFC is not tightly coupled to the browser.
 * If you need to use the BrowserWindow class in your MLFC, you should use the 
 * extend the CoreMLFC class. Only use the BrowserWindow, if you do not intend to
 * reuse the MLFC in some other project. 
 *
 * @see CoreMLFC
 * @author	Aki Teppo, Juha
 * @version	$Revision: 1.44 $
 */
public abstract class MLFC implements ResourceFetcher
{
  private MLFCListener    mlfcListener;
  private XMLDocument document;
  private Container container;
  private boolean host = false;
  private Hashtable params=null;
  protected boolean controlsShown = true;
  protected XSmilesContentHandler contentHandler;
  protected String title;

  /* ------ THESE METHODS ARE IMPLEMENTED BY THE REAL MLFC ----- */

  /**
   * Initialize the MLFC. 
   * This function is called just before the elements are initialized.
   * There is no pair to this method.
   */
  public void init()
  {
  }
  /**
   * Start the MLFC. 
   * This function is the pair to stop().
   */
  public abstract void start();
 
  
  /**
   * Pauses or unpauses the MLFC. 
   * Implementated if needed
   */
  public void pause() {      
  }
  
  /**
   * Stop the MLFC. 
   * This function is the pair to start().
   */
  public abstract void stop();

  /* -------- The following are optional methods ------ /*
  /**
   * Create a DOM element.
   */
   public Element createElementNS(DocumentImpl doc, String URI,String tagname)
   {
   		return new XSmilesElementImpl(doc,URI,tagname);
   }
   /**
    * Create a DOM attribute.
    */
    public Attr createAttributeNS(DocumentImpl doc,String namespaceURI, String qualifiedName)
        throws DOMException
    {
		return null;
//		Log.debug("******* createAttribute "+qualifiedName);
//    	if (errorChecking && !isXMLName(qualifiedName)) {
//            throw new DOMException(DOMException.INVALID_CHARACTER_ERR,
//                                   "DOM002 Illegal character");
//        }
//        return new InstanceAttrNSImpl(this, namespaceURI, qualifiedName);
    }

   /**
    * @return The mlfcController object for this MLFC. Null returned, if no 
    *         mlfcController defined in MLFC.
    * @see MLFCController
    */
   public MLFCController getMLFCController() {
     return null;
   }

   /**
    * Get the version of the MLFC. This should be overridden in the real MLFC with the
	* real version number. Version number is the Browser version number this MLFC was compiled with
	* and can be run with.
	* See the existing MLFCs how to implement getVersion() so that it automatically updates
	* the version number.
    * @return 	MLFC version number.
    */
   public String getVersion() {
   	return "unknown";
   }

  /* ----------------- END OF REAL MLFC METHODS -----------------------*/


  /**
   * Displays document in the MLFC area of the GUI window.
   *
   * @param doc the XML document to be displayed.
   * @param c   The main container of the browser window
   */
  public final void initMLFC(XMLDocument a_doc, Container cont) {
  	document=a_doc;
	container=cont;
	this.init();
  }

  /**
   * Displays document in the MLFC area of the GUI window.
   *
   * @param doc the XML document to be displayed.
   * @param c   The main container of the browser window
   */
  public final void startMLFC(XMLDocument a_doc, Container cont) {
  	document=a_doc;
	container=cont;
	this.start();
  }


  /**
   * Displays document in the MLFC area of the GUI window.
   *
   * @param doc the XML document to be displayed.
   * @param c   The main container of the browser window
   */
  public final void stopMLFC()
  {
  	document=null;
  	container=null;	
  	this.stop();
  }

  
  /**
   * All traffic to the browser, such as openLocation, etc goes through this listener.
   * If no listener supplied MLFCs should still function with some basic level. 
   * 
   * @param l The MLFCListener supplied by the browser
   * @see MLFCListener
   */
  public final void setMLFCListener(MLFCListener l) {
    mlfcListener=l;
  }
  
  public final void setContentHandler(XSmilesContentHandler h)
  {
      this.contentHandler=h;
  }
  
  public final XSmilesContentHandler getContentHandler()
  {
      return this.contentHandler;
  }
  public final ResourceReferencer getResourceReferencer()
  {
      return this.contentHandler.getResourceReferencer();
  }
  
  /**
   * All traffic to the browser, such as openLocation, etc goes through this listener.
   * If no listener supplied MLFCs should still function with some basic level. 
   *
   * @return The MLFCListener stored in the MLFC 
   */
  public final MLFCListener getMLFCListener() {
    return mlfcListener;
  }

	/**
	 * Retrieve a resource via an URL using authenticated HTTP get and store it
	 * as a resource under this contenthandler.
	 * The XML signature process uses that resource list as the list of resources
	 * that are referenced by the document.
	 * Use type from the class content.Resource
	 */
  public final XSmilesConnection get(URL dest, short type) throws Exception
  {
      return this.getContentHandler().get(dest,type); 
  }
  
  
  
  
  public final XMLDocument getXMLDocument()
  {
  	return document;
  }
  public final void setXMLDocument(XMLDocument doc)
  {
  	this.document = doc;
  }
  public final boolean isPrimary()
  {
  	return this.contentHandler.getPrimary();
  }

  public final void setHost(boolean flag) {
  	host = flag;
  }
  
  public final boolean isHost() {
  	return host;
  }
  
  public final Container getContainer()
  {
  	return container;
  }

  public final void setContainer(Container c)
  {
  	container = c;
  }
    
  /**
   * Informs the container that there is going to be a lot of
   * concurrent updates to the document and the container
   * shouldn't repaint itself until commit is called. By default this
   * method does nothing, subclasses should implement it when needed. 
   */
  public void startUpdateTransaction() {      
  }
  
  /**
   * Informs the container that it is free to repaint itself, and should do
   * it right now. By default this method does nothing, subclasses should implement it when needed.
   */
  public void commitUpdateTransaction() {      
  }
  
  /**
   * Informs the container that it is free to repaint itself, but it shouldn't do it now. 
   * By default this method does nothing, subclasses should implement it when needed. 
   */
  public void rollbackUpdateTransaction() {      
  }

	/**
	 * Get local name of a node.
	 * @param tagname		Tag name, including namspace prefix
	 * @return				Tag name without namespace prefix
	 */
  public String getLocalname(String tagname)
  {
    int index = tagname.indexOf(':');
    if (index<0) return tagname;
    String localname = tagname.substring(index+1);
    return localname;
  }

    /**
     * parameters are set BEFORE initialization of the elements 
     *
     */
    public Hashtable getMLFCParameters() 
    {
	return params;
    }
    
    /**
     * parameters are set BEFORE initialization of the elements 
     * 
     * @return The parameters that might be passed to the MLFC, e.g., in the case of the
     *         experimental <?xml-edit ?> processing instruction
     */
    public void setMLFCParameters(Hashtable p) 
    {
	params=p;
    }
    
    /**
     * notify that the zoom level has changed. Note that some content may
     * choose to not implement zooming.
     */
    public void setZoom(double zoom)
    {
        Log.debug("MLFC.setZoom: "+zoom);
    }
    
    /**
     * @return
     */
    public String getTitle()
    {
        // TODO Auto-generated method stub
        return this.title;
    }    
    public void setTitle(String t)
    {
        this.title=t;
        if (this.isPrimary())
        {
            this.mlfcListener.setTitle(t);
        }

    }    
    

    
}
