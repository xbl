/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Aug 4, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.dom.PseudoClassController;
import fi.hut.tml.xsmiles.dom.VisualElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ElementWithContext;


/**
 * This class represents an interface to the message element.
 * It is used by the CSSMessageImpl to show messages to the user.
 * @author honkkis
 *
 */
public interface MessageElement extends Element, PseudoClassController, VisualElement, ElementWithContext
{

    /**
     * @return
     */
    public String getLevel();
    
    public String getMessageAsString(); // Note that this works only with read-only messages...
    
    public String getMessageTypeAsString(); //e.g. "alert"

}
