/*
 * Created on Feb 3, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.ui;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.RepeatItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
/**
 * @author honkkis
 *
 * The pseudo-element ::repeat-item
 */
public class RepeatItemPseudoElement extends VisualElementImpl implements PseudoElement, RepeatItem
{
    protected Element parent;
    protected boolean cursorOn=false;
    protected boolean onceSet=false;
    public RepeatItemPseudoElement(Element parentElement)
    {
        super((org.apache.xerces.dom.DocumentImpl) parentElement.getOwnerDocument(), "xforms", "repeat-item");
        parent = parentElement;
    }
    
    public void setCursorOn(boolean value)
    {
    	if (this.cursorOn!=value || !onceSet) 
    	{
			this.cursorOn=value;
			this.styleChanged();
			onceSet=true;
    	}
    }
    
    public String getLocalName()
    {
        return this.getPseudoElementName();
        
    }
    
    public String getNodeName()
    {
        return this.getPseudoElementName();
    }

    public String getPseudoElementName() // return e.g. "value" for ::value
    {
        if (!cursorOn) 
        	return "repeat-item";
        else return "repeat-index";
    }
    public void setParentNode(Node n)
    {
        parent=(Element)n;
    }
    public Node getParentNode()
    {
        return parent;
    }
    
    public int getIndex()
    {
        XSmilesElementImpl parent = (XSmilesElementImpl)this.getParentNode();
        if (parent==null) return 0;
        NodeList children = parent.getChildNodes(true);
        int index=0;
        for (int i=0;i<children.getLength();i++)
        {
            if (children.item(i) instanceof RepeatItem)
            {
                index++;
                if (children.item(i)==this) return index;
            }
        }
        return 0;
    }
    /*
    		public void initStyle(XSmilesStyleSheet styleSheet)
    		{
    			super.initStyle(styleSheet);
    			//Log.debug(this+" style:"+styleSheet);
    		}
       */

/*    public CSSStyleDeclaration getStyle()
    {
    	return getOwnerStyle();
    }*/

}
