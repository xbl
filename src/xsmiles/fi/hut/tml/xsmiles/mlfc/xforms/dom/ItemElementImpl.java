/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectionItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;



/**
 * Item element
 * This is extended from DynBoundElement to get real context to child elements, since
 * itemset creates them outside the DOM. ref is never executed here.
 * @author Mikko Honkala
 */

public class ItemElementImpl extends DynBoundElementImpl implements SelectionItem
{
    
    protected XFormsControl control;
    protected int index;
    protected boolean childrenSearched = false;
    
    protected ValueElementImpl valueElement;
    protected CaptionElementImpl captionElement;
    /** the possible copy child */
    protected CopyElementImpl copyElement;
    
    /** was this item created by an itemset element */
    protected boolean createdByItemset = false;
    
    public ItemElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        this.bindingAttributesRequired=false;
    }
    
    /**
     * set whether this item was created by an itemset
     */
    protected void setCreatedByItemset(boolean val)
    {
        this.createdByItemset=val;
    }
    
    /**
     * @return was this item was created by an itemset
     */
    protected boolean getCreatedByItemset()
    {
        return this.createdByItemset;
    }
        /*
        public void reinit()
        {
            this.renewBinding();
            super.init();
        }*/
    
    public void createBinding() throws XFormsBindingException
    {
        //Log.debug("item createBinding doing nothing");
    }
    
    protected void findFirstValueAndCaption()
    {
        // go through children, mark first caption and first value
        Node child = this.getFirstChild();
        while (child!=null)
        {
            if (child instanceof ValueElementImpl&&valueElement==null)
            {
                valueElement=(ValueElementImpl)child;
            }
            if (child instanceof CaptionElementImpl && captionElement==null)
            {
                captionElement=(CaptionElementImpl)child;
            }
            if (child instanceof CopyElementImpl && copyElement==null)
            {
                copyElement=(CopyElementImpl)child;
            }
            child=child.getNextSibling();
        }
        this.childrenSearched=true;
    }
    
    public String getValue()
    {
        if (!childrenSearched) this.findFirstValueAndCaption();
        if (valueElement==null) return "";
        else return valueElement.getValue();
    }
    public String getCaption()
    {
        if (!childrenSearched) this.findFirstValueAndCaption();
        if (captionElement==null) return getValue();
        else
        {
            //Log.debug("Item: value:"+getValue()+" caption:"+captionElement.getCaptionText());
            return captionElement.getCaptionText();
        }
    }
    
    public CopyElementImpl getCopyElement()
    {
        return this.copyElement;
    }
    public XFormsControl getControl()
    {
        return control;
    }
    public void setControl(XFormsControl cont)
    {
        control=cont;
    }/*
        public JComponent getComponent()
        {
                return component;
        }
        public void setComponent(JComponent comp)
        {
                component=comp;
        }*/
    public int getIndex()
    {
        return index;
    }
    public void setIndex(int ind)
    {
        this.index=ind;
    }
    
    /** overrides the method from boundelementimpl, this is for itemset implementation */
    public InstanceNode getContextForDescendant()
    {
        return this.contextNode;
    }
    /**
     * This method is called only when the user makes interactive change to the control,
     * not when instance data changes by script or setValue or calculate
     */
    public void userSelected()
    {
        //		this.dispatch("DOMActivate");
        // DISPATCH XForms EVENT
        dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.SELECT_EVENT));
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectionItem#getLabel()
     */
    public String getLabel()
    {
        // TODO Auto-generated method stub
        return this.getCaption();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectionItem#select()
     */
    public void select()
    {
        control.setRefNodeValue(this.getValue(),false);
    }
}
