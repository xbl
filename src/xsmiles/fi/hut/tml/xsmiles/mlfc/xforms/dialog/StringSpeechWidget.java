/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 25, 2004
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.StringTokenizer;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TextAreaElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;

/**
 * @author mpohja
 */
public class StringSpeechWidget extends BaseSpeechWidget
{

    /**
     * @param e
     * @param handler
     */
    public StringSpeechWidget(Element e, FocusHandler handler)
    {
        super(e, handler);
        // TODO Auto-generated constructor stub
    }

    public String generateDialogQuestion()
    {
        return this.getLabel()+":"+ this.getCurrentValue();
    }

    public void generateGrammar(Grammar grammar)
    {
        GrammarGenerator.generateStringGrammar(grammar);
    }

    public boolean interpretResponse(Response r)
    {
        String response = r.response.trim();

        String respo = parseRespond(response);
        Log.debug("Response: " + respo);
        this.focusHandler.speak(this.getLabel() + ": " + respo);
        if (this.element instanceof TextAreaElement)
        {
            ((TextAreaElement)this.element).setCurrentTextValue(respo);
        }
        else
        {
            Data data = ((TypedElement) this.element).getData();
            data.setValueFromDisplay(respo);
            ((TypedElement) this.element).setData(data);
        }
        this.moveFocusToParent();
        return true;
    }

    private String parseRespond(String response)
    {
        StringTokenizer tokens = new StringTokenizer(response);
        String string = "", token;

        while (tokens.hasMoreElements())
        {
            token = tokens.nextToken();
            if (token.equals("space"))
                string += " ";
            else if (token.equals("underscore"))
                string += "_";
            else if (token.equals("dot"))
                string += ".";
            else if (token.equals("at"))
                string += "@";
            else
                string += token;
        }
        return string;
    }
}
