/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsBindingException;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;

/**
 * The vertex for the calculation algorithm
 * @author Mikko Honkala
 */


public class DependencyGraph
{
    
    
    Vector vertices; // should be an array
    //	Hashtable mappings; // this vector is never cleared (it is used to find a change node)
    
    
    public DependencyGraph()
    {
        //		mappings = new Hashtable();
        vertices= new Vector();
    }
    
    
    /**
     * recalculates this graph. Uses Vertex.recalculate to calculate
     * a single node.
     */
    public void recalculate()
    {
        // remove non zero vertices
        this.removeNonZeroVertices();
        //Log.debug("Cloned graph, with only zero vertices. Calculating.");
        //this.printGraph();
        // is Z empty
        while (this.vertices.size()>0)
        {
            // remove a vertex v from Z
            Vertex v = (Vertex)this.vertices.firstElement();
            //v.print(0);
            this.removeVertex(v);
            // is v.node computed : yes -> recalculcate v.node
            v.compute();
            // if the value of v.node changes, then add v to Ld ??
            // For each node w in v.depList a+b:
            Enumeration e = v.depList.elements();
            while (e.hasMoreElements())
            {
                Vertex w = (Vertex)e.nextElement();
                // a) decrement w.inDegree
                w.inDegree--;
                // b) if w.indegree ==0 add w to Z
                if (w.inDegree==0)
                {
                    this.addVertex(w);
                }
            }
        }
        
        
    }
    
    /*
    public static void  getXPathRefNodes(XPathLookup lookup, XPathExpr xpath, Node contextNode, Node namespaceNode, ModelContext modelContext,LookupResult result) throws TransformerException
    {
        // add listener
        // execute XPath
        Vector xpathRefNodes=null;
        lookup.setModelContext(modelContext);
        lookup.evalWithTrace(contextNode,xpath.getExpression(),namespaceNode,result);
        //Log.debug("Xpath: "+xpath.getExpression()+" referenced "+result.referredNodes.size()+" nodes");
        // return list of ref nodes
        //return xpathRefNodes;
    }*/
    
    /**
     * adds a new vertex to the graph.
     * If the vertex v already exists in graph:
     *		- if v.bind == null, then update v.bind = bind
     * This is called by MainDependencyGraph.addBind()
     */
    public Vertex addVertex(InstanceNode n,ExpressionContainer container, XPathExpr xpath, short property) throws XFormsBindingException
    {
        Vertex v = this.getVertex(n,property);
        if (v!=null)
        {
            v.wasAlreadyInGraph=true;
            if (v.container==null) v.container=container;
            if (v.xpath==null) v.xpath=xpath;
            // TODO: there's a problem with calculate vertexes, since for pure references,
            // also calculate vertexes are created... Currently only the last calculate 
            // vertex is used
            else if (property!=Vertex.CALCULATE_VERTEX)
            {
                String error= "Two binds try to set the same property: "+property+" for the same node: "+n+" : "+XFormsUtil.getText(n);
                throw new XFormsBindingException(null,error,"");
            }
            return v;
        }
        switch (property)
        {
            case Vertex.CALCULATE_VERTEX :
                v = new CalculateVertex(n,container,xpath);
                break;
            case Vertex.RELEVANT_VERTEX :
                v = new RelevantVertex(n,container,xpath);
                break;
            case Vertex.READONLY_VERTEX :
                v = new ReadonlyVertex(n,container,xpath);
                break;
            case Vertex.ISVALID_VERTEX :
                v = new IsvalidVertex(n,container,xpath);
                break;
            case Vertex.REQUIRED_VERTEX :
                v = new RequiredVertex(n,container,xpath);
                break;
        }
        vertices.addElement(v);
        //		mappings.put(n,v);
        return v;
    }
    
    
    
    
    public Vertex getVertex(InstanceNode n, short property)
    {
        Enumeration e=vertices.elements();
        while (e.hasMoreElements())
        {
            Vertex v = (Vertex)e.nextElement();
            if (v.instanceNode==n&&v.getVertexType()==property) return v;
        }
        return null;
    }
    
    
    
    public void addVertex(Vertex v)
    {
        if (!vertices.contains(v))
        {
            vertices.addElement(v);
            //			mappings.put(v.instanceNode,v);
            //			Log.debug("Added a vertex for:: "+v.instanceNode+v.instanceNode.getNodeValue());
        } else
        {
            Log.debug("Graph already contains: "+v.instanceNode);
        }
    }
    
    
    public void addEdge(Vertex from, Vertex to)
    {
        from.addDep(to);
    }
    
    
    public void printGraph()
    {
        Enumeration e = vertices.elements();
        while (e.hasMoreElements())
        {
            Vertex v = (Vertex)e.nextElement();
            Log.debug("\nNext vertex:");
            v.print(0);
        }
        
    }
    
    
    
    public void removeNonZeroVertices()
    {
        Vector nonzeros=new Vector();
        Enumeration e = vertices.elements();
        while (e.hasMoreElements())
        {
            Vertex v = (Vertex)e.nextElement();
            if (v.inDegree>0)
            {
                nonzeros.addElement(v);
            }
        }
        
        e = nonzeros.elements();
        while (e.hasMoreElements())
        {
            Vertex v = (Vertex)e.nextElement();
            this.removeVertex(v);
        }
        //Log.debug("Removed nonzero vertices, vertices: "+vertices.size());
    }
    
    public void removeVertex(Vertex v)
    {
        this.vertices.removeElement(v);
    }
    
    
    
    
    
}
