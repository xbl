/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;


/**
 * @author honkkis
 *
 */
public interface TriggerElement extends LabeledElement
{
    /** dispatch the DOMActivate event */
    public void dispatchActivateEvent();
    public void dispatchClickEvent();

}
