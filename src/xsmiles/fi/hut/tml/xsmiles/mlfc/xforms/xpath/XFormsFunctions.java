/*
 * Created on Apr 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Duration;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class XFormsFunctions {
    static long seventiesMillis;
    private static final long millisInDay = (long)(1000l*60l*60l*24l);
    static 
    {
        	Calendar seventies = XFormsConfiguration.getInstance().getCalendarFromSchemaString("1970-01-01T00:00:00Z");
            // Markku Laine: Fixed the daylight saving bug
            seventiesMillis = seventies.getTime().getTime() + seventies.getTimeZone().getOffset( seventies.getTimeInMillis() );
     }

    /** xforms:instance() */
    public static Node instance(String param, ModelContext modelContext)
    {
        if (param==null||param.length()<1) return null;
        else
        {
            Document doc = modelContext.getInstanceDocument(param);
            if (doc==null) return null;
            else
            {
                Element documentElem = doc.getDocumentElement();
                if (documentElem==null) return null;
                return documentElem;            
            }
        }
    }
    
	/** xforms:cursor() */
	public  static Object cursor(
		String param, XFormsContext handler) 
	{
		int repeatCursor = ((handler==null) ? 0 :handler.getCursor(param));
		return new Integer(repeatCursor);
	}
	
	/** xforms:boolean-from-string */
	public  static Boolean boolean_from_string(
		String param) 
	{
		return (param.equalsIgnoreCase("true") ? Boolean.TRUE : Boolean.FALSE);
	}
	/** xforms:property */
	public  static String property(
		String param) 
	{
		if (param.equals("version")) return "1.0";
		if (param.equals("conformance-level")) return XFormsConfiguration.getInstance().getConformanceLevel();
		return null;
	}
	
	/** xforms:now */
	public  static String now() 
	{
		Calendar calendar = new GregorianCalendar();
		Date trialTime = new Date();
		calendar.setTime(trialTime);
		java.text.DecimalFormat format = new java.text.DecimalFormat();
		format.setMinimumIntegerDigits(2);
		
		/*
		From schema datatypes spec:
		For example, to indicate 1:20 pm on May the 31st, 1999 for Eastern Standard Time 
		which is 5 hours behind Coordinated Universal Time (UTC), one would write: 
		1999-05-31T13:20:00-05:00. 
		*/
 		String date=
			calendar.get(Calendar.YEAR)+"-"+
			format.format((calendar.get(Calendar.MONTH)+1))+"-"+
			format.format(calendar.get(Calendar.DAY_OF_MONTH))+"T"+
			format.format(calendar.get(Calendar.HOUR_OF_DAY))+":"+
			format.format((calendar.get(Calendar.MINUTE)))+":"+
			format.format((calendar.get(Calendar.SECOND)))+"-"+
			format.format(((int)(calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000))))+":00";
		return date;

	}

	/** xforms:days-from-date() */
	public  static Number daysFromDate(String durationStr)
	{
            try
            {
                
                long days;
                
                Calendar orig = XFormsConfiguration.getInstance().getCalendarFromSchemaString(durationStr);
                // Markku Laine: Fixed the daylight saving bug
                long origMillis = orig.getTime().getTime() + orig.getTimeZone().getOffset( orig.getTimeInMillis() );
                
                // lets add / remove a second to reduce fluctuation at the output
                if (origMillis>0) origMillis+=1000;
                else origMillis-=1000;
                
                long millisDifference = origMillis-seventiesMillis;
                days = millisDifference / millisInDay;
                //days = (long)(((double)millisDifference)/((double)millisInDay));
                Log.debug("seventies:"+seventiesMillis+" daysFromDate: origMillis:"+origMillis+" millisDiff:"+millisDifference+" days:"+days);
                return new Long(days);
            } catch (Exception e)
            {
                return new Double(Double.NaN);
            }
            //return new XNumber(duration.getSeconds());
	}    
	/** xforms:days-from-date() */
	public  static Number secondsFromDateTime(String durationStr) 
	{
			// execute the  expression
            try
            {
                
                int days;
                
                Calendar orig = XFormsConfiguration.getInstance().getCalendarFromSchemaString(durationStr);
                long millisDifference = orig.getTime().getTime()-seventiesMillis;
                BigDecimal millis = new BigDecimal(""+millisDifference).setScale(5);
                BigDecimal divisor = new BigDecimal("1000").setScale(5);
                //long secs = (int)( ((float)millisDifference/(1000f)) );
                BigDecimal res = millis.divide(divisor,5,BigDecimal.ROUND_FLOOR);
                return new Long(res.longValue());
            } catch (Exception e)
            {
                return new Double(Double.NaN);
            }
            //return new XNumber(duration.getSeconds());
	}    

    
    /** xforms:seconds */
	public  static Number seconds(String durationStr) throws Exception 
	{
            Duration d = new Duration(durationStr);
            // maybe not the best check...
            if (d.getSigned()==false&&d.getMinutes()==0&&d.getYears()==0&&d.getHours()==0&&d.getDays()==0)
            {
                return new Double(Double.NaN);
            }
            double sign = (d.getSigned()?-1f:1f);
            double secs = d.getSeconds() + (((((d.getDays() * 24) + d.getHours()) * 60) + d.getMinutes()) * 60);
            return new Double(sign*secs);
	}  	
    /** xforms:seconds */
	public  static Number months(String durationStr) throws Exception
	{
            Duration d = new Duration(durationStr);
            // maybe not the best check...
            if (d.getSigned()==false&&d.getMinutes()==0&&d.getYears()==0&&d.getHours()==0&&d.getDays()==0)
            {
                return new Double(Double.NaN);
            }
            double sign = (d.getSigned()?-1f:1f);
            double months = d.getMonths() + 12*(d.getYears());
            return new Double(sign*months);
            //return new XNumber(duration.getSeconds());
	}    
	
	/** xforms:min */
	public static Object min(Enumeration stringValues) 
	{
		double min = Double.MAX_VALUE	;
		int pos;

		while (stringValues.hasMoreElements())
		{
		    
			String s = (String)stringValues.nextElement();

			if (null != s)
			{
				double sval=Double.parseDouble(s);
				if (sval<min)
					min=sval;
			}	
		}
		return new Double(min);
	}

	/** xforms:max */
	public static Object max(Enumeration stringValues) 
	{

		double max = Double.MIN_VALUE	;
		int pos;

		while (stringValues.hasMoreElements())
		{
			String s = (String)stringValues.nextElement();

			if (null != s)
			{
				double sval=Double.parseDouble(s);
				if (sval>max)
					max=sval;
			}	
		}
		return new Double(max);
	}
	
	/** xforms:count-non-empty */
	public static Object count_non_empty(Enumeration nodes) 
	{
		int pos,count=0;

		while (nodes.hasMoreElements())
		{
			Node node = (Node)nodes.nextElement();
			// check whether this node is a leaf node, and whether it contains text
			if (node.getNodeType() == Node.ATTRIBUTE_NODE && node.getNodeValue().length()>0)
			{
				count++;
			}
			else if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				int textLen = 0;
				Node child = node.getFirstChild();
				boolean hasElementChildren = false;
				while (child!=null)
				{
					if (child.getNodeType() == Node.ELEMENT_NODE)
					{
						hasElementChildren=true;
						break;
					}
					else if (child.getNodeType()==Node.TEXT_NODE)
					{
						textLen=textLen+child.getNodeValue().length();
					}
					child = child.getNextSibling();
				}
				if (hasElementChildren==false && textLen>0)
				{
					count++;
				}
			}
		}
		Log.debug("count_non_empty returns : "+count);
		return new Integer(count);
	}
	/** xforms:avg function */
	//public static XObject avg(XPathContext xctxt, Expression m_arg0) throws javax.xml.transform.TransformerException
	public static Object avg(Enumeration stringValues) 
	{

		double sum = 0.0;
		int pos,count=0;

		while (stringValues.hasMoreElements())
		{
			String s = (String)stringValues.nextElement();

			if (null != s)
				sum += Double.parseDouble(s);
		}

		return new Double(sum/((double)count));
	}
	
	/** xforms:avg function */
	//public static XObject avg(XPathContext xctxt, Expression m_arg0) throws javax.xml.transform.TransformerException
	public static Object xif(Boolean question,Object ret_true, Object ret_false) 
	{
	    if (question.equals(Boolean.TRUE)) return ret_true;
	    return ret_false;
	}






}
