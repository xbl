/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.ui;

import java.awt.Color;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.events.UIEvent;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RepeatElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;




/**
 * The repeat index handler. Only one instance of this class exists per document
 *
 * in the bubbling phase, all index values are set, and traversed indexes are
 * unshown in the UI
 *
 * when the event comes to the root repeat, it starts to show nested repeat indexes
 * if some nested repeat is outside the event target, then the nested repeat's
 * index is set to 1
 *
 *    *  FUNCTIONAL:
 * o Repeat element listens for DOMFocusIn
 * o When DOMFocusIn is received, repeat calls RepeatIndexHandler, which
 *             + unsets the previous cursors in event flow
 *             + sets the new cursors for the whole repeat hierarchy
 *
 *
 * @author Mikko Honkala
 */


public class RepeatIndexHandler //implements InstanceBaseElementImpl.ChildListener
{
    
  /*
    public final static short CURSORS_UNSET=3;
    public final static short CURSORS_SET=10;
    // the status of cursors in document
    protected short indexStatus = CURSORS_UNSET;
   */
    
    protected Hashtable repeatsWithCursor = new Hashtable();
    protected Hashtable previousCursorValues = new Hashtable();
    
    public final static String CSS_CURSOR_BACKGROUND_COLOR_PROPERTY = "cursor-background-color";
    public final static String CSS_CURSOR_COLOR_PROPERTY = "cursor-color";
    
    /** the repeat cursors color */
    protected Color cursorFGColor = Color.black;
    
    /** the repeat cursors color */
    protected Color cursorBGColor = Color.yellow;
    
    protected XFormsContext xformsContext;
    
    public RepeatIndexHandler(XFormsContext context)
    {
        this.xformsContext=context;
    }
    
    /** the repeat has caught the DOMFocusIn event */
    public void repeatCaughtFocusEvent(UIEvent event,RepeatHandler handler)
    {
        Node target = (Node)event.getTarget();
        int item = this.resolveItemNumber(target,handler);
        this.setRepeatIndex(target,handler,item);
    }
    
    /** call this method when new items have been inserted into repeat or
     * old ones removed. Set index in the model before calling this! */
    public void repeatChanged(RepeatHandler handler)
    {
        //String id = handler.getRepeatId();
        //int item = this.xformsContext.getCursor(id);
		setRepeatIndexRecursively(handler.getUIElement());
        //this.showIndex(handler,item,null);
    }
    
    /** call this before any changes to UI, it will clear out the previous cursor */
    public void repeatAboutToChange(RepeatHandler handler)
    {
		//this.unsetPreviousIndex(handler);
    	/*
        String id = handler.getRepeatId();
        RepeatHandler prevHandler = (RepeatHandler)this.repeatsWithCursor.get(id);
        if (prevHandler!=null)
        {
            Integer prevCursor = (Integer)this.previousCursorValues.get(prevHandler);
            if (prevCursor!=null)
            {
                int prev = prevCursor.intValue();
                this.unsetPreviousIndex(null,prevHandler,prev);
            }
        }
        */
    }
    
    /** call this method when a repeat has been initialized in the beginning.
     * it will show the initial cursor */
    public void repeatInitialized(RepeatHandler handler)
    {
        if (!this.hasAncestorRepeat(handler))
        {
            //String id = handler.getRepeatId();
            //int item = this.xformsContext.getCursor(id);
			setRepeatIndexRecursively(handler.getUIElement());
        }
    }
    
    public void setRepeatIndex(Node target, RepeatHandler handler, int item)
    {
        String repeatId = this.resolveRepeatId(handler);
        //this.unsetPreviousIndex(handler);
        this.setIndexValue(repeatId,item);
        // at the root repeat, we start to show indexes recursively
        if (!this.hasAncestorRepeat(handler))
        {
            Log.debug("**** ROOT REPEAT FOUND");
            //this.showIndex(handler,item,(Element)target);
            this.setRepeatIndexRecursively(handler.getUIElement());
        }
        // this method checks whether the focus event came from outside child repeats, in which case
        // the inner repeats must be reset
        //this.resetInnerRepeatsIfNecessary(handler,event)
    }
    
    
	public void setRepeatIndexRecursively (Element elem)
	{
		if (elem instanceof RepeatElementImpl)
		{
			// TODO: optimize so that if cursor was already in this repeat item, then do not 
			// reset it
			RepeatHandler handler=((RepeatElementImpl)elem).getRepeatHandler();
			this.unsetPreviousIndex(handler);
			String repeatId = this.resolveRepeatId(handler);
			int item = this.xformsContext.getCursor(repeatId);
			RepeatItemPseudoElement pitem = this.getRepeatItem(handler,item);
			if (pitem!=null)
			{
				pitem.setCursorOn(true);
				this.repeatsWithCursor.put(repeatId,handler);
				int cursor = this.xformsContext.getCursor(repeatId);
				RepeatItemPseudoElement cursorItem = this.getRepeatItem(handler,cursor);
				this.previousCursorValues.put(repeatId,cursorItem);
				//Log.debug("Setting cursor on for: "+repeatId+ "cursor");
				cursorItem.setCursorOn(true);
				this.setRepeatIndexRecursively(cursorItem);
			}
		}
		else if (elem instanceof Element)
		{
			Node child = elem.getFirstChild();
			while(child!=null)
			{
				if (child instanceof Element) this.setRepeatIndexRecursively(((Element)child));
				child=child.getNextSibling();
			}
		}
	}
    
    
    public RepeatItemPseudoElement getRepeatItem(RepeatHandler handler,int i)
    {
		Vector pseudoElements = ((PseudoElementContainerService)handler.getUIElement()).getPseudoElements();
    	/*Vector pseudoElements = ((PseudoElementContainer)handler.getUIElement())
    		.getPseudoElements();
    	return pseudoElements.elementAt(i);*/
    	if (pseudoElements.size()<i) return null;
    	if (pseudoElements.size()==0) return null;
    	if (i==0) i=1;
		return (RepeatItemPseudoElement)pseudoElements.elementAt(i-1);
    }
    
    /** goes towards the root and reports if any repeat's are found as
     * ancestors */
    protected boolean hasAncestorRepeat(RepeatHandler handler)
    {
        boolean hasRepeat = false;
        Node parent = handler.getUIElement().getParentNode();
        while (parent.getNodeType()!=Node.DOCUMENT_NODE)
        {
            if (parent instanceof RepeatElementImpl) return true;
            parent=parent.getParentNode();
        }
        return false;
    }
    
    /** find out which item was focused */
    protected int resolveItemNumber(Node target,RepeatHandler handler)
    {
        return this.getIndex(target,handler);
    }
    /** find out the repeat id of this repeat handler */
    protected String resolveRepeatId(RepeatHandler handler)
    {
        return handler.getRepeatId();
    }
    
    /** unset the previous cursors for all repeats in the document, leave prevCursor=-1 if you want to use
     * the cursor value from model**/
	protected void unsetPreviousIndex(RepeatHandler handler)
	{
		String id = this.resolveRepeatId(handler);
		//Log.debug("unsetting repeat value for : "+id);
		RepeatHandler handler2 = (RepeatHandler)this.repeatsWithCursor.get(id);
		RepeatItemPseudoElement previousCursor = (RepeatItemPseudoElement)this.previousCursorValues.get(id);
		
		//Optimization, if no change, do not unset cursor
		if (handler2!=null && previousCursor!=null)
		{
			int item = this.xformsContext.getCursor(id);
			RepeatItemPseudoElement currentCursor = this.getRepeatItem(handler,item);
			if (previousCursor==currentCursor)
			{
				//Log.debug("New and old cursor was the same, no change.");
				return;	
			}
		}
		
		
		if (previousCursor!=null)previousCursor.setCursorOn(false);
	}
    /*
    protected void unsetPreviousIndex(Node target,RepeatHandler handler, int prevCursor)
    {
        //if (this.indexStatus==CURSORS_SET)
        if (true)
        {
            String key = handler.getRepeatId();
            RepeatHandler repeat = (RepeatHandler)repeatsWithCursor.get(key);
            if (repeat!=null)
            {
                int prevIndex=prevCursor;
                if (prevCursor<0)
                    prevIndex = this.xformsContext.getCursor(key);
                
                Vector nodesWithIndex = repeat.getUINodesForIndex(prevIndex);
                this.showIndex(nodesWithIndex,false,(Element)target);
                repeatsWithCursor.remove(key);
            }
            else
            {
                Log.error("Unset: Could not find repeat: "+key);
            }
        }
    }
    */
    /** set the cursor value thru model */
    protected void setIndexValue(String repeatId, int item)
    {
        this.xformsContext.setCursor(repeatId,item);
    }
    
    
    /** shows the current index recursively. sets the index value to 1 if
     * repeathandler is different than whats in the hashtable .
     * This is called from the root repeat
     */
    /*
    protected void showIndex(RepeatHandler handler,int item,Element target)
    {
        // TODO
        Log.debug("showIndex : "+handler.getRepeatId()+" item:"+item);
        Vector nodesForIndex = handler.getUINodesForIndex(item);
        this.showIndex(nodesForIndex,true,target);
        this.repeatsWithCursor.put(handler.getRepeatId(),handler);
        this.previousCursorValues.put(handler,new Integer(item));
    }
    protected void showIndex(Vector elements,boolean cursoron, Element target)
    {
        Enumeration e = elements.elements();
        while(e.hasMoreElements())
        {
            Node n  = (Node)e.nextElement();
            this.showRepeatCursorRecursively(n,cursoron,target);
        }
    }*/
    
    /**
     * @return whether the target was met
     */
    /*
    protected boolean showRepeatCursorRecursively(Node elem, boolean cursoron, Element target)
    {
        // TODO: use CSS pseudoclasses
        if (elem==null) return false;
        if (elem instanceof RepeatElementImpl)
        {
            if (!cursoron)
            {
                return false;
            } else
            {
                // this part handles the inner repeats
                RepeatHandler handler = ((RepeatElementImpl)elem).getRepeatHandler();
                String id = handler.getRepeatId();
                RepeatHandler handler2 = (RepeatHandler)this.repeatsWithCursor.get(id);
                Log.debug("Processing inner repeat: "+id+" the previous repeat:"+handler2);
                int item = this.xformsContext.getCursor(id);
                if (handler2!=null&&handler!=handler2)
                {
                    // this means that the target was outside the inner repeat, so
                    // let's set the inner cursor to 1
                    this.unsetPreviousIndex(target,handler2,-1);
                    item=1;
                }
                Log.debug("trying to set inner repeat to: "+item);
                this.xformsContext.setCursor(id,(item<1?1:item));
                this.showIndex(handler,item,target);
                return false;
            }
        }
        if (elem.getNodeType()==Node.ELEMENT_NODE)
        {
            if (elem instanceof XFormsControl)
            {
                XFormsControl control = ((XFormsControl)elem);
                control.setRepeatCursorOn(cursoron,cursorBGColor,cursorFGColor);
                //Log.debug("setRepeatCursorOn: "+cursoron+" control:"+control+" : "+control.getAttribute("ref"));
            }
            if (elem==target)
            {
                // go no further, but set inner repeats cursor to 1
                //if (cursoron) this.resetInnerRepeatIndexes((Element)elem);
                return true;
            }
            // recursive: call for child elements also
            Node child = elem.getFirstChild();
            boolean targetMet=false;
            while (child!=null)
            {
                if (child.getNodeType()==Node.ELEMENT_NODE)
                {
                    boolean t = this.showRepeatCursorRecursively((Element)child,cursoron,target);
                    if (t==true) targetMet = true;
                }
                child=child.getNextSibling();
            }
            return targetMet;
        }
        return false;
    }
    
    
    
    */
    /**
     * This method handles the event when the user selects a control in repeat
     * Events are used to set the repeatcursor to whatever the user has selected
     */
    protected int getIndex(Node target, RepeatHandler handler)
    {
        Element parent = handler.getUIElement();
        //Log.debug("REPEAT !!GOT EVENT!! Target: "+target+" id:"+this.resolveRepeatId(handler));
        
        
        // Go down the tree, and try to find the repeat's parent
        Node targetancestor=target.getParentNode();
        Node prev=target;
        while (targetancestor!=parent&&targetancestor!=null)
        {
            prev=targetancestor;
            targetancestor=targetancestor.getParentNode();
        }
        //Log.debug("Targetancestor: "+targetancestor);
        if (targetancestor==parent)
        {
            int index = handler.getIndex(prev);
            return index;
        }
        Log.error("Could not find the right repeat node! : repeat: "+resolveRepeatId(handler));
        return -1;
    }
    /**
     * Find the active instance of the given id as determined by enclosing
     * repeat's indexes. This is called outside this class
     *
     * @param elem  the element to search for
     * @param id    the elemnt's id
     * @return the appropriate instance; if there are no enclosing repeat's then
     *         this is just <var>elem</var>
     */
    
    public Element findRepeatedId(Element elem, String id)
    {
        // find enclosing repeat
        Node ancestor = elem.getParentNode();
        while (ancestor!=null&&ancestor.getNodeType()!=Node.DOCUMENT_NODE)
        {
            if (ancestor instanceof RepeatElementImpl) break;
            ancestor = ancestor.getParentNode();
        }
        
        // if not enclosed in a repeat then we're done
        if (!(ancestor instanceof RepeatElementImpl))
            return elem;
        
        // get the proper enclosing repeat
        RepeatElementImpl repeat = (RepeatElementImpl) ancestor;
        repeat = (RepeatElementImpl) findRepeatedId(repeat, repeat.getId());
        
        // get the proper element within the enclosing repeat
        int pos = this.xformsContext.getCursor(repeat.getId());
        if (pos>0)
            return XSmilesElementImpl.searchElementWithId(repeat,id,pos);
        else
            return elem;
    }
}