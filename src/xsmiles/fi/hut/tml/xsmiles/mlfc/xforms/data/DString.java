/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;

import fi.hut.tml.xsmiles.Log;

public class DString  implements Data{

    protected String stringValue;
    protected boolean isValid=true;
    protected short datatype;
	public DString(short dtype)
	{
        datatype=dtype;
	}
    
        /**
     * is the current value valid according to the datatype
     */
    public boolean isValid()
    {
        return this.isValid;
    }	
    /**
     * set the value as a Java Object
     */
    public void setValueFromObject(Object obj) {
        if (obj instanceof String)
        {
            stringValue = (String)obj;
        }
        else
        {
            Log.error(this+" got wrong type of value");
        }
    }
    /**
     * set the value from a display string
     */ 
    public void setValueFromDisplay(String displayValue)
    {
        this.stringValue=displayValue;
    }
    
    /**
     * set the value from a Schema string
     */ 
    public void setValueFromSchema(String schemaValue)
    {
        this.stringValue=schemaValue;
    }

    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    public String toDisplayValue()
    {
        return this.stringValue;
    }

    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    public String toSchemaString()
    {
        return this.stringValue;
    }
    
    /**
     * get the java object corresponding to the value
     */
    public Object toObject()
    {
        return this.stringValue;
    }    
    /**
     * get the invalid value as a string
     */
    public String getInvalidValue()
    {
        return this.stringValue;
    }
    
    /**
     * set the validity status
     */
    public void setValid(boolean v)
    {
        this.isValid=v;
    }
    
    public void setInvalidString(String s)
    {
        this.stringValue=s;
    }    /** instructs subclass to set value to null, this is used for
     * invalid values
     */
    protected void clearValue()
    {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.data.Data#getDataType()
     */
    public short getDataType()
    {
        // TODO Auto-generated method stub
        return datatype;
    }
    
} // DString
