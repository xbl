/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;

// for simple type id's


public class DataFactory {

    /** "string" type */
    public  short PRIMITIVE_STRING ;
    /** "boolean" type */
    public   short PRIMITIVE_BOOLEAN;
    /** "decimal" type */
    public   short PRIMITIVE_DECIMAL ;
    /** "float" type */
    public   short PRIMITIVE_FLOAT     ;
    /** "double" type */
    public   short PRIMITIVE_DOUBLE  ;
    /** "duration" type */
    public   short PRIMITIVE_DURATION ;
    /** "dataTime" type */
    public   short PRIMITIVE_DATETIME  ;
    /** "time" type */
    public   short PRIMITIVE_TIME          ;
    /** "date" type */
    public   short PRIMITIVE_DATE         ;
    /** "gYearMonth" type */
    public   short PRIMITIVE_GYEARMONTH   ;
    /** "gYear" type */
    public   short PRIMITIVE_GYEAR     ;
    /** "gMonthDay" type */
    public   short PRIMITIVE_GMONTHDAY    ;
    /** "gDay" type */
    public   short PRIMITIVE_GDAY        ;
    /** "gMonth" type */
    public   short PRIMITIVE_GMONTH       ;
    /** "hexBinary" type */
    public   short PRIMITIVE_HEXBINARY    ;
    /** "base64Binary" type */
    public   short PRIMITIVE_BASE64BINARY ;
    /** "anyURI" type */
    public  short PRIMITIVE_ANYURI    ;
    /** "QName" type */
    public   short PRIMITIVE_QNAME   ;
    /** "NOTATION" type */
    public static  short PRIMITIVE_NOTATION ;
    
    public static final int XSMILES_BASE = 10000;
    public static final int XSMILES_INTEGER = XSMILES_BASE+1;
    
    public DataFactory()
	{
        initializeDatatypeIDs();
	}
    public static short PRIMITIVE_DATETIME_STATIC      = 7;
	
	protected void initializeDatatypeIDs()
	{
	    /** "string" type */
	    PRIMITIVE_STRING        = 1;
	    /** "boolean" type */
	    PRIMITIVE_BOOLEAN       = 2;
	    /** "decimal" type */
	    PRIMITIVE_DECIMAL       = 3;
	    /** "float" type */
	    PRIMITIVE_FLOAT         = 4;
	    /** "double" type */
	    PRIMITIVE_DOUBLE        = 5;
	    /** "duration" type */
	    PRIMITIVE_DURATION      = 6;
	    /** "dataTime" type */
	    PRIMITIVE_DATETIME      = PRIMITIVE_DATETIME_STATIC;
	    /** "time" type */
	    PRIMITIVE_TIME          = 8;
	    /** "date" type */
	    PRIMITIVE_DATE          = 9;
	    /** "gYearMonth" type */
	    PRIMITIVE_GYEARMONTH    = 10;
	    /** "gYear" type */
	    PRIMITIVE_GYEAR         = 11;
	    /** "gMonthDay" type */
	    PRIMITIVE_GMONTHDAY     = 12;
	    /** "gDay" type */
	    PRIMITIVE_GDAY          = 13;
	    /** "gMonth" type */
	    PRIMITIVE_GMONTH        = 14;
	    /** "hexBinary" type */
	    PRIMITIVE_HEXBINARY     = 15;
	    /** "base64Binary" type */
	    PRIMITIVE_BASE64BINARY  = 16;
	    /** "anyURI" type */
	    PRIMITIVE_ANYURI        = 17;
	    /** "QName" type */
	    PRIMITIVE_QNAME         = 18;
	    /** "NOTATION" type */
	    PRIMITIVE_NOTATION      = 19;
	}
    

    
    /**
     * create a data object based on the Xerces primitive type id
     */
    public Data createData(int primitiveType)
    {
        short primitiveTypeId=(short)primitiveType;
        if (primitiveTypeId == PRIMITIVE_BOOLEAN)
            return new DBoolean(primitiveTypeId);
        else if (primitiveTypeId == PRIMITIVE_DECIMAL)
            return new DDecimal(primitiveTypeId);
        else if (primitiveTypeId ==XSMILES_INTEGER)
            return new DInteger(primitiveTypeId);
        return new DString(primitiveTypeId);
    }
	
} // RangeDecimal
