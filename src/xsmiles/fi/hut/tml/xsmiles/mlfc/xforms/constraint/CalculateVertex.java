/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.InstanceElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.BindElement;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;



import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.*;

import java.util.*;

   

/**
 * The vertex for the calculation algorithm
 * @author Mikko Honkala
 */


public class CalculateVertex extends Vertex {
//	public static final short type=DependencyGraph.CALCULATE_VERTEX;
	public CalculateVertex(InstanceNode n, ExpressionContainer expr, XPathExpr xpathExpr) {
		super(n,expr,xpathExpr);
	}
	public CalculateVertex() {
		super();
	}
	
	public void compute()
	{
		String result = runXPathAsString();
		if (result!=null)
		{
			XFormsUtil.setText(this.instanceNode,result);
		}
	}
	public short getVertexType()
	{
		return CALCULATE_VERTEX;
	}

	
}
