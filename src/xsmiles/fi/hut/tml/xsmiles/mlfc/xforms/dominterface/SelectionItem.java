/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;


/**
 * @author honkkis
 *
 */
public interface SelectionItem
{
    public String getLabel();
    public String getValue();
    /** select this item and reflect it in the UI*/
    public void select();

}
