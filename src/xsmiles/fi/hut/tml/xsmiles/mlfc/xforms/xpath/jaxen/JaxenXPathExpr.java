/*
 * Created on Apr 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen;

import java.util.List;
import java.util.Vector;

import org.jaxen.JaxenException;
import org.jaxen.XPath;
import org.w3c.dom.Text;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class JaxenXPathExpr implements XPathExpr,LookupCallback {
    protected String expr;
    protected BaseXPathEx jexpr;
    Vector referredNodes;
    protected boolean trackReferences=false;
    public JaxenXPathExpr(String e) throws JaxenException
    {
        
        this.jexpr = new BaseXPathEx(e,this);
        //Log.debug("JAXEN: created an expression: "+e);
        //this.jexpr.setReferredNodes(this);
        this.expr=e;
    }
    public String getExpression()
    {
        return this.expr;
    }
    public XPath getXPath()
    {
        return jexpr;
    }
    
    public void trackReferences(boolean t)
    {
        this.trackReferences=t;
    }
    
    public void nodesReferenced(List l)
    {
        if (trackReferences)
        {
	        if (referredNodes==null)
	        {
	            referredNodes=new Vector();
	            //Log.debug("Created referant list for :"+this.expr);
	        }
		    //System.out.println("contextNodeSet: "+l);
		    for (int i=0;i<l.size();i++)
		    {
		        Object o = l.get(i);
		        if (o instanceof Text) o =((Text)o).getParentNode();
		        //Log.debug("Adding referred node:"+o);
		        this.referredNodes.add(o);
		    }
        }
    }

}
