/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import fi.hut.tml.xsmiles.mlfc.xforms.constraint.Vertex;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PropertyInheriter;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.xforms10.*;

import java.util.Vector;

// Element implementation 
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.ElementNSImpl;

/**
 *
 */
public class BasicInstanceElementImpl extends ElementNSImpl implements InstanceParentNode{


	/** The listeners for child additions/removals (repeat elements) */
	protected Vector childlisteners;

	//InstanceDocumentImpl ownerDoc = null;
	InstanceItem instanceItem;
	
	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public BasicInstanceElementImpl(InstanceDocument owner, String nameSpace, String name) {
	    super((DocumentImpl)owner, nameSpace, name);
		instanceItem=new InstanceItem(this);
	}
	
	public InstanceItem getInstanceItem()
	{
		return this.instanceItem;
	}
    public PropertyInheriter getPropertyInheriter()
	{
		return this.instanceItem;
	}

	
	
	
	
	public void addChildListener(ChildListener listener)
	{
		// currently we allow only one listener per element
		if (childlisteners==null) 
			childlisteners=new Vector();
		//if (childlisteners.contains(listener)) return;
		childlisteners.addElement(listener);
	}
	
	public void removeChildListener(ChildListener listener)
	{
		if (childlisteners==null) return;
		childlisteners.removeElement(listener);
	}
	
	public Node insertBefore(Node newChild, Node refChild) 
	    throws DOMException {
		Node n = super.insertBefore(newChild, refChild);
		this.notifyChange(newChild,true);
		return n;
	} 
    public Node removeChild(Node oldChild) 
        throws DOMException {
		Node n = super.removeChild(oldChild);
		this.notifyChange(oldChild,false);
		return n;
    }
	
	
	
	protected void notifyChange(Node newChild,boolean addition)
	{
		if (childlisteners!=null)
		{
                        Vector cl = (Vector)childlisteners.clone(); // because someone might change the vector in between
			for (int i=0;i<cl.size();i++)
			{
				ChildListener listener = (ChildListener)cl.elementAt(i);
				listener.childEvent(addition,this,newChild);
			}
		
		}
	
	}
        

    public void notifyXSInilChanged()
    {
        this.getInstanceItem().nofityXSInilChanged(this.getInstanceItem().getXSInil());
    }
	
	
	
	
	public Object clone()
	{
		try
		{
			BasicInstanceElementImpl node=(BasicInstanceElementImpl)super.clone();
			node.instanceItem=new InstanceItem(node);
			node.childlisteners=null;
			return node;
		} catch (Exception e)
		{
			Log.error(e);
			return null;
		}
	}
	

    
}

