/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.xforms10.*;

// Element implementation 
//import org.apache.xerces.dom.ElementNSImpl;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;

/**
 *
 */
public interface InstanceParentNode extends InstanceNode {
	
    public void addChildListener(ChildListener list);
    public void removeChildListener(ChildListener list);
    
    

     
}

