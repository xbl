/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


import org.w3c.dom.events.*;


    

/**
 * the abstract base class for repeat's actions such as insert, delete and setrepeatcursor
 * @author Mikko Honkala
 */


public abstract class RepeatHandlerBaseImpl extends ActionHandlerBaseImpl 
		{
	
	public RepeatHandlerBaseImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}
	
	
	public void activate(Event evt)
	{
                    this.renewBinding();

	}
	
	
//	RepeatElementImpl getRepeatElement()
//	{
//		if (this.repeat==null) return this.getHandler().getRepeat(this.getRepeatId());
//		else return this.repeat;
//	}
	
	String getRepeatId()
	{
		return this.getAttribute(REPEAT_ID_ATTRIBUTE);
	}
	
	String getAtString()
	{
		return this.getAttribute("at");
	}
	
	/** Execute the XPAth expression in the 'at' attribute and convert it to int value */
	int getAt()
	{
		try
		{
			String atS=this.getAtString();
			XPathExpr xpathE = this.getModel().getXPathEngine().createXPathExpression(atS);
			if (atS==null||atS.length()<1) return -1;
			String ret = this.getModel().getXPathEngine().evalToString(this.getRefNode(),xpathE,this,this.getBoundNodeset());
			double atdouble = Double.parseDouble(ret);
			int at =  (int)atdouble;
            if (at<1) at=1;
                        return at;
		}
		catch (Exception e)
		{
			Log.error(e);
			return -1;
		}
	}
}
