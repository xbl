/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 8, 2005
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dom.swing;


import java.awt.Dimension;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;



/**
 * @author honkkis
 *
 */
public class XMLTree extends JTree {
        XMLTreeModel model;
        TreeElementImpl owner;
		Node rootNode;
        
        public XMLTree(Node aRootNode, TreeElementImpl aOwner) {
            super(new XMLTreeModel(aRootNode));
			rootNode=aRootNode;
            owner=aOwner;
			this.model=(XMLTreeModel)this.getModel();
            getSelectionModel().setSelectionMode(
                    TreeSelectionModel.SINGLE_TREE_SELECTION);
            DefaultTreeCellRenderer renderer = new XMLNodeRenderer();
            setCellRenderer(renderer);
            Dimension size = new Dimension(300,700);
            this.setSize(size);
            this.setPreferredSize(size);
        }
        
        public void setRoot(Node aNewRootNode)
        {
            rootNode=aNewRootNode;
            model.setRoot(rootNode);
            model.fireTreeStructureChanged(rootNode,rootNode,true);
        }
        public String  convertValueToText(Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) 
        {
            if (owner!=null)
                return owner.convertValueToText((Node)value,selected,expanded,leaf,row,hasFocus);
            else return super.convertValueToText(value,selected,expanded,leaf,row,hasFocus);
        }
		/** the TreeElementImpl notifies about XML structure changes through this method */
		public void structureChanged(Node n, Node parent,boolean removed)
		{
			//this.setModel(this.createModel());
			//this.setSelectionPath(new TreePath(this.rootNode));
			//this.collapsePath(new TreePath(this.rootNode));
			//this.model.fireTreeStructureChanged(n);
			if (parent!=null) this.model.fireTreeStructureChanged(parent,n,removed);
			//this.model.fireTreeStructureChanged((Node)model.getRoot());
			{
				this.expandPath(this.constructPathTo(parent));
				this.setSelectionPath(this.constructPathTo(parent));
				//this.expandPath(this.constructPathTo(n));
			}
		}
		
		protected XMLTreeModel createModel()
		{
			this.model = new XMLTreeModel(this.rootNode);
			return this.model;
		}
		protected TreePath constructPathTo(Node n)
		{
			try
			{
				int i=1;
				Node parent = n;
				while (parent!=null&&parent!=this.rootNode)
				{
					i++;
					parent=parent.getParentNode();
				}
				if (parent!=this.rootNode||i==0)
				{
					Log.error("Could not find a tree path to :"+n);
					return new TreePath(rootNode); // fallback...
				}
				Object[] path = new Object[i];
				parent = n;
				while (parent!=null&&i>0)
				{
					i--;
					path[i]=parent;
					parent=parent.getParentNode();
				}
				TreePath tpath = new TreePath(path);
				Log.debug("Constructed path: "+tpath);
				return tpath;
			} catch (Throwable t)
			{
				Log.error(t);
				return new TreePath(this.rootNode);
			}
		}
}
