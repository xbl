/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


   

/**
 * The vertex for the calculation algorithm
 * @author Mikko Honkala
 */


abstract public class Vertex {
	
	public static final short CALCULATE_VERTEX = 1;
	public static final short RELEVANT_VERTEX = 2;
	public static final short READONLY_VERTEX = 3;
	public static final short REQUIRED_VERTEX = 4;
	public static final short ISVALID_VERTEX = 5;
	
	/** a flag to notify that this vertex was already in graph */
	public boolean wasAlreadyInGraph=false;

//	public short type=-1;

	InstanceNode instanceNode;
	Vector depList; //should be array?
	boolean visited;
	int inDegree;
	Vertex index;
//	protected String calculateString;
    /** in what context to execute the Xpath string */
    ExpressionContainer container;
    
    /** the xpath string to execute */
    XPathExpr xpath;

	public Vertex(InstanceNode n, ExpressionContainer expr, XPathExpr xpathExpr) {
		instanceNode = n;
		depList=new Vector();
		visited=false;
		inDegree=0;
		index=null;
        container=expr;
        xpath = xpathExpr;
	}
	
	public Vertex()
	{
		depList=new Vector();
	}

//	public Vertex clone()
//	{
//		Vertex n = new Vertex();
//		n.InstanceNode = InstanceNode;
//	}

	public void addDep(Vertex to)
	{
		// 1. A vertex will be added to a depList only once
		// 2. Vertex v is excluded from its own depList to allow self-references 
		// to occur without causing a circular reference exception.
		if (this.depList.contains(to) || this == to) return;
		this.depList.addElement(to);
		to.inDegree++;
	}
	public void print(int level)
	{
//		for (int i=0;i<level;i++)
//			System.out.print("\t");
		System.out.println(level+" : vertex: "+instanceNode+" : "
			+XFormsUtil.getText(instanceNode)+ "inDegree:"+inDegree);
		Enumeration e = depList.elements();
		while (e.hasMoreElements())
		{
			Vertex v = (Vertex)e.nextElement();
			System.out.println(1+" : depvertex: "+v.instanceNode+" : "
				+XFormsUtil.getText(v.instanceNode)+" inDegree:"+v.inDegree);
		}
	}
	
	abstract public void compute();
	abstract public short getVertexType();
	public XPathExpr getXPath()
    {
        return this.xpath;
    }
    public Node getContextNode()
    {
        return this.instanceNode;
    }
    public Node getNamespaceContextNode()
    {
        return this.container.getNamespaceContextNode();
    }
	
	public Object runXPath() 
	{
		XPathExpr xpath=this.getXPath();
		//Log.debug("Vertex run xpath: "+xpath);
		if (xpath==null) return null;
        // NOTE: the last argument should be the namespace node (the bind element)
        Object result=null;
        try
        {
    		result = container.getXPathEngine().eval(this.getContextNode(),xpath,this.container.getNamespaceContextNode(),this.container.getContextNodeList());
        }
        catch (Exception e)
        {
            Log.error(e);
        }
		return result;	
	}
	public String runXPathAsString() 
	{
		XPathExpr xpath=this.getXPath();
		//Log.debug("Vertex run xpath: "+xpath);
		if (xpath==null) return null;
        // NOTE: the last argument should be the namespace node (the bind element)
        String result=null;
        try
        {
    		result = container.getXPathEngine().evalToString(this.getContextNode(),xpath,this.container.getNamespaceContextNode(),this.container.getContextNodeList());
        }
        catch (Exception e)
        {
            Log.error(e);
        }
		return result;	
	}
}
