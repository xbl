/*
 * /* X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources. Created on Nov 9, 2004
 *  
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.VisualElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.LabeledElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.RepeatItem;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectOneElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TextAreaElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;

/**
 * @author honkkis
 *  
 */
public class BaseSpeechWidget implements SpeechWidget
{
    protected FocusHandler focusHandler;

    protected Element element;
    
    public static final String currentSelectionString="";

    public BaseSpeechWidget(Element e, FocusHandler handler)
    {
        element = e;
        focusHandler = handler;
    }

    public Element getElement()
    {
        return this.element;
    }

    public String getLabel()
    {
        return this.getLabel(this.element);
    }

    public String getLabelForSpeaking()
    {
        return this.getLabelForSpeaking(this.element,false);
    }

    public static String getLabel(Element e)
    {
        String label="unknown";
        if (e instanceof LabeledElement)
        {
            label= ((LabeledElement) e).getLabelAsText().trim();
        }
        else if (e instanceof RepeatItem)
        {
            return getLabelForRepeatItem((RepeatItem)e);
        }
        else label=determineHTMLElementLabel(e);
        return removeNonAlphanumeric(label);
    }
    
    protected static String getLabelForRepeatItem(RepeatItem e)
    {
        return "item "+e.getIndex();
    }
    
    protected static String determineHTMLElementLabel(Element e)
    {
        // If a document element, return the title, if found
        if (e==e.getOwnerDocument().getDocumentElement())
        {
            if (e.getOwnerDocument() instanceof ExtendedDocument)
            {
                ExtendedDocument extdoc=(ExtendedDocument)e.getOwnerDocument();
                String title = extdoc.getTitle();
                if (title!=null&&title.length()>1) return title.trim();
            }
        }
        return findLabelHeuristically(e);
    }
    
    protected static String findLabelHeuristically(Element e)
    {
        Node child = e.getFirstChild();
        while (child!=null)
        {
                if (child.getNodeType()==Node.TEXT_NODE)
                {
                    String textValue = child.getNodeValue().trim();
                    if (textValue.length()>1) return textValue;
                }
            child=child.getNextSibling();
        }
        child = e.getFirstChild();
        while (child!=null)
        {
            if (child instanceof VisualElement)
            {
                VisualElement vis = (VisualElement)child;
                if (vis.isCurrentlyVisible())
                {
                    String ret = findLabelHeuristically((Element)vis);
                    if (ret!=null) return ret;
                }
            }
            child=child.getNextSibling();
        }
        return e.getLocalName();
    }

    public static String getLabelForSpeaking(Element e,boolean forGrammar)
    {
        if (e instanceof LabeledElement)
        {
            String label= getLabel(e);

            if (!forGrammar&&
                        
                                (e instanceof TypedElement && ((TypedElement)e).isInputControl()==false)
                                || 
                                (((LabeledElement)e).getAppearance().equals("full"))
                    )
                                
            {
                String value=getValueAsString(e);
                if (value!=null&&value.length()>0)
                {
                    label+=": "+currentSelectionString+value;
                }
            }
            return label.trim();
        }
        else return getLabel(e);
    }
    
    public static String getValueAsString(Element e)
    {
        String value="";
        if (e instanceof TypedElement)
        {
            if (((TypedElement)e).getData()!=null)
                value = ((TypedElement)e).getData().toDisplayValue();
        }
        else if (e instanceof SelectOneElement)
        {
            try
            {
                value= ((SelectOneElement)e).getCurrentSelection().getLabel();
            } catch (ArrayIndexOutOfBoundsException ex)
            {
                Log.debug("getValueAsString: array out of bounds. Probably no selection right now.");
            }
        }
        return value;
        
    }

    
    /** static prompt for this kind of control, for instance, "select a number"*/
    public String getPrompt()
    {
        return "";
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateDialogQuestion()
     */
    public String generateDialogQuestion()
    {
        // TODO Auto-generated method stub
        try
        {
            throw new RuntimeException("Never come to basespeechwidget generate dialog question");
        } catch (Exception e)
        {
            Log.error(e);
        }
        return "";
        //return "";//this.getLabel() + " my question";
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateGrammar(boolean)
     */
    public void generateGrammar(Grammar grammar)
    {
        // TODO Auto-generated method stub
        //return null;
    }

    protected void moveFocusToParent()
    {
        this.focusHandler.setFocus(this.focusHandler.getParentFocus(this.element));
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#interpretResponse(fi.hut.tml.xsmiles.mlfc.xforms.dialog.Response)
     */
    public boolean interpretResponse(Response response)
    {
        Log.debug(this + " interpretResponse: " + response.response);

        return false;
    }
    
    public static String removeNonAlphanumeric(String str)
    {
        StringBuffer ret = new StringBuffer(str.length());
        char[] testChars = str.toCharArray();
        for (int i = 0; i < testChars.length; i++)
        {
            if (Character.isLetterOrDigit(testChars[i]) ||
                testChars[i] == '.'||testChars[i] == ' ')
            {
                ret.append(testChars[i]);
            }
        }
        return ret.toString();
    }

    public String getCurrentValue()
    {
        if (this.element instanceof TypedElement)
            return ((TypedElement) this.element).getData().toDisplayValue();
        else  if (this.element instanceof TextAreaElement)
            return ((TextAreaElement) this.element).getCurrentTextValue();
        else return "";
    }

    public boolean approximateResponse(Response response)
    {
        // TODO Auto-generated method stub
        return false;
    }
}