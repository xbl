/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.dom.FlowLayoutElement;

import fi.hut.tml.xsmiles.Utilities;


import fi.hut.tml.xsmiles.Log;

import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;


import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;

/**
 * xforms/label element
 * @author Mikko Honkala
 */

public class CaptionElementImpl extends DynBoundElementImpl
implements InstanceItemListener, FlowLayoutElement
{
    
    // TODO: listen to ref node changes, and update UI + if in item, dispatch subtree modified event
    // to rebuild selects
    
    public CaptionElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        this.bindingAttributesRequired=false;
    }
    
    public void init()
    {
        // TODO: bind on label
        if (this.getRef()!=null&&this.getRef().length()>0)
        {
            try
            {
                this.createBinding();
                //Log.debug("inited caption: ref:"+this.refNode+" contextNode:"+this.contextNode);
                if (!isInsideItem())
                {
	                XFormsElementImpl.removeAllChildren(this);
	                this.handleLabelWithRef();
	             }
                //this.debugNode(this.contextNode);
            } catch (XFormsBindingException e)
            {
                this.handleXFormsException(e);
                return;
            }
        }
        //this.registerListeners();
        super.init();
    }
    
    /**
     * The new CSS renderer will take care of the display of the ref
     * So we must modify the DOM to put there the correct value from the instance
     *
     */
    protected void handleLabelWithRef()
    {
        if (this.getRefNode()!=null&&!isInsideItem())
        {
            //XFormsElementImpl.removeAllChildren(this);
            String refText = XFormsUtil.getText(this.getRefNode());
            this.setTextContent(refText);
            /*
            if (!(this.getParentNode() instanceof ItemElementImpl))
            {
                this.getParentNode()).updateStyle();
            }
            */
            
        }
    }
    
    private boolean isInsideItem()
    {
        return (this.getParentNode() instanceof ItemElementImpl);
    }
    
    public void destroy()
    {
        //this.unregisterListeners();
        super.destroy();
    }
    
    public String getCaptionText()
    {
        try
        {
            return this.getTextWithPrecedence();
        } catch (XFormsLinkException e)
        {
            Log.error(e);
            return "";
        }
    }
    
    /**
     * An instance item instructs the control to check its status, when the status changes
     */
    public void checkValidity(InstanceItem item)
    {
    }
    
    /**
     * An instance item instructs the control to check its status, when the status changes
     */
    public void checkVisibility(InstanceItem item)
    {
    }
    
    public void setReadonly(boolean readonly)
    {
    }
    public void setRequired(boolean required)
    {
    }
    
    /**
     * The value of this instanceItem has changed
     */
    public void valueChanged(String newValue)
    {
        Log.debug("CAPTION: VALUE CHANGED: "+newValue);
        if (this.getRef()!=null&&this.getRef().length()>0)
        {
            this.handleLabelWithRef();
        }
            
         if (this.getParentNode() instanceof ItemElementImpl)
        {
            // this way the select can change it's view
            boolean result=false;
            result = this.dispatch(XFormsEventFactory.createEvent(XFormsConstants.SUBTREE_MODIFY_START));
            result = this.dispatch(XFormsEventFactory.createEvent(XFormsConstants.SUBTREE_MODIFY_END));
        }
        else if (this.getParentNode() instanceof XFormsControl)
        {
            boolean result = this.dispatch(XFormsEventFactory.createEvent(XFormsConstants.CAPTION_CHANGED));
        }
    }
    
    /**
     * notify this listener that there was an error in the value of the
     * instance item. This can be schema validity, constraint, required etc.
     */
    public void notifyError(Exception e,boolean atSubmission)
    {
    }
    
    
    protected void registerListeners()
    {
    }
    
    protected void unregisterListeners()
    {
    }
    /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        super.notifyBindingChanged(newBinding);
        boolean result = this.dispatch(XFormsEventFactory.createEvent(XFormsConstants.CAPTION_CHANGED));
        //Log.debug("BINDING HAS CHANGED FOR CAPTION"+this);
    }
    /** the ancestor may signal thru this method that its binding has changed, and therefore
     * I should renew my context node and binding */
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor)
    {
        if (this.getParentNode() instanceof ItemSetElementImpl)
        {
            //Log.debug("BLOCKING"+this +"got event that parents binding has changed. parent: "+ancestor);
        }
        else
        {
            super.notifyParentBindingChanged(ancestor);
        }
    }
    
    /** @return the instanceItemListener for the referred nodes, for value
     * changes without binding change */
    public InstanceItemListener getInstanceItemListener()
    {
        return this;
    }

    
    
    
    
    
}
