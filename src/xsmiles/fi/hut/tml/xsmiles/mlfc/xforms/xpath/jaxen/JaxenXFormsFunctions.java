/*
 * Created on Apr 20, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.jaxen.Context;
import org.jaxen.Function;
import org.jaxen.FunctionCallException;
import org.jaxen.FunctionContext;
import org.jaxen.Navigator;
import org.jaxen.UnresolvableException;
import org.jaxen.XPathFunctionContext;
import org.jaxen.function.BooleanFunction;
import org.jaxen.function.NumberFunction;
import org.jaxen.function.StringFunction;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsFunctions;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class JaxenXFormsFunctions extends XPathFunctionContext implements FunctionContext{
    public XPathEngine xpathEngine;

    //TODO: use registerfunction instead of overriding getfunction
    public JaxenXFormsFunctions()
    {
        super();
    }
    String ns=XFormsConstants.XFORMS_NS;
    public void registerXFormsFunctions()
    {
	    registerXFormsFunction(  "instance", new Instance(xpathEngine) );
	    registerXFormsFunction(  "index", new Cursor(xpathEngine) );

	    registerXFormsFunction(  "avg", new Avg(xpathEngine) );
	    registerXFormsFunction(  "min", new Min(xpathEngine) );
	    registerXFormsFunction(  "max", new Max(xpathEngine) );
	    registerXFormsFunction(  "count-non-empty", new CountNonEmpty(xpathEngine) );
	    registerXFormsFunction(  "property", new Property(xpathEngine) );
	    registerXFormsFunction(  "now", new Now(xpathEngine) );
	    registerXFormsFunction(  "days-from-date", new DaysFromDate(xpathEngine) );
	    registerXFormsFunction(  "seconds-from-dateTime", new SecondsFromDateTime(xpathEngine) );
	    registerXFormsFunction(  "seconds", new Seconds(xpathEngine) );
	    registerXFormsFunction(  "months", new Months(xpathEngine) );
	    registerXFormsFunction(  "boolean-from-string", new BooleanFromString(xpathEngine) );
	    registerXFormsFunction(  "if", new XIf(xpathEngine) );
	    registerXFormsFunction(  "current", new Current(xpathEngine) );
	
    }    
    public void registerXFormsFunction(String name,Function func)
    {
        // for older forms, support also the non-namespaced function
	    registerFunction( ns, name,func) ;
	    registerFunction( null, name,func) ;
    }
    public void setXPathEngine(XPathEngine eng)
    {
        this.xpathEngine=eng;
        registerXFormsFunctions();
    }
    /* (non-Javadoc)
     * @see org.jaxen.FunctionContext#getFunction(java.lang.String, java.lang.String, java.lang.String)
     */
    /*
    public Function getFunction(String namespaceURI, String prefix, String localName) throws UnresolvableException {
        if (namespaceURI==null||namespaceURI.equals(XFormsConstants.XFORMS_NS))
        {
            if (localName.equals("instance")) return new Instance(xpathEngine);
            if (localName.equals("cursor")) return new Cursor(xpathEngine);
        }
        return super.getFunction(namespaceURI,prefix,localName);
    }*/
    public class XFormsFunc 
    {
        public XPathEngine xpathengine;
        public XFormsFunc(XPathEngine eng)
        {
            this.xpathengine=eng;
        }
    }
    public class Instance extends XFormsFunc implements Function
    {
        public Instance(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                String    id = StringFunction.evaluate( args.get( 0 ),   nav );
                return XFormsFunctions.instance(id,xpathengine.getModelContext());
            }
            return null;
        }
    }
    public class Cursor extends XFormsFunc implements Function
    {
        public Cursor(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                String    id = StringFunction.evaluate( args.get( 0 ),   nav );
                return XFormsFunctions.cursor(id,xpathengine.getXFormsContext());
            }
            return null;
        }
    }

    public class BooleanFromString extends XFormsFunc implements Function
    {
        public BooleanFromString(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                String    val = StringFunction.evaluate( args.get( 0 ),   nav );
                return XFormsFunctions.boolean_from_string(val);
            }
            return null;
        }
    }
    public class Property extends XFormsFunc implements Function
    {
        public Property(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                String    val = StringFunction.evaluate( args.get( 0 ),   nav );
                return XFormsFunctions.property(val);
            }
            return null;
        }
    }
    public class Now extends XFormsFunc implements Function
    {
        public Now(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 0)
            {
                return XFormsFunctions.now();
            }
            return null;
        }
    }
    public class DaysFromDate extends XFormsFunc implements Function
    {
        public DaysFromDate(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                String    val = StringFunction.evaluate( args.get( 0 ),   nav );
                return XFormsFunctions.daysFromDate(val);
            }
            return null;
        }
    }
    public class SecondsFromDateTime extends XFormsFunc implements Function
    {
        public SecondsFromDateTime(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                String    val = StringFunction.evaluate( args.get( 0 ),   nav );
                return XFormsFunctions.secondsFromDateTime(val);
            }
            return null;
        }
    }
    public class Seconds extends XFormsFunc implements Function
    {
        public Seconds(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            try
            {
	            if (args.size() == 1)
	            {
	                Navigator nav = context.getNavigator();
	                String    val = StringFunction.evaluate( args.get( 0 ),   nav );
	                return XFormsFunctions.seconds(val);
	            }
            } catch (Exception e)
            {
                throw new FunctionCallException(e);
            }
            return null;
        }
    }
    public class Months extends XFormsFunc implements Function
    {
        public Months(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            try
            {
	            if (args.size() == 1)
	            {
	                Navigator nav = context.getNavigator();
	                String    val = StringFunction.evaluate( args.get( 0 ),   nav );
	                return XFormsFunctions.months(val);
	            }
            } catch (Exception e)
            {
                throw new FunctionCallException(e);
            }
            return null;
        }
    }
    
    
    public class Min extends XFormsFunc implements Function
    {
        public Min(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                Object obj= args.get(0);
                double sum  = 0;
                double term = 0;

                if (obj instanceof List)
                {
                    StringEnum e = new StringEnum((List)obj,nav);
                    return XFormsFunctions.min(e);
                }
                else
                {
                    sum += NumberFunction.evaluate( obj,
                                                    nav ).doubleValue();
                }

                return new Double(sum);
            }
            return null;
        }
    }
    public class Max extends XFormsFunc implements Function
    {
        public Max(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                Object obj= args.get(0);
                double sum  = 0;
                double term = 0;

                if (obj instanceof List)
                {
                    StringEnum e = new StringEnum((List)obj,nav);
                    return XFormsFunctions.max(e);
                }
                else
                {
                    sum += NumberFunction.evaluate( obj,
                                                    nav ).doubleValue();
                }

                return new Double(sum);
            }
            return null;
        }
    }
    public class XIf extends XFormsFunc implements Function
    {
        public XIf(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 3)
            {
                Navigator nav = context.getNavigator();
                Object obj0= args.get(0);
                Object obj1= args.get(1);
                Object obj2= args.get(2);
                Boolean check = BooleanFunction.evaluate( obj1, nav );
                return XFormsFunctions.xif(check,obj1,obj2);
            }
            return null;
        }
    }
    public class Avg extends XFormsFunc implements Function
    {
        public Avg(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                Object obj= args.get(0);
                double sum;

                if (obj instanceof List)
                {
                    StringEnum e = new StringEnum((List)obj,nav);
                    return XFormsFunctions.avg(e);
                }
                else
                {
                    sum = NumberFunction.evaluate( obj,
                                                    nav ).doubleValue();
                }

                return new Double(sum);
            }
            return null;
        }
    }
    public class CountNonEmpty extends XFormsFunc implements Function
    {
        public CountNonEmpty(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 1)
            {
                Navigator nav = context.getNavigator();
                Object obj= args.get(0);
                NodeEnum e;
                if (obj instanceof List)
                {
                    e = new NodeEnum((List)obj);
                    return XFormsFunctions.count_non_empty(e);
                }
                else if (obj instanceof Node)
                {
                    e = new NodeEnum((List)obj);
                    return XFormsFunctions.count_non_empty(e);
                }
                else return new Double(0); // TODO: is this right?
            }
            return null;
        }
    }
    public class Current extends XFormsFunc implements Function
    {
        public Current(XPathEngine eng) {super(eng);}
        public Object call(Context context, List args) throws FunctionCallException {
            if (args.size() == 0)
            {
                try
                {
	                Node n = (Node)context.getVariableValue(JaxenXPathEngine.VARIABLE_NS,"",JaxenXPathEngine.CONTEXT_NODE_VARIABLE);
	                //Node n =  (Node)context.getNodeSet().get(0);
	                //Log.debug("current returning: "+n);
	                return n;
                } catch (UnresolvableException e)
                {
                    Log.error(e);
                    throw new FunctionCallException(e.getMessage());
                }
            }
            return null;
        }
    }

/** ENUMERATIONS */
    public class StringEnum implements Enumeration
    {
        Iterator it;
        Navigator nav;
        public StringEnum(List l, Navigator n)
        {
            it=l.iterator();
            nav=n;
        }
        public boolean hasMoreElements() {
            return it.hasNext();
        }
        public Object nextElement() {
            Node n = (Node)it.next();
            return StringFunction.evaluate(n,nav);
        }
    }
    public class NodeEnum implements Enumeration
    {
        Iterator it;
        Node node;
        boolean nodeReturned=false;
        public NodeEnum(List l)
        {
            it=l.iterator();
        }
        public NodeEnum(Node n)
        {
            node=n;
        }
        public boolean hasMoreElements() {
            if (node!=null)
            {
                if (nodeReturned==false) return true;
                return false;
            }
            return it.hasNext();
        }
        public Object nextElement() {
            if (node!=null)
            {
                if (nodeReturned==false) 
                {
                    nodeReturned=true;
                    return node;
                    
                }
                else return null;
            }
            Node n = (Node)it.next();
            return n;
        }
    }



}
