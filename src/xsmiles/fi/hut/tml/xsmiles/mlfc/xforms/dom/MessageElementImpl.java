/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;



import java.util.Vector;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.MessageElement;
import fi.hut.tml.xsmiles.mlfc.xforms.ui.CSSMessageImpl;

import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;




import org.w3c.dom.events.*;

/**
 * XForm/setValue element implementation
 * @author Mikko Honkala
 */


public class MessageElementImpl extends ActionHandlerBaseImpl implements 
	EventHandlerService,
	MessageElement,
	PseudoElementContainerService
{
    
    protected CSSMessageImpl message;

    public MessageElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        this.bindingAttributesRequired=false;
    }
    
    protected String getHeading()
    {
        return "XForms message";
    }
    
    public void init()
    {
        
        // try to find my context node from ancestor
        // if repeat has already set my contextNode, don't fiddle with it!
        try
        {
            this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
            
        }
        if  (message==null)
        {
            this.message=new CSSMessageImpl();
            this.message.setMessage(this);
        }
        //this.findAndSetRefNode(getRef());
        super.init();
    }
    
     public String getLevel()
    {
        return this.getAttribute(LEVEL_ATTRIBUTE);
    }
    
    public void activate(Event evt)
    {
         Log.debug("Message.activate()"+evt.getType());
        String level = getLevel();
        if ("modal".equals(level)||"modeless".equals(level)||"ephemeral".equals(level))
        {
            boolean isModal=false;
            int timeToLive = -1;
            if (level.equals("modal")) isModal=true;
            // for ephemeral box, make timetolive 2 secs
            else if (level.equals("ephemeral")) timeToLive=2000;
            // open a messagebox
            // TODO: check whether CSS is in use
            ModelElementImpl model = this.getModel();
            boolean useCSSMessage = (model!=null
                    &&model.getModelStatus()!=ModelElementImpl.MODEL_CONSTRUCTING
                    &&model.getModelStatus()!=ModelElementImpl.MODEL_NOTCONSTRUCTED);
            boolean cssMessageFailed=false;
            if (useCSSMessage)
                cssMessageFailed = !this.showMessage();
            if (!useCSSMessage||cssMessageFailed)
            {
                String message ="";
                try
                {
                    message = this.getMessageText();
                } catch (XFormsLinkException e)
                {
                    dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.LINK_ERROR));
                    getHandler().showUserError(e);
                    return;
                }
                this.getHandler().showMessageDialog(isModal,getHeading(),message,false,timeToLive);
            }
        }
        // else ignore
    }
    protected boolean showMessage()
    {
        if (message==null) return false;
        message.show();
        this.dispatch(XFormsConstants.XSMILES_MESSAGE_SHOWN_EVENT);
        return true;
    }
    

    
    public String getMessageText() throws XFormsLinkException
    {
        return this.getTextWithPrecedence();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.PseudoElementContainerService#getPseudoElements()
     */
    public Vector getPseudoElements()
    {
        // TODO Auto-generated method stub
        if (this.message!=null) return message.getPseudoElements();
        else return null;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.MessageElement#getMessageAsString()
     */
    public String getMessageAsString()
    {
        // TODO Auto-generated method stub
        try
        {
            return getMessageText();
        } catch (XFormsLinkException e )
        {
            Log.error(e);
            return "";
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.MessageElement#getMessageTypeAsString()
     */
    public String getMessageTypeAsString()
    {
        // TODO Auto-generated method stub
        return "message";
    }
    
    

    
    
    
    
}
