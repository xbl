/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.util.URLUtil;

import java.util.Hashtable;

import java.io.*;

import java.net.*;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;


import org.w3c.dom.*;
import org.xml.sax.SAXException;


import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XUpload;

import fi.hut.tml.xsmiles.mlfc.general.Helper;
import fi.hut.tml.xsmiles.mlfc.general.Base64EncoderStream;
import fi.hut.tml.xsmiles.mlfc.general.HexBin;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.data.XFormsDatatypeException;



/**
 * The XForms/Button element
 * @author Mikko Honkala
 */

public class UploadElementImpl extends ButtonElementImpl implements ActionListener
{
    protected boolean isInputComponent=false;
    
    
    
    /** the possible filename element child */
    FileNameElementImpl file=null;
    /** the possible mediatype element child */
    MediaTypeElementImpl media=null;
    
    /**
     * Constructs a new XButton (XForms/button).
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    
    
    public UploadElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public XComponent createComponent()
    {
        XComponent c = this.getComponentFactory().getXUpload(this.checkCaption().getCaptionText());
        return c;
    }
    
    public void init()
    {
        try
        {
            createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        }
        super.init();
    }
    
    /**
     * finds the optional child elements
     */
    protected void setChildElements()
    {
        file=null;
        media=null;
        
        Node child =this.getFirstChild();
        while (child!=null)
        {
            if (file==null && child instanceof FileNameElementImpl) file=(FileNameElementImpl)child;
            if (media==null && child instanceof MediaTypeElementImpl) media=(MediaTypeElementImpl)child;
            if (file!=null&&media!=null) break;
            child=child.getNextSibling();
        }
    }
    
    
    /**
     * JButton clicks are handled by actionPerformed
     */
    public void actionPerformed(ActionEvent ae)
    {
        try
        {
            Log.debug("upload clicked");
            int datatypeID = this.checkDatatypeId();
            // this is a kludge, since only file selection events come from XUpload
            // other straight from the component itself
            // needs fix in components
            if (ae.getSource() instanceof XUpload)
            {
                setChildElements();
                //String filename = f.toString();
                String filename = ae.getActionCommand();
                // TODO: set media type
                File ffile =  new File(filename);
                String filenameWithoutPath = ffile.getName();
                URL url = URLUtil.fileToURL(ffile);
                URLConnection conn = url.openConnection();
                String mediaType = conn.getContentType();
                if (file!=null) file.setRefNodeValue(filenameWithoutPath,false);
                if (media!=null) media.setRefNodeValue(mediaType,false);
                InputStream is = conn.getInputStream();
                this.encodeFile(is,getRefNode(),datatypeID, mediaType,filenameWithoutPath);
                //input.setText(filename);
                //InstanceItem item= refNode.getInstanceItem();
                //item.setUploadFileName(filename);
                
            } /*			File f = this.getHandler().showFileDialog();
                        if (f!=null)
                        {
                        }
             **/
        } catch (XFormsDatatypeException ex)
        {
            Log.error(ex);
            this.getHandler().showUserError(ex);
        } catch (Exception e)
        {
            Log.error(e);
            this.getHandler().showUserError(e);
        }
        super.actionPerformed(ae);
        
        
    }
    
    static final byte[] buffer= new byte[1000];
    
    
    /** reads the file and converts it to the encoding specified, and puts it as the text of
     * the instance node to */
    static void encodeFile(InputStream is, InstanceNode to, int type, String mediaType, String filename) throws IOException
    {
        if (to==null)
        {
            Log.error("The reference in <upload> is null");
            return;
        }
        DataFactory dfact = XFormsConfiguration.getInstance().getDataFactory();
        if (type==dfact.PRIMITIVE_HEXBINARY)
        {
            encodeFileHexBin(is,to,mediaType,filename);
        }
        else if (type==dfact.PRIMITIVE_ANYURI)
        {
            encodeFileURI(is,to,mediaType,filename);
        }
        else
        {
            encodeFileBase64(is,to,mediaType,filename);
        }
    }
    /** reads the file and converts it to HexBin, and puts it as the text of
     * the instance node to */
    static void encodeFileHexBin(InputStream is, InstanceNode to,String mediaType, String filename) throws IOException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Helper.copyStream(is,os,500);
        byte[] bytes = os.toByteArray();
        to.getInstanceItem().createURIAttachment(bytes,mediaType,filename,false);
        String encoded = HexBin.encode(bytes);
        XFormsUtil.setText(to,encoded);
    }
    /** reads the file and converts it to URI to be used with multipart/related
     * and saves it as a hidden model item property */
    static void encodeFileURI(InputStream is, InstanceNode to, String mediaType, String filename) throws IOException
    {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        Helper.copyStream(is,bo,500);
        retrieveInstanceItem(to).createURIAttachment(bo.toByteArray(),mediaType,filename,true);
    }
    /** reads the file and converts it to Base64, and puts it as the text of
     * the instance node to */
    static void encodeFileBase64(InputStream is, InstanceNode to, String mediaType, String filename)
    {
        try
        {
            //FileInputStream is = new FileInputStream(f);
            ByteArrayOutputStream bo2 = new ByteArrayOutputStream();
            Helper.copyStream(is,bo2,500);
            
            byte[] bytes = bo2.toByteArray();
            retrieveInstanceItem(to).createURIAttachment(bytes,mediaType,filename,false);
            
            ByteArrayInputStream bi = new ByteArrayInputStream(bytes);
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            Base64EncoderStream os = new Base64EncoderStream(bo);
            Helper.copyStream(bi,os,500);
            bo.close();
            String conv = bo.toString();
            XFormsUtil.setText(to,conv);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    /** check the bound datatype, throw an exception if it is not one of the supported types */
    protected int checkDatatypeId() throws XFormsDatatypeException
    {
        int dt = this.getDatatypeId();
        DataFactory dfact =XFormsConfiguration.getInstance().getDataFactory();
        if (dt!=dfact.PRIMITIVE_HEXBINARY &&dt!=dfact.PRIMITIVE_BASE64BINARY&&dt!=dfact.PRIMITIVE_ANYURI)
        {
            throw new XFormsDatatypeException(this,"Upload binds only to uri, base64 or hexbin types.");
        }
        return dt;
    }
}
