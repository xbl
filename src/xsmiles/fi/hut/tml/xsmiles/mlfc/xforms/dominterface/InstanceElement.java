/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;

import java.io.InputStream;
import java.net.URL;

/**
 * @author Mikko Honkala
 */


public interface InstanceElement extends org.w3c.dom.Element
{
    /** reset this instance */
    public void reset() throws Exception;
    /** reset the instance document from this stream. Notify model that it
     *should update all bindings etc.
     */
    public void readInstanceAndReset(InputStream stream,URL url);

}

