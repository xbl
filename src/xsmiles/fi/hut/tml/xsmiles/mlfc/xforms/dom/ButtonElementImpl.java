/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Component;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



import org.w3c.dom.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TriggerElement;

/**
 * The XForms/Button element
 * @author Mikko Honkala
 */

public class ButtonElementImpl extends XFormsControl 
implements TriggerElement, ActionListener {
    protected boolean isInputComponent=false;
    /**
     * Constructs a new XButton (XForms/button).
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    
    
    public ButtonElementImpl(XFormsElementHandler owner, String ns, String name) {
        super(owner, ns, name);
        bindingAttributesRequired=false;
    }
    /** this is called in init, so that button can override the default
     * behaviour
     */
    protected void checkBindingState() {
        // if the binding was to a non-existent node, we should make the
        // form control invisible unless it is a button or submit
        //if (this.getBindingState()==DynBoundElementImpl.BOUND_NODE_NOT_FOUND)
        //this.setVisible(false);
    }
    
    protected void registerListener() {
        super.registerListener();
        this.component.addActionListener(this);
    }
    
    public void destroy() {
        if (this.component!=null) this.component.removeActionListener(this);
        super.destroy();
    }
    
    /**
     * Creates the visual component of this control.
     */
    
    public XComponent createComponent() {
        CaptionElementImpl capt = this.checkCaption();
        return this.getComponentFactory().getXButton(capt.getCaptionText(),null);
    }
    
    /**
     * Formats the content according to the CSS style attribute
     */
        /*
        protected void formatContent()
        {
                        this.formatComponent(contentComp);
                        this.setDefaultSize();
        }*/
    protected void changeComponentValue(String newValue) {
    }
    
    public void mouseClicked(java.awt.event.MouseEvent evt) {
    }
    

    /**
     * JButton clicks are handled by actionPerformed
     */
    public void actionPerformed(ActionEvent aes) {
        final ActionEvent ae = aes;
        if (ae.getActionCommand()=="help") {
            doHelp(ae);
        } else {
            dispatchClickEvent();
            dispatchActivateEvent();
        }
    }
    
        /*protected void sizeComponent(double zoom)
        {
                this.contentComp.setSize(this.contentComp.getPreferredSize());
                this.contentComp.invalidate();
                this.contentComp.validate();
         
                Log.debug("button.sizeComponent("+zoom+") preferredSize:"+contentComp.getPreferredSize());
        }*/
    
    /**
     * marks this component as being under the repeat cursor
     * @attr corsoron, whether to set or unset the cursor
     * @attr color optional color attribute
     */
    public void setRepeatCursorOn(boolean cursoron, Color bgcolor, Color fgcolor) {
    }
    /*
    private static Vector pseudoVector=new Vector(1);
    public Vector getPseudoElements() {
        return null;
    }*/
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TriggerElement#dispatchActivate()
     */
    
    
    
}
