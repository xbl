/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import java.awt.event.ActionListener;

/**
 * An adaptive control is a datatype aware control
 * that uses the datamapper functionality to map between schema
 * and display values 
 */

public interface Control extends TypedElement {
    /**
     * returns the abstract component for this control. The
     * abstract component can be used e.g. to style the component
     * but all listeners should be added to the AdaptiveControl and not directly 
     * to the XComponent */
    public XComponent getComponent();
    
    /** 
     * add a listener for changes in the component
     */
    public void addChangeListener(XChangeListener listener);
    
    /** 
     * add a listener for actions in the component
     */
    public void addActionListener(ActionListener listener);
    
    /**
     * set the components value
     *
     */
    //public void setValueFromObject(Object value);
    
    /**
     * get the components current value
     */
    public Data getValue();
    
    /**
     * this object is used to map between different representations of a value
     */
    //public void setData(Data data);
    
    /**
     * this function is used to notify the control to update its display according
     * to the content of Data
     */
    public void updateDisplay();
    
    /**
     * This notifies the text control to save its value before rewiring
     */
    public void rewiringAboutToHappen();
    
    /**
     * close up, free all memory (yet, do not do visible changes, such as setVisible
     * since this will slow things up
     */
    public void destroy();
	
} // AdaptiveControl
