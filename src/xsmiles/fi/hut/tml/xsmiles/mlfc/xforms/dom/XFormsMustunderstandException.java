/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.MLFCException;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.content.XSmilesContentInitializeException;



/**
 * XForms binding exception
 * @author Mikko Honkala
 */
public class XFormsMustunderstandException extends XFormsException
{
    public XFormsMustunderstandException(Element elem)
    {
        super("Terminating XForms Exception: \nThis form contains an element: "+elem.getLocalName()+" in namespace:"+elem.getNamespaceURI()+"\n, which is marked with xforms:mustunderstand, but which is not understood by X-Smiles");
    }
    public XFormsMustunderstandException(String reason)
    {
        super(reason);
        throw new RuntimeException("Never call this method: 1001");
    }
    
}
