/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.gui.components.XComponent;

import java.util.Hashtable;


import org.w3c.dom.*;
import org.xml.sax.SAXException;    
    

/**
 * The XForms/Secret element
 * @author Heng Guo, Mikko Honkala 
 */

public class SecretElementImpl extends InputElementImpl {
		
		/**
		 * Constructs a new 'secret' control
		 *
		 * @param my_handler 	The handler for this control
		 * @param my_elem 			The DOM element of this control
		 * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
		 */
		public SecretElementImpl(XFormsElementHandler owner, String ns, String name) {
			super(owner, ns, name);
		}
		/**
		 * Creates the visible containing component of this control
		 */
		public XComponent createComponent()
		{
			textcomponent = this.getComponentFactory().getXSecret('*');
			// TODO: remove?
			textcomponent.setText((String)this.getRefNodeValue());
			return textcomponent;    

		}

}
					

 