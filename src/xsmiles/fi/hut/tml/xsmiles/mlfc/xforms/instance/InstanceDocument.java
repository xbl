/*
 * Created on Mar 15, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import java.util.Vector;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.InstanceElement;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface InstanceDocument extends Document
{
    
    /** get my UI DOM instance element (needed for replace="instance" */
    public void setInstanceElement(InstanceElement inst);
    /** get my UI DOM instance element (needed for replace="instance" */
    public InstanceElement getInstanceElement();
    
    public void setEncoding(String enc);
    
    public Vector validateDocument(boolean isSubmission);
    
    public SchemaPool getSchemaPool();
    
    public void setSchemaPool(SchemaPool pool);
    
    
}
