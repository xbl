/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsLinkException;

import java.io.*;
import java.net.*;

import org.w3c.dom.Node;


/**
 * the interface for pooling or pre-reading schemas
 * @author Mikko Honkala
 * @author Ronald Tschal�r (Patches)
 */
public interface SchemaPool {
	
    /** add schema from URL */
    public void addSchema(String schemaURL) throws XFormsLinkException;
    
    /** add schema from reader */
    public void addSchema(Reader schemaReader, String schemaURL) throws XFormsLinkException;
    
    /** reset pool */
    public void reset();
    
}

