/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.util.Vector;
import java.util.StringTokenizer;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;


import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XSelectMany;


/**
 * The XForms/Checkbox element
 * @author Mikko Honkala
 */

public class SelectManyElementImpl extends SelectElementImpl
{
    protected boolean isInputComponent=true;
    protected XSelectMany selectMany; // this is used if the select is of type LISTBOX
    
    /**
     * Constructs a new XCheckbox
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    public SelectManyElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    
    /**
     * Creates the visual component of this control.
     */
    public XComponent createComponent()
    {
        String style=(String)this.getAttribute(APPEARANCE_ATTRIBUTE);
        if (style==null) style="";
        this.selectMany =  this.getComponentFactory().getXSelectMany(style,false);
        this.select=selectMany;
        return super.createComponent();
    }
    
    /** changes from the instance come here */
    public void changeComponentValue(String newValue)
    {
        // was initiated by us
        if (insideUpdateEvent) return;
        if (selectMany==null) return;
        insideUpdateEvent=true;
        //list.setValueIsAdjusting(true);
        StringTokenizer tokenizer = new StringTokenizer(newValue," ");
        int tokens=tokenizer.countTokens();
        int items[] = new int[tokens];
        int i=0;
        boolean hasOutOfRangeValues=false;
        while (tokenizer.hasMoreTokens())
        {
            String value=tokenizer.nextToken();
            ItemElementImpl item=findItem(value);
            if (item==null)
            {
                Log.error("Unknown value for Select item: "+value);
                hasOutOfRangeValues=true;
                items[i]=-1;
            }
            else items[i]=item.getIndex();
            //this.selectMany.setSelectedIndex(itemval);
            //item.select();
            i++;
        }
        if (!itemsEqual(selectMany.getSelectedIndices(),items))
	    {    
	        this.selectMany.clearSelection();
	        this.selectMany.setSelectedIndices(items);
        }
        //list.setValueIsAdjusting(false)
        
        insideUpdateEvent=false;
        if (hasOutOfRangeValues)
        {
            this.outOfRange=true;
            if (this.componentInited)   
                this.dispatch(XFormsEventFactory.createXFormsEvent(OUTOFRANGE_EVENT));
        }
        else this.outOfRange=false;
    }
    
    protected boolean itemsEqual(int[] i1, int[] i2)
    {
        if (i1==null||i2==null)
        {
            Log.debug("Items in selectmany null.");
            return false;
        }
        if (i1.length!=i2.length) return false;
        for (int i=0;i<i1.length;i++)
        {
            if (!contains(i1[i],i2)) return false;
        }
        return true;
    }
    
    protected boolean contains (int n,int[]a)
    {
        for (int i=0;i<a.length;i++)
        {
            if (n==a[i]) return true;
        }
        return false;
    }
    
    /** the method from ItemLister gets the selection events from the control*/
    public void itemStateChanged(ItemEvent e)
    {
        // if we initiated this change, return immediately
        if (insideUpdateEvent) return;
        // TODO:
                /*
            if (e.getValueIsAdjusting())
                return;
                 */
        // TODO: select events for selectMany do not yet work correctly
        // we always throw select event for all selections
        // and we throw it too early ( the value is not readable from the instance)
        int index[] = this.selectMany.getSelectedIndices();
        StringBuffer newvalue=new StringBuffer();
        boolean complexTypeFoundAndInstanceNodeReseted=false,stringChanged=false;
        InstanceNode instanceElement = this.getRefNode();
        for (int i=0;i<index.length;i++)
        {
            ItemElementImpl item=findItem(index[i]);
            this.userSelected(item);
            // does this element have a copy child and was it created by an itemset
            CopyElementImpl copy = item.getCopyElement();

            if (item.getCreatedByItemset()&&copy!=null)
            {
                if (instanceElement.getNodeType()!=Node.ELEMENT_NODE)
                    this.handleXFormsException(new XFormsBindingException(this,this.getXPath().getExpression(),"Model: "+this.getModelId()+" The node referenced by a select with a copy element inside must be an element."));
                else
                {
                    if (complexTypeFoundAndInstanceNodeReseted!=true)
                    {
                        this.removeChildElements(instanceElement);
                        complexTypeFoundAndInstanceNodeReseted=true;
                    }
                    // append a copy of the referenced node
                    this.appendSubtree(copy,instanceElement);
                }
            }
            else
            {
                if (i>0) newvalue.append(" ");
                newvalue.append(item.getValue());
                stringChanged=true;
            }
        }
        if (stringChanged)
            this.setRefNodeValue(newvalue.toString(),false);
        this.dispatchActivateEvent();
    }
}
