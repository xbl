/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;

import fi.hut.tml.xsmiles.mlfc.xforms.constraint.Vertex;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PropertyInheriter;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.xforms10.*;

// Element implementation 
import org.apache.xerces.dom.AttrNSImpl;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.PSVIAttrNSImpl;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;

/**
 *
 */
public class PSVIInstanceAttrNSImpl extends PSVIAttrNSImpl implements InstanceNode{
	InstanceItem instanceItem;
        
         protected boolean isXSInil = false;
            
	protected PSVIInstanceAttrNSImpl(InstanceDocument ownerDocument, java.lang.String namespaceURI, 
		java.lang.String qualifiedName)  
	{
		super((DocumentImpl)ownerDocument,(namespaceURI==null) ? "":namespaceURI,qualifiedName);
                if (namespaceURI!=null&&this.getLocalName()!=null&&namespaceURI.equals(InstanceItem.XSINS)&&this.getLocalName().equals("nil"))
                    this.isXSInil=true;
		instanceItem=new InstanceItem(this);
	}
	public InstanceItem getInstanceItem()
	{
		return this.instanceItem;
	}
    public PropertyInheriter getPropertyInheriter()
	{
		return this.instanceItem;
	}


	public Object clone()
	{
		try
		{
			PSVIInstanceAttrNSImpl node=(PSVIInstanceAttrNSImpl)super.clone();
			node.instanceItem=new InstanceItem(node);
			return node;
		} catch (Exception e)
		{
			Log.error(e);
			return null;
		}
	}
        
            /** this is overridden to propagate xsi:nil changes to the parent */
    public void setValue(String newvalue) {
        super.setValue(newvalue);
        if (this.isXSInil) 
        {
            Node n = this.getOwnerElement();
            if (n instanceof PSVIInstanceElementImpl)
            {
                ((PSVIInstanceElementImpl)n).notifyXSInilChanged();
            }
        }
    }
	
}

