/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;
import fi.hut.tml.xsmiles.Log;
import java.math.BigDecimal;
//import org.apache.xerces.impl.dv.xs.DecimalDV.MyDecimal;

public class DDecimal extends DData implements Data{

    protected BigDecimal decimalValue;
	public DDecimal(short dtype)
	{
        super(dtype);
	}
	
    /**
     * set the value as a Java Object
     */ 
    protected void setValueFromObjectInternal(Object obj)
    {
        if (obj instanceof BigDecimal)
        {
            decimalValue = (BigDecimal)obj;
            this.setValid(true);
        }
        else 
        {
            try
            {
                decimalValue = new BigDecimal(obj.toString());
            }
            catch (Exception e)
            {
                Log.error(e);
                Log.error(this+" got wrong type of value");
            }
        }
    }
    
    /**
     * set the value from a display string
     */ 
    protected boolean setValueFromDisplayInternal(String displayValue)
    {
        try
        {
            String normalized = displayValue.replace(',','.');
            decimalValue = new BigDecimal(normalized);
            return true;
        }
        catch (NumberFormatException e)
        {
            Log.error("Could not create a new decimal value from user input");
            return false;
        }
    }
    
    /**
     * set the value from a Schema string
     */ 
    public void setValueFromSchemaInternal(String displayValue)
    {
        Log.error("Not implemented");
    }

    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    public String toDisplayValueInternal()
    {
        if (this.decimalValue==null) return "";
        return this.decimalValue.toString();
    }
    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    protected String toSchemaStringInternal()
    {
        return this.decimalValue.toString();
    }
    
    /**
     * get the java object corresponding to the value
     */
    public Object toObjectInternal()
    {
        return this.decimalValue;
    }
    
    /** instructs subclass to set value to null, this is used for
     * invalid values
     */
    protected void clearValue()
    {
        this.decimalValue=null;
    }
} // RangeDecimal
