/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 6, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms;

import java.lang.reflect.Constructor;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ActionElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.AdaptiveControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.AlertElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.BindElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ButtonElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.CaptionElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.CaseElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ChoicesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.CopyElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.CustomControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.DeleteElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.DestroyElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.DispatchElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.DuplicateElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.FileNameElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.GroupElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.HelpElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.HintElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.InsertElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.InstanceElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ItemElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ItemSetElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.LoadURIElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.MediaTypeElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.MessageElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ModelElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.MustUnderstandAttrImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.OutputElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RebuildElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RecalculateElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RefreshElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RepeatElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ResetElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RevalidateElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SchemaElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SecretElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SelectBooleanElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SelectManyElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SelectOneElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SetFocusElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SetIndexElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SetValueElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SubmissionElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SubmitElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SubmitInstanceElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.SwitchElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.TextAreaElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ToggleElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.UploadElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ValueElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;


/**
 * @author honkkis
 *
 */
public class ElementFactory implements XFormsConstants
{
    public static Element createElementNS(XFormsElementHandler owner,String URI,String tagname)
    {
        // TODO: this might be more efficient with a hashtable, since
        // a lot of string comparison is going on. At least put most used elements on top
        try
        {
            String localname = getLocalname(tagname);
            XFormsElementImpl elem = null;
            // CAPTION
            if (localname.equals(CAPTION_ELEMENT))
                elem = new CaptionElementImpl(owner,URI,tagname);
            // FORM CONTROLS
            // Adaptive controls
            else if (
            localname.equals(INPUT_ELEMENT)
            ||localname.equals(RANGE_ELEMENT)
            )
                elem = new AdaptiveControl(owner,URI,tagname);
            else if (localname.equals(BUTTON_ELEMENT))
                elem = new ButtonElementImpl(owner,URI,tagname);
            else if (localname.equals(OUTPUT_ELEMENT))
                elem = new OutputElementImpl(owner,URI,tagname);
            else if (localname.equals(SUBMIT_ELEMENT))
                elem = new SubmitElementImpl(owner,URI,tagname);
            //else if (localname.equals(OUTPUT_ELEMENT))
            //	elem = new OutputElementImpl(owner,URI,tagname);
            else if (localname.equals(SECRET_ELEMENT))
                elem = new SecretElementImpl(owner,URI,tagname);
            else if (localname.equals(TEXTAREA_ELEMENT))
                elem = new TextAreaElementImpl(owner,URI,tagname);
            else if (localname.equals(UPLOAD_ELEMENT))
                elem = new UploadElementImpl(owner,URI,tagname);
            //else if (localname.equals(RANGE_ELEMENT))
            //	elem = new RangeElementImpl(owner,URI,tagname);
            else if (localname.equals(SELECT_ONE_ELEMENT))
                elem = new SelectOneElementImpl(owner,URI,tagname);
            else if (localname.equals(SELECT_MANY_ELEMENT))
                elem = new SelectManyElementImpl(owner,URI,tagname);
            else if (localname.equals(SELECT_BOOLEAN_ELEMENT))
                elem = new SelectBooleanElementImpl(owner,URI,tagname);
            else if (localname.equals(MODEL_ELEMENT))
                elem = new ModelElementImpl(owner,URI,tagname);
            else if (localname.equals(BIND_ELEMENT))
                elem = new BindElementImpl(owner,URI,tagname);
            else if (localname.equals(CHOICES_ELEMENT))
                elem = new ChoicesElementImpl(owner,URI,tagname);
            else if (localname.equals(ITEMSET_ELEMENT))
                elem = new ItemSetElementImpl(owner,URI,tagname);
            else if (localname.equals(ITEM_ELEMENT))
                elem = new ItemElementImpl(owner,URI,tagname);
            else if (localname.equals(VALUE_ELEMENT))
                elem = new ValueElementImpl(owner,URI,tagname);
            else if (localname.equals(COPY_ELEMENT))
                elem = new CopyElementImpl(owner,URI,tagname);
            else if (localname.equals(HINT_ELEMENT))
                elem = new HintElementImpl(owner,URI,tagname);
            else if (localname.equals(SCHEMA_ELEMENT))
                elem = new SchemaElementImpl(owner,URI,tagname);
            else if (localname.equals(INSTANCE_ELEMENT))
                elem = new InstanceElementImpl(owner,URI,tagname);
            else if (localname.equals(SUBMIT_INFO_ELEMENT))
                elem = new SubmissionElementImpl(owner,URI,tagname);
            else if (localname.equals(REPEAT_ELEMENT))
                elem = new RepeatElementImpl(owner,URI,tagname);
            else if (localname.equals(SWITCH_ELEMENT))
                elem = new SwitchElementImpl(owner,URI,tagname);
            else if (localname.equals(CASE_ELEMENT))
                elem = new CaseElementImpl(owner,URI,tagname);
            else if (localname.equals(GROUP_ELEMENT))
                elem = new GroupElementImpl(owner,URI,tagname);
            // ACTIONS
            else if (localname.equals(ACTION_ELEMENT))
                elem = new ActionElementImpl(owner,URI,tagname);
            else if (localname.equals(SETVALUE_ELEMENT))
                elem = new SetValueElementImpl(owner,URI,tagname);
            else if (localname.equals(TOGGLE_ELEMENT))
                elem = new ToggleElementImpl(owner,URI,tagname);
            else if (localname.equals(SUBMIT_INSTANCE_ELEMENT))
                elem = new SubmitInstanceElementImpl(owner,URI,tagname);
            else if (localname.equals(INSERT_ELEMENT))
                elem = new InsertElementImpl(owner,URI,tagname);
            else if (localname.equals(DELETE_ELEMENT))
                elem = new DeleteElementImpl(owner,URI,tagname);
            else if (localname.equals(MESSAGE_ELEMENT))
                elem = new MessageElementImpl(owner,URI,tagname);
            else if (localname.equals(MEDIA_TYPE_ELEMENT))
                elem = new MediaTypeElementImpl(owner,URI,tagname);
            else if (localname.equals(FILENAME_ELEMENT))
                elem = new FileNameElementImpl(owner,URI,tagname);
            else if (localname.equals(LOADURI_ELEMENT))
                elem = new LoadURIElementImpl(owner,URI,tagname);
            else if (localname.equals(SET_FOCUS_ELEMENT))
                elem = new SetFocusElementImpl(owner,URI,tagname);
            else if (localname.equals(SET_INDEX_ELEMENT))
                elem = new SetIndexElementImpl(owner,URI,tagname);
            else if (localname.equals(RECALCULATE_ELEMENT))
                elem = new RecalculateElementImpl(owner,URI,tagname);
            else if (localname.equals(RESET_ELEMENT))
                elem = new ResetElementImpl(owner,URI,tagname);
            else if (localname.equals(HELP_ELEMENT))
                elem = new HelpElementImpl(owner,URI,tagname);
            else if (localname.equals(ALERT_ELEMENT))
                elem = new AlertElementImpl(owner,URI,tagname);
            else if (localname.equals(REVALIDATE_ELEMENT))
                elem = new RevalidateElementImpl(owner,URI,tagname);
            else if (localname.equals(REBUILD_ELEMENT))
                elem = new RebuildElementImpl(owner,URI,tagname);
            else if (localname.equals(REFRESH_ELEMENT))
                elem = new RefreshElementImpl(owner,URI,tagname);
            else if (localname.equals(DISPATCH_ELEMENT))
                elem = new DispatchElementImpl(owner,URI,tagname);
            else if (localname.equals(DUPLICATE_ELEMENT))
                elem = new DuplicateElementImpl(owner,URI,tagname);
            else if (localname.equals(DESTROY_ELEMENT))
                elem = new DestroyElementImpl(owner,URI,tagname);
            else if (localname.equals(TREE_ELEMENT)) // TODO: use reflection
                elem = createFormControl(owner,URI,tagname,"fi.hut.tml.xsmiles.mlfc.xforms.dom.swing.TreeElementImpl");
                //elem = new TreeElementImpl(owner,URI,tagname);
            else if (localname.equals("customcontrol"))
                elem = new CustomControl(owner,URI,tagname);
            else
            {
                Log.error("Unknown XForms element: "+tagname);
            }
            return elem;
            //Log remove this catch?
        } catch (RuntimeException e)
        {
            Log.error(e);
            throw(e);
        }
    }
    
    static Class[] paramtypes = {XFormsElementHandler.class,String.class,String.class};;
    public static XFormsElementImpl createFormControl(XFormsElementHandler owner,String URI,String tagname,String classname)
    {
        try
        {
            Class c = Class.forName(classname);
            if (c !=null)
            {
                Constructor constr = c.getConstructor(paramtypes);
                if (constr!=null)
                {
                    Object[] params = {owner,URI,tagname};
                    Object newo = constr.newInstance(params);
                    if (newo!=null&&newo instanceof XFormsElementImpl) return (XFormsElementImpl)newo;
                }
                
            }
        } catch (Throwable t)
        {
            Log.error("Could not instantiate: "+classname+" reverting to label element.");
        }
        return new CaptionElementImpl(owner,URI,tagname);
    }
    
    public static String getLocalname(String tagname)
    {
        int index = tagname.indexOf(':');
        if (index<0) return tagname;
        return  tagname.substring(index+1);
    }

    
    /**
     * Create a DOM attribute.
     */
    public static Attr createAttributeNS(DocumentImpl doc,String namespaceURI, String qualifiedName,XFormsElementHandler owner)
    throws DOMException
    {
        String localname = getLocalname(qualifiedName);
        if (localname.equals("mustUnderstand")) return new MustUnderstandAttrImpl(doc,namespaceURI,qualifiedName,owner);
        return null;
    }
}
