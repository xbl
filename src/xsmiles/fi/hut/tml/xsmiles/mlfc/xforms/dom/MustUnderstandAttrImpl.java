/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.dom.XSmilesAttrNSImpl;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.content.XSmilesContentInitializeException;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.w3c.dom.xforms10.*;

import org.apache.xerces.dom.*;



/**
 * With this attribute ev:event, it is possible to define event handlers
 * without the listener element.
 * @author Mikko Honkala
 */
public class MustUnderstandAttrImpl extends XSmilesAttrNSImpl
{
    
    protected XFormsElementHandler handler;
    
    public MustUnderstandAttrImpl(DocumentImpl ownerDocument, java.lang.String namespaceURI, java.lang.String qualifiedName, XFormsElementHandler a_handler)
    {
        super(ownerDocument,namespaceURI,qualifiedName);
        this.handler = a_handler;
    }
    
    public void init()
    {
        Element parent = this.getOwnerElement();
        if (parent!=null)
        {
            if (!this.handler.understandElement(parent))
            {
                this.handleXFormsException( new XFormsMustunderstandException(parent) );
            }
        }
        
        
    }
    /** handle any XFormsException that's dispatched for this element */
    public void handleXFormsException(XFormsException e)
    {
        Log.error(e);
        throw new XSmilesContentInitializeException(e.toString());
    }
    
}

