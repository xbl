/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import java.awt.event.*;
import org.w3c.dom.Element;


public class InputString extends AbstractControl implements TextListener,FocusListener, ActionListener
{
    
    protected XInput fInput;
    public InputString(XFormsContext context,Element elem)
    {
        super(context,elem);
        this.createControl();
        this.registerListener();
    }
    /** creates the control with the component factory */
    protected void createControl()
    {
        fInput = this.fContext.getComponentFactory().getXInput();
        // Enable following when CSS borders work for input::value
        //fInput.removeBorders();
    }
    /**
     * returns the abstract component for this control. The
     * abstract component can be used e.g. to style the component
     * but all listeners should be added to the AdaptiveControl and not directly
     * to the XComponent */
    public XComponent getComponent()
    {
        return fInput;
    }
    /**
     * get the components current value
     */
    public Data getValue()
    {
        if (getData()==null||fInput==null) return null;
        getData().setValueFromDisplay(fInput.getText());
        return getData();
    }
    /** internal method for setting the listener for the component */
    protected void registerListener()
    {
        fInput.addTextListener(this);
        fInput.addFocusListener(this);
        fInput.addActionListener(this);
    }
    
    public void destroy()
    {
        fInput.removeTextListener(this);
        fInput.removeFocusListener(this);
        fInput.removeActionListener(this);
        super.destroy();
    }
    /**
     * set the components value
     *
     */
    public void updateDisplay()
    {
        if (!insideEvent)
        {
            insideEvent=true;
            fInput.setText((String)getData().toDisplayValue());
            insideEvent=false;
        }
    }
    // FOCUS LISTENER METHODS
    public void focusGained(FocusEvent ae)
    {
    }
    public void focusLost(FocusEvent ae)
    {
        insideEvent=true;
        this.fChangeListener.valueChanged(false,null);
        insideEvent=false;
    }
    
    /**
     * This notifies the text control to save its value before rewiring
     */
    public void rewiringAboutToHappen()
    {
        this.fChangeListener.valueChanged(false,null);
    }

    
    // TEXT LISTENER METHODS
    public void textValueChanged(TextEvent e)
    {
        insideEvent=true;
        if (this.fChangeListener!=null)
            this.fChangeListener.valueChanged(true,null);
        /*
        String text = fInput.getText();
        if (this.fChangeListener!=null)
                this.fChangeListener.valueChanged(text, true);
         */
        insideEvent=false;
    }
    public void notifyIncrementalChange(TextEvent e)
    {
        this.textValueChanged(e);
    }
    
    /** Invoked when an action occurs.
     * User has pressed enter in the field
     */
    public void actionPerformed(ActionEvent e)
    {
        Log.debug("InputString : action performed");
        insideEvent=true;
        if (this.fChangeListener!=null)
            this.fChangeListener.valueChanged(false,null);
        this.activate(e);
        insideEvent=false;
    }
    
} // InputBoolean
