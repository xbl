/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.mlfc.xforms.data.XFormsDatatypeException;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.adaptive.Control;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;




/**
 * The XForms datatype aware adaptive control (e.g. input/range) element
 * @author Mikko Honkala
 */

public class AdaptiveControl extends XFormsControl 
    implements XChangeListener, ActionListener, TypedElement
{
    //protected boolean isInputComponent=true;
    //protected XControl select;
    protected Control control;
    
    /**
     * Constructs a new select boolean
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    
    public AdaptiveControl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public XComponent createComponent()
    {
        int datatypeId = this.getDatatypeId();
        try
        {
            control = this.getHandler().getAdaptiveControlFactory().createControl(
            this.getHandler(),this.getLocalName(),datatypeId,this);
            XComponent comp = control.getComponent();
            if (this.getBindingState()==DynBoundElementImpl.BINDING_OK)
            {
                InstanceItem item = retrieveInstanceItem(this.getRefNode());
                //Data data = item.getData();
                //this.control.setData(data);
                this.changeComponentValue(null);
            }
            //control.setValueFromObject((String)this.getRefNodeValue());
            return comp;
        } catch (XFormsDatatypeException e)
        {
            this.handleXFormsException(e);
        }
        return null;
    }
    public String getOutputValue()
    {
        // TODO: <output value>
        if (this.getRefNode()==null) return null;
        InstanceItem item = retrieveInstanceItem(this.getRefNode());
        if (item==null) return null;
        Data data = item.getData();
        return data.toDisplayValue();
    }
    /** the refresh processing notifies, that the binding might have changed, and it needs to be re-evaluated */
    public void bindingMaybeDirty()
    {
    	// this is a kludge. For some reason, when the focus is in a input 
    	// control, and the user clicks something that makes that control 
    	// rewire, the focus lost event becomes latest, and then the control 
    	// has already been rewired, thus losing the current value. Note that this is only for controls where text is typed in textarea and input:string.
    	if (this.control!=null) this.control.rewiringAboutToHappen(); 
    	super.bindingMaybeDirty();
    }


    protected void registerListener()
    {
        this.control.addChangeListener(this);
        this.control.addActionListener(this);
        super.registerListener();
    }
    public void destroy()
    {
        if (this.control!=null)
        {
            this.control.destroy();
            this.control=null;
        }
        super.destroy();
    }
    /** this comes from the instance */
    public void changeComponentValue(String newValue)
    {
        if (insideUpdateEvent) return;
        insideUpdateEvent = true;
        //Log.debug("changeCompValue: "+newValue);
        if (control!=null)
            this.control.updateDisplay();
        insideUpdateEvent = false;
    }
    /**
     * ChangeListener method that notifies that the user has changed the value of the control.
     * NOTE that this looks at the control to get the current value
     * @param newValue A schema compatible string containing the new value
     * @param newObjValue The new value as a Java object
     */
    public void valueChanged(boolean valueChanging, Object src)
    {
        // if not valuechanging and not incremental at the same time
        if (insideUpdateEvent || this.control==null) return;
        if (!(valueChanging && !this.incremental))
        {
            insideUpdateEvent = true;
            Object newValue = this.control.getValue();
            if (newValue instanceof String)
            {
            	if (!this.getRefNodeValue().equals((String)newValue))
            		this.setRefNodeValue((String)newValue, valueChanging);
            }
            else if (newValue instanceof Data)
            {
                Data data = (Data)newValue;
                String strVal;
                strVal = data.toSchemaString();
                //if (data.isValid()) strVal = data.toSchemaString();
                //else strVal = data.getInvalidValue();
            	if (!this.getRefNodeValue().equals(strVal))
            		this.setRefNodeValue(strVal,valueChanging);
            }
        }
        insideUpdateEvent = false;
    }
    /** notifies the listener that the binding and the value changed  */
    public void adaptiveBindingChanged()
    {
        //Log.debug("BINDING HAS CHANGED FOR AdaptiveControl"+this);
        // TODO: empty nodeset
        if (this.getRefNode()==null)
        {
            Log.debug("New ref node is null: "+this.getCaptionText());
        }
        else if (this.init==false)
        {
            //Log.debug("new refnode: "+(String)this.getRefNodeValue());
            if (control!=null)
            {
                //this.control.setData(retrieveInstanceItem(this.getRefNode()).getData());
                this.control.updateDisplay();
            }
            
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement#isInputControl()
     */
    public boolean isInputControl()
    {
    	if (this.control!=null)
    		return this.control.isInputControl();
    	else return false;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement#getDataType()
     */
    public short getDataType()
    {
        // TODO Auto-generated method stub
        return this.control.getDataType();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement#getData()
     */
    public Data getData()
    {
        // TODO Auto-generated method stub
    	if (this.control!=null)
        return this.control.getData();
    	else return null;
    }

    /*
     * This is called for instance by the speech dialog to set the value of the control
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement#setData(fi.hut.tml.xsmiles.mlfc.xforms.data.Data)
     */
    public void setData(Data data)
    {
        this.control.setData(data);
        this.control.updateDisplay(); // why do we have to call all these 3 functions?
        this.valueChanged(false,data);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.FormControl#isWritable()
     */
    public boolean isWritable()
    {
        if (this.control!=null ) return this.control.isWritable();
        return false;
    }
    
}
