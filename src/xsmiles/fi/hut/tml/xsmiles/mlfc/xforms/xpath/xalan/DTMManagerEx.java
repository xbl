
//package org.apache.xpath;
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;


import org.apache.xml.dtm.ref.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import org.apache.xml.dtm.DTM;
import org.apache.xml.dtm.DTMException;
import org.apache.xml.dtm.DTMFilter;
import org.apache.xml.dtm.DTMIterator;
import org.apache.xml.dtm.DTMConfigurationException;
import org.apache.xml.dtm.DTMManager;
import org.apache.xml.dtm.DTMWSFilter;
import org.apache.xml.dtm.ref.dom2dtm.DOM2DTM;
import org.apache.xml.dtm.ref.sax2dtm.SAX2DTM;
import org.apache.xml.dtm.ref.sax2dtm.SAX2RTFDTM;
//import org.apache.xml.res.XMLErrorResources;
//import org.apache.xml.res.XMLMessages;
import org.apache.xml.utils.PrefixResolver;
import org.apache.xml.utils.SystemIDResolver;
import org.apache.xml.utils.XMLStringFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.xml.sax.InputSource;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import fi.hut.tml.xsmiles.Log;

import java.util.Vector;


public class DTMManagerEx extends DTMManagerDefault
{
    
    public static DTMManager newInstance(XMLStringFactory xsf)
    throws DTMConfigurationException
    {
        
        DTMManager factoryImpl = null;
        factoryImpl = (DTMManager) new DTMManagerEx();
        
        factoryImpl.setXMLStringFactory(xsf);
        
        return factoryImpl;
    }
    /**
     * Default constructor is protected on purpose.
     */
    /*
  protected DTMManagerEx()
  {
      super();
      Log.debug("DTMManagerEx created");
  }*/
    
    /** X-SMiles: the referants of XPath expressions are collected here */
    protected Vector referants = null;
    
    /** should this instance track the referants */
    protected boolean trackReferants = false;
    
    public void setReferantTracking(boolean r)
    {
        this.trackReferants=r;
    }
    
    public boolean getReferantTracking()
    {
        return this.trackReferants;
    }
    
    /** The DOM2DTMEx calls this method to add a new referant */
    public void addReferant(Node n)
    {
        if (referants==null) referants = new Vector(40);
        referants.addElement(n);
        //Log.debug("Referant added. Count:"+referants.size());
    }
    protected Vector referantFunctions;
    /** The DOM2DTMEx calls this method to add a new referant */
    public void addReferantFunction(String function)
    {
        if (referantFunctions==null) referantFunctions = new Vector(40);
        referantFunctions.addElement(function);
        //Log.debug("Referant function added. Count:"+referantFunctions.size());
    }
    
    public Vector getReferants()
    {
        if (referants==null) referants = new Vector(40);
        return this.referants;
    }
    public Vector getReferantFunctions()
    {
        if (referants==null) referantFunctions = new Vector(40);
        return this.referantFunctions;
    }

    
    public void clearReferants()
    {
        if (referants!=null) this.referants.removeAllElements();
        if (referantFunctions!=null) this.referantFunctions.removeAllElements();
    }
    public void detatchReferants()
    {
        this.referants=null;
        this.referantFunctions=null;
    }
    /**
     * This method is copied from DTMManagerDefault in Xalan 2.5.1, in order to
     * generate DOM2DTMEx objects for referent tracking purposes
     *
     * Get an instance of a DTM, loaded with the content from the
     * specified source.  If the unique flag is true, a new instance will
     * always be returned.  Otherwise it is up to the DTMManager to return a
     * new instance or an instance that it already created and may be being used
     * by someone else.
     *
     * A bit of magic in this implementation: If the source is null, unique is true,
     * and incremental and doIndexing are both false, we return an instance of
     * SAX2RTFDTM, which see.
     *
     * (I think more parameters will need to be added for error handling, and entity
     * resolution, and more explicit control of the RTF situation).
     *
     * @param source the specification of the source object.
     * @param unique true if the returned DTM must be unique, probably because it
     * is going to be mutated.
     * @param whiteSpaceFilter Enables filtering of whitespace nodes, and may
     *                         be null.
     * @param incremental true if the DTM should be built incrementally, if
     *                    possible.
     * @param doIndexing true if the caller considers it worth it to use
     *                   indexing schemes.
     *
     * @return a non-null DTM reference.
     */
    synchronized public DTM getDTM(Source source, boolean unique,
    DTMWSFilter whiteSpaceFilter,
    boolean incremental, boolean doIndexing)
    {
/*
    if(DEBUG && null != source)
      System.out.println("Starting "+
                         (unique ? "UNIQUE" : "shared")+
                         " source: "+source.getSystemId()
                         );
 */
        XMLStringFactory xstringFactory = m_xsf;
        int dtmPos = getFirstFreeDTMID();
        int documentID = dtmPos << IDENT_DTM_NODE_BITS;
        
        if ((null != source) && source instanceof DOMSource)
        {
            DOM2DTM dtm = new DOM2DTMEx(this, (DOMSource) source, documentID,
            whiteSpaceFilter, xstringFactory, doIndexing);
            
            addDTM(dtm, dtmPos, 0);
            
            //      if (DUMPTREE)
            //      {
            //        dtm.dumpDTM();
            //      }
            
            return dtm;
        }
        else
        {
            return super.getDTM(source,unique,whiteSpaceFilter,incremental,doIndexing);
        }
    }
}
