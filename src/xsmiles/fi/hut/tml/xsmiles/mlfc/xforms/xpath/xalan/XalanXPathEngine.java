/*
 * Created on Apr 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;

import java.util.Vector;

import javax.xml.transform.TransformerException;

import org.apache.xpath.objects.XBoolean;
import org.apache.xpath.objects.XNodeSet;
import org.apache.xpath.objects.XNumber;
import org.apache.xpath.objects.XObject;
import org.apache.xpath.objects.XString;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class XalanXPathEngine implements XPathEngine {

    // install extension functions (note: look also at XFormsContextEx,
    // for namespace prefixed functions
    static
    {
        ExtensionFunctions.installFunctions();
    }
    CachedXPathAPIEx xpathAPI;
    XFormsContext xformsContext;
    ModelContext modelContext;
    
    /** the cached XPath Lookup api context */
    protected XPathLookup xpathLookup;

    public XalanXPathEngine()
    {
        
    }
    public void setContext(XFormsContext handler,ModelContext mcontext)
    {
        this.xformsContext=handler;
        this.modelContext=mcontext;
    }
    
    public boolean hasFunction(String uri, String funcName)
    {
        return ExtensionFunctions.hasFunction(uri,funcName);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#createXPathExpression(java.lang.String)
     */
    public XPathExpr createXPathExpression(String xpath) {
        return new XPathExpression(xpath);
    }

    /* 
     *     evaluates an XPath expression against  a context 
     *  If the expression evaluates to a single primitive (String, Number or Boolean) type, it is returned directly. 
     * Otherwise, the returned value is a List (a node-set, in the terms of the specification) of values.
     * 
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#eval(org.w3c.dom.Node, fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr, org.w3c.dom.Node, org.w3c.dom.NodeList)
     */
    public XObject evalInternal(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList) throws Exception
    {
        if (contextNode==null) contextNode=this.modelContext.getInstanceDocument(null).getDocumentElement();
        if (xpathAPI==null) xpathAPI=new CachedXPathAPIEx(xformsContext,modelContext);
        String xpath = ((XPathExpression)expr).getExpression();
        XObject x=null;
        if (contextList!=null)
            x = xpathAPI.eval(contextNode,xpath,namespaceNode,contextList);
        else
            x = xpathAPI.eval(contextNode,xpath,namespaceNode);
        
        if (XFormsElementHandler.debugXForms) Log.debug("ModelElementImpl : XPath executed: "+xpath);
        return x;
        
    }
    public Object eval(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList) throws Exception {
        XObject x = evalInternal(contextNode,expr,namespaceNode,contextList);
        return convertToJavaType(x);
    }
    public String evalToString(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList) throws Exception
    {
        XObject x = evalInternal(contextNode,expr,namespaceNode,contextList);
        return x.str();
        
    }

    
    protected Object convertToJavaType(XObject obj)
    {
        Object ret=null;
        try
        {
	        if (obj instanceof XNumber)
	        {
	            ret = new Double(obj.num());
	        } 
	        else if (obj instanceof XBoolean)
	        {
	            ret = new Boolean(obj.bool());
	        }
	        else if (obj instanceof XString)
	        {
	            ret = new Boolean(obj.str());
	        }
	        else if (obj instanceof XNodeSet)
	        {
	            ret = obj.nodelist();
	        }
        } catch (TransformerException e)
        {
            Log.error(e);
        }
        return ret;
        // TODO: cast to nodelist or string, depending on the result
    }
    
    public NodeList evalToNodelist(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList) throws Exception
    {
        XObject ret = (XObject)this.evalInternal(contextNode,expr,namespaceNode,contextList);
        if (ret==null) return null;
        Object o =  ret.nodelist();
        if (o instanceof Node)
        {
            // wrap to nodelist
            Log.debug("Nodelist was a node...");
            
            return new NodeNodeList((Node)o);
        }
        else return (NodeList)o;
    }
    
    private class NodeNodeList implements NodeList 
    {
        Node myNode;
        public NodeNodeList(Node n)
        {
            myNode=n;
        }

        /* (non-Javadoc)
         * @see org.w3c.dom.NodeList#getLength()
         */
        public int getLength()
        {
            // TODO Auto-generated method stub
            return 1;
        }

        /* (non-Javadoc)
         * @see org.w3c.dom.NodeList#item(int)
         */
        public Node item(int index)
        {
            // TODO Auto-generated method stub
            return myNode;
        }
        
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#evalWithTrace(org.w3c.dom.Node, fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr, org.w3c.dom.Node, org.w3c.dom.NodeList, fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult)
     */
    public Object evalWithTrace(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList, LookupResult result) throws Exception {
        // TODO Auto-generated method stub
        XPathLookup lookup=this.getXPathLookup();
        Vector xpathRefNodes=null;
        lookup.setModelContext(modelContext);
        lookup.evalWithTrace(contextNode,((XPathExpression)expr).getExpression(),namespaceNode,result);
        return null;
    }
    
    public XPathLookup getXPathLookup()
    {
        if (this.xpathLookup==null) this.xpathLookup=new XPathLookup();
        return this.xpathLookup;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#getModelContext()
     */
    public ModelContext getModelContext() {
        return this.modelContext;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#getXFormsContext()
     */
    public XFormsContext getXFormsContext() {
        return this.xformsContext;
    }

}
