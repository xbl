/*
 * Created on Apr 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath;

/**
 * @author honkkis
 *
* An object holding a parsed XPath Expression that can be cached
 */
public interface XPathExpr {
    /** do not use this to evaluate, will be inefficient! */
    public String getExpression();

}
