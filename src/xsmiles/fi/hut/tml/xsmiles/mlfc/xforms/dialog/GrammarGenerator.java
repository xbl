/*
 * /* X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources. Created on Jul 9, 2004
 *  
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.Enumeration;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.LabeledElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectionItem;
import fi.hut.tml.xsmiles.util.StringUtils;

/**
 * @author honkkis
 *  
 */
public class GrammarGenerator
{
    //public static String testGrammar = "#JSGF V1.0;\n\ngrammar hello;\npublic
    // <greet> = (Good morning | Hello) ( Bhiksha | Evandro | Paul | Philip |
    // Rita | Will );\n";

    public static final String gStart = "#JSGF V1.0;\n\ngrammar hello;\npublic <greet> = ";
    public static final String gEnd = ";\n";

/*    public static Reader GenerateSimpleGrammarReader(Enumeration e)
    {
        return new StringReader(GenerateSimpleGrammar(e, true));
    }
*/
    public static void GenerateSimpleGrammar(Enumeration e, Grammar grammar)
    {
        String rule = "<simple> = (";
        boolean first = true;
        while (e.hasMoreElements())
        {
            Object o = e.nextElement();
            String item = null;
            //if (o instanceof FocusTreeNode)
            // item=((FocusTreeNode)o).getLabel();
            if (o instanceof SelectionItem)
                item = ((SelectionItem) o).getLabel();
            else if (o instanceof LabeledElement)
                item = ((LabeledElement) o).getLabelAsText();
            else if (o instanceof String)
                item = (String) o;
            else if (o instanceof Element) item=BaseSpeechWidget.getLabelForSpeaking((Element)o,true);
            if (item != null)
            {
                item=item.replace('.',' '); // dot ('.') is not allowed in grammar
                item=item.replace('?',' '); // dot ('.') is not allowed in grammar
                item=item.replace(':',' '); // dot ('.') is not allowed in grammar
                item=replaceNumbersWithStrings(item);
                if (!first)
                    rule += "|";
                rule += item;
            }
            first = false;
        }
        rule += ");";
        
        String header = "<simple>";
        grammar.setRule(rule, header);
    }
    
    public static String replaceNumbersWithStrings(String orig)
    {
        // TODO: very inefficient, go through each char and replace that way only once the string
       String ret = StringUtils.replace(orig,"1","one");
       ret = StringUtils.replace(ret,"2","two");
       ret = StringUtils.replace(ret,"3","three");
       ret = StringUtils.replace(ret,"4","four");
       ret = StringUtils.replace(ret,"5","five");
       ret = StringUtils.replace(ret,"6","six");
       ret = StringUtils.replace(ret,"7","seven");
       ret = StringUtils.replace(ret,"8","eight");
       ret = StringUtils.replace(ret,"9","nine");
       return ret.trim();
    }

    public static void generateIntegerGrammar(Grammar grammar)
    {
        //String numbers = "<Numbers> = (oh | zero | one | two | three | four | five | six | seven | eight | nine) * | ten | eleven | twelve;";
        String imp = "import <cfg.numbers.Numbers>;";
        String header = "<Numbers>";
        //grammar.setRule(numbers, header);
        grammar.setImport(imp, header);
    }
    
    public static void generateDateGrammar(Grammar grammar)
    {
        //String rule = "<date> = (january | february| march | april | may | june | july | august | september | october | november | december) + (one | two | three | four | five | six | seven | eight | nine | ten | eleven | twelve) + (two thousand [and] (one | two));";
        String imp = "import <cfg.date.Date>;";
        String header = "<Date>";
        //grammar.setRule(rule, header);
        grammar.setImport(imp, header);
    }
    
    public static void generateStringGrammar(Grammar grammar)
    {
        String imp = "import <cfg.letters.Letters>;";
        String header = "<Letters>";
        grammar.setImport(imp, header);        
    }

    public static String addHeader(Enumeration strings)
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append(gStart);
        while (strings.hasMoreElements())
        {
            //buffer.append('(');
            String o = (String) strings.nextElement();
            buffer.append(o);
            //buffer.append(')');
        }
        buffer.append(gEnd);
        return buffer.toString();
    }

}