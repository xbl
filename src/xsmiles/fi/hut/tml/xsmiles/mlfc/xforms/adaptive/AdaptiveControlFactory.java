/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.gui.components.XCalendar;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.data.XFormsDatatypeException;
import org.w3c.dom.Element;



public class AdaptiveControlFactory implements XFormsConstants {

    //public static final String SCHEMA_BOOLEAN = "xsd:boolean";
    //protected XFormsContext fContext;
    
    DataFactory dfact;
    

    public AdaptiveControlFactory()

    {
        dfact=XFormsConfiguration.getInstance().getDataFactory();
	}
    
    
    /**
     * Create an adaptive control of a specific type and datatype,
     * for instance createControl("input", "xsd:boolean") would create
     * an input control for boolean, namely a checkbox.
     * @attribute context used for creating controls
     * @attribute type the local name of the control
     * @attribute datatypeId the xerces specific datatype id
     * @attribute elem the element for requesting further information (e.g. the 
     * 'step' attribute for the range control
     */
    public Control createControl(XFormsContext context,String type,
        int datatypeId, Element elem) throws XFormsDatatypeException
    {
        if (type.equals(INPUT_ELEMENT))
            return this.createInputControl(context,datatypeId,elem);
        else if (type.equals(OUTPUT_ELEMENT))
            return this.createOutputControl(context,datatypeId,elem);
        else if (type.equals(RANGE_ELEMENT))
            return this.createRangeControl(context,datatypeId,elem);
        return null;
    }
    /** create an input control*/
    public AbstractControl createInputControl(XFormsContext context,
        int datatypeId, Element elem) throws XFormsDatatypeException
    {
            if (datatypeId == dfact.PRIMITIVE_BOOLEAN)
                return new InputBoolean(context,elem);
            else if ((datatypeId == dfact.PRIMITIVE_DATE) && 
            	(context.getComponentFactory().hasExtension(XCalendar.class)))
            {
                    return new InputDate(context,elem);
            }
            // All other types uses the following class
            // note that data.Data is used for display - schema conversion
            else if (datatypeId==dfact.PRIMITIVE_HEXBINARY||datatypeId==dfact.PRIMITIVE_BASE64BINARY)
                throw new XFormsDatatypeException(elem,"Input does not bind to xsd:hexBinary or xsd:base64Binary.");
            else return new InputString(context,elem);
    }
    AbstractControl createOutputControl(XFormsContext context,int datatypeId, 
        Element elem) throws XFormsDatatypeException
    {
            return new OutputAny(context,elem);
    }	
    AbstractControl createRangeControl(XFormsContext context,int datatypeId, 
        Element elem) throws XFormsDatatypeException
    {
            if (datatypeId == dfact.PRIMITIVE_DECIMAL)
                return new RangeDecimal(context,elem);
            else if (datatypeId == dfact.XSMILES_INTEGER)
                return new RangeInteger(context,elem);
            return //new RangeOther(context,elem);
                createInputControl(context, datatypeId, elem);
            // WAS: All other types throw an exception
            //throw new XFormsDatatypeException(elem,"Range binds only to sequential datatypes.");
    }	
} // AdaptiveControlFactory
