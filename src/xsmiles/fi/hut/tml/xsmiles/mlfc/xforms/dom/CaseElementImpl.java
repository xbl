/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.dom.FlowLayoutElement;

   

/**
 * The case element.
 * @author Mikko Honkala
 */


public class CaseElementImpl extends XFormsElementImpl implements FlowLayoutElement {

	protected boolean active = false;
	public CaseElementImpl(XFormsElementHandler owner, String ns, String name)
	{
		super(owner, ns, name);
	}

	/** 
	 */
	public void init()
	{
		this.getHandler().addCase(this);
		super.init();
	}
        
        public void setActive(boolean a)
        {
            this.active = a;
            styleChanged();
        }
        
            /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        //Log.debug("isPseudoClass : "+pseudoclass+" this.active:"+this.active );
        if (pseudoclass.equals(ACTIVE_CASE_PSEUDOCLASS))
        {
            // return true, if we are currently disabled (irrelevant)
            return this.active;
        }
        else if (pseudoclass.equals(INACTIVE_CASE_PSEUDOCLASS))
        {
            // return true, if we are currently enabled (relevant)
            //Log.debug("Case: active: "+this.active);
            return !this.active;
        }
        else return super.isPseudoClass(pseudoclass);
    }
	
}


