/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 31, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;


/**
 * @author honkkis
 *
 */
public interface XFormsModelElement
{
    XPathEngine getXPathEngine();
    Document getInstanceDocument(String id);
    Enumeration getInstanceDocuments();
}
