/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 25, 2005
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;


public interface RepeatItem
{
    // the index of this repeatItem
    public int getIndex();

}
