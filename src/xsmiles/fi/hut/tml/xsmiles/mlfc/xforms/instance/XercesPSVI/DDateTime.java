/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.*;

import java.util.Calendar;
import java.util.Date;

import org.apache.xerces.impl.dv.xs.DateTimeDV;
import org.apache.xerces.impl.dv.xs.DateTimeDVEx;

// for display string
import java.util.Locale; 
import java.text.DateFormat;

public class DDateTime extends DDate implements Data{

	public DDateTime(short dtype)
	{
        super(dtype);
	}
	
    public DDateTime()
    {
        super(DataFactory.PRIMITIVE_DATETIME_STATIC);
    }
    
    /**
     * set the value from a Schema string
     */ 
    protected void setValueFromSchemaInternal(String schemaValue)
    {
        //Log.error("Not implemented");
        try
        {
            DateTimeDVEx dv = new DateTimeDVEx();
            int[] xercesdate = (int[])(dv.getInternalValue(schemaValue,null));
            this.setValueFromXerces(xercesdate);
        } catch (Exception e)
        {
            String error=e.toString()+"While trying to parse date: "+schemaValue;
            Log.error(error);
            throw new RuntimeException(error);
        }
        
    }

    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    protected String toDisplayValueInternal()
    {
        // TODO: show also time!
        Locale locale = null;
        locale = Locale.getDefault();

        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG,locale);
        return (df.format(calValue.getTime()));
    }
    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    public String toSchemaStringInternal()
    {
        //Calendar calendar = new GregorianCalendar();
        Calendar calendar = calValue;
		java.text.DecimalFormat format = new java.text.DecimalFormat();
		format.setMinimumIntegerDigits(2);
		
		/*
		From schema datatypes spec:
		For example, to indicate 1:20 pm on May the 31st, 1999 for Eastern Standard Time 
		which is 5 hours behind Coordinated Universal Time (UTC), one would write: 
		1999-05-31T13:20:00-05:00. 
		*/
 		String date=
			calendar.get(Calendar.YEAR)+"-"+
			format.format((calendar.get(Calendar.MONTH)+1))+"-"+
			format.format(calendar.get(Calendar.DAY_OF_MONTH))+"T"+
			format.format(calendar.get(Calendar.HOUR_OF_DAY))+":"+
			format.format((calendar.get(Calendar.MINUTE)))+":"+
			format.format((calendar.get(Calendar.SECOND)))+"-"+
			format.format(((int)(calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000))))+":00";
        return date;
    }
    

} // DDateTime

