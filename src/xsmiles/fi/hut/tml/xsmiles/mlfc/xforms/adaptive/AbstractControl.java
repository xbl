/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ElementWithContext;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;

public abstract class AbstractControl 
    implements Control, TypedElement
{    
    protected XFormsContext fContext;
    protected XChangeListener fChangeListener;
    protected ActionListener fActionListener;
    //protected Data fData;
    /** whether I'm inside update event to prevent loops */
    protected boolean insideEvent=false;
    protected Element ownerElem;
    
    public AbstractControl(XFormsContext context, Element elem)
    {
        super();
        ownerElem=elem;
        this.fContext = context;
    }
    
    /**
     * add a listener for changes in the component
     */
    public void addChangeListener(XChangeListener listener)
    {
        fChangeListener = listener;
    }
    
    /**
     * add a listener for actions in the component
     */
    public void addActionListener(ActionListener listener)
    {
        fActionListener=listener;
    }
    
    /**
     * This notifies the text control to save its value before rewiring
     */
    public void rewiringAboutToHappen()
    {
    	
    }

    
    public boolean isWritable()
    {
        return true;
    }
    /**
     * close up, free all memory (yet, do not do visible changes, such as setVisible
     * since this will slow things up
     */
    public void destroy()
    {
        //this.fData=null;
    }
    
    /** call this method when the control has been activated */
    protected void activate(String command)
    {
        if (this.fActionListener!=null)
            this.fActionListener.actionPerformed(new ActionEvent(this,0,command));
    }
        /** call this method when the control has been activated */
    protected void activate(ActionEvent event)
    {
        if (this.fActionListener!=null)
            this.fActionListener.actionPerformed(event);
    }
    
    public boolean isInputControl() //input, range are input controls
    {
        return true;
    }
    public short getDataType()
    {
        return getData().getDataType();
    }
    public Data getData()
    {
        if (this.ownerElem instanceof ElementWithContext)   
		{
			InstanceNode refN =((ElementWithContext)ownerElem).getRefNode();
			if (refN!=null&&refN.getInstanceItem()!=null)
				return refN.getInstanceItem().getData();
		}
        return null;
    }
    
    public void setData(Data d)
    {
        getData().setValueFromSchema(d.toSchemaString());

    }
    
} // InputBoolean
