/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import org.w3c.dom.Element;

/**
 * this is the <input> element control implementation when the
 * datatype is derived from xsd:boolean. It is represented as a single
 * checkbox
 */
public class InputBoolean extends AbstractControl implements ItemListener
{
    
    protected XSelectBoolean fSelect;
    public InputBoolean(XFormsContext context, Element elem)
    {
        super(context,elem);
        this.createControl();
        this.registerListener();
    }
    
    protected void createControl()
    {
        fSelect = this.fContext.getComponentFactory().getXSelectBoolean();
    }
    /**
     * returns the abstract component for this control. The
     * abstract component can be used e.g. to style the component
     * but all listeners should be added to the AdaptiveControl and not directly
     * to the XComponent */
    public XComponent getComponent()
    {
        return fSelect;
    }
    /** internal method for setting the listener for the component */
    protected void registerListener()
    {
        fSelect.addItemListener(this);
    }
    public void destroy()
    {
        fSelect.removeItemListener(this);
    }
    
    /**
     * this function is used to notify the control to update its display according
     * to the content of Data
     */
    public void updateDisplay()
    {
        Boolean bool = (Boolean)getData().toObject();
        if (bool!=null)
        {
            //if (!insideUpdateEvent) return; // self-initiated
            try
            {
                fSelect.removeItemListener(this);
                //insideUpdateEvent=false;
                //			Log.debug("Checkbox changing value to: "+newValue);
                if (bool.equals(Boolean.TRUE))
                    fSelect.setSelected(true);
                else if (bool.equals(Boolean.FALSE))
                    fSelect.setSelected(false);
                else Log.error("Checkbox accepts only true/false values");
            } catch (Exception e)
            {
                Log.error(e);
            }
            finally
            {
                fSelect.addItemListener(this);
            }
        }
    }
    
    /**
     * This handles the changes from the widget XSelectBoolean
     */
    public void itemStateChanged(ItemEvent e)
    {
        //if (!insideUpdateEvent) return; // self-initiated
        if (this.fChangeListener!=null)
        {
            this.fChangeListener.valueChanged(false,null);
        }
    }
    
    /**
     * get the components current value
     */
    public Data getValue()
    {
        if (fSelect.getSelected())
        {
            getData().setValueFromObject(Boolean.TRUE);
        }
        else
        {
            getData().setValueFromObject(Boolean.FALSE);
        }
        return getData();
    }
} // InputBoolean
