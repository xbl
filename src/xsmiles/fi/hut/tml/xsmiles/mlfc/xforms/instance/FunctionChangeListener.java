/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;
/**
 * The listener for status / value changes in the instance item
 */
public interface FunctionChangeListener {

    /**
     */
    public void functionValueChanged(String function);


    
}

