/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyListener;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler.Dependency;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;

import fi.hut.tml.xsmiles.Utilities;



import org.w3c.dom.DOMException;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.apache.xerces.dom.events.EventImpl;

import org.xml.sax.SAXException;



import org.w3c.dom.xforms10.XFormsElement;


//import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.DocumentImpl;


import javax.xml.transform.TransformerException;
import java.net.URL;

/**
 * The superclass of all XForms elements that are bound to an instance node
 * It will listen for changes in the bound element, as well as all changes to the
 * binding itself, and will signal these changes up in the presentation DOM.
 * @author Mikko Honkala
 */

public abstract class DynBoundElementImpl extends XFormsElementImpl implements XFormsConstants, DynamicDependencyListener,ElementWithContext
{
    
    /** state: unitialized */
    public static final short UNINITIALIZED = 1;
    /** state: bindind attributes not found */
    public static final short BINDING_ATTRIBUTES_NOT_FOUND = 2;
    /** state: bound node was not found (non-relevant control) */
    public static final short BOUND_NODE_NOT_FOUND = 3;
    /** state: bindind failed (fatal error) */
    public static final short BINDING_FAILED = 4;
    /** state: binding ok */
    public static final short BINDING_OK = 5;
    
    /** the current binding state, one of the above */
    protected short binding_state=UNINITIALIZED;
    /** the dependency object */
    DynamicDependencyHandler.Dependency dependency;
    
    /** are binding attributes required (e.g. button does not require them */
    protected boolean bindingAttributesRequired=true;
    
    
    /** Repeat may set this context node */
    protected InstanceNode contextNode=null;
    
    protected BindElementImpl bind;

    
    /** are we initializing the control */
    protected boolean init=false;
    
    
    public DynBoundElementImpl(XFormsElementHandler handler, String ns, String name)
    {
        super(handler, ns, name);
    }
    
    
    /** check that model attribute is sane, if it exists */
    protected ModelElementImpl checkModelAttribute() throws XFormsBindingException
    {
        ModelElementImpl model = this.getModel();
        if (model==null&&this.bindingAttributesRequired)
            throw new XFormsBindingException(this,"Invalid model attribute value",this.getAttribute(MODEL_ID_ATTRIBUTE));
        return model;
    }
    
    
    protected BindElement checkBindAttribute() throws XFormsBindingException
    {
        Attr bindattr = this.getAttributeNode(BIND_ATTRIBUTE);
        if (bindattr==null) return null;
        String bindid = bindattr.getNodeValue();
        if (bindid==null||bindid.length()<1)
        {
            return null;
        }
        bind = this.getHandler().getBind(bindid);
        if (bind==null && this.bindingAttributesRequired)
            throw new XFormsBindingException(this,"invalid bind attribute value: "+bindid,"");
        //Log.debug(this.toString()+"Found bind attribute");
        return bind;
    }
    
    protected BindElementImpl getBind()
    {
        if (bind!=null) return bind;
        try
        {
            return (BindElementImpl)this.checkBindAttribute();
        } catch (XFormsBindingException e)
        {
            return null;
        }
    }
    
    public int getBindingType()
    {
        return Dependency.SINGLENODE_BINDING;
    }
    
    /** method for initing binding */
    void createBinding() throws XFormsBindingException
    {
        // do not re-create binding, unless destroyBinding has been called!
        if (this.binding_state!=this.UNINITIALIZED) return;
        // try to find my context node from ancestor
        // if repeat has already set my contextNode, don't fiddle with it!
        
        if (this.contextNode==null) getContextFromAncestor(this,this);
        ModelElementImpl model = this.checkModelAttribute();
        checkBindAttribute();
        
        Attr refAttr=this.getAttributeNode(REF_ATTRIBUTE);
        if (refAttr==null)
        {
            refAttr=this.getAttributeNode(NODESET_ATTRIBUTE);
        }
        if (refAttr==null)
        {
            refAttr=this.getAttributeNode(BIND_ATTRIBUTE);
        }
        if (refAttr==null)
        {
            this.binding_state=DynBoundElementImpl.BINDING_ATTRIBUTES_NOT_FOUND;
            if (this.bindingAttributesRequired)
            {
                this.debugNode(this);
                throw new XFormsBindingException(this,"",this.getAttribute(MODEL_ID_ATTRIBUTE));
            }
        }
        
        try
        {
            if (refAttr!=null) 
            {
                // check for lazy bastard
                InstanceElementImpl instance = model.getLazyBastardInstance();
                if (instance!=null)
                {
                    instance.createLazyBastardElement(this.getXPath().getExpression(),this);
                }
                int bindingType=this.getBindingType();

                dependency = model.getDynamicDependencyHandler().createDependency(bindingType);
                dependency.init(this);
            }
            else if (!this.bindingAttributesRequired)
            {
                // notify descendand elements!
                this.notifyBindingChangedRecursively(this);
            }
        } catch (Exception e)
        {
            //TODO: throw binding error
            Log.error(e,"ref: ");
            throw new XFormsBindingException(this,e.getMessage(),this.getAttribute(MODEL_ID_ATTRIBUTE));
        }
    }
    
    /** the refresh processing notifies, that the binding might have changed, and it needs to be re-evaluated */
    public void bindingMaybeDirty()
    {
        //Log.debug("Binding maybe dirty:"+this);
        try
        {
            this.dependency.reevaluateBinding();
        } catch (Exception e)
        {
            Log.error(e);
        }
    }

    
    public void resetContextNode()
    {
        this.contextNode=null;
    }
    public void renewBinding()
    {
        this.destroyBinding();
        try
        {
            this.createBinding();
            //if (this.dependency!=null)
                //this.dependency.reevaluateBinding();  
        } catch (Exception e)
        {
            Log.error(e,"While renewing binding for "+this);
        }
    }
    
    protected void destroyBinding()
    {
        // NOTE: we still want to keep contextNode!
        //this.refNodes=null;
        //this.refNode=null;
        if (dependency!=null) this.dependency.destroy();
        this.dependency=null;
        this.binding_state=this.UNINITIALIZED;
    }
    
    /** returns the current binding state */
    public short getBindingState()
    {
        return this.binding_state;
    }
    
    /**
     * Search for my reference nodes
     * this is multiple node binding
     */
    public NodeList getBoundNodeset() //throws XFormsBindingException
    {
        if (this.dependency==null) return null;
        return this.dependency.getBoundNodes();
    }
    
    public InstanceNode getContextForDescendant()
    {
        if (this.binding_state==BINDING_ATTRIBUTES_NOT_FOUND)
        {
            return this.contextNode;
        }
        NodeList nl = this.getBoundNodeset();
        if (nl==null||nl.getLength()==0) return null;
        else if (nl.item(0) instanceof Text)
		{
			return (InstanceNode)(nl.item(0).getParentNode());
		}
        else return (InstanceNode)nl.item(0);
    }
    
    public void destroy()
    {
        super.destroy();
        this.destroyBinding();
        this.setContextNode(null);
    }
    
    /**
     * Sets the reference instance node value
     */
    protected synchronized void setRefNodeValue(Object value, boolean valueChanging)
    {
        if (init==true) return;
        if (this.getRefNode()==null) return;
        if (value instanceof String)
        {
            String text=(String)value;
            this.getModel().uiValueChange(this.getRefNode(),text);
            /*
            if (valueChanging)
            {
                XFormsUtil.setText(this.getRefNode(),text);
                //dispatch(XFormsEventFactory.createXFormsEvent(XFormsConstants.VALUE_CHANGING_EVENT));
            }
            else // value changed
            {
                XFormsUtil.setText(getRefNode(),text);
                //dispatch(XFormsEventFactory.createXFormsEvent(XFormsConstants.VALUE_CHANGED_EVENT));
            }*/
        }
    }
    
    
    /**
     * gets the reference instance node value
     */
    protected Object getRefNodeValue()
    {
        if (getRefNode()==null) return "";
        else return XFormsUtil.getText(getRefNode());
    }
    public InstanceNode getRefNode()
    {
        //if (refNode==null) this.findAndSetRefNode(this.getRef());
        if (this.dependency==null) return null;
        return (InstanceNode)this.dependency.getRefNode();
    }
    
    public String getRef()
    {
        Attr refAttr=this.getAttributeNode("ref");
        if (refAttr==null)
        {
            refAttr=this.getAttributeNode("nodeset");
        }
        if (refAttr==null) return null;
        return refAttr.getNodeValue();
    }
    
    /** this method will get the context node from an possible ancestor element. Used also by ModelBoundElement. */
    static void getContextFromAncestor(DynBoundElementImpl bound, ElementWithContext refElem) throws XFormsBindingException
    {
        ElementWithContext parent = getParentBoundElement(bound, refElem);
        if (parent!=null)
        {
            bound.setContextNode((InstanceNode)parent.getContextForDescendant());
        }
    }
    /** Used also by ModelBoundElement. */
    protected static ElementWithContext getParentBoundElement(Node start,ElementWithContext refElem)
    {
        Node n = start.getParentNode();
        
        while (n!=start.getOwnerDocument().getDocumentElement())
        {
            if (n instanceof ElementWithContext)
            {
                if (((ElementWithContext)n).getBindingState()!=DynBoundElementImpl.BINDING_ATTRIBUTES_NOT_FOUND)
                {
                    if (isSameModel((ElementWithContext)n,refElem))
                        return (ElementWithContext)n;
                }
            }
            n=n.getParentNode();
        }
        return null;
    }
    
    public void setContextNode(InstanceNode node)
    {
        //Log.debug(this.toString()+":somebody set bound node context node to ");
        //this.debugNode(node);
        this.contextNode=node;
    }
    
    public Node getContextNode()
    {
        if (this.getBind()!=null) return this.getBind().getContextNode();
        if (this.contextNode==null)
        {
            try
            {
                return this.getModel().getInstance().getInstanceDocument().getDocumentElement();
            } catch (NullPointerException e)
            {
                Log.error(e);
                return null;
            }
        }
        return this.contextNode;
    }
    public XPathEngine getXPathEngine()
    {
        return this.getModel().getXPathEngine();
    }

    
    /**
     * Gets my model element. If not specified using "model" attribute, will look for parent DynBoundElementImpl
     */
    
    protected ModelElementImpl myModel;
    public ModelElementImpl getModel()
    {
        if (myModel==null)
        {
            //ModelElementImpl myModel =null;
            Attr bindattr = this.getAttributeNode(BIND_ELEMENT);
            if (bindattr!=null&&this.getBind()!=null) // there is the "bind" attribute, look for its model
            {
                myModel = this.getBind().getModel();
            }
            else
            {
                
                Attr modelAttr=this.getAttributeNode(MODEL_ID_ATTRIBUTE);
                if (modelAttr==null)
                {
                    // look for parent bound element, and find the first ancestors model
                    // TODO: in repeat, parent could be null sometimes...
                    Node parent = this.getParentNode();
                    while (parent!=null&&parent.getNodeType()==Node.ELEMENT_NODE)
                    {
                        if (parent instanceof DynBoundElementImpl)
                        {
                            myModel= ((DynBoundElementImpl)parent).getModel();
                            break;
                        }
                        parent=parent.getParentNode();
                    }
                    if (myModel==null&&parent!=null&&parent.getNodeType()==Node.DOCUMENT_NODE) myModel=handler.getModel("");
                    
                }
                else
                {
                    myModel = this.getModel(modelAttr.getNodeValue());
                }
            }
        }
        return myModel;
    }
    
    
    /** @return the model context for this expression  */
    public ModelContext getModelContext()
    {
        return this.getModel();
    }
    
    /** @return the namespace context node for the XPath expression  */
    public Node getNamespaceContextNode()
    {
        if (this.getBind()!=null) return this.getBind();
        return this;
    }
    
    /** get the XPath string to listen to  */
    public XPathExpr getXPath()
    {

            return this.getRefXPathExpr();
    }
    
    /** @return true if single node binding expression  */
    public boolean isSingleNodeBinding()
    {
        return true;
    }
    
    protected XPathExpr refExpr=null;
    public XPathExpr getRefXPathExpr()
    {
        if (refExpr==null) 
        {
            Attr bindattr = this.getAttributeNode(BIND_ELEMENT);
            if (bindattr!=null) // there is the "bind" attribute
            {
                // The 'bind' attribute overrides 'ref' or 'nodeset'
                
                if (getBind()==null)
                    return null;
                else
                {
                    this.refExpr=this.getModelContext().getXPathEngine().createXPathExpression(getBind().getRef());
                }
            }
            else
            {
		        String r = this.getRef();
		        if (r==null||r.length()==0) return null;
		        this.refExpr=this.getModelContext().getXPathEngine().createXPathExpression(r);
            }
        }
        return refExpr;
    }
    
    /**
     * This is called when a binding goes to zero nodes
     */
    public void checkVisibility()
	{
		
	}

	public void checkStyling()
	{
		this.styleChanged();
	}
	
    /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        //Log.debug("BINDING HAS CHANGED FOR "+this+" xpath: "+this.getXPath().getExpression()+" referenced:"+(newBinding==null?0:newBinding.getLength()));
        if (newBinding==null||newBinding.getLength()==0)
        {
            //Log.debug("The new binding is null!");
            this.binding_state = BOUND_NODE_NOT_FOUND;
			this.checkStyling();
        }
        //refNodes=newBinding;
        NodeList refNodes=newBinding;
        if (refNodes!=null&&refNodes.getLength()>0)
        {
            //Log.debug("new refNode: "+refNodes.item(0));
            /*
			if (refNodes instanceof InstanceNode)
			{
				Log.debug("refNodes list was a single node!"); // for some reason sometimes the nodelist is a single node...
				this.getRefNode=(InstanceNode)refNodes;
			}
			else if (refNodes.item(0).getNodeType()==Node.TEXT_NODE || refNodes.item(0).getNodeType()==Node.COMMENT_NODE)
            {
                // SOMETIMES FOR instance('id'), Xalan returns text nodes!
                this.refNode=(InstanceNode)refNodes.item(0).getParentNode();
            } else
            {
                this.refNode=(InstanceNode)refNodes.item(0);
            }*/
            this.binding_state=BINDING_OK;
        }
        // notify descendand elements!
        this.notifyBindingChangedRecursively(this);
    }
    
    /** the ancestor may signal thru this method that its binding has changed, and therefore
     * I should renew my context node and binding */
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor)
    {
        Node parentRefNode = ancestor.getRefNode();
        //Log.debug(this +"got event that parents binding has changed. parent: "+ancestor+" 's refNode:"+parentRefNode);
        if (parentRefNode!=this.getContextNode())
        {
            // renew binding
            if (parentRefNode==null) parentRefNode=(InstanceNode)ancestor.getContextNode();
            this.setContextNode( (InstanceNode)parentRefNode );
            this.renewBinding();
        }
    }
    
    protected void notifyBindingChangedRecursively(Element elem)
    {
        Node child = elem.getFirstChild();
        while (child!=null)
        {
            if (child instanceof DynBoundElementImpl)
            {
                // TODO: check that the model is the same!
                // TODO: check that binding attributes exists
                //if ( ((Element)child).getAttribute(MODEL_ID_ATTRIBUTE).equals(this.getAttribute(MODEL_ID_ATTRIBUTE)) )
                if ( isSameModel((ElementWithContext)child,this))
                {
                    ((DynBoundElementImpl)child).notifyParentBindingChanged(this);
                } else
                {
                    this.notifyBindingChangedRecursively((Element)child);
                }
            }
            else if (child instanceof Element)
            {
                this.notifyBindingChangedRecursively((Element)child);
            }
            child=child.getNextSibling();
        }
        
    }
    
    protected static boolean isSameModel(ElementWithContext elem1, ElementWithContext elem2)
    {
        return (elem1.getModel()==elem2.getModel());
    }
    /** @return the instanceItemListener for the referred nodes, for value
     * changes without binding change */
    //public abstract InstanceItemListener getInstanceItemListener();
    
    public InstanceItemListener getInstanceItemListener()
    {
        //Log.debug(this+" returning  null for instanceitemlistener");
        return null;
    }
    
    /** check if my binding has changed */
    public void checkBinding()
    {
        //ec.resetContextNode();
        try
        {
            if (this.dependency!=null)
            {
                this.contextNode=null;
                //this.dependency.valueChanged("");
                this.dependency.reevaluateBinding();
            }
        }
        catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    /** is this element bound. Not all elements are always bound, such as group without ref  */
    public boolean hasBindingAttributes()
    {
        return (this.getBindingState()!=BINDING_ATTRIBUTES_NOT_FOUND);
    }
    
    /** an utility function for evaluating the ref or nodeset XPath once and returning
     * the result. This can be used by elements, who know that they are leaf nodes, and
     * only want to evaluate it once
     */
    public InstanceNode evaluateRefNode() throws Exception, SAXException
    {
        XPathExpr xpath = this.getXPath();
        Node context = this.getContextNode();
        Node nscontext = this.getNamespaceContextNode();
        NodeList result = this.getModel().getXPathEngine().evalToNodelist(context,xpath,nscontext,null);
        if (result==null||result.getLength()<1) return null;
        else
        {
            Node node = result.item(0);
            if (node.getNodeType()==Node.TEXT_NODE)
            {
                node=node.getParentNode();
            }
            return (InstanceNode)node;
        }
    }
    
        /** get text: use precedence : refNode, linking attributes, inline text */
    public String getTextWithPrecedence() throws XFormsLinkException
    {
        if (this.getRefNode()!=null)
        {
            return XFormsUtil.getText(this.getRefNode());
        }
        else
        {
            String src = this.getSrc();
            if (src!=null)
            {
                // retrieve the resource, throw a link error if not possible
                try
                {
                    URL u = resolveURI(src);
                    return this.retrieveResourceAsString(u);
                } catch (Exception e)
                {
                    Log.error(e);
                    throw new XFormsLinkException(e.toString(),src);
                }
            }
            else
            {
                return this.getChildText();
            }
        }
    }

    /** iterate through output and text nodes */
    public String getChildText()
    {
            String captionText="";
            Node child = this.getFirstChild();
            while (child!=null)
            {
                if (child.getNodeType()==Node.TEXT_NODE)
                {
                    captionText+=Utilities.trimHTMLContent(child.getNodeValue());
                }
                else if (child.getNodeType()==Node.ELEMENT_NODE&&
                child.getLocalName().equals(OUTPUT_ELEMENT)&&
                child.getNamespaceURI().equals(XFORMS_NS))
                {
                    String value = ((XFormsControl)child).getOutputValue();
                    if (value!=null)
                        captionText+=value;
                }
                child = child.getNextSibling();
            }
            return captionText;
    }
            /** @return the list of context nodes of this expression */
    public NodeList getContextNodeList()
    {
        return this.getBoundNodeset();
    }
    
    public Object clone() throws CloneNotSupportedException
    {
        DynBoundElementImpl control = (DynBoundElementImpl)super.clone();
        // copy the XPathExpr for the ref/nodeset
        control.refExpr=this.refExpr;
        return control;
    }

}
