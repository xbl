/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.dom.EventHandlerService;

import org.w3c.dom.Element;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;

/**
 * loadURI element implementation
 * @author Mikko Honkala
 */


public class SetFocusElementImpl extends XFormsElementImpl implements EventHandlerService
{
    
    public SetFocusElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void activate(Event evt)
    {
        String idref = this.getIdRef();
        if (idref!=null&&idref.length()>0)
        {
            Element elem = searchElementWithId(this.getOwnerDocument().getDocumentElement(),idref);
            if (elem==null)
                return;
            elem = this.handler.getRepeatIndexHandler().findRepeatedId(elem, idref);
            ((EventTarget)elem).dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.FOCUS_EVENT));
        }
    }
    
    protected String getIdRef()
    {
        return this.getAttribute(CONTROL_ATTRIBUTE);
    }
    
    
}
