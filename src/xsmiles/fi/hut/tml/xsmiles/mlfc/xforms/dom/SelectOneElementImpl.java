/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.w3c.dom.css.*;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectOneElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectionItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;

/**
 * The XForms/Select1 element
 * @author Mikko Honkala
 */

public class SelectOneElementImpl extends SelectElementImpl implements SelectOneElement
{
    protected boolean isInputComponent=true;
    protected final static int RADIO=1,CHECKBOX=2,MENU=3,LISTBOX=4;
    protected int type;
    protected XSelectOne selectOne;
    
    public SelectOneElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    
    /**
     * Creates the visible containing component of this control
     */
    public XComponent createComponent()
    {
        String style=(String)this.getAttribute(APPEARANCE_ATTRIBUTE);
        if (style==null) style="";
        //Log.debug("Creating XSelectOne");
        this.selectOne =  this.getComponentFactory().getXSelectOne(style,this.isOpenSelection());
        this.select=selectOne;
        return super.createComponent();
    }
    public void changeComponentValue(String newValue)
    {
        //Log.debug("Select changing value to: "+newValue);
        insideUpdateEvent=true;
        ItemElementImpl item=findItem(newValue);
        if (item==null)
        {
            Log.error("Unknown value for exclusiveSelect item: "+newValue);
            this.dispatch(XFormsEventFactory.createXFormsEvent(OUTOFRANGE_EVENT));
            this.outOfRange=true;
        }
        else
        {
            this.outOfRange=false;
            if (selectOne!=null)
            {
                int selIndex=selectOne.getSelectedIndex();
                // NOTE: this test was added because in some AWT environment (SUN j2me personal profile)
                // we entered into a loop... 
                if (selIndex!=item.getIndex())
                    selectOne.setSelectedIndex(item.getIndex());
            }
        }
        insideUpdateEvent=false;
    }
    
    /** the method from ItemLister gets the selection events from the control*/
    public void itemStateChanged(ItemEvent e)
    {
        //Log.debug("ItemStateChanged: "+e);
        if (insideUpdateEvent) return;
        int index = selectOne.getSelectedIndex();
        ItemElementImpl item=findItem(index);
        if (item!=null)
        {
            // if item was found, then this is closed selection
            CopyElementImpl copy = item.getCopyElement();
            if (copy!=null)
            {
                InstanceNode instanceElement = this.getRefNode();
                if (instanceElement.getNodeType()!=Node.ELEMENT_NODE)
                    this.handleXFormsException(new XFormsBindingException(this,this.getXPath().getExpression(),"Model: "+this.getModelId()+" The node referenced by a select with a copy element inside must be an element."));
                else
                    this.replaceSubtree(copy,instanceElement);
                // TODO: rebuild
            }
            else
            {
                this.setRefNodeValue(item.getValue(),false);
            }
            this.userSelected(item);
        }
        else if (this.isOpenSelection())
        {
            Log.debug("User has typed in in an open selection: "+e.getItem().toString());
            this.setRefNodeValue(e.getItem().toString(),false);
            //debugNode(this.getRefNode().getOwnerDocument());
        }
        else
        {
            Log.error("Could not find an item corresponsing to user selection:"+e.toString());
        }
    }
    

    
 
    
    
    protected void userSelected(ItemElementImpl item)
    {
        super.userSelected(item);
        this.dispatchActivateEvent();
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectOneElement#getCurrentSelection()
     */
    public SelectionItem getCurrentSelection() throws ArrayIndexOutOfBoundsException
    {
        // TODO Auto-generated method stub
            return (SelectionItem)this.items.elementAt(this.select.getSelectedIndex());
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
     */
    public XSmilesConnection get(URL dest, short type)
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    
}
