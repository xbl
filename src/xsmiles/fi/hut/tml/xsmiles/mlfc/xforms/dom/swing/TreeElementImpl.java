/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom.swing;


import fi.hut.tml.xsmiles.Log;

import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.event.*;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XComponentWrapper;
import fi.hut.tml.xsmiles.gui.components.XSelect;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.CaptionElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectElement;
import fi.hut.tml.xsmiles.util.XSmilesConnection;


/**
 * Abstract select element
 * @author Mikko Honkala
 */

public class TreeElementImpl extends XFormsControl
implements 
org.w3c.dom.events.EventListener, 
TreeSelectionListener
{
    
    protected boolean isInputComponent=true;
    
    /** the abstract select component that the subclass creates */
    protected XMLTree tree;
    
    /**
     * Constructs a new 'select' control (exclusiveSelect or multipleSelect)
     *
     * @param my_handler    The handler for this control
     * @param my_elem           The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    public TreeElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        super.componentInit(); // for some reason the size gets fucked up otherwise...
        Log.debug("Inited the tree element.");
    }
    /**
     * Creates the visible containing component of this control
     */
    public XComponent createComponent()
    {
         
        Node root = this.getRefNode();
        this.tree=new XMLTree(root,this);
        XComponent xcomp = new XComponentWrapper(this.tree,this.handler.getComponentFactory().getCSSFormatter(),Color.white);
        Log.debug("Created the tree widget.");
        //this.tree.setSize(100,100);
        return xcomp;
    }
    
    /** register item listener */
    protected void registerListener()
    {
        super.registerListener();
        this.tree.addTreeSelectionListener(this);
        this.handler.nodeIndexChanged(this.getId(),this.getRefNode());
    }
	
    public void destroy()
	{
		this.removeAllMutationListeners(this.getRefNode());
		super.destroy();
	}

    protected String getItemRefStr()
    {
        NodeList l = this.getElementsByTagNameNS(XFormsConstants.XFORMS_NS,"item");
        if (l.getLength()>0)
        {
            Element e = (Element)l.item(0);
            l = e.getElementsByTagNameNS(XFormsConstants.XFORMS_NS,"label");
            if (l.getLength()>0)
            {
                e = (Element)l.item(0);

                String iref = e.getAttribute("ref");
                if (iref!=null && iref.length()>0) return iref;
            }
        }
        return ".";
    }
    protected XPathExpr cachedRefExpr;
    protected XPathExpr getValueRef()
    {

        if (cachedRefExpr==null)
        {
            String exprStr=this.getItemRefStr();
            cachedRefExpr = this.getModel().getXPathEngine().createXPathExpression(exprStr);
        }
        return cachedRefExpr;
    }
    protected Node getValueNSNode()
    {
        return this; // TODO: real node
    }
    protected String getValueXPathResult(Node n) throws Exception
    {
        return this.getModel().getXPathEngine().evalToString(n,this.getValueRef(),this.getValueNSNode(),null);
    }
    
    int i=0;
    public String  convertValueToText(Node node, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) 
    {
        try
        {
            return this.getValueXPathResult(node);
        } catch (Exception e)
        {
            Log.error(e);
            return node.getLocalName();
        }
        /*
        if (node instanceof Element)
        {
            Element e = (Element)node;
        }*/
        //return "Error"; 
    }

    
    

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl#changeComponentValue(java.lang.String)
     */
    protected void changeComponentValue(String newValue)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
     */
    public XSmilesConnection get(URL dest, short type)
    {
        // TODO Auto-generated method stub
        return null;
    }
    public void    valueChanged(TreeSelectionEvent e) 
    {
        TreePath path = this.tree.getSelectionPath();
        if (path!=null)
        {
			Log.debug("selected: "+path.toString()+" last component: "+path.getLastPathComponent());
            Object o  = path.getLastPathComponent();
            if (o instanceof Node)
            {
                this.handler.nodeIndexChanged(this.getId(),(Node)o);
            }
            else
            {
                Log.error("Selection of a tree is not a DOM Node!!!!!");
            }
        }
    }
	
    public void notifyBindingChanged(NodeList newBinding)
    {
		if (this.getRefNode()!=null)
			this.removeAllMutationListeners(this.getRefNode());
		super.notifyBindingChanged(newBinding);
		if (this.getRefNode()!=null)
        {
		    this.listenAllMutations(this.getRefNode());
		    if (this.tree!=null)this.tree.setRoot(this.getRefNode());
        }
    }
	
    final String[] evtNamesMut=
    {
        //"DOMSubtreeModified",
        //			"DOMAttrModified",
        //			"DOMCharacterDataModified",
        	        "DOMNodeInserted","DOMNodeRemoved",
        //	        "DOMNodeInsertedIntoDocument","DOMNodeRemovedFromDocument",
    };
    private void listenAllMutations(Node n)
    {
        EventTarget t=(EventTarget)n;
        for(int i=evtNamesMut.length-1;
        i>=0;
        --i)
        {
            t.addEventListener(evtNamesMut[i], this, false);
        }
    }
    
    protected void removeAllMutationListeners(Node n)
    {
        EventTarget t=(EventTarget)n;
		if (t!=null)
		{
        for(int i=evtNamesMut.length-1;
        i>=0;
        --i)
        {
				t.removeEventListener(evtNamesMut[i], this, false);
        }
		}
    }
	private Thread t;
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
		super.handleEvent(evt);
        final Node target=(Node)evt.getTarget();
        Log.debug("TreeElementImpl Got event: "+evt.getType());
        final Node parent = target.getParentNode();
        for(int i=evtNamesMut.length-1;       i>=0;       --i)
        {
			if (evt.getType().equals(evtNamesMut[i]))
			{
				// The NodeRemoved event is dispatched before the change happens... we have therefore
				// to wait for a while before updating the tree. 
				// TODO: create a custom event, which is fired to the parent after removal
				if (t==null)
				{
					t = new Thread()
					{
						public void run()
						{
							try
							{
								Thread.sleep(800);
								tree.structureChanged(target,parent,true);
							} catch (InterruptedException e)
							{
								Log.error(e);
							}
							t=null;
						}
					};
					t.start();
				}
			}
        }
    }


}
