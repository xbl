// MouseEventImpl.java
//
/*****************************************************************************
 * Copyright (C) CSIRO Mathematical and Information Sciences.                *
 * All rights reserved.                                                      *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the CSIRO Software License  *
 * version 1.0, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/
//
// $Id: XFormsEventImpl.java,v 1.5 2003/04/17 10:04:22 honkkis Exp $
//
package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import org.w3c.dom.views.AbstractView;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.dom.EventImpl;
/**
 * Implementation of the DOM mouse event
 */
public class XFormsEventImpl extends EventImpl implements org.w3c.dom.events.Event {

  Node relatedNode;

  public XFormsEventImpl() {
  }

  public void initXFormsEvent(String typeArg, boolean canBubbleArg,
                              boolean cancelableArg, Node relatedNodeArg) {

    super.initEvent(typeArg, canBubbleArg, cancelableArg);
    relatedNode = relatedNodeArg;
  }

  public Node getRelatedNode() {
    return relatedNode;
  }
  
  public Object clone() throws java.lang.CloneNotSupportedException
  {
  	XFormsEventImpl n = new XFormsEventImpl();
	n.initXFormsEvent(this.type,this.bubbles, this.cancelable, this.relatedNode);
  	return n;
  }

} // MouseEventImpl
