/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Mikko Honkala
 *
 */
public interface XPathEngine {
    public XPathExpr createXPathExpression(String xpath);
    //public XPathExpr trackedXPathExpression(String xpath);
    
    /** set the evaluation context and default context node if context node is null later */
    public void setContext(XFormsContext handler,ModelContext mcontext);
    public ModelContext getModelContext();
    public XFormsContext getXFormsContext();
    /* evaluates an XPath expression against  a context 
     *  If the expression evaluates to a single primitive (String, Number or Boolean) type, it is returned directly. 
     * Otherwise, the returned value is a List (a node-set, in the terms of the specification) of values.
     * 
     * */
    public Object eval(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList)
            throws Exception;
    public String evalToString(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList)
    		throws Exception;
    public NodeList evalToNodelist(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList)
    throws Exception;
    /** returns also all the referenced nodes, used by XForms Calculation engine and dynamic UI dependencies */
    public Object evalWithTrace(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList,LookupResult result)
    	throws Exception;
    
    /** for model/@function processing, checks whether this function exists
     */
    public boolean hasFunction(String uri, String funcName);

}
