/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;




/**
 * XForm/Bind element interface, used by dependency graph
 * @author Mikko Honkala
 */


public interface BindElement {
	public String getCalculateString();
	public String getRelevantString();
	public String getRequiredString();
	public String getReadonlyString();
	public String getIsvalidString();
    public String getTypeString();
	//public Object evalXPath(Node context, XPathExpr xpath, Node nsnode, NodeList contextNodeList) throws Exception;
    public XPathEngine getXPathEngine();
	public Node getNamespaceContextNode();
	public NodeList getBoundNodeset();// throws XFormsBindingException;

}
