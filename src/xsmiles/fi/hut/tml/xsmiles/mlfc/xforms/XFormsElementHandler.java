/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms;

import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.URLConnection;

import fi.hut.tml.xsmiles.mlfc.xforms.adaptive.AdaptiveControlFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatIndexHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogGUIDebug;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.Action;

import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.StylesheetService;
import org.w3c.dom.*;
import org.apache.xerces.dom.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XDialog;
import fi.hut.tml.xsmiles.gui.components.XConfirmDialog;
// TODO: swing dependency
//import fi.hut.tml.xsmiles.gui.components.swing.SwingFocusManager;
import fi.hut.tml.xsmiles.XMLDocument;

import fi.hut.tml.xsmiles.mlfc.xforms.dom.*;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.FunctionChangeListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;

import fi.hut.tml.xsmiles.dom.XMLBroker;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.*;




import java.util.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.File;

/**
 * The factory for creating DOM event instances
 * it also keeps track of different per page things, such as models, cursors  etc.
 * @author Mikko Honkala
 */
public class XFormsElementHandler implements XFormsContext, XFormsConstants, ActionListener
{
    /** the possible dialog UI */
    protected DialogGUIDebug dialogUI;
    
    /** All the XForm elements in the document */
    protected Hashtable models;
    
    /** the default XForm element */
    protected ModelElementImpl defaultXForm;
    
    /** All the Repeat elements in the document */
    protected Hashtable cursors;

    /** All the Repeat elements in the document */
    protected Hashtable nodeindexes;
    
    /** Listeners for nodeindex changes */
    protected Vector nodeIndexListeners;
    
    /** All the case elements in the document */
    protected Hashtable cases;
    
    /** the bind elements with id's */
    private Hashtable binds=new Hashtable(20);
    
    /** the ui index handler for repeats */
    protected RepeatIndexHandler repeatIndexHandler =new RepeatIndexHandler(this);
    
    
    //protected XMLDocument xmldoc;
    protected ExtendedDocument doc;
    protected DocumentImpl docImpl;
    
    /** the CSS stylesheet object */
    //protected XSmilesStyleSheet stylesheet;
    
    protected MLFC mlfc;
    
    /** factory used to create datatype aware controls */
    protected static AdaptiveControlFactory sAdaptiveControlFactory;
    static
    {
        sAdaptiveControlFactory = new AdaptiveControlFactory();
    }

    
    /** kludge for model/@function processing, checks whether this function exists
     * in the function table. For later xalan versions, this has to be changed
     *TODO: namespaces
     */
    /* moved to XPathEngine
    public static boolean hasFunction(String uri, String funcName)
    {
        return ExtensionFunctions.hasFunction(uri,funcName);
    }*/
    
    static boolean debugEvents=false;
    public static boolean debugXForms=false;
    public XFormsElementHandler(ExtendedDocument a_doc, MLFC xmlfc)
    {
        this.doc =a_doc;
        this.mlfc = xmlfc;
        this.docImpl=(DocumentImpl)doc;
        //this.setFocusManager();
        
        Log.debug("&&& Creating XForms Elements");
    }
    
    /** called when MLFC start is called */
    public void start()
    {
        //listenForHelpKeyPress();
        //if (XFormsConfiguration.getInstance().shouldStartDialogUI()) this.startDialogUI();
    }
    /**
     * Starts the additional dialog UI for speech
     */
    /*
    public void startDialogUI()
    {
        //this.dialogHandler = new DialogHandler();
        this.dialogUI=new DialogGUIDebug();
        dialogUI.setHandler(this);
        dialogUI.setComponentFactory(this.getComponentFactory());
        dialogUI.setDocument(this.getDocumentImpl());
        dialogUI.start();
    }
    */
    
    public RepeatIndexHandler getRepeatIndexHandler()
    {
        return this.repeatIndexHandler;
    }
    public boolean getDebugEvents()
    {
        return this.debugEvents;
    }
    public DocumentImpl getDocumentImpl()
    {
        return docImpl;
    }
    
    public void addNodeIndexListener(FunctionChangeListener list)
    {
        if (this.nodeIndexListeners==null) this.nodeIndexListeners=new Vector();
        if (!(this.nodeIndexListeners.contains(list))) this.nodeIndexListeners.addElement(list);
    }

    public void removeNodeIndexListener(FunctionChangeListener list)
    {
        if (this.nodeIndexListeners==null) return;
        this.nodeIndexListeners.removeElement(list);
    }

	
    public void nodeIndexChanged(String id, Node n)
    {
        if (this.nodeindexes==null) this.nodeindexes=new Hashtable();
        if (id==null)
        {
            Log.error("XForms nodeindex (repeat or tree) changed, but the element has null id");
        }
        else if (n==null)
        {
            Log.error("XForms nodeindex (repeat or tree) changed, but the node is null");
            
        }  else 
        {
            this.nodeindexes.put(id,n);
    		Log.debug("handler.nodeIndexChanged:"+id+":"+n);
            notifyFunctionChanged();
        }
    }
    
    protected void notifyFunctionChanged()
    {
        if (nodeIndexListeners!=null)
        {
			// first notify all dependencies
            Enumeration e = this.nodeIndexListeners.elements();
            while(e.hasMoreElements())
            {
                FunctionChangeListener list = (FunctionChangeListener)e.nextElement();
				if (!(list instanceof ModelElementImpl))
					list.functionValueChanged("");
            }
			// then notify all models
            e = this.nodeIndexListeners.elements();
            while(e.hasMoreElements())
            {
                FunctionChangeListener list = (FunctionChangeListener)e.nextElement();
				if (list instanceof ModelElementImpl)
					list.functionValueChanged("");
            }
        }
        
    }
    
    /** return my mlfc */
    protected MLFC getMLFC()
    {
        return this.mlfc;
    }
    public ComponentFactory getComponentFactory()
    {
        return this.mlfc.getMLFCListener().getComponentFactory();
    }
    public AdaptiveControlFactory getAdaptiveControlFactory()
    {
        return sAdaptiveControlFactory;
    }
    
    public ExtendedDocument getExtendedDocument()
    {
        return doc;
    }
    /**
     * Create a DOM attribute.
     */
    public Attr createAttributeNS(DocumentImpl doc, String namespaceURI, String qualifiedName)
    {
        return ElementFactory.createAttributeNS(doc,namespaceURI,qualifiedName,this);
    }
    
    public Element createElementNS(String URI,String tagname)
    {
        return ElementFactory.createElementNS(this,URI,tagname);
    }


    
    public void addModel(ModelElementImpl form)
    {
        if (this.models==null) this.models=new Hashtable();
        this.models.put(form.getId(),form);
        if (defaultXForm==null) defaultXForm=form;
        Log.debug("Added model: "+form.getId());
    }
    public void removeModel(ModelElementImpl form)
    {
        if (this.models!=null)
        {
            this.models.remove(form.getId());
        }
    }
    public void addCase(CaseElementImpl caseElem)
    {
        if (this.cases==null) this.cases=new Hashtable();
        this.cases.put(caseElem.getId(),caseElem);
        Log.debug("Added case: "+caseElem.getId());
    }
    
    public CaseElementImpl getCase(String id)
    {
        return (CaseElementImpl)this.cases.get(id);
    }
    public void setCursor(String id, int value)
    {
        if (this.cursors==null) this.cursors=new Hashtable();
        cursors.put(id,new Integer(value));
        this.notifyFunctionChanged();
        Log.debug("Cursor "+id+" set to:"+value);
    }
    
    public int getCursor(String id)
    {
        int errret = 1;
        if (cursors == null || id==null) return errret; // SHOULD WE RETURN -1?
        Object obj = cursors.get(id);
        if (obj==null) return errret; // SHOULD WE RETURN -1?
        Integer i = ((Integer)obj);
        if (i==null) return -1;
        return i.intValue();
    }
    
    public ModelElementImpl getModel(String xform)
    {
        if (xform==null||xform.length()<1) return defaultXForm;
        if (this.models==null || this.models.size()==0)
        {
            Log.error("There is no xform:model with id: "+xform);
            return null;
        }
        else return (ModelElementImpl)this.models.get(xform);
    }
    
    public Hashtable getModels()
    {
        return this.models;
    }
    
    public BindElementImpl getBind(String id)
    {
        return (BindElementImpl)binds.get(id);
    }
    
    public void addBind(BindElementImpl bind)
    {
        String bindid = bind.getId();
        if (bindid!=null&&bindid.length()>0)
        {
            binds.put(bindid,bind);
        }
        
    }
    
    public XSmilesStyleSheet getStyleSheet()
    {
        Document doc = this.docImpl ;
        XSmilesStyleSheet stylesheet=null;
        if (doc instanceof XSmilesDocumentImpl)
        {
            stylesheet=((XSmilesDocumentImpl)doc).getStyleSheet();
        }
        else
        {
            Log.error(this.toString()+": docElem was not instanceof StylesheetService");
        }

        return stylesheet;
    }
    
    public XMLDocument getXMLDocument()
    {
        return mlfc.getXMLDocument();
    }
    
    public BrowserWindow getBrowser()
    {
        return mlfc.getXMLDocument().getBrowser();
    }
    /**
     * @return url to a browser resource
     */
    public URL getXResource(String id) throws MalformedURLException
    {
        return Browser.getXResource(id);
    }
    
    
    public void destroy()
    {
        //unlistenForHelpKeyPress();
        this.defaultXForm=null;
        this.models=null;
    }
    public void showUserError(Throwable e)
    {
        // TODO: check if XForms exceptino and get more details.
        this.showMessageDialog(true, e.getClass().toString(), e.getMessage(), true, 10000);
    }
    public boolean askUserConfirmation(String title, String prompt)
    {
        GUI gui = mlfc.getXMLDocument().getBrowser().getCurrentGUI();
        XConfirmDialog dialog = gui.getComponentFactory().getXConfirmDialog();
        dialog.setTitle(title);
        dialog.setPrompt(prompt);
        int selected = dialog.select();
        return (selected==XDialog.SELECTION_OK);
    }
    
    public void showMessageDialog(boolean isModal, String title, String message, boolean errorMessage, long timeToLive)
    {
        GUI gui = mlfc.getXMLDocument().getBrowser().getCurrentGUI();
        if (errorMessage)
        {
            gui.showErrorDialog(isModal,title,message);
        }
        else
        {
            gui.showMessageDialog(isModal,title,message,timeToLive);
        }
    }
    
    public void showErrorText(String text)
    {
        getBrowser().getCurrentGUI().setStatusText(text);
    }
    
    public void openLocation(URL uri, short show)
    {
        if (show==XFormsConstants.SHOW_NEW)
        {
            this.getBrowser().newBrowserWindow(new XLink(uri));
        }
        else this.getBrowser().openLocation(uri.toString());
        
    }
    
    /** this method tries to listen to help keypress.
     * Note, that this works currently only in swing
     */
    protected void listenForHelpKeyPress()
    {
        Container cont = this.getMLFC().getContainer();
        this.getBrowser().getComponentFactory().addHelpListener(cont,this);
    }
    
    protected void unlistenForHelpKeyPress()
    {
        Container cont = this.getMLFC().getContainer();
        this.getBrowser().getComponentFactory().removeHelpListener(cont,this);
    }
    
    public void actionPerformed(ActionEvent event)
    {
        Log.debug("ActionPerformed: "+event);
    }
    
    /**
     * @return boolean indicating whether this browser understands a certain element
     */
    public boolean understandElement(Element elem)
    {
        return XMLBroker.isRegistered(elem.getNamespaceURI());
    }
    
    public XSmilesConnection openURLWithGET(URL u) throws Exception
    {
        return this.mlfc.get(u,Resource.RESOURCE_UNKNOWN);
    }
    
    protected Action outermostAction=null;
    /** for deferred updates */
    public void enterAction(Action action)
    {
        if (this.outermostAction==null) this.outermostAction=action;
    }
    
    /** when the outermost action exits, go thru all models and
     * run deferred events */
    public void exitAction(Action action)
    {
        if (action==outermostAction)
        {
            this.outermostAction=null;
            for (Enumeration mod=this.models.elements();mod.hasMoreElements();)
            {
                ModelElementImpl model = (ModelElementImpl)mod.nextElement();
                model.outermostActionExited();
            }
        }
    }
    
    public boolean getInsideAction()
    {
        return (outermostAction!=null);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext#getNodeIndex(java.lang.String)
     */
    public Node getNodeIndex(String id)
    {
            if (nodeindexes==null) return null;
            Node i = ((Node)nodeindexes.get(id));
            if (i==null) return null;
			Log.debug("handler.getNodeIndex:"+id+":"+i);
            return i;
    }
} // XFormsElementHandler
