/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler.Dependency;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatHandler;

import fi.hut.tml.xsmiles.dom.FlowLayoutElement;

import java.util.Vector;

import java.awt.Color;

import org.w3c.dom.*;
import org.w3c.dom.events.*;


import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;

/**
 * The repeat element.
 * @author Mikko Honkala
 */


public class RepeatElementImpl extends DynBoundElementImpl implements //EventListener,
org.w3c.dom.events.EventListener,
PseudoElementContainerService,
//InstanceBaseElementImpl.ChildListener, 
FlowLayoutElement
//IgnoredElementService
{
    
    public final static String CSS_CURSOR_BACKGROUND_COLOR_PROPERTY = "cursor-background-color";
    public final static String CSS_CURSOR_COLOR_PROPERTY = "cursor-color";
    
    /** In init, the current context node */
    protected InstanceNode currentContextNode;
    /** The prototype node */
    protected RepeatHandler repeatHandler;
    
    //protected Node instancePrototype;
    /** The prototype node's parent */
    //protected Node instancePrototypeParent;
    /** The prototype node's insert before */
    //protected Node instancePrototypeInsertBefore;
    
    /** The nodes, which this repeat has added to the DOM */
    //Vector addedNodes=new Vector(20);
    
    
    
    /** the repeat cursors color */
    protected Color cursorFGColor;
    
    /** the repeat cursors color */
    protected Color cursorBGColor;
    
    public RepeatElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    /**
     * In init, the content is created in the dom
     * This is recalled by insert and delete actions
     */
    public void init()
    {
        //Log.debug("Repeat.init() called");
        if (this.elementStatus==INITIALIZED) return;
        initialize();
        super.init();
        this.getHandler().getRepeatIndexHandler().repeatInitialized(this.getRepeatHandler());
    }
    
    protected void initialize()
    {
        this.registerListeners();
        String id = this.getId();
        //Log.debug("Repeat.initialize() called for id: "+id +this.hashCode());
        try
        {
            this.renewBinding();
            //this.setPrototypeNode();
        } catch (Exception e)
        {
            if (e instanceof XFormsException) this.handleXFormsException((XFormsException)e);
            else Log.error(e,"while repeat.initialize()");
            return;
        }
        finally
        {
        inited=true;
        }
        //super.init();
    }
    
    
    public int getBindingType()
    {
        return Dependency.NODESET_BINDING;
    }

    
    public RepeatHandler getRepeatHandler()
    {
        if (this.repeatHandler==null)
        {
            this.repeatHandler=new RepeatHandler(this.getHandler());
            this.repeatHandler.setUIElement(this);
        }
        return this.repeatHandler;
    }
    
    /**
     * Destroy this element and its descendants recursively.
     */
    public void destroy()
    {
        this.unregisterListeners();
        if (this.repeatHandler!=null) this.repeatHandler.destroy();
        super.destroy();
    }
    
        /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        //Log.debug("BINDING HAS CHANGED FOR "+this);
        if (this.getElementStatus()==INITIALIZED&&this.getModel().validation())
        {
            this.getModel().getInstance().getInstanceDocument().validateDocument(false);
        }
        super.notifyBindingChanged(newBinding);
        RepeatHandler rHandler = this.getRepeatHandler();
        if (rHandler!=null)
        {
            rHandler.notifyBindingChanged(newBinding);
        }
    }
    

    
    /** the ancestor may signal thru this method that its binding has changed, and therefore
     * I should renew my context node and binding */
    /*
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor)
    {
        //Log.debug(this +"BLOCKING the call: got event that parents binding has changed. parent: "+ancestor);
    }*/
    
    protected void notifyBindingChangedRecursively(Element elem)
    {
        //Log.debug(this+" BLOCKING the call: bindingChangedRecursively");
    }
    
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
        Log.debug("Repeat got DOM event: "+evt);
        // if the event was targeted to this repeat, set the focus to the first form control
        if (evt.getType()==XFormsEventFactory.FOCUS_EVENT)
        {
            if (evt.getTarget()==this)
            {
                XFormsControl control = findFirstRelevantControl(this);
                if (control!=null)
                    ((EventTarget)control).dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.FOCUS_EVENT));
            }
        }
        else if (evt.getType()==XFormsEventFactory.FOCUSIN_NOTIFICATION_EVENT)
        {
            this.focusEventReceived((UIEvent)evt);
        }
    }
    
    protected void focusEventReceived(UIEvent evt)
    {
            this.getHandler().getRepeatIndexHandler().repeatCaughtFocusEvent(evt,this.getRepeatHandler());
    }
    

    
    protected void registerListeners()
    {
            this.addEventListener(XFormsEventFactory.FOCUS_EVENT, this, false);
            this.addEventListener(XFormsEventFactory.FOCUSIN_NOTIFICATION_EVENT, this, false);
    }

    protected void unregisterListeners()
    {
            this.removeEventListener(XFormsEventFactory.FOCUS_EVENT, this, false);
            this.removeEventListener(XFormsEventFactory.FOCUSIN_NOTIFICATION_EVENT, this, false);
    }
    
	protected Vector pseudoElements;
	//protected ValuePseudoElement valuePseudoElement= new ValuePseudoElement(this);
	public Vector getPseudoElements()
	{
		if (pseudoElements==null)
		{
			pseudoElements=new Vector();
			//pseudoElements.addElement(valuePseudoElement);
		}
		return pseudoElements;
	}
	/** since the pseudoelement removals are not catched by the xsmilesvisualelement, this
	 * method can be used to notify a remove
	 * @param elem
	 */
	public void notifyPseudoRemoved(Element elem)
	{
		super.notifyPseudoRemoved(elem);
	}


    
    public Object clone() throws java.lang.CloneNotSupportedException
    {
        //Log.debug("** Repeat.clone called.");
        RepeatElementImpl repeat = (RepeatElementImpl)super.clone();
        repeat.repeatHandler=null;
        repeat.pseudoElements=null;
        return repeat;
    }
    
}


