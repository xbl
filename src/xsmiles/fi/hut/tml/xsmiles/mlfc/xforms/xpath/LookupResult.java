/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.xpath;


import java.util.Vector;



/**
 * The result of an XPath lookup
 * @author Mikko Honkala
 */


public class LookupResult
{
    public Vector referredNodes;
    public Object result;
    // currently, just a list of function strings...
    public Vector referredFunctions;
}

