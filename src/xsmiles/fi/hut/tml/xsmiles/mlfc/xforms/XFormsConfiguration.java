
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms;

import java.io.Reader;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.DOMInputSource;
import org.xml.sax.InputSource;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.BasicXsiTypeImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.DummySchemaPool;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PSVI;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PSVIDummyImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.SchemaPool;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XsiType;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.xml.JaxpXMLParser;

/**
 * This class holds the current configuration: whether or not to use XercesPSVI,
 * which datatype factory to use, etc.
 * 
 * This class is extended by XFormsConfigurationPSVI, which implements XForms FULL
 * @author honkkis
 *
 */
public class XFormsConfiguration
{
    protected static XFormsConfiguration config;
    protected DataFactory dfact;
    protected PSVI psvi;
    protected static boolean basic=false;
    
    public static final String XPATH_XALAN_CLASS="fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XalanXPathEngine";
    public static final String XPATH_JAXEN_CLASS="fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen.JaxenXPathEngine";
    
    public static String PROPERTY_XPATH_ENGINE="org.xsmiles.xpath.engine";    
    protected static void setRunBasic(boolean b)
    {
        // if its already true, do not set false...
        if (basic==false)
        {    
            basic=b;
            if (basic==true)
            {
               config=null; 
                Log.info("**** RUNNING XFORMS BASIC WITHOUT SCHEMA SUPPORT.");
            }
        }
    }
    
    public static void tryToUsePSVI()
    {
        try
        {
			Log.debug("*** trying to create "+XFormsConstants.XercesPSVIClassName);
            Object c = Class.forName(XFormsConstants.XercesPSVIClassName).newInstance();
			Log.debug("*** created "+XFormsConstants.XercesPSVIClassName);
        } catch (Throwable t)
        {
            setRunBasic(true);
        }
    }
    
    public static XFormsConfiguration getInstance()
    {
        if (config==null)
        {
            tryToUsePSVI();
            if (basic) config=new XFormsConfiguration();
            else
            {
                try
                {
                    config=(XFormsConfiguration)Class.forName(XFormsConstants.XFormsConfigurationPSVI).newInstance();
                }
                catch (Throwable t)
                {
                    config=new XFormsConfiguration();
                }
            }
        }
        return config;
    }
    
    public SchemaPool createSchemaPool() 
    {
            return new DummySchemaPool();
    }
    protected String getDefaultXPathEngineClass()
    {
        return XPATH_JAXEN_CLASS;
    }
    public XPathEngine createXPathEngine(XFormsContext handler,ModelContext mcontext) 
    {
        try
        {
            String engineClass=getDefaultXPathEngineClass();
            String userClass=null;
            XPathEngine e;
            try
			{
            	userClass=System.getProperty(PROPERTY_XPATH_ENGINE);
			} catch (Exception ex1)
			{
				Log.debug("Could not read system property: "+PROPERTY_XPATH_ENGINE);
			}
            if(userClass!=null) engineClass= userClass;
            {
               e=(XPathEngine) Class.forName(engineClass).newInstance();
            }
            e.setContext(handler,mcontext);
            return e;
        } catch (Exception ex)
        {
            Log.error(ex);
            return null;
        }
            
    }

    public PSVI getPSVI() 
    {
        return new PSVIDummyImpl();
    }
    
    public DataFactory getDataFactory()
    {
        if (dfact==null)
        {    
	            dfact=new DataFactory();
        }
        return dfact;
    }
    
    public String getInstanceDocumentClassName()
    {
        return XFormsConstants.InstanceDocumentClassname;
    }
    
    public XsiType createXsiType(String typeNamespace,String typeLocalName,SchemaPool pool)
    {
        return new BasicXsiTypeImpl(typeNamespace,typeLocalName,pool);
    }


    
    public Document loadDocument(Reader r, String baseURL, boolean plain, SchemaPool pool, Object entityResolver,Object errorHandler ) throws Exception
    {
        JaxpXMLParser parser = new JaxpXMLParser();
        InputSource input = new InputSource(r);
        input.setSystemId(baseURL);
        InstanceDocument doc=(InstanceDocument)parser.openDocument(input, this.getInstanceDocumentClassName());
        
        if (doc!=null)
            doc.setSchemaPool(pool);
        
        return doc;
    }
    
    public boolean shouldHandleXsiType()
    {
        // FOR XFORMS BASIC WE SHOULD HANLDE XSI TYPE BUT FOR FULL
        // THE PARSER HANDLES IT
        return true;
    }
    
    /** this is returned to the XPath function for conformance level */
    public String getConformanceLevel()
    {
        return "basic";
    }
    
    
    
    
    public Calendar getCalendarFromSchemaString(String str)
    {
        return new GregorianCalendar();
    }
    
    /*
    public boolean shouldStartDialogUI()
    {
        return true;
    }
    */
    

    
    

}
