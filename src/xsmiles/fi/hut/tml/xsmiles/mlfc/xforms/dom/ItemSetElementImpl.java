/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;



import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler.Dependency;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.ChildListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceParentNode;
//import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.PSVIInstanceElementImpl;

import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.Vector;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

/**
 * ItemSet element
 * @author Mikko Honkala
 */

public class ItemSetElementImpl extends DynBoundElementImpl
implements ChildListener
{
    
    protected Vector items;
    
    public static short UNINITIALIZED = 0;
    public static short INITIALIZED = 1;
    protected short itemsetState = UNINITIALIZED;
    public ItemSetElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    /** override normal init, because we modify the DOM */
    public void init()
    {
        try
        {
            this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.itemsetState=INITIALIZED;
            this.handleXFormsException(e);
        }
        this.itemsetState=INITIALIZED;
        //		this.createItems();
    }
    
    public void destroy()
    {
        this.unregisterListeners();
        super.destroy();
    }
    
    protected void registerListeners()
    {
        // listen for additions & removal of instance nodes
        // TODO: DynBoundElementImpl should handle this?
        if (this.getBoundNodeset()!=null&&this.getBoundNodeset().getLength()>0)
        {
            Node n = this.getBoundNodeset().item(0);
            if (n!=null && n instanceof InstanceNode)
            {
                InstanceParentNode elem = (InstanceParentNode)n.getParentNode();
                elem.addChildListener(this);
            }
        }
        
    }
    
    public int getBindingType()
    {
        return Dependency.NODESET_BINDING;
    }

    
    protected void unregisterListeners()
    {
        if (this.getBoundNodeset()!=null&&this.getBoundNodeset().getLength()>0)
        {
            Node n = getBoundNodeset().item(0);
            if (n!=null && n instanceof InstanceNode)
            {
                InstanceParentNode elem = (InstanceParentNode)n.getParentNode();
                elem.removeChildListener(this);
            }
        }
    }
    
    
    void createItems() throws XFormsBindingException
    {
        //if (this.itemsetState==UNINITIALIZED) this.init();
        // TODO: create the items, and put them in a vector
        items=new Vector(10);
        Log.debug("Itemset.initialize() called.");
        this.getBoundNodeset();
        if (this.getBoundNodeset()!=null)
        {
            this.registerListeners();
            if (getBoundNodeset().getLength()<1)
            {
                // the owner select must be notified also in this case, since it might need to empty the selection box
                this.dispatch(XFormsEventFactory.createEvent(XFormsEventFactory.SUBTREE_MODIFY_START));
                this.dispatch(XFormsEventFactory.createEvent(XFormsEventFactory.SUBTREE_MODIFY_END));
            }
            else
            {
	            for (int i=0;i<getBoundNodeset().getLength();i++)
	            {
	                Node cNode = getBoundNodeset().item(i);
	                // create a new item
	                ItemElementImpl item = new ItemElementImpl(this.getHandler(),XFORMS_NS,ITEM_ELEMENT);
	                item.setCreatedByItemset(true);
	                items.addElement(item);
	                // set its context node, because item is created outside the DOM
	                item.setContextNode((InstanceNode)cNode);
	                // copy my children to item
	                Node child = this.getFirstChild();
	                while (child!=null)
	                {
	                    Node copy = child.cloneNode(true);
	                    item.appendChild(copy);
	                    child=child.getNextSibling();
	                }
	                // append init temporarely into tree (e.g. to get right namespace context)
	                this.appendChild(item);
	                // initialize item
	                //item.setContextNode(this.refNode);
	                item.init();
	                //this.debugNode(item);
	                // remove item from tree
	                item.getParentNode().removeChild(item);
	                
	            }
            }
        }
    }
    /** this method returns the items created by this itemset */
    public Vector getItems()
    {
        try
        {
            if (items==null) this.createItems();
            return items;
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
            return null;
        }
    }
    
    public void childEvent(boolean added, Element parent, Node child)
    {
        this.items=null;
        unregisterListeners();
        // this way the select can change it's view
        boolean result=false;
        result = this.dispatch(XFormsEventFactory.createEvent(XFormsEventFactory.SUBTREE_MODIFY_START));
        result = this.dispatch(XFormsEventFactory.createEvent(XFormsEventFactory.SUBTREE_MODIFY_END));
    }
    
        /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        super.notifyBindingChanged(newBinding);
        Log.debug(this+" binding changed, building itemset");
        try
        {
            this.createItems();
        } catch (XFormsBindingException e)
        {
            Log.error(e);
        }
    }
    

}
