
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;


import org.w3c.dom.DOMException;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.apache.xerces.dom.events.EventImpl;


import org.w3c.dom.xforms10.XFormsElement;


//import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.DocumentImpl;


import javax.xml.transform.TransformerException;

/**
 * An element, which allows context node changes to propagate in the DOM tree.
 *
 * @author Mikko Honkala
 */

public interface ElementWithContext extends org.w3c.dom.Element
{
    /** an descendant asks for its context */
    public InstanceNode getContextForDescendant();
    
    /** notify that I should re-initialize my context node */
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor);//(InstanceNode newContext);
    
    /** get this elements model (look for parents if model attribute not found) */
    public ModelElementImpl getModel();
    
    public short getBindingState();
    
    /** renew my bindings */
    public void renewBinding();
    
    /** reset my context node */
    public void resetContextNode();
    
    /** check if my binding has changed */
    public void checkBinding();
    
    /** is this element bound. Not all elements are always bound, such as group without ref */
    public boolean hasBindingAttributes();
    
    /** */
    public   InstanceNode getRefNode();

    /** */
    public  String getTextWithPrecedence() throws XFormsLinkException;
    
    
    
}
