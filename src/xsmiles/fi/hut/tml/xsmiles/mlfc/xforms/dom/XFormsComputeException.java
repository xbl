/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.MLFCException;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;


/**
 * XForms compute exception
 * @author Mikko Honkala
 */
public class XFormsComputeException extends XFormsException {
    Node relatedNode;
    public XFormsComputeException(String reason, Node relNode)
    {
        super("Related node: "+relNode.getNodeName()+"\n"+XFormsElementImpl.serializeNode(relNode)+"\n"+reason);
        this.relatedNode=relNode;
    }
        
    public XFormsComputeException(String reason)
    {
        super(reason);
        Log.error(this+" this method should not be called");
    }

}
