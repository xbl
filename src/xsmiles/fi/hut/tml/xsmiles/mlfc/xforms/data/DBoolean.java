/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;
import fi.hut.tml.xsmiles.Log;

public class DBoolean extends DData implements Data{

    public static final String SCHEMA_TRUE = "true";
    public static final String SCHEMA_FALSE = "false";
    
    protected Boolean booleanValue;
	public DBoolean(short dtype)
	{
        super(dtype);
	}
	
    /**
     * set the value as a Java Object
     */ 
    protected void setValueFromObjectInternal(Object obj)
    {
        if (obj instanceof Boolean)
        {
            booleanValue = (Boolean)obj;
        }
        else
        {
            Log.error(this+" got wrong type of value");
        }
    }
    
    /**
     * set the value from a display string
     */ 
    protected boolean setValueFromDisplayInternal(String displayValue)
    {
        Log.error(this+ "cannot set from display value");
        return true;
    }
    
    /**
     * set the value from a Schema string
     */ 
    protected void setValueFromSchemaInternal(String displayValue)
    {
        if (displayValue.equals("true")||displayValue.equals("1")) this.booleanValue=Boolean.TRUE;
        else this.booleanValue=Boolean.FALSE;
    }

    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    protected String toDisplayValueInternal()
    {
        return this.booleanValue.toString();
    }
    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    protected String toSchemaStringInternal()
    {
        if (this.booleanValue.equals(Boolean.TRUE)) return SCHEMA_TRUE;
        else return SCHEMA_FALSE;
    }
    
    /**
     * get the java object corresponding to the value
     */
    protected Object toObjectInternal()
    {
        return this.booleanValue;
    }
    /** instructs subclass to set value to null, this is used for
     * invalid values
     */
    protected void clearValue()
    {
        this.booleanValue=null;
    }
} 