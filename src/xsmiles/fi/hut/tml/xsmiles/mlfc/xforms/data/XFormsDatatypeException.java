/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.data;

import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsException;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;
import fi.hut.tml.xsmiles.mlfc.MLFCException;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;
import org.w3c.dom.Element;


/**
 * XForms binding exception
 * @author Mikko Honkala
 */
public class XFormsDatatypeException extends XFormsException {
    protected Element xformsElement;
	public XFormsDatatypeException(Element affectedNode, String error) 
    {
        super("Node: "+affectedNode.getNodeName()+
            " binding failed because of wrong datatype. "+error);
        this.xformsElement = affectedNode;
    }
    public XFormsDatatypeException(String reason)
    {
        super(reason);
        throw new RuntimeException("Never call this method: 1001");
    }
    public Element getXFormsElement()
    {
        return xformsElement;
    }
}
