/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.GUI;

import java.util.Hashtable;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.FlowLayout;
import java.awt.TextComponent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.*;


import org.w3c.dom.*;
import org.xml.sax.SAXException;    
    
import fi.hut.tml.xsmiles.gui.components.XText;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

/**
 * Superclass for text component form controls
 * @author Mikko Honkala 
 */

public abstract class TextControl extends XFormsControl implements TextListener {
	protected boolean isInputComponent=true;
	protected boolean isReadOnly=false;
	protected XText textcomponent;

	/**
	 * Constructs a new text control
	 *
	 * @param my_handler 	The handler for this control
	 * @param my_elem 			The DOM element of this control
	 * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
	 */
	public TextControl(XFormsElementHandler owner, String ns, String name)
	{
		super(owner, ns, name);
		//defaultComponentColor=java.awt.Color.white;
	}

	public void changeComponentValue(String newValue)
	{
		//			Log.debug("Textbox changing value to: "+newValue);
		// return, if the value is the same as in the component currently
                if (newValue!=null && textcomponent!=null)
                {
                    if (newValue.equals(textcomponent.getText())) return;
                    // we remove the change listener while changing the component value
                    //this.textcomponent.getDocument().removeDocumentListener(this);
                    // TODO: remove textlistener?
                    textcomponent.setText(newValue);
                    //this.textcomponent.getDocument().addDocumentListener(this);
                }
	}

	public void destroy()
	{
		//if (this.textcomponent!=null) this.textcomponent.removeFocusListener(this);
		if (this.textcomponent!=null)
		{
			//this.textcomponent.getDocument().removeDocumentListener(this);
			this.textcomponent.removeTextListener(this);
			this.textcomponent=null;
		}
		super.destroy();
	}

	public void setReadonly(boolean ro)
	{
		super.setReadonly(ro);
		isReadOnly = true;
		GUI gui = getXMLDocument().getBrowser().getCurrentGUI();
		gui.hideKeypad();
	}

	/**
	  * Sets the content components font
	  */
	/*
	protected void setContentFont(Font f)
	{
		contentComp.setFont(f);
		textcomponent.setFont(f);
	}

	
	protected void setDefaultSize()
	{
		if (minimumSize!=null) this.setMinimumSize();
		super.setDefaultSize();
	}*/
/*
	public void focusGained(FocusEvent ae)
	{
		super.focusGained(ae);
		if( isReadOnly == false ) {
			GUI gui = getXMLDocument().getBrowser().getCurrentGUI();
 */
			/* This is fairly experimental; inputMode spec in XForms WD
			 * doesn't say anything about languages, just unicode scripts 
			 * and character cases. As the input language is most useful 
			 * in our case, however, find any xml:lang in effect for this 
			 * block and assume that's also the input language. As XML 
			 * spec doesn't specify or anticipate this kind of usage, 
			 * prepend it to our input-mode string, giving uniform use for 
			 * these modifiers.
			 * Note that this is potentially wrong: Nothing says that an 
			 * english form for example can't take names etc. in other 
			 * languages, and if language is later added as inputMode 
			 * modifier, this code won't be entirely compatible.
			 */
/*
    String inputMode = getAttribute(INPUTMODE_ATTRIBUTE);
			Node aNode = this;
			while( aNode != null ) {
				if( aNode.getNodeType() == Node.ELEMENT_NODE )
					if( ((Element)aNode).getAttribute("xml:lang") != "" ) break;
				aNode = aNode.getParentNode();
			}
			if( aNode != null ) {
				inputMode = inputMode + " " + ((Element)aNode).getAttribute("xml:lang");
			}
			gui.displayKeypad(ae.getComponent(), inputMode);
		}
	}
*/
    public void focusLost(FocusEvent ae)
	{
		updateInstance(false);
		super.focusLost(ae);
        /*
		GUI gui = getXMLDocument().getBrowser().getCurrentGUI();
		gui.hideKeypad();
         */
	}
	
    /** notifies the listener that the binding and the value changed  */
	/*
    public void notifyBindingChanged(NodeList newBinding)
    {
		this.updateInstance(false);
		super.notifyBindingChanged(newBinding);
    }*/

	protected void updateInstance(boolean valueChanging)
	{
		try
		{
			// Let's update the corresponding XML data
			if (textcomponent==null) return;
			String text = textcomponent.getText();
			insideUpdateEvent=true;
			this.setRefNodeValue(text,valueChanging);
			//				this.getModel().getSchema().revalidate();
		} catch (Exception e)
		{
			Log.error(e);
		}
		insideUpdateEvent=false;
	}
	
    /** the refresh processing notifies, that the binding might have changed, and it needs to be re-evaluated */
    public void bindingMaybeDirty()
    {
    	// this is a kludge. For some reason, when the focus is in a input 
    	// control, and the user clicks something that makes that control 
    	// rewire, the focus lost event becomes latest, and then the control 
    	// has already been rewired, thus losing the current value. Note that this is only for controls where text is typed in textarea and input:string.
    	this.updateInstance(false); 
    	super.bindingMaybeDirty();
    }

	
	protected void registerListener()
	{
		super.registerListener();
		// register document listener
		textcomponent.addTextListener(this);
	}
	// TEXT LISTENER METHODS
	public void textValueChanged(TextEvent e) 
	{
		this.notifyIncrementalChange(e);
	}
	public void notifyIncrementalChange(TextEvent e)
	{
		if (this.incremental) this.updateInstance(true);
	}
/*	
		public void changedUpdate(DocumentEvent e)
	{
		notifyIncrementalChange(e);
	}
	public void insertUpdate(DocumentEvent e)
	{
		notifyIncrementalChange(e);
	}
	public void removeUpdate(DocumentEvent e)
	{
		notifyIncrementalChange(e);
	}
*/

}

