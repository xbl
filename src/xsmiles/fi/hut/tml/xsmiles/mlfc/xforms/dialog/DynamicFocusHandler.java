/*
 * /* X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources. Created on Nov 9, 2004
 *  
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringReader;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.VisualElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.XSmilesDialog;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DBoolean;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DDecimal;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DString;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.GroupElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.GroupElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.MessageElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.RepeatItem;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SubmitElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TextAreaElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TriggerElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.DDate;
import fi.hut.tml.xsmiles.mlfc.xmlcss.xhtml.XHTMLElement;

/**
 * @author honkkis
 *  
 */
public class DynamicFocusHandler implements FocusHandler, ActionListener, EventListener
{

    protected DialogUI ui;
    protected XSmilesDialog dialog;
    protected Document document;
    protected SpeechWidget currentFocus;
    protected Grammar grammar;
    private boolean submissionGoingOn=false;
    protected boolean searchedForGlobalSubmit=false;
    protected SubmitElement globalSubmit=null;
 
    public DynamicFocusHandler(Document doc, DialogUI dui, XSmilesDialog dia)
    {
        this.ui = dui;
        this.dialog = dia;
        this.document = doc;
        // TODO: for HTML, e.g. <body> should be the focus root
        this.setFocus(this.getRootFocus());
        this.addListeners();
    }
    public static final String DOMFocusInEvent = "DOMFocusIn";
    protected void addListeners()
    {
        this.ui.addActionListener(this);
        try
        {
            EventTarget docElem =(EventTarget)this.document.getDocumentElement(); 
            docElem.addEventListener(
                    XFormsConstants.XSMILES_MESSAGE_SHOWN_EVENT,
                    this,
                    false
            );
            docElem.addEventListener(
                    DOMFocusInEvent,
                    this,
                    false
            );
            docElem.addEventListener(
                    XFormsConstants.SUBMIT_STARTED_EVENT,
                    this,
                    false
            );
        } catch (Exception e)
        {
            Log.error(e);
        }
        
    }
    protected void removeListeners()
    {
        this.ui.removeActionListener(this);
        try
        {
            EventTarget docElem =(EventTarget)this.document.getDocumentElement(); 
            docElem.removeEventListener(
                    XFormsConstants.XSMILES_MESSAGE_SHOWN_EVENT,
                    this,
                    false
            );
            docElem.removeEventListener(
                    DOMFocusInEvent,
                    this,
                    false
            );
            docElem.removeEventListener(
                    XFormsConstants.SUBMIT_STARTED_EVENT,
                    this,
                    false
            );
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    public void destroy()
    {
        this.removeListeners();
    }

    public void speakCurrentFocus()
    {
        // TODO: speak current focus through the UI
        //this.ui.append(this.currentFocus.getLabel());
        this.ui.append(this.currentFocus.generateDialogQuestion() +getGeneralPrompt() + '\n');
    }
    
    public String getGeneralPrompt()
    {
        return "";
        /*
        String submit="";
        SubmitElement gs = this.getGlobalSubmit();
        if (gs!=null)
        {
            submit = "\""+gs.getLabelAsText()+"\", ";
        }
        return submit+"\"back\"";
        */
    }

    public void installGrammar()
    {
        //Vector grammars=new Vector();
        //grammars.addElement(this.currentFocus.generateGrammar(false));
        //String grammar=GrammarGenerator.addHeader(grammars.elements());
        SubmitElement gs = this.getGlobalSubmit();
        /* remember to handle these at actionPerformed */
        String[] commands = {gs!=null?gs.getLabelAsText():null,"back","previous","browser back","browser forward",
                "browser reload","help","browser help","browser home"};
        grammar = new Grammar(commands); // the widget will add new commands
        this.currentFocus.generateGrammar(grammar);
        String grammarString = grammar.toString();
        Log.debug("Grammar is: " + grammar);
        this.ui.setGrammar(new StringReader(grammarString));
    }

    protected boolean isDocumentAlive()
    {
        //if (this.dialog.getBrowserWindow().getState() != BrowserLogic.READY)
          //  return false;
        if (this.document instanceof ExtendedDocument)
        {
            ExtendedDocument extdoc = (ExtendedDocument) this.document;
            XSmilesElementImpl docElem = (XSmilesElementImpl) extdoc.getDocumentElement();
            if (docElem == null || docElem.getElementStatus() == XSmilesElementImpl.DESTROYED)
                return false;
        }
        return true;
    }
    
    /** searches the Element and it's subtree for single submit element, and returns it
     * . If there are 0 or more than 1 submit elements, returns null
     * @param root
     * @return
     */
    protected static SubmitElement findGlobalSubmitElement(Element root)
    {
        NodeList l = root.getElementsByTagNameNS(XFormsConstants.XFORMS_NS,XFormsConstants.SUBMIT_ELEMENT);
        if (l!=null)
        {
            if (l.getLength()==1)
            {
                return (SubmitElement)l.item(0);
            }
        }
        return null;
    }
    
    
    /** searches the Element and it's subtree for single submit element, and returns it
     * . If there are 0 or more than 1 submit elements, returns null
     * @param root
     * @return
     */

    protected SubmitElement getGlobalSubmit()
    {
        if (!searchedForGlobalSubmit)
        {
            this.globalSubmit=this.findGlobalSubmitElement(this.document.getDocumentElement());
            this.searchedForGlobalSubmit=true;
        }
        return this.globalSubmit;
    }

    /**
     * this is called by the dialog system to explicitly set the focus to some
     * element If the element is an ComponentService, this will also set the GUI
     * focus to that component
     */
    public void setFocus(Element elem)
    {
        // TODO: check that the document is still active
        if (!(this.isDocumentAlive())&&(!submissionGoingOn))
            return;
        /*
        if ((!this.isActiveElement(elem)) || (!this.isElementVisible(elem)) )
        {
            Element parentFocus = this.getParentFocus(elem);
            if (parentFocus!=elem&&parentFocus!=null) this.setFocus(parentFocus);
        }*/
        if (elem instanceof GroupElement)
        {
            this.currentFocus = new GroupSpeechWidget(elem, this);
        }
        else if (elem instanceof SelectElement)
        {
            this.currentFocus = new SelectSpeechWidget(elem, this);
        }
        else if (elem instanceof SubmitElement)
        {
            this.currentFocus = new TriggerSpeechWidget(elem, this);
        }
        else if (elem instanceof TriggerElement)
        {
            this.speak(this.getLastReply());
            ((TriggerElement)elem).dispatchActivateEvent();
            ((TriggerElement)elem).dispatchClickEvent();
            this.setFocus(this.getParentFocus(elem));
            return;
        }
        else if (elem instanceof TypedElement)
        {
            TypedElement telem = (TypedElement) elem;
            Data data = telem.getData();
            Log.debug("TypedElement: " + telem + " data:" + data);
            if (!telem.isWritable())
            {
                // this is an output element
	            if (data instanceof DDecimal)
	                this.currentFocus = new IntegerOutputWidget(elem, this);
	            else if (data instanceof DString)
	                this.currentFocus = new StringOutputWidget(elem, this);
                
            }
            else
            {
                // this is an input element
	            if (data instanceof DDecimal)
	                this.currentFocus = new IntegerSpeechWidget(elem, this);
	            else if (data instanceof DDate)
	                this.currentFocus = new DateSpeechWidget(elem, this);
                else if (data instanceof DBoolean)
                    this.currentFocus = new BooleanSpeechWidget(elem, this);
	            else if (data instanceof DString)
	                this.currentFocus = new StringSpeechWidget(elem, this);
            }
        }
        else if (elem instanceof TextAreaElement)
        {
            this.currentFocus=new StringSpeechWidget(elem,this);
        }
        else if (elem instanceof XHTMLElement || elem instanceof VisualElement)
        {
            this.currentFocus=new BranchSpeechWidget(elem,this);
        }
        else
            this.currentFocus = new BaseSpeechWidget(elem, this);
        if (elem instanceof VisualComponentService)
        {
            ((VisualComponentService) elem).getComponent().requestFocus();
        }
        this.speakCurrentFocus();
        this.installGrammar();
    }
    
    private void sleepLittle()
    {
        try
        {
            Thread.sleep(1200);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }

    public boolean isElementVisible(Node n)
    {
        Element e = (Element)n;
        if (e instanceof VisualElement)
        {
            if (((VisualElement) e).isCurrentlyVisible() == false)
                return false;
            else if  (!this.isElementSpeakable(e))
            {
                return false;
            }

        }
        if (e instanceof VisualComponentService)
        {
            if (((VisualComponentService) e).getVisible() == false)
                return false;
        }
        return true;
    }
    
    private boolean isElementSpeakable(Element e)
    {
        if (e instanceof VisualElement)
        {
            String speak =((VisualElement)e).getStyle().getPropertyValue("speak");
            if (speak!=null)
            {
                if (speak.equalsIgnoreCase("none"))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public Element getNextFocus(Element e)
    {
        // TODO:
        return null;
    }

    public Element getPreviousFocus(Element e)
    {
        // TODO:
        return null;
    }

    public Element getRootFocus()
    {
        // TODO: find next branch element
        return this.findNextBranchElement(this.document.getDocumentElement());
        //return this.document.getDocumentElement();
    }

    protected Element findNextBranchElement(Element root)
    {
        /*
         * TODO: from root element move down to the first element, which has
         * more than one ancestor selections e.g. a group, which has more than
         * one ancestor select
         */
        Vector ancestors = this.getFocusPoints(root);
        if (ancestors.size() == 1)
            return findNextBranchElement((Element) ancestors.elementAt(0));
        else
            return root;
    }

    /**
     * returns: vector of focus points Operation: Doa depth first - search and
     * stop at any focus point and add it to the focus point list. Only search
     * ancestors. Ignore items with only one choice. (e.g. a group with only one
     * ancestor focus point)
     */
    public Vector getFocusPoints(Element e)
    {
        Vector v = new Vector(5);
        return this.getFocusPoints(e, v);
    }

    protected Vector getFocusPoints(Element e, Vector v)
    {
        NodeList children;
        if (e instanceof XSmilesElementImpl)
        {
            children=((XSmilesElementImpl)e).getChildNodes(true);
        }
        else children=e.getChildNodes();
        for (int i=0;i<children.getLength();i++)
        {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE)
            {
                Element childEl = (Element) child;
                if (this.isElementVisible(childEl))
                {
                    if (this.isActiveElement(childEl))
                        v.addElement(childEl);
                    else
                    {
                        // if the child was not active element, try its children
                        this.getFocusPoints(childEl, v);
                    }
                }
            }
        }
        return v;
    }

    protected boolean isActiveElement(Node el)
    {
        if (el instanceof VisualElementImpl)
        {
            VisualElementImpl e = (VisualElementImpl) el;
            if (el instanceof XFormsControl)
                return true;
            // TODO: group without active choise inside shoud be nonactive?
            else if (e instanceof GroupElementImpl)
                return true;
            else if (e instanceof RepeatItem)
            {
                return true;
            }
        }

        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.FocusHandler#getCurrentFocus()
     */
    public Element getCurrentFocus()
    {
        // TODO Auto-generated method stub
        return this.currentFocus.getElement();
    }

    private String lastReply;
    public String getLastReply()
    {
        return lastReply;
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    
    
    public void actionPerformed(ActionEvent arg0)
    {
    	if (arg0 instanceof DialogActionEvent) return;
        Log.debug("DynFocusHandler: Speech action performed: " + arg0);
        String command = arg0.getActionCommand();
        lastReply=command;
        Log.debug("Got command:" + command);
        if (command.equalsIgnoreCase("again"))
        {
            this.speakCurrentFocus();
        }
        else
        {
            Response r = new Response();
            r.response = command.trim();
            if (this.currentFocus != null)
            {
                // first, give the command to the currently focused widget
                if (!this.currentFocus.interpretResponse(r))
                {
                    // if the widget failed to interpret this, try here
                    if (r.response.equalsIgnoreCase("back") || r.response.equalsIgnoreCase("previous"))
                    {
                        this.speak(this.getLastReply());
                        ((BaseSpeechWidget) currentFocus).moveFocusToParent();
                    }
                    // the shortcut to the global submit button, if any
                    else if (
                            this.getGlobalSubmit()!=null &&
                            r.response.equalsIgnoreCase(this.getGlobalSubmit().getLabelAsText().trim()) &&
                            this.isElementVisible((Element)this.getGlobalSubmit())
                            )
                    {
                        this.doGlobalSubmit();
                    }
                    /* remember to add these also to the grammar! installGrammar() */
                    else if (r.response.equalsIgnoreCase("browser back"))
                    {
                        this.speak(this.getLastReply());
                        this.dialog.getBrowserWindow().navigate(NavigationState.BACK);
                    }
                    else if (r.response.equalsIgnoreCase("browser forward"))
                    {
                        this.speak(this.getLastReply());
                        this.dialog.getBrowserWindow().navigate(NavigationState.FORWARD);
                    }
                    else if (r.response.equalsIgnoreCase("browser reload"))
                    {
                        this.speak(this.getLastReply());
                        this.dialog.getBrowserWindow().navigate(NavigationState.RELOAD);
                    }
                    else if (r.response.equalsIgnoreCase("browser home"))
                    {
                        this.speak(this.getLastReply());
                        this.dialog.getBrowserWindow().navigate(NavigationState.HOME);
                    }
                    else if (r.response.equalsIgnoreCase("browser help")||r.response.equalsIgnoreCase("help"))
                    {
                        this.speak(this.getLastReply());
                        this.speak("Browser help: Available general commands are: \"browser back\",\"browser forward\",\"browser reload\", \"browser home\",and \"browser help\"");
                        this.speakCurrentFocus();
                    }
                    else
                    {
                        this.speak("Did not understand: "+r.response);
                        boolean approximate = this.currentFocus.approximateResponse(r);
                        if (approximate==false) this.speakCurrentFocus();
                    }
                }
            }
        }
    }

    /**
     * 
     */
    private void doGlobalSubmit()
    {
        SubmitElement submit = this.getGlobalSubmit();
        // submit should never be null here, but just in case
        if (submit!=null)
        {
            this.setFocus((Element)submit);
        }
        else
        {
            Log.error("Global submit was null in doGlobalSubmit");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.FocusHandler#getParentFocus(org.w3c.dom.Element)
     */
    public Element getParentFocus(Element e)
    {
        // TODO Auto-generated method stub
        Node parent = e.getParentNode();
        if (parent == null || parent.getNodeType() == Node.DOCUMENT_NODE)
        {
            return e;
        }
        else if (this.isActiveElement(parent)&&this.isElementVisible(parent))
        {
            return (Element) parent;
        }
        else
        {
            return this.getParentFocus((Element) parent);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.FocusHandler#speak(java.lang.String)
     */
    public void speak(String s)
    {
        if (s!=null&&s.length()>0)
            this.ui.append(s + '\n');
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.events.EventListener#handleEvent(org.w3c.dom.events.Event)
     */
    public void handleEvent(Event ev)
    {
        Log.debug("DynamicFocusHandler got event: "+ev.getType());
        if (ev.getType().equals(DOMFocusInEvent))
        {
            try
            {
                Element el = (Element)ev.getTarget();
                if (this.isActiveElement(el)&& this.isElementVisible(el))
                {
                    Log.debug("Active element got focus: "+el);
                    if (!(el instanceof TriggerElement))
                    {
                        this.setFocus(el);
                    }
                }
            }
            catch (Exception e)
            {
                Log.error(e);
            }
        }
        else if (ev.getType().equals(XFormsConstants.XSMILES_MESSAGE_SHOWN_EVENT))
        {
            if (ev.getTarget() instanceof MessageElement)
            {
                MessageElement message = (MessageElement)ev.getTarget();
                String messageStr = message.getMessageAsString().trim();
                if (messageStr!=null&&messageStr.length()>0)
                {
                    this.speak(message.getMessageTypeAsString()+": "+messageStr);
                }
                
            }
        }
        else if (ev.getType().equals(XFormsConstants.SUBMIT_STARTED_EVENT))
        {
            this.submissionGoingOn=true;
            this.ui.stopAll();
        }
    }
}