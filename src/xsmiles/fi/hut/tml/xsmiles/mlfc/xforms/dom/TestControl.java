/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;
import fi.hut.tml.xsmiles.gui.components.XCaption;

import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;

import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;

//import javax.swing.*;

import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.w3c.dom.css.*;

import org.xml.sax.SAXException;

import org.apache.xpath.objects.*;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XInput;

/**
 * The base class for the XForm controls, such as buttons and textboxes.
 * It uses the abstract component model of X-SMiles in the package
 * fi.hut.tml.xsmiles.gui.components
 *
 * @author Mikko Honkala
 */

public class TestControl extends VisualElementImpl implements VisualComponentService
{    
    protected Component component = null;//new JTextField("tesasdfsadsadfasdf t");
    //protected JComponent component = new JCheckBox("lsjkdflkjsdflkjsdljfksdlfjlk sdf");
    //protected JComponent component = new JTextArea("lsjkdflkjsdflkjsdljfksdlfjlk sdf");
    
            
	public TestControl(org.apache.xerces.dom.DocumentImpl ownerDocument, String namespaceURI, String qualifiedName) 
	{
		super(ownerDocument,namespaceURI, qualifiedName);
	}/**
     * Returns the visible component of this control
     */
    public Component getComponent()
    {
        //return component.getComponent();
        return component;
    }
    
    /**
     * Returns the approximate size of this extension element
     */
    public Dimension getSize()
    {
        return component.getSize();
    }
    public void setZoom(double zoom)
    {
    }
    public void setVisible(boolean visible)
    {
    }
    public boolean getVisible()
    {
        return true;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
}
