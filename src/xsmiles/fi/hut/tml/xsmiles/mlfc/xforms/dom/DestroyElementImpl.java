/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;


/**
 * XForm/insert element implementation. This is XForms 1.1
 * @author Mikko Honkala
 */


public class DestroyElementImpl extends ActionHandlerBaseImpl
{
    /** the prototype element for insert into empty situations */
    protected Element prototypeInstanceElement;
    
    public DestroyElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        this.renewBinding();
        //if (this.prototypeInstanceElement==null) this.savePrototypeElement();
    }

    

    
    public void activate(Event evt)
    {
        NodeList nodes = null;
        nodes = this.getBoundNodeset();
        
        // TODO: check that index is at bounds
        Log.debug("destroy: ");
        try
        {
            InstanceElementImpl instance=this.getModel().getInstance();
			if (this.getRefNode()!=null && this.getRefNode().getParentNode()!=null)
			{
                Node rNode = this.getRefNode();
                XFormsUtil.dumpDOM(false,rNode instanceof Element ? (Element)rNode:(Element)rNode.getParentNode());
				rNode.getParentNode().removeChild(rNode);
				//this.getModel().instanceStructureChanged();
	            // call repaint for the root node of this DOM
	            // TODO: context info
	            instance.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.DESTROY_EVENT));
	            this.getModel().rebuild(true);
			}
        } catch (Exception e)
        {
            this.getHandler().showUserError(e);
            Log.error(e);
        }
    }
	
    
}

