/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;



import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.dom.EventHandlerService;

import org.w3c.dom.events.*;

/**
 * recalculate action implementation
 * @author Mikko Honkala
 */


public class RefreshElementImpl extends ActionHandlerBaseImpl implements EventHandlerService {

	public static final String IDREF_ATTRIBUTE="idref";

	public RefreshElementImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}

	public void activate(Event evt)
	{	
		ModelElementImpl model = this.getModelInScope(this);
		model.refresh();
	}
	

	
}
