/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import java.awt.Dimension;

import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TextAreaElement;

    

/**
 * The XForms/Textarea element
 * @author Mikko Honkala 
 */

public class TextAreaElementImpl extends TextControl implements TextAreaElement {
	
	/** the minimum size for textarea components */
	protected final static Dimension mSize = new Dimension(50,10);


	/**
	 * Constructs a new 'textbox' control
	 *
	 * @param my_handler 	The handler for this control
	 * @param my_elem 			The DOM element of this control
	 * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
	 */
	public TextAreaElementImpl(XFormsElementHandler owner, String ns, String name)
	{
		super(owner, ns, name);
	}
	
	public XComponent createComponent()
	{
		XTextArea textarea = this.getComponentFactory().getXTextArea((String)this.getRefNodeValue());
		textcomponent = textarea;
		textarea.setWordWrapping(true);
		return textcomponent;    
	}

    public void setCurrentTextValue(String text)
    {
        // this comes from the interface TextAreaElement, and possibly from speech 
        this.setRefNodeValue(text,false);
    }
    public String getCurrentTextValue()
    {
        return (String)this.getRefNodeValue();
    }


}

