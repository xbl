/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;




import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

    

/**
 * FileName element
 * @author Mikko Honkala
 */

public class FileNameElementImpl extends DynBoundElementImpl{

	public FileNameElementImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}
	
	public void init()
	{
        try {
    		this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        }
        super.init();
	}
}
