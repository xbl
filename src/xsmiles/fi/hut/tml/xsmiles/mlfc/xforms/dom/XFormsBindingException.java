/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.MLFCException;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;


/**
 * XForms binding exception
 * @author Mikko Honkala
 */
public class XFormsBindingException extends XFormsException {
    protected XFormsElementImpl xformsElement;
	public XFormsBindingException(XFormsElementImpl affectedNode, String bindingExpr, String model) 
    {
        super("Node: "+(affectedNode==null?"":affectedNode.getNodeName())+
            " binding failed. Binding expression was: '"+
            bindingExpr+(model==null?"'":"' on model: '"+model+"'"));
        this.xformsElement = affectedNode;
    }
    public XFormsBindingException(String reason)
    {
        super(reason);
        throw new RuntimeException("Never call this method: 1001");
    }
    public XFormsElementImpl getXFormsElement()
    {
        return xformsElement;
    }
}
