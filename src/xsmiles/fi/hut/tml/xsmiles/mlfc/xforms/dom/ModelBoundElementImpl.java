/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


import org.w3c.dom.DOMException;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.apache.xerces.dom.events.EventImpl;


import org.w3c.dom.xforms10.XFormsElement;


//import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.DocumentImpl;


import javax.xml.transform.TransformerException;

/**
 * An element, which has ref expression in it and is located as a child of model, which means that
 * a) no dynamic bindings are allowed
 * b) model attribute is not allowed.
 * currently only BindElementImpl uses this as a
 * @author Mikko Honkala
 */

public class ModelBoundElementImpl extends XFormsElementImpl implements XFormsConstants, ElementWithContext
{
    /** the current binding state, one of the above */
    protected short binding_state=DynBoundElementImpl.UNINITIALIZED;
    
    /** Repeat may set this context node */
    protected InstanceNode contextNode=null;
    
    /** The nodelist for multiple node binding */
    protected NodeList refNodes=null;
    
    /** used for containing bindings context node */
    protected Node refNode=null;
    
    
    
    
    public ModelBoundElementImpl(XFormsElementHandler handler, String ns, String name)
    {
        super(handler, ns, name);
    }
    
    
    /** returns the current binding state */
    public short getBindingState()
    {
        return this.binding_state;
    }
    
    public NodeList getBoundNodeset()
    {
        return this.refNodes;
    }
    /** reset my context node */
    public void resetContextNode()
    {
    }
    
    
    /** this method will get the context node from an possible ancestor element. Used also by ModelBoundElement. */
    protected void getContextFromAncestor(ElementWithContext refElem) throws XFormsBindingException
    {
        ElementWithContext parent = getParentBoundElement(this, refElem);
        if (parent!=null)
        {
            this.setContextNode((InstanceNode)parent.getContextForDescendant());
        }
    }
    /** Used also by ModelBoundElement. */
    protected static ElementWithContext getParentBoundElement(Node start,ElementWithContext refElem)
    {
        Node n = start.getParentNode();
        
        while (n!=start.getOwnerDocument().getDocumentElement())
        {
            if (n instanceof ElementWithContext)
            {
                return (ElementWithContext)n;
            }
            n=n.getParentNode();
        }
        return null;
    }
    /**
     * Search for my reference nodes
     * this always executes the binding expression!
     */
    public NodeList createBinding() throws XFormsBindingException
    {
        if (this.contextNode==null) getContextFromAncestor(this);
        String refe =null;
        try
        {
            {
                refe = this.getRef();
                if (refe==null)
                {
                    this.binding_state=DynBoundElementImpl.BINDING_ATTRIBUTES_NOT_FOUND;
                    // TODO: check, should we throw an exception here?
                    return null;
                }
                                /* TODO: there is a bug here, the following will not work
                                 <xforms:input model="model1" ref="node">
                                   <xforms:caption model="model2" ref="node2">
                                 since the input will set the context node to the caption, even
                                 when they are in different models -MH
                                 */
                ModelElementImpl form=this.getModel();
                if (form==null)
                {
                    Log.error("No Model with id: "+this.getModelId());
                    this.binding_state=DynBoundElementImpl.BINDING_FAILED;
                    throw new XFormsBindingException(this,"non-existent model: "+this.getModelId(),this.getAttribute(MODEL_ID_ATTRIBUTE));
                }
                // TODO: cache XPathExpr
                XPathExpr xpathE=form.getXPathEngine().createXPathExpression(this.getRef());
                refNodes = form.getXPathEngine().evalToNodelist(contextNode,xpathE,this,null);
                if (refNodes!=null && refNodes.getLength()>0)
                {
                    Node n = refNodes.item(0);
                    if (n instanceof InstanceNode)
                        refNode=(InstanceNode)n;
                    else if (n instanceof Text)
                        refNode=(InstanceNode)n.getParentNode();
                }
            }
            
            if (refNodes==null||refNode==null)
            {
                Log.error("Unknown reference in XForms: "+refe+" in element: "+this.getNodeName());
                //We should make the form control non-relevant
                //throw new XFormsBindingException(this,refe,this.getAttribute(MODEL_ID_ATTRIBUTE));
                this.binding_state = DynBoundElementImpl.BOUND_NODE_NOT_FOUND;
                return null;
                
            }
            else
            {
                this.binding_state = DynBoundElementImpl.BINDING_OK;
                return refNodes;
            }
        } catch (Exception e)
        {
            Log.error(e);
            this.binding_state=DynBoundElementImpl.BINDING_FAILED;
            throw new XFormsBindingException(this,refe,this.getAttribute(MODEL_ID_ATTRIBUTE));
        }
    }
    
    public InstanceNode getContextForDescendant() //throws XFormsBindingException
    {
        return (InstanceNode)this.refNode;
    }
    
    public void destroy()
    {
        super.destroy();
        this.refNodes=null;
        this.refNode=null;
        this.contextNode=null;
    }
    
    public InstanceNode getRefNode()
    {
        //if (refNode==null) this.findAndSetRefNode(this.getRef());
        return (InstanceNode)refNode;
    }
    
    public String getRef()
    {
        // TODO: model binding (at least bind) should not accept ref (this is kept
        // because of old demos
        Attr refAttr=this.getAttributeNode("ref");
        if (refAttr==null)
        {
            refAttr=this.getAttributeNode("nodeset");
        }
        if (refAttr==null) return null;
        return refAttr.getNodeValue();
    }
    
    
    
    void setContextNode(InstanceNode node)
    {
        //Log.debug(this.toString()+":somebody set bound node context node to ");
        //this.debugNode(node);
        this.contextNode=node;
    }
    
    public Node getContextNode()
    {
        if (this.contextNode==null)
        {
            try
            {
                return this.getModel().getInstance().getInstanceDocument().getDocumentElement();
            } catch (NullPointerException e)
            {
                Log.error(e);
                return null;
            }
        }
        return this.contextNode;
    }
    
    public ModelElementImpl getModel()
    {
        Node parent = this.getParentNode();
        while (parent.getNodeType()==Node.ELEMENT_NODE)
        {
            if (parent instanceof ModelElementImpl)
                return (ModelElementImpl)parent;
            parent=parent.getParentNode();
        }
        Log.error("There was a xforms:bind, whose ancestor was not xforms:model");
        return null;
    }
    
    /** notify that I should re-initialize my context node  */
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor)
    {
        // this should never happen within the model, since there is no dynamics there
    }
    
    /** renew my bindings  */
    public void renewBinding()
    {
        Log.debug(this.getClass()+"renewBinding called. Doing nothing.");
    }
    
    /** check if my binding has changed */
    public void checkBinding()
    {
    }
    
    /** is this element bound. Not all elements are always bound, such as group without ref  */
    public boolean hasBindingAttributes()
    {
        return true;
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dom.ElementWithContext#getTextWithPrecedence()
     */
    public String getTextWithPrecedence() throws XFormsLinkException
    {
        // TODO Auto-generated method stub
        return null;
    }
    
}
