/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 10, 2004
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;

import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;

/**
 * @author mpohja
 */
public interface TypedElement extends FormControl
{
    public boolean isInputControl();//input, range are input controls
    public short getDataType();
    public Data getData();
    public void setData(Data data);
}
