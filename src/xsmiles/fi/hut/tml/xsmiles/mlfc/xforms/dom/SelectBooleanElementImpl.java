/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import org.w3c.dom.*;
import org.xml.sax.SAXException;    
    
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;


/**
 * The XForms/selectBoolean element
 * @author Mikko Honkala 
 */

public class SelectBooleanElementImpl extends XFormsControl implements ItemListener{
	protected boolean isInputComponent=true;
	protected XSelectBoolean select;

	/**
	 * Constructs a new select boolean
	 *
	 * @param my_handler 	The handler for this control
	 * @param my_elem 			The DOM element of this control
	 * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
	 */

	public SelectBooleanElementImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}
		
	public XComponent createComponent()
	{
		select = this.getComponentFactory().getXSelectBoolean();
		//select.setCaptionText(this.checkCaption().getCaptionText());
		//select.setCaptionText("");
		this.changeComponentValue((String)this.getRefNodeValue());
		return select;
	}
	protected void registerListener()
	{
		super.registerListener();
		select.addItemListener(this);
	}
	public void destroy()
	{
		select.removeItemListener(this);
		super.destroy();
	}
	/** this comes from the instance */
	public void changeComponentValue(String newValue)
	{
		if (insideUpdateEvent) return; // self-initiated
		try
		{
			select.removeItemListener(this);
			insideUpdateEvent=true;
	//			Log.debug("Checkbox changing value to: "+newValue);
			if (newValue.equals("true"))
				select.setSelected(true);
			else if (newValue.equals("false"))
				select.setSelected(false);
			else Log.error("Checkbox "+this.getAttribute("ref")+" accepts only true/false values");	
		} catch (Exception e)
		{
			Log.error(e);
		}
		finally 
		{
			select.addItemListener(this);
			insideUpdateEvent=false;
		}
	}
		
	/**
	 * This handles the changes from the widget XSelectBoolean
	 */
	public void itemStateChanged(ItemEvent e) {
		//if (insideUpdateEvent) return; // self-initiated
		if (e.getStateChange() == ItemEvent.DESELECTED)
			this.setRefNodeValue("false",false);
		else if (e.getStateChange() == ItemEvent.SELECTED)
			this.setRefNodeValue("true",false);
	}
}
