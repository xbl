
//package org.apache.xpath;
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;


import org.apache.xml.dtm.ref.dom2dtm.*;


import javax.xml.transform.dom.DOMSource;

import org.apache.xml.dtm.DTMManager;
import org.apache.xml.dtm.DTMWSFilter;
import org.apache.xml.utils.XMLStringFactory;
import org.w3c.dom.Node;



public class DOM2DTMEx extends DOM2DTM
{
    public DOM2DTMEx(DTMManager mgr, DOMSource domSource,
    int dtmIdentity, DTMWSFilter whiteSpaceFilter,
    XMLStringFactory xstringfactory,
    boolean doIndexing)
    {
        super(mgr, domSource, dtmIdentity, whiteSpaceFilter,
        xstringfactory, doIndexing);
        //Log.debug("DOM2DTMEx created!");
    }
    /**
     * Return an DOM node for the given node.
     *
     * @param nodeHandle The node ID.
     *
     * @return A node representation of the DTM node.
     */
    public Node getNode(int nodeHandle)
    {
        
        int identity = makeNodeIdentity(nodeHandle);
        Node n = (Node) m_nodes.elementAt(identity);
        // X-Smiles debug
        // TODO: add interface for this
        if (this.m_mgr instanceof DTMManagerEx)
        {
            if (((DTMManagerEx)this.m_mgr).getReferantTracking())
            {
                //System.out.println("DOM2DTMEx.getNode : node:"+n);
                ((DTMManagerEx)this.m_mgr).addReferant(n);
            }
        }
        return n;
    }
    
}
