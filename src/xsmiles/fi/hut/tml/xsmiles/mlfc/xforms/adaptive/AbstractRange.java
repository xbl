/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import org.w3c.dom.Element;

public abstract class AbstractRange extends AbstractControl
{
    protected Element element;
	public AbstractRange(XFormsContext context, Element elem)
	{
        super(context,elem);
        element=elem;
	}
	
} // RangeDecimal
