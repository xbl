/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;

public interface Data {
    
    /*
	public Data()
	{
	}
     */
    
    /**
     * set the value as a schema compatible string 
     * Note! this string should be valid, no validity checking is done
     */
    //public void setSchemaValue(String val);
    
    /**
     * set the value as a Java Object
     */ 
    public void setValueFromObject(Object obj);
    
    /**
     * set the value from a display string
     */ 
    public void setValueFromDisplay(String displayValue);
    
    /**
     * set the value from a Schema string
     */ 
    public void setValueFromSchema(String displayValue);

    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    public String toDisplayValue();
    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    public String toSchemaString();
    
    public short getDataType();
    /**
     * get the java object corresponding to the value
     */
    public Object toObject();
    
    /**
     * is the current value valid according to the datatype
     */
    public boolean isValid();
    
    /**
     * sets the current value valid or invalid
     */
    public void setValid(boolean valid);
    

    /**
     * get the invalid value as a string
     */
    public String getInvalidValue();
    
    /**
     * set the invalid value as a string
     */
    public void setInvalidString(String s);


	
} // RangeDecimal
