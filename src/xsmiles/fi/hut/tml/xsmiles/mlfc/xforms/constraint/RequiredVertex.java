/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


/**
 * The vertex for the calculation algorithm
 * @author Mikko Honkala
 */


public class RequiredVertex extends Vertex {
//	public static final short type=DependencyGraph.RELEVANT_VERTEX;

	public RequiredVertex(InstanceNode n, ExpressionContainer expr,XPathExpr xpathExpr) {
		super(n,expr,xpathExpr);
	}
	public RequiredVertex() {
		super();
	}
	

	public void compute()
	{
		Object result = runXPath();
		if (result!=null)
		{
			
			if (result instanceof Boolean)
			{
				boolean relevant=((Boolean)result).booleanValue();
				this.instanceNode.getInstanceItem().setRequired(relevant);
			}
		}
	}
	public short getVertexType()
	{
		return REQUIRED_VERTEX;
	}

	
	
	
	
}
