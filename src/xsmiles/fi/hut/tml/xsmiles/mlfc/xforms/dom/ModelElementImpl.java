/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.events.MutationEvent;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.MainDependencyGraph;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.SubDependencyGraph;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.Vertex;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler.Dependency;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.XFormsModelElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.FunctionChangeListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceParser;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.SchemaPool;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.util.XSmilesConnection;


/**
 * The model element implementation
 * - ties together instance  and schema
 * - Listens for mutation events from the instance and initiates recalculate and revalidate
 * @author Mikko Honkala
 * @author Ronald Tschal?r (patches)
 */

public class ModelElementImpl extends PrologElement
implements org.w3c.dom.events.EventListener , ModelContext, XFormsModelElement, FunctionChangeListener
{
    
    /** the first instance is the default */
    //protected InstanceElementImpl defaultInstance;
    
    /** all instances with an id are stored in a hashtable */
    protected Hashtable instances=new Hashtable();
    
    protected SchemaElementImpl model;
    protected MainDependencyGraph maindep;
    
    public final static short MODEL_NOTCONSTRUCTED=1;
    public final static short MODEL_CONSTRUCTING=2;
    public final static short MODEL_READY=5;
    public final static short MODEL_REFRESHING=10;
    public final static short MODEL_CALCULATING=20;
    public final static short MODEL_VALIDATING=30;
    
    protected short modelStatus=MODEL_NOTCONSTRUCTED;
    
    /** are we currently running the calculation engine */
    //public boolean calculating=false;
    
    /** are we currently validating the instance */
    //public boolean validating=false;
    
    /** are we currently initializing */
    //public boolean initializing=false;
    
    /** currently invalid nodes */
    protected Vector invalidNodes=new Vector();
    
    /** the changed nodes */
    protected Vector changedNodes = new Vector();
    
    /** the dynamic dependency handler */
    protected DynamicDependencyHandler dynDependencyHandler;// = new DynamicDependencyHandler(this.);
    
    /** my schema pool */
    protected SchemaPool schemaPool;
    /** the cached XPath api context */
    protected XPathEngine xpathAPI;
    
    /** the cached XPath Lookup api context */
    //protected XPathLookup xpathLookup;
    
    /** the cached default instance */
    protected InstanceElementImpl defaultInstance;
    
    /** for deferred updates */
    protected boolean needsRecalculate=false, needsRefresh=false, needsRevalidate=false, needsRebuild=false;
    
    /** for lazy bastard mode, only one generated instance can be found per model.
     * Note that there can only be one instance per model if the lazy bastard instance exists*/
    protected InstanceElementImpl lazyBastardInstance;
    
    public static final String SCHEMA_URI = "http://www.w3.org/2001/XMLSchema";
    
    
    public ModelElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        dynDependencyHandler = new DynamicDependencyHandler(owner,this);

    }
    /*
    public Node insertBefore(Node newChild, Node refChild)
    throws DOMException
    {
        // Add pointers for quick access
        if (newChild instanceof InstanceElementImpl)
        {
            if (defaultInstance==null) defaultInstance=(InstanceElementImpl)newChild;
            Attr idAttr = ((Element)newChild).getAttributeNode("id");
            if (idAttr!=null)
            {
                String id=idAttr.getNodeValue();
                if (id!=null&&id.length()>0)
                {
                    this.instances.put(id, newChild);
                }
            }
            else this.instances.put(this,newChild);
        }
        else if (newChild instanceof SchemaElementImpl)
        {
            model=(SchemaElementImpl)newChild;
        }
        return super.insertBefore(newChild,refChild);
    } // insertBefore(Node,Node):Node
*/
    /** the instance element registers itself using this method */
    public void instanceAdded(InstanceElementImpl newChild)
    {
            if (defaultInstance==null) defaultInstance=(InstanceElementImpl)newChild;
            Attr idAttr = ((Element)newChild).getAttributeNode("id");
            if (idAttr!=null)
            {
                String id=idAttr.getNodeValue();
                if (id!=null&&id.length()>0)
                {
                    this.instances.put(id, newChild);
                }
            }
            else this.instances.put(this,newChild);
    } // insertBefore(Node,Node):Node
    /** the instance element registers itself using this method */
    public void schemaAdded(SchemaElementImpl newChild)
    {
        model=newChild;
    } // insertBefore(Node,Node):Node
    public Node removeChild(Node oldChild) throws DOMException
    {
        // remove pointers from quick access
        this.internalRemove(oldChild);
        return super.removeChild(oldChild);
    }
    public Node replaceChild(Node newChild, Node oldChild) throws DOMException
    {
        // remove pointers from quick access
        this.internalRemove(oldChild);
        return super.replaceChild(newChild,oldChild);
    }
    protected void internalRemove(Node newChild)
    {
        /*
                if (newChild==defaultInstance)
                {
                        defaultInstance=null;
                }
        else
         */
        if (newChild instanceof InstanceElementImpl)
        {
            Attr idAttr = ((Element)newChild).getAttributeNode("id");
            if (idAttr!=null)
            {
                String id=idAttr.getNodeValue();
                if (id!=null&&id.length()>0)
                {
                    this.instances.remove(id);
                }
            }
            if (newChild==defaultInstance) defaultInstance = this.findFirstInstance();
        }
        else if (newChild==model)
        {
            model=null;
        }
    }
    
    protected void setModelStatus(short status)
    {
        this.modelStatus=status;
    }
    
    public short getModelStatus()
    {
        return this.modelStatus;
    }
    
    public void init()
    {
        this.setModelStatus(MODEL_CONSTRUCTING);
        try
        {
            this.checkFunctions();
            dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.MODEL_CONSTRUCT_EVENT));
            
            handler.addModel(this);
            maindep = new MainDependencyGraph();
            //			maindep.constructMainDependencyGraph(this);
            Log.debug("Main dependency graph created");
            //			maindep.recalculate();
            try
            {
                this.createSchemaPool();
            } catch (XFormsException e)
            {
                this.handleXFormsException(e);
                return;
            }
            dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.MODEL_CONSTRUCT_DONE_EVENT));
            super.init();
            maindep.recalculate();
            listenAllMutations();
        } catch (XFormsException e)
        {
            this.handleXFormsException(e);
        }
        this.setModelStatus(MODEL_READY);
        // TODO: UIInitialize event?
        dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.READY_EVENT));
    }
    
    public void checkFunctions() throws XFormsException
    {
        Attr fAttr = this.getAttributeNode("functions");
        if (fAttr==null) return;
        String functions = fAttr.getNodeValue();
        StringTokenizer tokenizer = new StringTokenizer(functions," ");
        while (tokenizer.hasMoreTokens())
        {
            String func = tokenizer.nextToken();
            if (this.getXPathEngine().hasFunction("",func)==false)
            {
                throw new XFormsComputeException("Unknown function described in model/@function: '"+func+"'\n",this);
            }
        }
    }
    /**
     * Goes dynamically thru children and returns the submitinfo, with the id = id;
     */
    public SubmissionElementImpl getSubmitInfo(String id)
    {
        NodeList children = this.getElementsByTagNameNS(XFORMS_NS,SUBMIT_INFO_ELEMENT);
        for (int i=0;i<children.getLength();i++)
        {
            SubmissionElementImpl submitInfo=(SubmissionElementImpl)children.item(i);
            if (id==null||id.length()<1)
            {
                return submitInfo;
            }
            if (id.equals(submitInfo.getId())) return submitInfo;
        }
        return null;
    }
    
    protected InstanceElementImpl findFirstInstance()
    {
        for (Node child=this.getFirstChild();child!=null;child=child.getNextSibling())
        {
            if (child instanceof InstanceElementImpl) return (InstanceElementImpl)child;
        }
        return null;
    }
    
    public InstanceElementImpl getInstance()
    {
        // TODO: cache (remember to update in removechild)
        if (defaultInstance==null) defaultInstance = this.findFirstInstance();
        if (defaultInstance==null)
        {
            // lets create a lazy bastard mode instance
            InstanceElementImpl lazyBastard = this.createLazyBastardInstance();
            this.defaultInstance=lazyBastard;
        }
        return defaultInstance;
    }
    public SchemaElementImpl getSchema()
    {
        return model;
    }
    public String getId()
    {
        return this.getAttribute("id");
    }
    
    public DynamicDependencyHandler getDynamicDependencyHandler()
    {
        return this.dynDependencyHandler;
    }
    
    
    /**
     * this method is overridden so that default actions for certain events can be processed
     */
    public boolean dispatchEvent(Event event)
    {
        // simply forward to super class
        boolean ret = super.dispatchEvent(event);
        if (event instanceof XFormsEventImpl)
        {
            XFormsEventImpl xe = (XFormsEventImpl)event;
            if (!xe.preventDefault)
            {
                if (xe.getType().equals(XFormsEventFactory.RECALCULATE_EVENT))
                {
                    // Default action for the recalculate event
                    this.internalRecalculate();
                }
                else if (xe.getType().equals(XFormsEventFactory.REVALIDATE_EVENT))
                {
                    // Default action for the revalidate event
                    
                    // in the new version, we always revalidate after a change,
                    // so this is no-op
                    //this.revalidate();
                    this.needsRevalidate=false;
                }
                else if (xe.getType().equals(XFormsEventFactory.RESET_EVENT))
                {
                    this.reset();
                }
                else if (xe.getType().equals(XFormsEventFactory.REFRESH_EVENT))
                {
                    this.refresh();
                    this.needsRefresh=false;
                }
                else if (xe.getType().equals(XFormsEventFactory.REBUILD_EVENT))
                {
                    // Default action for the revalidate event
                    this.rebuild(false);
                }
            }
        }
        return ret;
    }
    
    public boolean validation()
    {
        return (validationElement()||validationAttribute());
    }
    protected Element schemaElement;
    public Element getSchemaRoot()
    {
        return schemaElement;
    }
    protected boolean validationAttribute()
    {
        Attr schemaAttr = this.getAttributeNode(SCHEMA_ATTRIBUTE);
        if (schemaAttr==null) return false;
        String schemaVal = schemaAttr.getNodeValue();
        if (schemaVal!=null&&schemaVal.length()>0) return true;
        return false;
    }
    
    protected boolean validationElement()
    {
        SchemaElementImpl schemaElem = this.getSchema();
        if (schemaElem==null) return false;
        String href = schemaElem.getHref();
        if (href!=null&&href.length()>0) return true;
        if (schemaElement!=null) return true;
        else
        {
            Element elem=(Element)getElementsByTagNameNS(
            SCHEMA_URI,"schema").item(0);
            if (elem!=null)
            {
                Log.debug("Found a schema element inline.");
                this.schemaElement = elem;
                return true;
            }
        }
        //Log.debug("No inline schema element. Validation off.");
        return false;
    }
    
    
    /**
     * re-evaluate all UI bindings
     */
    public void reevaluateUIBindings()
    {
        try
        {
            this.dynDependencyHandler.reevaluateDependencies();
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    /**
     * Revalidate the instance
     */
    public synchronized boolean internalRevalidate()
    {
        this.needsRevalidate=false;
        return true;
    }
    
    
    
    /**
     * Recalculate the instance, when the node has changed.
     * First time creates the main dep graph,
     * other times, creates the sub graph
     */
    
    private final Vector changeVector = new Vector();
    public synchronized void internalRecalculate()
    {
        // TODO: return if no binds are in the xform
        //Log.debug("Model.Recalculate");
        this.setModelStatus(MODEL_CALCULATING);
        if (changedNodes.size()>0)
        {
            // changedNodes vector contains InstanceNodes
            // changeVector contains vertices
            changeVector.removeAllElements();
            for (int i = 0; i<changedNodes.size();i++)
            {
                InstanceNode changenode=(InstanceNode)changedNodes.elementAt(i);
                Vertex v = maindep.getVertex(changenode, Vertex.CALCULATE_VERTEX);
                if (v==null)
                {
                    Log.debug("No calc vertex for: "+changenode);
                }
                else
                {
                    changeVector.addElement(v);
                }
            }
            if (changeVector.size()>0)
            {
                // LATER TIMES
                SubDependencyGraph sub = new SubDependencyGraph();
                sub.constructSubDependencyGraph(changeVector);
                //Log.debug("********** SUB GRAPH");
                sub.recalculate();
            }
            changedNodes.removeAllElements();
        }
        this.needsRecalculate=false;
        this.setModelStatus(MODEL_READY);
    }
    
    void addBind(BindElementImpl bind) throws XFormsBindingException, Exception
    {
        this.getHandler().addBind(bind);
        if (maindep==null) return;
        maindep.addBindToGraph(bind,this);
    }
    
    
    void internalRebuild()
    {
        maindep = new MainDependencyGraph();
        
        Node n = this.getFirstChild();
        // re-init binds (they will add themselves into the dep graph)
        while (n!=null)
        {
            if (n instanceof BindElementImpl)
                ((BindElementImpl)n).init();
            n=n.getNextSibling();
        }
        Log.debug("Main dependency graph created");
        maindep.recalculate();
        this.needsRebuild=false;
        // dispatch the recalculate event
        dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.RECALCULATE_EVENT));
        // dispatch the revalidate event
        dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REVALIDATE_EVENT));
        // Actually this does not do anything, since view is always up to date
        dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REFRESH_EVENT));
        //this.needsRecalculate=false;
    }
    
    /** the ui should always change values through this method. This method
     * will do the value change and trigger recalculate etc. processing
     * @param n
     * @param text
     */
    public void uiValueChange(Node n, String text)
    {
    	if (n!=null)
    	{
	        XFormsUtil.setText(n,text);
	        this.runEventsAfterChanges();
    	}
    }
    
    protected void runEventsAfterChanges()
    {
        if (this.getModelStatus()!=MODEL_CALCULATING&&this.getModelStatus()!=MODEL_VALIDATING)
        {
            
                dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.RECALCULATE_EVENT));
                // dispatch the revalidate event
                dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REVALIDATE_EVENT));
                // Actually this does not do anything, since view is always up to date
                dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REFRESH_EVENT));
        }
    }
    /** METHODS FOR LISTENING TO DOM MUTATION EVENTS FROM THE INSTANCE */
    
    /**
     * The DOM event handler
     * This method gets all changes to the instance data, and dispatches correct events
     * NOTE: maybe this should be moved to the InstanceDocument,
     * the only problem is that actually they both need the information:
     * instance does internal changes and model dispatches events etc.
     */
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
        //printEvent(evt);
        // discard all changes if we are currently recalculating
        //if (!this.calculating&&!this.validating)//&&!this.initializing)
        {
            // We either get attr changed event for the attribute or characterdatamodified or dom node inserted
            // for a text node
            MutationEvent me=(MutationEvent)evt;
            String type = me.getType();
            Node target = (Node)me.getTarget();
            InstanceNode instanceNode = null;
            if (type.equals("DOMCharacterDataModified")||type.equals("DOMNodeInserted")||type.equals("DOMNodeRemoved") )
            {
                if (target.getNodeType() == Node.TEXT_NODE)
                {
                    instanceNode = (InstanceNode)target.getParentNode();
                    //Log.debug("datamodified or node inserted event, target a text node");
                }
                if (!type.equals("DOMCharacterDataModified"))
                {
                    this.markAllUIBindingsDirty();
                    this.instanceStructureChanged();
                }
            }
            else if (type.equals("DOMAttrModified"))
            {
                //Log.debug("DOMAttrModified event");
                instanceNode = (InstanceNode)me.getRelatedNode();
            }
            //			Log.debug("Event: "+type +" Target: "+target+" Instance node: "+instanceNode);
            if (instanceNode!=null)
            {
                //if (!this.calculating&&!this.validating)
                    if (this.getModelStatus()!=MODEL_CALCULATING&&this.getModelStatus()!=MODEL_VALIDATING)
                    
                {
                    // the instanceNode has been changed, mark it to the change list
                    if (!changedNodes.contains(instanceNode)) changedNodes.addElement(instanceNode);
                    // if we are inside action handling, then the events must be deferred
                    //if (handler.getInsideAction())
                    {
                        // mark deferred events
                        this.needsRecalculate=true;
                        this.needsRevalidate=true;
                        this.needsRefresh=true;
                    }
                    /*
                    else
                    {
                        // dispatch the recalculate event
                        dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.RECALCULATE_EVENT));
                        // dispatch the revalidate event
                        dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REVALIDATE_EVENT));
                        // Actually this does not do anything, since view is always up to date
                        dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REFRESH_EVENT));
                    }*/
                }
                //if (!this.validating)
                //{
                retrieveInstanceItem(instanceNode).notifyValueChanged();
                //}
            }
            else
            {
                Log.debug("Unhandled DOM event: "+evt+" type"+type);
            }
        }
        
    }
    
    private void markAllUIBindingsDirty()
    {
        // TODO Auto-generated method stub
        this.dynDependencyHandler.markAllUIDependenciesDirty();
    }
    final String[] evtNames=
    {
        //            "DOMSubtreeModified",
        "DOMAttrModified","DOMCharacterDataModified",
        "DOMNodeInserted","DOMNodeRemoved",
        //	        "DOMNodeInsertedIntoDocument","DOMNodeRemovedFromDocument",
    };
    private void listenAllMutations(Node n)
    {
        
        
        EventTarget t=(EventTarget)n;
        
        for(int i=evtNames.length-1;
        i>=0;
        --i)
        {
            //t.addEventListener(evtNames[i], this, true);
            t.addEventListener(evtNames[i], this, false);
        }
        
    }
    
    protected void removeAllMutationListeners(Node n)
    {
        if (n!=null)
        {
            EventTarget t=(EventTarget)n;
            
            for(int i=evtNames.length-1;
            i>=0;
            --i)
            {
                //t.addEventListener(evtNames[i], this, true);
                t.removeEventListener(evtNames[i], this, false);
            }
        }
    }
    protected void listenAllMutations()
    {
        // attach a change listener to the instance document
        Enumeration e = instances.elements();
        while(e.hasMoreElements())
        {
            InstanceElementImpl inst = (InstanceElementImpl)e.nextElement();
            listenAllMutations(inst.getInstanceDocument());
        }
    }
    
    protected void removeAllMutationListeners()
    {
        InstanceDocument instDoc;// = (InstanceDocumentImpl)this.getInstanceDocument();
        Enumeration e = instances.elements();
        while(e.hasMoreElements())
        {
            InstanceElementImpl inst = (InstanceElementImpl)e.nextElement();
            if (inst!=null)
            {
                instDoc = inst.getInstanceDocument();
                if (instDoc!=null)
                {
                    removeAllMutationListeners(instDoc);
                    //instDoc.destroy();
                }
            }
        }
    }
    
    private void instanceStructureChanged()
    {
        xpathAPI = null;
        //xpathLookup = null;
    }
    
    public void destroy()
    {
        try
        {
            dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.MODEL_DESTRUCT_EVENT));
            // attach a change listener to the instance document
            this.removeAllMutationListeners();
            this.instanceStructureChanged();
            super.destroy();
            //			this.getHandler().removeModel(this);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    /** return all binds in DOCUMENT ORDER (important for initialization) */
    public NodeList getBinds()
    {
        // TODO: optimize this by keeping a memory list of binds
        NodeList inodes=this.getElementsByTagNameNS(XFORMS_NS,BIND_ELEMENT);
        return inodes;
    }
    
    /** @return the pool where all schemas for this model are cached */
    public SchemaPool getSchemaPool()
    {
        return this.schemaPool;
    }
    
    /*
    public XPathLookup getXPathLookup()
    {
        if (this.xpathLookup==null) this.xpathLookup=new XPathLookup();
        return this.xpathLookup;
    }*/
    
    public static InstanceItem retrieveInstanceItem(Node n)
    {
        if (n instanceof InstanceNode) 
        {
           return ((InstanceNode)n).getInstanceItem();
        }
        return null;
    }
    
    
    
    /** this mehod copies xmlns:xxx attributes from an element to another. It goes towards to the
     * root and does it for every node if recursive is true
     */
    protected static void copyNamespaceDeclarations(Element to, Element from, boolean recursive, String elementPrefix)
    {
        InstanceParser.copyNamespaceDeclarations(to,from,recursive,elementPrefix);
    }
    
    protected void createSchemaPool() throws XFormsLinkException
    {
        this.schemaPool=XFormsConfiguration.getInstance().createSchemaPool();
        try
        {
            // TODO: add default xforms datatype schema
            URL xformsDatatypeSchema = this.getHandler().getXResource("xforms.schema.datatypes");
            this.schemaPool.addSchema(xformsDatatypeSchema.toString());
        } catch (Exception e)
        {
            Log.error(e);
        }
        // Add all child schema elements
        for (Node child = this.getFirstChild();child!=null;child=child.getNextSibling())
        {
            if (child.getNodeType()==Node.ELEMENT_NODE)
            {
                XSmilesElementImpl elem = (XSmilesElementImpl)child;
                if (elem.getNamespaceURI().equals(SCHEMA_URI)&&elem.getLocalName().equals("schema"))
                {
                    this.addSchemaFromElement(elem,this.schemaPool, elem.getBaseURI());
                }
            }
        }
        Attr schemaAttr = this.getAttributeNode(SCHEMA_ATTRIBUTE);
        if (schemaAttr!=null&&schemaAttr.getNodeValue().length()>0)
        {
            StringTokenizer tokenizer = new StringTokenizer(schemaAttr.getNodeValue()," \t\n");
            while (tokenizer.hasMoreTokens())
            {
                String token = tokenizer.nextToken();
                String schemaurl = null;
                try
                {
                    schemaurl = resolveURI(token).toExternalForm();
                }
                catch (MalformedURLException e)
                {
                    throw new XFormsLinkException(e.toString(), token);
                }
                
                if (token.charAt(0) == '#')
                {
                    Element schema = this.getOwnerDocument().getElementById(token.substring(1));
                    this.addSchemaFromElement(schema,this.schemaPool,schemaurl);
                }
                else
                {
                    try
                    {
	                    URL u = new URL(schemaurl);
	                    XSmilesConnection conn = this.get(u,Resource.RESOURCE_UNKNOWN);
	                    this.schemaPool.addSchema(new InputStreamReader(conn.getInputStream()),schemaurl.toString());
                    } catch (Exception e)
                    {
                        Log.error(e);
                        throw new XFormsLinkException(e.toString(),token);
                    }
                }
            }
        }
    }
    
    protected void addSchemaFromElement(Element schema,SchemaPool pool, String schemaurl) throws XFormsLinkException
    {
        try
        {
            CharArrayWriter buffer = new CharArrayWriter(100);
            // have to copy namespace declarations!
            //debugNode(schema);
            copyNamespaceDeclarations(schema,(Element)schema.getParentNode(),true,"");
            //debugNode(schema);
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.transform(new DOMSource(schema), new StreamResult(buffer));
                        /* or we can replace the two lines above with
                        new org.apache.xml.serialize.XMLSerializer(buffer, null).serialize(schema);
                         */
            pool.addSchema(new CharArrayReader(buffer.toCharArray()), schemaurl);
        } catch (Exception e)
        {
            throw new XFormsLinkException(e.toString(),schemaurl);
        }
    }
    
    /********* METHODS FOR ECMASCRIPTS ************/
    /**
     * public method for ECMAScripts to get the root of the instance document
     */
    public Node getInstanceRoot()
    {
        return this.getInstance().getInstanceRoot();
    }
    public Document getInstanceDocument()
    {
        return this.getInstance().getInstanceDocument();
    }
    
    public Document getInstanceDocument(String id)
    {
        if (id==null||id.length()<1) return this.getInstanceDocument();
        else if (this.instances.containsKey(id) )
            return ((InstanceElementImpl)this.instances.get(id)).getInstanceDocument();
        else return null;
    }
    public InstanceElementImpl getInstance(String id)
    {
        if (id==null||id.length()<1) return this.getInstance();
        else if (this.instances.containsKey(id) )
            return (InstanceElementImpl)this.instances.get(id);
        else return null;
    }
    /** rebuild the calculation engine */
    public void rebuild(boolean canBeDeferred)
    {
        // for deferred updates
        if (canBeDeferred && this.getHandler().getInsideAction())
        {
            this.needsRebuild=true;
        }
        else
        {
            this.internalRebuild();
            if (canBeDeferred) //was from insert/delete
            {
                this.reevaluateUIBindings();
            }
        }
    }
    
    /** recalculate this model */
    public void recalculate()
    {
        internalRecalculate();
    }
    /** revalidate this model */
    public void revalidate()
    {
        internalRevalidate();
    }
    /** recalculate this model */
    public void refresh()
    {
        this.internalRefresh();
        this.needsRefresh=false;
    }
    
    /**
     * This method does the refresh processing.
     * First, is re-evaluates (rewires) all dirty UI dependencies.
     * Second, it refreshes all dirty MIP states and values in the UI, and throws the events.
     *
     */
    protected synchronized void internalRefresh()
    {
        // re-evaluate all dirty UI bindings
        this.rewire();
        
        // check all dirty MIPS
        Vector dirtyMIPS = this.dynDependencyHandler.getAndClearDirtyMIPS();
		Log.debug("*** REFRESH (from model) dirty MIP bindings: "+dirtyMIPS.size());
        Enumeration denum = dirtyMIPS.elements();
        while (denum.hasMoreElements())
        {
            Dependency dep = (Dependency)denum.nextElement();
            InstanceItemListener list = dep.getDependencyListener().getInstanceItemListener();

            InstanceNode n = dep.getDependencyListener().getRefNode();

            if (dep.getBindingType()==Dependency.SINGLENODE_BINDING&&list!=null&&n!=null)
            {
				boolean stateChanged=dep.hasDirtyFlags();
                InstanceItem item = n.getInstanceItem();
                if (dep.testAndClearDirtyState(Dependency.READONLY_DIRTY))
                {
                    list.setReadonly(item.getReadonly());
                }			
                if (dep.testAndClearDirtyState(Dependency.RELEVANT_DIRTY))
                {
                    list.checkVisibility(item);
                }
                if (dep.testAndClearDirtyState(Dependency.REQUIRED_DIRTY))
                {
                    list.setRequired(item.getRequired());
                }
                if (dep.testAndClearDirtyState(Dependency.VALID_DIRTY))
                {
                    list.checkValidity(item);
                }
                if (dep.testAndClearDirtyState(Dependency.VALUE_DIRTY))
                {
                    list.valueChanged(item.getText());
                }
				if (stateChanged) list.checkStyling();
            }
            if (dep.getBindingType()==Dependency.VALUE_BINDING&&list!=null)
            {
				boolean stateChanged=dep.hasDirtyFlags();
                if (dep.testAndClearDirtyState(Dependency.VALUE_DIRTY))
                {
                    list.valueChanged(null);
                }
                dep.clearAllDirtyFlags();
				if (stateChanged) list.checkStyling();
            }
        }
		Log.debug("*** END OF REFRESH (from model)");
    }
    
    /**
     * This re-evaluates all dirty UI bindings. Called from internalRefersh
     *
     */
    protected void rewire()
    {
        Vector dirtyDeps = this.dynDependencyHandler.getAndClearDirtyDependencies();
		Log.debug("***** REWIRING (from model) dirty bindings: "+dirtyDeps.size());
        Enumeration denum = dirtyDeps.elements();
        while (denum.hasMoreElements())
        {
            Dependency dep = (Dependency)denum.nextElement();
            dep.getDependencyListener().bindingMaybeDirty();
        }
    }
    
    protected void resetInstances()
    {
        try
        {
            // reset all mutation listeners
            this.removeAllMutationListeners();
            // reset all instances
            for (Enumeration e = this.instances.elements();e.hasMoreElements();)
            {
                InstanceElementImpl inst= (InstanceElementImpl)e.nextElement();
                inst.reset();
            }
            // recreate mutation listeners
            this.listenAllMutations();
        } catch (Exception e)
        {
            Log.error(e,"At model.resetInstances()");
            this.getHandler().showUserError(e);
        }
    }
    
    /** reset all instances and the model */
    public void reset()
    {
        this.resetInstances();
        this.resetModel(false);
    }
    /** reset the model without resetting the instances */
    public void resetModel(boolean reAddEventListeners)
    {
        try
        {
            // dispatch the recalculate event
            dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REBUILD_EVENT));
            // dispatch the revalidate event
            dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REVALIDATE_EVENT));
            // Actually this does not do anything, since view is always up to date
            //dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REFRESH_EVENT));
            if (reAddEventListeners) {
            	this.listenAllMutations();
            }
            this.recreateUIBindings();
            this.refresh();
        } catch (Exception e)
        {
            Log.error(e,"At model.resetModel()");
            this.getHandler().showUserError(e);
        }
    }
    
    /** this method goes thru all UI elements which bind to this model
     * and recreates their bindings */
    public void recreateUIBindings()
    {
        Log.debug("Should recreateUIBindings here");
        this.recreateUIBindingsRecursively(this.getOwnerDocument().getDocumentElement());
    }
    
    /** this method recreates all UI bindings recursively when e.g. reset or replace="instance"
     * has happened
     */
    protected synchronized void recreateUIBindingsRecursively(Element elem)
    {
        if (elem==null) return;
        if (elem instanceof ElementWithContext)
        {
            if (!(elem instanceof BindElement))
            {
                ElementWithContext ec = (ElementWithContext)elem;
                if (ec.getModel()==this&&ec.hasBindingAttributes())
                {
                    ec.checkBinding();
                    return;
                }
            }
        }
        NodeList childrenWithPseudos;
        if (elem instanceof XSmilesElementImpl)
            childrenWithPseudos        = ((XSmilesElementImpl)elem).getChildNodes(true);
        else childrenWithPseudos=elem.getChildNodes();
        for (int i = 0;i<childrenWithPseudos.getLength();i++) //Node child=elem.getFirstChild();child!=null;child=child.getNextSibling())
        {
            Node child = childrenWithPseudos.item(i);
            if (child.getNodeType()==Node.ELEMENT_NODE)
            {
                this.recreateUIBindingsRecursively((Element)child);
            }
        }
    }
    
    
    
    public XPathEngine getXPathEngine()
    {
        if (xpathAPI==null) xpathAPI=XFormsConfiguration.getInstance().createXPathEngine(this.getHandler(),this.getModel());
        return xpathAPI;
    }

    public ModelElementImpl getModel()
    {
        return this;
    }
    
    /**
     * this method generates the lazy bastard instance by
     * adding a new 'instance' element as a child of 'model' and
     * adds an 'instanceData' element to it.
     * Note that
     * there can only be one instance if the lazy bastard instance exists
     */
    protected InstanceElementImpl createLazyBastardInstance()
    {
        Log.debug("Creating lazy bastard instance.");
        // TODO: get encoding from the host document!
        InstanceElementImpl inst = (InstanceElementImpl)this.getOwnerDocument().createElementNS(XFORMS_NS,INSTANCE_ELEMENT);
        inst.setLazyBastardMode(true);
        // TODO: xmlns="";
        Element instanceDataElem = this.getOwnerDocument().createElementNS("","instanceData");
        instanceDataElem.setAttribute("xmlns","");
        inst.appendChild(instanceDataElem);
        this.lazyBastardInstance=inst;
        this.insertBefore(inst,null);
        inst.init();
        this.removeAllMutationListeners();
        this.listenAllMutations();
        return inst;
    }
    
    /**
     * @return either null, if lazy bastard instance has not been created, or
     * the reference to the lazy bastard instance if one exists. Note that
     * there can only be one instance if the lazy bastard instance exists
     */
    public InstanceElementImpl getLazyBastardInstance()
    {
        if (lazyBastardInstance!=null)
            return lazyBastardInstance;
        else
        {
            this.getInstance(); // will search for instances and create a lazy instance if necessary
            return lazyBastardInstance;
        }
    }
    
    public void outermostActionExited()
    {
        Log.debug("Running deferred events");
        // TODO: only run these if marked necessary
        boolean rebuilt = false;
        if (needsRebuild)
        {
            dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REBUILD_EVENT));
            rebuilt=true;
        }
        if (needsRecalculate) 
        {
            needsRecalculate=false;
            dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.RECALCULATE_EVENT));
        }
        // dispatch the revalidate event
        if (needsRevalidate)
        {
            needsRevalidate=false;
            dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REVALIDATE_EVENT));
        }
        // Actually this does not do anything, since view is always up to date
        if (needsRefresh)
        {
            needsRefresh=false;
            dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REFRESH_EVENT));
        }
        //if (rebuilt) this.reevaluateUIBindings(); // hmm... why is this here, should come from other place???
        
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.XFormsModelElement#getInstanceDocuments()
     */
    public Enumeration getInstanceDocuments()
    {
        // TODO Auto-generated method stub
        //return this.instances.elements();
        Enumeration e = new InstanceEnumeration(this.instances.elements());
        return e;
    }
    
    class InstanceEnumeration implements Enumeration 
    {
        public Enumeration instanceEnum;
        public InstanceEnumeration(Enumeration i)
        {
            this.instanceEnum=i;
        }
        /* (non-Javadoc)
         * @see java.util.Enumeration#hasMoreElements()
         */
        public boolean hasMoreElements()
        {
            return instanceEnum.hasMoreElements();
        }
        /* (non-Javadoc)
         * @see java.util.Enumeration#nextElement()
         */
        public Object nextElement()
        {
            InstanceElementImpl inst = (InstanceElementImpl)instanceEnum.nextElement();
            return inst.getInstanceDocument();
        }
        
    }

    public void functionValueChanged(String function)
    {
        this.runEventsAfterChanges();
    }
}
