/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 16, 2004
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.DDate;

/**
 * @author mpohja
 */
public class DateSpeechWidget extends BaseSpeechWidget
{

    /**
     * @param e
     * @param handler
     */
    public DateSpeechWidget(Element e, FocusHandler handler)
    {
        super(e, handler);
        // TODO Auto-generated constructor stub
    }
    /** static prompt for this kind of control, for instance, "select a number"*/
    public String getPrompt()
    {
        return ". Select a date.";
    }
    public String generateDialogQuestion()
    {
        return this.getLabel() + " " + getCurrentValue()+this.getPrompt();
    }

    public void generateGrammar(Grammar grammar)
    {
        GrammarGenerator.generateDateGrammar(grammar);
    }

    SimpleDateFormat outputFormat = new SimpleDateFormat("MMMMM dd, yyyy");
    public boolean interpretResponse(Response r)
    {
        Data data = ((TypedElement) this.element).getData();
        Calendar cal=null;
        if (data instanceof DDate)
        {
            // try to get the current date of the date widget
            cal = ((DDate)data).toCalendar();
        }
        if (cal==null)
        {
            // set current date if date widget date was not available
            cal=Calendar.getInstance();
        }
        String response = r.response.trim();
        String formattedDate = parseRespond(response);
        Log.debug("Response: " + response + ". Formatted: " + formattedDate);
        Calendar newCal = DateConverter.parseCalender(formattedDate,cal);
        if (newCal==null) return false; // let's have the upper level to recognize the response
        Date date = newCal.getTime();
        SimpleDateFormat format = outputFormat;
        data = ((TypedElement) this.element).getData();
        data.setValueFromObject(date);
        ((TypedElement) this.element).setData(data);
        this.focusHandler.speak(this.getLabel() + ": " + format.format(date)+". "+this.getPrompt());
        //this.moveFocusToParent(); // don't move the focus to parent, since the user can now interactively change the date
        return true;
    }

    private String parseRespond(String response)
    {
        StringTokenizer tokens = new StringTokenizer(response);
        String date = "", value, number;

        while (tokens.hasMoreElements())
        {
            value = tokens.nextToken();
            if (value.equals("one"))
                date += "1";
            else if (value.equals("two"))
                date += "2";
            else if (value.equals("three"))
                date += "3";
            else if (value.equals("four"))
                date += "4";
            else if (value.equals("five"))
                date += "5";
            else if (value.equals("six"))
                date += "6";
            else if (value.equals("seven"))
                date += "7";
            else if (value.equals("eight"))
                date += "8";
            else if (value.equals("nine"))
                date += "9";
            else if (value.equals("ten"))
                date += "10";
            else if (value.equals("eleven"))
                date += "11";
            else if (value.equals("twelve"))
                date += "12";
            else if (value.equals("twenty"))
                date += "2";
            else if (value.equals("thirty"))
                date += "3";
            else if (value.equals("thousand"))
                date += "00";
            else if (!value.equals("and"))
            {
                date = value + " " + date;
                if (tokens.hasMoreTokens()) date+=", ";
            }
        }
        return date;
    }
}