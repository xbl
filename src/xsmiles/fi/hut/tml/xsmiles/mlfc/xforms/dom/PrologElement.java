/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;


import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

/**
 * The superclass of all XForms elements
 * @author Mikko Honkala
 */

public class PrologElement extends XFormsElementImpl{
	
//	public PrologElement(XFormsHandler handler, Element elem)
//	{
//		super(handler,elem);
//	}
	public PrologElement(XFormsElementHandler owner, String ns, String name) {
	  super(owner, ns, name);
	}

	
	public ModelElementImpl getModel()
	{
            Node parent = this.getParentNode();
            if (parent instanceof ModelElementImpl) return (ModelElementImpl)parent;
            else 
            {
                this.debugNode(this);
                Log.error("Parent was not ModelElementImpl");
                return null;
            }
	}
	public InstanceElementImpl getInstance(String id)
	{
		return getModel().getInstance(id);
	}
	public SchemaElementImpl getSchema()
	{
		return getModel().getSchema();
	}
	
}
