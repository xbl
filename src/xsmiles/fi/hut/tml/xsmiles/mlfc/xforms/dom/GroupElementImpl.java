/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.dom.FlowLayoutElement;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.GroupElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.LabeledElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;

import java.util.Vector;

import org.w3c.dom.*;
import org.w3c.dom.events.*;




/**
 * The group element. This is designed to work with the XHTML
 * flow layout implementation in X-Smiles by implementing
 * FlowLayoutElement
 *
 * @author Mikko Honkala
 */


public class GroupElementImpl extends DynBoundElementImpl 
	implements 
		GroupElement, FlowLayoutElement, InstanceItemListener, EventListener// SwingStylableElement
{
    
    /** The position of original group */
    Element parent;
    Node position;
    
    //boolean relevant=true;
    
    
    /** Vector holding the currently active added nodes in the DOM */
    Vector addedNodes=new Vector();
    //protected Style elementStyle = null;
    public CaptionElementImpl getCaption()
    {
        return XFormsControl.getCaption(this);
    }

    
    public GroupElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        this.bindingAttributesRequired=false;
    }
    public String getAppearance()
    {
        return (String)this.getAttribute(APPEARANCE_ATTRIBUTE);

    }

    private boolean getRelevant()
    {
           if (this.getBindingState()==BINDING_ATTRIBUTES_NOT_FOUND) return true;
            return (this.getBindingState()!=BOUND_NODE_NOT_FOUND)&&this.getRefNode()!=null&&
                this.getRefNode().getInstanceItem()!=null&& 
                this.getRefNode().getInstanceItem().getRelevant();
    }
    /**
     */
    public void init()
    {
        try
        {
            this.createBinding();
            this.registerListeners();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        }
        super.init();
        if (this.getRelevant()==false)
        {
            styleChanged();
        }
        //moveNodes((Element)this.getParentNode(),this.getNextSibling(),this,addedNodes);
    }
        /**
     * Destroy this element and its descendants recursively.
     */
    public void destroy()
    {
        this.unregisterListeners();
        super.destroy();
    }

    

    
    protected void registerListeners()
    {
            this.addEventListener(XFormsConstants.FOCUS_EVENT, this, false);
    }

    protected void unregisterListeners()
    {
            this.removeEventListener(XFormsConstants.FOCUS_EVENT, this, false);
    }
    
       public void handleEvent(org.w3c.dom.events.Event evt)
    {
        Log.debug("Repeat got DOM event: "+evt);
        if (evt.getType().equals(XFormsConstants.FOCUS_EVENT)) this.focusEventReceived(evt);
    }
    
    protected void focusEventReceived(Event evt)
    {
        // if the event was targeted to this repeat, set the focus to the first form control
        if (evt.getTarget()==this)
        {
            XFormsControl control = findFirstRelevantControl(this);
            if (control!=null)
                ((EventTarget)control).dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.FOCUS_EVENT));
        }
    }
 
    /**
     * This is called when a binding goes to zero
     */
    public void checkVisibility()
	{
        //styleChanged();
	}

    // INSTANCEITEMLISTENER
    
    /**
     * An instance item instructs the control to check its status, when the status changes
     */
    public void checkVisibility(InstanceItem item)
    {
        Log.debug("CheckVisibility for "+this+" : relevant:"+item.getRelevant());

    }
    /**
     * An instance item instructs the control to check its status, when the status changes
     */
    public void checkValidity(InstanceItem item)
    {
    }
    
    public void setReadonly(boolean readonly)
    {
    }
    public void setRequired(boolean required)
    {
    }
    
    /**
     * The value of this instanceItem has changed
     */
    public void valueChanged(String newValue)
    {
    }
    
    /**
     * notify this listener that there was an error in the value of the
     * instance item. This can be schema validity, constraint, required etc.
     */
    public void notifyError(Exception e,boolean atSubmission)
    {
    }
    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        if (pseudoclass.equals(DISABLED_PSEUDOCLASS))
        {
            // return true, if we are currently disabled (irrelevant)
            return ((!this.getRelevant()));//||this.getBindingState()==BOUND_NODE_NOT_FOUND);

        }
        else if (pseudoclass.equals(ENABLED_PSEUDOCLASS))
        {
            // return true, if we are currently enabled (relevant)
            return this.getRelevant();//&&(this.getBindingState()!=BOUND_NODE_NOT_FOUND);
        }
        else return super.isPseudoClass(pseudoclass);
    }

    public InstanceItemListener getInstanceItemListener()
    {
        return this;
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.LabeledElement#getLabelAsText()
     */
    public String getLabelAsText()
    {
        // TODO Auto-generated method stub
        if (this.getCaption()!=null)
            return this.getCaption().getText();
        else return "";
    }






}


