/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
//import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;
import fi.hut.tml.xsmiles.Log;

import org.apache.xpath.*;
import org.apache.xpath.objects.*;
import org.apache.xpath.axes.*;
import org.apache.xalan.transformer.*;
import org.apache.xalan.extensions.*;
import org.apache.xalan.templates.*;
import org.apache.xalan.*;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.NodeIterator;

import org.apache.xpath.objects.XObject;
import org.apache.xpath.objects.XString;
import org.apache.xpath.objects.XRTreeFrag;
import org.apache.xml.dtm.*;
import org.apache.xml.dtm.ref.DTMNodeIterator;
import org.apache.xml.dtm.ref.DTMNodeList;
import org.apache.xml.dtm.ref.DTMNodeProxy;


import javax.xml.transform.*;

import java.util.*;

import java.io.*;

import org.w3c.dom.Node;

public class XPathContextEx extends XPathContext
{
    XFormsContext handler;
    ModelContext modelContext;
    boolean shouldTrackReferents;
    
    public XPathContextEx(XFormsContext aHandler, ModelContext model)
    {
        this(aHandler,model, false);
    }
    public XPathContextEx(XFormsContext aHandler, ModelContext model, boolean aShouldTrackReferents)
    {
        super();
        shouldTrackReferents=aShouldTrackReferents;
        handler=aHandler;
        modelContext=model;
        if (shouldTrackReferents)
        {
            DTMManager my_dtmManager = DTMManagerEx.newInstance(
            org.apache.xpath.objects.XMLStringFactoryImpl.getFactory());
            this.m_dtmManager=my_dtmManager;
        }
        
        /**
         * Though XPathContext context extends
         * the DTMManager, it really is a proxy for this object, which
         * is the real DTMManager.
         */
        //m_dtmManager = new DTMManagerEx();
        /* does not work in Xalan 2.5.0 */
        /*
        ExtensionHandler exthandler =
        new ExtensionHandlerEx(
        XFormsConstants.XFORMS_NS,
        "java",
        this);
        this.getExtensionsTable().addExtensionNamespace(XFormsConstants.XFORMS_NS,exthandler);
         */
    }
    
    
    public DTMManager getDTMManagerEx()
    {
        return m_dtmManager;
    }
    
    public ModelContext getModelContext()
    {
        return this.modelContext;
    }
    public XFormsContext getXFormsContext()
    {
        return this.handler;
    }
    
    public boolean shouldTrackReferents()
    {
        return this.shouldTrackReferents;
    }
    
    public class ExtensionHandlerEx // for xalan 2.5.0 extends ExtensionHandler
    {
        //		Node callerNode;
        XPathContext xpathContext;
        /**
         * Construct a new extension namespace handler given all the information
         * needed.
         *
         * @param namespaceUri the extension namespace URI that I'm implementing
         * @param scriptLang   language of code implementing the extension
         */
        public ExtensionHandlerEx(String namespaceUri, String scriptLang,XPathContext a_xpathContext)
        {
            // for xalan 2.5.0 super(namespaceUri, scriptLang);
            xpathContext=a_xpathContext;
            //			callerNode=callerNodeArg;
        }
        
        /**
         * Process a call to a function.
         *
         * @param funcName Function name.
         * @param args     The arguments of the function call.
         * @param methodKey A key that uniquely identifies this class and method call.
         * @param exprContext The context in which this expression is being executed.
         *
         * @return the return value of the function evaluation.
         *
         * @throws TransformerException          if parsing trouble
         */
        public Object callFunction(
        String funcName, Vector args, Object methodKey,
        ExpressionContext exprContext) throws TransformerException
        {
            //Log.debug("callFuntion: "+funcName);
            /**********
             * xfm:cursor
             ***********/
            if (funcName.equals("index"))
            {
                Object arg0 = convert(args.elementAt(0), java.lang.String.class);
                if (arg0!=null)
                    return XFormsXPathExtensionHandler.cursor((String)arg0,handler);
            }
            /**********
             * xfm:instance
             ***********/
            if (funcName.equals("instance"))
            {
                Object arg0 = convert(args.elementAt(0), java.lang.String.class);
                if (arg0!=null)
                    return XFormsXPathExtensionHandler.instance(this.xpathContext,
                    (String)arg0,modelContext);
            }
            /**********
             * xfm:boolean-from-string
             ***********/
            if (funcName.equals("boolean-from-string"))
            {
                Object arg0 = convert(args.elementAt(0), java.lang.String.class);
                if (arg0!=null)
                    return XFormsXPathExtensionHandler.boolean_from_string((String)arg0);
            }
            /**********
             * xfm:property
             ***********/
            if (funcName.equals("property"))
            {
                Object arg0 = convert(args.elementAt(0), java.lang.String.class);
                if (arg0!=null)
                    return XFormsXPathExtensionHandler.property((String)arg0);
            }
            /**********
             * xfm:now
             ***********/
            if (funcName.equals("now"))
            {
                return XFormsXPathExtensionHandler.now();
            }
            /**********
             * xfm:min
             ***********/
            else if (funcName.equals("min"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.min(xpathContext,(Expression)arg0);
            }
            /**********
             * xfm:if
             ***********/
            else if (funcName.equals("if"))
            {
                Object arg0 = args.elementAt(0);
                Object arg1 = args.elementAt(1);
                Object arg2 = args.elementAt(2);
                if (arg0!=null
                && arg1!=null
                && arg2!=null)
                {
                    
                    return XFormsXPathExtensionHandler.xif(
                    xpathContext,
                    (Expression)arg0,
                    (Expression)arg1,
                    (Expression)arg2);
                }
            }
            /**********
             * xfm:max
             ***********/
            else if (funcName.equals("max"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.max(xpathContext,(Expression)arg0);
            }
            /**********
             * xfm:seconds
             ***********/
            else if (funcName.equals("seconds"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.seconds(xpathContext,(Expression)arg0);
            }
            /**********
             * xfm:months
             ***********/
            else if (funcName.equals("months"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.months(xpathContext,(Expression)arg0);
            }
            /**********
             * xfm:days-from-date
             ***********/
            else if (funcName.equals("days-from-date"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.daysFromDate(xpathContext,(Expression)arg0);
            }
            
            /**********
             * xfm:seconds-from-dateTime
             ***********/
            else if (funcName.equals("seconds-from-dateTime"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.secondsFromDateTime(xpathContext,(Expression)arg0);
            }
            /**********
             * xfm:avg
             ***********/
            else if (funcName.equals("avg"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.avg(xpathContext,(Expression)arg0);
            }
            /**********
             * xfm:count-non-empty
             ***********/
            else if (funcName.equals("count-non-empty"))
            {
                Object arg0 = args.elementAt(0);
                if (arg0!=null && arg0 instanceof Expression)
                    return XFormsXPathExtensionHandler.count_non_empty(xpathContext,(Expression)arg0);
            }
            else
            {
                return null;
            }
            
            return null;
        }
        
        /**
         * Tests whether a certain function name is known within this namespace.
         * @param function name of the function being tested
         * @return true if its known, false if not.
         */
        public  boolean isFunctionAvailable(String function)
        {
            return false;
        }
        
        /**
         * Tests whether a certain element name is known within this namespace.
         * @param function name of the function being tested
         *
         * @param element Name of element to check
         * @return true if its known, false if not.
         */
        public  boolean isElementAvailable(String element)
        {
            return false;
        }
        
        
        
        /**
         * Process a call to this extension namespace via an element. As a side
         * effect, the results are sent to the TransformerImpl's result tree.
         *
         * @param localPart      Element name's local part.
         * @param element        The extension element being processed.
         * @param transformer      Handle to TransformerImpl.
         * @param stylesheetTree The compiled stylesheet tree.
         * @param mode           The current mode.
         * @param sourceTree     The root of the source tree (but don't assume
         *                       it's a Document).
         * @param sourceNode     The current context node.
         * @param methodKey      A key that uniquely identifies this class and method call.
         *
         * @throws XSLProcessorException thrown if something goes wrong
         *            while running the extension handler.
         * @throws MalformedURLException if loading trouble
         * @throws FileNotFoundException if loading trouble
         * @throws IOException           if loading trouble
         * @throws TransformerException          if parsing trouble
         */
        public void processElement(
        String localPart, ElemTemplateElement element, TransformerImpl transformer,
        Stylesheet stylesheetTree, Object methodKey) throws TransformerException, IOException
        {
        }
    }
    /**
     * Convert the given XSLT object to an object of
     * the given class.
     * @param xsltObj The XSLT object that needs conversion.
     * @param javaClass The type of object to convert to.
     * @returns An object suitable for passing to the Method.invoke
     * function in the args array, which may be null in some cases.
     * @throws TransformerException may be thrown for Xalan conversion
     * exceptions.
     */
    static Object convert(Object xsltObj, Class javaClass)
    throws javax.xml.transform.TransformerException
    {
        if(xsltObj instanceof XObject)
        {
            XObject xobj = ((XObject)xsltObj);
            int xsltClassType = xobj.getType();
            
            switch(xsltClassType)
            {
                case XObject.CLASS_NULL:
                    return null;
                    
                case XObject.CLASS_BOOLEAN:
                {
                    if(javaClass == java.lang.String.class)
                        return xobj.str();
                    else
                        return new Boolean(xobj.bool());
                }
                // break; Unreachable
                case XObject.CLASS_NUMBER:
                {
                    if(javaClass == java.lang.String.class)
                        return xobj.str();
                    else if(javaClass == Boolean.TYPE)
                        return new Boolean(xobj.bool());
                    else
                    {
                        return convertDoubleToNumber(xobj.num(), javaClass);
                    }
                }
                // break; Unreachable
                
                case XObject.CLASS_STRING:
                {
                    if((javaClass == java.lang.String.class) ||
                    (javaClass == java.lang.Object.class))
                        return xobj.str();
                    else if(javaClass == Character.TYPE)
                    {
                        String str = xobj.str();
                        if(str.length() > 0)
                            return new Character(str.charAt(0));
                        else
                            return null; // ??
                    }
                    else if(javaClass == Boolean.TYPE)
                        return new Boolean(xobj.bool());
                    else
                    {
                        return convertDoubleToNumber(xobj.num(), javaClass);
                    }
                }
                // break; Unreachable
                
                case XObject.CLASS_RTREEFRAG:
                {
                    // GLP:  I don't see the reason for the isAssignableFrom call
                    //       instead of an == test as is used everywhere else.
                    //       Besides, if the javaClass is a subclass of NodeIterator
                    //       the condition will be true and we'll create a NodeIterator
                    //       which may not match the javaClass, causing a RuntimeException.
                    // if((NodeIterator.class.isAssignableFrom(javaClass)) ||
                    if ( (javaClass == NodeIterator.class) ||
                    (javaClass == java.lang.Object.class) )
                    {
                        DTMIterator dtmIter = ((XRTreeFrag) xobj).asNodeIterator();
                        return new DTMNodeIterator(dtmIter);
                    }
                    else if (javaClass == NodeList.class)
                    {
                        return ((XRTreeFrag) xobj).convertToNodeset();
                    }
                    // Same comment as above
                    // else if(Node.class.isAssignableFrom(javaClass))
                    else if(javaClass == Node.class)
                    {
                        DTMIterator iter = ((XRTreeFrag) xobj).asNodeIterator();
                        int rootHandle = iter.nextNode();
                        DTM dtm = iter.getDTM(rootHandle);
                        return dtm.getNode(dtm.getFirstChild(rootHandle));
                    }
                    else if(javaClass == java.lang.String.class)
                    {
                        return xobj.str();
                    }
                    else if(javaClass == Boolean.TYPE)
                    {
                        return new Boolean(xobj.bool());
                    }
                    else if(javaClass.isPrimitive())
                    {
                        return convertDoubleToNumber(xobj.num(), javaClass);
                    }
                    else
                    {
                        DTMIterator iter = ((XRTreeFrag) xobj).asNodeIterator();
                        int rootHandle = iter.nextNode();
                        DTM dtm = iter.getDTM(rootHandle);
                        Node child = dtm.getNode(dtm.getFirstChild(rootHandle));
                        
                        if(javaClass.isAssignableFrom(child.getClass()))
                            return child;
                        else
                            return null;
                    }
                }
                // break; Unreachable
                
                case XObject.CLASS_NODESET:
                {
                    // GLP:  I don't see the reason for the isAssignableFrom call
                    //       instead of an == test as is used everywhere else.
                    //       Besides, if the javaClass is a subclass of NodeIterator
                    //       the condition will be true and we'll create a NodeIterator
                    //       which may not match the javaClass, causing a RuntimeException.
                    // if((NodeIterator.class.isAssignableFrom(javaClass)) ||
                    if ( (javaClass == NodeIterator.class) ||
                    (javaClass == java.lang.Object.class) )
                    {
                        return xobj.nodeset();
                    }
                    // Same comment as above
                    // else if(NodeList.class.isAssignableFrom(javaClass))
                    else if(javaClass == NodeList.class)
                    {
                        return xobj.nodelist();
                    }
                    // Same comment as above
                    // else if(Node.class.isAssignableFrom(javaClass))
                    else if(javaClass == Node.class)
                    {
                        // Xalan ensures that iter() always returns an
                        // iterator positioned at the beginning.
                        DTMIterator ni = xobj.iter();
                        int handle = ni.nextNode();
                        return ni.getDTM(handle).getNode(handle); // may be null.
                    }
                    else if(javaClass == java.lang.String.class)
                    {
                        return xobj.str();
                    }
                    else if(javaClass == Boolean.TYPE)
                    {
                        return new Boolean(xobj.bool());
                    }
                    else if(javaClass.isPrimitive())
                    {
                        return convertDoubleToNumber(xobj.num(), javaClass);
                    }
                    else
                    {
                        DTMIterator iter = xobj.iter();
                        int childHandle = iter.nextNode();
                        DTM dtm = iter.getDTM(childHandle);
                        Node child = dtm.getNode(childHandle);
                        if(javaClass.isAssignableFrom(child.getClass()))
                            return child;
                        else
                            return null;
                    }
                }
                // break; Unreachable
                
                // No default:, fall-through on purpose
            } // end switch
            xsltObj = xobj.object();
            
        } // end if if(xsltObj instanceof XObject)
        
        // At this point, we have a raw java object, not an XObject.
        if (null != xsltObj)
        {
            if(javaClass == java.lang.String.class)
            {
                return xsltObj.toString();
            }
            else if(javaClass.isPrimitive())
            {
                // Assume a number conversion
                XString xstr = new XString(xsltObj.toString());
                double num = xstr.num();
                return convertDoubleToNumber(num, javaClass);
            }
            else if(javaClass == java.lang.Class.class)
            {
                return xsltObj.getClass();
            }
            else
            {
                // Just pass the object directly, and hope for the best.
                return xsltObj;
            }
        }
        else
        {
            // Just pass the object directly, and hope for the best.
            return xsltObj;
        }
    }
    static Object convertDoubleToNumber(double num, Class javaClass)
    {
        // In the code below, I don't check for NaN, etc., instead
        // using the standard Java conversion, as I think we should
        // specify.  See issue-runtime-errors.
        if((javaClass == Double.TYPE) ||
        (javaClass == java.lang.Double.class))
            return new Double(num);
        else if(javaClass == Float.TYPE)
            return new Float(num);
        else if(javaClass == Long.TYPE)
        {
            // Use standard Java Narrowing Primitive Conversion
            // See http://java.sun.com/docs/books/jls/html/5.doc.html#175672
            return new Long((long)num);
        }
        else if(javaClass == Integer.TYPE)
        {
            // Use standard Java Narrowing Primitive Conversion
            // See http://java.sun.com/docs/books/jls/html/5.doc.html#175672
            return new Integer((int)num);
        }
        else if(javaClass == Short.TYPE)
        {
            // Use standard Java Narrowing Primitive Conversion
            // See http://java.sun.com/docs/books/jls/html/5.doc.html#175672
            return new Short((short)num);
        }
        else if(javaClass == Character.TYPE)
        {
            // Use standard Java Narrowing Primitive Conversion
            // See http://java.sun.com/docs/books/jls/html/5.doc.html#175672
            return new Character((char)num);
        }
        else if(javaClass == Byte.TYPE)
        {
            // Use standard Java Narrowing Primitive Conversion
            // See http://java.sun.com/docs/books/jls/html/5.doc.html#175672
            return new Byte((byte)num);
        }
        else     // Some other type of object
        {
            return new Double(num);
        }
    }
}



