/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import java.awt.event.AdjustmentListener;
import java.math.BigDecimal;
import java.util.Hashtable;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XRange;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;


/** implements range control for decimal values */
public class RangeDecimal extends AbstractRange implements AdjustmentListener, XFormsConstants
{
    
    protected XRange fRange;
    static final int DEFAULT_NUMBER_OF_STEPS = 10;
    public RangeDecimal(XFormsContext context,Element elem)
    {
        super(context,elem);
        this.createControl();
        this.registerListener();
    }
    
    protected void createControl()
    {
        //fSelect = this.fContext.getComponentFactory().getXSelectBoolean();
        // TODO: fRange =
        this.getNumberOfSteps();
        int start=this.getStart();
        int end=this.getEnd();
        int stepsize=this.getStepsize();
        //int init=this.getInit();
        int init=start;
        
        this.fRange = this.fContext.getComponentFactory().getXRange(start,end,stepsize,XRange.HORIZONTAL);
        this.fRange.setValue(init);
        this.fRange.setLabelTable(this.createLabelTable());
    }
    protected int getStart()
    {
        return 1;
    }
    
    protected int getEnd()
    {
        return numsteps+1;
    }
    protected int getStepsize()
    {
        return 1;
    }
    
    protected Hashtable createLabelTable()
    {
        Hashtable labeltable = new Hashtable();
        labeltable.put(new Integer(1),this.getStartAttrValue().toString());
        labeltable.put(new Integer(this.getEnd()),this.getEndAttrValue().toString());
        return labeltable;
    }
    
    protected BigDecimal getStartAttrValue()
    {
        return this.convertToBigDecimal(this.element.getAttribute(START_ATTRIBUTE),"1");
    }
    protected BigDecimal getEndAttrValue()
    {
        return this.convertToBigDecimal(this.element.getAttribute(END_ATTRIBUTE),"10");
    }
    protected BigDecimal getStepAttrValue()
    {
        String stepStr = this.element.getAttribute(STEP_ATTRIBUTE);
        if (stepStr!=null&&stepStr.length()>0)
        {
            return this.convertToBigDecimal(stepStr,"1");
        }
        else
        {
            return null;
        }
    }
    protected BigDecimal convertToBigDecimal(String str, String def)
    {
        if (str!=null && str.length()>0)
            try
            {
                //Log.debug("convertToBigDecimal: "+str+" def:"+def);
                if (str!=null) return new BigDecimal(str);//.setScale(maxscale);
            } catch (NumberFormatException  e)
            {
                //Log.error(e);
            }
        return new BigDecimal(def);
    }
    protected BigDecimal start,end,step,realstep,scale;
    protected int numsteps;
    protected int maxscale;
    
    protected int getNumberOfSteps()
    {
        start = this.getStartAttrValue();
        end = this.getEndAttrValue();
        step = this.getStepAttrValue();
        maxscale = this.findmaxscale();
        
        scale = end.subtract(start).setScale(maxscale);
        if (step==null)
        {
            step=scale.divide(BigDecimal.valueOf(DEFAULT_NUMBER_OF_STEPS),
            BigDecimal.ROUND_HALF_UP);
        }
        
        numsteps = scale.divide(step,BigDecimal.ROUND_HALF_UP).intValue();
        realstep=scale.divide(BigDecimal.valueOf((long)numsteps).setScale(maxscale),
        BigDecimal.ROUND_HALF_UP);
        
        return numsteps;
    }
    protected int findmaxscale()
    {
        int m = Math.max(start.scale(),end.scale());
        if (step!=null)
            m = Math.max(m,step.scale());
        return m;
    }
    protected int convertToRangeValue(Object obj)
    {
        try
        {
            // ((value-start)/step)+1
            if (obj==null) return 0;
            BigDecimal value = (BigDecimal)obj;
            BigDecimal conv = value.subtract(start).divide(realstep,BigDecimal.ROUND_HALF_UP);
            //Log.debug("value, start, realstep, conv,scale: "+value+" ,"+start+","+realstep+","+conv+","+scale);
            return conv.intValue()+1;
        } catch (Exception e)
        {
            Log.error(e);
        }
        return 1;
    }
    protected BigDecimal convertFromRangeValue(int i)
    {
        // ret = (x-1) * realstep + start
        if (i==1) return start;
        if (i==numsteps+1) return end;
        BigDecimal x1 = BigDecimal.valueOf((long)(i-1));
        return x1.multiply(realstep).add(start);
    }
    /**
     * returns the abstract component for this control. The
     * abstract component can be used e.g. to style the component
     * but all listeners should be added to the AdaptiveControl and not directly
     * to the XComponent */
    public XComponent getComponent()
    {
        return fRange;
    }
    /** internal method for setting the listener for the component */
    protected void registerListener()
    {
        this.fRange.addAdjustmentListener(this);
    }
    public void destroy()
    {
        this.fRange.removeAdjustmentListener(this);
    }
    
    /**
     * this function is used to notify the control to update its display according
     * to the content of Data
     */
    public void updateDisplay()
    {
        this.fRange.removeAdjustmentListener(this);
        try
        {
            BigDecimal dec = (BigDecimal)getData().toObject();
            int converted = this.convertToRangeValue(dec);
            this.fRange.setValue(converted);
            checkRange();
        } catch (Exception e)
        {
            Log.error(e);
        }
        finally
        {
            this.fRange.addAdjustmentListener(this);
        }
    }
    
    protected void checkRange()
    {
        if (isInRange())
        {
            if (this.element instanceof XFormsControl)
            {
                ((XFormsControl)this.element).setOutOfRange(false);
            }
            this.DispatchInRange();
        }
        else
        {
            if (this.element instanceof XFormsControl)
            {
                ((XFormsControl)this.element).setOutOfRange(true);
            }
            this.DispatchOutOfRange();
        }
        
        
    }
    
    protected boolean isInRange()
    {
        BigDecimal dec=(BigDecimal)this.getData().toObject();
        if (dec.compareTo(this.end)<=0&&dec.compareTo(this.start)>=0) return true;
        else return false;
    }
    
    protected void DispatchOutOfRange()
    {
        if (this.element!=null&&this.element instanceof XFormsElementImpl)
        {
            ((XFormsElementImpl)this.element).dispatchEvent(XFormsEventFactory.createXFormsEvent(
                    XFormsConstants.OUTOFRANGE_EVENT));
        }
    }
    protected void DispatchInRange()
    {
        if (this.element!=null&&this.element instanceof XFormsElementImpl)
        {
            ((XFormsElementImpl)this.element).dispatchEvent(XFormsEventFactory.createXFormsEvent(
                    XFormsConstants.INRANGE_EVENT));
        }
        
    }
    
    /**
     * This handles the changes from the widget XSelectBoolean
     */
    /** event handler for slider adjustements */
    public void adjustmentValueChanged(java.awt.event.AdjustmentEvent adjustmentEvent)
    {
        //if (!insideUpdateEvent) return; // self-initiated
        if (this.fChangeListener!=null)
        {
            this.fChangeListener.valueChanged(false,null);
            checkRange();
        }
    }
    
    /**
     * get the components current value
     */
    public Data getValue()
    {
        
        int val = this.fRange.getValue();
        getData().setValueFromObject(this.convertFromRangeValue(val));
        return getData();
    }
    
} // RangeDecimal
