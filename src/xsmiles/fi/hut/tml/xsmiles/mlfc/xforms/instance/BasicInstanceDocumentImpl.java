/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.InstanceElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PropertyInheriter;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.SchemaPool;

import org.w3c.dom.*;
import org.w3c.dom.xforms10.*;

import java.util.Hashtable;
import java.util.Vector;

// Document implementation -
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.DOMLocatorImpl;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.impl.Constants;

import org.apache.xerces.util.XMLGrammarPoolImpl;

import org.apache.xerces.util.SymbolTable;


/**
 *
 */

//public class InstanceDocumentImpl extends PSVIDocumentImpl implements InstanceNode {
// setting the EntityResolver for normalizeDoc does not work in Xerces 2.1.0
public class BasicInstanceDocumentImpl extends DocumentImpl implements InstanceNode, InstanceDocument
{
    /** Property identifier: symbol table. */
    public static final String SYMBOL_TABLE =
    Constants.XERCES_PROPERTY_PREFIX + Constants.SYMBOL_TABLE_PROPERTY;
    
    /** Property identifier: grammar pool. */
    public static final String GRAMMAR_POOL =
    Constants.XERCES_PROPERTY_PREFIX + Constants.XMLGRAMMAR_POOL_PROPERTY;
    
    /** my instance element in the UI DOM */
    protected InstanceElement instanceElement;
    
    
    public BasicInstanceDocumentImpl()
    {
    }
    
    public Element createElement(String tag) throws DOMException
    {
        Element e = createElementNS((String)null, tag);
        return e;
    }
    /** this is a crude hack for scripts, because XSmilesDocuments do not implement real id's */
    public Element getElementById(String id)
    {
        Element docElem = this.getDocumentElement();
        if (docElem==null) return null;
        //if (!(docElem instanceof XSmilesElementImpl)) return null;
        //XSmilesElementImpl xDocElem = (XSmilesElementImpl)docElem;
        return XSmilesElementImpl.searchElementWithId(docElem,id);
        
    }
    /**
     * Create a new element - this method should be the only way to create new elements.
     * To copy an element, call this and then copy the attributes.
     */
    public Element createElementNS(String ns, String tag) throws DOMException
    {
        Element element = null;
        if (tag == null)
        {
            Log.error("Invalid null tag name!");
        }
        element=new BasicInstanceElementImpl(this,ns,tag);
        return element;
    }
    public String toString()
    {
        return "PSVIInstanceDocumentImpl: "+super.toString();
    }
    
    public Attr createAttributeNS(String namespaceURI, String qualifiedName)
    throws DOMException
    {
        //		Log.debug("******* createAttribute "+qualifiedName);
        if (errorChecking && !isXMLName(qualifiedName))
        {
            throw new DOMException(DOMException.INVALID_CHARACTER_ERR,
            "DOM002 Illegal character");
        }
        return new BasicInstanceAttrNSImpl(this, namespaceURI, qualifiedName);
    }
    public Attr createAttribute(String name)
    throws DOMException
    {
        if (errorChecking && !isXMLName(name))
        {
            throw new DOMException(DOMException.INVALID_CHARACTER_ERR,
            "DOM002 Illegal character");
        }
        return new BasicInstanceAttrImpl(this, name);
        
    } // createAttribute(String):Attr
    
    /**
     * forces revalidation of the whole doc (uses normalizeDocument)
     * this is called in insert / delete / submit
     * DOM node addition & deletion
     */
    protected Vector errors;
    
    protected SchemaPool schemaPool;
    
    public void setSchemaPool(SchemaPool pool)
    {
        this.schemaPool=pool;
    }
    
    public SchemaPool getSchemaPool()
    {
        return this.schemaPool;
    }
    
    protected boolean submissionOn=false;
    // TODO: create a errorhandler class, which gets submissionOn as parameter
    public synchronized Vector validateDocument(boolean isSubmission)
    {
        Log.debug("Starting validateDocument, no validation for BASIC: "+isSubmission);
        submissionOn=true;
        // TODO: should we first set the whole document schema valid?
        // otherwise some nodes that become valid in this process will not be notified
        errors = new Vector();
        return errors;
    }

    /**
     * NON-DOM: Xerces-specific constructor. "localName" is passed in, so we don't need
     * to create a new String for it.
     *
     * @param namespaceURI The namespace URI of the element to
     *                     create.
     * @param qualifiedName The qualified name of the element type to
     *                      instantiate.
     * @param localName     The local name of the element to instantiate.
     * @return Element A new Element object with the following attributes:
     * @throws DOMException INVALID_CHARACTER_ERR: Raised if the specified
     *                      name contains an invalid character.
     */
    public Element createElementNS(String namespaceURI, String qualifiedName,
    String localpart)
    throws DOMException
    {
        return this.createElementNS(namespaceURI,qualifiedName);
        // From Xerces.CoreDocumentImpl:
        //return new ElementNSImpl(this, namespaceURI, qualifiedName, localpart);
    }
    
    /**
     * Xerces-specific constructor. "localName" is passed in, so we don't need
     * to create a new String for it.
     *
     * @param namespaceURI  The namespace URI of the attribute to
     *                      create. When it is null or an empty string,
     *                      this method behaves like createAttribute.
     * @param qualifiedName The qualified name of the attribute to
     *                      instantiate.
     * @param localName     The local name of the attribute to instantiate.
     * @return Attr         A new Attr object.
     * @throws DOMException INVALID_CHARACTER_ERR: Raised if the specified
     * name contains an invalid character.
     */
    public Attr createAttributeNS(String namespaceURI, String qualifiedName,
    String localName)
    throws DOMException
    {
        return this.createAttributeNS(namespaceURI, qualifiedName);
    }
    
    /**
     * Deep-clone a document, including fixing ownerDoc for the cloned
     * children. Note that this requires bypassing the WRONG_DOCUMENT_ERR
     * protection. I've chosen to implement it by calling importNode
     * which is DOM Level 2.
     *
     * @return org.w3c.dom.Node
     * @param deep boolean, iff true replicate children
     */
    public Node cloneNode(boolean deep)
    {
        
        BasicInstanceDocumentImpl newdoc = new BasicInstanceDocumentImpl();
        // TODO: copy from xerces implementation
        this.cloneNode(newdoc);
        //
        //        // experimental
        //        newdoc.mutationEvents = mutationEvents;
        
        return newdoc;
        
    } // cloneNode(boolean):Node
    

    
    
    /**
     * internal method
     **/
    protected void cloneNode(DocumentImpl newdoc)
    {
        Element docelem = (Element)newdoc.importNode(this.getDocumentElement(),true);
        newdoc.appendChild(docelem);
        
    } // cloneNode(CoreDocumentImpl,boolean):void
    
    
    
    /** TO IMPLEMENT INSTANCE NODE */
    
    /** get the instance item of this node */
    public InstanceItem getInstanceItem()
    {
        return null;
    }
    public PropertyInheriter getPropertyInheriter()
    {
        return null;
    }

    

    
    /** set my UI DOM instance element (needed for replace="instance" */
    public void setInstanceElement(InstanceElement inst)
    {
        this.instanceElement=inst;
    }
    /** get my UI DOM instance element (needed for replace="instance" */
    public InstanceElement getInstanceElement()
    {
        return this.instanceElement;
    }
    

}

