/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;






/**
 * The vertex for the calculation algorithm
 * @author Mikko Honkala
 */


public class ReadonlyVertex extends Vertex {

	public ReadonlyVertex(InstanceNode n,ExpressionContainer expr,XPathExpr xpathExpr) {
		super(n,expr,xpathExpr);
	}
	public ReadonlyVertex() {
		super();
	}
	

	public void compute()
	{
		Object result = runXPath();
		if (result!=null)
		{
			
			if (result instanceof Boolean)
			{
				boolean relevant=((Boolean)result).booleanValue();
				this.instanceNode.getInstanceItem().setReadonly(relevant);
			}
		}
	}
	public short getVertexType()
	{
		return READONLY_VERTEX;
	}

	
	
	
	
}
