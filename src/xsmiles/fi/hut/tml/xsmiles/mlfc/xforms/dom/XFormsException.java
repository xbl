/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.MLFCException;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;


/**
 * XForms  exception
 * @author Mikko Honkala
 */
public class XFormsException extends MLFCException {
    public XFormsException(String reason)
    {
        super(reason);
    }

}
