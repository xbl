/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.xforms10.*;

// Element implementation 
//import org.apache.xerces.dom.ElementNSImpl;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;

/**
 *
 */
public interface InstanceNode extends Node {
	
	/** get the instance item of this node */
	public InstanceItem getInstanceItem();

    /** get the property inheriter of this instance node (usually the instance item)
     *
     */
    public PropertyInheriter getPropertyInheriter();
    

    
    

     
}

