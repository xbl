/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.xpath;



/**
 * simple XPathExpr, holds a String describing the  XPathExpression
 */
public class XPathExpression implements XPathExpr{
    protected String expr;
    public XPathExpression(String e)
    {
        this.expr=e;
    }
    public String getExpression()
    {
        return this.expr;
    }
} 
