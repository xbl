/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Aug 4, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.ui;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsEventFactory;
import fi.hut.tml.xsmiles.mlfc.xmlcss.XMLCSSMLFC;

public class MessageContentPseudoElement extends VisualElementImpl
implements PseudoElement,org.w3c.dom.events.EventListener 
{
    CSSMessageImpl message;
    public MessageContentPseudoElement(CSSMessageImpl mes)
    {
        super((org.apache.xerces.dom.DocumentImpl)mes.messageElem.getOwnerDocument(),"xforms","messagecontent");
        message=mes;
    }
    
    public String getPseudoElementName() // return e.g. "value" for ::value
    {
        return "messagecontent";
    }
    public void init()
    {
        this.createStructure();
        this.setText("");
        super.init();
    }
    Text messageText;
    /**
     * creates the content of the message box. The message string and
     * a button for closing down the form. It trusts that xforms:trigger will create the button
     * and listens for DOMActivate event in that element.
     *
     */
    public void createStructure()
    {
        String xhtmlns = XMLCSSMLFC.XHTML_NS;
        String xformsns = XFormsConstants.XFORMS_NS;
        Element p = this.createElementNS(xhtmlns,"p") ;
        messageText = this.createTextNode("");
        p.appendChild(messageText);
        Element button = this.createElementNS(xformsns,"trigger");
        button.setAttribute("style","display:block;align:center;margin-left:20px;margin-right:20px;margin-top:20px;");
        this.addActivateListener(button);
        Element label = this.createElementNS(xformsns,"label");
        label.appendChild(this.createTextNode("Close window"));
        button.appendChild(label);
        p.appendChild(button);
        /*
        */
        this.appendChild(p);
    }
    
    /** dispatch a custom DOM event, so that xforms message can listen to external window closing and
     * set the pseudoclass correctly
     */
    public void addActivateListener(Element button)
    {
        ((EventTarget)button).addEventListener(XFormsEventFactory.DOMACTIVATE_EVENT,this,false);
        ((EventTarget)this.message.messageElem).addEventListener(EventFactory.XSMILES_MESSAGE_CLOSE_EVENT,this,false);
        ((EventTarget)button).addEventListener(XFormsEventFactory.DOMFOCUSIN_EVENT,this,false);
        ((EventTarget)button).addEventListener(XFormsEventFactory.DOMFOCUSOUT_EVENT,this,false);
        //((EventTarget)button).addEventListener(XFormsEventFactory.DOMACTIVATE_EVENT,this,false);
    }
    
    public Element createElementNS(String ns, String name)
    {
        return message.messageElem.getOwnerDocument().createElementNS(ns,name);
    }
    public Text createTextNode(String text)
    {
        return message.messageElem.getOwnerDocument().createTextNode(text);
    }
    public void updateStyle()
    {
        super.updateStyle();
    }
    public void refreshContent()
    {
        //this.addContent();
    }
    
    protected void setText(String text)
    {
        this.messageText.setNodeValue(text);
    }
    public Node getParentNode()
    {
        if (message==null) return null;
        return message.messageElem;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.PseudoElement#setParentNode(org.w3c.dom.Node)
     */
    public void setParentNode(Node p)
    {
        // TODO Auto-generated method stub
        if (p!=null) message=null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.events.EventListener#handleEvent(org.w3c.dom.events.Event)
     */
    public void handleEvent(Event arg0)
    {
        // TODO Auto-generated method stub
        if (arg0.getType().equals(XFormsEventFactory.DOMACTIVATE_EVENT))
        {
            Log.debug("Close window activated.");
            this.message.closeWindow();
        }
        arg0.preventDefault();
        arg0.stopPropagation();
    }
}
