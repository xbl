/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;

/**
 * xsi:type interface
 * @author Mikko Honkala
 *
 */
public interface XsiType
{
    
    public int getPrimitiveTypeId();
    
    public Object validateString(String s) throws InvalidDatatypeValueException ;

}
