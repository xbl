/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import org.w3c.dom.Element;


/**
 * @author honkkis
 *
 */
public interface SpeechWidget
{
    
    public Element getElement();
    public String getLabel();
    public String generateDialogQuestion();
    public void generateGrammar(Grammar grammar);
    public boolean interpretResponse(Response response);
    public boolean approximateResponse(Response response);
    

}
