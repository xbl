/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XCalendar;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;


import java.util.Calendar;
import org.w3c.dom.Element;


/**
 * this is the <input> element control implementation when the
 * datatype is derived from xsd:date. It is represented as a calendar control
 */
public class InputDate extends AbstractControl implements XChangeListener {

    protected XCalendar fCalendar;
	public InputDate(XFormsContext context,Element elem)
	{
        super(context,elem);
        this.createControl();
        this.registerListener();
	}
    
    protected void createControl()
    {
		fCalendar = (XCalendar)this.fContext.getComponentFactory().getExtension(XCalendar.class);
    }
   /**
     * returns the abstract component for this control. The
     * abstract component can be used e.g. to style the component
     * but all listeners should be added to the AdaptiveControl and not directly 
     * to the XComponent */
    public XComponent getComponent()
    {
        return fCalendar;
    }
    /** internal method for setting the listener for the component */
    protected void registerListener()
	{
        fCalendar.addChangeListener(this);
		//fSelect.addItemListener(this);
	}


   /**
    * this function is used to notify the control to update its display according
    * to the content of Data
    */
    public void updateDisplay()
    {
        try
        {
            Calendar cal = (Calendar)getData().toObject();
            this.fCalendar.setDate(cal);
        } catch (Exception e)
        {
            Log.error(e,"Wrong datatype.");
        }
    }
   /**
     * get the components current value
     */
    public Data getValue()
    {
        getData().setValueFromObject(this.fCalendar.getDate());
        return getData();
    }
    
    /**
     * ChangeListener method that notifies that the user has changed the value of the control.
     * @param newValue A schema compatible string containing the new value
     * @param newObjValue The new value as a Java object
     */
    public void valueChanged(boolean valueChanging, Object src) {
        if (this.fChangeListener!=null) this.fChangeListener.valueChanged(valueChanging,src);
    }
    /**
     * close up, free all memory (yet, do not do visible changes, such as setVisible
     * since this will slow things up
     */
    public void destroy()
    {
        //fCalendar.removeChangeListener(this);
        this.fCalendar.closeCalendar();
        super.destroy();
    }
    
} // InputBoolean
