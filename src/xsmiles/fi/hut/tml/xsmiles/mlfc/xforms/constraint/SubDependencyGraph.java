/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import java.util.Enumeration;
import java.util.Stack;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;

/**
 * builds the pertinent subdependency graph
 *
 * @author Mikko Honkala
 */


public class SubDependencyGraph extends DependencyGraph{
	public class Pair
	{
		Vertex first;
		Vertex second;
		public Pair(Vertex f, Vertex s)
		{
			first=f;
			second=s;
		}
	}
	
	public SubDependencyGraph() {
		super();
	}


	/**
	 * builds the pertinent subdependency graph
	 */
	public void constructSubDependencyGraph(Vector Lc)
	{
		// Use depth-first search to explore master digraph subtrees rooted at
		// each changed vertex. A 'visited' flag is used to stop exploration
		// at the boundaries of previously explored subtrees (because subtrees
		// can overlap in directed graphs).

		//for each vertex r in Lc
		Stack stack = new Stack();
		Enumeration e = Lc.elements();
		while (e.hasMoreElements())
		{
			// Visit next Bind element that contains calculations
			Vertex r = (Vertex)e.nextElement();
			if (r.visited==false)
			{
				stack.push(new Pair(null,r));
				while(stack.empty()==false)
				{
					// (v, w) = pop dependency pair from stack
					Pair nw = (Pair)stack.pop();
					Vertex v = nw.first;
					Vertex w = nw.second;
					Vertex wS;
					// if w is not visited
					if (w.visited==false)
					{
						w.visited=true;
						// Create a vertex wS in S to represent w
					 	wS=createSubVertex(w.getVertexType());
						w.index=wS;
						wS.index=w;
						wS.instanceNode = w.instanceNode;
						wS.container = w.container;
                        wS.xpath = w.xpath;
						this.addVertex(wS);
						//  For each dependency node x of w
						Enumeration depenum=w.depList.elements();
						while (depenum.hasMoreElements())
						{
							Vertex x = (Vertex)depenum.nextElement();
							// Push the pair (w, x) onto the stack
							stack.push(new Pair(w,x));
			            }
					}
					
					// else Obtain wS from index of w
					else wS=w.index;
					// if v is not NIL
					if (v!=null)
					{
						// Obtain vS from index of v
						Vertex vS = v.index;
						// Add dependency node for wS to vS
						// Increment inDegree of wS ??? (Already implemented in addDep)
						vS.addDep(wS);
		            }
		        }
		    }
			Log.debug("Sub Graph created, vertices: "+vertices.size());
		}
		//
		// Now clear the visited flags set in the loop above
		//    
		//for each vertex vS in S
		Enumeration Senum = this.vertices.elements();
		while (Senum.hasMoreElements())
		{
			Vertex vS = (Vertex)Senum.nextElement();
			//    Obtain v from index of vS
			Vertex v=vS.index;
			//    Assign false to the visited flag of v
			v.visited=false;
		}
	}
	
	public Vertex createSubVertex(short type)
	{
//		Log.debug("createSubVertex: type:"+type);
		Vertex v = null;
		switch (type)
		{
			case Vertex.CALCULATE_VERTEX :
					v = new CalculateVertex();
					break;
			case Vertex.RELEVANT_VERTEX :
					v = new RelevantVertex();
					break;
			case Vertex.READONLY_VERTEX :
					v = new ReadonlyVertex();
					break;
			case Vertex.ISVALID_VERTEX :
					v = new IsvalidVertex();
					break;
			case Vertex.REQUIRED_VERTEX :
					v = new RequiredVertex();
					break;
		}
		return v;
	}
}
