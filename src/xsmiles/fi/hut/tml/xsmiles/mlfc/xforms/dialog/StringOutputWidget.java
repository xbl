/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 29, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import org.w3c.dom.Element;


/**
 * @author honkkis
 *
 */
public class StringOutputWidget extends StringSpeechWidget
{
    /**
     * @param e
     * @param handler
     */
    public StringOutputWidget(Element e, FocusHandler handler)
    {
        super(e, handler);
        // TODO Auto-generated constructor stub
    }
    public void generateGrammar(Grammar grammar)
    {
        this.focusHandler.setFocus(this.focusHandler.getParentFocus(this.element));
    }
    public boolean interpretResponse(Response r)
    {
        return false;
    }

}
