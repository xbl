/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.MLFCException;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.Submission;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;


/**
 * XForms binding exception
 * @author Mikko Honkala
 */
public class XFormsSubmitException extends XFormsException {
    protected XFormsElementImpl xformsElement;
	public XFormsSubmitException(Submission submission, String error) 
    {
        super("XForms submission error: "+error+" on submission:"+submission.toString());
    }
        public XFormsSubmitException(String reason)
    {
        super(reason);
        throw new RuntimeException("Never call this method: 1001");
    }
}
