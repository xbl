/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathContextEx;

import fi.hut.tml.xsmiles.Log;

import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.apache.xml.utils.XMLString;

//import org.apache.xpath.compiler.Compiler2;
import org.apache.xpath.XPath;
import org.apache.xpath.XPathVisitor;
import org.apache.xml.utils.PrefixResolverDefault;
import org.apache.xpath.objects.XObject;
import org.apache.xml.dtm.DTM;
import org.apache.xml.dtm.DTMManager;
import org.apache.xml.dtm.DTMIterator;

import org.apache.xalan.trace.TraceListener;
import org.apache.xalan.trace.TracerEvent;
import org.apache.xalan.trace.GenerateEvent;
import org.apache.xalan.trace.SelectionEvent;

import java.lang.reflect.Method;

import javax.xml.transform.*;

import fi.hut.tml.xsmiles.dom.ExtendedDocument;

/**
 * This class analyzes an XPath expression (possibly by executing it) and 
 * collects the referants of that expression.
 *
 * it is necessary to execute an arbitrary XPath expression and return the list of all nodes that the expression references in location paths.
 * For instance, the following expression :
 * "sum(/doc/item[@level='3']/@value) * /doc/tax/@tax" references value, level and tax attributes. 
 *
 * @author Mikko Honkala
 */


public class XPathLookup implements TraceListener, ErrorListener
{
    
    Vector xpathRefNodes;
    
    protected static Method listenerSetter;
    protected static boolean canTrace = true;
    protected ModelContext modelContext;
    
    /** the cached XPath Context object */
    XPathContextEx xpathSupport;
    
    public void setModelContext(ModelContext context)
    {
        modelContext = context;
    }
    
    protected void nodeSetSelected(DTMIterator nodes)
    {
        int pos;
        while (DTM.NULL != (pos = nodes.nextNode()))
        {
            DTM dtm = nodes.getDTM(pos);
            org.w3c.dom.Node node = dtm.getNode(pos);
            
            XMLString s = dtm.getStringValue(pos);
            //Log.debug("Node selected: "+node+" value:"+s);
            if (!xpathRefNodes.contains(node))
                xpathRefNodes.addElement(node);
        }
        nodes.detach();
    }
    
    
    /**
     * Method that is called when a trace event occurs.
     * The method is blocking.  It must return before processing continues.
     *
     * @param ev the trace event.
     */
    public void trace(TracerEvent ev)
    {
    }
    
    /**
     * Method that is called just after the formatter listener is called.
     *
     * @param ev the generate event.
     *
     * @throws javax.xml.transform.TransformerException
     */
    public void selected(SelectionEvent ev) throws javax.xml.transform.TransformerException
    {
        DTMIterator list = ev.m_selection.iter();
        this.nodeSetSelected(list);
    }
    
    /**
     * Method that is called just after the formatter listener is called.
     *
     * @param ev the generate event.
     */
    public void generated(GenerateEvent ev)
    {
    }
    
    /**
     * evalutate the XPath and return the result & all referred nodes
     *@param result store the result in to this object
     */
    public void evalWithTrace(Node contextNode, String query, Node namespaceNode, LookupResult result)
    throws TransformerException
    {
        //System.setProperty("jaxp.debug","true");
        //System.setProperty("org.apache.xml.dtm.DTMManager","fi.hut.tml.xsmiles.mlfc.xforms.xpath.DTMManagerEx");
        // Create an object to resolve namespace prefixes.
        // XPath namespaces are resolved from the input context node's document element
        // if it is a root node, or else the current context node (for lack of a better
        // resolution space, given the simplicity of this sample code).
        PrefixResolverDefault prefixResolver = new PrefixResolverDefault(
        (namespaceNode.getNodeType() == Node.DOCUMENT_NODE)
        ? ((Document) namespaceNode).getDocumentElement() : namespaceNode);
        
        // Create the XPath object.
        XPath xpath = new XPath(query, null, prefixResolver, XPath.SELECT, null);
        
        // Execute the XPath, and have it return the result
        // return xpath.execute(xpathSupport, contextNode, prefixResolver);
        if (xpathSupport==null) xpathSupport = new XPathContextEx(null,modelContext,true);
        int ctxtNode = xpathSupport.getDTMHandleFromNode(contextNode);
        XPathVisitor visitor = new XPathVisitorEx();
        // Get the referred nodes
        DTMManager mgr = xpathSupport.getDTMManager();

        if (mgr instanceof DTMManagerEx)
        {
            ((DTMManagerEx)mgr).setReferantTracking(true);
            ((DTMManagerEx)mgr).clearReferants();
        }
        XObject res = xpath.execute(xpathSupport, ctxtNode, prefixResolver);
        if (XFormsElementHandler.debugXForms) Log.debug("XPathLookup : XPath executed: "+query);
        // TODO: for some reason calling res.toString will change the result to the correct one
        // maybe the nodeset is then finalized or the referantes are tranversed. Odd... Well
        // this whole referant thing is a bit of a hack...
        res.toString();
        //TODO create interface for this
        if (mgr instanceof DTMManagerEx)
        {
            Vector referants = ((DTMManagerEx)mgr).getReferants();
            Vector referantFunctions = ((DTMManagerEx)mgr).getReferantFunctions();
            result.referredNodes=referants;
            result.referredFunctions=referantFunctions;
            result.result=res;
            ((DTMManagerEx)mgr).setReferantTracking(false);
            ((DTMManagerEx)mgr).detatchReferants();
        }
        //xpath.getExpression().callVisitors(xpath,visitor);
    }
    
        /* xalan 2.3.1 version
    public void evalWithTrace(Node contextNode, String query, Node namespaceNode, LookupResult result)
    throws TransformerException
    {
        PrefixResolverDefault pr = new PrefixResolverDefault(
        (namespaceNode.getNodeType() == Node.DOCUMENT_NODE)
        ? ((Document) namespaceNode).getDocumentElement() : namespaceNode);
         
        XPathParser parser = new XPathParser(this, null);
        Compiler2 cmp = new Compiler2(this, null,this);
        parser.initXPath(cmp, query, pr);
        //System.out.println("\niniting: "+query);
        Expression ex = cmp.compile(0);
        if (xpathSupport==null) xpathSupport = new XPathContextEx(null,modelContext);
        int ctxtNode = xpathSupport.getDTMHandleFromNode(contextNode);
         
        xpathSupport.pushNamespaceContext(pr);
         
        xpathSupport.pushCurrentNodeAndExpression(ctxtNode, ctxtNode);
        // Hack for bug in Xalan 2.3.1: current() pulls the current-node out
        // of the context-node-list, which happens to be different from
        // where the current-node is stored by
        // pushCurrentNodeAndExpression()
        xpathSupport.pushContextNodeList(new NodeSetDTM(ctxtNode, xpathSupport));
         
        XObject xobj = null;
         
        xpathRefNodes = new Vector();
        xobj = ex.execute(xpathSupport);
        //			System.out.println("Evaluated: "+ex.execute(xpathSupport));
        //cmp.printIterators(xpathSupport);
         
        //            symbols = context.getSymbols();
        //            idxMgr = context.getIndexManager();
         
        result.result=xobj;
        result.referredNodes=xpathRefNodes;
        //return xpathRefNodes;
    }
         */
    
    
    public void error(TransformerException exception)
    {
        System.out.println(exception.toString());
    }
    public void fatalError(TransformerException exception)
    {
        this.error(exception);
    }
    
    public void warning(TransformerException exception)
    {
        this.error(exception);
    }
    
    
}
