/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;




import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;



/**
 * Copy element
 * The real functionality is in the itemset implementation
 * @author Mikko Honkala
 */

public class CopyElementImpl extends DynBoundElementImpl
{
    
    public CopyElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        bindingAttributesRequired=true;
    }
    
    public void init()
    {
        try
        {
            this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
            return;
        }
        super.init();
    }
    
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor)
    {
        if (this.getParentNode() instanceof ItemSetElementImpl)
        {
            //Log.debug("BLOCKING"+this +"got event that parents binding has changed. parent: "+ancestor);
        }
        else
        {
            super.notifyParentBindingChanged(ancestor);
        }
    }
    
}
