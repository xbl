/*
 * Created on Mar 16, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface ChildListener
{
        public void childEvent(boolean added, Element parent, Node child);
}
