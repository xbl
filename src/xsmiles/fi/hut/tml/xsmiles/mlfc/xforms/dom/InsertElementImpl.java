/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerException;

import org.apache.xml.utils.XMLCharacterRecognizer;


/**
 * XForm/insert element implementation
 * @author Mikko Honkala
 */


public class InsertElementImpl extends RepeatHandlerBaseImpl
{
    /** the prototype element for insert into empty situations */
    protected Element prototypeInstanceElement;
    
    public InsertElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        this.renewBinding();
        //if (this.prototypeInstanceElement==null) this.savePrototypeElement();
    }

    
    /** save the first element of the bound nodeset as a prototype insert element */
    protected void savePrototypeElement(NodeList boundNodes)
    {
        if (boundNodes!=null&&boundNodes.getLength()>0)
        {
            Node firstNode = boundNodes.item(0);
            if (firstNode.getNodeType()==Node.ELEMENT_NODE)
            {
                this.prototypeInstanceElement=(Element)firstNode.cloneNode(true);
                //Log.debug("Saved prototype element: ");this.debugNode(this.prototypeInstanceElement);
            }
        }
    }
    
    public void activate(Event evt)
    {
        super.activate(evt);
        boolean before=true;
        
        // get the position attibute ("before" / "after")
        String position = this.getPosition();
        if (position!=null && position.equals("after")) before=false;
        int at = this.getAt();
        NodeList nodes = null;
        nodes = this.getBoundNodeset();
        
        // TODO: check that index is at bounds
        Log.debug("insert At: "+at);
        try
        {
                        /*
                        There are three cases for insert:
                        1. insert into empty
                                We have to get a) the XPath parent
                                               b) the prototype from the original instance
                        2. insert before a node
                        3. insert after a node
                         */
            InstanceElementImpl instance=this.getModel().getInstance();
            //this.getModel().instanceStructureChanged();
            
            Node proto=null, next=null, parent=null;
            if (nodes==null||nodes.getLength()==0)
            {
                // The INSERT-INTO EMPTY situation = we have to get the prototype
                // from the original instance data
                proto=this.prototypeFromOriginalInstance();
                parent=this.getParentForXPath(this.getRef());
                next=null;
            }
            else
            {
                proto=nodes.item(0);
                parent=proto.getParentNode();
                int atmax = (at>nodes.getLength()?nodes.getLength():at);
                if (before)
                {
                    next = nodes.item(atmax-1);
                }
                else
                {
                    next = nodes.item(atmax-1).getNextSibling();
                }
            }
            
            Node clone = proto.cloneNode(true);
            this.clearNodeText(clone,true);
            //			this.debugNode(clone);
            //this.getModel().instanceStructureChanged();
            if (parent!=null)
            {
	            parent.insertBefore(clone,next);
	            // call repaint for the root node of this DOM
	            // TODO: context info
	            instance.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.INSERT_EVENT));
	            this.getModel().rebuild(true);
            }
        } catch (Exception e)
        {
            this.getHandler().showUserError(e);
            Log.error(e);
        }
    }
    /*
    protected Node prototypeFromOriginalInstance_old() throws SAXException, TransformerException
    {
        InstanceElementImpl instance=this.getModel().getInstance();
        Node originalInstance=instance.getInitialInstance();
        this.getModel().instanceStructureChanged();
        Node found= this.getModel().getXPathEngine.evalToNodelist(this.getRef(), this, originalInstance).item(0);
        if (found==null) return null;
        //instance.instanceStructureChanged();
        return instance.getInstanceDocument().importNode(found,true);
    }*/
    protected Node prototypeFromOriginalInstance() throws SAXException,TransformerException
    {
        if (prototypeInstanceElement!=null)
        {
            return this.prototypeInstanceElement.cloneNode(true);
        }
        else
        {
            this.debugNode(this);
            throw new TransformerException("Insert: prototypeElement was null.");
        }
        
        //instance.instanceStructureChanged();
        /*
        Node found= instance.findNodeList(this.getRef(), this, originalInstance).item(0);
        if (found==null) return null;
        //instance.instanceStructureChanged();
        return instance.getInstanceDocument().importNode(found,true);
         **/
    }
    protected Node getParentForXPath(String xpath) throws SAXException, Exception
    {
        XPathExpr xpathE = this.getModel().getXPathEngine().createXPathExpression(xpath);
        InstanceElementImpl instance=this.getModel().getInstance();
        int index = xpath.lastIndexOf('/');
        String newPath = xpath.substring(0,index);
        Log.debug("Excecution newPath"+newPath+" index:"+index);
        Object obj = this.getModel().getXPathEngine().eval(null,xpathE,this,null);
        if (obj!=null&&obj instanceof NodeList)
        {
            NodeList list = (NodeList)obj;
            if (list!=null&&list.getLength()>0)
            {
                return list.item(0);
            }
        }
        return null;
        //return instance.findNode(newPath, this, null);
    }
    
    /**
     * Clear all text nodes and attribute values. If 'deep', do it recursively.
     */
    protected void clearNodeText(Node n,boolean deep)
    {
        // clear attributes
        NamedNodeMap nodemap = n.getAttributes();
        for (int i=0;i<nodemap.getLength();i++)
        {
            Attr attr = (Attr)nodemap.item(i);
            attr.setValue("");
        }
        // clear children
        Node child = n.getFirstChild();
        while (child!=null)
        {
            if (child.getNodeType()==Node.TEXT_NODE)
            {
                // clear text nodes if not only whitespace (to retain line feeds etc.)
                if (!XMLCharacterRecognizer.isWhiteSpace(child.getNodeValue()))
                    child.setNodeValue("");
                
            }
            if (child.getNodeType()==Node.ELEMENT_NODE && deep)
            {
                // Clear also child elements
                this.clearNodeText(child,true);
            }
            child=child.getNextSibling();
        }
    }
    
    public String getPosition()
    {
        return this.getAttribute("position");
    }
    
        /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        super.notifyBindingChanged(newBinding);
        savePrototypeElement(newBinding);
        
    }

    
    public Object clone() throws java.lang.CloneNotSupportedException
    {
        //Log.debug("** insert.clone called.");
        InsertElementImpl insert = (InsertElementImpl)super.clone();
        insert.prototypeInstanceElement=this.prototypeInstanceElement;
        return insert;
    }
    
    
}

