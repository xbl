/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 8, 2005
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dom.swing;


import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Vector;

public class XMLTreeModel implements TreeModel {
    private Vector treeModelListeners = new Vector();
    private Node rootNode;

    public XMLTreeModel(Node root) {
        rootNode = root;
    }


    public void setRoot(Node root)
    {
        rootNode=root;
    }

    /**
     * The only event raised by this model is TreeStructureChanged with the
     * root as path, i.e. the whole tree has changed.
     */
    protected void fireTreeStructureChanged(Node n, Node parent,boolean removed) {
        int len = treeModelListeners.size();
        TreeModelEvent e = new TreeModelEvent(this, 
                                              new Object[] {rootNode});
        for (int i = 0; i < len; i++) {
            //((TreeModelListener)treeModelListeners.elementAt(i)).treeNodesRemoved(e);
            //((TreeModelListener)treeModelListeners.elementAt(i)).treeNodesInserted(e);
            ((TreeModelListener)treeModelListeners.elementAt(i)).
            treeStructureChanged(e);
        }
		
    }


//////////////// TreeModel interface implementation ///////////////////////

    /**
     * Adds a listener for the TreeModelEvent posted after the tree changes.
     */
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.addElement(l);
    }

    /**
     * Returns the child of parent at index index in the parent's child array.
     */
    public Object getChild(Object parent, int index) {
        Element p = (Element)parent;
        return getChildNode(p,index);
    }

    /**
     * Returns the number of children of parent.
     */
    public int getChildCount(Object parent) {
        Element p = (Element)parent;
        return getChildCount(p);
    }
    /*
    protected NodeList getChildren(Element n)
    {
        return n.getElementsByTagName("*");
    }*/
    
    protected Node getChildNode(Element e, int index)
    {
        int i=0;
        Node n = e.getFirstChild();
        while (n!=null)
        {
            if (n.getNodeType()==Node.ELEMENT_NODE)
            {
                if (i==index) return n;
                i++;
            }
            n=n.getNextSibling();
        }
        return null;
    }
    
    protected int getChildCount(Element e)
    {
        int i=0;
        Node n = e.getFirstChild();
        while (n!=null)
        {
            if (n.getNodeType()==Node.ELEMENT_NODE) i++;
            n=n.getNextSibling();
        }
        return i;
    }

    /**
     * Returns the index of child in parent.
     */
    public int getIndexOfChild(Object parent, Object child) {
        Element e = (Element)parent;
        Node c = (Element)child;
        int i=0;
        Node n = e.getFirstChild();
        while (n!=null)
        {
            if (n.getNodeType()==Node.ELEMENT_NODE)
            {
                if (n==child) return i;
                i++;
            }
            n=n.getNextSibling();
        }
        return -1;
    }

    /**
     * Returns the root of the tree.
     */
    public Object getRoot() {
        return rootNode;
    }

    /**
     * Returns true if node is a leaf.
     */
    public boolean isLeaf(Object node) {
        if (!(node instanceof Element)) return true;
        Element p = (Element)node;
        return getChildCount(p) == 0;
    }

    /**
     * Removes a listener previously added with addTreeModelListener().
     */
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.removeElement(l);
    }

    /**
     * Messaged when the user has altered the value for the item
     * identified by path to newValue.  Not used by this model.
     */
    public void valueForPathChanged(TreePath path, Object newValue) {
        System.out.println("*** valueForPathChanged : "
                           + path + " --> " + newValue);
    }
}
