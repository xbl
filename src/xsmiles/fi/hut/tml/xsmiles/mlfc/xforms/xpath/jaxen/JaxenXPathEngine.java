/*
 * Created on Apr 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen;

import java.util.List;

import org.jaxen.FunctionContext;
import org.jaxen.JaxenException;
import org.jaxen.SimpleVariableContext;
import org.jaxen.VariableContext;
import org.jaxen.XPath;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class JaxenXPathEngine implements XPathEngine {
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#createXPathExpression(java.lang.String)
     */
    XFormsContext xformsContext;
    ModelContext modelContext;
    
    protected static JaxenXFormsFunctions functions=new JaxenXFormsFunctions();

    public JaxenXPathEngine()
    {
        Log.info("JaxenXPathEngine created.");
    }
    public void setContext(XFormsContext handler,ModelContext mcontext)
    {
        this.xformsContext=handler;
        this.modelContext=mcontext;
    }
    public XPathExpr createXPathExpression(String xpath) {
        try
        {
            return new JaxenXPathExpr(xpath);
        } catch (JaxenException e)
        {
            Log.error(e);
            return null;
        }
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#eval(org.w3c.dom.Node, fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr, org.w3c.dom.Node, org.w3c.dom.NodeList)
     */
    public Object eval(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList) throws Exception {
        XPath xpath=this.initXPath(contextNode,expr,namespaceNode,contextList);
        Object ret = xpath.evaluate(contextNode);
        if (ret instanceof List) return this.listToNodeList((List)ret);
        return ret;
    }
    public static final String VARIABLE_NS="xsmiles_xforms_variables";
    public static final String CONTEXT_NODE_VARIABLE="xsmiles_xforms_context_node";
    protected XPath initXPath(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList)
    {
        JaxenXPathExpr jexpr=(JaxenXPathExpr)expr;
        if (namespaceNode.getNodeType()==Node.DOCUMENT_NODE) namespaceNode = ((Document)namespaceNode).getDocumentElement();
        NamespaceAdapter adapter = new NamespaceAdapter((Element)namespaceNode);
        functions.setXPathEngine(this);
        XPath xpath = jexpr.getXPath();
        xpath.setNamespaceContext(adapter);
        xpath.setFunctionContext(functions);
        VariableContext vctx = xpath.getVariableContext();
        if (vctx instanceof SimpleVariableContext)
        {
            SimpleVariableContext simple = (SimpleVariableContext)vctx;
            simple.setVariableValue(VARIABLE_NS,CONTEXT_NODE_VARIABLE,contextNode);
        }
        
        return xpath;
        
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#evalToString(org.w3c.dom.Node, fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr, org.w3c.dom.Node, org.w3c.dom.NodeList)
     */
    public String evalToString(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList)
            throws Exception {
        // TODO Auto-generated method stub
        contextNode=this.checkContextNode(contextNode);
        XPath xpath=this.initXPath(contextNode,expr,namespaceNode,contextList);
        String ret = xpath.valueOf(contextNode);
        return ret;
    }
    public class ListNodeList implements NodeList
    {
        protected List list;
        public ListNodeList(List l)
        {
            this.list=l;
        }
        public int getLength()
        {
            return this.list.size();
        }
        public Node item(int i)
        {
            return (Node)this.list.get(i);
        }
    }
    public NodeList listToNodeList(List l)
    {
        return new ListNodeList(l);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#evalToNodelist(org.w3c.dom.Node, fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr, org.w3c.dom.Node, org.w3c.dom.NodeList)
     */
    public NodeList evalToNodelist(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList)
            throws Exception {
        // TODO Auto-generated method stub
        contextNode=this.checkContextNode(contextNode);
        XPath xpath=this.initXPath(contextNode,expr,namespaceNode,contextList);
        List nodes = xpath.selectNodes(contextNode);
        return listToNodeList(nodes);
        
    }
    
    protected Node checkContextNode(Node contextNode)
    {
        if (contextNode==null) contextNode=this.modelContext.getInstanceDocument(null).getDocumentElement();
        return contextNode;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#evalWithTrace(org.w3c.dom.Node, fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr, org.w3c.dom.Node, org.w3c.dom.NodeList, fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult)
     */
    public synchronized Object evalWithTrace(Node contextNode, XPathExpr expr, Node namespaceNode, NodeList contextList,
            LookupResult result) throws Exception {
        // TODO Auto-generated method stub
        JaxenXPathExpr jexpr=(JaxenXPathExpr)expr;
        jexpr.trackReferences(true);
        Object ret = this.eval(contextNode,expr,namespaceNode,contextList);
        //BaseXPathEx xpath = (BaseXPathEx)jexpr.getXPath();
        result.referredNodes=jexpr.referredNodes;
        jexpr.referredNodes=null;
        return ret;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#hasFunction(java.lang.String, java.lang.String)
     */
    public boolean hasFunction(String uri, String funcName) {
        // TODO Auto-generated method stub
        return false;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#getModelContext()
     */
    public ModelContext getModelContext() {
        return this.modelContext;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine#getXFormsContext()
     */
    public XFormsContext getXFormsContext() {
        // TODO Auto-generated method stub
        return this.xformsContext;
    }
}
