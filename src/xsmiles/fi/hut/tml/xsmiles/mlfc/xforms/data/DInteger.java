/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;
import fi.hut.tml.xsmiles.Log;
import java.math.BigDecimal;

public class DInteger extends DDecimal implements Data{

	public DInteger(short dtype)
	{
        super(dtype);
	}
    protected String toSchemaStringInternal()
    {
        return this.decimalValue.toBigInteger().toString();
    }
    // RangeDecimal
} 