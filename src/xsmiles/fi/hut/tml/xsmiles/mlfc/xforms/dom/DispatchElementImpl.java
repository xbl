/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.dom.EventImpl;


import org.w3c.dom.*;
import org.w3c.dom.events.*;

/**
 * revalidate action implementation
 * @author Mikko Honkala
 */


public class DispatchElementImpl extends XFormsElementImpl implements EventHandlerService
{
    
    
    public DispatchElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void activate(Event evt)
    {
        try
        {
            Element target = this.getTargetElement();
            String name = this.getName();
            Event ev = XFormsEventFactory.createEvent(name);
            if (ev==null) throw new XFormsException("Could not create event: "+name);
            if ( (ev instanceof EventImpl && !((EventImpl)ev).isInited()) || !(ev instanceof EventImpl))
            {
                // init
                ev.initEvent(name,this.getBubbles(),this.getCancelable());
                
            } 
            Log.debug("dispatching "+ev.getType()+ev+"to "+target);
            ((EventTarget)target).dispatchEvent(ev);
        } catch (Exception e)
        {
            Log.error(e);
            this.getHandler().showUserError(e);
        }
    }
    
    /** @return the element that the target attribute points to */
    protected Element getTargetElement() throws XFormsException
    {
        Attr nameAttr = this.getAttributeNode("target");
        if (nameAttr==null) throw new XFormsException("Missing target attribute");
        else 
        {
            String val = nameAttr.getNodeValue();
            if (val.length()<1)
                throw new XFormsException("Empty target attribute");
            Element target = this.getOwnerDocument().getElementById(val);
            if (target==null) throw new XFormsException("No element with id: "+val);
            return target;
            
        }
    }
    
    /** @return the name of the event, from the name attribute */
    protected String getName() throws XFormsException
    {
        Attr nameAttr = this.getAttributeNode("name");
        if (nameAttr==null) throw new XFormsException("Missing name attribute");
        else 
        {
            String val = nameAttr.getNodeValue();
            if (val.length()<1)
                throw new XFormsException("Empty name attribute");
            return val;
        }
    }
    
    /** @return a boolean value of given attribute, or default, if that attribute was not present */
    protected boolean getBooleanAttribute(String attrName, boolean defaultValue) throws XFormsException
    {
        Attr nameAttr = this.getAttributeNode(attrName);
        if (nameAttr==null) return defaultValue;
        String val  = nameAttr.getNodeValue();
        if (val.equals("true")||val.equals("1")) return true;
        if (val.equals("false")||val.equals("0")) return false;
        throw new XFormsException("Illegal boolean value for attribute: "+attrName+" value:"+val);
    }
    
    protected boolean getBubbles() throws XFormsException
    {
        return this.getBooleanAttribute("bubbles",true);
    }
    
    protected boolean getCancelable() throws XFormsException
    {
        return this.getBooleanAttribute("cancelable",true);
    }
    
    
}
