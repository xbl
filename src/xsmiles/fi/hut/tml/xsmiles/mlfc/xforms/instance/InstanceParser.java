/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import java.io.Reader;
import java.io.StringReader;
import java.util.Vector;

import org.apache.xerces.impl.Constants;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMError;
import org.w3c.dom.DOMErrorHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMEntityResolver;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.xml.serializer.SerializerFactory;
import fi.hut.tml.xsmiles.xml.serializer.XMLSerializerInterface;



/**
 * This class can read an instance from an stream or URI
 * and attach a schema to it (PSVI)
 */
public class InstanceParser implements ErrorHandler, EntityResolver, DOMEntityResolver,DOMErrorHandler
{
    
    
    protected Vector errorList;
    
    static final String documentClassname =XFormsConfiguration.getInstance().getInstanceDocumentClassName();
    
    /** Property identifier: symbol table. */
    public static final String SYMBOL_TABLE =
    Constants.XERCES_PROPERTY_PREFIX + Constants.SYMBOL_TABLE_PROPERTY;
    
    /** Property identifier: grammar pool. */
    public static final String GRAMMAR_POOL =
    Constants.XERCES_PROPERTY_PREFIX + Constants.XMLGRAMMAR_POOL_PROPERTY;
    
    
    
    /**
     * this method takes the original instance element and copies children, such
     * as PI's to the new instance document
     */
    protected void copySiblings(Document doc,Element instance)
    {
        Element newInstance = doc.getDocumentElement();
        boolean before=true;
        Node child = instance.getParentNode().getFirstChild();
        while(child!=null)
        {
            if (child.getNodeType()==Node.PROCESSING_INSTRUCTION_NODE)
            {
                Node imported = doc.importNode(child,false);
                if (before) doc.insertBefore(imported,newInstance);
                else doc.appendChild(imported);
            } else
            {
                before=false;
            }
            child=child.getNextSibling();
        }
    }
    /**
     * public Document read(Reader r, String baseURL, URL schemaURL)
     * {
     * return this.read(r,
     * }
     * public Document read(Reader r, String baseURL, String schema)
     **/
    protected Element findFirstElementChild(Element e)
    {
        Node child = e.getFirstChild();
        while (child!=null)
        {
            if (child instanceof Element) return (Element)child;
            child=child.getNextSibling();
        }
        return null;
    }
    
    protected static final String XSIATTR = "xmlns:xsi";
    protected static final String NONAMESPACEATTR = "xsi:noNamespaceSchemaLocation";
    
    protected static final String XSDATTR="xmlns:xsd";
    protected static final String XSDNS="http://www.w3.org/2001/XMLSchema";
    /*
    protected void addXSDAttribute(Element instance)
    {
        Attr ns = instance.getAttributeNode(XSDATTR);
        if (ns==null)
        {
            ns=instance.getOwnerDocument().createAttribute(XSDATTR);
            instance.setAttributeNodeNS(ns);
        }
        ns.setNodeValue(XSDNS);
    }*/
    
    protected static String getAttrPrefix(String attrName)
    {
        int index = attrName.lastIndexOf(':');
        if (index<0) return null;
        else return attrName.substring(index+1);
    }
    protected static String getElementPrefix(Element elem)
    {
        String elemName = elem.getNodeName();
        int index = elemName.indexOf(':');
        if (index<0) return null;
        else return elemName.substring(0,index);
    }
    
    /** this mehod copies xmlns:xxx attributes from an element to another. It goes towards to the
     * root and does it for every node if recursive is true
     */
    public static void copyClosestDefaultNSDeclaration(Element to, Element from)
    {
        // don't search if already exists
        if (to.getAttributeNode("xmlns")!=null) return;
        // go downward the tree unting xmlns= attribute is found, then copy
        for (Element n=from;n!=null;n=(Element)n.getParentNode())
        {
            Attr fromattr = n.getAttributeNode("xmlns");
            if (fromattr!=null)
            {
                Attr copied = to.getOwnerDocument().createAttribute("xmlns");
                copied.setNodeValue(fromattr.getNodeValue());
                to.setAttributeNode(copied);
                break;
            }
            
        }
    }
    /** this mehod copies xmlns:xxx attributes from an element to another. It goes towards to the
     * root and does it for every node if recursive is true
     */
    public static void copyNamespaceDeclarations(Element to, Element from, boolean recursive, String elementPrefix)
    {
        //Log.debug("copying from: ");XFormsElementImpl.debugNode(from);
        //Log.debug("copying to: ");XFormsElementImpl.debugNode(to);
        
        NamedNodeMap attrs = from.getAttributes();
        for (int i=0;i<attrs.getLength();i++)
        {
            Attr attr = (Attr)attrs.item(i);
            String attrName = attr.getNodeName();
            // this is a kludge, because xalan serializer adds the used namespaces again,
            // so it is a problem if the root element is prefixed with the same prefix as a
            // namespace declaration of an ancestor
            String attrPrefix = getAttrPrefix(attrName);
            //Log.debug("Checking : "+attrName+" to hasAttribute:"+to.hasAttribute(attrName)+" nodeNa:"+to.getNodeName());
            if (attrName.startsWith("xmlns:")&&!to.hasAttribute(attrName))
            {
                String attrValue = attr.getNodeValue();
                    /*
                    Attr copied = to.getOwnerDocument().createAttributeNS(
                        "http://www.w3.org/XML/1998/namespace",
                        getAttrPrefix(attrName));
                     */
                // In xerces 2.4.0, we have to create the ns declaration with the xml namespace
                //Attr copied = to.getOwnerDocument().createAttribute(attrName);
                Attr copied = to.getOwnerDocument().createAttributeNS("http://www.w3.org/2000/xmlns/",attrName);
                copied.setNodeValue(attrValue);
                to.setAttributeNode(copied);
                Log.debug("copied : "+copied);
                //to.setAttributeNode(((Attr)attr.cloneNode());
            }
        }
        if (recursive)
        {
            Node parent = from.getParentNode();
            if (parent.getNodeType()==Node.ELEMENT_NODE)
                copyNamespaceDeclarations(to,(Element)parent,recursive,elementPrefix);
        }
    }
    public static String writeXML(Node elem) throws Exception
    {
        return writeXML(elem,true);
    }
    public static String writeXML(Node elem,boolean fixupNS) throws Exception
    {
        // create DOM Serializer
        XMLSerializerInterface serializer = new 
            SerializerFactory().getXMLSerializer();
        //fi.hut.tml.xsmiles.xml.serializer.XMLSerializer();
        return serializer.writeToString(elem,fixupNS);
    }
    
    /**
     * Read in a instance from reader, using the given schema pool
     **/
    public Document read(Reader r, String baseURL, SchemaPool schemaPool)
    {
        // REVISIT: this does not currently handle schema imports
        // or entity references in the external instance
        try
        {
            System.out.println("Loading document" +baseURL);
            errorList=new Vector();
            Document doc = XFormsConfiguration.getInstance().loadDocument(r,baseURL,false,schemaPool,this,this);
            //CoreDocumentImpl core = (CoreDocumentImpl)doc;
            //core.setDocumentURI(docURL);
            return doc;
        } catch (Exception e)
        {
            Log.error(e);
        }
        return null;
    }
    /** method for reading an element and its contents to a new instance document
     * this method uses the other method with a newly created string reader */
    public Document read(Node root, String baseURL, SchemaPool schemaPool) throws Exception
    {
        // changing the attribute on the root element?
        Element instance=null;
        if (root instanceof Document)
            instance=((Document)root).getDocumentElement();
        else if (root instanceof Element)
            instance=findFirstElementChild((Element)root);
        if (instance == null) return null;
        
        //copy namespace declarations
        Node parent=instance.getParentNode();
        if (parent.getNodeType()==Node.ELEMENT_NODE)
        {
            this.copyNamespaceDeclarations(instance,(Element)parent,true,this.getElementPrefix(instance));
            // we don't have to do this with xerces 2.4.0
            //this.copyClosestDefaultNSDeclaration(instance,(Element)parent);
        }
        // write instance to temp stringbuffer
        // TODO: preserve inline instance's root PIs?
        String instStr = writeXML(instance,false);
        //Log.debug("TEMP INSTANCE:\n"+instStr);
        // read instance using my parser
        //Log.debug("Temp instance string: "+instStr);
        StringReader instanceReader = new StringReader(instStr);
        errorList=new Vector();
        InstanceDocument instDoc = (InstanceDocument)XFormsConfiguration.getInstance().loadDocument(instanceReader,baseURL,false,schemaPool,this,this);
        // this will copy PI's
        this.copySiblings(instDoc,instance);
        
        return instDoc;
    }
    
    //protected   DOMImplementationLS impl;
    /** create builder for JDK 1.1 -> */
    /*
    protected DOMBuilderImpl getDOMBuilderImpl11(boolean plain, SchemaPool pool) throws Exception
    {
        return XFormsConfiguration.getInstance().getDOMBuilderImpl(plain,pool,this,this);
       
    }
    */
    
    

    
    
    
    /***************** RESOLVE ENTITIES (SCHEMA) *************/
    public InputSource resolveEntity(String publicId, String systemId)
    {
        System.out.println("resolveEntity: "+publicId+" : "+systemId);
        /*
        if (systemId!=null&&systemId.length()>0)
        {
            try
            {
                //return new InputSource(this.getEntityInputStream(new URL(systemId)));
                return new InputSource(this.getSchemaReader());
            } catch (Exception e)
            {
                e.printStackTrace(System.out);
            }
        }*/
        return null;
    }
    
    public org.w3c.dom.ls.DOMInputSource resolveEntity(String publicId,
    String systemId,
    String baseURI)
    {
        //System.out.println("DOMresolveEntity: "+publicId+" : "+systemId);
        /*
        if (systemId!=null&&systemId.length()>0)
        {
            try
            {
                //return new InputSource(this.getEntityInputStream(new URL(systemId)));
                DOMInputSource source = impl.createDOMInputSource();
                source.setCharacterStream(this.schemaReader);
                //source.setCharacterStream(this.getEntityInputStream(new URL(base+"/"+schemaFilename)));
                return source;
                //return new InputSource(this.getEntityInputStream(new URL(base+"/"+schemaFilename)));
            } catch (Exception e)
            {
                e.printStackTrace(System.out);
            }
        }*/
        return null;
    }
    /******************** ERROR HANDLING ********************/
    
    
    public boolean handleError(DOMError error)
    {
        short severity = error.getSeverity();
        if (severity == error.SEVERITY_ERROR)
        {
            Log.debug("XX[dom3-error]: "+error.getMessage());
            errorList.addElement(error);
        }
        
        if (severity == error.SEVERITY_WARNING)
        {
            Log.debug("XX[dom3-warning]: "+error.getMessage());
        }
        return true;
        
    }
    
    public void error(SAXParseException exception)
    {
        handleError(exception);
    }
    public void fatalError(SAXParseException exception)
    {
        handleError(exception);
    }
    public void warning(SAXParseException exception)
    {
        handleError(exception);
    }
    static int errCount=0;
    private void handleError(SAXParseException ex)
    {
        /*
        try
        {
            Node current = ( Node ) parser.getProperty( "http://apache.org/xml/properties/dom/current-element-node" );
            //Node current = parser.getCurrentNode();
            errCount++;
            StringBuffer errorString = new StringBuffer();
            errorString.append("at line number, ");
            errorString.append(ex.getLineNumber());
            errorString.append(": ");
            errorString.append(ex.getMessage());
            String errStr = errorString.toString();
            System.out.println(errStr);
        } catch (Exception e)
        {
            e.printStackTrace(System.out);
        }*/
    }
    
    
}

