/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 6, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.awt.event.ActionListener;
import java.io.Reader;


/**
 * An interface for dialog-type of GUI, such as Command Prompt or the speech GUI
 * It will take in Speech grammars, or accept anything if grammar is null.
 * @author honkkis
 *
 */
public interface DialogUI
{
		public void append(String text);
    	public void showQuestion(String question, Reader grammar);
    	public void addActionListener(ActionListener l);
    	public void removeActionListener(ActionListener l);
    	public void destroy();
    	public void setGrammar(Reader r);
        public void stopSpeech();
        public void stopRecognizer();
        /** stops both the speech synth and recognizer */ 
        public void stopAll();
    	//public Result get

}
