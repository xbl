/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XChangeListener;
import java.awt.event.*;
import org.w3c.dom.Element;


public class OutputAny extends AbstractControl 
{
    protected XCaption fOutput;
	public OutputAny(XFormsContext context,Element elem)
	{
        super(context,elem);
        this.createControl();
	}
    /** creates the control with the component factory */
    protected void createControl()
    {
		fOutput = this.fContext.getComponentFactory().getXCaption("");
                
                fOutput.setMinimumSize(new java.awt.Dimension(150,10));
        // TODO: how to set caption (note: must be dynamic)
        // maybe the class extended from XFormsControl should set the caption
        //fInput.setCaptionText("testcaption");
    }
    /**
     * returns the abstract component for this control. The
     * abstract component can be used e.g. to style the component
     * but all listeners should be added to the AdaptiveControl and not directly 
     * to the XComponent */
    public XComponent getComponent()
    {
        return fOutput;
    }
    /**
     * get the components current value
     */
    public Data getValue()
    {
        return null;
    }
    
    public void destroy()
    {
        super.destroy();
    }
    /**
     * set the components value
     *
     */
    public boolean isInputControl() //input, range are input controls
    {
        return true;
    }

    public void updateDisplay()
    {
        if (!insideEvent)
        {
            insideEvent=true;
			if (this.ownerElem instanceof DisplayValueProvider)
			{
				DisplayValueProvider prov = (DisplayValueProvider)ownerElem;
				fOutput.setText(prov.getDisplayValue());
			}
			else
			{
				fOutput.setText((String)getData().toDisplayValue());
			}
            insideEvent=false;
        }
    }
    public boolean isWritable()
    {
        return false;
    }

} // OutputAny
