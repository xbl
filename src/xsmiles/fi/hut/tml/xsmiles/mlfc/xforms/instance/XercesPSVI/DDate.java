/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DData;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;

import java.util.Calendar;
import java.util.Date;

import org.apache.xerces.impl.dv.xs.DateDV;
import org.apache.xerces.impl.dv.xs.AbstractDateTimeDV;
import org.apache.xerces.impl.dv.xs.DateTimeDVEx;
//import org.apache.xerces.impl.dv.xs.AbstractDateTimeDV.DateTimeData;

// for display string
import java.util.Locale;
import java.text.DateFormat;

public class DDate extends DData implements Data
{
    
    
    protected Calendar calValue;
    public DDate(short dtype)
    {
        super(dtype);
    }
    
    /**
     * set the value as a Java Object
     */
    public void setValueFromObjectInternal(Object obj)
    {
        if (obj instanceof int[])
        {
            int[] xercesdate = (int[])obj;
            this.setValueFromXerces(xercesdate);
        }
        
        else if (obj instanceof Calendar)
        {
            calValue = (Calendar)obj;
        }
        else if (obj instanceof Date)
        {
            Date date = (Date)obj;
            calValue = Calendar.getInstance();
            calValue.setTime(date);
        }
        else if (DateTimeDVEx.isInternalValue(obj))
        {
            obj = DateTimeDVEx.getInternalValue(obj);
            int[] xercesdate = (int[])obj;
            this.setValueFromXerces(xercesdate);
        }
        else
        {
            Log.error(this+" got wrong type of value:"+obj);
        }
    }
    
    /**
     * set the value from a display string
     */
    public boolean setValueFromDisplayInternal(String displayValue)
    {
        Log.error(this+ "cannot set from display value");
        return true;
    }
    
    /**
     * set the value from a Schema string
     */
    protected void setValueFromSchemaInternal(String schemaValue)
    {
        //Log.error("Not implemented");
        try
        {
            DateDV dv = new DateDV();
            Object datev = dv.getActualValue(schemaValue);
            if (datev instanceof int[])
            {
                this.setValueFromXerces((int [])datev);
            }
            else if (DateTimeDVEx.isInternalValue(datev))
            {
                datev = DateTimeDVEx.getInternalValue(datev);
                int[] xercesdate = (int[])datev;
                this.setValueFromXerces(xercesdate);
            }
        } catch (Exception e)
        {
            Log.error(e,"While trying to parse date: "+schemaValue);
        }
        
    }
    
    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    protected String toDisplayValueInternal()
    {
        Locale locale = null;
        locale = Locale.getDefault();
        
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG,locale);
        return (df.format(calValue.getTime()));
    }
    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    protected String toSchemaStringInternal()
    {
        //Calendar calendar = new GregorianCalendar();
        Calendar calendar = calValue;
        java.text.DecimalFormat format = new java.text.DecimalFormat();
        format.setMinimumIntegerDigits(2);
        
                /*
                From schema datatypes spec:
                For example, to indicate 1:20 pm on May the 31st, 1999 for Eastern Standard Time
                which is 5 hours behind Coordinated Universal Time (UTC), one would write:
                1999-05-31T13:20:00-05:00.
                 */
        String date=
        calendar.get(Calendar.YEAR)+"-"+
        format.format((calendar.get(Calendar.MONTH)+1))+"-"+
        format.format(calendar.get(Calendar.DAY_OF_MONTH));
        return date;
    }
    
    /** this method reads in Xerces internal format and converts it to
     * a java calendar format. Timezone not supported yet */
    public void setValueFromXerces(int[] xercesdate)
    {
        try
        {
            int m_year = Math.abs(xercesdate[0]);
            int m_month = xercesdate[1];
            int m_day = xercesdate[2];
            int m_hour = xercesdate[3];
            int m_minute = xercesdate[4];
            int m_second = xercesdate[5]+(xercesdate[6]/1000);
            //m_zone = xercesdate[6]; // not sure how encoded
            
            boolean m_signed = (xercesdate[0] < 0);
            
          /* Works, but gives slightly different results. Need to
           * investigate... */
            Calendar cal = Calendar.getInstance();
            cal.set(m_year,(m_month-1),m_day,m_hour,m_minute,(int)m_second);
            calValue=cal;
            
        }
        catch (Exception ex)
        {
            Log.error(ex,"Creating a dateTimeObj");
        }
    }
    
    /**
     * get the java object corresponding to the value
     */
    public Object toObjectInternal()
    {
        return this.calValue;
    }
    /**
     * get the java object corresponding to the value
     */
    public Calendar toCalendar()
    {
        return this.calValue;
    }
    /** instructs subclass to set value to null, this is used for
     * invalid values
     */
    protected void clearValue()
    {
        this.calValue=null;
    }
} // DDate
