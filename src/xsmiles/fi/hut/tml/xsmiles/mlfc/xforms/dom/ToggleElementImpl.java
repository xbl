/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

import fi.hut.tml.xsmiles.dom.EventHandlerService;

import org.w3c.dom.events.*;

/**
 * Toggle action element implementation
 * @author Mikko Honkala
 */


public class ToggleElementImpl extends XFormsElementImpl implements EventHandlerService {


	public ToggleElementImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}
	public void activate(Event evt)
	{
		Log.debug("Toggle.activate(evt)");
		String caseId = this.getCaseId();
		if (caseId==null||caseId.length()==0)
		{
			Log.debug("No case attribute on toggle element.");
			return;
		}
		CaseElementImpl caseElem = this.getHandler().getCase(caseId);
		if (caseElem == null)
		{
			Log.debug("No case element with id."+caseId);
			return;
		}
		XFormsEventImpl event = (XFormsEventImpl)XFormsEventFactory.createEvent(XFormsEventFactory.SELECT_EVENT);
		event.initXFormsEvent(XFormsEventFactory.SELECT_EVENT,true,true,this);
		caseElem.dispatchEvent(event);
	}
	
	public String getCaseId()
	{
		return this.getAttribute("case");
	}
	
}
