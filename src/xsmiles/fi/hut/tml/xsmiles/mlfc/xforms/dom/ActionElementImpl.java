/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.EventBroker;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.Action;
import fi.hut.tml.xsmiles.dom.EventHandlerService;


import java.util.Hashtable;
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;

import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.xml.sax.SAXException;    



    

/**
 * XForm/Action element implementation
 * @author Mikko Honkala
 */


public class ActionElementImpl extends XFormsElementImpl implements EventHandlerService, Action {

	public ActionElementImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}
	
	
	/** 
	 * Goes thru its children, and activates them one at the time
	 */
	public void activate(Event evt)
	{
            try
            {
                this.getHandler().enterAction(this);
		Node n = this.getFirstChild();
		while (n!=null)
		{
			if (n instanceof EventHandlerService) ((EventHandlerService)n).activate(evt);
			n=n.getNextSibling();
		}
            } catch (Throwable e)
            {
                Log.error(e);
            } finally
            {
                this.getHandler().exitAction(this);
            }
		
	}
	
}
