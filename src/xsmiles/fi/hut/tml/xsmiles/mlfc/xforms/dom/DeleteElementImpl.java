/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

import org.w3c.dom.*;
import org.w3c.dom.events.*;

/**
 * XForm/delete element implementation
 * @author Mikko Honkala
 */


public class DeleteElementImpl extends RepeatHandlerBaseImpl
{
    
    public DeleteElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    public void init()
    {
        try
        {
            this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        }
        super.init();
    }
    public void activate(Event evt)
    {
        super.activate(evt);
        NodeList nodes = null;
        nodes = this.getBoundNodeset();

        int at = this.getAt();
        Log.debug("delete At: "+at);
        if (at > nodes.getLength() || --at < 0) return;
        try
        {
            Node deletenode = nodes.item(at);
            Element parent = (Element)deletenode.getParentNode();
            InstanceElementImpl instance=(InstanceElementImpl)this.getModel().getInstance();
            //this.getModel().instanceStructureChanged();
            parent.removeChild(deletenode);
            // call repaint for the root node of this DOM
            //this.getModel().instanceStructureChanged();
            // TODO: context info
            instance.dispatch(XFormsEventFactory.createXFormsEvent(XFormsConstants.DELETE_EVENT));
            this.getModel().rebuild(true);
            //this.getModel().reevaluateUIBindings();
            //if (this.getModel().validation())
            //    instance.getInstanceDocument().validateDocument(false);
            
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    
    String getRepeatId()
    {
        return this.getAttribute(REPEAT_ID_ATTRIBUTE);
    }
    
}
