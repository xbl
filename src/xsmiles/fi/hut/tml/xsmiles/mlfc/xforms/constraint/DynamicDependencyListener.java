/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The listener for dynamic binding
 * @author Mikko Honkala
 */


public interface DynamicDependencyListener extends ExpressionContainer {
   
    /** @return true if single node binding expression */
    public boolean isSingleNodeBinding();
    
    /** @return the currently bound instance node in single node binding */
    public InstanceNode getRefNode();
    
    /** @return the currently bound instance nodes in multinode binding */
    public NodeList getBoundNodeset();
    
    /** the refresh processing notifies, that the binding might have changed, and it needs to be re-evaluated */
    public void bindingMaybeDirty();
    
    /** notifies the listener that the binding and the value changed */
    public void notifyBindingChanged(NodeList newBinding);

    /** notifies the listener that the value changed */
    //public void notifyValueChanged();
    
    /** get the XPath string to listen to */
    public XPathExpr getXPath();
    
    /** @return the instanceItemListener for the referred nodes, for value 
     changes without binding change */
    public InstanceItemListener getInstanceItemListener();
    
   
}
