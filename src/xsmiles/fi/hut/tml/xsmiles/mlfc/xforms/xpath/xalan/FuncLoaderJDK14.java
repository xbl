/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;

import fi.hut.tml.xsmiles.Log;

import org.apache.xpath.compiler.FunctionTable;
import org.apache.xpath.compiler.FuncLoader;
import org.apache.xpath.functions.Function;


public class FuncLoaderJDK14 extends FuncLoader 
{
    protected Class funcClass;
      /**
       * Constructor FuncHereLoader
       *
       */
      public FuncLoaderJDK14(Class c) {
         super(c.getName(), 0);
         funcClass=c;
      }

      /**
       * Method getFunction
       *
       *
       * @throws javax.xml.transform.TransformerException
       */
      public Function getFunction()
              throws javax.xml.transform.TransformerException {
          try
          {        
             return (Function)funcClass.newInstance();
          } catch (InstantiationException e)
          {
              Log.error(e);
          } catch (IllegalAccessException e)
          {
              Log.error(e);
          }
          return null;
      }

      /**
       * Method getName
       *
       *
       */
      public String getName() {
         return funcClass.getName();
      }
   }
