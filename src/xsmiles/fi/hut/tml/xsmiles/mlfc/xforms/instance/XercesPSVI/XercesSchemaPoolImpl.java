/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.content.ResourceFetcher;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsLinkException;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.SchemaPool;

import java.io.*;
import java.net.*;

import org.w3c.dom.Node;

import org.apache.xerces.parsers.XMLGrammarPreparser;
import org.apache.xerces.parsers.IntegratedParserConfiguration;
import org.apache.xerces.util.ObjectFactory;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.util.XMLGrammarPoolImpl;
import org.apache.xerces.impl.Constants;

import org.apache.xerces.xni.grammars.XMLGrammarDescription;
import org.apache.xerces.xni.grammars.Grammar;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.apache.xerces.xni.parser.XMLParserConfiguration;

import org.apache.xerces.impl.xs.XSDDescription;
import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.xni.parser.XMLErrorHandler;

/**
 * the class implementation for pooling or pre-reading schemas
 * @author Mikko Honkala
 * @author Ronald Tschal?r (Patches)
 */
public class XercesSchemaPoolImpl implements SchemaPool, XMLErrorHandler
{
    // property IDs:
    
    /** Property identifier: symbol table. */
    public static final String SYMBOL_TABLE =
    Constants.XERCES_PROPERTY_PREFIX + Constants.SYMBOL_TABLE_PROPERTY;
    
    /** Property identifier: grammar pool. */
    public static final String GRAMMAR_POOL =
    Constants.XERCES_PROPERTY_PREFIX + Constants.XMLGRAMMAR_POOL_PROPERTY;
    
    // feature ids
    
    /** Namespaces feature id (http://xml.org/sax/features/namespaces). */
    protected static final String NAMESPACES_FEATURE_ID = "http://xml.org/sax/features/namespaces";
    
    /** Validation feature id (http://xml.org/sax/features/validation). */
    protected static final String VALIDATION_FEATURE_ID = "http://xml.org/sax/features/validation";
    
    /** Schema validation feature id (http://apache.org/xml/features/validation/schema). */
    protected static final String SCHEMA_VALIDATION_FEATURE_ID = "http://apache.org/xml/features/validation/schema";
    
    /** Schema full checking feature id (http://apache.org/xml/features/validation/schema-full-checking). */
    protected static final String SCHEMA_FULL_CHECKING_FEATURE_ID = "http://apache.org/xml/features/validation/schema-full-checking";
    
    // a larg(ish) prime to use for a symbol table to be shared
    // among
    // potentially man parsers.  Start one as close to 2K (20
    // times larger than normal) and see what happens...
    public static final int BIG_PRIME = 2039;
    /** the internal xerces schema pool object */
    protected XMLGrammarPoolImpl grammarPool;
    
    protected XMLGrammarPreparser preparser;
    
    protected boolean schemaFullChecking = true;
    protected SymbolTable symbolTable;
    
    
    protected int errorCount = 0;
    public XercesSchemaPoolImpl()
    {
        initPool();
    }
    
    protected void initPool()
    {
        symbolTable = new SymbolTable(BIG_PRIME);
        preparser = new XMLGrammarPreparser(symbolTable);
        grammarPool = new XMLGrammarPoolImpl();
        preparser.registerPreparser(XMLGrammarDescription.XML_SCHEMA, null);
        preparser.setProperty(GRAMMAR_POOL, grammarPool);
        preparser.setFeature(NAMESPACES_FEATURE_ID, true);
        preparser.setFeature(VALIDATION_FEATURE_ID, true);
        // note we can set schema features just in case...
        preparser.setFeature(SCHEMA_VALIDATION_FEATURE_ID, true);
        preparser.setFeature(SCHEMA_FULL_CHECKING_FEATURE_ID, schemaFullChecking);
        preparser.setErrorHandler(this);
    }
    
    /** add schema from URL */
    public void addSchema(String schemaURL) throws XFormsLinkException
    {
        Log.debug("SchemaPool: Caching schema: "+schemaURL);
        try
        {
            XSmilesConnection conn = HTTP.get(new URL(schemaURL),null);
            InputStream is = conn.getInputStream();
            Grammar g = preparser.preparseGrammar(XMLGrammarDescription.XML_SCHEMA, streamToXIS(is, schemaURL));
        } catch (SchemaParseException e)
        {
            throw new XFormsLinkException(e.toString(),schemaURL);
        } catch (java.lang.Exception e)
        {
            throw new XFormsLinkException(e.toString(),schemaURL);
        }
        // we don't really care about g; grammarPool will take care of everything.
    }
    
    /** add schema from reader */
    public void addSchema(Reader schemaReader, String schemaURL) throws XFormsLinkException
    {
        Log.debug("SchemaPool: Caching schema: "+schemaURL);
        try
        {
            Grammar g = preparser.preparseGrammar(XMLGrammarDescription.XML_SCHEMA, readerToXIS(schemaReader, schemaURL));
        } catch (SchemaParseException e)
        {
            throw new XFormsLinkException(e.toString(),schemaURL);
        } catch (java.io.IOException e)
        {
            throw new XFormsLinkException(e.toString(),schemaURL);
        }
        // we don't really care about g; grammarPool will take care of everything.
    }
    
    /** reset pool */
    public void reset()
    {
        Log.error(this+"reset() not implemented");
    }
    
    private static XMLInputSource stringToXIS(String uri)
    {
        return new XMLInputSource(null, uri, null);
    }
    private static XMLInputSource readerToXIS(Reader r, String uri)
    {
        return new XMLInputSource(null, uri, null, r, null);
    }
    private static XMLInputSource streamToXIS(InputStream r, String uri)
    {
        return new XMLInputSource(null, uri, null, r, null);
    }
    public XMLGrammarPoolImpl getXMLGrammarPoolImpl()
    {
        return this.grammarPool;
    }
    public SymbolTable getSymbolTable()
    {
        return this.symbolTable;
    }
    
    protected XSDDescription fXSDDescription=new XSDDescription();
    public SchemaGrammar getGrammar(String namespace)
    {
        fXSDDescription.reset();
        fXSDDescription.setTargetNamespace(namespace);
        if (grammarPool != null)
        {
            SchemaGrammar grammar = (SchemaGrammar)grammarPool.retrieveGrammar(fXSDDescription);
            if (grammar==null)
            {
                // TODO: error handling
                Log.error(this.toString()+" could not find grammar for "+namespace);
            }
            return grammar;
        }
        return null;
    }
    
    public void error(String str, String str1, org.apache.xerces.xni.parser.XMLParseException xMLParseException) throws org.apache.xerces.xni.XNIException
    {
        Log.error("Schema parsing: "+str);
        throw new SchemaParseException(str+" : "+str1+" : "+xMLParseException);
    }
    
    public void fatalError(String str, String str1, org.apache.xerces.xni.parser.XMLParseException xMLParseException) throws org.apache.xerces.xni.XNIException
    {
        Log.error("Schema parsing fatal: "+str);
        throw new SchemaParseException(str+" : "+str1+" : "+xMLParseException);
    }
    
    public void warning(String str, String str1, org.apache.xerces.xni.parser.XMLParseException xMLParseException) throws org.apache.xerces.xni.XNIException
    {
        Log.error("Schema parsing warning: "+str);
        throw new SchemaParseException(str+" : "+str1+" : "+xMLParseException);
    }
    public class SchemaParseException  extends java.lang.RuntimeException
    {
        
        /** constructs an exception without a message */
        public SchemaParseException()
        {
            super();
        }
        
        /** constructs an exception without a message */
        public SchemaParseException(String message)
        {
            super(message);
        }
    }
}

