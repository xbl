/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;
/**
 * The listener for property changes from the parent node
 */
public interface PropertyInheriter {

    public final static short PROPERTY_BASE=10;
    public final static short RELEVANT_PROPERTY=PROPERTY_BASE;
    public final static short READONLY_PROPERTY=PROPERTY_BASE+1;
    /** the parent's readonly status has changed
     * Inheritance Rules: If any ancestor node evaluates to true, 
     * this value is treated as true. Otherwise, the local value is used.
     * Note:
     * This is the equivalent of taking the logical OR of the evaluated readonly property on the local and every ancestor node.
     *
     * the parent's relevant status has changed
     * Inheritance Rules:  If any ancestor node evaluates to XPath false, this value is treated as false. 
     * Otherwise, the local value is used.
     * Note:
     * This is the equivalent of taking the logical AND of the evaluated relevant property on the local and every ancestor node.
     */
	public void inheritProperty(short property,boolean value);
	public void inheritToChildren(short property, boolean value);
	
}

