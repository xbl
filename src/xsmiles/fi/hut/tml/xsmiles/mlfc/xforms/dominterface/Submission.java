/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;


/**
 * @author Mikko Honkala
 */


public interface Submission extends org.w3c.dom.Element
{
    public String toString();
}

