/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 29, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import org.w3c.dom.Element;


/**
 * @author honkkis
 *
 */
public class IntegerOutputWidget extends IntegerSpeechWidget
{
    public IntegerOutputWidget(Element e, FocusHandler handler)
    {
        super(e, handler);
    }
    
    /** static prompt for this kind of control, for instance, "select a number"*/
    public String getPrompt()
    {
        return "";
    }

}
