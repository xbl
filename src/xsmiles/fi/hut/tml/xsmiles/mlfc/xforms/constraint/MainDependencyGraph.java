/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.BindElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ModelElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsBindingException;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;

/**
 * The dependency graph for the calculation algorithm
 * @author Mikko Honkala
 */


public class MainDependencyGraph extends DependencyGraph
{
    
    protected ModelElementImpl xform;
    public boolean calculated=false;
    
    
    public MainDependencyGraph()
    {
        super();
    }
    
    /**
     * Adds a single bind's ref nodes to the Main Graph
     * called by MainDependencyGraph.constructMainGraph()
     */
    public void addBindToGraph(BindElement bind, ModelContext modelContext) throws XFormsBindingException, Exception
    {
        if (bind!=null)
        {
            NodeList ns = bind.getBoundNodeset();
            // for each node n in ns
            if (ns!=null)
                for (int i=0;i<ns.getLength();i++)
                {
                    // n is a node in ns
                    InstanceNode n = (InstanceNode)ns.item(i);
                    ExpressionContainer container = (ExpressionContainer)bind;
                    if (bind.getCalculateString().length()>0) this.addRefNodeToGraph(n,container,
                    this.createExpression(bind.getCalculateString(),modelContext),Vertex.CALCULATE_VERTEX,modelContext);
                    if (bind.getRelevantString().length()>0) this.addRefNodeToGraph(n,container,
                            this.createExpression(bind.getRelevantString(),modelContext),Vertex.RELEVANT_VERTEX,modelContext);
                    if (bind.getReadonlyString().length()>0) this.addRefNodeToGraph(n,container,
                            this.createExpression(bind.getReadonlyString(),modelContext),Vertex.READONLY_VERTEX,modelContext);
                    if (bind.getRequiredString().length()>0) this.addRefNodeToGraph(n,container,
                            this.createExpression(bind.getRequiredString(),modelContext),Vertex.REQUIRED_VERTEX,modelContext);
                    if (bind.getIsvalidString().length()>0) this.addRefNodeToGraph(n,container,
                            this.createExpression(bind.getIsvalidString(),modelContext),Vertex.ISVALID_VERTEX,modelContext);
                    
                    // type attribute
                    this.addTypeToInstanceNode(n,bind);
                }
        }
    }
    
    protected XPathExpr createExpression(String s,ModelContext m)
    {
        return m.getXPathEngine().createXPathExpression(s);
    }
    
    /** this method checks for xforms: type attributes and
     * adds type information to the relevant node.
     * Note that type is not calculated property, so it is not
     * added as an vertex!
     */
    protected void addTypeToInstanceNode(InstanceNode n, BindElement bind)
    {
        String type = bind.getTypeString();
        if (type==null||type.length()<1) return;
        // parse localpart and prefix of xforms:type
        int colonPos = type.indexOf(':');
        if (colonPos>1 && colonPos<type.length())
        {
            String prefix = type.substring(0,colonPos);
            String localpart = type.substring(colonPos+1);
            Element elem=(Element)bind;//(this.instanceNode instanceof Element?(Element)this.instanceNode:(Element)this.instanceNode.getParentNode());
            String ns = XFormsUtil.getNamespaceURI(prefix,elem);
            InstanceItem i = n.getInstanceItem();
            i.setXFormsType(localpart,ns);
        }
    }
    protected LookupResult result = new LookupResult();
    /**
     * Adds a single bind's ref node to the Main Graph
     * called by MainDependencyGraph.addBindToGraph()
     */
    public void addRefNodeToGraph(InstanceNode n, ExpressionContainer container, XPathExpr xpath, short type, ModelContext modelContext) throws Exception, XFormsBindingException
    {
        // Add node n of NS into the Main Diagram as a vertice (*)
        //Vertex vertexn = this.getVertex(n,type);
        Vertex vertexn = this.addVertex(n,container,xpath,type);
        boolean hadVertex = vertexn.wasAlreadyInGraph;
        // reset the flag
        vertexn.wasAlreadyInGraph=false;
        
/*		else
                {
                        hasVertex=true;
                }
 */
        // Analyze the Xpath Expression 'calculate'. Read nodeset RefNS
        // (the nodes this XPAth references)
        //XPathExpression xpath=vertexn.getXPath();
        if (xpath==null)
        {
            // this bind did not have an xpath, remove the vertex
            if (hadVertex==false) this.removeVertex(vertexn);
            return;
        }
        modelContext.getXPathEngine().evalWithTrace(vertexn.getContextNode(),xpath,vertexn.getNamespaceContextNode(),null,result);
        Vector refns = result.referredNodes;
        // for each node n2 in refNS
        if (refns==null) return;
        if (refns.size()==0)
        {
            // this is a calculated value, that is not depending on anything, let's calculate it now
            vertexn.compute();
        }
        Enumeration enum2 = refns.elements();
        while (enum2.hasMoreElements())
        {
            InstanceNode n2 = (InstanceNode)enum2.nextElement();
            // add node n2 of RefNS into the main diagram as a vertice (*)
            Vertex vertexn2 = this.addVertex(n2,null, null,Vertex.CALCULATE_VERTEX);
            // add an edge from n2 to n (*)
            this.addEdge(vertexn2,vertexn);
            
            // (*) =  if not already added
        }
    }
    /**
     * Extends DependencyGraph.recalculate(). Restores the vertices vector after recalc
     */
    public void recalculate()
    {
        //this.printGraph();
        Vector tempvertices=(Vector)this.vertices.clone();
        super.recalculate();
        this.vertices=tempvertices;
        calculated=true;
    }
    
    
}
