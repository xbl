/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import java.net.URL;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.VisualElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.adaptive.DisplayValueProvider;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.DynamicDependencyHandler.Dependency;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * The XForms/Output element
 * This extends the adaptive control to provide support for the
 * <output value=""> syntax
 * @author Mikko Honkala
 */

public class OutputElementImpl extends AdaptiveControl implements DisplayValueProvider
{
    protected boolean usesValueAttribute = false;
    // for XML+CSS layout, the text is provided by putting a text node into the DOM
    protected Node nodeForValue = null;
    
    /**
     * Constructs a new 'output' control
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    public OutputElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        // output can have the special attribute value, so regular binding attributes are not required
        this.bindingAttributesRequired=false;
    }
    
    public void init()
    {
		Attr bindAttr = this.getAttributeNode("bind");
		Attr refAttr = this.getAttributeNode("ref");
		Attr valueAttr = this.getAttributeNode("value");
		if (bindAttr==null&&refAttr==null&&valueAttr!=null) this.usesValueAttribute=true;
        super.init();
    }
    public int getBindingType()
    {
		if (this.usesValueAttribute)
			return Dependency.VALUE_BINDING;
		return super.getBindingType();
    }
    
    public String getMediaType()
    {
        Attr mediaAttr = this.getAttributeNode(XFormsConstants.MEDIATYPE_ATTRIBUTE);
        if (mediaAttr==null) return null;
        else return mediaAttr.getNodeValue();
        
    }
    protected void checkBindingState()
    {
        if (this.getBindingState()==DynBoundElementImpl.BINDING_ATTRIBUTES_NOT_FOUND)
        {
            if (!this.usesValueAttribute)
			{
                this.handleXFormsException(new XFormsBindingException(this,"no binding attributes",this.getAttribute(MODEL_ID_ATTRIBUTE)));
            }
        }
        else super.checkBindingState();
    }
    protected void checkBindingVisibility()
    {
        // if the binding was to a non-existent node, we should make the
        // form control invisible unless it is a button or submit
        
        // why was this call removed? I added it back 3-aug-04 -mh
        //if (!this.usesValueAttribute) super.checkBindingVisibility();
    }
    
    protected int getDatatypeId()
    {
        if (this.usesValueAttribute==false) return super.getDatatypeId();
        else return 6666;
        
    }
    
    public XComponent createComponent()
    {
        if (!this.usesValueAttribute)
        {
            XComponent ret =  super.createComponent();
            this.createMediaObject();
            return ret;
        }
        /*
        try
        {
            control = this.getHandler().getAdaptiveControlFactory().createControl(
            this.getHandler(),this.getLocalName(),-1,this);
            XComponent comp = control.getComponent();
            //fData = XFormsConfiguration.getInstance().getDataFactory().createData(-1);
            //this.control.setData(fData);
            this.control.updateDisplay();
            this.createMediaObject();
            return comp;
        } catch (XFormsDatatypeException e)
        {
            this.handleXFormsException(e);
        }*/
        return null;
    }
    
    private boolean creatingMediaObject=false;
    public void createMediaObject()
    {
        try
        {
            if (creatingMediaObject) return;
            creatingMediaObject=true;
            String media = this.getMediaType();
            if (media!=null)
            {
                if (this.getBindingState()==DynBoundElementImpl.BINDING_OK)
                {
                    String rNodeValue = (String)this.getRefNodeValue();
                    Element objectE = this.getOwnerDocument().createElementNS(XFormsConstants.XHTML_NS,"object");
                    objectE.setAttribute("data",rNodeValue);
                    this.appendChild(objectE);
                    ((XSmilesElementImpl)objectE).init();
                }
            }
        } catch (Throwable t)
        {
            Log.error(t);
        }
        finally
        {
            creatingMediaObject=false;
        }
    }
    
    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        if (pseudoclass.equals(MEDIARENDERING_PSEUDOCLASS))
        {
            if (this.getMediaType()!=null) return true;
            else return false;
        }
        else return super.isPseudoClass(pseudoclass);
    }

    
    /** method for initing binding */
    void createBinding() throws XFormsBindingException
    {
    	try
    	{
	        // do not re-create binding, unless destroyBinding has been called!
	        if (this.binding_state!=this.UNINITIALIZED) return;
	        
            int bindingType=this.getBindingType();
	        if (bindingType==Dependency.VALUE_BINDING)
	        {
	
	            dependency = this.getModel().getDynamicDependencyHandler().createDependency(bindingType);
	            dependency.init(this);
	
	        }
	        else
	        {
	        	super.createBinding();
	        }
    	} catch (Exception e)
    	{
    		Log.error(e);
    	}
    }

    
    public String getOutputValue()
    {
        /*
        if (this.getAttributeNode(VALUE_ATTRIBUTE)!=null)
        {
            try
            {
                return this.control.getData().toDisplayValue();
            } catch (NullPointerException e)
            {
                Log.error("getOutputValue() "+e.getMessage());
                return "";
            }
        }
        else return super.getOutputValue();
        */
        return this.getDisplayValue();
    }

    
        /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        super.notifyBindingChanged(newBinding);
        Log.debug("Binding changed for output");
        this.refreshDisplayDOM();
    }
    public void changeComponentValue(String newValue)
    {
        super.changeComponentValue(newValue);
        if (this.getElementStatus()!=XSmilesElementImpl.INITIALIZING)
        {
            this.refreshDisplayDOM();
        }
        
    }
    public static final String XHTML_NS = "http://www.w3.org/1999/xhtml";

    public void refreshDisplayDOM()
    {
        String media = this.getMediaType();
        if (media==null)
        {
            // this is a textual output
            String text=this.getOutputValue();
            if (this.valuePseudoElement instanceof OutputValuePseudoElement)
            {
                Text textN = (Text)this.valuePseudoElement.getFirstChild();
                if (textN==null)
                {
                    this.valuePseudoElement.appendChild(this.getOwnerDocument().createTextNode(text));
                }
                else
                {
                    textN.setNodeValue(text);
                }
            }
            /*
            if (this.nodeForValue!=null&&this.nodeForValue instanceof Element)
            {
                Element textE=(Element)this.nodeForValue;
                textE.getFirstChild().setNodeValue(text);
            }
            else
            {
               this.nodeForValue=this.getOwnerDocument().createElementNS(XHTML_NS,"span");
               Text textN = this.getOwnerDocument().createTextNode(text);
               this.nodeForValue.appendChild(textN);
               this.getPseudoElements().addElement(nodeForValue);
               ((XSmilesElementImpl)nodeForValue).init();
            }*/
        }
        else
        {
            // this is media rendering output
            try
            {
                if (creatingMediaObject) return;
                creatingMediaObject=true;
                if (this.getBindingState()==DynBoundElementImpl.BINDING_OK)
                {
                    String rNodeValue = (String)this.getRefNodeValue();
                    if (this.valuePseudoElement instanceof OutputValuePseudoElement)
                    {
                        Element objectE = this.getOwnerDocument().createElementNS(XFormsConstants.XHTML_NS,"object");
                        objectE.setAttribute("data",rNodeValue);
                        if (valuePseudoElement.getFirstChild() instanceof VisualElement)
                        {
                            valuePseudoElement.removeChild(valuePseudoElement.getFirstChild());
                        }
                        this.valuePseudoElement.appendChild(objectE);
                        ((XSmilesElementImpl)objectE).init();
                    }
                }
            }
            catch (Throwable t)
            {
                Log.error(t);
            }
            finally
            {
                creatingMediaObject=false;
            }
        }
    }
	
    public void notifyParentBindingChanged(DynBoundElementImpl ancestor)
	{
		super.notifyParentBindingChanged(ancestor);
	}
	public XPathExpr getValueExpr()
	{
		Attr valAttr = this.getAttributeNode(VALUE_ATTRIBUTE);
		if (valAttr!=null)
		{
			XPathExpr expr = this.getModel().getXPathEngine().createXPathExpression(valAttr.getNodeValue());
			return expr;
		}
		return null;
	}
	
    public XPathExpr getRefXPathExpr()
    {
    	if (this.getBindingType()==Dependency.VALUE_BINDING)
    	{
    		return this.getValueExpr();
    	}
    	else
    		return super.getRefXPathExpr();
    }

	public String executeValAttr() throws Exception
	{
		XPathExpr expr = this.getValueExpr();
		if (expr!=null)
		{
            try
            {
                return this.getModel().getXPathEngine().evalToString(this.getContextNode(),expr,this,null);
            } catch (Throwable e)
            {
                Log.error(e);
                this.dispatchEvent(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.COMPUTE_EXCEPTION));
            }
		}
		return null;
	}
    
    /** 
     * Extends XFormsControls method and creates a general element instead of visual component element
     */
    protected void createValuePseudoElement()
    {
        // TODO: NON-TEXT PSEUDO ELEMENTS, SUCH AS BOOLEAN??
        valuePseudoElement= new OutputValuePseudoElement(this);
        this.getPseudoElements().clear();
        this.getPseudoElements().addElement(valuePseudoElement);
    }

	public String getDisplayValue() {
		// TODO Auto-generated method stub
		if (this.usesValueAttribute)
		{
			try
			{
				return this.executeValAttr();
			} catch (Exception e)
			{
				Log.error(e);
				return "error";
			}
		}
		else
		{
			InstanceNode refN = this.getRefNode();
			if (refN!=null)
			{
				InstanceItem item = refN.getInstanceItem();
				if (item!=null)
				{
					return (String)(item.getData().toDisplayValue());
				}
			}
		}
		return null;
	}
	
    
    

    public class OutputValuePseudoElement extends VisualElementImpl
    implements PseudoElement
    {
        XFormsControl owner;
        public OutputValuePseudoElement(XFormsControl control)
        {
            super((org.apache.xerces.dom.DocumentImpl)getParentOwnerDocument(),"xforms","value");
            owner=control;
        }
        
        public String getPseudoElementName() // return e.g. "value" for ::value
        {
            return "value";
        }
        
        public Node getParentNode()
        {
            if (owner==null) return null;
            return owner.getParentNodeForPseudoElement();
        }

        /* (non-Javadoc)
         * @see fi.hut.tml.xsmiles.dom.PseudoElement#setParentNode(org.w3c.dom.Node)
         */
        public void setParentNode(Node p)
        {
            // TODO Auto-generated method stub
            if (p!=null) owner=null;
        }

        /* (non-Javadoc)
         * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
         */
        public XSmilesConnection get(URL dest, short type)
        {
            // TODO Auto-generated method stub
            return null;
        }
        
        
        /*public CSSStyleDeclaration getStyle()
        {
            return getOwnerStyle();
        }*/
        
    }
    
    
}
