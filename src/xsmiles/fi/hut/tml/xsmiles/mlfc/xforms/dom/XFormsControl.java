/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.FlowLayoutElement;
import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.LabeledElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * The base class for the XForm controls, such as buttons and textboxes.
 * It uses the abstract component model of X-SMiles in the package
 * fi.hut.tml.xsmiles.gui.components
 *
 * @author Mikko Honkala
 */

public abstract class XFormsControl
extends DynBoundElementImpl
implements InstanceItemListener,  MouseListener, FocusListener,
ActionListener, // for help events
FlowLayoutElement, // for the old swing xhtml renderer
CompoundService, // horrible kludge for SMIL
PseudoElementContainerService,
LabeledElement,
org.w3c.dom.events.EventListener
{
    
    
    /** is this control incremental */
    protected boolean incremental=false;
    
    /** the current value of input mode */
    protected String inputMode;
    
    protected boolean isInputComponent;
    
    protected boolean componentInited=false;
    
    /** this flag specifies, whether the text in the control has
     * ever changed. It is used to show the validation status and send
     * the validation events.
     */
    //protected boolean valueChangedEver = false;
    
    /** this property is the VisualComponentService setVisible property
     * XForms has its own relevant property, which is a different visibility property */
    //protected boolean visible=true;
    
    /** this property is the current validity status
     * XForms model has its own validity status, which is different in the beginning */
    //protected boolean valid=true;
    
    
    /** this flag ensures, that updates are not done many times, and that
     * we dont get into a loop */
    protected boolean insideUpdateEvent=false;
    
    /** the abstract component for this control */
    protected XComponent component;
    
    /** compound that holds the label and the control */
    //protected XLabelCompound compound;
    
    /** the label (caption) component */
    protected XCaption captionComp;
    
    /** Am I a xforms:output element */
    protected boolean isOutput=false;
    
    
    /** am I in-range or out-of-range, default is in-range. used by range and selects */
    protected boolean outOfRange = false;
    
    /** this is a kludge that is used to create a compound component if
     * used with a host document that does not support CSS, such as SMIL and SVG */
    protected CompoundServiceImpl compoundService;
    
    /**
     * Constructs a new XFormsControl (AnyControl in XForms spec).
     *
     * @param my_handler 	The handler for this control
     * @param elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    
    public XFormsControl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        if (this.getLocalName().equals(OUTPUT_ELEMENT)) isOutput=true;
        createValuePseudoElement();
        
    }
	/** since the pseudoelement removals are not catched by the xsmilesvisualelement, this
	 * method can be used to notify a remove
	 * @param elem
	 */
	public void notifyPseudoRemoved(Element elem)
	{
		// NOP
	}
    public void init()
    {
        //Log.debug(this.toString()+".init()");
        if (this.elementStatus==INITIALIZED) return;
        
        init=true;
        
        // try to find my context node from ancestor
        // if repeat has already set my contextNode, don't fiddle with it!
        try
        {
            this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        }
        super.init();
        this.checkBindingState();
        ModelElementImpl model = this.getModel();
        if (model==null)
        {
            Log.error(this+" cannot find corresponding model.");
            //this.setCurrentState(INITIALIZED);
            return;
        }
        //this.componentInit(); let's do this on demand
        init=false;
    }
    
    /** this is only called on-demand, when getComponent is called for the valuepseudoelement */
    protected void componentInit()
    {
        if (this.getModel()==null)
        {
            Log.error(this+" cannot find corresponding model.");
            //this.setCurrentState(INITIALIZED);
            //return;
        }
        this.component = this.createComponent();
        this.componentInited=true;
        this.addCaption();
        
        // NO LONGER IN SPEC 
        //dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.FORM_CONTROL_INITIALIZE_EVENT));
        
        this.formatComponent(); // these have to be formatted befor compound is created

        // The <hint> element
        HintElementImpl h = (HintElementImpl)this.getElementsByTagNameNS(XFORMS_NS,HINT_ELEMENT).item(0);
        if (h!=null)
            setHint(h);
        this.registerControl();
        this.registerListener();
        if (inputMode!=null) this.setInputMode(inputMode);
        this.checkInitialMIPState(); // for readonly, etc.
        //this.checkBindingVisibility();
        
    }
    
    public String getAppearance()
    {
        return (String)this.getAttribute(APPEARANCE_ATTRIBUTE);

    }

    
    	/**
	 * Return the visual component for this extension element
         * This would return e.g. an XForms input element which
         * contains an container that has both the label and the
         * ::value (the widget). Note: this is a kludge.
	 */
	public VisualComponentService getVisualComponent()
        {
            if (componentInited==false) this.componentInit();
            if (compoundService == null)
            {    
                compoundService = new CompoundServiceImpl(this);
                compoundService.getVisualComponent();
                //this.checkBindingVisibility();
            }
            return compoundService.getVisualComponent();
        }
    
    /** Returns the string value of the caption-side CSS property,
     * used as a hack when the host language does not support CSS flow layout
     * to position the label */
    protected String getCaptionSide()
    {
        String cs = "top";
        CaptionElementImpl capt = this.getCaption();
        if (capt==null) return cs;
        CSSStyleDeclaration style = capt.getStyle();
        if (style==null) return cs;
        CSSValue val = style.getPropertyCSSValue(CSSConstants.CSS_CAPTION_SIDE_PROPERTY);
        if (val instanceof CSSPrimitiveValue)
        {
            CSSPrimitiveValue pVal = (CSSPrimitiveValue) val;
            return pVal.getStringValue();
        }
        return cs;
    }
    
    /** this is called in init, so that button can override the default
     * behaviour
     */
    protected void checkBindingState()
    {
        if (this.getBindingState()==DynBoundElementImpl.BINDING_ATTRIBUTES_NOT_FOUND)
        {
            this.handleXFormsException(new XFormsBindingException(this,"no binding attributes",this.getAttribute(MODEL_ID_ATTRIBUTE)));
        }
        
        //TODO required etc.

        
    }
    
    protected void checkInitialMIPState()
    {
        if (this.getBindingState()==DynBoundElementImpl.BINDING_OK)
        {
            this.setReadonly(retrieveInstanceItem((InstanceNode)this.getRefNode()).getReadonly());
        }
        
    }
    /*
    protected void checkBindingVisibility()
    {
        // if the binding was to a non-existent node, we should make the
        // form control invisible unless it is a button or submit
        if (this.getBindingState()==DynBoundElementImpl.BOUND_NODE_NOT_FOUND)
            valuePseudoElement.setVisible(false);
        else if (this.getBindingState()==DynBoundElementImpl.BINDING_OK)
        {
            if (valuePseudoElement.getVisible()==false)  valuePseudoElement.setVisible(true);

            this.checkVisibility(retrieveInstanceItem((InstanceNode)this.getRefNode()));
            this.setReadonly(retrieveInstanceItem((InstanceNode)this.getRefNode()).getReadonly());
        }
    }
    */
    
    public String getOutputValue()
    {
        // TODO: datatype conversion!
        return (String)this.getRefNodeValue();
    }
    public void destroy()
    {
        // remove from XForm
        XComponent comp = this.component;
        if (comp!=null)comp.removeMouseListener((MouseListener)this);
        if (comp!=null)comp.removeFocusListener(this);
        if (comp!=null)comp.removeHelpListener(this);
        // clear fields
        this.component=null;
        //this.getRefNode()=null;
        super.destroy();
    }
    
    protected CaptionElementImpl checkCaption()
    {
        CaptionElementImpl capt = this.getCaption();
        if (capt==null)
        {
            this.handleXFormsException(new XFormsException("No label child for "+this));
            return null;
        }
        else return capt;
    }
    public CaptionElementImpl getCaption()
    {
        return getCaption(this);
    }
    public static CaptionElementImpl getCaption(Element e)
    {
        Node child =e.getFirstChild();
        while (child!=null)
        {
            if (child instanceof CaptionElementImpl)
            {
                return (CaptionElementImpl)child;
            }
            child=child.getNextSibling();
        }
        return null;
    }
    
    
    
    //protected final static Dimension minimumCaptionSize = new java.awt.Dimension(50,10);
    public  void addCaption()
    {
        CaptionElementImpl caption = getCaption();
        if (caption!=null && this.component!=null)
        {
            XCaption capt = this.getHandler().getComponentFactory().getXCaption(caption.getCaptionText());
            this.captionComp=capt;
            //capt.setMinimumSize(minimumCaptionSize);
        }
    }
    
    /**
     * Creates the visible containing component of this control
     */
    public abstract XComponent createComponent();
    
    void registerControl()
    {
    }
    /**
     * Formats the content according to the CSS style attribute,
     * this can be overridden by the extending classes
     */
    protected void formatComponent()
    {
        CSSStyleDeclaration style=this.valuePseudoElement.getStyle();
        if (this.valuePseudoElement instanceof VisualComponentService)
        {
            ((VisualComponentService)this.valuePseudoElement).getComponent(); // this will force the creation of component
            // format the content component
            if (style==null||this.component==null) 
            {
                Log.debug("formatComponent: style or component was null: style:"+style+" component:"+component);
                //Log.error("formatComponent: style or component was null: style:"+style+" component:"+component);
                return;
            }
            if (this.component.getStyle()!=style)
            {
                // formatting of components should be moved to the renderer!
    	    //Log.debug(this+"formatComponent(): "+style.hashCode()+style);
                //Log.debug(this+"formatComponent(): "+style.hashCode()+style);
                this.component.setStyle(style);
            }
        }
    }
    protected void formatCaption()
    {
        // format the content component
        CaptionElementImpl caption = this.getCaption();
        if (caption==null) return;
        CSSStyleDeclaration style=caption.getStyle();
        if (style==null) return;
        // TODO: change when ComponentWithCaption is removed
        //this.component.setCaptionStyle(style);
        if (this.captionComp!=null) this.captionComp.setStyle(style);
        
    }
    
    abstract protected void changeComponentValue(String newValue);
    
    /**
     * marks the component that is has failed the schema test
     */
    /*
    public void setValid(boolean valid)
    {
        XComponent comp=this.component;
        if (!valid)
        {
            // dispatch a notification event
            //if (!init) dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.INVALID_EVENT));
            // re-run the CSS style engine, and restyle the component
            this.valid=false;
        }
        else
        {
            // dispatch a notification event
            //if (!init) dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.VALID_EVENT));
            this.valid=true;
        }
        this.styleChanged();
    }*/
    
    public void styleChanged()
    {
        super.styleChanged();
        this.formatCaption();
        this.formatComponent();
    }
    
    
 
    /**
     * Sets the components Tooltip (Hint) text
     */
    public void setHint(HintElementImpl h)
    {
        this.component.setHintText(h.getHintText());
    }
    
    protected String getCaptionText()
    {
        if (this.getCaption()==null) return "";
        else return this.getCaption().getCaptionText();
    }
    
    public String getLabelAsText()
    {
        return this.getCaptionText();
    }
    
        /*
        INSTANCE ITEM LISTENER'S FUNCTIONS
         */
    public void setReadonly(boolean ro)
    {
        if (!init)
        {
            if (ro)
            {
                XFormsEventImpl ev = (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.READONLY_EVENT);
                dispatch(ev);
            }
            else
            {
                XFormsEventImpl ev = (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.READWRITE_EVENT);
                dispatch(ev);
            }
        }
        if (this.component!=null) this.component.setEnabled(!ro);
        if (ro==true)
        {
            Log.debug("Control set readonly: "+this+this.getCaptionText());
        }
        //if (!init) this.styleChanged();
        //this.contentComp.setEnabled(!ro);
    }
    public void setRequired(boolean ro)
    {
        if (ro && !init)
        {
            XFormsEventImpl ev = (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.REQUIRED_EVENT);
            dispatch(ev);
        }
        else
        {
            XFormsEventImpl ev = (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.OPTIONAL_EVENT);
            dispatch(ev);
        }
        this.checkValidity(retrieveInstanceItem(this.getRefNode())); // TODO: depends whether required depends on validity 
        //this.contentComp.setEnabled(!ro);
    }
    
    public void setOutOfRange(boolean range)
    {
        if (range!=this.outOfRange)
        {
            this.outOfRange=range;
            //this.styleChanged();
        }
    }
    /**
     * An instance item instructs the control to check its visibility, when the status changes
     */
    public void checkVisibility(InstanceItem item)
    {
        if (this.component==null) return;
        // If refNode is null (in button for example), set visibility based on 'visible'
        if (getRefNode()==null)
        {
            //this.compound.setVisible(this.visible);
            //this.styleChanged();
            return;
        }
        //		Log.debug("CheckVisibility for "+this+" : "+component+" relevant:"+item.getRelevant()+" visible:"+this.visible);
        if (item.getRelevant()==false)//||this.visible==false)
        {
            //this.compound.setVisible(false);
            //this.styleChanged();
            // DISPATCH ALERT_EVENT, it is not cancelable
            if (!init) dispatch(
            (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.DISABLED_EVENT)
            );
            
        }
        else
        {
            //this.styleChanged();
            //this.compound.setVisible(true);
            if (!init) dispatch(
            (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.ENABLED_EVENT)
            );
        }
        // for e.g. SMIL that uses the compoundservice
        if (compoundService != null)
        {    
            compoundService.setRelevant(item.getRelevant());
        }
    }
    /**
     * An instance item instructs the control to check its validity, when the status changes
     */
    public void checkValidity(InstanceItem item)
    {
        // return if the user has not changed this control
        if (this.component==null||getRefNode()==null) return;
        if (item.getXFormsValid()&&item.getSchemaValid())
        {
            if (!init) dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.VALID_EVENT));

            //this.setValid(true);
        }
        else
        {
            if (!init) dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.INVALID_EVENT));
            // DISPATCH ALERT_EVENT, it is not cancelable
            XFormsEventImpl ev = (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.ALERT_EVENT);
            if (!init) dispatch(ev);
            //this.setValid(false);
        }
        //this.styleChanged();
    }
	
	
    
    /**
     * The value of this instanceItem has changed
     */
    public void valueChanged(String newValue)
    {
       // Log.debug(this+"valueChanged("+newValue);
        dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.VALUE_CHANGED_EVENT));
        this.changeComponentValue(newValue);
        //if (this.valueChangedEver==false)
        {
            //this.valueChangedEver=true;
            //this.checkValidity(retrieveInstanceItem(this.getRefNode()));
        }
        // the special case where output is child of label
        if (this.getLocalName().equals(OUTPUT_ELEMENT) && this.getParentNode() instanceof CaptionElementImpl)
        {
            boolean result = this.dispatch(XFormsEventFactory.createEvent(XFormsEventFactory.CAPTION_CHANGED));
        }
    }
	
	/** the modelelementimpl notifies this after calling setReadonly et al. */
	public void checkStyling()
	{
		if (!init) super.checkStyling();
	}
    
    protected synchronized void setRefNodeValue(Object value, boolean valueChanging)
    {
        super.setRefNodeValue(value,valueChanging);
        //if (this.valueChangedEver==false)
        {
            //this.valueChangedEver=true;
            //this.checkValidity(retrieveInstanceItem(this.getRefNode()));
        }
    }
    
    /**
     * Mouse events.
     */
    public void mouseClicked(java.awt.event.MouseEvent evt)
    {
        dispatch("click");
    }
    
    public void mouseEntered(java.awt.event.MouseEvent evt)
    {
        dispatch("mouseover");
    }
    
    public void mouseExited(java.awt.event.MouseEvent evt)
    {
        dispatch("mouseout");
    }
    
    public void mousePressed(java.awt.event.MouseEvent evt)
    {
        dispatch("mousedown");
    }
    
    public void mouseReleased(java.awt.event.MouseEvent evt)
    {
        dispatch("mouseup");
    }
    
    public void focusGained(FocusEvent ae)
    {
        org.w3c.dom.events.Event ev = XFormsEventFactory.createXFormsEvent(XFormsEventFactory.FOCUSIN_NOTIFICATION_EVENT);
        dispatch(ev);
    }
    public void focusLost(FocusEvent ae)
    {
        org.w3c.dom.events.Event ev = XFormsEventFactory.createXFormsEvent(XFormsEventFactory.FOCUSOUT_NOTIFICATION_EVENT);
        dispatch(ev);
    }
    
    
    protected void registerListener()
    {
        //super.registerListener(); // will start listening to instance node / binding changes
        XComponent comp = this.component;
        if (comp!=null)
        {
	        comp.addMouseListener(this);
	        comp.addFocusListener(this);
	        comp.addHelpListener(this);
	        this.addEventListener(XFormsEventFactory.CAPTION_CHANGED,this,false);
        }
        return;
    }
    
    public boolean getIncremental()
    {
        String interactive = this.getAttribute(INCREMENTAL_ATTRIBUTE);
        if (interactive!=null&&(interactive.equals("true")||interactive.equals("1")))
        {
            return true;
        }
        else return false;
    }
    
    /**
     * this method is overridden so that default actions for certain events can be processed
     */
    public boolean dispatchEvent(org.w3c.dom.events.Event event)
    {
        // simply forward to super class
        boolean ret = super.dispatchEvent(event);
        if (event instanceof XFormsEventImpl)
        {
            XFormsEventImpl xe = (XFormsEventImpl)event;
            if (!xe.preventDefault&&!xe.stopPropagation)
            {
                if (xe.getType().equals(XFormsEventFactory.FOCUS_EVENT))//&&this.visible)
                {
                    // Default action for the recalculate event
                    this.component.setFocus();
                }
            }
        }
        return ret;
    }
    
    
    /**
     * notify this listener that there was an error in the value of the
     * instance item. This can be schema validity, constraint, required etc.
     */
    public void notifyError(Exception e,boolean atSubmission)
    {
        if (atSubmission)
        {
            //this.valueChangedEver=true;
            this.checkValidity(retrieveInstanceItem(this.getRefNode()));
        }
        this.getHandler().showErrorText("XForms validation error: "+e.getMessage());
        
        
    }
    /** interactive attributes */
    public void setAttribute(String name, String value)
    {
        super.setAttribute(name,value);
        this.setAttributeValue(name,value);
    }
    
    public Attr setAttributeNode(Attr newAttr) throws DOMException
    {
        setAttributeValue(newAttr.getName(), newAttr.getValue());
        return super.setAttributeNode(newAttr);
    }
    
    protected void setAttributeValue(String name, String value)
    {
        if (name.equals(INCREMENTAL_ATTRIBUTE))
        {
            incremental =  (value!=null&&(value.equals("true")||value.equals("1")));
        }
        else if (name.equals(INPUTMODE_ATTRIBUTE))
        {
            inputMode =  value;
        }
    }
    protected void setInputMode(String im)
    {
        if (im==null||im.equals("")) return;
        String inputMode = im;
        Node aNode = this;
        while( aNode != null )
        {
            if( aNode.getNodeType() == Node.ELEMENT_NODE )
                if( ((Element)aNode).getAttribute("xml:lang") != "" ) break;
            aNode = aNode.getParentNode();
        }
        if( aNode != null )
        {
            inputMode = inputMode + " " + ((Element)aNode).getAttribute("xml:lang");
        }
        this.component.setInputMode(inputMode);
    }
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
        String type = evt.getType();
        if (type.equals(XFormsEventFactory.CAPTION_CHANGED))
        {
            CaptionElementImpl capt = this.getCaption();
            if (capt!=null)
            {
                this.captionComp.setText(capt.getCaptionText());
            }
        }
    }
    /** @return the instanceItemListener for the referred nodes, for value
     * changes without binding change */
    public InstanceItemListener getInstanceItemListener()
    {
        return this;
    }
    /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
		// lets save the currect data to the instance before changing the binding
        /*
         
        if (this instanceof AdaptiveControl)
        {
            ((AdaptiveControl)this).valueChanged(false,this);
        }*/
        super.notifyBindingChanged(newBinding);
        //Log.debug("BINDING HAS CHANGED FOR XFormsControl"+this);
        // TODO: empty nodeset
        if (this.getRefNode()==null)
        {
            Log.debug("New ref node is null: "+this.getCaptionText());
        }
        else if (this.init==false)
        {
            //Log.debug("new refnode: "+(String)this.getRefNodeValue());
            //if (this instanceof AdaptiveControl) ((AdaptiveControl)this).adaptiveBindingChanged();
            //this.valueChanged((String)this.getRefNodeValue());
        }
        //this.checkBindingVisibility();
    }
    
    protected void doHelp(ActionEvent event)
    {
        Log.debug(this+".doHelp() : "+event);
        this.dispatchEvent(XFormsEventFactory.createEvent(XFormsEventFactory.HELP_EVENT));
        
    }
    /**
     * JButton clicks are handled by actionPerformed
     */
    public void actionPerformed(ActionEvent ae)
    {
        if (ae.getActionCommand()=="help")
        {
            this.doHelp(ae);
        } else if (ae.getActionCommand()==XInput.ENTER_STROKED)
        {
            this.dispatchActivateEvent();
        }
    }
    /* TODO
    protected void checkDatatype() throws DatatypeException
    {
      // or should we keep an array of allowed datatypes? remember the datatype hierarchy
        if (this.data.getType()==HEXBIN || base64 ) // upload will override this method
        {
            throw new DatatypeException("");
        }
    }*/
    
    
    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        if (pseudoclass.equals(INVALID_PSEUDOCLASS))
        {
            if (this.getRefNode()!=null)
                return !(retrieveInstanceItem(this.getRefNode()).getValid());
            else return false;
        }
        else if (pseudoclass.equals(VALID_PSEUDOCLASS))
        {
            if (this.getRefNode()!=null)
                return retrieveInstanceItem(this.getRefNode()).getValid();
            return true;
        }
        else if (pseudoclass.equals(ENABLED_PSEUDOCLASS))
        {
            boolean relevant = true;
            if (this.getRefNode()!=null)
                relevant = retrieveInstanceItem(this.getRefNode()).getRelevant();
            // return true, if we are currently valid
            return (
					//this.visible&& 
					(this.getBindingState()!=DynBoundElementImpl.BOUND_NODE_NOT_FOUND)&&relevant);
        }
        else if (pseudoclass.equals(DISABLED_PSEUDOCLASS))
        {
            boolean relevant = true;
            if (this.getRefNode()!=null)
                relevant = retrieveInstanceItem(this.getRefNode()).getRelevant();
            // return true, if we are currently valid
            return (
					//!this.visible || 
					(this.getBindingState()==DynBoundElementImpl.BOUND_NODE_NOT_FOUND)||!relevant);
        }
        else if (pseudoclass.equals(READONLY_PSEUDOCLASS))
        {
            // return true, if we are currently readonly
            if (this.getRefNode()!=null) return retrieveInstanceItem(this.getRefNode()).getReadonly();
            else return false;
        }
        else if (pseudoclass.equals(READWRITE_PSEUDOCLASS))
        {
            // return true, if we are currently readwrite
            if (this.getRefNode()!=null) return !this.getRefNode().getInstanceItem().getReadonly();
            else return true;
        }
        else if (pseudoclass.equals(OUTOFRANGE_PSEUDOCLASS))
        {
            return outOfRange;
        }
        else if (pseudoclass.equals(INRANGE_PSEUDOCLASS))
        {
            return !outOfRange;
        }
        else return super.isPseudoClass(pseudoclass);
    }
	
	/** should this be visible. this is based on the ref node's relevant and refnode being non-null */
	public boolean isVisible()
	{
		if (this.getRefNode()==null) return false;
		else return retrieveInstanceItem(this.getRefNode()).getRelevant();

	}
    /**
     * This is called when a binding goes to zero nodes
     */
    public void checkVisibility()
	{
		super.checkVisibility();
		//this.styleChanged();
	}
    
    protected int getDatatypeId()
    {
        if (this.getRefNode()==null) return -1;
        InstanceItem item = retrieveInstanceItem(this.getRefNode());
        if (item==null) return -1;
        return item.getPrimitiveTypeId();
    }
    
    protected Document getParentOwnerDocument()
    {return this.getOwnerDocument();}
    public CSSStyleDeclaration getOwnerStyle()
    {return getStyle();}
    public Node getParentNodeForPseudoElement()
    {return this;}
    /*** pseudo - element: :value */
    
    /** get the vector containing all pseudoelements of this element.
     * null or empty vector means that there are no pseudoelements
     */
    protected Vector pseudoElements;
    protected VisualElementImpl valuePseudoElement; //= new ValuePseudoElement(this);
    
    protected void createValuePseudoElement()
    {
        valuePseudoElement= new ValuePseudoElement(this);
        this.getPseudoElements().clear();
        this.getPseudoElements().addElement(valuePseudoElement);
    }
    public Vector getPseudoElements()
    {
        if (pseudoElements==null)
        {
            pseudoElements=new Vector();
        }
        return pseudoElements;
    }
    
    /*
    public void initStyle(XSmilesStyleSheet styleSheet)
    {
        super.initStyle(styleSheet);
        this.valuePseudoElement.initStyle(styleSheet);
    }*/
    
    public class ValuePseudoElement extends VisualElementImpl
    implements PseudoElement, VisualComponentService
    {
        XFormsControl owner;
        public ValuePseudoElement(XFormsControl control)
        {
            super((org.apache.xerces.dom.DocumentImpl)getParentOwnerDocument(),"xforms","value");
            owner=control;
        }
        
        public String getPseudoElementName() // return e.g. "value" for ::value
        {
            return "value";
        }
        
        public Node getParentNode()
        {
            if (owner==null) return null;
            return owner.getParentNodeForPseudoElement();
        }
        /**
         * Returns the visible component of this control
         */
        public Component getComponent()
        {
            if (owner.componentInited==false) owner.componentInit();
            if (owner.component==null)
            {
                Log.error(this.toString()+"XForms fatal error: component was null, the component was never created.");
                return null;
            }
            //return component.getComponent();
            return owner.component.getComponent();
        }
        
        public Dimension getSize()
        {
            return getComponent().getSize();
        }
        
        
        public boolean getVisible()
        {
            return owner.isVisible()&&owner.getBindingState()!=DynBoundElementImpl.BOUND_NODE_NOT_FOUND;
        }
        
        public void setVisible(boolean a_vis)
        {
            //owner.visible=a_vis;
            owner.checkVisibility();
			if (this.getComponent()!=null)
				this.getComponent().setVisible(owner.isVisible()&&a_vis);
        }
        
        /**
         * Sets the zoom (1.0 is the default)
         */
        public void setZoom(double zoom)
        {
                /*CaptionElementImpl caption = this.getCaption();
                if (caption!=null) caption.setZoom(zoom);*/
            owner.component.setZoom(zoom);
            //super.setZoom(zoom);
        }
        /*
        public void initStyle(XSmilesStyleSheet styleSheet)
        {
            super.initStyle(styleSheet);
            //Log.debug("********* ::value"+this+" style:"+this.getStyle());
            formatComponent();
        }*/

        /* (non-Javadoc)
         * @see fi.hut.tml.xsmiles.dom.PseudoElement#setParentNode(org.w3c.dom.Node)
         */
        public void setParentNode(Node p)
        {
            // TODO Auto-generated method stub
            if (p!=null) owner=null;
        }

        /* (non-Javadoc)
         * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
         */
        public void visualEvent(int event,Object obj)
        {
            if (event==VisualComponentService.EVENT_STYLECHANGED)
            {
                //this.owner.formatCaption();
                this.owner.formatComponent();
            }
            
        }

        /* (non-Javadoc)
         * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
         */
        public XSmilesConnection get(URL dest, short type)
        {
            // TODO Auto-generated method stub
            return null;
        }
        
        
        /*public CSSStyleDeclaration getStyle()
        {
            return getOwnerStyle();
        }*/
        
    }
    
        public Object clone() throws CloneNotSupportedException
        {
            XFormsControl control = (XFormsControl)super.clone();
            // XSmilesElement clones the pseudo-elements as well
            control.valuePseudoElement= null;
            control.pseudoElements=null;
            control.createValuePseudoElement();
            //Log.debug("Cloning:"+this+"to: "+control.hashCode()+" valuepseudo: "+control.valuePseudoElement.hashCode());
            return control;
        }
        /** overridden, because there is always just one pseudoelement */
		public void clonePseudoElements(PseudoElementContainerService from, PseudoElementContainerService to)
		{
			return;
		}
		public ECMAScripter getECMAScripter()
		{
			ExtendedDocument edoc = (ExtendedDocument)this.getOwnerDocument();
			return edoc.getHostMLFC().getXMLDocument().getECMAScripter();
		}
    
}
