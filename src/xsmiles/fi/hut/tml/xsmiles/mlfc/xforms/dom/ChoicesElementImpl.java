/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;




import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;    
import java.io.StringReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

    

/**
 * Choises element
 * @author Mikko Honkala
 */

public class ChoicesElementImpl extends XFormsElementImpl{

	public ChoicesElementImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}
	void select()
	{
		this.dispatchActivateEvent();
	}
}
