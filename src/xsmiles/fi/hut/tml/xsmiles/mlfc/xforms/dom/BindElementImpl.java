/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.EventBroker;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.constraint.ExpressionContainer;



import org.w3c.dom.*;




/**
 * XForm/Bind element implementation
 * @author Mikko Honkala
 */


public class BindElementImpl extends ModelBoundElementImpl implements BindElement, ExpressionContainer
{
    
    protected String calculate,relevant,readonly,isvalid,required,type;
    
    protected ModelElementImpl xform;
    
    /** my context node, for outermost bind this the root of instance data */
    //protected Node contextNode;
    
    
    public BindElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        // if there is an enclosing bound element, use its context node
        try
        {
            //getContextFromAncestor(this,this);
            createBinding();
            this.getModel().addBind(this);
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        } catch (Exception ex)
        {
            this.handleXFormsException(new XFormsComputeException(ex.toString(),this));
        } catch (Throwable ex)
        {
            this.handleXFormsException(new XFormsComputeException(ex.toString(),this));
        }
        finally
        {
            super.init();
        }
    }
    
    private void printList(NodeList list)
    {
        for (int i=0;i<list.getLength();i++)
        {
            Node n = list.item(i);
            Log.debug(i+"th item: "+n);
        }
    }
    
    
    /**
     * Gets the default XForm element
     */
    public ModelElementImpl getModel()
    {
        Node docElem = this.getOwnerDocument().getDocumentElement();
        Node parent=this.getParentNode();
        while (parent!=docElem)
        {
            if (parent instanceof ModelElementImpl) return (ModelElementImpl)parent;
            parent=parent.getParentNode();
        }
        return null;
    }
    
    public String getCalculateString()
    {
        if (calculate==null)
            calculate=getAttribute(CALCULATE_ATTRIBUTE);
        return calculate;
    }
    public String getRelevantString()
    {
        if (relevant==null)
            relevant=getAttribute(RELEVANT_ATTRIBUTE);
        return relevant;
    }
    public String getRequiredString()
    {
        if (required==null)
            required=getAttribute(REQUIRED_ATTRIBUTE);
        return required;
    }
    public String getReadonlyString()
    {
        if (readonly==null)
            readonly=getAttribute(READONLY_ATTRIBUTE);
        return readonly;
    }
    public String getIsvalidString()
    {
        if (isvalid==null)
            isvalid=getAttribute(ISVALID_ATTRIBUTE);
        return isvalid;
    }
    public Node getNamespaceContextNode()
    {
        return this;
    }
    public ModelContext getModelContext()
    {
        return this.getModel();
    }
    public XPathEngine getXPathEngine()
    {
        return this.getModel().getXPathEngine();
        
    }
    public String getTypeString()
    {
        if (type==null)
            type=getAttribute(TYPE_ATTRIBUTE);
        return type;
    }
    
    
    /** @return the list of context nodes of this expression */
    public NodeList getContextNodeList()
    {
        return this.getBoundNodeset();
    }
    
 
    
    
}
