/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 6, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
//import fi.hut.tml.xsmiles.gui.components.swing.DefaultComponentFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.speech.Recognizer;
import fi.hut.tml.xsmiles.speech.SpeechFactory;
import fi.hut.tml.xsmiles.speech.Synth;


/**
 * This creates an additional Dialog UI layer on top of an XForms document.
 * It is possible to operate thee Dialog UI with a voice recognizer and voice output for instance
 * @author honkkis
 *
 */
public class DialogGUITest extends DialogGUIDebug
	implements DialogUI, ActionListener
{
    public static DialogGUIDebug getInstance(ComponentFactory f)
    {
        if (instance==null)
        {
            instance=new DialogGUITest();
            if (f!=null) instance.setComponentFactory(f);
            instance.start();
        }
        return instance;
    }
    
    public static void main(String args[])
    {
        //DialogGUIDebug test = getInstance(new DefaultComponentFactory());
        //test.start();
    }
	
	protected void speakButtonPressed()
	{
	    super.speakButtonPressed();
	}
	
	
	
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#showQuestion(java.lang.String)
     */
    public void showQuestion(String question, Reader grammar)
    {
        // TODO Auto-generated method stub
        
    }
    protected void fireEvent(ActionEvent e)
    {
        super.fireEvent(e);
    }



    /* This event is received when the user has typed in something
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent arg0)
    {
        String input = this.answerInput.getText();
        super.actionPerformed(arg0);
        this.answerGiven(input);
   }
    
    DateFormat formatOnlyMonth = new SimpleDateFormat("MMM");
    DateFormat formatOnlyMonthLong = new SimpleDateFormat("MMMMM");    
    protected Calendar parseDate(String s, Calendar origDate)
    {
        StringTokenizer tokenizer = new StringTokenizer(s," ,");
        while (tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            //First check if it is a number
            
            try
            {
                int number = Integer.parseInt(token);
                if (number>39&& number < 5000)
                {
                    // it is a year
                    origDate.set(Calendar.YEAR,number);
                }
                else if (number >0 && number <32)
                {
                    // it is a day number
                    origDate.set(Calendar.DAY_OF_MONTH,number);
                }
                else
                {
                    Log.debug("Did not understand:"+token);
                }
            }
            catch (NumberFormatException e)
            {
                // was not a number, try a month
                // FIRST TRY SHORT NOTATION "mar, jan"
    	        DateFormat format = formatOnlyMonth; 
    	        try
    	        {
    	            Date d = format.parse(token);
    	            origDate.set(Calendar.MONTH,d.getMonth());
    	        } catch (ParseException ex)
    	        {
                    // TRY LONG NOTATION ("january, march")
        	        format = formatOnlyMonthLong; 
        	        try
        	        {
        	            Date d = format.parse(token);
        	            origDate.set(Calendar.MONTH,d.getMonth());
        	        } catch (ParseException ex2)
        	        {
                        Log.debug("Did not understand:"+token);
        	            this.append(ex2.toString()+" : "+ex2.getMessage());
        	        }
    	        }
            }
        }
        
        return origDate;
    }
    
    protected int countDateTokens(String s)
    {
        StringTokenizer tokenizer = new StringTokenizer(s," ,");
        return tokenizer.countTokens();
    }
    Calendar current = Calendar.getInstance();
    protected void answerGiven(String answer)
    {
        current = this.parseDate(answer,current);
        if (current!=null)
        {
            this.append(current.getTime().toString());
        }
    }


}
