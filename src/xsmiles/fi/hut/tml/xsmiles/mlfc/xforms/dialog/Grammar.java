/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 19, 2004
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author mpohja
 */
public class Grammar
{
    Vector imports = new Vector(5), headers = new Vector(5), rules = new Vector(5);
    
    String start = "#JSGF V1.0 ISO8859-1 en;\n\ngrammar commands;";
    String ruleStart = "public <commands> = ";

    /**
     * 
     */
    public Grammar()
    {
        super();
    }

    /**
     * @param commands
     */
    public Grammar(String[] commands)
    {
//      This is to navigate up in a focus tree. "back" isn't probably the best command...
        this();
        String coms = "";
        for (int i =0 ;i<commands.length;i++)
        {
            String curr = commands[i];
            if (curr!=null&&curr.length()>0)
            {
                coms+=curr;
                if (i<commands.length-1) coms+=" | ";
            }
        }
        setRule("<nav> = "+coms+";", "<nav>");
    }

    /**
     * Set an import statement to grammar.
     * 
     * @param importRule whole statement, e.g., import &lt;command&gt;;
     * @param header whole header, e.g., &lt;command&gt;
     */
    public void setImport(String importRule, String header)
    {
        imports.add(importRule);
        headers.add(header);
    }
    
    /**
     * Set rules to grammar.
     * 
     * @param rule complete rule
     * @param header whole header, e.g., &lt;command&gt;
     */
    public void setRule(String rule, String header)
    {
        rules.add(rule);
        headers.add(header);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        String grammar = start, text;
        Enumeration imps = imports .elements();
        while (imps.hasMoreElements())
        {
            text = (String)imps.nextElement();
            grammar += "\n" + text;
        }
        
        grammar += "\n\npublic <commands> = ";
        
        imps = headers.elements();
        while (imps.hasMoreElements())
        {
            text = (String)imps.nextElement();
            grammar += text;
            if (imps.hasMoreElements())
                grammar += " | ";
        }
        
        grammar += ";\n";
        
        imps = rules.elements();
        while (imps.hasMoreElements())
        {
            text = (String)imps.nextElement();
            grammar += "\n" + text;
        }
        
        return grammar;
    }
}
