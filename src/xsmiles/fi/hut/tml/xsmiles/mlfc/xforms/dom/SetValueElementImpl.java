/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;

import fi.hut.tml.xsmiles.dom.EventHandlerService;


import org.w3c.dom.*;
import org.w3c.dom.events.*;

/**
 * XForm/setValue element implementation
 * @author Mikko Honkala
 */


public class SetValueElementImpl extends ActionHandlerBaseImpl implements EventHandlerService
{
    /** The context node for the xpath expression */
    //Node contextNode=null;
    
    public SetValueElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    
    public void activate(Event evt)
    {
        ModelElementImpl model = this.getModel();
        String ref = this.getRef();
        if (ref==null&&ref.length()==0)
        {
            Log.error("Missing or empty 'ref' attribute on setValue.");
        }
        try
        {
            String value = this.getValue();
            Log.debug("SetValue.activate: "+evt+"ref: "+ref+" value:"+value+" contextNode"+contextNode);
            XPathExpr xpathe=model.getXPathEngine().createXPathExpression(ref);
            InstanceNode node = (InstanceNode)model.getXPathEngine().evalToNodelist(contextNode,xpathe,this,null).item(0);
            XPathExpr valueExpr =model.getXPathEngine().createXPathExpression(value);
            String result=model.getXPathEngine().evalToString(node,valueExpr,this,null);
            //XFormsUtil.setText(node,result);
            model.uiValueChange(node,result);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    String getValue()
    {
        Attr valAttr = this.getAttributeNode("value");
        if (valAttr!=null)
            return valAttr.getNodeValue();
        else
            return "'"+this.getText()+"'";
    }
}
