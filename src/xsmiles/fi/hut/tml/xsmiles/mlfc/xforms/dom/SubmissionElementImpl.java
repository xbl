/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMWriterFilter;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Resources;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.InstanceElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.Submission;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.util.URLUtil;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.xml.serializer.SerializerFactory;
import fi.hut.tml.xsmiles.xml.serializer.URLSerializer;
import fi.hut.tml.xsmiles.xml.serializer.XMLSerializerInterface;



/**
 * XForm/SubmitInfo element
 * @author Mikko Honkala
 */


public class SubmissionElementImpl extends ModelBoundElementImpl implements Submission, org.w3c.dom.events.EventListener, DOMWriterFilter
{
    
    public SubmissionElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        //this.bindingAttributesRequired=false;
    }
    
    public ModelElementImpl getModel()
    {
        return (ModelElementImpl)this.getParentNode();
    }
    public InstanceElementImpl getInstance(String id)
    {
        return getModel().getInstance(id);
    }
    public SchemaElementImpl getSchema()
    {
        return getModel().getSchema();
    }
    
    public String getId()
    {
        return this.getAttribute("id");
    }
    
    public boolean getIndent()
    {
        return ("true".equals(this.getAttribute(INDENT_ATTRIBUTE))||"1".equals(this.getAttribute(INDENT_ATTRIBUTE))
        ?true:false);
    }
    public String getIncludeNamespacePrefixes()
    {
        return (String)this.getAttribute(INCLUDENAMESPACEPREFIXES_ATTRIBUTE);
    }
    public String getSeparator()
    {
        Attr sepAttr = this.getAttributeNode(SEPARATOR_ATTRIBUTE);
        if (sepAttr==null) return null;
        else return sepAttr.getNodeValue();
    }
    public short getReplace()
    {
        String replaceValue =  this.getAttribute(REPLACE_ATTRIBUTE);
        if (replaceValue.equals("none")) return REPLACE_NONE;
        else if (replaceValue.equals("instance")) return REPLACE_INSTANCE;
        // X-Smiles extensions
        else if (replaceValue.equals("new")) return REPLACE_NEW;
        else return REPLACE_ALL;
    }
    
    public boolean shouldIrrelevantBeRemovedFromSubmission()
    {
        Attr ra = this.getAttributeNodeNS("http://www.xsmiles.org/2003/xforms/extensions","removeirrelevant");
        if (ra==null) return true;
        if (ra.getNodeValue().equalsIgnoreCase("false")||ra.getNodeValue().equalsIgnoreCase("0")) return false;
        else return true;
    }
    
    public String getMethod()
    {
        return this.getAttribute(METHOD_ATTRIBUTE);
    }
    public String getLocalFile()
    {
        return this.getAttribute("localfile");
    }
    public String getTarget()
    {
        return this.getAttribute(ACTION_ATTRIBUTE);
    }
    /**
     * Get the encoding of the SubmitInfo if specified.
     *
     * @return the value of the "encoding" attribute, (null if not found)
     */
    public String getEncoding()
    {
        return this.getAttribute("encoding");
    }
    
    /** will return what is in the mediatype attribute, or "application/xml" as default
     * if the attribute is not present
     */
    public String getMediaType()
    {
        Attr attr = this.getAttributeNode(MEDIATYPE_ATTRIBUTE);
        if (attr==null) return "application/xml";
        else return attr.getNodeValue();
    }
    
    
    public void init()
    {
        
        // try to find my context node from ancestor
        // if repeat has already set my contextNode, don't fiddle with it!
        //if (this.contextNode==null) getContextFromAncestor(this);
        
        //this.initSingleNode(false);
        //this.findAndSetRefNode(getRef());
        super.init();
        this.addEventListener(XFormsEventFactory.SUBMIT_EVENT, this, false);
    }
    
    /**
     * Submit the form data according to form parameters
     *
     * <pre>
     * Submitting the Form
     * ------------------------------
     * // DEBUGging forms:
     *
     * Method must be get when debugging
     *
     * 1.
     * <xform
     * action="file:///d:/source/xsmiles/demo/fo/link.fo"
     * method="get"
     * localfile="temp2.xml"
     * id="form1">
     *
     * -> save to temp2.xml and go to link.fo
     *
     * 2.
     * <xform
     * method="get"
     * localfile="temp2.xml"
     * id="form1">
     *
     * -> save to localfile temp2.xml and load it in the browser
     *
     *
     *
     * // These methods are not implemented yet:
     * method="postxml" - post using post to action URL and retrieve next document
     * method="jms" - send form using java messaging (what document to load in the browser?)
     *
     * Get and post are implemented for backwards compatibility (old cgi-bins and servlets)
     * method="post" - post name-value pairs to action URL and retrieve next document
     * method="get" - go to the URL "action", if URL is http, append fields as script variables after '?'
     * </pre>
     */
    public synchronized void submit() throws XFormsSubmitException
    {
        try
        {
            // just return if submission is already going on.
            if (this.submitThread!=null&&this.submitThread.isAlive())
            {
                Log.error("A submission is already started, doing nothing.");
                return;
            }
            if (this.elementStatus==DESTROYED) 
            {
                Log.error("A submission element was destroyed, doing nothing.");
                return;
                
            }
            //this.renewBinding();
            this.createBinding();
            Node no = this.getRefNode();
            if (no!=null&&no.getNodeType()==Node.TEXT_NODE)
            {
                no = no.getParentNode();
            }
            InstanceNode n = (InstanceNode)no;
            if (n==null)
            {
                InstanceElementImpl inst = getModel().getInstance();
                if (inst==null)
                {
                    throw new XFormsSubmitException(this,"submit: no default instance found.");
                }
                n=(InstanceNode)inst.getInstanceRoot();
                if (n==null)
                {
                    throw new XFormsSubmitException(this,"submit: No instance document found.");
                }
            }
            InstanceDocument d = (InstanceDocument)n.getOwnerDocument();
            //SchemaElementImpl model=this.getSchema();
            if (this.getModel().validation())
            {
                // Check if the instance validates
                Vector errors = d.validateDocument(true);
                if (errors.size()>0)
                    //if (!this.getModel().revalidate())
                {
                    this.showError("XForms schema validation error","There are errors in the form, it could not be submitted.");
                    return;
                }
            }
            // check XForms validity
            boolean xformsvalid = XFormsUtil.isXFormsValid(n);
            if (!xformsvalid)
            {
                this.showError("XForms isValid error","There are errors in the form, it could not be submitted.");
                return;
            }
            String method = this.getMethod();
            String localfile = this.getLocalFile();
            String action = this.getTarget();
            // i18n:  Pass the specified encoding to the document
            //        to be sent (if specified)
            String encoding = this.getEncoding();
            if (encoding != null&&encoding.length()>0) d.setEncoding(encoding);
            
            URL url = null;
            if (action!=null)
                url = resolveURI(action);
            this.submitThread(method,localfile,action, url,n);
        } catch (XFormsSubmitException ex)
        {
            throw ex;
        } catch (Exception e)
        {
            Log.error(e);
            throw new XFormsSubmitException(this,e.toString());
        }
    }
    
    protected Thread submitThread;
    public synchronized void submitThread(final String method,final String localfile, final String action, final URL url, final InstanceNode n) throws Exception
    {
        this.dispatch(XFormsConstants.SUBMIT_STARTED_EVENT);
        final SubmissionElementImpl submission = this;
        submitThread = new Thread()
        {
            public void run()
            {
                try
                {
			        // If localfile is specified, FORM DEBUGGING
			        // Can only be used to write temp2.xml, config.xml to the local dir
			        File tempfile;
			        if (localfile!=null&&localfile.length()>0)
			        {
			            submission.submitLocalFile(localfile,action,url,n);
			        }
			        else
			        {
			            if (action==null)
			                throw new Exception("Form does not have action attribute.");
			            if (method.equalsIgnoreCase("get"))
			            {
			                submission.submitGet(url,n);
			            }
			            else if (method.equalsIgnoreCase("postxml")||method.equalsIgnoreCase("post"))
			            {
			                submission.submitPost(url,n);
			            }
			            else if (method.equalsIgnoreCase("multipart-post"))
			            {
			                submission.submitMultiPartPost(url,n);
			            }
			            else if (method.equalsIgnoreCase("urlencoded-post"))
			            {
			                submission.submitUrlEncodedPost(url,n);
			            }
			            else if (method.equalsIgnoreCase("form-data-post"))
			            {
			                submission.submitFormDataPost(url,n);
			            }
			            else if (method.equalsIgnoreCase("postjms"))
			            {
			                submission.submitJMS(url,n);
			            }
			            else if (method.equalsIgnoreCase("put"))
			            {
			                submission.submitPUT(url,n);
			            }
			            else throw new XFormsException("Form submit method: "+method+" is not supported.");
			        }
			        // Success, dispatch submit-done event to the DOM
			        // TODO: this does not actually work, since submitting is done by
			        // the browser in another thread...
                } catch (Exception e)
                {
                    Log.error(e);
                    submission.submitThread=null; //if an action starts a new submission, this thread should not block the new one
    		        submission.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.SUBMIT_ERROR));
                }
                submission.submitThread=null; //if an action starts a new submission, this thread should not block the new one
		        submission.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.SUBMIT_DONE_EVENT));
            }
        };
        submitThread.start();
     }    
    protected Hashtable properties = new Hashtable();
    protected void submitPost(URL url, InstanceNode root) throws XFormsSubmitException
    {
        try
        {
            //StringWriter wr = new StringWriter();
            String instancestr = this.serializeXML(root);
            //String instancestr = wr.toString();
            //Log.debug("Instance to be posted: "+instancestr);
            properties.put("Content-Type",this.getMediaType());
            XLink xlink = new XLink(url,instancestr,XLink.POST_HTTP,properties);
            this.finalizeSubmitAndReplace(xlink,this.getReplace(),root,this.getReplaceInstance());
            //getBrowser().openLocation(xlink, true);
        } catch (Exception e)
        {
            throw new XFormsSubmitException(this,e.getMessage());
        }
    }
    
    protected void submitUrlEncodedPost(URL url, InstanceNode root) throws XFormsSubmitException
    {
        try
        {
            String separator = this.getSeparator() == null ? ";" : this.getSeparator();
            URLSerializer serializer = new URLSerializer(separator);
            StringWriter wr = new StringWriter();
            serializer.serialize(root,wr);
            String instancestr = wr.toString();
            //Log.debug("Instance to be posted application/x-www-form-urlencoded: "+instancestr);
            properties.put("Content-Type","application/x-www-form-urlencoded");
            XLink xlink = new XLink(url,instancestr,XLink.POST_HTTP,properties);
            this.finalizeSubmitAndReplace(xlink,this.getReplace(),root,this.getReplaceInstance());
            //getBrowser().openLocation(xlink, true);
        } catch (Exception e)
        {
            throw new XFormsSubmitException(this,e.getMessage());
        }
    }
    protected final static short MODE_FORMDATA = 2;
    protected final static short MODE_RELATED = 3;
    
    protected void submitMultiPartPost(URL url, InstanceNode root) throws XFormsSubmitException
    {
        try
        {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            String instancestr = this.serializeXML(root);
            //Log.debug("Instance to be posted multipart/related: "+instancestr);
            String boundary=this.getBoundary();
            properties.put("Content-Type","multipart/related; boundary="+boundary+"; type="+this.getMediaType()+"; start=\"<"+this.getStartID()+">\"");
            
            os.write((this.getBoundary()+"\n").getBytes());
            this.addAttachment(os,instancestr.getBytes(),this.getMediaType(),null,this.getStartID(),this.getBoundary());
            // TODO: go thru instance from the root and append all URI attachments
            this.findAndAddAttachments(os,root, MODE_RELATED);
            
            XLink xlink = new XLink(url,os.toByteArray(),XLink.POST_HTTP, properties);
            this.finalizeSubmitAndReplace(xlink,this.getReplace(),root,this.getReplaceInstance());
            //getBrowser().openLocation(xlink, true);
        } catch (Exception e)
        {
            throw new XFormsSubmitException(this,e.getMessage());
        }
    }
    protected void submitFormDataPost(URL url, InstanceNode root) throws XFormsSubmitException
    {
        try
        {
            //TODO: Put in a way to access the attachment data from upload.
            //TODO: Either add a peer to serializeXML or make it take parameters to do multipart/form-data.
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            //Log.debug("Instance to be posted multipart/form-data");
            String boundary=this.getBoundary();
            properties.put("Content-Type","multipart/form-data; boundary="+boundary);
            
            os.write((this.getBoundary()+"\n").getBytes());
            // TODO: go thru instance from the root and append all URI attachments
            this.findAndAddAttachments(os,root, MODE_FORMDATA);
            
            XLink xlink = new XLink(url,os.toByteArray(),XLink.POST_HTTP, properties);
            this.finalizeSubmitAndReplace(xlink,this.getReplace(),root,this.getReplaceInstance());
            //getBrowser().openLocation(xlink, true);
        } catch (Exception e)
        {
            throw new XFormsSubmitException(this,e.getMessage());
        }
    }
    
    protected void findAndAddAttachments(ByteArrayOutputStream os, InstanceNode root, short mode) throws IOException
    {
        NodeIterator iter=
        ((DocumentTraversal)root.getOwnerDocument()).createNodeIterator(
        root, NodeFilter.SHOW_ELEMENT|NodeFilter.SHOW_ATTRIBUTE, null,true);
        for (Node n = iter.nextNode(); n!=null; n = iter.nextNode())
        {
            addAttachment(os,n,mode);
            // add also attributes
            NamedNodeMap attrs = n.getAttributes();
            for (int i=0;i<attrs.getLength();i++)
            {
                Attr a = (Attr)attrs.item(i);
                addAttachment(os,a,mode);
            }
        }
    }
    protected void addAttachment(ByteArrayOutputStream os, Node n, short mode) throws IOException
    {
        if (n instanceof InstanceNode)
        {
            InstanceNode in = (InstanceNode)n;
            InstanceItem item = retrieveInstanceItem(in);
            byte[] attachment = item.getAttachment();
            if (attachment!=null)
            {
                String mimetype=item.getAttachmentMime();
                String uri = item.getAttachmentURI();
                String transfer=item.getAttachmentTransfer();
                String filename = item.getAttachmentFilename();
                if (mode==MODE_RELATED&&item.getPrimitiveTypeId()==XFormsConfiguration.getInstance().getDataFactory().PRIMITIVE_ANYURI)
                    this.addAttachment(os,attachment,mimetype,transfer, uri,this.getBoundary());
                else if (mode==MODE_FORMDATA)
                    this.addAttachmentFormData(os,attachment,mimetype,n.getLocalName(),this.getBoundary(), filename);
            } else if (mode==MODE_FORMDATA)
            {
                if (in.getNodeType()==Node.ELEMENT_NODE)
                {
                    // TODO: char encoding
                    String text = URLSerializer.getTextIfOneChild((Element)in);
                    if (text!=null)
                        this.addAttachmentFormData(os,text.getBytes(),"text/plain",n.getLocalName(),this.getBoundary(), null);
                }
            }
        }
    }
    
    protected void addAttachment(ByteArrayOutputStream os, byte[] attachment, String contentType, String contentTransferEncoding, String contentId, String boundary) throws IOException
    {
        os.write(("Content-Type: "+contentType+"\n").getBytes());
        if (contentTransferEncoding!=null)
            os.write(("Content-Transfer-Encoding: "+contentTransferEncoding+"\n").getBytes());
        
        os.write(("Content-ID: <"+contentId+">\n").getBytes());
        os.write(attachment);
        os.write(("\n"+this.getBoundary()+"\n").getBytes());
    }
    protected void addAttachmentFormData(ByteArrayOutputStream os, byte[] attachment, String contentType, String name,  String boundary, String filename) throws IOException
    {
        os.write(("Content-Disposition: form-data; name=\""+name+"\"").getBytes());
        if (filename!=null)
            os.write(("; filename=\""+filename+"\"").getBytes());
        os.write(("\nContent-Type: "+contentType+"\n\n").getBytes());
        
        os.write(attachment);
        os.write(("\n"+this.getBoundary()+"\n").getBytes());
    }
    
    protected  String _boundary="--------xs1q2w3e4r5t6y7u8i"+(int)(Math.random()*100000);
    protected  String _start="content-start"+(int)(Math.random()*100000)+"@xsmiles.org";
    protected String getBoundary()
    {
        return _boundary;
    }
    
    protected String getStartID()
    {
        return _start;
    }
    
    
    
    /** X-Smiles extension to be able to replace any instance */
    protected String getReplaceInstance()
    {
        Attr ra = this.getAttributeNode("instance");
        if (ra==null)
        	ra = this.getAttributeNodeNS("http://www.xsmiles.org/2003/xforms/extensions","replaceinstance");
        if (ra==null) return null;
        else return ra.getNodeValue();
    }
    
    protected void submitJMS(URL url, InstanceNode root) throws XFormsSubmitException
    {
        try
        {
            //StringWriter wr = new StringWriter();
            String instancestr = this.serializeXML(root);
            //String instancestr = wr.toString();
            XLink xlink = new XLink(url,instancestr,XLink.POST_JMS,null);
            this.finalizeSubmitAndReplace(xlink,this.getReplace(),root,this.getReplaceInstance());
            //getBrowser().openLocation(xlink, true);
        } catch (Exception e)
        {
            throw new XFormsSubmitException(this,e.getMessage());
        }
    }
    
    
    protected void submitPUT(URL url, InstanceNode root) throws XFormsSubmitException
    {
        // file PUT's are stored to the filesystem
        if (url.getProtocol().equals("file"))
        {
            String fStr=null;
            fStr= URLUtil.urlToFile(url).toString();
            this.submitLocalFile(fStr,null,url,root);
        } else if (url.getProtocol().equals("http"))
        {
            // TODO: http PUT using XLink class
            try
            {
                String instancestr = this.serializeXML(root);
                //Log.debug("Instance to be PUT as XML: "+instancestr);
                properties.put("Content-Type",this.getMediaType());
                XLink xlink = new XLink(url,instancestr,XLink.PUT_HTTP,properties);
                this.finalizeSubmitAndReplace(xlink,this.getReplace(),root,this.getReplaceInstance());
                //getBrowser().openLocation(xlink, true);
            } catch (Exception e)
            {
                throw new XFormsSubmitException(this,e.getMessage());
            }
        }
    }
    protected void submitGet(URL url, InstanceNode root) throws Exception
    {
        if (!url.getProtocol().equals("file"))
        {
            
            // TBD: add form values to the URL before calling it
            String separator = this.getSeparator() == null ? ";" : this.getSeparator();
            URLSerializer serializer = new URLSerializer(separator);
            StringWriter wr = new StringWriter();
            serializer.serialize(root,wr);
            String instancestr = wr.toString();
            Log.debug("Serialized params: "+instancestr);
            url = new URL(url.toString()+"?"+instancestr);
            Log.debug("Constructed URL: "+url.toString());
            
            // http://www./../../..?f.t.name1=value1&f.t.name2=value2
        }
        this.finalizeSubmitAndReplace(new XLink(url),this.getReplace(),root,this.getReplaceInstance());
        
    }
    
    /** this method does the submission using the browserwindow object */
    protected void finalizeSubmitAndReplace(XLink link, short replace, InstanceNode root, String replaceinstance) throws Exception
    {
        if (this.elementStatus==DESTROYED) return;
        if (replace==REPLACE_ALL)
        {
            getBrowser().openLocation(link, true);
        } else if (replace==REPLACE_NEW)
        {
            getBrowser().newBrowserWindow(link);
        } else if (replace==REPLACE_INSTANCE)
        {
            InstanceElement inst;
            if (replaceinstance==null)
                inst = ((InstanceDocument)root.getOwnerDocument()).getInstanceElement();
            else
                inst = this.getModel().getInstance(replaceinstance);
            XSmilesConnection conn =getBrowser().openConnection(link); 
            InputStream stream = conn.getInputStream();
            // TODO: X-Smiles extension to replace any instance
            inst.readInstanceAndReset(stream,link.getURL());
            conn.releaseConnection();
        }
        else if (this.getReplace()==REPLACE_NONE)
        {
            XSmilesConnection conn =getBrowser().openConnection(link); 
            InputStream stream = conn.getInputStream();
            stream.read();
            stream.close();
            conn.releaseConnection();
        }
    }
    
    
    protected void submitLocalFile(String localfile, String action, URL url, InstanceNode root) throws XFormsSubmitException
    {
        File tempfile;
        boolean userPermission = false;
        if (localfile!=null&&localfile.length()>0)
        {
            try
            {
                File lfile=new File(localfile);
                tempfile=lfile;
                Log.debug("Saving form information into file: "+tempfile.getAbsolutePath());
                // localfile =filename :
                // save the instance to local hard disk
                
                if (tempfile.isDirectory()) throw new Exception("file: "+tempfile.getAbsolutePath()+" is a directory");
                boolean exists = tempfile.exists();
                userPermission = this.getUserPermissionForFileWrite(tempfile,exists);
                if (userPermission)
                {
                    // There might be problems with UTF-16 with OutputStream
                    // this was changed, because the Xerces 2.1.0 DOMWriter does
                    // not support writing to a java.io.Writer and
                    // has problems serializing namespaced documents with org.apache.xml.XMLSerializer
                    //FileWriter out = new FileWriter(tempfile);
                    FileOutputStream out = new FileOutputStream(tempfile);
                    XMLSerializerInterface serializer = new SerializerFactory().getXMLSerializer();
                    this.initXMLSerializer(serializer);
                    // TODO: this is just to test whether processing instructions are serialized
                    // should be changed to only a case where ref attribute is missing
                    Node nodeToBeSerialized;
                    Attr refAttr = this.getAttributeNode(REF_ATTRIBUTE);
                    if (refAttr==null) nodeToBeSerialized = root.getOwnerDocument();
                    else nodeToBeSerialized=root;
                    
                    serializer.writeNode(out,nodeToBeSerialized);
                    
                    //out.write(instancestr);
                    out.flush();out.close();
                    //				if(tempfile.getAbsolutePath().endsWith("bookmarks.xml"))
                    //					getBrowser().getCurrentGUI().issueBookmarksChangedEvent(new GUIEvent(this, 0));
                    /* IF THE FILE IS CONFIG.XML, then re-read configs */
                    URL tempFileURL = Utilities.toURL(tempfile);
                    //if (tempfile.getCanonicalPath().compareTo(new File(getBrowser().getOptionsDocument()).getCanonicalPath())==0)
                    if (tempFileURL.sameFile(Resources.getResourceURL("xsmiles.config")))
                    {
                        getBrowser().getBrowserConfigurer().readConfig(null);
                    }
					/* this does not work with the current UI system
                    if (this.getReplace()==REPLACE_NEW)
                    {
                        if (action==null||action.length()<1)
                        {
                            getBrowser().newBrowserWindow(new URL("file:"+tempfile.getAbsolutePath()).toString());
                        }
                        else
                        {
                            getBrowser().newBrowserWindow(url.toString());
                        }
                    }
                    else
						*/
						if (this.getReplace()!=REPLACE_NONE)
                    {
                        if (action==null||action.length()<1)
                        {
                            getBrowser().openLocation(new URL("file:"+tempfile.getAbsolutePath()));
                        }
                        else
                        {
                            getBrowser().openLocation(url);
                        }
                    }
                }
            } catch (XFormsSubmitException e)
            {
                throw e;
            } catch (Exception e)
            {
                throw new XFormsSubmitException(this,"While trying to save to locafile: "+localfile+"\nOriginal exeption:"+e.toString());
            }
        }
        else
        {
            throw new XFormsSubmitException(this,"While trying to save to locafile: "+localfile);
        }
        
    }
    
    protected void initXMLSerializer(XMLSerializerInterface serializer)
    {
        serializer.setFilter(this);
        serializer.setIndent(this.getIndent());
    }
    
    
    protected boolean getUserPermissionForFileWrite(File file, boolean exists)
    {
        String prompt;
        String title = "Confirm local file write";
        if (exists)
        {
            prompt = "The form is trying to write on top of an EXISTING file: \""+file.getAbsolutePath()+"\".  OK to overwrite?";
        }
        else
        {
            prompt = "The form is trying to write a new file: \""+file.getAbsolutePath()+"\".  OK to overwrite?";
        }
        boolean confirmed = this.getHandler().askUserConfirmation(title,prompt);
        return confirmed;
    }
    
    protected String serializeXML(Node n) throws XFormsSubmitException
    {
        try
        {
            XMLSerializerInterface serializer = new SerializerFactory().getXMLSerializer();
            this.initXMLSerializer(serializer);
            return serializer.writeToString(n);
        } catch (Exception e)
        {
            throw new XFormsSubmitException(this,"Error while serializing XML for submission\n"+e.toString());
        }
    }
    
    
    public void showError(String title, String explanation)
    {
        this.getHandler().getComponentFactory().showError(title,explanation);
    }
    
    /**
     * The DOM event handler
     */
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
        Log.debug("Submitinfo got DOM event: "+evt);
        try
        {
            this.submit();
        } catch (XFormsException e)
        {
            Log.error(e);
            Node parentNode = this.getParentNode();
            if (parentNode instanceof ModelElementImpl)
            {
                ModelElementImpl model = (ModelElementImpl)parentNode;
                model.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.SUBMIT_ERROR));
            }
            this.getHandler().showUserError(e);
            
        }
    }
    
    /** This method is from DOMWriterFilter and it will
     * discard all non-relevant XForms instance nodes
     */
    public int getWhatToShow()
    {
        return NodeFilter.SHOW_ALL;
    }
    
    /** This method is from DOMWriterFilter and it will
     * discard all non-relevant XForms instance nodes
     */
    public short acceptNode(org.w3c.dom.Node node)
    {
        //Log.debug("acceptNode: "+node);
        boolean removeIrrelevant = shouldIrrelevantBeRemovedFromSubmission();
        if (node instanceof Element)
        {
            if (node instanceof InstanceNode)
            {
                InstanceNode instanceNode = (InstanceNode)node;
                if (removeIrrelevant && retrieveInstanceItem(instanceNode).getRelevant()==false)
                {
                    //Log.debug("Rejecting: "+node);
                    return NodeFilter.FILTER_REJECT;
                }
                else
                {
                    //Log.debug("Accepting: "+node);
                    return NodeFilter.FILTER_ACCEPT;
                }
            }
            else
            {
                Log.error("SubmitInfo serializer got a node that is not InstanceNode");
                return NodeFilter.FILTER_REJECT;
            }
        }
        else
        {
            return NodeFilter.FILTER_ACCEPT;
        }
    }
    
    public String toString()
    {
        return this.getNodeName(); // JDK 11 XFormsElementImpl.serializeNode(this);
    }
}

