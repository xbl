/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;


import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerException;

import org.apache.xml.utils.XMLCharacterRecognizer;


/**
 * XForm/insert element implementation
 * @author Mikko Honkala
 */


public class DuplicateElementImpl extends ActionHandlerBaseImpl
{
    /** the prototype element for insert into empty situations */
    protected Element prototypeInstanceElement;
    
    public DuplicateElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        this.renewBinding();
        //if (this.prototypeInstanceElement==null) this.savePrototypeElement();
    }

    

    
    public void activate(Event evt)
    {
        NodeList nodes = null;
        nodes = this.getBoundNodeset();
        
        // TODO: check that index is at bounds
        try
        {
            InstanceElementImpl instance=this.getModel().getInstance();
            //this.getModel().instanceStructureChanged();
			Node origin=this.getOrigin();
			if (origin!=null)
			{
				// TODO: attributes, etc
				Node clone = origin.cloneNode(true);
				Node before = this.getBefore();
				if (clone.getOwnerDocument()!=this.getRefNode().getOwnerDocument())
				{
						clone = this.getRefNode().getOwnerDocument().importNode(clone,true);
				}
	            this.getRefNode().insertBefore(clone,before);
				//this.getModel().instanceStructureChanged();
	            // call repaint for the root node of this DOM
	            // TODO: context info
	            instance.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.DUPLICATE_EVENT));
	            this.getModel().rebuild(true);
			}
        } catch (Exception e)
        {
            this.getHandler().showUserError(e);
            Log.error(e);
        }
    }
	
	public Node getOrigin()
	{
		try
		{
			Attr originAttr = this.getAttributeNode(ORIGIN_ATTRIBUTE);
			if (originAttr==null)
			{
				return null;
			}
			String origin = originAttr.getNodeValue();
			if (origin.length()>0)
			{
				XPathExpr expr = this.getModel().getXPathEngine().createXPathExpression(origin);
				NodeList list = this.getModel().getXPathEngine().evalToNodelist(this.getContextNode(),expr,this,null);
				if (list.getLength()>0) return list.item(0);
			}
		} catch (Exception e)
		{
			// TODO: dispatch a binding exception etc
			Log.error(e);
		}
		return null;
	}

	public Node getBefore()
	{
		try
		{
			Attr originAttr = this.getAttributeNode(BEFORE_ATTRIBUTE);
			if (originAttr==null)
			{
				return null;
			}
			String origin = originAttr.getNodeValue();
			if (origin.length()>0)
			{
				XPathExpr expr = this.getModel().getXPathEngine().createXPathExpression(origin);
				NodeList list = this.getModel().getXPathEngine().evalToNodelist(this.getContextNode(),expr,this,null);
				if (list.getLength()>0) return list.item(0);
			}
		} catch (Exception e)
		{
			// TODO: dispatch a binding exception etc
			Log.error(e);
		}
		return null;
	}
    
    
    
}

