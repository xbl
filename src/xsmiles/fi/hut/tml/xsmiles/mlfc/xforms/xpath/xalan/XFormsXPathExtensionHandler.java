/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 

package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;



import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Duration;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsFunctions;
//import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.DDate;
//import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.DDateTime;
import fi.hut.tml.xsmiles.Log;
import org.apache.xpath.*;
//import org.apache.xpath.XPathExpressionContext;
import org.apache.xpath.objects.*;

import org.apache.xalan.extensions.ExpressionContext;
import org.apache.xml.dtm.DTM;
import org.apache.xml.dtm.DTMIterator;

import org.apache.xml.utils.XMLString;

//import org.apache.xerces.impl.dv.xs.DateDV;


import javax.xml.transform.*;


import java.util.*;
import java.math.BigDecimal;
import java.io.IOException;


import org.w3c.dom.*;

public class XFormsXPathExtensionHandler
{
    static long seventiesMillis;
    static 
    {
        	Calendar seventies = XFormsConfiguration.getInstance().getCalendarFromSchemaString("1970-01-01T00:00:00Z");
            seventiesMillis = seventies.getTime().getTime();
     }

	public XFormsXPathExtensionHandler()
	{
	}

	public  static Object test(
		ExpressionContext exprContext, NodeList nodeset, Node callerNode) throws TransformerException
	{
		Log.debug("test called!" +"callerNode: "+callerNode);
		return null;
	}
	/** xforms:cursor() */
	public  static XObject cursor(
		String param, XFormsContext handler) throws TransformerException
	{
		int repeatCursor = ((handler==null) ? 0 :handler.getCursor(param));
		return new XNumber(new Integer(repeatCursor));
	}
    
    /** xforms:nodeindex() */
    public  static XObject nodeindex(XPathContext xpathContext,
        String param, XFormsContext handler, ModelContext modelContext) throws TransformerException
    {
        Node nodeindex = ((handler==null) ? null :handler.getNodeIndex(param));
        if (nodeindex==null) nodeindex=modelContext.getInstanceDocument(null);

        return new XNodeSetForDOM(nodeindex,xpathContext.getDTMManager());            

    }
    
    /** xforms:instance() */
    public static XNodeSet instance(XPathContext xpathContext,String param, ModelContext modelContext)
    {
        //XPathExpressionContext = xpathContext.
        if (modelContext==null)
        {
            Log.error("XFormsXPathExtensionHandler.nstance: modelContext == null");
            return new XNodeSet(xpathContext.getDTMManager());
        }
        if (param==null||param.length()<1) return new XNodeSet(xpathContext.getDTMManager());
        else
        {
            Document doc = modelContext.getInstanceDocument(param);
            if (doc==null) return new XNodeSet(xpathContext.getDTMManager());
            else
            {
                Element documentElem = doc.getDocumentElement();
                if (documentElem==null) return new XNodeSet(xpathContext.getDTMManager());
                return new XNodeSetForDOM(documentElem,xpathContext.getDTMManager());            
            }
        }
    }
	/** xforms:boolean-from-string */
	public  static XBoolean boolean_from_string(
		String param) throws TransformerException
	{
		return (param.equalsIgnoreCase("true") ? XBoolean.S_TRUE : XBoolean.S_FALSE);
	}
	/** xforms:property */
	public  static XString property(
		String param) throws TransformerException
	{
		if (param.equals("version")) return new XString("1.0");
		if (param.equals("conformance-level")) return new XString(XFormsConfiguration.getInstance().getConformanceLevel());
		return null;
	}
	
	/** xforms:now */
	public  static XString now() throws TransformerException
	{
		return new XString(XFormsFunctions.now());
	}
        private static final long millisInDay = (long)(1000l*60l*60l*24l);

	/** xforms:days-from-date() */
	public  static XNumber daysFromDate(XPathContext xctxt,Expression exp) throws TransformerException
	{
			// execute the  expression
            try
            {
                XObject val=exp.execute(xctxt);
                String durationStr = val.toString();
                Number days = XFormsFunctions.daysFromDate(durationStr);
                return new XNumber(days);
            } catch (Exception e)
            {
                return new XNumber(Double.NaN);
            }
            //return new XNumber(duration.getSeconds());
	}    
	/** xforms:days-from-date() */
	public  static XNumber secondsFromDateTime(XPathContext xctxt,Expression exp) throws TransformerException
	{
			// execute the  expression
            try
            {
                XObject val=exp.execute(xctxt);
                String durationStr = val.toString();
                Number res = XFormsFunctions.secondsFromDateTime(durationStr);
                return new XNumber(res);
            } catch (Exception e)
            {
                return new XNumber(Double.NaN);
            }
            //return new XNumber(duration.getSeconds());
	}    

    
    /** xforms:seconds */
	public  static XNumber seconds(XPathContext xctxt,Expression exp) throws TransformerException
	{
			// execute the  expression
			XObject val=exp.execute(xctxt);
            String durationStr = val.toString();
            try
            {
                Number res = XFormsFunctions.seconds(durationStr);
                return new XNumber(res);
            } catch (Exception e)
            {
                throw new TransformerException(e.toString());
            }
	}  	
    /** xforms:seconds */
	public  static XNumber months(XPathContext xctxt,Expression exp) throws TransformerException
	{
			// execute the  expression
			XObject val=exp.execute(xctxt);
            String durationStr = val.toString();
            try
            {
                Number res = XFormsFunctions.months(durationStr);
                return new XNumber(res);
            } catch (Exception e)
            {
                throw new TransformerException(e.toString());
            }
	}    
    
    /** xforms:if */
	public  static XObject xif(XPathContext xctxt,Expression test, Expression exp_true, Expression exp_false) throws TransformerException
	{
		try
		{
			int pos,count=0;
			XMLString s;
			Expression retexp;
			
			// execute the test expression
			XObject testeval = test.execute(xctxt);
			if (!(testeval instanceof XBoolean)) return null;
			boolean testbool = ((XBoolean)testeval).bool();
			
			// Excecute both expressions for evalations sake
			XObject trueval=exp_true.execute(xctxt);
			XObject falseval=exp_false.execute(xctxt);
			
			if (testbool) return trueval;
			else return falseval;
		} catch (Exception e)
		{
			Log.error(e);
		}
		return null;
	}



	/** xforms:min */
	public static XObject min(XPathContext xctxt, Expression m_arg0) throws javax.xml.transform.TransformerException
	{

		DTMIterator nodes = m_arg0.asIterator(xctxt, xctxt.getCurrentNode());
		double min = Double.MAX_VALUE	;
		int pos;

		while (DTM.NULL != (pos = nodes.nextNode()))
		{
			DTM dtm = nodes.getDTM(pos);
			XMLString s = dtm.getStringValue(pos);

			if (null != s)
			{
				double sval=s.toDouble();
				if (sval<min)
					min=sval;
			}	
		}
		nodes.detach();

		return new XNumber(min);
	}

	/** xforms:max */
	public static XObject max(XPathContext xctxt, Expression m_arg0) throws javax.xml.transform.TransformerException
	{

		DTMIterator nodes = m_arg0.asIterator(xctxt, xctxt.getCurrentNode());
		double max = Double.MIN_VALUE	;
		int pos;

		while (DTM.NULL != (pos = nodes.nextNode()))
		{
			DTM dtm = nodes.getDTM(pos);
			XMLString s = dtm.getStringValue(pos);

			if (null != s)
			{
				double sval=s.toDouble();
				if (sval>max)
					max=sval;
			}	
		}
		nodes.detach();

		return new XNumber(max);
	}

	/** xforms:count-non-empty */
	public static XObject count_non_empty(XPathContext xctxt, Expression m_arg0) throws javax.xml.transform.TransformerException
	{

		DTMIterator nodes = m_arg0.asIterator(xctxt, xctxt.getCurrentNode());
		int pos,count=0;

		while (DTM.NULL != (pos = nodes.nextNode()))
		{
			DTM dtm = nodes.getDTM(pos);
			Node node = dtm.getNode(pos);
			// check whether this node is a leaf node, and whether it contains text
			if (node.getNodeType() == Node.ATTRIBUTE_NODE && node.getNodeValue().length()>0)
			{
				count++;
			}
			else if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				int textLen = 0;
				Node child = node.getFirstChild();
				boolean hasElementChildren = false;
				while (child!=null)
				{
					if (child.getNodeType() == Node.ELEMENT_NODE)
					{
						hasElementChildren=true;
						break;
					}
					else if (child.getNodeType()==Node.TEXT_NODE)
					{
						textLen=textLen+child.getNodeValue().length();
					}
					child = child.getNextSibling();
				}
				if (hasElementChildren==false && textLen>0)
				{
					count++;
				}
			}
			/*
			XMLString s = dtm.getStringValue(pos);

			if (null != s && s.length()>0)
			{
				count++;
			}
			 */	
		}
		nodes.detach();
		Log.debug("count_non_empty returns : "+count);
		return new XNumber(count);
	}
	/** xforms:avg function */
	public static XObject avg(XPathContext xctxt, Expression m_arg0) throws javax.xml.transform.TransformerException
	{

		DTMIterator nodes = m_arg0.asIterator(xctxt, xctxt.getCurrentNode());
		double sum = 0.0;
		int pos,count=0;

		while (DTM.NULL != (pos = nodes.nextNode()))
		{
			count++;
			DTM dtm = nodes.getDTM(pos);
			XMLString s = dtm.getStringValue(pos);

			if (null != s)
				sum += s.toDouble();
		}
		nodes.detach();

		return new XNumber(sum/((double)count));
	}


}
        


