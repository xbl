/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 6, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.event.GUIEventAdapter;
import fi.hut.tml.xsmiles.gui.XSmilesDialog;


/**
 * This class is launched by the browser menu command.
 * It will listen for document changes in the browser window and if
 * XForms documents are loaded, it will start to present the form using the DialogUI.
 * 
 * The operation is as follows:
 * 	The focus is always in some XForms control or group. The options are
 * then all the active things that can be done with that group/control 
 * 
 * @author honkkis
 *
 */
public class DynamicDialogHandler extends GUIEventAdapter implements XSmilesDialog, ActionListener
{
    protected DialogUI myUI;
    protected BrowserWindow browser;
    protected static DynamicDialogHandler previous;
    
    /** the element which currently has the focus */
    protected FocusHandler focusHandler;
    //protected DOMTraverser traverser;
    
    public DynamicDialogHandler()
    {
    }
    
    
    
    
    
    public synchronized static void setGlobalObject(DynamicDialogHandler h)
    {
        if (h!=previous)
        {
            if (previous!=null) previous.destroy();
            previous=h;
        }
    }
    public void setBrowserWindow(BrowserWindow b)
    {
        this.removeListeners();
        this.browser=b;
        this.createListeners();
        Log.debug("DialogHandler: browser or doc changed");
        setGlobalObject(this);
        if (this.myUI==null)
        {
            this.createUI();
        }
        this.createFocusHandler();
    }
    public void start()
    {
    }
    
    private void createFocusHandler()
    {
        Document doc = this.getCurrentDocument();
        if (this.focusHandler!=null) this.focusHandler.destroy();
        this.focusHandler=new DynamicFocusHandler(doc,this.getUI(),this);
    }
    
    protected Document getCurrentDocument()
    {
        try
        {
            return this.browser.getXMLDocument().getDocument();
        } catch (NullPointerException e)
        {
            return null;
        }
    }
    
    public DialogUI getUI()
    {
        return this.myUI;
    }
    protected void createUI()
    {
        myUI=DialogGUIDebug.getInstance(this.browser.getComponentFactory());
        myUI.addActionListener(this);
    }
    protected void removeListeners()
    {
        try
        {
            this.browser.getCurrentGUI().removeGUIEventListener(this);
        }
        catch (Exception e)
        {
            Log.error(e.toString());
        }
    }
    protected void createListeners()
    {
        //this.browser.getCurrentGUI().addGUIEventListener(this);
    }
    public void destroy()
    {
        this.removeListeners();
        this.myUI.destroy();
        this.focusHandler.destroy();
    }
    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent arg0)
    {
    	
    	if (arg0 instanceof DialogActionEvent)
    	{
            String command=arg0.getActionCommand();
            if (command.equals("installAll"))
            {
            	this.getBrowserWindow().newBrowserWindow(new XLink("http://www.xsmiles.org/speech_installation.html"));
            }
    		
    	}
        /*
        String command=arg0.getActionCommand();
        Log.debug("Got command:"+command);
        Response r = new Response();
        r.response=command.trim();
        */
        //this.traverser.interpretResponse(r);
    }





    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.XSmilesDialog#getBrowserWindow()
     */
    public BrowserWindow getBrowserWindow()
    {
        // TODO Auto-generated method stub
        return this.browser;
    }

    /*
    public void browserReady()
    {
        XMLDocument newdoc =this.getBrowserWindow().getXMLDocument();  
        if (newdoc!=null&&this.doc!=newdoc.getDocument())
        {
            this.doc=newdoc.getDocument();
            this.focusHandler=new DynamicFocusHandler(doc,this.getUI(),this);
            super.browserReady();
        }
            
    }
    */
}
