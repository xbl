/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PSVI;
//import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.*;


// Element implementation 
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;


import org.apache.xerces.xni.psvi.*;
import org.apache.xerces.impl.xs.psvi.*;
import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.*;

/**
 * This class contains the PSVI related functions. 
 * The idea is to separate them as much as possible to a single class.
 * Note that the underlying DOM has to support PSVI 
 */
public class PSVIImpl implements PSVI{
	/* PSVI type methods */
    public static final String URI_SCHEMAFORSCHEMA = "http://www.w3.org/2001/XMLSchema";
	
	/**
	 * This method digs the underlying primitive type, no matter if the
	 * type is defined as a restriction to a complex type or just as type="xs:xxx"
	 **/
	public int getPrimitiveTypeId(Node node)
	{
		XSSimpleType simpletype = getSimpleType(node);
		if (simpletype==null) 
		{
			//Log.error("Could not read the type of : "+node.getNodeName());
			return -1;
		}
        return getPrimitiveTypeIdInternal(simpletype);
    }
    public int getPrimitiveTypeIdInternal(XSSimpleType simpletype)
    {
        if (simpletype==null) return -1;

        // we want sometimes some other than the primitive type
        // xsd:integer is not primitive type
        XSTypeDefinition subtype = simpletype.getBaseType();
        while (subtype!=null)
        {
            String name = subtype.getName();
            if (name!=null&&name.equals("integer"))
            {
                String uri = subtype.getNamespace();
                if (uri.equals(URI_SCHEMAFORSCHEMA))
                {
                    return XFormsConfiguration.getInstance().getDataFactory().XSMILES_INTEGER;
                }
            }
            //System.out.println("subtype : "+subtype+subtype.getName());
            subtype = subtype.getBaseType();
        }

        
        XSSimpleTypeDefinition primitive = simpletype.getPrimitiveType();
		if (!(primitive instanceof XSSimpleType))
		{
			System.out.println("Wasn't instanceof XSSimpletype");
			return -1;
		}
		XSSimpleType primitiveType = (XSSimpleType)primitive;
		short id = primitiveType.getPrimitiveKind();
		//Log.debug("Primitive type: "+id);
		return id;
		//return "xxx";
	}

	public XSSimpleType getSimpleType(Node node)
	{
		if (!(node instanceof ItemPSVI))
		{
			Log.error("XForms instance error: PSVI not supported by underlying DOM");
			return null;
		}
		ItemPSVI elem = (ItemPSVI)node;
		if (elem.getValidationAttempted()==ItemPSVI.VALIDATION_NONE)
		{
			//Log.error("Validation for node: "+node+" has not been done!");
			return null;
		}
		XSTypeDefinition typedef = elem.getTypeDefinition();
		//XSTypeDefinition basetypedef = typedef.getBaseType();
		//System.out.println("typedef: "+typedef+" name:"+typedef.getName());
		if (typedef==null)
		{
			//Log.error("Could not read the type of : "+node.getNodeName());
			return null;
		}
		short type = typedef.getTypeCategory();
		if (type==XSTypeDefinition.COMPLEX_TYPE)
		{
			XSComplexTypeDefinition complex = (XSComplexTypeDefinition)typedef;
			if (complex.getContentType()==XSComplexTypeDefinition.CONTENTTYPE_SIMPLE)
			{
				typedef=complex.getSimpleType();
			}
			else
			{
				return null;
			}
		}
		XSSimpleType simpletype = (XSSimpleType)typedef;
		return simpletype;
	}
	
	private Object validateString(Node elem, String s) throws InvalidDatatypeValueException 
	{
		XSSimpleType type = getSimpleType(elem);
		if (type==null) 
		{
			//Log.error("validateString: type was null");
			return null;
		}
		Object o = type.validate(s,null,null);
		//System.out.println("returned: "+o);
		return o;
	}
	public Object validateElement(InstanceNode elem) throws InvalidDatatypeValueException
	{
		String value = XFormsUtil.getText(elem);
		return validateString(elem,value);
	}

}

