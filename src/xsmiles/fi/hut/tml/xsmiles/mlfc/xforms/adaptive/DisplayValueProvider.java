package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

public interface DisplayValueProvider {
	public String getDisplayValue();
}
