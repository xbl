/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.UIEventImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Node;
import org.w3c.dom.events.Event;
import org.w3c.dom.DOMException;

import java.util.Hashtable;

/**
 * The factory for creating DOM event instances
 */
public class XFormsEventFactory extends EventFactory implements XFormsConstants{

  
  public XFormsEventFactory() {
  }
  
  /** XForms event prototypes */
  static Hashtable events;
  static
  {
  	// create event prototypes
	// TODO: check that bubbling / cancelling is ok since the new events roundup finishes
  	events=new Hashtable(30);
	// INITIALIZATION EVENTS name,bubbles, cancelable, related info
	events.put(MODEL_CONSTRUCT_EVENT,  createXFormsEvent(MODEL_CONSTRUCT_EVENT,true,false,null));
	events.put(MODEL_DESTRUCT_EVENT,  createXFormsEvent(MODEL_DESTRUCT_EVENT,true,false,null));
	events.put(MODEL_CONSTRUCT_DONE_EVENT, createXFormsEvent(MODEL_CONSTRUCT_DONE_EVENT,true,false,null));
	events.put(READY_EVENT,  createXFormsEvent(READY_EVENT,true,false,null));
	events.put(UI_INITIALIZE_EVENT,    createXFormsEvent(UI_INITIALIZE_EVENT,false,false,null));
  	// no longer supported events.put(FORM_CONTROL_INITIALIZE_EVENT,    createXFormsEvent(FORM_CONTROL_INITIALIZE_EVENT,false,false,null));
  	// INTERACTION EVENTS
  	events.put(NEXT_EVENT,			createXFormsEvent(NEXT_EVENT,false,true,null));
  	events.put(PREVIOUS_EVENT, 		createXFormsEvent(PREVIOUS_EVENT,false,true,null));
  	events.put(FOCUS_EVENT, 		createXFormsEvent(FOCUS_EVENT,false,true,null));
  	events.put(BLUR_EVENT, 			createXFormsEvent(BLUR_EVENT,false,true,null));
  	events.put(ACTIVATE_EVENT, 		createXFormsEvent(ACTIVATE_EVENT,true,true,null));
  	events.put(VALUE_CHANGING_EVENT,createXFormsEvent(VALUE_CHANGING_EVENT,true,true,null));
  	events.put(VALUE_CHANGED_EVENT, createXFormsEvent(VALUE_CHANGED_EVENT,true,true,null));
  	events.put(SCROLL_FIRST_EVENT, 	createXFormsEvent(SCROLL_FIRST_EVENT,true,true,null));
  	events.put(SCROLL_LAST_EVENT, 	createXFormsEvent(SCROLL_LAST_EVENT,true,true,null));
  	events.put(INSERT_EVENT, 		createXFormsEvent(INSERT_EVENT,true,true,null));
  	events.put(DELETE_EVENT, 		createXFormsEvent(DELETE_EVENT,true,true,null));
  	events.put(SELECT_EVENT, 		createXFormsEvent(SELECT_EVENT,true,true,null));
  	events.put(DESELECT_EVENT, 		createXFormsEvent(DESELECT_EVENT,true,true,null));
  	events.put(HELP_EVENT, 			createXFormsEvent(HELP_EVENT,true,true,null));
  	events.put(HINT_EVENT, 			createXFormsEvent(HINT_EVENT,true,true,null));
  	events.put(ALERT_EVENT, 		createXFormsEvent(ALERT_EVENT,false,true,null));
  	events.put(REQUIRED_EVENT, 		createXFormsEvent(REQUIRED_EVENT,true,false,null));
  	events.put(OPTIONAL_EVENT, 		createXFormsEvent(OPTIONAL_EVENT,true,false,null));
  	events.put(VALID_EVENT, 		createXFormsEvent(VALID_EVENT,true,false,null));
  	events.put(INVALID_EVENT, 		createXFormsEvent(INVALID_EVENT,true,false,null));
  	events.put(READONLY_EVENT, 		createXFormsEvent(READONLY_EVENT,true,false,null));
  	events.put(READWRITE_EVENT, 		createXFormsEvent(READWRITE_EVENT,true,false,null));
  	events.put(ENABLED_EVENT, 		createXFormsEvent(ENABLED_EVENT,true,false,null));
  	events.put(DISABLED_EVENT, 		createXFormsEvent(DISABLED_EVENT,true,false,null));
    events.put(INRANGE_EVENT,      createXFormsEvent(INRANGE_EVENT,true,false,null));
    events.put(OUTOFRANGE_EVENT,      createXFormsEvent(OUTOFRANGE_EVENT,true,false,null));
  	events.put(REFRESH_EVENT, 		createXFormsEvent(REFRESH_EVENT,true,true,null));
  	events.put(REVALIDATE_EVENT, 	createXFormsEvent(REVALIDATE_EVENT,true,true,null));
  	events.put(REBUILD_EVENT, 	createXFormsEvent(REBUILD_EVENT,true,true,null));
  	events.put(RECALCULATE_EVENT, 	createXFormsEvent(RECALCULATE_EVENT,true,true,null));
  	events.put(RESET_EVENT, 		createXFormsEvent(RESET_EVENT,true,true,null));
  	events.put(SUBMIT_EVENT, 		createXFormsEvent(SUBMIT_EVENT,true,true,null));
    events.put(SUBMIT_STARTED_EVENT,        createXFormsEvent(SUBMIT_STARTED_EVENT,true,true,null));
  	events.put(SUBMIT_DONE_EVENT, 		createXFormsEvent(SUBMIT_DONE_EVENT,true,false,null));
  	events.put(SUBMIT_ERROR, 		createXFormsEvent(SUBMIT_ERROR,true,false,null));
  	events.put(BINDING_EXCEPTION, createXFormsEvent(BINDING_EXCEPTION,true,false,null));
  	events.put(SCHEMA_CONSTRAINTS_ERROR, createXFormsEvent(SCHEMA_CONSTRAINTS_ERROR,true,true,null));
  	events.put(TRAVERSAL_ERROR, 	createXFormsEvent(TRAVERSAL_ERROR,true,true,null));
  	events.put(LINK_EXCEPTION, 	createXFormsEvent(LINK_EXCEPTION,true,false,null));
    events.put(COMPUTE_EXCEPTION,  createXFormsEvent(COMPUTE_EXCEPTION,true,false,null));
  	events.put(LINK_ERROR, 	createXFormsEvent(LINK_ERROR,true,false,null));
  	events.put(INVALID_DATATYPE_ERROR, createXFormsEvent(INVALID_DATATYPE_ERROR,true,true,null));
	// implementation internal events
  	events.put(SUBTREE_MODIFY_END, createXFormsEvent(SUBTREE_MODIFY_END,true,true,null));
  	events.put(SUBTREE_MODIFY_START, createXFormsEvent(SUBTREE_MODIFY_START,true,true,null));
  	events.put(CAPTION_CHANGED, createXFormsEvent(CAPTION_CHANGED,true,true,null));
    
  	// DOM events prototypes
    events.put(FOCUSIN_NOTIFICATION_EVENT, EventFactory.createEvent(FOCUSIN_NOTIFICATION_EVENT));
    events.put(FOCUSOUT_NOTIFICATION_EVENT, EventFactory.createEvent(FOCUSOUT_NOTIFICATION_EVENT));
    events.put(DOMACTIVATE_EVENT, EventFactory.createEvent(DOMACTIVATE_EVENT));
  }

  public static Event createEvent(String eventType) throws DOMException {
    Event theEvent = null;
	

    if (eventType.startsWith("xforms-"))
    {
        theEvent = createXFormsEvent(eventType);
        if (theEvent==null) theEvent =  EventFactory.createEvent(eventType);
    }
    else if (eventType.startsWith("xsmiles:")) 
		return new XFormsEventImpl();
    else {
		// Use the main event factory
		theEvent = EventFactory.createEvent(eventType);
    }
    return theEvent;
  }

  public static Event createXFormsEvent(String typeArg)
  {
   Event ev=null;
  	try
  	{
	  // first check, whether we have a prototype ready
	  ev = (Event)events.get(typeArg);
	  if (ev==null) 
	  {
	  	Log.debug("Could not find event prototype for: "+typeArg);
		return null;
	  }
      if (ev instanceof XFormsEventImpl)
    	  return (Event)((XFormsEventImpl)ev).clone();
      if (ev instanceof UIEventImpl)
    	  return (Event)((UIEventImpl)ev).clone();
  	} catch (CloneNotSupportedException e)
  	{
        if (ev instanceof XFormsEventImpl)
    		return createXFormsEvent(typeArg,ev.getBubbles(),ev.getCancelable(), ((XFormsEventImpl)ev).getRelatedNode());
        else	return createXFormsEvent(typeArg,ev.getBubbles(),ev.getCancelable(), null);
		//Log.error(e);
		//return null;
  	}
   return null;
  }

  public static XFormsEventImpl createXFormsEvent(String typeArg, boolean canBubbleArg,
                              boolean cancelableArg, Node relatedNodeArg)
	{
		XFormsEventImpl theEvent = new XFormsEventImpl();
		theEvent.initXFormsEvent(typeArg,canBubbleArg,cancelableArg,relatedNodeArg);
		return theEvent;
	
	}

} // EventFactory
