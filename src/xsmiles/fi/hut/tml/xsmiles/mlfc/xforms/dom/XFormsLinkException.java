/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.MLFCException;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.Node;


/**
 * XForms binding exception
 * @author Mikko Honkala
 */
public class XFormsLinkException extends XFormsException {
	public XFormsLinkException(String reason, String resource) 
    {
        super("Failed to read resource: "+resource+".\nReason: "+reason);
    }
        public XFormsLinkException(String reason)
    {
        super(reason);
        throw new RuntimeException("Never call this method: 1001");
    }

}
