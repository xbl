/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dominterface;

import java.util.Vector;


/**
 * @author honkkis
 *
 */
public interface SelectOneElement extends SelectElement
{
    public SelectionItem getCurrentSelection();

}
