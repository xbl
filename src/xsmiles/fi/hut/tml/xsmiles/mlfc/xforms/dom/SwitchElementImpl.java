/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.dom.FlowLayoutElement;

import java.util.Vector;

import org.w3c.dom.*;
import org.w3c.dom.events.*;


/**
 * The switch element.
 * @author Mikko Honkala
 */


public class SwitchElementImpl extends XFormsElementImpl implements EventListener,FlowLayoutElement{

	/** currently active case */
	CaseElementImpl activeCase;
	
	/** The position of original switch */
	Element parent;
	Node position;
	

	/** Vector holding the currently active added nodes in the DOM */
	Vector addedNodes=new Vector();
	
	public SwitchElementImpl(XFormsElementHandler owner, String ns, String name)
	{
		super(owner, ns, name);
	}

	/** 
	 */
	public void init()
	{
	
		// create the event listener
		this.addEventListener(XFormsEventFactory.SELECT_EVENT, this, true);
		// remove the switch from DOM
		//position = this.getNextSibling();
		//parent = (Element)this.getParentNode();
		//parent.removeChild(this);
		// go through children and set the default case on 
		Node child = this.getFirstChild();
		CaseElementImpl firstCase=null;
		while (child!=null)
		{
			if (child instanceof CaseElementImpl)
			{
				CaseElementImpl caseElem = 
					(CaseElementImpl)child;
				if (firstCase == null) firstCase=caseElem;
				String selected=caseElem.getAttribute("selected");
				if (selected.equalsIgnoreCase("true"))
				{
					this.toggle(caseElem,true);
					break;
				}
				
			}
			child=child.getNextSibling();
			
		}
		if (activeCase==null) this.toggle(firstCase,true);
		super.init();
	}

        

	
	/**
	 * This method handles the toggle event
	 */
	public void handleEvent(Event evt)
	{
		Node target = (Node)evt.getTarget();
                // the latter test (parentNode) is there because
                // we don't want nested switches to get these events from others 
                // than its own cases -MH
		if (target instanceof CaseElementImpl&&target.getParentNode()==this)
		{
			CaseElementImpl caseElem = (CaseElementImpl)target;
			Log.debug("Switch !!GOT EVENT!!"+evt+"Target: "+caseElem.getId());
			this.toggle(caseElem,true);
			
		}
	}
	
	public void toggle(CaseElementImpl caseElem,boolean refresh)
	{
		if (caseElem==activeCase) return;
		if (activeCase!=null)
		{
			// DISPATCH XForms EVENT
                        activeCase.setActive(false);
			activeCase.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.DESELECT_EVENT));
			activeCase=null;
		}
		// add the case to DOM
                caseElem.setActive(true);
		this.activeCase=caseElem;
		// DISPATCH XForms EVENT
		caseElem.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.SELECT_EVENT));
	}
	
}


