/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.*;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

/**
 * Hint element
 * @author Mikko Honkala
 */

public class HintElementImpl extends DynBoundElementImpl
{
    
    public HintElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        bindingAttributesRequired=false;
    }
    
    public void init()
    {
        try
        {
            this.createBinding();
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
        }
        
        super.init();
    }
    
    public String getHintText()
    {
        try
        {
            return this.getTextWithPrecedence();
        } catch (XFormsLinkException e)
        {
            Log.error(e);
            return "";
        }
    }
    public String getMessageTypeAsString()
    {
        // TODO Auto-generated method stub
        return "Help";
    }

    
}
