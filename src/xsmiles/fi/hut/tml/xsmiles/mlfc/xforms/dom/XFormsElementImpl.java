/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.VisualElement;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;

import fi.hut.tml.xsmiles.content.XSmilesContentInitializeException;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.data.XFormsDatatypeException;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.mlfc.general.Helper;

import java.net.URL;
import java.net.URLConnection;

import java.io.InputStream;
import java.io.ByteArrayOutputStream;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Element;
import org.w3c.dom.events.*;

import org.w3c.dom.css.*;




import org.apache.xerces.dom.events.EventImpl;


import org.w3c.dom.xforms10.XFormsElement;


//import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.DocumentImpl;

import java.util.Vector;
import java.io.StringWriter;


import fi.hut.tml.xsmiles.dom.XSmilesStyle;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.dom.XSmilesAttrNSImpl;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

import org.w3c.dom.traversal.*;


/**
 * The superclass of all XForms elements
 * @author Mikko Honkala
 */

public class XFormsElementImpl extends  VisualElementImpl
implements XFormsElement, XFormsConstants
{
    
    
    // The ExtendedDocument that this element belongs to
    protected ExtendedDocument ownerDoc;
    protected XFormsElementHandler handler;
    protected boolean inited=false;

    
    /** whether to debug messages for each event dispatch */
    public boolean debugEvents=false;
    
    public XFormsElementImpl(XFormsElementHandler handler, String ns, String name)
    {
        super(handler.getDocumentImpl(), ns, name);
        setHandler(handler);
        setOwnerDoc(handler.getExtendedDocument());
    }
    
    /** 
     * 
     * @return true if the owner document is an XML+CSS document
     */
    public boolean isCSSLayoutDoc()
    {
        return (this.getOwnerDocument().getDocumentElement() instanceof VisualElement);
    }
    
    
    /** Support the style attribute in XForms */
    public String getStyleAttrValue()
    {
        return getAttribute("style");
    }
    
    
    public void init()
    {
        //this.initStyle(this.getHandler().getStyleSheet());
        super.init();
        this.inited=true;
        
    }
    
        /*
        public void initStyle(XSmilesStyleSheet styleSheet)
        {
                if (styleSheet==null) return;
                // TODO: remove this test, after moving to the new CSS implementation
                this.setStyle(styleSheet.getParsedStyle(this));
//		Log.debug("InitStyle: "+this+": "+style);
        }*/
    protected void setOwnerDoc(ExtendedDocument owner)
    {
        this.ownerDoc=owner;
    }
    protected void setHandler(XFormsElementHandler a_handler)
    {
        this.handler=a_handler;
    }
    
    XFormsElementHandler getHandler()
    {
        return this.handler;
    }
    
    public XMLDocument getXMLDocument()
    {
        return handler.getXMLDocument();
    }
    public ComponentFactory getComponentFactory()
    {
        return handler.getComponentFactory();
    }
    public BrowserWindow getBrowser()
    {
        return handler.getBrowser();
    }
    public ModelElementImpl getModel(String id)
    {
        return handler.getModel(id);
    }
    
    /** starts with an element and goes down the tree to find a scoped
     * model. If none is found, then the default model is used
     */
    public ModelElementImpl getModelInScope(Element elem)
    {
        for (Node n = elem;(n!=null&&n.getNodeType()==Node.ELEMENT_NODE);n=n.getParentNode())
        {
            if (n instanceof ElementWithContext)
            {
                ModelElementImpl model = ((ElementWithContext)n).getModel();
                if (model!=null) return model;
            }
        }
        return this.getModel();
    }
    
    
    private Element findFirstChild(String name)
    {
        NodeList elems=this.getChildNodes();
        for (int i=0;i<elems.getLength();i++)
        {
            Node elem=elems.item(i);
            if (elem.getNodeType()==Node.ELEMENT_NODE)
            {
                if (elem.getNodeName().equals(name)) return (Element)elem;
            }
        }
        return null;
    }
    
    public String getId()
    {
        return getAttribute("id");
    }
    public void setId(String id) throws DOMException
    {
        // TODO: setId
    }
    
    /**
     *  The class attribute. This method should be called getClass, but it is
     * already reserved by java.lang.Object.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly.
     */
    public String getClassName()
    {
        return this.getAttribute("class");
    }
    public void setClassName(String cl) throws DOMException
    {
        //TODO: setClassName
    }
    
    
    /**
     * Dispatch the event to DOM for this element.
     */
    protected boolean dispatch(String type)
    {
        // Dispatch the event
        //		Log.debug("Event '"+type+"' dispatching...");
        org.w3c.dom.events.Event evt;
        if (type.startsWith("xforms-"))
        {
            XFormsEventImpl event = (XFormsEventImpl)XFormsEventFactory.createEvent(type);
            event.initXFormsEvent(type,true,true,this);
            evt=event;
        }
        else
        {
            evt = new EventImpl(); //createEvent();
            evt.initEvent(type, true, true);
        }
        //		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
        return this.dispatch(evt);
    }
    
    protected static final String EVENTS_NS = "http://www.w3.org/2001/xml-events"; 
    
    /**
     * this is a kludge method to initialize child XML events listeners,
     * because XForms will dispatch events at the intialization phase
     * so this method will provide at least basic functionality while
     * still preserving "one-pass" initialization
     */
    protected void initializeXMLEvents(Element root)
    {
        // TODO elements
        NodeIterator iter=
        ((DocumentTraversal)root.getOwnerDocument()).createNodeIterator(
        root, NodeFilter.SHOW_ELEMENT|NodeFilter.SHOW_ATTRIBUTE, null,true); //NodeFilter.SHOW_ELEMENT|NodeFilter.SHOW_ATTRIBUTE
        for (Node n = iter.nextNode(); n!=null; n = iter.nextNode())
        {
            if (n.getNodeType()==Node.ELEMENT_NODE)
            {
                NamedNodeMap attributes = n.getAttributes();
                for (int i=0;i<attributes.getLength();i++)
                {
                    Attr a = (Attr)attributes.item(i);
                    String nsURI = a.getNamespaceURI();
                    if (nsURI!=null&&nsURI.equals(EVENTS_NS))
                    {
                        if (a instanceof XSmilesAttrNSImpl) ((XSmilesAttrNSImpl)a).init();
                    }
                }
            }
        }
    }
    
    /**
     * Dispatch the event to DOM for this element.
     */
    protected boolean dispatch(Event evt)
    {
        // Dispatch the event
        return ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    }
    
    /**
     * This method is overridden so that we could do event debugging.
     */
    public boolean dispatchEvent(Event event)
    {
        if (this.elementStatus==UNINITIALIZED) this.initializeXMLEvents(this);
        // simply forward to super class
        if (this.handler.getDebugEvents() && event instanceof XFormsEventImpl) Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
        else if (debugEvents) Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
        return super.dispatchEvent(event);
    }
    
    public static void debugNode(Node n)
    {
        try
        {
            fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(
            new java.io.OutputStreamWriter(System.out),
            n,true,true,false);
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    public static String serializeNode(Node n)
    {
        try
        {
            StringWriter sw= new StringWriter();
            fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(
            sw,
            n,true,true,false);
            return sw.toString();
        } catch (Exception e)
        {
            Log.error(e);
        }
        return "";
    }
    /**
     * Gets the default XForm element
     */
    public ModelElementImpl getModel()
    {
        String aid=this.getModelId();
        return getModel(aid);
    }
    /**
     * The id of the xform of the reference node
     */
    public String getModelId()
    {
        return this.getAttribute(MODEL_ID_ATTRIBUTE);
    }
    
    
    /**
     * move the children of 'parent' before the element 'insertBefore'
     * this is called by init
     */
    protected static void moveNodes(Element parent, Node insertBefore, Element from,Vector nodeVector)
    {
        Node n = from.getFirstChild();
        while (n!=null)
        {
            //			this.debugNode(n);
            Node next=n.getNextSibling();
            //this.removeChild(n);
            parent.insertBefore(n,insertBefore);
            // Save a pointer to the created node
            nodeVector.addElement(n);
            n=next;
        }
    }
    
    /** handle any XFormsException that's dispatched for this element */
    public void handleXFormsException(XFormsException e)
    {
        Log.error(e);
        if (e instanceof XFormsBindingException)
        {
            XFormsBindingException bindex = (XFormsBindingException)e;
            XFormsElementImpl elem = bindex.getXFormsElement();
            if (elem==null) elem=this;
            elem.dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.BINDING_EXCEPTION));
            throw new XSmilesContentInitializeException(e.toString());
        }
        else
        {
            throw new XSmilesContentInitializeException(e.toString());
        }
        
    }
    
    public void printEvent(org.w3c.dom.events.Event evt)
    {
        if(evt instanceof MutationEvent)
        {
            Log.debug("XForms dom event +"+((MutationEvent)evt).getType()+", new value:"+((MutationEvent)evt).getNewValue());
        }
    }
    
    public void dispatchActivateEvent()
    {
        dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.ACTIVATE_EVENT));
        dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.DOMACTIVATE_EVENT));
    }
    public void dispatchClickEvent()
    {
        dispatch("click");
    }
    
        /** retrieve the resource attribute string or null if not existing */
    protected String getSrc()
    {
        Attr srcAttr = this.getAttributeNode(SRC_ATTRIBUTE);
        if (srcAttr==null) return null;
        return srcAttr.getNodeValue();
    }
    
    /** retrieve a string fetched from this URL with GET */
    protected String retrieveResourceAsString(URL url) throws Exception
    {
            XSmilesConnection conn =  this.getHandler().openURLWithGET(url);
            InputStream is = conn.getInputStream();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            Helper.copyStream(is,os,500);
            return os.toString();
    }
    
    protected XFormsControl findFirstRelevantControl(Element start)
    {
        for (Node child=start.getFirstChild();child!=null;child=child.getNextSibling())
        {
            if (child instanceof XFormsControl) 
            {
                XFormsControl control = (XFormsControl)child;
                if (control.isVisible()) return control;
            }
            if (child.getNodeType()==Node.ELEMENT_NODE) 
            {
                XFormsControl control=this.findFirstRelevantControl((Element)child);
                if (control!=null) return control;
            }
        }
        return null;
    }
    
    public static void removeAllChildren(Element elem)
    {
        if (elem != null)
        {
            Node child = elem.getFirstChild();
            while (child != null)
            {
                Node next = child.getNextSibling();
                child.getParentNode().removeChild(child);
                child = next;
            }
        }
    }
    
    public static InstanceItem retrieveInstanceItem(Node n)
    {
        return ModelElementImpl.retrieveInstanceItem(n);
    }
    
    
}
