/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import fi.hut.tml.xsmiles.mlfc.xforms.dom.InstanceElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathEngine;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpression;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;




import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.*;
import javax.xml.transform.TransformerException;

import java.util.*;

   

/**
 * This object knows how to execute XPath expressions in the correct 
 * context
 * @author Mikko Honkala
 */


public interface ExpressionContainer {
    /** 
     * executes an arbitrary XPath expression 
     */
	//public XObject evalXPath(Node context,XPathExpression xpath, Node nsnode) throws TransformerException;
	//public Object evalXPath(Node context,XPathExpr xpath, Node nsnode, NodeList contextNodeList) throws Exception;
    public XPathEngine getXPathEngine();


    /** @return the context node for the XPath expression */
    public Node getContextNode();

    /** @return the namespace context node for the XPath expression */
    public Node getNamespaceContextNode();    
    
    /** @return the model context for this expression */
    public ModelContext getModelContext();
    
    /** @return the list of context nodes of this expression */
    public NodeList getContextNodeList();
    


    
}
