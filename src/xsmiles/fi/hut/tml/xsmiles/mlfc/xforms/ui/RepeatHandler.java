/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.ui;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.DynBoundElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.RepeatElementImpl;
import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.RefreshableService;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.ChildListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceParentNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;


import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Color;

import org.w3c.dom.*;

/**
 * The handler for repeats. This class handles "expanding" the UI dom depending on the
 * nodeset that the repeat is bound. This class does not handle the index (cursor in the UI).
 * @author Mikko Honkala
 */

public class RepeatHandler //implements ChildListener
{

    public final static String CSS_CURSOR_BACKGROUND_COLOR_PROPERTY = "cursor-background-color";
    public final static String CSS_CURSOR_COLOR_PROPERTY = "cursor-color";

    public final short REPEAT_UNINITIALIZED = 1;
    //public final short REPEAT_CLONED=10;
    public final short REPEAT_EXPANDED = 20;

    /** the current status of this repeat */
    protected short status = REPEAT_UNINITIALIZED;
    /** The prototype node */

    //protected Node instancePrototype;
    /** The prototype node's parent */
    //protected Node instancePrototypeParent;
    /** The prototype node's insert before */
    //protected Node instancePrototypeInsertBefore;

    /** The nodes, which this repeat has added to the DOM */
    //Vector addedNodes=new Vector(20);

    /** The parent node in the instance, that I will listen to for additions and deletions */
    protected InstanceNode repeatInstanceParent;

    /** The current repeat start index */
    protected int startIndex = -1;

    /** The number of children for a single repeat iteration */
    protected int repeatChildCount = -1;

    /** the repeat cursors color */
    protected Color cursorFGColor;

    /** the repeat cursors color */
    protected Color cursorBGColor;

    /** the currently bound nodes */
    protected NodeList refNodes;

    /** the owner element for this handler (e.g. the repeat element */
    protected Element ownerElement;

    /** the prototype for the UI nodes (cloned from the owner element, only children are used */
    protected Element prototypeUIElement;

    /** the id of this repeat */
    protected String id;

    /** the map between the instance nodes, and corresponding first node in the UI */
    protected Hashtable instanceTable;

    protected XFormsContext xformsContext;

    public RepeatHandler(XFormsContext context)
    {
        xformsContext = context;
        instanceTable = new Hashtable();
    }

    public void destroy()
    {
        Log.debug("RepeatHandler.destoy()");
        //this.unregisterChildListeners();
    }

    public void setUIElement(Element elem)
    {
        this.ownerElement = elem;
        this.prototypeUIElement = null;
        if (elem instanceof RepeatElementImpl)
        {
            this.id = ((RepeatElementImpl) elem).getId();
        }
        xformsContext.setCursor(id, 1);
    }

    public Element getUIElement()
    {
        return this.ownerElement;
    }
    public String getRepeatId()
    {
        return this.id;
    }

    protected void createPrototypeElement()
    {
        Element uiElem = this.getUIElement();
        if (prototypeUIElement == null && uiElem != null)
        {
            Element ui = (Element) uiElem.cloneNode(true);
            //XSmilesElementImpl.debugNode(ui,true);

            /*
            Element repeatItem=new RepeatItemPseudoElement(uiElem);
            Node child = ui.getFirstChild();
            while (child!=null)
            {
            	repeatItem.appendChild(child);
            	child=child.getNextSibling();
            }*/
            this.prototypeUIElement = ui;
        }
        this.repeatChildCount = 1; //this.countChildNodes(this.prototypeUIElement);
    }

    public int countChildNodes(Element parent)
    {
        Node child = parent.getFirstChild();
        int count = 0;
        while (child != null)
        {
            child = child.getNextSibling();
            count++;
        }
        return count;
    }

    /** re-inits this repeat's UI elements totally */
    public void expandUI()
    {
        this.createPrototypeElement();
        XFormsElementImpl.removeAllChildren(this.getUIElement());
        this.instanceTable.clear();
        NodeList bound = this.getBoundNodeset();
        if (bound != null)
        {
            for (int i = 0; i < bound.getLength(); i++)
            {
                this.createPresentationNodes(-1, bound.item(i), true);
            }
        }
        //XSmilesElementImpl.debugNode(this.getUIElement(),true);
    }

    /**
     */
    public NodeList getBoundNodeset() //throws XFormsBindingException
    {
        return this.refNodes;
    }
    public void createPresentationNodes(int cursor, Node context, boolean append)
    {
        Vector pseudoElements = ((PseudoElementContainerService) this.getUIElement()).getPseudoElements();
        //			TODO: clone node must clone also pseudoelements?
        //XSmilesElementImpl.debugNode(prototypeUIElement,true);
        Node clone = this.prototypeUIElement.cloneNode(true);
        Element pseudo = new RepeatItemPseudoElement(this.getUIElement());
        Node c = clone.getFirstChild();
        while (c != null)
        {
            Node nextc = c.getNextSibling();
            pseudo.appendChild(c);
            this.setContextNodeRecursively(c, context);
            c = nextc;
        }
        //XSmilesElementImpl.debugNode(pseudo,true);

        //if (first)
        {
            this.instanceTable.put(context, pseudo);
            //first=false;
        }
        if (append)
        {
            //this.getUIElement().appendChild(clone);
            pseudoElements.add(pseudo);
        } else
        {
        	pseudoElements.insertElementAt(pseudo,cursor);
        }
        // x-smiles element does not automatically call init for pseudoelements...
        ((XSmilesElementImpl) pseudo).init();
    }


    protected void setContextNodeRecursively(Node p, Node context)
    {
        if (p.getNodeType() == Node.ELEMENT_NODE)
        {
            if (p instanceof XFormsElementImpl)
            {
                // for nested repeats, we set the context node also.
                /*
                if (p instanceof RepeatElementImpl)
                {
                 
                    RepeatElementImpl repeat=(RepeatElementImpl)p;
                    this.descendantRepeats.addElement(repeat);
                    repeat.setParentRepeat(this);
                    repeat.setContextNode(currentContextNode);
                    // do not process nested repeats children
                    return;
                }*/
                if (p instanceof DynBoundElementImpl)
                {
                    // TODO: check if same model?
                    DynBoundElementImpl control = (DynBoundElementImpl) p;
                    control.setContextNode((InstanceNode) context);
                    return;
                }
            }
            Node child = p.getFirstChild();
            while (child != null)
            {
                if (child.getNodeType() == Node.ELEMENT_NODE)
                {
                    this.setContextNodeRecursively(child, context);
                }
                child = child.getNextSibling();
            }
        }

    }


    public static void removeAllChildrenTest(Element elem)
    {
        if (elem != null)
        {
            //elem = (Element)elem.getOwnerDocument().getDocumentElement().getElementsByTagNameNS("http://www.w3.org/1999/xhtml","p").item(0);
            //elem.removeChild(elem.getFirstChild());
            //Text text = elem.getOwnerDocument().createTextNode("empty");
            //elem.insertBefore(text,elem.getFirstChild());

            Node child = elem.getFirstChild();
            while (child != null)
            {
                //Log.debug("Removing child: "+child);
                elem.removeChild(child);
                child = elem.getFirstChild();
            }
        }
    }
	/** find a cursor value from a child node in UI DOM, used by selection events **/
	protected int getIndex(Node added)
	{
		// the node must be a pseudoelement in the pseudoelement vector
		Vector pseudoElements=((PseudoElementContainerService)this.getUIElement()).getPseudoElements();
		int ind = pseudoElements.indexOf(added);
		return ind+1;
	}


    public Vector getUINodesForIndex(int index)
    {
        NodeList children = this.getUIElement().getChildNodes();
        Vector indexNodeHolder = new Vector(10);
        int begindex = (index - 1) * this.repeatChildCount;
        int endindex = begindex + this.repeatChildCount - 1;
        for (int i = begindex; i <= endindex; i++)
        {
            indexNodeHolder.addElement(children.item(i));
        }
        return indexNodeHolder;
    }

    protected Vector current = new Vector();
    protected synchronized Node addRemoveSelectively(NodeList newList)
    {
        NodeList origList = refNodes;

        /** for later setting the index */
        Node addedNode = null;
        // for deleted nodes, go thru original list and delete all from UI that are not
        // found in newList
        // also, collect a vector of current list
        current.removeAllElements();
        if (origList != null)
        {
            for (int i = 0; i < origList.getLength(); i++)
            {
                Node n = origList.item(i);
                if (newList == null || !(hasNode(n, newList)))
                {
                    this.removeFromUI(n);
                } else
                    current.addElement(n);
            }
        }
        // for added nodes, go thru the new list, and add to the correct position if not found in the current list
        // NOTE: this expects the lists to be in the same order to work correctly!
        if (newList != null)
        {
            int cursorCurrent = 0;
            int cursorNew = 0;
            while (cursorNew < newList.getLength())
            {
                Node n = newList.item(cursorNew);
                if (!hasNode(n, current))
                {
                    Node after = null;
                    if (cursorCurrent > 0)
                    {
                        // find out the correct insert position using current cursor and the number of added nodes
                        Node afterInstanceNode = (Node) current.elementAt(cursorCurrent - 1);
                        Node uiStartNode = (Node) this.instanceTable.get(afterInstanceNode);
                        if (uiStartNode != null)
                            after = getNthSibling(uiStartNode, this.repeatChildCount - 1);
                    }
                    addedNode = n;
                    //this.createPresentationNodes(after, n, false);
					this.createPresentationNodes(cursorCurrent,n, false);
                    current.insertElementAt(n, cursorCurrent);
                    try
                    {
                        // for swing implementation
                        Thread.sleep(5);
                    } catch (Exception e)
                    {
                        Log.error(e);
                    }
                }
                cursorCurrent++;
                cursorNew++;
            }
        }
        return addedNode;
    }

    protected void setRepeatIndex(Node n)
    {
        int index = this.getIndexOfInstanceNode(n);
        Log.debug("Newly added node's index: " + index);
        if (index >= 0)
            xformsContext.setCursor(this.getRepeatId(), index + 1);
    }

    public static Node getNthSibling(Node start, int count)
    {
        while (start != null && count > 0)
        {
            start = start.getNextSibling();
            count--;
        }
        return start;
    }

    /** see, if a node is contained in a nodelist */
    protected boolean hasNode(Node n, NodeList list)
    {
        for (int i = 0; i < list.getLength(); i++)
        {
            if (list.item(i) == n)
                return true;
        }
        return false;
    }
    /** see, if a node is contained in a nodelist */
    protected boolean hasNode(Node n, Vector list)
    {
        for (int i = 0; i < list.size(); i++)
        {
            if (list.elementAt(i) == n)
                return true;
        }
        return false;
    }

	protected void removeFromUI(Node n)
	{
		Node uiNode = (Node) this.instanceTable.get(n);
		if (uiNode != null)
		{
			// it must be a psedo element
			Vector pseudoElements = ((PseudoElementContainerService)this.getUIElement()).getPseudoElements();
            PseudoElement pseudo = (PseudoElement)uiNode;
            pseudo.setParentNode(null);
			pseudoElements.remove(pseudo);
            
			((PseudoElementContainerService)this.getUIElement()).notifyPseudoRemoved((Element)uiNode);
			}
			this.instanceTable.remove(n);
	}

    protected void checkCursorInBounds()
    {
        int nodesetlength = (this.getBoundNodeset() == null ? 0 : this.getBoundNodeset().getLength());
        int cursor = xformsContext.getCursor(this.getRepeatId());
        if (nodesetlength < 1)
        {
            xformsContext.setCursor(this.getRepeatId(), 0);
        } else if (cursor > nodesetlength)
        {
            xformsContext.setCursor(this.getRepeatId(), nodesetlength);
        }
    }

    /** notifies the listener that the binding and the value changed  */
    public void notifyBindingChanged(NodeList newBinding)
    {
        //Log.debug("BINDING HAS CHANGED FOR " + this);
    	{
	        //Element r = (Element) this.getUIElement().getOwnerDocument().getElementsByTagNameNS(XFormsConstants.XFORMS_NS,"repeat").item(0);
	        //XFormsUtil.dumpDOM(true,r);
    	}
        this.setRefreshBlocking(true);
        // this will remove the repeat index
        this.xformsContext.getRepeatIndexHandler().repeatAboutToChange(this);
        if (this.prototypeUIElement == null)
        {
            this.refNodes = newBinding;
            this.expandUI();
        } else
        {
            Node addedNode = addRemoveSelectively(newBinding);
            this.refNodes = newBinding;
            if (addedNode != null)
                this.setRepeatIndex(addedNode);
            this.checkCursorInBounds();
        }
        //XFormsElementImpl.debugNode(this.getUIElement().getOwnerDocument().getDocumentElement());

        // we don't want to lose child listeners if refNodes becomes empty! otherwise insert into empty does not work
        /*
        if (refNodes != null && refNodes.getLength() > 0)
            this.registerChildListeners();
            */
        // this will renew
        this.xformsContext.getRepeatIndexHandler().repeatChanged(this);
        this.setRefreshBlocking(false);
        this.notifyRefresh();

    }

    protected void notifyRefresh()
    {
        Document hostDoc = this.ownerElement.getOwnerDocument();
        if (hostDoc instanceof ExtendedDocument)
        {
            ExtendedDocument ext = (ExtendedDocument) hostDoc;
            Object hostMLFC = ext.getHostMLFC();
            if (hostMLFC instanceof RefreshableService)
            {
                ((RefreshableService) hostMLFC).refresh();
            }
        }
        ((VisualElementImpl)this.getUIElement()).styleChanged();
    }
    protected void setRefreshBlocking(boolean state)
    {
        Document hostDoc = this.ownerElement.getOwnerDocument();
        if (hostDoc instanceof ExtendedDocument)
        {
            ExtendedDocument ext = (ExtendedDocument) hostDoc;
            Object hostMLFC = ext.getHostMLFC();
            if (hostMLFC instanceof RefreshableService)
            {
                ((RefreshableService) hostMLFC).setBlocking(state);
            }
        }
    }
/*
    protected void registerChildListeners()
    {
        this.unregisterChildListeners();
        if (refNodes != null && refNodes.getLength() > 0)
        {
            InstanceNode firstRefNode = (InstanceNode) refNodes.item(0);
            this.repeatInstanceParent = (InstanceNode) refNodes.item(0).getParentNode();
            if (this.repeatInstanceParent != null && !(this.repeatInstanceParent instanceof Document))
                 ((InstanceParentNode) this.repeatInstanceParent).addChildListener(this);
            else
                Log.error("Instance parent null, or document");
        } else
        {
            Log.error("No ref nodes on repeat " + this.getRepeatId());
        }
    }*/
    /*
    public void childEvent(boolean added, Element parent, Node child)
    {
        Log.debug("repeat: Insert or delete event: " + this.getRepeatId());
        ((DynBoundElementImpl) this.getUIElement()).renewBinding();
        if (added)
        {
        } else
        {

        }
    }*/
    /** 1 -based index */
    protected int getIndexOfInstanceNode(Node child)
    {
        for (int i = 0; i < this.getBoundNodeset().getLength(); i++)
        {
            if (child == this.getBoundNodeset().item(i))
                return i++;
        }
        Log.error("Could not find the index of instance node");
        return -1;
    }

    /*
    protected void unregisterChildListeners()
    {
        if (repeatInstanceParent != null)
             ((InstanceParentNode) this.repeatInstanceParent).removeChildListener(this);
    }*/

}
