/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;

import fi.hut.tml.xsmiles.gui.components.XRange;
import fi.hut.tml.xsmiles.gui.components.XComponent;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.math.BigDecimal;
import java.util.Hashtable;

import org.w3c.dom.Element;


/** implements range control for decimal values */
public class RangeInteger extends RangeDecimal
{
    public RangeInteger(XFormsContext context,Element elem)
    {
        super(context,elem);
    }
    protected Hashtable createLabelTable()
    {
        Hashtable labeltable = new Hashtable();
        labeltable.put(new Integer(1),start.toBigInteger().toString());
        labeltable.put(new Integer(this.getEnd()),end.toBigInteger().toString());
        return labeltable;
    }
    protected BigDecimal convertToBigDecimal(String str, String def)
    {
        
        try
        {
            if (str!=null) return new BigDecimal(new BigDecimal(str).toBigInteger());//.setScale(maxscale);
        } catch (NumberFormatException  e)
        {
            //Log.error(e);
        }
        return new BigDecimal(new BigDecimal(def).toBigInteger());
    }
    protected int findmaxscale()
    {
        return 0;
    }
    
} // RangeDecimal
