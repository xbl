/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms;

import fi.hut.tml.xsmiles.mlfc.xforms.dom.ModelElementImpl;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;

import org.w3c.dom.Element;
import org.w3c.dom.Attr;

import java.awt.*;

//import fi.hut.tml.xsmiles.xml.dom.DocumentImpl;
import org.apache.xerces.dom.DocumentImpl;

public class XFormsMLFC extends MLFC
{

	protected XFormsElementHandler elementHandler;

	public XFormsMLFC()
	{
	}

	/**
	 * Get the version of the MLFC. This version number is updated
	 * with the browser version number at compilation time. This version number
	 * indicates the browser version this MLFC was compiled with and should be run with.
	 * @return 	MLFC version number.
	 */
	public final String getVersion()
	{
		return Browser.version;
	}
	public void init()
	{
		try
		{

		    String basicStr = this.getMLFCListener().getProperty("xforms/configuration/basic");
		    XFormsConfiguration.setRunBasic(basicStr.equalsIgnoreCase("true"));
		    ((ExtendedDocument) getXMLDocument().getDocument()).getStyleSheet().addXMLDefaultStyleSheet(
				Browser.getXResource("xsmiles.xforms.stylesheet"));
		} catch (Exception e)
		{
			Log.error(e);
		}
		super.init();
	}
	public void start()
	{
		Log.debug("XFormsMLFC start.");
		String debugEventsStr = this.getMLFCListener().getProperty("logging/debuglevel/eventsdebug");
		elementHandler.debugEvents = debugEventsStr.equalsIgnoreCase("true");
		String debugXFormsStr = this.getMLFCListener().getProperty("logging/debuglevel/xformsdebug");
		elementHandler.debugXForms = debugXFormsStr.equalsIgnoreCase("true");
		elementHandler.start();
		//	  elementHandler.setXMLDocument(this.getXMLDocument());
	}

	public void stop()
	{
		Log.debug("Xforms.stop()");
		elementHandler.destroy();
	}

	/**
	 * Create a DOM element.
	 */
	public Element createElementNS(DocumentImpl doc, String URI, String tagname)
	{
		if (elementHandler == null)
			elementHandler = new XFormsElementHandler((ExtendedDocument) doc, this);
		return elementHandler.createElementNS(URI, tagname);
	}

	/**
	* Create a DOM attribute.
	*/
	public Attr createAttributeNS(DocumentImpl doc, String namespaceURI, String qualifiedName)
	{
		if (elementHandler == null)
			elementHandler = new XFormsElementHandler((ExtendedDocument) doc, this);
		//		Log.debug("**XML events mlfc:createAttribute "+localname);
		return elementHandler.createAttributeNS(doc, namespaceURI, qualifiedName);
	}

}
