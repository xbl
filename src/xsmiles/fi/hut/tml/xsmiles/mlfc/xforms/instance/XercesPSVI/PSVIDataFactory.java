/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;

// for simple type id's
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.PSVIImpl;

import org.apache.xerces.impl.dv.XSSimpleType;

public class PSVIDataFactory extends DataFactory{

	public PSVIDataFactory()
	{
	    super();
	}
    
	protected void initializeDatatypeIDs()
	{
	    /** "string" type */
	    PRIMITIVE_STRING        = XSSimpleType.PRIMITIVE_STRING  ;
	    /** "boolean" type */
	    PRIMITIVE_BOOLEAN       = XSSimpleType.PRIMITIVE_BOOLEAN;
	    /** "decimal" type */
	    PRIMITIVE_DECIMAL       = XSSimpleType.PRIMITIVE_DECIMAL;
	    /** "float" type */
	    PRIMITIVE_FLOAT         = XSSimpleType.PRIMITIVE_FLOAT;
	    /** "double" type */
	    PRIMITIVE_DOUBLE        = XSSimpleType.PRIMITIVE_DOUBLE;
	    /** "duration" type */
	    PRIMITIVE_DURATION      = XSSimpleType.PRIMITIVE_DURATION;
	    /** "dataTime" type */
	    PRIMITIVE_DATETIME      = XSSimpleType.PRIMITIVE_DATETIME;
	    /** "time" type */
	    PRIMITIVE_TIME          = XSSimpleType.PRIMITIVE_TIME;
	    /** "date" type */
	    PRIMITIVE_DATE          = XSSimpleType.PRIMITIVE_DATE;
	    /** "gYearMonth" type */
	    PRIMITIVE_GYEARMONTH    = XSSimpleType.PRIMITIVE_GYEARMONTH;
	    /** "gYear" type */
	    PRIMITIVE_GYEAR         = XSSimpleType.PRIMITIVE_GYEAR;
	    /** "gMonthDay" type */
	    PRIMITIVE_GMONTHDAY     = XSSimpleType.PRIMITIVE_GMONTHDAY;
	    /** "gDay" type */
	    PRIMITIVE_GDAY          = XSSimpleType.PRIMITIVE_GDAY;
	    /** "gMonth" type */
	    PRIMITIVE_GMONTH        = XSSimpleType.PRIMITIVE_GMONTH;
	    /** "hexBinary" type */
	    PRIMITIVE_HEXBINARY     = XSSimpleType.PRIMITIVE_HEXBINARY;
	    /** "base64Binary" type */
	    PRIMITIVE_BASE64BINARY  = XSSimpleType.PRIMITIVE_BASE64BINARY;
	    /** "anyURI" type */
	    PRIMITIVE_ANYURI        = XSSimpleType.PRIMITIVE_ANYURI;
	    /** "QName" type */
	    PRIMITIVE_QNAME         = XSSimpleType.PRIMITIVE_QNAME;
	    /** "NOTATION" type */
	    PRIMITIVE_NOTATION      = XSSimpleType.PRIMITIVE_NOTATION;
	}   
    /**
     * create a data object based on the Xerces primitive type id
     */
    public Data createData(int primitiveTypeId)
    {
        if (primitiveTypeId == XSSimpleType.PRIMITIVE_DATE)
            return new DDate((short)primitiveTypeId);
        return super.createData(primitiveTypeId);
    }
	
} // RangeDecimal
