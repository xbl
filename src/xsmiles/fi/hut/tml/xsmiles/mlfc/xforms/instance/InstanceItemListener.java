/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;
/**
 * The listener for status / value changes in the instance item
 */
public interface InstanceItemListener {

	/**
	 * An instance item instructs the control to check its status, when the status changes
	 */
	public void checkVisibility(InstanceItem item);
	/**
	 * An instance item instructs the control to check its status, when the status changes
	 */
	public void checkValidity(InstanceItem item);
	
	public void setReadonly(boolean readonly);
	public void setRequired(boolean required);
	/**
	 * The value of this instanceItem has changed
	 */
	public void valueChanged(String newValue);
    
        /** 
         * notify this listener that there was an error in the value of the 
         * instance item. This can be schema validity, constraint, required etc.
         */
        public void notifyError(Exception e,boolean atSubmission);

		public void checkStyling();
	
}

