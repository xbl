/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.SchemaDVFactory;
import org.apache.xerces.impl.dv.ValidatedInfo;
import org.apache.xerces.impl.dv.XSSimpleType;
import org.apache.xerces.impl.xs.psvi.XSSimpleTypeDefinition;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;

/**
 * Xsi type implementation For XForms Basic
 * @author honkkis
 *
 */
public class BasicXsiTypeImpl implements XsiType
{
    /*
    public BasicXsiTypeImpl(String namespace,String type, SchemaPool schemaPool)
    {
    }
    public int getPrimitiveTypeId()
    {
        return XFormsConfiguration.getInstance().getDataFactory().PRIMITIVE_STRING;
    }
    
    public Object validateString(String s) throws InvalidDatatypeValueException 
    {
        return null;
    }*/
    
    
    final static SchemaDVFactory factory = SchemaDVFactory.getInstance();
    protected XSSimpleType simpleType;
    final static String xsdNamespace="http://www.w3.org/2001/XMLSchema";
    public BasicXsiTypeImpl(String namespace,String type, SchemaPool schemaPool)
    {
        if (type==null||type.length()<1)
        {
            Log.error("xsi:type was null or empty string");
            return;
        }
        if (namespace==null||namespace.equals(xsdNamespace)||namespace.length()==0) 
            this.simpleType = factory.getBuiltInType(this.getLocalname(type));
        else {
            Log.error("Invalid datatype: "+type+" in namespace:"+namespace+". Only schema builtin types are supported in XForms Basic.");
        }
        if(this.simpleType == null){
            Log.error("Invalid built-in Simple datatype given as argument: "+type+" namespace:" +namespace);
        }
    }
    protected String getLocalname(String tagname)
    {
        int index = tagname.indexOf(':');
        if (index<0) return tagname;
        String localname = tagname.substring(index+1);
        return localname;
    }    /**
     * This method digs the underlying primitive type, no matter if the
     * type is defined as a restriction to a complex type or just as type="xs:xxx"
     **/
    public int getPrimitiveTypeId()
    {
        if (this.simpleType==null) return -1;
        XSSimpleTypeDefinition primitive = simpleType.getPrimitiveType();
        if (!(primitive instanceof XSSimpleType))
        {
            System.out.println("Wasn't instanceof XSSimpletype");
            return -1;
        }
        XSSimpleType primitiveType = (XSSimpleType)primitive;
        short id = primitiveType.getPrimitiveKind();
        //Log.debug("Primitive type: "+id);
        return id;
        //return "xxx";
        //return ((PSVIImpl)XFormsConfiguration.getInstance().getPSVI()).getPrimitiveTypeIdInternal(simpleType);
    }
    
    /**
     * this method shows how to validate the content against the given simple type.
     *
     * @param String content to validate
     * @param XSSimpleType SimpleType Definition schema component against which to validate the content.
     *
     * @return ValidatedInfo validatedInfo object.
     */
    private ValidatedInfo validateString(String content, XSSimpleType simpleT) throws InvalidDatatypeValueException{

        //create an instance of 'ValidatedInfo' to get back information (like actual value,
        //normalizedValue etc..)after content is validated.
        ValidatedInfo validatedInfo = new ValidatedInfo();

        //get proper validation context , this is very important we need to get appropriate validation context while validating content
        //validation context passed is generally different while validating content and  creating simple type (applyFacets)
        //ValidationContext validationState = getValidationContext();

        //try{
        simpleT.validate(content, null, validatedInfo);
        //}catch(InvalidDatatypeValueException ex){
        //    System.err.println(ex.getMessage());
        //}

        //now 'validatedInfo' object contains information

        // for number types (decimal, double, float, and types derived from them),
        // Object return is BigDecimal, Double, Float respectively.
        // for some types (string and derived), they just return the string itself
        Object value = validatedInfo.actualValue;
        //so returned Object can be casted to actual java object like..
        //Boolean booleanDT = (Boolean)value;

        //The normalized value of a string value
        String normalizedValue = validatedInfo.normalizedValue ;

        //If the type is a union type, then the member type which
        //actually validated the string value.
        XSSimpleType memberType = validatedInfo.memberType ;

        return validatedInfo;

    }//validateString()	
    public Object validateString(String s) throws InvalidDatatypeValueException 
    {
        if (this.simpleType==null) return null;
        ValidatedInfo info = this.validateString(s,this.simpleType);
        if (info==null) return null;
        return info.actualValue;
    }
    
    

}
