/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 9, 2005
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dom.swing;

import javax.swing.tree.DefaultTreeCellRenderer;


/**
 * @author honkkis
 *
 */
public class XMLNodeRenderer extends DefaultTreeCellRenderer
{
    public XMLNodeRenderer()
    {
        super();
    }
}
