/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;



import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

import fi.hut.tml.xsmiles.dom.EventHandlerService;

import org.w3c.dom.events.*;



    

/**
 * XForm/action handler's abstract base class
 * @author Mikko Honkala
 */


public abstract class ActionHandlerBaseImpl extends DynBoundElementImpl 
		implements EventHandlerService {
	
	public ActionHandlerBaseImpl(XFormsElementHandler owner, String ns, String name) {
		super(owner, ns, name);
	}

	public abstract void activate(Event evt);

}
