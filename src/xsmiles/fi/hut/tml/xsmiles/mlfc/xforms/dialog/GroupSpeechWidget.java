/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Element;


/**
 * @author honkkis
 *
 */
public class GroupSpeechWidget extends BaseSpeechWidget
{
    
    public GroupSpeechWidget(Element e, FocusHandler handler)
    {
        super(e,handler);
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateDialogQuestion()
     */
    public String generateDialogQuestion()
    {
        StringBuffer question = new StringBuffer();
        question.append(this.getLabel());
        question.append(". ");
        // TODO Auto-generated method stub
        Vector children = this.getChildFocusPoints();
        Enumeration enumeration=children.elements();
        while (enumeration.hasMoreElements())
        {
            Element e=(Element)enumeration.nextElement();
            question.append(""+this.getLabelForSpeaking(e,false)+". ");
        }
        return question.toString();
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateGrammar(boolean)
     */
    public void generateGrammar(Grammar grammar)
    {
        // TODO Auto-generated method stub
        GrammarGenerator.GenerateSimpleGrammar(focusHandler.getFocusPoints(this.element).elements(), grammar);
    }
    
    public boolean approximateMode=false;
    public Element approximatedNode=null;
    
    public boolean approximateResponse(Response response)
    {
        // TODO Auto-generated method stub
        Vector children = this.getChildFocusPoints();
        int index = closestResponse(response,children);
        if (index<0) return false;
        approximatedNode=(Element)children.elementAt(index);
        this.focusHandler.speak(this.focusHandler.getLastReply()+". Did you mean: "+this.getLabel((Element)children.elementAt(index)));
        approximateMode=true;
        return true;
    }
    public boolean interpretApproximateResponse(Response response)
    {
        this.approximateMode=false;
        Element app = this.approximatedNode;
        this.approximatedNode=null;
        String r=response.response;
        if (r.equalsIgnoreCase("yes")||
                r.equalsIgnoreCase("i did")||
                r.equalsIgnoreCase("true")
                )
        {
            this.focusHandler.setFocus(app);
            return true;
        }
        this.focusHandler.speakCurrentFocus();
        return true;
    }

    
    public int closestResponse(Response response, Vector children)
    {
        double minLev = 600;
        int index = -1;
        for (int i = 0;i<children.size();i++)
        {
            Object child = children.elementAt(i);
            //if (child instanceof BaseSpeechWidget)
            {
                //BaseSpeechWidget cw = (BaseSpeechWidget)child;
                String label = this.getLabel((Element)child);
                if (label!=null&&label.length()>0)
                {
                    int distance = LevenshteinDistance.LD(label.toLowerCase(),response.response.toLowerCase());
                    double dist = ((double)distance)/((double)label.length());
                    if (dist<minLev)
                    {
                        minLev=dist;
                        index=i;
                    }
                }
            }
        }
        return index;
    }

    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.FocusTreeNode#interpreteResponse(fi.hut.tml.xsmiles.mlfc.xforms.dialog.Response)
     */
    public boolean interpretResponse(Response response)
    {
        if (approximateMode==true) return interpretApproximateResponse(response);
        else
        {
            Enumeration e = this.getChildFocusPoints().elements();
            while (e.hasMoreElements())
            {
                Element node = (Element)e.nextElement();
                String label=this.getLabel(node).trim();
                String labelWithoutNumbers=GrammarGenerator.replaceNumbersWithStrings(label);
                
                if (label.equalsIgnoreCase(response.response.trim()))
                {
                    this.focusHandler.setFocus(node);
                    return true;
                }
                else if (labelWithoutNumbers.equalsIgnoreCase(response.response.trim()))
                {
                    this.focusHandler.setFocus(node);
                    return true;
                }
            }
            return false;
        }
    }
 
    protected Vector getChildFocusPoints()
    {
        // TODO: cache?
        return this.focusHandler.getFocusPoints(this.element);
    }





}
