
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI;

import java.io.Reader;
import java.util.Calendar;

import org.apache.xerces.dom.DOMInputSourceImpl;
import org.apache.xerces.dom.PSVIDOMImplementationImpl;
import org.apache.xerces.impl.Constants;
import org.apache.xerces.parsers.DOMBuilderImpl;
import org.apache.xerces.parsers.StandardParserConfiguration;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.util.XMLGrammarPoolImpl;
import org.apache.xerces.xni.parser.XMLParserConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.DOMInputSource;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PSVI;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.SchemaPool;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.XsiType;

/**
 * This class holds the current configuration for XForms Full with Xerces: whether or not to use XercesPSVI,
 * which datatype factory to use, etc.
 * @author honkkis
 *
 */
public class XFormsConfigurationPSVI extends XFormsConfiguration
{
    /** Property identifier: symbol table. */
    public static final String SYMBOL_TABLE =
        Constants.XERCES_PROPERTY_PREFIX + Constants.SYMBOL_TABLE_PROPERTY;
    
    /** Property identifier: grammar pool. */
    public static final String GRAMMAR_POOL =
        Constants.XERCES_PROPERTY_PREFIX + Constants.XMLGRAMMAR_POOL_PROPERTY;
    
    
    public SchemaPool createSchemaPool() 
    {
             return new XercesSchemaPoolImpl();
    }
    
    
    public DataFactory getDataFactory()
    {
        if (dfact==null)
        {    
            dfact=new PSVIDataFactory();
        }
        return dfact;
    }
    
    public String getInstanceDocumentClassName()
    {
        return XFormsConstants.PSVIDocumentClassname;
    }
    
    public XsiType createXsiType(String typeNamespace,String typeLocalName,SchemaPool pool)
    {
        return new PSVIXsiTypeImpl(typeNamespace,typeLocalName,pool);
    }
    
    protected String getDefaultXPathEngineClass()
    {
        return XPATH_XALAN_CLASS;
    }

    
    
    public PSVI getPSVI() 
    {
        return new PSVIImpl();
    }
    
    protected void setSymbolTable(XMLParserConfiguration config, SchemaPool pool)
    {
         SymbolTable sym;
         XMLGrammarPoolImpl fGrammarPool;
         fGrammarPool = ((XercesSchemaPoolImpl)pool).getXMLGrammarPoolImpl();
         sym = ((XercesSchemaPoolImpl)pool).getSymbolTable();
         
         if (fGrammarPool!=null) config.setProperty("http://apache.org/xml/properties/internal/grammar-pool",
         fGrammarPool);
         if (sym !=null)
         config.setProperty(SYMBOL_TABLE, sym);
    }
    
    public Calendar getCalendarFromSchemaString(String str)
    {
      DDate d;
        try
        {
             d= new DDateTime();
 	        d.setValueFromSchema(str);
        } catch (Throwable e)
        {
            d = new DDate((short)-1);
	        d.setValueFromSchema(str);
        }
        if (d!=null)
        {
	        return d.toCalendar();
        }
        return null;
    }
    
    
    
    public DOMBuilderImpl getDOMBuilderImpl(boolean plain, SchemaPool pool,Object entityResolver, Object errorHandler)
    {
        this.getDOMImpl();
        //fGrammarPool = new XMLGrammarPoolImpl();
        XMLParserConfiguration config = new StandardParserConfiguration();
        this.setSymbolTable(config,pool);
        DOMBuilderImpl builder = new DOMBuilderImpl(config);//,fGrammarPool);
        if (!plain) builder.setParameter("http://apache.org/xml/properties/dom/document-class-name",
                this.getInstanceDocumentClassName());
        Boolean namespaces = Boolean.TRUE;
        builder.setParameter("http://xml.org/sax/features/namespaces",namespaces);
        if (pool!=null&&!plain)
        {
            builder.setParameter("http://xml.org/sax/features/validation",Boolean.TRUE);
            builder.setParameter( "http://apache.org/xml/features/validation/schema",  Boolean.TRUE );
            builder.setParameter( "psvi",  Boolean.TRUE );
            builder.setParameter("entity-resolver",entityResolver);
            builder.setParameter("error-handler",errorHandler);
        }
        return builder;
    }
    
    protected   DOMImplementationLS impl;

    public DOMInputSource createDOMInputSource(DOMImplementationLS dimpl)
    {
        DOMInputSource source= (DOMInputSource)new DOMInputSourceImpl();
        //DOMInputSource source = dimpl.createDOMInputSource();
        return source;
    }

    public DOMImplementationLS getDOMImpl()
    {
        if (impl==null)
        {
            impl= (DOMImplementationLS)PSVIDOMImplementationImpl.getDOMImplementation();
            
            //DOMImplementationSourceImpl simpl = new DOMImplementationSourceImpl();
            //impl = (DOMImplementationLS)simpl.getDOMImplementation("LS-Load") ;
        }
        return impl;
    }

    public Document loadDocument(Reader r, String baseURL, boolean plain, SchemaPool pool, Object entityResolver,Object errorHandler ) throws Exception
    {
        DOMBuilderImpl builder = this.getDOMBuilderImpl(plain,pool,entityResolver,errorHandler);
        
        if (pool==null)
        {
            Log.error("Schema pool was null while reading with baseURL"+baseURL);
        }
        
        DOMImplementationLS dimpl = this.getDOMImpl();
        DOMInputSource source = this.createDOMInputSource(dimpl);
        
        source.setCharacterStream(r);
        source.setBaseURI(baseURL);
        
        final String docPropId = "http://apache.org/xml/properties/dom/document-class-name";
        if (!plain) builder.setParameter(docPropId,this.getInstanceDocumentClassName());
        
        InstanceDocument doc = (InstanceDocument)builder.parse(source);
        //XMLGrammarPoolImpl fGrammarPool;
        //fGrammarPool = ((XercesSchemaPoolImpl)pool).getXMLGrammarPoolImpl();
        if (doc!=null)
            doc.setSchemaPool(pool);
        
        return doc;
    }
    
    public boolean shouldHandleXsiType()
    {
        // FOR XFORMS BASIC WE SHOULD HANLDE XSI TYPE BUT FOR FULL
        // THE PARSER HANDLES IT
        return false;
    }
    
    /** this is returned to the XPath function for conformance level */
    public String getConformanceLevel()
    {
        return "full";
    }


    
    
    
    

}
