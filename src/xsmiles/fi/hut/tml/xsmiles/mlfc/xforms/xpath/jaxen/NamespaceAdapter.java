/*
 * Created on Apr 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen;

import org.jaxen.NamespaceContext;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;

/**
 * @author honkkis
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class NamespaceAdapter implements NamespaceContext{

    /* (non-Javadoc)
     * @see org.jaxen.NamespaceContext#translateNamespacePrefixToUri(java.lang.String)
     */
    protected Element nsElement;
    public NamespaceAdapter(Element elem)
    {
        this.nsElement=elem;
    }
    public String translateNamespacePrefixToUri(String prefix) {
        if (prefix==null||prefix.length()==0) return null;
        String uri = XFormsUtil.getNamespaceURI(prefix,nsElement);
        if (uri!=null) return uri;
        else
        {
            Log.debug("Could not find prefix: "+prefix);
            return null;
        }
    }
}
