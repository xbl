/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;

import fi.hut.tml.xsmiles.mlfc.xforms.xpath.ModelContext;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan.XPathContextEx;

import fi.hut.tml.xsmiles.Log;

import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.apache.xml.utils.XMLString;

//import org.apache.xpath.compiler.Compiler2;
import org.apache.xpath.compiler.XPathParser;
import org.apache.xpath.Expression;
import org.apache.xpath.NodeSetDTM;
import org.apache.xml.utils.PrefixResolverDefault;
import org.apache.xpath.objects.XObject;
import org.apache.xpath.XPathVisitor;
import org.apache.xpath.ExpressionOwner;
import org.apache.xpath.axes.LocPathIterator;
import org.apache.xml.dtm.DTM;
import org.apache.xml.dtm.DTMIterator;

import org.apache.xalan.trace.*;

import java.lang.reflect.Method;

import javax.xml.transform.*;

import fi.hut.tml.xsmiles.dom.ExtendedDocument;

/**
 * The result of an XPath lookup
 * @author Mikko Honkala
 */


public class XPathVisitorEx extends XPathVisitor
{
    public Vector referredNodes;
    public XObject result;
    public boolean visitLocationPath(ExpressionOwner owner, LocPathIterator path) 
    {
        //XObject obj=path.asIterator(path.;
        /*
        try
        {
            //obj = path.execute(null);
            obj = ;
        } catch (TransformerException e)
        {
            Log.error(e);
        }*/
        //int size = path.size();
        Log.debug("visitLocationPath: "+path);//+" size:"+size);//+" OBJ:"+obj);
        return super.visitLocationPath(owner,path);
    }
}

