/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import java.awt.event.FocusEvent;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

/**
 * Superclass for text component form controls
 * @author Mikko Honkala 
 */

public class CustomControl extends XFormsControl //implements TextListener 
{

	/**
	 * Constructs a new text control
	 *
	 * @param my_handler 	The handler for this control
	 * @param my_elem 			The DOM element of this control
	 * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
	 */
	public CustomControl(XFormsElementHandler owner, String ns, String name)
	{
		super(owner, ns, name);
		//defaultComponentColor=java.awt.Color.white;
	}

	public void changeComponentValue(String newValue)
	{
		Log.debug("Custom control changing value to: "+newValue);
		// return, if the value is the same as in the component currently
	}

	public void destroy()
	{
		//if (this.textcomponent!=null) this.textcomponent.removeFocusListener(this);
		// TODO: remove all listeners etc
		super.destroy();
	}

	public void setReadonly(boolean ro)
	{
		super.setReadonly(ro);
		// TODO: dispatch to the XBL content
	}

    public void focusLost(FocusEvent ae)
	{
    	// TODO: check this
		updateInstance(false);
		super.focusLost(ae);
        /*
		GUI gui = getXMLDocument().getBrowser().getCurrentGUI();
		gui.hideKeypad();
         */
	}
	
    /** notifies the listener that the binding and the value changed  */
	/*
    public void notifyBindingChanged(NodeList newBinding)
    {
		this.updateInstance(false);
		super.notifyBindingChanged(newBinding);
    }*/

	protected void updateInstance(boolean valueChanging)
	{
		/*
		try
		{
			// Let's update the corresponding XML data
			//this.setRefNodeValue(text,valueChanging);
			//				this.getModel().getSchema().revalidate();
		} catch (Exception e)
		{
			Log.error(e);
		}*/
		insideUpdateEvent=false;
	}
	
    /** the refresh processing notifies, that the binding might have changed, and it needs to be re-evaluated */
    public void bindingMaybeDirty()
    {
    	// this is a kludge. For some reason, when the focus is in a input 
    	// control, and the user clicks something that makes that control 
    	// rewire, the focus lost event becomes latest, and then the control 
    	// has already been rewired, thus losing the current value. Note that this is only for controls where text is typed in textarea and input:string.
    	this.updateInstance(false); 
    	super.bindingMaybeDirty();
    }

	
	protected void registerListener()
	{
		super.registerListener();
		// register document listener
		//textcomponent.addTextListener(this);
	}

	public XComponent createComponent() {
		this.changeComponentValue((String)this.getRefNodeValue());
		// TODO Auto-generated method stub
		return null;
	}
	
	public void customControlSetValue(String st)
	{
		this.setRefNodeValue(st,false);
	}
}

