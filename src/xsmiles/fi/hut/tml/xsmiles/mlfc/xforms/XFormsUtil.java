/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms;

import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.QName;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;



/**
 * XForms releated utility functions
 */
public class XFormsUtil
{
    /* sets the text for a instance node */
    public static void setText(Node n,String text)
    {
        if (n.getNodeType()==Node.ATTRIBUTE_NODE)
        {
            n.setNodeValue(text);
            if (n instanceof InstanceNode)
            ((InstanceNode)n).getInstanceItem().notifyValueChanged();
        }
        else
        {    
	        //Log.debug("InstanceBaseElem setText("+text);
	        NodeList children=n.getChildNodes();
	        boolean found=false;
	        for (int i=0;i<children.getLength();i++)
	        {
	            Node child=children.item(i);
	            if (!found && child.getNodeType()==Node.TEXT_NODE)
	            {
	                found=true;
	//				Log.debug("Setting node: "+this+" value:"+text+" doc: "+this.getOwnerDocument());
	                child.setNodeValue(text);
	            }
	        }
	        if (found==false)
	        {
	            Text textnode=n.getOwnerDocument().createTextNode(text);
	            n.appendChild(textnode);
	//			Log.debug("Creating text node for: "+this+" value:"+text+" doc: "+this.getOwnerDocument());
	        }
	        //this.instanceItem.DOMValueChanged(text);
	        //this.instanceItem.notifyValueChanged();
        }
    }
    /** get the text value of this node */

    
    public static String getText(Node elem)
    {
        if (elem.getNodeType()==Node.ATTRIBUTE_NODE)
            return elem.getNodeValue();
        else if (elem.getNodeType()==Node.ELEMENT_NODE)
        {    
	        String text="";
	        NodeList children=elem.getChildNodes();
	        for (int i=0;i<children.getLength();i++)
	        {
	            Node child=children.item(i);
	            if (child.getNodeType()==Node.TEXT_NODE)
	            {
	                text+=child.getNodeValue();
	            }
	        }
	        return text;
        }
        return null;
    }
    
    /** goes thru all children and checks whether they are xforms valid
     * will notify registered controls to make sure that invalidity is
     * reflected in the UI
     */
    public static boolean isXFormsValid(InstanceNode n)
    {
        // TODO: check attributes
        if (!n.getInstanceItem().getXFormsValid()) return false;
        NodeList children = n.getChildNodes();
        boolean validity=true;
        for (int i=0;i<children.getLength();i++)
        {
            Node child = children.item(i);
            if (child instanceof InstanceNode)
            {
                InstanceNode childNode = (InstanceNode)child;
                if (childNode.getInstanceItem().getRelevant())
                {
                    boolean valid = isXFormsValid(childNode);
                    if (!valid)
                    {
                        // TODO: atSubmission
                        boolean atSubmission = false;
                        childNode.getInstanceItem().notifyError(new Exception("Does not meet XForms 'constraint' model item property."),atSubmission);
                        validity=false;
                    }
                }
            }
        }
        return validity;
    }
    
    public static QName getQName(String rawName, Element nsContext)
    {
        if (rawName==null||rawName.length()<1) return null;
        // parse localpart and prefix of xforms:type
        int colonPos = rawName.indexOf(':');
        if (colonPos>1 && colonPos<rawName.length())
        {
            String prefix = rawName.substring(0,colonPos);
            String localpart = rawName.substring(colonPos+1);
            String ns = XFormsUtil.getNamespaceURI(prefix,nsContext);
            QName qname= new QName(localpart,ns);
            return qname;
        }
        return null;

    }
    
    /** this method goes thru the tree from the provided element to the root
     * and tries to find a namespace prefix mapping and returns the mapped URI or
     * null if not found
     */
    public static String getNamespaceURI(String prefix, Element elem)
    {
        // search for xmls:prefix attribute
        Attr nsAttr = elem.getAttributeNode("xmlns:" + prefix);
        // return contents if found
        if (nsAttr != null)
            return nsAttr.getNodeValue();
        Node parent = elem.getParentNode();
        if (parent.getNodeType() == Node.ELEMENT_NODE)
            return getNamespaceURI(prefix, (Element) parent);
        else
            return null;
    }
    
    public static void dumpDOM(boolean views, Element e)
    {
    	Log.debug("---------------- DUMPING DOM --------------");
    	dumpDOM(views,e,0);
    }
    
    public static void dumpDOM(boolean views, Element e, int level)
    {
    	if (e==null) return;
    	String dstr="";
    	for (int i=0;i<level;i++)
    	{
    		dstr+="--";
    	}
    	dstr += e.toString()+e.hashCode();
    	if (views && e instanceof VisualElementImpl)
    	{
    		Vector viewV = ((VisualElementImpl)e).getViews();
    		
    		if (viewV!=null && viewV.size()>0)
    			dstr+="[view: "+viewV.elementAt(0).toString()+viewV.elementAt(0).hashCode()+"]";
    	}
    	Log.debug(dstr);
        NodeList children;
        if (views)
            children = ((XSmilesElementImpl)e).getChildNodes(true);
        else children = ((Element)e).getChildNodes();
    	for (int i=0;i<children.getLength();i++)
    	{
    		Node child = children.item(i);
    		if (child.getNodeType()==Node.ELEMENT_NODE)
    		{
    			dumpDOM(views,(Element)child,level+1);
    		}
    	}
    }
}
