/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import org.w3c.dom.xforms10.XFormsInstanceElement;

import fi.hut.tml.xsmiles.dom.AsyncChangeHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.InstanceElement;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceParser;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceDocument;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.StringUtils;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.mlfc.general.SimpleXPath;
import java.util.Hashtable;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.awt.EventQueue;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;



/**
 * Instance element
 * @author Mikko Honkala
 */

public class InstanceElementImpl extends PrologElement
implements XFormsInstanceElement,InstanceElement
{
        /*
          Add exeptions :
          - value is not found in set/get
         */
    private XMLParser parser;
    
    protected InstanceDocument instanceDoc;
    
    /** Copy of the initial instance */
    protected InstanceDocument initialInstanceDoc;
    
    // The reference to external instance
    protected String href;
    
    /** whether this was created using a bind to a non-existing instance */
    protected boolean lazyBastardMode=false;
    
    
    
    public InstanceElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        Log.debug("******* called Instance.init()");
        parser = getBrowser().getXMLParser();
        href=this.getAttribute(XFormsConstants.EXTERNAL_LINKING_ATTR);
        if (href!=null&&href.length()>0)
        {
            try
            {
                this.fetchExternalInstance();
            } catch (XFormsException e)
            {
                this.handleXFormsException(e);
            }
        }
        else
        {
            this.fetchInternalInstance();
        }
        instanceDoc.setInstanceElement(this);
        initialInstanceDoc = this.copyInstance(instanceDoc);
        //		this.debugNode(initialInstanceDoc);
        if (this.getModel()!=null)
        {
            this.getModel().instanceAdded(this);
        }
        super.init();
    }
    
    public void destroy()
    {
        if (instanceDoc!=null)
        {
            //instanceDoc.destroy();
            instanceDoc=null;
        }
        if (initialInstanceDoc!=null)
        {
            //initialInstanceDoc.destroy();
            initialInstanceDoc=null;
        }
        super.destroy();
    }
    
    /** reset the instance document from this stream. Notify model that it
     *should update all bindings etc.
     */
    public void readInstanceAndReset(InputStream stream,URL url)
    {
        try
        {
        	// MH: thread-safety bug fix
            final InstanceDocument newInstanceDoc = this.readInstanceFromStream(stream,url);
            
            Runnable r = new Runnable()
            {
                public void run()
                {
                	// MH: before changing the instance document, remove all model's event listeners to the old doc
                	// Note that resetModel() will re-add those listeners
                	getModel().removeAllMutationListeners();
                	instanceDoc	= newInstanceDoc;
                    getModel().resetModel(true);
                }
            };
            if (this.getOwnerDocument() instanceof AsyncChangeHandler)
            {
                ((AsyncChangeHandler)this.getOwnerDocument()).invokeLater(r);
            }
            else
            {
                EventQueue.invokeLater(r);
            }
        
        
        } catch (Exception e)
        {
            Log.error(e,"At model.reset()");
            this.getHandler().showUserError(e);
        }
        
    }
    /**
     * This function generates a new InstanceDocumentImpl,
     * and copies the nodes under the internal instance to this new documentImpl
     */
    protected void fetchInternalInstance()
    {
        Log.debug("Using inline instance.");
        
        //instanceDoc= new InstanceDocumentImpl();
        // i18n:  Be sure to preserve encoding of owner doc by default
        try
        {
            //Reader schemaReader = this.getSchemaReader();
            instanceDoc = (InstanceDocument)new InstanceParser().read(this,this.getXMLDocument().getXMLURL().toString(),this.getModel().getSchemaPool());
            if (this.ownerDoc instanceof DocumentImpl)
                this.instanceDoc.setEncoding(((DocumentImpl)this.ownerDoc).getEncoding());
        } catch (Exception e)
        {
            String errorString = "Unable to parse inline instance into a instance Doc. Instance id: "+this.getAttribute("id");
            Log.error(e,errorString);
            XFormsException xe = new XFormsException(errorString+"\n"+e.getMessage());
            this.handleXFormsException(xe);
        }
    }
    /** read instance document from an external source */
    protected void fetchExternalInstance() throws XFormsLinkException
    {
        URL externalInstanceURL=null;
        try
        {
            Log.debug("Fetching an external instance from: "+href);
            externalInstanceURL=resolveURI(href);
            XSmilesConnection conn =  this.getHandler().openURLWithGET(externalInstanceURL);
            this.instanceDoc = this.readInstanceFromStream(conn.getInputStream(),externalInstanceURL);
        } catch (Exception e)
        {
            this.getModel().dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.LINK_EXCEPTION));
            throw new XFormsLinkException(e.toString(),externalInstanceURL.toString());
        }
    }
    /** construct an instance document implementation from a stream */
    protected InstanceDocument readInstanceFromStream(InputStream stream, URL url) throws XFormsLinkException
    {
        Reader instReader = new InputStreamReader(stream);
        //Document tempDoc = parser.openDocument(conn.getInputStream(),false,externalInstanceURL.toString());
        //Document tempDoc = parser.openDocument(externalInstanceURL);
        InstanceDocument doc = (InstanceDocument)new InstanceParser().read(instReader,url.toString(),this.getModel().getSchemaPool());
        if (doc==null)
        {
            this.getModel().dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.LINK_EXCEPTION));
            throw new XFormsLinkException("Error parsing",url.toString());
        }
        
        doc.setInstanceElement(this);
        return doc;
    }
    
    
    public Node getInstanceNode()
    {
        return this.instanceDoc;
    }
    
    public InstanceDocument getInstanceDocument()
    {
        return this.instanceDoc;
    }
    
    
    /**
     *
     * Creates a String with the data within the <instance> -element
     * in it.
     *
     * @return a String: XML document with the instance data in it
     */
    public String getInstanceAsString() throws SAXException
    {
        String inst=parser.write(this.instanceDoc);
        return inst;
    }
    /*
    public Node findTextNode(String path, Node nsNode) throws SAXException
    {
        return findNode(path+"/text()",nsNode,null);
    }
    public Node findRefNode(String path, Node nsNode, Node contextNode) throws SAXException
    {
        return findNode(path,nsNode, contextNode);
    }
    public Node findNode(String path, Node nsNode, Node contextNode) throws SAXException
    {
        if (xpathAPI==null) xpathAPI=new CachedXPathAPIEx(this.getHandler(),this.getModel());
        String xpath = path;//path.replace('.','/');
        Node n=null;
        try
        {
            // Problems with default namespaces
            //			String p = "./:"+StringUtils.replace(xpath,"/","/:");
            n = xpathAPI.selectSingleNode(
            (contextNode==null?instanceDoc.getDocumentElement():contextNode),
            xpath,(nsNode==null?instanceDoc.getDocumentElement():nsNode));
        } catch (Exception e)
        {
            Log.error(e);
            Log.error("While trying to find node: "+path);
            this.instanceStructureChanged();
        }
        //		n=SimpleXPath.selectSingleNode(dom,xpath);
        return n;
    }
     
     */
    
    /** Create a copy of the instance document */
    protected InstanceDocument copyInstance(InstanceDocument doc)
    {
        try
        {
            InstanceDocument copyDoc = (InstanceDocument)doc.cloneNode(true);
            return copyDoc;
        }
        catch (Exception e)
        {
            Log.error(e);
            return null;
        }
        
    }
    
    /** reset this instance */
    public void reset() throws Exception
    {
        Log.debug("Instance.reset() id:"+this.getAttribute("id"));
        //XMLSerializer serializer = new XMLSerializer();
        //String tempstring = serializer.writeToString(getInitialInstance());
        instanceDoc = (InstanceDocument)new InstanceParser().read(getInitialInstance(),this.getXMLDocument().getXMLURL().toString(),this.getModel().getSchemaPool());
        instanceDoc.setInstanceElement(this);
        
        //this.getModel().instanceStructureChanged();
        //TODO: preserve encoding already?
    }
    
    /** Accessor method: return the initial instance copy */
    Node getInitialInstance()
    {
        return initialInstanceDoc;
    }
    
    /********* METHODS FOR ECMASCRIPTS ************/
    /**
     * public method for ECMAScripts to get the root of the instance document
     */
    public Node getInstanceRoot()
    {
        return instanceDoc.getDocumentElement();
    }
    
    void setLazyBastardMode(boolean lazy)
    {
        this.lazyBastardMode=lazy;
    }
    
    boolean getLazyBastardMode()
    {
        return this.lazyBastardMode;
    }
    
    public void createLazyBastardElement(String elementName, Element owner) throws XFormsBindingException
    {
        try
        {
            if (hasLazyBastardElement(elementName)==false)
            {
                Log.debug("Trying to create lazy bastard element: "+elementName);
                Element elem=instanceDoc.createElementNS("",elementName);
                instanceDoc.getDocumentElement().appendChild(elem);
            }
        } catch (Exception e)
        {
            Log.error(e);
            throw new XFormsBindingException((XFormsElementImpl)owner,elementName,"Lazy instance did not allow generation of this reference.");
        }
    }
    
    private boolean hasLazyBastardElement(String elementName)
    {
        for (Node n = instanceDoc.getDocumentElement().getFirstChild();n!=null;n=n.getNextSibling())
        {
            if (n.getNodeType()==Node.ELEMENT_NODE)
            {
                Element e = (Element)n;
                if (e.getLocalName().equals(elementName)) return true;
            }
        }
        return false;
        
    }
}
