/*
 * ExtensionsFunctions.java
 *
 * Created on 1. elokuuta 2003, 12:41
 */

package fi.hut.tml.xsmiles.mlfc.xforms.xpath.xalan;



import org.apache.xpath.compiler.FunctionTable;
import org.apache.xpath.compiler.FuncLoader;
import org.apache.xpath.functions.Function;

/**
 *
 * @author  honkkis
 */
public class ExtensionFunctions
{
    
    /** only install once */
    protected static boolean  installed=false;
    
    /** Creates a new instance of ExtensionsFunctions */
    public ExtensionFunctions()
    {
    }
    
    public static void installFunctions()
    {
        if (installed) return;
        installFunction("now",new FuncNow());
        installFunction("instance", new FuncInstance());
        installFunction("days-from-date", new FuncDaysFromDate());
        installFunction("boolean-from-string", new FuncBooleanFromString());
        installFunction("min", new FuncMin());
        installFunction("max", new FuncMax());
        installFunction("count-non-empty", new FuncCountNonEmpty());
        installFunction("avg", new FuncAvg());
        installFunction("months", new FuncMonths());
        installFunction("property", new FuncProperty());
        installFunction("seconds", new FuncSeconds());
        installFunction("if", new FuncIf());
        installFunction("index", new FuncCursor());
        installFunction("nodeindex", new FuncNodeIndex());
        installFunction("seconds-from-dateTime", new FuncSecondsFromDateTime());
        installed=true;
    }
    
    /** kludge for model/@function processing, checks whether this function exists
     * in the function table. For later xalan versions, this has to be changed
     *TODO: namespaces
     */
    public static boolean hasFunction(String uri, String funcName)
    {
        if (uri==null||uri.length()==0)
        {
            for (int i = 0; i < FunctionTable.m_functions.length; i++)
            {
                FuncLoader loader = FunctionTable.m_functions[i];
                if (loader!=null)
                {
                    String name = loader.getName();
                    if (name==funcName) return true;
                }
            }
        }
        return false;
    }
    // install extension functions (note: look also at XFormsContextEx,
    // for namespace prefixed functions
    protected static void installFunction(String name,Function func)
    {
        FunctionTable.installFunction(name, func);
          /*
               Log.debug(
                  "Registered class " + func.getClass().getName()
                  + " for XPath function 'here()' function in internal table");
           */
           /* the following is taken from apache's xml-security package
            * org.apache.xml.security.Init
            */
               /* The following tweak by "Eric Olson" <ego@alum.mit.edu>
                * is to enable xml-security to play with JDK 1.4 which
                * unfortunately bundles an old version of Xalan
                */
        FuncLoaderJDK14 funcLoader = new FuncLoaderJDK14(func.getClass());
        
        for (int i = 0; i < FunctionTable.m_functions.length; i++)
        {
            FuncLoader loader = FunctionTable.m_functions[i];
            
            if (loader != null)
            {
                //Log.debug("Func " + i + " " + loader.getName());
                
                if (loader.getName().equals(funcLoader.getName()))
                {
                    FunctionTable.m_functions[i] = funcLoader;
                }
            }
        }
    }
    static
    {

          /*
          String name="instance";
          int index=37;
          FunctionTable.m_functions[index] =
            new FuncLoaderEx(name,FuncInstance.class,index);
          KeywordsEx.addFunction(name, index);
           */
    }    
}
