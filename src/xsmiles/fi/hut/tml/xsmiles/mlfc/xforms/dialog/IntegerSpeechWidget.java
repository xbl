/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 10, 2004
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.AdaptiveControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;

/**
 * @author mpohja
 */
public class IntegerSpeechWidget extends BaseSpeechWidget
{
   static Hashtable numbers;
   static {
       initHash();
   }

    /**
     * @param e
     * @param handler
     */
    public IntegerSpeechWidget(Element e, FocusHandler handler)
    {
        super(e, handler);
    }

    public String generateDialogQuestion()
    {
        return this.getLabel() + " " + getCurrentValue() + this.getPrompt();
    }

    /** static prompt for this kind of control, for instance, "select a number" */
    public String getPrompt()
    {
        return ". Select a number.";
    }

    public void generateGrammar(Grammar grammar)
    {
        GrammarGenerator.generateIntegerGrammar(grammar);
    }

    public boolean interpretResponse(Response r)
    {
        String response = r.response.trim();
        Integer number = parseRespond(response);
        Log.debug("Parsed number: " + number);

        if (number == null)
            return false;

        this.focusHandler.speak(this.getLabel() + ": " + number.toString());

        Data data = ((TypedElement) this.element).getData();
        data.setValueFromObject(number);
        ((TypedElement) this.element).setData(data);
        this.moveFocusToParent();
        return true;
        //        return false;
    }

    private Integer parseRespond(String response) 
    {
        try
        {
            Vector numberTokens = new Vector();
            StringTokenizer tokens = new StringTokenizer(response);
            String token, number;
            boolean thousand = false, hundred = false;
            int value = 0, a = 0, b = 0, c = 0, i, tempValue = 0;
    
            while (tokens.hasMoreElements())
            {
                numberTokens.add(tokens.nextToken());
            }
    
            for (i = numberTokens.size() - 1; i >= 0; i--)
            {
                token = (String) numberTokens.elementAt(i);
                if (isMeaningful(token))
                {
                    if (token.equals("hundred"))
                        hundred = true;
                    else if (token.equals("thousand"))
                    {
                        value = updateValue(a, b, c, false);
                        a = b = c = 0;
                        hundred = false;
                        thousand = true;
                    }
                    else
                    {
                        tempValue = ((Integer) numbers.get(token)).intValue();
        
                        if (tempValue < 20 && !hundred)
                            a = tempValue;
                        else if (tempValue < 20)
                            c = tempValue;
                        else
                            b = tempValue;
                    }
                }
            }
            value += updateValue(a, b, c, thousand);
            return new Integer(value);
        } catch (Exception e)
        {
            return null;
        }
    }
    
    private boolean isMeaningful(String token)
    {
        return !"and".equals(token);
    }

    private int updateValue(int a, int b, int c, boolean k)
    {
        int value = (c * 100) + b + a;
        if (k)
            value *= 1000;
        return value;
    }

    private static void initHash()
    {
        numbers = new Hashtable(30, 1);
        numbers.put("oh", new Integer(0));
        numbers.put("zero", new Integer(0));
        numbers.put("one", new Integer(1));
        numbers.put("two", new Integer(2));
        numbers.put("three", new Integer(3));
        numbers.put("four", new Integer(4));
        numbers.put("five", new Integer(5));
        numbers.put("six", new Integer(6));
        numbers.put("seven", new Integer(7));
        numbers.put("eight", new Integer(8));
        numbers.put("nine", new Integer(9));
        numbers.put("ten", new Integer(10));
        numbers.put("eleven", new Integer(11));
        numbers.put("twelve", new Integer(12));
        numbers.put("thirteen", new Integer(13));
        numbers.put("fourteen", new Integer(14));
        numbers.put("fifteen", new Integer(15));
        numbers.put("sixteen", new Integer(16));
        numbers.put("seventeen", new Integer(17));
        numbers.put("eighteen", new Integer(18));
        numbers.put("nineteen", new Integer(19));
        numbers.put("twenty", new Integer(20));
        numbers.put("thirty", new Integer(30));
        numbers.put("fourty", new Integer(40));
        numbers.put("fifty", new Integer(50));
        numbers.put("sixty", new Integer(60));
        numbers.put("seventy", new Integer(70));
        numbers.put("eighty", new Integer(80));
        numbers.put("ninety", new Integer(90));
    }
}