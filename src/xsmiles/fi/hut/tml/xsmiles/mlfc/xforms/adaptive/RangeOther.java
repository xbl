/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.adaptive;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;

import fi.hut.tml.xsmiles.gui.components.XRange;
import fi.hut.tml.xsmiles.gui.components.XComponent;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.math.BigDecimal;
import java.util.Hashtable;

import org.w3c.dom.Element;


/** implements a disabled range control for out-of-range datatypes */
public class RangeOther extends RangeInteger
{
    public RangeOther(XFormsContext context, Element elem)
    {
        super(context,elem);
    }
    
    protected void createControl()
    {
        //fSelect = this.fContext.getComponentFactory().getXSelectBoolean();
        // TODO: fRange =
        this.fRange = this.fContext.getComponentFactory().getXRange(0,0,0,XRange.HORIZONTAL);
        this.fRange.setValue(0);
        this.fRange.setLabelTable(this.createLabelTable());
        this.fRange.setEnabled(false);
    }
    protected Hashtable createLabelTable()
    {
        Hashtable labeltable = new Hashtable();
        labeltable.put(new Integer(0),"0");
        labeltable.put(new Integer(1),"0");
        return labeltable;
    }
    protected int findmaxscale()
    {
        return 0;
    }
    
        /**
     * This handles the changes from the widget XSelectBoolean
     */
    /** event handler for slider adjustements */
    public void adjustmentValueChanged(java.awt.event.AdjustmentEvent adjustmentEvent)
    {
        // disabled
        return;    
    }
        /**
     * this function is used to notify the control to update its display according
     * to the content of Data
     */
    public void updateDisplay()
    {
    }


    
} // RangeDecimal
