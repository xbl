/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.Vector;

import org.w3c.dom.Element;


/**
 * @author honkkis
 *
 */
public interface FocusHandler
{
    public void setFocus(Element elem);
    
    public Element getNextFocus(Element e);

    public Element getPreviousFocus(Element e);

    public Element getRootFocus();
    
    public Element getParentFocus(Element e);

    public Element getCurrentFocus();

    public Vector getFocusPoints(Element e);
    
    /* speak or re-speak current focus */
    public void speakCurrentFocus();

    /* speak or re-speak current focus */
    public void speak(String s);
    
    public void destroy();
    public String getLastReply();



}
