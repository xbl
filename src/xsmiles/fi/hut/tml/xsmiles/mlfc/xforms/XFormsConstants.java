/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms;


/**
 * an interface to provide context information of this particular document with
 * XForms elements in it
 */
public interface XFormsConstants
{
    public static final String XFORMS_NS="http://www.w3.org/2002/xforms";
    public static final String XHTML_NS="http://www.w3.org/1999/xhtml";
    
    static final String XFormsConfigurationPSVI =
        "fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.XFormsConfigurationPSVI";
    static final String PSVIDocumentClassname =
        "fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.PSVIInstanceDocumentImpl";
    static final String InstanceDocumentClassname =
        "fi.hut.tml.xsmiles.mlfc.xforms.instance.BasicInstanceDocumentImpl";
    static final String XercesPSVIClassName =
        "org.apache.xerces.parsers.StandardParserConfiguration";
    
    static final String BasicDataFactoryClassName=
        "fi.hut.tml.xsmiles.mlfc.xforms.data.DataFactory";
    
    public static String PSVISchemaPoolClassName="fi.hut.tml.xsmiles.mlfc.xforms.instance.XercesPSVI.XercesSchemaPoolImpl";
    
    
    // attributes
    public static final String ACCESSKEY = "accesskey";
    public static final String ACTION_ATTRIBUTE ="action";
    public static final String APPEARANCE_ATTRIBUTE = "appearance";
    public static final String AT_ATTRIBUTE = "at";
    public static final String BIND_ATTRIBUTE = "bind";
    public static final String BUBBLES_ATTRIBUTE = "bubbles";
    public static final String CALCULATE_ATTRIBUTE = "calculate";
    public static final String CANCELABLE_ATTRIBUTE = "cancelable";
    public static final String CASE_ATTRIBUTE = "case";
    public static final String CDATA_SECTION_ELEMENTS_ATTRIBUTE = "cdata-section-elements";
    public static final String CONSTRAINT_ATTRIBUTE = "constraint";
    public static final String CONTROL_ATTRIBUTE = "control";
    public static final String ENCODING_ATTRIBUTE = "encoding";
    public static final String END_ATTRIBUTE = "end";
    public static final String EXTERNAL_LINKING_ATTR = "src";
    public static final String FUNCTIONS_ATTRIBUTE = "functions";
    public static final String INCLUDENAMESPACEPREFIXES_ATTRIBUTE = "includenamespaceprefixes";
    public static final String INCREMENTAL_ATTRIBUTE = "incremental";
    public static final String INDENT_ATTRIBUTE = "indent";
    public static final String INDEX_ATTRIBUTE = "index";
    public static final String INPUTMODE_ATTRIBUTE = "inputmode";
    public static final String ISVALID_ATTRIBUTE = "constraint";
    public static final String LEVEL_ATTRIBUTE = "level";
    public static final String MAXOCCURS_ATTRIBUTE = "maxOccurs";
    public static final String MEDIATYPE_ATTRIBUTE = "mediatype";
    public static final String METHOD_ATTRIBUTE = "method";
    public static final String MINOCCURS_ATTRIBUTE = "minOccurs";
    public static final String MODEL_ATTRIBUTE = "model";
    public static final String MODEL_ID_ATTRIBUTE ="model";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String NAVINDEX_ATTRIBUTE = "navindex";
    public static final String NODESET_ATTRIBUTE = "nodeset";
    public static final String NUMBER_ATTRIBUTE = "number";
    public static final String OMIT_XML_DECLARATION_ATTRIBUTE = "omit-xml-declaration";
    public static final String P3PTYPE_ATTRIBUTE = "p3ptype";
    public static final String POSITION_ATTRIBUTE = "position";
    public static final String READONLY_ATTRIBUTE = "readonly";
    public static final String REF_ATTRIBUTE = "ref";
    public static final String RELEVANT_ATTRIBUTE = "relevant";
    public static final String REPEAT_ATTRIBUTE = "repeat";
    public static final String REPEAT_BIND_ATTRIBUTE = "repeat-bind";
    public static final String REPEAT_ID_ATTRIBUTE ="repeat";
    public static final String REPEAT_MODEL_ATTRIBUTE = "repeat-model";
    public static final String REPEAT_NODESET_ATTRIBUTE = "repeat-nodeset";
    public static final String REPEAT_NUMBER_ATTRIBUTE = "repeat-number";
    public static final String REPEAT_STARTINDEX_ATTRIBUTE = "repeat-startindex";
    public static final String REPLACE_ATTRIBUTE = "replace";
    public static final String REQUIRED_ATTRIBUTE = "required";
    public static final String RESOURCE_ATTRIBUTE = "resource";
    public static final String RESOURCE_LINKING_ATTR = "resource";
    public static final String SCHEMA_ATTRIBUTE = "schema";
    public static final String SELECTED_ATTRIBUTE = "selected";
    public static final String SELECTION_ATTRIBUTE = "selection";
    public static final String SEPARATOR_ATTRIBUTE = "separator";
    public static final String SHOW_ATTR = "show";
    public static final String SHOW_ATTRIBUTE = "show";
    public static final String SRC_ATTRIBUTE = "src";
    public static final String STANDALONE_ATTRIBUTE = "standalone";
    public static final String STARTINDEX_ATTRIBUTE = "startindex";
    public static final String START_ATTRIBUTE = "start";
    public static final String STEP_ATTRIBUTE = "step";
    public static final String SUBMISSION_ATTRIBUTE = "submission";
    public static final String SUBMIT_INFO_ATTRIBUTE = "submission";
    public static final String TARGET_ATTRIBUTE = "target";
    public static final String TYPE_ATTRIBUTE = "type";
    public static final String VALUE_ATTRIBUTE = "value";
    public static final String VERSION_ATTRIBUTE = "version";
    public static final String ORIGIN_ATTRIBUTE = "origin";
    public static final String BEFORE_ATTRIBUTE = "before";

    // the element names
    public static final String ACTION_ELEMENT = "action";
    public static final String ALERT_ELEMENT = "alert";
    public static final String BIND_ELEMENT = "bind";
    public static final String BUTTON_ELEMENT = "trigger";
    public static final String CAPTION_ELEMENT = "label";
    public static final String CASE_ELEMENT = "case";
    public static final String CHOICES_ELEMENT = "choices";
    public static final String COPY_ELEMENT = "copy";
    public static final String DELETE_ELEMENT = "delete";
    public static final String DISPATCH_ELEMENT = "dispatch";
    public static final String EXTENSION = "extension";
    public static final String FILENAME = "filename";
    public static final String FILENAME_ELEMENT = "filename";
    public static final String GROUP = "group";
    public static final String GROUP_ELEMENT = "group";
    public static final String HELP = "help";
    public static final String HELP_ELEMENT = "help";
    public static final String HINT = "hint";
    public static final String HINT_ELEMENT = "hint";
    public static final String INPUT = "input";
    public static final String INPUT_ELEMENT = "input";
    public static final String INSERT = "insert";
    public static final String INSERT_ELEMENT = "insert";
    public static final String DUPLICATE_ELEMENT = "duplicate";
    public static final String DESTROY_ELEMENT = "destroy";
    public static final String INSTANCE = "instance";
    public static final String INSTANCE_ELEMENT = "instance";
    public static final String ITEM = "item";
    public static final String ITEMSET = "itemset";
    public static final String ITEMSET_ELEMENT = "itemset";
    public static final String ITEM_ELEMENT = "item";
    public static final String LABEL = "label";
    public static final String LOAD = "load";
    public static final String LOADURI_ELEMENT = "load";
    public static final String MEDIATYPE = "mediatype";
    public static final String MEDIA_TYPE_ELEMENT = "mediatype";
    public static final String MESSAGE = "message";
    public static final String MESSAGE_ELEMENT = "message";
    public static final String MODEL_ELEMENT = "model";
    public static final String OUTPUT_ELEMENT = "output";
    public static final String RANGE = "range";
    public static final String RANGE_ELEMENT = "range";
    public static final String REBUILD_ELEMENT = "rebuild";
    public static final String RECALCULATE_ELEMENT = "recalculate";
    public static final String REFRESH_ELEMENT = "refresh";
    public static final String REPEAT = "repeat";
    public static final String REPEAT_ELEMENT= "repeat";
    public static final String RESET = "reset";
    public static final String RESET_ELEMENT = "reset";
    public static final String REVALIDATE = "revalidate";
    public static final String REVALIDATE_ELEMENT = "revalidate";
    public static final String SCHEMA_ELEMENT = "schema";
    public static final String SECRET = "secret";
    public static final String SECRET_ELEMENT = "secret";
    public static final String SELECT = "select";
    public static final String SELECT1 = "select1";
    public static final String SELECT_BOOLEAN_ELEMENT = "selectboolean";
    public static final String SELECT_MANY_ELEMENT = "select";
    public static final String SELECT_ONE_ELEMENT = "select1";
    public static final String SEND = "send";
    public static final String SETFOCUS = "setfocus";
    public static final String SETINDEX = "setindex";
    public static final String SETVALUE = "setvalue";
    public static final String SETVALUE_ELEMENT = "setvalue";
    public static final String SET_FOCUS_ELEMENT = "setfocus";
    public static final String SET_INDEX_ELEMENT = "setindex";
    public static final String SUBMISSION = "submission";
    public static final String SUBMIT = "submit";
    public static final String SUBMIT_ELEMENT = "submit";
    public static final String SUBMIT_INFO_ELEMENT = "submission";
    public static final String SUBMIT_INSTANCE_ELEMENT = "send";
    public static final String SWITCH = "switch";
    public static final String SWITCH_ELEMENT = "switch";
    public static final String TEXTAREA = "textarea";
    public static final String TEXTAREA_ELEMENT = "textarea";
    public static final String TOGGLE = "toggle";
    public static final String TOGGLE_ELEMENT = "toggle";
    public static final String TRIGGER = "trigger";
    public static final String UPLOAD = "upload";
    public static final String UPLOAD_ELEMENT = "upload";
    public static final String VALUE = "value";
    public static final String VALUE_ELEMENT = "value";
    public static final String TREE_ELEMENT = "tree";
     
    /** pseudoclasses */
    public static final String DISABLED_PSEUDOCLASS ="disabled";
    public static final String ENABLED_PSEUDOCLASS ="enabled";
    public static final String INVALID_PSEUDOCLASS ="invalid";
    public static final String VALID_PSEUDOCLASS ="valid";
    public static final String READONLY_PSEUDOCLASS ="read-only";
    public static final String READWRITE_PSEUDOCLASS ="read-write";
    public static final String ACTIVE_CASE_PSEUDOCLASS ="xsmiles-active-case";
    public static final String INACTIVE_CASE_PSEUDOCLASS ="xsmiles-inactive-case";
    public static final String MEDIARENDERING_PSEUDOCLASS ="xsmiles-mediarendering";
    public static final String OUTOFRANGE_PSEUDOCLASS ="out-of-range";
    public static final String INRANGE_PSEUDOCLASS ="in-range";
    
    /** functions */
    public static final String FUNC_INDEX ="index";
    public static final String FUNC_NODEINDEX ="nodeindex";
    
    
    /** attribute values */
    
    /** the values of show attribute */
    public static final short SHOW_NEW = 1;
    public static final short SHOW_REPLACE = 10;
    
    /** the values of replace sttribute */
    public static final short REPLACE_ALL = 10;
    public static final short REPLACE_INSTANCE = 20;
    public static final short REPLACE_NONE = 30;
    // X-Smiles extensions
    public static final short REPLACE_NEW = 40;

    public static final String XSMILES_MESSAGE_SHOWN_EVENT ="xsmiles-message-shown";
    public static final String SUBMIT_STARTED_EVENT ="xforms-submit-started";
    
    // INITIALIZATION EVENTS
    public static final String MODEL_CONSTRUCT_EVENT = "xforms-model-construct";
    public static final String MODEL_DESTRUCT_EVENT = "xforms-model-destruct";
    public static final String MODEL_CONSTRUCT_DONE_EVENT = "xforms-model-construct-done";
    public static final String READY_EVENT = "xforms-ready";
    public static final String UI_INITIALIZE_EVENT = "xforms-ui-initialize";
    //public static final String FORM_CONTROL_INITIALIZE_EVENT = "xforms-form-control-initialize";
    // INTERACTION EVENTS
    public static final String NEXT_EVENT = "xforms-next";
    public static final String PREVIOUS_EVENT = "xforms-previous";
    public static final String FOCUS_EVENT = "xforms-focus";
    public static final String BLUR_EVENT = "xforms-blur";
    public static final String ACTIVATE_EVENT = "xforms-activate";
    public static final String VALUE_CHANGING_EVENT = "xforms-value-changing";
    public static final String VALUE_CHANGED_EVENT = "xforms-value-changed";
    public static final String SCROLL_FIRST_EVENT = "xforms-scroll-first";
    public static final String SCROLL_LAST_EVENT = "xforms-scroll-first";
	public static final String DUPLICATE_EVENT = "xforms-duplicate";
	public static final String DESTROY_EVENT = "xforms-destroy";
	public static final String INSERT_EVENT = "xforms-insert";
    public static final String DELETE_EVENT = "xforms-delete";
    public static final String SELECT_EVENT = "xforms-select";
    public static final String DESELECT_EVENT = "xforms-deselect";
    public static final String HELP_EVENT = "xforms-help";
    public static final String HINT_EVENT = "xforms-hint";
    public static final String ALERT_EVENT = "xforms-alert";
    public static final String REQUIRED_EVENT = "xforms-required";
    public static final String OPTIONAL_EVENT = "xforms-optional";
    public static final String INRANGE_EVENT = "xforms-in-range";
    public static final String OUTOFRANGE_EVENT = "xforms-out-of-range";
    public static final String ENABLED_EVENT = "xforms-enabled";
    public static final String DISABLED_EVENT = "xforms-disabled";
    public static final String VALID_EVENT = "xforms-valid";
    public static final String INVALID_EVENT = "xforms-invalid";
    public static final String READONLY_EVENT = "xforms-readonly";
    public static final String READWRITE_EVENT = "xforms-readwrite";
    public static final String REFRESH_EVENT = "xforms-refresh";
    public static final String REVALIDATE_EVENT = "xforms-revalidate";
    public static final String RECALCULATE_EVENT = "xforms-recalculate";
    public static final String REBUILD_EVENT = "xforms-rebuild";
    public static final String RESET_EVENT = "xforms-reset";
    public static final String SUBMIT_EVENT = "xforms-submit";
    public static final String SUBMIT_DONE_EVENT = "xforms-submit-done";
    public static final String SUBMIT_ERROR = "xforms-submit-error";
    public static final String BINDING_EXCEPTION = "xforms-binding-exception";
    public static final String SCHEMA_CONSTRAINTS_ERROR = "xforms-schema-constraints-error";
    public static final String TRAVERSAL_ERROR = "xforms-traversal-error";
    public static final String INVALID_DATATYPE_ERROR = "xforms-invalid-datatype-error";
    public static final String LINK_EXCEPTION = "xforms-link-exception";
    public static final String COMPUTE_EXCEPTION = "xforms-link-exception";
    public static final String LINK_ERROR = "xforms-link-error";
    
    public static final String FOCUSIN_NOTIFICATION_EVENT = "DOMFocusIn";
    public static final String FOCUSOUT_NOTIFICATION_EVENT = "DOMFocusOut";
    
    // implementation internal events
    
    // this is used at least by repeat to notify that it has done lot of changes to the DOM
    // if somebody cancels this events, the whole document will not be refresh() - ed
    public static final String SUBTREE_MODIFY_START = "xforms-subtree-modified-start";
    public static final String SUBTREE_MODIFY_END = "xforms-subtree-modified-end";
    public static final String CAPTION_CHANGED = "xforms-caption-changed";

    

}
