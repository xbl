/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.xpath;

import org.w3c.dom.Document;



/**
 * an interface to provide context information of this particular document with 
 * XForms elements in it
 */
public interface ModelContext   {
    public Document getInstanceDocument(String id);
    public XPathEngine getXPathEngine();
} 
