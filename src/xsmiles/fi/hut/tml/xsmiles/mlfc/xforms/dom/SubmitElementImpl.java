/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;



import java.util.Hashtable;
import java.awt.*;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.dom.EventImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SubmitElement;


/**
 * The XForms/Submit element
 * @author Mikko Honkala
 *
 */

public class SubmitElementImpl extends ButtonElementImpl implements ActionListener, SubmitElement
{
    
    /**
     * Constructs a new 'submit' control
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    
    public SubmitElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        
    }
    
    public void doSubmission()
    {
        InstanceElementImpl instance=this.getModel().getInstance();
        if (instance!=null)
        {
            try
            {
                String to = getTo();
                Log.debug("Submit info id: "+to);
                SubmissionElementImpl target=this.getModel().getSubmitInfo(to);
                if (target==null)
                {
                    String error = "Could not find a "+SUBMIT_INFO_ELEMENT+" with id '"+to+"'"; 
                    Log.error(error);
                    this.getHandler().showUserError(new Exception(error));
                    this.dispatchEvent((XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.BINDING_EXCEPTION));
                }
                
                XFormsEventImpl event = (XFormsEventImpl)XFormsEventFactory.createXFormsEvent(XFormsEventFactory.SUBMIT_EVENT);
                target.dispatchEvent(event);
                
                //				Log.debug("Should submit here");
            } catch (Exception e)
            {
                Log.error(e);
            }
        }
        else Log.error("No XForm instance found.");
    }
    public String getTo()
    {
        return this.getAttribute(SUBMIT_INFO_ATTRIBUTE);
    }
    /**
     * this method is overridden so that default actions for certain events can be processed
     */
    public boolean dispatchEvent(org.w3c.dom.events.Event event)
    {
        // simply forward to super class
        boolean ret = super.dispatchEvent(event);
        if (event instanceof EventImpl)
        {
            EventImpl xe = (EventImpl)event;
            if (!xe.preventDefault&&!xe.stopPropagation)
            {
                if (xe.getType().equals(XFormsEventFactory.DOMACTIVATE_EVENT))
                {
                    this.doSubmission();
                }
            }
        }
        return ret;
    }

    
}
