/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.util.Vector;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.event.*;

import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XSelect;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectElement;


/**
 * Abstract select element
 * @author Mikko Honkala
 */

public abstract class SelectElementImpl extends XFormsControl
implements org.w3c.dom.events.EventListener, ItemListener, SelectElement

{
    
    protected boolean isInputComponent=true;
    /** The items of this select*/
    protected Vector items;
    
    /** the abstract select component that the subclass creates */
    protected XSelect select;
    
    /**
     * Constructs a new 'select' control (exclusiveSelect or multipleSelect)
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    public SelectElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    
    public void init()
    {
        super.init();
        this.listenAllMutations(this);
    }
    /**
     * Creates the visible containing component of this control
     */
    public XComponent createComponent()
    {
        Vector its=this.getItems();
        initializeItems();
        this.replaceItemsInWidget(its,select);
        this.changeComponentValue((String)this.getRefNodeValue());
        return this.select;
    }
    
    /** register item listener */
    protected void registerListener()
    {
        super.registerListener();
        this.select.addItemListener(this);
    }
    
    
    /**
     * init and reinit calls this method to initialize the items
     */
    protected void initializeItems()
    {
        //Log.debug("initializeItems");
        Vector its=this.getItems();
        for (int i=0;i<its.size();i++)
        {
            ItemElementImpl item=(ItemElementImpl)its.elementAt(i);
            item.setIndex(i);
            item.setControl(this);
        }
    }
    
    /** add items to the select widget */
    static void replaceItemsInWidget(Vector its,XSelect sel)
    {
        //Log.debug("replaceItemsInWidget");
        sel.removeAll();
        for (int i=0;i<its.size();i++)
        {
            ItemElementImpl item=(ItemElementImpl)its.elementAt(i);
            sel.addSelection(item.getCaption());
            // TODO: remove debug
            //Log.debug("menu: added item:"+item);
        }
    }
    /** this method is called when the items are added or removed
     */
    public void reinitializeItems()
    {
        //Log.debug("Select reinitialize items");
        this.items=null;
        this.elementStatus=UNINITIALIZED;
        this.initializeItems();
        this.replaceItemsInWidget(items,select);
    }
    
    public void destroy()
    {
        this.removeAllMutationListeners(this);
        super.destroy();
    }
    
    /** this accessor function returns all the ancestor items
     * note: it also returns all the items that are created using
     * itemset.
     */
    public Vector getItems()
    {
                        /*
                          'itemset' can be anywhere that 'item' can,
                          the only restriction is that itemset cannot contain 'choises' elements
                         */
        //NodeList inodes=getElementsByTagNameNS(xforms_ns,"item");
        if (this.items==null)
        {
            this.items = new Vector(10);
            this.getItems(this.items,this);
        }
        return this.items;
    }
    
    /** private method for recursively getting the descendant elements */
    private void getItems(Vector itemv, Element e)
    {
        // TODO: remove debug
        //this.debugNode(e);
        Node n = e.getFirstChild();
        while (n!=null)
        {
            // TODO: remove debug
            //Log.debug("going thru node: "+n);
            if (n instanceof ItemElementImpl)
            {
                //Log.debug(n.toString()+" was instance of itemelementimpl");
                itemv.addElement(n);
            }
            else if (n instanceof ItemSetElementImpl)
            {
                Log.debug("Found itemSet");
                Vector is = ((ItemSetElementImpl)n).getItems();
                for (int i=0;i<is.size();i++)
                {
                    ItemElementImpl item = (ItemElementImpl)is.elementAt(i);
                    itemv.addElement(item);
                }
            }
            else if (n instanceof ChoicesElementImpl)
            {
                this.getItems(itemv,(Element)n);
            }
            n = n.getNextSibling();
        }
    }
/*
                public ItemElementImpl findItem(JComponent comp)
                {
                        Vector its=this.getItems();
                        for (int i=0;i<its.size();i++)
                        {
                                ItemElementImpl item=(ItemElementImpl)its.elementAt(i);
                                if (item.getComponent()==comp) return item;
                        }
                        return null;
                }
 */
    public ItemElementImpl findItem(String value)
    {
        if (value==null) return null;
        Vector its=this.getItems();
        for (int i=0;i<its.size();i++)
        {
            ItemElementImpl item=(ItemElementImpl)its.elementAt(i);
            if (item.getValue().equals(value)) return item;
        }
        return null;
    }
    public ItemElementImpl findItem(int index)
    {
        Vector its=this.getItems();
        if (index>(its.size()-1)||index<0) return null;
        return (ItemElementImpl)its.elementAt(index);
    }
    public void setHint(HintElementImpl h)
    {
        String hinttext=h.getText();
        this.select.setHintText(hinttext);
    }
    protected void userSelected(ItemElementImpl item)
    {
        item.userSelected();
    }
    
    
    /** METHODS FOR LISTENING TO DOM MUTATION EVENTS FROM THE INSTANCE */
    
    
    protected boolean internalModificationInProcess = false;
    /**
     * The DOM event handler
     * This method gets all changes to the contents of the select, and will then rebuild the select
     */
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
        // We either get attr changed event for the attribute or characterdatamodified or dom node inserted
        // for a text node
        String type = evt.getType();
        Log.debug("*** select got event: "+type);
        if (type.equals(XFormsEventFactory.SUBTREE_MODIFY_START))
        {
            internalModificationInProcess =true;
        }
        else if (type.equals(XFormsEventFactory.SUBTREE_MODIFY_END))
        {
            evt.preventDefault();
            evt.stopPropagation();
            internalModificationInProcess =false;
            this.reinitializeItems();
        }
        else if (evt instanceof MutationEvent&&! (internalModificationInProcess))
        {
            MutationEvent me=(MutationEvent)evt;
            Node target = (Node)me.getTarget();
            this.reinitializeItems();
        }
        
    }
    
    final String[] evtNames=
    {
        "DOMSubtreeModified",
        XFormsEventFactory.SUBTREE_MODIFY_START,
        XFormsEventFactory.SUBTREE_MODIFY_END,
        //			"DOMAttrModified",
        //			"DOMCharacterDataModified",
        //	        "DOMNodeInserted","DOMNodeRemoved",
        //	        "DOMNodeInsertedIntoDocument","DOMNodeRemovedFromDocument",
    };
    private void listenAllMutations(Node n)
    {
        EventTarget t=(EventTarget)n;
        for(int i=evtNames.length-1;
        i>=0;
        --i)
        {
            t.addEventListener(evtNames[i], this, false);
        }
    }
    
    protected void removeAllMutationListeners(Node n)
    {
        EventTarget t=(EventTarget)n;
        
        for(int i=evtNames.length-1;
        i>=0;
        --i)
        {
            t.removeEventListener(evtNames[i], this, false);
        }
    }
    
    protected void removeChildElements(InstanceNode target)
    {
        for (Node n = target.getFirstChild();n!=null;)
        {
            if (n.getNodeType()==Node.ELEMENT_NODE)
            {
                Node toBeRemoved = n;
                n=n.getNextSibling();
                target.removeChild(toBeRemoved);
            }
            else
                n=n.getNextSibling();
        }
    }
    /** replace the whole subtree by a value from the copy element */
    protected void replaceSubtree(CopyElementImpl copy, InstanceNode target)
    {
        if (target!=null&&copy!=null&&copy.getRefNode()!=null)
        {
            this.removeChildElements(target);
            this.appendSubtree(copy,target);
        }
        else Log.error("SelectOne: could not copy.");
    }
    
    protected void appendSubtree(CopyElementImpl copy, InstanceNode target)
    {
        if (target!=null&&copy!=null&&copy.getRefNode()!=null)
        {
            InstanceNode copyref = copy.getRefNode();
            //InstanceNode clone = (InstanceNode)copyref.cloneNode(true);
            InstanceNode clone =  (InstanceNode)target.getOwnerDocument().importNode(copyref,true);
            target.appendChild(clone);
            Log.debug("Copied item:");
            debugNode(clone);
        }
    }
    
    protected boolean isOpenSelection()
    {
        Attr attr = this.getAttributeNode(SELECTION_ATTRIBUTE);
        if (attr==null) return false;
        else if (attr.getNodeValue().equals("open")) return true;
        return false;
    }
}
