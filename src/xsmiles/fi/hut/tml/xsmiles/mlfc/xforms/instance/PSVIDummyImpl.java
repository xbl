/*
 * Created on Mar 16, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;

/**
 * @author honkkis
 *
 */
public class PSVIDummyImpl implements PSVI
{
    
    public Object validateElement(InstanceNode node) throws InvalidDatatypeValueException
    {
        return null;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.instance.PSVI#getPrimitiveTypeId(org.w3c.dom.Node)
     */
    public int getPrimitiveTypeId(Node node)
    {
        return XFormsConfiguration.getInstance().getDataFactory().PRIMITIVE_STRING;
    }
    
}
