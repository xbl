/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jan 17, 2005
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import fi.hut.tml.xsmiles.Log;


/**
 * @author honkkis
 *
 */
public class DateConverter
{
    
    static DateFormat formatOnlyMonth = new SimpleDateFormat("MMM");
    static DateFormat formatOnlyMonthLong = new SimpleDateFormat("MMMMM");    
    public static Calendar parseCalender(String s, Calendar origDate)

    {
        boolean wasDate=false;
        StringTokenizer tokenizer = new StringTokenizer(s," ,");
        while (tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            //First check if it is a number
            
            try
            {
                int number = Integer.parseInt(token);
                if (number>39&& number < 5000)
                {
                    // it is a year
                    origDate.set(Calendar.YEAR,number);
                    wasDate=true;
                }
                else if (number >0 && number <32)
                {
                    // it is a day number
                    origDate.set(Calendar.DAY_OF_MONTH,number);
                    wasDate=true;
                }
                else
                {
                    Log.debug("Did not understand:"+token);
                }
            }
            catch (NumberFormatException e)
            {
                // was not a number, try a month
                // FIRST TRY SHORT NOTATION "mar, jan"
    	        DateFormat format = formatOnlyMonth; 
    	        try
    	        {
    	            Date d = format.parse(token);
    	            origDate.set(Calendar.MONTH,d.getMonth());
                    wasDate=true;
    	        } catch (ParseException ex)
    	        {
                    // TRY LONG NOTATION ("january, march")
        	        format = formatOnlyMonthLong; 
        	        try
        	        {
        	            Date d = format.parse(token);
        	            origDate.set(Calendar.MONTH,d.getMonth());
                        wasDate=true;
        	        } catch (ParseException ex2)
        	        {
                        Log.debug("Did not understand:"+token);
        	            //this.append(ex2.toString()+" : "+ex2.getMessage());
        	        }
    	        }
            }
        }
        if (wasDate)
        return origDate;
        else return null;
    }

    
}
