/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.xpath;

import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatIndexHandler;
import java.util.Hashtable;

import org.w3c.dom.Node;



/**
 * an interface to provide context information of this particular document with 
 * XForms elements in it
 */
public interface XFormsContext   {


	public void setCursor(String id, int value);
	public int getCursor(String id);
    public Node getNodeIndex(String id);
    
    public ComponentFactory getComponentFactory();
    public boolean getDebugEvents();
   	public Hashtable getModels();
            public RepeatIndexHandler getRepeatIndexHandler();


	
} 
