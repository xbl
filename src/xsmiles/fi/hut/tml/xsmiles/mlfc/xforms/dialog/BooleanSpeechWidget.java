/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 10, 2004
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DBoolean;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.AdaptiveControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TriggerElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TypedElement;

/**
 * @author mpohja
 */
public class BooleanSpeechWidget extends TriggerSpeechWidget
{

    /**
     * @param e
     * @param handler
     */
    public BooleanSpeechWidget(Element e, FocusHandler handler)
    {
        super(e, handler);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateDialogQuestion()
     */
    public String generateDialogQuestion()
    {
        DBoolean d = (DBoolean)((TypedElement)this.element).getData();
        String selValue="Currently not selected.";
        if (d.toObject().equals(Boolean.TRUE))
        {
            selValue="Currently selected.";
        }
        String question=this.getLabel()+". "+selValue+" Do you want to select it?";
        return question+"\n";
    }

    public boolean interpretResponse(Response r)
    {
        // TODO Auto-generated method stub
        boolean found = false;
        String response=r.response.trim();
        Enumeration e =yesAnswers.elements();
        Data d = ((TypedElement)this.element).getData();
        while (e.hasMoreElements())
        {
            String item = (String)e.nextElement();
            if (item.trim().equalsIgnoreCase(response)) 
            {
                Log.debug("Yes answer selected");
                d.setValueFromSchema("true");
                found=true;
                this.focusHandler.speak(this.getLabel()+" selected.");
             }
        }
        if (found==false)
        {
            Log.debug("No answer selected");
            d.setValueFromSchema("false");
            this.focusHandler.speak(this.getLabel()+" unselected.");
        }
        ((TypedElement)this.element).setData(d);
        this.moveFocusToParent();
        return true;
    }

}