/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;

import java.util.Hashtable;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.FlowLayout;
import java.awt.TextComponent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.XInput;

/**
 * The XForms/Textbox element
 * @author Mikko Honkala
 *
 * NOTE: THIS CLASS IS NOT USED ANYMORE. LOOK AT PACKAGE ADAPTIVE: Class InputString
 */

public class InputElementImpl extends TextControl implements ActionListener
{
    
    /** the action for enter keypress */
    protected final static String ENTER_STROKED = "enter_stroked";
    
    /** the minimum size for input components */
    protected final static Dimension mSize = new Dimension(50,10);
    
    
    /**
     * Constructs a new 'textbox' control
     *
     * @param my_handler 	The handler for this control
     * @param my_elem 			The DOM element of this control
     * @param catchMutationEvents Wheather to catch DOM mutation events of the ref node
     */
    public InputElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    /**
     * Creates the visible containing component of this control
     */
    public XComponent createComponent()
    {
        textcomponent = this.getComponentFactory().getXInput();
        // TODO: remove
        textcomponent.setText((String)this.getRefNodeValue());
        return textcomponent;
        
    }
    
    public void destroy()
    {
        if (this.textcomponent!=null)
        {
            //this.textcomponent.resetKeyboardActions();
            this.textcomponent.removeActionListener(this);
        }
        super.destroy();
    }
    
    
    /** The action listener's action performed event */
    public void actionPerformed(ActionEvent ae)
    {
        String actionCommand = ae.getActionCommand();
        //Log.debug("Input: action performed: "+ae+" action:"+ae.getActionCommand());
        if (actionCommand==ENTER_STROKED)
        {
            updateInstance(false);
            dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.ACTIVATE_EVENT));
            dispatch(XFormsEventFactory.createXFormsEvent(XFormsEventFactory.DOMACTIVATE_EVENT));
        }
    }
    
    /** register enter keypress */
    protected void registerListener()
    {
        super.registerListener();
        this.textcomponent.addActionListener(this);
    }
}
