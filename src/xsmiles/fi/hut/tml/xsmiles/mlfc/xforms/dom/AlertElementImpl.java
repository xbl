/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.*;
import org.w3c.dom.events.*;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

/**
 * Hint element
 * @author Mikko Honkala
 */

public class AlertElementImpl extends MessageElementImpl implements org.w3c.dom.events.EventListener
{
    
    public AlertElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        bindingAttributesRequired=false;
    }
    
    public void init()
    {
        super.init();
        this.registerListeners();
    }
    
    public String getLevel()
    {
        return "modal";
    }

    protected String getHeading()
    {
        return "XForms alert";
    }
    
    public void destroy()
    {
        super.destroy();
        unregisterListeners();
    }
    
    public void registerListeners()
    {
        ((EventTarget)this.getParentNode()).addEventListener(XFormsConstants.INVALID_EVENT, this, false);
    }
    public void unregisterListeners()
    {
        ((EventTarget)this.getParentNode()).removeEventListener(XFormsConstants.INVALID_EVENT, this, false);
    }
    public void handleEvent(org.w3c.dom.events.Event evt)
    {
        Log.debug("Alert got DOM event: "+evt);
        if (evt.getType().equals(XFormsConstants.INVALID_EVENT))
        {
            super.activate(evt);
            //evt.stopPropagation();
        }
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dominterface.MessageElement#getMessageTypeAsString()
     */
    public String getMessageTypeAsString()
    {
        // TODO Auto-generated method stub
        return "Alert";
    }

}
