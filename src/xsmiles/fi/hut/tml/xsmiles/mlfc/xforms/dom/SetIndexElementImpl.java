/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.ui.RepeatHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XPathExpr;

import org.w3c.dom.Element;
import org.w3c.dom.events.Event;

/**
 * setindex element implementation
 * @author Mikko Honkala
 */

public class SetIndexElementImpl extends DynBoundElementImpl implements EventHandlerService
{
    
    public SetIndexElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        this.bindingAttributesRequired=false;
    }
    
    public void activate(Event evt)
    {
        String repeat = this.getRepeatRef();
        String index  = this.getIndex();
        if (repeat!=null&&repeat.length()>0&&index!=null&&index.length()>0)
        {
            try
            {
                Element elem = searchElementWithId(this.getOwnerDocument().getDocumentElement(),repeat);
                if (!(elem instanceof RepeatElementImpl))
                {
                    Log.error(this+" repeat attribute doesn't reference a repeat element.");
                    return;
                }
                elem = this.handler.getRepeatIndexHandler().findRepeatedId(elem, repeat);
                RepeatHandler rh = ((RepeatElementImpl) elem).getRepeatHandler();
                
                XPathExpr xpathe = this.getModel().getXPathEngine().createXPathExpression(index);
                Object ret = this.getModel().getXPathEngine().eval(this.contextNode,xpathe,this,null);
                int idx = ((Number)ret).intValue();
                
                if (idx < 1)
                    idx = 1;
                else if (idx > rh.getBoundNodeset().getLength())
                    idx = rh.getBoundNodeset().getLength();
                
                this.handler.getRepeatIndexHandler().setRepeatIndex(elem, rh, idx);
                this.getModel().needsRecalculate=true;
                this.getModel().needsRevalidate=true;
                this.getModel().needsRefresh=true;
            } catch (Exception e)
            {
                Log.error(e, this+" invalid index attribute.");
            }
        }
        else
        {
            Log.error(this+" missing repeat or index attribute.");
        }
    }
    
    protected String getRepeatRef()
    {
        return this.getAttribute(REPEAT_ID_ATTRIBUTE);
    }
    
    protected String getIndex()
    {
        return this.getAttribute(INDEX_ATTRIBUTE);
    }
}
