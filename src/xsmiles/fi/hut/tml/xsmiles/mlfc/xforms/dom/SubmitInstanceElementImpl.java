/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;

import fi.hut.tml.xsmiles.dom.EventHandlerService;


import org.w3c.dom.*;
import org.w3c.dom.events.*;

/**
 * XForm/setValue element implementation
 * @author Mikko Honkala
 */


public class SubmitInstanceElementImpl extends XFormsElementImpl implements EventHandlerService
{
    public SubmitInstanceElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
    }
    public void activate(Event evt)
    {
        // send an xforms:submit event to the submitinfo event target
        XFormsEventImpl event = (XFormsEventImpl)XFormsEventFactory.createEvent(XFormsEventFactory.SUBMIT_EVENT);
        event.initXFormsEvent(XFormsEventFactory.SUBMIT_EVENT,true,true,this);
        SubmissionElementImpl submitInfo = this.getSubmitInfo();
        if (submitInfo == null)
        {
            this.getHandler().showUserError(new XFormsException("Send element had an submission id that could not be found: "+this.getAttribute(SUBMIT_INFO_ATTRIBUTE)));
        }
        Log.debug("send: submission: "+submitInfo);
        submitInfo.dispatchEvent(event);
    }
    
    SubmissionElementImpl getSubmitInfo()
    {
        String submit_id = this.getAttribute(SUBMIT_INFO_ATTRIBUTE);
        NodeList submissions = this.getOwnerDocument().getElementsByTagNameNS(XFORMS_NS, "submission");
        for (int i = 0; i < submissions.getLength(); i++)
        {
            SubmissionElementImpl submission = (SubmissionElementImpl)submissions.item(i);
            if (submit_id.equals(submission.getId())) return submission;
        }
        return null;
    }
    
}
