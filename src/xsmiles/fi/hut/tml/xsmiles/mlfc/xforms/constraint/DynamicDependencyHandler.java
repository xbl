/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.constraint;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.DynBoundElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.ModelElementImpl;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.FunctionChangeListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItemListener;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.LookupResult;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.XFormsContext;

/**
 * The handler for dynamic UI dependencies
 * This handler will notify if the bound nodeset changes as well
 * as the bound node value changes.
 * in expression /root[name='mikko']/length, the bound node will be length, and all the name
 * nodes will be referred nodes.
 * @author Mikko Honkala
 */


public class DynamicDependencyHandler
{
    
    /** current dependencies */
    protected Hashtable dependencies = new Hashtable(30);
    protected XFormsContext context;
    protected ModelElementImpl model;
    public static final int DIRTYINITIALSIZE=10;
    
    protected Vector dirtyMIPS=new Vector(DIRTYINITIALSIZE);
    protected Vector dirtyDependencies=new Vector(DIRTYINITIALSIZE);
    
    public DynamicDependencyHandler(XFormsContext aContext, ModelElementImpl aModel)
    {
        context=aContext;
        model=aModel;
    }
    
    public void addDependency(Dependency dep)
    {
        dependencies.put(dep,dep);
    }
    public void removeDependency(Dependency dep)
    {
        dependencies.remove(dep);
    }

    public void addDirtyDependency(Dependency dep)
    {
        // TODO: optimize ?
        if (!this.dirtyDependencies.contains(dep))
        {
            this.dirtyDependencies.addElement(dep);
            //Log.debug("Added dirty dependency. Count is now: "+this.dirtyDependencies.size());
        }
    }
    
    public void addDirtyMIP(Dependency dep)
    {
        // TODO: optimize ?
        if (!this.dirtyMIPS.contains(dep))
        {
            this.dirtyMIPS.addElement(dep);
            //Log.debug("Added dirty MIP for "+dep.getDependencyListener()+". Count is now: "+this.dirtyMIPS.size());
        }
    }
    
    public Vector getAndClearDirtyMIPS()
    {
        Log.debug("getAndClearDirtyMips. Count is now: "+this.dirtyDependencies.size());
        Vector dirtyM =  this.dirtyMIPS;
        this.dirtyMIPS=new Vector(DIRTYINITIALSIZE);
        return dirtyM;
    }
    
    public Vector getAndClearDirtyDependencies()
    {
        Log.debug("getAndClearDirtyDeps. Count is now: "+this.dirtyDependencies.size());
        Vector dirtyDeps =  this.dirtyDependencies;
        this.dirtyDependencies=new Vector(DIRTYINITIALSIZE);
        return dirtyDeps;
    }
    
    public Dependency createDependency(int aBindingType) throws Exception
    {
		Dependency dep;
		if (aBindingType==Dependency.VALUE_BINDING)
		{
			dep = new ValueDependency(aBindingType);
		}
		else
		{
			dep =  new Dependency(aBindingType);//depListener,aBindingType);
		}
        this.addDependency(dep);
        return dep;
    }
    
    /** this is a rather costly operation that executes all XPath statements to
     * see whether they have changed. Call this after insertion or deletion of a 
     * node 
     */
    public void reevaluateDependencies() throws Exception
    {
        for (Enumeration e=dependencies.elements();e.hasMoreElements();)
        {
            Dependency dep = (Dependency)e.nextElement();
            dep.reevaluateBinding();
        }
    }
    
    public void destroy()
    {
        Enumeration e = dependencies.elements();
        while(e.hasMoreElements())
        {
            Dependency dep = (Dependency)e.nextElement();
            dep.destroy();
        }
        dependencies.clear();
    }
    
    public class Dependency implements InstanceItemListener,FunctionChangeListener
    {
        
	    protected LookupResult xpathresult = new LookupResult();
        public static final int SINGLENODE_BINDING = 2;
        public static final int NODESET_BINDING = 3;
        public static final int VALUE_BINDING = 5;
        
        public int bindingType;
        // the nodes which this dependent refers to
        Vector referredNodes=null;//new Vector();
        // the nodelist where this dependend binds to
        NodeList boundNodes;
        protected boolean hasFunctionReferences = false; // e.g. the nodeindex and cursor dynamic functions
        // e.g. the form control
        protected DynamicDependencyListener listener;
        
        // the listener for node value changes (not binding changes)
        protected ValueChangeListener valueChangeListener=new ValueChangeListener();
        
        /** the current binding state, one of the above */
        protected short binding_state=DynBoundElementImpl.UNINITIALIZED;
        
       public static final int VALID_DIRTY = 1 << 1;
       public static final int REQUIRED_DIRTY = 1 << 2;
       public static final int READONLY_DIRTY = 1 << 3;
       public static final int RELEVANT_DIRTY = 1 << 4;
       public static final int VALUE_DIRTY = 1 << 5;
       
       protected static final int defaultDirtyState=0;
       protected static final int afterValueChangeState=VALID_DIRTY|REQUIRED_DIRTY|READONLY_DIRTY|RELEVANT_DIRTY|VALUE_DIRTY; 
       protected int dirtyState=defaultDirtyState;
        
       public Dependency(int aBindingType) //throws Exception
       {
           this.bindingType=aBindingType;
       }
        public void init(DynamicDependencyListener depListener) throws Exception
        {
            this.listener=depListener;
            this.reevaluateBinding();
        }
        
        public DynamicDependencyListener getDependencyListener()
        {
            return this.listener;
        }
        
        /** @return the current binding state */
        public short getBindingState()
        {
            return this.binding_state;
        }
        
        public boolean testDirtyState(int stateids)
        {
            return (this.dirtyState&stateids)>0;
        }
        public void setDirtyState(int stateids,boolean val,boolean addToDirtyList)
        {
            if (val) this.dirtyState|=stateids;
            else this.dirtyState&=~stateids;
            if (addToDirtyList) 
			{
				addDirtyMIP(this);
			}
        }
		
		public boolean hasDirtyFlags()
		{
			return this.dirtyState!=0;
		}
        
        public boolean testAndClearDirtyState(int stateid)
        {
            boolean dirty = this.testDirtyState(stateid);
            this.setDirtyState(stateid,false,false);
            return dirty;
        }
        
        /** @return all the nodes that the xpath expression refers to. Note! this
         * will also return the nodes that are result of the expression */
        
        protected void executeAndGetReferredNodes(DynamicDependencyListener l,LookupResult result) throws Exception
        {
            l.getModelContext().getXPathEngine().evalWithTrace(listener.getContextNode(),listener.getXPath(),
            listener.getNamespaceContextNode(),null,result);
        }
        
        protected NodeList executeAndGetBoundNodeset(DynamicDependencyListener l) throws Exception
        {
            Object obj = listener.getXPathEngine().eval(listener.getContextNode(),
            listener.getXPath(),listener.getNamespaceContextNode(),null);
            if (obj==null||!(obj instanceof NodeList))
                return null;
            return (NodeList)obj;
        }
        
        protected void removeNodeListFromVector(NodeList list, Vector vect)
        {
            for (int i=0;i<list.getLength();i++)
            {
                Node n = list.item(i);
                vect.removeElement(n);
            }
        }
        
        /** dump list contents */
        protected void dumpList(NodeList list)
        {
            if (list==null) 
                Log.debug("list:null");
            else
            {
                Log.debug("List: length:"+list.getLength()+"\n");
                for (int i=0;i<list.getLength();i++)
                {
                    Node n = list.item(i);
                    Log.debug(list.item(i).toString()+" : "+n.getNodeValue());
                    fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsElementImpl.debugNode(n);
                }
            }
        }
        
        /** compares two sets */
        protected boolean isDifferentSet(NodeList nl1, NodeList nl2)
        {
            /*Log.debug("Comparing:");
            dumpList(nl1);
            dumpList(nl2);
             */
            if (nl1==null&&nl2==null) return false;
            else if (nl1==null||nl2==null) return true;
            else if (nl1.getLength()!=nl2.getLength()) return true;
            else
            {
                for (int i=0;i<nl1.getLength();i++)
                {
                    if (!hasNode(nl1.item(i),nl2)) return true;
                }
                return false;
            }
        }
        
        /** see, if a node is contained in a nodelist */
        protected boolean hasNode(Node n, NodeList list)
        {
            for (int i=0;i<list.getLength();i++)
            {
                if (list.item(i)==n) return true;
            }
            return false;
        }
		
		public class RealNodeList implements NodeList
		{
			Node node;
			public RealNodeList(Node n)
			{
				node=n;
			}
			public Node item(int arg0) {
				// TODO Auto-generated method stub
				return node;
			}

			public int getLength() {
				// TODO Auto-generated method stub
				return 1;
			}
			
		}
        
        public NodeList getBoundNodes()
        {
            return this.boundNodes;
        }
        
        public Node getRefNode()
        {
            //if (this.bindingType==this.SINGLENODE_BINDING)
            {
                if (this.boundNodes!=null)
                {
                    if (this.boundNodes.getLength()>0)
                    {
                        return this.boundNodes.item(0);
                    }
                }
            }
            return null;
        }
		
		
		protected NodeList convertToTrueNodeList(NodeList nodes)
		{
			if (!(nodes instanceof Node)) return nodes;
			return new RealNodeList((Node)nodes);
		}
        
        
        /** execute the XPath and see if the bound nodeset has changed.
         * if changed, then update internal listeners and notify the
         * listener
         */
        public void reevaluateBinding() throws Exception
        {
            NodeList oldBoundNodes=this.boundNodes;
            /*
            this.getReferredNodes(this.listener,xpathresult);
            NodeList newBoundNodes = null;
            if (xpathresult.result!=null) newBoundNodes = xpathresult.result.nodelist();
             */
            //Log.debug("Re-evaluating binding for: "+this.listener+"...");
            NodeList newBoundNodes = this.convertToTrueNodeList(this.executeAndGetBoundNodeset(this.listener));
            /*
            Log.debug("... oldBoundNodes length:"+
                    (this.boundNodes!=null?this.boundNodes.getLength():0)+" new nodes length:"+
                    (newBoundNodes!=null?newBoundNodes.getLength():0));
              */              
            // compare the old referred nodes to this new one
            //if (this.isDifferentSet(newBoundNodes,this.listener.getBoundNodeset()))
            if (this.isDifferentSet(newBoundNodes,oldBoundNodes))
                // if changed
            {
                // remove possible previous listeners
                this.removeBoundListeners();
                //this.referredNodes=newRefNodes;
                this.boundNodes=newBoundNodes;
                
                // collect all referred nodes only at first initialization
                if (this.referredNodes==null)
                {
                    //this.referredNodes = this.getReferredNodes(this.listener);
                    this.executeAndGetReferredNodes(this.listener,xpathresult);
                    this.referredNodes = xpathresult.referredNodes;
                    // remove result from referred nodes
                    this.removeNodeListFromVector(newBoundNodes,this.referredNodes);
                    // add instance item listeners to referred nodes
                    Enumeration e = this.referredNodes.elements();
                    while (e.hasMoreElements())
                    {
                        InstanceNode n = (InstanceNode)e.nextElement();
                        InstanceItem i = n.getInstanceItem();
                        if (i!=null)
                            i.addInstanceItemListener(this);
                        //Log.debug("Added ref listener for : "+n.getText());
                    }
                    Vector referredFunctions = xpathresult.referredFunctions;
                    if (referredFunctions!=null && referredFunctions.size()>0)
                    {
                        if (DynamicDependencyHandler.this.context instanceof  XFormsElementHandler)
                        {
                            XFormsElementHandler handler = (XFormsElementHandler)DynamicDependencyHandler.this.context;
                            handler.addNodeIndexListener(this);
                            handler.addNodeIndexListener(model);
							this.hasFunctionReferences=true;
                        }
                    }
                    
                }
                
                // notifyBindingChanged(resultnodes)
                { // DEBUG
					/*
                    if (boundNodes!=null&&boundNodes.getLength()>0)
                    {
                        String text= XFormsUtil.getText(this.getRefNode());
                        Log.debug("The binding has changed:"+text);
                        
                    }                    
                    */
                }
                this.dirtyState=afterValueChangeState;
                addDirtyMIP(this);
                this.listener.notifyBindingChanged(this.boundNodes);
                // bound nodes
				if (boundNodes instanceof Node && boundNodes.item(0)!=boundNodes)
				{
					Log.debug("Dynamic dependencyHandler: boundnodes is a node"+boundNodes);
				}
                //for (int i=0;i<boundNodes.getLength();i++)
                if (boundNodes!=null&&boundNodes.getLength()>0)
                {
                    int i=0;
                    Node no = boundNodes.item(i);
                    // SOMETIMES FOR instance('id'), Xalan returns text nodes!
                    InstanceNode n = (no.getNodeType()==Node.TEXT_NODE?
                    (InstanceNode)boundNodes.item(i).getParentNode():(InstanceNode)boundNodes.item(i));
                    if (n!=null&&n.getInstanceItem()!=null)
                        n.getInstanceItem().addInstanceItemListener(this.valueChangeListener);
                    //Log.debug("Added bound listener for : "+n.getText());
                }
            }
        }
        
        //protected void removeInstanceItemListeners() {
        //}
        
        protected void removeBoundListeners()
        {
			/*
            if (DynamicDependencyHandler.this.context instanceof  XFormsElementHandler)
            {
                XFormsElementHandler handler = (XFormsElementHandler)DynamicDependencyHandler.this.context;
                handler.removeNodeIndexListener(this);
            }*/

            // bound nodes
            if (boundNodes!=null)
                for (int i=0;i<boundNodes.getLength();i++)
                {
                    Node tn = (Node) boundNodes.item(i);
                    if (tn instanceof InstanceNode)
                    {
                        InstanceNode n = (InstanceNode)tn;
						InstanceItem item = n.getInstanceItem();
						if (item!=null)
						{
							item.removeInstanceItemListener(this.valueChangeListener);
						}
                    }
                }
        }
        
        protected void removeRefListeners()
        {
            // referred nodes
            if (referredNodes!=null)
            {
                Enumeration e = referredNodes.elements();
                while(e.hasMoreElements())
                {
                    InstanceNode i = (InstanceNode)e.nextElement();
                    InstanceItem instit =i.getInstanceItem(); 
                    if (instit!=null) i.getInstanceItem().removeInstanceItemListener(this);
                }
                referredNodes.removeAllElements();
            }
        }
        
        public void destroy()
        {
            this.removeBoundListeners();
            this.removeRefListeners();
			
            if (this.hasFunctionReferences && DynamicDependencyHandler.this.context instanceof  XFormsElementHandler)
            {
                XFormsElementHandler handler = (XFormsElementHandler)DynamicDependencyHandler.this.context;
                handler.removeNodeIndexListener(this);
            }
            removeDependency(this);
            dirtyMIPS.remove(this);
            dirtyDependencies.remove(this);

        }
        
        /** An instance item instructs the control to check its status, when the status changes
         */
        public void checkValidity(InstanceItem item)
        {
        }
        
        /** An instance item instructs the control to check its status, when the status changes
         */
        public void checkVisibility(InstanceItem item)
        {
        }
        
        /**
         * notify this listener that there was an error in the value of the
         * instance item. This can be schema validity, constraint, required etc.
         */
        public void notifyError(Exception e,boolean atSubmission)
        {
        }
        
        public void setReadonly(boolean readonly)
        {
        }
        
        public void setRequired(boolean required)
        {
        }
        
        /** The value of this instanceItem has changed
         */
        public void valueChanged(String newValue)
        {
            // this means that we must re-evaluate the nodeset
            try
            {
                addDirtyDependency(this);
                //this.createInstanceItemListeners();
            }
            catch (Exception e)
            {
                Log.error(e);
            }
        }
		

        
        /** this class listens for the change in the referred nodeset*/
        public class ValueChangeListener implements InstanceItemListener
        {
            /** The value of this instanceItem has changed
             */
            public void valueChanged(String newValue)
            {
                Log.debug("DynDep: The ref node value changed to:"+newValue);
                //InstanceItemListener instList = listener.getInstanceItemListener();
                //if (instList!=null) instList.valueChanged(newValue);
                dirtyState=afterValueChangeState;
                addDirtyMIP(Dependency.this);
                
            }
            
            /** An instance item instructs the control to check its status, when the status changes
             */
            public void checkValidity(InstanceItem item)
            {
                setDirtyState(VALID_DIRTY,true,true);
            }
            
            /** An instance item instructs the control to check its status, when the status changes
             */
            public void checkVisibility(InstanceItem item)
            {
                setDirtyState(RELEVANT_DIRTY,true,true);
            }
            /**
             * notify this listener that there was an error in the value of the
             * instance item. This can be schema validity, constraint, required etc.
             */
            public void notifyError(Exception e,boolean atSubmission)
            {
                InstanceItemListener instList = listener.getInstanceItemListener();
                if (instList!=null) instList.notifyError(e,atSubmission);
            }
            
            public void setReadonly(boolean readonly)
            {
                setDirtyState(READONLY_DIRTY,true,true);
            }
            
            public void setRequired(boolean required)
            {
                setDirtyState(REQUIRED_DIRTY,true,true);
            }

			public void checkStyling() {
				// TODO Auto-generated method stub
				
			}
        }


        public int getBindingType()
        {
            // TODO Auto-generated method stub
            return this.bindingType;
        }
        public void functionValueChanged(String function)
        {
            addDirtyDependency(this);
            //model.functionValueChanged(function);
        }
		public void checkStyling() {
			// TODO Auto-generated method stub
			
		}
		public void clearAllDirtyFlags() {
			// TODO Auto-generated method stub
			this.dirtyState=defaultDirtyState;
		}
    }
	
    public class ValueDependency extends Dependency
    {
	       public ValueDependency(int aBindingType) //throws Exception
	       {
	           super(aBindingType);
	       }
		   
	        /** execute the XPath and see if the bound nodeset has changed.
	         * if changed, then update internal listeners and notify the
	         * listener
	         */
	        public void reevaluateBinding() throws Exception
	        {
				this.removeRefListeners();
                this.executeAndGetReferredNodes(this.listener,xpathresult);

                this.referredNodes = xpathresult.referredNodes;
                Enumeration e = this.referredNodes.elements();
                while (e.hasMoreElements())
                {
                    InstanceNode n = (InstanceNode)e.nextElement();
                    InstanceItem i = n.getInstanceItem();
                    if (i!=null)
                        i.addInstanceItemListener(this);
                    //Log.debug("Added ref listener for : "+n.getText());
                }
                Vector referredFunctions = xpathresult.referredFunctions;
                if (referredFunctions!=null && referredFunctions.size()>0)
                {
                    if (DynamicDependencyHandler.this.context instanceof  XFormsElementHandler)
                    {
                        XFormsElementHandler handler = (XFormsElementHandler)DynamicDependencyHandler.this.context;
                        handler.addNodeIndexListener(this);
                        handler.addNodeIndexListener(model);
						this.hasFunctionReferences=true;
                    }
                }
                this.dirtyState=VALUE_DIRTY;
                addDirtyMIP(this);
	        }
    }

    public void markAllUIDependenciesDirty()
    {
        // TODO Auto-generated method stub
        Enumeration deps= this.dependencies.elements();
        while (deps.hasMoreElements())
        {
            this.addDirtyDependency((Dependency)deps.nextElement());
        }
        
    }
}
