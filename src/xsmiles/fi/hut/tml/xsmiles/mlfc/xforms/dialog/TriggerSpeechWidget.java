/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.TriggerElement;


/**
 * @author honkkis
 *
 */
public class TriggerSpeechWidget extends BaseSpeechWidget
{
    static Vector answers=new Vector(4);
    static Vector yesAnswers = new Vector(2);
    static Vector noAnswers = new Vector(2);
    static 
    {
        String a;

        a= "yes";
        answers.addElement(a);
        yesAnswers.addElement(a);
        a= "no";
        answers.addElement(a);
        noAnswers.addElement(a);

    }
    public TriggerSpeechWidget(Element e, FocusHandler handler)
    {
        super(e,handler);
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateDialogQuestion()
     */
    public String generateDialogQuestion()
    {
        String question="Are you sure you want to :"+this.getLabel()+"?";
        return question+"\n";
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateGrammar(boolean)
     */
    public void generateGrammar(Grammar grammar)
    {
        // TODO Auto-generated method stub
        Vector items = answers;
        GrammarGenerator.GenerateSimpleGrammar(items.elements(), grammar);
        //return GrammarGenerator.GenerateSimpleGrammar(focusHandler.getFocusPoints(this.element).elements(),appendHeader);
    }
    
    public boolean interpretResponse(Response r)
    {
        // TODO Auto-generated method stub
        String response=r.response.trim();
        Enumeration e =yesAnswers.elements();
        while (e.hasMoreElements())
        {
            String item = (String)e.nextElement();
            if (item.trim().equalsIgnoreCase(response)) 
            {
                Log.debug("Yes answer selected");
                this.focusHandler.speak("Doing: "+this.getLabel());
                ((TriggerElement)this.element).dispatchActivateEvent();
                try
                {
                Thread.sleep(1000);
                } catch (Exception ex)
                {
                    Log.error(ex);
                }
             }
        }
        this.moveFocusToParent();
        return true;
        //this.showQuestion("I did not understand: "+response+"\n"+this.getQuestion());
        
    }


}
