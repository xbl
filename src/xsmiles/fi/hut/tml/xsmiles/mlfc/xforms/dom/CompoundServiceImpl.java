/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;

import fi.hut.tml.xsmiles.mlfc.css.CSSConstants;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;
import fi.hut.tml.xsmiles.gui.components.XCaption;

import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import java.awt.*;

import org.w3c.dom.css.*;



/**
 * The base class for the XForm controls, such as buttons and textboxes.
 * It uses the abstract component model of X-SMiles in the package
 * fi.hut.tml.xsmiles.gui.components
 *
 * @author Mikko Honkala
 */

public class CompoundServiceImpl implements CompoundService
{
    
    
    /** compound that holds the label and the control */
    protected XLabelCompound compound;
    
    /** the label (caption) component */
    protected XCaption captionComp;
    
    protected XFormsControl control;
    
    protected VisualComponentServiceImpl visualComponent;
    
    public CompoundServiceImpl(XFormsControl ctrl)
    {
        this.control=ctrl;
        this.init();
    }
    public void init()
    {
        this.addCaption();
        
        this.formatComponent(); // these have to be formatted befor compound is created
        this.formatCaption();
        this.compound=this.control.getHandler().getComponentFactory().getXLabelCompound(
        control.component,this.captionComp,this.getCaptionSide()
        );
        this.visualComponent = new VisualComponentServiceImpl();
    }
    /** Returns the string value of the caption-side CSS property */
    protected String getCaptionSide()
    {
        String cs = "top";
        CaptionElementImpl capt = control.getCaption();
        if (capt==null) return cs;
        CSSStyleDeclaration style = capt.getStyle();
        if (style==null) return cs;
        CSSValue val = style.getPropertyCSSValue(CSSConstants.CSS_CAPTION_SIDE_PROPERTY);
        if (val instanceof CSSPrimitiveValue)
        {
            CSSPrimitiveValue pVal = (CSSPrimitiveValue) val;
            return pVal.getStringValue();
        }
        return cs;
    }
    
    
    
    
    //protected final static Dimension minimumCaptionSize = new java.awt.Dimension(50,10);
    public  void addCaption()
    {
        CaptionElementImpl caption = control.getCaption();
        if (caption!=null && control.component!=null)
        {
            XCaption capt = control.getHandler().getComponentFactory().getXCaption(caption.getCaptionText());
            this.captionComp=capt;
            //capt.setMinimumSize(minimumCaptionSize);
        }
    }
    

    
    /**
     * Formats the content according to the CSS style attribute,
     * this can be overridden by the extending classes
     */
    protected void formatComponent()
    {
        // format the content component
        /*
        CSSStyleDeclaration style=control.valuePseudoElement.getStyle();
        if (style==null||this.component==null) return;
        control.component.setStyle(style);
         */
    }
    protected void formatCaption()
    {
        // format the content component
        CaptionElementImpl caption = control.getCaption();
        if (caption==null) return;
        CSSStyleDeclaration style=caption.getStyle();
        if (style==null) return;
        // TODO: change when ComponentWithCaption is removed
        //this.component.setCaptionStyle(style);
        if (this.captionComp!=null) this.captionComp.setStyle(style);
        
    }
    
    public VisualComponentService getVisualComponent()
    {
        return this.visualComponent;
    }
    
    public class VisualComponentServiceImpl implements VisualComponentService
    {	/**
	 * Return the visual component for this extension element
	 */
	public Component getComponent()
        {
            return compound.getComponent();
        }
	
        /**
	 * Returns the approximate size of this extension element
	 */
	public Dimension getSize()
        {
            return compound.getSize();
        }
	public void setZoom(double zoom)
        {
            compound.setZoom(zoom);
        }
	public void setVisible(boolean visible)
        {
            compound.setVisible(visible&&relevant);
        }
	public boolean getVisible()
        {
            return (relevant&&compound.getComponent().isVisible()); // todo
        }
	public boolean relevant=true;
	
	public void setRelevant(boolean r){
	    this.relevant=r;
	    this.setVisible(r&&this.getVisible());
	}

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualComponentService#visualEvent(int)
     */
    public void visualEvent(int event,Object obj)
    {
        // TODO Auto-generated method stub
        
    }
    }
    public void setRelevant(boolean r){
        visualComponent.setRelevant(r);
    }   
    
    
}
