/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Aug 4, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Timer;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.PopupHandler;
import fi.hut.tml.xsmiles.dom.PseudoElement;
import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.VisualComponentService;
import fi.hut.tml.xsmiles.dom.VisualElement;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesStyleSheet;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsLinkException;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.MessageElement;


/**
 * @author honkkis
 * This class implements the <message> element so that the
 * CSS renderer is used to display the message
 * 
 * It can only be used when the host document is rendered by CSS
 * 
 * # if ref node exists || src

    * Create pseudoElement:
          o ContentPseudoElement
                + if ref
                      # Place a new output element with ref="." there
                + if src
                      # Place the content of the file in there
                + ?? If src is HTML/XML document, should we launch a new browser window with that content?
    * Copy possible original content to memory and remove from DOM

# if ref node does not exist

    * Copy possible original content back to DOM

# Create a pseudoElement with a button "Close"

    * when clicked, it will turn the pseudoclass off

 */
public class CSSMessageImpl implements PseudoElementContainerService
{
    protected MessageElement messageElem;
    public final String PSEUDOCLASS_SHOW="xsm-messageon";
    public final String PSEUDOCLASS_PARENTSHOW="xsm-messageparenton";
    Vector pseudoElements;
    MessageContentPseudoElement contentPseudo;
    
    public CSSMessageImpl()
    {
        
    }
    public void setMessage(MessageElement message)
    {
        messageElem=message;
    }
    
    public MessageContentPseudoElement getContentPseudo()
    {
        if (this.contentPseudo==null) this.createContentPseudo();
        return this.contentPseudo;
    }
    public  void closeWindow()
    {
        PopupHandler handler = this.getPopupHandler();
        if (handler!=null) handler.closePopup((StylableElement)messageElem);
        //this.changeWindowPseudoClass(false);
    }
    
    protected static final long EPHEMERAL_TIME=4000;
    
    public void show()
    {
        
        if (messageElem.getRefNode()!=null)
        {
            try
            {
                this.getContentPseudo().setText(messageElem.getTextWithPrecedence());
            }
            catch (XFormsLinkException e)
            {
                Log.error(e);
            }
        }
        //this.changeWindowPseudoClass(true);
        PopupHandler handler = this.getPopupHandler();
        if (handler!=null) handler.showAsPopup((StylableElement)messageElem,messageElem.getLevel());
        if (messageElem.getLevel().equals("ephemeral"))
        {
            this.closeWindow(EPHEMERAL_TIME);
        }
    }
    
    protected PopupHandler getPopupHandler()
    {
        Document doc = messageElem.getOwnerDocument();
        if (doc instanceof ExtendedDocument)
        {
            Object mlfc = ((ExtendedDocument)doc).getHostMLFC();
            if (mlfc instanceof PopupHandler) return (PopupHandler)mlfc;
        }
        return null;
    }
    
    /* not in use anymore
    protected void changeWindowPseudoClass(boolean value)
    {
        VisualElement changeRoot=messageElem;
        // this test reduces flickering 
        if (messageElem.getPseudoClass(PSEUDOCLASS_SHOW)!=value)
        {
	        messageElem.setPseudoClass(PSEUDOCLASS_SHOW,value);
            VisualElement parent = (VisualElement)messageElem.getParentNode();
            // if the parent is e.g. an Action Element, we must set it as visible as well. Note that this is a kludge
            if (value)
            {
                if (!parent.isCurrentlyVisible())
                {
                    parent.setPseudoClass(PSEUDOCLASS_PARENTSHOW,value);
                    changeRoot=parent;
                }
            }
            else
            {
                parent.setPseudoClass(PSEUDOCLASS_PARENTSHOW,value);
                changeRoot=parent;
            }
                
	        changeRoot.styleChanged();
        }        
    }*/
    
    protected Thread closeThread;
    protected void closeWindow(final long msecs)
    {
        if (closeThread==null||!(closeThread.isAlive()))
        {
	        closeThread = new Thread()
	        {
	            public void run()
	            {
	                try
	                {
	                    Log.debug("Waiting "+msecs+" msecs before closing window");
	                    Thread.sleep(msecs);
	                    Log.debug("Closing window");
	                    CSSMessageImpl.this.closeWindow();
	                }
	                catch (Exception e)
	                {
	                    Log.error(e);
	                }
	            }
	        };
	        closeThread.start();
        }
        else
        {
            Log.debug("Closethread already alive.");
        }
    }
    
    protected void createContentPseudo()
    {
        if (contentPseudo==null)
        {
            contentPseudo=new MessageContentPseudoElement(this);
            this.getPseudoElements().addElement(contentPseudo);
        }
       else contentPseudo.refreshContent();
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.PseudoElementContainerService#getPseudoElements()
     */
    public Vector getPseudoElements()
    {
        if (pseudoElements==null)
        {
            pseudoElements=new Vector(2);
            this.createContentPseudo();
        }        
        // TODO Auto-generated method stub
        return pseudoElements;
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.PseudoElementContainerService#notifyPseudoRemoved(org.w3c.dom.Element)
     */
    public void notifyPseudoRemoved(Element elem)
    {
        // TODO Auto-generated method stub
    }
    
 
}
