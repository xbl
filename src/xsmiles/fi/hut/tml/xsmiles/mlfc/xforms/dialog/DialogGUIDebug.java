/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 6, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XLabelCompound;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.gui.components.XTextArea;
import fi.hut.tml.xsmiles.gui.components.awt.AWTComponentFactory;
import fi.hut.tml.xsmiles.speech.Recognizer;
import fi.hut.tml.xsmiles.speech.SpeechFactory;
import fi.hut.tml.xsmiles.speech.Synth;
import fi.hut.tml.xsmiles.speech.XRecognizerListener;
import fi.hut.tml.xsmiles.speech.XResultEvent;


/**
 * This creates an additional Dialog UI layer on top of an XForms document.
 * It is possible to operate thee Dialog UI with a voice recognizer and voice output for instance
 * @author honkkis
 *
 */
public class DialogGUIDebug implements DialogUI, ActionListener, XRecognizerListener
{
    protected JFrame frame;
    protected Container mainPanel,bottomPanel,topPanel,buttonPanel;
	protected static ComponentFactory cfactory;
	protected XTextArea questionArea;
	protected XInput answerInput;
    protected XCaption statusLabel;
	protected XSelectBoolean speechSelect;
	protected XLabelCompound speechCompound;

	protected XSelectBoolean recSelect;
	protected XLabelCompound recCompound;
	
	protected XButton speakButton,stopButton,installButton;

	protected Vector listeners=new Vector();
	
	
    // lets save the latest grammar here, if recognizer is created later
    Reader grammarReader =null;

	
	static DialogGUIDebug instance;
    public static DialogGUIDebug getInstance(ComponentFactory f)
    {
        if (instance==null)
        {
            instance=new DialogGUIDebug();
            if (f!=null) instance.setComponentFactory(f);
            instance.start();
        }
        return instance;
    }
    
	public void start()
	{
	    this.showDialog();
	}
	public void showDialog()
	{
	    if (frame==null)
	    {
	        this.createFrame();
	        this.createListeners();
	        this.frame.setSize(800,600);
	        this.frame.show();
	    }
	}
	

	protected ComponentFactory getComponentFactory()
	{
	    if (cfactory==null)
	    {
	        Log.error("ComponentFactory was null for SignDialog... create a default componentfactory");
	        cfactory=new AWTComponentFactory();
	    }
	    return cfactory;
	}
	
	public void setComponentFactory(ComponentFactory f)
	{
	    this.cfactory=f;
	}

	
	Container createContainer(LayoutManager layout)
	{
	    return new JPanel(layout);
	}
	void createFrame()
	{
	    frame=new JFrame("Speech Dialog");
	    mainPanel=createContainer(new BorderLayout());
	    bottomPanel=createContainer(new BorderLayout());
	    buttonPanel=createContainer(new FlowLayout());
	    
	    questionArea=this.getComponentFactory().getXTextArea("");
	    questionArea.setEditable(false);
	    answerInput=this.getComponentFactory().getXInput();
	    answerInput.setText("");
	    answerInput.addActionListener(this);
	    XCaption capt;
	    
	    // speeh
	    this.speechSelect=this.getComponentFactory().getXSelectBoolean();
	    this.speechSelect.setSelected(false);
	    this.speechSelect.addItemListener(new ItemListener()
	            {

                    public void itemStateChanged(ItemEvent arg0)
                    {
                        enableSpeech(speechSelect.getSelected());
                        
                    }
	            });
	    capt=this.getComponentFactory().getXCaption("Enable speech synthesizer");
	    this.speechCompound=this.getComponentFactory().getXLabelCompound(
	            this.speechSelect,capt,"left");
	    this.buttonPanel.add(this.speechCompound.getComponent());

	    // rec
	    this.recSelect=this.getComponentFactory().getXSelectBoolean();
	    this.recSelect.setSelected(false);
	    this.recSelect.addItemListener(new ItemListener()
	            {

                    public void itemStateChanged(ItemEvent arg0)
                    {
                        enableRecognizer(recSelect.getSelected());
                        
                    }
	            });
	    
	    capt=this.getComponentFactory().getXCaption("Enable speech recognizer");
	    this.recCompound=this.getComponentFactory().getXLabelCompound(
	            this.recSelect,capt,"left");
	    this.buttonPanel.add(this.recCompound.getComponent());

        this.statusLabel=this.getComponentFactory().getXCaption("Dialog started.");
        
	    // speak button
	    this.installButton=this.getComponentFactory().getXButton("Install",null);
	    this.speakButton=this.getComponentFactory().getXButton("Speak",null);
        this.stopButton=this.getComponentFactory().getXButton("Stop",null);
        this.speakButton.setEnabled(false);
        this.stopButton.setEnabled(false);
	    this.buttonPanel.add(this.speakButton.getComponent());
        this.buttonPanel.add(this.stopButton.getComponent());
        this.buttonPanel.add(this.installButton.getComponent());
        this.buttonPanel.add(this.statusLabel.getComponent());
	    this.speakButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent arg0)
            {
                speakButtonPressed();
                
            }});
        this.stopButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent arg0)
            {
                stopButtonPressed();
                
            }});
	    this.installButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent arg0)
            {
            	fireEvent(new DialogActionEvent(this,0,"installAll"));
                
            }});
	    
	    
	    bottomPanel.add(answerInput.getComponent(),BorderLayout.CENTER);

	    mainPanel.add(questionArea.getComponent(),BorderLayout.CENTER);
	    mainPanel.add(bottomPanel,BorderLayout.SOUTH);
	    bottomPanel.add(buttonPanel,BorderLayout.SOUTH);

        addToFrame(mainPanel);
	}
	

    protected void speakButtonPressed()
    {
        if (this.synth!=null)
        {
            this.synth.cancelAll();
        }
        this.getOneResultAsync();
    }
    protected void stopButtonPressed()
    {
        if (this.recognizer!=null)
        {
            this.recognizer.stopRecognizing();
        }
        if (this.synth!=null)
        {
            this.synth.cancelAll();
        }
    }
    private void getOneResultAsync()
    {
        if (this.recognizer!=null)
        {
            this.recognizer.stopRecognizing();
            Log.debug("Starting async recognizer");
            this.speakButton.setEnabled(false);
            this.recognizer.getOneResultAsync();
        }
    }

    
    protected void speakButtonPressedSync()
	{
	    if (this.recognizer!=null)
	    {
	        if (this.synth!=null) this.synth.cancelAll();
	        String res = this.recognizer.getOneResultSync();
	        if (res!=null)
	        {
	            Log.debug("DialogGUI: you uttered: "+res);
	            ActionEvent ev = new ActionEvent(this,0,res);
	            if (this.synth!=null) this.synth.cancelAll();
	            this.questionArea.append("[(SPOKEN) "+this.answerInput.getText()+"]\n");
	            // TODO Auto-generated method stub
	            this.fireEvent(ev);
	        }
	    }
	}
	
	
	
	protected Synth synth;
	protected void enableSpeech(boolean enable)
	{
	    if (enable)
	    {
	        if (synth==null) synth=SpeechFactory.getSynth();
	        ActionEvent ev = new ActionEvent(this,0,"again");
	        this.fireEvent(ev);
	    }
	    /* FOR SOME REASON DESTROY/RESTART DOES NOT WORK
	    else
	    {
	        if (synth!=null) synth.destroy();
	        synth=null;
	    }
	     */   
	}
	
	protected Recognizer recognizer;
	protected void enableRecognizer(boolean enable)
	{
	    if (enable)
	    {
	        try
	        {
		        if (recognizer==null) recognizer=SpeechFactory.getRecognizer();
	            if (this.recognizer!=null)
                {
                    if ( this.grammarReader!=null)
                    {
                        this.recognizer.setGrammar(this.grammarReader);
                        this.recognizer.addXResultListener(this);
                        this.speakButton.setEnabled(true);
                        this.stopButton.setEnabled(true);
                        this.statusLabel.setText("Recognizer ready.");
                    }
                }
                
	        } catch (Exception e)
	        {
	            Log.error(e);
	        }
	    }
	    /* FOR SOME REASON DESTROY/RESTART DOES NOT WORK
	    else
	    {
	        if (synth!=null) synth.destroy();
	        synth=null;
	    }
	     */   
	}
	
	protected void speak(String s)
	{
	    if (synth!=null&&this.speechSelect.getSelected())
	    {
	        //synth.cancelAll();
	        synth.speak(s,false);
	    }
	}
	
	void addToFrame(Component c)
	{
	    frame.getContentPane().add(c);
	}

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#showQuestion(java.lang.String)
     */
    public void showQuestion(String question, Reader grammar)
    {
        // TODO Auto-generated method stub
        
    }
    protected void fireEvent(ActionEvent e)
    {
        Enumeration enumeration = this.listeners.elements();
        while (enumeration.hasMoreElements())
        {
            try
            {
                ((ActionListener)enumeration.nextElement()).actionPerformed(e);
            }
            catch (Throwable t)
            {
                Log.error(t);
            }
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#addActionListener(java.awt.event.ActionListener)
     */
    public void addActionListener(ActionListener l)
    {
        if (!this.listeners.contains(l))
            this.listeners.addElement(l);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#removeActionListener(java.awt.event.ActionListener)
     */
    public void removeActionListener(ActionListener l)
    {
        // TODO Auto-generated method stub
        this.listeners.removeElement(l);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#destroy()
     */
    public void destroy()
    {
        // TODO Auto-generated method stub
        if (this.frame!=null) this.frame.dispose();
        this.frame=null;
        instance=null;
    }

    /* This event is received when the user has typed in something
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent arg0)
    {
        Log.debug("action Performed: "+arg0);
        ActionEvent ev = new ActionEvent(this,0,this.answerInput.getText());
        if (this.synth!=null) this.synth.cancelAll();
        this.questionArea.append("["+this.answerInput.getText()+"]\n");
        // TODO Auto-generated method stub
        this.fireEvent(ev);
        this.answerInput.setText("");
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#append(java.lang.String)
     */
    public void append(String text)
    {
        // TODO Auto-generated method stub
        this.questionArea.append(text);
        // move the scrollbar to the end
        this.questionArea.setCaretPosition(this.questionArea.getText().length());
        this.speak(text);
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#setGrammar(java.io.Reader)
     */
    public void setGrammar(Reader r)
    {
        try
        {
            if (this.recognizer!=null) this.recognizer.setGrammar(r);
            else grammarReader=r;
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    protected void createListeners()
    {
    	this.registerKeyboardAction(
    			KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, InputEvent.CTRL_MASK)
    			,new ActionListener()
    			{
			        // Actions from the buttons, back, forward, etc
			        public void actionPerformed(ActionEvent e)
			        {
			            Log.debug("menu actionPerformed: " + e);
                        if (speakButton.getEnabled())
                            speakButtonPressed();
                        else stopButtonPressed();
			        }
    			}
    			);
    }

    protected void registerKeyboardAction(KeyStroke stroke, ActionListener listener)
    {
    	JComponent root=null;
    	if (this.frame!=null) root=(JComponent) frame.getContentPane();
    	if (root!=null)
    	{
	    	root.registerKeyboardAction(listener
	    	/*
	    		new ActionListener()
	            {
			        // Actions from the buttons, back, forward, etc
			        public void actionPerformed(ActionEvent e)
			        {
			            Log.debug("menu actionPerformed: " + e);
			            action(command);
			        }
			    }*/
	    		,"keyboard command",
	            stroke,JComponent.WHEN_IN_FOCUSED_WINDOW);
    	}
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.XResultListener#resultEvent(fi.hut.tml.xsmiles.speech.XResultEvent)
     */
    public void resultEvent(XResultEvent e)
    {
        ActionEvent ev = new ActionEvent(this,0,e.result);
        if (this.synth!=null) this.synth.cancelAll();
        this.questionArea.append("[(SPOKEN) "+this.answerInput.getText()+"]\n");
        // TODO Auto-generated method stub
        this.speakButton.setEnabled(true);
        this.fireEvent(ev);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.speech.XRecognizerListener#statusChange(short, java.lang.String)
     */
    public void statusChange(short statusCode, String statusMessage)
    {
        Log.debug("Status change: "+statusMessage+" :"+statusCode);
        this.statusLabel.setText(statusMessage);
        if (statusCode==XRecognizerListener.statusIdle)
        {
            this.speakButton.setEnabled(true);
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#stopSpeech()
     */
    public void stopSpeech()
    {
        if (this.synth!=null)
        {
            this.synth.cancelAll();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#stopRecognizer()
     */
    public void stopRecognizer()
    {
        if (this.recognizer!=null)
        {
            this.recognizer.stopRecognizing();
        }
        this.speakButton.setEnabled(true);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.DialogUI#stopAll()
     */
    public void stopAll()
    {
        this.stopSpeech();
        this.stopRecognizer();
        
    }

}
