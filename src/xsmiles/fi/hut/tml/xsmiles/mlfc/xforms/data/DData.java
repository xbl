/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.xforms.data;
import fi.hut.tml.xsmiles.Log;

/**
 * This class encapsulates the handling of invalid values, otherwise
 * the subclasses will handle all conversion
 */
public abstract class DData implements Data{

    
    private String invalidValue;
    private boolean isValid=true;
    protected short datatype;
    
	public DData(short dtype)
	{
        this.datatype=dtype;
	}
    
    public short getDataType()
    {
        return this.datatype;
    }
    
        /**
     * is the current value valid according to the datatype
     */
    public boolean isValid()
    {
        return this.isValid;
    }

    /**
     * get the invalid value as a string
     */
    public String getInvalidValue()
    {
        return invalidValue;
    }
    
    /**
     * set the validity status
     */
    public void setValid(boolean v)
    {
        this.isValid=v;
        if (v) this.invalidValue=null;
    }
    
    public void setInvalidString(String s)
    {
        this.invalidValue=s;
        this.isValid=false;
        this.clearValue();
    }
    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
    public final String toDisplayValue()
    {
        if (this.isValid())
            return this.toDisplayValueInternal();
        else return this.invalidValue;
    }
    
    /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    public String toSchemaString()
    {
        if (this.isValid())
            return this.toSchemaStringInternal();
        else
            return this.invalidValue;
    }
    
    /**
     * get the java object corresponding to the value
     */
    public Object toObject()
    {
        return this.toObjectInternal();
    }
    /**
     * set the value as a Java Object
     */ 
    public final void setValueFromObject(Object obj)
    {
        this.setValueFromObjectInternal(obj);
        this.setValid(true);
    }
    
    /**
     * set the value from a display string
     */ 
    public final void setValueFromDisplay(String displayValue)
    {
        boolean valid = this.setValueFromDisplayInternal(displayValue);
        if (valid)
        {
            this.setValid(true);
        }
        else
        {
            this.setInvalidString(displayValue);
        }
    }
    
    /**
     * set the value from a Schema string
     */ 
    public final void setValueFromSchema(String displayValue)
    {
        this.setValueFromSchemaInternal(displayValue);
    }
    /** instructs subclass to set value to null, this is used for
     * invalid values
     */
    protected abstract void clearValue();
    /**
     * get the display value (e.g. 3,12 / 3.12 depending on the locale)
     */
   protected abstract String toDisplayValueInternal();
    /**
     * internal method for setting the value as a Java Object
     */ 
    protected abstract void setValueFromObjectInternal(Object obj);
    
    /**
     * internal method for setting the value from a display string
     */ 
    protected abstract boolean setValueFromDisplayInternal(String displayValue);
    
    /**
     * internal method for setting the value from a Schema string
     */ 
    protected abstract void setValueFromSchemaInternal(String displayValue);
    
        /**
     * get a schema compatible string from this data. E.g.
     * From a date you would get a String in xsd:date format
     */
    protected abstract String toSchemaStringInternal();
    
    /**
     * get the java object corresponding to the value
     */
    protected abstract Object toObjectInternal();


} // RangeDecimal
