/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Nov 9, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.xforms.dialog;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.LabeledElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectElement;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.SelectionItem;


/**
 * @author honkkis
 *
 */
public class SelectSpeechWidget extends BaseSpeechWidget
{
    public SelectSpeechWidget(Element e, FocusHandler handler)
    {
        super(e,handler);
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateDialogQuestion()
     */
    public String generateDialogQuestion()
    {
        String question=this.getLabel()+"."+this.getValueAsString(this.element)+". Select from: ";
        if (this.element instanceof SelectElement)
        {
            SelectElement select = (SelectElement)this.element;
            Enumeration e =select.getItems().elements();
            while (e.hasMoreElements())
            {
                SelectionItem item = (SelectionItem)e.nextElement();
                question+="\""+removeNonAlphanumeric(item.getLabel().trim())+"\", ";
            }
            return question+"\n";
        }
        
        return "ERROR: Could not generate question from the node: "+element;
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.xforms.dialog.SpeechWidget#generateGrammar(boolean)
     */
    public void generateGrammar(Grammar grammar)
    {
        // TODO Auto-generated method stub
        Vector items = ((SelectElement)this.element).getItems();
        GrammarGenerator.GenerateSimpleGrammar(items.elements(), grammar);
        //return GrammarGenerator.GenerateSimpleGrammar(focusHandler.getFocusPoints(this.element).elements(),appendHeader);
    }
    
    public boolean interpretResponse(Response r)
    {
        // TODO Auto-generated method stub
        String response=r.response.trim();
        if (this.element instanceof SelectElement)
        {
            SelectElement select = (SelectElement)this.element;
            Enumeration e =select.getItems().elements();
            while (e.hasMoreElements())
            {
                SelectionItem item = (SelectionItem)e.nextElement();
                if (removeNonAlphanumeric(item.getLabel().trim()).equalsIgnoreCase(response)) 
                {
                    String reply =((LabeledElement)this.element).getLabelAsText()+". You selected: "+item.getLabel(); 
                    Log.debug(reply);
                    this.focusHandler.speak(reply);
                    item.select();
                    this.moveFocusToParent();
                    return true;
                }
            }
        }
        return false;
        //this.showQuestion("I did not understand: "+response+"\n"+this.getQuestion());
        
    }


}
