/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import fi.hut.tml.xsmiles.mlfc.xforms.constraint.Vertex;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.PropertyInheriter;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.xforms10.*;

// Element implementation 
import org.apache.xerces.dom.AttrImpl;
//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;

/**
 *
 */
public class BasicInstanceAttrImpl extends AttrImpl implements InstanceNode{
	//InstanceDocument ownerDoc = null;
	InstanceItem instanceItem;
            
	protected BasicInstanceAttrImpl(BasicInstanceDocumentImpl ownerDocument, java.lang.String name)  
	{
		super(ownerDocument,name);
		instanceItem=new InstanceItem(this);
	}
	public InstanceItem getInstanceItem()
	{
		return this.instanceItem;
	}
	public PropertyInheriter getPropertyInheriter()
	{
		return this.instanceItem;
	}

	
	public Object clone()
	{
		try
		{
			BasicInstanceAttrImpl node=(BasicInstanceAttrImpl)super.clone();
			node.instanceItem=new InstanceItem(node);
			return node;
		} catch (Exception e)
		{
			Log.error(e);
			return null;
		}
	}
	
	
	
}

