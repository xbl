/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceNode;
import fi.hut.tml.xsmiles.mlfc.xforms.instance.InstanceItem;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.*;

import java.util.Vector;


import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;


/**
 * XForm/SubmitInfo element
 * @author Mikko Honkala
 */

public class SchemaElementImpl extends PrologElement {
	
	protected Element schemaElement;
	
    public SchemaElementImpl(XFormsElementHandler owner, String ns, String name) {
    	super(owner, ns, name);
    }
	
    public String getHref()
    {
	return this.getAttribute(XFormsConstants.EXTERNAL_LINKING_ATTR);
    }
	
    public void init()
    {
        if (this.getModel()!=null)
        {
            this.getModel().schemaAdded(this);
        }
        super.init();
    }

	
    /**
     * Revalidates the whole instance.
     * @return true, if the instance is valid, false otherwise
     */
    /*
     NOT IN USE
    public Vector revalidate(Document doc)
    {
	if (validator==null) 
		validator=new XercesValidator(this.getModel(),this.getHref());
	return validator.revalidate(doc);
    }*/
	
	// TODO: remove this method
	/*
    public void validationFailed(Node n,String error)
    {
		InstanceNode instancenode=this.getModel().getInstanceNode(n);
		instancenode.getInstanceItem().setSchemaValid(false);
		getBrowser().getCurrentGUI().setStatusText("XForms validation error: "+error);
    }
	*/
}
