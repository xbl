/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.instance;

import java.util.Enumeration;
import java.util.Vector;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.QName;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConfiguration;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.mlfc.xforms.data.DString;
import fi.hut.tml.xsmiles.mlfc.xforms.data.Data;

//import fi.hut.tml.xsmiles.mlfc.smil.viewer.sparser.ElementNSImpl;



/**
 *
 */
public class InstanceItem implements PropertyInheriter
{
    InstanceNode instanceNode = null;
    protected Vector controls;
    
    /** Other constraint values, calculated from the binds */
    protected boolean
    local_relevant=true, inherited_relevant = true,
    local_readonly=false, inherited_readonly = false,
    required=false, xformsvalid=true, schemavalid=true;
    
    /** The file name of the upload if it exists*/
    protected String uploadFileName;
        

    /** For URI uploads the URI of this attachment, if it exists*/
    protected String attachmentURI;
    /** For URI uploads the attachment, if it exists*/
    protected byte[] attachment;
    /** For URI uploads the attachment encoding, if it exists*/
    protected String attachmentMime;
    /** For URI uploads the attachment filename, if it exists*/
    protected String attachmentFilename;
    
    /** the last schema error for this item */
    protected String schemaError;
    
    /** the datamapper item for this value */
    protected Data dataMapper;
    
    /** the xsi:type implementation for this item, may be null */
    protected XsiType xsiType;
    
    /** the string value of bind's type attribute that should be used for this item */
    protected String typeLocalName;
    protected String typeNamespace;
    
    /**
     * Constructor - Set the owner, name and namespace.
     */
    public InstanceItem(InstanceNode owner)
    {
        this.instanceNode= owner;
    }
    
    /** the last schema error for this item */
    public String getSchemaError()
    {
        return schemaError;
    }
    /** the last schema error for this item */
    public void setSchemaError(String error)
    {
        schemaError = error;
    }
    
        /*
         * XFORMS InfoSet additions
         */
    public String toString()
    {
        return "InstanceElementImpl: "+super.toString();
    }
    
    /**
     * Set the xforms:type property
     */
    public void setXFormsType(String local,String ns)
    {
        this.typeLocalName=local;
        this.typeNamespace=ns;
    }
    
    /** inherit this property to children */
    public void inheritToChildren(short property, boolean value)
    {
        if (instanceNode.getNodeType()==Node.ELEMENT_NODE)
        {
	        // CHILD ELEMENTS
	        Node child=instanceNode.getFirstChild();
	        while(child!=null)
	        {
	            if (child instanceof InstanceNode)
	            {
	                InstanceNode elementchild = (InstanceNode)child;
	                elementchild.getPropertyInheriter().inheritProperty(property,value);
	            }
	            child=child.getNextSibling();
	        }
	        // ATTRIBUTES
	        NamedNodeMap attrs = instanceNode.getAttributes();
	        for (int i=0;i<attrs.getLength();i++)
	        {
	            Attr attr = (Attr)attrs.item(i);
	            if (attr instanceof InstanceNode)
	            {
	                ((InstanceNode)attr).getPropertyInheriter().inheritProperty(property,value);
	            }
	        }
        }
    }
    
    /**
     * Set the relevant property.
     */
    public void setRelevant(boolean a_relevant)
    {
        this.local_relevant=a_relevant;
        this.checkRelevant();
    }
    protected void checkRelevant()
    {
        boolean computed_relevant = this.getRelevant();
        if (controls!=null)
        {
            Enumeration e=controls.elements();
            while (e.hasMoreElements())
            {
                InstanceItemListener control=(InstanceItemListener)e.nextElement();
                control.checkVisibility(this);
            }
        }
        this.instanceNode.getPropertyInheriter().inheritToChildren(PropertyInheriter.RELEVANT_PROPERTY,computed_relevant);
    }
    /**
     * Get the relevant property.
     */
    public boolean getRelevant()
    {
        return (this.local_relevant&&this.inherited_relevant);
    }
    
    /**
     * Set the xformsvalid property.
     */
    public void setXFormsValid(boolean a_isvalid)
    {
        boolean atSubmission = false; // TODO
        if (!a_isvalid)
            this.notifyError(new Exception("Node '"+this.instanceNode.getNodeName()+"' does not meet XForms 'constraint' model item property."),atSubmission);

        // do nothing, if no change of state
        if (a_isvalid==this.xformsvalid) return;
        this.xformsvalid=a_isvalid;
        //		Log.debug("***** isxformsvalid for: "+this.instanceNode+" set to:"+xformsvalid);
        validitySet();
    }
    /**
     * Get the xformsvalid property+required property.
     */
    public boolean getXFormsValid()
    {
        if (xformsvalid==false)
        {
            return false;
        }
        else if (required==false) return true;
        else
        {
            // if required = true, check first the content (should be non null)
            //The value of the bound instance data node must be convertible 
            // to an XPath string with a length greater than zero.
            boolean hasValue = this.hasValue();
            
            if (hasValue==false) return false;
            // If the bound instance data node is an element, 
            // the element must not have the xsi:nil attribute set to true.
            if (this.getXSInil()) return false;    
            return true;
            
        }
    }
    
    public boolean getValid()
    {
        return this.getSchemaValid()&&this.getXFormsValid();
    }
    
    public static final String XSINS = "http://www.w3.org/2001/XMLSchema-instance";
    public boolean getXSInil()
    {
        if (instanceNode.getNodeType()==Node.ELEMENT_NODE)
        {
	        Attr nilattr = ((Element)instanceNode).getAttributeNodeNS(XSINS,"nil");
	        if (nilattr==null) return false;
	        String nilval = nilattr.getNodeValue();
	        if (nilval.equals("true")||nilval.equals("1")) return true;
	        else return false;
        }
        return false;
    }
    
    
    protected boolean hasValue()
    {
        // TODO: check whether this item has some value
        String text =XFormsUtil.getText( this.instanceNode);
        Log.debug("Checking whether this has value: '"+text+"'");
        return (text!=null&&text.length()>0);
    }
    
    /**
     * Set the schemavalid property.
     */
    public void setSchemaValid(boolean a_isvalid)
    {
        // do nothing, if no change of state
        if (a_isvalid==this.schemavalid) return;
        this.schemavalid=a_isvalid;
        //		Log.debug("***** schemavalid for: "+this.instanceNode+" set to:"+schemavalid);
        validitySet();
    }
    
    /**
     * This method is called internally when the validity status changes
     */
    protected void validitySet()
    {
        if (controls==null) return;
        Enumeration e=controls.elements();
        while (e.hasMoreElements())
        {
            InstanceItemListener control=(InstanceItemListener)e.nextElement();
            
            // A control is valid only if schema and xforms is valid
            control.checkValidity(this);
        }
    }
    
    /**
     * Get the isxformsvalid property.
     */
    public boolean getSchemaValid()
    {
        return schemavalid;
    }
    
    /**
     * Set the required property.
     */
    public void setRequired(boolean a_required)
    {
        this.required=a_required;
        if (controls==null) return;
        Enumeration e=controls.elements();
        while (e.hasMoreElements())
        {
            InstanceItemListener control=(InstanceItemListener)e.nextElement();
            control.setRequired(this.required);
        }
        boolean atSubmission=false; // TODO
        this.notifyError(new Exception("Node '"+this.instanceNode.getNodeName()+"' does not meet XForms 'required' model item property."),atSubmission);
    }
    /**
     * Get the required property.
     */
    public boolean getRequired()
    {
        return required;
    }
    /**
     * Set the readonly property.
     */
    public void setReadonly(boolean a_readonly)
    {
        // what if the value does not change?
        this.local_readonly=a_readonly;
        this.checkReadonly();
    }
    /**
     * tell controls to update state + inherit to children
     */
    protected void checkReadonly()
    {
        boolean computed_readonly=this.getReadonly();
        if (controls!=null)
        {
            Enumeration e=controls.elements();
            while (e.hasMoreElements())
            {
                InstanceItemListener control=(InstanceItemListener)e.nextElement();
                control.setReadonly(computed_readonly);
            }
        }
        this.instanceNode.getPropertyInheriter().inheritToChildren(PropertyInheriter.READONLY_PROPERTY,computed_readonly);
    }
    /**
     * Get the readonly property.
     */
    public boolean getReadonly()
    {
        return (local_readonly||inherited_readonly);
    }
    
    public String getUploadFileName()
    {
        return uploadFileName;
    }
    
    public void setUploadFileName(String f)
    {
        uploadFileName=f;
    }
    
    public void addInstanceItemListener(InstanceItemListener control)
    {
        if (this.controls==null) this.controls=new Vector();
        this.controls.addElement(control);
        //		Log.debug("Control: "+control+" registered to "+this.instanceNode+" relevant="+relevant);
        control.checkVisibility(this);
        control.checkValidity(this);
        if (local_readonly) control.setReadonly(true);
    }
    public void removeInstanceItemListener(InstanceItemListener control)
    {
        if (this.controls==null) return;
        this.controls.removeElement(control);
    }
    
    public void notifyValueChanged()
    {
        Log.debug("Instanceitem: notifyValueChanged() called");
        // here we should get all changes to the node and should dispatch
        // node revalidation
        {
            boolean valid=true;
            try
            {
                Object value = this.revalidate();
                this.updateDataMapperValue(value);
            } catch (InvalidDatatypeValueException e)
            {
                this.setSchemaValid(false);
                this.setSchemaError(e.getMessage());
                Log.debug("Schema error: "+this.getSchemaError());
                boolean atSubmission = false; //todo
                this.notifyError(e,atSubmission);
                this.updateDataMapperInvalid(XFormsUtil.getText(this.instanceNode));
                //Log.debug("InstanceItem.notifyValueChanged got this error."+e.getMessage());
                valid=false;
            }
            this.setSchemaValid(valid);
        }
        // required property
        if (this.required) 
        {
            boolean atSubmission=false; // TODO
            boolean hasValue = this.hasValue();
            if (!hasValue) this.notifyError(new Exception("Node '"+this.instanceNode.getNodeName()+"' does not meet XForms 'required' model item property."),atSubmission);
            validitySet();
        }
        
        if (controls==null) return;
        String newVal = this.getText();
        for (int i=0;i<controls.size();i++)
        {
            InstanceItemListener control=(InstanceItemListener)controls.elementAt(i);
            control.valueChanged(newVal);
        }
    }
    
    public String getText()
    {
        String newVal = XFormsUtil.getText(instanceNode);
        return newVal;
        
    }
    
    /** the instance element may signal me that xsi:nil has changed
     * then if required=true, validity must be reassessed 
     */
    public void nofityXSInilChanged(boolean value)
    {
        if (this.required)
        {
            this.validitySet();
        }
    }
    
    /**
     * notify this item that there was an error in the value of the
     * instance item. This can be schema validity, constraint, required etc.
     */
    public void notifyError(Exception e,boolean atSubmission)
    {
        Log.debug("InstanceItem '"+this.instanceNode.getNodeName()+"' error: "+e.getMessage());
        if (controls==null) return;
        for (int i=0;i<controls.size();i++)
        {
            InstanceItemListener control=(InstanceItemListener)controls.elementAt(i);
            control.notifyError(e,atSubmission);
        }
    }
    public static final int TYPE_UNREAD=-888;
    protected int primitiveTypeId=TYPE_UNREAD;
    /** PSVI type methods */
    public int getPrimitiveTypeId()
    {
        // we suppose that once primitive type has
        // been read, it will not change
        if (primitiveTypeId==TYPE_UNREAD)
        {
            boolean hasXsiType=false;
            String type=null;
            
            if (XFormsConfiguration.getInstance().shouldHandleXsiType())
            {

            /*
             // xsi type is now handled by xerces 2.3.0
             // BUT for XForms basic we still have to handle xsi:type ourselves
              */
	             final String xsins = "http://www.w3.org/2001/XMLSchema-instance";
	            Attr xsiAttr = null;
	            if (this.instanceNode instanceof Element)
	                xsiAttr =  ((Element)this.instanceNode).getAttributeNodeNS(xsins,"type");
	            if (xsiAttr!=null)
	            {
	                // xsi:type attribute found
	                hasXsiType=true;
	                type = xsiAttr.getNodeValue();
	                QName qname=XFormsUtil.getQName(type,(Element)this.instanceNode);
	                InstanceDocument doc = (InstanceDocument)this.instanceNode.getOwnerDocument();
	                SchemaPool pool = (SchemaPool)doc.getSchemaPool();
	                this.xsiType = XFormsConfiguration.getInstance().createXsiType(qname.nsURI,qname.localName,pool);
	                this.primitiveTypeId=xsiType.getPrimitiveTypeId();
	            }
            }
            if (!hasXsiType)
            {
	            if (this.typeLocalName!=null)
	            {
	                InstanceDocument doc = (InstanceDocument)this.instanceNode.getOwnerDocument();
	                SchemaPool pool = (SchemaPool)doc.getSchemaPool();
	                this.xsiType = XFormsConfiguration.getInstance().createXsiType(this.typeNamespace,this.typeLocalName,pool);
	                this.primitiveTypeId=xsiType.getPrimitiveTypeId();
	            }
	            else
	            {
	                // use PSVI from DOM
	                this.primitiveTypeId = XFormsConfiguration.getInstance().getPSVI().getPrimitiveTypeId(this.instanceNode);
	            }
            }
        }
        return this.primitiveTypeId;
    }
    
    public Object revalidate() throws InvalidDatatypeValueException
    {
        //Log.debug("Revalidating single node: "+this.instanceNode);
        if (this.xsiType!=null)
        {
            // xsi:type
            String s =XFormsUtil.getText( this.instanceNode);
            return xsiType.validateString(s);
        }
        else
        {
            // USE PSVI from DOM
            return XFormsConfiguration.getInstance().getPSVI().validateElement(this.instanceNode);
        }
    }
    protected void createDataMapper()
    {
        this.dataMapper = XFormsConfiguration.getInstance().getDataFactory().createData(this.getPrimitiveTypeId());
        try
        {
            Object obj = this.revalidate();
            this.updateDataMapperValue(obj);
        } catch (InvalidDatatypeValueException e)
        {
            Log.debug(e.getMessage());
            updateDataMapperInvalid(XFormsUtil.getText(this.instanceNode));
        } catch (NullPointerException e)
        {
            Log.debug(e.getMessage());
            updateDataMapperInvalid(XFormsUtil.getText(this.instanceNode));
        }
    }
    
    protected void updateDataMapperInvalid(String value)
    {
        if (this.dataMapper!=null) this.dataMapper.setInvalidString(value);
    }
    protected Object updateDataMapperValue(Object value)
    {
        Object val;
        if (this.getPrimitiveTypeId()==-1||this.dataMapper instanceof DString||value==null)
            val = XFormsUtil.getText(this.instanceNode);
        else
            // create method toObject which does not do other revalidating
            val=value;//this.revalidate();
        if (this.dataMapper!=null)
        {
            this.dataMapper.setValueFromObject(val);
            this.dataMapper.setValid(true);
        }
        return val;
    }
    
    public Data getData()
    {
        if (this.dataMapper==null)
            this.createDataMapper();
        return this.dataMapper;
    }
    
    /** the parent's readonly status has changed
     * Inheritance Rules: If any ancestor node evaluates to true,
     * this value is treated as true. Otherwise, the local value is used.
     * Note:
     * This is the equivalent of taking the logical OR of the evaluated readonly property on the local and every ancestor node.
     *
     * the parent's relevant status has changed
     * Inheritance Rules:  If any ancestor node evaluates to XPath false, this value is treated as false.
     * Otherwise, the local value is used.
     * Note:
     * This is the equivalent of taking the logical AND of the evaluated relevant property on the local and every ancestor node.
     */
    public void inheritProperty(short property,boolean value)
    {
        if (property==PropertyInheriter.RELEVANT_PROPERTY)
        {
            if (inherited_relevant==value) return;
            this.inherited_relevant=value;
            this.checkRelevant();
        }
        else if (property==PropertyInheriter.READONLY_PROPERTY)
        {
            // if the value does not change, no-op
            if (inherited_readonly==value) return;
            this.inherited_readonly=value;
            this.checkReadonly();
        }
    }
    
    public void createURIAttachment(byte[] data, String mediaType, String filename,boolean createURI)
    {
        this.attachment=data;
        this.attachmentMime = mediaType;
        this.attachmentFilename = filename;
        if (createURI)
        {
            this.attachmentURI = this.createAttachmentURI();
            XFormsUtil.setText(this.instanceNode,attachmentURI);
        }
    }
    
    protected String createAttachmentURI()
    {
        int random = (int)(Math.random()*1000000);
        return "cid:X"+random+"@xsmiles.org";
    }
    
    public byte[] getAttachment()
    {
        return this.attachment;
    }
    public String getAttachmentURI()
    {
        return this.attachmentURI;
    }
    public String getAttachmentMime()
    {
        return this.attachmentMime;
    }
    public String getAttachmentFilename()
    {
        return this.attachmentFilename;
    }
    public String getAttachmentTransfer()
    {
        return "binary";
    }
    
    
}

