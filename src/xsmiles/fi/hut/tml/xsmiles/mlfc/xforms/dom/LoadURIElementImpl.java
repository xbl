/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xforms.dom;


import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.mlfc.xforms.XFormsElementHandler;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsConstants;
import fi.hut.tml.xsmiles.mlfc.xforms.XFormsUtil;
import fi.hut.tml.xsmiles.dom.EventHandlerService;

import java.net.URL;

import org.w3c.dom.events.*;

/**
 * loadURI element implementation
 * @author Mikko Honkala
 */


public class LoadURIElementImpl extends ActionHandlerBaseImpl implements EventHandlerService
{
    
    public static final String HREF_ATTRIBUTE=XFormsConstants.RESOURCE_LINKING_ATTR;
    
    public LoadURIElementImpl(XFormsElementHandler owner, String ns, String name)
    {
        super(owner, ns, name);
        this.bindingAttributesRequired=false;
    }
    
    public void activate(Event evt)
    {
        Log.debug("LoadURIElementImpl.activate("+evt+")");
        this.getBoundNodeset();
            /*
        try {
        } catch (XFormsBindingException e)
        {
            this.handleXFormsException(e);
            return;
        }*/
        
        String URIstr=null;
        // first try to read href from the bould instance node, then from the attribute
        if (this.getRefNode() !=null)
            URIstr = XFormsUtil.getText(getRefNode());
        else URIstr = this.getHref();
        Log.debug("loadURI: "+URIstr);
        URL uri = null;
        try
        {
            if (URIstr!=null&&URIstr.length()>0) uri = resolveURI(URIstr);
        } catch (Exception e)
        {
            Log.error(e);
            return;
        }
        this.dispatch(XFormsConstants.SUBMIT_STARTED_EVENT);
        handler.openLocation(uri,this.getShow());
    }
    
    protected String getHref()
    {
        return this.getAttribute(HREF_ATTRIBUTE);
    }
    
    protected short getShow()
    {
        String attrval = this.getAttribute(SHOW_ATTR);
        if (attrval!=null&&attrval.equals("new"))
            return SHOW_NEW;
        else return SHOW_REPLACE;
    }
    
    
}
