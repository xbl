/* X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources. Created on May 26, 2004
 *  
 */
package fi.hut.tml.xsmiles.mlfc.encryption;

import java.io.*;
import java.net.*;
import java.security.*;
import org.apache.xml.security.encryption.*;
import org.w3c.dom.*;
import fi.hut.tml.xsmiles.*;


/**
 * Utility class for creating encrypted data.
 *
 * @author honkkis, nps
 *  
 */
public class EncryptionCreator {

    /**
     * Get the default encryption key.
     *
     * @param key  the current encryption key
     */
    public static EncryptionKey getKey(EncryptionKey key)
    {
        try {
            if (key == null) {
                key = new EncryptionKey();
                key.storeURL = Resources.getResourceURL("xsmiles.default.symmetrickey");
                key.keyAlias=Resources.getResourceString("xsmiles.default.key.alias");
                key.keyPass=Resources.getResourceString("xsmiles.default.key.pass");
                key.storePass=Resources.getResourceString("xsmiles.default.keystore.pass");
            }
        } catch (Exception e) {
            Log.error(e);
        }

        return key;
    }


    /**
     * Create an empty document.
     *
     * @param ns  should the document be namespace aware?
     *
     * @throws Exception  there was error creating the document
     */
    public static Document createEmptyDoc(boolean ns) throws Exception {

        javax.xml.parsers.DocumentBuilderFactory dbf =
            javax.xml.parsers.DocumentBuilderFactory.newInstance();

         dbf.setNamespaceAware(ns);

         javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder();
         return db.newDocument();

    }


    /**
     * Encrypt a document.
     *
     * @param node  the node to be encrypted
     * @param url   the base URL of doc
     * @param key   the encryption key
     *
     * @return the encrypted element, or null if the key could not be retrieved
     *
     * @throws Exception  the node could not be encrypted
     */
    public static Element encrypt(Node node, URL url, EncryptionKey key) throws Exception {

        KeyStore ks;

        // retrieve the key for encryption
        try {
            key = getKey(key);
            ks = getKeyStore(key);
        }
        catch (Exception e) {
            Log.error(e, "Could not open keystore " + key);
            return null;
        }
        Key secretKey = ks.getKey(key.keyAlias, key.keyPass.toCharArray());

        // create a copy of the node to be encrypted
        Document targetDoc = createEmptyDoc(true);
        if (node instanceof Document)
            targetDoc.appendChild(targetDoc.importNode(((Document)node).getDocumentElement(), true));
        else
            targetDoc.appendChild(targetDoc.importNode(node, true));

        XMLCipher cipher = XMLCipher.getInstance(XMLCipher.AES_128);
        cipher.init(XMLCipher.ENCRYPT_MODE, secretKey);
        cipher.doFinal(targetDoc, targetDoc.getDocumentElement());

        return targetDoc.getDocumentElement();

    }


    /**
     * Decrypt a document.
     *
     * @param node  the node to be decrypted
     * @param url   the base URL of doc
     * @param key   the encryption key
     *
     * @return the decrypted element, or null if the key could not be retrieved
     *
     * @throws Exception  the node could not be encrypted
     */
    public static Element decrypt(Node node, URL url, EncryptionKey key) throws Exception {

        KeyStore ks;

        // retrieve the key for encryption
        try {
            key = getKey(key);
            ks = getKeyStore(key);
        }
        catch (Exception e) {
            Log.error(e, "Could not open keystore " + key);
            return null;
        }
        Key secretKey = ks.getKey(key.keyAlias, key.keyPass.toCharArray());

        // create a copy of the node to be decrypted
        Document targetDoc = createEmptyDoc(true);
        if (node instanceof Document)
            targetDoc.appendChild(targetDoc.importNode(((Document)node).getDocumentElement(), true));
        else
            targetDoc.appendChild(targetDoc.importNode(node, true));

        XMLCipher cipher = XMLCipher.getInstance(XMLCipher.AES_128);
        cipher.init(XMLCipher.DECRYPT_MODE, secretKey);
        cipher.doFinal(targetDoc, targetDoc.getDocumentElement());

        return targetDoc.getDocumentElement();

    }


    /**
     * Get the key store for an encryption key.
     *
     * @param key  the key
     *
     * @return  the keystore for the given key
     *
     * @throws Exception  the key sotre could not be accessed
     */
    public static KeyStore getKeyStore(EncryptionKey key) throws Exception {

        String keystorePass = Resources.getResourceString("xsmiles.default.key.pass");
        String keystoreType = "JCEKS";

        InputStream keystoreIs = null;
        URL keyURL;
        if (key == null) {
            key=getKey(key);
        } 
        keyURL=key.storeURL;

        keystoreIs = keyURL.openConnection().getInputStream();
        KeyStore ks = KeyStore.getInstance(keystoreType);
        ks.load(keystoreIs, keystorePass.toCharArray());

        return ks;
    }

}