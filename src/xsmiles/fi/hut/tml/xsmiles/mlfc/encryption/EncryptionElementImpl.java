/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.encryption;

import java.awt.event.*;
import java.net.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.security.encryption.*;
import org.apache.xml.security.exceptions.XMLSecurityException;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.dom.*;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.mlfc.xforms.*;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.*;
import fi.hut.tml.xsmiles.mlfc.xforms.dominterface.*;
import fi.hut.tml.xsmiles.mlfc.xforms.xpath.*;


/**
 * <encrypt> element implementation
 *
 *
 * @author hguo, nps
 */
public class EncryptionElementImpl extends XSmilesElementImpl implements EventHandlerService, ActionListener {

    /** the MLFC that created this element */
    private EncryptionMLFC encryptionMLFC = null;

    /** the document that owns this element */
    private DocumentImpl ownerDoc = null;

    /** the main encryption dialog */
    private EncryptionDialog  dialog;

    /** the key to use for encryption */
    protected EncryptionKey encryptionKey;

    /** the dialog for selecting a key */
    protected KeyDialog keyDialog;

    static {
        org.apache.xml.security.Init.init();
    }


    /**
     * Constructor.
     *
     * @param owner       the document that owns this element
     * @param encryption  the MLFC that is creating this element
     * @param namespace   the namespace to which this element belongs
     * @param tag         the tag name of this element
     */
    public EncryptionElementImpl(DocumentImpl owner, EncryptionMLFC encryption, String namespace, String tag) {

        // do fundamential set up
        super(owner, namespace, tag);

        // save information about our creator
        encryptionMLFC = encryption;
        ownerDoc = owner;

        // tell everyone about it
	Log.debug("Encryption element created!");	

    }


    /**
     * Handler for activation of the encryption event.
     *
     * @param  evt  the DOM event
     */
    public void activate(Event evt)  {

        Log.debug("Encryption.activate");
        showDialog();

    }


    /**
     * Display the encryption dialog.
     */
    private void showDialog() {

        // make sure that we have an encryption key
        if (encryptionKey == null)
            encryptionKey = EncryptionCreator.getKey(encryptionKey);

        // destroy any existing dialog
        if (dialog != null)
            dialog.disposeDialog();

        // display the dialog
        dialog = new EncryptionDialog();
        dialog.setActionListener(this);
        dialog.setComponentFactory(this.encryptionMLFC.getMLFCListener().getComponentFactory());
        dialog.showDialog();

        // update the dialog contents
        dialog.writeToInstance(this.getRefNode());
        dialog.writeToUI(this.getUINode());

    }


    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {

        try {
            Log.debug(e.toString());
            if (e.getActionCommand().equalsIgnoreCase("Encrypt")) {
                this.encrypt();
            } else if (e.getActionCommand().equalsIgnoreCase("Decrypt")) {
                this.decrypt();
            } else if (e.getActionCommand().equalsIgnoreCase("Select Key")) {
                this.selectKey();
            }

        }
        catch (Exception ex) {
            Log.error(ex);
        }
    }


    /**
     * Choose a key to use for encryption.
     */
    protected void selectKey() {

        // destroy any existing key selection dialog
        if (keyDialog != null)
            keyDialog.disposeDialog();

        // display the key selection dialog
        keyDialog = new KeyDialog();
        keyDialog.setKey(this.encryptionKey);
        keyDialog.setActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0)
            {
                EncryptionKey  key = keyDialog.getEncryptionKey();
                if (key != null) encryptionKey=key;
            }
        });
        keyDialog.setComponentFactory(this.getComponentFactory());
        encryptionKey = keyDialog.showDialog();

        // log a debugging message
        Log.debug("Encryption key: " + encryptionKey);

    }


    /**
     * Get the node that describes the user interface.
     *
     * @return  the document that describes the form
     */
    private Node getUINode() {

        URL url = this.encryptionMLFC.getXMLDocument().getXMLURL();
        Document xmldoc = null;
        try {
            xmldoc = encryptionMLFC.getXMLDocument().getBrowser().getXMLParser().openDocument(url);
        }
        catch(Exception e) {
            Log.error(e);
	}

        return xmldoc;
    }


    /**
     * Get the node of the instance document to which the encrypted data should
     * be written.
     *
     * @return  the node indicated by the ``to'' attribute of the encryption element
     */	
    private Node getToNode() {

    	try {
            XFormsModelElement model = this.getModel();
            String to = this.getAttribute("to");
            XPathExpr expr = model.getXPathEngine().createXPathExpression(to);
            NodeList nl = model.getXPathEngine().evalToNodelist(model.getInstanceDocument(null),expr,this,null);
            Node n = nl.item(0);
            return n;
    	}
        catch (Exception e) {
            Log.error(e);
            return null;
        }

    }


    /**
     * Get the node of the instance document to which this element is bound.
     *
     * @return the node of the instance document that will be encrypted, or null if there was an error
     */
    Node getRefNode() {

        try {
            XFormsModelElement model = this.getModel();
            if (this.getAttributeNode("ref") == null) {
                // the whole document will be encrypted
                return model.getInstanceDocument(null);	    	        
            } else {
                // the element is specified by an XPath expression
                String to;
                to = this.getAttribute("ref");
                XPathExpr expr = model.getXPathEngine().createXPathExpression(to);
                NodeList nl = model.getXPathEngine().evalToNodelist(
                    model.getInstanceDocument(null),
                    expr,
                    this,
                    null
                );
                Node n = nl.item(0);
                return n;
            }
        }
        catch (Exception e) {
            Log.error(e);
            return null;
        }
    }


    /**
     * Get the model element to which this action is bound
     *
     * @return the model element to which this action is bound
     */
    public XFormsModelElement getModel() {

        try {
            // just return the first model for now
            Element el = (Element)this.getOwnerDocument().getElementsByTagNameNS(
                XFormsConstants.XFORMS_NS,
                "model"
            ).item(0);
            return (XFormsModelElement)el;
        } catch (Exception e) {
            Log.error(e);
            return null;
        }
	    
    }


    /**
     * Replace a node of the instance document.
     *
     * @param from  the node to replace the old node
     * @param to    the node to be replaced
     *
     * @throws Exception  the node could not be replaced
     */
    void replaceInstanceNode(Node from, Node to) throws Exception {

        // sanity check
	if (from == null || to == null)
            return;

        // check the nodes are compatible
        if (from.getNodeType() != ELEMENT_NODE ||
            (to.getNodeType() != Node.ELEMENT_NODE && to.getNodeType() != Node.DOCUMENT_NODE)) {
	        throw new Exception("Cannot replace other that DOCUMENT or ELEMENT!");
        }

        // replace the node
        if (from.getNodeType() == Node.DOCUMENT_NODE) {
            from = ((Document)from).getDocumentElement();
        }
        Document doc = (Document)(to instanceof Document?to:to.getOwnerDocument());
        Element fromCopy = (Element)doc.importNode(from,true);
        removeAllChildren(to);
        to.appendChild(fromCopy);

    }


    /**
     * Remove all of the children of a node.
     *
     * @param e  the node whose children are to be removed
     */
    static void removeAllChildren(Node e) {

        while (e.getFirstChild() != null) {
            e.removeChild(e.getFirstChild());
        }

    }


    /**
     * Do encryption.
     *
     * @throws  the instance data could not be replaced
     */
    private synchronized void encrypt() throws Exception {

        Element ciphertext = EncryptionCreator.encrypt(
            this.getRefNode(),
            this.encryptionMLFC.getXMLDocument().getXMLURL(),
            this.encryptionKey
        );
        this.replaceInstanceNode(ciphertext, this.getRefNode());
        this.dialog.writeToInstance(ciphertext);
        this.dispatchEvent(EventFactory.createEvent("cipherdata-created",true,false));

    }


    /**
     * Do decryption.
     *
     * @throws  the instance data could not be replaced
     */
    private synchronized void decrypt() throws Exception {

        Element plaintext = EncryptionCreator.decrypt(
            this.getRefNode(),
            this.encryptionMLFC.getXMLDocument().getXMLURL(),
            this.encryptionKey
        );
        this.replaceInstanceNode(plaintext, this.getRefNode());
        this.dialog.writeToInstance(plaintext);
        this.dispatchEvent(EventFactory.createEvent("cipherdata-decrypted",true,false));

    }


    /**
     * Get the component factory for this MLFC.
     *
     * @return the component factory for this MLFC
     */
    public ComponentFactory getComponentFactory() {

        return this.encryptionMLFC.getMLFCListener().getComponentFactory();

    }

}