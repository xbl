/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.encryption;

import org.w3c.dom.*;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.mlfc.*;


/**
 * EncryptionMLFC is the XML Signature main function. It creates the elements
 * and controls them. Namespace: http://www.xsmiles.org/2006/encryption
 *
 * @author hguo, nps
 */
public class EncryptionMLFC extends MLFC {

    /** the document for which this MLFC was created */
    private DocumentImpl xsmilesDoc = null;

    /** the namespace that this MLFC processes */
    public static final String namespace = "http://www.xsmiles.org/2006/encryption"; 

    /** the XML Encryption namespace */    
    public static final String XMLENC_ns= "http://www.w3.org/2001/04/xmlenc#";


    /**
     * Constructor. Does nothing.
     */
    public EncryptionMLFC() {

    }


    /**
     * Get the version of the MLFC. This version number is updated 
     * with the browser version number at compilation time. This version number
     * indicates the browser version this MLFC was compiled with and should be run with.
     *
     * @return 	MLFC version number.
     */
    public final String getVersion() {

	return Browser.version;

    }


    /**
     * Create a DOM element.
     *
     * @param doc  the document to which this element will belong
     * @param ns   the namespace of this element
     * @param tag  tge tag name of this element
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag) {

        // save a reference to the document for later
        xsmilesDoc = doc;

        // initialise
        Element element = null;

        // sanity check
        if (tag == null) {
            Log.error("Invalid null tag name!");
            return null;
        }

        // create the element if we recognise the tag
        String localname = getLocalname(tag);
	if (localname.equals("encrypt")) {
            element = new EncryptionElementImpl(doc, this, ns, tag);
        }

        // Other tags go back to the XSmilesDocument as null...
        if (element == null)
            Log.debug("XML Encryption didn't understand element: " + tag);

        return element;

    }

    /**
     * Start the MLFC.
     */
    public void start() {

        Log.debug("EncryptionMLFC start.");

    }

    /**
     * Stop the MLFC.
     */
    public void stop() {

        Log.debug("EncryptionMLFC end.");

    }

}