/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.encryption;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import org.w3c.dom.*;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;


/**
 * Encryption dialogue.
 *
 * @author honkkis, nps
 */
public class EncryptionDialog implements ActionListener {

    /** the frame of the dialogue */
    protected JFrame frame;

    /** the main panel of the dialogue */
    protected Container mainPanel;

    /** the central panel of the dialogue */
    protected Container centerPanel;

    /** the button panel */
    protected Container buttonPanel;

    /** the encrypt button */
    protected XButton encryptButton;

    /** the cancel button */
    protected XButton cancelButton;

    /** the decrypt button */
    protected XButton decryptButton;

    /** the key selection button */
    protected XButton selectKeyButton;

    /** the text area for the instance document */
    protected XTextArea instLog;

    /** the text area for the user interface document */
    protected XTextArea uiLog;

    /** the keystore file name */
    protected File keystoreFile = null;

    /** the component factory */
    protected ComponentFactory cfactory;

    /** the dialogue's action listener */	
    protected ActionListener actionListener;


    /**
     * Display the dialogue.
     *
     * @return null
     */
    public EncryptionKey showDialog() {

        if (frame == null) {
            this.createFrame();
            this.frame.setSize(800,600);
            this.frame.show();
        }

        return null;

    }


    /**
     * Set the action listener for the dialogue. This listener will be notified
     * when the dialogue is closed.
     *
     * @param l  the action listener that will handle this dialogue's events
     */
    public void setActionListener(ActionListener l) {

        this.actionListener = l;

    }


    /**
     * Destroy the dialogue.
     */
    public void disposeDialog() {

        if (this.frame != null) {
            this.frame.dispose();
            this.frame = null;
        }

    }

	
    /**
     * Get the component factory being used by this dialogue.
     *
     * @return the component factory being used
     */	
    protected ComponentFactory getComponentFactory() {

        if (cfactory == null) {
            Log.error("ComponentFactory was null for KeyDialog... create a default componentfactory");
            cfactory=new AWTComponentFactory();
        }

        return cfactory;

    }


    /**
     * Set the component factory to be used by this dialogue.
     *
     * @param f  the component factory to use
     */
    protected void setComponentFactory(ComponentFactory f) {

        this.cfactory = f;

    }


    /**
     * Create a container for the dialogue components.
     *
     * @param layout  the layout manager for this container
     */
    private Container createContainer(LayoutManager layout) {

        return new JPanel(layout);

    }


    /**
     * Create the main frame for the dialogue.
     */
    private void createFrame() {

        // create the panels
        frame=new JFrame("Encrypt a form");
        mainPanel=createContainer(new BorderLayout());
        centerPanel=createContainer(new GridLayout(1,2));
        buttonPanel = createContainer(new FlowLayout());

        // the textarea for the instance data
        instLog = this.getComponentFactory().getXTextArea("the instance data");//30,40);
        instLog.setEditable(false);
        
        // the textarea for UI data
        uiLog = this.getComponentFactory().getXTextArea("the ui source");//30,40);
        uiLog.setEditable(false);
        
        // the label for the frame
        XCaption fcaption = this.getComponentFactory().getXCaption("The instance data is on the left, and the UI source on the right.");

        // the encryption button
        encryptButton = this.getComponentFactory().getXButton("Encrypt",null);
        encryptButton.addActionListener(this);
        encryptButton.setActionCommand("Encrypt");

        // the cancel button
        cancelButton = this.getComponentFactory().getXButton("Close",null);
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("Cancel");

        // the decryption button
        decryptButton = this.getComponentFactory().getXButton("Decrypt",null);
        decryptButton.addActionListener(this);
        decryptButton.setActionCommand("Decrypt");

        // the key selection button
        selectKeyButton = this.getComponentFactory().getXButton("Select Key",null);
        selectKeyButton.addActionListener(this);
        selectKeyButton.setActionCommand("Select Key");

        // add the buttons to their panel
        buttonPanel.add(encryptButton.getComponent());
        buttonPanel.add(cancelButton.getComponent());
        buttonPanel.add(decryptButton.getComponent());
        buttonPanel.add(selectKeyButton.getComponent());

        // arrange everything on the main panel
        mainPanel.add(fcaption.getComponent(),BorderLayout.NORTH);
        mainPanel.add(buttonPanel,BorderLayout.SOUTH);
        mainPanel.add(centerPanel,BorderLayout.CENTER);
        centerPanel.add(instLog.getComponent());
        centerPanel.add(uiLog.getComponent());

        addToFrame(mainPanel);
        
    }


    /**
     * Add a component to the dialogue.
     *
     * @param c  the component to add
     */
    void addToFrame(Component c) {

        frame.getContentPane().add(c);

    }


    /**
     * Button handler.
     *
     * @param e  the event to be processed
     */
    public void actionPerformed(ActionEvent e) {

        try {
            if (e.getActionCommand().equals("Encrypt")) {					
                this.actionListener.actionPerformed(e);
            }
            else if (e.getActionCommand().equals("Decrypt")) {
                this.actionListener.actionPerformed(e);
            }
            else if (e.getActionCommand().equals("Select Key")) {
                this.actionListener.actionPerformed(e);
            }
            else this.disposeDialog();
        }
        catch (Exception ex) {
            Log.error(ex);
        }

    }


    /**
     * Display a document fragment in the instance document window.
     *
     * @param n  the root of the document fragment to write
     */
    public void writeToInstance(Node n) {

        this.writeToTextArea(n,instLog);

    }


    /**
     * Display a document fragment in the user interface window.
     *
     * @param n  the root of the document fragment to write
     */
    public void writeToUI(Node n) {

        this.writeToTextArea(n,uiLog);

    }


    /**
     * Display a document fragment in a text area
     *
     * @param n    the root of the document fragment to write
     * @param log  the text area to write to
     */
    protected void writeToTextArea(Node n, XTextArea log) {

        try {
            StringWriter w = new StringWriter();
            fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(w,n,true,true,false);
            log.setText(w.toString());
        }
        catch (Exception e) {
            Log.error(e);
        }
    }

}