/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 2, 2004
 *
 */
package fi.hut.tml.xsmiles.mlfc.encryption;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.gui.components.*;
import fi.hut.tml.xsmiles.gui.components.awt.*;
import fi.hut.tml.xsmiles.util.*;


/**
 * Dialogue for choosing an encryption key.
 * 
 * @author honkkis, nps
 *
 */
public class KeyDialog implements ActionListener {

    /** the frame of the dialogue */
    protected JFrame frame;

    /** the main panel of the dialogue */
    protected Container mainPanel;

    /** the file section button */
    protected XButton fileButton;

    /** the key password field */
    protected XInput keyPassInput;

    /** the key alias field */
    protected XInput keyAliasInput;

    /** the keystore password field */
    protected XInput storePassInput;

    /** the keystore URL field */
    protected XInput URLInput;

    /** the keystore URL */
    protected URL keystoreURL = null;

    /** the component factory */
    protected ComponentFactory cfactory;

    /** the encryption key */
    protected EncryptionKey encryptionKey;

    /** the dialogue's action listener */
    protected ActionListener actionListener;

    /** the name of the file selection command */
    protected static final String FILEBUTTON_COMMAND="KEY_FILEBUTTON";

    /** the name of the OK button's command */
    protected static final String OK_COMMAND="KEY_OK";

    /** the name of the cancel button's command */
    protected static final String CANCEL_COMMAND="KEY_CANCEL";


    /**
     * Display the dialogue.
     *
     * @return null
     */
    public EncryptionKey showDialog() {

        if (frame == null) {
            this.createFrame();
            this.frame.setSize(400,300);
            this.frame.show();
        }

        return null;

    }


    /**
     * Set the encryption key.
     *
     * @param key  the key to be used
     */
    public void setKey(EncryptionKey key) {

        this.encryptionKey = key;

    }


    /**
     * Set the action listener for the dialogue. This listener will be notified
     * when the dialogue is closed.
     *
     * @param l  the action listener that will handle this dialogue's events
     */
    public void setActionListener(ActionListener l) {

        this.actionListener = l;

    }


    /**
     * Destroy the dialogue.
     */
    public void disposeDialog() {

        if (this.frame != null) {
            this.frame.dispose();
            this.frame = null;
        }

    }

	
    /**
     * Get the component factory being used by this dialogue.
     *
     * @return the component factory being used
     */	
    protected ComponentFactory getComponentFactory() {

        if (cfactory == null) {
            Log.error("ComponentFactory was null for KeyDialog... create a default componentfactory");
            cfactory=new AWTComponentFactory();
        }

        return cfactory;

    }


    /**
     * Set the component factory to be used by this dialogue.
     *
     * @param f  the component factory to use
     */
    protected void setComponentFactory(ComponentFactory f) {

        this.cfactory = f;

    }


    /**
     * Create a container for the dialogue components.
     *
     * @param layout  the layout manager for this container
     */
    private Container createContainer(LayoutManager layout) {

        return new JPanel(layout);

    }
	

    /**
     * Create a caption component.
     *
     * @param s  the caption text
     */
    public XCaption getCaption(String s) {

        XCaption c = this.getComponentFactory().getXCaption(s);
        // TODO: set size
        return c;

    }


    /**
     * Create the main frame for the dialogue.
     */
    private void createFrame() {

        // create basic elements
        String cs = "left";
        frame = new JFrame("Encrypt a form");
        mainPanel = createContainer(new GridLayout(0,1));
        XCompound compound;

        // create the keystore password field
        this.storePassInput=this.getComponentFactory().getXInput();
        compound=this.getComponentFactory().getXLabelCompound(
                storePassInput,this.getCaption("KeyStore password:"),cs);
        mainPanel.add(compound.getComponent());

        // creeate the key password field
        keyPassInput=this.getComponentFactory().getXInput();
        compound=this.getComponentFactory().getXLabelCompound(
                keyPassInput,this.getCaption("Key password:"),cs);
        mainPanel.add(compound.getComponent());

        // create the key alias field
        keyAliasInput=this.getComponentFactory().getXInput();
        compound=this.getComponentFactory().getXLabelCompound(
                keyAliasInput,this.getCaption("Key alias:"),cs);
        mainPanel.add(compound.getComponent());

        // create the key store URL
        this.URLInput=this.getComponentFactory().getXInput();
        compound=this.getComponentFactory().getXLabelCompound(
                URLInput,this.getCaption("KeyStore URL"),cs);
        mainPanel.add(compound.getComponent());

        // fill in the default values    
        this.setDefaults();

        // create the file selection button
        this.fileButton=this.getComponentFactory().getXButton("Select file",null);
        this.fileButton.setActionCommand(FILEBUTTON_COMMAND);
        this.fileButton.addActionListener(this);
        mainPanel.add(this.fileButton.getComponent());

        // create a container for the buttons
        Container buttons = this.createContainer(new FlowLayout());
        XButton ok, cancel;

        // create the OK button
        ok=this.getComponentFactory().getXButton("OK",null);
        ok.setActionCommand(OK_COMMAND);
        ok.addActionListener(this);
        buttons.add(ok.getComponent());

        // create the cancel button
        cancel=this.getComponentFactory().getXButton("Cancel",null);
        cancel.setActionCommand(CANCEL_COMMAND);
        cancel.addActionListener(this);
        buttons.add(cancel.getComponent());

        mainPanel.add(buttons);    
        addToFrame(mainPanel);

    }


    /**
     * Fill the dialogue with default values.
     */
    private void setDefaults() {

        if (this.encryptionKey!=null) {
            this.keyAliasInput.setText(this.encryptionKey.keyAlias);
            this.keyPassInput.setText(this.encryptionKey.keyPass);
            this.storePassInput.setText(this.encryptionKey.storePass);
            if (this.encryptionKey.storeURL!=null)
                this.URLInput.setText(this.encryptionKey.storeURL.toString());
        }

    }


    /**
     * Get the key to be used for encryption.
     *
     * @return the key identified by the dialogue
     */
    public EncryptionKey getEncryptionKey() {

        return this.encryptionKey;

    }


    /**
     * Add a component to the dialogue.
     *
     * @param c  the component to add
     */
    void addToFrame(Component c) {

        frame.getContentPane().add(c);

    }

    /** the last file opened */
    protected static File lastFile = new File(".");


    /**
     * Button handler.
     *
     * @param e  the event to be processed
     */
    public void actionPerformed(ActionEvent e) {

        try {

            if (e.getActionCommand().equals(CANCEL_COMMAND)) {					
                if (this.actionListener != null)
                    this.actionListener.actionPerformed(e);
                this.disposeDialog();
            }

            else if (e.getActionCommand().equals(OK_COMMAND)) {
                if (this.keystoreURL != null) {
                    this.encryptionKey = new EncryptionKey();
                    this.encryptionKey.keyAlias=this.keyAliasInput.getText();
                    this.encryptionKey.keyPass=this.keyPassInput.getText();
                    this.encryptionKey.storePass=this.storePassInput.getText();
                    this.encryptionKey.storeURL=this.keystoreURL;
                }
                if (this.actionListener!=null )
                    this.actionListener.actionPerformed(e);
                this.disposeDialog();
            }

            else if (e.getActionCommand().equals(FILEBUTTON_COMMAND)) {
                // open up a file dialog and change the URL field
                XFileDialog dialog = this.getComponentFactory().getXFileDialog(false,lastFile.getAbsolutePath());
                int res = dialog.select();
                if (res==dialog.SELECTION_OK) {
                    File f = dialog.getSelectedFile();
                    lastFile =f;
                    URL u = URLUtil.fileToURL(f);
                    this.keystoreURL=u;
                    this.URLInput.setText(this.keystoreURL.toString());
                }
            }

	} catch (Exception ex) {
            Log.error(ex);
	}

    }

}
