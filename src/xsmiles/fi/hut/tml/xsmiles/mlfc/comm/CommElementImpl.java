/*
 * Created on 31.3.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;


/**
 * @author ssundell
 *
 */
public class CommElementImpl extends XSmilesElementImpl
{

    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public CommElementImpl(
        DocumentImpl ownerDocument,
        String namespaceURI,
        String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
    }

}
