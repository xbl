/*
 * Created on 17.8.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.comm.*;

import org.apache.xerces.dom.DocumentImpl;

import java.util.Enumeration;

import org.apache.xerces.dom.events.EventImpl;
import org.w3c.dom.events.*;

/**
 * @author ssundell
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CommAddressBookElementImpl extends XSmilesElementImpl
{
    private CommMLFC mlfc;
    private AddressBook aBook;
    
    public CommAddressBookElementImpl(DocumentImpl _doc, CommMLFC _mlfc, String _ns, String _tag)
    {
        super(_doc, _ns, _tag);
        mlfc = _mlfc;
    }
    
    public void init()
    {
        String prefix;
        if (getPrefix() == null)
            prefix = "";
        else
            prefix = getPrefix() + ":";
        
        aBook = CommBroker.getCommSession(mlfc.getType()).getAddressBook();
        aBook.loadAddressBook();
        
        for (Enumeration e = aBook.getContacts().elements(); e.hasMoreElements();)
        {
            this.appendChild(new CommUserElementImpl((DocumentImpl)ownerDocument, mlfc, namespaceURI, prefix + "user", (User)e.nextElement()));
        }
        super.init();
        dispatch ("comm-addressbook-loaded");
    }
    
    
	protected boolean dispatch (String type)
	{
		org.w3c.dom.events.Event evt;
		evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		return ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
	}
	
	public boolean dispatchEvent(Event event)
	{
		Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
		return super.dispatchEvent(event);
	}
}
