/*
 * Created on 6.4.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.comm.session.Messaging;
import fi.hut.tml.xsmiles.comm.session.Message;
import fi.hut.tml.xsmiles.comm.implementation.general.messages.MessageFactory;

/**
 * @author ssundell
 *
 */
public class CommMsgElementImpl extends XSmilesElementImpl
{
    private CommMLFC commmlfc;
    private String sessionType;
    private String direction;

    /**
     * @param ownerDocument
     * @param namespaceURI
     * @param qualifiedName
     */
    public CommMsgElementImpl(
        DocumentImpl doc,
        CommMLFC mlfc,
        String namespaceURI,
        String qualifiedName)
    {
        super(doc, namespaceURI, qualifiedName);
        commmlfc = mlfc;
    }

    public void init()
    {
        sessionType = commmlfc.getType();
        direction = getParentNode().getLocalName();
        Log.debug ("COMM: " + direction);
        
        
        super.init();
        
        if (direction.equals("incoming"))
            dispatch("comm-incoming-message");
        else if (direction.equals("outgoing"))
            if (!getAttribute("status").equals("sent")) {
                String msgType = getAttribute("message-type");
                CommSessionElementImpl sessionNode = (CommSessionElementImpl)getParentNode().getParentNode().getParentNode();
                Messaging messaging = sessionNode.getMessaging();
                Log.debug("Sending " + getText());
                if (msgType == null || msgType.equals("") || msgType.equals("text/plain"))
                {
                    Message msg = MessageFactory.createMessage("text/plain");
                    msg.setContent(getText());
                    
                    messaging.sendMessage(msg);
                    setAttribute("status", "sent");
                    dispatch("comm-transmitted-message");
                }
                else if (msgType.equals("application/xml"))
                    // TODO: Yes, well, not working...
                    // Quick instructions, possibly even something working: create a Document and copy
                    // the elements below this node into it, then serialize it and send.
                    Log.debug ("Not yet working");
                else
                    Log.debug ("Unknown message format");
            }
    }
    
    protected boolean dispatch (String type)
    {
        org.w3c.dom.events.Event evt;
        evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        return ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    }
    
    public boolean dispatchEvent(Event event)
    {
        Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
        return super.dispatchEvent(event);
    }
    

}
