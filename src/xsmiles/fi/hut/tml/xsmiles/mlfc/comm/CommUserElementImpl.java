/*
 * Created on 31.3.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.comm.*;
import fi.hut.tml.xsmiles.comm.events.*;
import fi.hut.tml.xsmiles.comm.implementation.general.events.CommEventImpl;
import fi.hut.tml.xsmiles.comm.presence.*;

import java.net.URL;
import java.net.MalformedURLException;

import java.util.Enumeration;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.*;

import org.apache.xerces.dom.events.EventImpl;
import org.w3c.dom.events.*;

/**
 * @author ssundell
 *
 */
public class CommUserElementImpl extends XSmilesElementImpl implements PresenceListener, CommEventListener
{
    private CommMLFC commmlfc;
    private User user;
    private String sessionType;
    private boolean localUser = false;

    public CommUserElementImpl (DocumentImpl doc, CommMLFC mlfc, String ns, String tag)
    {
        super(doc, ns, tag);
        commmlfc = mlfc;
    }
    
    public CommUserElementImpl (DocumentImpl doc, CommMLFC mlfc, String ns, String tag, User _user)
    {
        super(doc, ns, tag);
        commmlfc = mlfc;
        user = _user;
    }

    
    public void init()
    {
        Log.debug("CommMLFC: Initializing user");
        super.init();
        sessionType = commmlfc.getType();
        if (user == null)
        {
            user = CommBroker.getCommSession(sessionType).createUser();
            if (hasAttributes()) {
                NamedNodeMap attrs = getAttributes();
                for (int i=0; i < attrs.getLength(); i++)
                {
                    String name = attrs.item(i).getNodeName();
                    String value = attrs.item(i).getNodeValue();
                    user.setValue(name, value);
                }                
            }
        }
        else
        {
            for (Enumeration e=user.getKeys();e.hasMoreElements();)
            {
                String ukey = (String)e.nextElement(); 
                setAttribute(ukey, user.getValue(ukey).toString());
            }
        }
        
        if (getParentNode().getLocalName().equals("addressbook"))
        {
            Presentity pres = user.getPresentity();
            if (pres != null)
                pres.subscribe(this);
        }
    }
    
    public void destroy()
    {
        if (localUser)
        {        
            NodeList bases = getElementsByTagNameNS("urn:ietf:params:xml:ns:pidf", "basic");
            if (bases.getLength() > 0)
            {
                Node base = bases.item(0);
                base.replaceChild(ownerDocument.createTextNode("closed"), base.getFirstChild());
                publish(base.getParentNode());
            }
        }
        Presentity pres = user.getPresentity();
        if (pres != null)
            pres.remove();
        super.destroy();
    }
    
    protected void setUser(User _user)
    {
        user = _user;
        for (Enumeration e=user.getKeys();e.hasMoreElements();)
        {
            String ukey = (String)e.nextElement(); 
            setAttribute(ukey, user.getValue(ukey).toString());
        }
    }
    
    public String getUserId()
    {
        String userId = (String)user.getValue("name");
        if (userId == null)
          userId = (String)user.getValue("address");
        return userId;
    }
    
    protected User getUser()
    {
        return user;
    }
    
    public void publish(Node presence)
    {
        localUser=true;
        Presentity pres = user.getPresentity();
        if (pres != null)
            pres.publish(presence);            
    }
    
    public void statusChange(Node status)
    {
        if (status == null)
            return;
        
        NodeList statusList = getElementsByTagName("presence");
        Node stat = ownerDocument.importNode(status, true);
        if (statusList.getLength() == 0)
            appendChild(stat);
        else
            replaceChild(stat, statusList.item(0));
        
        dispatch("comm-presence-status-change");
    }

    public void incomingEvent(CommEvent evt)
    {
        int type = evt.getType();
        
            
    }
    
	protected boolean dispatch (String type)
	{
		org.w3c.dom.events.Event evt;
		evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		return ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
	}
	
	public boolean dispatchEvent(Event event)
	{
		Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
		return super.dispatchEvent(event);
	}
}
