/*
 * Created on 31.3.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.comm.*;

import java.util.Enumeration;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.NamedNodeMap;

/**
 * @author ssundell
 *
 */
public class CommGroupElementImpl extends XSmilesElementImpl
{
    private CommMLFC commmlfc;
    private Group group;
    private String sessionType;

    public CommGroupElementImpl (DocumentImpl doc, CommMLFC mlfc, String ns, String tag)
    {
        super(doc, ns, tag);
        commmlfc = mlfc;
    }
    
    public CommGroupElementImpl (DocumentImpl doc, CommMLFC mlfc, String ns, String tag, Group _group)
    {
        super(doc, ns, tag);
        commmlfc = mlfc;
        group = _group;
    }

    
    public void init()
    {
        sessionType = commmlfc.getType();
        if (group == null)
        {
        	group = CommBroker.getCommSession(sessionType).createGroup();
            if (hasAttributes()) {
                NamedNodeMap attrs = getAttributes();
                for (int i=0; i < attrs.getLength(); i++)
                {
                    String name = attrs.item(i).getNodeName();
                    String value = attrs.item(i).getNodeValue();
                    group.setValue(name, value);
                }                
            }
        }       
    }
    
    protected void setGroup(Group _group)
    {
    	group = _group;
        for (Enumeration e=group.getKeys();e.hasMoreElements();)
        {
            String ukey = (String)e.nextElement(); 
            setAttribute(ukey, group.getValue(ukey).toString());
        }
    }
    
    public String getGroupId()
    {
        String groupID = (String)group.getValue("name");
        /*
        if (userId == null)
          userId = (String)user.getValue("address");
          */
        return groupID;
    }
    
    protected Group getGroup()
    {
        return group;
    }

}
