/*
 * Created on 31.3.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.comm.*;
import fi.hut.tml.xsmiles.comm.events.*;
import fi.hut.tml.xsmiles.comm.session.SessionListener;
import fi.hut.tml.xsmiles.comm.session.Session;

import fi.hut.tml.xsmiles.Log;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.*;


/**
 * @author ssundell
 *
 */
public class CommCommSessionElementImpl extends XSmilesElementImpl implements SessionListener, CommEventListener
{
    private String sessionType;
    private CommSession commsession;
    private CommMLFC mlfc;
    private boolean online;
    


    public CommCommSessionElementImpl(DocumentImpl _doc, CommMLFC _mlfc, String _ns, String _tag)
    {
        super(_doc, _ns, _tag);
        mlfc = _mlfc;
    }
    
    /**
     * Initializes the communications session - in other words, calls the appropriate methods to signin.
     */
    public void init() {
        sessionType = getAttribute("sessiontype");
        mlfc.setType(sessionType);
        
        boolean csMain = Boolean.valueOf(getAttribute("main")).booleanValue();
        
        CommBroker.setMlfcListener(mlfc.getMLFCListener());
        
        String prefix;
        if (getPrefix() == null)
            prefix = "";
        else
            prefix = getPrefix() + ":";
        
        commsession = CommBroker.getCommSession(sessionType);
        if (commsession != null)
        {    
            commsession.addEventListener(this);
        	online = true;
        	commsession.signIn();
        	
        	if (online)
        	{
        	    // 	Include the local user information, to be able to send the publish.
        	    Log.debug("CommMLFC: Creating the local user.");
        	    CommUserElementImpl local = new CommUserElementImpl((DocumentImpl)ownerDocument, mlfc, namespaceURI, prefix + "user", commsession.getOwner());
        	    local.setAttribute("local", "true");
        	    this.appendChild(local);
        	    
        
        		
        		
        	}
        	
        	if (csMain)
        	{
        		commsession.setSessionListener(this);
        		// New address book.
        		//if (online)
//        			this.appendChild(new CommAddressBookElementImpl((DocumentImpl)ownerDocument, mlfc, namespaceURI, prefix + "addressbook"));
        	}
        }
        super.init();
        dispatch("comm-commsession-init");
    }
    
    public void destroy()
    {
        Log.debug("Destroying the CommCommSession element.");
        super.destroy();
        commsession.signOut();
        commsession.removeEventListener(this);
    }
    
    public void incomingSession(Session _session)
    {
    	String prefix;
		if (getPrefix() == null)
			prefix = "";
		else
			prefix = getPrefix() + ":";
				
		XSmilesElementImpl pending = new XSmilesElementImpl((DocumentImpl)ownerDocument, namespaceURI, prefix + "pending");
        CommSessionElementImpl session = new CommSessionElementImpl((DocumentImpl)ownerDocument, mlfc, namespaceURI, prefix + "session", _session);
        
        pending.appendChild(session);
        
        Contact contact = _session.getTarget();
        Class c[] = contact.getClass().getInterfaces();
        for(int i=0;i< c.length;i++)
        {
            Log.debug(c[i].getName());
            if (c[i].getName().equals("fi.hut.tml.xsmiles.comm.User"))
            {
                CommUserElementImpl user;
                if (getPrefix() == null)
                    user = new CommUserElementImpl((DocumentImpl)ownerDocument, mlfc, namespaceURI, "user", (User)contact);
                else
                    user = new CommUserElementImpl((DocumentImpl)ownerDocument, mlfc, namespaceURI, getPrefix() + ":user", (User)contact);
                user.setAttribute("username", contact.getName());
                user.setAttribute("address", (String)contact.getValue("address"));
                session.appendChild(user);
            }
            else if (c[i].getName().equals("fi.hut.tml.xsmiles.comm.Group")) 
            {
                 // TODO: So far no group support
            }
        }
        appendChild(pending);
        session.init();
        session.dispatch("comm-incoming-session");
        
    }
    
    public void incomingEvent(CommEvent evt)
    {
        switch (evt.getType())
        {
            case CommEvent.EVENT_COMMSESSION_CONNECT_ERROR:
                online = false;
                dispatch("comm-connection-error");
            	//destroy();
            	break;
        }
    }
    
    protected boolean dispatch (String type)
    {
        org.w3c.dom.events.Event evt;
        evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        return ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    }
    
    public boolean dispatchEvent(Event event)
    {
        Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
        return super.dispatchEvent(event);
    }
}
