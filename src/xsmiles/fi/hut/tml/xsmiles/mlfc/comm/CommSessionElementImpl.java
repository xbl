/*
 * Created on 31.3.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import fi.hut.tml.xsmiles.comm.CommBroker;
import fi.hut.tml.xsmiles.comm.events.*;
import fi.hut.tml.xsmiles.comm.session.*;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.events.EventImpl;
import org.w3c.dom.events.*;

/**
 * @author ssundell
 *
 */
public class CommSessionElementImpl extends XSmilesElementImpl implements MessageListener, CommEventListener 
{
    private CommMLFC commmlfc;
    private String sessionType;
    private Session session;
    private Messaging messaging;
    
    public CommSessionElementImpl (DocumentImpl doc, CommMLFC mlfc, String ns, String tag)
    {
        super(doc, ns, tag);
        commmlfc = mlfc;
    }
    
    public CommSessionElementImpl (DocumentImpl doc, CommMLFC mlfc, String ns, String tag, Session _session)
    {
        super(doc, ns, tag);
        commmlfc = mlfc;
        session = _session;
    }
    
    public void init()
    {
        super.init();
        String prefix;
        
        sessionType = commmlfc.getType();
        
        if (getPrefix() == null)
            prefix = "";
        else
            prefix = getPrefix() + ":";
        
        if (session == null)
            if (getElementsByTagNameNS(namespaceURI, "group").getLength() > 0)
            {
                //TODO: Add the Group connection system
            	session = ((CommGroupElementImpl)(getElementsByTagNameNS(namespaceURI, "group").item(0))).getGroup().connect();
                //Log.debug("Groups not working yet");
            }
            else if (getElementsByTagNameNS(namespaceURI, "user").getLength() > 0)
                session = ((CommUserElementImpl)(getElementsByTagNameNS(namespaceURI, "user").item(0))).getUser().connect();
            else
                session = CommBroker.getCommSession(sessionType).createSession();
        
		if (!getParentNode().getLocalName().equals("pending"))
		{
			messaging = session.createMessaging();
			messaging.addMessageListener(this);
		}
		
		if (session != null)
		{
		    session.addEventListener(this);
		}
        
        
    }
    
    public void destroy()
    {
        Log.debug("Destroying a COMM session element.");
        if (session != null)
            if (!getParentNode().getLocalName().equals("pending"))
            {
                // Not pending means active, so we'll close it.
                session.closeSession();
            }
            else
            {
                // We'll assume that if we have a pending session,
                // it will be declined.
                session.decline();
            }
        super.destroy();
    }
    
    protected Session getSession()
    {
        return session;
    }
    
    protected Messaging getMessaging()
    {
        return messaging;
    }
    
    public void incomingMessage(Message message)
    {
        String prefix = getPrefix();
        if (!prefix.equals(""))
        	prefix += ":";
        
        NodeList incoming_nodes = getElementsByTagNameNS(namespaceURI, "incoming");
        Node incom_node;
        if (incoming_nodes.getLength() <= 0)
        {
            NodeList messaging_nodes = getElementsByTagName(prefix + "messaging");
            Node messaging_node;
            if (messaging_nodes.getLength() <= 0)
                messaging_node = appendChild(ownerDocument.createElementNS(namespaceURI, prefix + "messaging"));
            else
                messaging_node = messaging_nodes.item(0);
            
            incom_node = messaging_node.appendChild(ownerDocument.createElementNS(namespaceURI, prefix + "incoming"));
        }
        else
            incom_node = incoming_nodes.item(0);
        
        if (message.getType().equals("text/plain"))
        {
            CommMsgElementImpl msg_node = new CommMsgElementImpl((DocumentImpl)ownerDocument, commmlfc, namespaceURI, prefix + "message");
            //Node msg_node = ownerDocument.createElementNS(namespaceURI, prefix + "message");
            incom_node.appendChild(msg_node);
            msg_node.appendChild(ownerDocument.createTextNode((String)message.getContent()));
            msg_node.setAttribute("nick", message.getNickname());
            msg_node.init();
        }
    }
    
    public void sendMessage(String message)
    {
        String prefix = getPrefix();

        if (!prefix.equals(""))
            prefix += ":";


        NodeList outgoing_nodes = getElementsByTagName(prefix + "outgoing");
        Node out_node;
        if (outgoing_nodes.getLength() <= 0)
        {
            NodeList messaging_nodes = getElementsByTagName(prefix + "messaging");
            Node messaging_node;
            if (messaging_nodes.getLength() <= 0)
                messaging_node = appendChild(ownerDocument.createElementNS(namespaceURI, prefix + "messaging"));
            else
                messaging_node = messaging_nodes.item(0);
            
            out_node = messaging_node.appendChild(ownerDocument.createElementNS(namespaceURI, prefix + "outgoing"));
        }
        else
            out_node = outgoing_nodes.item(0);
        
        CommMsgElementImpl msg_node = new CommMsgElementImpl((DocumentImpl)ownerDocument, commmlfc, namespaceURI, prefix + "message");
        //    ownerDocument.createElementNS(namespaceURI, prefix + "message"); 

        // If we have session, we surely have a commsession as well. More to the point,
        // there should be only one in a document that includes live session.
        // This is a way of getting the "nick" attribute into the created message element.
        Node commSession = ownerDocument.getElementsByTagNameNS(namespaceURI, "commsession").item(0);
        NodeList commChildren = commSession.getChildNodes();
        for (int i = 0; i < commChildren.getLength(); i++)
        {
            Node nod = commChildren.item(i);
            if (nod.getLocalName().equals("user"))
            {
                msg_node.setAttribute("nick", ((CommUserElementImpl)nod).getUserId());
            }
        }
        
        
        out_node.appendChild(msg_node);
        msg_node.appendChild(ownerDocument.createTextNode(message));
        msg_node.init();
    }
    
    protected boolean dispatch (String type)
    {
        org.w3c.dom.events.Event evt;
        evt = new EventImpl(); //createEvent();
        evt.initEvent(type, true, true);
        return ((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
    }
    
    public boolean dispatchEvent(Event event)
    {
        Log.debug("** EVENT DISPATCH: type: "+event.getType()+" target:"+this+" bubbles: "+event.getBubbles()+" cancelable: "+event.getCancelable());
        return super.dispatchEvent(event);
    }
    
    public void incomingEvent(CommEvent evt)
    {
        switch (evt.getType())
        {
            case CommEvent.EVENT_SESSION_ACCEPT:
                dispatch("comm-session-accepted");
            	break;
            case CommEvent.EVENT_SESSION_DECLINE:
                dispatch("comm-session-declined");
            	break;
            case CommEvent.EVENT_SESSION_UNCONNECTED:
                dispatch("comm-session-unconnected");
            	Log.debug("Unconnected call in CommSessionElementImpl.");
            	session = null;
            	destroy();
            	break;
        }
        
    }
}
