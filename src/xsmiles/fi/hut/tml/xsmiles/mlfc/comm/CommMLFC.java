/*
 * Created on 30.3.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.mlfc.comm;

import fi.hut.tml.xsmiles.mlfc.MLFC;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;

/**
 * @author ssundell
 *
 */
public class CommMLFC extends MLFC
{
    private String sessionType;
    
    public CommMLFC()
    {
        super();
        
    }
    
    public void start()
    {
    }
    
    public void stop()
    {
    }
    
    /**
       * Get the version of the MLFC. This version number is updated 
       * with the browser version number at compilation time. This version number
       * indicates the browser version this MLFC was compiled with and should be run with.
       * @return    MLFC version number.
       */
    public final String getVersion()
    {
        return Browser.version;
    }
    
   /**
    * Create a DOM element.
    */
    public Element createElementNS(DocumentImpl doc, String ns, String tag)
    {
        Log.debug("Creating CommMLFC element " + tag);
        Element element = null;
        
        if (tag == null)
        {
            Log.error("Invalid tag name!");
            return null;
        }
        
        String localname = getLocalname(tag);
        
        if (localname.equals("commsession"))
        {
            element = new CommCommSessionElementImpl(doc, this, ns, tag);
        }
        if (localname.equals("addressbook"))
        {
            element = new CommAddressBookElementImpl(doc, this, ns, tag);
        }
        else if (localname.equals("session"))
        {
            element = new CommSessionElementImpl(doc, this, ns, tag);
        }
        else if (localname.equals("user"))
        {
            element = new CommUserElementImpl(doc, this, ns, tag);
        }
        else if (localname.equals("group"))
        {
            element = new CommGroupElementImpl(doc, this, ns, tag);
        }
        else if (localname.equals("message"))
        {
            element = new CommMsgElementImpl(doc, this, ns, tag);
        }
        
        return element;        
    }
    
    protected void setType(String _type)
    {
        sessionType = _type;
    }
    
    protected String getType()
    {
        return sessionType;
    }
}
