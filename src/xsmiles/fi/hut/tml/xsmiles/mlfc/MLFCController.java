package fi.hut.tml.xsmiles.mlfc;


/**
 * An abstract controller class for controlling MLFCs. This enables ECMAScripts
 * to control the mlfc itself. (page up/page down/next page/zoom up/down and so on)
 *
 * @author Juha
 * @version 0
 * @see MLFC
 */
public class MLFCController {

  /**
   * Goes one page forwards
   */
  public void pageForward() {}

  /**
   * Goes one page backwards
   */
  public void pageBack() {}

  /**
   * Zoom view in
   */
  public void zoomIn() {}

  /**
   * Zoom view out
   */
  public void zoomOut() {}

  /**
   * @return false, if cannot move any more up
   */
  public boolean scrollUp() {
    return false;
  }
  
  /**
   * @return false, if cannot move any more down
   */
  public boolean scrollDown() {
    return false;
  }

  /**
   * @return false, if cannot move any more left
   */
  public boolean scrollLeft() {
    return false;
  }
  
  /**
   * @return false, if cannot move any more right
   */
  public boolean scrollRight() {
    return false;
  }

  public void moveActiveSpotUp() {
  }

  public void moveActiveSpotDown() {
  }

  public void followActiveLink() {
  }
}
