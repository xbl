/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.css.sac.SelectorList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.*;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectorParser;

/**
 * Implements the item element of the timesheet. Item element can control one
 * or many document elements.
 * 
 * @author tjjalava
 * @since Feb 26, 2004
 * @version $Revision: 1.27 $, $Date: 2006/01/10 13:39:08 $
 */
public class ItemImpl extends TimedElementImpl {

    private DocumentImpl owner;
    private Vector elements;
    private String select;
    private int activeElements = 0; 

    /**
     * Creates a new instance for item element
     * 
     * @param owner
     * @param namespace
     * @param tag
     */
    public ItemImpl(DocumentImpl owner, String namespace, String tag) {
        super(owner, namespace, tag);
        this.owner = owner;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.XSmilesElementImpl#init()
     */
    public void init() {
        super.init();
        elements = new Vector();
        Hashtable classElem = getCache();
        select = getAttribute(SELECT_ATTR);

        Vector list = (Vector) classElem.get(select);
        if (list == null) {
            SelectorList sl = CSSSelectorParser.parseSelectors(select);
            searchElements(sl, ((ExtendedDocument) owner).getStyleSheet().getCSSSelectors(), owner.getDocumentElement(), list = new Vector());
            classElem.put(select, list);
        }
        for (Enumeration e = list.elements(); e.hasMoreElements();) {
            elements.addElement(getElementController((XSmilesElementImpl) e.nextElement()));
        }
    }
   
    /**
     * This item's ElementControllers inform the end of their media object
     * using this method
     * 
     * @param elem
     *                   the element controller
     */
    public synchronized void mediaEnded(ElementController elem) {
        if (endSchedule.isIndefinite()) {
            return;
        }
        elem.setActive(false);
        activeElements--;
        if (activeElements == 0) {
            if (shouldRepeat()) {
                doRepeat();
            } else {
                if (!eventTable.hasEvent(this, STOP)) {
                    scheduleEvent(currentTick, STOP);
                }
            }
        }
    }

    /**
     * Returns a new instance of
     * {@link fi.hut.tml.xsmiles.mlfc.timesheet.ElementController ElementController}
     * based on the type of the argument. If the argument is an instance of
     * {@link fi.hut.tml.xsmiles.dom.MediaElement MediaElement}, then this
     * method returns a new instance of
     * {@link fi.hut.tml.xsmiles.mlfc.timesheet.MediaElementController MediaElementController}
     * 
     * @param elem
     *                   the document element
     * @return new controller for the element
     */
    private ElementController getElementController(XSmilesElementImpl elem) {
        if (elem instanceof MediaElement) {
            addPauseListener(this);
            MediaElementController media = new MediaElementController(this, elem);
            if (media.needsPrefetch()) {
                addPrefetchedMedia((MediaElement) elem);
            }
            return media;
        } else {
            return new ElementController(this, elem);
        }
    }

    /**
     * Goes through the dom and adds all elements matching the <code>sel</code>
     * argument to the <code>list</code>.
     * 
     * @param sl
     *                   selector list
     * @param sel
     *                   the selectors the elements are matched against
     * @param node
     *                   the root node where to begin the search
     * @param list
     *                   vector, where the matching elements are placed
     */
    private void searchElements(SelectorList sl, Selectors sel, Element node, Vector list) {
        if (sl==null) return;
        for (int i = 0; i < sl.getLength(); i++) {
            if (sel.selectorMatchesElem(sl.item(i), node)) {
                //Log.debug(node.getLocalName() + " matches " + sl.item(i));
                list.addElement(node);
            }
        }
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof VisualElementImpl) {
                searchElements(sl, sel, (Element) child, list);
            }
        }
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doSetPaused()
     */
    protected void doSetPaused() {
        for (Enumeration e = elements.elements(); e.hasMoreElements();) {
            ElementController ec = (ElementController)e.nextElement();
            if (ec.isActive()) {
                ec.setPaused(paused);
            }
        }
    }
        
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doRepeat()
     */
    protected void doRepeat() {
        doStartElement();                
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStartElement()
     */
    protected void doStartElement() {
        activeElements = 0;
        for (Enumeration e = elements.elements(); e.hasMoreElements();) {
            ElementController ec = (ElementController) e.nextElement();
            ec.setActive(true);
            //if (!ec.isStatic()) {
                activeElements++;
            //}
        }        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStopElement()
     */
    protected void doStopElement() {        
        activeElements = 0;
        for (Enumeration e = elements.elements(); e.hasMoreElements();) {
            ElementController ec = (ElementController) e.nextElement();
            ec.setActive(false);
        }
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#setEnabled(boolean)
     */
    public void setEnabled(boolean enabled) {
        for (Enumeration e = elements.elements(); e.hasMoreElements();) {
            ElementController ec = (ElementController) e.nextElement();
            ec.setEnabled(enabled);
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#elementStarting(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected void elementStarting(TimedElementImpl elem) {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#handleElementEnded(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected boolean handleElementEnded(TimedElementImpl elem) {
        return false;
    }
}
