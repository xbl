/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.util.Enumeration;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;


/**
 * Class ExclImpl
 * 
 * @author tjjalava
 * @since May 10, 2004
 * @version $Revision: 1.6 $, $Date: 2005/04/21 11:34:50 $
 */
public class ExclImpl extends TimedElementImpl {
        
    private TimedElementImpl currentChild;
    private TimesheetEventListener listener;
    
    /**
     * @param owner
     * @param namespace
     * @param tag
     */
    public ExclImpl(DocumentImpl owner, String namespace, String tag) {
        super(owner, namespace, tag);
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doRepeat()
     */
    protected void doRepeat() {
        // excl duration is implicitely indefinite so repeating it makes no sense.
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#handleElementEnded(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected boolean handleElementEnded(TimedElementImpl elem) {
        if (currentChild == elem) {
            currentChild = null;
        }
        return true; // excl always takes care of the ending itself. Duration is implicitely indefinite 
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#elementStarting(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected void elementStarting(TimedElementImpl elem) {        
        if (!elementStarted) {
            notifyStart();
            elementStarted = true;
            if (!eventTable.hasEvent(this, STOP)) {
                scheduleEnd();
            }
        } 
        
        if (currentChild != null && currentChild != elem) {            
            currentChild.stopElement();
        }
        
        currentChild = elem;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStartElement()
     */
    protected void doStartElement() {
        if (!eventTable.hasEvent(this,STOP)) {
            endSchedule.setIndefinite(true);
        }
        listener = new TimesheetEventListener(this);
        currentChild = null;        
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {
            TimedElementImpl child = (TimedElementImpl)e.nextElement();
            ((EventTarget)child).addEventListener(ELEMENT_BEGIN_EVENT, listener, false);
            String begin = child.getAttribute(BEGIN_ATTR);
            if (begin!=null && begin.length()>0) {
                child.schedule();         
            }
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStopElement()
     */
    protected void doStopElement() {
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {
            TimedElementImpl child = (TimedElementImpl)e.nextElement();
            child.stopElement();            
            ((EventTarget)child).removeEventListener(ELEMENT_BEGIN_EVENT, listener, false);
        }        
    }
    
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.EventHandlerService#activate(org.w3c.dom.events.Event)
     */
    public synchronized void activate(Event evt) {
        EventTarget target = evt.getTarget();
        if (childItems.contains(target)) {
            elementStarting((TimedElementImpl)target);
        }
        super.activate(evt);
    }
}
