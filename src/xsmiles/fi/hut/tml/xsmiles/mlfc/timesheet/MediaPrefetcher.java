/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.MediaElement;

/**
 * Handles the prefetching of the objects, both before the start of the timesheet and during the playing of the timesheet.
 * Fetches objects in parallel, the maximum number of simultaneous fetchers can be defined by changing the {@link #MAX_RUNNERS MAX_RUNNERS} field.
 * There's two ways to fetch objects. {@link #doPrefetch() doPrefetch} method fetches all MediaElements added to the batch by {@link #addMedia(MediaElement) addMedia}
 * method and blocks until all objects have been fetched. Method {@link #doBackgroundPrefetch(MediaElement) doBackgroundPrefetch} schedules a prefetch of the argument
 * to start as soon as possible, supposedly in a very near future, but doesn't stay around to wait for this to happen.  
 * 
 * @author tjjalava
 * @since Apr 15, 2004
 * @version $Revision: 1.3 $, $Date: 2004/04/23 10:52:44 $
 */
class MediaPrefetcher implements Runnable {

    /**
     * The number of maximum simultaneous prefetch threads
     */
    protected static final int MAX_RUNNERS = 5;

    private Vector fetchees = new Vector();
    private Thread main;

    private PrefetchMutex mutex = new PrefetchMutex();

    /**
     * A mutex used by the fetcher threads for synchronization and access control 
     */
    private class PrefetchMutex {

        private int active = 0;
        private int running = 0;
        private boolean finished = false;

        /**
         * Informs that the current thread is starting
         */
        public synchronized void start() {
            active++;
            notifyAll();
        }
        
        /**
         * Waits for the next opportunity to run
         */
        public synchronized void run() {
            while (!canRun()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            running++;
            notifyAll();
        }

        /**
         * Informs tha the current thread is stopping
         */
        public synchronized void stop() {
            active--;
            running--;            
            notifyAll();
        }
        
        /**
         * Is the thread allowed to start running
         * @return
         */
        private boolean canRun() {
            return running<MAX_RUNNERS;
        }

        /**
         * Are there threads waiting to start running
         * @return
         */
        public boolean activeLeft() {
            return active > 0;
        }

        /**
         * Finish the mutex. This mutex cannot be used after this
         */
        public synchronized void finish() {
            active = 0;
            finished = true;
            notifyAll();
        }

        /**
         * Checks if this mutex is finished
         * @return
         */
        public boolean isFinished() {
            return finished;
        }
    }

    /**
     * The fetcher thread that does the actual prefetching
     * 
     */
    private class Prefetcher extends Thread {

        private MediaElement element;

        /**
         * @param element object that is to be prefetched
         */
        public Prefetcher(MediaElement element) {
            this.element = element;
        }

        public void run() {
            mutex.start();            
            mutex.run();

            if (!mutex.isFinished()) {
                try {
                    element.prefetch();
                } catch (Exception e) {
                    Log.error(e.getMessage());
                }
            }
                            
            mutex.stop();            
        }
    }

    /**
     * Add a new MediaElement to the prefetch batch
     * @param elem
     */
    public void addMedia(MediaElement elem) {
        fetchees.addElement(elem);
    }
    
    /**
     * Schedules a prefetch to be started as soon as possible, meaning the instance when the maximum amount of threads is not exhausted. This method doesn't block.
     * @param elem
     * @throws IllegalStateException if mutex is finished
     */
    public synchronized void doBackgroundPrefetch(MediaElement elem) throws IllegalStateException {
        if (mutex.isFinished()) {
            throw new IllegalStateException("Prefetcher has been interrupted");
        }
        (new Prefetcher(elem)).start();
    }

    /**
     * Does a batch prefetch, prefetching all the objects in the batch. Blocks until all objects are fetched.
     * @throws IllegalStateException if mutex is finished
     */
    public synchronized void doPrefetch() throws IllegalStateException {
        Log.debug("doPrefetch: " + fetchees.size() + " objects");        
        for (Enumeration e = fetchees.elements(); e.hasMoreElements();) {
            doBackgroundPrefetch((MediaElement) e.nextElement());
        }
        main = new Thread(this, "Prefetcher-Main");
        main.start();
        try {
            main.join();
        } catch (InterruptedException e) {
        }
        main = null;
        Log.debug("donePrefetch");
    }

    /**
     * Interrupt current batch prefetch and destroy the mutex. This MediaPrefetcher cannot be used after this.
     */
    public void interrupt() {
        if (main!=null) {
            main.interrupt();
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        synchronized (mutex) {
            while (mutex.activeLeft()) {
                Log.debug("Active: "+mutex.active+", running "+mutex.running);
                try {
                    mutex.wait();
                } catch (InterruptedException e) {
                    Log.debug("Prefetcher interrupted");
                    mutex.finish();
                }
            }
        }
    }
}