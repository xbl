/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution please refer to the LICENSE_XSMILES file
 * included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.awt.EventQueue;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AsyncChangeHandler;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.dom.MediaElement;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.timesheet.TimedElement;
import fi.hut.tml.xsmiles.timesheet.timer.TimedEventTable;
import fi.hut.tml.xsmiles.timesheet.timer.Timer;
import fi.hut.tml.xsmiles.timesheet.timer.TimedEventTable.TimedEvent;

/**
 * Implementation of the Timesheets. Subclasses of this class implement all the timesheet elements in the document.
 * 
 * @author tjjalava
 * @since Mar 5, 2004
 * @version $Revision: 1.35 $, $Date: 2006/02/14 13:11:09 $
 */
public abstract class TimedElementImpl extends XSmilesElementImpl implements TimedElement, EventHandlerService {

    /**
     * Event code for the TimedEvent START_SCHEDULED command
     */
    public static final int START_SCHEDULED = 1001;

    public static final int START_EVENT = 1002;

    /**
     * Event code for the TimedEvent STOP command
     */
    public static final int STOP = 1003;
    
    /* Custom DOM events */
    
    public static final String SELECT_NEXT = "select-next";
    
    public static final String SELECT_PREV = "select-prev";
    
    public static final String SELECT_FIRST = "select-first";
    
    public static final String SELECT_LAST = "select-last";

    /**
     * Value used for repeat count when repeat attribute is infinite
     */
    protected static final int INFINITY = Integer.MAX_VALUE;

    /**
     * The current active state of this TimedElement instance
     */
    protected boolean active = false;

    /**
     * Event table for start and end events of the children of this TimedElement
     */
    protected TimedEventTable eventTable;

    /**
     * Current time
     */
    protected long currentTick = 0;

    /**
     * Is this instance paused
     */
    protected boolean paused = false;

    /**
     * The parent element of this TimedElement. TimesheetElement doesn't have a parent.
     */
    protected TimedElementImpl parent;

    /**
     * The value of <code>repeat</code> attribute
     */
    protected int repeatCount = 0;

    /**
     * The status of this TimedElement, whether it is started or not
     */
    protected boolean elementStarted;

    /**
     * Holds the information of when this element is to be started
     */
    protected Schedule beginSchedule;

    /**
     * Holds the information of when this element is to be stopped
     */
    protected Schedule endSchedule;

    /**
     * All children of this element that are instances of TimedElement. Helps enumerating them.
     */
    protected Vector childItems;

    /**
     * The event listener of this TimedElement
     */
    protected TimesheetEventListener eventListener;

    /**
     * Inner class that holds all the information for this element's start or stop scheduling
     */
    protected class Schedule {

        private long scheduledTicks = 0;
        private Hashtable events = new Hashtable();
        private boolean scheduled = false;
        private boolean indefinite = false;

        /**
         * Sets a new dom event the TimedElement should listen to
         * 
         * @param target the target element to listen to
         * @param event the event that should be listened
         * @param ticks the offset of how long after the event is captured, the TimedElement should react
         */
        public void addEvent(EventTarget target, String event, long ticks) {
            events.put(target.hashCode() + event, new Long(ticks));
            target.addEventListener(event, eventListener, false);
        }

        /**
         * Returns the offset of a certain event
         * 
         * @param target the target element of the event
         * @param event the event
         * @return the offset of how long after the event is captured, the TimedElement should react
         */
        public long getEventTime(EventTarget target, String event) {
            Long l = (Long) events.get(target.hashCode() + event);
            if (l == null) {
                return -1;
            } else {
                return l.longValue();
            }
        }

        /**
         * Sets the time attribute as definite or indefinite
         * 
         * @param value <code>true</code> to set indefinite, <code>false</code>
         */
        public void setIndefinite(boolean value) {
            indefinite = value;
        }

        /**
         * Is the time attribute set indefinite
         * 
         * @return <code>true</code> if attribute is indefinite
         */
        public boolean isIndefinite() {
            return indefinite;
        }

        /**
         * Is element scheduled to start on some fixed time, ie. element's schedule doesn't depend only on some dom event
         * 
         * @return <code>true</code> if the element is scheduled
         */
        public boolean isScheduled() {
            return scheduled;
        }

        /**
         * Set the fixed time for this schedule
         * 
         * @param ticks the time in ticks
         * @throws RuntimeException if the element is already schedule, thus preventing a double schedule
         */
        public void setScheduledTime(long ticks) {
            if (!scheduled) {
                scheduledTicks = ticks;
                scheduled = true;
            } else {
                throw new RuntimeException("Element is already scheduled");
            }
        }

        /**
         * Get the time element's scheduled time
         * 
         * @return the time in ticks
         */
        public long getScheduledTime() {
            return scheduledTicks;
        }
    }

    /**
     * Parses the repeat count of the argument
     * 
     * @param elem the element which repeat count is to be parsed
     * @return repeat count or
     * @{link #INFINITY INFINITY} if the attribute value is <code>infinite<code>
     */
    protected static int parseRepeatCount(TimedElementImpl elem) {
        String repeat = elem.getAttribute(REPEAT_ATTR);
        if (repeat != null) {
            if (repeat.equals(INDEFINITE_VALUE)) {
                return INFINITY;
            } else {
                try {
                    return Integer.parseInt(repeat);
                } catch (Throwable thr) {
                    return 0;
                }
            }
        } else {
            return 0;
        }
    }
    
    /**
     * Parses the attribute containing schedule information of this element, ie. either <code>begin</code> or <code>dur</code> attributes
     * 
     * @param attr the value of the attribute
     * @return an instance of inner class <code>Schedule</code> containing all the scheduling information of the attribute
     * @throws NumberFormatException if attribute cannot be parsed
     */
    protected Schedule parseTimeAttribute(String attr) throws NumberFormatException {
        Schedule schedule = new Schedule();
        if (attr == null || attr.length() == 0) {
            return schedule;
        }
        StringTokenizer tokens = new StringTokenizer(attr, ",");
        try {
            while (tokens.hasMoreTokens()) {
                String token = tokens.nextToken();

                if (token.equals(INDEFINITE_VALUE)) {
                    schedule = new Schedule();
                    schedule.setIndefinite(true);
                    return schedule;
                }

                String time = null;
                String observer = null;
                String event = null;
                int ind;
                if ((ind = token.indexOf('.')) > 0) {
                    observer = token.substring(0, ind);
                    token = token.substring(ind + 1);
                    if ((ind = token.indexOf('+')) > 0) {
                        event = token.substring(0, ind);
                        time = token.substring(ind + 1);
                    } else {
                        event = token;
                        time = "0ms";
                    }
                } else {
                    time = token;
                }

                long ticks = 0;
                if (time.endsWith("ms")) {
                    ticks = Timer.timeToTicks(Long.parseLong(time.substring(0, time.length() - 2)));
                } else {
                    if (time.endsWith("s")) {
                        ticks = Timer.timeToTicks(Long.parseLong(time.substring(0, time.length() - 1)) * 1000);
                    } else {
                        throw new NumberFormatException();
                    }
                }

                if (observer != null) {
                    if (event == null) {
                        throw new NumberFormatException();
                    }
                    schedule.addEvent((EventTarget) searchElementWithId(getOwnerDocument().getDocumentElement(), observer), event, ticks);
                } else {
                    schedule.setScheduledTime(ticks);
                }
            }
            return schedule;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        throw new NumberFormatException("Couldn't parse time: " + attr);
    }

    /**
     * Creates a new dom event. Events created have argument "can bubble" and "cancelable"
     * 
     * @param event the type of event
     * @return new Event instance
     */
    protected static Event createEvent(String event) {
        return EventFactory.createEvent(event, true, true);
    }

    /**
     * Subclasses must implement this to handle the repeating
     */
    protected abstract void doRepeat();

    /**
     * Called by a child element of this element to notify that the child is going to start due to some dom event it was listening to. Subclasses must implement
     * this to handle the waking up of one of it's children
     * 
     * @param elem the TimedElement that is starting
     */
    protected abstract void elementStarting(TimedElementImpl elem);

    /**
     * Subclasses must implement this to make it ready to be started
     */
    protected abstract void doStartElement();

    /**
     * Subclasses must implement this to take care of all things needed for this element to stop
     */
    protected abstract void doStopElement();

    /**
     * Checks whether this element should take some specific action when a child element notifies it's going to end. If nothing special is needed to be done,
     * the method should return false.
     * 
     * @param elem the element that is about to end
     * @return <code>true</code> if something special is done, <code>false</code> for the default action
     */
    protected abstract boolean handleElementEnded(TimedElementImpl elem);
    
    /**
     * Sets this element to listen to the selected custom DOMEvents. Subclasses provide the implementation if needed.
     */
    protected void setEventListeners() {        
    }
    
    /**
     * Handle the DOMEvent targeted to this element.
     * 
     * @param evt the catched DOM event
     */
    protected void handleEvent(Event evt) {        
    }

    /**
     * Creates a new TimedElement instance. Initialized the eventTable *
     */
    protected TimedElementImpl(DocumentImpl owner, String namespace, String tag) {
        super(owner, namespace, tag);
        eventTable = new TimedEventTable();
        eventListener = new TimesheetEventListener(this);
    }

    /**
     * Informs this TimedElement instance that the argument has ended
     * 
     * @param elem the element instance that has ended
     */
    protected final void elementEnded(TimedElementImpl elem) {
        if (!handleElementEnded(elem)) {
            if (shouldRepeat()) {
                doRepeat();
            } else {
                stopElement();
                notifyEnd();
            }
        }
    }

    /**
     * Called by the Timer thread when eventTable has START_SCHEDULED event scheduled for the argument on current update round.
     */
    protected final void startElement() {
        if (!elementStarted) {
            scheduleEnd();
            elementStarted = true;
            doStartElement();
            if (elementStarted) {
                dispatchEvent(createEvent(ELEMENT_BEGIN_EVENT));
            }
        }
    }

    /**
     * Called by the Timer thread when eventTable has STOP event scheduled for the argument on current update round.
     */
    protected final void stopElement() {
        unSchedule();
        if (elementStarted) {
            doStopElement();
            elementStarted = false;
            dispatchEvent(createEvent(ELEMENT_END_EVENT));
        }
    }

    /**
     * Subclasses should implement this method for the actions caused by the change in pause status
     */
    protected void doSetPaused() {
    }

    /**
     * Informs the timesheet that the argument should know the changes in pause-state.
     * 
     * @param elem TimedElement needing the pause information
     */
    protected void addPauseListener(TimedElement elem) {
        parent.addPauseListener(elem);
    }

    /**
     * Returns a Hashtable that is common to all the elements in current timesheet. Can be used to save information commonly needed.
     * 
     * @return Hashtable
     */
    protected Hashtable getCache() {
        return parent.getCache();
    }

    /**
     * Informs the timesheet that the argument should be prefetched before the timing is started.
     * 
     * @param media TimedElement needing prefetch
     */
    protected void addPrefetchedMedia(MediaElement media) {
        ((TimedElementImpl) getParentNode()).addPrefetchedMedia(media);
    }

    /**
     * Schedules a new event.
     * 
     * @param tick time when event should happen
     * @param element TimedElement this event is concerning
     * @param event the event code, from {@link fi.hut.tml.xsmiles.timesheet.TimedElement TimedElement}interface
     */
    protected void scheduleEvent(long tick, int event) {
        eventTable.addEvent(tick, this, event);
    }

    /**
     * Unschedules all the children of this element and clears the event table
     */
    protected void unSchedule() {
        eventTable.clearEvents(this, START_SCHEDULED);
        eventTable.clearEvents(this, STOP);
        repeatCount = 0;
    }

    /**
     * Notifies the parent element that this element is about to start
     */
    protected void notifyStart() {
        if (parent != null) {
            parent.elementStarting(this);
        }
    }

    /**
     * Notifies the parent element that this element is about to end
     */
    protected void notifyEnd() {
        if (parent != null) {
            parent.elementEnded(this);
        }
    }

    /**
     * Schedule the time when this element should stop
     */
    protected synchronized void scheduleEnd() {
        if (endSchedule.isScheduled()) {
            scheduleEvent(currentTick + endSchedule.getScheduledTime(), STOP);
        }
        repeatCount = parseRepeatCount(this);
    }

    /**
     * Should this element still repeat itself
     * 
     * @return <code>true</code> if repeat count is still more that zero
     */
    protected boolean shouldRepeat() {
        if (repeatCount != INFINITY && repeatCount != 0) {
            repeatCount--;
        }
        return repeatCount > 0;
    }

    protected boolean hasEventScheduled(int event) {
        return eventTable.hasEvent(this, event);
    }

    /*
     * ------------------- XSmilesElementImpl ----------------------------------
     */

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.XSmilesElementImpl#init()
     */
    public void init() {
        Node p = getParentNode();
        if (p instanceof TimedElementImpl) {
            parent = (TimedElementImpl) p;
        }
        childItems = new Vector();
        NodeList children = getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof TimedElementImpl) {
                childItems.addElement(child);
            }
        }
        String bgn = getAttribute(BEGIN_ATTR);
        if (bgn == null || bgn.length() == 0) {
            bgn = "0s";
        }
        beginSchedule = parseTimeAttribute(bgn);
        endSchedule = parseTimeAttribute(getAttribute(DURATION_ATTR));
        setEventListeners();
        super.init();
    }

    /**
     * Sets the element active or non-active. For visual elements this propably means to set it visible or hidden
     * 
     * @param active true to set element active, false otherwise
     */
    public synchronized final void setActive(boolean active) {
        this.active = active;
        if (!active) {
            doStopElement();
            elementStarted = false;
        }
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {
            ((TimedElementImpl) e.nextElement()).setActive(active);
        }
    }

    /**
     * Sets the pause status of the element
     * 
     * @param paused pause status
     */
    public final void setPaused(boolean paused) {
        this.paused = paused;
        doSetPaused();
        dispatchEvent(createEvent(paused ? ELEMENT_PAUSED_EVENT : ELEMENT_NOT_PAUSED_EVENT));
    }
    
    /**
     * Enables or disables the timesheet. For timecontainers this method will recursively 
     * call the setEnabled-method of its children. The actual enabling/disabling happens in <code>ItemImpl</code>. 
     * 
     * @param enabled
     */
    public void setEnabled(boolean enabled) {        
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {
            ((TimedElementImpl) e.nextElement()).setEnabled(enabled);
        }
    }

    /**
     * Is element active right now
     * 
     * @return the active status
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Schedules the element, sets the time when the element should be set active. If element has children, this should propably also schedule them somehow.
     * 
     * @param startTime start active
     */
    public void schedule() {
        if (beginSchedule.isScheduled()) {
            long start = beginSchedule.getScheduledTime();
            if (start > 0) {
                scheduleEvent(currentTick + beginSchedule.getScheduledTime(), START_SCHEDULED);
            } else {
                scheduleEvent(currentTick, START_SCHEDULED);
                //startElement();
            }
        }
    }

    /**
     * Is element paused right now
     * 
     * @return pause status
     */
    public boolean isPaused() {
        return paused;
    }

    /*
     * ----------------------- TimedElement interface -----------------------------
     */
    private void doUpdate()
    {
        for (Enumeration e = eventTable.getEvents(currentTick); e.hasMoreElements();) {
            TimedEvent event = (TimedEvent) e.nextElement();
            switch (event.getEvent()) {
            case START_EVENT:
                notifyStart();
            case START_SCHEDULED:
                startElement();
                break;
            case STOP:
                stopElement();
                notifyEnd();
                break;
            }
        }
        currentTick++;
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {
            ((TimedElement) e.nextElement()).update();
        }
        
    }
    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.timesheet.TimedElement#update(long)
     */
    public synchronized void update() {
        if (!isActive()) {
            return;
        }
        // this takes care that doc changes and events are run in the main thread!
        boolean isDispatch=(this.getOwnerDocument() instanceof AsyncChangeHandler)?
                ((AsyncChangeHandler)this.getOwnerDocument()).isDispatchThread()
                :
                    EventQueue.isDispatchThread();
        if (isDispatch)
        {
               doUpdate();
        }
        else
        {
            Runnable r =  new Runnable()
            {
                public void run()
                {
                    doUpdate();
                }
            };
            if (this.getOwnerDocument() instanceof AsyncChangeHandler)
            {
                ((AsyncChangeHandler)this.getOwnerDocument()).invokeLater(r);
            }
            else
            {
                EventQueue.invokeLater(r);
            }
        }
    }

    /*
     * --------------------------------- EventHandlerService --------------------------------------------
     */

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.EventHandlerService#activate(org.w3c.dom.events.Event)
     */
    public synchronized void activate(Event evt) {
        EventTarget target = evt.getTarget();
        String eventType = evt.getType();
        long ticks;
        
        // checks if the event can be found in the schedule tables
        if ((ticks = beginSchedule.getEventTime(target, eventType)) >= 0) {
            Log.debug(toString() + ": START_EVENT");
            scheduleEvent(currentTick + ticks, START_EVENT);
            return;
        } 
       
        if ((ticks = endSchedule.getEventTime(target, eventType)) >= 0) {
            Log.debug(toString() + ": STOP");
            scheduleEvent(currentTick + ticks, STOP);
            return;
        }
        
        // otherwise it's some custom event that should be handled in the subclass
        handleEvent(evt);
    }

    /*
     * --------------------------------------------- Object ------------------------------------------------
     */

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String ret = "timesheet:" + getLocalName();
        if (getId().length() > 0) {
            ret += ":" + getId();
        }
        if (getAttribute(SELECT_ATTR).length() > 0) {
            ret += "->" + getAttribute(SELECT_ATTR);
        }
        return ret;
    }
}