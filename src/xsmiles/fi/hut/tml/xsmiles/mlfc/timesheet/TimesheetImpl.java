/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.MediaElement;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.timesheet.TimedElement;

/**
 * This is the implementation of the timesheet root element. Every timesheet
 * has only one instance of this class.
 * 
 * @author tjjalava
 * @since Feb 26, 2004
 * @version $Revision: 1.24 $, $Date: 2004/10/14 11:02:49 $
 */
public class TimesheetImpl extends TimedElementImpl {

    private TimeSheetMLFC mlfc;
    private MLFC hostMlfc;
    private Hashtable cache = new Hashtable();
    private MediaPrefetcher prefetcher = new MediaPrefetcher();    

    /**
     * Creates the root element implementation for the timesheet
     * 
     * @param mlfc
     *                   owner MLFC
     * @param owner
     *                   the document of the timesheet
     * @param namespace
     * @param tag
     */
    public TimesheetImpl(TimeSheetMLFC mlfc, DocumentImpl owner, String namespace, String tag) {
        super(owner, namespace, tag);
        this.mlfc = mlfc;
        hostMlfc = ((ExtendedDocument) owner).getHostMLFC();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.XSmilesElementImpl#init()
     */
    public void init() {
        super.init();
        setActive(false);
        mlfc.addPauseListener(this);
    }

    /**
     * Starts prefetching items that need to be prefetched. This method hangs
     * until all items are prefetched or
     * {@link #interruptPrefetch() interruptPrefetch()}is called
     */
    public void doPrefetch() {
        prefetcher.doPrefetch();
    }

    /**
     * Interrupts the current prefetch. Causes
     * {@link #doPrefetch() doPrefetch()}method to return immediately
     */
    public void interruptPrefetch() {
        prefetcher.interrupt();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#addPauseListener(fi.hut.tml.xsmiles.timesheet.TimedElement)
     */
    protected void addPauseListener(TimedElement elem) {
        mlfc.addPauseListener(elem);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#getCache()
     */
    protected Hashtable getCache() {
        return cache;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#addPrefetchedMedia(fi.hut.tml.xsmiles.dom.MediaElement)
     */
    protected void addPrefetchedMedia(MediaElement media) {
        prefetcher.addMedia(media);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.timesheet.TimedElement#update(long)
     */
    public void update() {
        hostMlfc.startUpdateTransaction();
        super.update();
        hostMlfc.commitUpdateTransaction();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#setEnabled(boolean)
     */
    public void setEnabled(boolean enabled) {
        hostMlfc.startUpdateTransaction();
        super.setEnabled(enabled);
        hostMlfc.commitUpdateTransaction();
    }
    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doRepeat()
     */
    protected void doRepeat() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#handleElementEnded(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected boolean handleElementEnded(TimedElementImpl elem) {
        return true;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStartElement()
     */
    protected void doStartElement() {
        for (Enumeration e=childItems.elements();e.hasMoreElements();) {
            ((TimedElementImpl)e.nextElement()).schedule();
        }
    }
    
    protected void doStopElement() {
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {            
            ((TimedElementImpl) e.nextElement()).stopElement();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#elementStarting(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected void elementStarting(TimedElementImpl elem) {
    }
}
