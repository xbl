/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.awt.event.MouseEvent;

import fi.hut.tml.xsmiles.dom.MediaElement;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.timesheet.TimedElement;

/**
 * Controls the elements that refer to media outside this document
 * 
 * @author tjjalava
 * @since Apr 6, 2004
 * @version $Revision: 1.8 $, $Date: 2004/10/14 11:18:13 $
 */
class MediaElementController extends ElementController implements MediaListener {

    private MediaElement mediaElement;
    private boolean needsPrefetch = false;    

    /**
     * New MediaElementController
     * @param parent temporal parent
     * @param elem the element this controller is controlling
     */
    public MediaElementController(ItemImpl parent, XSmilesElementImpl elem) {
        super(parent, elem);
        mediaElement = (MediaElement) elem;
        String pref = parent.getAttribute(TimedElement.PREFETCH_ATTR);
        if (pref!=null && Boolean.valueOf(pref).booleanValue()) {
            try {
                needsPrefetch = true;
                //mediaElement.prefetch();
            } catch (Exception e) {            
                e.printStackTrace();
            }
        }        
    }
    
    /**
     * Whether the controlled element needs to prefetched
     * @return
     */
    public boolean needsPrefetch() {
        return needsPrefetch;
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.ElementController#isStatic()
     */
    public boolean isStatic() {
        return mediaElement.isStatic();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.ElementController#setActive(boolean)
     */
    public void setActive(boolean active) {        
        if (active) {
            try {
                mediaElement.addMediaListener(this);
                mediaElement.setActive(true);
                mediaElement.prefetch();
                super.setActive(true);
                mediaElement.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            super.setActive(false);
            mediaElement.removeMediaListener(this);
            mediaElement.stop();
            mediaElement.setActive(false);
        }
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.ElementController#setEnabled(boolean)
     */
    public void setEnabled(boolean enabled) {
        if (!enabled) {
            try {
                super.setEnabled(false);
                mediaElement.setActive(true);                
                mediaElement.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {            
            mediaElement.stop();
            mediaElement.setActive(false);
            super.setEnabled(true);
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.ElementController#setPaused(boolean)
     */
    public void setPaused(boolean paused) {
        boolean mediaPaused = mediaElement.getStatus() == MediaElement.PAUSED;
        if (mediaPaused != paused) {
            mediaElement.pause();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mediaPrefetched()
     */
    public void mediaPrefetched() {
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mediaEnded()
     */
    public void mediaEnded() {
        parent.mediaEnded(this);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e) {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseEntered()
     */
    public void mouseEntered() {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseExited()
     */
    public void mouseExited() {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mousePressed()
     */
    public void mousePressed() {
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.gui.media.general.MediaListener#mouseReleased()
     */
    public void mouseReleased() {
    }
}
