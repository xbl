/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;


/**
 * This class controls the temporal state of one xml document element. Instances of this class control only the
 * elements that are rendered by the local renderer. For elements that refer to outside objects (mainly <code>&lt;object></code> elements),
 * {@link fi.hut.tml.xsmiles.mlfc.timesheet.MediaElementController MediaElementController} is used instead.
 * 
 * @author tjjalava
 * @since Apr 6, 2004
 * @version $Revision: 1.9 $, $Date: 2004/10/14 11:02:49 $
 */
class ElementController {
    
    /**
     * The name of the pseudo class that controls the visibility of timed elements
     */
    protected static final String INACTIVE_CLASS = "timed-inactive";
    
    /**
     * This is the key used when finding out, which controller controls an element
     */
    protected static final String TIMESHEET_OWNER = "timesheetowner";
    
    /**
     * The element this ElementController is controlling
     */
    protected XSmilesElementImpl element;
    
    protected boolean active = false;
    
    /**
     * The temporal parent of this controller
     */
    protected ItemImpl parent;
    private String id = String.valueOf(hashCode());
    private String cssClass;    

    /**
     * New ElementController
     * @param parent temporal parent, the timesheet item element that is creating this controller
     * @param elem the element this controller is controlling
     */
    public ElementController(ItemImpl parent, XSmilesElementImpl elem) {
        this.element = elem;         
        this.parent = parent;
        
        cssClass = parent.getAttribute("class");
        if (cssClass.length()==0) {
            cssClass = null;
        }
        element.setPseudoClass(INACTIVE_CLASS, true);
    }
        
    /**
     * Sets this element active or inactive
     * @param active
     */
    public void setActive(boolean active) {
        if (element==null) {
            return;
        }         
        
        if (this.active == active) {
            return;
        }
        this.active = active;
        
        // set the "timed-inactive"-pseudoclass that should hide the element when needed
        // timesheetowner-stuff is for the situations when two overlapping shedulings would cancel each other
        // this way no timed item should set element inactive if some other timed item has taken control over it
        // if cssClass is set (attribute "class"), set pseudoclass <cssClass> on or off
        if (active) {
            element.setPseudoClass(INACTIVE_CLASS, false);
            if (cssClass!=null) {
                element.setPseudoClass(cssClass, true);
            }            
            element.setUserData(TIMESHEET_OWNER, id, null);            
        } else {            
            String timesheetOwner = (String) element.getUserData(TIMESHEET_OWNER);            
            if (timesheetOwner==null || timesheetOwner.length()==0 || timesheetOwner.equals(id)) {                
                //element.removeAttribute(TIMESHEET_OWNER);
                element.setUserData(TIMESHEET_OWNER, null, null);
                element.setPseudoClass(INACTIVE_CLASS, true);
                if (cssClass!=null) {
                    element.setPseudoClass(cssClass, false);
                }
            }                
        }
                                
        if (element instanceof VisualElementImpl) {
            ((VisualElementImpl)element).styleChanged();
        }
    }
    
    /**
     * Enables or disables the timesheet control on this element. If timesheet control is disabled, the
     * document will lay the element out normally, otherwise it is laid out according to the timesheet.
     * 
     * @param enabled
     */
    public void setEnabled(boolean enabled) {        
        element.setPseudoClass(INACTIVE_CLASS, enabled);
        if (element instanceof VisualElementImpl) {
            ((VisualElementImpl)element).styleChanged();
        }
    }
    
    /**
     * Sets this element paused or non-paused
     * @param paused
     */
    public void setPaused(boolean paused) {        
    }
    
    /**
     * Static elements have no defined duration. These include all html elements and object elements that refer to static media, like images. 
     * Non-static media include audio and video
     * @return
     */
    public boolean isStatic() {
        return true;
    }
    
    public boolean isActive() {
        return active;
    }
}
