/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;

import fi.hut.tml.xsmiles.dom.EventHandlerService;


/**
 * Class TimesheetEventListener
 * 
 * @author tjjalava
 * @since Apr 27, 2004
 * @version $Revision: 1.1 $, $Date: 2004/04/29 15:24:44 $
 */
class TimesheetEventListener implements EventListener {

    
    private EventHandlerService handler;

    public TimesheetEventListener(EventHandlerService handler) {
        this.handler = handler;
    }
    
    /* (non-Javadoc)
     * @see org.w3c.dom.events.EventListener#handleEvent(org.w3c.dom.events.Event)
     */
    public void handleEvent(Event evt) {
        handler.activate(evt);
    }

}
