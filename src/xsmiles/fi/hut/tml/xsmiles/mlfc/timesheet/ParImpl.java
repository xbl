/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.util.Enumeration;

import org.apache.xerces.dom.DocumentImpl;

/**
 * Implementation of the "par" element in timesheet
 * 
 * @author tjjalava
 * @since Mar 5, 2004
 * @version $Revision: 1.18 $, $Date: 2004/09/27 13:03:18 $
 */
public class ParImpl extends TimedElementImpl {

    private int childCount = 0;

    /**
     * New instance for the element
     * 
     * @param owner
     * @param namespace
     * @param tag
     */
    public ParImpl(DocumentImpl owner, String namespace, String tag) {
        super(owner, namespace, tag);
    }

    protected boolean handleElementEnded(TimedElementImpl elem) {
        childCount--;
        return childCount != 0 || eventTable.hasEvent(this, STOP) || endSchedule.isIndefinite();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doRepeat()
     */
    protected void doRepeat() {
        doStartElement();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStartElement()
     */
    protected void doStartElement() {
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {
            childCount++;
            ((TimedElementImpl) e.nextElement()).schedule();
        }
        if (childCount==0 && !eventTable.hasEvent(this, STOP)) {
            stopElement();
            notifyEnd();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStopElement()
     */
    protected void doStopElement() {
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {            
            ((TimedElementImpl) e.nextElement()).stopElement();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#elementStarting(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected void elementStarting(TimedElementImpl elem) {
        if (!elementStarted) {
            notifyStart();
            elementStarted = true;            
            if (!eventTable.hasEvent(this, STOP)) {
                scheduleEnd();
            }
        }
        childCount++;
    }
}
