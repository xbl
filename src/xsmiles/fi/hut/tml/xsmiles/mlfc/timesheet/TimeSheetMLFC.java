/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution please refer to the LICENSE_XSMILES file
 * included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XCaption;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XSelectBoolean;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;
import fi.hut.tml.xsmiles.timesheet.TimedElement;
import fi.hut.tml.xsmiles.timesheet.timer.Clocker;
import fi.hut.tml.xsmiles.timesheet.timer.Timer;

/**
 * The MLFC for Timesheets
 * 
 * @author tjjalava
 * @since Feb 26, 2004
 * @version $Revision: 1.33 $, $Date: 2005/07/22 16:21:31 $
 */
public class TimeSheetMLFC extends CoreMLFC implements ActionListener, Clocker, ItemListener {

    private static final String RUNNING = "    Running    ";
    private static final String PAUSED = "    Paused     ";
    private static final String STOPPED = "    Stopped    ";
    private static final String INITIALIZING = "   Initializing   ";
    private static final String DISABLED = "   Disabled   ";

    private static final String PLAY = "Play";
    private static final String STOP = "Stop";
    private static final String PAUSE = "Pause";
    private static final String DISABLE = "Disable Timesheet";

    private static final short INIT_STATE = 10;
    private static final short PLAY_STATE = 11;
    private static final short PAUSE_STATE = 12;
    private static final short STOP_STATE = 13;
    private static final short DISABLED_STATE = 14;

    private TimesheetImpl timeRoot;
    private Vector tmpRoots;
    private Timer timer;
    private XCaption stateLabel;
    private XCaption timeLabel;
    private Vector pauseListeners = new Vector();
    private boolean readyToPlay = false;
    private boolean paused = false;
    private boolean stopCalled = false;
    private XButton play;
    private XButton pause;
    private XButton stop;
    private XSelectBoolean disable;
    private XCaption disableLabel;

    private long seconds = 0;  
    private boolean initialized = false;

    /**
     * Handles the actions made by user clicking the buttons
     *  
     */
    private class ActionThread extends Thread {

        private String action;

        /**
         * Creates a new action handler for a specified action
         * 
         * @param action the specified action
         */
        public ActionThread(String action) {
            this.action = action;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run()
         */
        public void run() {
            if (action.equals(PLAY)) {
                TimeSheetMLFC.this.start();
                return;
            }

            if (action.equals(STOP)) {
                TimeSheetMLFC.this.stop();
                return;
            }

            if (action.equals(PAUSE)) {
                TimeSheetMLFC.this.pause();
                return;
            }
            
            if (action.equals("DISABLE")) {
                TimeSheetMLFC.this.disable();
                return;
            }
            
            if (action.equals("ENABLE")) {
                TimeSheetMLFC.this.enable();
                return;
            }
        }
    }
    
    /**
     * Adds a new TimedElement to the list of elements that need to get the Pause-events
     * 
     * @param pauseLsnr
     */
    public void addPauseListener(TimedElement pauseLsnr) {
        if (!pauseListeners.contains(pauseLsnr)) {
            pauseListeners.addElement(pauseLsnr);
        }
    }

    /**
     * Enables the timesheet applied to the document
     */
    private void enable() {
        timeRoot.setEnabled(true);
        setState(STOP_STATE);
    }

    /**
     * Disables the timesheet applied to the document. All elements in the document are laid out
     * like there were no timesheet.
     */
    private void disable() {        
        setState(DISABLED_STATE);
        timeRoot.setEnabled(false);
    }

    /**
     * Sets the state of the timesheet. Enables/disables the buttons and sets the correct state label
     * 
     * @param state
     */
    private void setState(short state) {
        if (play != null) {
            switch (state) {
            case INIT_STATE:
                play.setEnabled(false);
                pause.setEnabled(false);
                stop.setEnabled(false);
		disable.setEnabled(false);
		disableLabel.setEnabled(false);
		stateLabel.setText(INITIALIZING);
		timeLabel.setText("    ");
                seconds = 0;
                break;
            case PLAY_STATE:
                readyToPlay = true;
                play.setEnabled(false);
                pause.setEnabled(true);
                stop.setEnabled(true);
		disable.setEnabled(false);
		disableLabel.setEnabled(false);
		stateLabel.setText(RUNNING);
		timeLabel.setText(seconds + " s      ");
		break;
            case PAUSE_STATE:
                play.setEnabled(true);
                pause.setEnabled(true);
                stop.setEnabled(true);
		disable.setEnabled(false);
		disableLabel.setEnabled(false);
		stateLabel.setText(PAUSED);
		break;
            case STOP_STATE:
                play.setEnabled(true && readyToPlay);
                pause.setEnabled(false);
                stop.setEnabled(false);
		disable.setEnabled(true);
		disableLabel.setEnabled(true);
		stateLabel.setText(STOPPED);
		timeLabel.setText("       ");
		seconds = 0;
                break;
            case DISABLED_STATE:
                play.setEnabled(false);
                pause.setEnabled(false);
                stop.setEnabled(false);
		disable.setEnabled(true);
		disableLabel.setEnabled(true);
		stateLabel.setText(DISABLED);
		timeLabel.setText("       ");
		seconds = 0;
                break;
            }
        }
    }

    /**
     * Pauses or unpauses the MLFC
     */
    public synchronized void pause() {
        if (timeRoot == null || !timeRoot.isActive()) {
            return;
        }
        Log.debug("TimeSheetMLFC pause()");
        paused = !paused;
        timer.pause(paused);
        setState(paused ? PAUSE_STATE : PLAY_STATE);
        for (Enumeration e = pauseListeners.elements(); e.hasMoreElements();) {
            ((TimedElementImpl) e.nextElement()).setPaused(paused);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        (new ActionThread(e.getActionCommand())).start();
    }
    
    /* (non-Javadoc)
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
            (new ActionThread("DISABLE")).start();
        } else {
            (new ActionThread("ENABLE")).start();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#start()
     */
    public synchronized void start() { 
        if (!initialized) {
            return;
        }
        if (paused) {
            pause();
        }
        if (!timeRoot.isActive()) {
            setState(INIT_STATE);
            Log.debug("TimeSheetMLFC start()");
            if (!stopCalled) {
                timeRoot.doPrefetch(); //may take a long time
            }
            if (!stopCalled) {
                timeRoot.schedule();
                timeRoot.setActive(true);
                setState(PLAY_STATE);
                timer.start();
            }
            stopCalled = false;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#stop()
     */
    public void stop() {
        if (!initialized) {
            return;
        }
        stopCalled = true;
        if (timeRoot != null) {
            // interrupt the prefetch process, otherwise this can take forever
            timeRoot.interruptPrefetch();

            synchronized (this) {
                if (timeRoot.isActive()) {
                    Log.debug("TimeSheetMLFC stop()");
                    if (paused) {
                        pause();
                    }
                    timer.stop();
                    timeRoot.setActive(false);
                }
            }
            setState(STOP_STATE);
        }
        stopCalled = false;
    }
    
    /**
     * Goes through all timesheet elements defined in the document and selects the first suitable timesheet according to the
     * <code>media</code> attribute of the element. If no timesheets are defined for current media, returns null. Also, if 
     * <code>src</code> attribute is defined, fetches the external timesheet and appends the elements to the end of the locally
     * defined timesheet.
     * 
     * @return the selected timesheet, or null if non was selected.
     * @throws IOException external document couldn't be read.
     * @throws SAXException XML parser error.
     */
    private TimesheetImpl selectTimesheet() throws IOException, SAXException {        
        for (Enumeration e=tmpRoots.elements();e.hasMoreElements();) {
            TimesheetImpl timeSheet = (TimesheetImpl) e.nextElement();
            
            // Check Media Query attribute
            String media = timeSheet.getAttribute("media");
            if (media != null && media.length() > 0) {
                // Evaluate media string
                if (getXMLDocument().evalMediaQuery(media) == false) {
                    Log.debug("Skipping <timesheet> element, media query evaluated to false: "+ media);
                    continue;
                }
            }
            // Media Query passed
            	
            String src = timeSheet.getAttribute("src");        
            if (src != null && src.length() > 0) {
                URL url = timeSheet.resolveURI(src);
                Document external = Browser.xmlParser.openDocument(url);
                Node time = timeSheet.getOwnerDocument().importNode(external.getDocumentElement(), true);                    
                NodeList children = time.getChildNodes();
                for (int j=0;j<children.getLength();j++) {
                    Node child = children.item(j);
                    if (child instanceof TimedElementImpl) {
                        timeSheet.appendChild(child);            	            
                    }
                }            	    
                timeSheet.init();
            }
            return timeSheet;
        }
        return null;
    }
    
    public TimeSheetMLFC() {
        tmpRoots = new Vector();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#init()
     */
    public void init() {
        Log.debug("TimeSheetMLFC init()");
        try {
            timeRoot = selectTimesheet();
        } catch (Exception e1) {        
            Log.error(e1, "Couldn't select a timesheet");
        }
        if (timeRoot == null) {
            return;
        }
        timer = new Timer(timeRoot);
        boolean showClock = false;
        try {
            showClock = new Boolean(getMLFCListener().getProperty("timesheet/showclock")).booleanValue();
        } catch (Exception e) {
            Log.error(e);
        }

        if (isPrimary()) {
            ComponentFactory cf = getMLFCListener().getComponentFactory();
            XPanel cb = getMLFCListener().getMLFCControls().getMLFCToolBar();
            System.out.println("WE ARE ADDING HERE:"+cb.toString());
	    cb.removeAll();
            try {
                play = cf.getXButton(PLAY, "img/splay.gif");
                pause = cf.getXButton(PAUSE, "img/spause.gif");
                stop = cf.getXButton(STOP, "img/sstop.gif");
                disable = cf.getXSelectBoolean();
                play.addActionListener(this);
                pause.addActionListener(this);
                stop.addActionListener(this);
                disable.addItemListener(this);
                
		stateLabel = cf.getXCaption(INITIALIZING);
                stateLabel.setBackground(null);
                disableLabel = cf.getXCaption(DISABLE);
                disableLabel.setBackground(null);
                disable.setBackground(null);
                timeLabel = cf.getXCaption("       ");
                timeLabel.setBackground(null);

                cb.add(play);
                cb.add(pause);
                cb.add(stop);
                cb.add(disableLabel);
                cb.add(disable);
                cb.add(stateLabel);

                if (showClock) {
                    timer.setClocker(this);
                    cb.add(timeLabel);
                }
                setState(INIT_STATE);
            } catch (Throwable t) {
                Log.error(t);
            }
        }
        initialized = true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.mlfc.MLFC#createElementNS(org.apache.xerces.dom.DocumentImpl, java.lang.String, java.lang.String)
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag) { 
        if (tag == null) {
            Log.error("Invalid null tag name!");
            return null;
        }
        String localname = getLocalname(tag);

        if (localname.equals(TimedElement.TIMESHEET_ELEM) || localname.equals("time")) { // time is for the older timesheet documents..
            TimesheetImpl tmpRoot = new TimesheetImpl(this, doc, ns, tag);
            tmpRoots.addElement(tmpRoot);
            return tmpRoot;
        }

        if (localname.equals(TimedElement.SEQ_ELEM)) {
            return new SeqImpl(doc, ns, tag);
        }

        if (localname.equals(TimedElement.EXCL_ELEM)) {
            return new ExclImpl(doc, ns, tag);
        }

        if (localname.equals(TimedElement.PAR_ELEM)) {
            return new ParImpl(doc, ns, tag);
        }

        if (localname.equals(TimedElement.ITEM_ELEM)) {
            return new ItemImpl(doc, ns, tag);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.timesheet.timer.Clocker#update()
     */
    public void update() {
        if (timeLabel != null) {
            timeLabel.setText((++seconds) + " s     ");
        }
    }
}
