/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.timesheet;

import java.util.Enumeration;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.events.Event;

/**
 * This class implements the "seq" element of the timesheet.
 * 
 * @author tjjalava
 * @since Feb 26, 2004
 * @version $Revision: 1.22 $, $Date: 2004/10/07 12:28:20 $
 */
public class SeqImpl extends TimedElementImpl {
    
    /**
     * List of custom DOMEvents this element is listening
     */
    private static String[] implementedEvents = {
        SELECT_NEXT,
        SELECT_PREV,
        SELECT_FIRST,
        SELECT_LAST
    };
        
    private int currentIndex = 0;
    
    /**
     * Creates a new instance
     * 
     * @param owner
     * @param namespace
     * @param tag
     */
    public SeqImpl(DocumentImpl owner, String namespace, String tag) {
        super(owner, namespace, tag);
    }
        
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#handleElementEnded(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected boolean handleElementEnded(TimedElementImpl elem) {
        int index = childItems.indexOf(elem)+1;
        if (index>=childItems.size()) {            
            return eventTable.hasEvent(this, STOP) || endSchedule.isIndefinite();
        } else {
            ((TimedElementImpl)childItems.elementAt(index)).schedule();
            currentIndex = index;
            return true;
        }
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doRepeat()
     */
    protected void doRepeat() {
        doStartElement();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStartElement()
     */
    protected void doStartElement() {
        if (childItems.size()>0) {
            ((TimedElementImpl)childItems.elementAt(0)).schedule();
            currentIndex = 0;
        } else {
            if (!eventTable.hasEvent(this, STOP)) {
                notifyEnd();
            }
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#doStopElement()
     */
    protected void doStopElement() {
        for (Enumeration e = childItems.elements(); e.hasMoreElements();) {            
            ((TimedElementImpl) e.nextElement()).stopElement();
        }
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#elementStarting(fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl)
     */
    protected void elementStarting(TimedElementImpl elem) {
        if (!elementStarted) {
            notifyStart();
            elementStarted = true;
            if (!eventTable.hasEvent(this, STOP)) {
                scheduleEnd();
            }
        } else {
            ((TimedElementImpl)childItems.elementAt(currentIndex)).stopElement();
        }
        currentIndex = childItems.indexOf(elem);
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#setEventListeners()
     */
    protected void setEventListeners() {
        for (int i=0;i<implementedEvents.length;i++) {
            addEventListener(implementedEvents[i], eventListener,false);
        }
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.mlfc.timesheet.TimedElementImpl#handleEvent(org.w3c.dom.events.Event)
     */
    protected void handleEvent(Event evt) {
        if (!isActive()) {
            return;
        }
        
        String event = evt.getType();
        
        if (event.equals(SELECT_NEXT) && currentIndex<childItems.size()-1) {
            ((TimedElementImpl)childItems.elementAt(currentIndex)).stopElement();
            currentIndex++;
            ((TimedElementImpl)childItems.elementAt(currentIndex)).schedule();            
            return;
        }
        
        if (event.equals(SELECT_PREV) && currentIndex>0) {
            ((TimedElementImpl)childItems.elementAt(currentIndex)).stopElement();
            currentIndex--;
            ((TimedElementImpl)childItems.elementAt(currentIndex)).schedule();            
            return;
        }
        
        if (event.equals(SELECT_FIRST) && currentIndex != 0) {
            ((TimedElementImpl)childItems.elementAt(currentIndex)).stopElement();
            currentIndex = 0;
            ((TimedElementImpl)childItems.elementAt(currentIndex)).schedule();            
            return;
        }
        
        if (event.equals(SELECT_LAST) && currentIndex != childItems.size()-1) {
            ((TimedElementImpl)childItems.elementAt(currentIndex)).stopElement();
            currentIndex = childItems.size()-1;
            ((TimedElementImpl)childItems.elementAt(currentIndex)).schedule();            
            return;
        }
    }
}