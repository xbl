/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.mlfc.general; 
 

public interface MediaQueryEvaluator {

    public static final String[] MEDIA_TYPES = {
            "braille",
            "embossed",
            "handheld",
            "print",
            "projection",
            "screen",
            "speech",
            "tty",
            "tv"
    };

    public boolean evalMediaQuery (String stringToEvaluate);
}
