/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.mlfc.general;

import java.awt.*;
import fi.hut.tml.xsmiles.gui.compatibility.CompatibilityFactory;

/**
 * ColorConverter parses and returns color values.
 *
 * Modified by Kari Pihkala 20.2.2001 - supports now color names (green, blue...)
 * Modified by KP 11.5.2001 		  - supports now rgb(r,g,b) syntax
 */
public class ColorConverter {

  public static Color hexaToRgb(String hexa) {
  	Color rgb = null;
	String cname;
	Integer r,g,b;
	
	if (hexa == null)
		hexa="transparent";
		
  	if (hexa.length() > 0 && hexa.charAt(0) != '#') {
	
		// Color names are case-insensitive
		cname = hexa.toLowerCase();
	
		// rgb(r, g, b)
		if (cname.startsWith("rgb(") == true) {
			try {
				r = new Integer(cname.substring(4,cname.indexOf(",")).trim());
				g = new Integer(cname.substring(cname.indexOf(",")+1,cname.lastIndexOf(",")).trim());
				b = new Integer(cname.substring(cname.lastIndexOf(",")+1,cname.length()-1).trim());
			} catch (NumberFormatException e) {
				return new Color(0xff, 0xff, 0xff);
			} catch (IndexOutOfBoundsException e) {
				// If ',' was not found
				return new Color(0xff, 0xff, 0xff);
			}
			return new Color(r.intValue(), g.intValue(), b.intValue());
		}
	
		// The name of the color has been given (green, blue, black...)
		if (cname.equals("black")) 
			rgb = new Color(0x00, 0x00, 0x00);
		if (cname.equals("silver")) 
			rgb = new Color(0xc0, 0xc0, 0xc0);
		if (cname.equals("gray")) 
			rgb = new Color(0x80, 0x80, 0x80);
		if (cname.equals("white")) 
			rgb = new Color(0xff, 0xff, 0xff);
		if (cname.equals("maroon")) 
			rgb = new Color(0x80, 0x00, 0x00);
		if (cname.equals("red")) 
			rgb = new Color(0xff, 0x00, 0x00);
		if (cname.equals("purple")) 
			rgb = new Color(0x80, 0x00, 0x80);
		if (cname.equals("fuchsia")) 
			rgb = new Color(0xff, 0x00, 0xff);
		if (cname.equals("green")) 
			rgb = new Color(0x00, 0x80, 0x00);
		if (cname.equals("lime")) 
			rgb = new Color(0x00, 0xff, 0x00);
		if (cname.equals("olive")) 
			rgb = new Color(0x80, 0x80, 0x00);
		if (cname.equals("yellow")) 
			rgb = new Color(0xff, 0xff, 0x00);
		if (cname.equals("navy")) 
			rgb = new Color(0x00, 0x00, 0x80);
		if (cname.equals("blue")) 
			rgb = new Color(0x00, 0x00, 0xff);
		if (cname.equals("teal")) 
			rgb = new Color(0x00, 0x80, 0x80);
		if (cname.equals("aqua")) 
			rgb = new Color(0x00, 0xff, 0xff);
		if (cname.equals("transparent")) 
                        // does not work in JDK 1.1
			//rgb = new Color(0x00, 0x00, 0x00, 0x00);
                        rgb= getTransparentColor();

	} else {

		// The hexacode of the color has been given (#ff1588)  
		if (hexa.length() >= 7) {
		    int red = intValue(hexa.substring(1, 3));
		    int green = intValue(hexa.substring(3, 5));
		    int blue = intValue(hexa.substring(5, 7));
		    rgb = new Color(red, green, blue);
		} else
		// The hexacode of the color has been given (#f0d)  
			if (hexa.length() >= 4) {
			    int red = intValue(hexa.substring(1, 2)+"0");
			    int green = intValue(hexa.substring(2, 3)+"0");
			    int blue = intValue(hexa.substring(3, 4)+"0");
			    rgb = new Color(red, green, blue);
			}
	}

	// Default color is transparent
	if (rgb == null)
            rgb= getTransparentColor();

    return rgb;
  }
  
  public static Color getTransparentColor()
  {
                        // does not work in JDK 1.1
			//rgb = new Color(0x00, 0x00, 0x00, 0x00);
                        return CompatibilityFactory.getCompatibility().createTransparentColor();
  }

  private static int intValue(String hexa) {
	int integer=Character.digit((hexa.charAt(0)), 16) * 16 + Character.digit((hexa.charAt(1)), 16);
	if (integer < 0)
		integer = 0;
	if (integer > 255)
		integer = 255;	

    return integer;
  }

  public static Color invertColor(Color color) {
      int red   = color.getRed();
      int green = color.getGreen();
      int blue  = color.getBlue();
      
      Color invert = new Color(255 - red, 255 - green, 255 - blue);
      return invert;
  }
  
}