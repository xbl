/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.general;
import java.util.*;
import java.io.*;
import fi.hut.tml.xsmiles.mlfc.*;
import fi.hut.tml.xsmiles.*;
import java.awt.event.*;
import java.net.URL;

public class Helper {

    public String documentBase;

    public Helper() {
        
    }

    public String parseRelativeURL(String src, XMLDocument xmldoc) {
           this.setDocumentBase(xmldoc);
           return this.parseRelativeURL(src);
    }
    
    
    public void setDocumentBase(XMLDocument xmldoc)
     {
       	String document = xmldoc.getLink().getURL().toString();
        document = document.replace('\\','/');
        int i = document.lastIndexOf("/");
        documentBase = document.substring(0, i);
        //Log.debug("Setting the document base at " + documentBase); 
    }

/**
 * parses an element's relative URL into a complete URL 
 *
 * @param String src : the relative location to be parsed
 * @return The parsed string (the same as URL.toString() would give
 */


    public String parseRelativeURL(String src)
    {
  		    int number = 0;
		    StringTokenizer baseurl = new StringTokenizer(documentBase, "/");
		    StringTokenizer relurl = new StringTokenizer(src, "/");
		    String endpart = new String("");
		    String path = new String("");

		    while (relurl.hasMoreElements())
		    {
			    //endpart = endpart.concat("/");
			    String s = (String)relurl.nextElement();
			    //Log.debug(s);
			    if (s.equals(".."))
			    {
				    number += 1;
			    } 
			    else if (s.equals("."))
			    {
			        endpart = endpart.concat(s);
			        if (relurl.hasMoreElements()) { endpart = endpart.concat("/"); }
    			    			    
			    }
			    else {
			        endpart = endpart.concat(s);
			        if (relurl.hasMoreElements()) { endpart = endpart.concat("/"); }
			    }
    			
		    }
            //Log.debug("effective endpart : " + endpart);

		    int len = baseurl.countTokens() - number;
		    int i = 0;
		    while(i < len)
		    {
			    String toadd = (String)baseurl.nextElement();
			    if (toadd.equals("http:")) { 
			        path = path.concat(toadd + "//"); 
			    } else if (toadd.equals("file:")){ 
			        path = path.concat(toadd + "///"); 
			    } else {
			        path = path.concat(toadd + "/" );
			    }
			    i++;
		    }

		    return new String(path.concat(endpart)); 		
	    }


	public static long copyStream(InputStream in, OutputStream out,int buffersize) throws IOException
	{
		byte[] buffer = new byte[buffersize];
		long count=0;
		while (true) {
		  int bytesRead = in.read(buffer);
		  if (bytesRead == -1) break;
		  count+=bytesRead;
		  out.write(buffer, 0, bytesRead);
		}
		return count;
	}
        
        
	public static long copyStream(Reader in, Writer out,int buffersize) throws IOException
	{
		char[] buffer = new char[buffersize];
		long count=0;
		while (true) {
		  int bytesRead = in.read(buffer);
		  if (bytesRead == -1) break;
		  count+=bytesRead;
		  out.write(buffer, 0, bytesRead);
		}
		return count;
	}
	

}