/**
 * Copyright � Sergey Melnik (Stanford University, Database Group) 
 *
 * Distribution policies are governed by the W3C software license.
 * http://www.w3.org/Consortium/Legal/copyright-software   
 * 
 * All Rights Reserved.
 */
package fi.hut.tml.xsmiles.mlfc.general;
//package org.w3c.rdf.tools.sorter;

import java.io.*;
import java.util.*;

/**
 * A quick sort implementation. Should be merged with <code>Sorter</code>...
 *
 * @author Sergey Melnik <melnik@db.stanford.edu>
 */

public final class QuickSort {

  Comparer comparer;
  Vector input;

  /**
   * To be passed along to the Comparer to
   * distinguish different types of sorting
   */
  Object handle;

  QuickSort(Vector input, Comparer comparer, Object handle) {

    this.input = input;
    this.comparer = comparer;
    this.handle = handle;
  }

  public static void sort(Vector input) {
    
    new QuickSort(input, null, null).quickSort();
  }

  /**
   * Takes a vector of objects as input and sorts them
   * using <code>comparer</code>. The <code>handle</code>
   * is passed during every invocation of <code>comparer.compare()</code>
   */  
  public static void sort(Vector input, Comparer comparer, Object handle) {

    new QuickSort(input, comparer, handle).quickSort();
  }

  void quickSort() {
    
    qs(0, input.size() - 1);
    return;
  }

  void qs(int lo0, int hi0) {

    int lo = lo0;
    int hi = hi0;

    if (lo >= hi) {
      return;
    }
    else if( lo == hi - 1 ) {
      /*
       *  sort a two element list by swapping if necessary 
       */
      if (compare(a(lo), a(hi)) > 0) {
	swap(lo, hi);
      }
      return;
    }

    /*
     *  Pick a pivot and move it out of the way
     */
    Object pivot = a((lo + hi) / 2);
    swap((lo + hi) / 2, hi);

    while( lo < hi ) {
      /*
       *  Search forward from a[lo] until an element is found that
       *  is greater than the pivot or lo >= hi 
       */
      while ( compare(a(lo), pivot) <= 0 && lo < hi) {
	lo++;
      }

      /*
       *  Search backward from a[hi] until element is found that
       *  is less than the pivot, or hi <= lo 
       */
      while (compare(pivot, a(hi)) <= 0 && lo < hi ) {
	hi--;
      }

      /*
       *  Swap elements a[lo] and a[hi]
       */
      if( lo < hi ) {
	swap(lo, hi);
      }

    }

    /*
     *  Put the median in the "center" of the list
     */
    input.setElementAt( a(hi), hi0 );
    input.setElementAt( pivot, hi );

    /*
     *  Recursive calls, elements a[lo0] to a[lo-1] are less than or
     *  equal to pivot, elements a[hi+1] to a[hi0] are greater than
     *  pivot.
     */
    qs(lo0, lo-1);
    qs(hi+1, hi0);
  }

  Object a(int pos) {

    return input.elementAt(pos);
  }

  /**
   * <0: o1 < o2
   *  0: o1 = o2
   * >0: o1 > o2
   */
  int compare(Object o1, Object o2) {

    if(comparer == null) { // try to do my best comparing objects

      if(o1 instanceof Integer) { // compare integers

	int i1 = ((Integer)o1).intValue();
	int i2 = ((Integer)o2).intValue();

	if(i1 < i2)
	  return -1;
	else if(i1 > i2)
	  return 1;
	else
	  return 0;

      } else { // compare string values

	return String.valueOf(o1).compareTo( String.valueOf(o2) );
      }
    } else { // we do have a comparer

      return comparer.compare(handle, o1, o2);
    }
  }

  void swap(int low, int high) {
    
    Object temp = input.elementAt(low);
    input.setElementAt(input.elementAt(high), low);
    input.setElementAt(temp, high);
    return;
  }

  public static void main(String[] args) {

    Vector v = new Vector();

    for(int i=0; i<args.length; i++)
      v.addElement( args[i] );

    QuickSort.sort(v);

    //    for (int i=0; i < input.length; i++) // we know it's "number" but
    //      System.out.println(v.elementAtinput[i]);  // arrays which we may use
    System.out.println("Result:\n" + v);
  }

}
