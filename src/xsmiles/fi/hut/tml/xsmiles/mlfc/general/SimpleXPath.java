/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.mlfc.general;

import fi.hut.tml.xsmiles.Log;

import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import java.util.StringTokenizer;

/**
  * This class implements a really simple XPath API
  * It is used instead of Xalan's XPathAPI, because
  * Xalan doesn't handle default namespaces correctly
  * This implementation just doesn't care about default namespaces...
  */
public class SimpleXPath
{
  private static int FIND_ELEM=1;
  private static int FIND_TEXT=2;
  private static int FIND_ATT=3;
  /**
   * Use an XPath string to select a single node.
   * Usage 'doc/para/text' goes to first <doc> element and expects to find <para><text> there
   * - Searches only elements, 
   * - '*' means the first child node, 
   * - searches only the first branch
   * - @attr can be the last token, meaning an attribute
   */
  public static Node selectSingleNode(Node contextNode, String str)
    throws SAXException
  {
//  	Log.debug("searching for "+str+" in "+contextNode);
  	Node currentNode = contextNode;
  	StringTokenizer tokenizer = new StringTokenizer(str,"/");
	String token;
	while (tokenizer.hasMoreTokens())
	{
		token = tokenizer.nextToken();
//		Log.debug("currentNode = "+currentNode);
		try 
		{
			currentNode = findNode(token,currentNode);
		} catch (SAXException e)
		{
			throw new SAXException("SimpleXPath: Node '"+str+"' could not be found in "+contextNode);
		}
	}
	return currentNode;	
  }
  private static Node findNode(String token,Node node) throws SAXException
  {
  	int find=FIND_ELEM;
  	if (node==null) return null;
	if (token.equals("text()")) find = FIND_TEXT;
	else if (token.startsWith("@")) 
	{ 
		find = FIND_ATT;
		NamedNodeMap att = node.getAttributes();
		String comp = token.substring(1);
		Node ret = att.getNamedItem(comp);
		return ret;
	}
	NodeList childlist = node.getChildNodes();
	for (int i=0;i<childlist.getLength();i++)
	{
		Node child = childlist.item(i);
		if (find==FIND_TEXT)
		{
			if (child.getNodeType()==Node.TEXT_NODE)
				return child;
		}	
		else if (child.getNodeType()==Node.ELEMENT_NODE)
		{
			if (token.equals("*")) return child;
			String name = child.getNodeName();
			if (name.equals(token)) return child;
		}
	}
    throw new SAXException("SimpleXPath: Node '"+token+"' could not be found in "+node);
  }

}
