/**
 * Copyright � Sergey Melnik (Stanford University, Database Group) 
 *
 * Distribution policies are governed by the W3C software license.
 * http://www.w3.org/Consortium/Legal/copyright-software   
 * 
 * All Rights Reserved.
 * 
 * @author      Sergey Melnik <melnik@db.stanford.edu>
 */
package fi.hut.tml.xsmiles.mlfc.general;
//package org.w3c.rdf.tools.sorter;

/**
 * A <em>comparer</em> performs a comparison of
 * two objects.
 *
 * @author Sergey Melnik <melnik@db.stanford.edu>
 */

public interface Comparer {

  /**
   * Handle is used e.g. to distinguish different types of sorting
   * or just passed as an additional parameter.
   *
   * <0: o1 < o2
   *  0: o1 = o2
   * >0: o1 > o2
   */
  public int compare(Object handle, Object o1, Object o2);
}
