/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.general;

import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.Log;

/**
 * A class to evaluate media queries defined by the "media" attribute in
 * xml-stylesheet processing instructions or xhtml css link elements.
 * This class has been programmed according to Media Queries Working
 * Draft, 23 Jan. 2002. See the individual methods how X-Smiles
 * handles the media features, for instance "monochrome".
 */
public class MediaQuery  {
  public static String guiName="desktop";
  
	/**
	 * Evaluate media attribute according to the Media Queries spec.
	 * If this method returns true, it is ok to use the stylesheet.
	 * <p>This method is static, so it can be called from where ever people want to
	 * call it. The only requirement is that XMLConfigurer is known.</p>
	 *
	 * @param media		Media attribute as a string
	 * @param guiconfig	XMLConfigurer describing the properties of the current GUI
	 * @return			true if Media Queries evaluate to true, otherwise false
	 */
	public static boolean evalMedia(String media, XMLConfigurer guiconfig)  {
		// If query attribute wasn't defined, then this is fine.
		if (media == null || media.length() == 0)
			return true;

		// Get the queries separated by comma and evaluate them one by one
		// joining the results with logical OR
		int comma = 0;
		String query = null;
		while (true)  {
			// Get until ',' or end of string
			comma = media.indexOf(',');
			if (comma != -1)
				query = media.substring(0, comma);
			else
				query = media;	
			
			// Evaluate and if true, then exit
			if (evalQuery(query, guiconfig) == true)
				return true;

			// Exit if the last query, none of them evaluated to true.
			if (comma == -1)
				return false;

			// Remove the evaluated part
			media = media.substring(comma+1);
		}
		// NOTREACHED
	}
	
	/**
	 * Evaluates one query, i.e. "not screen and (min-width: 640px) and (max-width: 700px)".
	 * @param media			String containing one query
	 * @param guiconfig		XMLConfigurer describing the properties of the current GUI
	 * @return				true if Media Query evaluates to true, otherwise false
	 */
	private static boolean evalQuery(String media, XMLConfigurer guiconfig)  {
		// This is true, unless "not" has been specified in the media string
		boolean condtrue = true;
		String origmedia = media;
		
		String word = getFirstWord(media);
		
		// If "only" found, just remove it
		if (word.equalsIgnoreCase("only"))  {
			media = removeFirstWord(media);
			word = getFirstWord(media);
		} else {
			// If "not" found, set negation flag
			if (word.equalsIgnoreCase("not"))  {
				media = removeFirstWord(media);
				word = getFirstWord(media);
				// Negate true results
				condtrue = false;
			}
		}

		// Return true, if media type wasn't given
		if (media.length() == 0) {
			return condtrue;
		}
		
		// Check the <media_type>
		// TODO: What about values: "handheld", "tv"??
		String device_mediatype = guiconfig.getGUIProperty(guiName,"mediaType");
		String mediatype = word;
		if (!(word.equalsIgnoreCase("all") || word.equalsIgnoreCase(device_mediatype)))  {			
			return !condtrue; // false
		}
		media = removeFirstWord(media);

		// Loop forever, looking for "and <expression>" bits
		// Exit the loop if an expression evaluates to false (exprs are logically ANDed).
		while (true)  {
			// Get optional "[ and <expression> ]"
			word = getFirstWord(media);
			media = removeFirstWord(media);

			// No "[ and <expression> ]"?
			if (word.length() == 0)
				return condtrue;	// true

			// Not keyword "and", but some garbage found?
			if (!word.equalsIgnoreCase("and"))  {
				Log.error("Media Query Error, \"and\" missing: "+origmedia);
				return !condtrue;	// false
			}

			// Get <expression>
			word = getExpression(media);
			media = removeExpression(media);
			
			// Expression not found or not understood?
			if (word.length() == 0)  {
				Log.error("Media Query Error, expression not understood: "+origmedia);
				return !condtrue;	// false
			}
			
			// Evaluate expression. Exit if evaluates to false
			if (!evalExpression(word, guiconfig, device_mediatype)) {
				return !condtrue;	// false
			}
		}
	}

	private static boolean evalExpression(String expr, XMLConfigurer guiconfig,
										String device_mediatype)  {
		
		// Parse expression into property and value
		String property = "", value = "";
		try  {
			property = expr.substring(0, expr.indexOf(':')).trim();
		} catch (IndexOutOfBoundsException e)  {
		}
		try  {
			value = expr.substring(expr.indexOf(':')+1).trim();
		} catch (IndexOutOfBoundsException e)  {
		}
		// ':' is not found
		if (expr.indexOf(':') == -1)  {
			property = expr;
			value = "";
		}

		// Evaluate screen, handheld and tv properties
		if (device_mediatype.equals("screen") || device_mediatype.equals("handheld") ||
			device_mediatype.equals("tv"))  {

			if (checkWidth(property, value, guiconfig) == true)
				return true;
			if (checkHeight(property, value, guiconfig) == true)
				return true;
			if (checkDeviceWidth(property, value, guiconfig) == true)
				return true;
			if (checkDeviceHeight(property, value, guiconfig) == true)
				return true;
			if (checkDeviceAspectRatio(property, value, guiconfig) == true)
				return true;
			if (checkColor(property, value, guiconfig) == true)
				return true;
			if (checkColorIndex(property, value, guiconfig) == true)
				return true;
			if (checkMonochrome(property, value, guiconfig) == true)
				return true;
			if (checkResolution(property, value, guiconfig) == true)
				return true;
		}

		// Evaluate tv properties - "scan" property is additional 
		// compared to screen and handheld devices
		if (device_mediatype.equals("tv"))  {
			if (checkScan(property, value, guiconfig) == true)
				return true;		
		}

		// All checks must have returned false, so exit with false
		return false;
	}

	/**
	 * Check the width property. This method will also check min-width and max-width.
	 * If the parameter property string is not "x-width", this will return false.
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not width, min-width or max-width.
	 *						false if property is one of widths, but doesn't match.
	 */
	private static boolean checkWidth(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("width") == false)
			return false;
		
		// expression (width) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int screenWidth = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just parse - it should be a plain integer value
			screenWidth = Integer.parseInt(guiconfig.getGUIProperty(guiName,"displayWidth"));
		} catch (NumberFormatException e)  {
			return false;
		}
		if (isMin(property) == true)  {
			if (screenWidth >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (screenWidth <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == screenWidth)
			return true;

		return false;	
	}

	/**
	 * Check the height property. This method will also check min-height and max-height.
	 * If the parameter property string is not "x-height", this will return false.
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not height, min-height or max-height.
	 *						false if property is one of heights, but doesn't match.
	 */
	private static boolean checkHeight(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("height") == false)
			return false;
		
		// expression (height) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int screenHeight = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just parse - it should be a plain integer value
			screenHeight = Integer.parseInt(guiconfig.getGUIProperty(guiName,"displayHeight"));
		} catch (NumberFormatException e)  {
			return false;
		}
		if (isMin(property) == true)  {
			if (screenHeight >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (screenHeight <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == screenHeight)
			return true;

		return false;	
	}
	
	/**
	 * Check the device-width property. This method will also check min-device-width and max-device-width.
	 * If the parameter property string is not "x-device-width", this will return false.
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not device-width, min-device-width or max-device-width.
	 *						false if property is one of device-widths, but doesn't match.
	 */
	private static boolean checkDeviceWidth(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("device-width") == false)
			return false;
		
		// expression (device-width) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int screenWidth = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just parse - it should be a plain integer value
			screenWidth = Integer.parseInt(guiconfig.getGUIProperty(guiName,"displayWidth"));
		} catch (NumberFormatException e)  {
			return false;
		}
		if (isMin(property) == true)  {
			if (screenWidth >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (screenWidth <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == screenWidth)
			return true;

		return false;	
	}

	/**
	 * Check the device-height property. This method will also check min-device-height and max-device-height.
	 * If the parameter property string is not "x-device-height", this will return false.
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not device-height, min-device-height or max-device-height.
	 *						false if property is one of device-heights, but doesn't match.
	 */
	private static boolean checkDeviceHeight(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("device-height") == false)
			return false;
		
		// expression (device-height) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int screenHeight = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just parse - it should be a plain integer value
			screenHeight = Integer.parseInt(guiconfig.getGUIProperty(guiName,"displayHeight"));
		} catch (NumberFormatException e)  {
			return false;
		}
		if (isMin(property) == true)  {
			if (screenHeight >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (screenHeight <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == screenHeight)
			return true;

		return false;	
	}
	
	/**
	 * Check the device-aspect-ratio property. This method not check 
	 * min-device-aspect-ratio and max-device-aspect-ratio because they are invalid properties.
	 * If the parameter property string is not "device-aspect-ratio", this will return false.
	 * <p>NOTE: X-Smiles calculates aspect ratio displayWidth / displayHeight (config).</p>
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not device-aspect-ratio.
	 *						false if property doesn't match displayWidth / displayHeight.
	 */
	private static boolean checkDeviceAspectRatio(String property, String value, XMLConfigurer guiconfig)  {
		if (property.equalsIgnoreCase("device-aspect-ratio") == false)
			return false;
		
		// expression (device-aspect-ratio) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse values to integers
		int screenWidth = 0, screenHeight = 0, w = 0, h = 0, slash = -1;
		try  {
			// Get values separated with '/'
			slash = value.indexOf('/');
			if (slash == -1)
				return false;
				
			// Parse integers
			w = Integer.parseInt(value.substring(0, slash));
			h = Integer.parseInt(value.substring(slash+1));
			// Just parse config values - these should be plain integer values
			screenWidth = Integer.parseInt(guiconfig.getGUIProperty(guiName,"displayWidth"));
			screenHeight = Integer.parseInt(guiconfig.getGUIProperty(guiName,"displayHeight"));
		} catch (NumberFormatException e)  {
			return false;
		} catch (IndexOutOfBoundsException e)  {
			return false;
		}

		// Equals ( w/h == screenWidth/screenHeight  -> multiplied with h and screenHeight
		if (w * screenHeight == h * screenWidth)
			return true;

		return false;	
	}

	/**
	 * Check the color property. This method will also check min-color and max-color.
	 * If the parameter property string is not "x-color", this will return false.
	 * <p>NOTE: BW devices should have color value of 0, but it is not implemented.</p>
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not color, min-color or max-color.
	 *						false if property is one of colors, but doesn't match.
	 */
	private static boolean checkColor(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("color") == false)
			return false;
		
		// expression (color) will always return true (not true for bw devices)
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int screenColor = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just parse - it should be a plain integer value
			screenColor = Integer.parseInt(guiconfig.getGUIProperty(guiName,"screenDepth"));
		} catch (NumberFormatException e)  {
			return false;
		}
		
		
		if (isMin(property) == true)  {
			if (screenColor >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (screenColor <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == screenColor)
			return true;

		return false;	
	}

	/**
	 * Check the color-index property. This method will also check min-color-index and max-color-index.
	 * If the parameter property string is not "x-color-index", this will return false.
	 * <p>NOTE: BW devices should have color value of 0, but it is not implemented.</p>
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not color-index, min-color-index or max-color-index.
	 *						false if property is one of color-indexs, but doesn't match.
	 */
	private static boolean checkColorIndex(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("color-index") == false)
			return false;
		
		// expression (color-index) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int screenColor = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just parse - it should be a plain integer value
			screenColor = Integer.parseInt(guiconfig.getGUIProperty(guiName,"screenDepth"));
			// Convert screen depth to number of colors
			screenColor = (int)Math.pow(2.0, (double)screenColor);
		} catch (NumberFormatException e)  {
			return false;
		}
		if (isMin(property) == true)  {
			if (screenColor >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (screenColor <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == screenColor)
			return true;

		return false;	
	}

	/**
	 * Check the monochrome property. This method will also check min-monochrome and max-monochrome.
	 * If the parameter property string is not "x-monochrome", this will return false.
	 * <p>NOTE: X-Smiles is assumed to be run always on a color capable device, thus this
	 * will always return false.</p>
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				false.
	 */
	private static boolean checkMonochrome(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("monochrome") == false)
			return false;
		
		// monochrome expressions will always return false
		return false;
	}

	/**
	 * Check the resolution property. This method will also check min-resolution and max-resolution.
	 * If the parameter property string is not "x-resolution", this will return false.
	 * <p>NOTE: X-Smiles has been hard-coded to always have 300dpi resolution.</p>
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true if this property evaluates to true.
	 *						false if property is not resolution, min-resolution or max-resolution.
	 *						false if property is one of resolutions, but doesn't match.
	 */
	private static boolean checkResolution(String property, String value, XMLConfigurer guiconfig)  {
		String feature = trimMinMax(property);
		if (feature.equalsIgnoreCase("resolution") == false)
			return false;
		
		// expression (resolution) will always return true
		if (value.length() == 0)
			return true;
		
		// Parse value to integer
		int resolution = 0, val = 0;
		try  {
			// Remove units and parse
			val = parseToInteger(value);
			// Just use hard-coded value 300
			resolution = 300;
		} catch (NumberFormatException e)  {
			return false;
		}
		if (isMin(property) == true)  {
			if (resolution >= val)
				return true;
			return false;	
		}
		if (isMax(property) == true)  {
			if (resolution <= val)
				return true;
			return false;	
		}
		// Equals
		if (val == resolution)
			return true;

		return false;	
	}

	/**
	 * Check the scan property. This method will not check min-scan and max-scan, because they
	 * are not allowed properties.
	 * If the parameter property string is not "scan", this will return false.
	 * <p>NOTE: X-Smiles has been hard-coded to always return true for "scan".</p>
	 * @param property		Property in the media attribute
	 * @param value			Value in the media attribute
	 * @return				true.
	 */
	private static boolean checkScan(String property, String value, XMLConfigurer guiconfig)  {

		if (property.equalsIgnoreCase("scan") == false)
			return false;

		// Valid values are "progressive" and "interlace"
		return true;
	}

	private static final int PX = 0;
	private static final int CM = 1;
	private static final int EM = 2;
	private static final int PT = 3;
	private static final int DPI = 4;

	/**
	 * Throws NumberFormatException if the value cannot be parsed.
	 */
	private static int parseToInteger(String str)  {
		int unit = PX; // Default PX units
		if (str.endsWith("px"))  {
			str = str.substring(0, str.length()-2);
			unit = PX;
		}
		if (str.endsWith("cm"))  {
			str = str.substring(0, str.length()-2);
			unit = CM;
		}
		if (str.endsWith("em"))  {
			str = str.substring(0, str.length()-2);
			unit = EM;
		}
		if (str.endsWith("pt"))  {
			str = str.substring(0, str.length()-2);
			unit = PT;
		}
		if (str.endsWith("dpi"))  {
			str = str.substring(0, str.length()-3);
			unit = DPI;
		}
			
		// Take the long route because of JDK1.1...
		float i = Float.valueOf(str).floatValue();
		// Convert units CM, EM and PT into PX
		/// TODO.. 
		// Currently CM is converted into PX using a hard-coded
		// conversion 10 CM = 283 PX (XSL FO seems to be using something like this)
		if (unit == CM)  {
			i = i * 28.3f;
		}
		
		return (int)i;
	}
	
	/**
	 * Check if the string is a minimum value (starts min- or device-min-...)
	 * @param str	String to be checked
	 * @return		true if str is min-property, otherwise false
	 */
	private static boolean isMin(String str)  {
		if (str == null)
			return false;
	
		if (str.length() > 4)
			if (str.substring(0, 4).equalsIgnoreCase("min-"))
				return true;

		if (str.equalsIgnoreCase("device-min-width") || str.equalsIgnoreCase("device-min-height"))
			return true;

		return false;
	}

	/**
	 * Check if the string is a maximum value (starts with max- or device-max-...)
	 * @param str	String to be checked
	 * @return		true if str is max-property, otherwise false
	 */
	private static boolean isMax(String str)  {
		if (str == null)
			return false;
	
		if (str.length() > 4)
			if (str.substring(0, 4).equalsIgnoreCase("max-"))
				return true;

		if (str.equalsIgnoreCase("device-max-width") || str.equalsIgnoreCase("device-max-height"))
			return true;

		return false;
	}

	/**
	 * Remove the pretext from the string. (removes min- and max- prefixed and -min- -max-
	 * for device-width and device-height)
	 * @param str	String to be trimmed
	 * @return		str without min- or max- pretexts
	 */
	private static String trimMinMax(String str)  {
		if (str == null)
			return null;
	
		if (str.length() > 4)
			if (str.substring(0, 4).equalsIgnoreCase("min-") ||
				str.substring(0, 4).equalsIgnoreCase("max-"))
					return str.substring(4);

		if (str.equalsIgnoreCase("device-min-width") || str.equalsIgnoreCase("device-max-width"))
			return "device-width";

		if (str.equalsIgnoreCase("device-min-height") || str.equalsIgnoreCase("device-max-height"))
			return "device-height";

		return str;	
	}

	/**
	 * Remove the expression and whitespaces after it from the string. 
	 * Expression begins and ends with a parenthesis.
	 * Note that parenthesis are also removed from the string 
	 * (trim() is also called for the string).
	 * If the string has only whitespaces, an empty ("") string will be returned.
	 * @param str		The expression in this string is removed.
	 * @return			The given string without the expression.
	 */
	private static String removeExpression(String str)  {
		int i = str.indexOf(")");
		if (i == -1)
			return "";
		String newString = str.substring(i+1).trim();
		return newString;
	}

	/**
	 * Get the first expression in the string. Expression begins and ends with a parenthesis.
	 * The returned word has been trimmed and doesn't contain parenthesis.
	 * @param str		This string is searched for an expression
	 * @return			The expression, or empty "" if not found or if the expression
	 * 					doesn't begin and end with a parenthesis
	 */
	private static String getExpression(String str)  {
		str = str.trim();
		if (str.charAt(0) != '(')
			return "";	
		int i = str.indexOf(')');
		if (i == -1)
			return "";
		String newString = str.substring(1, i).trim();
		return newString;
	}

	/**
	 * Remove the first word and whitespaces after it from the string. Words are separated 
	 * by whitespaces.
	 * Note that whitespaces are also removed from 
	 * the end of the string (trim() is called for the string).
	 * If the string only contains one word or whitespaces, an empty ("") string will be returned.
	 * @param str		The first word in this string is removed.
	 * @return			The given string without the first word.
	 */
	private static String removeFirstWord(String str)  {
		str = str.trim();
		int i = indexOfWhitespace(str);
		if (i == -1)
			return "";
		String newString = str.substring(i+1).trim();
		return newString;
	}

	/**
	 * Get the first word in the string. Words are separated by whitespaces. 
	 * The returned word has been trimmed.
	 * @param str		This string is searched for the first word
	 * @return			The first word
	 */
	private static String getFirstWord(String str)  {
		str = str.trim();
		int i = indexOfWhitespace(str);
		if (i == -1)
			return str;
		String newString = str.substring(0, i).trim();
		return newString;
	}
	
	/**
	 * Get the index of the first whitespace.
	 * @param str		String
	 * @return			if a whitespace occurs within the given string, 
	 *					then the index of the first whitespace is 
	 *					returned; if whitespaces don't occur, -1 is returned.
	 */
	 private static int indexOfWhitespace(String str)  {
	 	int i;
		for (i = 0 ; i < str.length() ; i++)  {
			if (Character.isWhitespace(str.charAt(i)) == true)
				return i;
		}
		return -1;
	 }
}
