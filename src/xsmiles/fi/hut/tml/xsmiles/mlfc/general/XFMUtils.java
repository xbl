package fi.hut.tml.xsmiles.mlfc.general;

import java.util.Hashtable;
import java.util.Enumeration;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.Log;

/**
 * Utility class for parsing e.g., #frames fragment
 * @author Juha
 */
public class XFMUtils 
{
    // The base part of the URI (excluding #frames() definitions)
    // but including CGI parameters.
    protected String baseURI;

    // The #frames() definitions
    private String tauhka="";
    
    private MLFC mlfc=null;

    // Hold the #frames definitions in the hashtable,
    // with ids as indices
    protected Hashtable urlPopulators=null;

    /**
     * Creates the urlpopulators on creation
     */
    public XFMUtils(MLFC m) {
	mlfc=m;
    }


    /**
     * Decodes the XFrames type URI into a hashtable of id-sourcename pairs.
     * Also saves the hashtable into internal urlPopulators variable.
     */
    public Hashtable decodeURL(String url) 
    {	
	char[]str=url.toCharArray();

	// The baseURI
	// The string containing frames specification
	String framesit="";

	// Other stuff, like cgi parameters
	// String tauhka="";

	int b=url.indexOf("#frames(");
	if(b==-1)
	    b=url.indexOf("#FRAMES(");
    
	if(b==-1) {
	    // NO frames populator
	    baseURI=url;
	    return null;
	}
	baseURI=url.substring(0,b);
	String loppu=url.substring(b+8, url.length());

	urlPopulators=parseURIs(loppu);
	return urlPopulators;
    }


    /**
     * A little obfuscated but works like a russian car. Works also for recursive framesets.
     * @param uriEnd The end of the URI starting from the #frames fragment.
     *            #frames(................................$, where dots represent the 
     *            string, and dollar indicates the end of the URI
     *
     * ALSO sets the tauhka variable to contain the possible portion of the URI after
     * the frameset. Tauhka contains possible cgi parameters.
     *
     * @return a hashtable which contains the ids and uris
     * @see decodeURL()
     */
    public Hashtable parseURIs(String uriEnd) 
    {
	// Log.debug("Parsing URI:");
	final int ID=2;
	final int URI=3;
	final int OTHER=4;
	String id="";
	int in=0; 
	String uri="";
	Hashtable tab=new Hashtable();
	int nextState=ID;  
	int state=ID;
    
	char[]str=uriEnd.toCharArray();

	for(int i=0;i<str.length;i++) {
	    state=nextState;
	    if(state==ID) {
		if(str[i] == '=') {
		    nextState=URI;
		} else {
		    id=id + str[i];
		}
	    } else if(state==URI) {
		if(str[i] == '(') {             // going deeper
		    uri=uri+str[i];	  
		    in++;
		} else if(str[i] == ')') {      // coming back
		    in--;
		    if(in==-1) {
			// Log.debug("putting last pair: " + id + " " + uri);
			tauhka="";
			if(i+1 < str.length)
			    tauhka=uriEnd.substring(i+1,str.length);  // The last URI received
			// Log.debug("tauhka: " + tauhka);
			tab.put(id,uri);
			return tab;
		    } else 
			uri=uri+str[i];
		} else if(in==0 && str[i] == ',') {             // uri received, get next id 
		    // Log.debug("putting pair: "+id + " " + uri);
		    tab.put(id,uri);
		    uri="";
		    id="";	
		    nextState=ID;
		} else {
		    uri=uri+str[i];
		}
	    }
	}
	return tab;
    }
  
    /** 
     * Create a frames URI from the uriPopulators hashtable. 
     * This is needed when updating the current uri of the browser.
     * Compile the framesuri based on the current population, and old (baseURI+cgi parameters)
     *
     * http://ww.com/in.xfm?a=adssda&b=das#frames=(addssda=das
     */
    public String createFramesURI() 
    {
	if(baseURI==null || baseURI.equals("")) {
	    decodeURL(mlfc.getXMLDocument().getLink().getURL().toString().trim());
	}
	String uri= baseURI + "#frames(";
	boolean first=true;
	if(urlPopulators==null)
	    return baseURI;
	Enumeration e=urlPopulators.keys();
	if(e!=null)
	    for(; e.hasMoreElements();) {
		Object id=e.nextElement();
		if(first==true) {
		    uri=uri+id+"="+(String)urlPopulators.get(id);
		    first=false;
		} else
		    uri=uri + "," + id + "=" + (String)urlPopulators.get(id);
	    }
	uri=uri + ")" + tauhka;
	return uri;
    }
  
    /**
     * @return a hashtable containing urlpopulators defined within the URI
     */
    public Hashtable getURIPopulators() 
    {
	if(urlPopulators==null) {
	    urlPopulators=decodeURL(mlfc.getXMLDocument().getLink().getURL().toString().trim());
	}
	if(urlPopulators==null)
	    urlPopulators=new Hashtable();
	return urlPopulators;
    }
}
