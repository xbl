package fi.hut.tml.xsmiles.mlfc.drml.dom;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.MLFC;

public class DataReferenceElementImpl extends XSmilesElementImpl
{
    MLFC mlfc;

    public DataReferenceElementImpl(DocumentImpl ownerDocument, String value)
    {
        super(ownerDocument, value);
        // TODO Auto-generated constructor stub
    }

    public DataReferenceElementImpl(MLFC mlfc, DocumentImpl ownerDocument,
            String namespaceURI, String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
        this.mlfc = mlfc;
    }

}
