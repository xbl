package fi.hut.tml.xsmiles.mlfc.drml;

import java.io.StringWriter;
import java.lang.reflect.Array;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.drml.dom.DataReferenceElementImpl;
import fi.hut.tml.xsmiles.mlfc.drml.dom.ItemElementImpl;
import fi.hut.tml.xsmiles.protocol.wesahmi.BrowserSubscriber;

/**
 * Handles Data Reference Markup Language (DRML)
 * 
 * @author mpohja
 * 
 */
public class DRMLFC extends MLFC implements Runnable
{

    public DRMLFC()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Create a DOM element.
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag)
    {
        Element element = null;
        Log.debug("DRMLFC: " + ns + ":" + tag);

        if (tag == null)
        {
            Log.error("Invalid null tag name!");
            return null;
        }
        String localname = getLocalname(tag);
        if (localname.equals("dref"))
        {
            element = new DataReferenceElementImpl(this, doc, ns, tag);
        }
        else if (localname.equals("item"))
        {
            element = new ItemElementImpl(doc, ns, tag);
        }

        // Other tags go back to the XSmilesDocument as null...
        if (element == null)
            Log.debug("DRMLFC didn't understand element: " + tag);

        return element;
    }

    public void start()
    {
        Log.debug("DRMLFC.start()"); // at " + System.currentTimeMillis());
        new Thread(this).start();
    }

    public void stop()
    {
        // TODO Auto-generated method stub

    }

    public void run()
    {
        BrowserSubscriber subscriber = getMLFCListener().getSubscriber();
        String data = "  <wes:clientID>" + subscriber.getClientID()
            + "</wes:clientID>\n";
        
        String[] flightNumbers = subscriber.getFlightNumbers(),
            sdts = subscriber.getFlightTimes();
        int count = Array.getLength(flightNumbers);
        for (int i = 0; i < count; i++)
        {
            if (flightNumbers[i] != null && flightNumbers[i] != "")
            {
                data += "<wes:flight number='" + i+1 + "' flightNumber='"
                + flightNumbers[i] + "' sdt='" + sdts[i] + "'>";
            
                NodeList nodes = this.getXMLDocument().getDocument()
                .getElementsByTagNameNS("http://www.x-smiles.org/ns/drml",
                        "item");
                Element item;
                for (int j = 0; j < nodes.getLength(); j++)
                {
                    item = (Element) nodes.item(j);
                    data = data + "    <" + item.getFirstChild().getNodeValue() + "/>\n";
                }
                
                /*data += "  <wes:clientID>" + subscriber.getClientID()
                + "</wes:clientID>\n";*/
                data += "</wes:flight>\n";
            }
        }        

        NodeList nodes = this.getXMLDocument().getDocument()
                .getElementsByTagNameNS("http://www.x-smiles.org/ns/drml",
                        "item");
        Element item;
        //data = data + "  <dref xmlns=\"http://www.x-smiles.org/ns/drml\">\n";
        for (int i = 0; i < nodes.getLength(); i++)
        {
            item = (Element) nodes.item(i);
            data = data + "    <" + item.getFirstChild().getNodeValue() + "/>\n";
        }
        Node newView = this.getXMLDocument().getDocument()
            .getElementsByTagName("newView").item(0);
        data = data + "<newView>" + newView.getFirstChild().getNodeValue() + "</newView>\n";
        //data = data + "  </dref>\n";
        Log.debug("Refs:\n" + data);
        Log.debug(soapWrap(data));
        try
        {
            Thread.sleep(1200);
        }
        catch (Exception e)
        {
            Log.error(e);
        }
        subscriber.sendSubscriptionRefresh(soapWrap(data));
    }

    String soapWrap(String content)
    {
        String soapMessage = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wes=\"http://www.tml.hut.fi/Research/wesahmi\">\n"
                + "  <env:Body>\n"
                + content
                + "  </env:Body>\n"
                + "</env:Envelope>";

        return soapMessage;
    }
}
