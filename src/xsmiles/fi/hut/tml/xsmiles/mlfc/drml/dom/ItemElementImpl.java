package fi.hut.tml.xsmiles.mlfc.drml.dom;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

public class ItemElementImpl extends XSmilesElementImpl
{

    public ItemElementImpl(DocumentImpl ownerDocument, String value)
    {
        super(ownerDocument, value);
        // TODO Auto-generated constructor stub
    }

    public ItemElementImpl(DocumentImpl ownerDocument, String namespaceURI,
            String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
        // TODO Auto-generated constructor stub
    }

    public void init()
    {
        super.init();
        Log.debug("Data reference: " + this.getFirstChild().getNodeValue());
    }
}
