/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xslfo;


import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.VisualElementImpl;
import fi.hut.tml.xsmiles.dom.RefreshableService;
import fi.hut.tml.xsmiles.dom.StylesheetService;

//import fi.hut.tml.xsmiles.mlfc.css.XMLStyleSheet2;


import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;


import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;


import org.w3c.dom.xforms10.XFormsElement;


//import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.DocumentImpl;

/**
 * The superclass of all XForms elements
 * @author Mikko Honkala
 */

public class RootElementImpl extends  VisualElementImpl 
	implements RefreshableService
	//, StylesheetService
{
	protected XSLFOMLFC mlfc;	
	
	public RootElementImpl(XSLFOMLFC mlfcArg, DocumentImpl doc,String ns, String name) {
	  super(doc, ns, name);
	  this.mlfc=mlfcArg;
	}
	
	public void refresh()
	{
		Log.debug("fo:root.refresh() called");
		mlfc.refresh();
	}
        
        public void setBlocking(boolean state) {
        }
        
//	public XMLStyleSheet2 getStyleSheet()
//	{
//		return mlfc.getStyleSheet();
//	}
}
