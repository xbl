/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


package fi.hut.tml.xsmiles.mlfc.xslfo.render.jdk12;
import org.apache.fop.render.awt.*;

import fi.hut.tml.xsmiles.mlfc.xslfo.render.CommonExtensions;
 
//XSmiles
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xslfo.form.FormWidgetArea;
import fi.hut.tml.xsmiles.mlfc.xslfo.image.XSmilesImage;
import fi.hut.tml.xsmiles.mlfc.xslfo.image.GifJpegImage2;

import org.apache.fop.layout.*;
import org.apache.fop.image.*;
import org.apache.fop.viewer.*;

import org.xml.sax.SAXException;

//import org.w3c.dom.svg.*;

import java.util.*;
import java.net.URL;
import java.io.*;

import javax.swing.*;
import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;

// JDK 1.2
import java.awt.image.BufferedImage;

import org.apache.fop.render.Renderer;
import org.apache.fop.render.awt.AWTRenderer;
/**
 * Links XSMiles extensions  in CommonExtensions to the org.apache.fop.awt.AWTRenderer.
 * - Form components
 * - Links
 * - Embedded SVG & SMIL
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.3 $
 */

public class XSmilesAWTRenderer extends AWTRenderer2 
  implements Renderer,
	     fi.hut.tml.xsmiles.mlfc.xslfo.render.AWTRendererInterface {
  // TR - Removed for JDK 1.1.x
  // , Printable, Pageable
  
  protected Translator res;	
  protected Container container;
  //  protected int count;
  protected CommonExtensions common;  
  protected final static int PADX=0,PADY=0;
  
  protected MLFCListener mlfcListener;
  
  /**
   * Constructor.
   * @param cont The container to add form and link widgets
   *
   */
  public XSmilesAWTRenderer(Translator aRes,MLFCListener ml, XMLDocument doc) 
  {
    super(aRes);
    Log.debug("JDK1.2 XSmilesAWTRenderer starting");
    mlfcListener=ml;
    
    res = aRes;
    common = new CommonExtensions(this,mlfcListener,doc);
  }
  
  public void setParentComponent(Container cont)
  {
    setComponent(cont);
    container = cont;
    common.setParentComponent(cont);
  }
  
  
  //  public static void setPanel(Panel p) 
  //  {
  //  	panel = p;
  //  }
  
  public void render(int aPageNumber)
  {
    Page page = (Page)pageList.elementAt(aPageNumber);
    int pageH = (int)((float)page.getHeight() / 1000f);
    // wollen wir links abbilden? -- Ja!
    common.initPage(aPageNumber,scaleFactor,pageH,
    		(page.hasLinks() ? page.getLinkSets() : null));
    super.render(aPageNumber);
    Log.debug(" XSmilesAWT renderer done rendering");
    }

    /**
     * Navigates to the page pointed by the link that is active
     *
     */
    public void followActiveLink() {
	common.followActiveLink();
    }
    
    /**
     * Moves the "active" link down
     */
    public void moveActiveLinkDown() {
	common.moveActiveLinkDown();
    }

    /**
     * Moves the "active" link up
     */
    public void moveActiveLinkUp() {
	common.moveActiveLinkUp();
    }
    
    public void renderFormArea(FormWidgetArea area) {
	int h = common.renderFormArea(area,this.getRectangle(area));
	this.currentYPosition -= h;
    }
    
    AreaTree tree;
    public void setAreaTree(AreaTree atree) 
    {
	tree=atree;
    }
    /**
     * XSmiles extension: External SVG images using SVGMLFC
     *
     */
    public void renderImageArea(ImageArea area) {
	
	FopImage img = area.getImage();
	Log.debug("AWTRender.renderImageArea() "+img.getURL());
	if (null==img) 
	    {
		super.renderImageArea(area);
	    }
	if (img instanceof XSmilesImage) 
	    {
		int h = common.renderExternalImageArea(area,this.getRectangle(area));
		currentYPosition -= h;
	    }
	else if (img instanceof GifJpegImage2)
	{
	
		GifJpegImage2 gifjpegimg = (GifJpegImage2)img;
		if (gifjpegimg.image==null)
		{
			Log.error("gifjpegimg.image was null in XSmilesAWTRenderer! Reverting");
			super.renderImageArea(area);
		}
		else
		{
			int x = currentAreaContainerXPosition + area.getXOffset();

			int y = currentYPosition;
			int w = area.getContentWidth();
			int h = area.getHeight();

		    int startx = (x + 500) / 1000;
		    int starty = pageHeight - ((y + 500) / 1000);
		    int endx = (x + w + 500) / 1000;
		    int endy = pageHeight - ((y + h + 500) / 1000);

		    // reverse start and end y because h is positive
		    graphics.drawImage(gifjpegimg.image, startx, starty, endx - startx,
		                       starty - endy, null);
	        currentYPosition -= h;
		}
						   
	}
	else super.renderImageArea(area);
    }
	
    public Component getRenderedComponent()
    {
	JLabel    previewImageLabel = new JLabel();
	
	BufferedImage pageImage = null;

	previewImageLabel.setVisible(false);
	pageImage = this.getLastRenderedPage();
	if (pageImage == null)
	    return null;
	/*
	  Graphics graphics = pageImage.getGraphics();
	  graphics.setColor(Color.black);
	  graphics.drawRect(0, 0, pageImage.getWidth() - 1,
	  pageImage.getHeight() -1 );
	*/
	previewImageLabel.setIcon(new ImageIcon(pageImage));

	previewImageLabel.setSize(pageImage.getWidth(), pageImage.getHeight());


	previewImageLabel.setVisible(true);
	return previewImageLabel;
	//this.validate();

	
    }
	
    protected Rectangle getRectangle(org.apache.fop.layout.Area a) {
	return new Rectangle(currentAreaContainerXPosition,
			     currentYPosition,
			     a.getAllocationWidth(),
			     a.getHeight());
    }
	
}


