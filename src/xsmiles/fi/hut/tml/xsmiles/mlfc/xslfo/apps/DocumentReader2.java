/*
 * $Id: DocumentReader2.java,v 1.3 2002/05/20 13:27:21 honkkis Exp $
 * Copyright (C) 2001 The Apache Software Foundation. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xslfo.apps;

/*
Modified by Mikko Honkala for X-Smiles to 
store and query the latest DOM node,
and also to support unqualified DOM nodes
*/
//package org.apache.fop.tools;
import org.apache.fop.tools.*;

import java.io.IOException;
// import java.util.*;

// DOM
import org.w3c.dom.*;

// SAX
import org.xml.sax.*;
import org.xml.sax.helpers.AttributesImpl;

/**
 * This presents a DOM as an XMLReader to make it easy to use a Document
 * with a SAX-based implementation.
 *
 * @author Kelly A Campbell
 *
 */

public class DocumentReader2 extends DocumentReader implements XMLReader {


    // //////////////////////////////////////////////////////////////////
    // Parsing.
    // //////////////////////////////////////////////////////////////////

    /**
     * Parse an XML DOM document.
     *
     *
     *
     * @param source The input source for the top-level of the
     * XML document.
     * @exception org.xml.sax.SAXException Any SAX exception, possibly
     * wrapping another exception.
     * @exception java.io.IOException An IO exception from the parser,
     * possibly from a byte stream or character stream
     * supplied by the application.
     * @see org.xml.sax.InputSource
     * @see #parse(java.lang.String)
     * @see #setEntityResolver
     * @see #setDTDHandler
     * @see #setContentHandler
     * @see #setErrorHandler
     */
    private ContentHandler _contentHandler = null;
	
   public void setContentHandler(ContentHandler handler) {
       _contentHandler = handler;
   }

   public ContentHandler getContentHandler() {
       return _contentHandler;
   }
   /**
    *X-Smiles addition
    *This static kludge is needed because the form elements need access to the 
    *DOM node
    *
    */
    protected Node currentNode;
    protected static Node cNode;
	public static Node getCurrentNode()
	{
		return cNode;
	}
    public void parse(InputSource input) throws IOException, SAXException {
        if (input instanceof DocumentInputSource) {
            Document document = ((DocumentInputSource)input).getDocument();
            if (_contentHandler == null) {
                throw new SAXException("ContentHandler is null. Please use setContentHandler()");
            }

            // refactored from org.apache.fop.apps.Driver
            /* most of this code is modified from John Cowan's */

            AttributesImpl currentAtts;

            /* temporary array for making Strings into character arrays */
            char[] array = null;

            currentAtts = new AttributesImpl();

            /* start at the document element */
            currentNode = document;
            cNode=currentNode;
            while (currentNode != null) {
                switch (currentNode.getNodeType()) {
                case Node.DOCUMENT_NODE:
                    _contentHandler.startDocument();
                    break;
                case Node.CDATA_SECTION_NODE:
                case Node.TEXT_NODE:
                    String data = currentNode.getNodeValue();
                    int datalen = data.length();
                    if (array == null || array.length < datalen) {
                        /*
                         * if the array isn't big enough, make a new
                         * one
                         */
                        array = new char[datalen];
                    }
                    data.getChars(0, datalen, array, 0);
                    _contentHandler.characters(array, 0, datalen);
                    break;
                case Node.PROCESSING_INSTRUCTION_NODE:
                    _contentHandler.processingInstruction(currentNode.getNodeName(),
                                                          currentNode.getNodeValue());
                    break;
                case Node.ELEMENT_NODE:
//					System.out.println("DB: node: uri:"+
//												currentNode.getNamespaceURI()+
//                                                 "localname:"+currentNode.getLocalName()+
//                                                 "name:"+currentNode.getNodeName());
                    NamedNodeMap map = currentNode.getAttributes();
                    currentAtts.clear();
                    for (int i = map.getLength() - 1; i >= 0; i--) {
                        Attr att = (Attr)map.item(i);
                        currentAtts.addAttribute(att.getNamespaceURI(),
                                                 att.getLocalName(),
                                                 att.getName(), "CDATA",
                                                 att.getValue());
                    }
					//X-SMILES : to test for unqualified elements
					String uri=currentNode.getNamespaceURI();
					if (uri==null) uri="";
					String nname = currentNode.getNodeName();
                    String lname=currentNode.getLocalName();
                    if (lname==null) lname=nname;
                    _contentHandler.startElement(uri,
                                                 lname,
                                                 nname,
                                                 currentAtts);
                    break;
                }

                Node nextNode = currentNode.getFirstChild();
                if (nextNode != null) {
                    currentNode = nextNode;
                    cNode=currentNode;
                    continue;
                }

                while (currentNode != null) {
                    switch (currentNode.getNodeType()) {
                    case Node.DOCUMENT_NODE:
                        _contentHandler.endDocument();
                        break;
                    case Node.ELEMENT_NODE:
                        _contentHandler.endElement(currentNode.getNamespaceURI(),
                                                   currentNode.getLocalName(),
                                                   currentNode.getNodeName());
                        break;
                    }

                    nextNode = currentNode.getNextSibling();
                    if (nextNode != null) {
                        currentNode = nextNode;
                        cNode=currentNode;
                        break;
                    }

                    currentNode = currentNode.getParentNode();
                    cNode=currentNode;
                }
            }

        } else {
            throw new SAXException("DocumentReader22 only supports parsing of a DocumentInputSource");
        }

    }
}


