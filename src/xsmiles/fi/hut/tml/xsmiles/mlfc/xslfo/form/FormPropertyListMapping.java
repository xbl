/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/*-- $Id: FormPropertyListMapping.java,v 1.16 2001/11/09 13:08:25 honkkis Exp $ -- 
 */

package fi.hut.tml.xsmiles.mlfc.xslfo.form;
import org.apache.fop.fo.*;

import org.apache.fop.fo.properties.*;

import java.util.Hashtable;
import java.util.Enumeration;
/**
 * Maps the form element properties
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.16 $
 */

public class FormPropertyListMapping {

  private static Hashtable s_htGeneric = new Hashtable(200);
  private static Hashtable s_htElementLists = new Hashtable(10);
  
  static {

    String uri = FormElementMapping.xforms_ns;
	
	s_htGeneric.put("ref",Id.maker("ref"));
	s_htGeneric.put("id",Id.maker("id"));
	s_htGeneric.put("type",Id.maker("type"));
	s_htGeneric.put("xform",Id.maker("xform"));
	s_htGeneric.put("rows",Id.maker("rows"));
	s_htGeneric.put("cols",Id.maker("cols"));
	s_htGeneric.put("onevent",Id.maker("onevent"));
	s_htGeneric.put("to",Id.maker("to"));
	s_htGeneric.put("style",Id.maker("style"));
	s_htGeneric.put("value",Id.maker("value"));
	s_htGeneric.put("name",Id.maker("name"));
	s_htGeneric.put("calculate",Id.maker("calculate"));
	s_htGeneric.put("stepsize",Id.maker("stepsize"));
	s_htGeneric.put("start",Id.maker("start"));
	s_htGeneric.put("end",Id.maker("end"));
	//s_htGeneric.put("master-name",MasterName.maker());
	
    }
    public static Hashtable getGenericMappings() {
      return s_htGeneric;
    }
  
    public static Enumeration getElementMappings() {
      return s_htElementLists.keys();
    }
  
    public static Hashtable getElementMapping(String elemName) {
      return (Hashtable)s_htElementLists.get(elemName);
    }

}


