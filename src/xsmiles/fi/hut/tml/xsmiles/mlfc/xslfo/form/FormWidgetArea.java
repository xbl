/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */


/*-- $Id: FormWidgetArea.java,v 1.6 2003/01/22 14:10:13 honkkis Exp $ -- 

 */
package fi.hut.tml.xsmiles.mlfc.xslfo.form;
// XSmiles
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xslfo.form.FormElement;
import fi.hut.tml.xsmiles.mlfc.xslfo.render.AWTRendererInterface;
import fi.hut.tml.xsmiles.dom.VisualComponentService;



// FOP
import org.apache.fop.layout.*;
import org.apache.fop.render.Renderer;

// Java
import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.awt.TextField;
import java.awt.Panel;
import java.awt.Component;
import java.awt.event.ActionListener;


/**
 * The superclass for all form widgets (inputs, buttons, textareas...) in the Area Tree.
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.6 $
 */
public class FormWidgetArea extends Area {
	protected FormElement myElement;
	protected Component myComponent;

    public FormWidgetArea (FormElement elem,FontState fontState, int allocationWidth, int maxHeight) {
		super(fontState,allocationWidth,maxHeight);
		myElement=elem;
		if (myElement.getControl()!=null)
		{
			currentHeight = (int)myElement.getControl().getSize().height*1000;		
			// Width doesn't apply since Input.layout doesn't do anything with it
			contentRectangleWidth = (int)myElement.getControl().getSize().width*1000;		
		} else
		{
			currentHeight=0;
			contentRectangleWidth=0;
		}
    }

    public void render(Renderer renderer) {
	//Log.debug("FormWidgetArea.render()  "+this);
	if (myElement != null&&myElement.getControl()!=null)
		{
			// Only our own renderer can render this component
			if (renderer instanceof AWTRendererInterface) 
			{
				((AWTRendererInterface)renderer).renderFormArea(this);
			}
		}
    }
	public static FormWidgetArea createWidgetArea(FormElement elem)
	{
		FormWidgetArea widgetarea=null;
		widgetarea=new FormWidgetArea(elem,null,1,1);
//
//		widgetarea=elem.createArea(elem,null,1,1);
		widgetarea.setFormElement(elem);
		return widgetarea;
	}
	public FormElement getFormElement() 
	{
		return myElement;
	}
	public void setFormElement(FormElement elem) 
	{
		myElement=elem;
	}
	public Component getComponent()
	{
		return myElement.getComponent();
	}
	public VisualComponentService getExtension()
	{
		return myElement.getExtension();
	}
	public void setComponent(Component comp)
	{
		myComponent = comp;
	}

	
}
