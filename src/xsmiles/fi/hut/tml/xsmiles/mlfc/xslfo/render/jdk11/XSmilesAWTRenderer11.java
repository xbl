/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



package fi.hut.tml.xsmiles.mlfc.xslfo.render.jdk11;
import org.apache.fop.render.awt.*;

 
//XSmiles
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.xslfo.form.FormWidgetArea;
import fi.hut.tml.xsmiles.mlfc.xslfo.image.XSmilesImage;

import org.apache.fop.layout.*;
import org.apache.fop.image.*;
import org.apache.fop.viewer.*;

import org.xml.sax.SAXException;

//import org.w3c.dom.svg.*;

import java.util.*;
import java.net.URL;
import java.io.*;

import javax.swing.*;
//import com.sun.java.swing.*;

import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;


import fi.hut.tml.xsmiles.mlfc.xslfo.render.AWTRendererInterface;
import fi.hut.tml.xsmiles.mlfc.xslfo.render.CommonExtensions;

import org.apache.fop.render.Renderer;
//import org.apache.fop.render.awt.AWTRenderer;
/**
 * XSMiles extensions to the org.apache.fop.awt.AWTRenderer.
 * - Form components
 * - Links
 * - Embedded SVG & SMIL
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.19 $
 */

public class XSmilesAWTRenderer11 
	extends fi.hut.tml.xsmiles.mlfc.xslfo.render.jdk11.AWTRenderer2 
	implements Renderer,AWTRendererInterface {
	// TR - Removed for JDK 1.1.x
	// , Printable, Pageable
 
  protected Translator res;	
  protected static Container container;
//  protected int count;
  protected CommonExtensions common;  
  protected final static int PADX=0,PADY=0;
  
  /**
   * Constructor.
   * @param cont The container to add form and link widgets
   *
   */
//  public XSmilesAWTRenderer11(Translator aRes,Container cont) 
//  {
//    super(aRes, cont);
//    Log.debug("JDK1.1 XSmilesAWTRenderer starting");
//	
//	container = cont;
//	res = aRes;
//	setParent(cont);
//	common = new CommonExtensions(cont,this);
//  }
//  
  public XSmilesAWTRenderer11(Translator aRes, MLFCListener ml, XMLDocument doc) 
  {
  super(aRes);
  Log.debug("JDK1.1 XSmilesAWTRenderer11 starting");
  
  res = aRes;
  common = new CommonExtensions(this,ml,doc);
  }
  
  public void setParentComponent(Container cont)
  {
  setParent(cont);
  container = cont;
  common.setParentComponent(cont);
  }
  
  
//  public static void setPanel(Panel p) 
//  {
//  	panel = p;
//  }

    public void render(int aPageNumber)
     {
    Page page = (Page)pageList.elementAt(aPageNumber);
    int pageH = (int)((float)page.getHeight() / 1000f);
    // wollen wir links abbilden? -- Ja!
    common.initPage(aPageNumber,scaleFactor,pageH,
    		(page.hasLinks() ? page.getLinkSets() : null));
    super.render(aPageNumber);
    Log.debug(" XSmilesAWT renderer done rendering");
    }



    /**
     * Moves the "active" link down
     */
    public void moveActiveLinkDown() {
	common.moveActiveLinkDown();
    }

    /**
     * Moves the "active" link up
     */
    public void moveActiveLinkUp() {
	common.moveActiveLinkUp();
    }

    
    /**
     * Navigates to the page pointed by the link that is active
     *
     */
    public void followActiveLink() {
	common.followActiveLink();
    }

    public void renderFormArea(FormWidgetArea area) {
		int h = common.renderFormArea(area,this.getRectangle(area));
	    this.currentYPosition -= h;
    }
	

	public void setAreaTree(AreaTree atree) 
	{
		tree=atree;
	}
	/**
	 * XSmiles extension: External SVG images using SVGMLFC
	 *
	 */
	public void renderImageArea(ImageArea area) {
		
		FopImage img = area.getImage();
		Log.debug("AWTRender.renderImageArea() "+img.getURL());
		if (null==img) 
		{
			super.renderImageArea(area);
		}
		if (img instanceof XSmilesImage) 
		{
			int h = common.renderExternalImageArea(area,this.getRectangle(area));
			currentYPosition -= h;
		}
		else super.renderImageArea(area);
	}
	
	public Component getRenderedComponent()
	{

		return super.getRenderedComponent();

	}
	
  	protected Rectangle getRectangle(org.apache.fop.layout.Area a) 
  	{
		return this.getBounds(a);
  	}
	
	
}





