/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xslfo.render;
import org.apache.fop.render.awt.*;


//XSmiles
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;

import fi.hut.tml.xsmiles.dom.VisualComponentService;

import fi.hut.tml.xsmiles.gui.components.ActionEventEx;
import fi.hut.tml.xsmiles.gui.components.XLinkComponent;
import fi.hut.tml.xsmiles.gui.components.XComponent;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.gui.components.XHoverListener;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.xslfo.XSLFOMLFC;
import fi.hut.tml.xsmiles.mlfc.xslfo.form.FormWidgetArea;
import fi.hut.tml.xsmiles.mlfc.xslfo.image.XSmilesImage;

import fi.hut.tml.xsmiles.util.EventUtil;

import org.apache.fop.layout.*;
import org.apache.fop.messaging.MessageHandler;
import org.apache.fop.datatypes.*;
import org.apache.fop.image.*;
import org.apache.fop.svg.*;
//import org.apache.fop.dom.svg.*;
//import org.apache.fop.dom.svg.SVGArea;
import org.apache.fop.render.pdf.*;
import org.apache.fop.viewer.*;
import org.apache.fop.apps.*;

import org.xml.sax.SAXException;

//import org.w3c.dom.svg.*;

import java.awt.*;
import java.awt.Image;
import java.awt.image.*;
import java.util.*;
import java.net.URL;
import java.io.*;
import java.beans.*;
import javax.swing.*;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.*;

import org.apache.fop.render.Renderer;
import org.apache.fop.render.awt.AWTRenderer;

/**
 * XSMiles extensions to the org.apache.fop.awt.AWTRenderer.
 * - Form components
 * - Links
 * - Embedded SVG & SMIL
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.45 $
 */
public class CommonExtensions
{
    // TR - Removed for JDK 1.1.x
    // , Printable, Pageable
    
    protected Translator res;
    protected Container container; // where to put Extension Controls
    //  protected int count;
    protected int pageHeight;
    protected Hashtable widgets;
    protected Hashtable mlfcList;
    protected Vector linkComponents;
    protected int    activeLink=0;      //the position of activeLink
    //    protected fi.hut.tml.xsmiles.mlfc.xslfo.render.FormHandler formhandler;
    protected Renderer renderer;
    protected final static int PADX=0,PADY=0;
    protected double scaleFactor;
    
    
    protected MLFCListener mlfcListener;
    protected XMLDocument xmldoc;
    
    /**
     * Constructor.
     * @param cont The container to add form and link widgets
     *
     */
    public CommonExtensions(Renderer a_renderer, MLFCListener ml, XMLDocument doc)
    {
        Log.debug("CommonExtensions starting");
        mlfcListener=ml;
        xmldoc=doc;
        renderer = a_renderer;
        //		container = cont;
        widgets = new Hashtable();
        mlfcList = new Hashtable();
        linkComponents = new Vector();
        
        // setParent(Container);
    }
    public void setParentComponent(Container cont)
    {
        this.container=cont;
    }
    public void initPage(int aPageNumber, double aScale, int aPageHeight,Vector linkSets)
    {
        
        Log.debug("------- Render page: "+aPageNumber+" ----------- scale:"+aScale );
        //scaleFactor = aScale;
        scaleFactor=aScale;
        removeComponents(widgets,this.container);
        removeLinks();
        pageHeight = aPageHeight;
        if (pageHeight == 0) Log.error("***** pageheight == 0");
        widgets=new Hashtable();
        if (linkSets!=null)
        {
            this.renderLinks(linkSets);
        }
        
    }
    
    public void renderLinks(Vector linkSets)
    {
        Enumeration e = linkSets.elements();
        while(e.hasMoreElements())
        {
            LinkSet ls = (LinkSet) e.nextElement();
            renderLinkSet(ls);
        }
    }
    
    public void renderLinkSet(LinkSet linkSet)
    {
        String   dest = linkSet.getDest();
        //Iterator i    = linkSet.getRects().iterator();
        Enumeration e = linkSet.getRects().elements();
        
        while(e.hasMoreElements())
        {
            LinkedRectangle lr = (LinkedRectangle)e.nextElement();
            Rectangle r = lr.getRectangle();
            if (r != null)
            {
                XComponent c = createLinkComponent(r, dest);
                //Log.debug("Adding link: "+r+" dest:"+dest);
                c.setVisible(true);
                container.add(c.getComponent(), 0);
                
                //debug
                //			  Button b = new java.awt.Button("test");
                //			  b.setBounds(c.getBounds());
                //			  container.add(b,0);
                
                
                container.invalidate();
                linkComponents.addElement(c);
            }
            
        }
        if(linkComponents.size()>0)
            ((XLinkComponent)linkComponents.elementAt(0)).setActive(true);
    }
    
    /**
     * Move the active link up (--linksComponents)
     * I'll arrange a method in XSLFOMLFC for this
     */
    public void moveActiveLinkUp()
    {
        if(linkComponents!=null&&linkComponents.size()!=0)
        {
            ((XLinkComponent)linkComponents.elementAt(activeLink)).setActive(false);
            if(--activeLink<0)
                activeLink=(linkComponents.size()-1);
            ((XLinkComponent)linkComponents.elementAt(activeLink)).setActive(true);
        }
    }
    
    /**
     * Move the active link up (linksComponents--)
     * I'll arrange a method in XSLFOMLFC for this
     */
    public void moveActiveLinkDown()
    {
        if(linkComponents!=null&&linkComponents.size()!=0)
        {
            ((XLinkComponent)linkComponents.elementAt(activeLink)).setActive(false);
            if(++activeLink>=linkComponents.size())
                activeLink=0;
            ((XLinkComponent)linkComponents.elementAt(activeLink)).setActive(true);
        }
    }
    
    private XComponent createLinkComponent(Rectangle r, String dest)
    {
        XLinkComponent c = mlfcListener.getComponentFactory().getXLinkComponent("");
        c.addClickedActionListener(new ClickListener());
        c.addHoverListener(new URLFListener());
        double divisor = (1000.0 / (scaleFactor/100.0));
        c.setBounds((int)(r.x / divisor)-5,
        (int)( (pageHeight - (r.y / 1000)) *(scaleFactor/100))-5,
        (int)(r.width / divisor)+5,
        (int)(r.height / divisor)+5);
        c.setDestination(dest);
        return c;
    }
    
    private class ClickListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            Log.debug("Click listened");
            try
            {
                URL url = new URL(xmldoc.getXMLURL(),e.getActionCommand());
                MouseEvent me = null;
                if (e instanceof ActionEventEx)
                    me=((ActionEventEx)e).getMouseEvent();
                if (me!=null)
                {
                    if (EventUtil.isPropertyRequest(me))
                    {
                        mlfcListener.showLinkPopup(url,xmldoc,me);
                    }
                    else if (EventUtil.isFollowAlternativeRequest(me)&&mlfcListener.getIsTabbedGUI())
                    {
                            Log.debug("Middle click");
                            mlfcListener.openInNewTab(new XLink(url),null);
                    }
                    else
                        mlfcListener.openLocation(url);
                }
                else
                {
                    mlfcListener.openLocation(url);
                }
            } catch (java.net.MalformedURLException err)
            {
                Log.error(err);
            }
        }
    }
    
    private class URLFListener implements XHoverListener
    {
        public void focusGained(String s)
        {
            mlfcListener.setStatusText(s);
        }
        public void focusLost()
        {
            mlfcListener.setStatusText("");
        }
        
    }
    
    
    /**
     * Navigates to the page pointed by the link that is active
     *
     */
    public void followActiveLink()
    {
        if(linkComponents!=null||linkComponents.elementAt(activeLink)!=null)
        {
            mlfcListener.openLocation(((XLinkComponent)linkComponents.elementAt(activeLink)).getDestination());
        }
    }
    
    /**
     *  set's components on the previous page invisible
     *
     */
    protected void removeComponents(Hashtable fields,Container p)
    {
        for (Enumeration e = fields.elements();e.hasMoreElements();)
        {
            Object obj=e.nextElement();
            if (obj instanceof FormWidgetArea)
            {
                FormWidgetArea formArea = (FormWidgetArea)obj;
                Component tf = formArea.getComponent();
                tf.setVisible(false);
            }
            else if (obj instanceof XSmilesImage)
            {
                // XSmilesImages contain containers
                ((XSmilesImage)obj).getContainer().setVisible(false);
                // Pausing of SMIL did quite not work.
                //				Object obj2 = mlfcList.get(obj);
                //				MLFC mlfc = (MLFC)obj2;
                //				if (mlfc!=null)
                //				{
                //					Log.debug("Trying to pause MLFC:"+mlfc.getClass());
                //					if (mlfc instanceof SMILMLFC) ((SMILMLFC)mlfc).pauseMLFC();
                //				} else Log.error("XSmilesAWTRenderer.removeComponents:null");
            }
        }
    }
    
    private void removeLinks()
    {
        //Iterator i = linkComponents.iterator();
        Enumeration e = linkComponents.elements();
        while(e.hasMoreElements())
        {
            XComponent c = (XComponent) e.nextElement();
            container.remove(c.getComponent());
            c.getComponent().setVisible(false);
        }
        linkComponents = new Vector();
    }
    
    public int renderFormArea(FormWidgetArea area,Rectangle rect)
    {
        
        org.apache.fop.layout.Area app=area;
        //<<<<<<< XSmilesAWTRenderer.java
        //Rectangle rect = this.getBounds(area);
        int x = rect.x;
        int y = rect.y;
        //	    this.showWidget(area,new Double(x/1000).intValue(),new Double(pageHeight-(y/1000)).intValue());
        //		Rectangle2D rect = this.getBounds(area);
        //	    double x = rect.getX();
        //	    double y = rect.getY();
        double divisor = (1000.0 / (scaleFactor/100.0));
        this.showWidget(area,new Double(x/divisor).intValue(),new Double((pageHeight-(y/1000.0))*(scaleFactor/100.0)).intValue());
        int h = area.getHeight();
        return h;
    }
    
    
    /**
     * This is called for every widget on the current page.
     * It checks whether the component exists in the area object
     * if it doesn't -> it is created
     * if it does -> it is set visible
     *
     */
    protected void showWidget(FormWidgetArea area, int posx, int posy)
    {
        //Log.debug("showWidget "+area.getFormElement().getElementName());
        //this.count++;
        VisualComponentService ext=area.getExtension();
        Component t=ext.getComponent();
        
        // If this page has been once rendered, the component already exists
        // Otherwise we have to create it
        org.apache.fop.layout.Area parent = area.getParent();
        //org.apache.fop.layout.Area parent2=parent.getParent();
        if (t!=null)
        {
            //		Log.debug("getTooltipText: "+((JComponent)t).getToolTipText());
            container.add(t,0);
            container.invalidate();
        }
        else
        {
            Log.error("showWidget, should not came here");
        }
        // We have all the inputs in the hashtable and
        //textfields.put(area.getInput().getName(),t);
        widgets.put(area.getComponent(),area);
        t.setLocation(posx,posy);
        t.setVisible(true);
        ext.setZoom(this.scaleFactor/100);
        ext.setVisible(true);
        t.invalidate();
    }
    
    /**
     * XSmiles extension: External SVG images using SVGMLFC
     *
     */
    public int renderExternalImageArea(ImageArea area,Rectangle rect)
    {
        
        // XSMILES SVG EXTENSION
        // SVGMLFC uses CSIRO viewer
        FopImage img = area.getImage();
        XSmilesImage ximg = (XSmilesImage)img;
        String urlString = ximg.getURL();
        Container p=ximg.getContainer();
        this.widgets.put(ximg,ximg);
        // Rectangle2D rect = this.getBounds(area);
        // double x = rect.getX();
        // double y = rect.getY();
        
        //			Rectangle rect = this.getBounds(area);
        double x = rect.x;
        double y = rect.y;
        
        
        int w = area.getContentWidth();
        int h = area.getHeight();
        
                /*			Rectangle bounds = new Rectangle((int)(x / 1000),
                (int)(pageHeight - y / 1000),
                w / 1000,
                h / 1000);*/
        double divisor = (1000.0 / (scaleFactor/100.0));
        Rectangle bounds = new Rectangle((int)(x / divisor),
        (int)((pageHeight-(y/1000.0))*(scaleFactor/100.0)),
        (int)(w / 1000),
        (int)(h / 1000));
        // Check if the container already exists
        if (p!=null)
        {
            // Just set the container visible
            MLFC mlfc = (MLFC)mlfcList.get(ximg);
            p.setBounds(bounds);
            //mlfc.setSize(bounds.getSize());
            p.setVisible(true);
            //				Pausing of SMIL did quite not work.
            //				if (mlfc!=null)
            //				{
            //					if (mlfc instanceof SMILMLFC) ((SMILMLFC)mlfc).continueMLFC();
            //				}
            
        }
        else
        {
            try
            {
                // Create the container and the external MLFC
                URL url = new URL(urlString);
                // Should this be just container?
                p = new JPanel(false);
                p.setLayout(new BulletinLayout()); // why do we have to use null layout here?
                this.container.add(p,0);
                ximg.setContainer(p);
                // create an XLink object
                XLink link = new XLink(url);
                p.setBounds(bounds);
                // call MLFCManager.activateSecondaryMLFC(xlink,p)
                mlfcListener.displayDocumentInContainer(link,p);
                //mlfcList.put(ximg,mlfc);
                
            } catch(java.net.MalformedURLException mue)
            {
                // cannot normally occur because, if URL is wrong, constructing FopImage
                // will already have failed earlier on
            }
        }
        return h;
    }
    
    
}



