/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

/**
 * FormElement class
 * @author Mikko Honkala
 *
 * $Id: FormElement.java,v 1.33 2004/06/29 12:52:19 honkkis Exp $
 *
 */

package fi.hut.tml.xsmiles.mlfc.xslfo.form;
//import  org.apache.fop.fo.flow;

//XSmiles
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.xslfo.apps.Driver2;
import fi.hut.tml.xsmiles.mlfc.xslfo.apps.DocumentReader2;
import fi.hut.tml.xsmiles.mlfc.xslfo.XSLFOMLFC;
import fi.hut.tml.xsmiles.mlfc.xforms.dom.XFormsControl;

import fi.hut.tml.xsmiles.dom.CompoundService;
import fi.hut.tml.xsmiles.dom.VisualComponentService;


// FOP
import org.apache.fop.fo.*;
import org.apache.fop.fo.properties.*;
import org.apache.fop.layout.*;
import org.apache.fop.layout.inline.*;
import org.apache.fop.apps.FOPException;
import org.apache.fop.datatypes.*;

import org.w3c.dom.*;

import java.awt.Component;

/**
 * The base class for form elements (inputs, textfields, buttons, etc.) in the fo tree.
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.33 $
 */
 


public class FormElement extends FObj
    {
	protected org.w3c.dom.Element domElement;
	protected Component myComponent;
	protected boolean extensionHandled=false;
	protected VisualComponentService control; // used for getting the size
    AreaContainer areaContainer;
    public static class Maker extends FObj.Maker {
    	public FObj make(FObj parent, PropertyList propertyList)
    	    throws FOPException {
//    	    Log.debug("FormElement.Maker.make() ");
    		
    	    return new FormElement(parent, propertyList);
    	}
    }

    public static FObj.Maker maker() {
    	return new FormElement.Maker();
    }

    public FormElement(FObj parent, PropertyList propertyList) {
		super(parent, propertyList);
//		Log.debug("FormElement.constructor ( ) ");
		// If property size is set, then save it as an int, otherwise save -1
		domElement=(Element)DocumentReader2.getCurrentNode();
//		String lname = domElement.getLocalName();
//		if (lname!=null&&lname.equals("xform"))
//		{
//			fi.hut.tml.xsmiles.mlfc.xslfo.XSLFOMLFC.handleExtension(this.getDomElement());
//			extensionHandled=true;
//		}
    }
	public org.w3c.dom.Element getDomElement()
	{
		return this.domElement;
	}

	public VisualComponentService getControl()
	{
		return control;
	}
	
	public Status layout(Area area) throws FOPException {
		if (!(area instanceof ForeignObjectArea)) {
				// this is an error
				throw new FOPException("SVG not in fo:instream-foreign-object");
		}
		this.createComponent();
		FormWidgetArea widgetArea = FormWidgetArea.createWidgetArea(this);
		ForeignObjectArea foa = (ForeignObjectArea)area;
		foa.setObject(widgetArea);
		foa.setIntrinsicWidth(widgetArea.getAllocationWidth());
		foa.setIntrinsicHeight(widgetArea.getHeight());
		foa.increaseHeight(widgetArea.getHeight());
		return new Status(Status.OK);
	}
	public Component createComponent()
	{
		if (extensionHandled) return null;
//		control = fi.hut.tml.xsmiles.mlfc.xslfo.XSLFOMLFC.handleExtension(this.getDomElement());
//		if (control!=null) 
//		{   
//			myComponent = control.getComponent();
//			return myComponent;
//		}
		Element elem = this.getDomElement();
		if (elem instanceof VisualComponentService)
		{
			control = (VisualComponentService)elem;
			if (control!=null) 
			{   
				myComponent = control.getComponent();
				return myComponent;
			}
		}
		else if (elem instanceof CompoundService)
		{
			control = ((CompoundService)elem).getVisualComponent();
			if (control!=null) 
			{   
				myComponent = control.getComponent();
				return myComponent;
			}
		}
		else
		{
			Log.error("Forms dom element is not VisualComponentService: "+elem.toString());
		}
		return null;
	}
	
	public Component getComponent()
	{
		return myComponent;
	}
	public VisualComponentService getExtension()
	{
		return control;
	}
}
