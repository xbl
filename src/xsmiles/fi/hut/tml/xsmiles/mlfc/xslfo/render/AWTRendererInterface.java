/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xslfo.render;

import fi.hut.tml.xsmiles.BrowserWindow;


import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;

import org.apache.fop.layout.AreaTree;
import org.apache.fop.layout.Page;

import fi.hut.tml.xsmiles.mlfc.xslfo.form.FormWidgetArea;
/**
 * XSMiles extensions to the org.apache.fop.awt.AWTRenderer.
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.9 $
 */

public interface AWTRendererInterface {
    public void setScaleFactor(double newScaleFactor);
    public int getPageCount();
    public void setPageNumber(int aValue);
    public void render(int pageNum);
    public Component getRenderedComponent();
    public void setAreaTree(AreaTree atree);
    public void renderFormArea(FormWidgetArea formArea);
    public void moveActiveLinkUp();
    public void moveActiveLinkDown();
    public void followActiveLink();
	public void setParentComponent(Container cont);
}


