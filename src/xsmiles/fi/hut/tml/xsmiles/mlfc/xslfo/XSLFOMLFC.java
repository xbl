/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.mlfc.xslfo;
/*
  This class containes parts of file AWTCommandLine
  Made the changes posted to the FOP mailing list:
  http://archive.covalent.net/xml/fop-dev/2000/09/0279.xml
*/

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URL;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

import org.apache.fop.viewer.SecureResourceBundle;
import org.apache.fop.viewer.Translator;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XButton;
import fi.hut.tml.xsmiles.gui.components.XInput;
import fi.hut.tml.xsmiles.gui.components.XPanel;
import fi.hut.tml.xsmiles.gui.components.XSelectOne;
import fi.hut.tml.xsmiles.gui.components.awt.BulletinLayout;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCController;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.mlfc.xslfo.apps.Driver2;
import fi.hut.tml.xsmiles.mlfc.xslfo.image.ExternalGraphic2;
import fi.hut.tml.xsmiles.mlfc.xslfo.render.AWTRendererInterface;


/**
 *
 * MLFC for displaying XSL FO documents.
 *
 */
public class XSLFOMLFC extends MLFC 
{
  /*
    Because these are static, multiple instances of XSLFOMLFC may not work
  */
  protected     Driver2       driver;
  private 	Container     contentPanel;  // fo graphics added here

  // The component for choosing the page
  //private XPageSelector    pageSelector; 
  private JScrollPane      scrollPane;

  private AWTRendererInterface 	renderer; 
  protected Component	   foGraphicComponent; 
  private Translator       resource;
  public static String     TRANSLATION_PATH = "/org/apache/fop/viewer/resources/";
  public int               currentPage = 0;
  public int               pageCount   = 0;

  
  XSelectOne selectOne;
  XButton back, forward;
  XInput page;
  ScaleListener selectOneListener;
  
  /** The possible CSS stylesheet object. TODO: change this not to be dependant of CSS package */
//  XMLStyleSheet2 styleSheet;

	/**
	 * Get the version of the MLFC. This version number is updated 
	 * with the browser version number at compilation time. This version number
	 * indicates the browser version this MLFC was compiled with and should be run with.
	 * @return 	MLFC version number.
	 */
	public final String getVersion() {
		return Browser.version;
	}

  public void start() 
  {
    // Process the initial ecmascripts
//    currentDoc.processEcmaScript(currentDoc.getDocument());

    contentPanel=new JPanel();
    contentPanel.setLayout(new BulletinLayout());

    scrollPane=new JScrollPane(contentPanel);
    this.getContainer().add(scrollPane);

    ExternalGraphic2.baseURL=this.getXMLDocument().getXMLURL();  

    Log.debug("starting display of XMLDoc at XSLFOMLFC display : $Revision: 1.128 $");
    try 
      { 
	Log.debug(this.getXMLDocument().getLink().getURL().toString());
      } catch (Exception e) 
		
	{
	  Log.error(e);
	}
    Document doc = this.getXMLDocument().getDocument();
	
    try 
      {
	buildAndFormatFOTree(doc, null, contentPanel);
	createAbstractControls();
        this.getContainer().invalidate();
        this.getContainer().getParent().validate();
	
      } catch (Exception e) 
	{
	  Log.error(e);
	}
  }
  
  /**
   * Create a DOM element.
   */
   public Element createElementNS(DocumentImpl doc, String URI,String tagname)
   {
   		String localname=getLocalname(tagname);
   		if (localname.equals("root")) return new RootElementImpl(this,doc,URI,tagname);
		else return null;
   }
   
   public void refresh()
   {
   		if (this.getContainer()==null||scrollPane==null) return;
   		this.getContainer().remove(scrollPane);
		renderer=null;
   		this.start();	
		this.getContainer().validate();
   }
   
    /*
protected String getLocalname(String tagname)
   {
   	int index = tagname.indexOf(':');
   	if (index<0) return tagname;
   	String localname = tagname.substring(index+1);
   	return localname;
   }
    */
   
  

  /**
   * Create abstract components with the current GUI's component factory
   * and beam them to the GUI
   */
  private void createAbstractControls() {
    // Only display controls for a primary document
    if (isPrimary() == true) {
	    ComponentFactory cf=getMLFCListener().getComponentFactory();
	    XPanel cb = getMLFCListener().getMLFCControls().getMLFCToolBar();
	    cb.removeAll();
	    
	    selectOne = cf.getXSelectOne("minimal",false);
	    selectOne.addSelection("25%");
	    selectOne.addSelection("50%");
	    selectOne.addSelection("75%");
	    selectOne.addSelection("100%");
	    selectOne.addSelection("125%");
	    selectOne.addSelection("150%");
	    selectOne.addSelection("200%"); 
	    selectOne.addSelection("300%");
	    selectOne.setSelected("100%");
	    selectOneListener = new ScaleListener();
	    selectOne.addItemListener(selectOneListener);
	    back=cf.getXButton("img/ipaq/fop-m.gif");
	    back.setActionCommand("Back");
	    forward=cf.getXButton("img/ipaq/fop-pl.gif");
	    forward.setActionCommand("Forward");
	    
	    page=cf.getXInput();
	    page.setBackground(Color.white);
	    page.setText("Page 1/" + pageCount);
	    page.addFocusListener(new FocusLiz());
	    
	    cb.add(selectOne);


	    if(pageCount>1) {
	      cb.add(back);
	      cb.add(page);
	      cb.add(forward);
	    }

	    back.addActionListener(new PageListener());
	    page.addActionListener(new PageListener());
	    forward.addActionListener(new PageListener());
	    //cb.validate();
    }
  }

  private class FocusLiz implements FocusListener 
  {
    public void focusGained(FocusEvent e) 
    {
      page.setText("");
    }
    
    public void focusLost(FocusEvent e)
    {
      page.setText("Page "+(currentPage+1) + "/" +pageCount);
    }
  }
  private class ScaleListener implements ItemListener {
    public void itemStateChanged(ItemEvent e) {
      Log.debug(e.toString());
      if (e.getStateChange()==ItemEvent.DESELECTED) return;
      setScale((String)e.getItem());
    }
  }

  private class PageListener implements ActionListener {
    public void actionPerformed(ActionEvent ev) {
		String e = ev.getActionCommand();
      if(e.equals("Back"))
	goToPreviousPage();
      if(e.equals("Forward"))
	goToNextPage();
      else {
	try {
		e = page.getText();
	  int p=Integer.parseInt(e);
	  if(p>0 && p<=pageCount)
	    p=p-1;
	  goToPage(p);
	} catch(Exception ee) {
	}
      } 
      Log.debug(e);
    }
  }

  /**
   * @param cont The container, where to place extension elements
   */
  public void buildAndFormatFOTree(Document foDoc,String language, Container cont) 
  {
    resource=null;
    if (this.getJavaVersion()<1.2)
      {
	if (renderer==null) 
	  {
	    renderer = this.createRenderer(
					   "fi.hut.tml.xsmiles.mlfc.xslfo.render.jdk11.XSmilesAWTRenderer11",
					   resource
					   );
	    Log.debug("Created XSL FO renderer");	   
	  }
	
	renderer.setParentComponent(cont);	   
	((AWTRendererInterface)renderer).setScaleFactor(100.0);
      } else
	{
	  if (renderer==null) 
	    {
	      renderer = this.createRenderer(
					     "fi.hut.tml.xsmiles.mlfc.xslfo.render.jdk12.XSmilesAWTRenderer",
					     resource);
	      Log.debug("Created XSL FO renderer");	   
	    }	   
	  renderer.setParentComponent(cont);	   
	  //Double dscale = new Double(scaleSelected.substring(0,scaleSelected.length()-1));
	  //  ((AWTRendererInterface)renderer).setScaleFactor(dscale.doubleValue());
	  
	}


    try {
      if (driver == null)
	{
	  driver = new Driver2();
	  driver.addElementMapping("fi.hut.tml.xsmiles.mlfc.xslfo.image.StandardElementMapping2");
	  //driver.addPropertyList("org.apache.fop.fo.StandardPropertyListMapping");
	
	
	  driver.addElementMapping("fi.hut.tml.xsmiles.mlfc.xslfo.form.FormElementMapping");
	  //driver.addPropertyList("fi.hut.tml.xsmiles.mlfc.xslfo.form.FormPropertyListMapping");
	  Log.debug("Created a XSL FO driver");
	} else
	  {
	    driver.reset();
	  }
      driver.setRenderer((org.apache.fop.render.Renderer)renderer);
      driver.render(foDoc);
      this.showPage(false);
	    
    } catch (Exception e) {
      Log.error(e);
    }
  }


  public void stop() 
  {
    Log.debug("*** XSLFO MLFC deactivate");
    foGraphicComponent=null;

    // remove all mlfc control components from gui.
    getMLFCListener().getMLFCControls().getMLFCToolBar().removeAll();
	if (selectOne != null)
	  selectOne.removeItemListener(selectOneListener);

    if(contentPanel!=null)
      contentPanel.removeAll();
    if (driver!=null) 
      driver.reset();
    renderer=null;
    driver=null;
  }
  /**
   * Change the current visible page
   *
   * @param number the page number to go to
   */
  //  private void goToPage(int number) {
  public void goToPage(int number) {
    Log.debug("Going to page " +number);
    currentPage = number;
    renderer.setPageNumber(number);
    showPage(true);
    page.setText("Page "+(number+1) + "/" +pageCount);
  }

  /**
   * Shows the previous page.
   */
  private void goToPreviousPage() {
    if (currentPage <= 0) 
      return;
    currentPage--;
    goToPage(currentPage);
  }


  /**
	 * Shows the next page.
	 */
  private void goToNextPage() {
    if (currentPage >= pageCount - 1)
      return;
    currentPage++;
    goToPage(currentPage);
  }

  /**
	 * Shows the last page.
	 */
  private void goToLastPage() {

    if (currentPage == pageCount - 1) return;
    currentPage = pageCount - 1;

    goToPage(currentPage);
  }

  /**
   * Shows the first page.
   */
  private void goToFirstPage(ActionEvent e) {
    if (currentPage == 0)
      return;
    currentPage = 0;
    goToPage(currentPage);
  }

  public void setScale(String scaleStr) {
    scaleStr=scaleStr.substring(0,scaleStr.length()-1)+".0";
    //Log.debug(scaleStr);
    Double bd=new Double(scaleStr);
    //Log.debug(bd.toString() + " " +bd.doubleValue());
    try {
      setScale(bd.doubleValue());
    } catch(Exception ee) {
      Log.error(ee);
    }
  }

  public void setScale(double scaleFactor) {
    ((AWTRendererInterface)renderer).setScaleFactor(scaleFactor);
    showPage(true);
  }

  private boolean pageRendered;
  public void showPage(boolean doRender) 
  {
    contentPanel.setVisible(false);
    // XXX should get the Page object somewhere
	if (doRender) renderer.render(currentPage);
    
    // remove previous page
    if (foGraphicComponent!=null) 
      contentPanel.remove(foGraphicComponent);
    
    foGraphicComponent=null;
    foGraphicComponent = renderer.getRenderedComponent();
    contentPanel.add(foGraphicComponent,BorderLayout.CENTER);
	
    pageCount = renderer.getPageCount();
    //contentPanel.setSize(foGraphicComponent.getSize());

    contentPanel.setVisible(true);
  }

  public void dispose() {
    System.exit(0);
  }

  private SecureResourceBundle getResourceBundle(String path) 
  {
    InputStream in = null;
    try {
      URL url = getClass().getResource(path);
      in = url.openStream();
    } catch(Exception ex) {
      Log.error("Can't find URL to: <" + path + "> " + ex.getMessage());
    }
    return new SecureResourceBundle(in);
  }
  public AWTRendererInterface createRenderer(String className,Translator aRes)
  {
    Object instance = null;
    Class guiClass = null;
    try {
      Class[] constructorParameters;
      Object[] initArgs;
      guiClass = Class.forName(className);
      
      constructorParameters = new Class[3];
      constructorParameters[0] = Class.forName("org.apache.fop.viewer.Translator");
      constructorParameters[1] = MLFCListener.class;
      constructorParameters[2] = XMLDocument.class;
      //	    constructorParameters[1] = Class.forName("java.awt.Container");
      Constructor constructor =
	guiClass.getDeclaredConstructor(
					constructorParameters);
      initArgs = new Object[3];
      initArgs[0] = aRes;
      initArgs[1] = getMLFCListener();
      initArgs[2] = getXMLDocument();
      //	    initArgs[1] = cont;
      instance = constructor.newInstance(initArgs);
      return (AWTRendererInterface)instance;
    } catch (java.lang.reflect.InvocationTargetException ex) {
      Throwable t = ex.getTargetException();
      String msg;
      if (t != null) {
	msg = t.getMessage();
      }
      else {
	msg = ex.getMessage();
      }
      Log.error(msg);
    } catch (Exception ex) {
      Log.error(ex);
    }
    return null;
  }	
  
  public MLFCController getMLFCController() {
    Log.debug("returning XSLFOController");
    return new XSLFOController();
  }



  private double getJavaVersion()
  {
    //return 1.1;
    return getMLFCListener().getJavaVersion();
  }


  private class XSLFOController extends MLFCController {
    private Dimension panelSize,vpSize;
    private int step=25;
    private JViewport vp;

    public XSLFOController() {
      //vp=scrollPane.getViewport();
      //vpSize=vp.getViewSize();
      //panelSize=mlfcPanel.getPreferredSize();
    }
    public void pageForward() {
      Log.debug("Page Forward");
      goToNextPage();
    }
    
    public void pageBack() {
      Log.debug("Page Back");
      goToPreviousPage();
    }
    
    /**
     * @return false, if cannot move any more up
     */
    public boolean scrollUp() {
      return true;
      /*vp=scrollPane.getViewport();
	vpSize=scrollPane.getViewportBorderBounds().getSize();
	panelSize=vp.getViewSize();
      Log.debug("Scrolling up :" + vpSize + " " +panelSize + " ");
      int x=vp.getViewPosition().x;
      int y=vp.getViewPosition().y;
      if(y==0)
	return false;
      else {
	if(y-step < 0)
	  vp.setViewPosition(new Point(x,0));
	else
	  vp.setViewPosition(new Point(x,y-step));
      }
      Log.debug("Position" + vp.getViewPosition() + " ");
      return true;
      */
    }
    
    /**
     * @return false, if cannot move any more down
     */
    public boolean scrollDown() {
      return true;
      /*
      vp=scrollPane.getViewport();
      vpSize=scrollPane.getViewportBorderBounds().getSize();
      panelSize=vp.getViewSize();
      Log.debug("Scrolling down :" + vpSize + " " +panelSize + " ");
      int x=vp.getViewPosition().x;
      int y=vp.getViewPosition().y;
      if(y+vpSize.height == panelSize.height)
	return false;
      else {
	if((panelSize.height - (y+vpSize.height)) < step)
	  vp.setViewPosition(new Point(x,(panelSize.height-vpSize.height)));
	else
	  vp.setViewPosition(new Point(x,y+step));
      }
      Log.debug("Position" + vp.getViewPosition() + " ");
      return true;
      */
    }

    /**
     * @return false, if cannot move any more up
     */
    public boolean scrollLeft() {
      return true;
      /*
      vp=scrollPane.getViewport();
      vpSize=scrollPane.getViewportBorderBounds().getSize();
      panelSize=vp.getViewSize();
      Log.debug("Scrolling up :" + vpSize + " " +panelSize + " ");
      int x=vp.getViewPosition().x;
      int y=vp.getViewPosition().y;
      if(x==0)
	return false;
      else {
	if(x-step < 0)
	  vp.setViewPosition(new Point(0,y));
	else
	  vp.setViewPosition(new Point(x-step,y));
      }
      Log.debug("Position" + vp.getViewPosition() + " ");
      return true;
      */
    }
   
    public void moveActiveSpotUp() {
      renderer.moveActiveLinkUp();
    }

    public void moveActiveSpotDown() {
      renderer.moveActiveLinkDown();
    }

    public void followActiveLink() {
      renderer.followActiveLink();
    }


    /**
     * @return false, if cannot move any more down
     */
    public boolean scrollRight() {
      return true;
      /*
      vp=scrollPane.getViewport();
      vpSize=scrollPane.getViewportBorderBounds().getSize();
      panelSize=vp.getViewSize();
      Log.debug("Scrolling down :" + vpSize + " " +panelSize + " ");
      int x=vp.getViewPosition().x;
      int y=vp.getViewPosition().y;
      if(x+vpSize.height == panelSize.height)
	return false;
      else {
	if((panelSize.height - (x+vpSize.height)) < step)
	  vp.setViewPosition(new Point((panelSize.height-vpSize.height),y));
	else
	  vp.setViewPosition(new Point(x+15,y));
      }
      Log.debug("Position" + vp.getViewPosition() + " ");
      return true;
      */
    }
  }

  
}
