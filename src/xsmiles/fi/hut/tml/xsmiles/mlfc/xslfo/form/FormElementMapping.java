/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/*-- $Id: FormElementMapping.java,v 1.32 2003/10/03 15:33:39 honkkis Exp $ -- 
 */

package fi.hut.tml.xsmiles.mlfc.xslfo.form;

import org.apache.fop.fo.*;
import java.util.*;
/**
 * Maps the Form elements
 *
 * @author       Mikko Honkala
 * @version      $Revision: 1.32 $
 */

public class FormElementMapping implements ElementMapping {
	//public final static String xforms_ns="http://www.w3.org/2001/12/xforms";
	//public final static String xforms_ns="http://www.w3.org/2001/11/xforms-editors-copy";
	public final static String xforms_ns="http://www.w3.org/2002/xforms";
	
	public final static String smil_ns="http://www.w3.org/2001/SMIL20/Language";

	public final static String x3d_ns="http://www.x-smiles.org/2002/x3d";

    public void addToBuilder(TreeBuilder builder) {
		this.addToBuilder(builder,xforms_ns);
    }
	
	protected void addToBuilder(TreeBuilder builder, String uri)
	{

	// The XForms UI bindings
	builder.addMapping(uri, "model", FormElement.maker()); 
	builder.addMapping(uri, "submission", FObjMixed.maker()); 
	builder.addMapping(uri, "schema", FObjMixed.maker()); 
	builder.addMapping(uri, "instance", FObjMixed.maker()); 


	builder.addMapping(uri, "submit", FormElement.maker()); 
	builder.addMapping(uri, "trigger", FormElement.maker()); 
	builder.addMapping(uri, "textarea",FormElement.maker()); 
	builder.addMapping(uri, "input",FormElement.maker()); 
	builder.addMapping(uri, "range",FormElement.maker());
	builder.addMapping(uri, "secret",FormElement.maker()); 
	builder.addMapping(uri, "selectboolean",FormElement.maker()); 
	builder.addMapping(uri, "select1",FormElement.maker()); 
	builder.addMapping(uri, "select",FormElement.maker()); 
	builder.addMapping(uri, "output",FormElement.maker()); 
	builder.addMapping(uri, "onevent", FObjMixed.maker()); 
	builder.addMapping(uri, "script", FObjMixed.maker()); 
	builder.addMapping(uri, "item", FObjMixed.maker()); 
	builder.addMapping(uri, "script", FObjMixed.maker()); 
	builder.addMapping(uri, "label", FObjMixed.maker()); 
	builder.addMapping(uri, "hint", FObjMixed.maker()); 

	// SMIL mappings
	this.addSmilToBuilder(builder,smil_ns);

	// X3D mappings
	this.addX3DToBuilder(builder,x3d_ns);

	// The old XHTML bindings
//	String uri = "http://www.w3.org/1999/xhtml";
//
//	builder.addMapping(uri, "form", Form.maker()); 
//	builder.addMapping(uri, "input",
//			   Input.maker()); 
//	builder.addMapping(uri, "textarea",
//			   TextAreaFo.maker()); 
//	builder.addMapping(uri, "button",
//			   Button.maker()); 

        builder.addPropertyList(uri, FormPropertyListMapping.getGenericMappings());
        /* Add any element mappings */
        for (Enumeration e = FormPropertyListMapping.getElementMappings();
                e.hasMoreElements(); ) {
            String elem = (String)e.nextElement();
            builder.addElementPropertyList(uri, elem,
                                           FormPropertyListMapping.getElementMapping(elem));
        }
	

    }

	/**
	 * SMIL mappings
	 */
    protected void addSmilToBuilder(TreeBuilder builder, String uri)
    {

	    // The SMIL UI bindings
	    builder.addMapping(uri, "ref", FormElement.maker()); 
	    builder.addMapping(uri, "animation", FormElement.maker()); 
	    builder.addMapping(uri, "audio", FormElement.maker()); 
	    builder.addMapping(uri, "img", FormElement.maker()); 
	    builder.addMapping(uri, "text", FormElement.maker()); 
	    builder.addMapping(uri, "textstream", FormElement.maker()); 
	    builder.addMapping(uri, "video", FormElement.maker()); 
    }

    /**
     * X3D mappings
     */
    protected void addX3DToBuilder(TreeBuilder builder, String uri)
    {

        // The X3D UI bindings
        builder.addMapping(uri, "X3D", FormElement.maker()); 
    }


}
