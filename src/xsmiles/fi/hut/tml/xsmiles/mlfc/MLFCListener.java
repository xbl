package fi.hut.tml.xsmiles.mlfc;

import java.awt.Container;
import java.net.URL;

import fi.hut.tml.xsmiles.BrowserLogic;
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.mlfc.MLFCControls;
import fi.hut.tml.xsmiles.protocol.wesahmi.BrowserSubscriber;

/**
 * All traffic from the MLFC goes though this class. _NO_ direct references to
 * the Browser class from the MLFCs.
 *
 * A MLFC can be used independently through this interface, without having to
 * use the whole browser.
 *
 * @author Juha
 */
public interface MLFCListener
{
    
    /**
     * @param l Open document l, in browser.
     */
    public void openLocation(URL l);
    
    /**
     * @param s Open document with the URL string s
     */
    public void openLocation(String s);
    
    /**
     * Open url in new browserwindow
     * @param url
     */
    public void openLocationTop(String url);
    
    /**
     * Open url in new browserwindow
     * @param url
     * @param id id of the window to open url in
     */
    public void openLocationTop(XLink url, String id);

    /**
     * @return true if GUI can open new 's
     */
    public boolean  getIsTabbedGUI();
    
    /**
     * opens link in new tab with a certain id
     */
    public void openInNewTab(XLink l, String id);
    
    /**
     * Opens link from external program. The target is defined in configuration. 
     */
    public void openURLFromExternalProgram(String url);
    
    /**
     * Opens link from external program with id. The target is defined in configuration. 
     */
    public void openURLFromExternalProgram(String url, String id);

    /**
     * Opens link from external program with id. The target is defined in configuration. 
     */
    public void openURLFromExternalProgram(BrowserSubscriber browserSubscriber, String url, String id);

    public void reloadCurrentPage();
    
    /**
     * When MLFC stops, it closes a View from Wesahmi system.
     *
     */
    public void closeView();
    
    /**
     * Render embedded document in container provided. This can be used to embed
     * XML documents in each other.
     *
     * @param l Link to a XML document, which is to be rendered in container.
     * @param c   The Container, where document is to be rendered.
     */
    public XSmilesContentHandler displayDocumentInContainer(XLink l, Container c);
    
    /**
     * same as displayDocumentInContainer, but
     * will not call prefetch and play, so it is up to the user to call these functions
     */
    public XSmilesContentHandler createContentHandler(XLink link, Container cArea, boolean primary) throws Exception;
    
    /**
     * Uses the specified contentType, ie. doesn't try to read it from the HTTP-connection
     * 
     * @param contentType
     * @param link
     * @param cArea
     * @param primary
     * @return
     * @throws Exception
     */
    public XSmilesContentHandler createContentHandler(String contentType, XLink link, Container cArea, boolean primary) throws Exception;
    
    
    /**
     *?@param prop XPath to property
     * @return Value of property
     * @see XMLConfigurer for different properties.
     *
     * An example could be of a property could be "gui/screensize".
     */
    public String getProperty(String prop);
    
    /**
     * @param prop A GUI property requested.
     * @return the value
     * @see XMLConfigurator
     */
    public String getGUIProperty(String prop);
    
    /**
     * @param status The status of the MLFC in string form
     */
    public void setStatusText(String status);
    
    /**
     * If stand-alone. Then this is ir-relevant.
     *
     * @param s The new state in which to put the browser in
     * @see BrowserLogic enumeration for valid states.
     * @deprecated Use the CoreMLFC framework instead
     */
    public void setBrowserState(int i);
    
    /**
     * @param title Set the title of the presentation to
     */
    public void setTitle(String title);
    
    /**
     *
     */
    //public void linkActive(URL link);
    
    /**
     * @return The componentFactory, which deals out generic
     *         fi.hut.tml.xsmiles.gui.components.* based components.
     *         All function as specified in these interfaces.
     */
    public ComponentFactory getComponentFactory();
    
    public MLFCControls getMLFCControls();
    
    /**
     * @return Java version. Because some Javas, such as kaffe have
     *         problems with giving the correct Java version.
     */
    public double getJavaVersion();
    
    /**
     * Checks if there is a MLFC available for the given namespace URI.
     * @param namespace	URI for the namespace.
     * @return 			true if namespace has an MLFC.
     */
    public boolean isNamespaceSupported(String namespace);
    
    /**
     * @return the content area of the browser
     * @deprecated, use the getContainer() method of the MLFC
     */
    public Container getContentArea();
    
    /**
     * Show an error. This will stop the MLFC, and show a error page.
     */
    public void showErrorDialog(String title, String message);
    
    /** The modes are from XSmilesView class */
    public void showSource(XMLDocument doc, int mode, String heading);
    
    /**
     * open a link popup
     */
    public void showLinkPopup(URL url, XMLDocument doc,java.awt.event.MouseEvent e);
    
    /**
     * Navigate back, forward, reload, change view, stop etc.
     *
     * @param command See NavigationState for static variables associated with commands
     * @see NavigationState
     */
    public void navigate(int command);
    
    /** get the current zoom level */
    public double getZoom();
    

    public void setSubscriber(BrowserSubscriber s);
    
    public BrowserSubscriber getSubscriber();
    
}
