/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.events;

import java.net.URL;
import java.net.MalformedURLException;
import java.net.URLConnection;

import java.io.*;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.content.Resource;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.Log;
//import org.w3c.dom.smil20.*;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.mlfc.general.Helper;
import fi.hut.tml.xsmiles.util.XSmilesConnection;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;

/**
 * XML Events' Script Element Implementation. Currently, this element doesn't
 * have any DOM interface. None of the attributes should be modified through
 * DOM.
 */
public class ScriptElementImpl extends XSmilesElementImpl implements EventHandlerService
{

    // EventsMLFC
    EventsMLFC eventsMLFC = null;

    // XSmilesDocumentImpl - to create new elements
    DocumentImpl ownerDoc = null;

    private boolean initialized = false;

    /**
     * Constructor - Set the owner, name and namespace.
     */
    public ScriptElementImpl(DocumentImpl owner, EventsMLFC events, String namespace, String tag)
    {
        super(owner, namespace, tag);
        eventsMLFC = events;
        ownerDoc = owner;
        //Log.debug("Script created!");
    }

    /**
     * Initialize this element. This requires that the DOM tree is available.
     */
    public void init()
    {
    	try
		{
	        Log.debug("Script.init");
	        initialized = true;
	        String global = this.getAttribute("global");
	        if (global != null && global.equals("true"))
	        {
	            // This should have a generic way to access XMLDocument
	            ECMAScripter ecma = eventsMLFC.getXMLDocument().getECMAScripter();
	            // SVG may have already initilized ecmascripter
	            // Still, check it really has been initialized for global scripts
	            // The same initialize() is called in EventsMLFC.start(), but it is
	            // too late.
	            if (!ecma.isInitialized())
	                ecma.initialize(eventsMLFC.getXMLDocument().getDocument());
	            this.activate(null);
	            
	        }
		} catch (Exception e)
		{
			Log.error(e,"Could not initialize ecmascript engine.");
		}
		super.init();

    }

    /**
     * Run the scripts in this element. This is EventHandlerService's method
     */
    public void activate(Event evt)
    {
        Log.debug("Script.run");
        if (!initialized)
            return;
        try
        {
            String script = this.getScript();
            if (script != null && script.equals("") == false)
            {
                // TODO: Create a new DOM event object
                // and expose it to the ecmascripter
                //Event evt=this.createEvent();
                Log.debug("Getting ecmascripter");

                // This should have a generic way to access XMLDocument
                ECMAScripter ecma = eventsMLFC.getXMLDocument().getECMAScripter();
                if (evt != null)
                    ecma.exposeToScriptEngine("evt", evt);
                ecma.eval(script);
                if (evt != null)
                    ecma.deleteExposedObject("evt", evt);
            } else
                Log.debug("No scripts found.");

        } catch (Exception ex)
        {
            Log.error(ex);
        }
    }

    /**
     * Returns the script String of the owner element
     */
    public String getScript()
    {
        String scriptStr;
        Attr srcAttr = this.getAttributeNode("src");
        if (srcAttr != null)
        {
            scriptStr = this.getExternalScript(srcAttr.getNodeValue());
            if (scriptStr != null)
                return scriptStr;
        }
        return this.getInlineScript();
    }
    /**
     * Returns the script String of the owner element
     */
    public String getInlineScript()
    {
        NodeList children = this.getChildNodes();
        String scripttext = "";
        for (int c = 0; c < children.getLength(); c++)
        {
            org.w3c.dom.Node script = children.item(c);
            if (script.getNodeType() == org.w3c.dom.Node.TEXT_NODE
                || script.getNodeType() == org.w3c.dom.Node.CDATA_SECTION_NODE)
            {
                scripttext += script.getNodeValue();
            }
        }

        return scripttext;
    }
    /**
     * Returns the script String from the URL
     */
    public String getExternalScript(String unresolvedURL)
    {
		String scripttext=null;
        // resolve URL
		try
		{
            URL url = this.resolveURI(unresolvedURL);
            XSmilesConnection conn = this.eventsMLFC.get(url,Resource.RESOURCE_SCRIPT);
            InputStream stream = conn.getInputStream();
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            Helper.copyStream(stream,outStream,1000);
            scripttext=outStream.toString();
		} catch (Exception e)
		{
			Log.error(e);
		}
        // receive script text from URL
        return scripttext;
    }

    /**
     * Generate dom event
     */
    private org.w3c.dom.events.Event createMouseEvent()
    {
        MouseEventImpl evt = (MouseEventImpl) EventFactory.createEvent("MouseEvent");
        evt.initMouseEvent(
            "click",
            false,
            false,
            null,
            (short) 0,
            (int) 0,
            (int) 0,
            (float) 0,
            (float) 0,
            false,
            false,
            false,
            false,
            (short) 0,
            this);
        //			    evt.initMouseEvent("onmousemove", false, false, canvas, (short)0,
        // (int)e.getX(), (int)e.getY(),
        //			       (float)((WorldMouseEvent)e).getWorldX() ,
        // (float)((WorldMouseEvent)e).getWorldY(),
        //			        false, false, false, false, (short)0, null);
        //evt.setTarget((EventTarget)this);
        return evt;

    }

    /**
     * Destroy this element.
     */
    public void destroy()
    {
    }

    /**
     * Get the unique id.
     * 
     * @return id
     * @exception DOMException
     *                       NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is
     *                       readonly.
     */
    public String getId()
    {
        return getAttribute("id");
    }

    /**
     * Set the value of id.
     * 
     * @param id
     *                  Value to assign to id.
     */
    public void setId(String id) throws DOMException
    {
        if (id != null && id.length() > 0)
        {
            setAttribute("id", id);
        } else
        {
            removeAttribute("id");
        }
    }
}
