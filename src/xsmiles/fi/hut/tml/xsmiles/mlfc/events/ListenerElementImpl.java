/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.events;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.Log;
//import org.w3c.dom.smil20.*;

/**
 * XML Events' Listener Element Implementation.
 * Currently, this element doesn't have any DOM interface.
 * None of the attributes should be modified through DOM.
 *
 * Note: the static methods are reused by EventAttrNSImpl
 */
public class ListenerElementImpl extends XSmilesElementImpl implements EventListener {

	// EventsMLFC
	EventsMLFC eventsMLFC = null;

	// XSmilesDocumentImpl - to create new elements
	DocumentImpl ownerDoc = null;

	// Event handler
	Element eventHandler = null;

	/**
	 * Constructor - Set the owner, name and namespace.
	 */
	public ListenerElementImpl(DocumentImpl owner, EventsMLFC events, String namespace, String tag)
	{
		super(owner, namespace, tag);
		eventsMLFC = events;
		ownerDoc = owner;
		//Log.debug("Listener created!");
	}

	/**
	 * Initialize this element.
	 * This requires that the DOM tree is available.
	 */
	public void init()
	{
	    super.init();
		Element e = null;
		String observer = getAttribute("observer");
		String event = getAttribute("event");
		String phase = getAttribute("phase");
		String handler = getAttribute("handler");
		Log.debug("Listener.init: observer: "+observer+" event:"+event+" phase: "+phase+" handler:"+handler);
		if (event == null || event.length() == 0)
		{
			Log.error(getAttribute("id")+": Event attribute missing.");
			return;
		}

		if (observer != null && observer.length() > 0)
		{
			e = searchElementWithId(observer,getOwnerDocument());
		} 		
		// if observer attribute is missing, then the parent node is the observer	
		else e=(Element)this.getParentNode();
		if (e == null)
		{
			Log.error(getAttribute("id")+": Observer "+observer+" not found.");
			return;
		}


		if (handler != null && handler.length() > 0)
			if (handler.charAt(0) == '#')
			{
				handler = handler.substring(1);
				eventHandler = searchElementWithId(handler,getOwnerDocument());
				Log.debug("Handler found:"+handler);
				if (eventHandler == null)
					Log.error(getAttribute("id")+": Handler "+handler+" not found.");			
			}
			else
				Log.error(getAttribute("id")+": Cannot process handler "+handler);

			// Add this as the listener for events in observer
			((EventTarget)e).addEventListener(event, this, phase.equals("capture")?true:false);
	}



	/**
	 * Searches for element with id under body.
	 * @param	id	id to search for
	 * @return	the element with id, or null if not found.
	 */
	public static Element searchElementWithId(String id, Document doc)
	{
		// Get root element
		NodeList children = doc.getChildNodes();
		Node node = null;

		// If no children, then return null
		if (children.getLength() == 0)
		{
			return null;
		}

		for (int i=0 ; i < children.getLength() ; i++)
		{
			node = children.item(i);
			// The first Element under the Document is the root element
			if (node instanceof Element)
			{
				return searchElementWithId((Element)node, id);
			}
		}

		// No elements under the document
		return null;
	}

	/**
	 * This method handles the event, when it is received
	 */
	public void handleEvent(Event evt)
	{
		//
		Log.debug("!!GOT EVENT!!"+evt);

		// If this listener registered as target listener, check that target matches
		String target = getAttribute("target");

		// Stop propagation if instructed to do so
		String propagate = getAttribute("propagate");
		String defaultAction = getAttribute("defaultAction");
		handleEvent(evt, target, propagate, defaultAction, eventHandler);
	}

	public static void handleEvent(Event evt, String target, String propagate, 
						String defaultAction, Element eventHandler)
	{
		if (target != null && target.length() > 0)
		{
			EventTarget evttarget = evt.getTarget();
			if (evttarget instanceof Element)
				if (((Element)evttarget).getAttribute("id").equals(target) == true)
				{
					Log.debug("GOT TARGET EVENT!!!");
				}
				else
				{
					Log.debug("False alarm!");
					return;
				}
		}
		if (propagate != null && propagate.equals("stop") == true)
		{
			evt.stopPropagation();
		}
		if (defaultAction != null && defaultAction.equals("cancel") == true)
		{
			evt.preventDefault();
		}

		// Run scripts
		if (eventHandler != null && eventHandler instanceof EventHandlerService)
		{
			((EventHandlerService)eventHandler).activate(evt);
		}
	}

	/**
	 * Destroy this element.
	 */
	public void destroy()
	{
	}

	/**
	 * Get the unique id.
	 * @return id
	 * @exception DOMException NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getId()
	{
		return getAttribute("id");
	}

	/**
	 * Set the value of id.
	 * @param id  Value to assign to id.
	 */
	public void setId(String id) throws DOMException
	{
		if (id != null && id.length() > 0)
		{
			setAttribute("id", id);
		}
		else
		{
			removeAttribute("id");
		}
	}
}


