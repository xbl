/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.events;

import fi.hut.tml.xsmiles.dom.PseudoElementContainerService;
import fi.hut.tml.xsmiles.dom.XSmilesAttrNSImpl;
import fi.hut.tml.xsmiles.dom.EventHandlerService;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;
import org.w3c.dom.xforms10.*;

import org.apache.xerces.dom.*;


/**
 * With this attribute ev:event, it is possible to define event handlers
 * without the listener element.
 * @author Mikko Honkala
 */
public class EventAttrNSImpl extends XSmilesAttrNSImpl implements EventListener
{
    
    String event,phase;
    Element handler,observer;
    
    protected EventAttrNSImpl(DocumentImpl ownerDocument, java.lang.String namespaceURI,
    java.lang.String qualifiedName)
    {
        super(ownerDocument,namespaceURI,qualifiedName);
    }
    
    public void init()
    {
    	if (!inited)
        {
            //		Log.debug("ev:event init() called: "+this.getNodeValue());
            Attr observerAttr = this.getOwnerElement().getAttributeNodeNS(EventsMLFC.namespace,"observer");
            Attr handlerAttr = this.getOwnerElement().getAttributeNodeNS(EventsMLFC.namespace,"handler");
            Attr phaseAttr = this.getOwnerElement().getAttributeNodeNS(EventsMLFC.namespace,"phase");
            event=this.getNodeValue();
            if (handlerAttr==null)
            {
                // if handler attribute is missing, the ownerelement is the handler
                handler = this.getOwnerElement();
            }
            else
            {
                // find the id
                String handlerStr=handlerAttr.getNodeValue();
                if (handlerStr.length() > 0)
                {
                    if (handlerStr.charAt(0) == '#')
                    {
                        handlerStr = handlerStr.substring(1);
                        handler = ListenerElementImpl.searchElementWithId(handlerStr,this.getOwnerDocument());
                        //					Log.debug("Handler found:"+handlerStr);
                        if (handler == null)
                        {
                            Log.error(event+": Handler "+handlerStr+" not found.");
                        }
                    } else
                    {
                        Log.error(event+": Cannot process handler "+handlerStr);
                    }
                } else
                {
                    Log.error(event+": empty handler "+handlerStr);
                }
            }
            if (observerAttr==null)
            {
                if (handlerAttr==null)
                {
                    observer = (Element)this.getOwnerElement().getParentNode();
                }
                else
                {
                    observer = this.getOwnerElement();
                }
            }
            else
            {
                String observerStr=observerAttr.getNodeValue();
                if (observerStr != null && observerStr.length() > 0)
                {
                    observer = ListenerElementImpl.searchElementWithId(observerStr,getOwnerDocument());
                    if (observer == null)
                    {
                        Log.error(event+": Observer "+observerStr+" not found.");
                        super.init();
                        return;
                    }
                }
                
            }
            if (phaseAttr==null) phase="";
            else phase=phaseAttr.getNodeValue();
            ((EventTarget)observer).addEventListener(event, this, phase.equals("capture")?true:false);
        }
        super.init();
    }
    
    
    
    /**
     * This method handles the event, when it is received
     */
    public void handleEvent(Event evt)
    {
        Log.debug("!!GOT EVENT!!"+evt);
        
        String target=null,propagate=null, defaultAction=null;
        // If this listener registered as target listener, check that target matches
        Attr targetAttr = this.getOwnerElement().getAttributeNodeNS(EventsMLFC.namespace,"target");
        Attr propagateAttr = this.getOwnerElement().getAttributeNodeNS(EventsMLFC.namespace,"propagate");
        Attr defaultActionAttr = this.getOwnerElement().getAttributeNodeNS(EventsMLFC.namespace,"defaultAction");
        
        if (targetAttr!=null) target=targetAttr.getNodeValue();
        if (propagateAttr!=null) propagate=propagateAttr.getNodeValue();
        if (defaultActionAttr!=null) defaultAction=defaultActionAttr.getNodeValue();
        
        ListenerElementImpl.handleEvent(evt,target,propagate,defaultAction, handler);
    }
    
    public Object clone() throws CloneNotSupportedException {
        Log.debug("Cloning:"+this);
        Object element = this.getOwnerDocument().createAttributeNS(this.getNamespaceURI(),this.getLocalName());
        return element;
    }
}

