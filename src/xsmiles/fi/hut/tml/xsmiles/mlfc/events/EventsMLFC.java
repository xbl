/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.events;

import java.net.URL;
import java.awt.Dimension;
import java.awt.Container;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.XMLDocument;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.Browser;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;

/**
 * EventsMLFC is the XML Events main function. It creates the elements and attributes
 * and controls them.
 * Namespace: http://www.w3.org/2001/xml-events
 */
public class EventsMLFC extends MLFC {


	// DocumentImpl for XSmiles
	DocumentImpl xsmilesDoc = null;
	public static final String namespace = "http://www.w3.org/2001/xml-events"; 


	/**
	 * Constructor.
	 */
	public EventsMLFC()
	{
	}

	/**
	 * Get the version of the MLFC. This version number is updated 
	 * with the browser version number at compilation time. This version number
	 * indicates the browser version this MLFC was compiled with and should be run with.
	 * @return 	MLFC version number.
	 */
	public final String getVersion() {
		return Browser.version;
	}

    /*
      protected String getLocalname(String tagname)
      {
      int index = tagname.indexOf(':');
      if (index<0) return tagname;
      String localname = tagname.substring(index+1);
      return localname;
      }
    */

	/**
	 * Create a DOM element.
	 */
	public Element createElementNS(DocumentImpl doc, String ns, String tag)
	{
		xsmilesDoc = doc;
		Element element = null;
//		Log.debug("XML Events: "+ns+":"+tag);

		if (tag == null)
		{
			Log.error("Invalid null tag name!");
			return null;
		}
		String localname = getLocalname(tag);
		if (localname.equals("listener"))
		{
			element = new ListenerElementImpl(doc, this, ns, tag);
		} else if (localname.equals("script"))
		{
			element = new ScriptElementImpl(doc, this, ns, tag);
		}
		
		// Other tags go back to the XSmilesDocument as null...
		if (element == null)
			Log.debug("XML Events didn't understand element: "+tag);

		return element;


	}
	
	/**
	 * Create a DOM attribute.
	 */
	 public Attr createAttributeNS(DocumentImpl doc,String namespaceURI, String qualifiedName)
	     throws DOMException
	 {
		String localname = getLocalname(qualifiedName);
//		Log.debug("**XML events mlfc:createAttribute "+localname);
		if (localname.equals("event")) return new EventAttrNSImpl(doc,namespaceURI,qualifiedName);
		return null;
        //return new InstanceAttrNSImpl(this, namespaceURI, qualifiedName);
	 }
	

//	public void init()
//	{
//	}

	/**
	 * Displays the presentation. Initializes, prefetches and plays the presenation.
	 */
	public void start()
	{
		Log.debug("EVENTS.initialize() called.");

		try
		{
		ECMAScripter ecma = this.getXMLDocument().getECMAScripter();
		
		// SVG may have already initilized ecmascripter
		if (!ecma.isInitialized()) ecma.initialize(this.getXMLDocument().getDocument());
		}
		catch (Throwable t)
		{
		    Log.error(t,"ECMAScripter missing... continuing without.");
		}
		// Process the initial ecmascripts
		if (!this.isPrimary())
		{
			return;
		}
	}

	/**
	 * Append the given URL to be a full URL.
	 * @param partURL		Partial URL, e.g. fanfaari.wav
	 * @return 				Full URL, e.g. http://www.x-smiles.org/demo/fanfaari.wav
	 */
	public URL createURL(String partURL)
	{
		try
		{
			return new URL(this.getXMLDocument().getXMLURL(),partURL);
		} catch (java.net.MalformedURLException e)
		{
			Log.error(e);
			return null;
		}
	}

	public void stop()
	{
		Log.debug("EVENTSMLFC.deactivate()");
		xsmilesDoc = null;
	}


	/**
	 * Not implemented method.
	 */
	public void setSize(Dimension d) {
	}

	/**
	 * Called from the LinkHandler - this method asks the browser to go to this external link.
	 * @param url		URL to jump to.
	 */
	public void gotoExternalLink(String url)
	{
		Log.debug("...going to "+url);
		URL u = createURL(url);
		getMLFCListener().openLocation(u);
	} 

	/**
	 * Display status text in the broswer. Usually shows the link destination.
	 */
	public void displayStatusText(String url)
	{
		getMLFCListener().setStatusText(url);
	}


}

 

