/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jul 12, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.SelectorList;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSStyleSheet;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectors;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSOMParser;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSRuleListImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleDeclarationImpl;
import fi.hut.tml.xsmiles.mlfc.css.XSmilesCSSStyleRule;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * XBL-element: style
 * 
 * @author Mikko Vestola
 * @author Juho Rutila
 */
public class StyleElementImpl extends ElementImpl
{
    public StyleElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);

    }

    /**
     * As the document is traversed, this style is applied
     * to all the templates that are siblings with
     * parent resources-element.
     * See {@link http://www.w3.org/TR/xbl/#the-style}
     */
    public void init()
    {
        Node node = this.getParentNode().getParentNode().getFirstChild();
        while (node != null)
        {
            if (node instanceof TemplateElementImpl)
            {
                this.apply((XSmilesElementImpl) node);
            }
            node = node.getNextSibling();
        }
    }

    /**
     * This style is applied to the element.
     * @param element
     */
    public void apply(StylableElement element)
    {
        Log.debug("applying StyleElementImpl");
        if (this.getAttribute("media") != ""
                && !((XSmilesDocumentImpl) this.ownerDocument).getXMLDocument()
                        .evalMediaQuery(this.getAttribute("media")))
            return;
        XSmilesCSSOMParser parser = new XSmilesCSSOMParser();
        InputSource inputSource = this.getInputSource();
        if (inputSource == null)
            return;

        try
        {
            CSSStyleSheet stylesheet = parser.parseStyleSheet(inputSource);
            XSmilesCSSRuleListImpl rulelist = (XSmilesCSSRuleListImpl) stylesheet
                    .getCssRules();

            /*
             * We go through all the rules in this elements style. If rule's
             * selector matches to element, we append that style description to
             * the element's style.
             */
            for (int i = 0; i < rulelist.getLength(); i++)
            {
                XSmilesCSSStyleRule rule = (XSmilesCSSStyleRule) rulelist
                        .item(i);
                SelectorList selectors = rule.getSelectorList();
                XSmilesCSSStyleDeclarationImpl decl = (XSmilesCSSStyleDeclarationImpl) rule
                        .getStyle();
                for (int j = 0; j < selectors.getLength(); j++)
                {
                    if (new CSSSelectors().selectorMatchesElem(selectors
                            .item(j), element))
                    {
                        XSmilesCSSStyleDeclarationImpl current_decl = (XSmilesCSSStyleDeclarationImpl) element
                                .getStyle();
                        if (current_decl != null)
                        {
                            ((XSmilesCSSStyleDeclarationImpl) element
                                    .getStyle()).copyPropertySet(decl);
                        } else
                        {
                            element.setStyle(decl);
                        }
                    }
                }
            }
        } catch (DOMException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        /*
         * Style needs to be traversed to element's child nodes too. TODO:
         * Should it just work to put the style to parent and then children know
         * it from their parents?
         */
        Node node = element.getFirstChild();
        while (node != null)
        {
            if (node instanceof StylableElement)
                if (!(node instanceof XSmilesElementImpl)
                        || ((XSmilesElementImpl) node).getApplyBindingSheets())
                {
                    apply((StylableElement) node);
                }
            node = node.getNextSibling();
        }
    }

    protected InputSource getInputSource()
    {
        String style_filename = this.getAttribute("src");
        Reader reader = null;
        if (style_filename != "")
        {
            XSmilesConnection conn;
            try
            {
                conn = HTTP.get(new URL(
                        ((XSmilesDocumentImpl) this.ownerDocument)
                                .getXMLDocument().getXMLURL(), style_filename),
                        null);
                reader = new BufferedReader(new InputStreamReader(conn
                        .getInputStream()));
            } catch (MalformedURLException e)
            {
                Log.error(e.getMessage());
                return null;
            } catch (Exception e)
            {
                Log.error(e.getMessage());
                return null;
            }
        } else
        {
            reader = new StringReader(this.getFirstChild().getNodeValue());
        }
        return new InputSource(reader);
    }
}
