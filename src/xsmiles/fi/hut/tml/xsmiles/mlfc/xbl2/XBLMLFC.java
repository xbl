/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.xbl2;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.*;
import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.util.ArrayList;

/**
 * This class represents XBLMLFC that creates all the XBL specific elements.
 * When the XML parser notices that there are XBL elements in the XML file, the
 * XML parser creates an XBLMLFC and calls the method createElementNS for each
 * XBL element inside the host document and thus creates the XBL DOM.
 * 
 * @author Mikko Vestola
 * 
 */
public class XBLMLFC extends MLFC
{

    /*
     * DocumentImpl for XSmiles, this is the host document of the XBL subtree.
     */
    DocumentImpl xsmilesDoc = null;

    /**
     * Constructor. Creates the XBLMLFC.
     */
    public XBLMLFC()
    {
        Log.debug("XBLMLFC created");
    }

    /**
     * This method creates the XBL DOM elements and returns them. It is called
     * by the XML parser for each XBL element that is inside the document.
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag)
    {
        if (xsmilesDoc == null)
            xsmilesDoc = doc;

        Element element = null;
        Log.debug("XBL element: " + tag);

        // Tag name must be other than null
        if (tag == null)
        {
            Log.error("Invalid null tag name!");
            return null;
        }

        // Get shorter form of the tag with namespace prefix
        String localname = getLocalname(tag);

        // Create a new XBL 2.0 DOM element correspondig the tag name
        if (localname.equals("binding"))
        {
            element = new BindingElementImpl(doc, ns, tag);
        } else if (localname.equals("content"))
        {
            element = new ContentElementImpl(doc, ns, tag);
        } else if (localname.equals("div"))
        {
            element = new DivElementImpl(doc, ns, tag);
        } else if (localname.equals("handler"))
        {
            element = new HandlerElementImpl(doc, ns, tag);
        } else if (localname.equals("handlers"))
        {
            element = new HandlersElementImpl(doc, ns, tag);
        } else if (localname.equals("implementation"))
        {
            element = new ImplementationElementImpl(doc, ns, tag);
        } else if (localname.equals("inherited"))
        {
            element = new InheritedElementImpl(doc, ns, tag);
        } else if (localname.equals("prefetch"))
        {
            element = new PrefetchElementImpl(doc, ns, tag);
        } else if (localname.equals("resources"))
        {
            element = new ResourcesElementImpl(doc, ns, tag);
        } else if (localname.equals("script"))
        {
            element = new ScriptElementImpl(doc, ns, tag);
        } else if (localname.equals("style"))
        {
            element = new StyleElementImpl(doc, ns, tag);
        } else if (localname.equals("template"))
        {
            element = new TemplateElementImpl(doc, ns, tag);
        } else if (localname.equals("xbl"))
        {
            element = new XBLElementImpl(doc, ns, tag);
        }

        // Other elements are not valid
        if (element == null)
            Log.debug("XBLMLFC didn't understand element: " + tag);

        return element;

    }

    /**
     * This method starts the XBL "presentation".
     */
    public void start()
    {
        Log.debug("XBLMLFC started");
    }

    /**
     * Stops the MLFC. For now, doesn't do anything interesting.
     */
    public void stop()
    {
        Log.debug("XBLMLFC stopped");
    }

}
