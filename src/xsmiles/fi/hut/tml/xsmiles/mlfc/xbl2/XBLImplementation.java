package fi.hut.tml.xsmiles.mlfc.xbl2;

public interface XBLImplementation
{
    /**
     * The xblBindingAttached() method is called by the user agent after the
     * binding has been attached
     */
    public void xblBindingAttached();

    /**
     * The xblEnteredDocument() method is called by the user agent in two cases:
     * 1) When the bound element, or one of its ancestors, or one of the
     * elements in a higher shadow scope, is inserted into the document 2) When
     * the binding is originally attached, if the bound element is already in
     * the document.
     * 
     */
    public void xblEnteredDocument();

    /**
     * The xblLeftDocument() method is called by the user agent when the bound
     * element, or one of its ancestors, or one of the elements in a higher
     * shadow scope, is removed from the document. (See: handling insertion and
     * removal from the document.)
     */
    public void xblLeftDocument();
}