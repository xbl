/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jul 12, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import org.apache.xerces.dom.ChildNode;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * XBL-element: resources
 * 
 * @author Mikko Vestola
 * @author Juho Rutila
 */
public class ResourcesElementImpl extends ElementImpl
{
    public ResourcesElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }
    /**
     * Applies all the child style-elements.
     * @param element To what element styles are applied
     */

    public void apply(XSmilesElementImpl element)
    {
        Log.debug("applying ResourcesElementImpl");
        Node iter = (Node) this.firstChild;
        while (iter != null)
        {
            if (iter instanceof StyleElementImpl)
            {
                ((StyleElementImpl) iter).apply(element);
            }
            iter = iter.getNextSibling();
        }
    }
}
