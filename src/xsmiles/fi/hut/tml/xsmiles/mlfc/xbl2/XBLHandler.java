package fi.hut.tml.xsmiles.mlfc.xbl2;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.w3c.dom.Node;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;
import org.apache.xerces.dom.TextImpl;
import org.apache.xerces.dom.events.MutationEventImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.HandlerElementImpl;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.dom.EventImpl;
import fi.hut.tml.xsmiles.dom.UIEventImpl;

/**
 * This class is used for message passing between handler elements in xbl trees
 * and ScriptHandler.
 * 
 * @author Juho Rutila
 * @author Juho Vuohelainen
 * @author Mikko Vestola
 * 
 */
public class XBLHandler
{

    private String event = null;

    private XSmilesElementImpl element = null;

    private EventListener eventListener;

    private HandlerElementImpl handlerElem;

    public void setHandlerElementImpl(HandlerElementImpl h)
    {
        this.handlerElem = h;
    }

    public String getScript()
    {
        
        /* Get the script from the handler element so that the
         * script can be dynamically modified.
         */ 
        Node it = this.handlerElem.getFirstChild();
        while (it != null)
        {
            if (it instanceof TextImpl)
            {
                return ((TextImpl) it).getData();
            }
            it = it.getNextSibling();
        }
        
        return "";
    }

    public void setEvent(String e)
    {
        this.event = e;
    }

    public String getEvent()
    {
        return this.event;
    }

    public void setElement(XSmilesElementImpl e)
    {
        this.element = e;
    }

    public XSmilesElementImpl getElement()
    {
        return this.element;
    }

    public EventListener getEventListener()
    {
        return this.eventListener;
    }
    public HandlerElementImpl getHandlerElement()
    {
    	return this.handlerElem; 
    }

    /**
     * Sets an EventListener for listening events on this.element. This is to be
     * called by ScriptHandler but is included in XBLHandler in order to keep
     * all information about scripts in one place.
     * 
     * @param listener EventListener to listen to events in this.element
     */
    public void setEventListener(EventListener listener)
    {
        if (handlerElem.hasAttribute("phase")
                && handlerElem.getAttribute("phase").equals("capture"))
            element.addEventListener(this.event, listener, true);
        		
        else
            element.addEventListener(this.event, listener, false);
        if (handlerElem.hasAttribute("phase")
        		&& handlerElem.getAttribute("phase").equals("default-action"))
        	((XSmilesElementImpl)handlerElem.getOwnerDocument().getDocumentElement()).addEventListener(this.event, listener, false); 
        eventListener = listener;
    }
        
    /**
     * Removes the event listener that was set for the element
     * with the method setEventListener().
     * @see #setEventListener(EventListener)
     * @author Mikko Vestola
     */
    public void removeEventListener() {
        
        Log.debug("Removing event listener from: "+this.element);
        
        /* If the phase was capture, we need to know that when
         * removing the event listener from the element.
         */
        boolean useCapture = false;
        
        if (handlerElem.hasAttribute("phase")
                && handlerElem.getAttribute("phase").equals("capture"))
            useCapture = true;
        
        this.element.removeEventListener(this.event, this.eventListener, useCapture);
        this.eventListener = null;        
        
    }

    /**
     * This method filters the given event. It checks various parameters of the
     * event (like click-count, button etc.) and based on that, it returns
     * boolean value representing that should the script of this XBLHandler be
     * executed (so was the event that kind of event that should fire the script
     * of this XBLHandler).
     * 
     * @param e Event that this XBLHandler listens for.
     * @return True, if the script should be executed. False, if the script must
     *         not be executed.
     * @author Mikko Vestola
     */
    public boolean filter(Event e)
    {

        /*
         * Check that event type matches the event this handler is set to handle
         * (shouldn't be possible to be some other event, but just in case).
         */
        if (!e.getType().equals(this.handlerElem.getEventName()))
            return false;

 
        /* TODO:: the trusted filter */

        /* All the events are not necessary EventImpls (e.g. MutationEventImpls).
         * Thus check for ClassCastException. */
        try {
            /* Check if the phase of this event is accepted */
            if (!this.handlerElem.acceptsPhase(((EventImpl)e).getDefaultPhase()? 2019716164 :  (int) e.getEventPhase()))
                return false;
        } catch (ClassCastException ex) {
            Log.debug("Default phase cannot be checked! Event not an EventImpl!");
            if (!this.handlerElem.acceptsPhase((int) e.getEventPhase()))
                return false;
        }

        // Check filters for mouse events
        if (e instanceof MouseEventImpl)
        {

            MouseEventImpl me = ((MouseEventImpl) e);

            Log.debug("MOUSE EVENT. Clicks: " + me.getDetail() + ", button: "
                    + me.getButton() + ", control: " + me.getCtrlKey()
                    + ", shift: " + me.getShiftKey() + ", alt: "
                    + me.getAltKey() + ", meta: " + me.getMetaKey());

            // Get events button
            short button = me.getButton();

            // Compare to handler elements button filter
            if (!this.handlerElem.acceptsButton(button))
                return false;

            // Check modifiers
            if (!this.handlerElem.acceptsModifiers(me.getCtrlKey(), me
                    .getShiftKey(), me.getAltKey(), me.getMetaKey()))
                return false;

            // Check click-count
            if (!this.handlerElem.acceptsClickCount(me.getDetail()))
                return false;

        }

        // Check filters for mutation events
        if (e instanceof MutationEventImpl)
        {

            MutationEventImpl mutEvt = ((MutationEventImpl) e);

            // Get event's attributes
            String attrName = mutEvt.getAttrName();
            String prevValue = mutEvt.getPrevValue();
            String newValue = mutEvt.getNewValue();
            short attrChange = mutEvt.getAttrChange();

            Log.debug("MUTATION EVENT. Attr-name: " + attrName
                    + ", prev-value: " + prevValue + ", new-value: " + newValue
                    + ", attrChange: " + attrChange);

            // Check attr-change
            if (!this.handlerElem.acceptsAttrChange(attrChange))
                return false;

            // Check attr-name
            if (!this.handlerElem.acceptsAttrName(attrName))
                return false;

            // Check new-value
            if (!this.handlerElem.acceptsNewValue(newValue))
                return false;

            // Check prev-value
            if (!this.handlerElem.acceptsPrevValue(prevValue))
                return false;

        }

        /*
         * If the propagate attribute is set to stop, call the stopPropagation
         * fot the event. According to the W3C Spec, the stop propagate
         * attribute has no effect, if the phase attribute is set to
         * default-action.Propagation must only be stopped if all the other filters 
         * pass.
         */
        if (!this.handlerElem.propagate())
        {
            if (!(this.handlerElem.hasAttribute("phase") && this.handlerElem
                    .getAttribute("phase").equals("default-action")))
                e.stopPropagation();
        }
        
        /* Check if the default action should be prevented
         * The default action must only be prevented if all the other filters pass.
         */
        if (!this.handlerElem.acceptsDefaultAction())
            e.preventDefault();


        // If we get here, the script is ready to be executed
        return true;

    }

}
