/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Feb 27, 2006
 *
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Node;
import org.w3c.css.sac.SelectorList;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectorParser;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectors;
import java.util.Vector;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * XBL-element: content
 *
 * This class represent Content XBL-element.
 * 
 * @author Mikko Vestola
 * 
 * @author Jukka Julku
 * 
 */
public class ContentElementImpl extends ElementImpl
{
    private Vector assignedElements = null;

    public ContentElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

    /**
     * Assign element to this node.
     * 
     * @param element Element to be assigned
     * 
     * @return true Element was succesfully assigned.
     * @return false Element was not assigned.
     */

    public boolean assignElement(Node element)
    {
        if (this.hasAttribute("locked"))
        {
            if (this.getAttribute("locked").equals("true"))
            {
                return false;
            }
        }

        if (element != null)
        {
            if (assignedElements == null)
            {
                assignedElements = new Vector();
            }

            boolean b = assignedElements.add(element);
            if (b == true)
            {
                if (element instanceof XSmilesElementImpl)
                {
                    if (this.hasAttribute("apply-binding-sheets"))
                    {
                        ((XSmilesElementImpl) element)
                                .setApplyBindingSheets(this.getAttribute(
                                        "apply-binding-sheets").equals("true"));
                    } else
                    {
                        ((XSmilesElementImpl) element)
                                .setApplyBindingSheets(false);
                    }

                }
            }
            return b;

        }
        return false;
    }

    /**
     * Unassign the given element from this node.
     * 
     * @param element Element to be unassigned.
     * 
     * @return true Element was unassigned.
     * @return false Element was not unassigned.
     */

    public boolean unassignElement(Node element, boolean force)
    {
        if (force == false)
        {
            if (this.hasAttribute("locked"))
            {
                if (this.getAttribute("locked").equals("true")
                        && this.match(element))
                {
                    return false;
                }
            }
        }

        if (element != null)
        {
            if (assignedElements != null)
            {
                boolean b = assignedElements.remove(element);
                if (b == true && element instanceof XSmilesElementImpl)
                {
                    ((XSmilesElementImpl) element).setApplyBindingSheets(true);
                }
                return b;
            }
        }
        return false;
    }

    /**
     * Tries to match the given element to the node.
     * 
     * Element matches a content node if: Content node does not have includes
     * attribute or the includes attribute is valid selector that matches the
     * element.
     * 
     * Content element does not match any element if it is locked.
     * 
     * @param child Element to match.
     * 
     * @return true Element matches this content.
     * @return false Element does not match this content.
     */

    public boolean matchesElement(Node child)
    {
        if (this.hasAttribute("locked"))
        {
            if (this.getAttribute("locked").equals("true"))
            {
                return false;
            }
        }
        return match(child);
    }

    /**
     * Tries to match the given element to the node without taking locked
     * attribute in account.
     * 
     * Element matches a content node if: Content node does not have include
     * attribute or the include attribute is a valid selector that matches the
     * element.
     * 
     * @param child element to match.
     * 
     * @return true element matches this content.
     * @return false element does not match this content.
     */

    private boolean match(Node child)
    {
        if (this.hasAttribute("includes"))
        {
            if (child instanceof XSmilesElementImpl)
            {
                SelectorList selectors = CSSSelectorParser.parseSelectors(this
                        .getAttribute("includes"));
                CSSSelectors sel = new CSSSelectors();

                for (int i = 0; i < selectors.getLength(); i++)
                {
                    if (sel.selectorMatchesElem(selectors.item(i),
                            (XSmilesElementImpl) child))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    /** Returns the elements that should replace this node.
     * 
     * @return java.util.Vector containing the elements.
     */

    public Vector getElementsToAssign()
    {
        if (assignedElements != null)
        {
            return this.assignedElements;
        }
        Vector v = new Vector();

        Node child = this.getFirstChild();
        while (child != null)
        {
            Node n = this.removeChild(child);
            v.addElement(n);
            child = this.getFirstChild();
        }

        return v;
    }

    /** Returns the elements that are assigned to this node.
     * 
     * @return java.util.Vector containing all the elements assigned to this
     *         element so far.
     */

    public Vector getAssignedElements()
    {
        if (assignedElements != null)
        {
            return this.assignedElements;
        }
        return new Vector();
    }

}
