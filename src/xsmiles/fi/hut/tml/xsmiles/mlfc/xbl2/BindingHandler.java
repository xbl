/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Feb 27, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2;

import fi.hut.tml.xsmiles.mlfc.xbl2.dom.*;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.dom.StylableElement;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.awt.Container;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * @author Juho Vuohelainen
 * @author Jukka Julku
 * @author Mikko Vestola
 * 
 */
public class BindingHandler
{

    private LinkedList bindingElementImpls;

    private LinkedList xSmilesElementImpls;

    private LinkedList shadowContents;

    private XSmilesDocumentImpl doc;

    /*
     * Here is stored a list of all binding elements (inline and external) that
     * are loaded in this document (maybe not bound to any element).
     */
    private ArrayList allBindingElements;

    /*
     * Here is saved all the external binding documents loaded with the
     * processing instruction
     */
    private Vector bindingDocumentsPI;

    /**
     * Default constructor
     */
    public BindingHandler(XSmilesDocumentImpl doc)
    {

        // The document that the bindings will be applied to
        this.doc = doc;

        /*
         * LinkedList of Vectors. The first element of each Vector is an
         * BindingElementImpl object and the rest are XSmilesElementImpl-
         * objects bound to that BindingElementImpl object.
         */
        bindingElementImpls = new LinkedList();

        /*
         * LinkedList of Vectors. The first element of each Vector is an
         * XSmilesElementImpl object and the rest are BindingElementImpl-
         * objects bound to that XSmilesElementImpl object.
         */
        xSmilesElementImpls = new LinkedList();

        /*
         * LinkedList of vectors. The first element of each Vector is an
         * XSmilesElementImpl object and the rest are TemplateElementImpl-
         * objects representing the shadown content of the bound element.
         * 
         */
        shadowContents = new LinkedList();
        
        /*
         * Initialize empty arraylist where we save all the bindings in this
         * document (inline bindings and external bindings presented in external
         * binding documents).
         */
        this.allBindingElements = new ArrayList();

    }

    /**
     * Returns all the binding elements in a list loaded in this document.
     * 
     * @return List containing all bindings loaded in the document. Every
     *         binding may not be bound to an element. The order of the bindings
     *         are that in which order the appeared in the document (first the
     *         bindings imported with the PI and then inline bindings).
     * @author Mikko Vestola
     */
    public ArrayList getAllBindings()
    {
        return this.allBindingElements;
    }

    /**
     * Returns all the external binding documents loaded with the processing
     * instruction.
     * 
     * @return Vector containing external XML documents
     * @author Mikko Vestola
     */
    public Vector getBindingDocumentsPI()
    {
        return this.bindingDocumentsPI;
    }

    /**
     * Adds an XML document to the PI binding documents.
     * 
     * @param doc XML document to be added
     * @see #getBindingDocumentsPI()
     * @author Mikko Vestola
     */
    public void addBindingDocumentPI(XMLDocument doc)
    {
        if (this.bindingDocumentsPI == null)
            this.bindingDocumentsPI = new Vector();
        this.bindingDocumentsPI.add(doc);
        Log.debug("Added binding document: " + doc.getXMLURL());
    }

    /**
     * Starts the bindinghandler. Finds the elements that the bindings will be
     * bound to and finally applies the bindings in the right order.
     * 
     * @author Mikko Vestola
     */
    public void start()
    {

        if (this.bindingDocumentsPI != null)
        {

            /*
             * For each external binding document, get all binding elements
             * presented in those documents and put them in the bindings array.
             * External bindings MUST BE added to the array before the inline
             * bindings because they are loaded with PI.
             */
            for (int i = 0; i < this.bindingDocumentsPI.size(); i++)
            {

                // Get the document and get all bindings
                XMLDocument doc = (XMLDocument) this.bindingDocumentsPI.get(i);
                NodeList externalBindings = doc.getDocument()
                        .getElementsByTagNameNS("http://www.w3.org/ns/xbl",
                                "binding");

                for (int j = 0; j < externalBindings.getLength(); j++)
                {
                    this.addBindingToDocument(externalBindings.item(j), true);
                }

            }
        }

        // Get all inline binding elements in the host document
        NodeList inlineBindings = this.doc.getElementsByTagNameNS(
                "http://www.w3.org/ns/xbl", "binding");

        // Add all inline bindings to the bindings array
        for (int i = 0; i < inlineBindings.getLength(); i++)
        {
            this.addBindingToDocument(inlineBindings.item(i), false);
        }

        /*
         * Start adding the bindings to the elements. First start from the root
         * element of the host document.
         */
        if (this.allBindingElements.size() > 0)
            this
                    .addBindings((XSmilesElementImpl) this.doc
                            .getDocumentElement());

        /*
         * All bindings have been added, call the bindinghandler to apply them.
         */
        if (this.allBindingElements.size() > 0)
            this.applyBindings();

    }

    /**
     * This method adds binding dynamically to an element. It is supposed to be
     * called after the document has been loaded and all the binding elements in
     * the document have been loaded.
     * 
     * @param bindingURI The binding URI (absolute or relative) that is going to
     *            be bound to the element.
     * @param elem The element that the binding is going to be bound to.
     * @return Returns the binding element if the URI is a correct reference to
     *         a binding element. Null, it the URI does not refer to any binding
     *         element.
     * @author Mikko Vestola
     */
    public BindingElementImpl addBindingDynamically(String bindingURI, XSmilesElementImpl elem)
    {

        Log.debug("Adding binding dynamically: " + bindingURI);

        BindingElementImpl binding = null;

        // Resolve URI to absolute URI
        try
        {
            bindingURI = elem.resolveURI(bindingURI).toString();
        } catch (MalformedURLException e)
        {
            Log.error("Error resolving binding URI!");
            return null;
        }

        // Search if the binding is already loaded in the document
        for (int i = 0; i < this.getAllBindings().size(); i++)
        {
            BindingElementImpl b = (BindingElementImpl) this.getAllBindings()
                    .get(i);

            if (b.matchesElementURI(bindingURI))
            {
                binding = b;
                break;
            }
        }

        // If not found, binding might not be loaded, so try to load it
        if (binding == null)
            binding = this.searchExternalBinding(bindingURI);

        if (binding != null)
        {
            this.addExtendingBindings(binding, null, elem);
            this.applyBindings();
        }

        return binding;
    }

    /**
     * Adds a binding element to the document's list of all bindings.
     * 
     * @param node The binding element
     * @param isExternal Is the binding element an external binding (true) or is
     *            it inline binding presented in the document (false).
     * @author Mikko Vestola
     */
    private BindingElementImpl addBindingToDocument(Node node, boolean isExternal)
    {

        /*
         * Always check that the elelement is a binding element. Also check that
         * the binding element's parent node is an xbl element and that the xbl
         * element does not have an xbl element as its parent node (what IS NOT
         * allowed).
         */

        Node parent;
        Node parentsParent;

        if (node instanceof BindingElementImpl
                && (parent = node.getParentNode()) != null
                && parent instanceof XBLElementImpl
                && ((parentsParent = parent.getParentNode()) == null || !(parentsParent instanceof XBLElementImpl)))
        {

            // Get the URI of the document where this binding is
            String URI = node.getBaseURI();

            /*
             * If the binding is not presented in this document, import the
             * binding element to this document, and also its child elements
             * (otherwise causes DOMException).
             */
            if (isExternal)
                node = this.doc.importNode(node, true);

            // Add to list
            this.allBindingElements.add(node);

            // Set bindinghandler
            ((BindingElementImpl) node).setBindingHandler(this);

            ((BindingElementImpl) node).setOrigBaseURI(URI);

            return (BindingElementImpl) node;

        } else
            return null;

    }

    /**
     * This function bounds the bindings in the document to the element given in
     * parameters. Only those bindings will be bound that refer to the given
     * element (binding's element attribute). Continues recursively to element's
     * child elements and bounds all the referring bindings also to these
     * elements.
     * 
     * @param element An element that the bindings in the document should be
     *            bound to.
     * @author Mikko Vestola
     */
    private void addBindings(XSmilesElementImpl element)
    {

        /*
         * Check every binding element that does their element attribute match
         * the element.
         */
        for (int i = 0; i < this.getAllBindings().size(); i++)
        {
            BindingElementImpl binding = null;

            if (this.getAllBindings().get(i) instanceof BindingElementImpl)
                binding = (BindingElementImpl) this.getAllBindings().get(i);

            // If the element matches the binding's element selector
            if (binding != null && binding.bindingMatchesElement(element))
            {

                /*
                 * Bound the binding to the element. The method checks that does
                 * the binding inherit other bindings that must be first be
                 * bound to the element.
                 */
                this.addExtendingBindings(binding, null, element);

            }

        }

        /*
         * Continue searching child elements so we can find all elements in the
         * document that match the given selector and add bindings also to these
         * elements.
         */

        Node elem = element.getFirstChild();
        while (elem != null)
        {
            if (elem instanceof XSmilesElementImpl)
                addBindings((XSmilesElementImpl) elem);
            elem = elem.getNextSibling();

        }

    }

    /**
     * This method bounds the given binding and the bindings it extends from
     * (explicit inheritance chain) to the given element in the right order (the
     * base binding of the explicit inheritance chain is bound first). The
     * method checks that does the binding inherit other bindings that must be
     * first bound to the element and calls the addBinging() method in the
     * BindingHandler class to all these bindings.
     * 
     * @param binding The binding element that will be bound to the element
     * @param chain The chain (ArrayList) of bindings that are already in the
     *            explicit inheritance chain of the binding. Set to null when
     *            first calling this method.
     * @param element The element to which the binding will been bound to
     * @see BindingHandler#addBinding(BindingElementImpl, XSmilesElementImpl)
     * @author Mikko Vestola
     */
    public void addExtendingBindings(BindingElementImpl binding, ArrayList chain, XSmilesElementImpl element)
    {
        /* Check that does the binding inherit from other bindings */
        if (binding.hasAttribute("extends"))
        {

            // Initialize the chain
            if (chain == null)
                chain = new ArrayList();

            // Add this binding to the explicit inheritance chain
            chain.add(binding);

            // Get the binding that this binding inherits from
            BindingElementImpl extending = this.getExtendingBinding(binding);

            // If there is an extending binding element
            if (extending != null)
            {
                /*
                 * If the extending binding is already in the explicit
                 * inheritance chain (so it might be the the same binding that
                 * started the explicit inheritance chain), stop here (because
                 * otherwise it will form a loop of inheritance, like A extends
                 * B, B extends C and C extends A). Otherwise call the method
                 * again with the updated chain.
                 */
                if (!chain.contains(extending))
                    this.addExtendingBindings(extending, chain, element);
            }

        }

        // Finally add the binding to the element
        this.addBinding(binding, element);
        Log.debug(binding + " bound to element: " + element.getLocalName());

    }

    /**
     * This method finds the binding that the binding given in parameters
     * extends from and returns it. So in other words, if binding A has
     * attribute extends="#B", the method returns the binding element which has
     * id="B" (if any).
     * 
     * @param binding The binding element that has an extends attribute.
     * @return Returns the extending binding element or null if the extending
     *         binding is not found or the binding does not extend from any
     *         other binding.
     * @author Mikko Vestola
     */
    private BindingElementImpl getExtendingBinding(BindingElementImpl binding)
    {

        if (!binding.hasAttribute("extends"))
            return null;

        BindingElementImpl b = null;

        for (int i = 0; i < this.getAllBindings().size(); i++)
        {

            if (this.getAllBindings().get(i) instanceof BindingElementImpl)
                b = (BindingElementImpl) this.getAllBindings().get(i);

            /*
             * Find out that does the URI, presented in the binding's extends
             * attribute, refer to the binding b (if it refers, b is the
             * extending binding).
             */

            try
            {

                if (b != null
                        && b.matchesElementURI(binding.resolveURI(
                                binding.getAttribute("extends")).toString()))
                    return b;
            } catch (MalformedURLException e)
            {
                Log.error("Error when finding extending binding with id: "
                        + binding.getAttribute("extends"));
            }

        }

        /*
         * If we get here, the extending binding is not found from the bindings
         * loaded in this document. It is maybe an external binding that is not
         * loaded (or maybe a reference that is in error). So try to load it.
         */
        try
        {
            b = this.searchExternalBinding(binding.resolveURI(
                    binding.getAttribute("extends")).toString());
        } catch (MalformedURLException e)
        {
            Log.error("Error when finding extending binding with id: "
                    + binding.getAttribute("extends"));
        }

        return b;

    }

    /**
     * This method searches binding element from external documents not loaded
     * with PI and returns the binding. NOTE: this method is intended only for
     * searching bindings that are not imported to the document with inline XBL
     * or with processing instruction.
     * 
     * @param URI The absolute URI of the binding. E.g.
     *            http://www.site.com/bindings.xml#bindingID1 Or the URI might
     *            also refer to the binding document itself, e.g.
     *            http://www.site.com/bindings.xml.
     * @return Returns the binding element that this URI refers. If the URI
     *         refers only to the document itself, the first binding of the
     *         document is returned (if any). If the binding is not found from
     *         the external documents, or if the binding refers to this document
     *         or some other document that is imported to this document with PI,
     *         returns null.
     * @author Mikko Vestola
     */
    private BindingElementImpl searchExternalBinding(String URI)
    {

        String[] splittedURI = this.parseExternalBindingURI(URI);
        String documentURI = splittedURI[0];
        String bindingID = splittedURI[1];
        Log.debug("Document: " + documentURI + ", Binding ID: " + bindingID);

        /*
         * This means that the URI is referring only to the document, so we load
         * the first binding of this document.
         */
        boolean getFirstBinding = false;

        if (bindingID.equals(""))
            getFirstBinding = true;

        /*
         * The binding is referring to a document that is not imported to this
         * document. Load the document first.
         */
        XMLDocument extDoc = this.loadExternalBindingDocument(documentURI);

        if (extDoc == null)
            return null;

        Element elem;

        /*
         * If the binding URI does not refer to a specific binding but only to
         * the document, just load the first binding element of the document if
         * available.
         * 
         * Otherwise get the binding element by its ID.
         */
        if (getFirstBinding)
        {
            NodeList bindings = extDoc.getDocument().getElementsByTagNameNS(
                    "http://www.w3.org/ns/xbl", "binding");

            // Set elem to the first binding
            if (bindings != null && bindings.getLength() >= 1)
                elem = (Element) bindings.item(0);
            else
                elem = null;

        } else
            elem = extDoc.getDocument().getElementById(bindingID);

        /*
         * Add the binding to the binding list and return the binding element if
         * found.
         */
        if (elem != null && elem instanceof BindingElementImpl)
        {
            return this.addBindingToDocument(elem, true);
        }

        // The binding has not been found
        return null;

    }

    /**
     * This method parses the external binding URI to binding document URI and
     * binding element ID.
     * 
     * @param URI The absolute URI of the binding. E.g.
     *            http://www.site.com/bindings.xml#bindingID1.
     * @return An array containing two strings. Both can be empty strings. E.g
     *         ["http://www.site.com/bindings.xml", "bindingID"]
     * @author Mikko Vestola
     */
    private String[] parseExternalBindingURI(String URI)
    {

        String[] array = new String[2];
        array[0] = "";
        array[1] = "";

        StringTokenizer st = new StringTokenizer(URI, "#");
        int i = 0;
        while (st.hasMoreTokens())
        {
            String s = st.nextToken();
            array[i] = s;
            i++;
        }

        return array;
    }

    /**
     * This loads an external binding document and returns it.
     * 
     * @param documentURI Binding document to be loaded
     * @return XMLDocument, null if the document could not be loaded.
     * @see BindingHandler#addBindingDocumentFetched(XMLDocument)
     * @author Mikko Vestola
     */
    private XMLDocument loadExternalBindingDocument(String documentURI)
    {

        try
        {
            /*
             * Load the document and get the XMLDocument from the handler.
             */
            XLink xblDoc = new XLink(this.doc.resolveURI(documentURI));
            XSmilesContentHandler handler = this.doc.getHostMLFC()
                    .getMLFCListener().createContentHandler(xblDoc,
                            new Container(), false);
            handler.play();
            XMLDocument doc = handler.getXMLDocument();

            return doc;

        } catch (Exception e)
        {

        }

        // If some error loading the document
        return null;

    }

    /**
     * Add a binding between BindingElementImpl be and XSmilesElementImpl xe.
     * Does nothing if either one is null.
     * 
     * @param be
     * @param xe
     */
    public void addBinding(BindingElementImpl be, XSmilesElementImpl xe)
    {

        if (be == null || xe == null)
            return;
        Log.debug("BindingHandler: adding binding: " + be + " bound to " + xe);

        Iterator beIter = bindingElementImpls.iterator();
        Iterator xeIter = xSmilesElementImpls.iterator();
        Iterator scIter = shadowContents.iterator();

        boolean found = false;

        // Check if 'be' already has at least one binding
        while (beIter.hasNext() && !found)
        {
            Vector beBindings = (Vector) beIter.next();

            // Add 'xe' to the list (Vector)
            if (beBindings.get(0).equals(be))
            {
                beBindings.add(xe);
                found = true;
            }
        }

        // 'be' hasn't been bound to anything yet
        if (!found)
        {
            // Create a new Vector for bindings and add 'xe' to it
            Vector beBindings = new Vector();
            beBindings.add(be);
            beBindings.add(xe);
            bindingElementImpls.add(beBindings);
        }

        found = false;

        // Check if 'xe' already has at least one binding
        while (xeIter.hasNext() && !found)
        {
            Vector xeBindings = (Vector) xeIter.next();

            // Add 'be' to the list (Vector)
            if (xeBindings.get(0).equals(xe))
            {
                xeBindings.add(be);
                found = true;

                // add new shadow template
                while (scIter.hasNext())
                {
                    Vector sc = (Vector) scIter.next();

                    if (sc.get(0).equals(xe))
                    {
                        TemplateElementImpl t = getTemplate(be);

                        if (t != null)
                        {
                            sc.add(t.createShadowContent(xe));
                        } else
                        {
                            // sc.add(null);
                        }
                        redistributeChildren(sc, xe);
                        break;
                    }
                }
            }
        }

        // 'xe' hasn't been bound to anything yet
        if (!found)
        {
            // Create a new Vector for bindings and add 'xe' to it
            Vector xeBindings = new Vector();
            xeBindings.add(xe);
            xeBindings.add(be);
            xSmilesElementImpls.add(xeBindings);

            // add new shadow content
            Vector sc = new Vector();

            sc.add(xe);

            TemplateElementImpl t = getTemplate(be);

            if (t != null)
            {
                sc.add(t.createShadowContent(xe));
            } else
            {
                // sc.add(null);
            }
            shadowContents.add(sc);

            redistributeChildren(sc, xe);
        }
    }

    /**
     * This method (re)distributes the original child elements of the bound
     * element into shadow trees bound to it.
     * 
     * @note This method expects that the shadowTrees Vector still has the
     *       boundElement as its first element.
     * 
     * @param shadowTrees Vector of shadow content templates in inheritance
     *            order
     * @param boundElement Element to which the shadow trees are bound to
     */

    private void redistributeChildren(Vector shadowTrees, XSmilesElementImpl boundElement)
    {
        if (shadowTrees == null || shadowTrees.size() == 0)
            return;

        // Unassign the childs elements first if possible
        TemplateElementImpl temp = null;
        for (int i = shadowTrees.size() - 1; i > 0; i--)
        {
            if (shadowTrees.elementAt(i) != null)
            {
                temp = (TemplateElementImpl) shadowTrees.elementAt(i);
                temp.unassignChildren(boundElement, temp, false);
            }
        }

        // Find a new content element for the childs
        for (int i = shadowTrees.size() - 1; i > 0; i--)
        {
            if (shadowTrees.elementAt(i) != null)
            {
                temp = (TemplateElementImpl) shadowTrees.elementAt(i);
                temp.distributeChildren(boundElement);
            }
        }
    }

    /**
     * This method creates the final flattened tree out of the shadow tree and
     * attaches it into the bound element.
     * 
     * @param shadowTrees Vector of shadow content templates in inheritance
     *            order
     * @param boundElement Element to which the shadow trees are bound to
     */

    private void createFinalFlattenedTree(Vector shadowTrees, XSmilesElementImpl boundElement)
    {
        int first = 0;
        int last = 0;

        if (shadowTrees == null || shadowTrees.size() == 0)
            return;

        // Find the index of the last template
        for (int i = 0; i < shadowTrees.size(); i++)
        {
            if (shadowTrees.elementAt(i) != null)
            {
                last = i;
                break;
            }
        }

        // Find the index of the first template
        for (int i = shadowTrees.size() - 1; i >= 0; i--)
        {
            if (shadowTrees.elementAt(i) != null)
            {
                first = i;
                break;
            }
        }

        // if there is templates
        if (first >= 0)
        {
            // iteratively construct final flattened tree from the shadow
            // templates
            for (int i = first; i >= last; i--)
            {

                if (shadowTrees.elementAt(i) != null)
                {
                    TemplateElementImpl temp = (TemplateElementImpl) shadowTrees
                            .elementAt(i);
                    if (i == first)
                    {
                        if (first == last)
                        {
                            temp.createFinalFlattenedTree(boundElement, true,
                                    true);
                        } else
                        {
                            temp.createFinalFlattenedTree(boundElement, true,
                                    false);
                        }
                    } else if (i == last)
                    {
                        temp
                                .createFinalFlattenedTree(boundElement, false,
                                        true);
                    } else
                    {
                        temp.createFinalFlattenedTree(boundElement, false,
                                false);
                    }
                }
            }
        }
    }

    /**
     * Returns the template element of the bindingelement given.
     * 
     * @param b binding element
     * @return TemplateElementImpl, the template element
     * @return null, there was no template under this binding.
     */

    private TemplateElementImpl getTemplate(BindingElementImpl b)
    {
        Node n = b.getFirstChild();
        while (n != null)
        {
            String ns = n.getNamespaceURI();
            String ln = n.getLocalName();
            if (ln != null && ln.equals("template") && ns != null
                    && ns.equals("http://www.w3.org/ns/xbl"))
            {
                return (TemplateElementImpl) n;
            }
            n = n.getNextSibling();
        }
        return null;
    }

    /**
     * Get all shadow content templates for the given element.
     * Modification to the returned Vector do not modify the actual bindings.
     * 
     * @param xe Bound element.
     * @return Vector of shadow content.
     */

    private Vector getShadowContent(XSmilesElementImpl xe)
    {
        Iterator scIter = shadowContents.iterator();
        Vector retVector;

        while (scIter.hasNext())
        {
            Vector sc = (Vector) scIter.next();

            // Add 'sc' to the list (Vector)
            if (sc.get(0).equals(xe))
            {
                retVector = (Vector) sc.clone();
                retVector.remove(0);
                retVector.trimToSize();

                return retVector;
            }
        }

        return null;
    }

    /**
     * Get all bindings for XSmilesElementImpl xe as a Vector
     * BindingHandlerImpls. Modifying the returned Vector does not modify the
     * actual bindings.
     * 
     * @param xe
     * @return
     */
    public Vector getBindings(XSmilesElementImpl xe)
    {
        Iterator xeIter = xSmilesElementImpls.iterator();
        Vector retVector;

        while (xeIter.hasNext())
        {
            Vector xeBindings = (Vector) xeIter.next();

            // Add 'be' to the list (Vector)
            if (xeBindings.get(0).equals(xe))
            {
                retVector = (Vector) xeBindings.clone();
                retVector.remove(0);
                retVector.trimToSize();

                return retVector;
            }
        }

        return null;
    }

    /**
     * Get all bindings for BindingElementImpl be as a Vector of
     * XSmilesElementImpls. Modifying the returned Vector does not modify the
     * actual bindings.
     * 
     * @param be
     * @return
     */
    public Vector getBindings(BindingElementImpl be)
    {
        Iterator beIter = bindingElementImpls.iterator();
        Vector retVector;

        while (beIter.hasNext())
        {
            Vector beBindings = (Vector) beIter.next();

            // Add 'be' to the list (Vector)
            if (beBindings.get(0).equals(be))
            {
                retVector = (Vector) beBindings.clone();
                retVector.remove(0);
                retVector.trimToSize();

                return retVector;
            }
        }

        return null;
    }

    /**
     * Apply bindings for all XSmilesElementImpls in the order they were added.
     */
    public void applyBindings()
    {
        Log.debug("BindingHandler: applyBindings()");
        
        /* Remove all event handlers from the bound elements
         * so applying the bindings doesn't create duplicate event listeners.
         * Every binding element has a ScriptHandler that knows
         * the event handlers in the bound elements and can remove them.
         */ 
        for (int i = 0; i < this.bindingElementImpls.size(); i++)
        {
            BindingElementImpl b = (BindingElementImpl)((Vector)bindingElementImpls.get(i)).get(0);
            HandlersElementImpl handlers = b.getHandlersElement();
            if (handlers!=null)
                handlers.resetScriptHandler();
        }
        
        
        Vector xeBindings;

        // Iterate through all Vectors in the LinkedList
        for (int i = 0; i < xSmilesElementImpls.size(); i++)
        {

            /*
             * Clone the vector because otherwise the remove(0)-call will remove
             * the element from the original vector and this would cause
             * problems if for example the TemplateElementImpl would want to get
             * the bindings bound to some element.
             */
            xeBindings = (Vector) ((Vector) xSmilesElementImpls.get(i)).clone();

            // Remove xe from the beginning of the Vector temporarily so that
            // xeBindings' all elements are BindingElementImpls
            XSmilesElementImpl xe = (XSmilesElementImpl) xeBindings.remove(0);
            applyBindings(xe, xeBindings);
            xeBindings.add(0, xe);
            // break;

            // Create the final flattened tree for this bound element
            Vector v = getShadowContent(xe);
            if (v != null)
            {
                createFinalFlattenedTree(v, xe);
            }
        }
    }

    /**
     * Apply bindings for specified XSmilesElementImpl in the order they were
     * added.
     * 
     * @param xe
     */
    public void applyBindings(XSmilesElementImpl xe)
    {

        Vector xeBindings;

        // Find the Vector whose first element is xe
        for (int i = 0; i < xSmilesElementImpls.size(); i++)
        {

            xeBindings = (Vector) xSmilesElementImpls.get(i);

            if (xeBindings.get(0).equals(xe))
            {
                // Remove xe from the beginning of the Vector temporarily so
                // that
                // xeBindings' all elements are BindingElementImpls
                xeBindings.remove(0);
                applyBindings(xe, xeBindings);
                xeBindings.add(0, xe);
                break;
            }
        }
    }

    /**
     * Apply XSmilesElementImpl binding for all BindingElementImpls in Vector in
     * the order they were added.
     * 
     * @param v
     */
    protected void applyBindings(XSmilesElementImpl xe, Vector v)
    {
        BindingElementImpl be;

        // Iterate through the Vector
        for (int i = 0; i < v.size(); i++)
        {
            be = (BindingElementImpl) v.get(i);

            be.applyBinding(xe);
        }
    }

    private void seekAndDestroy(LinkedList impls, XSmilesElementImpl match, XSmilesElementImpl target)
    {
        Iterator iter = impls.iterator();
        while (iter.hasNext())
        {
            Vector ei = (Vector) iter.next();
            if (ei.get(0).equals(match))
            {
                if (target == null)
                {
                    Log.debug("We found our match");
                    impls.remove(match);
                    break;
                } else
                {
                    Iterator it = ei.iterator();
                    it.next(); // Let's skip the first (match) element ... [0]
                    while (it.hasNext())
                    {
                        XSmilesElementImpl el = (XSmilesElementImpl) it.next();
                        if (el.equals(target))
                        {
                            Log.debug("We found our target " + el);
                            if (!(el instanceof BindingElementImpl))
                                resetBindings(el);
                            ei.remove(el);
                            break;
                        }
                    }
                }
            }
        }
    }

    /*
     * Resets all the changes made by bindings in the element el. - It takes his
     * parent's style - It resets final flattened tree - TODO: reset scripts
     * This method could also be implemented in XSmilesElementImpl.
     */
    private void resetBindings(XSmilesElementImpl el)
    {
        if (el instanceof StylableElement)
            ((StylableElement) el).setStyle(((StylableElement) el
                    .getParentNode()).getStyle());
        if (((XSmilesElementImpl) el).isInitedForReset())
        {
            Log.debug("Let's reset final tree from " + el);
            ((XSmilesElementImpl) el).resetFinalFlattenedTree();
        } else
        {
            Log.debug("ei resetoida");
        }
    }

    public void removeBinding(BindingElementImpl be, XSmilesElementImpl xe)
    {
        Log.debug("Removing bindings from BindingHandler");
        if (be != null && xe != null)
            Log.debug("BindingHandler: removing binding " + be
                    + " that is bound to " + xe);
        if (be != null && xe == null)
            Log.debug("BindingHandler: removing binding " + be
                    + " from all bounds");
        if (be == null && xe != null)
            Log.debug("BindingHandler: removing all bindings from " + xe);

        if (be == null && xe == null)
            return;

        seekAndDestroy(bindingElementImpls, be, xe);
        seekAndDestroy(xSmilesElementImpls, xe, be);
        Iterator scIt = shadowContents.iterator();
        while (scIt.hasNext())
        {
            Vector sc = (Vector) scIt.next();
            if (sc.get(0).equals(xe))
            {
                for (int i = 1; i < sc.size(); i++)
                    if (((TemplateElementImpl) sc.get(i)).clonedFrom() == this
                            .getTemplate(be))
                    {
                    	((TemplateElementImpl)sc.get(i)).unassignChildren(xe, ((TemplateElementImpl)sc.get(i)), true);
                        sc.remove(i);
                    	this.redistributeChildren(sc, xe);
                        break;
                    }
            }
        }       
    }
}
