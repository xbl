/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jul 12, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import java.util.AbstractList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.w3c.dom.Node;
import org.w3c.dom.events.Event;

import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.ScriptHandler;

/**
 * XBL-element: handlers
 * 
 * @author Mikko Vestola
 * @author Juho Rutila
 */

public class HandlersElementImpl extends ElementImpl
{

    private ScriptHandler scriptHandler;

    public HandlersElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

    public void apply(XSmilesElementImpl element)
    {

        if (this.scriptHandler==null)
            this.scriptHandler = new ScriptHandler();
            
        /**
         * Find handler children
         */
        Node iter = this.getFirstChild();

        while (iter != null)
        {
            if (iter instanceof HandlerElementImpl)
            {
                XBLHandler handler = ((HandlerElementImpl) iter)
                        .getXBLHandler();
                handler.setElement(element);

                /*
                 * Check that is the handler associated with an event.
                 */
                if (((HandlerElementImpl) iter).hasAttribute("event"))
                {

                    String eventType = ((HandlerElementImpl) iter)
                            .getAttribute("event");

                    handler.setEvent(eventType);

                    /*
                     * ScriptHandler.addHandler() only takes an XBLHandler as
                     * argument. The XBLHandler should have its element, event
                     * and script attributes set (not eventListener).
                     */
                    this.scriptHandler.addHandler(handler);

                }

            }
            iter = iter.getNextSibling();
        }
    }

    public void setScriptHandler(ScriptHandler sh)
    {
        this.scriptHandler = sh;
    }

    public Object getScriptHandler()
    {
        return this.scriptHandler;
    }
    
    /**
     * Removes all the event listeners from the ScriptHandler of this
     * handlers element and removes the ScriptHandler. 
     */
    public void resetScriptHandler() {
        if (this.scriptHandler!=null)
            this.scriptHandler.removeEventHandlers();
        
       this.scriptHandler = null;       

    }

}
