/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Feb 27, 2006
 *
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import org.apache.xerces.dom.DocumentImpl;

/**
 * This class represents the root element of the XBL document.
 * 
 * @author Mikko Vestola
 */
public class XBLElementImpl extends ElementImpl
{

    public XBLElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

}
