package fi.hut.tml.xsmiles.mlfc.xbl2;

public interface ElementXBL
{
    /**
     * The xblImplementations attribute on all elements must return an instance
     * of an XBLImplementationList object (the same object for the lifetime of
     * the element), which is a live list of the implementation objects provided
     * by the bindings for that bound element at any particular point in time.
     * 
     * @return
     */
    public XBLImplementationList getXblImplementations();

    public void addBinding(String bindingURI);

    public void removeBinding(String bindingURI);

    public boolean hasBinding(String bindingURI);
}
