package fi.hut.tml.xsmiles.mlfc.xbl2;

/**
 * The xblImplementations attribute on all elements must return an instance of
 * an XBLImplementationList object (the same object for the lifetime of the
 * element), which is a live list of the implementation objects provided by the
 * bindings for that bound element at any particular point in time.
 * 
 * The list must be ordered such that the most derived binding is last, and the
 * base binding has index zero.
 * 
 * @author Juho Vuohelainen
 * 
 */
public interface XBLImplementationList
{
    /**
     * The item(n) method must return the nth implementation object associated
     * with the bound element. If the index is not a number between zero and
     * length-1 (inclusive), then the method must raise an INDEX_SIZE_ERR DOM
     * exception.
     * 
     * @return implementation object in the list defined by parameter index
     */
    XBLImplementation item(int index);

    /**
     * The length attribute must return the number of implementation objects
     * associated with the bound element, or zero if the element is not a bound
     * element or has none of its bindings have implementations.
     * 
     * @return Number of implementation objects associated with the bound
     *         element.
     */
    public int getLength();
}
