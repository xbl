/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.mlfc.xbl2;

/**
 * This class represents XBL specific exceptions.
 * 
 * @author Mikko Vestola
 * 
 */
public class XBLException extends Exception
{

    public XBLException(String message)
    {
        super(message);
    }

}
