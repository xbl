/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jul 12, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.VisualElementImpl;

/**
 * XBL-element: div
 * 
 * @author Mikko Vestola
 * 
 */
public class DivElementImpl extends VisualElementImpl
{
    public DivElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

}
