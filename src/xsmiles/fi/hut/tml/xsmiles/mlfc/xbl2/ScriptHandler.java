package fi.hut.tml.xsmiles.mlfc.xbl2;

import java.util.LinkedList;
import java.util.Iterator;

import fi.hut.tml.xsmiles.mlfc.xbl2.ScriptHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.dom.EventImpl; 

import org.w3c.dom.Element;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.Event;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.Log;

//import org.w3c.dom.Document;

/**
 * Class for handling scripts. Scripts are added to ScriptHandler with an
 * XBLHandler parameter which includes information about
 * 
 * @author Juho Vuohelainen
 */
public class ScriptHandler
{
    private LinkedList handlers; // XBLHandler objects are stored here

    public ScriptHandler()
    {
        handlers = new LinkedList();
    }

    /**
     * Takes an XBLHandler object and sets its event listener.
     * 
     * @param xh XBLHandler object that contains the script and its associated
     *            event and element.
     */
    public void addHandler(XBLHandler xh)
    {
        // Create an EventHandler and set it to listen to events in
        // the element in XBLHandler.
        EventHandler eh = new EventHandler(xh);
        xh.setEventListener(eh);

        // Store XBLHandler in case we need it later
        handlers.add(xh);
    }

    /**
     * Returns all handlers associated with an element in a LinkedList. The
     * returned XBLHandlers are NOT clones of originals and modifying them will
     * affect the behaviour of the application.
     * 
     * @param element XSmilesElementImpl element whose handlers are to be
     *            returned
     * @return LinkedList of XBLHandlers
     */
    public LinkedList getHandlers(XSmilesElementImpl element)
    {
        LinkedList returnList = new LinkedList();

        if (element == null)
            return returnList;

        XBLHandler tmp;
        for (int i = 0; i < handlers.size(); i++)
        {

            tmp = (XBLHandler) handlers.get(i);

            if (tmp.getElement().equals(element))
                returnList.add(tmp);
        }

        return returnList;
    }
    
    /**
     * This method removes all the event handlers from the bound elements
     * for the binding that owns this ScriptHandler.
     */
    public void removeEventHandlers() {
        
        for (int i = 0; i < handlers.size(); i++)
        {

            XBLHandler h = (XBLHandler)handlers.get(i);
            h.removeEventListener();
        }
        
        this.handlers = null;
        
    }

    public String toString()
    {
        Iterator it = handlers.iterator();
        String toret = "";

        while (it.hasNext())
        {
            XBLHandler handler = (XBLHandler) it.next();
            toret += handler.getElement().getNodeName() + " -> "
                    + handler.getScript() + "\n";
        }

        return toret.trim();
    }

    /**
     * Inner class for listening events. One EventHandler is assigned for each
     * XBLHandler.
     * 
     * @author Juho Vuohelainen
     */
    public class EventHandler implements EventListener
    {
        XBLHandler handler;

        public EventHandler(XBLHandler h)
        {
            handler = h;
        }

        public void handleEvent(Event evt)
        {
            // Get event target that fired this event (element in DOM tree)
            EventTarget evttarget = evt.getTarget();

            Log.debug("EVENT FIRED: " + evt.getType() + ", "
                    + evt.getClass().toString());
            
            /* Start the default phase if the event is at the bubble 
             * phase and currently in the root element
             * All the events are not necessary EventImpls (e.g. MutationEventImpls).
             * Thus check for ClassCastException. */            
             
            try {
                                
                if (((EventImpl)evt).currentTarget
                		== handler.getElement().getOwnerDocument().getDocumentElement()
                		&& evt.getEventPhase()==3)
                {
                	((EventImpl)evt).startDefaultPhase();
                	return; 
                }
                //add the current handler to default handlers if appropriate
                if (handler.getHandlerElement().hasAttribute("phase")
                		&& handler.getHandlerElement().getAttribute("phase").equals("default-action"))
                {
                	if (!((EventImpl)evt).getDefaultPhase())
                		((EventImpl)evt).addHandler(handler); 
                	
                }
                Log.debug("\nWIR KOMMEN HIER JA! \n" + ((EventImpl)evt).getDefaultPhase() + evt.getEventPhase());
                
            } catch (ClassCastException ex) {
                Log.debug("Default phase cannot be executed! Event not an EventImpl!");  
            }
            
            // Check this event should launch the script or not
            if (!handler.filter(evt))
                return;

            // Get ECMAScripter so we can evaluate script
            ECMAScripter scripter = null;
            ExtendedDocument edoc = (ExtendedDocument) handler.getElement()
                    .getOwnerDocument();
            scripter = edoc.getHostMLFC().getXMLDocument().getECMAScripter();

            if (!scripter.isInitialized())
                scripter.initialize(handler.getElement().getOwnerDocument());

            // Set Java objects to javascript objects (so evt maps to event)
            scripter.exposeToScriptEngine("event", evt);
            scripter.exposeToScriptEngine("document", handler.getElement()
                    .getOwnerDocument());

            // run the script
            scripter.eval(handler.getScript());

        }
    }
}
