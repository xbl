/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Nov 15, 2007
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * The template element contains child nodes that can be in any namespace.
 * 
 * @author Tomi Salminen
 * @author Jukka Julku
 * @author Mikko Meronen
 */
public class TemplateElementImpl extends ElementImpl
{

    private XSmilesElementImpl boundElement;

    private TemplateElementImpl clonedFromThis;

    public TemplateElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);

        /**
         * Constructor
         */

    }

    /**
     * This method returns the very instance of TemplateElemntImpl this node was
     * cloned from.
     * 
     * @return TemplateElementImpl this node was cloned from
     */

    public TemplateElementImpl clonedFrom()
    {
        return this.clonedFromThis;
    }

    /** This method creates a shadow content template.
     * 
     * @param boundElemet Element to which the shadow content template is bound to.
     * 
     * @return The created shadow content template.
     */

    public TemplateElementImpl createShadowContent(XSmilesElementImpl boundElement)
    {
        boundElement.initFinalFlattenedTree();
        TemplateElementImpl clone = (TemplateElementImpl) this.cloneNode(true);
        clone.clonedFromThis = this;
        /* search and set xbl:attrs */
        this.boundElement = boundElement;
        this.traverseXBLAttr(clone);

        return clone;
    }

    /**
     * This method applies shadow content template to a bound element.
     * 
     * @param boundElemet bound element.
     * @param isFirst Is this the first shadow content template to be attached to the tree?
     * @param isLast Is this the last shadow content template to be attached to the tree?
     * 
     */

    public void createFinalFlattenedTree(XSmilesElementImpl boundElement, boolean isFirst, boolean isLast)
    {

    	//Check if template has the apply-author-sheets value as false or doesn't have it specified. If, then
    	//set target boundElements authorSheets as false
    	if(!this.hasAttribute("apply-author-sheets")|| !this.getAttribute("apply-author-sheets").equals("true"))
			boundElement.setAuthorSheets(false);
		else
		    boundElement.setAuthorSheets(true);
    	
        if (isFirst)
        {

            while (boundElement.getFirstChild() != null)
                boundElement.removeChild(boundElement.getFirstChild());

            Node assister = this.getFirstChild();
            while (assister != null)
            {
                boundElement.appendChild(assister.cloneNode(true));
                assister = assister.getNextSibling();
            }
        } else
        {
            Node inherited = lookForInherited((Node) boundElement);

            if (inherited != null)
            {
                // all other inherited-element can be replaced by their childs
                replaceInheritedNodes(inherited, boundElement);

                // The found element is replaced by this template (cloned)
                Node parent = inherited.getParentNode();

                Node assister = this.getFirstChild();
                while (assister != null)
                {
                    parent.insertBefore(assister.cloneNode(true), inherited);
                    assister = assister.getNextSibling();
                }
                parent.removeChild(inherited);
            }
        }
        if (isLast)
        {
            applyContents(boundElement);
            replaceInheritedNodes(null, boundElement);
        }
    }

    /**
     * Replaces all except one of the inherited-elements with their children.
     * 
     * @param no Inherited-element not to be replaced
     * @param node Next element to travel
     * 
     * @return true If the element last traveled needs to be replaced
     * @return false Otherwise
     */

    private boolean replaceInheritedNodes(Node no, Node node)
    {
        if (node != null)
        {
            String ns = node.getNamespaceURI();
            String ln = node.getLocalName();
            if (ln != null && ln.equals("inherited") && ns != null
                    && ns.equals("http://www.w3.org/ns/xbl"))
            {
                if (node != no)
                {
                    return true;
                }
            } else
            {
                Node child = node.getFirstChild();
                while (child != null)
                {
                    boolean b = replaceInheritedNodes(no, child);

                    Node temp = child.getNextSibling();
                    if (b == true)
                    {

                        Node rep = child.getFirstChild();

                        while (rep != null)
                        {
                            Node tmp = rep.getNextSibling();
                            node.insertBefore(rep, child);
                            rep = tmp;
                        }

                        node.removeChild(child);
                    }

                    child = temp;
                }
            }
        }
        return false;
    }

    private Node lookForInherited(Node parent)
    {
        /**
         * Looks for an Inherited-element in the parameter node and its children
         * 
         * @param Node
         * @return Node
         */

        String ns = parent.getNamespaceURI();
        String ln = parent.getLocalName();
        if (ln != null && ln.equals("inherited") && ns != null
                && ns.equals("http://www.w3.org/ns/xbl"))
        {
            return parent;
        } else
        {
            Node child = parent.getFirstChild();
            while (child != null)
            {
                Node temp = lookForInherited(child);
                if (temp != null)
                    return temp;
                child = child.getNextSibling();
            }
        }

        return null;
    }

    /**
     * This method tries to unassign all distributed child elements of the bound element
     * from this shadow template.
     * 
     * Unassigned children are added to the bound elements list of undistributed
     * children.
     * 
     * @param boundElement The element which children have been redistributed to this element.
     * @param node The node from which the operation is started. This should be the shadow content template!
     * @param force True if the content in locked elements is to be removed.
     */

    public void unassignChildren(XSmilesElementImpl boundElement, Node node, boolean force)
    {
        if (node != null)
        {
            String ns = node.getNamespaceURI();
            String ln = node.getLocalName();
            if (ln != null && ln.equals("content") && ns != null
                    && ns.equals("http://www.w3.org/ns/xbl"))
            {
                ContentElementImpl c = (ContentElementImpl) node;
                Vector v = c.getAssignedElements();
                for (int i = 0; i < v.size(); i++)
                {
                    Node n = (Node) v.elementAt(i);
                    if (c.unassignElement(n, force))
                    {
                        i--;
                        if (boundElement.addUndistributedChild(n) == false)
                        {
                            // Log.debug("Child that is tried to be
                            // undistributed is not BoundElements original
                            // child!");
                        }
                    }
                }
            } else
            {
                Node child = node.getFirstChild();
                while (child != null)
                {
                    unassignChildren(boundElement, child, force);
                    child = child.getNextSibling();
                }
            }
        }
    }

    /**
     * This method tries to distribute undistributed children of bound element to child
     * content elements of this template. Assigned children are removed from the
     * bound elements list of undistributed children.
     * 
     * @param boundElement Bound element whose children are distributed.
     */

    public void distributeChildren(XSmilesElementImpl boundElement)
    {
        if (boundElement != null)
        {
            Vector uNodes = boundElement.getUndistributedChildNodes();

            // For each explicit child of bound element

            for (int i = 0; i < uNodes.size(); i++)
            {

                ContentElementImpl cont = findMatch(this, (Node) uNodes
                        .elementAt(i));
                if (cont != null)
                {

                    Node temp = boundElement
                            .removeUndistributedChild((Node) uNodes
                                    .elementAt(i));
                    if (temp != null)
                    {
                        cont.assignElement(temp);
                    }
                }
            }
        }
    }

    /**
     * Goes trough node and its child in pre-order depth first order and matches
     * the given node to all xbl:content nodes
     * 
     * @param parent next node to travel
     * @param match node to be matched to content node
     * 
     * @return first node that matches the given node
     */

    private ContentElementImpl findMatch(Node parent, Node match)
    {
        if (parent != null)
        {
            String ns = parent.getNamespaceURI();
            String ln = parent.getLocalName();
            if (ln != null && ln.equals("content") && ns != null
                    && ns.equals("http://www.w3.org/ns/xbl"))
            {
                if (((ContentElementImpl) parent).matchesElement(match))
                {
                    return (ContentElementImpl) parent;
                }
            } else
            {
                Node child = parent.getFirstChild();

                while (child != null)
                {
                    ContentElementImpl temp = null;

                    if ((temp = findMatch(child, match)) != null)
                    {
                        return temp;
                    }

                    child = child.getNextSibling();
                }
            }
        }
        return null;
    }

    /**
     * Goes trough node and its child in pre-order depth first order and
     * replaces all xbl:content nodes with their assigned content, or if none,
     * with their original childs.
     * 
     * @param node next no to travel
     */

    private Vector applyContents(Node node)
    {
        if (node != null)
        {
            String ns = node.getNamespaceURI();
            String ln = node.getLocalName();
            if (ln != null && ln.equals("content") && ns != null
                    && ns.equals("http://www.w3.org/ns/xbl"))
            {
                return ((ContentElementImpl) node).getElementsToAssign();
            } else
            {
                Node child = node.getFirstChild();
                while (child != null)
                {
                    Vector v = applyContents(child);

                    Node temp = child.getNextSibling();
                    if (v != null)
                    {
                        if (v.size() == 0)
                        {
                            node.removeChild(child);
                        } else
                        {
                            Node last = (Node) v.lastElement();

                            child = node.replaceChild(last, child);

                            for (int j = 0; j < v.size() - 1; j++)
                            {
                                node.insertBefore((Node) v.elementAt(j), last);
                            }
                        }
                    }

                    child = temp;
                }
            }
        }

        return null;
    }

    /**
     * Traverses the subtree of this template element, finds elements that have
     * attributes and calls a method that finds the xbl:attrs
     * 
     * @param parentNode the parent node from which to search
     */

    private void traverseXBLAttr(Node parentNode)
    {
        for (Node node = parentNode.getFirstChild(); node != null; node = node
                .getNextSibling())
        {
            if (node.getNodeType() == ELEMENT_NODE && node.hasAttributes())
                this.getXBLAttr((XSmilesElementImpl) node);
            traverseXBLAttr(node);
        }
    }

    /**
     * Searches for an xbl:attr field in an element. If an xbl:attr is found,
     * its values are space-separated and passed on to a method for parsing and
     * applying the corresponding attributes.
     * 
     * @param bindingNode the Element from which the xbl:attr attribute is
     *            searched
     */

    private void getXBLAttr(XSmilesElementImpl xblAttrNode)
    {
        String attr;
        Node xblAttr;
        if ((xblAttr = xblAttrNode.getAttributeNode("xbl:attr")) == null)
            return;

        /* Get all the space-separated values */
        StringTokenizer strtok = new StringTokenizer(xblAttr.getNodeValue());
        while (strtok.hasMoreTokens())
        {
            attr = strtok.nextToken();
            this.setXBLAttr(attr, xblAttrNode); // parse the tokens
        }
    }

    /**
     * Parse the values of a single attribute in the value field of an xbl:attr.
     * The space-separated values of a xbl:attr should be of form : [s1 ':'] s2
     * ['=' [s3 ':'] s4] ['#' s5] where s1, s3 are prefixes and s2,s4 are local
     * names, s5 is a type designation.
     * 
     * @param attr a single value of an xbl:attr field
     * @param xblAttrNode the Node to which attributes are forwarded
     */

    private void setXBLAttr(String attr, XSmilesElementImpl xblAttrNode)
    {

        char a;
        boolean ipf1 = false, ipf2 = false, itypedes = false, ipair = false;
        String str = "", pf1 = "", pf2 = "", typedes = "", local1 = "", local2 = "";
        /* Loop the string and parse the parts */
        for (int i = 0; i < attr.length(); i++)
        {
            a = attr.charAt(i);
            switch (a)
            {
                case '=': // pair of qnames found
                    if (ipair || itypedes || ipf2) // in error
                        return;
                    local1 += str;
                    str = "";
                    ipair = true;
                    break;
                case ':': // prefix found
                    if (ipair) // 2nd prefix

                    {
                        if (ipf2 || itypedes) // in error
                            return;
                        pf2 += str;
                        str = "";
                        ipf2 = true;

                    } else
                    // 1st prefix
                    {
                        if (ipf1) // in error
                            return;
                        pf1 += str;
                        str = "";
                        ipf1 = true;

                    }
                    break;
                case '#': // type designation found
                    if (itypedes) // in error
                        return;
                    if (ipair)
                        local2 += str;
                    else
                        local1 += str;
                    str = "";
                    itypedes = true;
                    break;
                default:
                    str += a;
            }
        }

        if (itypedes)
            typedes += str;
        else if (ipair)
            local2 += str;
        else
            local1 += str;

        /*
         * type designation, if present, must be text (or url), otherwise in
         * error
         */
        if (itypedes && !(typedes.trim().matches("text")))
            return;

        /* Check for xbl:text */
        if (ipair && this.setXBLText(pf1, local1, pf2, local2, xblAttrNode))
            return;

        /*
         * Finally, set the attributes. Use a null namespace URI if there is no
         * prefix
         */
        xblAttrNode
                .setAttributeNS(ipf1 ? null : xblAttrNode.getNamespaceURI(),
                        local1, this.boundElement.getAttribute(ipair ? local2
                                : local1));

    }

    /**
     * Checks if this xbl:attr contains a text node to be forwarded
     * 
     * @param pf1 First prefix
     * @param local1 First local part
     * @param pf2 Second prefix
     * @param local2 Second local part
     * @param xblAttrNode The target Node
     * @return if an xbl:text has been processed (regardless of its validity)
     */
    private boolean setXBLText(String pf1, String local1, String pf2, String local2, XSmilesElementImpl xe)
    {
        String str = "";
        Node node;
        int ntype;

        /*
         * xbl:text found on the left side. Create a text node based on the
         * attribute value on the right. If the element to which the text node
         * is being created has children other than attributes, the current
         * xbl:text field is in error
         */
        if (pf1.matches("xbl") && local1.matches("text"))
        {
            node = xe.getFirstChild();
            while (node != null)
            {
                if (node.getNodeType() != ATTRIBUTE_NODE)
                    return true; // in error
                node = node.getNextSibling();
            }
            xe.appendChild(getOwnerDocument().createTextNode(
                    this.boundElement.getAttribute(local2)));

            return true;

        }

        /*
         * xbl:text found on the right side. Create an attribute based on the
         * concatenated values of the bound element's text and CDATA children
         */
        else if (pf2.matches("xbl") && local2.matches("text"))
        {
            node = this.boundElement.getFirstChild();
            while (node != null)
            {
                ntype = node.getNodeType();
                if (ntype == TEXT_NODE || ntype == CDATA_SECTION_NODE)
                    str += node.getNodeValue();
                node = node.getNextSibling();
            }

            xe.setAttributeNS(pf1.matches("") ? null : xe.getNamespaceURI(),
                    local1, str);
            return true;

        }

        return false;
    }

}
