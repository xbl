/*
 /* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 *
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.css.sac.Selector;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.css.sac.SelectorList;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectorParser;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectors;
import fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler;

/**
 * XBL-element: binding.
 * 
 * @author Mikko Vestola
 */
public class BindingElementImpl extends ElementImpl
{

    /*
     * These are the CSS selectors that the element attribute of the binding
     * element has. Needed to found the elements that the bindig will be
     * applied.
     */
    private SelectorList selectors;

    private CSSSelectors CSSSelectors;

    private BindingHandler bindingHandler;
    
    // The handlers element of this binding
    private HandlersElementImpl handlersElem;
    private boolean hasNotHandlers;

    // The URI of the document where this binding is loaded
    private String originalBaseURI;

    public BindingElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

    public String getOrigBaseURI()
    {
        return this.originalBaseURI;
    }

    public void setOrigBaseURI(String URI)
    {
        this.originalBaseURI = URI;
    }

    /**
     * This function finds out if the element given in parameters matches the
     * binding's selector in its element attribute.
     * 
     * @param element Element we want to find out that does it match the
     *            selector
     * @return True if binding's selector matches element, false otherwise
     */
    public boolean bindingMatchesElement(XSmilesElementImpl element)
    {
        /*
         * If the binding doesn't have an element attribute, it does not match
         * the element.
         */
        if (!this.hasAttribute("element"))
            return false;

        // Set CSS selectors for this binding element if not already set
        if (this.selectors == null)
        {
            /*
             * This command gets the selector list (selector may be comma
             * seperated list of selectors, like element="h1, h2").
             */
            this.selectors = CSSSelectorParser.parseSelectors(this
                    .getAttribute("element"));
            this.CSSSelectors = new CSSSelectors();
        }

        /*
         * Loop through the selector list and find if the element matches any of
         * these selectors.
         */
        for (int i = 0; i < this.selectors.getLength(); i++)
        {
            // If the element matches the binding's element selector
            if (this.CSSSelectors.selectorMatchesElem(this.selectors.item(i),
                    element))
                return true;
        }

        // If we get here, the selector didn't match the element
        return false;
    }

    /**
     * Returns the bindingHandler that knows which binding belongs to which
     * element.
     * 
     * @return BindingHandler
     */
    public BindingHandler getBindingHandler()
    {
        return this.bindingHandler;
    }

    /**
     * Sets the binding handler that is responsible of this binding.
     * 
     * @param handler BindingHandler
     */
    public void setBindingHandler(BindingHandler handler)
    {
        this.bindingHandler = handler;
    }

    /**
     * Returns binding in text form: Binding [id=IDattr, element=ElemAttr]
     */
    public String toString()
    {
        String element = this.getAttribute("element");
        String id = this.getAttribute("id");
        return "Binding [id=" + id + ", element=" + element + "]";
    }

    /**
     * Applies this binding to the specified element. In other words, this
     * binding applies all the template, implementation, handlers and resources
     * elements to the element given in parameters.
     * 
     * @param element The element that this binding is going to be applied.
     */
    public void applyBinding(XSmilesElementImpl element)
    {

        /*
         * Get binding's child nodes (template, implementation, handlers and
         * resources elements).
         */
        NodeList baseElements = this.getChildNodes();

        /*
         * Max. one element is allowed per element type in the binding (so e.g.
         * no two template elements). Ignore the extra elements.
         */
        boolean hasTemplate = false;
        boolean hasImplementation = false;
        boolean hasHandlers = false;
        boolean hasResources = false;

        // Loop through child elements
        for (int i = 0; i < baseElements.getLength(); i++)
        {
            Node node = baseElements.item(i);
            if (node instanceof TemplateElementImpl && !hasTemplate)
            {
                // ((TemplateElementImpl) node).apply(element);
                hasTemplate = true;
            } else if (node instanceof ImplementationElementImpl
                    && !hasImplementation)
            {
                ((ImplementationElementImpl) node).apply(element);
                hasImplementation = true;
            } else if (node instanceof HandlersElementImpl && !hasHandlers)
            {
                ((HandlersElementImpl) node).apply(element);
                hasHandlers = true;
            } else if (node instanceof ResourcesElementImpl && !hasResources)
            {
                ((ResourcesElementImpl) node).apply(element);
                hasResources = true;
            }

        }
    }

    /**
     * This method checks that does this binding match the URI given in
     * parameters. For example a binding may refer to other bindigs via extends
     * attribute (e.g. binding
     * extends="http://www.site.com/extBindings.xml#b01").
     * 
     * @param URI The absolute URI referring to the binding element.
     * @return Returns true if the URI refers to the binding. If this binding
     *         does not have an ID attribute, returns false (since it cannot be
     *         referenced). Otherwise returns also false.
     */
    public boolean matchesElementURI(String URI)
    {

        String thisURI;
        if (this.hasAttribute("id"))
        {
            thisURI = this.getOrigBaseURI() + "#" + this.getAttribute("id");
            if (thisURI.equals(URI))
                return true;
        }

        return false;

    }

    public URL resolveURI(String src) throws MalformedURLException
    {
        String baseURI = this.originalBaseURI;
        if (baseURI != null)
            return new URL(new URL(this.getOrigBaseURI()), src);
        else
            return new URL(src);
    }
    
    /**
     * Returns the handlers element of this binding element.
     * @return The first handlers element below this binding or
     * null if the binding hasn't any handlers element.
     */
    public HandlersElementImpl getHandlersElement() {
        
        if (this.handlersElem!=null)
            return this.handlersElem;
        
        if (this.hasNotHandlers)
            return null;
                
        NodeList baseElements = this.getChildNodes();

        // Loop through child elements
        for (int i = 0; i < baseElements.getLength(); i++)
        {
            Node node = baseElements.item(i);
            if (node instanceof HandlersElementImpl) {
                this.handlersElem = ((HandlersElementImpl) node);
                return this.handlersElem;
            }
        }        
        
        /* If not found, store the boolean value so no need to iterate
         * throung the childs next time.
         */
        this.hasNotHandlers = true;
        return null;
        
    }

}
