/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jul 12, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLImplementation;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * XBL-element: implementation
 * 
 * @author Mikko Vestola
 * @author Juho Vuohelainen
 */
public class ImplementationElementImpl extends ElementImpl implements
        XBLImplementation
{
    private String script;

    public ImplementationElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

    public void apply(XSmilesElementImpl element)
    {
        Log.debug("Applying ImplementationElementImpl");

        if (this.getAttribute("src") != null
                && this.getAttribute("src").length() > 0)
        {
            script = getSrc();
        } else if (this.getText() != null && this.getText().length() > 0)
        {
            script = this.getText().trim();
        } else
            return;

        Log.debug("Script: " + script);
        ExtendedDocument edoc = (ExtendedDocument) element.getOwnerDocument();
        ECMAScripter scripter = edoc.getHostMLFC().getXMLDocument()
                .getECMAScripter();

        if (!scripter.isInitialized())
            scripter.initialize(element.getOwnerDocument());

        scripter.exposeToScriptEngine("document", element.getOwnerDocument());
        scripter.eval(script);
    }

    public void xblBindingAttached()
    {
        // TODO Auto-generated method stub
        Log.debug("ImplementationElementImpl.xblBindingAttached()");
    }

    public void xblEnteredDocument()
    {
        // TODO Auto-generated method stub
        Log.debug("ImplementationElementImpl.xblEnteredDocument()");
    }

    public void xblLeftDocument()
    {
        // TODO Auto-generated method stub
        Log.debug("ImplementationElementImpl.xblLeftDocument()");
    }

    private String getSrc()
    {
        String url = this.getAttribute("src");
        BufferedReader reader = null;
        StringBuffer src = new StringBuffer();

        XSmilesConnection conn;
        try
        {
            conn = HTTP.get(new URL(((XSmilesDocumentImpl) this.ownerDocument)
                    .getXMLDocument().getXMLURL(), url), null);
            reader = new BufferedReader(new InputStreamReader(conn
                    .getInputStream()));
            String line;
            while ((line = reader.readLine()) != null)
            {
                src.append(line);
            }
        } catch (MalformedURLException e)
        {
            Log.error(e.getMessage());
            return null;
        } catch (Exception e)
        {
            Log.error(e.getMessage());
            return null;
        }

        return src.toString().trim();
    }
}