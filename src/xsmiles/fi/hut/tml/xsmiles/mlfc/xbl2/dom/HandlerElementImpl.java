/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on Jul 12, 2006
 */
package fi.hut.tml.xsmiles.mlfc.xbl2.dom;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.TextImpl;
import org.apache.xerces.dom.events.MutationEventImpl;
import org.w3c.dom.Node;

import java.util.*;

import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;

/**
 * XBL-element: handler
 * 
 * @author Mikko Vestola
 * 
 */
public class HandlerElementImpl extends ElementImpl
{

    public static final String CTRLKEY = "control";

    public static final String SHIFTKEY = "shift";

    public static final String ALTKEY = "alt";

    public static final String METAKEY = "meta";

    public HandlerElementImpl(DocumentImpl owner, String namespace, String tag)
    {
        super(owner, namespace, tag);
    }

    public XBLHandler getXBLHandler()
    {
        XBLHandler handler = new XBLHandler();
        handler.setHandlerElementImpl(this);

        return handler;
    }

    /**
     * This method parses the space separated list and returns an array
     * containing the separated values.
     * 
     * @param list Space separated list (e.g. "12 3 35 foo bar"), is allowed to
     *            contain other whitespace characters which are removed.
     * @return Returns the values in array, whitespaces removed, each value in
     *         its own cell (e.g. [12,3,35,foo,bar]. If the string is just
     *         whitespaces, returns an empty array.
     * @author Mikko Vestola
     */
    public static String[] parseSpaceSeparatedList(String list)
    {

        ArrayList values = new ArrayList();

        StringTokenizer st = new StringTokenizer(list);
        while (st.hasMoreTokens())
        {
            String s = st.nextToken();
            values.add(s);
        }

        String[] valuesInArray = new String[values.size()];

        return (String[]) values.toArray(valuesInArray);
    }

    /**
     * From the W3C XBL 2.0 specification: The propagate attribute specifies
     * whether, after processing all listeners at the current node, the event is
     * allowed to continue on its path (either in the capture or the bubble
     * phase). The possible values are stop and continue (the default). If stop
     * is specified, then after the event handler has been fired, the event's
     * stopPropagation() method must be called.
     * 
     * @return If the propagate attribute is set to "stop", returns false.
     *         Otherwise returns true.
     * @author Mikko Vestola
     */
    public boolean propagate()
    {

        if (this.hasAttribute("propagate"))
        {
            if (this.getAttribute("propagate").equals("stop"))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks that will the handler accept this button when filtering events.
     * The handler has a list of button numbers that it accepts.
     * 
     * @param buttonNumb Button number
     * @return True, if the hanler has the button number in its button attribute
     *         (space separated list) or the handler accepts every button (so
     *         the button attribute is not set). False, if the handler does not
     *         accept the button.
     * @author Mikko Vestola
     */
    public boolean acceptsButton(int buttonNumb)
    {

        if (!this.hasAttribute("button"))
            return true;

        // Get space separated values
        String[] list = parseSpaceSeparatedList(this.getAttribute("button"));

        for (int i = 0; i < list.length; i++)
        {

            try
            {

                int button = Integer.parseInt(list[i]);

                if (button == buttonNumb)
                    return true;

            } catch (NumberFormatException e)
            {
                // Not an integer, ignore it
            }
        }

        // If we get here, the button is not in the list
        return false;

    }

    /**
     * This method checks that does this handler accept the modifiers given in
     * parameters. Handler has a list of modifiers (e.g. modifiers="alt
     * -control") and based on the list, it filters events. The filter matches
     * if all the modifiers that the UI supports are accounted for, and none of
     * the modifiers made the filter fail.
     * 
     * @param ctrlKey Was control key down?
     * @param shiftKey Was shift key down?
     * @param altKey Was alt key down?
     * @param metaKey Was meta key down?
     * @return True, if the filter matches. False, if the filter does not match.
     * @author Mikko Vestola
     */
    public boolean acceptsModifiers(boolean ctrlKey, boolean shiftKey, boolean altKey, boolean metaKey)
    {

        // If modifiers are not set
        if (!this.hasAttribute("modifiers"))
            return true;

        // Here are saved that are the known modifiers accounted
        boolean ctrlAccounted = false;
        boolean shiftAccounted = false;
        boolean altAccounted = false;
        boolean metaAccounted = false;

        // Save here modifiers (+modifier, -modifier and modifier?)
        HashSet mustBeModifiers = new HashSet();
        HashSet mightBeModifiers = new HashSet();
        HashSet denidedModifiers = new HashSet();

        // Get space separated values
        String[] list = parseSpaceSeparatedList(this.getAttribute("modifiers"));

        // If the modifiers are empty, it is same as modifiers="none"
        if (list.length == 0)
        {
            String[] newList = { "none" };
            list = newList;
        }

        // If the modifiers IS ONLY "any", the filter matches
        if (list.length == 1 && list[0].equals("any"))
            return true;

        // Go through the modifier list
        for (int i = 0; i < list.length; i++)
        {

            // What form the modifier is?
            boolean mustBe = false;
            boolean mustNotBe = false;
            boolean mightBe = false;

            String modifier = list[i];

            /*
             * Filter fails, if none modifiers are allowed but the event has
             * modifiers.
             */
            if (modifier.equals("none")
                    && (ctrlKey || shiftKey || altKey || metaKey))
                return false;
            else if (modifier.equals("none"))
                return true;

            /* Every modifier is accounted if "any" is set. */
            if (modifier.equals("any"))
            {
                ctrlAccounted = true;
                shiftAccounted = true;
                altAccounted = true;
                metaAccounted = true;
                continue;
            }

            // Check what form the modifier is
            if (modifier.startsWith("+"))
            {
                mustBe = true;
                modifier = modifier.substring(1);
            } else if (modifier.startsWith("-"))
            {
                mustNotBe = true;
                modifier = modifier.substring(1);
            } else if (modifier.endsWith("?"))
            {
                mightBe = true;
                modifier = modifier.substring(0, modifier.length() - 1);
            } else
            {
                // If no sign is set, then it is same as +modifier.
                mustBe = true;
            }

            // Set synonyms
            if (modifier.equals("accel"))
                modifier = CTRLKEY;
            if (modifier.equals("access"))
                modifier = ALTKEY;

            /*
             * If the modifier was form of +modifier
             */
            if (mustBe)
            {
                /*
                 * Check that was the modifier a known modifier. The filter
                 * fails, if the UA does not support the modifier.
                 */
                if (!(modifier.equals(CTRLKEY) || modifier.equals(SHIFTKEY)
                        || modifier.equals(ALTKEY) || modifier.equals(METAKEY)))
                    return false;
                else
                {
                    /*
                     * UA supports this modifier. Check that is the modifier set
                     * in the event. If not, then it makes the filter fail.
                     */
                    if (modifier.equals(CTRLKEY) && !ctrlKey)
                        return false;
                    else if (modifier.equals(SHIFTKEY) && !shiftKey)
                        return false;
                    else if (modifier.equals(ALTKEY) && !altKey)
                        return false;
                    else if (modifier.equals(METAKEY) && !metaKey)
                        return false;
                }

                /*
                 * The UA supports the modifier and it is set in the event. Add
                 * the modifier to the collection.
                 */
                mustBeModifiers.add(modifier);
            } else if (mustNotBe)
                denidedModifiers.add(modifier);
            else if (mightBe)
                mightBeModifiers.add(modifier);

        } // The end of for loop

        /*
         * Check that does the event has some supported modifiers that are not
         * allowed. If it has, then it makes the filter fail.
         */
        if (ctrlKey && denidedModifiers.contains(CTRLKEY))
            return false;
        if (shiftKey && denidedModifiers.contains(SHIFTKEY))
            return false;
        if (altKey && denidedModifiers.contains(ALTKEY))
            return false;
        if (metaKey && denidedModifiers.contains(METAKEY))
            return false;

        // Check that are the supported modifiers accounted.
        if (mustBeModifiers.contains(CTRLKEY)
                || mightBeModifiers.contains(CTRLKEY))
            ctrlAccounted = true;
        if (mustBeModifiers.contains(SHIFTKEY)
                || mightBeModifiers.contains(SHIFTKEY))
            shiftAccounted = true;
        if (mustBeModifiers.contains(ALTKEY)
                || mightBeModifiers.contains(ALTKEY))
            altAccounted = true;
        if (mustBeModifiers.contains(METAKEY)
                || mightBeModifiers.contains(METAKEY))
            metaAccounted = true;

        /*
         * From W3C Spec: The filter matches if all the modifiers that returned
         * true are accounted for, and none of the values made the filter fail.
         * 
         * Now, all the possible cases that could fail the filter are checked.
         * Just check that are the modifiers, that are set in the event, all
         * accounted. If not, the filter does not match.
         */
        if (ctrlKey && !ctrlAccounted)
            return false;
        if (shiftKey && !shiftAccounted)
            return false;
        if (altKey && !altAccounted)
            return false;
        if (metaKey && !metaAccounted)
            return false;

        // If we get here, the filter matches
        return true;

    }

    /**
     * This function checks if the "click-count" filter attribute on the event
     * matches the one in the handler. Click-count attribute is a
     * space-separated list of values one of which must match for this filter to
     * pass.
     * 
     * @param cc click-count from the detail-attribute of the event
     * @return whether the filter matches the event.
     */

    public boolean acceptsClickCount(int cc)
    {
        if (!this.hasAttribute("click-count")) // click-count isn't a filter
            return true;
        String[] list = parseSpaceSeparatedList(this
                .getAttribute("click-count"));
        for (int i = 0; i < list.length; i++)
        {
            try
            {
                if (Integer.parseInt(list[i]) == cc)
                    return true;
            } catch (NumberFormatException nfe)
            {
                continue; // non-integers shouldn't affect the parsing
            }

        }
        return false;

    }

    /**
     * This function checks if the "trusted" filter attribute on the event
     * matches the one in the handler.
     * 
     * @param trusted value of the trusted attribute on the event object
     * @return whether the filter matches the event.
     */
    public boolean acceptsTrusted(boolean trusted)
    {
        if (!(this.hasAttribute("trusted")))
            return true;
        if (this.getAttribute("trusted").matches("true"))
        {
            if (!trusted)
                return false;
        }

        return true; // the trusted attribute is something else than true;

    }

    /**
     * Checks whether the events phase matches the phase attribute on this
     * handler
     * 
     * @param phase the events phase
     * @return whether it matches the phase attribute on the handler
     */

    public boolean acceptsPhase(int phase)
    {
        int filter;
        if (!this.hasAttribute("phase"))
            return true;
        String phs = this.getAttribute("phase");
        if (phs.matches("capture"))
            filter = 1;
        else if (phs.matches("target"))
            filter = 2;
        else if (phs.matches("default-action"))
            filter = 2019716164;
        else
            filter = 3;
        if (filter == phase)
            return true;
        else
            return false;
    }

    /**
     * Checks whether the default action should be allowed.
     * 
     * @return true if allowed, false if it should be prevented
     */
    public boolean acceptsDefaultAction()
    {
        if (this.hasAttribute("default-action")
                && this.getAttribute("default-action").matches("cancel"))
            return false;
        else
            return true;
    }

    /**
     * Helper method for filtering mutation events.
     * 
     * @param prevValue The prevValue attribute of the event object.
     * @return True if the handler's prev-value attribute matches the string
     *         given in parameters or the handler doesn't have the specified
     *         attribute. False otherwise.
     * @author Mikko Vestola
     */
    public boolean acceptsPrevValue(String prevValue)
    {

        if (!this.hasAttribute("prev-value"))
            return true;

        if (prevValue == null)
            return false;

        if (this.getAttribute("prev-value").equals(prevValue))
            return true;
        else
            return false;

    }

    /**
     * Helper method for filtering mutation events.
     * 
     * @param newValue The newValue attribute of the event object.
     * @return True if the handler's new-value attribute matches the string
     *         given in parameters or the handler doesn't have the specified
     *         attribute. False otherwise.
     * @author Mikko Vestola
     */
    public boolean acceptsNewValue(String newValue)
    {

        if (!this.hasAttribute("new-value"))
            return true;

        if (newValue == null)
            return false;

        if (this.getAttribute("new-value").equals(newValue))
            return true;
        else
            return false;

    }

    /**
     * Helper method for filtering mutation events.
     * 
     * @param attrName The attrName attribute of the event object.
     * @return True if the handler's attr-name attribute matches the string
     *         given in parameters or the handler doesn't have the specified
     *         attribute. False otherwise.
     * @author Mikko Vestola
     */
    public boolean acceptsAttrName(String attrName)
    {

        if (!this.hasAttribute("attr-name"))
            return true;

        if (attrName == null)
            return false;

        if (this.getAttribute("attr-name").equals(attrName))
            return true;
        else
            return false;

    }

    /**
     * Helper method for filtering mutation events.
     * 
     * @param attrChange The attrChange attribute of the event object.
     * @return True if some of the values in the handler's attr-change attribute
     *         (space-separated list of values from the three literal keywords:
     *         modification, addition and removal, which map to the short values
     *         in MutationEventImpl) matches the short value given in parameters
     *         or the handler doesn't have the attr-change attribute. False
     *         otherwise.
     * @see MutationEventImpl
     * @author Mikko Vestola
     */
    public boolean acceptsAttrChange(short attrChange)
    {

        if (!this.hasAttribute("attr-change"))
            return true;

        // Get space separated values
        String[] list = parseSpaceSeparatedList(this
                .getAttribute("attr-change"));

        // If the list is empty, ignore it
        if (list.length == 0)
        {
            return true;
        }

        /*
         * Did the list have any correct values (modification, addition,
         * removal).
         */
        boolean hadCorrectValue = false;

        short value;

        // Go through the list
        for (int i = 0; i < list.length; i++)
        {

            /*
             * Mapping between short values and strings. Ignore any other values
             * (continue to next item in list).
             */
            if (list[i].equals("modification"))
            {
                value = MutationEventImpl.MODIFICATION;
                hadCorrectValue = true;
            } else if (list[i].equals("addition"))
            {
                value = MutationEventImpl.ADDITION;
                hadCorrectValue = true;
            } else if (list[i].equals("removal"))
            {
                value = MutationEventImpl.REMOVAL;
                hadCorrectValue = true;
            } else
                continue;

            /*
             * The event's attrChange value matches one of the elements in the
             * attr-change list, so return true.
             */
            if (attrChange == value)
                return true;

        }

        /*
         * If we get here, the event's attrChange value did not match any of the
         * values in the handler's attr-change attribute. If the list did not
         * have any correct values, it is ignored, so return true.
         */
        if (hadCorrectValue)
            return false;
        else
            return true;

    }

    public String getEventName()
    {
        return this.getAttribute("event");
    }
}
