/*
 * Created on 29.12.2003
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.mlfc.sip;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Browser;

import java.awt.*;
import javax.swing.*;

import org.w3c.dom.Element;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

// SIP client imports - probably should be removed and integrated better to the current implementation
import fi.hut.tml.sip.*;
import fi.hut.tml.genericgui.*;


/**
 * @author ssundell
 *
 */
public class SIPMLFC extends MLFC
{
    private String sipAddress;
    private Container  sipPanel;
    private JPanel panel = new JPanel();
    //GenericGUIFactory factory = new GenericGUIFactory(GenericGUIFactory.GUI_SWING);
    //protected SipClientUIGenericPanel sipGenPanel;
    
    public SIPMLFC()
    {
    }
    
    /**
     * Get the version of the MLFC. This version number is updated
     * with the browser version number at compilation time. This version number
     * indicates the browser version this MLFC was compiled with and should be run with.
     * @return 	MLFC version number.
     */
    public final String getVersion()
    {
        return Browser.version;
    }
    
    public void start()
    {
      /*  Container container=this.getContainer();
        boolean primary = this.isPrimary();
        
        Rectangle rect;
        if (!primary)
        {
            Log.debug("SIPMLFC as secondary, this is too ugly to use!");
            Log.debug("SIPMLFC: Getting SIP Panel");
            
            sipGenPanel = new SipClientUIGenericPanel("cfg/sip.properties",factory);
            sipPanel = (java.awt.Container)sipGenPanel.getPanel().getObject();
            
            rect = container.getBounds();
            
            sipPanel.setSize(rect.getSize());
            
            
            
            Log.debug("SIPMLFC: sipPanel" + sipPanel.getSize().toString());
            Log.debug("SIPMLFC: container " + container.getSize().toString());
            
            Log.debug("SIPMLFC: Adding SIP Panel");
            container.add(sipPanel);
            
            Log.debug("SIPMLFC: validating");
            container.setBounds(rect);
            
            container.invalidate();
            
            if (container.getParent() != null)
                container.getParent().validate();

            return;
        }
        Log.debug("SIPMLFC.start() starting as PRIMARY MLFC");
        
        panel.setLayout(new BorderLayout());
        panel.setBackground(java.awt.Color.white); //optional
        container.add(panel);
        try
        {
            sipGenPanel = new SipClientUIGenericPanel("cfg/sip.properties",factory);
            sipPanel = (java.awt.Container)sipGenPanel.getPanel().getObject();
            panel.add(sipPanel);
            Log.debug("SIPMLFC: sipPanel" + sipPanel.getSize().toString());
            Log.debug("SIPMLFC: container " + container.getSize().toString());
        
            container.doLayout();
            container.invalidate();
            if (container.getParent()!=null) container.getParent().validate();
        
            if (sipAddress!=null)
                sipGenPanel.callParty(sipAddress);  
        }
        catch (Exception e)
        {
            Log.error(e.toString());
        }
        */
    }
    
    public void stop()
    {
        /*
        this.getContainer().remove(panel);
        panel.removeAll();
        panel.setLayout(null);
        sipGenPanel.closePanel();
        */
	}
    
    /**
     * Create a DOM element.
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag)
    {
        Element element = null;
        Log.debug("SIP : "+ns+":"+tag);
        
        if (tag == null)
        {
            Log.error("Invalid tag name!");
            return null;
        }
        
        String localname = getLocalname(tag);
        
        if (localname.equals("root"))
            element = new XSmilesElementImpl(doc,ns,tag);
        else if (localname.equals("call"))
        {
            Log.debug("**** Creating SIP call element");
            element = new SIPCallElementImpl(doc, this, ns, tag);
        }
        
        return element;
    }
    
    protected void setAddress(String address) {
        sipAddress = address;
    }
}
