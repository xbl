/*
 * Created on 12.1.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.mlfc.sip;

import org.apache.xerces.dom.DocumentImpl;

import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

/**
 * @author ssundell
 *
 */
public class SIPCallElementImpl extends XSmilesElementImpl
{
    private SIPMLFC mlfc;
    
    /**
     * @param ownerDocument
     * @param sipMLFC
     * @param namespaceURI
     * @param qualifiedName
     */
    public SIPCallElementImpl(DocumentImpl ownerDocument,SIPMLFC sipMlfc,String namespaceURI,String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
        mlfc = sipMlfc;
    }
    
    public void init()
    {
        String address = this.getAttribute("address");
        if (address!=null && !address.equals("") && !address.equals("sip:"))
            mlfc.setAddress(address);
            
    }
}
