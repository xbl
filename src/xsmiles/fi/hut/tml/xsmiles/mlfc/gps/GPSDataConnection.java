package fi.hut.tml.xsmiles.mlfc.gps;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.comm.*;
import fi.hut.tml.xsmiles.Log;

/**
 * A Simple class for communicating with GPS receiver.
 * Needs java Comm API implmentation, which is available from 
 * e.g. http://www.interstice.com/kevinh/linuxcomm.html, which
 * is a GNU serial communications implementation called RXTX. Both win32, 
 * and linux. 
 *
 * @author Unknown, Modification by Juha
 */
public class GPSDataConnection implements SerialPortEventListener {
  
  private int t=0;
  private String sCMD="";
  private String sWRN=" ";
  private String sUTCt="";
  private String sUTCd="";
  private String sLATI1="";
  private String sLATI2="";
  private String sLONG1="";
  private String sLONG2="";
  private String sSPEED="";
  private String sCOURSE="";
  private String sREST="";
  private boolean bRMC=false;
  private boolean debugging=false; // Print out serial data coming from GPS

 

  private InputStream inputStream;
  private SerialPort serialPort;
  private Thread readThread;
  private CommPortIdentifier thePort;
  private long timeThen;

  // What is the minimum interval between GPS data changes
  private int refreshRate=3000;
  
  private GPSListener gpsListener;

  /** 
   * A simple main for debugging.
   */
  public static void main(String[] args) {
    GPSDataConnection dc=new GPSDataConnection("COM1");

    while(true) {
      Thread.yield();
    }
    // A loop to keep the program running
    /*Thread t=new Thread {
      public run() {
      while(true) {
      Thread.sleep(20000);
      }
      }
      };
      t.start();
    */
  }
  
  public GPSDataConnection(String port) {
    CommPortIdentifier portId;
    Enumeration portList;
    String linuxPort=null;


    
    timeThen=System.currentTimeMillis();
    
    Log.debug("Starting up GPS data connection...");
    portList = CommPortIdentifier.getPortIdentifiers();
    
    Log.debug("Port-IDs fetched...");
    while (portList.hasMoreElements()) 
      {
	Log.debug("going through port-IDs ...");
	portId = (CommPortIdentifier) portList.nextElement();
	Log.debug("Port:"+portId.getName());

	if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) 
	  {
	    System.out.println("Serial Port found:"+portId.getName());
	    
	    String pName=portId.getName();

	    if(pName.endsWith("ttyS0"))
	      pName="COM1";
	    else if(pName.endsWith("ttyS1"))
	      pName="COM2";
	    if (pName.equals(port))  // COM1
	      {
		Log.debug("Found good port "+ portId.toString());
		startLogging(portId);
		thePort=portId;
		break;
	      }
	  }
      }
    System.out.println("Ulos...");
  }

  /**
   * Close serial connections. FIXME, check how to close up
   */
  public void closeUp() {
    if(serialPort!=null) {
      serialPort.close();
      Log.debug("CLosing up serial connection");
    }
  }
  
  /**
   * Set GPS data listener... There can only be one ;)
   */
  public void addGPSListener(GPSListener gl) {
    gpsListener=gl;
  }
  
  public void setMaxRate(int rate) {
    refreshRate=rate;
  }

  /** 
   * Configure serial port portId, set parameters, and start listening
   *
   * @param portId
   */
  private void startLogging(CommPortIdentifier portId) {
    // Open port
    try {
      serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
      Log.debug("Opened serial port" +serialPort.toString());
    } catch (PortInUseException e) {
      Log.debug("Problem with opening the serial port");
      Log.error(e);
    }
    
    // Get inputstream
    try 
      {
	inputStream = serialPort.getInputStream();
      } 
    catch (IOException e) 
      {
	Log.debug("Problem with getting input stream from serial port");
	Log.error(e);
      }

    // Add serialeventListener
    try 
      {
	serialPort.addEventListener(this);
      } 
    catch (TooManyListenersException e) 
      {
	Log.debug("Too many serial port listeners");
	Log.error(e);
      }
    serialPort.notifyOnDataAvailable(true);

    // Set parameters for serial communication
    try {
      serialPort.setSerialPortParams(4800,
				     SerialPort.DATABITS_8,
				     SerialPort.STOPBITS_1,
				     SerialPort.PARITY_NONE);
    } catch (UnsupportedCommOperationException e) {
      Log.debug("Problems with setting serial parameters");
      Log.error(e);
    }
  }
  
  /**
   * Serial event listener. We are only listening data available
   */
  public void serialEvent(SerialPortEvent event) {
    switch(event.getEventType()) {
      /* case SerialPortEvent.BI:
	 case SerialPortEvent.OE:
	 case SerialPortEvent.FE:
	 case SerialPortEvent.PE:
	 case SerialPortEvent.CD:
	 case SerialPortEvent.CTS:
	 case SerialPortEvent.DSR:
	 case SerialPortEvent.RI:
      */
    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
      break;
    case SerialPortEvent.DATA_AVAILABLE:
      byte[] readBuffer = new byte[20];
      try {
	while (inputStream.available() > 0) {
	  int numBytes = inputStream.read(readBuffer);
	}
	parseLine(new String(readBuffer));
      } catch (IOException e) 
	{
	  Log.debug("Sarjaliikenneongelma");
	  Log.error(e);
	}
      break;
    }
  }

  /**
   * Parse the $GPRMC sentence from standard NMEA protocol.
   */
  public void parseLine(String line)
  {
    if(debugging)
      Log.debug(line);
    int n;
    char c=' ';
    for(n=0;n<line.length();n++)
      {
	c=line.charAt(n);
	if(c=='$')
	  {
	    t=1;
	    sCMD="";    // Which command.. I think $GPRMC is the one were looking for
	    sUTCt="";   // UTC Time e.g., 225446 (= 22:54:46 UTC)
	    sWRN=" ";   // GPS Receiver warning A(=OK) V(=Warning)
	    sLATI1="";  // Latitude e.g., 4916.45 (= 49deg, 16.45min)
	    sLATI2="";  // e.g. Latitude N (= North)
	    sLONG1="";  // Logitude e.g., 12311.12 (= 123deg 11.12min)
	    sLONG2="";  // e.g. Longitude W (==West)
	    sSPEED="";  // Speed e.g., 000.5 (000.5 Knots)
	    sCOURSE=""; // Course e.g., 054.7 (54.7 degrees?)
	    sUTCd="";   // UTC date e.g., 191194 (=19, November 1994)
	    sREST="";   // Rest of the data.. Magnetic variation, and checksum
	    bRMC=false;
	  }
	if(c=='\0') continue;

	/*
	   This can be implemented in a better way, but 
	   it might work, so why bother.
	 */ 
	switch(t)
	  {
	  case 1:
	    if(c==',')
              {
                if(sCMD.equals("$GPRMC"))
		  {
		    bRMC=true;
		    t=2;
		  }
                else
		  t=50;
              }
	    else
	      sCMD=sCMD+c;
	    break;
	  case 2:
	    if(c==',')
              {
		t=3;
              }
	    else
	      sUTCt=sUTCt+c;
	    break;
	  case 3:
	    if(c==',')
              {
		t=4;
              }
	    else
	      sWRN=sWRN+c;
	    break;
	  case 4:
	    if(c==',')
              {
		t=5;
              }
	    else
	      sLATI1=sLATI1+c;
	    break;
	  case 5:
	    if(c==',')
              {
		t=6;
              }
	    else
	      sLATI2=sLATI2+c;
	    break;
	  case 6:
	    if(c==',')
              {
		t=7;
              }
	    else
	      sLONG1=sLONG1+c;
	    break;
	  case 7:
	    if(c==',')
              {
		t=8;
              }
	    else
	      sLONG2=sLONG2+c;
	    break;
	  case 8:
	    if(c==',')
              {
		t=9;
              }
	    else
	      sSPEED=sSPEED+c;
	    break;
	  case 9:              
	    if(c==',')
              {
		t=10;
              }
	    else
	      sCOURSE=sCOURSE+c;
	    break;
	  case 10:
	    if(c==',')
              {       
		t=11;
              }
	    else
	      sUTCd=sUTCd+c;
	    break;
	  case 11:
	    if(bRMC)
              {
		if(sWRN.length()>0)       
		  if(sWRN.charAt(1)=='A')
		    {
		      /*posi.xPOsC(sLONG1,sLONG2);
			posi.yPOs(sLATI1,sLATI2);
			posi.PosAdd();*/
		      long timeNow=System.currentTimeMillis();
		      if((timeNow-timeThen)>refreshRate) {
			Log.debug("Send data " +timeNow + " " + timeThen);
			timeThen=System.currentTimeMillis();
			Log.debug("Ajan pit�is olla "+System.currentTimeMillis());
			gpsListener.gpsDataReceived(sLATI1, sLONG1, sSPEED, sUTCd, sUTCd, sCOURSE);
		      }
		    }
                //else
	      }
	    t=12;
	    break;
	  case 50:
	   
	    sREST=sREST+c;
	    break;
	  }
      }
  }
}
