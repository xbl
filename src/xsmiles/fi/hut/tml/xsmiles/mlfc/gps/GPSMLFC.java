/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gps;

import java.net.URL;
import java.awt.Dimension;
import java.awt.Container;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Browser;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.XMLDocument;
import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;

import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import java.util.*;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;

/**
 * GPSMLFC implements provides access to GPS data.
 * Namespace: http://www.fobar.com/gps
 */
public class GPSMLFC extends MLFC
{
    
    
    private Vector gpsElements;
    
    /**
     * Constructor.
     */
    public GPSMLFC()
    {
        gpsElements=new Vector();
    }
    
    /*
  protected String getLocalname(String tagname)
  {
    int index = tagname.indexOf(':');
    if (index<0) return tagname;
    String localname = tagname.substring(index+1);
    return localname;
  }
     */
    
    /**
     * Get the version of the MLFC. This version number is updated
     * with the browser version number at compilation time. This version number
     * indicates the browser version this MLFC was compiled with and should be run with.
     * @return 	MLFC version number.
     */
    public final String getVersion()
    {
        return Browser.version;
    }
    
    /**
     * Create a DOM element.
     */
    public Element createElementNS(DocumentImpl doc, String ns, String tag)
    {
        Element element = null;
        Log.debug("GPS : "+ns+":"+tag);
        
        if (tag == null)
        {
            Log.error("Invalid null tag name!");
            return null;
        }
        
        String localname = getLocalname(tag);
        
    /* Point tag e.g.,
    <gpsdata lat"34" long="3" speed="3" utcd"190801"
             utct="112233" course="130" emulation="true"/>
     */
        
        if (localname.equals("gpsdata"))
        {
            Log.debug("**** Creating GPS data element");
            element = new GPSDataElementImpl(doc, this, ns, tag);
            gpsElements.addElement(element);
        }
        else if (localname.equals("area"))
        {
            Log.debug("**** Creating GPS area element");
            element = new AreaElementImpl(doc, this, ns, tag);
            gpsElements.addElement(element);
        }
        // DEBUG START
        /*
        else if (localname.equals("test"))
        {
            Log.debug("**** Creating GPS test element");
            element = new fi.hut.tml.xsmiles.mlfc.xforms.dom.TestControl(doc,  ns, tag);
        }
         */
        // DEBUG END
        // Other tags go back to the XSmilesDocument as null...
        if (element == null)
            Log.debug("GPS MLFC didn't understand element, it is propably not implemented yet: "+tag);
        
        return element;
    }
    
    public void start()
    {
        XMLDocument doc = this.getXMLDocument();
        
        Log.debug("EVENTS.initialize() called.");
        ECMAScripter ecma = doc.getECMAScripter();
        
        // SVG may have already initilized ecmascripter
        if (!ecma.isInitialized()) ecma.initialize(doc.getDocument());
        
        
    }
    
    
    
    /**
     * Append the given URL to be a full URL.
     * @param partURL		Partial URL, e.g. fanfaari.wav
     * @return  Full URL, e.g. http://www.x-smiles.org/demo/fanfaari.wav
     */
    public URL createURL(String partURL)
    {
        try
        {
            return new URL(this.getXMLDocument().getXMLURL(),partURL);
        } catch (java.net.MalformedURLException e)
        {
            Log.error(e);
            return null;
        }
    }
    
    
    
    
    /**
     * Not implemented method.
     */
    public void setSize(Dimension d)
    {
    }
    
    /**
     * Called from the LinkHandler - this method asks the browser to go to this external link.
     * @param url		URL to jump to.
     */
    public void gotoExternalLink(String url)
    {
        Log.debug("...going to "+url);
        URL u = createURL(url);
        getMLFCListener().openLocation(u);
    }
    
    /**
     * Display status text in the broswer. Usually shows the link destination.
     */
    public void displayStatusText(String url)
    {
        getMLFCListener().setStatusText(url);
    }
    
    
    
    /**
     * The opposite of init()
     * deactivate is only called for displayable MLFCs
     */
    public void stop()
    {
        //super.destroy();
        Log.debug("Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?");
        Log.debug("Destroying objects related with GPS MLFC");
        Enumeration e = gpsElements.elements();
        while(e.hasMoreElements())
            ((XSmilesElementImpl)e.nextElement()).destroy();
        gpsElements=null;
    }
}



