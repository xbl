/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gps;

public interface GPSListener {
  /**
   * When data is received, this method is called
   */
  public void gpsDataReceived(String lat, String longi, String speed, 
			      String utcd, String utct, String course);
}
