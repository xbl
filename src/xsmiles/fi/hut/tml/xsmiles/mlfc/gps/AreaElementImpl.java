/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gps;

import fi.hut.tml.xsmiles.dom.AnimationService;
import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
//import org.w3c.dom.smil20.*;

/**
 * Area  Element send dom changed events to inform 
 * changes in coordinate data - that a particular area has been entered/exited.
 * Area element has all coordinates in format "2400000", no floating points.
 * This enables SMIL Animation module to animate the area locations.
 * Also, the distance is in form "120", which denotes meters.
 *
 * @author Kari
 */
public class AreaElementImpl extends XSmilesElementImpl implements AnimationService {

	// GPSMLFC
	GPSMLFC gpsMLFC = null;

	// XSmilesDocumentImpl - to create new elements
	DocumentImpl ownerDoc = null;

	// The current state - inside or outside the area
	boolean insideArea = false;

	// The area specified in the attributes.
	float areaLat=0, areaLong=0, areaDistance=0;
	
	// Animated values - value null indicates no animation
	String animLat = null, animLong = null, animDist = null;
	
	// Reference to the grpdata element - to retrieve the latitude and longitude data
	GPSDataElementImpl gpsDataElement = null;
	
	/**
	* Constructor - Set the owner, name and namespace.
	*/
	public AreaElementImpl(DocumentImpl owner, GPSMLFC gps, String namespace, String tag)
	{
		super(owner, namespace, tag);
		gpsMLFC = gps;
		ownerDoc = owner;
		Log.debug("GPS Area element created!");
	}

	private void dispatch(String type)
	{	 
		// Dispatch the event
		Log.debug("Dispatching GPS Area Event: "+type);
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		Log.debug("GPSArea.init");
		Node n = getParentNode();
		if (n instanceof GPSDataElementImpl)
			gpsDataElement = (GPSDataElementImpl)n;
		else
			Log.error("GPS Area element should always be under a gpsdata element!");
	}

	/**
	 * Test if the given coordinates are within this area element. If the area
	 * is at the given location, true is returned. This element will also send
	 * gpsAreaEntered and gpsAreaExited events, when the area is entered or exited.
	 * 
	 * @param latitude		A string in form "24.00000"
	 * @param longitude		A string in form "61.00000"
	 * @return				true if the area is covered.
	 */
	public boolean isWithinArea(String latitude, String longitude)
	{
		float currentLat=0, currentLong=0;
		boolean status;
		
		// If GPS data hasn't been received yet, but someone
		// is animating this element, then just exit.
		if (latitude == null || longitude == null)
			return false;
		
		// Parse gps strings
		try  {
			currentLat = Float.parseFloat(latitude);
			currentLong = Float.parseFloat(longitude);
		} catch (NumberFormatException e)  {
			Log.error("GPS Area couldn't parse GPS received latitude/longitude!");
			return insideArea;
		}

		// TODO: this should be parsed only in the beginning and when the attributes
		// are changed - for efficiency!
		// Parse area strings
		try  {
			areaLat = Float.parseFloat(getLatitudeAttribute());
			areaLong = Float.parseFloat(getLongitudeAttribute());
			areaDistance = Float.parseFloat(getDistanceAttribute());
		} catch (NumberFormatException e)  {
			Log.error("GPS Area couldn't parse GPS received latitude/longitude!");
			return insideArea;
		}
		
		// GPS sends data as "24.00000", while area has format "2400000"
		// Convert gps floats to area integers
		currentLat = currentLat * 1000;
		currentLong = currentLong * 1000;
				
		// Check if the current location is within area - sqrt( (x2-x1)^2 + (y2-y1)^2 )
		float distance = (float)Math.sqrt( (currentLat - areaLat)*(currentLat - areaLat) +	
											(currentLong - areaLong)*(currentLong - areaLong) );
									
		Log.debug("distance: "+distance);

		if (distance < areaDistance)
			status = true;
		else
			status = false;
		
		// Have we entered area?
		if (insideArea == false && status == true)  {
			dispatch("gpsAreaEntered");
			insideArea = true;
			return true;
		}

		// Have we exited area?
		if (insideArea == true && status == false)  {
			dispatch("gpsAreaExited");
			insideArea = false;
			return false;
		}

		return insideArea;
	}

	/**
	* When GPS data is received, the attributes of this element change, 
	* and a DOM event is sent
	*/
	public void gpsDataReceived(String lat, String longi, String speed, String utcd, 
		String utct, String course)
	{
		this.setAttribute("lat", lat);
		this.setAttribute("long", longi);
		this.setAttribute("speed", speed);
		this.setAttribute("utcd", utcd);
		this.setAttribute("utct", utct);
		this.setAttribute("course", course);
		dispatch("gpsDataChanged");
	}

	/**
	* Destroy this element.
	*/
	public void destroy()
	{
		Log.debug("GPS Area destroy()");
	}
	
	private String getLatitudeAttribute()  {
		if (animLat != null)
			return animLat;
		return getAttribute("lat");
	}

	private String getLongitudeAttribute()  {
		if (animLong != null)
			return animLong;
		return getAttribute("long");
	}

	private String getDistanceAttribute()  {
		if (animDist != null)
			return animDist;
		return getAttribute("distance");
	}
	
	// AnimationService implementation
	
	/**
	 * Convert String attribute to an float value
	 */
	public float convertStringToUnitless(String attr, String value)  {
		return 0;
	}
	public String convertUnitlessToString(String attr, float value)  {
		return null;
	}

	/**
	 * The attribute value got with this method takes precedence over
	 * the DOM attribute value.
	 * @param attr	Animated attribute
	 */
	public String getAnimAttribute(String attr)  {
		if (attr.equals("lat"))
			return animLat;
		if (attr.equals("long"))
			return animLong;
		if (attr.equals("distance"))
			return animDist;
		return "";	
	}

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 * @param attr	Attribute to be animated
	 * @param value	Animation value to be set
	 */
	public void setAnimAttribute(String attr, String value)  {
		if (attr.equals("lat"))
			animLat = value;
		if (attr.equals("long"))
			animLong = value;
		if (attr.equals("distance"))
			animDist = value;
	}

	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 * @param attr	Attribute to be animated (animation removed)
	 */
	public void removeAnimAttribute(String attr)  {
		if (attr.equals("lat"))
			animLat = null;
		if (attr.equals("long"))
			animLong = null;
		if (attr.equals("distance"))
			animDist = null;
	}

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 */
	public void refreshAnimation()  {
	
		isWithinArea(gpsDataElement.getLatitude(), gpsDataElement.getLongitude());
	}
	
}


