/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.mlfc.gps;

import org.w3c.dom.DOMException;
import org.w3c.dom.*;
import org.w3c.dom.events.*;

import org.apache.xerces.dom.DocumentImpl;
import fi.hut.tml.xsmiles.dom.XSmilesElementImpl;
import org.apache.xerces.dom.events.EventImpl;

import fi.hut.tml.xsmiles.Log;
//import org.w3c.dom.smil20.*;

import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.EventFactory;
import fi.hut.tml.xsmiles.dom.MouseEventImpl;
import javax.swing.*;
import java.awt.event.*;
import java.util.Calendar;
import java.awt.*;

/**
 * GPSData  Element send dom changed events to inform 
 * changes in coordinate data
 *
 * @author Juha
 */
public class GPSDataElementImpl extends XSmilesElementImpl implements GPSListener, ActionListener {

	// GPSMLFC
	GPSMLFC gpsMLFC = null;

	// XSmilesDocumentImpl - to create new elements
	DocumentImpl ownerDoc = null;

	// Emulate GPS data?
	private boolean emu=false;

	// Window for emulation purposes
	private static JFrame emuWindow;
	private JLabel latit, longit, speedt, courset, utcdt, utctt;
	private JTextField latitf, longitf, speedtf, coursetf, utcdtf, utcttf;
	private JButton okb;

	// The serial connection to the GPS
	private GPSDataConnection gpsConnection;
	
	// Latitude and longitude values for AreaElement
	private String latitude = null, longitude = null;

	/**
	* Constructor - Set the owner, name and namespace.
	*/
	public GPSDataElementImpl(DocumentImpl owner, GPSMLFC gps, String namespace, String tag)
	{
		super(owner, namespace, tag);
		gpsMLFC = gps;
		ownerDoc = owner;
		Log.debug("GPS element created!");
	}

	private void dispatch(String type)
	{	 
		// Dispatch the event
		Log.debug("Dispatching GPS Event");
		org.w3c.dom.events.Event evt = new EventImpl(); //createEvent();
		evt.initEvent(type, true, true);
		((org.w3c.dom.events.EventTarget)this).dispatchEvent(evt);
		return;
	}

	/**
	* Initialize this element.
	* This requires that the DOM tree is available.
	*/
	public void init()
	{
		Log.debug("GPS.init");

		// If emulation on, then pop up a window for inserting GPS values.
		String emulation=this.getAttribute("emulation");
		if(emulation!=null&&emulation.equals("true"))
		{
			emu=true;
			createEmulationWindow();
		}
		else
		{
			String port=gpsMLFC.getMLFCListener().getProperty("gps/port");
			if(port==null || port.equals(""))
			{
				Log.error("No gps configuration option found, switching to default: COM1");
				port="COM1";
			}
			Log.debug("Cranking up GPSDataConnection with port COM1...");
			gpsConnection=new GPSDataConnection(port);
			gpsConnection.addGPSListener(this);

				String rate=getAttribute("refreshRate");
			if(rate==null || rate.equals(""))
			{
				Log.error("No refresh rate specified in configuration file, using default, 1000");
				rate="3000";
			}
			int refRate=Integer.parseInt(rate);

			gpsConnection.setMaxRate(refRate);
		}

		super.init();
	}

	public void createEmulationWindow()
	{
		emuWindow=new JFrame("GPS Emulator");
		emuWindow.setSize(344,218);
		//emuWindow.setRezisable(false);

		latit=new JLabel("Latitude (e.g., 4916.45)");
		if(getAttribute("lat")==null ||getAttribute("lat").equals(""))
			latitf=new JTextField("60.000000");
		else 
			latitf=new JTextField(getAttribute("lat"));

		longit=new JLabel("Longitude (e.g., 12311.12)");
		if(getAttribute("long")==null ||getAttribute("long").equals(""))
			longitf=new JTextField("24.00000");
		else 
			longitf=new JTextField(getAttribute("long"));


		speedt=new JLabel("Speed in knots (e.g., 0.5)");
		if(getAttribute("speed")==null ||getAttribute("speed").equals(""))
			speedtf=new JTextField("0.000005");
		else
			speedtf=new JTextField(getAttribute("speed"));

		courset=new JLabel("Course (e.g., 054.7 = 54.7 degrees?)");
		if(getAttribute("course")==null ||getAttribute("course").equals(""))
			coursetf=new JTextField("10.000000");
		else 
			coursetf=new JTextField(getAttribute("course"));

		utcdt=new JLabel("Date (e.g., 191194 = 19.11.1994)");
		utcdtf=new JTextField("        ");
		utctt=new JLabel("UTC Time (e.g., 225446 = 22:54:46)");
		utcttf=new JTextField("        ");
		setDefaultTime();
		okb=new JButton("ok");
		okb.addActionListener(this);
		Container c=emuWindow.getContentPane();
		c.setLayout(new FlowLayout());
		c.add(latit);
		c.add(latitf);
		c.add(longit);
		c.add(longitf);
		c.add(speedt);
		c.add(speedtf);
		c.add(courset);
		c.add(coursetf);
		c.add(utcdt);
		c.add(utcdtf);
		c.add(utctt);
		c.add(utcttf);
		c.add(okb);
		//emuWindow.pack();
		emuWindow.show();
	}

	private void setDefaultTime()
	{
		Calendar d = Calendar.getInstance();
		//    Date d=new Date();
		String date = ""+getFieldAsString(d,Calendar.DATE)+getFieldAsString(d,Calendar.MONTH)+
			getFieldAsString(d,Calendar.YEAR);
		String time = ""+getFieldAsString(d,Calendar.HOUR_OF_DAY)+getFieldAsString(d,Calendar.MINUTE)+
			getFieldAsString(d,Calendar.SECOND);
		utcdtf.setText(date);
		utcttf.setText(time);
	}

	// Get fields as strings padded with leading zeros, chopped to two digits
	private String getFieldAsString(Calendar d, int field)
	{
		int val = d.get(field);
		String s = ("00"+val);
		return s.substring(s.length()-2);
	}

	/**
	* Emulation window events
	*/
	public void actionPerformed(ActionEvent e)
	{
		if (utcdtf.getText().length() == 0 &&
			utcttf.getText().length() == 0)
			setDefaultTime();

		Log.debug("GPS EMU Action performed");
		latitude = latitf.getText();
		longitude = longitf.getText();
		this.setAttribute("lat", latitude);
		this.setAttribute("long", longitude);
		this.setAttribute("speed", speedtf.getText());
		this.setAttribute("utcd", utcdtf.getText());
		this.setAttribute("utct", utcttf.getText());
		this.setAttribute("course", coursetf.getText());
		dispatch("gpsDataChanged");
		checkAreaChildren(latitf.getText(), longitf.getText());
	}


	/**
	* When GPS data is received, the attributes of this element change, 
	* and a DOM event is sent
	*/
	public void gpsDataReceived(String lat, String longi, String speed, String utcd, 
		String utct, String course)
	{
		latitude = lat;
		longitude = longi;
		this.setAttribute("lat", lat);
		this.setAttribute("long", longi);
		this.setAttribute("speed", speed);
		this.setAttribute("utcd", utcd);
		this.setAttribute("utct", utct);
		this.setAttribute("course", course);
		dispatch("gpsDataChanged");
		checkAreaChildren(lat, longi);
	}

	/**
	 * Check Area elements under this element.
	 */
	private void checkAreaChildren(String lat, String longi)  {
		// Check area elements under this element
		Node area;
		NodeList children = getChildNodes();
		for (int i=0 ; i < children.getLength() ; i++) {
			area = children.item(i);
			if (area instanceof AreaElementImpl) {
				((AreaElementImpl)area).isWithinArea(lat, longi);
			}
		}
	}

	/**
	 * Return latest latitude
	 */
	protected String getLatitude()  {
		return latitude;
	}

	/**
	 * Return latest longitude
	 */
	protected String getLongitude()  {
		return longitude;
	}

	/**
	* Destroy this element.
	*/
	public void destroy()
	{
		Log.debug("Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?Viddu destroyataanko t�t� elementtie� koskaan?");
		if(emu)
		{
			if(emuWindow!=null)
				emuWindow.dispose();
			emuWindow=null;
		}
		else
			gpsConnection.closeUp();
		gpsConnection=null;
	}
}


