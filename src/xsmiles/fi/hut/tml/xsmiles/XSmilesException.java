/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles;

/**
 * The base class for XSmiles Exceptions
 *
 * @author	Mikko Honkala
 * @version	$Revision: 1.1 $
 */
public class XSmilesException extends java.lang.Exception {
    
    /** constructs an exception without a message */
    public XSmilesException()
    {
        super();
    }
    
        /** constructs an exception without a message */
    public XSmilesException(String message)
    {
        super(message);
    }
}

