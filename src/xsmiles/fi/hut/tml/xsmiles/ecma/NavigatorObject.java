/* X-Smiles
 *
 */

package fi.hut.tml.xsmiles.ecma;
 
import java.util.*; 
import java.io.*;
import java.net.URL;
import java.net.MalformedURLException;

import org.w3c.dom.*; 
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Version;
import fi.hut.tml.xsmiles.BrowserWindow;

/**
 * The javascript 'navigator' object
 * @author	Mikko Honkala
 */ 
public class NavigatorObject extends JavascriptObject
{ 
    
  public NavigatorObject()
  {
  } 
  // from http://www.xs4all.nl/~ppk/js/detect.html

  public static String appVersion=Version.getVersion();
  public static String userAgent="X-Smiles/"+appVersion;
  public static String platform="Linux"; // TODO: read from java
  public static String appCodeName="X-Smiles"; 
  public static String appName="X-Smiles"; 
  public static String language="en-US"; // TODO: read from java 
  public static String[] mimeTypes=new String[]{"text/html","text/plain","application/xml"}; // TODO: read from x-smiles 
  public static String oscpu="Linux"; // TODO: read from java 
  public static String vendor="HUT"; 
  public static String vendorSub="TML"; 
  public static String product ="X-Smiles"; 
  public static String productSub =appVersion; 
  public static String cookieEnabled ="false"; // TODO: read from x-smiles 
  

}
