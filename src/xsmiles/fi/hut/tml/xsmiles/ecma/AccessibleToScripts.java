/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 14, 2004
 *
 */
package fi.hut.tml.xsmiles.ecma;


/**
 * All classes that implement this empty interface can be accessed by ECMAscripts
 * . Note that scripts still have to get the pointer somehow, either thru the document, window, etc.
 * 
 * For security reasons, do not expose unneccessary classes to scripts.
 * @author honkkis
 *
 */
public interface AccessibleToScripts
{
}
