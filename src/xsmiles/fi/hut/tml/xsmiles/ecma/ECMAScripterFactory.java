/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 17, 2004
 *
 */
package fi.hut.tml.xsmiles.ecma;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;


/**
 * @author honkkis
 *
 */
public class ECMAScripterFactory
{
    public static final String rhinoClass="fi.hut.tml.xsmiles.ecma.rhino.RhinoScripter";
    public static ECMAScripter getScripter(String scripter, BrowserWindow br)
    {
        if (scripter.equals("rhino")) 
        {
            try
            {
                return (ECMAScripter)Class.forName(rhinoClass).newInstance();
            } catch (Throwable t)
            {
                Log.error(t);
            }
        }
        return new DummyECMAScripter();
    }
    

}
