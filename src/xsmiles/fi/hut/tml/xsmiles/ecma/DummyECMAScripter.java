/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on May 17, 2004
 *
 */
package fi.hut.tml.xsmiles.ecma;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.BrowserWindow;


/**
 * @author honkkis
 *
 */
public class DummyECMAScripter implements ECMAScripter
{

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#getESEventBroker()
     */
    public ESEventBroker getESEventBroker()
    {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#initialize(org.w3c.dom.Document)
     */
    public void initialize(Document doc)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#isInitialized()
     */
    public boolean isInitialized()
    {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#destroy()
     */
    public void destroy()
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#eval(java.lang.String)
     */
    public void eval(String scriptText)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#exposeToScriptEngine(java.lang.String, java.lang.Object)
     */
    public void exposeToScriptEngine(String name, Object theObject)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.ecma.ECMAScripter#deleteExposedObject(java.lang.String, java.lang.Object)
     */
    public void deleteExposedObject(String name, Object theObject)
    {
        // TODO Auto-generated method stub
        
    }

    public void setBrowserWindow(BrowserWindow browser)
    {
        // TODO Auto-generated method stub
        
    }

}
