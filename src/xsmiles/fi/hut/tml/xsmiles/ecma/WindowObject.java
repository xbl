/* X-Smiles
 *
 */

package fi.hut.tml.xsmiles.ecma;
 
import java.awt.EventQueue;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.dom.AsyncChangeHandler;

/**
 * The javascript 'window' object
 * @author	Mikko Honkala
 */ 
public class WindowObject extends JavascriptObject
{ 
    BrowserWindow browserWindow;
    Scripter scripter;     
    Timer timer;
  public WindowObject()
  {
  } 
  
  public void setBrowserWindow(BrowserWindow w)
  {
      this.browserWindow=w;
  }
  public void setScripter(Scripter s)
  {
      this.scripter=s;
  }
  
  protected Hashtable tasks;
  // will return the id to the timer
  public String setInterval(String o1, Integer o2)
  {
      Log.debug("window.setInterval called. "+o1+"  : "+o2);
      Timer t=this.getTimer();
      ScriptTimerTask task = new ScriptTimerTask(o1,scripter);
      t.schedule(task, o2.longValue(),o2.longValue());
      String id = ""+t.hashCode();
      this.tasks.put(id,task);
      return id;
      
      //scripter.eval(o1);
  }
  public void clearInterval(String id)
  {
      if (this.tasks==null)return;
      ScriptTimerTask task = (ScriptTimerTask)this.tasks.get(id);
      if (task!=null) task.cancel();
  }
  
  Timer getTimer()
  {
      if (timer==null) timer=new Timer();
      if (tasks==null) tasks=new Hashtable();
      return timer;
  }
  
  //protected Vector timers;
  
  class ScriptTimerTask extends TimerTask
  {
      Scripter sc;
      String call;
      	public ScriptTimerTask(String function, Scripter scripter)
      	{
      	    this.call=function;
      	    this.sc=scripter;
      	}
      	public void run()
      	{
      	    if (sc!=null&&call!=null)
            {
                Runnable r = new Runnable()
                {
                    public void run()
                    {
                        scripter.eval(call);
                    }
                };
                /* should do this
                if (this.getOwnerDocument() instanceof AsyncChangeHandler)
                {
                    ((AsyncChangeHandler)this.getOwnerDocument()).invokeLater(r);
                }
                else
                {*/
                    EventQueue.invokeLater(r);
               // } 
            }
      	}
  }
  
  void destroyTimers()
  {
      if (timer!=null) timer.cancel();
   /*   Enumeration e =timers.elements();
      while (e.hasMoreElements())
      {
          Timer t = (Timer)e.nextElement();
          t.cancel();
      }*/
  }
  
  
  void destroy()
  {
      destroyTimers();
  }

}

