/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Feb 21, 2006
 *
 */
package fi.hut.tml.xsmiles.ecma;


public interface ScriptRunner
{
    public void eval(String scriptText);
    /**
     * Expose a java object to javascript as javascript object
     */
    public void exposeToScriptEngine(String name,Object theObject);
    /**
     * Remove a previously exposed object
     */
    public void deleteExposedObject(String name,Object theObject);
}
