/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.ecma;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.BrowserWindow;

/**
 * EcmaScript interface that all ecma script engines must implement.
 * A stub implementation of the ECMAScripter.
 *
 *
 * @author	Mikko Honkala
 * @author	Jukka Heinonen
 * @version	$Revision: 1.17 $
 */
public interface ECMAScripter
{ 
    public void setBrowserWindow(BrowserWindow browser);
    public ESEventBroker getESEventBroker();
    
    

   /**
    * Resets the ECMAScript interpreter. The funtion is called from the class
	* XMLDocument. It creates a new DOMHandler instance, which is not yet given a 
	* document. It also creates a new ecma interpreter
    */
    public void initialize(Document doc);
	public boolean isInitialized();

    public void destroy();

    /*
			Evaluate script text and change DOM if needed      
    */
    public void eval(String scriptText);
	/**
	 * Expose a java object to javascript as javascript object
	 */
    public void exposeToScriptEngine(String name,Object theObject);
    /**
     * Remove a previously exposed object
     */
    public void deleteExposedObject(String name,Object theObject);
	
    
}
