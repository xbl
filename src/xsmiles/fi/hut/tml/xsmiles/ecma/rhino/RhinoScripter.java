/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



/*
 * X-smiles project 1999
 *
 * $Id: RhinoScripter.java,v 1.4 2006/05/23 11:12:31 honkkis Exp $
 *
 */


package fi.hut.tml.xsmiles.ecma.rhino;
import org.mozilla.javascript.*;


/**
 * An rhino implementation of the ECMAScripter.
 *
 *
 * @author  Mikko Honkala
 * @author	Jukka Heinonen
 * @version	$Revision: 1.4 $
 */


import java.util.*;
import java.io.*;
import org.w3c.dom.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.ecma.ESEventBroker;
import fi.hut.tml.xsmiles.ecma.Scripter;

public class RhinoScripter extends Scripter implements ECMAScripter, ClassShutter
{
    //Context context;
    Scriptable scope;
    private static int entered=0;
    protected XSecurityController securityController=new XSecurityController();
    protected XSmilesWrapFactory wrapFactory = new XSmilesWrapFactory(this);
    //BrowserWindow browser;
    

    /**
     * Constructor
     *
     * @param browser The <code>Browser</code> object
     */
    public RhinoScripter(BrowserWindow browser)
    {
        super(browser);
        Log.debug("%%%% RHINOSCRIPTER Created");
        //this.browser=browser;
    }
    
    public RhinoScripter()
    {
        super();
        Log.debug("%%%% RHINOSCRIPTER Created");
        //this.browser=browser;
    }
    
    
    /**
     * Resets the ECMAScript interpreter. The funtion is called from the class
     * XMLDocument. It creates a new DOMHandler instance, which is not yet given a
     * document. It also creates a new ecma interpreter
     */
    public void initialize(Document doc)
    {
        //        doc=null;
        Context context = Context.enter();
        try
        {
            Log.debug("Context entered: "+(++entered));
            //	UNCOMMENT WHEN RHINO 1.5R2 arrives
            context.setClassShutter(this);
            context.setSecurityController(securityController);
            context.setWrapFactory(wrapFactory);
            context.setOptimizationLevel(-1);
            scope = context.initStandardObjects(null);
            Log.debug("RhinoScripter.reset(), scope: "+scope.hashCode());
        } catch (Exception e)
        {
            Log.error("making of Rhino interpreter failed");
            Log.error(e);
        }
        ecmabroker = new ESEventBroker(this);
        this.createBasicObjects();
        this.setDocument(doc);
        if (context!=null)
        {
            context.exit();
            Log.debug("Context exited: "+(--entered));
        }
        super.initialize(doc);
    }
    
    public void createBasicObjects()
    {
        super.createBasicObjects();
        // TODO create the alert function
        try
        {
            Class[] args=
            {
                Class.forName("java.lang.String")
            };
            getScope().put("alert", getScope(),
            new FunctionObject(
            "alert",
            browserObject.getClass().getMethod("alert",args),
            getScope()));
        } catch (Exception e)
        {
            Log.error(e);
        }
    }
    
    public static int count=0;
    public void exposeToScriptEngine(String name,Object theObject)
    {
        count++;
        Log.debug("RhinoScripter about to expose '" + name + "' to script engine. count: "+count+"scope: "+scope.hashCode());
        Context context=Context.enter();
        Scriptable jsArgs = context.toObject(theObject,getScope());
        getScope().put(name, getScope(), jsArgs);
        context.exit();
    }
    public void deleteExposedObject(String name,Object theObject)
    {
        count--;
        //    Log.debug("RhinoScripter about to delete '" + name + "' to script engine. count:"+count);
        //    Scriptable jsArgs = Context.enter().toObject(theObject,getScope());
        if (getScope()!=null)
            getScope().delete(name);
        //	Context.exit();
    }
    public Scriptable getScope()
    {
        return scope;
    }
    //  public Context getContext()
    //  {
    //    return context;
    //  }
    
    
    
    
  /*
    Evaluate script text and change DOM if needed
   */
    public void eval(String scriptText)
    {
        
        String writeToDocument;
        try
        {
            Context context=Context.enter();
            //	context=getContext();
            Log.debug("Context entered: "+(++entered));
            
            try
            {
                context.setClassShutter(this);
                context.setWrapFactory(wrapFactory);
                context.setSecurityController(securityController);
                context.setOptimizationLevel(-1);
            } catch (java.lang.SecurityException e)
            {
                Log.error("** Security exception, continueing:"+e.getMessage());
            }
            //Log.debug("RhinoScripter evaluating function: "+scriptText);
            Object result = context.evaluateString(scope, scriptText, "<cmd>", 1, null);
        } catch (Exception e)
        {
        	String errortext="** JAVASCRIPT ERROR: evaluating '" + scriptText + "'"+e.getMessage();
            Log.error(errortext);
            this.browser.getCurrentGUI().setStatusText(errortext);
        }
        finally
        {
            Context.exit();
            Log.debug("Context exited: "+(--entered));
            
        }
        
        
    }
    
    public Class defineClass(String name, byte[] data, Object securityDomain)
    {
        //		Log.debug("*** defineClass "+name+":"+securityDomain);
        return null;
    }
    
    
    public Class[] getClassContext()
    {
        //		Log.debug("*** getClassContext");
        return null;
    }
    public Object getSecurityDomain(Class cl)
    {
        //		Log.debug("*** getSecurityDomain "+cl);
        return null;
    }
    public boolean visibleToScripts(String fullClassName)
    {
        //		Log.debug("*** visibleToScripts: "+fullClassName);
        return false;
    }
    
    
    
}
