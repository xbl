/*
 * /* X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources. Created on May 13, 2004
 *  
 */
package fi.hut.tml.xsmiles.ecma.rhino;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import org.mozilla.javascript.Callable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.GeneratedClassLoader;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.SecurityController;
import fi.hut.tml.xsmiles.util.java2.SecurityZones;
/**
 * @author honkkis
 *  
 */
public class XSecurityController extends SecurityController {
	public GeneratedClassLoader createClassLoader(ClassLoader parentLoader,
			Object securityDomain) {
		throw new RuntimeException("Not supported");
	}
	public Object getDynamicSecurityDomain(Object securityDomain) {
		// TODO: Hmm.. First tried with lowest permissions, but did not work.
		// if a script changes the XML dom and an image is moved or created,
		// it must be re-read from file... what about HTTP etc... 
		return SecurityZones.getInstance().getScriptContext().getContext();
	}
	public Object callWithDomain(Object securityDomain, final Context cx,
			final Callable callable, final Scriptable scope,
			final Scriptable thisObj, final Object[] args)
			throws JavaScriptException {
		try {
			AccessController.doPrivileged(new PrivilegedExceptionAction() {
				public Object run() throws JavaScriptException {
					return callable.call(cx, scope, thisObj, args);
				}
			}, (AccessControlContext) securityDomain);
		} catch (PrivilegedActionException ex) {
			throw (JavaScriptException) ex.getException();
		}
		return null;
	}
}