/*

X-Smiles
*/

package fi.hut.tml.xsmiles.ecma.rhino;

import java.util.Hashtable;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.WrapFactory;
import org.w3c.dom.events.EventTarget;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.ecma.AccessibleToScripts;

/**
 * This is an utility class allowing to pass an ECMAScript function
 * as a parameter of the <code>addEventListener</code> method of
 * <code>EventTarget</code> objects as DOM Level 2 recommendation
 * required.
 * @author <a href="mailto:cjolif@ilog.fr">Christophe Jolif</a>
 * @version $Id: XSmilesWrapFactory.java,v 1.2 2004/05/17 14:33:23 honkkis Exp $
 */
public class XSmilesWrapFactory extends WrapFactory {
    private RhinoScripter interpreter;
    static Hashtable knownClasses=new Hashtable(30);

    public XSmilesWrapFactory(RhinoScripter interp) {
        interpreter = interp;
        setJavaPrimitiveWrap(false);
    }

    public Object wrap(Context ctx, Scriptable scope,
                       Object obj, Class staticType) {
        /* later, copy event target code from batik in order to support javascript event handlers
        if (obj instanceof EventTarget) {
            return interpreter.buildEventTargetWrapper((EventTarget)obj);
        }*/
        //Log.debug("Trying to wrap: "+obj);
        if (
                (obj==null) ||
                (obj instanceof AccessibleToScripts) ||
                (obj.getClass().getName().startsWith("java.lang")) ||  // to support java.lang.Integer etc.
                (isDOMObject(obj))
                )
        {
            //Log.debug("It is ok to wrap: "+obj);
            return super.wrap(ctx, scope, obj, staticType);
        }
        throw new RuntimeException("ECMAScript error: object :"+obj+" is not accessible by scripts");
    }
    /** security: only wrap java.lang.* and org.w3c.dom.* objects to scripts
     * 
     * @param obj
     * @return
     */
    private boolean isDOMObject(Object obj)
    {
        if (obj instanceof  org.w3c.dom.Node) return true;
        if (knownClasses.contains(obj.getClass())) return true;
        if (isDOMClass(obj.getClass()))
        {
            this.knownClasses.put(obj.getClass(),obj.getClass());
            return true;
        }
        return false;
    }
    private boolean isDOMClass(Class c)
    {
        Class[] interfaces = c.getInterfaces();
        if (interfaces!=null)
        {
	        for (int i=0;i<interfaces.length;i++)
	        {
	            String interfaceStr = interfaces[i].getName();
	            //Log.debug("Checking interface:"+interfaceStr);
	            if (interfaceStr.startsWith("org.w3c.dom"))
	            {
	                return true;
	            }
	        }
        }
        Class superclass= c.getSuperclass();
        if (superclass!=null) return isDOMClass(superclass);
        return false;
    }
}
