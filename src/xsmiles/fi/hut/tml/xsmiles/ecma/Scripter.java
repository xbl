/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */



/*
 * X-smiles project
 *
 * $Id: Scripter.java,v 1.15 2006/05/23 11:12:49 honkkis Exp $
 * @author	Mikko Honkala
 *
 */


package fi.hut.tml.xsmiles.ecma;

import java.util.*;
import java.io.*;
import org.w3c.dom.*;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;

/**
 * Superclass for EcmaScripter implementations. Contains method for
 * finding and executing script text within <script> elements in the
 * document.
 * @author Mikko Honkala
 */

public abstract class Scripter
{
    protected BrowserWindow browser;
    //  protected Document doc;
    protected ESEventBroker ecmabroker;
    protected BrowserObject browserObject;
    protected WindowObject windowObject;
    protected static NavigatorObject navigatorObject=new NavigatorObject();
    protected boolean initialized=false;
    /**
     * Constructor
     *
     * @param browser The <code>Browser</code> object
     */
    
    public Scripter(BrowserWindow browser)
    {
        Log.debug("Settign browser to : " + browser);
        this.browser=browser;
    }
    public Scripter()
    {
    }
    public void setBrowserWindow(BrowserWindow browser)
    {
        Log.debug("Settign browser to : " + browser);
        this.browser=browser;
    }
    public void initialize(Document doc)
    {
        if (initialized==true)
        {
            throw new RuntimeException("Scripter initialize called twice!");
        }
        initialized=true;
    }
    public boolean isInitialized()
    {
        return this.initialized;
    }
    protected void createBasicObjects()
    {
        // TODO: create browser object + alert function
        if (this.browserObject==null) browserObject  = new BrowserObject(browser);
        if (this.windowObject==null)
        {
            windowObject  = new WindowObject();
            windowObject.setBrowserWindow(browser);
            windowObject.setScripter(this);
        }
        exposeToScriptEngine("window",windowObject);
        exposeToScriptEngine("browser",browserObject);
        exposeToScriptEngine("navigator",navigatorObject);
    }
    public void destroy()
    {
        deleteExposedObject("window",null);
        if (windowObject!=null)
            windowObject.destroy();
        
        deleteExposedObject("browser",null);
        deleteExposedObject("navigator",null);
        deleteExposedObject("document",null);
        deleteExposedObject("alert",null);
    }
    
    
    public ESEventBroker getESEventBroker()
    {
        return ecmabroker;
    }
    
    
    /**
     * Asettaa DOMHandlerille documentin.
     * Privaatti metodi, jota kutsutaan siin? vaiheessa,
     * kun SCRIPT-tagien sis?lt?? aletaan tulkata.
     *
     */
    public void setDocument(Document document)
    {
        //    doc=document;
        exposeToScriptEngine("document",document);
    }
    
    public void exposeToScriptEngine(String name,Object theObject)
    {
        Log.error("Scripter.exposeToScriptEngine method should be overridden");
    }
    public void deleteExposedObject(String name,Object theObject)
    {
        Log.error("Scripter.deleteExposedObject method should be overridden");
    }
    
    
    
    
    public void eval(String scriptText)
    {
        Log.error("Scripter.eval method should be overridden by: "+this.getClass());
    }
}
