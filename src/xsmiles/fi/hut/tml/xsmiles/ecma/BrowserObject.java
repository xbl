/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 *
 *
 * Copyright (c) 1999-2001 Helsinki University of Technology.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Telecommunications Software and Multimedia Laboratory, Helsinki
 *	  University of Technology (http://www.tml.hut.fi/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "HUT" and "Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact info@xsmiles.org.
 *
 * 5. Products derived from this software may not be called "X-Smiles",
 *    nor may "X-Smiles" appear in their name, without prior written
 *    permission of The Telecommunications Software and Multimedia 
 *    Laboratory, Helsinki University of Technology
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE TELECOMMUNICATIONS SOFTWARE AND
 * MULTIMEDIA LABORATORY, HELSINKI UNIVERSITY OF TECHNOLOGY OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 */

package fi.hut.tml.xsmiles.ecma;
 
import java.util.*; 
import java.io.*;
import java.net.URL;
import java.net.MalformedURLException;

import org.w3c.dom.*; 
import fi.hut.tml.xsmiles.NavigationState;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.BrowserWindow;

/**
 * The javascript 'browser' object
 * @author	Mikko Honkala
 */ 
public class BrowserObject extends JavascriptObject
{ 
  protected BrowserWindow browser;
  
  private static BrowserObject cacheObject;

    
  public BrowserObject(BrowserWindow browser)
  {
    this.browser=browser; 
    cacheObject=this;
  } 
  
  public static void alert(String text)
  {
    //JOptionPane.showMessageDialog(null, "[ECMAScript] "+text);
    cacheObject.browser.showErrorDialog("ECMAScript alert", 
			      text, true);
  }
  
  public static void print(String text)
  {
    System.out.print(text);
  }

  public static void println(String text)
  {
    System.out.println(text);
  }
  

  /**
   * @return Returns a Vector which contains the stylesheet titles
   * @see changeStylesheet(String name)
   */
  public Vector getStylesheetTitles() {
    if(browser.getXMLDocument().getStylesheetTitles()==null) {
      return new Vector();
    }
    else 
      return browser.getXMLDocument().getStylesheetTitles();
  }
  

  /**
   * Change stylesheet to a stylesheet, whoose title is 
   * @param name The title of the stylesheet to change to
   */
  public void changeStylesheet(String name)
  {
    Log.debug("change stylesheet called");
    browser.setPreferredStylesheetTitle(name);
    //browser.getBrowserConfigurer().setGUIProperty(browser,"preferredStylesheet",name);
    // Tell browser window to reload
    browser.navigate(NavigationState.RELOAD);
  }


  /**
   * @return a Vector of gui names
   */
  public Vector getGUINames() {
    return browser.getGUIManager().getGUINames();
  }
  
    /**
   * @return a Vector of gui names
   */
  public String getDefaultGUIName() {
    return browser.getDefaultGUIName();
  }
  
  /**
   * Type of GUI to change to
   */
  public void changeGUI(String name) {
	if (browser.getGUIManager().getCurrentGUIName().equals(name) == false)
	    browser.getGUIManager().showGUI(name);
  }
  
  public void reload()
  {
      browser.navigate(NavigationState.RELOAD);
  }
  public void navigate(String url)
  {
    try
      {
	browser.openLocation(new URL(url), true);
      } catch (MalformedURLException e)
	{
	  Log.error(e);
	}
  }
  //	public void 
	
    
    
    

}
