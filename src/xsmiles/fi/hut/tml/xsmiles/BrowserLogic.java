/* X-Smiles
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1), and blaa, blaa,
 * blaah, blaah. Check the file that comes with this distribution for full 
 * legal disclaimer.
 *
 */
package fi.hut.tml.xsmiles;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.gui.GUI;
import fi.hut.tml.xsmiles.gui.Language;

/**
 * Not only contains enumeration, but also browser logic, on what is done when
 * state changes.
 * 
 * FIXME! BrowserLogic could contain timeouts when transitioning from one state
 * to another..
 * 
 * The "enumeration" values for the browser state (of type int).
 * 
 * @author Juha
 * @author (Jukka Heinonen)
 */
public class BrowserLogic {

    public final static int INITIALIZINGBROWSER = 0;
    public final static int RETRIEVINGDOCUMENT = 1;
    public final static int RETRIEVINGCOMPONENT = 2;
    public final static int INITIALIZINGCOMPONENT = 3;
    public final static int RETRIEVINGCONTENTDATA = 4;
    public final static int DISPLAYINGCONTENT = 7;
    public final static int STOPPED = 6;
    public final static int READY = 5;
    public final static int ERROROCCURED = 8;
    public final static int RETRIEVINGERRORDOCUMENT = 9;
    public final static int RETRIEVINGSECONDARYDOCUMENT = 10;
    public final static int SHUTTINGDOWN = 11;
    public final static int NOT_STOPPABLE = 12;
    public final static int ACTIVATING_MLFC = 13;
    public final static int INITIALIZING_MLFC = 14;

    private final static Hashtable stateStrings = new Hashtable();
    private BrowserWindow browser /* @ non_null */;
    private int state, previousState;
    private String stateString = "";
    private GUI gui /* @ non_null */;
    private DummyGUI eventGUI;

    public BrowserLogic(BrowserWindow b) {
        browser = b;
        eventGUI = new DummyGUI(browser.getEventBroker());
        stateStrings.put(new Integer(INITIALIZINGBROWSER), Language.INITIALIZINGBROWSER);
        stateStrings.put(new Integer(RETRIEVINGDOCUMENT), Language.RETRIEVINGDOCUMENT);
        stateStrings.put(new Integer(RETRIEVINGERRORDOCUMENT), Language.RETRIEVINGERRORDOCUMENT);
        stateStrings.put(new Integer(RETRIEVINGCOMPONENT), Language.RETRIEVINGCOMPONENT);
        stateStrings.put(new Integer(INITIALIZINGCOMPONENT), Language.INITIALIZINGCOMPONENT);
        stateStrings.put(new Integer(RETRIEVINGCONTENTDATA), Language.RETRIEVINGCONTENTDATA);
        stateStrings.put(new Integer(READY), Language.READY);
        stateStrings.put(new Integer(STOPPED), Language.STOPPED);
        stateStrings.put(new Integer(ERROROCCURED), Language.ERROROCCURED);
        stateStrings.put(new Integer(RETRIEVINGSECONDARYDOCUMENT), Language.RETRIEVINGSECONDARYDOCUMENT);
        stateStrings.put(new Integer(RETRIEVINGERRORDOCUMENT), Language.RETRIEVINGERRORDOCUMENT);
        stateStrings.put(new Integer(SHUTTINGDOWN), Language.SHUTTINGDOWN);
    }

    /**
     * @param Change
     *                   state without any description of state change.
     */
    public void setState(int s) {
        setState(s, "");
    }

    /**
     * @param s
     *                   Change the state of the browser.
     * @param text
     *                   A descriptor of what is happening can be used to pass state
     *                   change description related strings. (not mandatory)
     */
    public void setState(int s, String text) {
        if (text == null)
            text = "";

        if (s == INITIALIZINGBROWSER)
            Log.debug("Browser Initializing...");
        else
            handleStateChange(s, text);
    }

    private static int iter = 0;

    private void handleStateChange(int s, String text) {
        gui = browser.getCurrentGUI();
        eventGUI.setRealGUI(gui);

        switch (s) {
        case STOPPED:
            eventGUI.browserReady();
            stateString = "Stopped";
            eventGUI.setEnabledStop(false);
            break;
        case RETRIEVINGDOCUMENT:
            stateString = Language.RETRIEVINGDOCUMENT + " " + text;
            eventGUI.setLocation(text);
            gui.getMLFCControls().getMLFCToolBar().removeAll();
            eventGUI.setEnabledStop(true);
            long totalmem = Runtime.getRuntime().totalMemory();
            long allmem = totalmem - Runtime.getRuntime().freeMemory();
            Log.debug((iter++) + "th Iteration. Total mem:" + totalmem + " Allocated mem: " + allmem + " (" + allmem / 1000000 + " MB)");

            eventGUI.browserWorking();

            break;
        case RETRIEVINGERRORDOCUMENT:

            eventGUI.browserWorking();
            //statustext has already been updated, when error occured.
            break;
        case ERROROCCURED:
            eventGUI.browserReady();
            stateString = Language.ERROROCCURED + " " + text;
            break;
        case RETRIEVINGSECONDARYDOCUMENT:
            eventGUI.browserWorking();
            stateString = Language.RETRIEVINGSECONDARYDOCUMENT + " " + text;
            break;
        case SHUTTINGDOWN:
            Log.debug("Browser shutting down");
            break;
        case NOT_STOPPABLE:
            eventGUI.setEnabledStop(false);
            break;
        case INITIALIZING_MLFC:
            stateString = Language.INITIALIZING_MLFC;
            break;
        case ACTIVATING_MLFC:
            stateString = Language.ACTIVATING_MLFC;
            break;
        case READY:

            eventGUI.browserReady();
            stateString = Language.READY + " (" + browser.getDocumentLoadDuration() + " secs)";
            eventGUI.setEnabledStop(false);
            if (gui != null) {
                java.awt.Window window = gui.getWindow();
                if (window != null) {
                    window.invalidate();
                    window.validate();
                    window.doLayout();
                }
            }
            browser.getContentArea().doLayout();
            break;
        }
        previousState = state;
        state = s;

        eventGUI.setStatusText(stateString);
        browser.getEventBroker().issueBrowserStateChangedEvent(s, stateString);
        // Inform all possible listeners, such as the GUI, that the state has
        // changed.
    }

    /**
     * ?@return The current state of the browser
     */
    public int getState() {
        return state;
    }

    /**
     * Gets the state string corresponging to the integer value of the browser
     * state.
     * 
     * @return state string
     */
    public String getStateString(int i) {
        return (String) stateStrings.get(new Integer(state));
    }

    /**
     * Send GUI events to EventBroker, and forward them to the current GUI if
     * one exists.
     */
    private class DummyGUI {

        private EventBroker e;
        private GUI realGUI = null;

        // realGUI is never null, because there is always atleast a NullGUI

        public DummyGUI(EventBroker d) {
            e = d;
        }

        public void setRealGUI(GUI g) {
            realGUI = g;
        }

        public void browserWorking() {
            e.browserWorking();
            realGUI.browserWorking();
        }

        public void browserReady() {
            e.browserReady();
            realGUI.browserReady();
        }

        public void setLocation(String s) {
            e.setLocation(s);
            realGUI.setLocation(s);
        }

        public void setStatusText(String statusText) {
            e.setStatusText(statusText);
            realGUI.setStatusText(statusText);
        }

        public void setEnabledBack(boolean value) {
            realGUI.setEnabledBack(value);
        }

        public void setEnabledForward(boolean value) {
            realGUI.setEnabledForward(value);
        }

        public void setEnabledHome(boolean value) {
            realGUI.setEnabledHome(value);
        }

        public void setEnabledStop(boolean value) {
            realGUI.setEnabledStop(value);
        }

        public void setEnabledReload(boolean value) {
            realGUI.setEnabledReload(value);
        }

        public void setTitle(String title) {
            realGUI.setTitle(title);
        }
    }
}
