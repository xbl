/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles;

//import fi.hut.tml.xsmiles.gui.console.Console; // Console is swing-dependent, use reflection

import java.util.StringTokenizer;

import java.io.File;

import java.net.URL;

import java.lang.reflect.*;


/**
 *
 * @author  Mikko Honkala
 * @version minor re-structure.
 * @version	$Revision: 1.13 $
 */
public class Utilities
{
    private static double javaVersion=-1;
    /**
     * Open the Log window, to output debug data, using static method.
     */
    public static void openStaticLog()
    {
        // console is swing dependant, use reflection
        try
        {
            Class consoleClass = Class.forName("fi.hut.tml.xsmiles.gui.console.Console");
            Method isOpen = consoleClass.getDeclaredMethod("isOpen", null);
            Boolean ret = (Boolean)isOpen.invoke(null,null);
            if (ret.booleanValue()==true) return;
            Object consoleObj = consoleClass.newInstance();
            Method hookStandards = consoleClass.getDeclaredMethod("hookStandards", null);
            hookStandards.invoke(consoleObj,null);
            Log.reHookStandards();
        } catch (Throwable t)
        {
            Log.error(t,"NON FATAL: Must be without swing, since console cannot be instantiated.");
        }
    }
    
    
    /**
     * Open the Log window, to output debug data, using static method.
     */
    public static void closeStaticLog()
    {
        // console is swing dependant, use reflection
        try
        {
            Class consoleClass = Class.forName("fi.hut.tml.xsmiles.gui.console.Console");
            Method isOpen = consoleClass.getDeclaredMethod("isOpen", null);
            Boolean ret = (Boolean)isOpen.invoke(null,null);
            if (ret.booleanValue()==true)
            {
                Method hookStandards = consoleClass.getDeclaredMethod("closeWindow", null);
                hookStandards.invoke(null,null);
            }
        } catch (Throwable t)
        {
            Log.error(t,"NON FATAL: Must be without swing, since console cannot be closed.");
        }
    }
    
    public static double getJavaVersion()
    {
        if (javaVersion>0) return javaVersion;
        else
        {
            String verstr = System.getProperty("java.version").substring(0,3);
            if (verstr.equalsIgnoreCase("J2M"))
            {
                javaVersion=1.1;
                return javaVersion;
            }
            javaVersion = new Double(verstr).doubleValue();
            if ( javaVersion > 1.1 )
            {
                try
                {
                    Class classTest = Class.forName("java.awt.geom.AffineTransform");
                } catch (ClassNotFoundException e)
                {
                    javaVersion = 1.1;
                }
            }
            return javaVersion;
        }
    }
    
    /** returns true, if this is an embedded system, such personal profile on IBM j9 */
    public static boolean isEmbedded()
    {
    	try
    	{
			String verstr = System.getProperty("java.vm.name");
			if (verstr.equalsIgnoreCase("J9")) return true;
    	} catch (Throwable t)
    	{
    	}
    	return false;
    	
    }
    
    /**
     * tests html white space
     * http://www.w3.org/TR/html401/struct/text.html#h-9.1
     *    * ASCII space (&#x0020;),
     * ASCII tab (&#x0009;),
     * ASCII form feed (&#x000C;),
     *carriage return (&#x000D;), a line feed (&#x000A;),
     * Zero-width space (&#x200B;)
     *
     */
    public static boolean isHTMLWhiteSpace(char ch)
    {
        // New approach. Following are iso-latin-1 white spaces.
        return (ch <= 0x0020) &&
        (((((1L << 0x0009) |
        (1L << 0x000A) |
        (1L << 0x000C) |
        (1L << 0x000D) |
        (1L << 0x0020)) >> ch) & 1L) != 0);
        
/*        return
            ((((
            // how to check this efficiently? FastJavac reports errors for the following:
            //  Left shift exceeds size of the type being shifted.
            //(1L << 0x200B) |
            (1L << 0x0009) |
            (1L << 0x000A) |
            (1L << 0x000C) |
            (1L << 0x000D) |
            (1L << 0x0020)) >> ch) & 1L) != 0);
 */
    }
    
    public static String trimHTMLContent(String value)
    {
        if (value==null) return null;
        if (value.length()<1) return value;
        String text = "";
        StringTokenizer token = new StringTokenizer(value, "\n\t\r\f ", false);
        if (!token.hasMoreTokens())
            text = " ";
        else
        {
            while (token.hasMoreTokens())
            {
                text = text + token.nextToken() + " ";
            }
        }
        return text;
    }
            
    public static String prelineHTMLContent(String value)
    {
        if (value==null) return null;
        if (value.length()<1) return value;
        String text = "";
        StringTokenizer token = new StringTokenizer(value, "\t\r\f ", false);
        if (!token.hasMoreTokens())
            text = " ";
        else
        {
            while (token.hasMoreTokens())
            {
                text = text + token.nextToken() + " ";
            }
        }
        return text;
    }
    
    public static URL toURL(File file) throws java.net.MalformedURLException
    {
        String path = file.getAbsolutePath();
        if (File.separatorChar != '/')
        {
            path = path.replace(File.separatorChar, '/');
        }
        if (!path.startsWith("/"))
        {
            path = "/" + path;
        }
        if (!path.endsWith("/") && file.isDirectory())
        {
            path = path + "/";
        }
        return new URL("file", "", path);
    }
    
    public static void setJavaVersion(double ver)
    {
        Log.info("Java version programmatically set to : "+ver);
        javaVersion=ver;
    }
    
}


