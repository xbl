/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 * The rest of the license available at the web site or under /bin
 */

package fi.hut.tml.xsmiles;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.net.URL;
import java.net.MalformedURLException;

// import fi.hut.tml.xsmiles.gui.console.*;
// import fi.hut.tml.xsmiles.gui.gui2.swing.KickStartSwing;
import fi.hut.tml.xsmiles.protocol.XURLStreamHandlerFactory;
import fi.hut.tml.xsmiles.protocol.http.XHttpURLConnection;
import fi.hut.tml.xsmiles.protocol.socket.REXSocketListener;
import fi.hut.tml.xsmiles.protocol.socket.SLPSocketListener;
import fi.hut.tml.xsmiles.protocol.socket.SoapSocketListener;
import fi.hut.tml.xsmiles.protocol.socket.SocketListener;
import fi.hut.tml.xsmiles.protocol.socket.UrlSocketListener;
import fi.hut.tml.xsmiles.protocol.wesahmi.BrowserSubscriber;

import fi.hut.tml.xsmiles.util.java2.ApplicationSecurityEnforcer;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.xslt.XSLTEngine;

// for frame test
import java.awt.*;
import java.awt.event.*;

// startup dialog
// import fi.hut.tml.xsmiles.gui.components.swing.InitDialog;

/**
 * This is the main class of the program. reads the parameters and instantiates
 * one BrowserWindow with the default GUI.
 * 
 * Giving an argument sends the debug to that file TR 12.4.2000 (change done by
 * Farooq)
 * 
 * @author Aki Teppo
 * @version $Revision: 1.68 $
 */
public class Xsmiles
{
    protected static String initDialogClassname = "fi.hut.tml.xsmiles.gui.components.swing.InitDialog";

    protected static Vector browserWindows;

    protected static Xsmiles instance;

    protected String initialURLStr;

    protected File configFile;

    protected SocketListener socketListener;

    /*
     * public static void main(String args[]) { KickStartSwing.main(args);
     * //instance = new Xsmiles(); //instance.initSequence(args); }
     */
    public void initSequence(String args[])
    {
        Log.info("-*-=XSmiles ver " + Browser.version + " starting=-*-");
        Log.info("Java version: " + System.getProperty("java.version"));
        // setSystemProperties(); //did not work _MH
        browserWindows = new Vector();
        String initialURL = null;
        boolean showsplash = true;
        try
        {
            int i = 0;
            while (i < args.length)
            {
                String trimmed = args[i].trim();
                i++;
                if (trimmed != null)
                {
                    if (trimmed.equalsIgnoreCase("-console")
                            || trimmed.equalsIgnoreCase("-log"))
                    {
                        Utilities.openStaticLog();
                    }
                    else if (trimmed.equalsIgnoreCase("-nosplash"))
                    {
                        showsplash = false;
                    }
                    else if (trimmed.equalsIgnoreCase("-config"))
                    {
                        if (i < args.length)
                        {
                            String trimmedArg = args[i].trim();
                            i++;
                            // configFile = new File(trimmedArg);
                            Resources.addResourceURL("xsmiles.config",
                                    trimmedArg);
                            Log.info("Using config file in: "
                                    + Resources
                                            .getResourceURL("xsmiles.config"));
                        }
                    }
                    else
                    {
                        initialURLStr = trimmed;

                    }

                }
            }
        }
        catch (Exception e)
        {
            Log.error(e);
        }

        // Create class loader to load X-Smiles classes such as AAML
        try
        {
            MLFCLoader.createClassLoader();
        }
        catch (Throwable e)
        {
            Log.error(e);
        }

        // AAML kludgetus initialization. Performed here, otherwise DirectSound
        // and EAX
        // cannot be initialized successfully.
        initializeAAML();

        Frame initDialog = null;
        try
        {
            // Initialization
            // About dialog

            if (!webstart() && showsplash)
            {
                initDialog = (Frame) Class.forName(initDialogClassname)
                        .newInstance();
                // initDialog= new InitDialog("test");
            }
        }
        catch (Throwable t)
        {
            Log.debug("Could not init initDialog." + t.getClass().toString()
                    + " : " + t.getMessage());
            Log.error(t);
        }

        try
        {
            // Create a new instance of our application's frame, and make it
            // visible.
            java.net.URL
                    .setURLStreamHandlerFactory(new XURLStreamHandlerFactory());
        }
        catch (java.security.AccessControlException a)
        {
            System.err.println(a);
            a.printStackTrace();
        }
        catch (Throwable t)
        {
            System.err.println(t);
            t.printStackTrace();
        }
        if (initialURLStr != null)
        {
            try
            {
                URL u = new URL(Utilities.toURL(new File(".")), initialURLStr);

                initialURL = u.toString();
            }
            catch (MalformedURLException e)
            {
                Log.error(e);
            }
            catch (Throwable t)
            {
                Log.error(t);
                initialURL = "http://www.xsmiles.org";
            }
        }

        // Browser for socket service.
        Browser b = null;
        try
        {
            b = this.launchBrowser(initialURL);
            // Set the config to CC/PP protocol, so that we know which profile
            // to send
            // to the server. (property "ccpp/DeviceProfile").
            XHttpURLConnection.setConfigurer(b.getBrowserConfigurer());
            if (initDialog != null)
                initDialog.dispose();
            // Also, set the browser, it is used to get the BrowserConstraints.
        }
        catch (Throwable t)
        {
            System.err.println(t);
            t.printStackTrace();
            // Ensure the application exits with an error condition.
            System.exit(1);
        }

        XMLConfigurer conf = this.getXMLConfigurer();
        if (conf.getBooleanProperty("main/socketservice/enabled"))
        {
            int port = conf.getIntProperty("main/socketservice/port");
            socketListener = new SLPSocketListener(port, b.browserTable);
            Log.debug("Browser is listening port " + port + " for SLP");
        }

        if (conf.getBooleanProperty("main/soapsocket/enabled"))
        {
            int port = conf.getIntProperty("main/soapsocket/port");
            socketListener = new SoapSocketListener(port, b.browserTable);
            Log.debug("Browser is listening port " + port
                    + " for SOAP messages");
        }

        if (conf.getBooleanProperty("main/rexsocket/enabled"))
        {
            int port = conf.getIntProperty("main/rexsocket/port");
            socketListener = new REXSocketListener(port, b.browserTable);
            Log.debug("Browser is listening port " + port + " for REX events");
        }

        if (conf.getBooleanProperty("main/wesahmisip/enabled"))
        {
            Log.debug("***********Start creating BrowserSubscriber.");
            String[] flightNumbers = new String[6];
            flightNumbers[0] = conf.getProperty("main/wesahmisip/flightNumber1");
            flightNumbers[1] = conf.getProperty("main/wesahmisip/flightNumber2");
            flightNumbers[2] = conf.getProperty("main/wesahmisip/flightNumber3");
            flightNumbers[3] = conf.getProperty("main/wesahmisip/flightNumber4");
            flightNumbers[4] = conf.getProperty("main/wesahmisip/flightNumber5");
            flightNumbers[5] = conf.getProperty("main/wesahmisip/flightNumber6");

            String[] sdts = new String[6];
            sdts[0] = conf.getProperty("main/wesahmisip/sdt1");
            sdts[1] = conf.getProperty("main/wesahmisip/sdt2");
            sdts[2] = conf.getProperty("main/wesahmisip/sdt3");
            sdts[3] = conf.getProperty("main/wesahmisip/sdt4");
            sdts[4] = conf.getProperty("main/wesahmisip/sdt5");
            sdts[5] = conf.getProperty("main/wesahmisip/sdt6");

            BrowserSubscriber subscriber = new BrowserSubscriber(
                    b.browserTable, conf.getProperty("main/wesahmisip/sipaddress"),
                    conf.getProperty("main/wesahmisip/service"),
                    conf.getProperty("main/wesahmisip/url"),
                    conf.getProperty("main/wesahmisip/clientID"),
                    flightNumbers, sdts);
            subscriber.registerToService();

        }
    }

    public XMLConfigurer getXMLConfigurer()
    {
        XMLConfigurer conf = new XMLConfigurer();
        return conf;

    }

    public Browser launchBrowser(String initialURL) throws Throwable
    {
        Browser b = new Browser();
        XMLConfigurer conf = this.getXMLConfigurer();

        // TODO: actually we need to enable security also in webstart, since we
        // are running in
        // unrestricted mode
        this.applySecurityPolicy((!webstart())
                && conf.getBooleanProperty("security/policy/enabled"));
        b.setBrowserConfigurer(conf);
        if (initialURL == null || initialURL.length() == 0)
            b.initBrowser(null, null, null, null, true, true);
        // else b=new Browser(initialURL,"initial");
        else
            b.initBrowser(new XLink(initialURL), null, "initial", null, true,
                    true);

        return b;
    }

    // check if using webstart
    public static boolean webstart()
    {
        boolean webstart = false;
        try
        {
            Class c = Class.forName("javax.jnlp.BasicService");
            if (c != null)
                return true;
            ;
        }
        catch (Exception e)
        {
        }
        return webstart;
    }

    /**
     * Search for AAML libraries. If not found, then AAML is disabled.
     */
    private static void initializeAAML()
    {
        try
        {
            // Just to check whether AAML exists in the classpath
            Class aaml = MLFCLoader
                    .loadClass("fi.hut.tml.xsmiles.mlfc.aaml.AAMLFC");
            Object o = aaml.newInstance();

        }
        catch (java.lang.NoClassDefFoundError e)
        {
            Log.debug("AAML disabled. Classes not found in classpath.");
        }
        catch (Exception e)
        {
            Log.debug("AAML disabled. Classes not found in classpath.");
        }
    }

    public Xsmiles()
    {
    }

    /**
     * URL for Xsmiles's security policy file
     */
    public static final String XSMILES_SECURITY_POLICY = "file:xsmiles.policy";

    protected ApplicationSecurityEnforcer securityEnforcer;

    // this can be overridden with an empty function for instance in applet
    protected void applySecurityPolicy(boolean policyEnabled)
    {
        if (policyEnabled)
        {
            Log.info("Security policy enabled: " + XSMILES_SECURITY_POLICY);
            securityEnforcer = new ApplicationSecurityEnforcer(this.getClass(),
                    XSMILES_SECURITY_POLICY);
            securityEnforcer.enforceSecurity(true);
        }
        else
        {
            Log.info("Running in unsecure mode, security policy disabled.");
        }
        // ApplicationSecurityEnforcer enforcer= new
        // ApplicationSecurityEnforcer();
    }

    /**
     * this method will set the system properties. XForms needs a certain
     * version of Xalan, so this will set
     * -Djava.endorsed.dirs=c:/source/xsmiles/bin/lib
     */
    /*
     * does not seem to work -MH protected static void setSystemProperties() {
     * System.getProperties().list(System.out); // display new properties
     * Properties p = new Properties(System.getProperties()); String dir = new
     * File("lib").getAbsolutePath(); p.setProperty("java.endorsed.dirs",dir);
     *  // set the system properties System.setProperties(p);
     * System.getProperties().list(System.out); // display new properties }
     */
}
