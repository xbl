/* 
 * (c), (TM), and (R) 2024 Thor Gnudsung foundation. 
 */
package fi.hut.tml.xsmiles;

import java.util.Vector;
import java.util.Hashtable;

/**
 * An combination of the vector and hashtable.
 * The datastructure for holding the BWs is order.
 *
 * @see BrowserID for more information on handling BWs 
 * @author Juha
 */
public class BrowserTable 
{
  private Hashtable tab=new Hashtable();
  private Vector browserOrder = new Vector();
  
  public BrowserTable() 
  {
  }

  /**
   * @return a vector containing browsers, if none found, then return null
   */
  public Vector getBrowsersByID(String id) 
  {
    Object o=tab.get(id);

    if(o==null)
      return null;
    else 
      return (Vector) o;
  }
  
  /**
   * @param bw Add a browserWindow to the datastructure. 
   */
  public void add(BrowserWindow bw) 
  {
    String id=bw.getID().id;
    Object o=tab.get(id);

    // If no id found, create a new entry 
    // (new vector to hold bws, and add it to hashtable)
    if(o==null) {
      Vector v=new Vector();
      v.addElement(bw);
      tab.put(id,v);
    } else {
      ((Vector)o).addElement(bw);
    }
    // Add window also to vector to remain the order.
    browserOrder.add(bw);
  }
  
  /**
   * Remove the browserWindow from table
   */
  public void remove(BrowserWindow bw) 
  {
    String id=bw.getID().id;
    Vector idVector=(Vector)tab.get(id);
    idVector.removeElement(bw);
    // Remove also from order vector.
    browserOrder.remove(bw);
  }

  /**
   * Print out a list of browsers
   */
  public void listBrowsers() 
  {
    Log.debug("BrowserTable contents: \n");
    Log.debug(tab.toString());
  }
  
  /**
   * Return a last created browser window.
   * 
   * @return 
   */
  public BrowserWindow getLatestBrowserWindow()
  {
      return (BrowserWindow)browserOrder.lastElement();
  }
}
