/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 * The rest of the license available at the web site or under /bin
 */

package fi.hut.tml.xsmiles;



// startup dialog
//import fi.hut.tml.xsmiles.gui.components.swing.InitDialog;

/**
 * A dummy class for .exe launcher in windows
 */
public class Launcher
{
}
