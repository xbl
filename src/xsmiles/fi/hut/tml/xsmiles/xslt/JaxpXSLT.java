/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */



package fi.hut.tml.xsmiles.xslt;

/**
 * This is the XSLTEngine implementation for JAXP
 * @author Mikko Honkala
 *
 * $Id: JaxpXSLT.java,v 1.20 2004/05/06 11:46:30 honkkis Exp $
 */

import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.File;
import java.io.StringWriter;
import java.io.StringReader;
import java.io.Writer;
import java.io.FileDescriptor;
import java.util.Properties;
import java.net.URL;


import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.Utilities;
import fi.hut.tml.xsmiles.xml.serializer.XMLSerializer;
import fi.hut.tml.xsmiles.mlfc.general.Helper;

import org.apache.xerces.dom.DocumentImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


// JAXP
import javax.xml.transform.*;
import javax.xml.transform.sax.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.apache.xerces.dom.CoreDocumentImpl;





public class JaxpXSLT implements XSLTEngine
{
    /** the transformer factory */
    TransformerFactory tfactory=null;
    /**
     * Parses xml and xsl documents passed as URL's
     *
     * @param xml the source XML DOM document
     * @param xsl the XSL as DOM document
     * @return the output of the evalauted xml and xsl URL documents as a DOM document
     */
    public Document transform(Document xml,Document xsl,URL base_url) throws SAXException, TransformerException
    {
        // Create input source documents.
        Source xmlID = new DOMSource(xml);
        Source stylesheetID = new DOMSource(xsl);
        
        Document doc = transform(xmlID,stylesheetID);
        // to make xml:base and element.getBaseURI to work -MH
        if (doc instanceof CoreDocumentImpl) ((CoreDocumentImpl)doc).setDocumentURI(base_url.toString());
        return doc;
        
    }
    
    /**
     * Parses xml and xsl documents passed as URL's
     *
     * @param xml the source XML DOM document
     * @param xsl the XSL as DOM document
     * @return the output of the evalauted xml and xsl URL documents as a DOM document
     */
    public Document transform(Document xml,URL xsl,URL base_url) throws SAXException, TransformerException
    {
        // Create input source documents.
        Source xmlID = new DOMSource(xml);
        Source stylesheetID = new StreamSource(xsl.toString());
        
        Document doc =  transform(xmlID,stylesheetID);
        // to make xml:base and element.getBaseURI to work -MH
        if (doc instanceof CoreDocumentImpl) ((CoreDocumentImpl)doc).setDocumentURI(base_url.toString());
        return doc;
    }
    public Document transform(URL xml,URL xsl) throws SAXException, TransformerException
    {
        // Create input source documents.
        Source xmlID = new StreamSource(xml.toString());
        Source stylesheetID = new StreamSource(xsl.toString());
        
        return transform(xmlID,stylesheetID);
    }
    /**
     * Legacy method.
     * Parses xml and xsl documents passed as URL's
     *
     * @param xml URL of the xml Document
     * @param xsl URL of the stylsheet
     * @return the output of the evalauted xml and xsl URL documents as string
     */
    public String transformToString(URL xml,URL xsl) throws SAXException, TransformerException
    {
        Log.error("JaxpXSLT.transformToString not supported.");
        return null;
    }
    
    protected Document transform(javax.xml.transform.Source xml,javax.xml.transform.Source xsl) throws SAXException, TransformerException
    {
        if (tfactory==null) tfactory = TransformerFactory.newInstance();
        Transformer transformer = tfactory.newTransformer(xsl);
        //		if(tFactory.getFeature(DOMResult.FEATURE))
        //		{
        //			transformer = tFactory.newTransformer(xsl);
        //			Log.debug("JAXP XSLT transform : "+transformer.toString());
        //
        //		}
        //		else
        //		{
        //			Log.error("The parser does not support DOM result");
        //			return null;
        //		}
        // Create an empty DOMResult for the Result.
        // Uncomment these to take X-Smiles document into use
        Document doc = new fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl();
        DOMResult domResult = new DOMResult(doc);
        
        
        //DOMResult domResult = new DOMResult();
        // Perform the transformation, placing the output in the DOMResult.
        transformer.transform(xml, domResult);
        return (Document)domResult.getNode();
    }
    
    /** this method checks the JDK version, since there are serialization problems in JDK 1.1*/
    public static void SerializeNode(Writer writer,Node node,boolean preserveSpace,boolean indenting, boolean addXMLDecl)
    throws TransformerException, TransformerConfigurationException, SAXException, IOException
    {
        if (Utilities.getJavaVersion()<1.2)
        {
            SerializeNodeXerces(writer,node,preserveSpace,indenting,addXMLDecl);
        }
        else
        {
        	try
			{
        		SerializeNodeJAXP(writer,node,preserveSpace,indenting,addXMLDecl);
			} catch (Exception e)
			{
				Log.error("JaxpXSLT.serializeNode() could not use JAXP, trying Xerces"+e.getMessage());
	            SerializeNodeXerces(writer,node,preserveSpace,indenting,addXMLDecl);
			}
        }
    }
    
    /** preferred method, which works under jdk1.3 with xerces 2.4.0 DOM3 + Xalan 2.5.1 */
        public static void SerializeNodeJAXP(Writer writer,Node node,boolean preserveSpace,boolean indenting, boolean addXMLDecl)
    throws TransformerException, TransformerConfigurationException, SAXException, IOException
    {
        TransformerFactory tfactory = TransformerFactory.newInstance();
        Transformer serializer = tfactory.newTransformer();
        
        
        serializer.setOutputProperty(OutputKeys.INDENT, indenting ? "yes" :"no");
        serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, addXMLDecl ? "no" : "yes");
        serializer.setOutputProperty(OutputKeys.METHOD, "xml");
        
        // i18n:  Serialize in the encoding specified in the document.
        //        If no encoding has been specified, use ISO-8859-1.
        String encoding = null;
        if (node.getOwnerDocument() instanceof DocumentImpl)
        {
            encoding = ((DocumentImpl)node.getOwnerDocument()).getEncoding();
        }
        if (encoding == null) encoding = "ISO-8859-1";
        serializer.setOutputProperty(OutputKeys.ENCODING, encoding);
        
        Properties oprops = new Properties();
        //	  oprops.put("method", "html");
        oprops.put("indent-amount", "2");
        //	  serializer.setOutputProperties(oprops);
        //	  serializer.transform(new DOMSource(doc),
        //	                       new StreamResult(System.out));
        
        serializer.transform(new DOMSource(node),
        new StreamResult(writer));
        
    }
    
    
    /** method, which works under jdk1.1 with xerces 2.4.0 DOM3 + Xalan 2.5.1 */
    public static void SerializeNodeXerces(Writer writer,Node node,boolean preserveSpace,boolean indenting, boolean addXMLDecl)
    throws TransformerException, TransformerConfigurationException, SAXException, IOException
    {
        try
        {
            // TODO: optimize, since this method takes a writer, but xerces needs an outputstream, a string
            // buffer is currently used, which takes some memory
            XMLSerializer ser = new XMLSerializer();
            String serStr = ser.writeToString(node);
            StringReader read = new StringReader(serStr);
            Helper.copyStream(read, writer, 1000);
        } catch (Throwable t)
        {
            Log.error(t);
        }
        
    }    
}

