/* X-Smiles
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 */

/*
 * X-smiles project 1999
 *
 * $Id: DocumentHistory.java,v 1.24 2005/01/10 12:09:31 tjjalava Exp $
 *
 */

package fi.hut.tml.xsmiles;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Stores the list of visited sites. This history can be accessed with the
 * Back and Forward buttons in the main browser window.
 *
 * @author	Jukka Heinonen
 * @author      Juha
 * @version	$Revision: 1.24 $
 */
public class DocumentHistory
{
    /**
     * Used to store visited sites as XLink objects
     */
    private Vector visitedSites;
    
    /**
     * points out the currently shown document in the history
     * NOTE: 0-based
     */
    private int finger;
    
    //private BrowserWindow myBrowser;
    
    /**
     * The constructor that is used by the project.
     *
     * @param browser the system browser
     */
    public DocumentHistory()
    {
        visitedSites = new Vector();
        finger = -1; // The history is empty in the beginning
        //myBrowser = browser;
    }
    
    // these methods are called by the Browser and cause a new document to
    // be retrieved (that is they call the retrieveResource method.)
    
    /**
     * Goes backwards in document history
     *
     * This method shouln't be called if the current document is in the
     * beginning of the history, that is one cannot go backwards in document
     * history. This should be prevented by disabling the corresponding button.
     */
    public synchronized XLink backwardsInHistory()
    {
        XLink newLink;
        
        if (finger < 1)
        {
            //dumpHistory();
            Log.error("DocumentHistory Internal error"+
            "Unable to go backwards in document history!");
            return null;
        } else
        {
            if (finger>0) finger--;
            newLink = getLinkAtFinger(finger);
            
            //dumpHistory();
            return newLink;
      /*if(finger==0)
        checkButtons();*/
        }
    }
    
    
    /**
     * Goes forward in document history
     *
     * This method shouln't be called if the current document is in the
     * end of the history, that is one cannot go forward in document
     * history. This should be prevented by disabling the corresponding button.
     */
    public synchronized XLink forwardInHistory()
    {
        //int lastElementIndex = visitedSites.size() - 1;
        XLink newLink;
        
        //if (lastElementIndex < 0) {
        //  lastElementIndex = 0;
        //}
        
        if (finger+1 >= visitedSites.size() || visitedSites.size() == 0)
        {
            //dumpHistory();
            Log.error("DocumentHistory internal error"+
            "Unable to go forward in document history!");
            return null;
        } else
        {
            finger++;
            newLink = this.getLinkAtFinger(finger);
            //dumpHistory();
            return newLink;
      /*
      if (finger == lastElementIndex)
        checkButtons();*/
        }
    }
    
    public synchronized XLink getLastDocument()
    {
        if(finger<0)
            return null;
        return getLinkAtFinger(finger);
    }
    protected XLink getLinkAtFinger(int f)
    {
        try {
            return (XLink) visitedSites.elementAt(f);
        } catch (java.lang.ArrayIndexOutOfBoundsException e)
        {
            Log.error(e.toString());
            return null;
        }
    }
    
    public void dumpHistory()
    {
        Log.debug("--------------------\nDocumentHistory finger: "+finger+" contents:");
        int i=0;
        for (Enumeration e = visitedSites.elements();e.hasMoreElements();)
        {
            XLink next = (XLink)e.nextElement();
            String fingerStr="";
            if (i==finger) fingerStr="FINGER: ";
            Log.debug(fingerStr+next.getURL().toString());
            i++;
        }
        Log.debug("-----------------------------");
    }
    
    /**
     * Stores a new document in the history.
     * If a page is reloaded several time, then only one link is added to history.
     *
     * Is called by the <code>Browser</code> when a new document is requested
     * using <code>retrieveResource</code> method. See the Technical Requirements
     * Specification for further details on document history.
     */
    public void addURL(XLink link)
    {
        //int lastElementIndex = visitedSites.size() - 1;
        XLink previous=null;
        
        
        //    if (lastElementIndex < 0) {
        //  lastElementIndex = 0;
        //}
        
        if (finger+1 >= visitedSites.size() || finger <0)
        {
            // We are currently at the end of the history
            
            
            // Check if previous is the same as this one.
            
            // previous is the last of the history
            if (visitedSites.size()>1)
            {
                previous=getLinkAtFinger(visitedSites.size()-1);
            }
            // If there is no previous link, or if previous is not the same,
            // then add link to history
            // Because link is added, the finger is also updated
            if(previous==null||(!previous.getURLString().equals(link.getURLString())))
            {
                //Log.debug("Adding link: "+link.getURLString());
                //if (previous!=null) Log.debug(" previous was:"+previous.getURLString());
                visitedSites.addElement(link);
                finger++;
            }
        } else if (finger+1 < visitedSites.size())
        {
            // Remove all visited sites that are after the current finger location
            do
            {
                visitedSites.removeElementAt(visitedSites.size()-1);
            } while (finger+1 < visitedSites.size());
            
            // Get the last item in history vector
            previous=finger>=0 ? getLinkAtFinger(finger-1) : null;
            
            // if the last item is different than new link
            // then add it to the vector and update finger
            if (previous==null||(!previous.getURLString().equals(link.getURLString())))
            {
                visitedSites.addElement(link);
                finger++;
            }
        } else
        {
            Log.error("DocumentHistory internal error: Finger lost, set to end.");
            finger=visitedSites.size()-1;
        }
        //dumpHistory();
        //checkButtons();
    }
    
    public boolean canGoBack()
    {
        return (finger > 0);
    }
    public boolean canGoForward()
    {
        return (finger+1 < visitedSites.size());
    }
    /** return the vector containing the whole history */
    public Vector getHistory()
    {
        return this.visitedSites;
    }
}

