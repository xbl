/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.timesheet.timer;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import fi.hut.tml.xsmiles.timesheet.TimedElement;

/**
 * Class TimedEventTable
 * 
 * @author tjjalava
 * @since Mar 3, 2004
 * @version $Revision: 1.8 $, $Date: 2004/08/17 11:22:28 $
 */
public class TimedEventTable {
    
    private Hashtable eventTable;
    private Vector timedElements;
    
    public static class TimedEvent {
        private int event;
        private TimedElement element;
        
        public TimedEvent(TimedElement element, int event) {
            this.element = element;
            this.event = event;
        }
                
        /**
         * @return Returns the element.
         */
        public TimedElement getElement() {
            return element;
        }

        /**
         * @return Returns the event.
         */
        public int getEvent() {
            return event;
        }
        
        public boolean equals(Object obj) {
            if (obj instanceof TimedEvent) {
                return ((TimedEvent)obj).getElement().equals(element) && ((TimedEvent)obj).getEvent() == event;
            } else {
                return false;
            }
        }
    }
    
    public TimedEventTable() {
        eventTable = new Hashtable();
        timedElements = new Vector();
    }
    
    private void addEvent(long time, TimedEvent event) {
        Long id = new Long(time);
        Vector events = (Vector) eventTable.get(id);
        if (events == null) {            
            eventTable.put(id, events = new Vector());
        }
        events.addElement(event);
        timedElements.addElement(event.getElement());
    }
    
    public void addEvent(long time, TimedElement element, int event) {
        addEvent(time, new TimedEvent(element, event));
    }
    
    public boolean hasEvent(TimedElement element, int event) {
        TimedEvent tEvent = new TimedEvent(element, event);
        for (Enumeration e=eventTable.elements();e.hasMoreElements();) {
            if (((Vector)e.nextElement()).contains(tEvent)) {
                return true;
            }
        }
        return false;
    }
    
    public Enumeration getTimedElements() {
        return timedElements.elements();
    }
    
    public void removeEvents(long time) {
        eventTable.remove(new Long(time));
    }
    
    public void clearEvents(TimedElement element, int event) {
        TimedEvent tEvent = new TimedEvent(element, event);
        for (Enumeration e=eventTable.elements();e.hasMoreElements();) {
            ((Vector)e.nextElement()).removeElement(tEvent);
        }
    }
    
    private int getEventsCount() {
        return eventTable.size();
    }
    
    public Enumeration getEvents(long time) {
        Long key = new Long(time);
        Vector events = (Vector) eventTable.remove(key);
        if (events == null) {
            return (new Vector()).elements();
        } else {
            return events.elements();
        }
    }
}
