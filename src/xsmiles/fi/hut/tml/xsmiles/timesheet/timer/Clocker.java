/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.timesheet.timer;


/**
 * Class Clocker
 * 
 * @author tjjalava
 * @since Apr 30, 2004
 * @version $Revision: 1.1 $, $Date: 2004/05/03 14:56:45 $
 */
public interface Clocker {
    
    public void update();

}
