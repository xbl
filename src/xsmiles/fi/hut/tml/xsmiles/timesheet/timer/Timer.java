/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights reserved. For details on use and redistribution
 * please refer to the LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.timesheet.timer;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.timesheet.TimedElement;

/**
 * Class Timer is the scheduler for TimedElement instances. Timer listens for ticks that are produced by static singleton 
 * instance of the inner class Clock. By every tick the Timer calls the update(long)-method of the registered TimedElement instances.
 * 
 * @author tjjalava
 * @since Mar 2, 2004
 * @version $Revision: 1.17 $, $Date: 2004/09/16 11:56:17 $
 */
public class Timer implements Runnable {

    /**
     * The interval for ticks
     */
    protected static final int INTERVAL = 50;
    
    protected final Clock clock = new Clock();
    private Object mutex = new Object();  
    private PauseMutex pauseMutex = new PauseMutex();
    
    private Thread runner;
    private boolean active;
    private long tick = -1;
    private TimedElement timeRoot;
    private Clocker clocker = null;
    private long ticksInSecond = 1000 / INTERVAL;    
            
    private class PauseMutex {        
        private boolean paused = false;
        
        public synchronized void checkPaused() {
            while (paused) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
        
        public synchronized void setPaused(boolean paused) {
            this.paused = paused;
            notifyAll();
        }
    }
    
    /**
     * Class Clock produces one clock tick every INTERVAL milliseconds
     * 
     */
    protected class Clock implements Runnable {

        private long tickNumber = 0;
        private boolean running = false;        
        private Thread clockRunner;
        
        public void run() {
            // notify every thread that clock has started running
            synchronized (mutex) {
                running = true;
                mutex.notifyAll();
            }  
            long nextExec = System.currentTimeMillis();
            while (running) {                
                try {
                    // wait for INTERVAL
                    long now = System.currentTimeMillis();                    
                    if (now < nextExec) {
                        Thread.sleep(nextExec-now);
                        nextExec = nextExec + INTERVAL;
                    } else {
                        nextExec = System.currentTimeMillis() + INTERVAL;
                    }                    
                } catch (InterruptedException e) {                    
                }
    
                pauseMutex.checkPaused();
                
                if (!running) {
                    return;
                }
                
                synchronized (mutex) {                    
                    tickNumber++;
                    mutex.notifyAll();
                }
                
                if (clocker != null && tickNumber % ticksInSecond == 0) {
                    clocker.update();
                }
             
            }
        }
                
        public boolean isRunning() {
            return running;
        }
        
        public void start() {
            tickNumber = 0;
            clockRunner = new Thread(this, "Clockthread");
            clockRunner.start();
        }
        
        /**
         * Stops the clock. Blocks until the clock is finished.
         */
        public void stopTime() {
            synchronized (mutex) {
                running = false;   
                pause(false);
                clockRunner.interrupt();                
                tickNumber = 0;                
                mutex.notifyAll();
            }
            try {
                clockRunner.join();
            } catch (InterruptedException e) {            
            }
        }
        
        /**
         * Timer instances call this method to wait for the next clock tick. If Timer has fallen behind time, 
         * method returns immediately, so that Timer can catch up with the clock.         
         * 
         * @param lastTick the caller's own ticknumber
         */
        public void waitForTick(long lastTick) {            
            synchronized (mutex) {                
                while (running && lastTick >= tickNumber) {
                    try {
                        mutex.wait();
                    } catch (InterruptedException e) {                        
                    }
                }        
            }
        }        
    }
    
    public void setClocker(Clocker clocker) {
        this.clocker = clocker;
    }
    
    /**
     * Starts the clock.
     */
    private void startTime() {
        if (clock.isRunning()) {
            return;
        }
        clock.start();
        Log.debug("Timer: clock started");
    }
    
    /**
     * Stops the clock
     */
    private void stopTime() {
        clock.stopTime();
        Log.debug("Timer: clock stopped");
    }
            
    /**
     * Converts milliseconds, used in the attributes of Timesheets into ticks
     *  
     * @param millis the time used in timesheets
     * @return ticks corresponding to the millis
     */
    public static long timeToTicks(long millis) {
        if (millis<0) {
            return -1;
        }
        return (millis/INTERVAL);
    }
    
    public static long ticksToTime(long ticks) {
        if (ticks<0) {
            return -1;            
        }
        return ticks*INTERVAL;
    }
    
    /**
     * Creates the Timer instance
     * 
     */
    public Timer(TimedElement root) {
        timeRoot = root;
    }  
                
    /**
     * Starts the Timer
     */
    public void start() {
        if (active) {
            return;
        }
        startTime();
        active = true;          
        runner = new Thread(this, "UpdateThread");
        tick = -1;
        runner.start();
    }
    
    public void pause(boolean paused) {
        pauseMutex.setPaused(paused);
    }
    
    /**
     * Stops Timer
     */
    public void stop() {
        if (!active) {
            return;
        }
        pause(false);
        active = false;
        runner.interrupt();
        stopTime();
    }
    
    public void run() {
        synchronized (mutex) {
            // wait for the clock to start
            while (active && !clock.isRunning()) {
                try {
                    mutex.wait();
                } catch (InterruptedException e) {                
                }
            }
        }
        while (active) {
            clock.waitForTick(tick++);
            if (active) {
                timeRoot.update();
            }
        }
    }
}
