/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.timesheet;


/**
 * Interface for elements that need timing
 * 
 * @author tjjalava
 * @since Mar 2, 2004
 * @version $Revision: 1.18 $, $Date: 2004/08/10 14:16:28 $
 */
public interface TimedElement {
    
    /**
     * ELEMENT_BEGIN event value
     */
    public static final String ELEMENT_BEGIN_EVENT = "begin";
    
    /**
     * ELEMENT_PAUSED event value
     */
    public static final String ELEMENT_PAUSED_EVENT = "paused";
    
    /**
     * ELEMENT_NOT_PAUSED event value
     */
    public static final String ELEMENT_NOT_PAUSED_EVENT = "not-paused";
    
    /**
     * ELEMENT_END event value
     */
    public static final String ELEMENT_END_EVENT = "end";
    
    // element names
    /**
     * Tag for timesheet element
     */
    public static final String TIMESHEET_ELEM = "timesheet";
    
    /**
     * Tag for seq element
     */
    public static final String SEQ_ELEM = "seq";
    
    /**
     * Tag for excl element
     */
    public static final String EXCL_ELEM = "excl";
    
    /**
     * Tag for par element
     */
    public static final String PAR_ELEM = "par";
    
    /**
     * Tag for item element
     */
    public static final String ITEM_ELEM = "item";
        
    //  common attributes
    /**
     * Name for the begin attribute
     */
    public static final String BEGIN_ATTR = "begin";
        
    /**
     * Name for the duration attribute
     */
    public static final String DURATION_ATTR = "dur";
    
    /**
     * Name for the repeat attribute
     */
    public static final String REPEAT_ATTR = "repeat";
    
    // 	Item attributes and values
    /**
     * Name for the select attribute
     */    
    public static final String SELECT_ATTR = "select";
    
    /**
     * Name for the prefetch attribute
     */
    public static final String PREFETCH_ATTR = "prefetch";
    
    /**
     * Value for indefinite time or repeat
     */
    public static final String INDEFINITE_VALUE = "indefinite";
        
    /**
     * Called by the Timer instance of this element every tick. Do all the actions
     * corresponding to the current ticknumber here.
     *
     */
    public void update();    
}
