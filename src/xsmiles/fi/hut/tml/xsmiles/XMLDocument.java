/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles;

import java.io.*;
import org.w3c.dom.*;        //located in domcore.jar
import org.w3c.dom.events.*; //located in domcore.jar
import java.net.*;
import java.util.Vector;
import java.util.StringTokenizer;
import java.util.Enumeration;
import java.awt.Container;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.dom.VisualComponentService;

import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.xslt.XSLTEngine;
import fi.hut.tml.xsmiles.xml.XMLParser;
import fi.hut.tml.xsmiles.messaging.Messaging;
import fi.hut.tml.xsmiles.gui.BrowserConstraints;
import org.xml.sax.SAXException;
import org.xml.sax.ErrorHandler;
import javax.xml.transform.TransformerException;

/**
 * The XMLDocument is an interface for XMLDocuments, with additional 
 * functionality needed by the browser, such as transformation capabilities,
 * extension handling
 *
 * @author      Kreivi
 * @author	Mikko Honkala
 * @auhtor      Juha
 */
public interface XMLDocument
{
  /**
   * Accessor method.
   * @return the source of the destination XML document as a Vector.
   *
   */
  public Vector getSourceVector();
  
  /**
   * Accessor method.
   *
   * @return returns the source of the source XML document as a Vector.
   */
  public Vector getXMLVector();

  
  /**
   * Accessor method.
   *
   * @return returns the source of XSL-stylesheet as a Vector.
   */
  public Vector getXSLVector();
  
  /**
   * Accessor method.
   * @return the XLink object associated with the document.
   *
   */
  public XLink getLink();

  /**
   * Accessor method
   * @return the URL of the source xml document
   *
   */
  public URL getXMLURL();
  
  /**
   * Accessor method
   * @return the URL of the xsl document
   */
  public URL getXSLURL();


  /**
   * @return Instance of the ECMAScripter associated with this document
   */
  public ECMAScripter getECMAScripter();

    
  /**
   * Change stylesheet tranform, and render.
   * @param name The title of the stylesheet to change to
   */
  //  public void changeStylesheet(String name);

  /**
   * @deprecated
   */
  public BrowserWindow getBrowser();
  
  /**
   * @return The transformed version of document
   */
  public Document getDocument();

  /**
   * @return Original untransformed XML document
   */
  public Document getXMLDocument();

  /**
   * @return stylesheet in dom
   */
  public Document getXSLDocument();

  /**
   * @return Transformed xml document in a string
   * @param d the dom to convert
   * @param b prettyprinting
   */
  public String getSourceText(Document d, boolean b);

  /**
   * Fetch document
   */
  public void retrieveDocument() throws Exception;

//  /**
//   * Fetch document
//   */
//   public Document processEcmaScript(Document a_doc);
//
  /**
   * @return Titles of stylesheets as a Vector
   */
  public Vector getStylesheetTitles();

  /**
   * If the current stylesheet does not exist, then "" is returned. If no title has been
   * specified, then the title = the URI of the stylesheet currently used
   * @return the title of the current stylesheet
   */
  public String getCurrentStylesheetTitle();

	/**
	 * @param title Sets the preferred stylesheet title.
	 */
  public void setPreferredStylesheetTitle(String title);


  /**
   * Public method accessible from MLFCs to evaluate Media Query strings (e.g. xhtml <link> element).
   * @param media		Media attribute string
   * @return 			true if media query evaluates to true
   */
  public boolean evalMediaQuery(String media);

  public void deactivate();
}
