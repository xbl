/*
 * Created on 1.4.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.comm;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import java.util.Hashtable;

/**
 * @author ssundell
 *
 */
public class CommBroker
{
	public static Hashtable commSessionImplementations=new Hashtable();
	static
	{
		commSessionImplementations.put("jxta","fi.hut.tml.xsmiles.comm.implementation.jxta.JxtaCommSession");
		commSessionImplementations.put("sip","fi.hut.tml.xsmiles.comm.implementation.sip.SipCommSession");
	}
    private static Hashtable commSessions = new Hashtable();
    //private static Hashtable pendingSessions = new Hashtable();
    private static MLFCListener mlfcListener;
    
    public static void setMlfcListener(MLFCListener _mlfcListener)
    {
        mlfcListener = _mlfcListener;
    }
    
    public static CommSession getCommSession(String type)
    {
        if (!commSessions.containsKey(type))
        {
        	CommSession session=createCommSession(type);
        	if (session==null)
        	{
        		Log.error("failed to create Comm session for type: "+type);
        		return null;
        	}
            commSessions.put(type, session);
        }
        return (CommSession)commSessions.get(type);    
    }
    
    private static CommSession createCommSession(String type)
	{
    	// USE reflection to allow addition of new comm implementations
    	try
		{
    		String className = (String)commSessionImplementations.get(type);
    		CommSession commSession = (CommSession)Class.forName(className).newInstance();
    		commSession.setMLFCListener(mlfcListener);
    		return commSession;
		} catch (Throwable t)
		{
			Log.error(t,"Failed to instantiate CommSession for type: "+type);
			return null;
		}
	}

}
