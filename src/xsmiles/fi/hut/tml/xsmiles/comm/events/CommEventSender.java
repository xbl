/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 10.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.events;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm, 10.11.2004
 */
public interface CommEventSender
{
    public void addEventListener(CommEventListener listener);
    public void removeEventListener(CommEventListener listener);
    public void dispatchEvent(CommEvent evt);
}
