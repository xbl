/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 10.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.events;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.event, 10.11.2004
 */
public interface CommEvent
{
    public static final int EVENT_OTHER = 999;
    public static final int EVENT_COMMSESSION_CONNECT_ERROR = 1;
    
    public static final int EVENT_SESSION_ACCEPT=10;
    public static final int EVENT_SESSION_DECLINE=11;
    public static final int EVENT_SESSION_UNCONNECTED=12;
    
    public static final int EVENT_PRESENCE_SUBSCRIPTION_ERROR = 20;
    
    
    public void addDescription(String desc);
    
    public int getType();
        
    public String getDescription();
    
}
