/*
 * Created on Sep 30, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.events;

/**
 * @author ssundell
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public interface CommEventListener
{
    public void incomingEvent(CommEvent evt);
}
