/*
 * Created on 15.3.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.comm.session;


/**
 * Message listener interface for COMM API. 
 * 
 * @author ssundell
 * 
 */
public interface MessageListener
{
    public void incomingMessage(Message message);
}
