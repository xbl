/*
 * Created on 15.3.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.comm.session;

import fi.hut.tml.xsmiles.comm.*;
import java.util.Vector;

import fi.hut.tml.xsmiles.comm.events.CommEventSender;

/**
 * Session package and class include session specific information, such as the user list
 * of a P2P group, file shares in that particular group and so on.
 * The session can be opened for a single user or for a group
 * (depending on the underlying P2P network architecture, of course).
 * 
 * @author ssundell
 * 
 */
public interface Session extends CommEventSender
{
    /**
     * Opens the session - in other words, tries to establish a contact to the network.
     * 
     */
    public void openSession(Contact _contact);
    
    /**
     * Close the session.
     *
     */
    public void closeSession();
    
    
    /**
     * Returns the user list for the current session.
     * 
     * @return User list
     */
    public Vector getUsers();
    
    /**
     * Get the target of the session - that is, the user or group that we're connected to.
     * 
     * @return
     */
    public Contact getTarget();
    
    /**
     * Accept the incoming session 
     *
     */
    public void accept();
    
    /**
     * Decline the incoming dession.
     *
     */
    public void decline();
    
    public Messaging createMessaging();
    
    
}    

