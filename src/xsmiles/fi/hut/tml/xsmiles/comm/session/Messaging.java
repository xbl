/*
 * Created on 15.3.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.comm.session;

/**
 * The basic messaging interface for COMM API.
 * Notice that the messaging is always in a session context - the comm session
 * needs to be established before messages can be sent.
 * 
 * @author ssundell
 * 
 */
public interface Messaging
{
    /**
     * Send an XML formatted message.
     *
     */
    public void sendMessage(Message message);
    
    /**
     * Add a listerner to receive messages in a session.
     * 
     * @param listener
     */
    public void addMessageListener (MessageListener listener);
    
    public void removeMessageListener (MessageListener listener);
}
