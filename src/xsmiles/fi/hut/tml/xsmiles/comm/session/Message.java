/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 10.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.session;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.session, 10.11.2004
 */
public interface Message
{
    public String getType();
    
    public String getNickname();
    
    public void setNickname(String name);
    
    public Object getContent();
    
    public void setContent(Object content);
    
    public String getContentAsString();
    
    public void setContentAsString(String content);

}
