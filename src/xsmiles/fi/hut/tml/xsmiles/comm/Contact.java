/*
 * Created on 13.5.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.comm;

import java.util.Enumeration;

import fi.hut.tml.xsmiles.comm.session.Session;
import fi.hut.tml.xsmiles.comm.events.CommEventSender;

/**
 * @author ssundell
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface Contact extends CommEventSender
{
    public Session connect();
    public String getName();
    
    /**
     * Get keys to the information stored in the user object.
     * 
     * @return Enumeration of the keys
     */
    public Enumeration getKeys();
    
    /**
     * Set the information
     * 
     * @param key	Key to the information
     * @param value	Value of the information
     */
    public void setValue(String key, Object value);
    
    public Object getValue(String key);
    
}
