/*
 * Created on 1.4.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.comm;

import fi.hut.tml.xsmiles.comm.events.*;
import fi.hut.tml.xsmiles.comm.session.Session;
import fi.hut.tml.xsmiles.comm.session.SessionListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * @author ssundell
 *
 */
public interface CommSession extends CommEventSender
{
    public void signIn();
    public void signOut();
    public User getOwner();
    /** create user object */
    public User createUser();
    
    /** create group object */
    public Group createGroup();

    /** create session object */
    public Session createSession();

    /** create session for given address */
	public Session createSession(String address);
	
    /** create addressbook object */
    public AddressBook getAddressBook();
    
    /**
     * A generic method to retrieve extension features from the implementation. 
     * 
     * @param feature The feature to retrieve
     * @return An object implementing that feature, or null if not available.
     */
    public Object getFeature(Class feature);
    
    /** add the MLFC listener object */
    public void setMLFCListener(MLFCListener listener);
    

    /**
     * Sets the session listener to make the system aware of the incoming session requests.
     * 
     * @param listener
     */
    public void setSessionListener(SessionListener listener);
    
}
