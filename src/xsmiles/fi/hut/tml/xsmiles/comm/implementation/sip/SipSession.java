/*
 * Created on 1.4.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.comm.implementation.sip;

import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.comm.*;
import fi.hut.tml.xsmiles.comm.events.*;
import fi.hut.tml.xsmiles.comm.session.*;
import fi.hut.tml.xsmiles.comm.implementation.general.events.*;
import fi.hut.tml.xsmiles.comm.implementation.general.messages.*;

import fi.hut.tml.sip.stack.*;
import fi.hut.tml.sip.stack.sdp.SdpMessage;
import fi.hut.tml.sip.stack.connection.SipCall;
import fi.hut.tml.sip.stack.connection.SipCallImpl;
import fi.hut.tml.sip.stack.event.SipStackListener;
import fi.hut.tml.sip.stack.msrp.*;
import java.util.Vector;


/**
 * @author ssundell
 *
 */
public class SipSession extends CommEventSenderBase implements Session, Messaging, MSRPListener, SipStackListener
{
    private SipUser target;
    private SipController controller;
    private SipCall call;
    private String targetUrl;
    private String sessionName;
    private MessageListener mListener;
    private MSRPSession msgSession;
    private boolean open = false;
    private SipCommSession commSession;

    /**
     * 
     */
    public SipSession(SipCommSession _commSession)
    {
        super();
    	init(_commSession);
    }
    
    public SipSession(SipCommSession _commSession, SipCall _call)
    {
        init(_commSession);
        call = _call;
		targetUrl = call.getAddress();
		setTarget();
		
		commSession.addPending(targetUrl, this);
	}
    
    private void init(SipCommSession _commSession)
    {
        commSession=_commSession;
        controller = ((SipCommSession)CommBroker.getCommSession("sip")).getController();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.comm.session.Session#openSession()
     */
    public void openSession (Contact _contact)
    {
    	if (open)
    		return;

    	// We'll handle the Group when the time comes. So far, only User is applicable.
    	if (!(_contact instanceof SipUser ))
    	    return;
    	
        targetUrl = (String)_contact.getValue("address");
        sessionName = (String)_contact.getValue("name");
        call = controller.createCall(SdpMessage.SdpMessageTypeMSG, this, null, targetUrl);
        msgSession = controller.getMsrpSession(((SipCallImpl)call).getMsrpLocalUrl());
        msgSession.setListener(this);
        
        setTarget();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.comm.session.Session#closeSession()
     */
    public void closeSession()
    {
        // Quick try to get this working. - ssundell, 2004-09-28
        if (open)
            call.hangup();
        else if (call != null)
            call.cancel();
    }


    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.comm.session.Session#getUsers()
     */
    public Vector getUsers()
    {
        return null;
    }
    
    public void sendMessage(Message msg)
    {
        msgSession.sendMessage(msg.getType(), msg.getContentAsString());
    }

    public void addMessageListener (MessageListener _listener)
    {
        mListener = _listener;
    }
    
    public void removeMessageListener (MessageListener _listener)
    {
        if (_listener == mListener)
            mListener = null;
    }
    
    
    
    public void incomingMessage(MSRPMessage _msg)
    {
        Message msg = MessageFactory.createMessage(_msg.getHeaderValue(MSRPHeader.ContentType));
        if (msg != null)
        {            
            msg.setContentAsString(_msg.getBody());
            msg.setNickname(target.getName());
            mListener.incomingMessage(msg);
        }
        else
            Log.debug("Unknown incoming message format.");
    }
    
    public Contact getTarget()
    {
        return target;
    }
    
    public void accept()
    {
        call.receive();
        msgSession = controller.getMsrpSession(((SipCallImpl)call).getMsrpLocalUrl());
        msgSession.setListener(this);
        open = true;
        this.commSession.removePending(targetUrl);
        dispatchEvent(CommEventFactory.createEvent(CommEvent.EVENT_SESSION_ACCEPT));
    }
    
    public void decline()
    {
    	call.cancel();
    	commSession.removePending(targetUrl);
    	dispatchEvent(CommEventFactory.createEvent(CommEvent.EVENT_SESSION_DECLINE));
    }
    
	public Messaging createMessaging() {
		return (Messaging)this;
	}
	
	private void setTarget()
	{
	    target = new SipUser();
        target.setValue("name", call.getUser());
        target.setValue("address", call.getAddress());
	}
	
	public void processStackEvent(fi.hut.tml.sip.stack.event.SipStackEvent evt){
        int status = evt.getStatus();
        
        switch(status) {
            case SipCall.UNCONNECTED:
                open = false;
            	call = null;
                dispatchEvent(CommEventFactory.createEvent(CommEvent.EVENT_SESSION_UNCONNECTED));
                break;
            case SipCall.CALLING:
                break;
            case SipCall.TRYING:
                break;
            case SipCall.CONNECTED:
                open = true;
                break;
            case SipCall.CONNECTEDRTP:
                break;
            case SipCall.INCOMINGCALL:
                break;
            case SipCall.DEAD:
                break;
            case SipCall.CANCELLING:
                break;
        }
        
    }
}
