/*
 * Created on 1.4.2004
 *
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */

package fi.hut.tml.xsmiles.comm.implementation.sip;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

import fi.hut.tml.xsmiles.comm.*;
import fi.hut.tml.xsmiles.comm.events.*;
import fi.hut.tml.xsmiles.comm.session.*;

import fi.hut.tml.xsmiles.comm.implementation.general.events.*;

import fi.hut.tml.sip.stack.SipLocalInfo;
import fi.hut.tml.sip.stack.SipController;

import fi.hut.tml.sip.stack.event.SipStackListener;
import fi.hut.tml.sip.stack.connection.SipCall;

/**
 * @author ssundell
 *
 */
public class SipCommSession extends CommEventSenderBase implements CommSession, SipStackListener
{
    private fi.hut.tml.sip.stack.SipLocalInfo localinfo;
    private SipUser owner;
    private SipController controller;
    private SessionListener sListener;
    private int online = 0;
    private Hashtable pendingSessions = new Hashtable();
    private MLFCListener mlfcListener;
    
    public SipCommSession()
    {
    }
    public void init()
    {
        if (localinfo == null)
        {
            String user = mlfcListener.getProperty("sip/username");
            String passwd = mlfcListener.getProperty("sip/password");
            String server = mlfcListener.getProperty("sip/servername");
            String address = mlfcListener.getProperty("sip/address");
            String localPort = mlfcListener.getProperty("sip/localport");
            String audioPort = mlfcListener.getProperty("sip/audioport"); 
            String videoCapture = mlfcListener.getProperty("sip/videocapture");
            String videoPort = mlfcListener.getProperty("sip/videoport");
            String messagePort = mlfcListener.getProperty("sip/messageport");
            localinfo = new SipLocalInfo(user, server, passwd, address, localPort, videoCapture, audioPort, videoPort, messagePort);
            try {
                owner = new SipUser(localinfo.getUser(), localinfo.getRegisterUri().toString(), null);
            } catch (Exception e) {
                Log.error("Error in owner sip address!");
                e.printStackTrace();
            }
        }
        
        if (controller == null)
        {
            controller = new SipController(localinfo, new java.awt.Panel());
            controller.addSipStackListener(this);
        }
    }
    
    public void signIn()
    {
        if (online == 0)
        {
            init();
        	boolean connected = controller.connect();
        	if (!connected)
        	{    
        	    sendEvent(CommEvent.EVENT_COMMSESSION_CONNECT_ERROR);
        	    return;
        	}
        }
        online++;
    }

    public void signOut()
    {
    	online--;
    	if (online == 0)
        	controller.close();
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.comm.session.Session#addSessionListener(fi.hut.tml.xsmiles.comm.session.SessionListener)
     */
    public void setSessionListener(SessionListener _listener)
    {
        sListener = _listener;
    }
    
    public User getOwner()
    {
        return owner;
    }
    
    public SipController getController()
    {
        return controller;
    }
    
    public void processStackEvent(fi.hut.tml.sip.stack.event.SipStackEvent evt)
    {
        int status = evt.getStatus();

        switch(status)
        {
            case SipCall.UNCONNECTED:
                Log.debug("No connection.");
                break;
            case SipCall.CALLING:
                Log.debug("Connecting...");
                break;
            case SipCall.TRYING:
                Log.debug("Connecting...");
                break;
            case SipCall.CONNECTED:
                Log.debug("Connected.");
                break;
            case SipCall.CONNECTEDRTP:
                break;
            case SipCall.INCOMINGCALL:
                Log.debug("Incoming, incoming!");
                SipCall call = (SipCall)evt.getSource();
                SipSession newSession = new SipSession(this, call);
                call.addSipStackListener(newSession);
                sListener.incomingSession(newSession);
                break;
            case SipCall.DEAD:
                Log.debug("No connection.");
                break;
            case SipCall.CANCELLING:
                Log.debug("Call cancelled.");
                break;
        }        
    }

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createUser()
	 */
	public User createUser() {
        return new SipUser();
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createGroup()
	 */
	public Group createGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createSession()
	 */
	public Session createSession() {
        return new SipSession(this);
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#createSession(java.lang.String)
	 */
	public Session createSession(String address) {
		if (pendingSessions.containsKey(address))
		{
			Session session = (Session)pendingSessions.get(address);
			session.accept();
			return session;
		}
		
		return new SipSession(this);
		
	}

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#getAddressBook()
	 */
	public AddressBook getAddressBook() {
        return new SipAddressBook();
	}
	
	public Object getFeature(Class feature)
	{
	    return null;
	}
	
    public void addPending(String _target, Session _session)
    {
    	pendingSessions.put(_target, _session);
    }
    
    public void removePending(String _target)
    {
    	pendingSessions.remove(_target);
    }

	/* (non-Javadoc)
	 * @see fi.hut.tml.xsmiles.comm.CommSession#setMLFCListener(fi.hut.tml.xsmiles.mlfc.MLFCListener)
	 */
	public void setMLFCListener(MLFCListener listener) {
		// TODO Auto-generated method stub
		this.mlfcListener=listener;
		
	}
	
	private void sendEvent(int type)
	{
	    dispatchEvent(CommEventFactory.createEvent(type));
	}
	
	

}
