/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 9.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.implementation.general.messages;


/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.implementation.general.messages, 9.11.2004
 */
public class TextMessage extends MessageBase
{
    private String body;
    
    protected TextMessage()
    {
        super("text/plain");
    }
    
    public Object getContent()
    {
        return body;
    }
    
    public String getContentAsString()
    {
        return body;
    }

    public void setContent(Object content)
    {
        if (String.class.isInstance(content))
            body = (String)content;
    }
    
    public void setContentAsString(String content)
    {
        body = content;
    }
}
