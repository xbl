/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 10.12.2004
 *
 */
package fi.hut.tml.xsmiles.comm.implementation.sip;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;


import fi.hut.tml.xsmiles.comm.implementation.general.XMLAddressBook;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.implementation.sip, 10.12.2004
 */
public class SipAddressBook extends XMLAddressBook
{

    public SipAddressBook()
    {
        super();
    }


    /**
     * The addContact method takes a contact element from the XML format address book as a parameter,
     * extracts attributes from it and stores them into a SipUser object. This object is then stored into
     * contacts Hashtable, using address as a key.  
     * 
     * @parameter user Contact element to be added
     */
    protected void addContact(Element user)
    {
        String addr = user.getAttribute("address");
        if (addr == null || !addr.startsWith("sip:"))
            return;
        
        NamedNodeMap attribs = user.getAttributes();
        SipUser sipUser = new SipUser();
        
        for (int i = 0; i < attribs.getLength(); i++)
        {
            Node attrib = attribs.item(i);
            sipUser.setValue(attrib.getNodeName(), attrib.getNodeValue());
        }
        contacts.put(addr, sipUser);
    }

}
