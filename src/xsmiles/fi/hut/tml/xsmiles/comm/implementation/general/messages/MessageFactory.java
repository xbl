/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 9.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.implementation.general.messages;

import java.util.Hashtable;


import fi.hut.tml.xsmiles.Log;


/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.implementation.general.messages, 9.11.2004
 */
public class MessageFactory
{
    private static Hashtable msgTypes;
    
    static
    {
        msgTypes = new Hashtable(2);
        
        msgTypes.put("text/plain", "fi.hut.tml.xsmiles.comm.implementation.general.messages.TextMessage");
        msgTypes.put("application/xml", "fi.hut.tml.xsmiles.comm.implementation.general.messages.XMLMessage");
    }
    
    public static MessageBase createMessage(String type)
    {
        if (type == null || type.equals(""))
            return null;
        
        try
    	{
            String className = (String)msgTypes.get(type);
        	MessageBase message = (MessageBase)Class.forName(className).newInstance();
        	return message;
    	} catch (Throwable t)
    	{
    	    Log.error(t,"Failed to instantiate CommSession for type: "+type);
    		return null;
    	}
    }
}
