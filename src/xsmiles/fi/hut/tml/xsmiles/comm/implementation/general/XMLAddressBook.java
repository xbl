/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 9.12.2004
 *
 */
package fi.hut.tml.xsmiles.comm.implementation.general;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Hashtable;

import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.xml.sax.InputSource;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.comm.AddressBook;
import fi.hut.tml.xsmiles.comm.implementation.general.events.CommEventSenderBase;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.implementation.general, 9.12.2004
 */
public abstract class XMLAddressBook extends CommEventSenderBase implements AddressBook
{
    private static String filename = "cfg/addressbook2.xml";
    private Document addBookDoc;
    protected Hashtable contacts;
    
    public XMLAddressBook()
    {
        contacts = new Hashtable();
    }
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.comm.AddressBook#loadAddressBook()
     */
    public void loadAddressBook()
    {
        Log.debug("Creating a new COMM document");
		DOMParser parser = new DOMParser();
        
		try {
			parser.setFeature("http://xml.org/sax/features/validation", false);
			parser.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			parser.setFeature("http://xml.org/sax/features/external-general-entities", false);
			parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			parser.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			
			//TODO: Change the filename to be something configurable.
			parser.parse(new InputSource(new FileInputStream(filename)));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		addBookDoc = parser.getDocument();
		
		NodeList contactList = addBookDoc.getElementsByTagName("user");
		for (int i = 0; i < contactList.getLength(); i++)
		    addContact((Element)contactList.item(i));
		
		contactList = addBookDoc.getElementsByTagName("group");
		for (int i = 0; i < contactList.getLength(); i++)
		    addContact((Element)contactList.item(i));
		
		
    }

    public void saveAddressBook()
    {
        try {
            XMLSerializer serializer = new XMLSerializer(new FileOutputStream(filename), new OutputFormat("XML", "UTF-8", false));
            serializer.serialize(addBookDoc);
        }
        catch (Exception e)
        {
            Log.debug("SipPresenceDocument: Couldn't serialize document");
            e.printStackTrace();
        }
    }    
    
    protected abstract void addContact(Element user);

    public Hashtable searchInfo(String _search, String _field)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Hashtable getContacts()
    {
        return contacts;
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.comm.AddressBook#entryCount()
     */
    public int entryCount()
    {
        return contacts.size();
    }
}
