/*
 * Created on Sep 24, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.general;

import java.util.Enumeration;
import java.util.Hashtable;

import fi.hut.tml.xsmiles.comm.Contact;
import fi.hut.tml.xsmiles.comm.session.Session;
import fi.hut.tml.xsmiles.comm.implementation.general.events.*;

/**
 * @author qlin, ssundell
 *
 */
public abstract class ContactBase extends CommEventSenderBase implements Contact {
    protected Hashtable infoTable;
    
    public ContactBase()
    {
        super();
        infoTable = new Hashtable();
    }
	
    public abstract Session connect();
	
    public Enumeration getKeys()
    {
        return infoTable.keys();
    }
	
    public void setValue(String key, Object value)
    {
        infoTable.put(key, value);
    }
    
    public Object getValue(String key)
    {
        return infoTable.get(key);
    }
    
    public String getName()
    {
        if (infoTable.containsKey("name"))
            return (String)infoTable.get("name");
        else if (infoTable.containsKey("address"))
            return (String)infoTable.get("address");
        else
            return null;
    }
    
    protected void sendEvent(int type)
	{
        dispatchEvent(CommEventFactory.createEvent(type));
	}
    
}
