/*
 * Created on 29.3.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.sip;

import fi.hut.tml.xsmiles.Log;


import fi.hut.tml.xsmiles.comm.*;
import fi.hut.tml.xsmiles.comm.session.Session;
import fi.hut.tml.xsmiles.comm.implementation.general.*;
import fi.hut.tml.xsmiles.comm.implementation.general.events.CommEventImpl;
import fi.hut.tml.xsmiles.comm.presence.*;

import fi.hut.tml.xsmiles.dom.*;

import fi.hut.tml.sip.stack.SipController;
import fi.hut.tml.sip.stack.SipUri;
import fi.hut.tml.sip.stack.events.*;
import fi.hut.tml.sip.stack.events.presence.*;


import org.w3c.dom.*;
import org.apache.xerces.dom.*;

/**
 * @author ssundell
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SipUser extends ContactBase implements User, Presentity, SipEventsListener
{
    private SipPresence presSub;
    private SipPresencePub presPub;
    private PresenceListener presListener;
	
    public SipUser()
    {
        super();
    }
    
        
    public SipUser(String name, String address, String image)
    {
    	super();
        
        if (name == null || name.equals("") || address == null) {
            Log.error("No user name defined!");
            return;
        }
        
        infoTable.put("name", name);
        infoTable.put("address", address);
        if (image != null)
            infoTable.put("avatar", image);
    }
	
    public Session connect()
    {
        Session session = CommBroker.getCommSession("sip").createSession((String)infoTable.get("address"));
        session.openSession(this);
        return session;
    }
	    
    public void subscribe(PresenceListener _listener)
    {
        SipController controller = ((SipCommSession)CommBroker.getCommSession("sip")).getController();
        // We'll trust that this is SipPresence object we're getting...
        presListener = _listener;
        presSub = (SipPresence)controller.createSubscription(fi.hut.tml.sip.stack.events.SipEvents.Package_PRESENCE, new SipUri((String)getValue("address")));
        if (presSub != null)
        {
            presSub.addListener(this);
        	presSub.subscribe();
        }
        else
            sendEvent(CommEventImpl.EVENT_PRESENCE_SUBSCRIPTION_ERROR);
    }
    
    public void remove()
    {
        if (presPub != null)
            presPub.remove();
        if (presSub != null)
            presSub.removeListener(this);
        presListener = null;
    }
    
    public void publish(Node _status)
    {
        Log.debug("Publishing local presence information.");
        SipController controller = ((SipCommSession)CommBroker.getCommSession("sip")).getController();
        presPub = (SipPresencePub)controller.createPublication(fi.hut.tml.sip.stack.events.SipEvents.Package_PRESENCE);
        
        if (presPub != null)
        {
            DocumentImpl doc = new DocumentImpl();
            Element status = doc.createElementNS("urn:ietf:params:xml:ns:pidf", "status");
            
            Node presTmp = doc.importNode(_status, true);
            
            NodeList children = _status.getChildNodes();
            for (int i = 0; i < children.getLength(); i++)
            {
                Log.debug("Children: " + children.getLength());
                status.appendChild(doc.importNode(children.item(i), true));
            }
            
            //presPub.publish(createPublishableNode(doc, status));
            presPub.publish(status.cloneNode(true));
        }
    }

    /**
     * This method is called when the presence information for one or another subscription changes.
     * It copies the nodes from a status element into the comm namespace and under "presence" element.
     * After that it calls the presence listeners.
     */
    public void statusChange()
    {        
        Node status = presSub.getStatus();
        
        if (status != null)
        {
            XSmilesDocumentImpl doc = new XSmilesDocumentImpl();
        
            Element presence = doc.createElementNS("http://www.x-smiles.org/2004/comm", "presence");
            
            NodeList children = status.getChildNodes();
            
            for (int i = 0; i < children.getLength(); i++)
            {
                presence.appendChild(doc.importNode(children.item(i), true));
            }
            
            
        
            //presence.appendChild(createNode(doc, status));
        
            presListener.statusChange(presence);
        }
    }
    
    public boolean authorize()
    {
        // TODO: We really need to implement this...
        return true;
    }
    
    // The SIP user should get events only from the presence subscription (or publication)
    public void incomingEvent(int type, String description)
    {
        
    }
    
    public Presentity getPresentity()
    {
        return (Presentity)this;
    }
    
}
