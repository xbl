/*
 * Created on Oct 6, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.general.events;

import fi.hut.tml.xsmiles.comm.events.CommEvent;

/**
 * @author ssundell
 *
 */
public class CommEventImpl implements CommEvent
{
    
    private int type;
    private String description;
    
    public CommEventImpl(int _type)
    {
        switch (_type)
        {
            case -1:
                description="Unknown error";
        }
        type = _type;
    }
    
    public CommEventImpl(int _type, String desc)
    {
        type = _type;
        description = desc;
    }
    
    public void addDescription(String desc)
    {
        description = desc;
    }
    
    public int getType()
    {
        return type;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    
}
