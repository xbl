/*
 * Created on Oct 7, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package fi.hut.tml.xsmiles.comm.implementation.general.events;

import fi.hut.tml.xsmiles.comm.events.*;

import java.util.Vector;
import java.util.Enumeration;


/**
 * @author ssundell
 *
 * 
 */
public class CommEventFactory
{
    public static CommEvent createEvent(int type)
	{
        switch (type)
        {
            case CommEvent.EVENT_COMMSESSION_CONNECT_ERROR:
                return new CommEventImpl(type, "Unable to connect.");
            case CommEvent.EVENT_SESSION_ACCEPT:
                return new CommEventImpl(type, "Accepted incoming session.");
            case CommEvent.EVENT_SESSION_DECLINE:
                return new CommEventImpl(type, "Declined incoming session.");
            case CommEvent.EVENT_SESSION_UNCONNECTED:
                return new CommEventImpl(type, "No call.");
            case CommEvent.EVENT_PRESENCE_SUBSCRIPTION_ERROR:
                return new CommEventImpl(type, "Couldn't establish subscription.");
            default:
                return new CommEventImpl(CommEvent.EVENT_OTHER);
        }
	}
}
