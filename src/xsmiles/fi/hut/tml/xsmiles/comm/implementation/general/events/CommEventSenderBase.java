/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 10.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.implementation.general.events;

import fi.hut.tml.xsmiles.comm.events.*;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.implementation.general.event, 10.11.2004
 */
public class CommEventSenderBase implements CommEventSender
{
    private Vector eventListeners;

    public CommEventSenderBase()
    {
        eventListeners = new Vector();
    }
    
    public void addEventListener(CommEventListener listener)
    {
        eventListeners.add(listener);
    }

    public void removeEventListener(CommEventListener listener)
    {
        eventListeners.remove(listener);
    }

    public void dispatchEvent(CommEvent evt)
    {
        for (Enumeration e=eventListeners.elements(); e.hasMoreElements();)
            ((CommEventListener)e.nextElement()).incomingEvent(evt);
    }

}
