/*
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to 
 * the LICENSE_XSMILES file included with these sources.
 * 
 * Created on 9.11.2004
 *
 */
package fi.hut.tml.xsmiles.comm.implementation.general.messages;

import fi.hut.tml.xsmiles.comm.session.Message;

/**
 * @author Sami Sundell
 *
 * xsmiles / fi.hut.tml.xsmiles.comm.session, 9.11.2004
 */
public abstract class MessageBase implements Message
{
    private String type;
    private String nick;
    
    public MessageBase(String _type)
    {
        type = _type;
    }
    
    public String getType()
    {
        return type;
    }
    
    
    public abstract Object getContent();
    public abstract String getContentAsString();
    
    public abstract void setContent(Object content);
    public abstract void setContentAsString(String textual);
    
    public String getNickname()
    {
        return nick;
    }
    
    public void setNickname(String name)
    {
        nick = name;
    }
}
