/*
 * Created on 15.3.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.comm;

import fi.hut.tml.xsmiles.comm.presence.Presentity;


/**
 * The general User interface for COMM API.
 * 
 * The user object should contain all the necessary information for establishing
 * a contact with the user. It could also contain other user-specific information -
 * name, age, gender, birthday, mobile phone number and so on.
 * 
 * The user object can be used in both the user lists and address books.
 * 
 *  
 * @author ssundell
 * 
 */
public interface User extends Contact
{

    /** create presentity object */
    public Presentity getPresentity();
    
}
