/*
 * Created on 4.8.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author Sami Sundell
 */
package fi.hut.tml.xsmiles.comm.presence;

import org.w3c.dom.Node;

import fi.hut.tml.xsmiles.comm.events.CommEventSender;

/**
 * @author ssundell
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface Presentity extends CommEventSender
{
        
    public void subscribe(PresenceListener _listener);
    public void remove();
    
    public void publish(Node _node);
}
