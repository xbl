/*
 * Created on 15.3.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.comm;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.comm.events.CommEventSender;

/**
 * The address book interface.
 * The address book should countain user records in some data structure that can then be easily converted
 * into a modifiable DOM structure.
 * 
 * @author ssundell
 * 
 */
public interface AddressBook extends CommEventSender
{
    
    /**
     * Gets the address book from some, possibly protocol dependent storage.
     * It could be a local file or a storage server (SIP registrar, LDAP storage, whatever) 
     *
     */
    public void loadAddressBook();
    
    /**
     * Save the address book.
     *
     */
    public void saveAddressBook();
    
    /**
     * Searches the address book for some info and returns the users that match. For example,
     * we could search for name and return address.
     * 
     * @param _search What to search
     * @param _field The target field of the search
     * @return
     */
    public Hashtable searchInfo(String _search, String _field);
    
    public Hashtable getContacts();
    
    public int entryCount();
}
