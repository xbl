/*
 * Created on 15.3.2004
 *
 * X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * @author <a href="mailto:ssundell@tml.hut.fi">Sami Sundell</a>
 */
package fi.hut.tml.xsmiles.comm;

import java.util.Hashtable;

import fi.hut.tml.xsmiles.comm.events.CommEventSender;

/**
 * The Groups interface for COMM API contains methods related to the general communications
 * groups handling.
 * 
 * The actual meaning of a group depends on the underlying communications network
 * it could be a JXTA group, or an IRC channel. In some networks there are no groups. 
 * 
 * @author ssundell
 * 
 */
public interface Groups extends CommEventSender
{
    /**
     * Get a list of available groups.
     *
     */
    public Hashtable getGroups();
    /**
     * Create a new group.
     * 
     * @param group
     */
    public void addGroup(Group group);
    
    /**
     * Remove a group.
     * 
     * @param group
     */
    public void deleteGroup (Group group);
    
    /**
     * Join a group.
     * 
     * @param group
     */
    public void joinGroup (Group group);
    
    /**
     * Leave a group.
     * 
     * @param group
     */
    public void leaveGroup (Group group);
    
    /**
     * Invite a user into a group.
     * 
     * @param group
     * @param user
     */
    public void inviteUser (Group group, User user);
}
