/* X-Smiles
 *
 *
 * The Telecommunications Software and Multimedia Laboratory, Helsinki
 * University of Technology Software License, Version 1.0
 * (based on the Apache Software License Version 1.1)
 * The rest of the license available at the web site or under /bin
 */

package fi.hut.tml.xsmiles;

import java.util.Vector;

/**
 * Static list for keeping the browserwindow references.
 * @author Mikko Honkala
 * @version	$Revision: 1.1 $
 */
public class BrowserWindowList
{
    protected static Vector browserWindows;
    
    
    public BrowserWindowList()
    {
    }
}
