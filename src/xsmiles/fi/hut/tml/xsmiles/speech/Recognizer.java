/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 2, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;

import java.io.Reader;


/**
 * @author honkkis
 *
 */
public interface Recognizer
{
    public void setGrammar(Reader r) throws Exception;
    public String getOneResultSync();
    /** gets one result asynchronously, note that you have to set the result listener, in order to get the results, since
     * this method returns immediately
     */
    public void getOneResultAsync(); 
    
    public void stopRecognizing();
    public void addXResultListener(XRecognizerListener list);

}
