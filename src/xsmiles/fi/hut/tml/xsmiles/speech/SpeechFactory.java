/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 6, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.MLFCLoader;


/**
 * @author honkkis
 *
 */
public class SpeechFactory
{
    protected static String synthClass = "fi.hut.tml.xsmiles.speech.SynthImpl";
    protected static String recognizerClass = "fi.hut.tml.xsmiles.speech.RecognizerImpl";
    protected static Recognizer recognizer;
    protected static Synth synth;
    public static Recognizer getRecognizer()
    {
        try
        {
	        if (recognizer==null)
	        {
	            try
	            {
	            	recognizer = (Recognizer)Class.forName(recognizerClass).newInstance();
		        } catch (Throwable t)
		        {
		            Log.error(t);
		        }
		        if (recognizer==null)
		        {
	            	recognizer = (Recognizer)MLFCLoader.loadClass(recognizerClass).newInstance();
		        }
	        }
        } catch (Throwable t)
        {
            Log.error(t);
        }
        return recognizer;
    }
    public static Synth getSynth()
    {
        try
        {
        if (synth==null)
        {
            try
            {
            	synth = (Synth)Class.forName(synthClass).newInstance();
            } catch (Throwable t)
            {
                Log.error(t);
            }
            if (synth==null)
            {
            	synth= (Synth)MLFCLoader.loadClass(synthClass).newInstance();
            }
           
        }
        } catch (Throwable t)
        {
            Log.error(t);
        }
        return synth;
    }
    
    public static void destroyed(Object o)
    {
        if (o==synth) synth=null;
        else if (o==recognizer) recognizer=null;
    }

}
