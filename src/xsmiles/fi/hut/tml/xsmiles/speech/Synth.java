/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 1, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;


/**
 * @author honkkis
 *
 */
public interface Synth
{
    public void speak(String s,boolean wait );
    public void waitEngine();
    public void cancelAll();
    public void destroy();
}
