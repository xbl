/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 5, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;


/**
 * @author honkkis
 *
 */
public class XResultEvent
{
    public String result;
    public XResultEvent(String res)
    {
        result=res;
    }

}
