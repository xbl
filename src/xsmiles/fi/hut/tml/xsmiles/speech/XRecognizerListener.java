/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jul 5, 2004
 *
 */
package fi.hut.tml.xsmiles.speech;


/**
 * @author honkkis
 *
 */
public interface XRecognizerListener
{
    
    public static final short statusRecognizing = 6;
    public static final short statusInitializing = 7;
    public static final short statusIdle=666;
    public void resultEvent(XResultEvent ev);
    public void statusChange(short statusCode, String statusMessage);
}
