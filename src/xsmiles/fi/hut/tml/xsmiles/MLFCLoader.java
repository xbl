/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

/*
 * X-smiles project 1999
 *
 * $Id: MLFCLoader.java,v 1.22 2003/02/13 14:08:04 honkkis Exp $
 *
 */

package fi.hut.tml.xsmiles;

import fi.hut.tml.xsmiles.gui.components.ComponentFactory;

import java.util.Hashtable;
import java.io.*;
import java.net.*;
import java.util.Vector;

/**
 * Retrieves MLFCs for the MLFCManager.
 *
 * See Technical Requirements Specification and Developer's Walkthrough
 * for further details on how to implement MLFCs.
 *
 * @author	Jukka Heinonen
 * @author  Mikko Honkala
 * @author Juha
 * @version	$Revision: 1.22 $
 */
public class MLFCLoader
{ 
	private BrowserWindow myBrowser;
	//protected static ClassLoaderInterface classLoader;
	protected static NetworkClassLoader classLoader;
//	protected static JarFilter myFilter;

//	static
//	{
//		try
//		{
//			//myFilter = new JarFilter();
//			createClassLoader();
//		} catch (Exception e)
//		{
//			Log.error(e);
//		}
//	}
	public MLFCLoader(BrowserWindow browser)
	{
		myBrowser = browser;
		return;
	}

	public static void createClassLoader() throws MalformedURLException
	{
		try
		{
			// experimental
			// go thru every jar in bin/extensions, and add them to the classloader
			
			Vector jars = new Vector(10);
			getJars(new File("extensions"),jars);
			classLoader = new NetworkClassLoader(MLFCLoader.class.getClassLoader());
			for (int i=0;i<jars.size();i++)
			{
				File f = (File)jars.elementAt(i);
				URL u = Utilities.toURL(f);
				classLoader.addURL(u);
				Log.debug("Added extension jar: "+u);
			}
		} catch (Throwable e)
		{
			Log.error(e);
			Log.debug("Could not instantiate JDK12 class loader, using system class loader.");
			return;
		}
	}
	
	/** recursive method for getting all the jar files in a dir and subdirs */
	public static Vector getJars(File file, Vector v)
	{
		if (file.isDirectory())
		{
			String[] files = file.list();
			for (int i=0;i<files.length;i++)
			{
				String fname = files[i];
				File f = new File(file,fname);
				getJars(f,v);
			}
		}
		else
		{
			String name = file.getName();
			if (name.endsWith(".jar")||name.endsWith(".zip"))
			{
				v.addElement(file);
			}
		}
		return v;
	}
	
	public static Class loadClass(String name) throws java.lang.ClassNotFoundException
	{
		if (classLoader==null) return Class.forName(name);
		else return classLoader.loadClass(name);
	}
	
}







