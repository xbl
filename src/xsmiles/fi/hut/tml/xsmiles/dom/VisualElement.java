/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Aug 4, 2004
 *
 */
package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;


/**
 * @author honkkis
 *
 */
public interface VisualElement extends PseudoClassController
{
    /** notify that the style should be recomputed */
    public void styleChanged();
    
    /**
     * returns the stored style for this element
     */
    public CSSStyleDeclaration getStyle();
    
    /**
     * Is the element currently shown. Look at the display and visibility properties
     * checks parents as well
     * @author honkkis
     *
     */
    public boolean isCurrentlyVisible();
    

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualElement#isCurrentlyVisible()
     * Does not check parents! Use isCurrentlyVisible for parents
     */
    public boolean isVisibleByCSSProperties();

}
