/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.events.Event;

/*
 * XSmiles refreshable service interface.
 * Used for inter-element communication
 */

public interface RefreshableService {
	/**
	 * Refresh this element
	 */
	public void refresh();
        
        /* if set to true, block all refresh commands */
        public void setBlocking(boolean state);

}
