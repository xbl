package fi.hut.tml.xsmiles.dom;

import java.net.URL;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Node;


/**
 * This is a class, which encapsulates the different types of processing 
 * instructions in X-Smiles. 
 * 
 * Currently it can be an XForms document or a plain XSLT stylesheet.
 * 
 * @author Juha
 */
public class XSmilesPI
{
    private String title;
    private ProcessingInstruction piNode;
    private URL href;
    
    /**
     * The constructor get the node, and it gets the title of the PI
     * @param pi ProcessingInstruction
     * @param t Type
     * @param h href to target
     */
    public  XSmilesPI(ProcessingInstruction pi, String t, URL h) {
	title=t;
	piNode=pi;
	href=h;
    }
    
    /**
     * @return the title of this processing instruction
     */
    public String getTitle() {
	return title;
    }
    
    /**
     * @return The processing instruction node
     */
    public ProcessingInstruction getPINode() {
	return piNode;
    }
    
    public URL getHREF() 
    {
	return href;
    }
}
