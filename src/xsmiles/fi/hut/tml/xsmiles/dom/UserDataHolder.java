/*
 * X-Smiles licence
 *
 */
package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.Node;

/**
 * We don't want to count on DOM L3 services
 * @author honkkis
 *
 */
public interface UserDataHolder
{
    public Object getUserDataEx(Node node, String key);
    public void setUserDataEx(Node node, String key,Object obj);
}
