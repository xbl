/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
// $Id: XMLBroker.java,v 1.21 2006/02/21 20:18:07 honkkis Exp $
//

package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.*;
import org.w3c.dom.*;
import org.apache.xerces.dom.*;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.MLFCLoader;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;

//import fi.hut.tml.xsmiles.mlfc.xforms.dom.*;

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Vector;

/**
 * The factory for creating DOM event instances
 */
public class XMLBroker
{
    // XMLMLFC is used if there is a PI pointing to a CSS and the language
    // is otherwise unknown
    /** the default XML+CSS MLFC. This default can be changed by registering a 
     * new MLFC with the xml namespace */
    public static String XMLMLFC_NAME="fi.hut.tml.xsmiles.mlfc.xml.XMLMLFC"; 
    
    public static final String XMLNS = "http://www.w3.org/XML/1998/namespace";
    
    protected boolean hostMLFCisXMLMLFC=false;

    /**
     * parasite MLFC is a non-host mlfc, such as XForms
     */
    protected Hashtable parasiteMLFCs, activeMLFCs, strToMLFC;
    protected MLFC hostMLFC;
    protected String hostURI;
    

    
    //Is this a presetation DOM or not
    protected boolean presentationDOM=true;
    protected String rootElementName;
    
    /** true if this document has CSS reference PI at root */
    protected boolean hasCSSPI = false;
    
    /** true if this document has XSL reference PI at root */
    protected boolean hasXSLPI = false;
    
    public XMLBroker()
    {
        parasiteMLFCs=new Hashtable(3);
        activeMLFCs=new Hashtable(5);
        strToMLFC = new Hashtable(5);
    }
    public MLFC getHostMLFC()
    {
        return hostMLFC;
    }
    /** this is a kludge that SVGMLFC uses, for it's own copy of XMLBroker */
    public void setHostMLFC(MLFC h_mlfc, String uri)
    {
        hostMLFC=h_mlfc;
        this.activeMLFCs.put(uri,h_mlfc);
        h_mlfc.setHost(true);
    }
    public Hashtable getParasiteMLFCs()
    {
        return parasiteMLFCs;
    }
    
    public boolean isPresentationDOM()
    {
        return presentationDOM;
    }
    
    public Element createElement(
    org.apache.xerces.dom.DocumentImpl doc,String tagname) throws DOMException
    {
        // TODO: for example smil unqualified elements are created here!
        return this.createElementNS(doc, null, tagname);
    }
    public void createdProcessingInstruction(String target, String data, ProcessingInstruction proc)
    {
        // check that it is a transform processing instruction
        String type = XSmilesXMLDocument.getAttribute(data,"type");
        if (target.equals("xml-stylesheet") && (rootElementName==null) && type!=null)
        {
            if (type.equals("text/xsl"))
            {
                this.presentationDOM=false;
                this.hasXSLPI=true;
            }
            else if (type.equals("text/css"))
            {
                this.hasCSSPI=true;
            }
        }
    }
    public boolean docHasCSSPI()
    {
        return (this.hasCSSPI&&(!this.hasXSLPI));
    }
    
    public MLFC findMLFC(String URI, String tagname)
    {
        MLFC mlfc = null;
        String mlfcName = "unknown";
        
        // Use URI or if it is null, use ""
        mlfc = (MLFC)activeMLFCs.get(URI==null?"":URI);
        if (mlfc==null)
        {
            // URI NS not yet handled
            try
            {
                // Get name of MLFC for element or URI NS
                mlfcName = getMLFC(tagname, URI);
                // MLFC not found! Create a generic element.
                if (mlfcName == null)
                {
                    // if the root element has non-recognized owner, this is not
                    // a presentation DOM, unless there is a CSS stylesheet at the root
                    if (this.docHasCSSPI())
                    {
                        if (this.hostMLFC==null)
                        {
                            mlfcName=XMLMLFC_NAME;
                            hostMLFCisXMLMLFC=true;
                        }
                        else if (hostMLFCisXMLMLFC)
                        {
                            return this.hostMLFC;
                        }
                        else return null;
                    }
                    else if (rootElementName==null)
                    {
                        presentationDOM=false;
                        return null;
                    }
                    else return null;
                }
                // Get the MLFC
                mlfc = (MLFC)strToMLFC.get(mlfcName);
                if (mlfc == null)
                {
                    // MLFC has not yet been instantiated, so create it
                    //Object c = Class.forName(mlfcName).newInstance();
                    // MH: use our own classloader to load the mlfc
                    Object c = MLFCLoader.loadClass(mlfcName).newInstance();
                    if (c != null && c instanceof MLFC)
                    {
                        mlfc = (MLFC) c;
                        Log.debug(" - MLFC "+mlfcName+" version "+mlfc.getVersion()+" instantiated.");
                        strToMLFC.put(mlfcName, mlfc);
                        activeMLFCs.put((URI==null?"":URI), mlfc);
                    }
                    else
                    {
                        //	  myBrowser.showErrorDialog("MLFC error",
                        //				    "The components found on the hard disk are not X-Smiles compliant");
                        Log.error("MLFC error\n"+
                        "The components found on the hard disk are not X-Smiles compliant");
                        mlfc = null;
                    }
                    
                    
                }
            } catch (ClassNotFoundException e)
            {
                // Catch ClassNotFoundException thrown in MLFCLoader.loadClass() - MLFC not found!
                String str = "MLFC error: "+mlfcName+" not found!\n"+
                "X-Smiles has an invalid namespace-MLFC mapping or \n"+
                "the extension MLFC hasn't been installed. \n"+
                "See X-Smiles installation how to install extension MLFCs.";
                Log.error(str);
                Log.error(e);
                throw new DOMException((short)-1, str);
            } catch (Exception e)
            {
                //				throw e;
                Log.error(e);
                throw new DOMException((short)-1,e.getMessage());
            }
            // First MLFC becomes host MLFC
            if(hostMLFC==null)
            {
                hostMLFC=mlfc;
                mlfc.setHost(true);
            } else
            {
                parasiteMLFCs.put(URI,mlfc);
                mlfc.setHost(false);
            }
        }
        return mlfc;
    }
    
    public Element createElementNS(
    org.apache.xerces.dom.DocumentImpl doc, String URI,String tagname) throws DOMException
    {
        Element elem=null;
        if (presentationDOM==false)
            return null;
        // if this is not a root element and does not have a namespace
        if (URI==null&&hostMLFC!=null)
        {
            elem = hostMLFC.createElementNS(doc,null,tagname);
            return elem;
        }
        MLFC mlfc=this.findMLFC(URI,tagname);
        
        if (rootElementName==null)
        {
            // THIS IS THE ROOT ELEMENT
            rootElementName=tagname;
            // SVG kludge
            /*
            if (rootElementName.equals("svg"))
            {
                presentationDOM=false;
            }*/
        }
        if (mlfc!=null) elem = mlfc.createElementNS(doc,URI,tagname);
        if (elem!=null) return elem;
        else return null;
    }
    
    public Attr createAttributeNS(DocumentImpl doc,String URI, String qualifiedName)
    throws DOMException
    {
        //Log.debug("** XML Broker.createAttribute "+qualifiedName+" uri:"+URI);
        Attr attr=null;
        if (presentationDOM==false)
            return null;
        MLFC mlfc = this.findMLFC(URI,null);
        
        // Use URI or if it is null, use ""
        if (mlfc!=null) attr = mlfc.createAttributeNS(doc,URI,qualifiedName);
        return attr;
    }
    
    /**
     * Finds an MLFC from local hard drive or if unsuccessful over the network.
     *
     * The MLFCs are located in mlfc/XXXMLFC directory. Tries to instantiate
     * a MLFC according to the argument mlName. If no appropriate MLFC is found
     * on the hard disk, tries to retrieve a ZIP package from the resource
     * location, unzip it to hard disk and then again tries to instantiate it.
     *
     * @param mlName MLFC component name
     *
     * @return requested MLFC name or <code>null</code> if was unable to retrieve the required MLFC
     */
    
    protected String getMLFC(String localname, String namespace)
    throws IllegalAccessException,InstantiationException,ClassNotFoundException
    {
        MLFC mlfc = null;
        //    resourceLocation = myBrowser.getResourceLocation();
        //
        String fullMLFCClassName=null;
        if (namespace!=null)
        {
            // Tries to find a namespace mapping
            fullMLFCClassName = this.getMLFCClassNS(namespace);
        }
        else if (localname!=null&&fullMLFCClassName==null)
        {
            // Tries to find an local tag name mapping
            fullMLFCClassName = this.getMLFCClass(localname);
        }
        
        
        return fullMLFCClassName;
    }
    
    static protected Hashtable registeredMLFCsNS=new Hashtable(8);
    static protected Hashtable registeredMLFCs=new Hashtable(8);
    static public void registerMLFCNS(String a_ns,String a_class)
    {
        Log.info("XMLBroker: MLFC "+a_class+"registered with namespace: "+a_ns);
        registeredMLFCsNS.put(a_ns,a_class);
        if (a_ns.equals(XMLNS)) XMLMLFC_NAME=a_class;
    }
    
    static public String getMLFCClassNS(String ns)
    {
        return (String)registeredMLFCsNS.get(ns);
    }
    static public void registerMLFC(String tag,String a_class)
    {
        Log.info("XMLBroker: MLFC "+a_class+"registered with tagname: "+tag);
        registeredMLFCs.put(tag,a_class);
    }
    
    static public boolean isRegistered(String namespace)
    {
        return (getMLFCClass(namespace)!=null);
    }
    static public String getMLFCClass(String ns)
    {
        return (String)registeredMLFCs.get(ns);
    }
    public void destroy()
    {
        parasiteMLFCs=null;
        activeMLFCs=null;
    }
    
    //	/**
    //	 * calls init() for every node in document.
    //	 */
    //	public void init(Document document)
    //	{
    //		// TODO: if no XSmilesElementImpls created, just returns.
    //		Log.debug("///////// broker.init(doc) called.");
    //		this.initElement(document.getDocumentElement());
    //	}
    //
    //	/**
    //	 * Initialize this smil element.
    //	 */
    //	protected void initElement(Element e) {
    //		if (e instanceof XSmilesElementImpl)
    //		{
    //			((XSmilesElementImpl)e).init();
    //		}
    //		NodeList children = e.getChildNodes();
    //		// If no children, then just exit
    //		if (children.getLength() == 0) {
    //			return;
    //		}
    //
    //		// Init all children
    //		for (int i=0; i<children.getLength() ; i++) {
    //			Node child = children.item(i);
    //			if (child.getNodeType()==Node.ELEMENT_NODE)
    //				this.initElement((Element)child);
    //
    //		}
    //
    //	}
} // EventFactory
