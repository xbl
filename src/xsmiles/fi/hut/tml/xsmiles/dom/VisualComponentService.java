/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;

import java.awt.Dimension;
import java.awt.Component;

/*
 * XSmiles visual component service interface.
 * Used for inter-element communication
 */
public interface VisualComponentService {

    public static final int EVENT_STYLECHANGED=6;
	/**
	 * Return the visual component for this extension element
	 */
	public Component getComponent();
	
        /**
	 * Returns the approximate size of this extension element
	 */
	public Dimension getSize();
	public void setZoom(double zoom);
	public void setVisible(boolean visible);
	public boolean getVisible();

    // currently supports EVENT_STYLECHANGED with object=null, in future there would be more 
    public void visualEvent(int event,Object object);
    
	
}
