/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.gui.media.general.MediaListener;

/**
 * Interface for elements that refer to some outside media, like image, audio or video
 * 
 * @author tjjalava
 * @since Mar 23, 2004
 * @version $Revision: 1.8 $, $Date: 2004/04/23 10:52:44 $
 */
public interface MediaElement {
    
    /**
     * Status code for playing media
     */
    public static final int PLAYING = 5001;
    
    /**
     * Status code for stopped media
     */
    public static final int STOPPED = 5002;
    
    /**
     * Status code for paused media
     */
    public static final int PAUSED = 5003;
    
    /**
     * Sets this element active or inactive
     * @param active
     */
    public void setActive(boolean active);
    
    /**
     * Starts playing the media 
     * @throws Exception
     */
    public void play() throws Exception;
    
    /**
     * Stops the media
     */
    public void stop();
    
    /**
     * Pauses the media
     */
    public void pause();   
    
    /**
     * Adds a new listener for the media
     * @param listener
     */
    public void addMediaListener(MediaListener listener);
    
    /**
     * Removes a certain listener
     * @param listener
     */
    public void removeMediaListener(MediaListener listener);
    
    /**
     * Returns the current status of the media
     * @return the status of the media
     */
    public int getStatus();

    /**
     * Prefetches the media
     * @throws Exception
     */
    public void prefetch() throws Exception;

    /**
     * Static elements have no defined duration. These include all html elements and object elements that refer to static media, like images. 
     * Non-static media include audio and video
     * @return
     */
    public boolean isStatic();
    
}
