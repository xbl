/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.css.CSSStyleDeclaration;

import fi.hut.tml.xsmiles.mlfc.css.RuleTreeNode;

public interface StylableElement extends org.w3c.dom.Element
{
	/** get the resolved CSS style for this element */
	public CSSStyleDeclaration getStyle();
	
	/** set the resolved CSS style for this element */
	public void setStyle(CSSStyleDeclaration style);
	/** return the string contents of the style attribute. If no support for style attribute, return null */
	public String getStyleAttrValue();
	
	/** ask whether this element belongs to a certain CSS pseudoclass */
	public boolean isPseudoClass(String pseudoclass);
	
	public boolean hasStyle();
	
	public void setRuleNode(RuleTreeNode rtn);
	
	public RuleTreeNode getRuleNode();
	
}
