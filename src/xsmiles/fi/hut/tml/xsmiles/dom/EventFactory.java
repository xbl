// EventFactory.java
//
/*******************************************************************************
 * Copyright (C) CSIRO Mathematical and Information Sciences. * All rights
 * reserved. *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the CSIRO Software License *
 * version 1.0, a copy of which has been included with this distribution in *
 * the LICENSE file. *
 ******************************************************************************/
//
// $Id: EventFactory.java,v 1.10 2008/01/29 09:27:19 jorutila Exp $
//
//package org.csiro.svg.dom.events;
package fi.hut.tml.xsmiles.dom;

import java.util.Hashtable;

import org.w3c.dom.DOMException;
import org.w3c.dom.events.Event;
import org.w3c.dom.views.AbstractView;

import fi.hut.tml.xsmiles.Log;

/**
 * The factory for creating DOM event instances
 */
public class EventFactory {

    public static final String DOMFOCUSIN_EVENT = "DOMFocusIn";
    public static final String DOMFOCUSOUT_EVENT = "DOMFocusOut";
    public static final String DOMACTIVATE_EVENT = "DOMActivate";
    
    /** dispatch a custom DOM event, so that xforms message can listen to external window closing and
     * set the pseudoclass correctly
     */
    public static final String XSMILES_MESSAGE_CLOSE_EVENT ="xsm-message-close";
    
    private static Hashtable mouseEvents;
    private static Hashtable uiEvents;
    
    static {
        mouseEvents = new Hashtable();        
        mouseEvents.put("click", new boolean[] {true, true});
        mouseEvents.put("mousedown", new boolean[] {true, true});
        mouseEvents.put("mouseup", new boolean[] {true, true});
        mouseEvents.put("mouseover", new boolean[] {true, true});
        mouseEvents.put("mousemove", new boolean[] {true, false});
        mouseEvents.put("mouseout", new boolean[] {true, true});
        
        uiEvents = new Hashtable();
        uiEvents.put(DOMFOCUSIN_EVENT, new boolean[] { true, false });
        uiEvents.put(DOMFOCUSOUT_EVENT, new boolean[] { true, false });
        uiEvents.put(DOMACTIVATE_EVENT, new boolean[] { true, true });
    }

    public static Event createEvent(String eventType) throws DOMException {

        if (eventType.equalsIgnoreCase("Event")) {
            return new EventImpl();
        } 
        
        if (uiEvents.containsKey(eventType)) {
            Event event = new UIEventImpl();
            boolean[] args = (boolean[]) uiEvents.get(eventType);
            event.initEvent(eventType, args[0], args[1]);
            return event;
        }
        
        if (mouseEvents.containsKey(eventType)) {
            Event event = new MouseEventImpl();
            boolean[] args = (boolean[]) mouseEvents.get(eventType);
            event.initEvent(eventType, args[0], args[1]);
            return event;
        } 
        
        Log.debug("Unrecognised event type '" + eventType + "' you're about to get a default event!");
        return new EventImpl();
    }
    
    public static Event createEvent(String eventType, boolean canBubble, boolean cancelable) {
        Event event = new EventImpl();
        event.initEvent(eventType, canBubble, cancelable);
        return event;
    }
    
    /**
     * @deprecated use method createEvent(String eventType) instead 
     * @param typeArg
     * @param canBubbleArg
     * @param cancelableArg
     * @param viewArg
     * @param info
     * @return
     */
    public static UIEventImpl createUIEvent(String typeArg, boolean canBubbleArg, boolean cancelableArg, AbstractView viewArg, int info) {
        UIEventImpl theEvent = (UIEventImpl) EventFactory.createEvent(typeArg);
        theEvent.initUIEvent(typeArg, canBubbleArg, cancelableArg, viewArg, info);
        return theEvent;
    }
}
