/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
// $Id: XSmilesDocumentImpl.java,v 1.44 2008/02/18 07:48:03 jorutila Exp $
//
//package org.csiro.svg.dom.events;
package fi.hut.tml.xsmiles.dom;

import java.awt.Container;
import java.awt.EventQueue;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.ecma.ECMAScripter;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.css.CSSImpl2;
import fi.hut.tml.xsmiles.mlfc.xbl2.BindingHandler;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.ScriptElementImpl;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.XBLElementImpl;

/**
 * The factory for creating DOM event instances
 */
public class XSmilesDocumentImpl extends org.apache.xerces.dom.DocumentImpl
        implements ExtendedDocument, StylesheetService, AsyncChangeHandler
{
    protected XMLBroker broker;

    protected MLFC sourceMLFC = null;

    protected XSmilesStyleSheet styleSheet;

    /** the duration for CSS engine */
    public static double lastDurD = 0f;

    public static double lastDurC = 0f;

    public boolean inited = false;

    protected Vector bindingURLs;

    /*
     * BindingHandler that is responsible of the logic of handling the bindings
     * in this document. Keeps track that which binding belongs to which element, 
     * which element has which bindings and handles binding inheritance etc.
     */
    private BindingHandler bindingHandler;

    public XSmilesDocumentImpl()
    {
        super();
        broker = new XMLBroker();
    }

    /**
     * Returns the document's BindingHandler that handles the
     * logic of XML Binding Language 2.0 elements. With the
     * BindingHandler one can e.g. add bindings dynamically to
     * the document's elements.
     * @return Returns the bindinghandler responsible of handling
     * the bindings in this document.
     */
    public BindingHandler getBindingHandler()
    {
        // Create bindinghandler for XBL if not initialized
        if (this.bindingHandler==null)
            this.bindingHandler = new BindingHandler(this);

        return this.bindingHandler;
    }
    
    public void setBindingHandler(BindingHandler bh)
    {
        this.bindingHandler = bh;
    }

    public MLFC getHostMLFC()
    {
        if (sourceMLFC != null)
            return sourceMLFC;
        return broker.getHostMLFC();
    }

    public void setHostMLFC(MLFC source)
    {
        sourceMLFC = source;
    }

    public Hashtable getParasiteMLFCs()
    {
        if (broker == null)
            return null;
        return broker.getParasiteMLFCs();
    }

    public XSmilesStyleSheet getStyleSheet()
    {
        if (styleSheet == null)
        {
            styleSheet = new CSSImpl2((XSmilesXMLDocument) getHostMLFC()
                    .getXMLDocument(), this);
        }
        return styleSheet;
    }
    
    public void setStyleSheet(XSmilesStyleSheet sheet)
    {
        this.styleSheet = sheet;
    }

    /**
     * Create a DOM element
     */
    public Element createElement(String tagname) throws DOMException
    {
        return this.createElementNS(null, tagname);
    }

    public ProcessingInstruction createProcessingInstruction(String target,
            String data)
    {
        ProcessingInstruction proc = super.createProcessingInstruction(target,
                data);
        broker.createdProcessingInstruction(target, data, proc);
        return proc;
    }

    public Element createElementNS(String URI, String tagname)
            throws DOMException
    {
        Element elem = broker.createElementNS(this, URI, tagname);
        if (elem == null)
            elem = new VisualElementImpl(this, URI, tagname);
        return elem;
    }

    /**
     * NON-DOM: Xerces-specific constructor. "localName" is passed in, so we
     * don't need to create a new String for it.
     * 
     * @param namespaceURI
     *            The namespace URI of the element to create.
     * @param qualifiedName
     *            The qualified name of the element type to instantiate.
     * @param localName
     *            The local name of the element to instantiate.
     * @return Element A new Element object with the following attributes:
     * @throws DOMException
     *             INVALID_CHARACTER_ERR: Raised if the specified name contains
     *             an invalid character.
     */
    public Element createElementNS(String namespaceURI, String qualifiedName,
            String localpart) throws DOMException
    {
        return this.createElementNS(namespaceURI, qualifiedName);
        // From Xerces.CoreDocumentImpl:
        // return new ElementNSImpl(this, namespaceURI, qualifiedName,
        // localpart);
    }

    /**
     * Xerces-specific constructor. "localName" is passed in, so we don't need
     * to create a new String for it.
     * 
     * @param namespaceURI
     *            The namespace URI of the attribute to create. When it is null
     *            or an empty string, this method behaves like createAttribute.
     * @param qualifiedName
     *            The qualified name of the attribute to instantiate.
     * @param localName
     *            The local name of the attribute to instantiate.
     * @return Attr A new Attr object.
     * @throws DOMException
     *             INVALID_CHARACTER_ERR: Raised if the specified name contains
     *             an invalid character.
     */
    public Attr createAttributeNS(String namespaceURI, String qualifiedName,
            String localName) throws DOMException
    {
        return this.createAttributeNS(namespaceURI, qualifiedName);
    }

    /**
     * Creates a Text node given the specified string.
     */
    public Text createTextNode(String data)
    {
        return new XSmilesTextImpl(this, data);
    }

    /**
     * Create a DOM attribute.
     */
    public Attr createAttributeNS(String namespaceURI, String qualifiedName)
            throws DOMException
    {
        /*
         * Only attributes with non-null namespace and non 'xmlns' namespace are
         * sent to XML broker -> appropriate MLFC
         */
        if (namespaceURI == null)
            return super.createAttributeNS(namespaceURI, qualifiedName);
        if (namespaceURI.equals("http://www.w3.org/2000/xmlns/"))
            return super.createAttributeNS(namespaceURI, qualifiedName);
        if (namespaceURI.equals("http://www.w3.org/XML/1998/namespace"))
            return super.createAttributeNS(namespaceURI, qualifiedName);
        // Log.debug("** XSmilesDocumentImpl.createAttribute "+qualifiedName+"
        // ns:"+namespaceURI);
        Attr attr = broker.createAttributeNS(this, namespaceURI, qualifiedName);
        if (attr == null)
            attr = super.createAttributeNS(namespaceURI, qualifiedName);
        return attr;
    }

    public void destroy()
    {
        Element docelem = this.getDocumentElement();
        if (docelem instanceof XSmilesElementImpl)
        {
            ((XSmilesElementImpl) docelem).destroy();
        }
        broker.destroy();
        this.removeChild(this.getDocumentElement());
        if (eventListeners != null)
            eventListeners.clear();
        broker = null;
        Log.debug("Document destroyed");
    }

    public void init()
    {
        if (!broker.isPresentationDOM())
            return;
        // Log.debug("//////// DocumentImpl.init()");
        if (this.getDocumentElement() instanceof InitializableElement)
        {
            InitializableElement root = (InitializableElement) this
                    .getDocumentElement();
            this.setPIStylesheets();
            // MH: new CSS implementation

            // TODO: Init XBL PIs
            this.setPIXBLDocs();

            Log.debug("Init elements");
            root.init();
            assignPIBindings();

            // Find and load script elements
            NodeList scriptList =
            	this.getElementsByTagNameNS("http://www.w3.org/ns/xbl", "script");
            
            //Log.debug("Loading XBL script elements");
            for(int i = 0; i < scriptList.getLength(); i++)
            {
            	Node node = scriptList.item(i);
            	((ScriptElementImpl) node).loadScript();
            }
            
            // Search inline XBL elements, if found initialize bindingHandler
            NodeList xblTrees =
                this.getElementsByTagNameNS("http://www.w3.org/ns/xbl", "xbl");
            
            if (xblTrees.getLength()>0)
                this.getBindingHandler();
            
            /* Start applying bindings if the document has XBL specific elements
             * (XBL processing instruction or inline xbl elements).
             */
            if (this.bindingHandler!=null)
                this.bindingHandler.start();

        }
        // System.out.println("CSSEngineD--------------"+lastDurD+ "-sec");
        // System.out.println("CSSConvertor--------------"+lastDurC+ "-sec");
        lastDurD = 0;
        lastDurC = 0;
        this.inited = true;

    }

    void assignPIBindings()
    {
        /*
         * if (bindingDocuments != null) { Enumeration xblDocs =
         * bindingDocuments.elements(); XSmilesDocumentImpl xblDoc; while
         * (xblDocs.hasMoreElements()) { // Class cast exception: xblDoc =
         * (XSmilesDocumentImpl) xblDocs.nextElement(); NodeList bindings =
         * xblDoc.getDocumentElement().getChildNodes(); Node node; for (int i =
         * 0; i < bindings.getLength(); i++) { node = bindings.item(i); if (node
         * instanceof BindingElementImpl) { ((XSmilesElementImpl)
         * getDocumentElement()) .assignPIBindings((BindingElementImpl) node);
         *  } } // TODO } }
         */}

    /**
     * this is a crude hack for scripts, because XSmilesDocuments do not
     * implement real id's
     */
    public Element getElementById(String id)
    {
        Element docElem = this.getDocumentElement();
        if (docElem == null)
            return null;
        if (!(docElem instanceof XSmilesElementImpl))
            return null;
        XSmilesElementImpl xDocElem = (XSmilesElementImpl) docElem;
        return XSmilesElementImpl.searchElementWithId(xDocElem, id);

    }

    /**
     * Return an Vector of StylesheetReference objects containing all stylesheet
     * references of a certain type created by <?xml-stylesheet..> - processing
     * instruction, in the beginning of the doc.
     */
    public Vector getStylesheetReferences(String reqtype)
    {
        Vector stylesheets = new Vector();
        Node child = this.getFirstChild();
        while (child != null)
        {
            if (child.getNodeType() == Node.PROCESSING_INSTRUCTION_NODE)
            {
                ProcessingInstruction pi = (ProcessingInstruction) child;
                String target = pi.getTarget();
                String data = pi.getData();
                String type = XSmilesXMLDocument.getAttribute(data, "type");
                if (type != null && type.equals(reqtype))
                {
                    String href = XSmilesXMLDocument.getAttribute(data, "href");
                    if (href != null && href.length() > 0)
                    {
                        // TODO: alternate
                        StylesheetReference ref = new StylesheetReference(href,
                                type, false);
                        stylesheets.addElement(ref);
                    }
                }
            }
            child = child.getNextSibling();
        }
        return stylesheets;
    }

    public void setPIStylesheets()
    {
        Vector stylesheetlist = getStylesheetReferences("text/css");
        Enumeration enumeration = stylesheetlist.elements();
        while (enumeration.hasMoreElements())
        {
            StylesheetReference ref = (StylesheetReference) enumeration
                    .nextElement();
            try
            {
                // if the URL starts with #, then try to find the element with
                // that ID and use that embedded style
                if (ref.href.startsWith("#"))
                {
                    String id = ref.href.substring(1);
                    Element e = this.getElementById(id);
                    if (e != null && e instanceof XSmilesElementImpl)
                    {
                        XSmilesElementImpl xe = (XSmilesElementImpl) e;
                        // serialize the contents of the element
                        this.getStyleSheet().addXMLStyleSheet(xe.getText(),
                                new URL(xe.getBaseURI()));
                    }
                }
                else
                    this.getStyleSheet().addXMLStyleSheet(
                            new URL(getXMLDocument().getXMLURL(), ref.href));
            }
            catch (MalformedURLException mue)
            {
                Log.error(mue);
            }

        }
    }

    public void setPIXBLDocs()
    {
        Node child = this.getFirstChild();
        while (child != null)
        {
            if (child.getNodeType() == Node.PROCESSING_INSTRUCTION_NODE)
            {
                ProcessingInstruction pi = (ProcessingInstruction) child;
                String target = pi.getTarget();
                if (target.equals("xbl"))
                {
                    if (bindingURLs == null)
                        bindingURLs = new Vector();
                    String data = pi.getData();
                    String url = XSmilesXMLDocument.getAttribute(data, "href");
                    bindingURLs.add(url);
                    Log.debug("Target: " + target + ", URL: " + url);
                }
            }
            child = child.getNextSibling();
        }
        createBindingDocs();
    }

    void createBindingDocs()
    {
        if (bindingURLs != null)
        {
            Enumeration enu = bindingURLs.elements();
            String url;
            while (enu.hasMoreElements())
            {
                url = (String) enu.nextElement();
                try
                {
                    XLink xblDoc = new XLink(resolveURI(url));
                    XSmilesContentHandler handler = getHostMLFC()
                            .getMLFCListener().createContentHandler(xblDoc,
                                    new Container(), false);
                    handler.play();
                    this.getBindingHandler().addBindingDocumentPI(handler
                            .getXMLDocument());
                }
                catch (Exception e)
                {
                    Log.error(e);
                }
            }
        }
    }

    public Document loadBindingDocument(String documentURI)
    {
        return null;
    }

    public XMLDocument getXMLDocument()
    {
        return getHostMLFC().getXMLDocument();
    }

    protected boolean wasHTMLDocument = false;

    /**
     * this method returns true, if the original was HTML and then it was
     * converted to XHTML. This is useful for those features that need HTML case
     * independency
     */
    public boolean isHTMLDocument()
    {
        return this.wasHTMLDocument;
    }

    /** was this originally HTML document -> XHTML */
    public void setHTMLDocument(boolean isHTMLDoc)
    {
        this.wasHTMLDocument = isHTMLDoc;
    }

    public boolean isInited()
    {
        return this.inited;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.dom.ExtendedDocument#getTitle()
     */
    public String getTitle()
    {
        // TODO Auto-generated method stub
        try
        {
            return this.getHostMLFC().getTitle();
        }
        catch (Throwable t)
        {
            Log.error(t.getMessage());
        }
        return "";
    }

    public AsyncChangeHandler getChangeHandler()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void invokeLater(Runnable doRun)
    {
        EventQueue.invokeLater(doRun);
    }

    public boolean isDispatchThread()
    {
        return EventQueue.isDispatchThread();
    }

    public Element getAnonymousElementByAttribute(Element root,
            String attrname, String value)
    {
        Node child = root.getFirstChild();
        while (child != null)
        {
            if (child.getNodeType() == Node.ELEMENT_NODE)
            {
                Element e = (Element) child;
                Attr attr = e.getAttributeNode(attrname);
                if (attr != null)
                {
                    String attrvalue = attr.getValue();
                    if (attrvalue.equals(value))
                        return e;
                }
                Element retE = this.getAnonymousElementByAttribute(e, attrname,
                        value);
                if (retE != null)
                    return retE;
            }
            child = child.getNextSibling();
        }
        return null;
    }

    public void eval(String scriptText)
    {
        // TODO Auto-generated method stub
        this.getECMAScripter().eval(scriptText);
    }

    public void exposeToScriptEngine(String name, Object theObject)
    {
        // TODO Auto-generated method stub
        this.getECMAScripter().exposeToScriptEngine(name, theObject);
    }

    public void deleteExposedObject(String name, Object theObject)
    {
        // TODO Auto-generated method stub
        this.getECMAScripter().deleteExposedObject(name, theObject);
    }

    public ECMAScripter getECMAScripter()
    {
        try
        {
            return this.getHostMLFC().getXMLDocument().getECMAScripter();
        }
        catch (Exception e)
        {
            Log.error(e);
            return null;
        }
    }

    /**
     * Resolve an uri to an absolute uri using the base-uri of the current
     * element.
     * 
     * @param src
     *            the uri to resolve
     * @exception MalformedURLException
     *                if the resulting URL has an unsupported scheme or a parse
     *                error occured
     */
    public URL resolveURI(String src) throws MalformedURLException
    {
        String baseURI = this.getBaseURI();
        // Log.debug("Base URI: "+baseURI);
        if (baseURI != null)
            return new URL(new URL(getBaseURI()), src);
        else
            return new URL(src);
    }

} // XSmilesDocumentImpl
