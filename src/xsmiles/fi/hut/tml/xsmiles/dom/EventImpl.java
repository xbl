// EventImpl.java
//
/*****************************************************************************
 * Copyright (C) CSIRO Mathematical and Information Sciences.                *
 * All rights reserved.                                                      *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the CSIRO Software License  *
 * version 1.0, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/
//
// $Id: EventImpl.java,v 1.6 2003/05/12 11:04:44 honkkis Exp $
//

//package org.csiro.svg.dom.events;
package fi.hut.tml.xsmiles.dom;


import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.events.Event;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLHandler;
import java.util.LinkedList; 
/**
 * Implementation of a general DOM event
 */
public class EventImpl extends org.apache.xerces.dom.events.EventImpl implements Event {

    
  protected boolean inited=false;
  private LinkedList defaultHandlers=new LinkedList(); 
  private boolean defaultPhase = false; 
  
  public void initEvent(String eventTypeArg, boolean canBubbleArg, boolean cancelableArg) {
    super.initEvent(eventTypeArg,canBubbleArg,cancelableArg);
    this.inited=true;
  }
  public boolean isInited()
  {
      return this.inited;
  }

  public String toString()
  {
      return getClass().getName() + "(type=" + getType() + ", target=" + getTarget() + ")";
  }
  
  public void addHandler(XBLHandler handler)
  {
  this.defaultHandlers.add(handler); 
  }
  /** 
   * A method for xbl2 default phase processing. This methods is used for the
   * xbl2 event default phase processing defined by the W3C specification. 
   */
  public void startDefaultPhase() 
  {
	  XBLHandler handler = null; 
	  this.defaultPhase = true; 
	   try 
	   {
		   while (true) 
		   {
		   if (this.preventDefault)
			   return; 
     	   handler=(XBLHandler)this.defaultHandlers.remove(0);
     	   this.currentTarget = handler.getElement(); 
		   handler.getEventListener().handleEvent(this);    
		   }
	   }
	   catch (IndexOutOfBoundsException iob) 
	   {
		   return; 
	   }
	     
  }

  public boolean getDefaultPhase ()
  {
	  return this.defaultPhase; 
  }
} // EventImpl
