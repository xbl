/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
// $Id: XSmilesElementImpl.java,v 1.79 2008/02/18 07:48:03 jorutila Exp $
//
//package org.csiro.svg.dom.events;
package fi.hut.tml.xsmiles.dom;

import java.awt.EventQueue;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.Vector;

import org.apache.xerces.dom.ChildNode;
import org.apache.xerces.dom.ElementNSImpl;
import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.events.Event;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.content.ResourceFetcher;
import fi.hut.tml.xsmiles.content.ResourceReferencer;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectorParser;
import fi.hut.tml.xsmiles.mlfc.css.CSSSelectors;
import fi.hut.tml.xsmiles.mlfc.css.RuleTreeNode;
import fi.hut.tml.xsmiles.mlfc.xbl2.dom.BindingElementImpl; 
import fi.hut.tml.xsmiles.mlfc.xbl2.ElementXBL;
import fi.hut.tml.xsmiles.mlfc.xbl2.XBLImplementationList;
import fi.hut.tml.xsmiles.timesheet.TimedElement;
import fi.hut.tml.xsmiles.timesheet.timer.TimedEventTable.TimedEvent;
import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * XSmiles DOM Elements
 */
public class XSmilesElementImpl extends ElementNSImpl
        implements
            StylableElement,
            ResourceFetcher,
            PseudoClassController,
            InitializableElement
{

    public final static short UNINITIALIZED = 0;
    public final static short INITIALIZED = 1;
    public final static short INITIALIZING = 15;
    public final static short DESTROYED = 666;

    /**
     * the CSS style declaration for this element, Only updateStyle() is allowed
     * to change this directly to null
     */
    protected CSSStyleDeclaration style = null;

    /** the layout element (used by e.g. XHTML) */
    // public LayoutElement layoutElement;
    /** the current status (initialized etc.) */
    protected short elementStatus = UNINITIALIZED;

    protected Hashtable setPseudoClasses;// = new Hashtable();

    private RuleTreeNode ruleTreeNode = null;

    private XSmilesElementImpl originalContent = null;

    private Vector undistributedChildren = null;
    private Vector isDistributed = null;
    
    private boolean authorSheets = true;

    /* if this element is redistributed under xbl content element,
     * this variable has the value of apply-binding-sheets attribute of
     * the corresponding content element. Otherwise this value MUST be true! 
     * The value is used to limit propagetion of XBL-styles. */
    private boolean xblApplyBindingSheets = true;

    public XSmilesElementImpl(org.apache.xerces.dom.DocumentImpl ownerDocument, String value)
    {
        super(ownerDocument, value);
    }

    public XSmilesElementImpl(org.apache.xerces.dom.DocumentImpl ownerDocument, String namespaceURI,
            String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
    }

    /**
     * Initialize this element.
     */
    public void init()
    {
        this.elementStatus = INITIALIZING;
        if (this.getOwnerDocument() instanceof ExtendedDocument)
        {
            // initStyle(((ExtendedDocument)
            // this.getOwnerDocument()).getStyleSheet());
        }

        NodeList children = getChildNodes(true);
        // If no children, then just exit

        for (int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            if (child != null)
                if (child instanceof InitializableElement)
                    ((InitializableElement) child).init();
        }

        // init all attributes
        if (attributes != null)
        {
            for (int i = 0; i < attributes.getLength(); i++)
            {
                Attr attr = (Attr) attributes.item(i);
                if (attr instanceof InitializableElement)
                {
                    ((InitializableElement) attr).init();
                }
            }
        }
        // Create styles during initialization.
        // this.getStyle();
        this.elementStatus = INITIALIZED;
    }

    public short getElementStatus()
    {
        return this.elementStatus;
    }

    public void dispatchEventInMainThread(Event ev)
    {
        if (this.getOwnerDocument() instanceof AsyncChangeHandler)
        {
            if (((AsyncChangeHandler) this.getOwnerDocument()).isDispatchThread())
            {
                super.dispatchEvent(ev);
                return;
            }
        }
        final Event e = ev;
        Runnable r = new Runnable()
        {
            public void run()
            {
                dispatchEvent(e);
            }
        };
        if (this.getOwnerDocument() instanceof AsyncChangeHandler)
        {
            ((AsyncChangeHandler) this.getOwnerDocument()).invokeLater(r);
        }
        else
        {
            EventQueue.invokeLater(r);
        }
    }

    /**
     * Update this and child elements' styles
     */
    public void updateStyle()
    {
        if (ruleTreeNode != null)
        {
            ruleTreeNode.removeElement(this);
            ruleTreeNode = null;
        }
        
        String xblBinding = null;
        if(this.style != null)
        {
        	// Look for existing XBL bindings in style and remove them
        	xblBinding = this.style.getPropertyValue("binding");
        }
        
        if(xblBinding != null && 
        		xblBinding.startsWith("url(") &&
        		xblBinding.charAt(xblBinding.length()-1) == ')')
        {
        	Log.debug(this.name + ": CSS XBL binding to be removed: " + xblBinding);
        	// Strip enclosing "url()" for addBinding
        	xblBinding = xblBinding.substring(4, xblBinding.length()-1);
        	removeBinding(xblBinding);
        }/*
        	Vector b = ((XSmilesDocumentImpl)this.ownerDocument).getBindingHandler().getBindings(this);
        
        	BindingElementImpl binding = null;
        	if(b != null)
        	{
        		for(int i = 0; i < b.size(); i++)
        		{
        			if(((BindingElementImpl)b.elementAt(i)).getId().equals(xblBinding))
        			{
        				binding = (BindingElementImpl)b.elementAt(i);
        				Log.debug("		break!");
        	     		if(binding != null)
                		{
                			((XSmilesDocumentImpl) this.ownerDocument).getBindingHandler().removeBinding(binding, this);
                			if (binding.getAttribute("extends") != "")
                				this.removeBinding(binding.getAttribute("extends"));
                		}
        	     		((XSmilesDocumentImpl) this.ownerDocument).getBindingHandler().applyBindings();
        	            
        				break;
        			}
        		}
        	}*/
      
        
        // call CSSImpl to remove the node from style tree
        this.style = null;
        // Log.debug("updateStyle for "+this);
        if (this.getOwnerDocument() instanceof ExtendedDocument)
        {
            // initStyle(((ExtendedDocument)
            // this.getOwnerDocument()).getStyleSheet());
        }
        // This is the old swing CSS implementation
        /*
         * if (this.getLayoutElement() != null)
         * this.getLayoutElement().setStyle(this);
         */
        NodeList children = getChildNodes(true); // this reads also
        // pseudoelements
        // Init all children
        for (int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            if (child instanceof XSmilesElementImpl)
                ((XSmilesElementImpl) child).updateStyle();
            // if (child instanceof OnEventElementImpl)
            // ((OnEventElementImpl)child).init();
        }
    }

    public void setRuleNode(RuleTreeNode rtn)
    {

        ruleTreeNode = rtn;
    }

    public RuleTreeNode getRuleNode()
    {

        return ruleTreeNode;
    }
    public static final String xblpropname = "xblbinding";

    public void initStyle(XSmilesStyleSheet styleSheet)
    {
        try
        {
            // Log.debug("InitStyle:"+this);
            // if (this instanceof StylableElement && (this.getParentNode()
            // instanceof StylableElement|| this.getParentNode() instanceof
            // Document) && styleSheet
            // != null)
            if (this instanceof StylableElement && styleSheet != null)
            {
                CSSStyleDeclaration cssStyle = styleSheet.getParsedStyle((StylableElement) this);
                // this.setStyle(cssStyle);
                // Log.debug(this+ " Set style: "+cssStyle);

                // elementStyle =
                // StyleSwingConvertor.convert((XSmilesCSSStyleDeclarationImpl)style,
                // this.getLocalName());
                // Log.debug("--------------------" + elementStyle.toString());

            }
            else
            {
                Log.debug("Note: " + this + "'s parent " + this.getParentNode()
                        + this.getParentNode().getClass().toString()
                        + "was not instanceof StylableElement");
                // setStyle(new XSmilesCSSStyleDeclarationImpl(null));
                // elementStyle = (new
                // StyleContext()).addStyle(this.getLocalName(), null);
            }
        }
        catch (Exception e)
        {
            Log.error(e);
            e.printStackTrace();
        }
    }
    
    
     /**
     * Destroy this element and its descendants recursively.
     * 
     * NOTE: When overriding this method, please be sure to call
     * super.destroy(), otherwise the destroy call will not propagate.
     * 
     * NOTE: Actually, the recursion, which destroys the elements should be
     * here, because usually everybody forgets to call super.destroy when
     * overriding, thus causing the destroy sceme to break down.
     */
    public void destroy()
    {
        // Destroy all children
        Node child = this.getFirstChild();
        while (child != null)
        {
            if (child instanceof XSmilesElementImpl)
                ((XSmilesElementImpl) child).destroy();
            child = child.getNextSibling();
        }
        this.elementStatus = DESTROYED;

    }

    /**
     * returns the stored style for this element
     */
    public CSSStyleDeclaration getStyle()
    {
        if (this.style == null)
        {
            // style was not yet calculated, calculate now
            if (this.getOwnerDocument() instanceof ExtendedDocument)
            {
                try {
                    initStyle(((ExtendedDocument) this.getOwnerDocument()).getStyleSheet());
                } catch (NullPointerException e)
                {
                    initStyle(null);
                }
            }
        }
        return this.style;
    }

    public boolean hasStyle()
    {
        if (style != null)
        {
            return true;
        }
        return false;
    }

    /** set the resolved CSS style for this element */
    public void setStyle(CSSStyleDeclaration a_style)
    {
        this.style = a_style;
        
        // Set XBL binding
        String xblBinding = null;
        if(this.style != null)
        	xblBinding = this.style.getPropertyValue("binding");
        if(xblBinding != null && 
        		xblBinding.startsWith("url(") &&
        		xblBinding.charAt(xblBinding.length()-1) == ')')
        {
        	Log.debug(this.name + ": CSS XBL binding found: " + xblBinding);
        	// Strip enclosing "url()" for addBinding
        	xblBinding = xblBinding.substring(4, xblBinding.length()-1);
            ((XSmilesDocumentImpl) this.ownerDocument).getBindingHandler().addBindingDynamically(xblBinding, this); 
        }
    }

    /**
     * by default return the value of 'id' attribute, any element can override
     * this
     */
    public String getId()
    {
        return getAttribute("id");
    }

    /**
     * By default return null, so style attribute is not supported, this can be
     * overridden by subclasses
     */
    public String getStyleAttrValue()
    {
        return null;
    }

    /**
     * Searches for element with id from the tree under element e.
     * 
     * @param e
     *            start element
     * @param id
     *            id to search for
     * @return the first element with id, or null if not found.
     */
    public static Element searchElementWithId(Element e, String id)
    {
        return searchElementWithId(e, id, 1);
    }

    /**
     * Searches for the n-th element with id from the tree under element e.
     * 
     * @param e
     *            start element
     * @param id
     *            id to search for
     * @param n
     *            which occurence
     * @return the element with id, or null if not found.
     */
    public static Element searchElementWithId(Element e, String id, int n)
    {
        if (id == null || id.length() == 0 || n < 1)
            return null;

        return searchElementWithId(e, id, new int[]{n});
    }

    private static Element searchElementWithId(Element e, String id, int[] n)
    {
        // This is a very ugly and slow way to register the listener
        // - Goes through the entire tree to find the id!
        Element result = null;

        String elemId = null;
        if (e instanceof XSmilesElementImpl)
            elemId = ((XSmilesElementImpl) e).getId();
        else
            elemId = e.getAttribute("id");
        if (id.equals(elemId))
            return e;

        NodeList children = e.getChildNodes();
        Node node = null;

        // If no children, then return null
        if (children.getLength() == 0)
        {
            return null;
        }

        for (int i = 0; i < children.getLength(); i++)
        {
            node = children.item(i);
            if (node instanceof Element)
            {
                result = searchElementWithId((Element) node, id, n);
                // If found, then return it up.
                if (result != null)
                {
                    if (--n[0] <= 0)
                        return result;
                }
            }
        }

        // Not this one or any of the children, return null
        return null;
    }

    public String getText()
    {
        return getText(this);
    }

    public static String getText(Node node)
    {
        String text = "";
        if (node.getNodeType() == Node.ATTRIBUTE_NODE)
        {
            return node.getNodeValue();
        }
        else if (node.getNodeType() == Node.ELEMENT_NODE)
        {
            Node child = node.getFirstChild();
            while (child != null)
            {
                if (child.getNodeType() == Node.TEXT_NODE)
                {
                    text += child.getNodeValue();
                }
                if (child.getNodeType() == Node.CDATA_SECTION_NODE)
                {
                    text += child.getNodeValue();
                }
                child = child.getNextSibling();
            }
            return text;
        }
        return null;
    }

    public void setPseudoClass(String pseudoclass, boolean value)
    {

        if (value && !getPseudoClass(pseudoclass))
        {
            if (setPseudoClasses == null)
                setPseudoClasses = new Hashtable(3);
            setPseudoClasses.put(pseudoclass, new Boolean(value));
        }
        if (!value && getPseudoClass(pseudoclass))
        {
            if (setPseudoClasses == null)
                setPseudoClasses = new Hashtable(3);
            setPseudoClasses.remove(pseudoclass);
        }
    }

    public boolean getPseudoClass(String pseudoclass)
    {
        return (setPseudoClasses == null ? false : setPseudoClasses.containsKey(pseudoclass));
    }

    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        if (pseudoclass.equals("first-child"))
        {
            // search siblings, and return true, if none is an element
            boolean elementFound = false;
            Node prev = this.getPreviousSibling();
            while (true)
            {
                // if we reach the first sibling return true
                if (prev == null)
                    return true;
                // if we have a previous element sibling, return false,
                // we are not first child
                if (prev.getNodeType() == Node.ELEMENT_NODE)
                    return false;
                prev = prev.getPreviousSibling();
            }
        }
        return getPseudoClass(pseudoclass);
    }

    /** set the layout element corresponding to this node (used by e.g. XHTML) */
    /*
     * public void setLayoutElement(LayoutElement elem) { this.layoutElement =
     * elem; }
     */

    /** get the layout element corresponding to this node (used by e.g. XHTML) */
    /*
     * public LayoutElement getLayoutElement() { return this.layoutElement; }
     */
    /** called if this node has been removed from layout tree */
    /*
     * public void removeLayoutElement() { this.layoutElement = new Object(); }
     */

    /**
     * Resolve an uri to an absolute uri using the base-uri of the current
     * element.
     * 
     * @param src
     *            the uri to resolve
     * @exception MalformedURLException
     *                if the resulting URL has an unsupported scheme or a parse
     *                error occured
     */
    public URL resolveURI(String src) throws MalformedURLException
    {
        String baseURI = this.getBaseURI();
        // Log.debug("Base URI: "+baseURI);
        if (baseURI != null)
            return new URL(new URL(getBaseURI()), src);
        else
            return new URL(src);
    }

    public Object clone() throws CloneNotSupportedException
    {
        Log.debug("Cloning:" + this);
        XSmilesElementImpl element = (XSmilesElementImpl) super.clone();
        // XSmilesElement clones the pseudo-elements as well
        // control.valuePseudoElement= new ValuePseudoElement(control);
        if (this instanceof PseudoElementContainerService)
        {
            this.clonePseudoElements((PseudoElementContainerService) this,
                    (PseudoElementContainerService) element);
        }
        return element;
    }

    public void clonePseudoElements(PseudoElementContainerService from, PseudoElementContainerService to)
    {
        Vector fromV = from.getPseudoElements();
        Vector toV = to.getPseudoElements();
        toV.removeAllElements();
        if (fromV == null || fromV.size() == 0)
            return;
        else
        {
            Enumeration enumeration = fromV.elements();
            while (enumeration.hasMoreElements())
            {
                Element e = (Element) enumeration.nextElement();
                Node clone = e.cloneNode(true);
                toV.addElement(clone);
            }
        }
    }

    public void cloneSubtree(Element source)
    {

    }

    /**
     * implementation of a nodelist that contains also pseudoelements
     * 
     * @author honkkis
     * 
     */
    public class PseudoNodeList implements NodeList
    {

        Node owner;

        public PseudoNodeList(Node o)
        {
            this.owner = o;
        }

        public Node item(int n)
        {
            NodeList normalNodes = owner.getChildNodes();
            if (n < normalNodes.getLength())
                return normalNodes.item(n);
            if (owner instanceof PseudoElementContainerService)
            {
                int pseudon = n - normalNodes.getLength();
                Vector pseudoElements = ((PseudoElementContainerService) owner).getPseudoElements();
                if (pseudoElements == null)
                    return null;
                // if (pseudoElements.size()<pseudon) return null;
                else
                    try
                    {
                        return (Node) pseudoElements.elementAt(pseudon);
                    }
                    catch (ArrayIndexOutOfBoundsException e)
                    {
                        return null;
                    }
            }
            return null;
        }

        public int getLength()
        {
            int normalcount = owner.getChildNodes().getLength();
            if (owner instanceof PseudoElementContainerService)
            {
                int pseudoCount = 0, count = 0;
                Vector pseudoElements = ((PseudoElementContainerService) owner).getPseudoElements();
                if (pseudoElements != null)
                    pseudoCount = pseudoElements.size();
                return normalcount + pseudoCount;
            }
            return normalcount;
        }
    }

    /**
     * this method includes also pseudoelements
     * 
     * @param includePseudoElements
     * @return
     */
    public NodeList getChildNodes(boolean includePseudoElements)
    {
        if (includePseudoElements && this instanceof PseudoElementContainerService)
        {
            NodeList pseudoNodeList = new PseudoNodeList(this);
            return pseudoNodeList;
        }
        return this.getChildNodes();
    }

    public static void debugNode(Node n)
    {
        try
        {
            fi.hut.tml.xsmiles.xslt.JaxpXSLT.SerializeNode(new java.io.OutputStreamWriter(System.out),
                    n, true, true, false);
        }
        catch (Exception e)
        {
            Log.error(e);
        }
    }

    public static void debugNode(Node n, boolean printPseudoElements)
    {
        if (!printPseudoElements)
            debugNode(n);
        else
            try
            {
                Log.debug(n.toString());
                Node child = n.getFirstChild();
                while (child != null)
                {
                    debugNode(child, printPseudoElements);
                    child = child.getNextSibling();
                }
                if (printPseudoElements && n instanceof PseudoElementContainerService)
                {
                    Enumeration enumeration = ((PseudoElementContainerService) n).getPseudoElements()
                            .elements();
                    while (enumeration.hasMoreElements())
                    {
                        XSmilesElementImpl e = (XSmilesElementImpl) enumeration.nextElement();
                        debugNode(e, true);
                    }
                }
            }
            catch (Exception e)
            {
                Log.error(e);
            }
    }

    public ResourceReferencer getResourceReferencer()
    {
        MLFC mlfc = ((ExtendedDocument) this.getOwnerDocument()).getHostMLFC();
        return mlfc.getResourceReferencer();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
     */
    public XSmilesConnection get(URL dest, short type) throws Exception
    {
        try
        {
            MLFC mlfc = ((ExtendedDocument) this.getOwnerDocument()).getHostMLFC();
            return mlfc.get(dest, type);
        }
        catch (Exception e)
        {
            Log.error(e);
            return HTTP.get(dest, null);
        }
    }

    /**
     * Replaces original-node with current-node. 
     * @param original
     * @param current
     */
	public void replaceNode(XSmilesElementImpl original, XSmilesElementImpl current)
    {
        this.insertBefore(current, original);
        this.removeChild(original);
    }

    /**
     * Returns the original DOM tree of this element.
     * @return XSmilesElementImpl
     */
    public XSmilesElementImpl getOriginalClone()
    {
        return (XSmilesElementImpl) originalContent;
        // return (XSmilesElementImpl) this.cloneNode(true);
    }

    /**
     * Changes this node back to its original DOM tree.
     * Changes node's undistributed children to same as in original DOM tree.
     */
    public void resetFinalFlattenedTree()
    {
   	    while(this.getFirstChild()!=null)
   	    {
		    this.removeChild(this.getFirstChild());
   	    }
   	    
        for(int i = 0;  i < this.undistributedChildren.size(); i++)
        {
        	this.appendChild((Node)this.undistributedChildren.elementAt(i));
        	this.isDistributed.remove(i);
        	this.isDistributed.insertElementAt(new Boolean(false), i);
        }
   	    
        this.fNodeListCache = null;
    }
    
    public boolean isInitedForReset()
    {
        return this.originalContent != null;
    }

    /**
     * Clones this element's content so this element can be resetted
     * with resetFinalFlattenedTree() sometime.
     */
    public boolean initFinalFlattenedTree()
    {
        if (this.originalContent != null) return false;
        this.originalContent = (XSmilesElementImpl) this.cloneNode(true);
        // Should the original childs actually be reverted from undistributedChildren instead?

        this.undistributedChildren = new Vector();
        this.isDistributed = new Vector();
        Node n = this.getFirstChild();
        while(n != null)
        {
        	this.undistributedChildren.add(n);
        	this.isDistributed.add(new Boolean(false));
        	n = n.getNextSibling();
        }
        
        return true;
    }

    /**
     * Method for getting child nodes that aren't yet distributed.
     * @return NodeList of undistributed children
     */
    public Vector getUndistributedChildNodes()
    {
    	Vector v = new Vector();
    	if(this.isDistributed == null)
    	{
    		return v;
    	}
    	for(int i = 0; i < this.isDistributed.size(); i++)
    	{
    		if(((Boolean)this.isDistributed.elementAt(i)).booleanValue() == false)
    		{
    			v.add((Node)this.undistributedChildren.elementAt(i));
    		}
    	}
        return v;
    }

    /**
     * Removes the the given node from this element, so 
     * that the node can be redistributed.
     * 	
     * @param node Node to be removed
     * 
     * @return The element removed.
     * @return null if the element cannot be removed
     */
    public Node removeUndistributedChild(Node node)
    {
    	if(this.isDistributed == null)
    	{
    		return null;
    	}
    	int i = undistributedChildren.indexOf(node);
    	
    	if(i >= 0 && ((Boolean)this.isDistributed.elementAt(i)).booleanValue() == false)
    	{
    	    this.isDistributed.removeElementAt(i);
    	    this.isDistributed.insertElementAt(new Boolean(true), i);
            return node; 
    	}
    	return null;
    }
    
    /**
     * Adds a node back to this element's content.
     * 
     * @param node Element to be added.
     * 
     * @return true if the operation succeed
     * @return false otherwise
     * 
     */
    public boolean addUndistributedChild(Node node)
    {
    	if(this.isDistributed == null)
    	{
    		return false;
    	}
        int i = this.undistributedChildren.indexOf(node);   
        
        if(i >= 0 && ((Boolean)this.isDistributed.elementAt(i)).booleanValue() == true)
        {
    	    this.isDistributed.removeElementAt(i);
    	    this.isDistributed.insertElementAt(new Boolean(false), i);
            return true;     	
        }  
        return false;
    }

    public void addBinding(String bindingURI)
    {        
        ((XSmilesDocumentImpl) this.ownerDocument).
        getBindingHandler().addBindingDynamically(bindingURI, this); 
        
        if(this instanceof VisualElementImpl)
        	((VisualElementImpl)this).styleChanged();
    }
      
    public void removeBinding(String bindingURI)
    {
        Log.debug("removeBinding("+bindingURI+")");
        SelectorList selectors = CSSSelectorParser.parseSelectors(bindingURI);
        CSSSelectors cssSelectors = new CSSSelectors();

        /*
         * Loop through the selector list and find if the element matches any of
         * these selectors.
         */
        for (int i = 0; i < selectors.getLength(); i++)
        {
            ListIterator iter = ((XSmilesDocumentImpl)this.ownerDocument).getBindingHandler().getAllBindings().listIterator();
            while(iter.hasNext())
            {                
                BindingElementImpl binding = (BindingElementImpl)iter.next();
                if (cssSelectors.selectorMatchesElem(selectors.item(i), binding))
                {
                    ((XSmilesDocumentImpl) this.ownerDocument).getBindingHandler().removeBinding(binding, this);
                    if (binding.getAttribute("extends") != "")
                        this.removeBinding(binding.getAttribute("extends"));
                }
            }
        }      
        
        if(this instanceof VisualElementImpl)
        	((VisualElementImpl)this).styleChanged();
        
        ((XSmilesDocumentImpl) this.ownerDocument).getBindingHandler().applyBindings();

    
    }
    

    public boolean hasBinding(String bindingURI)
    {
        // TODO Auto-generated method stub
        Log.debug("hasBinding("+bindingURI+") NOT IMPLEMENTED");
        return false;
    }
    /**
     * Sets if styles included from XBL-tree into whose content-element this
     * node is redistributed are applied to this element and its children.
     * 
     * If this node is not redistributed in xbl-content element call of this 
     * method has no effect.
     * 
     * @param b value to be set
     */
    
    public void setApplyBindingSheets(boolean b){
    	this.xblApplyBindingSheets = b;
    }
   
    /**
     * Returns value telling if styles included from XBL-tree into whose 
     * content-element this node is redistributed are applied to this element 
     * and its children.
     * 
     * @return as explained above
     */
    public boolean getApplyBindingSheets(){
    	return this.xblApplyBindingSheets;
    }

    /**
     * Sets the attribute authorSheets to value. This is when a binding has been applied
     * to this element and it's templates apply-author-sheets is according to value. Needed
     * in xbl 2.0 apply-author-sheets
     *@param boolean value of templates apply-author-sheets
     *@return
     */
    public void setAuthorSheets(boolean value)
    {
    	this.authorSheets = value;
    }

    /**
     * Returns the value of authorSheets.
     *@return boolean value of authorSheets
     */
   public boolean getAuthorSheets()
    {
    	return this.authorSheets;
    }
    
	/**
	 * The xblImplementations  attribute on all elements must return an
	 * instance of an XBLImplementationList object (the same object for the
	 * lifetime of the element), which is a live list of the implementation
	 * objects provided by the bindings for that bound element at any
	 * particular point in time.
	 * @return
	 */
    public XBLImplementationList getXblImplementations() {
    	XBLImplementationList list = null;
    	// TODO get the list from BindingHandler?
    	return list;
    }
}
