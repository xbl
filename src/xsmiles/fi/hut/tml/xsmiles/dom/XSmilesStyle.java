
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.dom; 
 
import java.awt.Component;

import java.util.Enumeration;
import java.util.Hashtable;

public interface XSmilesStyle {

	public int getAttributeCount();

    public boolean isDefined(Object attrName);

//    public boolean isEqual(XSmilesStyle attr);

//    public XSmilesStyle copyAttributes();

    public Object getAttribute(Object key);

    public Enumeration getAttributeNames();

    public boolean containsAttribute(Object name, Object value);

//    public boolean containsAttributes(XSmilesStyle attributes);

//    public XSmilesStyle getResolveParent();

//    public static final Object NameAttribute = StyleConstants.NameAttribute;

//    public static final Object ResolveAttribute = StyleConstants.ResolveAttribute;

//------------------------------------------------------------------------------------

	public void addAttribute(Object name, Object value);

//    public void addAttributes(XSmilesStyle attributes);

    public void removeAttribute(Object name);

    public void removeAttributes(Enumeration names);

//    public void removeAttributes(XSmilesStyle attributes);

//    public void setResolveParent(XSmilesStyle parent);


//------------------------------------------------------------------------------------

    public String getName();

	
}
 
 
 
 
 
 
 
 