/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;



/*
 * XSmiles refreshable service interface.
 * Used for inter-element communication
 */
public interface StylesheetService {

	/**
	 * get the stylesheet object
	 */
	public XSmilesStyleSheet getStyleSheet();
	

}
