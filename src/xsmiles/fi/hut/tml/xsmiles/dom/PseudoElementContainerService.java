/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;

import java.util.Vector;

import org.w3c.dom.Element;

/*
 * XSmiles animation service interface.
 * Used for inter-element communication.
 */

public interface PseudoElementContainerService {
    
    /** get the vector containing all pseudoelements of this element.
     * null or empty vector means that there are no pseudoelements
     */
    public Vector getPseudoElements();
    
    /** since the pseudoelement removals are not catched by the xsmilesvisualelement, this
     * method can be used to notify a remove
     * @param elem
     */
    public void notifyPseudoRemoved(Element elem);
    
/*
     public void insertElementAt(PseudoElement e, int location);
    public void add(PseudoElement e);
    public void remove();
 
 */
}
