/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.css.CSSStyleDeclaration;

/**
 * An element that implements this interface should be layed out as
 * a normal flow layout element, such as <html:p>
 */ 
public interface FlowLayoutElement extends StylableElement
{
}
