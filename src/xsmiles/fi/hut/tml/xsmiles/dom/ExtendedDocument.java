/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
// $Id: ExtendedDocument.java,v 1.14 2006/05/23 11:12:09 honkkis Exp $
//

//package org.csiro.svg.dom.events;
package fi.hut.tml.xsmiles.dom;

import java.util.Hashtable;

import org.w3c.dom.Element;

import fi.hut.tml.xsmiles.ecma.ScriptRunner;
import fi.hut.tml.xsmiles.mlfc.MLFC;

/**
 * The factory for creating DOM event instances
 */
public interface ExtendedDocument extends org.w3c.dom.Document, ScriptRunner
{
    
    //  public XFormsElementHandler getXFormsHandler();
    
    /**
     * Accessor function to the browser object.
     */
    //  public BrowserWindow getBrowser() ;
    //  public XMLDocument getXMLDocument();
    //  public void setXMLDocument(XMLDocument xml_doc);
    //  public org.apache.xerces.dom.DocumentImpl getDocumentImpl();
    public MLFC getHostMLFC();
    /** this is mainly for source MLFC */
    public void setHostMLFC(MLFC mlfc);
    public Hashtable getParasiteMLFCs();
    public void init();
    public void destroy();
    public XSmilesStyleSheet getStyleSheet();
    
    public String getBaseURI();
    /** this method returns true, if the original was HTML
     * and then it was converted to XHTML. This is useful
     * for those features that need HTML case independency
     */
    public boolean isHTMLDocument();
    /** was this originally HTML document -> XHTML */
    public void setHTMLDocument(boolean isHTMLDoc);
    
    public boolean isInited();
    
    /** this is for executing layout or DOM changing code from an external thread */
    public AsyncChangeHandler getChangeHandler();
    /**
     * @return
     */
    public String getTitle();
    public Element getAnonymousElementByAttribute(Element root, String attrname, String value);
    
} // EventFactory
