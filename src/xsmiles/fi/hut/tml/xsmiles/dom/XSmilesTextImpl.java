/*
 * XSmilesTextImpl.java
 *
 * Created on March 7, 2003, 2:19 PM
 */

package fi.hut.tml.xsmiles.dom;

import org.apache.xerces.dom.TextImpl;
import org.apache.xerces.dom.CoreDocumentImpl;

/**
 *
 * @author  mpohja
 */
public class XSmilesTextImpl extends TextImpl implements XSmilesLayoutReference
{
    /** the layout element (used by e.g. XHTML) */
    protected LayoutElement layoutElement;

    /** Creates a new instance of XSmilesTextImpl */
    public XSmilesTextImpl(CoreDocumentImpl ownerDoc, String data)
    {
        super(ownerDoc, data);
    }

    /** set the layout element corresponding to this node (used by e.g. XHTML) */
    public void setLayoutElement(LayoutElement elem)
    {
        this.layoutElement=elem;
    }

    /** get the layout element corresponding to this node (used by e.g. XHTML) */
    public LayoutElement getLayoutElement()
    {
        return this.layoutElement;
    }
}