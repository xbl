/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.Node;

/**
 * this interface is meant for elements in a layout tree, such as XHTML DOcument elements,
 * which need to be notified of XML DOM changes
 */ 
public interface LayoutElement 
{
    /** The XML DOM has changed, please update layout document as well */
    public void insertBefore(Node newChild, Node refChild);
    
    /** The XML DOM has changed, please update layout document as well */
    public void removeChild(Node oldChild);

    /** The XML DOM has changed, please update layout document as well */
    public void styleChanged();
    
    /** Set the style according to this node */
    public void setStyle(Node node);

}
