/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Feb 20, 2006
 *
 */
package fi.hut.tml.xsmiles.dom;


public interface InitializableElement
{
    /**
     * Initialize this element.
     */
    public void init();
}
