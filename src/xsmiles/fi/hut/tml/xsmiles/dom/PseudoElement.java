/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/*
 * XSmiles animation service interface.
 * Used for inter-element communication.
 */

public interface PseudoElement extends Element{
    public void setParentNode(Node parent);
    public String getPseudoElementName(); // return e.g. "value" for ::value
}
