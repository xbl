/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * 
 * Created on May 12, 2005
 */
package fi.hut.tml.xsmiles.dom;

/**
 * @author mpohja
 */
public interface PopupHandler
{
    public void showAsPopup(StylableElement elem,String containerType);
    public void closePopup(StylableElement elem);
}
