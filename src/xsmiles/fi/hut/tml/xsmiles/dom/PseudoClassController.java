/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Aug 4, 2004
 *
 */
package fi.hut.tml.xsmiles.dom;


/**
 * @author honkkis
 *
 */
public interface PseudoClassController
{
    /** sets pseudo class on/off */
    public void setPseudoClass(String pseudoclass, boolean value);
    /** query only those set using setPseudoClassMethod */
    public boolean getPseudoClass(String pseudoclass);
    /** the CSS engine queries this method */
    public boolean isPseudoClass(String pseudoclass);

}
