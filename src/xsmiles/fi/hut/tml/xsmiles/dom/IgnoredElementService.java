/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

/*
 * XSmiles ignored element service interface.
 * Used for inter-element communication
 */

public interface IgnoredElementService {
    
}
