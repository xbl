// UIEventImpl.java
//
/*****************************************************************************
 * Copyright (C) CSIRO Mathematical and Information Sciences.                *
 * All rights reserved.                                                      *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the CSIRO Software License  *
 * version 1.0, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/
//
// $Id: UIEventImpl.java,v 1.3 2003/03/31 16:56:56 honkkis Exp $
//

package fi.hut.tml.xsmiles.dom;
//package org.csiro.svg.dom.events;

import org.w3c.dom.events.UIEvent;
import org.w3c.dom.views.AbstractView;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;

/**
 * Implementation of the DOM UI event
 */

public class UIEventImpl extends EventImpl implements UIEvent {

  AbstractView  abstractView;
  int detail;

  public UIEventImpl() {
  }

  //
  //  typeArg is one of DOMFocusIn, DOMFocusOut, DOMActivate
  //
  public void initUIEvent(String typeArg, boolean canBubbleArg,
                          boolean cancelableArg, AbstractView viewArg,
                          int detailArg) {
    //TODO: Implement this org.w3c.dom.events.UIEvent method
    super.initEvent(typeArg,canBubbleArg,cancelableArg);
    abstractView=viewArg;
    detail=detailArg;
  }

  public AbstractView getView() {
    //TODO: Implement this org.w3c.dom.events.UIEvent method
    return abstractView;
  }

  public int getDetail() {
    //TODO: Implement this org.w3c.dom.events.UIEvent method
    return detail;
  }
  
  public Object clone() throws java.lang.CloneNotSupportedException
  {
  	UIEventImpl n = new UIEventImpl();
	n.initUIEvent(this.getType(),this.bubbles, this.cancelable, null,-1);
  	return n;
  }

} // UIEventImpl
