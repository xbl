/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;

import java.awt.Dimension;
import java.awt.Component;

/*
 * XSmiles visual compoundservice interface.
 * Used for inter-element communication
 *
 * NOTE: THIS IS A BAD KLUDGE TO SUPPORT XFORMS LABEL AND ::VALUE
 * IN HOST LANGUAGES THAT DO NOT SUPPORT CSS LAYOUT, SUCH AS SMIL AND SVG
 */
public interface CompoundService {

	/**
	 * Return the visual component for this extension element
         * This would return e.g. an XForms input element which
         * contains an container that has both the label and the
         * ::value (the widget). Note: this is a kludge.
	 */
	public VisualComponentService getVisualComponent();
}
