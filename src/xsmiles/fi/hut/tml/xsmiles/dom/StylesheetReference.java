/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;



/**
 * Simple container for stylesheet references
 */
public class StylesheetReference {

    public StylesheetReference(String aHref, String aType, boolean aAlternate)
    {
        this.alternate = aAlternate;
        this.href=aHref;
        this.type=aType;
    }
	/** the href */
    public String href;
    
    /** the mime type */
    public String type;
    
    /** alternate stylesheet */
    public boolean alternate;
	

}

