/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.dom;

/**
 * @author honkkis
 *
 */
public class QName
{
    public String nsURI;
    public String localName;
    public QName(String l,String n)
    {
        this.nsURI=n;
        this.localName=l;
    }
}
