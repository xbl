/*
 * XSmilesLayoutReference.java
 *
 * Created on March 7, 2003, 3:02 PM
 */

package fi.hut.tml.xsmiles.dom;

/**
 *
 * @author  mpohja
 */
public interface XSmilesLayoutReference
{
    /** set the layout element corresponding to this node (used by e.g. XHTML) */
    //public void setLayoutElement(LayoutElement elem);

    /** get the layout element corresponding to this node (used by e.g. XHTML) */
    //public LayoutElement getLayoutElement();    
}
