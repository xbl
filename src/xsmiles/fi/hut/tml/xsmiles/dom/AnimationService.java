/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;

import org.w3c.dom.events.Event;

/*
 * XSmiles animation service interface.
 * Used for inter-element communication.
 */

public interface AnimationService {
	/**
	 * Convert String attribute to an float value
	 */
	public float convertStringToUnitless(String attr, String value);
	public String convertUnitlessToString(String attr, float value);

	/**
	 * Returns the animated value of attribute attr.
	 */
	public String getAnimAttribute(String attr);

	/**
	 * The attribute value set with this method should take precedence over
	 * the DOM attribute value.
	 * @param attr	Attribute to be animated
	 * @param value	Animation value to be set
	 */
	public void setAnimAttribute(String attr, String value);

	/**
	 * The anim attribute value removed with this method allows
	 * the DOM attribute value be visible.
	 * @param attr	Attribute to be animated (animation removed)
	 */
	public void removeAnimAttribute(String attr);

	/**
	 * Refresh element with all the animation values. This is called after
	 * several calls to setAnimAttribute() and removeAttribute().
	 */
	public void refreshAnimation();
}
