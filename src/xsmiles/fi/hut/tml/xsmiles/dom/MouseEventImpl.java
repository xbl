// MouseEventImpl.java
//
/*****************************************************************************
 * Copyright (C) CSIRO Mathematical and Information Sciences.                *
 * All rights reserved.                                                      *
 * ------------------------------------------------------------------------- *
 * This software is published under the terms of the CSIRO Software License  *
 * version 1.0, a copy of which has been included with this distribution in  *
 * the LICENSE file.                                                         *
 *****************************************************************************/
//
// $Id: MouseEventImpl.java,v 1.2 2001/11/21 14:10:32 honkkis Exp $
//

package fi.hut.tml.xsmiles.dom;

import org.w3c.dom.events.UIEvent;
import org.w3c.dom.events.MouseEvent;
import org.w3c.dom.views.AbstractView;
import org.w3c.dom.Node;
import org.w3c.dom.events.EventTarget;

public class MouseEventImpl extends UIEventImpl implements MouseEvent {

  int screenX;
  int screenY;
  int clientX;
  int clientY;
  boolean ctrlKey;
  boolean shiftKey;
  boolean altKey;
  boolean metaKey;
  short button;
  EventTarget relatedTarget;

  public MouseEventImpl() {
  }


/*  [javac] C:\source\csiro\sources\org\csiro\svg\dom\events\MouseEventImpl.java
:23: org.csiro.svg.dom.events.MouseEventImpl should be declared abstract; it doe
s not define initMouseEvent(java.lang.String,boolean,boolean,org.w3c.dom.views.A
bstractView,int,int,int,int,int,boolean,boolean,boolean,boolean,short,org.w3c.do
m.events.EventTarget) in org.csiro.svg.dom.events.MouseEventImpl
[javac] public class MouseEventImpl extends UIEventImpl implements MouseEven
t {
*/
/*
   public void initMouseEventx(String typeArg, boolean canBubbleArg,
                              boolean cancelableArg, AbstractView viewArg,
                              int detailArg, int screenXArg, int screenYArg,
                              int clientXArg, int clientYArg, boolean ctrlKeyArg,
                              boolean altKeyArg, boolean shiftKeyArg,
                              boolean metaKeyArg, short buttonArg, Node relatedNodeArg) {

    super.initUIEvent(typeArg, canBubbleArg, cancelableArg, viewArg, detailArg);
    screenX = screenXArg;
    screenY = screenYArg;
    clientX = clientXArg;
    clientY = clientYArg;
    ctrlKey = ctrlKeyArg;
    altKey = altKeyArg;
    shiftKey = shiftKeyArg;
    metaKey = metaKeyArg;
    button = buttonArg;
    relatedNode = relatedNodeArg;
  }*/
  
  public void initMouseEvent(String typeArg, 
                             boolean canBubbleArg, 
                             boolean cancelableArg, 
                             AbstractView viewArg, 
                             int detailArg, 
                             int screenXArg, 
                             int screenYArg, 
                             int clientXArg, 
                             int clientYArg, 
                             boolean ctrlKeyArg, 
                             boolean altKeyArg, 
                             boolean shiftKeyArg, 
                             boolean metaKeyArg, 
                             short buttonArg, 
                             EventTarget relatedTargetArg) {
	super.initUIEvent(typeArg, canBubbleArg, cancelableArg, viewArg, detailArg);
	screenX = screenXArg;
	screenY = screenYArg;
	clientX = clientXArg;
	clientY = clientYArg;
	ctrlKey = ctrlKeyArg;
	altKey = altKeyArg;
	shiftKey = shiftKeyArg;
	metaKey = metaKeyArg;
	button = buttonArg;
	relatedTarget = relatedTargetArg;
	}
  
	public void initMouseEvent(String typeArg, 
	                           boolean canBubbleArg, 
	                           boolean cancelableArg, 
	                           AbstractView viewArg, 
	                           int detailArg, 
	                           int screenXArg, 
	                           int screenYArg, 
	                           float clientXArg, 
	                           float clientYArg, 
	                           boolean ctrlKeyArg, 
	                           boolean altKeyArg, 
	                           boolean shiftKeyArg, 
	                           boolean metaKeyArg, 
	                           short buttonArg, 
	                           EventTarget relatedTargetArg) 
	                           {
		initMouseEvent(	 typeArg, 
	                            canBubbleArg, 
	                            cancelableArg, 
	                            viewArg, 
	                            detailArg, 
	                            screenXArg, 
	                            screenYArg, 
	                           (int) clientXArg, 
	                           (int) clientYArg, 
	                            ctrlKeyArg, 
	                            altKeyArg, 
	                            shiftKeyArg, 
	                            metaKeyArg, 
	                            buttonArg, 
	                            relatedTargetArg);				   
	}

  public int getScreenX() {
    return screenX;
  }

  public int getScreenY() {
    return screenY;
  }

  public int getClientX() {
    return clientX;
  }

  public int getClientY() {
    return clientY;
  }

  public boolean getCtrlKey(){
    return ctrlKey;
  }

  public boolean getShiftKey() {
    return shiftKey;
  }

  public boolean getAltKey() {
    return altKey;
  }

  public boolean getMetaKey() {
    return metaKey;
  }

  public short getButton() {
    return button;
  }

  
  public EventTarget getRelatedTarget()
  {
  	return this.relatedTarget;
  }


} // MouseEventImpl
