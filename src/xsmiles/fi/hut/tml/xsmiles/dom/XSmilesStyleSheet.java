/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
 
package fi.hut.tml.xsmiles.dom;

import java.net.URL;
import java.lang.String;
import fi.hut.tml.xsmiles.dom.StylableElement;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;



public interface XSmilesStyleSheet {

	/** Add a default (user agent) style sheet */
	public void addXMLDefaultStyleSheet(URL defaultUrl);
	
	/** Add a author style sheet */
	public void addXMLStyleSheet(URL stylesheetUrl);
	/** Add a author style sheet */
	public void addXMLStyleSheet(String stylesheetText, URL baseUrl);
	
	/** get parsed and combined style for an element */
	public CSSStyleDeclaration getParsedStyle(StylableElement elem);

        /** prepare a parsed stylesheet (add mediaqueryevaluator + do sorting) */
        public void prepareStyleSheet(CSSStyleSheet ss);
        
        public Selectors getCSSSelectors();
        
        // TODO: add method removeRuleTreeNode(Element e)

	//public XSmilesStyle getParsedStyle(StylableElement elem, String pseudoClass, String id);
	//public CSSStyleDeclaration getStyleForElement(StylableElement elem);
	
}