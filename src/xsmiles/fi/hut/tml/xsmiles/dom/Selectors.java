/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import org.w3c.css.sac.Selector;
import org.w3c.dom.Element;



public interface Selectors
{
    
    /** Does this selector match  */
    public boolean selectorMatchesElem(Selector selector, Element element);
    
    /** in HTML converts border=0 to border-width:0px, etc. */
    public String getStyleAttrValue(StylableElement element);
    
}