/*
 * X-Smiles Copyright (C) Helsinki University of Technology. All rights
 * reserved. For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
// $Id: VisualElementImpl.java,v 1.62 2008/02/20 14:37:55 mpohja Exp $
//
//package org.csiro.svg.dom.events;
package fi.hut.tml.xsmiles.dom;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;

import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.csslayout.view.View;
import fi.hut.tml.xsmiles.mlfc.css.CSSStyleChangeListener;
/**
 * XSmiles DOM Elements
 */
public class VisualElementImpl extends XSmilesElementImpl
        implements
            XSmilesLayoutReference,
            CSSStyleChangeListener,
            VisualElement,
            EventListener
{
    /*
    public static short NOT_INITIALIZED = 10;
    public static short INITIALIZING = 15;
    public static short INITIALIZED = 20;
    protected short element_state = NOT_INITIALIZED;
    */
    boolean hover = false, focusLost = false, hasListeners = false;
    protected Vector views;

    public VisualElementImpl(org.apache.xerces.dom.DocumentImpl ownerDocument, String value)
    {
        super(ownerDocument, value);
    }

    public VisualElementImpl(org.apache.xerces.dom.DocumentImpl ownerDocument, String namespaceURI,
            String qualifiedName)
    {
        super(ownerDocument, namespaceURI, qualifiedName);
    }

    private void setListeners()
    {
        addEventListener("mouseover", this, false);
        addEventListener("mouseout", this, false);
    }

    /*
    private void checkFocusStatus()
    {
    }
    */

    public boolean isFocusPoint()
    {
        if (this.getAttributes().getNamedItem("href") != null) return true;
        return false;
    }

    // public void
/*
    public short getCurrentState()
    {
        return this.element_state;
    }
*/
    public Vector getViews()
    {
        return views;
    }

    public void addView(View view)
    {
        if (views == null)
            views = new Vector();
        views.add(view);
    }

    /*
     * public void removeView(View view) { views.remove(view); }
     */

    public void removeAllViews()
    {
        views.removeAllElements();
    }

    /*
    public short setCurrentState(short st)
    {
        return this.element_state = st;
    }*/

    public void init()
    {
        super.init();
        this.elementStatus = INITIALIZED;
    }

    /**
     * this method tries to propagate DOM changes to the layout element, if such
     * is found
     */
    public Node insertBefore(Node newChild, Node refChild)
    {
        Node n = super.insertBefore(newChild, refChild);
        if (views != null)
        {
            /*
             * TODO: this approach had the problem of initing elements when
             * their parents were not inside the tree yet, and causing
             * exceptions The solution would be to call init only when parent
             * has been inited
             */
            // if (n instanceof XSmilesElementImpl)
            // ((XSmilesElementImpl)n).init();
            Node parent = this.getParentNode();
            if (parent instanceof VisualElementImpl)
            {
                if (this.getElementStatus() == INITIALIZED)
                {
                    if (n instanceof InitializableElement)
                    {
                        Log.debug("InsertBefore: new: " + n + " : parent:" + this);
                        ((InitializableElement) n).init();
                    }
                }
            }
            long start = System.currentTimeMillis();
            if (newChild.getNodeType() == Node.TEXT_NODE
                    || !(newChild instanceof IgnoredElementService))
                updateViews();
            long time = System.currentTimeMillis() - start;
            Log.debug("Element addition took " + Long.toString(time) + " ms");
        }
        return n;
    }

    /**
     * this method tries to propagate DOM changes to the layout element, if such
     * is found
     */
    public Node replaceChild(Node newChild, Node oldChild)
    {
        Node n = super.replaceChild(newChild, oldChild);
        if (newChild instanceof XSmilesElementImpl)
            ((XSmilesElementImpl) newChild).init();
        updateViews();
        return n;
    }

    /**
     * Do all stuff when a node is removed. One thing is to remove all
     * components from view
     * 
     * @param removed
     */
    /*
     * now the view calls viewRemoved for its children public void
     * nodeRemoved(Node removed, View anyView) { anyView.nodeRemoved(removed); }
     */

    public Node removeChild(Node oldChild)
    {
        Vector oldViews = null;
        if (oldChild instanceof VisualElementImpl)
        {
            oldViews = ((VisualElementImpl) oldChild).getViews();
        }
        // Log.debug("Views to be removed: " + oldViews);
        // This used to be done after repaint. I don't remember why I changed
        // that...
        // Change back if it causes problems.
        Node removed = super.removeChild(oldChild);
        if (oldViews != null)
        {
            View view = null;
            Enumeration e = oldViews.elements();
            while (e.hasMoreElements())
            {
                view = (View) e.nextElement();
                view.getParentView().removeChild(view);
            }
            if (view != null)
                view.repaintDocument();
            // this.nodeRemoved(oldChild, view);
        }
        return removed;
    }

    public void handleEvent(Event evt)
    {
        // Log.debug(this + " got event " + evt);

        String type = evt.getType();
        if (type.equals("mouseover"))
        {
            focusLost = false;
            setStatus(true);
        }
        else if (type.equals("mouseout"))
        {
            focusLost = true;
            /*
             * if (evt.getEventPhase() != Event.AT_TARGET) { MouseOuter mo = new
             * MouseOuter(50); mo.start(); } else
             */loseFocus();
        }
        // Log.debug("Hover: "+hover+" "+this);
    }

    public void setStatus(boolean status)
    {
        if (status != hover)
        {
            hover = status;
            styleChanged();
        }
        // Log.debug("set status to "+status);
    }

    private void loseFocus()
    {
        if (focusLost)
            setStatus(false);
    }

    /** ask whether this element belongs to a certain CSS pseudoclass */
    public boolean isPseudoClass(String pseudoclass)
    {
        if (pseudoclass.equals("hover"))
        {
            // Log.debug("Hover: "+hover+" "+this);
            if (!hasListeners)
            {
                hasListeners = true;
                // Set event listeners only for elements belonging to a hover
                // pseudoclass
                setListeners();
            }
            return hover;
        }
        else
            return super.isPseudoClass(pseudoclass);
    }

    /**
     * since the pseudoelement removals are not catched by the
     * xsmilesvisualelement, this method can be used to notify a remove
     * 
     * @param elem
     */
    public void notifyPseudoRemoved(Element elem)
    {
        if (views != null)
        {
            View view = null;
            Enumeration e = views.elements();
            if (e.hasMoreElements())
            {
                view = (View) e.nextElement();
                // this.nodeRemoved(elem, view);
            }
        }
    }

    /**
     * Recreates all the child views for this node's children.
     */
    protected void updateViews()
    {
        // Vector views = (Vector) ((org.apache.xerces.dom.NodeImpl)
        // this).getUserData("views");
        if (views != null && !views.isEmpty())
        {
            View view = null, childView = null;
            Enumeration e = views.elements();
            while (e.hasMoreElements())
            {
                view = (View) e.nextElement();
                view.removeChildren();
                /*
                 * Enumeration children = view.getChildren().elements(); while
                 * (children.hasMoreElements()) { childView =
                 * (View)children.nextElement(); childView.removeFromTree(); }
                 */}
            if (view != null)
            {
                //boolean displayNone = this.getStyle().getPropertyValue("display").equals("none");
                boolean displayNone = !this.isCurrentlyVisible(); // will also check parents, MH
                // TODO: not like this...
                if (!displayNone)
                    view.createChildViews(this);
                views.removeAllElements();
                if (!displayNone)
                {
                    views.addElement(view);
                    // ((org.apache.xerces.dom.NodeImpl)
                    // this).setUserData("views", views, null);
                    view.setStyle(style);
                    view.repaintDocument(view);
                }
                // view.repaintDocument();
            }
            else
                Log.error("insertBefore failed to create child views for " + this);
        }
        else
        {
            // This should be optimized if possible.
            try {
            	if(this.getParentNode() != null)
            		((VisualElementImpl)this.getParentNode()).updateViews();
            } catch (java.lang.ClassCastException e) {
                Log.error("Class cast exeption in updateViews().");
            }
        }
    }

    public void setAttribute(String name, String value)
    {
        super.setAttribute(name, value);
        // MP: commented out. Is this necessary?
        if (this.elementStatus != XSmilesElementImpl.UNINITIALIZED)
            styleChanged();
    }

    public void removeAttribute(String name)
    {
        super.removeAttribute(name);
        if (this.elementStatus != XSmilesElementImpl.UNINITIALIZED)
            styleChanged();
    }

    public boolean belongsToTree()
    {
        Node parent = this.getParentNode();
        while (parent != null)
        {
            if (parent.getNodeType() == Node.DOCUMENT_NODE)
                return true;
            parent = parent.getParentNode();
        }
        return false;
    }

    public void styleChanged()
    {
        if (!this.belongsToTree())
            return;
        boolean wasAbsolute = false;
        String display, position;
        if (this.getStyle() == null)
            display = "";
        else
            display = this.style.getPropertyValue("display");
        if (this.getStyle() != null)
            position = this.getStyle().getPropertyValue("position");
        else
            position = "";
        if (position.equals("relative") || position.equals("absolute"))
        {
            wasAbsolute = true;
            // Vector views = (Vector) ((org.apache.xerces.dom.NodeImpl)
            // this).getUserData("views");
            if (views != null)
            {
                View view = null;
                Enumeration e = views.elements();
                while (e.hasMoreElements())
                {
                    view = (View) e.nextElement();
                    view.removeFromTree();
                }
            }
        }
        updateStyle();
        if (this.getStyle() != null)
            position = this.getStyle().getPropertyValue("position");
        else
            position = "";
        if (wasAbsolute && (position.equals("absolute") || position.equals("relative")))
        {
            Vector parentViews = ((VisualElementImpl) this.getParentNode()).getViews();
            View newView = null;
            if (parentViews != null)
            {
                View view = null;
                Enumeration e = parentViews.elements();
                if (e.hasMoreElements())
                {
                    view = (View) e.nextElement();
                    newView = view.createChildView(this, getParentNode());
                }
                if (newView!=null) newView.repaintDocument(newView);
                // view.repaintView();
            }
        }
        else if (!display.equals("") && !display.equals("inline")
                && display.equals(this.style.getPropertyValue("display"))
                && !position.equals("absolute") && !position.equals("relative"))
            updateViews();
        // ((VisualElementImpl) this.getParentNode()).updateViews();
        else if (this.getParentNode() instanceof VisualElementImpl)
        {
            // Update has to be done through parent in order to enable
            // all possible style changes (e.g., display value).
            // For some reason, the parent cannot be inline element??
            VisualElementImpl parentNode = (VisualElementImpl) this.getParentNode();
            while (parentNode.style.getPropertyValue("display").equals("inline")
                    || parentNode.style.getPropertyValue("display").equals(""))
                parentNode = (VisualElementImpl) parentNode.getParentNode();
            parentNode.updateViews();
        }
    }

    /**
     * Handles consecutive mouseout-mouseover events.
     * 
     * @author mpohja
     */
    private class MouseOuter extends Thread
    {
        long wait;

        MouseOuter(long msec)
        {
            wait = msec;
        }

        public void run()
        {
            try
            {
                Thread.sleep(wait);
                loseFocus();
            }
            catch (InterruptedException e)
            {
                Log.error(e);
            }
        }
    }


    
    /** checks whether belongs to CSS tree and is not display:none or visibility:hidden for any ancestor node */
    public boolean isCurrentlyVisible()
    {
        if (!isVisibleByCSSProperties()) return false;
        Node parent = this.getParentNode();
        while (parent != null)
        {
            if (parent.getNodeType() == Node.DOCUMENT_NODE)
                return true;
            if (parent instanceof VisualElement)
            {
                if (!((VisualElement)parent).isVisibleByCSSProperties()) return false;
            }
            //else return false; maybe html is not visualelement...
            parent = parent.getParentNode();
        }
        return false;
    }
    
    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.dom.VisualElement#isCurrentlyVisible()
     * Does not check parents! Use isCurrentlyVisible for parents
     */
    public boolean isVisibleByCSSProperties()
    {
        if (this.getStyle()==null) return false; // ??
        CSSValue display = this.getStyle().getPropertyCSSValue("display");
        CSSValue visibility = this.getStyle().getPropertyCSSValue("visibility");
        if (display!=null&&display.toString().equalsIgnoreCase("none")) return false;
        if (visibility!=null && visibility.toString().equalsIgnoreCase("hidden")) return false;
        return true;
    }
    
}