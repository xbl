/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */

package fi.hut.tml.xsmiles.dom;

import fi.hut.tml.xsmiles.Log;
import org.w3c.dom.DOMException;
import org.w3c.dom.*;

import org.apache.xerces.dom.*;


/**
 * Base class for namespaced attributes in X-Smiles
 * @author Mikko Honkala
 */
public class XSmilesAttrNSImpl extends AttrNSImpl implements InitializableElement{
    
        /** has this attribute been inited */
        protected boolean inited=false;
            
	protected XSmilesAttrNSImpl(DocumentImpl ownerDocument,java.lang.String namespaceURI, 
		java.lang.String qualifiedName)  
	{
		super(ownerDocument,namespaceURI,qualifiedName);
	}
	
	public void init()
	{
            inited=true;
	}

}

