package fi.hut.tml.xsmiles.content;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

import fi.hut.tml.xsmiles.BrowserLogic;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.gui.components.XFileDialog;
import fi.hut.tml.xsmiles.mlfc.general.Helper;

/**
 * Handles application/xml
 *
 * This class should be reusable for optimization purposes
 *
 * @author Mikko Honkala
 */
public class UnknownContentHandler extends BaseContentHandler implements XSmilesContentHandler
{
    
    public UnknownContentHandler()
    {
    }
    
    /**
     * fetches an resource
     * @param url the documents url, used for relative references
     * @param stream the input stream of the content
     * @param manager the MLFCManager that could be used for this content
     */
    protected void fetch(InputStream stream, XLink url)
    {
        
        if (getPrimary() == false)
            return;
        Log.debug(this.toString()+": retrieving document");
        saveContentFromURL(stream,url.getURL(),this.fBrowser.getComponentFactory());
        this.getContainer().removeAll();
        this.getContainer().repaint();
        this.fBrowser.getBrowserLogic().setState(BrowserLogic.READY);
    }
    
    public static void saveContentFromURL(InputStream stream, URL url, ComponentFactory factory)
    {
        String filename = null;
        if (url!=null&&url!=null)
        {
            filename = url.getFile();
        }
        XFileDialog dialog = factory.getXFileDialog(true,filename);
        int ret = dialog.select();
        if (ret == XFileDialog.SELECTION_OK )
        {
            File f = dialog.getSelectedFile();
            Log.debug("The user selected : "+f.toString());
            if (!f.exists())
            {
                try
                {
                    FileOutputStream fos = new FileOutputStream(f);
                    Helper.copyStream(stream,fos,1000);
                    fos.flush();
                    fos.close();
                } catch (Exception e)
                {
                    Log.error(e,"Could not write "+url.toString()+" to file:"+f.toString());
                }
            } else
            {
                Log.error("The file exists, will not overwrite: "+f.toString());
            }
        }
    }
    
    
    public void play()  throws Exception
    {
        this.fetch(this.fInputStream, this.fLink);
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.XSmilesContentHandler#getTitle()
     */
    public String getTitle()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
}
