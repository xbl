/*
 * ContentLoaderListener.java
 *
 * Created on August 12, 2003, 3:31 PM
 */

package fi.hut.tml.xsmiles.content;

/**
 * @author  mpohja
 */
public interface ContentLoaderListener
{
    /**
     * Tells if the content handler should handle content.
     */
    boolean doHandle(String mime);
}
