package fi.hut.tml.xsmiles.content;

import java.net.URL;
import java.util.Hashtable;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XLink;

/**
 * ContentHandlerFactory creates content handlers based on the content-type and possibly the URI This instance is created by the UI, and usually extended with
 * UI specific handler mappings
 * 
 * @author Mikko Honkala
 */
public class ContentHandlerFactory {

    public final static String XMLHANDLER = "fi.hut.tml.xsmiles.content.xml.XMLContentHandler";
    public final static String HTMLHANDLER = "fi.hut.tml.xsmiles.content.html.HTMLContentHandler";

    protected Hashtable typeHandlers;
    protected Hashtable endingHandlers;
    protected BrowserWindow browserWindow; // to access configurer and tick boxes

    public ContentHandlerFactory(BrowserWindow bw) {
        typeHandlers = new Hashtable();
        endingHandlers = new Hashtable();
        browserWindow = bw;
        this.createMappings();
    }

    public void createMIMEMapping(String mime, Class aClass) {
        typeHandlers.put(mime, aClass);
    }

    public void createEndingMapping(String ending, Class aClass) {
        endingHandlers.put(ending, aClass);
    }

    protected void createMappings() {
        // video
        try {
            this.createTextMappings();
        } catch (Throwable e) {
            Log.error("No TextMedia found. (" + e.toString() + ") .Running without it.");
        }

        try {
            final Class XMLHANDLER_CLASS = Class.forName(XMLHANDLER);
            final Class HTMLHANDLER_CLASS = Class.forName(HTMLHANDLER);
            createMIMEMapping("application/xml", XMLHANDLER_CLASS);
            createMIMEMapping("application/xhtml+xml", XMLHANDLER_CLASS);
            createMIMEMapping("application/smil", XMLHANDLER_CLASS);
            createMIMEMapping("text/xml", XMLHANDLER_CLASS);
            createMIMEMapping("text/html", HTMLHANDLER_CLASS);
            createMIMEMapping("image/svg+xml", XMLHANDLER_CLASS); // the svg mime type

            createEndingMapping("xhtml", XMLHANDLER_CLASS);
            createEndingMapping("xfm", XMLHANDLER_CLASS);
            createEndingMapping("xml", XMLHANDLER_CLASS);
            createEndingMapping("xsl", XMLHANDLER_CLASS);
            createEndingMapping("svg", XMLHANDLER_CLASS);
            createEndingMapping("fo", XMLHANDLER_CLASS);
            createEndingMapping("smil", XMLHANDLER_CLASS);
            createEndingMapping("smi", XMLHANDLER_CLASS);
            createEndingMapping("x3d", XMLHANDLER_CLASS);
            createEndingMapping("xsd", XMLHANDLER_CLASS);
            createEndingMapping("html", HTMLHANDLER_CLASS);
            createEndingMapping("htm", HTMLHANDLER_CLASS);
        } catch (ClassNotFoundException e) {
            Log.error(e, "ContentHandlerFactory could not find content handler class");
        }
    }

    protected void createTextMappings() {
    }

    /**
     * This returns just the bare content type. e.g. for "text/html; charset = xxx", it would return "text/html"
     */
    protected String getBareContentType(String contentType) {
        int pos = contentType.indexOf(';');
        if (pos < 0)
            return contentType;
        return contentType.substring(0, pos);
    }

    /**
     * This returns just the bare content type, replacing the subpart with '*'. e.g. for "text/html; charset = xxx", it would return "text/*"
     */
    protected String getWildBareContentType(String contentType) {
        String bare = getBareContentType(contentType);
        int pos = bare.indexOf('/');
        if (pos < 0)
            return bare;
        return bare.substring(0, pos) + "/*";
    }

    public XSmilesContentHandler createContentHandler(String cType, XLink url) {
        String contentType;
        String MIMEOverride = url.getMIMEOverride();
        if (MIMEOverride != null) {
            contentType = MIMEOverride;
        } else {
            contentType = cType;
        }

        // TODO: create dynamic way to add content handles
        // probably requires the use of reflection to create the handler
        Class handlerclass = null;
        // First try to resolve the contentType string
        try {
            Log.debug("Trying to create ContentHandler for content type: " + contentType);
            if (contentType != null && contentType != "content/unknown") {
                String bareContent = this.getBareContentType(contentType);
                handlerclass = (Class) typeHandlers.get(bareContent);
                if (handlerclass != null) {
                    return this.createContentHandler(handlerclass, bareContent, url);
                }
            }
            // The content type was not associated with any handler, check for file extensions
            String ext = getSuffix(url.getURL());
            Log.debug("null contentType, checking extension: " + ext);
            if (ext != null) {
                handlerclass = (Class) endingHandlers.get(ext);
                if (handlerclass != null) {
                    return this.createContentHandler(handlerclass, contentType, url);
                }
            }

            // Try audio/* or video/* wild cards
            String bareContent = this.getWildBareContentType(contentType);
            handlerclass = (Class) typeHandlers.get(bareContent);
            if (handlerclass != null) {
                return this.createContentHandler(handlerclass, bareContent, url);
            }

        } catch (Exception e) {
            Log.error(e, "The classname: " + handlerclass + " for content-type: " + contentType + " is of the wrong type");
        }
        Log.error("Could not instantiate a ContentHandler for the url: " + url.getURL().toString());
        return new UnknownContentHandler();
    }

    /**
     * Checks if given URL end with the given suffix String.
     */
    protected static String getSuffix(URL url) {
        String str = url.getFile().trim().toLowerCase();
        int pos = str.lastIndexOf('.');
        if (pos > -1) {
            String suffix = str.substring(pos + 1);
            return suffix;
        }
        return null;
    }

    protected XSmilesContentHandler createContentHandler(Object instance) throws IllegalAccessException {
        if (instance instanceof XSmilesContentHandler) {
            return (XSmilesContentHandler) instance;
        } else {
            throw new IllegalAccessException("Illegal content handler");
        }
    }

    protected XSmilesContentHandler createContentHandler(Class a_class, String contentType, XLink url) throws InstantiationException, IllegalAccessException {
        Object c = a_class.newInstance();
        return createContentHandler(c);
    }
}
