/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.content;

/**
 * The base class for XSmilesExceptions
 *
 * @author	Mikko Honkala
 * @version	$Revision: 1.1 $
 */
public class XSmilesContentInitializeException extends java.lang.RuntimeException {
    
    /** constructs an exception without a message */
    public XSmilesContentInitializeException()
    {
        super();
    }
    
        /** constructs an exception without a message */
    public XSmilesContentInitializeException(String message)
    {
        super(message);
    }
}

