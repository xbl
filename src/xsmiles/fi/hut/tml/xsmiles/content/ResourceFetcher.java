/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 1, 2004
 *
 */
package fi.hut.tml.xsmiles.content;

import java.net.URL;
import java.net.URLConnection;

import fi.hut.tml.xsmiles.util.XSmilesConnection;


/**
 * @author honkkis
 *
 */
public interface ResourceFetcher
{
	/**
	 * Retrieve a resource via an URL using authenticated HTTP get and store it
	 * as a resource under this contenthandler.
	 * The XML signature process uses that resource list as the list of resources
	 * that are referenced by the document.
	 * Use type from the class content.Resource.
	 * use RESOURCE_DO_NOT_STORE, for not storing it in the list of loaded resources
	 */
    public XSmilesConnection get(URL dest, short type) throws Exception;
}
