/* X-Smiles
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * look in licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles.content;

import java.util.Enumeration;
import java.util.Vector;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.MLFCLoader;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * Activates and deactivates MLFCs upon requests.
 *
 * See Technical Requirements Specification for further details on
 * MLFC management.
 * Note: there are 3 phases in creating/stopping a primary MLFC: init-start-destroy : the
 * first two are for synchronization purposes
 *
 * @author	Jukka Heinonen
 * @author       Juha
 * @author   Mikko Honkala
 */
public class ContentManager 
{
  private BrowserWindow   browser;
  private MLFCLoader      mlfcLoader;
  protected MLFCListener mlfcListener;
  private Vector          activeContent;
  
  protected XSmilesContentHandler primaryHandler;

  
  /**
   * Define mappings between MLFCs and their class-names, and 
   * initialize private variables.
   * @param myBrowser The BrowserWindow.
   */
  public ContentManager(BrowserWindow myBrowser, MLFCListener listener)
  {
    browser = myBrowser;
    mlfcLoader = new MLFCLoader(browser);
    mlfcListener=listener;
    activeContent = new Vector();
    return;
  }

  public void addContentHandler(XSmilesContentHandler handler)
  {
  	this.activeContent.addElement(handler);
  }
  
  public void addPrimaryContentHandler(XSmilesContentHandler handler)
  {
  	this.addContentHandler(handler);
        this.primaryHandler = handler;
  }
  
  public XSmilesContentHandler getPrimaryContentHandler()
  {
      return this.primaryHandler;
  }
  
  /** @return all content handlers, also primary */
  public Vector getContentHandlers()
  {
      return this.activeContent;
  }
	
  /**
   * destroys all currently active MLFCs.
   */
  public void stopCurrentActiveContent()
  {
    try
      {

	// Deactivate all the MLFCs currently stored in the activeMLFCs Vector
	for (Enumeration e = activeContent.elements(); e.hasMoreElements() ;)
	  {
	  	Object obj = e.nextElement();
		if (obj instanceof XSmilesContentHandler)
		{
			XSmilesContentHandler handlerToBeRemoved
				= (XSmilesContentHandler)obj;
            handlerToBeRemoved.setMLFCListener(null);
			handlerToBeRemoved.close();
            
		}
        else {
            Log.error("stop active content: not instance of XSmilesContentHandler");
        }
	  }
	activeContent.removeAllElements();
      } catch (Exception e)
	{
	  Log.error(e);
	}

  }
} 

