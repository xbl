/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 1, 2004
 *
 */
package fi.hut.tml.xsmiles.content;

import java.util.Enumeration;


/**
 * A class that implements this interface is a holder of references to resource URLs.
 * 
 * @author Mikko Honkala
 *
 */
public interface ResourceReferencer
{
    public void addResource(Resource r);
    public Enumeration getResources();
}
