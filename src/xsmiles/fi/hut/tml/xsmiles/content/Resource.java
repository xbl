/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 1, 2004
 *
 */
package fi.hut.tml.xsmiles.content;

import java.net.URL;


/**
 * This class is a wrapper for any kind of resource that a content references.
 * For instance, an XML document can reference images, stylesheets, applets, objects, scripts, etc.
 * 
 * In future, there will also be link to cached content here.
 * 
 * This is also used by the XML signature MLFC to sign all referenced content.
 * @author Mikko Honkala
 *
 */
public class Resource
{
    protected short type;
    protected String contentType;
    protected URL url;
    
    public static short RESOURCE_IMAGE = 5;
    public static short RESOURCE_STYLESHEET = 8;
    public static short RESOURCE_SCRIPT = 19;
    public static short RESOURCE_APPLET = 18;
    public static short RESOURCE_OBJECT = 25;
    public static short RESOURCE_UNKNOWN = 125;

    // use this to not to store this to the list of loaded resources
    public static short RESOURCE_DO_NOT_STORE = 200;

    public Resource(URL u, short resourcetype, String content)
    {
        type=resourcetype;
        contentType=content;
        url=u;
    }
    
    public short getType()
    {
        return type;
    }
    
    public URL getURL()
    {
        return url;
    }
    
    public String getContentType()
    {
        return contentType;
    } 

}
