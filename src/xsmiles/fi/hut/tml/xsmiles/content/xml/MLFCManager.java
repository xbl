/*
 * X-Smiles USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF look in
 * licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles.content.xml;

import java.awt.Container;
import java.util.Enumeration;
import java.util.Hashtable;

import org.w3c.dom.Document;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.mlfc.CoreMLFC;
import fi.hut.tml.xsmiles.mlfc.MLFC;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;

/**
 * Activates and deactivates MLFCs upon requests.
 * 
 * See Technical Requirements Specification for further details on MLFC
 * management. Note: there are 3 phases in creating/stopping a primary MLFC:
 * init-start-destroy : the first two are for synchronization purposes
 * 
 * @author Jukka Heinonen
 * @author Juha
 * @author Mikko Honkala
 */
public class MLFCManager {
    
    public static final String SOURCEMLFC_CLASS = "fi.hut.tml.xsmiles.mlfc.SourceMLFC";

    public static void initMLFC(XMLDocument document, Container c, MLFCListener mlfcListener, BrowserWindow browser, XSmilesContentHandler ch) {
        if (document!=null && (ExtendedDocument)document.getDocument()!=null)
        {
            ExtendedDocument extdoc = ((ExtendedDocument) document.getDocument());
            MLFC mlfc = extdoc.getHostMLFC();
            if (mlfc == null) {
                // TODO:
                mlfc = getSourceMLFC();
                extdoc.setHostMLFC(mlfc);
            }
            if (mlfc != null) {
                // This is the primary MLFC - flag set before init() for elements.
                mlfc.setContentHandler(ch);
                mlfc.setMLFCListener(mlfcListener);
                mlfc.setContainer(c);
                if (mlfc instanceof CoreMLFC)
                    ((CoreMLFC) mlfc).setBrowserWindow(browser);

                Hashtable parasites = extdoc.getParasiteMLFCs();
                // set the XMLDocument variable for all MLFCs
                setXMLDocument(mlfc, parasites, document, mlfcListener);
                // start parasite mlfcs
                initMLFCs(parasites, document, c, mlfcListener, browser, ch);
                mlfc.initMLFC(document, c);
                // this will call init() recursively for all DOM elements
                extdoc.init();
            }

        }
    }

    public static void pauseMLFC(XMLDocument document) {
        ExtendedDocument extdoc = ((ExtendedDocument) document.getDocument());
        MLFC mlfc = extdoc.getHostMLFC();

        if (mlfc != null) {
            mlfc.pause();
            Hashtable parasites = extdoc.getParasiteMLFCs();
            if (parasites == null) {
                return;
            }
            for (Enumeration e = parasites.elements(); e.hasMoreElements();) {
                ((MLFC) e.nextElement()).pause();
            }
        }
    }

    /**
     * Activates a new Primary MLFC.
     * 
     * ?@param document The XMLDocument which is to be shown.
     */
    public static void activateMLFC(XMLDocument document, Container c, MLFCListener mlfcListener, BrowserWindow browser, boolean primary) throws Exception {
        if (document!=null && (ExtendedDocument)document.getDocument()!=null)
        {
	        ExtendedDocument extdoc = ((ExtendedDocument) document.getDocument());
	        MLFC mlfc = extdoc.getHostMLFC();
	
	        if (mlfc != null) {
	            Hashtable parasites = extdoc.getParasiteMLFCs();
	            // set the XMLDocument variable for all MLFCs
	
	            // start parasite mlfcs
	            startMLFCs(parasites, document, c, mlfcListener, browser);
	
	            // Set the browserWindow reference to CoreMLFCs
	
	            // start host mlfc
	            if (mlfc instanceof CoreMLFC)
	                ((CoreMLFC) mlfc).startMLFC(document, c, primary, browser);
	            else
	                mlfc.startMLFC(document, c);
	        }
	        //activeMLFCs.addElement(mlfc);
	        return;
        }
    }

    /** this sets the XMLDocument variable for mlfcs */
    static void setXMLDocument(MLFC host, Hashtable parasites, XMLDocument doc, MLFCListener mlfcListener) {
        host.setXMLDocument(doc);
        if (parasites == null)
            return;
        Enumeration e = parasites.elements();
        while (e.hasMoreElements()) {
            MLFC mlfc = (MLFC) e.nextElement();
            mlfc.setXMLDocument(doc);
            // Set MLFC listener for parasite MLFCs
            mlfc.setMLFCListener(mlfcListener);
        }

    }

    /**
     * Displays a document in source view.
     * 
     * @param document
     *                     the document to be shown
     * @param mode
     *                     The mode of the souce view (source, source&XSL,...)
     */
    /*
     * public void activateSourceMLFC(XMLDocument document, int mode, Container
     * c) { EventBroker eventBroker;
     * 
     * //stopCurrentActiveMLFCs(); sourceMLFC = this.getSourceMLFC();
     * 
     * sourceMLFC.setMLFCListener(mlfcListener);
     * sourceMLFC.startMLFC(document,c,true); // activeSourceMLFC=sourceMLFC;
     * ((SourceMLFC)sourceMLFC).display(document, mode, c); }
     */
    /**
     * stops all MLFCs in the doc.
     */
    public static void stopMLFC(XMLDocument doc) {
        try {
            if (doc != null) {
                ExtendedDocument extdoc = (ExtendedDocument) doc.getDocument();
                // SERGE:
                if (extdoc != null) {
	                Hashtable parasites = extdoc.getParasiteMLFCs();
	                MLFC hostMLFC = extdoc.getHostMLFC();
	                //extdoc.destroy();
	                if (parasites != null) {
	                    for (Enumeration e = parasites.elements(); e.hasMoreElements();) {
	                        ((MLFC) e.nextElement()).stop();
	                    }
	                }
	                hostMLFC.stop();
                }

            }
        } catch (Exception e) {
            Log.error(e);
        }
    }
    
    public static void closeMLFC(XMLDocument doc) {    	
    	stopMLFC(doc);
        if (doc!=null && (ExtendedDocument)doc.getDocument()!=null)
        {
	    	((ExtendedDocument)doc.getDocument()).destroy();
	        doc.deactivate();
        }
    }

    /**
     * Activates a secondary MLFC. Can be used with mixed documents.
     */
    /*
     * public MLFC activateSecondaryMLFC(XMLDocument document, Container
     * container) { MLFC mlfc =
     * ((ExtendedDocument)document.getDocument()).getHostMLFC(); // This is a
     * secondary MLFC - flag set before init() for elements.
     * mlfc.setPrimary(false);
     * 
     * if (mlfc == null) { } else { mlfc.setMLFCListener(mlfcListener);
     * mlfc.setContainer(container); if(mlfc instanceof CoreMLFC)
     * ((CoreMLFC)mlfc).setBrowserWindow(browser);
     * 
     * Hashtable parasites =
     * ((ExtendedDocument)document.getDocument()).getParasiteMLFCs();
     * ExtendedDocument extdoc=(ExtendedDocument)document.getDocument(); // set
     * the XMLDocument variable for all MLFCs
     * setXMLDocument(mlfc,parasites,document);
     * 
     * extdoc.init();
     * 
     * this.startMLFCs(parasites,document,container,mlfcListener,browser);
     * mlfc.startMLFC(document,container,false); activeMLFCs.addElement(mlfc); }
     * 
     * return mlfc; }
     */
    /**
     * @return The current primary MLFC, which is used at this moment null is
     *                returned, if no MLFC is used at the moment (should be impossible)
     */
    /*
     * public MLFC getMLFC() { return primaryMLFC; }
     */
    /*
     * public MLFCController getMLFCController() { if(primaryMLFC==null) return
     * null; else return primaryMLFC.getMLFCController(); }
     */

    /// KLUDGE
    /*
     * public static void startMLFCs(Hashtable MLFCs, XMLDocument doc, Container
     * c) { startMLFCs(MLFCs, doc, c, null, browser); }
     */

    public static void startMLFCs(Hashtable MLFCs, XMLDocument doc, Container c, MLFCListener ml) {
        startMLFCs(MLFCs, doc, c, ml, null);
    }

    /**
     * Initialize the xforms elements in this document / presentation. will be
     * MOVED TO MLFCManager!
     */
    public static void startMLFCs(Hashtable MLFCs, XMLDocument doc, Container c, MLFCListener ml, BrowserWindow browza) {
        if (MLFCs == null)
            return;
        
        MLFC xblmlfc = null;
        // Run XBL MLFC first
        if (MLFCs.containsKey("http://www.w3.org/ns/xbl"))
        {
            xblmlfc = (MLFC)MLFCs.remove("http://www.w3.org/ns/xbl");
            xblmlfc.startMLFC(doc, c);
        }
        
        Enumeration e = MLFCs.elements();
        
        while (e.hasMoreElements()) {
            MLFC mlfc = (MLFC) e.nextElement();

            //mlfc.setMLFCListener(ml);

            // start host mlfc
            if (mlfc instanceof CoreMLFC)
                ((CoreMLFC) mlfc).startMLFC(doc, c, false, browza);
            else
                mlfc.startMLFC(doc, c);
        }
        
        // Put XBL MLFC back into the parasites Hashtable
        if (xblmlfc != null)
            MLFCs.put("http://www.w3.org/ns/xbl", xblmlfc);
        
    }

    public static void initMLFCs(Hashtable MLFCs, XMLDocument doc, Container c, MLFCListener ml, XSmilesContentHandler ch) {
        initMLFCs(MLFCs, doc, c, ml, null, ch);
    }

    /**
     * Initialize the xforms elements in this document / presentation. will be
     * MOVED TO MLFCManager!
     */
    public static void initMLFCs(Hashtable MLFCs, XMLDocument doc, Container c, MLFCListener ml, BrowserWindow browza, XSmilesContentHandler ch) {
        if (MLFCs == null)
            return;
        Enumeration e = MLFCs.elements();
        while (e.hasMoreElements()) {
            MLFC mlfc = (MLFC) e.nextElement();
            mlfc.setContentHandler(ch);
            mlfc.setMLFCListener(ml);

            mlfc.initMLFC(doc, c);
        }
    }

    public static void stopMLFCs(Hashtable MLFCs) {
        Enumeration e = MLFCs.elements();
        while (e.hasMoreElements()) {
            MLFC mlfc = (MLFC) e.nextElement();
            mlfc.stop();
        }
    }

    /**
     * Returns a new Source MLFC object.
     * 
     * @return Source MLFC
     */
    public static MLFC getSourceMLFC() {
        // use reflection, since sourceMLFC is swing dependant
        Log.debug("SourceMLFC starting up...");
        try {
            MLFC sMLFC = (MLFC) Class.forName(SOURCEMLFC_CLASS).newInstance();
            return sMLFC;
        } catch (Throwable e) {
            Log.error(e, "Cannot start sourceMLFC, could be running without swing");
        }
        return null;
    }

    /**
     * notify that the zoom level has changed. Note that some content may choose
     * to not implement zooming.
     */
    public static void setZoom(double zoom, Document doc) {
        if (doc != null && doc instanceof ExtendedDocument) {
            MLFC primary = ((ExtendedDocument) doc).getHostMLFC();
            primary.setZoom(zoom);
            Hashtable parasites = ((ExtendedDocument) doc).getParasiteMLFCs();
            Enumeration parasitesEnum = parasites.elements();
            while (parasitesEnum.hasMoreElements())
            {
                MLFC parasite = (MLFC)parasitesEnum.nextElement();
                parasite.setZoom(zoom);
            }
        }
    }

}

