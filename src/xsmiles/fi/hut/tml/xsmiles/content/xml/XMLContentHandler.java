package fi.hut.tml.xsmiles.content.xml;

import java.awt.Container;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import fi.hut.tml.xsmiles.*;
import fi.hut.tml.xsmiles.content.BaseContentHandler;
import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.dom.ExtendedDocument;
import fi.hut.tml.xsmiles.protocol.http.XHttpURLConnection;
import fi.hut.tml.xsmiles.xslt.JaxpXSLT;
import fi.hut.tml.xsmiles.xslt.XSLTFactory;

/**
 * Handles application/xml
 * 
 * @author Mikko Honkala
 */
public class XMLContentHandler extends BaseContentHandler
		implements
			XSmilesContentHandler {

	/** the XMLDocument of this contenthandler */
	protected XMLDocument doc;
	private boolean destroyed = false;

	public XMLContentHandler() {
		super();
	}

	/**
	 * Prefetches media. The URL must have been set using setUrl(). The data
	 * will be downloaded from the URL. After calling this method, the media
	 * will be in memory and can be played. This is a blocking method.
	 */
	public void prefetch() throws Exception {
		if (!fPrefetched) {
			this.fetch(this.fInputStream, this.fLink);
			super.prefetch();
		}
	}

	/**
	 * Plays the media. The media will be added to the container set using
	 * setContainer(). It will be visible. It will also play any animation it
	 * possibly has. Also, audio media is started using this method.
	 * <p>
	 * If the media is not yet prefetched, it will first be prefetched.
	 */
	public void play() throws Exception {
		//this.getContainer().setVisible(false);
		prefetch();
		this.initDocument();
		this.getContainer().setVisible(true);
		// for future: another thread may call close() between these calls,
		// so check status
		if (!this.fClosed) {
			this.activateDocument();
		}
		super.play();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fi.hut.tml.xsmiles.gui.media.general.Media#pause()
	 */
	public void pause() {
		MLFCManager.pauseMLFC(fDoc);
		super.pause();
	}
	/**
	 * Stops the media. The media will be stopped and it will be invisible.
	 * Audio will be silent. It is possible to call start() after stop()
	 */
	public void stop() {
		if (!this.getPrimary() && this.getContainer() != null) {
			this.getContainer().setVisible(false);
		}
		MLFCManager.stopMLFC(fDoc);
		super.stop();
	}

	/**
	 * This will freeze all memory and references to this media. If the media is
	 * not yet stopped, it will first be stopped. It is not possible to call
	 * start() after close() has been called.
	 */
	public void close() {
		if (!fClosed) {
			super.close();
			this.destroy();
		}
	}

	/**
	 * fetches an resource
	 * 
	 * @param url
	 *            the documents url, used for relative references
	 * @param stream
	 *            the input stream of the content
	 * @param manager
	 *            the MLFCManager that could be used for this content
	 */
	protected void fetch(InputStream stream, XLink url) {
		//Log.debug(this.toString()+": retrieving document");
		this.fDoc = this.fetchParseAndTransformDocument(url, stream);
		//this.closeConnection();
		//doc.fetchXXX;
	}

	protected void initDocument() throws Exception {
		//fBrowser.getMLFCManager().initPrimaryMLFC(fDoc,fContainer);
		MLFCManager.initMLFC(fDoc, fContainer, fMLFCListener, fBrowser, this	);
	}
	protected void activateDocument() throws Exception {
		if (!this.getPrimary()) {
			// KLUDGE: this was needed in order to make secondary XML content
			// work in
			// SMIL. Usually SMIL has null layout here. This is bad (AWT
			// dependent)
			this.getContainer().removeAll();
			this.getContainer().setLayout(new java.awt.BorderLayout());
		}
		MLFCManager.activateMLFC(fDoc, fContainer, fMLFCListener, fBrowser,
				this.getPrimary());
	}

	protected XMLDocument createDocument(BrowserWindow b, InputStream s, XLink l) {
		return new XSmilesXMLDocument(b, s, l);
	}

	public void setContainer(Container cont) {
		super.setContainer(cont);
		//Log.debug(this.toString()+" : Set container");
	}
	/**
	 * this protected method uses XMLDocument to read the stream and tries to
	 * catch all exeptions
	 */
	protected XMLDocument fetchParseAndTransformDocument(XLink link,
			InputStream stream) {

		/*
		 * Log.debug(link.getURL().toString() + " " +
		 * link.getURL().toString().indexOf(errorFile));
		 */

		try {
			doc = this.createDocument(this.fBrowser, stream, link);
			//currentDocument = doc;

			// Kludge to enable ccpp with multiple browser windows. Gets the
			// title of GUI.
			// Set the ccpp profile of the current GUI to the static
			// XHttpURLConnection.
			//
			// I might want to put this in a synchronized block, but that could
			// jam everything.
			XHttpURLConnection.rereadDeviceURI(this.fBrowser.getGUIManager()
					.getCurrentGUIName());
			doc.retrieveDocument();

			//Log.debug("Document retrieved");
		} catch (TransformerException e) {
			Log.error(e);
			// ---- XSL EXCEPTION
			if (fBrowser.getBrowserLogic() != null) {
				fBrowser.getBrowserLogic().setState(BrowserLogic.ERROROCCURED,
						"XSL Transformation error.");
			}

			String mes = e.getMessageAndLocation();

			// Show error document, or dialog of errordocument fails
			/*
			 * if(link.getURL().toString().endsWith(errorFileFile))
			 * errorCount=666;
			 */
			fBrowser.showErrorDialog("Error retrieving document",
					"XSL Error:\n" + "\n" + link.getURL().toString() + "\n"
							+ mes, false, e);
			return null;
		} catch (SAXException e) {
			// ---- XML EXCEPTION
			Log.error(e);
			fBrowser.getBrowserLogic().setState(BrowserLogic.ERROROCCURED,
					"XML Parser error.");

			SAXParseException pe = fBrowser.getXMLParser()
					.getLastParseException();
			String mes = e.getMessage();
			if (pe != null)
				mes += "\n" + "At line:" + pe.getLineNumber() + " column:"
						+ pe.getColumnNumber() + " of " + pe.getSystemId();
			pe = null;

			// Show error document, or dialog of errordocument fails
			/*
			 * if(link.getURL().toString().endsWith(errorFileFile))
			 * errorCount=666;
			 */
			fBrowser.showErrorDialog("Error retrieving document",
					"XML Parse Error:\n" + "\n" + link.getURL().toString()
							+ "\n" + mes, false, e);
			return null;
		} catch (Exception e) {
			// ---- OTHER EXCEPTION
			Log.error(e);
			fBrowser.getBrowserLogic().setState(BrowserLogic.ERROROCCURED,
					"Unknown error while loading document.");

			// Show error document, or dialog of errordocument fails
			/*
			 * if(link.getURL().toString().endsWith(errorFileFile))
			 * errorCount=666;
			 */
			fBrowser.showErrorDialog("Error retrieving document", link.getURL()
					.toString()
					+ "\n" + e.getMessage(), false, e);
			return null;
		}
		return doc;
	}
	
	protected void destroy() {		
		if (!destroyed) {
			destroyed = true;
			MLFCManager.closeMLFC(doc);
		}
	}

	/**
	 * notify that the zoom level has changed. Note that some content may choose
	 * to not implement zooming.
	 */
	public void setZoom(double zoom) {
		MLFCManager.setZoom(zoom, doc.getDocument());
	}

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.XSmilesContentHandler#getTitle()
     */
    public String getTitle()
    {
        // TODO Auto-generated method stub
        if (doc==null || (ExtendedDocument)doc.getDocument()==null) return "";
        return ((ExtendedDocument)doc.getDocument()).getTitle();
    }

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.XSmilesContentHandler#getContentStream()
     */
    public InputStream getContentStream()
    {
        try
        {
	        Document docu = ((ExtendedDocument)doc.getDocument());
	        StringWriter wr = new StringWriter();
	        boolean preserveSpace=true, indenting=true,addXMLDecl=true;
	        JaxpXSLT.SerializeNode(wr,docu,preserveSpace,indenting,addXMLDecl);
	        wr.flush();
	        wr.close();
	        ByteArrayInputStream s = new ByteArrayInputStream(wr.toString().getBytes());
	        return s;
        } catch (Throwable t)
        {
            Log.error(t);
            return null;
        }
    }

}