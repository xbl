package fi.hut.tml.xsmiles.content;

import java.awt.Container;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.gui.media.general.Media;
import fi.hut.tml.xsmiles.gui.media.general.MediaListener;
import fi.hut.tml.xsmiles.mlfc.MLFCListener;
import fi.hut.tml.xsmiles.util.XSmilesConnection;

/**
 * ContentHandler is an interface to a class that handles certain kind of content
 * distinguished by the content type. E.g. application/xml would be handled
 * by a content handler specialized in XML documents 
 *
 * @author Mikko Honkala
 */
public interface XSmilesContentHandler 
    extends Media
{
   
  /**
   * sets the browser window instance. This is needed mainly by the XMLContentHandler
   */
    public void setBrowserWindow(BrowserWindow browser);
    
   	/**
	 * All traffic to the browser, such as ComponentFactory, etc goes through this listener.
	 * If no listener supplied, the content should still function with some basic level. 
	 * 
	 * @param listener The MLFCListener supplied by the browser
	 * @see MLFCListener
	 */
	public void setMLFCListener(MLFCListener listener);

	/**
	 * the connection object, that should be manually closed ASAP
	 * @param conn
	 */
	public void setConnection(XSmilesConnection conn);
	/**
	 * Adds a MediaListener for this media. The listener will be called <ol>
	 * <li>When the media has been prefetched. (NOT IMPLEMENTED YET).</li>
	 * <li>When the media ends. (continuous media, video/audio) </li></ol>
	 * Static media, such as text and images will end immediately, notifying
	 * immediately about the end of media.
	 */
	public void addMediaListener(MediaListener listener);
	
    /**
     * set this content as primary or secondary.
     * Primary handler is usually the main content
     * in a browser window, while secondary content
     * is usually contained within the primary content
     * @param primary boolean value denoting whether this content is primary or not
     */
    public void setPrimary(boolean primary);
    
    /**
     * get the primary property for this content
     */
    public boolean getPrimary();
    
    //public void setMLFCManager(MLFCManager manager);
    /** 
     * sets the input stream where the media is fetched from.
     * If the inputstream is set, then the media should not
     * re-open the URL
     */
    public void setInputStream(InputStream stream);
    /** 
     * sets the XLink (url) for this media
     * if only the link is set, then the input stream should be automatically
     * opened by the media
     */
    public void setURL(XLink url);
        /** 
     * sets the XLink (url) for this media
     * if only the link is set, then the input stream should be automatically
     * opened by the media
     */
    public void setUrl(URL url);
    
    	/**
	 * Set the coordinates for the media. These are relative to the given container,
	 * set using setContainer(). Setting the bounds will always immediately move
	 * the media to a new location, if it is visible.
	 */
	public void setBounds(int x, int y, int width, int height);

	/**
	 * Set the sound volume for media. Only applicable for sound media formats.
	 * @param percentage	Sound volume, 0-100- (0 is quiet, 100 is original loudness, 200 twice as loud;
	 * dB change in signal level = 20 log10(percentage / 100) )
	 */
	 public void setSoundVolume(int percentage);

	/**
	 * Prefetches media. The URL must have been set using setUrl(). 
	 * The data will be downloaded from the URL. After calling this method,
	 * the media will be in memory and can be played. This is a blocking method.
	 */
	public void prefetch() throws Exception;

	/**
	 * Plays the media. The media will be added to the container set using setContainer().
	 * It will be visible. It will also play any animation it possibly has. Also,
	 * audio media is started using this method.
	 * <p>If the media is not yet prefetched, it will first be prefetched.
	 */
	public void play() throws Exception;

	/**
	 * Pauses the media. The media will stay visible, but any animations will be paused.
	 * Audio media will be silent. NOT IMPLEMENTED YET.
	 * ?How to restart paused media?
	 */
	public void pause();

	/**
	 * Stops the media. The media will be stopped and it will be invisible. Audio will be
	 * silent.
     * It is possible to call start() after stop()
	 */
	public void stop();

	/**
	 * This will freeze all memory and references to this media. If the media is not
	 * yet stopped, it will first be stopped.
     * It is not possible to call start() after close() has been called.
	 */
	public void close();

	/**
	 * This moves the time position in media. Works only for continuous media (video/audio).
	 * @param millisecs		Time in millisecs
	 */
	 public void setMediaTime(int millisecs);

	 /**
	  * get a stream to the content if possible
	  * This should read the contents from a cache if possible.
	  * This is used for instance to save the content to a file
	  * Note that this could re-open the URL connection
	  */
	 public InputStream getContentStream();
	/**
	 * Requests the media player to display a control panel for media.
	 * For audio and video, these can be a volume/play/stop controls,
	 * for images, these can be zoom controls etc.
	 * The controls are GUI dependent, generated through ComponentFactory.
	 * @param visible	true=Display controls, false=don't display controls.
	 */
	public void showControls(boolean visible);	
    //public void fetch(InputStream stream, XLink url) throws Exception;
    /**
     * for XML type of media, this returns the XML content holder object
     * XMLDocument, otherwise null
     */
    public XMLDocument getXMLDocument();

	/**
	 * Checks if this media is static or continuous.
	 * @return true	if media is static (duration is zero, see getOriginalDuration().
	 */
	public boolean isStatic();

	/**
	 * Get the duration of media. Only applicable for continuous media (audio, video).
	 * @return The duration of media in millisecs. zero for static media (images, text)
	 * 			-1 means indefinite (infinite streamed media or unknown duration).
	 */
	public int getOriginalDuration();
	
	/** 
	 * Get the real width of the media. If not visible, or size is unknown, then returns -1.
	 * @return Original width of the media. -1 means no particular width (audio)
	 * or unknown width (text, XML media...)
	 */
	public int getOriginalWidth();

	/** 
	 * Get the real height of the media. If not visible, or size is unknown, then returns -1.
	 * @return Original height of the media. -1 means no particular height (audio)
	 * or unknown height (text, XML media...)
	 */
	public int getOriginalHeight();

	/**
	 * Sets the container the media will be rendered in. If media is audio, this
	 * can be null. Can be called if media is already visible in one container,
	 * it will then move to the new container.
	 * @param container   This container will contain the media.
	 */
	public void setContainer(Container container);
	
	/**
	 * notify that the zoom level has changed. Note that some content may
	 * choose to not implement zooming.
	 */
	public void setZoom(double zoom);
	
	/**
	 * Retrieve a resource via an URL using authenticated HTTP get and store it
	 * as a resource under this contenthandler
	 *
	 */
    public XSmilesConnection get(URL dest, short type) throws Exception;
    
    /** get the list of referenced resources */
    public ResourceReferencer getResourceReferencer();

    /** get the possible title (can be null) */
    public String getTitle();

    
//public void initDocument() throws Exception;
    //public void activateDocument() throws Exception;
    /**
     * destroys this content.
     * this method
     */
	//public void destroy();
}
