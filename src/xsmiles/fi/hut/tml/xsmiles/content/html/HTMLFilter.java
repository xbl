/* 
 * (C) Copyright 2002, Andy Clark.  All rights reserved.
 *
 * This file is distributed under an Apache style license. Please
 * refer to the LICENSE file for specific details.
 */

package fi.hut.tml.xsmiles.content.html;

import fi.hut.tml.xsmiles.Log;

//package org.cyberneko.html.filters;
import org.cyberneko.html.filters.*;

import java.lang.reflect.Method;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.NamespaceContext;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XMLDocumentHandler;
import org.apache.xerces.xni.XMLLocator;
import org.apache.xerces.xni.XMLResourceIdentifier;
import org.apache.xerces.xni.XMLString;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.parser.XMLDocumentFilter;
import org.apache.xerces.xni.parser.XMLDocumentSource;

/**
 * This class implements a filter that simply passes document
 * events to the next handler. It can be used as a base class to
 * simplify the development of new document filters.
 *
 * @author Andy Clark
 *
 * @version $Id: HTMLFilter.java,v 1.2 2002/11/15 09:38:13 honkkis Exp $
 */
public class HTMLFilter 
    extends DefaultFilter {

    //
    // Data
    //

    /** Start element. */
    public void startElement(QName element, XMLAttributes attributes, Augmentations augs)
        throws XNIException {
        if (fDocumentHandler != null) {
            //element.rawname = element.rawname.toLowerCase(); 
            if (element.uri==null||element.uri.length()==0)
            {
                element.uri="http://www.w3.org/1999/xhtml";
            }
            //Log.debug("StartElement: "+element);
            fDocumentHandler.startElement(element, attributes, augs);
        }
    } // startElement(QName,XMLAttributes,Augmentations)




} // class DefaultFilter
