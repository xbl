package fi.hut.tml.xsmiles.content.html;

import fi.hut.tml.xsmiles.content.XSmilesContentHandler;
import fi.hut.tml.xsmiles.content.xml.XMLContentHandler;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.BrowserLogic;
import fi.hut.tml.xsmiles.XLink;
import fi.hut.tml.xsmiles.Log;

import fi.hut.tml.xsmiles.protocol.http.XHttpURLConnection;

import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.awt.Container;

import java.net.URL;
import java.io.InputStream;

import java.util.Vector;

/**
 * Handles text/html
 *
 * This class should be reusable for optimization purposes
 * 
 * @author Mikko Honkala
 */
public class HTMLContentHandler extends XMLContentHandler implements XSmilesContentHandler {

    
    public HTMLContentHandler()
    {
    }

    
    protected XMLDocument createDocument(BrowserWindow b, InputStream s, XLink l)
    {
        return new HTMLDocument(b, s, l);
    }

}
