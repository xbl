/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 */
package fi.hut.tml.xsmiles.content.html;

import java.io.*;
import org.w3c.dom.*;        //located in domcore.jar
import org.w3c.dom.events.*; //located in domcore.jar
import java.net.*;
import java.util.Vector;
import java.util.StringTokenizer;
import java.util.Enumeration;
import java.awt.Container;

import fi.hut.tml.xsmiles.XMLConfigurer;
import fi.hut.tml.xsmiles.Log;
import fi.hut.tml.xsmiles.XSmilesXMLDocument;
import fi.hut.tml.xsmiles.XMLDocument;
import fi.hut.tml.xsmiles.BrowserWindow;
import fi.hut.tml.xsmiles.XLink;

import fi.hut.tml.xsmiles.dom.ExtendedDocument;

import org.xml.sax.InputSource;
import org.apache.xerces.xni.parser.XMLDocumentFilter;



/**
 * The XMLdocument is an object that can retrieve and parse a XML document
 * when given the URL of the document as an argument in the constructor. 
 * The document can then be accessed through the DOM-interface that it provides.
 * The document also saves the original source of the document, so that 
 * the source can be given to any component that is interested of it (for
 * example the SourceViewMLFC)
 *
 * @author      Kreivi
 * @author	Mikko Honkala
 * @auhtor      Juha
 * @version	$Revision: 1.5 $
 */
public class HTMLDocument extends XSmilesXMLDocument implements XMLDocument {

    	/**
	*  This constructor is used by the Browser when it requests 
	*  a new XMLdocument. <br>
	*  The DOM-implementation is set here.
	*  @param Browser browser
	*  @param XLink xlink the source of the document.
	*  
	*/
	public HTMLDocument(BrowserWindow browser, XLink link)
	{
		super(browser,link);
	}
    
    	/**
	*  This constructor is used by the Browser when it requests 
	*  a new XMLdocument. <br>
	*  The DOM-implementation is set here.
	*  @param Browser browser
	*  @param XLink xlink the source of the document.
	*  
	*/
	public HTMLDocument(BrowserWindow browser, InputStream stream, XLink link)
	{
		super(browser,stream,link);
	}

	/**
	* An alternative constructor, where the source can be given 
	* as a string.
	*
	* @param Browser browser 
	* @param String s the source of the document, given as a string.
	*/
	// NOTE: Not implemented.. try the XMLDocument(BrowserWindow, domDocument)
	public HTMLDocument(BrowserWindow browser, String s)
	{
		super(browser,s);
	}


	/**
	* Another alternative constructor
	* 
	* @param browser The BrowserWindow
	* @param doc The Document
	*/
	public HTMLDocument(BrowserWindow browser, Document doc)
	{
		super(browser,doc);
	}
	/**
	* This is the method that performs all the actual work of the XMLDocument.
	* It fetches and parses the document (determined in the constructor) and
	* makes the document available for use by the other browser components.
	* The handle to the DOM-interface is kept in a variable called doc.
	*
	* Run by a separate thread, so that fetching can be interrupted, for example in 
	* error situations.
	*/
	public void retrieveDocument() throws Exception
	{
        Log.debug("HTMLDocument retrieving the document.");
//		try
//		{
			// Set instance variables
		
			// debug: remove this wait
			//try {Thread.sleep(3000);} catch (Exception e) {}
			this.sourceXMLURL = this.m_link.getURL();
			//		Log.debug("*** xmldoc "+this+" sourceXMLURL:"+sourceXMLURL);
            /*
			if ((this.m_link.getLinkType())==XLink.POST)
			{
				// Use post method
				this.sourceXMLDoc = doPost(m_link);
			}
			else
             */
			{
				String tidyConversion = "";
				// Get browser configurer from Browser
				XMLConfigurer config = m_browser.getBrowserConfigurer();
				tidyConversion = config.getProperty("html_fo_conversion/tidy_conversion");
				if (tidyConversion.equalsIgnoreCase("true"))
				{
					//this.sourceXMLDoc = readWithTidy(this.sourceXMLURL,this.m_stream);
					this.sourceXMLDoc = readWithNekoHTML(this.sourceXMLURL,this.m_stream);
				}
				else
				{
				  if (m_stream==null) this.sourceXMLDoc = fetchXMLDOM(this.sourceXMLURL);
                                    else  this.sourceXMLDoc = fetchXMLDOM(m_stream,this.sourceXMLURL);
				}
                                if (this.sourceXMLDoc !=null&&this.sourceXMLDoc instanceof ExtendedDocument)
                                {
                                    ((ExtendedDocument)this.sourceXMLDoc).setHTMLDocument(true);
                                }
			}
			transformDocument();
//		} catch (Exception e)
//		{
//			Log.error(e);
//		}	
	}

	/**
    * This method reads the URL using Neko HTML
	*/
	protected Document readWithNekoHTML(URL sourceURL, InputStream stream) 
	throws Exception
	{
        org.cyberneko.html.parsers.DOMParser parser = new org.cyberneko.html.parsers.DOMParser();
	    parser.setFeature("http://xml.org/sax/features/namespaces",true);
        parser.setProperty("http://cyberneko.org/html/properties/names/elems","lower"); 

        XMLDocumentFilter html = new HTMLFilter();
        XMLDocumentFilter[] filters = { html };

        //XMLParserConfiguration parser = new HTMLConfiguration();
        parser.setProperty("http://cyberneko.org/html/properties/filters", filters);
        InputSource input = new InputSource(stream);
        input.setSystemId(sourceURL.toString());
        //parser.setDocumentClassName("fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl");
        parser.setProperty("http://apache.org/xml/properties/dom/document-class-name",
                                       "fi.hut.tml.xsmiles.dom.XSmilesDocumentImpl");        
        parser.parse(input);
        Document doc = parser.getDocument();
        return doc;
    }
	// Added by vesa, Aug 24, 2001
	/**
	* This method is alternative for fetchXMLDOM().
	* Creates the DOM tree for the document defined by @param URL sourceURL
	* Sets the variable sourceXMLDoc to refer to the document.
	* <p> 
	* If the document is a HTML document, it is handled separately by the
	* util.XHTMLTool.
	*</p>
	*/
    /*
	protected Document readWithTidy(URL sourceURL, InputStream stream) 
	throws Exception
	{
		Document sourceDoc = null;

		//if (XHTMLTool.isHTMLDocument(sourceURL))
        // we already know that this is HTML document
		if (true)
		{
			// Fetch the content of the sourceURL and convert it with JTidy
			//WAS: String source = XHTMLTool.getXHTMLString(this.sourceXMLURL);	    
			String source = XHTMLTool.getXHTMLString(sourceURL);
			// If we're dealing with a HTMLL document, the default name space
			// which is created by JTidy has to be removed, because Xalan does
			// not handle default namespaces correctly.
			// After this, a style sheet reference is added, which refers 
			// to the XHTML->FO style sheet.
			sourceDoc = m_parser.openDocument(new StringReader(source), true);

				String ssUrl = "";
			XMLConfigurer config = m_browser.getBrowserConfigurer();
			ssUrl = config.getProperty("html_fo_conversion/stylesheet/url");
			String stylesheetEnabled = config.getProperty("html_fo_conversion/stylesheet/enabled");
            if (stylesheetEnabled==null) stylesheetEnabled = "false";
			if ((!stylesheetEnabled.equals("false")) && 
                (!stylesheetEnabled.equals("0")) &&
                ((ssUrl != null) && (ssUrl.length() > 1)))
			{
				URL ss = null;
				// Add the specified ss URL to source Document
				try
				{
                    if (ssUrl.startsWith("file:")||ssUrl.indexOf(':')<0)
                    {
                        ss = new URL(Browser.toURL(new File(".").getAbsoluteFile()),ssUrl);
                    }
					ss = new URL(ssUrl);
				}
				catch (MalformedURLException mue)
				{
					System.out.println("XHTML stylesheet URL '"+
						ssUrl+"' is BAD.\n\r"+mue);
				}
				System.out.println("XHTML stylesheet URL is '"+ss.toString()+"'.");
				XHTMLTool.addStyleSheetReference(ss, sourceDoc);
			}
			else
			{
				return sourceDoc;
			}

			return sourceDoc;
		}
		else
		{				  
            if (m_stream==null) return  fetchXMLDOM(this.sourceXMLURL);
            else return fetchXMLDOM(m_stream,this.sourceXMLURL);

			//return fetchXMLDOM(sourceURL);
		}
	}
     */
}

