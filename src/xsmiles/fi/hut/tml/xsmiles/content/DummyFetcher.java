/*
/* X-Smiles
 * Copyright (C) Helsinki University of Technology. All rights reserved.
 * For details on use and redistribution please refer to the
 * LICENSE_XSMILES file included with these sources.
 * Created on Jun 1, 2004
 *
 */
package fi.hut.tml.xsmiles.content;

import java.net.URL;
import java.net.URLConnection;

import fi.hut.tml.xsmiles.util.HTTP;
import fi.hut.tml.xsmiles.util.XSmilesConnection;


/**
 * @author honkkis
 *
 */
public class DummyFetcher implements ResourceFetcher
{

    /* (non-Javadoc)
     * @see fi.hut.tml.xsmiles.content.ResourceFetcher#get(java.net.URL, short)
     */
    public XSmilesConnection get(URL dest, short type) throws Exception
    {
        return HTTP.get(dest,null);
    }

}
