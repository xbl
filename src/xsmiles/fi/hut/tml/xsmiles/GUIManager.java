/*
 * X-Smiles
 * license can be found in licenses/XSMILES_LICENSE
 */
package fi.hut.tml.xsmiles;

import fi.hut.tml.xsmiles.gui.*;
import fi.hut.tml.xsmiles.gui.components.ComponentFactory;
import fi.hut.tml.xsmiles.event.BrowserEvent;
import fi.hut.tml.xsmiles.event.BrowserEventListener;
import java.awt.Frame;
import java.awt.ScrollPane;
import java.awt.*;
import java.net.*;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Vector;
import java.lang.reflect.Constructor;
import org.w3c.dom.*;

/**
 * This class manages switching of GUIs. The initiation is done with
 * the java.lang.reflect classes, so that we don't have to even have
 * all the GUIs, for the system to function. GUI configuration stuff in config.xml
 *
 * @author  Juha, Niklas von Knorring, Teemu Ropponen
 * @version  $Revision: 1.31 $
 */
public class GUIManager
{
    public  int  numGUIs;
    public String initialGUI;
    public String currentGUI;
    private Vector            guiNames;
    private Vector            themeVector;
    private File              themeDir;
    private BrowserWindow     browser;
    private XMLConfigurer bc;
    private GUI               skin;
    private int i;
    
    /**
     * Constructs GUI class.
     *
     * @param browser reference to the main BrowserWindow object
     */
    public GUIManager(BrowserWindow browser)
    {
        this.browser = browser;
        init();
    }
    
    /**
     * Read a list of GUIs from config.xml
     * read which GUI is going to be the initial GUI
     * Get a location for the theme packs
     *
     * @see XMLConfigurer
     */
    private void init()
    {
        bc=browser.getBrowserConfigurer();
        createGUINameVector();
        initialGUI=bc.getProperty("gui/initialgui");
        //createThemeVector(bc.getProperty("gui/themepacklocation"));
    }
    
    /**
     * Destroy old GUI, init new one and issue GUIChangedEvent so
     * that MLFC's remember to update their MLFCToolboxes to the new gui
     *
     * @param guiName The name of the gui (see config.xml)
     * @param reload Reload document after gui visible
     */
    public void showGUI(String guiName, boolean reload)
    {
        this.showGUI(guiName,reload,true);
    }
    public void showGUI(String guiName, boolean reload,boolean destroyOld)
    {
        Container c=null;
        
        if(skin!=null&&destroyOld)
        {
            destroyOldGui();
        }
        
        //Log.debug("%%% GUIManager: running gc");
        //System.gc();
        
        String guiPath="gui/devices/"+guiName+"/class";
        
        String guiClass=bc.getProperty(guiPath);
        if(guiClass==null || guiClass.equals(""))
        {
            Log.error("No GUI Class found for GUI "+guiName);
        }
        
        currentGUI=guiName;
        skin=createGUI(guiClass,browser,browser.getContentArea());
        
        skin.start();
        /*
        try
        {
            Thread.sleep(10000);
        }
        catch (InterruptedException ex)
        {
            Log.error(ex);
        }
        */
        Log.debug("Gui loaded");
        if(reload)
            reInitGUI();
        }
    
    public void showGUI(String guiName)
    {
        showGUI(guiName,true);
    }
    /**
     * Creates the initial GUI
     */
    public void createMainGUI()
    {
        showGUI(initialGUI, false);
    }
    
    public void createMainGUI(boolean b)
    {
        showGUI(initialGUI, b);
    }
    /**
     * ???
     */
    private void reInitGUI()
    {
        if (skin.shouldReloadAtStartup())
            browser.navigate(NavigationState.RELOAD_LAST_IN_HISTORY);
        // MH; Should wait for the GUI document to finish, and then do reload
        /*
        Thread waitThread = new Thread()
        {
            public void run()
            {
                try
                {
                    Thread.sleep(3500);
                } catch (InterruptedException e)
                {
                }
            }
        };
        waitThread.start();
         */
    }
    
    /**
     * nullify? why?
     */
    protected void destroyOldGui()
    {
        if (skin != null)
        {
            skin.destroy();
            skin = null;
        }
        browser.getEventBroker().removeAllListeners();
    }
    
    /**
     * @return The current GUI
     */
    public GUI getCurrentGUI()
    {
        if(skin==null)
        {
            Log.debug("Oh bloody hell. skin is null for some reason... we have to create a nullgui. This should most propably be done in a better way.");
            //skin = new NullGUI(browser);
            skin = this.createGUI("fi.hut.tml.xsmiles.gui.swing.NullGUI", browser,null);
        }
        return skin;
    }
    
    /**
     * @return Vector of all gui Names
     */
    public Vector getGUINames()
    {
        return guiNames;
    }
    
    /**
     * @return Theme names in vector
     */
    public Vector getThemeNames()
    {
        return themeVector;
    }
    
    /**
     * @return the Number of GUIs
     */
    public int getNumGUIs()
    {
        return numGUIs;
    }
    
    public String getCurrentGUIName()
    {
        if(currentGUI==null)
        {
            return initialGUI;
        }
        return currentGUI;
    }
    
    /**
     * Set the preferences of the GUI to a title corresponding
     * the name in the configuration file. Only needed, if the browser
     * is run without a GUI, and special constraints are wanted for the browser content
     * area.
     * @param s The title of the GUI corresponding to the one in config.xml
     */
    public void setCurrentGUIName(String s)
    {
        currentGUI=s;
    }
    
    public ComponentFactory getComponentFactory()
    {
        return null;
        //return guiManager.getCurrentGUI().getComponentFactory();
    }
    
    /**
     * Create a cross-device GUI component factory
     */
    // private ComponentFactory createComponentFactory(String className)
    //{
    /**
     * Object instance = null;
     * Class cfClass = null;
     * try {
     * Class[] constructorParameters;
     * Object[] initArgs;
     * cfClass = Class.forName(className);
     *
     * if (container!=null)
     * {
     * constructorParameters = new Class[0];
     * Constructor constructor =
     * guiClass.getDeclaredConstructor(
     * constructorParameters);
     * initArgs = new Object[2];
     * initArgs[0] = browser;
     * initArgs[1] = container;
     * instance = constructor.newInstance(initArgs);
     * return (GUI)instance;
     * } else
     * {
     * constructorParameters = new Class[1];
     * constructorParameters[0] = Class.forName("fi.hut.tml.xsmiles.BrowserWindow");
     * Constructor constructor =
     * guiClass.getDeclaredConstructor(constructorParameters);
     * initArgs = new Object[1];
     * initArgs[0] = browser;
     * instance = constructor.newInstance(initArgs);
     * return (GUI)instance;
     * }
     * } catch (java.lang.reflect.InvocationTargetException ex) {
     * Throwable t = ex.getTargetException();
     * String msg;
     * if (t != null) {
     * msg = t.getMessage();
     * }
     * else {
     * msg = ex.getMessage();
     * }
     * Log.error("GUIMANAGER: InvocationTargetException: " + msg);
     * Log.error(ex);
     * ex.printStackTrace();
     * } catch (Exception ex) {
     * Log.error(ex);
     * }
     * Log.error("GUIMANAGER: Returning null gui!");
     * return null;
     */
    //return null;
    // }
    
    /**
     * This class calls the UI constructors with the class.forName
     *
     */
    private GUI createGUI(String className,BrowserWindow browser, Container container)
    {
        Object instance = null;
        Class guiClass = null;
        Constructor constructor=null;
        Class[] constructorParameters=null;
        Object[] initArgs;
        try
        {
            guiClass = Class.forName(className);
            
            if (container!=null)
            {
                if (browser==null) Log.error("createGUI: browser null");
                constructorParameters = new Class[2];
                constructorParameters[0] = Class.forName("fi.hut.tml.xsmiles.BrowserWindow");
                constructorParameters[1] = Class.forName("java.awt.Container");
                constructor =
                guiClass.getDeclaredConstructor(
                constructorParameters);
                initArgs = new Object[2];
                initArgs[0] = browser;
                initArgs[1] = container;
                instance = constructor.newInstance(initArgs);
                return (GUI)instance;
            } else
            {
                constructorParameters = new Class[1];
                constructorParameters[0] = Class.forName("fi.hut.tml.xsmiles.BrowserWindow");
                constructor =
                guiClass.getDeclaredConstructor(constructorParameters);
                initArgs = new Object[1];
                initArgs[0] = browser;
                instance = constructor.newInstance(initArgs);
                return (GUI)instance;
            }
        } catch (java.lang.reflect.InvocationTargetException ex)
        {
            Throwable t = ex.getTargetException();
            String msg;
            if (t != null)
            {
                msg = t.getMessage();
            }
            else
            {
                msg = ex.getMessage();
            }
            Log.error("GUIMANAGER: InvocationTargetException: " + msg+" : browser:"+browser+" container:"+container+" constructor:"+constructor+" parameters:"+constructorParameters);
            Log.error(ex);
        } catch (Exception ex)
        {
            Log.error(ex);
        }
        Log.error("GUIMANAGER: Returning null gui!");
        return null;
    }
    
    /**
     * Fetch all elements that represent a GUI from config.xml
     * If the element is market active, then append it to the list of guis
     */
    private void createGUINameVector()
    {
        guiNames=new Vector();
        numGUIs=0;
        Element e=bc.findElement("gui/devices");
        NodeList guis=e.getChildNodes();
        
        for(int i=0;i<guis.getLength();i++)
        {
            Node no=guis.item(i);
            if (no.getNodeType()==Node.ELEMENT_NODE)
            {
                Element gui=(Element)no;
                if(((Element)gui.getElementsByTagName("active").item(0)).getFirstChild().getNodeValue().equals("true"))
                {
                    guiNames.addElement(gui.getTagName());
                    numGUIs++;
                }
                
            }
        }
    }
    
    /**
     * Get themes from directory
     *
     * @param dir themeDir
     */
    private void createThemeVector(String tdir)
    {
        String[] themeNames=new String[30];
        themeDir=new File(tdir);
        i=0;
        if(themeDir.isDirectory())
        {
            try
            {
                themeNames=themeDir.list(new FilenameFilter()
                {
                    public boolean accept(File dir, String name)
                    {
                        if(name.endsWith("zip")||name.toUpperCase().endsWith("ZIP"))
                        {
                            i++;
                            return true;
                        } else
                            return false;
                    }
                });
                if(themeNames==null)
                {
                    Log.error("No themeNames found");
                }
            } catch(SecurityException e)
            {
                Log.error(e);
            }
            themeVector=new Vector();
            while(i-->0)
            {
                themeVector.addElement(themeNames[i]);
            }
        } else
        {
            Log.error("No theme directory found: "+tdir);
        }
    }
}
