
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares customAttributes element.
 *  
 * Defines attributes:
 *  o 
 *  + 
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILCustomAttributesElement extends SMILElement {

}