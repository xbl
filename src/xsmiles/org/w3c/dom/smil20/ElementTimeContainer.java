/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.NodeList;

import org.w3c.dom.DOMException;

/**
 *  This is a placeholder - subject to change. This represents generic 
 * timelines. 
 */
public interface ElementTimeContainer extends ElementTime {
    // fillTypes
    public static final short FILL_REMOVE               = 0;
    public static final short FILL_FREEZE               = 1;

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFill();
    public void setFill(String fill)
                     throws DOMException;

    /**
     *  A NodeList that contains all timed childrens of this node. If there are
     *  no timed children, the <code>Nodelist</code> is empty.  An iterator 
     * is more appropriate here than a node list but it requires Traversal 
     * module support. 
     */
    public NodeList getTimeChildren();

    /**
     *  Returns a list of child elements active at the specified invocation. 
     * @param instant  The desired position on the local timeline in 
     *   milliseconds. 
     * @return  List of timed child-elements active at instant. 
     */
    public NodeList getActiveChildrenAt(int instant); // FLOAT BUG

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr();
    public void setAbstractAttr(String abstractAttr)
                              throws DOMException;

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor();
    public void setAuthor(String author)
                              throws DOMException;

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright();
    public void setCopyright(String copyright)
                              throws DOMException;

    /**
     *  See the region attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getRegion();
    public void setRegion(String region)
                              throws DOMException;


	//// added methods ////

	public void childEnded(long childDuration);
	
}

