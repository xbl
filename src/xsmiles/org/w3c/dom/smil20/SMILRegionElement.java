/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  Controls the position, size and scaling of media object elements. See the  
 * region element definition . 
 *
 * Defines attributes:
 *  o fit, top, z-index
 *  + Test, showBackground, bottom, left, right, top, soundLevel, regionName
 *
 * Defines methods:
 *  o
 *  +
 */
public interface SMILRegionElement extends SMILElement, ElementLayout { //, ElementTest,
//											XElementCustomTest, XElementSkipContent {
    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFit();
    public void setFit(String fit)
                                      throws DOMException;

    /**
	 * top
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTop();
    public void setTop(String top)
                                      throws DOMException;
	/**
	 * bottom
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBottom();
	public void setBottom(String bottom) throws DOMException;

	/**
	 * left
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getLeft();
	public void setLeft(String left) throws DOMException;

	/**
	 * right
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRight();
	public void setRight(String right) throws DOMException;

	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public int getZIndex();
	public void setZIndex(int zIndex)
	                                  throws DOMException;
	// showBackground types
	public static final short SHOWBACKGROUND_ALWAYS            = 0;
	public static final short SHOWBACKGROUND_WHENACTIVE        = 1;

	/**
	 * showBackground
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getShowBackground();
	public void setShowBackground(short showBackground) throws DOMException;

	/**
	 * soundLevel
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getSoundLevel();
	public void setSoundLevel(String soundLevel) throws DOMException;

	/**
	 * regionName
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegionName();
	public void setRegionName(String regionName) throws DOMException;

}

