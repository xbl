
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares brush element.
 *  
 * Defines attributes:
 *  o 
 *  + abstract, region, fill, author, copyright, color, erase, tabindex, transIn, transOut, readIndex
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILBrushElement extends SMILElement, ElementTime, SMILRegionInterface  {
    // fillTypes
    public static final short FILL_REMOVE               = 0;
    public static final short FILL_FREEZE               = 1;

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFill();
    public void setFill(String fill)
                     throws DOMException;

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr();
    public void setAbstractAttr(String abstractAttr)
                              throws DOMException;

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor();
    public void setAuthor(String author)
                              throws DOMException;

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright();
    public void setCopyright(String copyright)
                              throws DOMException;

    /**
     *  See the  readIndex attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getReadIndex();
    public void setReadIndex(String readIndex)
                              throws DOMException;

	// erase types
	public static final short ERASE_WHENDONE               = 0;
	public static final short ERASE_NEVER                  = 1;

    /**
     *  See the erase attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getErase();
    public void setErase(short erase) throws DOMException;

    /**
     *  See the color attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getColor();
    public void setColor(String color) throws DOMException;

    /**
     *  See the tabindex attribute in LinkingAttributes module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getTabindex();
    public void setTabindex(int tabindex) throws DOMException;

    /**
     *  See the transIn attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransIn();
    public void setTransIn(String transIn) throws DOMException;

    /**
     *  See the transOut attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransOut();
    public void setTransOut(String transOut) throws DOMException;

}