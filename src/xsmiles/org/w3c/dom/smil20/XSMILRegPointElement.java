
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares regPoint for the region element. 
 *  
 * Defines attributes:
 *  o 
 *  + top, bottom, left, right, regAlign
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILRegPointElement extends SMILElement {
    /**
	 * top
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTop();
    public void setTop(String top)
                                      throws DOMException;
	/**
	 * bottom
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBottom();
	public void setBottom(String bottom) throws DOMException;

	/**
	 * left
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getLeft();
	public void setLeft(String left) throws DOMException;

	/**
	 * right
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRight();
	public void setRight(String right) throws DOMException;

	/**
	 * regAlign
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegAlign();
	public void setRegAlign(String regAlign) throws DOMException;

}