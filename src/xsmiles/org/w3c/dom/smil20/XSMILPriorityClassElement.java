/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  A SMIL document is the root of the SMIL Hierarchy and holds the entire 
 * content. Beside providing access to the hierarchy, it also provides some 
 * convenience methods for accessing certain sets of information from the 
 * document.  Cover document timing, document locking?, linking modality and 
 * any other document level issues. Are there issues with nested SMIL files?  
 * Is it worth talking about different document scenarios, corresponding to 
 * differing profiles? E.g. Standalone SMIL, HTML integration, etc. 
 *  
 * Defines attributes:
 *  o 
 *  + peers, higher, lower, pauseDisplay, abstract, author, copyright
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILPriorityClassElement extends SMILElement {

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr();
    public void setAbstractAttr(String abstractAttr)
                              throws DOMException;

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor();
    public void setAuthor(String author)
                              throws DOMException;

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright();
    public void setCopyright(String copyright)
                              throws DOMException;

	// pauseDisplay types
	public static final short PAUSEDISPLAY_SHOW                 = 0;
	public static final short PAUSEDISPLAY_HIDE                 = 1;
	public static final short PAUSEDISPLAY_DISABLE              = 2;

    /**
     *  See the pauseDisplay attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getPauseDisplay();
    public void setPauseDisplay(short pauseDisplay)
                              throws DOMException;

    // peers types 
    public static final short PEERS_STOP                = 0;
    public static final short PEERS_PAUSE               = 0;
    public static final short PEERS_DEFER               = 0;
    public static final short PEERS_NEVER               = 0;

    /**
     *  See the peers attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getPeers();
    public void setPeers(short peers)
                              throws DOMException;

    // higher types
    public static final short HIGHER_PAUSE                = 0;
    public static final short HIGHER_STOP                 = 1;
    /**
     *  See the higher attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getHigher();
    public void setHigher(short higher)
                              throws DOMException;

    // lower types
    public static final short LOWER_DEFER                = 0;
    public static final short LOWER_NEVER                = 1;
    /**
     *  See the lower attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getLower();
    public void setLower(short lower)
                              throws DOMException;
}

