/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  This interface defines the set of basic timing attributes that are common to all 
 * timed elements. 
 */
public interface XElementSubRegionAttributes {
    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFit();
    public void setFit(String fit)
                                      throws DOMException;

    /**
	 * top
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTop();
    public void setTop(String top)
                                      throws DOMException;
	/**
	 * bottom
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBottom();
	public void setBottom(String bottom) throws DOMException;

	/**
	 * left
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getLeft();
	public void setLeft(String left) throws DOMException;

	/**
	 * right
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRight();
	public void setRight(String right) throws DOMException;

	/**
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public int getZIndex();
	public void setZIndex(int zIndex)
	                                  throws DOMException;
	/**
	 * backgroundColor
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getBackgroundColor();
	public void setBackgroundColor(String backgroundColor) throws DOMException;

	/**
	 * regPoint
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegPoint();
	public void setRegPoint(String regPoint) throws DOMException;

	/**
	 * regAlign
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getRegAlign();
	public void setRegAlign(String regAlign) throws DOMException;

}

