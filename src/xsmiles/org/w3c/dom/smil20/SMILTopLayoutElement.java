/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  Declares layout properties for the top-layout element. See the  top-layout 
 * element definition . 
 *  
 * Defines attributes:
 *  o
 *  + open, close 
 *
 * Defines methods:
 *  o
 *  +
 */

public interface SMILTopLayoutElement extends SMILElement, ElementLayout {
    // open types
    public static final short OPEN_ONSTART                = 0;
    public static final short OPEN_WHENACTIVE             = 1;

    /**
	 * open attribute.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getOpen();
    public void setOpen(short open) throws DOMException;

    // close types
    public static final short CLOSE_ONREQUEST              = 0;
    public static final short CLOSE_WHENNOTACTIVE          = 1;

    /**
     * open attribute.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getClose();
    public void setClose(short close) throws DOMException;
	
}

