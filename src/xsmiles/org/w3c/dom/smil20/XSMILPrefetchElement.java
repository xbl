
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares prefetch element.
 *  
 * Defines attributes:
 *  o 
 *  + mediaSize, mediaTime, bandwidth, src, clipBegin, clipEnd
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILPrefetchElement extends SMILElement, ElementTime {

    /**
     *  See the mediaSize attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getMediaSize();
    public void setMediaSize(String mediaSize)
                              throws DOMException;

    /**
     *  See the mediaTime attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getMediaTime();
    public void setMediaTime(String mediaTime)
                              throws DOMException;
    /**
     *  See the bandwidth attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBandwidth();
    public void setBandwidth(String bandwidth)
                              throws DOMException;
    /**
     *  See the  clipBegin attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getClipBegin();
    public void setClipBegin(String clipBegin)
                              throws DOMException;

    /**
     *  See the  clipEnd attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getClipEnd();
    public void setClipEnd(String clipEnd)
                              throws DOMException;

    /**
     *  See the  src attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSrc();
    public void setSrc(String src)
                              throws DOMException;
}