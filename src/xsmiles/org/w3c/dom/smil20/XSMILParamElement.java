
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares param element.
 *  
 * Defines attributes:
 *  o 
 *  + name, value, valuetype, type
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILParamElement extends SMILElement {
    /**
     *  See the name attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getName();
    public void setName(String name) throws DOMException;

    /**
     *  See the value attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getValue();
    public void setValue(String value) throws DOMException;


    /**
     *  See the valuetype attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getValueType();
    public void setValueType(String valueType) throws DOMException;


    /**
     *  See the  type attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getType();
    public void setType(String type) throws DOMException;

}