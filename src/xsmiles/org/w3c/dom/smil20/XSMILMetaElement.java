
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares param element.
 *  
 * Defines attributes:
 *  o 
 *  + content, name
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILMetaElement extends SMILElement {
    /**
     *  See the content attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getContent();
    public void setContent(String content) throws DOMException;

    /**
     *  See the name attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getName();
    public void setName(String name) throws DOMException;


}