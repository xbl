/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  Declares media content. 
 */
public interface SMILMediaElement extends ElementTime, SMILElement, SMILRegionInterface {
    // fillTypes
    public static final short FILL_REMOVE               = 0;
    public static final short FILL_FREEZE               = 1;

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFill();
    public void setFill(String fill)
                     throws DOMException;

    /**
     *  See the  abstract attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAbstractAttr();
    public void setAbstractAttr(String abstractAttr)
                              throws DOMException;

    /**
     *  See the  author attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAuthor();
    public void setAuthor(String author)
                              throws DOMException;

    /**
     *  See the  copyright attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getCopyright();
    public void setCopyright(String copyright)
                              throws DOMException;

    /**
     *  See the  alt attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAlt();
    public void setAlt(String alt)
                              throws DOMException;

    /**
     *  See the  clipBegin attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getClipBegin();
    public void setClipBegin(String clipBegin)
                              throws DOMException;

    /**
     *  See the  clipEnd attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getClipEnd();
    public void setClipEnd(String clipEnd)
                              throws DOMException;


    /**
     *  See the  longdesc attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getLongdesc();
    public void setLongdesc(String longdesc)
                              throws DOMException;

    /**
     *  See the  readIndex attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getReadIndex();
    public void setReadIndex(String readIndex)
                              throws DOMException;

    /**
     *  See the  src attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSrc();
    public void setSrc(String src)
                              throws DOMException;

    /**
     *  See the  title attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTitle();
    public void setTitle(String title)
                              throws DOMException;

    /**
     *  See the  type attribute from  . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getType();
    public void setType(String type)
                              throws DOMException;

	// erase types
	public static final short ERASE_WHENDONE               = 0;
	public static final short ERASE_NEVER                  = 1;

    /**
     *  See the erase attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getErase();
    public void setErase(short erase) throws DOMException;

	// mediaRepeat types
	public static final short MEDIAREPEAT_PRESERVE               = 0;
	public static final short MEDIAREPEAT_STRIP                  = 1;

    /**
     *  See the mediaRepeat attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getMediaRepeat();
    public void setMediaRepeat(short mediaRepeat) throws DOMException;

    /**
     *  See the tabindex attribute in LinkingAttributes module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getTabindex();
    public void setTabindex(int tabindex) throws DOMException;

    /**
     *  See the transIn attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransIn();
    public void setTransIn(String transIn) throws DOMException;

    /**
     *  See the transOut attribute in transition effects module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTransOut();
    public void setTransOut(String transOut) throws DOMException;
}

