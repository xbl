/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

import fi.hut.tml.xsmiles.mlfc.smil.viewer.MyFloat;

/**
 *  This interface defines the set of basic timing attributes that are common to all 
 * timed elements. 
 */
public interface XElementBasicTime extends ElementTimeControl {
    /**
     *  The desired value (as a list of times) of the  begin instant of this 
     * node. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBegin();
    public void setBegin(TimeList begin)
                     throws DOMException;

    /**
     *  The list of active  ends for this node. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getEnd();
    public void setEnd(TimeList end)
                     throws DOMException;

    /**
     *  The desired simple  duration value of this node in seconds.
     * @return 	String (eg. 2s, media or indefinite)
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public Time getDur();
    public void setDur(String dur)
                     throws DOMException;

    // restartTypes - TODO: DEFAULT?
    public static final short RESTART_ALWAYS            = 0;
    public static final short RESTART_NEVER             = 1;
    public static final short RESTART_WHEN_NOT_ACTIVE   = 2;

    /**
     *  A code representing the value of the  restart attribute, as defined 
     * above. Default value is <code>RESTART_ALWAYS</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getRestart();
    public void setRestart(short restart)
                     throws DOMException;


    /**
     *  The  repeatCount attribute causes the element to play repeatedly 
     * (loop) for the specified number of times. A negative value repeat the 
     * element indefinitely. Default value is 0 (unspecified). 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getRepeatCount();
    public void setRepeatCount(String repeatCount)
                     throws DOMException;

    /**
     *  The  repeatDur causes the element to play repeatedly (loop) for the 
     * specified duration in milliseconds. Negative means "indefinite". 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public Time getRepeatDur();
    public void setRepeatDur(MyFloat repeatDur)
                     throws DOMException;


	/**
	 * 
	 *  The  repeat causes the element to play repeatedly (loop) for the 
	 * specified duration in milliseconds. 
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public MyFloat getRepeat();
	public void setRepeat(MyFloat repeatDur)
	              throws DOMException;

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMin();
	public void setMin(Time min)
	               throws DOMException;

	/**
	 *  The minimum play time for this element in milliseconds. Negative means "indefinite".
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public Time getMax();
	public void setMax(Time max)
	              throws DOMException;

	//// public methods ////
	/** 
	 * PREFETCH - prefetching the media
	 * This prefetches the element for playing. It set's the element to
	 * 'prefetched' state.
	 */
	public void prefetch();
	
	/** 
	 * STARTUP - getting the first interval
	 * This initializes the element for playing. It set's the element to  
	 * 'wait' state.
	 */
	public void startup();
	
	/**
	 * ACTIVE TIME - playing an interval.
	 * This is called to activate the element - after begin time has been reached.
	 * This sets the element to 'play' state.
	 */
	public void activate();

	/** 
	 * This is called to freeze the element
	 */
	public void freeze();
		
	/** 
	 * END OF AN INTERVAL
	 * This is called to deactivate the element - after the end time has been reached.
	 * This will search for the next interval and call activate, or set the element to 'fill' state.
	 */
	public void deactivate();

	/**
	 * This closes this element. The element will not be active or show fill behaviour.
	 * It will be in 'idle' state.
	 */
	public void closedown();

	/**
	 * This uninitializes the element. All memory should be freed and the element cannot be
	 * used anymore after calling this method.
	 */
	public void destroy();

	/**
	 * Get the current interval begin time. This is used to calculate the parent time.
	 */
	public Time getCurrentIntervalBegin();

	/**
	 * Returns time in body time space,
	 * @param t		Time to convert, in millisecs
	 * @return 		time in body time, in millisecs
	 */
	public long getTimeInBodyTime(long t);

	/**
	 * @return	true if this node is active.
	 */
	public boolean isActive();	
	/**
	 * @return	true if this node has ever started.
	 */
	public boolean hasStarted();
}

