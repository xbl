
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares customTest element.
 *  
 * Defines attributes:
 *  o 
 *  + defaultState, override, uid
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILCustomTestElement extends SMILElement {
    /**
	 * defaultState
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getDefaultState();
    public void setDefaultState(boolean defaultState)
                                      throws DOMException;

	// fillTypes
	public static final short OVERRIDE_HIDDEN               = 0;
	public static final short OVERRIDE_VISIBLE              = 1;

	/**
	 * override
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getOverride();
	public void setOverride(short override) throws DOMException;

	/**
	 * uid
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getUid();
	public void setUid(String uid) throws DOMException;

}