
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares transition element.
 *  
 * Defines attributes:
 *  o 
 *  + type, subtype, startProgress, endProgress, direction, fadeColor, horzRepeat, vertRepeat, 
 *    borderWidth, borderColor, coordinated, clipBoundary
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILTransitionElement extends SMILElement {
    /**
     *  See the  type attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getType();
    public void setType(String type) throws DOMException;

    /**
     *  See the subtype attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSubType();
    public void setSubType(String subType) throws DOMException;

    /**
     *  See the startProgress attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getStartProgress();
    public void setStartProgress(String startProgress) throws DOMException;

    /**
     *  See the endProgress attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getEndProgress();
    public void setEndProgress(String endProgress) throws DOMException;

    // direction Types
    public static final short DIRECTION_FORWARD               = 0;
    public static final short DIRECTION_REVERSE               = 1;

    /**
     *  A code representing the value for the direction attribute.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getDirection();
    public void setDirection(short direction) throws DOMException;

    /**
     *  See the fadeColor attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getFadeColor();
    public void setFadeColor(String fadeColor) throws DOMException;

    /**
     *  See the horzRepeat attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getHorzRepeat();
    public void setHorzRepeat(int horzRepeat) throws DOMException;

    /**
     *  See the vertRepeat attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getVertRepeat();
    public void setVertRepeat(int vertRepeat) throws DOMException;

    /**
     *  See the borderWidth attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getBorderWidth();
    public void setBorderWidth(int borderWidth) throws DOMException;

    /**
     *  See the borderColor attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBorderColor();
    public void setBorderColor(String borderColor) throws DOMException;

    /**
     *  See the coordinated attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getCoordinated();
    public void setCoordinated(boolean coordinated) throws DOMException;

    // clipBoundary Types
    public static final short CLIPBOUNDARY_CHILDREN               = 0;
    public static final short CLIPBOUNDARY_PARENT                 = 1;

    /**
     *  A code representing the value for the clipBoundary attribute.
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getClipBoundary();
    public void setClipBoundary(short clipBoundary) throws DOMException;

}