
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;


/**
 *  Declares an element.
 *  
 * Defines attributes:
 *  o 
 *  + href, sourceLevel, destinationLevel, sourcePlaystate, destinationPlaystate, show, accessKey, 
 *    tabindex, target, external, actuate
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILAElement extends SMILElement, XElementBasicTime {
    /**
     *  See the href attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getHref();
    public void setHref(String href) throws DOMException;

    /**
     *  See the sourceLevel attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getSourceLevel();
    public void setSourceLevel(String sourceLevel) throws DOMException;

    /**
     *  See the destinationLevel attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getDestinationLevel();
    public void setDestinationLevel(String destinationLevel) throws DOMException;

    // sourcePlaystate Types
    public static final short SOURCEPLAYSTATE_PLAY                 = 0;
    public static final short SOURCEPLAYSTATE_PAUSE                = 1;
    public static final short SOURCEPLAYSTATE_STOP                 = 2;

    /**
     *  See the sourcePlaystate attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getSourcePlaystate();
    public void setSourcePlaystate(short sourcePlaystate) throws DOMException;

    // destinationPlaystate Types
    public static final short DESTINATIONPLAYSTATE_PLAY                 = 0;
    public static final short DESTINATIONPLAYSTATE_PAUSE                = 1;
    public static final short DESTINATIONPLAYSTATE_STOP                 = 2;

    /**
     *  See the destinationPlaystate attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getDestinationPlaystate();
    public void setDestinationPlaystate(short destinationPlaystate) throws DOMException;

    // show Types
    public static final short SHOW_REPLACE              = 0;
    public static final short SHOW_NEW                  = 1;
    public static final short SHOW_PAUSE                = 2;

    /**
     *  See the show attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getShow();
    public void setShow(short show) throws DOMException;

    /**
     *  See the accesskey attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getAccessKey();
    public void setAccessKey(String accesskey) throws DOMException;

    /**
     *  See the tabindex attribute in LinkingAttributes module. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public int getTabindex();
    public void setTabindex(int tabindex) throws DOMException;

    /**
     *  See the target attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getTarget();
    public void setTarget(String target) throws DOMException;

    /**
     *  See the external attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public boolean getExternal();
    public void setExternal(String external) throws DOMException;

    // actuate Types
    public static final short ACTUATE_ONREQUEST              = 0;
    public static final short ACTUATE_ONLOAD                 = 1;

    /**
     *  See the actuate attribute. 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getActuate();
    public void setActuate(short actuate) throws DOMException;

}