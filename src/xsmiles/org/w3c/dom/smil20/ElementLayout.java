/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  This interface is used by SMIL elements root-layout, top-layout and region.
 *  
 * Defines attributes:
 *  o title, backgroundColor, height, width
 *  + -title
 *
 * Defines methods:
 *  o
 *  +
 */
public interface ElementLayout {
    /**
	 * KP: REMOVED, DFINED IN SMILElement
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    //public String getTitle();
    //public void setTitle(String title)
    //                                  throws DOMException;

    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getBackgroundColor();
    public void setBackgroundColor(String backgroundColor)
                                      throws DOMException;

    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getHeight();
    public void setHeight(int height)
                                      throws DOMException;

    /**
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public String getWidth();
    public void setWidth(int width)
                                      throws DOMException;

}

