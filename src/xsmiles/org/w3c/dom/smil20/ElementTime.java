/*
 * Copyright (c) 2000 World Wide Web Consortium,
 * (Massachusetts Institute of Technology, Institut National de
 * Recherche en Informatique et en Automatique, Keio University). All
 * Rights Reserved. This program is distributed under the W3C's Software
 * Intellectual Property License. This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See W3C License http://www.w3.org/Consortium/Legal/ for more
 * details.
 */

package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;

/**
 *  This interface defines the set of timing attributes that are common to all 
 * timed elements. 
 */
public interface ElementTime extends XElementBasicTime {

    // syncBehavior types
    public static final short SYNCBEHAVIOR_DEFAULT                   = 0;
    public static final short SYNCBEHAVIOR_CAN_SLIP                  = 1;
    public static final short SYNCBEHAVIOR_LOCKED                    = 2;
    public static final short SYNCBEHAVIOR_INDEPENDENT               = 3;

    /**
     *  SyncBehavior 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getSyncBehavior();
    public void setSyncBehavior(short syncBehaviour)
                     throws DOMException;

    /**
     *  SyncTolerance - TODO: Correct return type?
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getSyncTolerance();
    public void setSyncTolerance(short syncTolerance)
                     throws DOMException;

	// syncBehaviorDefault types
	public static final short SYNCBEHAVIORDEFAULT_INHERIT                   = 0;
	public static final short SYNCBEHAVIORDEFAULT_CAN_SLIP                  = 1;
	public static final short SYNCBEHAVIORDEFAULT_LOCKED                    = 2;
	public static final short SYNCBEHAVIORDEFAULT_INDEPENDENT               = 3;

    /**
     *  SyncBehaviorDefault
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getSyncBehaviorDefault();
    public void setSyncBehaviorDefault(short syncToleranceDefault)
                     throws DOMException;

	/**
	 *  SyncToleranceDefault - TODO: Return value?
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public short getSyncToleranceDefault();
	public void setSyncToleranceDefault(short syncToleranceDefault)
	              throws DOMException;

    // restartDefaultTypes
    public static final short RESTARTDEFAULT_INHERIT           = 0;
    public static final short RESTARTDEFAULT_NEVER             = 1;
    public static final short RESTARTDEFAULT_WHEN_NOT_ACTIVE   = 2;
    public static final short RESTARTDEFAULT_ALWAYS            = 3;

    /**
     *  A code representing the value of the  restart attribute, as defined 
     * above. Default value is <code>RESTART_ALWAYS</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getRestartDefault();
    public void setRestartDefault(short restartDefault)
                     throws DOMException;

    // fillDefaultTypes
    public static final short FILLDEFAULT_INHERIT              = 0;
    public static final short FILLDEFAULT_REMOVE               = 1;
    public static final short FILLDEFAULT_FREEZE               = 2;
    public static final short FILLDEFAULT_HOLD                 = 3;
    public static final short FILLDEFAULT_TRANSITION           = 4;
    public static final short FILLDEFAULT_AUTO                 = 5;

    /**
     *  A code representing the value of the  fill attribute, as defined 
     * above. Default value is <code>FILL_REMOVE</code> . 
     * @exception DOMException
     *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
     */
    public short getFillDefault();
    public void setFillDefault(short fillDefault)
                     throws DOMException;

}

