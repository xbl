
/*
 *
 * Extension to SMIL DOM.
 *
 */
package org.w3c.dom.smil20;

import org.w3c.dom.DOMException;


/**
 *  Declares smil element.
 *  
 * Defines attributes:
 *  o 
 *  + 
 *
 * Defines methods:
 *  o
 *  +
 */
public interface XSMILSMILElement extends SMILElement {


}