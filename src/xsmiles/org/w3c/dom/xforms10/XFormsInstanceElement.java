
package org.w3c.dom.xforms10;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 */
public interface XFormsInstanceElement extends XFormsElement { 
	public Node getInstanceRoot();
}
