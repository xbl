package org.w3c.dom.xforms10;

import fi.hut.tml.xsmiles.dom.StylableElement;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

/**
 */
public interface XFormsElement extends StylableElement,Element{

	// Init this element
	public void init();

	// Destroy this element
	public void destroy();

	/**
	*  The unique id.
	* @exception DOMException
	*    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	*/
	public String getId();
	public void setId(String id) throws DOMException;

	/**
	 *  The class attribute. This method should be called getClass, but it is 
	 * already reserved by java.lang.Object.
	 * @exception DOMException
	 *    NO_MODIFICATION_ALLOWED_ERR: Raised if this attribute is readonly. 
	 */
	public String getClassName();
	public void setClassName(String cl) throws DOMException;


}

