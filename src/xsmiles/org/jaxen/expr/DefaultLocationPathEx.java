/*
 * X-Smiles : copied from jaxen 1.1 CVS snapshot 2004-04-15
 * CHANGES MARKED WITH X-SMILES
 * -MH 
 *//*
 * $Header: /cvs/xsmiles/src/xsmiles/org/jaxen/expr/DefaultLocationPathEx.java,v 1.3 2004/04/21 14:22:48 honkkis Exp $
 * $Revision: 1.3 $
 * $Date: 2004/04/21 14:22:48 $
 *
 * ====================================================================
 *
 * Copyright (C) 2000-2002 bob mcwhirter & James Strachan.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions, and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions, and the disclaimer that follows
 *    these conditions in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. The name "Jaxen" must not be used to endorse or promote products
 *    derived from this software without prior written permission.  For
 *    written permission, please contact license@jaxen.org.
 *
 * 4. Products derived from this software may not be called "Jaxen", nor
 *    may "Jaxen" appear in their name, without prior written permission
 *    from the Jaxen Project Management (pm@jaxen.org).
 *
 * In addition, we request (but do not require) that you include in the
 * end-user documentation provided with the redistribution and/or in the
 * software itself an acknowledgement equivalent to the following:
 *     "This product includes software developed by the
 *      Jaxen Project (http://www.jaxen.org/)."
 * Alternatively, the acknowledgment may be graphical using the logos
 * available at http://www.jaxen.org/
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE Jaxen AUTHORS OR THE PROJECT
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * ====================================================================
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Jaxen Project and was originally
 * created by bob mcwhirter <bob@werken.com> and
 * James Strachan <jstrachan@apache.org>.  For more information on the
 * Jaxen Project, please see <http://www.jaxen.org/>.
 *
 * $Id: DefaultLocationPathEx.java,v 1.3 2004/04/21 14:22:48 honkkis Exp $
 */
package org.jaxen.expr;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.jaxen.Context;
import org.jaxen.JaxenException;

import fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen.LookupCallback;

abstract class DefaultLocationPathEx extends DefaultLocationPath implements LocationPath
{
    private final static Object PRESENT = new Object();
    protected LookupCallback referredNodes;
    public DefaultLocationPathEx(LookupCallback ref) // x-smiles
    {
        super();
        //this.steps = new LinkedList();
        this.referredNodes = ref; // x-smiles
    }

    public Object evaluate(Context context) throws JaxenException
    {
        List nodeSet = context.getNodeSet();
        List contextNodeSet = new ArrayList(nodeSet.size());
        contextNodeSet.addAll(nodeSet);

        Iterator stepIter = getSteps().iterator();
        Step eachStep = null;
        while ( stepIter.hasNext() )
        {
            eachStep = (Step) stepIter.next();
            Context stepContext = new Context(context.getContextSupport());
            stepContext.setNodeSet(contextNodeSet);
            contextNodeSet = eachStep.evaluate(stepContext);
        }
        // x-smiles mod start
        if (referredNodes!=null)
        {
            referredNodes.nodesReferenced(contextNodeSet);
	        //System.out.println("contextNodeSet: "+contextNodeSet);
        }
        // x-smiles mod end

        return contextNodeSet;
    }

}

