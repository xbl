#!/bin/sh
#uncomment this to run the original user interface
#java -Xmx200m -Djava.endorsed.dirs=lib/endorsed -cp $CLASSPATH:xsmiles.jar fi.hut.tml.xsmiles.Xsmiles

# the new UI
export LD_LIBRARY_PATH=/home/honkkis/bin/jmp/lib:$LD_LIBRARY_PATH
java -Xrunjmp:nomonitors,nomethods,filter=fi.hut.tml.xsmiles -Xmx20m  -Djava.endorsed.dirs=lib/endorsed -Dswing.aatext=true -cp $CLASSPATH:xsmiles.jar fi.hut.tml.xsmiles.gui.gui2.swing.KickStartSwing "$@"

#no profiler
#java -Xmx15m -Dorg.xsmiles.xpath.engine=fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen.JaxenXPathEngine -Djava.endorsed.dirs=lib/endorsed -Dswing.aatext=true -cp $CLASSPATH:xsmiles.jar fi.hut.tml.xsmiles.gui.gui2.KickStart "$@"


# awt gui
#java -Xmx12m -Xrunjmp:nomonitors,nomethods,filter=fi.hut.tml.xsmiles -Dorg.xsmiles.xpath.engine=fi.hut.tml.xsmiles.mlfc.xforms.xpath.jaxen.JaxenXPathEngine -Djava.endorsed.dirs=lib/endorsed -Dswing.aatext=true -cp $CLASSPATH:xsmiles.jar fi.hut.tml.xsmiles.gui.gui2.KickStart "$@"

