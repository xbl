#!/usr/bin/perl
#
print <<END;
-----------------------------------------------
X-Smiles Startup Script for development version
-----------------------------------------------
END

# The directory where build exists
$build="../build";

$ENV{'CLASSPATH'}="$ENV{'CLASSPATH'}:$build";

foreach(`pwd`) {
    chop;
    $pwd=$_;
    last;
}

$cp=".";

print "Setting up \$CLASSPATH:\n";
foreach(`ls lib |grep .jar`) {
    chomp;
    $cp="$cp:$pwd/lib/$_";

}
$ENV{'CLASSPATH'}="$ENV{'CLASSPATH'}:$cp";
print "-> $ENV{'CLASSPATH'}";
print "\n";

print "Looking for JMF:\n";

foreach(`echo $CLASSPATH|grep jmf.jar`) {
    print "-> JMF already in \$CLASSPATH\n";
    $jmf="found";
    last;
}

# If JMF not found, then look for it
if($jmf ne "found") {
    foreach(`locate jmf.jar`) {
	chomp;
	#print $_;
	$ENV{'CLASSPATH'}="$ENV{'CLASSPATH'}:$_";
	print "-> JMF added to \$CLASSPATH\n";
	last;
    }
}



print "Looking for installed jdk's:\n";


foreach(`locate /bin/java |awk '{if(\$1~/.*a\$/) print \$0;}'`) {
  chomp;
  if(/j2sdk/ && /1.4.0/) {
    print "-> found: ";
    print $_;
    print "\n";
    `$_ fi.hut.tml.xsmiles.Xsmiles -log`;
    exit;
    }
}

foreach(`locate /bin/java |awk '{if(\$1~/.*a\$/) print \$0;}'`) {
  chomp;
    if(/1.2/||/1.3/||/1.4/) {
      print "-> found: ";
      print $_;
      print "\n";
      `$_ fi.hut.tml.xsmiles.Xsmiles -log`;
	exit;
    }
}

print "No Java Runtime Environment Found.\nIf you have one, try manually with:\njava -jar xsmiles.jar";










