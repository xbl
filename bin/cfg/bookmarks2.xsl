<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xt="http://www.jclark.com/xt" xmlns:xfm="http://www.w3.org/2000/12/xforms" version="1.0" extension-element-prefixes="xt">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/*">
		<fo:root>
			<fo:layout-master-set>
				<!-- layout for the first page -->
				<fo:simple-page-master master-name="first" page-height="{/*/@height}" page-width="18cm" margin-top="1cm" margin-bottom="2cm" margin-left="2.5cm" margin-right="2.5cm">
					<fo:region-before extent="3cm"/>
					<fo:region-body margin-top="3cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
				<!-- layout for the other pages -->
				<fo:simple-page-master master-name="rest" height="{/*/@height}" page-width="18cm" margin-top="1cm" margin-bottom="2cm" margin-left="2.5cm" margin-right="2.5cm">
					<fo:region-before extent="2.5cm"/>
					<fo:region-body margin-top="2.5cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
				<fo:page-sequence-master master-name="basicPSM">
					<fo:repeatable-page-master-alternatives>
						<fo:conditional-page-master-reference master-name="first" page-position="first"/>
						<fo:conditional-page-master-reference master-name="rest" page-position="rest"/>
						<!-- recommended fallback procedure -->
						<fo:conditional-page-master-reference master-name="rest"/>
					</fo:repeatable-page-master-alternatives>
				</fo:page-sequence-master>
			</fo:layout-master-set>
			<fo:page-sequence master-name="basicPSM">
				<!-- header -->
				<fo:static-content flow-name="xsl-region-before">
					<fo:block text-align="end" font-size="10pt" font-family="serif" line-height="14pt">
						<xsl:value-of select="@title"/> - p. <fo:page-number/>
					</fo:block>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body">
					<fo:block font-size="20pt" line-height="24pt" font-family="sans-serif" space-after.optimum="15pt" background-color="blue" color="white" text-align="center">
				My bookmarks
			</fo:block>
					<xsl:apply-templates select="section"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<!-- *************** section *******************-->
	<xsl:template match="section">
		<fo:block font-size="18pt" font-family="sans-serif" space-before.optimum="20pt" space-after.optimum="15pt" background-color="blue" color="white" text-align="center">
			<xsl:value-of select="@name"/>
		</fo:block>
		<xsl:apply-templates select="bookmark"/>
	</xsl:template>
	<!-- *************** bookmark *******************-->
	<xsl:template match="bookmark">
		<!-- Normal text -->
		<xsl:choose>
			<xsl:when test="not (@href = '')">
				<fo:block color="blue" font-size="14pt" space-after.optimum="3pt">
					<fo:basic-link external-destination="{@href}">
						<xsl:value-of select="@name"/>
					</fo:basic-link>
				</fo:block>
			</xsl:when>
			<xsl:otherwise>
				<fo:block color="black" font-size="14pt" space-after.optimum="3pt">
					<xsl:value-of select="@name"/>
				</fo:block>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
