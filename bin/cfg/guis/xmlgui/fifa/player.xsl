<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/*">
    <html>
      <head>
	<link rel="stylesheet" type="text/css" href="player.css" />
      </head>
      <body>
	<p>
	  <xsl:apply-templates select="img|name|nationality|weight|height|goals|birthdate|caps|club"/>
	</p>
	<p>
	  <xsl:apply-templates select="bio"/>
	</p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="name">
     <b><xsl:value-of select="."/></b><br/>
  </xsl:template>

  <xsl:template match="img">
    <img src="{@src}" alt=""/><br/>
  </xsl:template>

  <xsl:template match="nationality"><b>Nationality:</b> <xsl:value-of select="."/><br/></xsl:template>

  <xsl:template match="position"><b>Position: </b><xsl:value-of select="."/><br/></xsl:template>

  <xsl:template match="height"><b>Height: </b><xsl:value-of select="."/><br/></xsl:template>

  <xsl:template match="weight">
      <b>Weight: </b><xsl:value-of select="."/><br/>
  </xsl:template>

  <xsl:template match="goals">
      <b>Goals: </b><xsl:value-of select="."/><br/>
  </xsl:template>

  <xsl:template match="caps">
      <b>Caps: </b><xsl:value-of select="."/><br/>
  </xsl:template>

  <xsl:template match="club">
      <b>Club: </b><xsl:value-of select="."/><br/>
  </xsl:template>

  <xsl:template match="birthdate">
    <b>Birthdate: </b><xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="bio">
      <xsl:value-of select="."/>
  </xsl:template>
</xsl:stylesheet>
