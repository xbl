#!/usr/bin/perl
$file=$ARGV[0];

foreach(`cat $file`) {
  if(/([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*)/) {
    $reg=$1;
    $left=$2;
    $top=$3;
    $width=$4;
    $height=$5;
    chomp($height);
    print "<region id=\"$reg\" height=\"$height\" top=\"$top\" z-index=\"3\" width=\"$width\" left=\"$left\"/>\n";
  }
  
}
