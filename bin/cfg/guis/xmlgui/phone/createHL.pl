#!/usr/bin/perl
$file=$ARGV[0];

foreach(`cat $file`) {
  if(/([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*)/) {
    $region=$1;
    $left=$2;
    $top=$3;
    $width=$4;
    $height=$5;
    chomp($height);
    print "<img id=\"$region" . "Norm\" src=\"$region" . ".png\" region=\"$region\" dur=\"indefinite\"/>\n<img id=\"$region" . "HL\" region=\"$region\" src=\"$region" . "_hl.png\" dur=\"1s\" begin=\"$region" . "Norm.mouseover\" end=\"$region" . "HL.mouseout\"/>\n<img id=\"$region" . "CL\" region=\"$region\" src=\"$region" . "_cl.png\" dur=\"0.3s\" begin=\"$region" . "HL.mousedown\" end=\"$region" . "CL.mouseup\"/>\n";
  }
  
}
