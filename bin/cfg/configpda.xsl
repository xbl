<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xfm="http://www.w3.org/2002/xforms" version="1.0">
  <xsl:import href="./config.xsl"/>  
  
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/*">
    
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <xform id="form1" xmlns="http://www.w3.org/2000/12/xforms">
	<submission    id="submit1"
		    method="put"
		    action="./config.xml"
		    />
	<instance>
	  <xsl:processing-instruction name="xml-stylesheet">
	    alternate="yes" title="PDA" type="text/xsl" href="configpda.xsl"
	  </xsl:processing-instruction>
	  <xsl:processing-instruction name="xml-stylesheet">
	    title="desktop" type="text/xsl" href="config.xsl"
	  </xsl:processing-instruction>
	  <xsl:copy-of select="."/>
	</instance>
      </xform>
      <fo:layout-master-set>
	
	<fo:page-sequence-master master-name="basicPSM">
	  <fo:repeatable-page-master-alternatives>
	    <fo:conditional-page-master-reference master-name="first" page-position="first"/>
	  </fo:repeatable-page-master-alternatives>
	</fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-name="basicPSM">
	<!-- header, no header -->
	
	
	<fo:flow flow-name="xsl-region-body">
	  <fo:block font-size="28pt" font-family="sans-serif" line-height="30pt" space-after.optimum="2pt" 				color="black" padding-top="3pt">
	    Configuration
	  </fo:block>
	  <fo:block font-size="11pt" font-family="serif" line-height="10pt" space-after.optimum="35pt" color="black" text-align="left" padding-top="3pt">
	    Page <fo:page-number/>
	  </fo:block>
	  
	  <xsl:call-template name="configStuph">
	    <xsl:with-param name="columns">15</xsl:with-param>
	  </xsl:call-template>
	  
	</fo:flow>
      </fo:page-sequence>
    </fo:root>
    
  </xsl:template>
</xsl:stylesheet>
