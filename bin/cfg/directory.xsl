<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xt="http://www.jclark.com/xt" version="1.0" extension-element-prefixes="xt">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/*">
    
    <!-- This stylesheet converts X-Smiles directory tree (URL: "dir:/d:/") into html -->
    
    <!-- ************** Directory protocol stylesheet *******************-->
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
	<title>
	  <xsl:value-of select="name"/>
	</title>
      </head>
      <body>
	<!-- Clicking folder name refreshes it -->
	
	<h2>
	<!--<img src="file:/home/jvierine/research/xsmiles/bin/cfg/dir/bigFolder.gif"/>-->
	<br/>   Current: <a><xsl:attribute name="href">dir:/<xsl:value-of select="name"/></xsl:attribute><xsl:value-of select="name"/></a></h2>

	<p>
	  <xsl:apply-templates select="dir|file|parent|error"/>
	</p>
<br/><br/>
	<h3>Total files:<xsl:value-of select="count(dir)+count(file)"/></h3>
      </body>
    </html>
    
  </xsl:template>
  
  <!-- *************** parent *******************-->
  <xsl:template match="parent">
    <b>
    <!--
<img src="http://www.xsmiles.org/images/folder.gif"/>'
-->
<a>

	  <xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>.. Parent</a></b><br/>
  </xsl:template>
  
  <!-- *************** dir *******************-->
  <xsl:template match="dir">
    <p>
    <b>
    <!--
<img src="http://www.xsmiles.org/images/folder.gif"/>  
-->
<a>

	  <xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>

	  <xsl:value-of select="."/> (dir)</a></b>
     </p>
  </xsl:template>
  
  <!-- *************** file *******************-->
  <xsl:template match="file">
    <p>
    <!--
    <img src="http://www.xsmiles.org/images/file.gif"/>
    -->
<a>

	<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
	<xsl:value-of select="."/>
      </a><br/>
      </p>
  </xsl:template>

  <!-- *************** error *******************-->
  <xsl:template match="error">
   <h3> ERROR: <xsl:value-of select="."/></h3><br/>
  </xsl:template>
  
</xsl:stylesheet>
