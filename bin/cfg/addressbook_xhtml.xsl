<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xfm="http://www.w3.org/2000/12/xforms" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/*">
    <html:html>
        <html:head>
            <html:link href="controls.css" rel="stylesheet" type="text/css"/>
            <html:style type="text/css">               
                .section {background-color: #cccccc; border-width:2px; margin-bottom: 10px; padding-bottom: 5px;}
                h1 {font-size:18px; color:blue; margin-bottom: 8px;}
                h2 {align: center;background-color:blue; color:white; margin-bottom: 3px; padding-left: 5px;margin-top:0px;}
                .address {padding-left: 10px; font-weight: bold; margin-bottom: 1px; margin-top: 1px;}
            </html:style>
        </html:head>
        <html:body>
            <html:h1>Address Book</html:h1>
            <xsl:apply-templates select="document('addressbook2.xml')/addressbook/user"/>
        </html:body>
    </html:html>
    </xsl:template>
    <xsl:template match="user">
        <html:p class="address"><a href="{@address}" target="documentwindow"><xsl:value-of select="@name"/></a>
        </html:p>
    </xsl:template>
</xsl:stylesheet>
