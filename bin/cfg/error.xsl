<xsl:stylesheet xmlns="http://www.w3.org/2001/SMIL20/Language" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
<xsl:template match="/*">
	<!-- <?xml-stylesheet type="text/css"  href="#cssstyle"?> -->
	<xsl:processing-instruction name="xml-stylesheet">href="#style" type="text/css"</xsl:processing-instruction>
  <smil xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xfm="http://www.w3.org/2002/xforms">
  <head>
  	<style type="text/css" id="style">
  		input::value,textarea::value {background-color:#eeeeee;}
  		input label,output label {caption-side:left;}
  		textarea label {caption-side:top;}
  		textarea::value {width: 900px;height: 270px;font-size:9pt;}
  		textarea.additional::value{width:700px;font-size:10pt;}
  	</style>
    <layout>
      <root-layout title="ERROR" width="700" height="720" id="errorPageId" background-color="#FFFFFF"/>
      <region id="documentURLReg" height="25" top="50" z-index="2" width="1000" left="10" fit="fill"/>
	      <region id="titleReg" height="40" top="10" z-index="2" width="1000" left="10" fit="meet"/>
	      <region id="goReg" height="30" top="45" z-index="2" width="100" left="630" fit="meet"/>
      <region id="descriptionReg" fit="fill" left="10" top="75" width="700" height="300" z-index="2"/>

      <region id="coffeeReg" fit="fill" left="500" top="80" width="300" height="273" z-index="1"/>

      <region id="submitTitleReg" height="40" top="370" z-index="2" width="400" left="10" fit="meet"/>

      <region id="nameReg" height="50" top="410" z-index="2" width="700" left="10" fit="fill"/>
      <region id="additionalReg" height="200" top="470" z-index="2" width="700" left="10" fit="fill"/>
      <region id="submitReg" height="30" top="670" z-index="2" width="200" left="10" fit="fill"/>
    </layout>    <xfm:model id="form1">
      <xfm:submission id="submit1" method="post" action="http://sinex.tml.hut.fi/cgi-bin/errors.cgi"/>
      <xfm:schema/>
      <xfm:instance id="instance1" xmlns="">
      <errordata>
          <!-- these we get automatically -->
 	  <title>
		<xsl:apply-templates select="title"/>
	  </title>

          <description>
             <xsl:apply-templates select="description"/>
             <xsl:apply-templates select="stacktrace"/>
          </description> 

	  <stacktrace></stacktrace>

          <version> 
             <xsl:apply-templates select="version"/>
          </version>
          <documentURL> 
             <xsl:apply-templates select="documentURL"/>
          </documentURL>


	  <!-- way cool would be log related with error -->
          <log></log>

	  <name>anonymous@anon.com</name>

          <additionalInfo>no additional info</additionalInfo> 
	</errordata>
      </xfm:instance>
    </xfm:model>

    <meta name="title" content="ERROR"/>


  </head>
  
  <body>
    <par>

       <text region="titleReg" id="titleId" src="data:text/html,&lt;html&gt;&lt;body style=&quot; font-size: 30px; font-weight: bold; font-family: Impact; color: black &quot;&gt;{/error/title}!&lt;/body&gt;&lt;/html&gt;" dur="indefinite"/>
              <xfm:input region="documentURLReg" ref="/errordata/documentURL">
         <xfm:label>At:</xfm:label>
       </xfm:input>
              <xfm:trigger region="goReg" >
         <xfm:label>Reload</xfm:label>
         <xfm:load ref="/errordata/documentURL" ev:event="DOMActivate"/>
       </xfm:trigger>


       <xfm:textarea fit="meet" region="descriptionReg"  ref="/errordata/description">
         <xfm:label>Error description:</xfm:label>
       </xfm:textarea>

       <animate attributeName="width" begin="2s" dur="0.2s" from="900" id="anim" targetElement="descriptionReg" to="550" fill="freeze"/> 
       
       <xfm:textarea region="additionalReg" ref="/errordata/additionalInfo" class="additional">
         <xfm:label>Additional description:</xfm:label>
       </xfm:textarea>

       <xfm:input region="nameReg"  ref="/errordata/name">
         <xfm:label>Your e-mail:</xfm:label>
       </xfm:input>
       
       <img dur="indefinite" id="coffee" region="coffeeReg" src="http://www.xsmiles.org/error4.jpg"/>

       <text region="submitTitleReg" id="titleId" src="data:text/html,&lt;html&gt;&lt;body style=&quot; font-size: 30px; font-weight: bold; font-family: Impact; color: black &quot;&gt;Submit bug report&lt;/body&gt;&lt;/html&gt;" dur="indefinite"/>

<!--       <xfm:input region="errorTitleReg" style="width: 490px;" ref="/errordata/title">
         <xfm:label>Error title:</xfm:label>
       </xfm:input> -->

       <xfm:submit region="submitReg">
         <xfm:label>Submit bug</xfm:label>
       </xfm:submit>
   

    </par>
  </body>
</smil>
</xsl:template>


	<xsl:template match="title">
			<xsl:value-of select="."/>
	</xsl:template>

	<!-- *************** description *******************-->
	<xsl:template match="description|stacktrace|documentURL">
			<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>
