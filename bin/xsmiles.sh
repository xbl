#!/bin/sh
#uncomment this to run the original user interface
#java -Xmx200m -Djava.endorsed.dirs=lib/endorsed -cp $CLASSPATH:xsmiles.jar fi.hut.tml.xsmiles.Xsmiles

# the new UI
java -Xmx200m -Djava.endorsed.dirs=lib/endorsed -Dswing.aatext=true -cp $CLASSPATH:xsmiles.jar fi.hut.tml.xsmiles.gui.gui2.swing.KickStartSwing "$@"

